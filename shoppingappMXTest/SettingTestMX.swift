//
//  SettingTestMX.swift
//  shoppingappMXTest
//
//  Created by Marcos on 21/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

@testable import shoppingappMX

class SettingTestMX: XCTestCase {

    struct UnexpectedNilError: Error {}

    var promotion: PromotionBO!

    override func setUp() {
        super.setUp()
        let promotionsBO = PromotionsGenerator.getPromotionsOneDayLeftBO()
        promotion = promotionsBO.promotions[0]
    }

    override func tearDown() {
        super.tearDown()
    }

    // MARK: Promotions

    func test_givenAPromotionBOThatStartIn1Days_whenICallEffectiveDateText_thenTextIsInitAndIsNotInterval() throws {

        //given
        promotion.startDate = Calendar.current.date(byAdding: .day, value: 1, to: Date())
        promotion.endDate = Calendar.current.date(byAdding: .day, value: 7, to: Date())
        let formattedDate = promotion.startDate.string(format: Date.DATE_FORMAT_DAY_MONTH_SHORT)

        //when
        let outputText = Settings.Promotions.effectiveDateText(forPromotionBO: promotion, compactSize: false)

        //then
        XCTAssert(outputText?.date == Localizables.promotions.key_promotions_init_date +  " " + formattedDate.capitalized)
        XCTAssert(outputText?.isInterval == false)

    }

    func test_givenAPromotionBOThatYetStarted_whenICallEffectiveDateText_thenTextIsFinishAndIsNotInterval() throws {

        //given
        promotion.startDate = Date()
        promotion.endDate = Calendar.current.date(byAdding: .day, value: 7, to: Date())
        let formattedDate = promotion.endDate.string(format: Date.DATE_FORMAT_DAY_MONTH_SHORT)

        //when
        let outputText = Settings.Promotions.effectiveDateText(forPromotionBO: promotion, compactSize: false)

        //then
        XCTAssert(outputText?.date == Localizables.promotions.key_promotions_finish_date +  " " + formattedDate.capitalized)
        XCTAssert(outputText?.isInterval == false)

    }
    
    // MARK: - localizationKeyForHelpOnOffAndCard
    
    func test_givenAnOnCard_whenIGetLocalizableKey_thenRootKeysShouldMatch() {
        
        // given
        let cardEntity = CardsGenerator.getCardWithCardTypeRelatedCard(withCardType: "CREDIT_CARD").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)
        
        // when
        let keyRoot = Settings.CardHelpOnOff.localizationKeyForHelpOnOffAndCard(card: cardBO)
        
        // then
        XCTAssert("KEY_CARD_HELP_OFF_" == keyRoot)
    }
    
    func test_givenAnOffCard_whenICreateOnOffHelpDisplayData_thenRootKeysShouldMatch() {
        
        // given
        let cardEntity = CardsGenerator.getCardWithCardTypeRelatedCard(withCardType: "CREDIT_CARD").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)
        cardBO.activations.removeAll()
        
        // when
        let keyRoot = Settings.CardHelpOnOff.localizationKeyForHelpOnOffAndCard(card: cardBO)
        
        // then
        XCTAssert("KEY_CARD_HELP_ON_" == keyRoot)
        
    }

}
