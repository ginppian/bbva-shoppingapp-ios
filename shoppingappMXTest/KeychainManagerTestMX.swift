//
//  KeychainManagerTestMX.swift
//  shoppingappMXTest
//
//  Created by Javier Dominguez on 13/03/2019.
//  Copyright © 2019 BBVA. All rights reserved.
//

import XCTest

@testable import shoppingappMX

class KeychainManagerTestMX: XCTestCase {
    
    override func setUp() {
        super.setUp()
        
        KeychainManagerTestHelper.configureKeychainManagerForTesting()
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    // MARK: - isUserSaved
    
    func test_givenSavedUserId_whenICallIsUserSaved_thenIGetTrue() {
        
        //given
        KeychainManager.shared.saveUserId(forUserId: "userId")
        
        //when
        let isSaved = KeychainManager.shared.isUserSaved()
        
        //then
        XCTAssert(isSaved == true)
    }
    
    func test_givenNoSavedUserId_whenICallIsUserSaved_thenIGetFalse() {
        
        //given
        
        //when
        let isSaved = KeychainManager.shared.isUserSaved()
        
        //then
        XCTAssert(isSaved == false)
    }
    
    // MARK: - isUserIdEqualToLastLoggedUserId
    
    func test_givenSavedUserIdAndSameLastLoggedUserId_whenICallIsUserIdEqualToLastLoggedUserId_thenIGetTrue() {
        
        //given
        KeychainManager.shared.saveUserId(forUserId: "userId")
        KeychainManager.shared.saveLastLoggedUserId(forUserId: "userId")
        
        //when
        let isEqual = KeychainManager.shared.isUserIdEqualToLastLoggedUserId()
        
        //then
        XCTAssert(isEqual == true)
    }
    
    func test_givenSavedUserIdAndDistintLastLoggedUserId_whenICallIsUserIdEqualToLastLoggedUserId_thenIGetFalse() {
        
        //given
        KeychainManager.shared.saveUserId(forUserId: "userId")
        KeychainManager.shared.saveLastLoggedUserId(forUserId: "LastLoggedUserId")
        
        //when
        let isEqual = KeychainManager.shared.isUserIdEqualToLastLoggedUserId()
        
        //then
        XCTAssert(isEqual == false)
    }
    
    // MARK: - LastLoggedUserId
    
    func test_givenAnEmptyLastLoggedUserId_whenICallGetLastLoggedUserId_thenIGetEmptyLastLoggedUserId() {
        
        //given
        let expectedLastLoggedUserId: String = ""
        KeychainManager.shared.saveLastLoggedUserId(forUserId: expectedLastLoggedUserId)
        
        //when
        let lastLoggedUserId = KeychainManager.shared.getLastLoggedUserId()
        
        //then
        XCTAssert(lastLoggedUserId == expectedLastLoggedUserId)
    }
    
    func test_givenLastLoggedUserId_whenICallGetLastLoggedUserId_thenIGetCorrectLastLoggedUserId() {
        
        //given
        let expectedLastLoggedUserId: String = "userid"
        KeychainManager.shared.saveLastLoggedUserId(forUserId: expectedLastLoggedUserId)
        
        //when
        let lastLoggedUserId = KeychainManager.shared.getLastLoggedUserId()
        
        //then
        XCTAssert(lastLoggedUserId == expectedLastLoggedUserId)
    }
    
    // MARK: - UserId
    
    func test_givenUserId_whenICallGetUserId_thenUserIdMatch() {
        
        // given
        let expectedUserId: String = "UserId"
        KeychainManager.shared.saveUserId(forUserId: expectedUserId)
        
        // when
        let userId = KeychainManager.shared.getUserId()
        
        // then
        XCTAssert(userId == expectedUserId)
    }
    
    // MARK: - resetKeyChainItems
    
    func test_givenAny_whenICallResetKeyChainItems_thenIGetResetItems() {
        
        //given
        
        //when
        KeychainManager.shared.resetKeyChainItems()
        
        //then
        XCTAssert(KeychainManager.shared.getUserId() == "")
        XCTAssert(KeychainManager.shared.getFirstname() == "")
        XCTAssert(KeychainManager.shared.getUniqueIdentifier() == nil)
        XCTAssert(KeychainManager.shared.getLastLoggedUserId() == "")
    }
}
