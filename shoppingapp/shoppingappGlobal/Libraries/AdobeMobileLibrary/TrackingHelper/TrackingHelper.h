//
//  TrackingHelper.h
//  BBVA_Plugin
//
//  Created by Anegón Núñez, A. on 24/10/16.
//  Copyright © 2016 Accenture Digital. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "OmnitureTrackData.h"

extern NSString * const kTimeToCompleteOptionStart;
extern NSString * const kTimeToCompleteOptionStop;

@import UIKit;

/**
 Class to retrieve the variables and metrics needed
 */
@interface TrackingHelper : NSObject

@property (nonatomic, copy) NSMutableDictionary* datos_envio;


/**
 Add '0' at the prefix of the text

 @param text   where the method must add the number of '0'
 @param digits number of '0' that the method must add

 @return the result of adding '0' to the text
 */
- (NSString *)formatText:(NSString *)text digits:(NSUInteger)digits;

/**
 Retrieve the current number day of the current month with 2 digits

 @return DAY_OF_MONTH
 */
- (NSString *)getDayOfMonth;

/**
 Retrieve the number of the current month with 2 digits
 
 @return MONTH
 */
- (NSString *)getMonth;

/**
 Retrieve the number of the current year
 
 @return YEAR
 */
- (NSString *)getYear;

/**
 Retrieve the number of the current day inside the current week with 2 digits

 @return DAY_OF_WEEK
 */
- (NSString *)getDayOfWeek;

/**
 Retrieve the current date in the format YYYY-MM-DD

 @return date formatted YYYY-MM-DD
 */
- (NSString *)getDate;

/**
 Retrieve the current week inside the month formatted into 2 digits

 @return WEEK_OF_YEAR
 */
- (NSString *)getWeek;

/**
 Retrieve the hour of the current day with 2 digits
 
 @return HOUR_OF_DAY
 */
- (NSString *)getHour;

/**
 Retrieve the minute of the current day with 2 digits
 
 @return MINUTE
 */
- (NSString *)getMinute;

/**
 Retrieve the second of the current day with 2 digits
 
 @return SECOND
 */
- (NSString *)getSecond;

/**
 Retrieve the current time in the format HH:MM:SS
 
 @return time formatted HH:MM:SS
 */
- (NSString *)getTime;

/**
 Retrieve the time of the day between values, "Morning", "Afternoon", "Evening", "Late Night"

 @return the time of the day
 */
- (NSString *)getTimeOfDay;

/**
 Retrieve the date with the format "Fecha | Día | Día de la semana | mes | HH:MM:SS | Momento del día | Semana del año | año"

 @return a string with the date
 */
- (NSString *)getTimeParting;

/**
 Retrieve the previous page string

 @return string representing the previous page
 */
- (NSString *)getPrevPage;

/**
 Set the previous page string

 @param pageName the new page

 @return void
 */
- (void)setPrevPage:(NSString *)pageName ;

/**
 Retrieve the previous page type string and replace it with the new one
 
 @param pageType the new page type
 
 @return string representing the previous page type
 */
- (NSString *)getPrevPageType:(NSString *)pageType;

/**
 Retrieve the previous page section string and replace it with the new one
 
 @param pageSection the new page section
 
 @return string representing the previous page section
 */
- (NSString *)getPrevPageSection:(NSString *)pageSection;

/**
 Retrieve the initial percent of the view showing following the screen
 
 @param parentView The parent of the view
 
 @return @p CGFloat represents the percentage
 
 */


- (CGFloat)getInitialPercentPageView:(UIView *)parentView;



/**
 Retrieve the current percentage of the view showing following the screen
 
 @param parentView the parent of the view
 
 @return @p CGFloat represents the percentage
 */
- (CGFloat)getPercentPageView:(UIView *)parentView;

/**
 Retrieve the percentage viewed during the session
 
 @param parentView the parent of the view
 @param key        key to storage the value
 
 @return @p CGFloat represents the percentage
 */
- (CGFloat)getSessionPercentPageView:(UIView *)parentView key:(NSString *)key;

/**
 Retrieve the number of vertical pixels of the screen
 
 @return @p CGFloat vertical pixels
 */
- (CGFloat)getNumVerticalPixels;

/**
 Get the time from last login and return the number of days inside a string. lastVisit
 If not exists it prints "First Time"

 @return @p NSString representing the time until the last login
 */
- (NSString *)getSessionInterval;

/**
 Retrieve the time interval between start and stop timer. timeToConversion

 @param option the options start or stop

 @return the time between start and stop
 */
- (NSString *)getTimeToConversion:(NSString *)option;

/**
 Retrieve the time interval between start and stop timer. timeToExit
 
 @param option the option start /stop
 
 @return the time between start and stop
 */
- (NSString *)getTimeToExit:(NSString *)option;

/**
 Retrieve the time interval between start and stop timer. conversionWindow

 @param option start / stop

 @return the time between start and stop
 */
- (NSString *)getAppFlowCompletionTime:(NSString *)option;

/**
 Retrieve the time interval between start and stop timer. loadtime
 
 @param option start / stop
 
 @return the time between start and stop
 */
- (NSString *)getLoadTime:(NSString *)option;
/**
 Check if the user has executed the app for the first time

 @return value to know if is the first time or no
 */
- (NSString *)getReturningVisitor;

- (void)trackScreen:(OmnitureTrackData *)trackData;

- (void)trackAction:(OmnitureTrackData *)trackData;

- (void)trackEvent:(OmnitureTrackData *)trackData;

- (void)trackErrorScreen:(OmnitureTrackData *)trackData;

@end
