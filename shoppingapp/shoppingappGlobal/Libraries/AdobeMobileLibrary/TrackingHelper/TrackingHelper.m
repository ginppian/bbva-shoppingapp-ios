//
//  TrackingHelper.m
//  BBVA_Plugin
//
//  Created by Anegón Núñez, A. on 24/10/16.
//  Copyright © 2016 Accenture Digital. All rights reserved.
//

#import "TrackingHelper.h"
#import "ADBMobile.h"

#define DEBUG_OMNITURE_TRACES  0

// File preferences name where cache data will be storaged
static NSString * const PREFS_NAME = @"PluginsData";

//Cache variables
static NSString * const kReturningValue = @"Repeat";
static NSString * const kPreviousPage = @"prevPage";
static NSString * const kPreviousPageSegment = @"prevPageSegment";
static NSString * const kReturningNewValue = @"New";
static NSString * const kPreviousPageType = @"prevPageType";
static NSString * const kPreviousPageSection = @"prevPageSection";
static NSString * const kPreviousArea = @"kPreviousArea";

//Time to complete options
static NSString * const START = @"start";
static NSString * const STOP = @"stop";

// Constant representing the digit to add
static NSString * const kAddPrefixCharValue = @"0";

//String format constants
static NSString * const kFormattedStringDate = @"%@-%@-%@";
static NSString * const kFromattedStringTime = @"%@:%@:%@";
static NSString * const kFormatIntTo2StrDigits = @"%02ld";
static NSString * const kFormatIntToStr = @"%ld";
static NSString * const kFormatTimeParting = @"%@ | %@ | %@ | %@ | %@ : %@ | %@ | %@ | %@";
static NSString * const kFormattedTimeValue = @"HH:mm:ss";

//Current time of day constatns
static NSString * const kCurrentDayConstantMorning = @"Morning";
static NSString * const kCurrentDayConstantAfternoon = @"Afternoon";
static NSString * const kCurrentDayConstantEvening = @"Evening";
static NSString * const kCurrentDayConstantNight = @"Late Night";

//Last session value constatns
static NSString * const kSessionLastVisit = @"lastVisit";
static NSString * const kSessionIntervalFirstTimeValue = @"First time";
static NSString * const kSessionLastVisitLess1Day = @"Less than 1 day";
static NSString * const kSessionLastVisitLess7Days = @"Less than 7 days";
static NSString * const kSessionLastVisitMore7Days = @"More than 7 days";
static NSString * const kSessionLastVisitMore30Days = @"More than 30 days";

//Get Time constants
NSString * const kTimeToCompleteOptionStart = @"start";
NSString * const kTimeToCompleteOptionStop = @"stop";
static NSString * const kTimeToCompleteTypeTimeToConversion = @"timeToConversion";
static NSString * const kTimeToCompleteTypeTimeToExit = @"timeToExit";
static NSString * const kTimeToCompleteTypeAppFlowTimeWindow = @"conversionWindow";
static NSString * const kTimeToCompleteTypeLoadTime = @"loadtime";
static NSString * const kTimeMorningValue = @"06:00:00";
static NSString * const kTimeAfternoonValue = @"12:00:00";
static NSString * const kTimeEveningValue = @"19:00:00";
static NSString * const kTimeNightValue = @"23:59:59";

@interface TrackingHelper()
@property (nonatomic) BOOL firstPageSent;
@end

@implementation TrackingHelper

- (instancetype)init {
    self = [super init];
    if (self) {
        _datos_envio = [NSMutableDictionary dictionary];
        _firstPageSent = false;
    }
    return self;
}

#pragma mark - Private methods

/**
 Include '0' chars in the left of the string following the number of digits
 
 @param text   text to add 0 until has the same number of digits
 @param digits number of digits
 
 @return the text witht the number of digits we need
 */
- (NSString *)formatText:(NSString *)text digits:(NSUInteger)digits {
    
    NSMutableString *result = [NSMutableString stringWithString:text];
    while ([result length] < digits) {
        
        [result insertString:kAddPrefixCharValue atIndex:0];
    }
    
    return result;
}

/**
 Calculate the time between start and stop
 
 @param option start / stop
 @param type   the type that we want the time
 
 @return time
 */
- (NSString *)getTimeToComplete:(NSString *)option type:(NSString *)type {
    
    NSString *result = [NSString string];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([option isEqualToString:kTimeToCompleteOptionStart]) {
        
        NSNumber *currentTimeMillis = [NSNumber numberWithDouble:[[NSDate date] timeIntervalSince1970]];
        [defaults setObject:currentTimeMillis forKey:type];
    } else if ([option isEqualToString:kTimeToCompleteOptionStop]) {
        
        NSNumber *startTime = [defaults objectForKey:type];
        NSTimeInterval startTimeInterval = startTime.doubleValue;
        NSTimeInterval endTimeInterval = [[NSDate date] timeIntervalSince1970];
        NSTimeInterval difference = endTimeInterval - startTimeInterval;
        NSInteger time = difference;
        result = [NSString stringWithFormat:@"%li", (long)time];
        [defaults removeObjectForKey:type];
    }
    [defaults synchronize];
    
    return result;
}

#pragma mark - Public methods

- (NSString *)getDayOfMonth {
    
    NSDate *currentDate = [NSDate date];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay
                                               fromDate:currentDate];
    
    return [NSString stringWithFormat:kFormatIntTo2StrDigits, (long)[components day]];
}

- (NSString *)getMonth {
    
    NSDate *currentDate = [NSDate date];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay
                                               fromDate:currentDate];
    
    return [NSString stringWithFormat:kFormatIntTo2StrDigits, (long)[components month]];
}

- (NSString *)getYear {
    
    NSDate *currentDate = [NSDate date];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay
                                               fromDate:currentDate];
    
    return [NSString stringWithFormat:kFormatIntToStr, (long)[components year]];
}

- (NSString *)getDayOfWeek {
    
    NSDate *currentDate = [NSDate date];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitWeekday
                                               fromDate:currentDate];
    
    return [NSString stringWithFormat:kFormatIntToStr, (long)[components weekday]];
}

- (NSString *)getDate {
    
    return [NSString stringWithFormat:kFormattedStringDate,
            [self getYear],
            [self getMonth],
            [self getDayOfMonth]];
}

- (NSString *)getWeek {
    
    NSDate *currentDate = [NSDate date];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitWeekOfYear
                                               fromDate:currentDate];
    
    return [NSString stringWithFormat:kFormatIntTo2StrDigits, (long)[components weekOfYear]];
}

- (NSString *)getHour {
    
    NSDate *currentDate = [NSDate date];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitHour
                                               fromDate:currentDate];
    
    return [NSString stringWithFormat:kFormatIntTo2StrDigits, (long)[components hour]];
}

- (NSString *)getMinute {
    
    NSDate *currentDate = [NSDate date];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitMinute
                                               fromDate:currentDate];
    
    return [NSString stringWithFormat:kFormatIntTo2StrDigits, (long)[components minute]];
}

- (NSString *)getSecond {
    
    NSDate *currentDate = [NSDate date];
    NSCalendar* calendar = [NSCalendar currentCalendar];
    NSDateComponents* components = [calendar components:NSCalendarUnitYear|NSCalendarUnitMonth|NSCalendarUnitDay|NSCalendarUnitSecond
                                               fromDate:currentDate];
    
    return [NSString stringWithFormat:kFormatIntTo2StrDigits, (long)[components second]];
}

- (NSString *)getTime {
    
    return [NSString stringWithFormat:kFromattedStringTime,
            [self getHour],
            [self getMinute],
            [self getSecond]];
}

- (NSString *)getTimeOfDay {
    
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    [dateFormat setDateFormat:kFormattedTimeValue];
    NSString *morning = kTimeMorningValue;
    NSDate *morningTime = [dateFormat dateFromString:morning];
    NSString *afternoon = kTimeAfternoonValue;
    NSDate *afternoonTime = [dateFormat dateFromString:afternoon];
    NSString *evening = kTimeEveningValue;
    NSDate *eveningTime = [dateFormat dateFromString:evening];
    NSString *night = kTimeNightValue;
    NSDate *nightTime = [dateFormat dateFromString:night];
    NSString *currentTimeOfday = [NSString string];
    unsigned int flags = NSCalendarUnitHour | NSCalendarUnitMinute | NSCalendarUnitSecond | NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitYear;
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:flags fromDate:morningTime];
    NSDateComponents *componentsCurrentTime = [calendar components:flags fromDate:[NSDate date]];
    //set date components to have the same year, month and day
    [componentsCurrentTime setDay:[components day]];
    [componentsCurrentTime setMonth:[components month]];
    [componentsCurrentTime setYear:[components year]];
    NSDate *currentTime = [calendar dateFromComponents:componentsCurrentTime];
    if ([currentTime compare:morningTime] == NSOrderedDescending &&
        [currentTime compare:afternoonTime] == NSOrderedAscending) {
        
        currentTimeOfday = kCurrentDayConstantMorning;
    } else if ([currentTime compare:afternoonTime] == NSOrderedDescending &&
               [currentTime compare:eveningTime] == NSOrderedAscending) {
        
        currentTimeOfday = kCurrentDayConstantAfternoon;
    } else if ([currentTime compare:eveningTime] == NSOrderedDescending &&
               [currentTime compare:nightTime] == NSOrderedAscending) {
        
        currentTimeOfday = kCurrentDayConstantEvening;
    } else {
        
        currentTimeOfday = kCurrentDayConstantNight;
    }
    
    return currentTimeOfday;
}

- (NSString *)getTimeParting {
    
    return [NSString stringWithFormat:kFormatTimeParting,
            [self getDate],
            [self getDayOfMonth],
            [self getDayOfWeek],
            [self getMonth],
            [self getHour],
            [self getMinute],
            [self getTimeOfDay],
            [self getWeek],
            [self getYear]];
}

- (void)setPrevPage:(NSString *)pageName {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (pageName != nil) {
        
        [defaults setObject:pageName forKey:kPreviousPage];
        [defaults synchronize];
    }
}

- (NSString *)getPrevPage{

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *prevPage = [defaults objectForKey:kPreviousPage];
    return prevPage ? prevPage : @"";
}


- (void)setPrevPageSegment:(NSString *)pageSegment {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (pageSegment != nil) {

        [defaults setObject:pageSegment forKey:kPreviousPageSegment];
        [defaults synchronize];
    }
}

- (NSString *)getPrevPageSegment{

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *prevPageSegment = [defaults objectForKey:kPreviousPageSegment];
    return prevPageSegment ? prevPageSegment : @"";
}



- (NSString *)getPrevPageType:(NSString *)pageType {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *prevPageType = [defaults objectForKey:kPreviousPageType];
    if (pageType != nil) {
        
        [defaults setObject:pageType forKey:kPreviousPageType];
        [defaults synchronize];
    }
    
    return prevPageType ? prevPageType : @"";
}

- (NSString *)getPrevPageSection:(NSString *)pageSection {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *prevPageSection  = [defaults objectForKey:kPreviousPageSection];
    if (pageSection != nil) {
        
        [defaults setObject:pageSection forKey:kPreviousPageSection];
        [defaults synchronize];
    }
    
    return prevPageSection ? prevPageSection : @"";
}
- (CGFloat)getInitialPercentPageView:(UIView *)parentView {
    
    CGFloat percentageShowed = [parentView frame].size.height;
    if ([parentView isKindOfClass:[UIScrollView class]]) {
        
        UIScrollView *scroll = (UIScrollView *)parentView;
        CGSize contentSize = scroll.contentSize;
        if (contentSize.height == 0.0 && contentSize.width == 0.0) {
            //no content size yet
            return 0.0;
        } else {
            BOOL isVertical = (contentSize.height > contentSize.width) ? YES : NO;
            if (isVertical) {
                
                percentageShowed = (([UIScreen mainScreen].bounds.size.height + scroll.contentOffset.y) * 100.0) / contentSize.height;
            } else {
                
                percentageShowed = (([UIScreen mainScreen].bounds.size.width + scroll.contentOffset.x) * 100.0) / contentSize.width;
            }
        }
    } else {
        
        //the parent view is an UIView
        percentageShowed = 100.0;
    }
    
    return percentageShowed;
}

- (CGFloat)getPercentPageView:(UIView *)parentView {
    
    CGFloat percentageShowed = [parentView frame].size.height;
    if ([parentView isKindOfClass:[UIScrollView class]]) {
        
        UIScrollView *scroll = (UIScrollView *)parentView;
        CGSize contentSize = scroll.contentSize;
        if (contentSize.height == 0.0 && contentSize.width == 0.0) {
            //no content size yet
            return 0.0;
        } else {
            BOOL isVertical = (contentSize.height > contentSize.width) ? YES : NO;
            if (isVertical) {
                
                percentageShowed = (([UIScreen mainScreen].bounds.size.height + scroll.contentOffset.y) * 100.0) / contentSize.height;
            } else {
                
                percentageShowed = (([UIScreen mainScreen].bounds.size.width + scroll.contentOffset.x) * 100.0) / contentSize.width;
            }
        }
    } else {
        
        //the parent view is an UIView
        percentageShowed = 100.0;
    }
    
    return percentageShowed;
}

- (CGFloat)getSessionPercentPageView:(UIView *)parentView key:(NSString *)key {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSNumber *currentPercentage = [defaults objectForKey:key];
    CGFloat percentage = currentPercentage.floatValue;
    if (percentage >= 100.0) {
        
        percentage = 100.0;
    } else {
        
        CGFloat percentageAux = [self getPercentPageView:parentView];
        if (percentage < percentageAux) {
            percentage = percentageAux;
        }
    }
    [defaults setObject:[NSNumber numberWithFloat:percentage] forKey:key];
    [defaults synchronize];
    
    return percentage;
}

- (CGFloat)getNumVerticalPixels {
    
    CGRect screenBounds = [[UIScreen mainScreen] bounds];
    CGFloat screenScale = [[UIScreen mainScreen] scale];
    CGSize screenSize = CGSizeMake(screenBounds.size.width * screenScale, screenBounds.size.height * screenScale);
    
    return screenSize.height;
}

- (NSString *)getSessionInterval {
    
    NSString *result = kSessionIntervalFirstTimeValue;
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSNumber *lastSessionValue = (NSNumber *)[defaults objectForKey:kSessionLastVisit];
    if (lastSessionValue != nil) {
        
        //Get the number of days
        NSNumber *timeIntervalNumber = (NSNumber *)[defaults objectForKey:kSessionLastVisit];
        NSTimeInterval timeInterval = [timeIntervalNumber doubleValue];
        NSDate *fromDate = [NSDate dateWithTimeIntervalSince1970:timeInterval];
        NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
        NSDateComponents *components = [gregorianCalendar components:NSCalendarUnitDay
                                                            fromDate:fromDate
                                                              toDate:[NSDate date]
                                                             options:0];
        NSUInteger numberOfDays = components.day;
        if (numberOfDays < 1) {
            
            result = kSessionLastVisitLess1Day;
        } else if (numberOfDays < 7) {
            
            result = kSessionLastVisitLess7Days;
        } else if (numberOfDays < 30) {
            
            result = kSessionLastVisitMore7Days;
        } else {
            
            result = kSessionLastVisitMore30Days;
        }
    }
    NSTimeInterval timeInterval = [[NSDate date] timeIntervalSince1970];
    [defaults setObject:[NSNumber numberWithDouble:timeInterval] forKey:kSessionLastVisit];
    [defaults synchronize];
    
    return result;
}

- (NSString *)getTimeToConversion:(NSString *)option {
    
    NSString *result = [NSString string];
    result = [self getTimeToComplete:option type:kTimeToCompleteTypeTimeToConversion];
    
    return result;
}

- (NSString *)getTimeToExit:(NSString *)option {
    
    NSString *result = [NSString string];
    result = [self getTimeToComplete:option type:kTimeToCompleteTypeTimeToExit];
    
    return result;
}

- (NSString *)getAppFlowCompletionTime:(NSString *)option {
    
    NSString *result = [NSString string];
    result = [self getTimeToComplete:option type:kTimeToCompleteTypeAppFlowTimeWindow];
    
    return result;
}

- (NSString *)getLoadTime:(NSString *)option {
    
    NSString *result = [NSString string];
    result = [self getTimeToComplete:option type:kTimeToCompleteTypeLoadTime];
    
    return result;
}

- (NSString *)getReturningVisitor {
    
    NSString *result = [NSString string];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    BOOL returning = [defaults boolForKey:kReturningValue];
    result = returning ? kReturningValue : kReturningNewValue;
    [defaults setBool:YES forKey:kReturningValue];
    [defaults synchronize];
    
    return result;
}

- (NSString *)getMarketingCloudId {
    
    return [ADBMobile visitorMarketingCloudID];
    
}

- (void)savePreviousArea:(NSString *)pageArea {
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if (pageArea != nil) {
        
        [defaults setObject:pageArea forKey:kPreviousArea];
        [defaults synchronize];
    }
    
}

- (NSString *)getPreviousArea{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *prevArea = [defaults objectForKey:kPreviousArea];
    
    if (prevArea == nil) {
        return kPublicArea;
    }
    
    return prevArea;
}

#pragma mark - Utils

- (void)huellaAvanzadaWithData:(OmnitureTrackData *)trackData{

    _datos_envio[@"channel"] = trackData.pageSegment;

    _datos_envio[@"eVar12"] = [trackData logged];
    _datos_envio[@"prop12"] = [trackData logged];

    _datos_envio[@"eVar17"] = trackData.language;
    _datos_envio[@"prop17"] = trackData.language;

    _datos_envio[@"eVar16"] = trackData.area;
    _datos_envio[@"prop16"] = trackData.area;

    _datos_envio[@"eVar14"] = trackData.pageType;
    _datos_envio[@"prop14"] = trackData.pageType;

    _datos_envio[@"prop67"] = trackData.appVersion;

    _datos_envio[@"eVar15"] = trackData.environment;
    _datos_envio[@"prop15"] = trackData.environment;

    if (trackData.isLogged) {
        _datos_envio[@"eVar38"] = trackData.userProfile;
    }
    
    _datos_envio[@"eVar37"] = trackData.customerID;

    _datos_envio[@"eVar4"] = [self getTimeParting];
    _datos_envio[@"eVar24"] = [self getSessionInterval];
    _datos_envio[@"eVar25"] = [self getReturningVisitor];

    _datos_envio[@"eVar29"] = trackData.businessUnit;


    _datos_envio[@"prop24"] = [self getPrevPageType:trackData.pageType];

    NSString *currentSection = [self getLastSectionFromPageName:trackData.pageName];
    _datos_envio[@"prop25"] = [self getPrevPageSection:currentSection];


    _datos_envio[@"eVar31"] = kSiteAppName;
    _datos_envio[@"prop31"] = kSiteAppName;

    _datos_envio[@"prop68"] = [self getMarketingCloudId];

}

- (void)clearVars {
    
    _datos_envio = [NSMutableDictionary dictionary];
    
}

- (NSDictionary *)siteSectionsFromPage:(NSString *) pageName {
    
    NSMutableDictionary *siteSections = [[NSMutableDictionary alloc] init];
    
    NSArray *sections = [pageName componentsSeparatedByString:@":"];
    NSMutableString *concatSection = [NSMutableString stringWithString:sections[0]];
    
    [siteSections setObject:[NSString stringWithString:concatSection] forKey:@"prop1"];
    
    for (int i=1; i< sections.count; i++) {
        
        [concatSection appendString:[NSString stringWithFormat:@":%@", sections[i]]];
        [siteSections setObject:[NSString stringWithString:concatSection] forKey:[NSString stringWithFormat:@"prop%i", (i+1)]];
        
    }
    
    return [siteSections copy];
}

- (NSString *)getLastSectionFromPageName:(NSString *) pageName{

    NSArray *sections = [pageName componentsSeparatedByString:@":"] ;

    NSUInteger indexOfGeneral = [sections indexOfObject:kPageSegmentGeneral];
    NSUInteger indexOfPersonas = [sections indexOfObject:kPageSegmentPeople];

    NSString *section;

    if(indexOfGeneral != NSNotFound && (indexOfGeneral + 1) < [sections count] ) {
        section = [sections objectAtIndex:indexOfGeneral + 1];
    } else if(indexOfPersonas != NSNotFound && (indexOfPersonas + 1) < [sections count] ) {
        section = [sections objectAtIndex:indexOfPersonas + 1];
    } else {
        section = [sections firstObject];
    }

    if( [section length] > 0 ) {
        return section;
    } else {
        return @"";
    }

}

#pragma mark - Utility methods

- (NSString *)fillCustomData:(OmnitureTrackData *)trackData isEvent:(BOOL)isEvent {

    if (trackData.customLink) {
        
        _datos_envio[@"customLink"] = trackData.customLink;
    }
    
    if(!isEvent) {
        NSString *pageName = [NSString stringWithFormat:@"%@:%@:%@:%@", kApp, trackData.area, trackData.pageSegment, trackData.pageName];

        _datos_envio[@"eVar1"] = pageName;
        _datos_envio[@"appState"] = pageName;

        NSDictionary *siteSections = [self siteSectionsFromPage:trackData.pageName];
        [_datos_envio addEntriesFromDictionary:siteSections];

        NSString *pageNamePrevious = [NSString stringWithFormat:@"%@:%@:%@:%@", kApp, [self getPreviousArea], [self getPrevPageSegment], [self getPrevPage]];
        [self setPrevPage:trackData.pageName];
        [self setPrevPageSegment:trackData.pageSegment];

        if(_firstPageSent) {
            _datos_envio[@"prop21"] = pageNamePrevious;
        }

        [self savePreviousArea:trackData.area];

        return pageName;
    } else {
        _datos_envio[@"eVar1"] = trackData.pageName;
        _datos_envio[@"appState"] = trackData.pageName;
        _datos_envio[@"prop1"] = [self getLastSectionFromPageName:trackData.pageName];
        return trackData.pageName;
    }
}

#pragma mark - Track methods

- (void)trackScreen:(OmnitureTrackData *)trackData {

    NSString *pageName = [self fillCustomData:trackData isEvent:NO];
    [self huellaAvanzadaWithData:trackData];

    _datos_envio[@"eVar34"] = kVisitsNumber;
    [ADBMobile trackState:pageName
                     data:_datos_envio];

#if DEBUG_OMNITURE_TRACES && DEBUG
    NSLog(@"OMNITURE SCREEN:%@ %@",pageName,  _datos_envio.description);
#endif
    
    if(!_firstPageSent) {
        _firstPageSent = YES;
    }
    [self clearVars];
}

- (void)trackErrorScreen:(OmnitureTrackData *)trackData {
    
    NSString *pageName = [self fillCustomData:trackData isEvent:NO];
    [self huellaAvanzadaWithData:trackData];

    _datos_envio[@"eVar34"] = kVisitsNumber;
    _datos_envio[@"eVar32"] = trackData.error;
    _datos_envio[@"evento_errorPage"] = @"event32";
    [ADBMobile trackState:pageName
                     data:_datos_envio];
    
#if DEBUG_OMNITURE_TRACES && DEBUG
    NSLog(@"OMNITURE SCREEN:%@ %@",pageName,  _datos_envio.description);
#endif
    
    [self clearVars];
}

- (void)trackAction:(OmnitureTrackData *)trackData {

    [self fillCustomData:trackData isEvent:NO];
    [self huellaAvanzadaWithData:trackData];

    [ADBMobile trackAction:trackData.customLink
                      data:_datos_envio];

#if DEBUG_OMNITURE_TRACES && DEBUG
    NSLog(@"OMNITURE ACTION:%@ %@", trackData.customLink, _datos_envio.description);
#endif
    
    [self clearVars];
}

- (void)trackEvent:(OmnitureTrackData *)trackData {

    [self fillCustomData:trackData isEvent:YES];
    [self huellaAvanzadaWithData:trackData];

    [ADBMobile trackAction:trackData.customLink
                      data:_datos_envio];
    
#if DEBUG_OMNITURE_TRACES && DEBUG
    NSLog(@"OMNITURE EVENT:%@ %@", trackData.customLink, _datos_envio.description);
#endif

    [self clearVars];
}



@end
