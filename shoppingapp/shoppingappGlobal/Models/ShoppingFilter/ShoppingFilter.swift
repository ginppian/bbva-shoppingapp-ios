//
//  ShoppingFilterTransport.swift
//  shoppingapp
//
//  Created by jesus.martinez on 21/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit

class ShoppingFilter {

    static let text_filter_key = "TEXT_FITLER_KEY"

    var textFilter: String?
    var promotionTypeFilter: [PromotionType]?
    var promotionUsageTypeFilter: [PromotionUsageType]?
    var categoriesFilter: [CategoryType]?

    func isEmpty() -> Bool {

        return (textFilter == nil || textFilter!.isEmpty)
            && (promotionTypeFilter == nil || promotionTypeFilter!.isEmpty)
            && (promotionUsageTypeFilter == nil || promotionUsageTypeFilter!.isEmpty)
            && (categoriesFilter == nil || categoriesFilter!.isEmpty)

    }

    func add(promotionType: PromotionType) {

        if promotionTypeFilter == nil {
            promotionTypeFilter = [PromotionType]()
        }

        if !promotionTypeFilter!.contains(promotionType) {
            promotionTypeFilter?.append(promotionType)
        }

    }

    func remove(promotionType: PromotionType) {

        let indexOfPromotionType = promotionTypeFilter?.index(of: promotionType)

        if let indexToRemove = indexOfPromotionType {
            promotionTypeFilter?.remove(at: indexToRemove)
        }

    }

    func add(promotionUsageType: PromotionUsageType) {

        if promotionUsageTypeFilter == nil {
            promotionUsageTypeFilter = [PromotionUsageType]()
        }

        if !promotionUsageTypeFilter!.contains(promotionUsageType) {
            promotionUsageTypeFilter?.append(promotionUsageType)
        }

    }

    func remove(promotionUsageType: PromotionUsageType) {

        let indexOfPromotionUsageType = promotionUsageTypeFilter?.index(of: promotionUsageType)

        if let indexToRemove = indexOfPromotionUsageType {
            promotionUsageTypeFilter?.remove(at: indexToRemove)
        }

    }

    func add(categoryType: CategoryType) {

        if categoriesFilter == nil {
            categoriesFilter = [CategoryType]()
        }

        if !categoriesFilter!.contains(categoryType) {
            categoriesFilter?.append(categoryType)
        }

    }

    func remove(categoryType: CategoryType) {

        let indexOfCategoriesFilter = categoriesFilter?.index(of: categoryType)

        if let indexToRemove = indexOfCategoriesFilter {
            categoriesFilter?.remove(at: indexToRemove)
        }

    }

    func reset() {

        textFilter = nil
        promotionTypeFilter = nil
        promotionUsageTypeFilter = nil
        categoriesFilter = nil

    }

    func retrieveServiceParameters() -> String {

        var params = ""

        if let textFilter = textFilter, !textFilter.isEmpty {

            params += "&commerceInformation.name=\(textFilter)"
        }

        if let promotionUsageTypeFilter = promotionUsageTypeFilter, !promotionUsageTypeFilter.isEmpty {

            let promotionUsageTypeFilterString = promotionUsageTypeFilter.compactMap { promotionUsageType in promotionUsageType.rawValue }.joined(separator: ",")
            params += "&usageTypes.id=in=(\(promotionUsageTypeFilterString))"
        }

        if let promotionTypeFilter = promotionTypeFilter, !promotionTypeFilter.isEmpty {

            let promotionTypeFilterString = promotionTypeFilter.compactMap { promotionTypeFilter in promotionTypeFilter.toService }.joined(separator: ",")
            params += "&promotionType.id=in=(\(promotionTypeFilterString))"
        }

        if let categoriesFilter = categoriesFilter, !categoriesFilter.isEmpty {

            let categoriesFilterString = categoriesFilter.compactMap { categoriesFilter in categoriesFilter.rawValue }.joined(separator: ",")
            params += "&categories.id=in=(\(categoriesFilterString))"
        }

        return params
    }

    func retrieveLiterals(withCategories categoriesBO: CategoriesBO) -> [(title: String, key: String)] {

        var literals = [(title: String, key: String)]()

        if let text = textFilter, !text.isEmpty {
            literals.append((title: text, key: ShoppingFilter.text_filter_key))
        }

        if let promotionTypes = promotionTypeFilter, !promotionTypes.isEmpty {

            for promotionType in promotionTypes where !promotionType.literal.isEmpty {
                    literals.append((title: promotionType.literal, key: promotionType.rawValue))
            }

        }

        if let promotionUsageTypes = promotionUsageTypeFilter, !promotionUsageTypes.isEmpty {

            for promotionUsageType in promotionUsageTypes  where !promotionUsageType.literal.isEmpty {
                literals.append((title: promotionUsageType.literal, key: promotionUsageType.rawValue))

            }

        }

        if let categories = categoriesFilter, !categories.isEmpty {

            for category in categories {

                if let name = category.getCategoryName(fromCategories: categoriesBO) {
                    literals.append((title: name, key: category.rawValue))
                }

            }

        }

        return literals

    }

    func remove(byFilterType filterType: String) {

        guard !filterType.isEmpty else {
            return
        }

        if filterType == ShoppingFilter.text_filter_key {
            textFilter = nil
            return
        }

        if let promotionType = PromotionType(rawValue: filterType), promotionType != .unknown {
            remove(promotionType: promotionType)
            return
        }

        if let promotionUsageType = PromotionUsageType(rawValue: filterType), promotionUsageType != .unknown {
            remove(promotionUsageType: promotionUsageType)
            return
        }

        if let categoryFilter = CategoryType(rawValue: filterType), categoryFilter != .unknown {
            remove(categoryType: categoryFilter)
            return
        }

    }

}

extension ShoppingFilter: Equatable {

    public static func == (lhs: ShoppingFilter, rhs: ShoppingFilter) -> Bool {

        let textAreEquals = lhs.textFilter == rhs.textFilter
        var promotionTypeAreEquals = false
        var promotionUsageTypeAreEquals = false
        var categoriesFilterAreEquals = false

        if let leftPromotionTypeFilter = lhs.promotionTypeFilter, let rightPromotionTypeFilter = rhs.promotionTypeFilter {
            promotionTypeAreEquals = leftPromotionTypeFilter == rightPromotionTypeFilter
        } else {
            promotionTypeAreEquals = (lhs.promotionTypeFilter == nil && rhs.promotionTypeFilter == nil)
        }

        if let leftPromotionUsageTypeFilter = lhs.promotionUsageTypeFilter, let rightPromotionUsageTypeFilter = rhs.promotionUsageTypeFilter {
            promotionUsageTypeAreEquals = leftPromotionUsageTypeFilter == rightPromotionUsageTypeFilter
        } else {
            promotionUsageTypeAreEquals = (lhs.promotionUsageTypeFilter == nil && rhs.promotionUsageTypeFilter == nil)
        }

        if let leftCategoriesFilter = lhs.categoriesFilter, let rightCategoriesFilter = rhs.categoriesFilter {
            categoriesFilterAreEquals = leftCategoriesFilter == rightCategoriesFilter
        } else {
            categoriesFilterAreEquals = (lhs.categoriesFilter == nil && rhs.categoriesFilter == nil)
        }

        return textAreEquals && promotionTypeAreEquals && promotionUsageTypeAreEquals && categoriesFilterAreEquals

    }

}
