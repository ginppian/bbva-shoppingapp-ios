//
//  ShoppingFilterTransport.swift
//  shoppingapp
//
//  Created by jesus.martinez on 26/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit

class ShoppingFilterTransport: Model {

    var shouldAllowSelectUsageType = false
    var filters: ShoppingFilter?
    var categoriesBO: CategoriesBO?
    var shouldFilterInPreviousScreen = false

}
