//
//  CurrencyBO.swift
//  shoppingapp
//
//  Created by jesus.martinez on 3/8/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation

class CurrencyBO: ModelBO {

    enum CurrencyPosition {
        case left
        case right
    }

    let code: String
    let position: CurrencyPosition
    let needDecimals: Bool
    let symbol: String

    init(code: String, position: CurrencyPosition, needDecimals: Bool, symbol: String = "") {

        self.code = code
        self.position = position
        self.needDecimals = needDecimals
        self.symbol = CurrencyBO.findSymbol(forCode: self.code)

    }

    private static func findSymbol(forCode code: String) -> String {

        for localeId in Locale.availableIdentifiers {

            let locale = Locale(identifier: localeId)

            if let currencyCode = locale.currencyCode, let symbol = locale.currencySymbol, currencyCode == code {

                //Surprisingly at least ios 10.3.1 returns "S/." instead of expected "/S"
                if symbol == "S/." {
                    return "S/"
                }
                return symbol

            }

        }

        return code
    }

}

extension CurrencyBO: Equatable {
    
    static func == (lhs: CurrencyBO, rhs: CurrencyBO) -> Bool {
        
        return lhs.code == rhs.code
            && lhs.position == rhs.position
            && lhs.needDecimals == rhs.needDecimals
            && lhs.symbol == rhs.symbol
    }
}
