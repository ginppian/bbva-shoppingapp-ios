//
//  CurrencyEntity.swift
//  shoppingapp
//
//  Created by jesus.martinez on 3/8/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation

class CurrencyEntity: ModelEntity {

    let code: String

    init(code: String) {

        self.code = code

    }

}
