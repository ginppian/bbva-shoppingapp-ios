//
//  PickerOptionsDisplay.swift
//  shoppingapp
//
//  Created by Javier Pino on 19/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation

class PickerOptionsDisplay: ModelDisplay {

    var titles: [String]?
    var selectedIndex: Int?
}
