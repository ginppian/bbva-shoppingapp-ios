//
//  LimitsBO.swift
//  shoppingapp
//
//  Created by jesus.martinez on 24/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class LimitsBO: ModelBO {

    var status: Int
    var limits: [LimitBO]

    init(limitsEntity: LimitsEntity) {

        status = limitsEntity.status

        var limits = [LimitBO]()

        if let limitsEntity = limitsEntity.data {
            for limitEntity in limitsEntity {
                limits.append(LimitBO(limitEntity: limitEntity))
            }
        }

        self.limits = limits
    }

    func replaceLimit(_ limit: LimitBO) {
        limits = limits.map { $0.type.rawValue != limit.type.rawValue ? $0 : limit }
    }

}

enum LimitType: String {
    case atm_daily = "ATM_DAILY"
    case atm_monthly = "ATM_MONTHLY"
    case pos_daily = "POS_DAILY"
    case pos_monthly = "POS_MONTHLY"
    case office_daily = "OFFICE_DAILY"
    case office_monthly = "OFFICE_MONTHLY"
    case transaction_daily = "TRANSACTION_DAILY"
    case transaction_monthly = "TRANSACTION_MONTHLY"
    case prepaid_limit_monthly = "PREPAID_LIMIT_MONTHLY"
    case credit_limit_monthly = "CREDIT_LIMIT_MONTHLY"
    case granted_credit = "GRANTED_CREDIT"
    case unknowm

    var title: String {

        switch self {

        case .atm_daily:
            return Localizables.cards.key_card_limit_daily_cash_withdrawal
        case .atm_monthly:
            return Localizables.cards.key_card_limit_monthly_cash_withdrawal
        case .pos_daily:
            return Localizables.cards.key_card_limit_daily_shopping
        case .pos_monthly:
            return Localizables.cards.key_card_limit_monthly_shopping
        case .office_daily:
            return Localizables.cards.key_card_limit_daily_withdrawal_in_office
        case .office_monthly:
            return Localizables.cards.key_card_limit_monthly_withdrawal_in_office
        case .transaction_daily:
            return Localizables.cards.key_card_limit_daily_operations
        case .transaction_monthly:
            return Localizables.cards.key_card_limit_monthly_operations
        case .prepaid_limit_monthly:
            return Localizables.cards.key_card_limit_monthly_card_recharge
        case .credit_limit_monthly:
            return Localizables.cards.key_card_monthly_credit_card
        case .granted_credit:
            return Localizables.cards.key_card_limit_maximum_consumption
        default:
            return ""
        }
    }

    var disposedLimitTitle: String {

        switch self {

        case .atm_daily:
            return Localizables.cards.key_card_retired_today
        case .atm_monthly:
            return Localizables.cards.key_card_retired_this_month
        case .pos_daily:
            return Localizables.cards.key_card_bought_today
        case .pos_monthly:
            return Localizables.cards.key_card_bought_this_month
        case .office_daily:
            return Localizables.cards.key_card_retired_today
        case .office_monthly:
            return Localizables.cards.key_card_retired_this_month
        case .transaction_daily:
            return Localizables.cards.key_card_consumed_today
        case .transaction_monthly:
            return Localizables.cards.key_card_consumed_this_month
        case .prepaid_limit_monthly:
            return Localizables.cards.key_card_consumed_this_month
        case .credit_limit_monthly:
            return Localizables.cards.key_card_consumed_this_month
        case .granted_credit:
            return Localizables.cards.key_cards_consumed_text
        default:
            return Localizables.cards.key_cards_consumed_text
        }
    }

    var amountsLimitTitle: String {

        switch self {

        case .atm_daily:
            return Localizables.cards.key_card_daily_limit
        case .atm_monthly:
            return Localizables.cards.key_card_monthly_limit
        case .pos_daily:
            return Localizables.cards.key_card_daily_limit
        case .pos_monthly:
            return Localizables.cards.key_card_monthly_limit
        case .office_daily:
            return Localizables.cards.key_card_daily_limit
        case .office_monthly:
            return Localizables.cards.key_card_monthly_limit
        case .transaction_daily:
            return Localizables.cards.key_card_daily_limit
        case .transaction_monthly:
            return Localizables.cards.key_card_monthly_limit
        case .prepaid_limit_monthly:
            return Localizables.cards.key_card_monthly_limit
        case .credit_limit_monthly:
            return Localizables.cards.key_card_monthly_limit
        case .granted_credit:
            return Localizables.cards.key_cards_current_limit_text
        default:
            return Localizables.cards.key_cards_current_limit_text
        }
    }

    var notEditableTitle: String {

        switch self {

        case .atm_daily:
            return Localizables.cards.key_card_retired_maximum_allowed_daily
        case .atm_monthly:
            return Localizables.cards.key_card_retired_maximum_allowed_monthly
        case .pos_daily:
            return Localizables.cards.key_card_bought_maximum_allowed_daily
        case .pos_monthly:
            return Localizables.cards.key_card_bought_maximum_allowed_monthly
        case .office_daily:
            return Localizables.cards.key_card_retired_maximum_allowed_daily
        case .office_monthly:
            return Localizables.cards.key_card_retired_maximum_allowed_monthly
        case .transaction_daily:
            return Localizables.cards.key_card_consumed_maximum_allowed_daily
        case .transaction_monthly:
            return Localizables.cards.key_card_consumed_maximum_allowed_monthly
        case .prepaid_limit_monthly:
            return Localizables.cards.key_card_consumed_maximum_allowed_monthly
        case .credit_limit_monthly:
            return Localizables.cards.key_card_consumed_maximum_allowed_monthly
        case .granted_credit:
            return Localizables.cards.key_card_consumed_maximum_allowed
        default:
            return Localizables.cards.key_card_consumed_maximum_allowed
        }
    }

    var inputAmountLimitTitle: String {

        let multiple_amount = Int(Settings.Limits.multiples_limit_amount)

        switch self {

        case .atm_daily:
            return Localizables.cards.key_card_daily_limit_multiples + " \(multiple_amount)"
        case .atm_monthly:
            return Localizables.cards.key_card_monthly_limit_multiples + " \(multiple_amount)"
        case .pos_daily:
            return Localizables.cards.key_card_daily_limit_multiples + " \(multiple_amount)"
        case .pos_monthly:
            return Localizables.cards.key_card_monthly_limit_multiples + " \(multiple_amount)"
        case .office_daily:
            return Localizables.cards.key_card_daily_limit_multiples + " \(multiple_amount)"
        case .office_monthly:
            return Localizables.cards.key_card_monthly_limit_multiples + " \(multiple_amount)"
        case .transaction_daily:
            return Localizables.cards.key_card_daily_limit_multiples + " \(multiple_amount)"
        case .transaction_monthly:
            return Localizables.cards.key_card_monthly_limit_multiples + " \(multiple_amount)"
        case .prepaid_limit_monthly:
            return Localizables.cards.key_card_monthly_limit_multiples + " \(multiple_amount)"
        case .credit_limit_monthly:
            return Localizables.cards.key_card_monthly_limit_multiples + " \(multiple_amount)"
        case .granted_credit:
            return Localizables.cards.key_card_actual_limit_multiples + " \(multiple_amount)"
        default:
            return Localizables.cards.key_card_actual_limit_multiples + " \(multiple_amount)"
        }
    }
    
    var tagName: String {
        
        switch self {
            
        case .atm_daily:
            return "efectivo cajero"
        case .atm_monthly:
            return "efectivo cajero mensual"
        case .pos_daily:
            return "compras"
        case .pos_monthly:
            return "compras mensual"
        case .office_daily:
            return "efectivo oficina"
        case .office_monthly:
            return "efectivo oficina mensual"
        case .transaction_daily:
            return "transacciones"
        case .transaction_monthly:
            return "transacciones mensual"
        case .prepaid_limit_monthly:
            return "prepago mensual"
        case .credit_limit_monthly:
            return "credito mensual"
        case .granted_credit:
            return "consumo mensual"
        default:
            return ""
        }
    }
}

class LimitBO: ModelBO {
    var type: LimitType
    var name: String!
    var amountLimits: [AmountCurrencyBO?]?
    var allowedInterval: AllowedIntervalBO?
    var disposedLimits: [AmountCurrencyBO?]?
    var isEditable: Bool!

    init(limitEntity: LimitEntity?) {

        if let limitTypeId = limitEntity?.id {
            type = LimitType(rawValue: limitTypeId) ?? .unknowm
        } else {
            type = .unknowm
        }

        name = limitEntity?.name

        var amountLimits = [AmountCurrencyBO?]()

        if let amountslimitEntity = limitEntity?.amountLimits {
            for amountLimitEntity in amountslimitEntity {
                amountLimits.append(AmountCurrencyBO(amountCurrency: amountLimitEntity))
            }
        }

        self.amountLimits = amountLimits

        if let allowedIntervalEntity = limitEntity?.allowedInterval {
            allowedInterval = AllowedIntervalBO(allowedIntervalEntity: allowedIntervalEntity)
        }

        var disposedLimits = [AmountCurrencyBO?]()

        if let disposedLimitsEntity = limitEntity?.disposedLimits {
            for disposedLimit in disposedLimitsEntity {
                disposedLimits.append(AmountCurrencyBO(amountCurrency: disposedLimit))
            }
        }

        self.disposedLimits = disposedLimits

        isEditable = false

        if let isEditableEntity = limitEntity?.isEditable {
            isEditable = isEditableEntity
        }

    }

    func amountFormatterForLimit() -> AmountFormatter {

        var amountFormatter = AmountFormatter(codeCurrency: SessionDataManager.sessionDataInstance().mainCurrency)

        if let currentLimitCurrency = amountLimits?.first??.currency, let consumedLimitCurrency = disposedLimits?.first??.currency, let maximumAmount = allowedInterval?.maximumAmount, let minimumAmount = allowedInterval?.minimumAmount {

            if currentLimitCurrency == consumedLimitCurrency && currentLimitCurrency == maximumAmount.currency && currentLimitCurrency == minimumAmount.currency {

                amountFormatter = AmountFormatter(codeCurrency: currentLimitCurrency)
            }
        }

        return amountFormatter
    }

    func minimumLimitDecimalNumber() -> NSDecimalNumber {

        var minimumLimit = NSDecimalNumber.zero

        if let consumedAmountValue = disposedLimits?.first??.amount, let minimumAmount = allowedInterval?.minimumAmount {

            let minimum = max(consumedAmountValue.decimalValue, minimumAmount.amount.decimalValue)
            minimumLimit = NSDecimalNumber(decimal: minimum)
        }

        return minimumLimit
    }

    func minimumLimitFormatter() -> String {

        var minimumFormatter = ""

        if let consumedAmountValue = disposedLimits?.first??.amount, let minimumAmount = allowedInterval?.minimumAmount {

            let minimum = max(consumedAmountValue.decimalValue, minimumAmount.amount.decimalValue)

            let amountFormatter = amountFormatterForLimit()
            let minimumNumber = NSDecimalNumber(decimal: minimum)
            minimumFormatter = amountFormatter.format(amount: minimumNumber)
        }

        return minimumFormatter
    }

    func maximumLimitDecimalNumber() -> NSDecimalNumber {

        var maximumLimit = NSDecimalNumber.zero

        if let maximumAmount = allowedInterval?.maximumAmount {

            let maximum = maximumAmount.amount.decimalValue
            maximumLimit = NSDecimalNumber(decimal: maximum)
        }

        return maximumLimit
    }

    func maximumLimitFormatter() -> String {

        var maximumFormatter = ""

        if let maximumAmount = allowedInterval?.maximumAmount {

            let maximum = maximumAmount.amount.decimalValue

            let amountFormatter = amountFormatterForLimit()
            let maximumNumber = NSDecimalNumber(decimal: maximum)
            maximumFormatter = amountFormatter.format(amount: maximumNumber)
        }

        return maximumFormatter
    }

    func consumedLimitAmount() -> NSDecimalNumber {

        var consumedLimitAmount = NSDecimalNumber.zero

        if let amount = disposedLimits?.first??.amount {
            consumedLimitAmount = amount
        }

        return consumedLimitAmount
    }

    func minimumLimitAmount() -> NSDecimalNumber {

        var minimumLimitAmount = NSDecimalNumber.zero

        if let allowedInterval = allowedInterval, let minimumAmount = allowedInterval.minimumAmount {
            minimumLimitAmount = minimumAmount.amount
        }

        return minimumLimitAmount
    }
    
    func infoMessageMultiplesAndMaximum() -> String {
        
        let amountFormatter = amountFormatterForLimit()
        let multipleAmountNumber = NSDecimalNumber(value: Settings.Limits.multiples_limit_amount)
        let multipleAmountFormatter = amountFormatter.format(amount: multipleAmountNumber)
        
        let maximumFormatter = maximumLimitFormatter()
        
        switch type {
            
        case .atm_daily:
            return String(format: Localizables.cards.key_card_limit_atm_daily_multiples_and_maximum, multipleAmountFormatter, maximumFormatter)
        case .pos_daily:
            return String(format: Localizables.cards.key_card_limit_pos_daily_multiples_and_maximum, multipleAmountFormatter, maximumFormatter)
        case .office_daily:
            return String(format: Localizables.cards.key_card_limit_office_daily_multiples_and_maximum, multipleAmountFormatter, maximumFormatter)
        case .transaction_daily:
            return String(format: Localizables.cards.key_card_limit_transaction_daily_multiples_and_maximum, multipleAmountFormatter, maximumFormatter)
        default:
            return infoMessage()
        }
    }

    func infoMessage() -> String {

        let minimumFormatter = minimumLimitFormatter()
        let maximumFormatter = maximumLimitFormatter()

        if minimumFormatter.isEmpty || maximumFormatter.isEmpty {
            return ""
        }

        switch type {

        case .atm_daily:
            return Localizables.cards.key_card_limit_min_text + " \(minimumFormatter) " + Localizables.cards.key_card_limit_and_max_text + " \(maximumFormatter)"
        case .atm_monthly:
            return Localizables.cards.key_card_minimum_monthly + " \(minimumFormatter) " + Localizables.cards.key_card_limit_and_max_text + " \(maximumFormatter)"
        case .pos_daily:
            return Localizables.cards.key_card_limit_min_text + " \(minimumFormatter) " + Localizables.cards.key_card_limit_and_max_text + " \(maximumFormatter)"
        case .pos_monthly:
            return Localizables.cards.key_card_minimum_monthly + " \(minimumFormatter) " + Localizables.cards.key_card_limit_and_max_text + " \(maximumFormatter)"
        case .office_daily:
            return Localizables.cards.key_card_limit_min_text + " \(minimumFormatter) " + Localizables.cards.key_card_limit_and_max_text + " \(maximumFormatter)"
        case .office_monthly:
            return Localizables.cards.key_card_minimum_monthly + " \(minimumFormatter) " + Localizables.cards.key_card_limit_and_max_text + " \(maximumFormatter)"
        case .transaction_daily:
            return Localizables.cards.key_card_limit_min_text + " \(minimumFormatter) " + Localizables.cards.key_card_limit_and_max_text + " \(maximumFormatter)"
        case .transaction_monthly:
            return Localizables.cards.key_card_minimum_monthly + " \(minimumFormatter) " + Localizables.cards.key_card_limit_and_max_text + " \(maximumFormatter)"
        case .prepaid_limit_monthly:
            return Localizables.cards.key_card_minimum_monthly + " \(minimumFormatter) " + Localizables.cards.key_card_limit_and_max_text + " \(maximumFormatter)"
        case .credit_limit_monthly:
            return Localizables.cards.key_card_minimum_monthly + " \(minimumFormatter) " + Localizables.cards.key_card_limit_and_max_text + " \(maximumFormatter)"
        case .granted_credit:
            return Localizables.cards.key_card_minimum + " \(minimumFormatter) " + Localizables.cards.key_card_limit_and_max_text + " \(maximumFormatter)"
        default:
            return Localizables.cards.key_card_minimum + " \(minimumFormatter) " + Localizables.cards.key_card_limit_and_max_text + " \(maximumFormatter)"
        }

    }

    func messageAlreadyConsumed() -> String {

        let minimumFormatter = minimumLimitFormatter()

        if minimumFormatter.isEmpty {
            return ""
        }

        switch type {

        case .atm_daily:
            return Localizables.cards.key_card_today_you_have_retired + " \(minimumFormatter)"
        case .atm_monthly:
            return Localizables.cards.key_card_this_month_you_have_retired + " \(minimumFormatter)"
        case .pos_daily:
            return Localizables.cards.key_card_today_already_bought + " \(minimumFormatter)"
        case .pos_monthly:
            return Localizables.cards.key_card_this_month_bought + " \(minimumFormatter)"
        case .office_daily:
            return Localizables.cards.key_card_today_you_have_retired + " \(minimumFormatter)"
        case .office_monthly:
            return Localizables.cards.key_card_this_month_you_have_retired + " \(minimumFormatter)"
        case .transaction_daily:
            return Localizables.cards.key_card_limit_today_consumed_text + " \(minimumFormatter)"
        case .transaction_monthly:
            return Localizables.cards.key_card_this_month_consumed + " \(minimumFormatter)"
        case .prepaid_limit_monthly:
            return Localizables.cards.key_card_this_month_consumed + " \(minimumFormatter)"
        case .credit_limit_monthly:
            return Localizables.cards.key_card_this_month_consumed + " \(minimumFormatter)"
        case .granted_credit:
            return Localizables.cards.key_card_already_consumed + " \(minimumFormatter)"
        default:
            return Localizables.cards.key_card_already_consumed + " \(minimumFormatter)"
        }
    }

    func messageMinimum() -> String {

        let minimumFormatter = minimumLimitFormatter()

        if minimumFormatter.isEmpty {
            return ""
        }

        switch type {

        case .atm_daily:
            return Localizables.cards.key_card_limit_min_text + " \(minimumFormatter)"
        case .atm_monthly:
            return Localizables.cards.key_card_minimum_this_month + " \(minimumFormatter)"
        case .pos_daily:
            return Localizables.cards.key_card_limit_min_text + " \(minimumFormatter)"
        case .pos_monthly:
            return Localizables.cards.key_card_minimum_this_month + " \(minimumFormatter)"
        case .office_daily:
            return Localizables.cards.key_card_limit_min_text + " \(minimumFormatter)"
        case .office_monthly:
            return Localizables.cards.key_card_minimum_this_month + " \(minimumFormatter)"
        case .transaction_daily:
            return Localizables.cards.key_card_limit_min_text + " \(minimumFormatter)"
        case .transaction_monthly:
            return Localizables.cards.key_card_minimum_this_month + " \(minimumFormatter)"
        case .prepaid_limit_monthly:
            return Localizables.cards.key_card_minimum_this_month + " \(minimumFormatter)"
        case .credit_limit_monthly:
            return Localizables.cards.key_card_minimum_this_month + " \(minimumFormatter)"
        case .granted_credit:
            return Localizables.cards.key_card_minimum + " \(minimumFormatter)"
        default:
            return Localizables.cards.key_card_minimum + " \(minimumFormatter)"
        }
    }

}

extension LimitBO: Equatable {

    public static func == (lhs: LimitBO, rhs: LimitBO) -> Bool {

        if let leftAmountLimits = lhs.amountLimits, let rightAmountLimits = rhs.amountLimits {

            if leftAmountLimits.count != rightAmountLimits.count {
                return false
            }
        }

        if let leftAmountLimits = lhs.amountLimits, let rightAmountLimits = rhs.amountLimits {

            for index in 0..<leftAmountLimits.count {
                if leftAmountLimits[index]?.amount != rightAmountLimits[index]?.amount || leftAmountLimits[index]?.currency != rightAmountLimits[index]?.currency {
                    return false
                }
            }
        }

        return lhs.name == rhs.name
            && lhs.type == rhs.type
    }

}

class AllowedIntervalBO: ModelBO {
    var maximumAmount: AmountCurrencyBO?
    var minimumAmount: AmountCurrencyBO?

    init(allowedIntervalEntity: AllowedIntervalEntity) {

        if let maximumAmountEntity = allowedIntervalEntity.maximumAmount {
            maximumAmount = AmountCurrencyBO(amountCurrency: maximumAmountEntity)
        }

        if let minimumAmountEntity = allowedIntervalEntity.minimumAmount {
            minimumAmount = AmountCurrencyBO(amountCurrency: minimumAmountEntity)
        }
    }
}
