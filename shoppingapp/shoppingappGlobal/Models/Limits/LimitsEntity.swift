//
//  LimitsEntity.swift
//  shoppingapp
//
//  Created by jesus.martinez on 24/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import HandyJSON

class LimitsEntity: ModelEntity, HandyJSON {
    var status: Int!
    var data: [LimitEntity]?

    required init() {}

}

struct LimitEntity: HandyJSON {
    var id: String!
    var name: String!
    var amountLimits: [AmountCurrencyEntity]?
    var allowedInterval: AllowedIntervalEntity?
    var disposedLimits: [AmountCurrencyEntity]?
    var isEditable: Bool?

}

struct AllowedIntervalEntity: HandyJSON {
    var maximumAmount: AmountCurrencyEntity?
    var minimumAmount: AmountCurrencyEntity?

}
