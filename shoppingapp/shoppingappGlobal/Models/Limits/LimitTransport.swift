//
//  LimitTransport.swift
//  shoppingapp
//
//  Created by Marcos on 26/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class LimitTransport: ModelTransport {

    let limit: LimitBO
    let cardId: String
    let cardType: CardTypeBO?
    
    init(withLimit limit: LimitBO, andCardId cardId: String, cardType: CardTypeBO? = nil) {
        self.limit = limit
        self.cardId = cardId
        self.cardType = cardType
    }

}

extension LimitTransport: Equatable {
    static func == (lhs: LimitTransport, rhs: LimitTransport) -> Bool {
        return lhs.limit == rhs.limit
            && lhs.cardId == rhs.cardId
            && lhs.cardType == rhs.cardType
    }
}
