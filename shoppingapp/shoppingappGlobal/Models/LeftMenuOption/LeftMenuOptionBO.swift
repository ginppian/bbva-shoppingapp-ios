//
//  LeftMenuOptionBO.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 18/10/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation

enum LeftMenuOption: String {
    case about
    case settings
    case help
    case web
    case call
    case mobileToken
    case unknowm
}

class LeftMenuOptionBO: ModelBO {

    let image: String
    let title: String
    var option: LeftMenuOption

    init(image: String, title: String, option: String) {

        self.image = image
        self.title = title
        let menuOption = LeftMenuOption(rawValue: option) ?? LeftMenuOption.unknowm
        self.option = menuOption
    }
}
