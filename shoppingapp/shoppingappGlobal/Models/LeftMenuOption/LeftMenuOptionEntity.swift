//
//  LeftMenuOptionEntity.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 18/10/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation

class LeftMenuOptionEntity: ModelEntity {

    let image: String
    let title: String
    let option: String

    init(image: String, title: String, option: String) {

        self.image = image
        self.title = title
        self.option = option
    }
}
