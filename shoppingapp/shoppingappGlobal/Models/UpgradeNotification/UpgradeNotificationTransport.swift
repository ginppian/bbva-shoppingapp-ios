//
//  UpgradeNotificationTransport.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 7/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class UpgradeNotificationTransport: ModelTransport {
    
    let isMandatory: Bool?
    let url: String?
    let message: String?
    
    init(_ isMandatory: Bool?, _ url: String?, _ message: String?) {
        
        self.isMandatory = isMandatory
        self.url = url
        self.message = message
    }
}

extension UpgradeNotificationTransport: Equatable {
    
    static func == (lhs: UpgradeNotificationTransport, rhs: UpgradeNotificationTransport) -> Bool {
        return lhs.isMandatory == rhs.isMandatory &&
            lhs.url == rhs.url &&
            lhs.message == rhs.message
    }
}
