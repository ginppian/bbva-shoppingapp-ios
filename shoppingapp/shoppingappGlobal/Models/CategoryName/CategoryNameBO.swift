//
//  CategoryNameBO.swift
//  shoppingapp
//
//  Created by Arturo Marzo de la Fuente on 31/10/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class CategoryNameBO: ModelBO {

    var language: String!
    var name: String!

    init(categoryNameEntity: CategoryNameEntity) {
        
        language = categoryNameEntity.language
        name = categoryNameEntity.name
    }
}
