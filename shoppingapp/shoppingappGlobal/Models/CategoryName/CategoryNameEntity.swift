//
//  CategoryNameEntity.swift
//  shoppingapp
//
//  Created by Arturo Marzo de la Fuente on 31/10/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import HandyJSON

class CategoryNameEntity: ModelEntity, HandyJSON {

    var language: String!
    var name: String!

    required init() {}
}
