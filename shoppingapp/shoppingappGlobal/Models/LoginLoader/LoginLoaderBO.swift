//
//  ModelsBO.swift
//  shoppingapp
//
//  Created by ruben.fernandez on 9/6/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation

//GranTingTicket BO
class GrandTingTicketBO: ModelBO {

    var authenticationState: String?
    var clientStatus: String?
    var multistepProcessId: String?

    init(grantingTicket: GrandTingTicketEntity?) {
        authenticationState = grantingTicket?.authenticationResponse?.authenticationState
        clientStatus = grantingTicket?.backendUserResponse?.clientStatus
        multistepProcessId = grantingTicket?.backendUserResponse?.multistepProcessId
    }
}

//User Model
class UserBO: ModelBO {

    var customerId: String?
    var firstName: String?
    var lastName: String?
    var startDate: Date?

    init(user: UserEntity) {
        customerId = user.data?[0].customerId
        firstName = user.data?[0].firstName
        lastName = user.data?[0].lastName
        if let unformattedStarDate = user.data?[0].residence?.startDate {

            startDate = ServiceDateFormat.serviceDateFormatter(service: .customers).date(from: unformattedStarDate)
        }

    }

    func completeUserName() -> String {

        let name = firstName ?? ""
        let surname = lastName ?? ""

        return name + " " + surname
    }
}
