//
//  ModelEntity.swift
//  shoppingapp
//
//  Created by ruben.fernandez on 9/6/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation
import HandyJSON

//GrandTingTicker Model
class GrandTingTicketEntity: ModelEntity, HandyJSON {

    var authenticationResponse: AutenticationResponseEntity?
    var backendUserResponse: BackendUserResponse?

    required init() {}
}

struct AutenticationResponseEntity: HandyJSON {
    var authenticationState: String?
    var authenticationData: [Any]?
}

struct BackendUserResponse: HandyJSON {
    var userType: String?
    var clientId: String?
    var clientStatus: String?
    var accountingTerminal: String?
    var multistepProcessId: String?
}

//Customer Data Model
class UserEntity: ModelEntity, HandyJSON {
    var data: [CustomerEntity]?
    var apiInfo: ApiInfoEntity?

    required init() {}

}

struct CustomerEntity: HandyJSON {
    var customerId: String?
    var firstName: String?
    var lastName: String?
    var suffix: String?
    var birthData: BirthDateEntity?
    var nationalities: [GenericIdAndNameEntity]?
    var identityDocuments: [IdentityDocumentsEntity]?
    var membershipDate: String?
    var personalTitle: GenericIdAndNameEntity?
    var maritalStatus: GenericIdAndNameEntity?
    var gender: GenericIdAndNameEntity?
    var bank: BankEntity?
    var residence: ResidenceEntity?
}

struct BirthDateEntity: HandyJSON {
    var city: String?
    var state: GenericIdAndNameEntity?
    var country: GenericIdAndNameEntity?
    var birthDate: String?
}

struct IdentityDocumentsEntity: HandyJSON {
    var documentType: GenericIdAndNameEntity?
    var country: GenericIdAndNameEntity?
    var state: GenericIdAndNameEntity?
    var status: GenericIdAndNameEntity?
    var issueDate: String?
    var expirationDate: String?
    var documentNumber: String?
}

struct BankEntity: HandyJSON {
    var bankId: String?
    var name: String?
    var branch: BranchEntity?

}

struct BranchEntity: HandyJSON {
    var branchId: String?
    var name: String?
    var costCenter: String?
}

struct ResidenceEntity: HandyJSON {
    var country: GenericIdAndNameEntity?
    var residenceType: GenericIdAndNameEntity?
    var startDate: String?
}

struct ApiInfoEntity: HandyJSON {
    var category: String?
    var name: String?
    var version: String?
}

struct GenericIdAndNameEntity: HandyJSON {
    var id: String?
    var name: String?
}
