//
//  DidacticAreasEntity.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 7/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import HandyJSON

class DidacticAreasEntity: ModelEntity, HandyJSON {

    var version: String = ""
    var data: [DidacticAreaEntity]?
    var categoriesData: DidacticCategoriesEntity?

    required init() {}
}

struct DidacticAreaEntity: HandyJSON {

    var helpId: String?
    var type: String?
    var imageThumb: String?
    var imageBackground: String?
    var url: String?
    var platform: String?
    var startDate: String?
    var endDate: String?
    var titleColor: ColorEntity?
    var descriptionColor: ColorEntity?
    var descriptionOnTapColor: ColorEntity?
    var texts: [TextsEntity]?
}

struct DidacticCategoriesEntity: HandyJSON {

    var defaultCategory: [DidacticCategoryEntity]?
    var monday: TimeEntity?
    var tuesday: TimeEntity?
    var wednesday: TimeEntity?
    var thursday: TimeEntity?
    var friday: TimeEntity?
    var saturday: TimeEntity?
    var sunday: TimeEntity?
}

struct DidacticCategoryEntity: HandyJSON {

    var categoryId: String?
    var imageThumb: String?
    var imageBackground: String?
    var titleColor: ColorEntity?
    var descriptionColor: ColorEntity?
    var descriptionOnTapColor: ColorEntity?
    var texts: [TextsEntity]?
    var categoryName: [CategoryNameEntity]?
}

struct TimeEntity: HandyJSON {

    var morning: [DidacticCategoryEntity]?
    var afternoon: [DidacticCategoryEntity]?
    var night: [DidacticCategoryEntity]?

}
