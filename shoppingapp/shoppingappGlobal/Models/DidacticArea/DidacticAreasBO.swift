//
//  DidacticAreasBO.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 7/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

enum TypeOfDidacticArea: String {
    case didactic
    case offer
    case unknowm
}

enum TypeOfPlatform: String {
    case both
    case ios
    case android
    case unknowm
}

enum DaysOfWeek: String {
    case sunday
    case monday
    case tuesday
    case wednesday
    case thursday
    case friday
    case saturday
}

enum TimesOfDay: String {
    case morning
    case afternoon
    case night
}

class DidacticAreasBO: ModelBO {

    var version: String
    var items: [DidacticAreaBO]?
    var categories: DidacticCategoriesBO?

    var itemsDidacticArea: [DidacticAreaBO]?
    var itemsIdSpecial: [DidacticAreaBO]?

    init() {
        version = ""
    }

    init(didacticAreasEntity: DidacticAreasEntity) {

        version = didacticAreasEntity.version

        var didacticAreaBO: [DidacticAreaBO]

        if let didacticAreasEntity = didacticAreasEntity.data {

            didacticAreaBO = [DidacticAreaBO]()

            for didacticAreaEntity in didacticAreasEntity {
                didacticAreaBO.append(DidacticAreaBO(didacticAreaEntity: didacticAreaEntity))
            }

            items = [DidacticAreaBO]()
            items = didacticAreaBO

            itemsDidacticArea = items?.filter { item -> Bool in

                if (item.type == .offer) && (item.startDate != nil) && (item.endDate != nil) {
                    return false
                }

                if item.platform != nil {

                    if item.platform != .both && item.platform != .ios {
                        return false
                    }
                }

                return true
            }

            itemsIdSpecial = items?.filter { item -> Bool in

                if (item.type == .offer && item.startDate != nil && item.endDate != nil), let platform = item.platform, (platform == .both || platform == .ios) {
                        
                    return true
                }

                return false
            }
        }

        if let categoriesEntity = didacticAreasEntity.categoriesData {
           categories = DidacticCategoriesBO(didacticCategoriesEntity: categoriesEntity)
        }

    }
}

class DidacticAreaBO: ModelBO {

    var helpId: String?
    var type: TypeOfDidacticArea?
    var imageThumb: String?
    var imageBackground: String?
    var url: URL?
    var platform: TypeOfPlatform?
    var startDate: Date?
    var endDate: Date?
    var titleColor: ColorBO?
    var descriptionColor: ColorBO?
    var descriptionOnTapColor: ColorBO?
    var texts: [TextBO]?

    init(didacticAreaEntity: DidacticAreaEntity) {

        helpId = didacticAreaEntity.helpId

        type = .unknowm

        if let typeDidacticArea = didacticAreaEntity.type {
            type = TypeOfDidacticArea(rawValue: typeDidacticArea) ?? TypeOfDidacticArea.unknowm
        }

        imageThumb = didacticAreaEntity.imageThumb
        imageBackground = didacticAreaEntity.imageBackground
        
        if let urlEntity = didacticAreaEntity.url {
            url = URL(string: urlEntity)
        }

        if let device = didacticAreaEntity.platform {
            platform = TypeOfPlatform(rawValue: device) ?? TypeOfPlatform.unknowm
        }

        if let startDateEntity = didacticAreaEntity.startDate {
            startDate = Date.date(fromServerString: startDateEntity, withFormat: Date.DATE_FORMAT_LONG_SERVER_OTHER)
        }

        if let endDateEntity = didacticAreaEntity.endDate {
            endDate = Date.date(fromServerString: endDateEntity, withFormat: Date.DATE_FORMAT_LONG_SERVER_OTHER)
        }

        if let titleEntity = didacticAreaEntity.titleColor {
            titleColor = ColorBO(colorEntity: titleEntity)
        }

        if let descriptionEntity = didacticAreaEntity.descriptionColor {
            descriptionColor = ColorBO(colorEntity: descriptionEntity)
        }

        if let descriptionOnTapEntity = didacticAreaEntity.descriptionOnTapColor {
            descriptionOnTapColor = ColorBO(colorEntity: descriptionOnTapEntity)
        }

        var textBO = [TextBO]()

        if let textsEntity = didacticAreaEntity.texts {
            for textEntity in textsEntity {
                textBO.append(TextBO(textEntity: textEntity))
            }
        }

        texts = textBO

    }

    func getSelectedLanguage(forKey key: String) -> TextBO? {

        var textBo: TextBO?

        if let textList = texts {
            for textBO in textList where textBO.language == key {
                textBo = textBO
            }

        }
        return textBo
    }

    func getImageURL(forUrl url: String) -> String {

        let imageUrlWithPlatform = url.replacingOccurrences(of: "%so%", with: Constants.PLATFORM)
        let imageUrl = imageUrlWithPlatform.replacingOccurrences(of: "%size%", with: ImagesScales.imageScale)

        return imageUrl
    }
}

class DidacticCategoriesBO: ModelBO {

    var defaultCategory: [DidacticCategoryBO]?
    var monday: TimeBO?
    var tuesday: TimeBO?
    var wednesday: TimeBO?
    var thursday: TimeBO?
    var friday: TimeBO?
    var saturday: TimeBO?
    var sunday: TimeBO?

    init(didacticCategoriesEntity: DidacticCategoriesEntity) {

        var defaultCategoryBO: [DidacticCategoryBO]

        if let defaultCategoriesEntity = didacticCategoriesEntity.defaultCategory {

            defaultCategoryBO = [DidacticCategoryBO]()

            for defaultCategoryEntity in defaultCategoriesEntity {
                defaultCategoryBO.append(DidacticCategoryBO(didacticCategoryEntity: defaultCategoryEntity))
            }

            defaultCategory = defaultCategoryBO
        }

        if let mondayEntity = didacticCategoriesEntity.monday {
            monday = TimeBO(timeEntity: mondayEntity)
        }

        if let tuesdayEntity = didacticCategoriesEntity.tuesday {
            tuesday = TimeBO(timeEntity: tuesdayEntity)
        }

        if let wednesdayEntity = didacticCategoriesEntity.wednesday {
            wednesday = TimeBO(timeEntity: wednesdayEntity)
        }

        if let thursdayEntity = didacticCategoriesEntity.thursday {
            thursday = TimeBO(timeEntity: thursdayEntity)
        }

        if let fridayEntity = didacticCategoriesEntity.friday {
            friday = TimeBO(timeEntity: fridayEntity)
        }

        if let saturdayEntity = didacticCategoriesEntity.saturday {
            saturday = TimeBO(timeEntity: saturdayEntity)
        }

        if let sundayEntity = didacticCategoriesEntity.sunday {
            sunday = TimeBO(timeEntity: sundayEntity)
        }

    }

    func getTimeBO(byWeekday weekday: String) -> TimeBO? {
        switch weekday {
        case DaysOfWeek.monday.rawValue:
            return monday
        case DaysOfWeek.tuesday.rawValue:
            return tuesday
        case DaysOfWeek.wednesday.rawValue:
            return wednesday
        case DaysOfWeek.thursday.rawValue:
            return thursday
        case DaysOfWeek.friday.rawValue:
            return friday
        case DaysOfWeek.saturday.rawValue:
            return saturday
        case DaysOfWeek.sunday.rawValue:
            return sunday
        default:
            return nil
        }
    }
}

class DidacticCategoryBO: ModelBO {

    var categoryId: String?
    var imageThumb: String?
    var imageBackground: String?
    var titleColor: ColorBO?
    var descriptionColor: ColorBO?
    var descriptionOnTapColor: ColorBO?
    var texts: [TextBO]?
    var categoryName: [CategoryNameBO]?
    
    init(didacticCategoryEntity: DidacticCategoryEntity) {

        categoryId = didacticCategoryEntity.categoryId
        imageThumb = didacticCategoryEntity.imageThumb
        imageBackground = didacticCategoryEntity.imageBackground

        if let titleEntity = didacticCategoryEntity.titleColor {
            titleColor = ColorBO(colorEntity: titleEntity)
        }

        if let descriptionEntity = didacticCategoryEntity.descriptionColor {
            descriptionColor = ColorBO(colorEntity: descriptionEntity)
        }

        if let descriptionOnTapEntity = didacticCategoryEntity.descriptionOnTapColor {
            descriptionOnTapColor = ColorBO(colorEntity: descriptionOnTapEntity)
        }

        var textBO = [TextBO]()

        if let textsEntity = didacticCategoryEntity.texts {
            for textEntity in textsEntity {
                textBO.append(TextBO(textEntity: textEntity))
            }
        }

        texts = textBO
        
        var categoryNameBO = [CategoryNameBO]()
        
        if let categoryNames = didacticCategoryEntity.categoryName {
            for categoryName in categoryNames {
                categoryNameBO.append(CategoryNameBO(categoryNameEntity: categoryName))
            }
        }
        
        categoryName = categoryNameBO
    }

    func getSelectedLanguage(forKey key: String) -> TextBO? {

        var textBo: TextBO?

        if let textList = texts {
            for textBO in textList where textBO.language == key {
                    textBo = textBO
            }

        }
        return textBo
    }
    
    func getCategoryName(forLanguageKey key: String) -> CategoryNameBO? {
        
        var name: CategoryNameBO?
        
        if let categoryNamesList = categoryName {
            for categoryNameBO in categoryNamesList where categoryNameBO.language == key {
                name = categoryNameBO
            }
        }
        return name
    }

    func getImageURL(forUrl url: String) -> String {

        let imageUrlWithPlatform = url.replacingOccurrences(of: "%so%", with: Constants.PLATFORM)
        let imageUrl = imageUrlWithPlatform.replacingOccurrences(of: "%size%", with: ImagesScales.imageScale)

        return imageUrl
    }
}

class TimeBO: ModelBO {

    var morning: [DidacticCategoryBO]?
    var afternoon: [DidacticCategoryBO]?
    var night: [DidacticCategoryBO]?

    init(timeEntity: TimeEntity) {

        var morningBO: [DidacticCategoryBO]
        var afternoonBO: [DidacticCategoryBO]
        var nightBO: [DidacticCategoryBO]

        if let morningsEntity = timeEntity.morning {

            morningBO = [DidacticCategoryBO]()

            for morningEntity in morningsEntity {
                morningBO.append(DidacticCategoryBO(didacticCategoryEntity: morningEntity))
            }

            morning = morningBO
        }

        if let afternoonsEntity = timeEntity.afternoon {

            afternoonBO = [DidacticCategoryBO]()

            for afternoonEntity in afternoonsEntity {
                afternoonBO.append(DidacticCategoryBO(didacticCategoryEntity: afternoonEntity))
            }

            afternoon = afternoonBO
        }

        if let nightsEntity = timeEntity.night {

            nightBO = [DidacticCategoryBO]()

            for nightEntity in nightsEntity {
                nightBO.append(DidacticCategoryBO(didacticCategoryEntity: nightEntity))
            }

            night = nightBO
        }
    }

    func getDidacticCategoryBO(byTime time: String) -> [DidacticCategoryBO]? {
        switch time {
        case TimesOfDay.morning.rawValue:
            return morning
        case TimesOfDay.afternoon.rawValue:
            return afternoon
        case TimesOfDay.night.rawValue:
            return night
        default:
            return nil
        }
    }
}
