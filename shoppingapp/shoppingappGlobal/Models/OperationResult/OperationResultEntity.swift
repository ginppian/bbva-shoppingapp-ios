//
//  OperationResultEntity.swift
//  shoppingapp
//
//  Created by jesus.martinez on 1/3/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import HandyJSON

class OperationResultEntity: ModelEntity, HandyJSON {
    var status: Int! = 0
    var data: OperationResultDataEntity?

    required init() {}
}

struct OperationResultDataEntity: HandyJSON {
    var operationDate: String?
    var operationNumber: String?
}
