//
//  OperationResultBO.swift
//  shoppingapp
//
//  Created by jesus.martinez on 1/3/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
class OperationResultBO: ModelBO {
    var status: Int! = 0
    var data: OperationResultDataBO?

    init(entity: OperationResultEntity) {

        status = entity.status
        data = OperationResultDataBO(entity: entity.data)
    }

}

class OperationResultDataBO: ModelBO {
    var operationDate: Date?
    var operationNumber: String?

    init(entity: OperationResultDataEntity?) {

        if let operationDateEntity = entity?.operationDate {

             operationDate = Date.date(fromServerString: operationDateEntity, withFormat: Date.DATE_FORMAT_LONG_SERVER_OTHER)

        }

        operationNumber = entity?.operationNumber

    }
}
