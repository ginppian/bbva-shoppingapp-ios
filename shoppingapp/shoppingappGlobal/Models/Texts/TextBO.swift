//
//  TextBO.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 7/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class TextBO: ModelBO {

    var language: String!
    var title: String!
    var description: String!

    init(textEntity: TextsEntity) {
        language = textEntity.language
        title = textEntity.title
        description = textEntity.description
    }
}
