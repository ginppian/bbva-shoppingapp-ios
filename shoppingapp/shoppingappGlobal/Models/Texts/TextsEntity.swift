//
//  TextsEntity.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 7/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import HandyJSON

class TextsEntity: ModelEntity, HandyJSON {

    var language: String!
    var title: String!
    var description: String!

    required init() {}
}
