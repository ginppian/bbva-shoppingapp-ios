//
//  TransactionFilterTransport.swift
//  shoppingapp
//
//  Created by ruben.fernandez on 18/8/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation

class TransactionFilterTransport: ModelTransport { // Transport data structure (not Entity)

    var pageSize: Int?
    var conceptText: String?
    var fromDateText: String?
    var toDateText: String?
    var filtertype: TransactionFilterType = TransactionFilterType.all
    var fromAmountText: String?
    var toAmountText: String?
    var cardBO: CardBO?

    func isUnitialized() -> Bool {

        return ((self.conceptText == nil || self.conceptText == "")
        && (self.fromDateText == nil || self.fromDateText == "")
        && (self.toDateText == nil || self.toDateText == "")
        && self.filtertype == .all
        && (self.fromAmountText == nil || self.fromAmountText == "")
        && (self.toAmountText == nil || self.toAmountText == ""))
    }
}

extension TransactionFilterTransport: Equatable {

    public static func == (lhs: TransactionFilterTransport, rhs: TransactionFilterTransport) -> Bool {

        return lhs.conceptText == rhs.conceptText
            && lhs.fromDateText == rhs.fromDateText
            && lhs.toDateText == rhs.toDateText
            && lhs.filtertype == rhs.filtertype
            && lhs.fromAmountText == rhs.fromAmountText
            && lhs.toAmountText == rhs.toAmountText
            && lhs.cardBO?.cardId == rhs.cardBO?.cardId
    }

}
