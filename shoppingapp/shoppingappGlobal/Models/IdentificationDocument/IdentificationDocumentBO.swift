//
//  IdentificationDocumentBO.swift
//  shoppingapp
//
//  Created by Javier Pino on 18/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation

class IdentificationDocumentBO: ModelBO {

    let code: String
    let title: String

    init(code: String, title: String) {
        self.code = code
        self.title = title
    }
}
