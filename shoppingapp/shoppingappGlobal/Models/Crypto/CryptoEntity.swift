//
//  CryptoEntity.swift
//  shoppingappMX
//
//  Created by Armando Vazquez on 5/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

//Points out relatship between entities used libMCrypto
protocol CryptoEntity { }

/// Holds the Public Key data
struct PublicKeyEntity: CryptoEntity {
    var check: String?
    var value: String

    init(keyData: KpubData) {
        check = keyData.kPubCheck
        value = keyData.kPubValue
    }
}

///Holds decryption data
struct HostDataEntity: CryptoEntity {
    var check: String?
    var data: String

    ///Initializer for the decryption method
    init(data: String, check: String?) {
        self.check = check
        self.data = data
    }

    ///Initializer for the encryption method
    init(hostData: HostData) {
        self.check = hostData.dataCheck
        self.data = hostData.hostData
    }
}
