//
//  ConfigureNotificationsTransport.swift
//  shoppingapp
//
//  Created by Arturo Marzo de la Fuente on 09/01/2019.
//  Copyright © 2019 BBVA. All rights reserved.
//

import Foundation
import HandyJSON

struct ConfigureNotificationsTransport: HandyJSON {
    
    var isPermissionAccepted: Bool
    
    init() {
        self.init(isPermissionAccepted: false )
    }
    
    init(isPermissionAccepted: Bool) {
        self.isPermissionAccepted = isPermissionAccepted
    }
}
