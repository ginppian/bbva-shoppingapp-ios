//
//  ShoppingMapListTransport.swift
//  shoppingapp
//
//  Created by Marcos on 3/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class ShoppingMapListTransport: ModelTransport {
    var promotions = [PromotionBO]()
    var userLocation: GeoLocationBO?
    var storeLocation: GeoLocationBO?

}
