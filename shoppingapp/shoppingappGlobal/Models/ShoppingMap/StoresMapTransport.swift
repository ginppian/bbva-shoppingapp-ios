//
//  StoresMapTransport.swift
//  shoppingappGlobal
//
//  Created by Developer on 18/07/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class StoresMapTransport: ModelTransport {
    
    var promotionBO: PromotionBO?
    var selectedStore: StoreBO?
    var userLocation: GeoLocationBO?
    var category: CategoryBO?

}
