//
//  ColorBO.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 7/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class ColorBO: ModelBO {

    var alpha: Float!
    var hexString: String!

    init(colorEntity: ColorEntity) {
        alpha = colorEntity.alpha
        hexString = colorEntity.hexString
    }
}
