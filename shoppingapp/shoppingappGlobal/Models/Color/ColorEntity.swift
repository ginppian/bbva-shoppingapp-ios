//
//  ColorEntity.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 7/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import HandyJSON

class ColorEntity: ModelEntity, HandyJSON {

    var alpha: Float!
    var hexString: String!

    required init() {}
}
