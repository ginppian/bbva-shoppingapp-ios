//
//  ErrorDisplay.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 11/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation

class ErrorDisplay: ModelDisplay {

    var iconNamed = ""
    var iconColor: UIColor
    var message: String?
    var codeMessage: String?
    var showCancel = false
    var okTitle: String?
    var messageAttributed: NSAttributedString?

    init (error: ErrorBO?) {

        message = error?.errorMessage()
        codeMessage = error?.errorCode()

        showCancel = error?.allowCancel ?? false
        okTitle = error?.okButtonTitle ?? Localizables.common.key_common_accept_text
        messageAttributed = error?.messageAttributed

        switch error?.errorType {
        case .warning?:
            iconNamed = ConstantsImages.Common.alert_icon
            iconColor = .BBVALIGHTRED
        case .info?:
            iconNamed = ConstantsImages.Common.info_icon
            iconColor = .BBVALIGHTBLUE
        default:
            iconNamed = ConstantsImages.Common.alert_icon
            iconColor = .BBVALIGHTRED
        }

    }

    init() {
        iconColor = .BBVALIGHTRED
    }
}
