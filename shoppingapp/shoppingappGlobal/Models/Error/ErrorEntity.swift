//
//  ErrorEntity.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 11/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation
import HandyJSON

extension _ExtendCustomModelType where Self: ErrorEntity {

     mutating func didFinishMapping() {
        genericServerError = true
    }
}

class ErrorEntity: ModelEntity, HandyJSON {
    var message: String! {
        didSet {
            genericServerError = false
        }
    }
    var code: String!
    var httpStatus: Int!
    var genericServerError: Bool = false

    required init() {}

    init(message: String) {
        self.message = message
    }

    init(_ message: String, code: String) {
        self.message = message
        self.code = code
    }

    convenience init(_ message: String, code: Int) {
        self.init(message, code: String(code))
    }
    
    convenience init(message: String, httpStatus: Int) {
        self.init(message, code: String(httpStatus))
        self.httpStatus = httpStatus
    }

    func mapping(mapper: HelpingMapper) {

        mapper.specify(property: &message, name: "error-message")
        mapper.specify(property: &code, name: "error-code")
        mapper.specify(property: &httpStatus, name: "http-status")

    }
}
