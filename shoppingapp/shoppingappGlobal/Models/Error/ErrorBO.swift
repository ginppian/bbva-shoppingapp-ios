//
//  ErrorBO.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 11/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation

enum ErrorCode: String {
    case expiredSession = "68"
}

class ErrorBO: ModelBO {

    var status: Int?
    var code: String?
    private var message: String?
    var isErrorShown = false
    var okButtonTitle: String?
    var messageAttributed: NSAttributedString?

    var errorType: ErrorType
    var allowCancel = false

    init (error: ErrorEntity?) {

        // dto
        if let error = error {
            self.status = error.httpStatus
            self.message = error.genericServerError ? Localizables.common.key_common_generic_error_text : error.message
            self.code = error.code
        }

        // dao
        self.isErrorShown = false

        errorType = .warning
    }

    init(message: String, errorType: ErrorType, allowCancel: Bool) {

        self.message = message
        self.errorType = errorType
        self.allowCancel = allowCancel
        self.isErrorShown = false

    }
    init(messageAttributed: NSAttributedString, errorType: ErrorType, allowCancel: Bool, okTitle: String) {

        self.messageAttributed = messageAttributed
        self.errorType = errorType
        self.allowCancel = allowCancel
        self.okButtonTitle = okTitle
        self.isErrorShown = false

    }
    init() {
        errorType = .warning
    }

    func sessionExpired() -> Bool {

        guard let httpStatus = status, let errorCode = code else {
            return false
        }

        return httpStatus == ConstantsHTTPCodes.STATUS_403 && errorCode == ErrorCode.expiredSession.rawValue

    }

    func errorMessage() -> String? {
        if sessionExpired() {
            return Localizables.login.key_expired_tsec
        } else {
            return message
        }
    }
    
    func errorCode() -> String? {
        if sessionExpired() {
            return nil
        } else {
            return code
        }
    }
}

extension ErrorBO: Equatable {
    public static func == (lhs: ErrorBO, rhs: ErrorBO) -> Bool {
        return lhs.status == rhs.status &&
                lhs.code == rhs.code &&
                lhs.isErrorShown == rhs.isErrorShown &&
                lhs.okButtonTitle == rhs.okButtonTitle &&
                lhs.messageAttributed == rhs.messageAttributed &&
                lhs.errorType == rhs.errorType &&
                lhs.allowCancel == rhs.allowCancel
    }
}

enum ErrorType {
    case warning
    case info
}
