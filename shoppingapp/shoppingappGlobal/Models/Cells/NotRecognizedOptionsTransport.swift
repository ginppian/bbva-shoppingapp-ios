//
//  NotRecognizedOptionsTransport.swift
//  shoppingapp
//
//  Created by AZIZEBULBUL on 08/08/2018.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import HandyJSON

struct NotRecognizedOptionsTransport: HandyJSON {

    var isBlockHidden: Bool
    var isPowerOffHidden: Bool
    var isCallHidden: Bool
    var isLoginHidden: Bool

    init() {
        self.init(isBlockHidden: false, isPowerOffHidden: false, isCallHidden: false, isLoginHidden: false)
    }

    init(isBlockHidden: Bool, isPowerOffHidden: Bool, isCallHidden: Bool, isLoginHidden: Bool) {
        self.isBlockHidden = isBlockHidden
        self.isPowerOffHidden = isPowerOffHidden
        self.isCallHidden = isCallHidden
        self.isLoginHidden = isLoginHidden
    }

}
