//
//  AboutOptionsTransport.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 26/7/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import HandyJSON

struct AboutOptionsTransport: HandyJSON {

    var isLegalAdviceLinkHidden: Bool
    var isVersionLinkHidden: Bool
    var isRateLinkHidden: Bool
    var isShareLinkHidden: Bool

    init() {
        self.init(isLegalAdviceLinkHidden: false, isVersionLinkHidden: false, isRateLinkHidden: true, isShareLinkHidden: false)
    }

    init(isLegalAdviceLinkHidden: Bool, isVersionLinkHidden: Bool, isRateLinkHidden: Bool, isShareLinkHidden: Bool) {
        self.isLegalAdviceLinkHidden = isLegalAdviceLinkHidden
        self.isVersionLinkHidden = isVersionLinkHidden
        self.isRateLinkHidden = isRateLinkHidden
        self.isShareLinkHidden = isShareLinkHidden
    }
}
