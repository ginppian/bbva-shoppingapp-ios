//
//  AppInstalledTransport.swift
//  shoppingappMX
//
//  Created by jesus.martinez on 27/12/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import HandyJSON

struct AppInstalledTransport: HandyJSON {
    
    static let glomo = "glomo"
    static let bancomer = "bmovil"
    static let store = "store"

    let appName: String
    
    init() {
        appName = AppInstalledTransport.store
    }
    
    init(appName: String) {
        self.appName = appName
    }
}

extension AppInstalledTransport: Equatable {
    
    static func == (lhs: AppInstalledTransport, rhs: AppInstalledTransport) -> Bool {
        return lhs.appName == rhs.appName
    }
}
