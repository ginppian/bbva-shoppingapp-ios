//
//  CellsRequestBO.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 10/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit
import HandyJSON

class CellsRequestBO: ModelBO, HandyJSON {
    var requestTypeId: String!
    var cellsPageRedirect: String!
    var requestObject: String?

    required init() {}

}

struct RequestActivateCardObjectCells: HandyJSON {
    var cardNumber: String!
    var id: String!
}

struct RequestActivateCardByCVVObjectCells: HandyJSON {
    var value: String!
}

struct RequestCancelCardObjectCells: Model, Codable {
    let id: String
}

struct RequestGenerateCardObjectCells: HandyJSON {
    var cardId: String!
    var cardNumber: String!
    var cardType: String!
}

struct RequestBlockCardObjectCells: Model, HandyJSON {

    var id: String?
    var typeBlock: String?
}

struct ActivateCardSuccessObjectCells: HandyJSON {
    var cardNumber: String!
}

struct ActivateByCVVCardErrorObjectCells: HandyJSON {

    let status: String
    let type: String

    init() {
        self.init(status: "error", type: "NOT_VALID_CVV")
    }
    
    init(status: String, type: String) {
        self.status = status
        self.type = type
    }

}

struct OnOffCardObjectCells: Model, Codable {

    var id: String!
    var enabled: Bool!
}

struct ConfigureObjectCells: Model, HandyJSON {
    
    var itemId: String?
    var enabled: Bool?
}

struct NotificationsVibrationCells: Model, HandyJSON {
    
    var value: Bool?
}
