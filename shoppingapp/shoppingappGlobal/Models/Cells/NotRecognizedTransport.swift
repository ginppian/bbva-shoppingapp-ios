//
//  NotRecognizedTransport.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 31/7/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import HandyJSON

struct NotRecognizedTransport: HandyJSON {

    var isBlockHidden: Bool
    var isPowerOffHidden: Bool
    var isCallHidden: Bool

    init() {
        self.init(isBlockHidden: false, isPowerOffHidden: false, isCallHidden: false)
    }

    init(isBlockHidden: Bool, isPowerOffHidden: Bool, isCallHidden: Bool) {
        self.isBlockHidden = isBlockHidden
        self.isPowerOffHidden = isPowerOffHidden
        self.isCallHidden = isCallHidden
    }
}
