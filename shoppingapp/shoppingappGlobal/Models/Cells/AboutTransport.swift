//
//  AboutTransport.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 23/7/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import HandyJSON

struct AboutTransport: HandyJSON {

    var appVersion: String
    var operatingSystem: String
    var operatingSystemVersion: String
    var deviceModel: String

    init() {
        self.init(appVersion: "", operatingSystem: "", operatingSystemVersion: "", deviceModel: "")
    }

    init(appVersion: String, operatingSystem: String, operatingSystemVersion: String, deviceModel: String) {
        self.appVersion = appVersion
        self.operatingSystem = operatingSystem
        self.operatingSystemVersion = operatingSystemVersion
        self.deviceModel = deviceModel
    }

}
