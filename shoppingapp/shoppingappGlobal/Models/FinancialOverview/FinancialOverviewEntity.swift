//
//  FinancialOverviewEntity.swift
//  shoppingapp
//
//  Created by jesus.martinez on 9/7/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit
import HandyJSON

typealias FinancialOverviewRelatedContractEntityGrouppedByCardId = [CardID: [FinancialOverviewRelatedContractEntity]]

protocol FinancialOverviewEntityProtocol {

    var status: Int! { get set }
    var data: FinancialOverviewDataEntity? { get set }

    func applyContractRelation()
}

class FinancialOverviewEntity: ModelEntity, Codable, FinancialOverviewEntityProtocol {

    var status: Int! = 200
    var data: FinancialOverviewDataEntity?

    func applyContractRelation() {

        guard let financialContracts = data?.contracts else {
            return
        }

        var newContracts = [FinancialOverviewContractEntity]()

        for var oldContract in financialContracts {

            var relatedContracts = [RelatedContractEntity]()

            if let relatedContract = oldContract.relatedContracts {

                let filteredNotRepitedContracts = relatedContract.reduce([FinancialOverviewRelatedContractEntity]()) { contracts, financialOverviewRelatedContractEntity -> [FinancialOverviewRelatedContractEntity] in

                    var nextContracts = contracts

                    if !contracts.contains(where: { $0.contractId == financialOverviewRelatedContractEntity.contractId }) {
                        nextContracts.append(financialOverviewRelatedContractEntity)
                    }

                    return nextContracts
                }

                for contract in filteredNotRepitedContracts {

                    for financialContract in financialContracts where oldContract.id != financialContract.id {

                        if let contracts = financialContract.relatedContracts, let financialRelatedContract = contracts.first(where: { financialOverviewRelatedEntity -> Bool in
                            financialOverviewRelatedEntity.contractId == contract.contractId
                        }) {

                            let relatedContractId = financialRelatedContract.contractId
                            let contractId = financialContract.id
                            let number = financialContract.number

                            if let relatedContractId = relatedContractId, let contractId = contractId, let number = number {

                                let numberTypeId = financialContract.productType == FinancialOverviewContractEntity.cardProductType ? "PAN" : "IBAN"
                                let productTypeId = financialContract.productType == FinancialOverviewContractEntity.cardProductType ? "CARDS" : "ACCOUNTS"

                                let entity = RelatedContractEntity(relatedContractId: relatedContractId, contractId: contractId, number: number, numberType: ID_Type(id: numberTypeId, name: nil), product: ID_Type(id: productTypeId, name: nil), relationType: ID_Type(id: "LINKED_WITH", name: nil))

                                if let physicalSupport = oldContract.detail?.physicalSupport, physicalSupport.id == "NORMAL_PLASTIC" {

                                    relatedContracts.append(entity)

                                } else if let physicalSupport = oldContract.detail?.physicalSupport, physicalSupport.id == "STICKER" {

                                    if let physicalSupport = financialContract.detail?.physicalSupport, (physicalSupport.id != "STICKER" && physicalSupport.id != "VIRTUAL") {
                                        relatedContracts.append(entity)
                                    }

                                } else if let physicalSupport = oldContract.detail?.physicalSupport, physicalSupport.id == "VIRTUAL" || physicalSupport.id == "NFC" {

                                    if let physicalSupport = financialContract.detail?.physicalSupport, (physicalSupport.id != "STICKER" && physicalSupport.id != "VIRTUAL") {
                                        relatedContracts.append(entity)
                                    }
                                }
                            }
                        }
                    }
                }
            }

            oldContract.relatedContractsEntity = relatedContracts
            newContracts.append(oldContract)
        }

        data?.contracts = newContracts
    }
}

struct FinancialOverviewDataEntity: Codable {

    var contracts: [FinancialOverviewContractEntity]?
    var family: [FamilyEntity]?
}

struct FinancialOverviewContractEntity: Codable {

    static let accountProductType = "ACCOUNTS"
    static let cardProductType = "CARDS"
    static let loanProductType = "LOANS"
    static let depositProductType = "DEPOSITS"
    static let investmentFundProductType = "INVESTMENT_FUND"

    var productType: String?
    var id: String?
    var number: String?
    var status: ID_Type?
    var alias: String?
    var product: ID_Type?
    var subProductType: ID_Type?
    var singnedAuthorization: ID_Type?
    var relatedContracts: [FinancialOverviewRelatedContractEntity]?
    var detail: DetailEntity?
    var currencies: [CurrenciesEntity]?
    var relatedContractsEntity: [RelatedContractEntity]?
    var userAccessControl: [UserAccessControlEntity]?
}

struct FinancialOverviewRelatedContractEntity: Codable {
    var id: String?
    var contractId: String?
    var relationType: ID_Type?
}

struct DetailEntity: Codable {
    var images: [ImagesEntity]?
    var physicalSupport: ID_Type?
    var expirationDate: String?
    var activations: [IDNameActiveEntity]?
    var rewards: [FinancialOverviewRewardEntity]?
    var level: String?
    var indicators: [IDNameActiveEntity]?
    var specificAmounts: [SpecificAmountsEntity]?
}

struct FinancialOverviewRewardEntity: Codable {
    var id: String?
    var name: String?
    var nonMonetaryValue: Int?

    init(id: String?, name: String?, nonMonetaryValue: Int?) {
        self.id = id
        self.name = name
        self.nonMonetaryValue = nonMonetaryValue
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decode(String.self, forKey: .id)
        name = try values.decode(String.self, forKey: .name)

        do {
            nonMonetaryValue = try values.decode(Int.self, forKey: .nonMonetaryValue)
        } catch {
            do {
                let valueString = try values.decode(String.self, forKey: .nonMonetaryValue).lowercased()
                nonMonetaryValue = Int(valueString)
            } catch {
                nonMonetaryValue = nil
            }
        }
    }
}

struct IDNameActiveEntity: Codable {
    var id: String?
    var name: String?
    var isActive: Bool?

    init(id: String?, name: String?, isActive: Bool?) {
        self.id = id
        self.name = name
        self.isActive = isActive
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decode(String.self, forKey: .id)
        name = try values.decode(String.self, forKey: .name)

        do {
            isActive = try values.decode(Bool.self, forKey: .isActive)
        } catch {
            do {
                let valueString = try values.decode(String.self, forKey: .isActive).lowercased()
                isActive = (valueString == "true")
            } catch {
                isActive = false
            }
        }
    }
}

struct SpecificAmountsEntity: Codable {

    static let availableBalance = "AVAILABLE_BALANCE"
    static let pendingChargeBalance = "PENDING_CHARGE_BALANCE"
    static let currentBalance = "CURRENT_BALANCE"

    var id: String?
    var amounts: [AmountCurrencyEntity]?
}

struct FamilyEntity: Codable {
    var id: String?
    var name: String?
    var balances: [BalancesEntity]?
}

struct BalancesEntity: Codable {
    var id: String?
    var name: String?
    var amountBalances: [AmountCurrencyEntity]?
}
