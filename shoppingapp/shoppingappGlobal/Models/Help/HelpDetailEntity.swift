//
//  HelpDetailEntity.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 13/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import HandyJSON

class HelpDetailEntity: ModelEntity, HandyJSON {

    var helpId: String?
    var image: [ContentEntity]?
    var video: [ContentEntity]?
    var title: [ContentEntity]?
    var questions: [QuestionEntity]?

    required init() {}
}

struct ContentEntity: HandyJSON {

    var language: String?
    var content: String?

}

struct QuestionEntity: HandyJSON {

    var questionId: String?
    var title: [ContentEntity]?
    var answers: [AnswerEntity]?

}

struct AnswerEntity: HandyJSON {

    var type: String?
    var data: [ContentEntity]?
    var extraData: [ContentEntity]?

}
