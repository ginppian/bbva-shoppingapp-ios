//
//  HelpDetailBO.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 16/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class HelpDetailBO: ModelBO {

    var helpId: String?
    var image: [ContentBO]?
    var video: [ContentBO]?
    var title: [ContentBO]?
    var questions: [QuestionBO]?

    init(helpDetailEntity: HelpDetailEntity) {

        if let id = helpDetailEntity.helpId {
            helpId = id
        }

        var imageBO = [ContentBO]()

        if let imagesEntity = helpDetailEntity.image {
            for imageEntity in imagesEntity {
                imageBO.append(ContentBO(contentEntity: imageEntity))
            }
        }

        image = imageBO

        var videoBO = [ContentBO]()

        if let videosEntity = helpDetailEntity.video {
            for videoEntity in videosEntity {
                videoBO.append(ContentBO(contentEntity: videoEntity))
            }
        }

        video = videoBO

        var titleBO = [ContentBO]()

        if let titlesEntity = helpDetailEntity.title {
            for titleEntity in titlesEntity {
                titleBO.append(ContentBO(contentEntity: titleEntity))
            }
        }

        title = titleBO

        var questionsBO = [QuestionBO]()

        if let questionsEntity = helpDetailEntity.questions {
            for questionEntity in questionsEntity {
                questionsBO.append(QuestionBO(questionEntity: questionEntity))
            }
        }

        questions = questionsBO
    }

    func getImageByLanguage(forKey key: String) -> ContentBO? {

        var contentBO: ContentBO?

        if let imageList = image {
            for content in imageList where content.language == key {
                contentBO = content
            }
        }
        return contentBO
    }

    func getVideoByLanguage(forKey key: String) -> ContentBO? {

        var contentBO: ContentBO?

        if let videoList = video {
            for content in videoList where content.language == key {
                    contentBO = content
            }
        }
        return contentBO
    }

    func getTitleByLanguage(forKey key: String) -> ContentBO? {

        var contentBO: ContentBO?

        if let titleList = title {
            for content in titleList where content.language == key {
                contentBO = content
            }
        }
        return contentBO
    }

    func getImageURL(forUrl url: String) -> String {

        let imageUrlWithPlatform = url.replacingOccurrences(of: "%so%", with: Constants.PLATFORM)
        let imageUrl = imageUrlWithPlatform.replacingOccurrences(of: "%size%", with: ImagesScales.imageScale)

        return imageUrl
    }
}

class QuestionBO: ModelBO {

    var questionId: String?
    var title: [ContentBO]?
    var answers: [AnswerBO]?

    init(questionEntity: QuestionEntity) {

        if let id = questionEntity.questionId {
            questionId = id
        }

        var titleBO = [ContentBO]()

        if let titlesEntity = questionEntity.title {
            for titleEntity in titlesEntity {
                titleBO.append(ContentBO(contentEntity: titleEntity))
            }
        }

        title = titleBO

        var answersBO = [AnswerBO]()

        if let answersEntity = questionEntity.answers {
            for answerEntity in answersEntity {
                answersBO.append(AnswerBO(answerEntity: answerEntity))
            }
        }

        answers = answersBO
    }

    func getTitleByLanguage(forKey key: String) -> ContentBO? {

        var contentBO: ContentBO?

        if let titleList = title {
            for content in titleList where content.language == key {
                contentBO = content
            }
        }
        return contentBO
    }
}

enum AnswerType: String {
    case text
    case image
    case video
    case unknown
}

class ContentBO: ModelBO {

    var language: String?
    var content: String?

    init(contentEntity: ContentEntity?) {

        if let lang = contentEntity?.language {
            language = lang
        }

        if let cont = contentEntity?.content {
            content = cont
        }

    }

    func getImageURL(forUrl url: String) -> String {

        let imageUrlWithPlatform = url.replacingOccurrences(of: "%so%", with: Constants.PLATFORM)
        let imageUrl = imageUrlWithPlatform.replacingOccurrences(of: "%size%", with: ImagesScales.imageScale)

        return imageUrl
    }
}

class AnswerBO: ModelBO {

    var type: AnswerType = .unknown
    var data: [ContentBO]?
    var extraData: [ContentBO]?

    private static let TEXT = "text"
    private static let IMAGE = "image"
    private static let VIDEO = "video"

    init(answerEntity: AnswerEntity?) {

        switch answerEntity?.type {

        case AnswerBO.TEXT?:
            type = .text
        case AnswerBO.IMAGE?:
            type = .image
        case AnswerBO.VIDEO?:
            type = .video
        default:
            type = .unknown
        }

        var dataBO = [ContentBO]()

        if let datasEntity = answerEntity?.data {
            for dataEntity in datasEntity {
                dataBO.append(ContentBO(contentEntity: dataEntity))
            }
        }

        data = dataBO

        var extraDataBO = [ContentBO]()

        if let extraDatasEntity = answerEntity?.extraData {
            for extraDataEntity in extraDatasEntity {
                extraDataBO.append(ContentBO(contentEntity: extraDataEntity))
            }
        }

        extraData = extraDataBO
    }

    func getDataByLanguage(forKey key: String) -> ContentBO? {

        var contentBO: ContentBO?

        if let dataList = data {
            for content in dataList where content.language == key {
                contentBO = content
            }
        }
        return contentBO
    }

    func getExtraDataByLanguage(forKey key: String) -> ContentBO? {

        var contentBO: ContentBO?

        if let dataList = extraData {
            for content in dataList where content.language == key {
                contentBO = content
            }
        }
        return contentBO
    }
}
