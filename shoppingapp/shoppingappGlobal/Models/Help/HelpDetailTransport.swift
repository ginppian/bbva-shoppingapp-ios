//
//  HelpDetailTransport.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 17/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class HelpDetailTransport: ModelTransport {

    let helpId: String

    init(_ helpId: String) {
        self.helpId = helpId
    }

}
