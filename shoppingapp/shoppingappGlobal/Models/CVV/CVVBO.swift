//
//  CVVModelBO.swift
//  shoppingapp
//
//  Created by Daniel García on 30/01/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class CVVBO: ModelBO {

    private let cardCvvID = Settings.CardCvv.cvvId

    private var cardSecurityData: [SecurityDataBO]?

    var status: Int!
    var cardId: String?

    var cardCVV: SecurityDataBO? {
        return cardSecurityData?.first(where: { $0.id == cardCvvID })
    }

    init(cardId: String, cardCVV: SecurityDataBO) {

        self.cardId = cardId
        self.cardSecurityData = [SecurityDataBO]()
        self.cardSecurityData?.append(cardCVV)

    }

    init(cvvEntity: CVVEntity) {

        status = cvvEntity.status

        if let cardSecurityDataElements = cvvEntity.data {

            cardSecurityData = [SecurityDataBO]()

            for cardSecurityDataEntity in cardSecurityDataElements {
                cardSecurityData?.append(SecurityDataBO(entity: cardSecurityDataEntity))
            }

        }

    }

}

struct SecurityDataBO: Equatable {

    let id: String
    let name: String?
    let code: String

    init(entity: SecurityDataEntity) {
        id = entity.id
        name = entity.name
        code = entity.code
    }

    static func == (lhs: SecurityDataBO, rhs: SecurityDataBO) -> Bool {

        if lhs.code == rhs.code, lhs.id == rhs.id, lhs.name == rhs.name {
            return true
        } else {
            return false
        }

    }

}
