//
//  CVVModelEntity.swift
//  shoppingapp
//
//  Created by Daniel.Garcia on 30/01/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import HandyJSON

class CVVEntity: ModelEntity, HandyJSON {

    private let cardCvvID = Settings.CardCvv.cvvId

    var status: Int!
    var apiInfo: ApiInfoEntity?
    var data: [SecurityDataEntity]?
    var cardCVV: SecurityDataEntity? {
        get {
            return data?.first(where: { $0.id == cardCvvID })
        }
        set(newCardCvv) {
            if let newCvv = newCardCvv, let cardCvvIndex = data?.index(where: { $0.id == cardCvvID }) {
                data?.insert(newCvv, at: cardCvvIndex)
            }
        }
    }

    required init() {}
}

struct SecurityDataEntity: HandyJSON {
    var id: String!
    var name: String?
    var code: String!
}
