//
//  CardCVVTransport.swift
//  shoppingapp
//
//  Created by Daniel on 30/04/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit

class CVVTransport: ModelBO {
    var cardId: String

    init(with cardId: String) {
        self.cardId = cardId
    }
}
