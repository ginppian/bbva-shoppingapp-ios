//
//  PaginationBO.swift
//  shoppingapp
//
//  Created by jesus.martinez on 7/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation

class PaginationBO: ModelBO {

    let links: LinksBO?
    let page: Int?
    let totalPages: Int?
    let totalElements: Int?
    let pageSize: Int?

    init?(paginationEntity: PaginationEntity?) {

        guard let pagination = paginationEntity else {
            return nil
        }

        links = LinksBO(linksEntity: pagination.links)
        page = pagination.page
        totalPages = pagination.totalPages
        totalElements = pagination.totalElements
        pageSize = pagination.pageSize

    }

}

class LinksBO: ModelBO {

    let first: String?
    let last: String?
    let previous: String?
    var next: String?

    init?(linksEntity: LinksEntity?) {

        guard let links = linksEntity else {
            return nil
        }

        first = links.first
        last = links.last
        previous = links.previous
        next = links.next

    }

    func generateNextURL(isFinancial: Bool) -> String {

        let serverHost = SessionDataManager.sessionDataInstance().serverHost!
        let serverHostURL = URL(string: serverHost)

        let selectedEnvironment = Configuration.selectedEnvironment()
        let directory = isFinancial ? ServerHostURL.defaultDirectoryFinancial() : ServerHostURL.defaultDirectory()

        guard let next = next, !next.isEmpty else {
            return serverHost
        }

        var nextURL = URL(string: next)

        if let url = nextURL {

            var urlComponents = URLComponents()
            urlComponents.scheme = serverHostURL?.scheme
            urlComponents.host = serverHostURL?.host
            urlComponents.port = serverHostURL?.port

            let backslashCharacter: Character = "/"

            if !url.path.isEmpty {

                let backslash = ((url.path.first != backslashCharacter) ? String(backslashCharacter) : "")

                urlComponents.path = backslash + url.path

                if selectedEnvironment != "mock" {

                    if !url.path.contains("\(directory)") {
                        urlComponents.path = String(backslashCharacter) + directory + backslash + url.path
                    }
                }
            }

            if let query = url.query, !query.isEmpty {
                urlComponents.percentEncodedQuery = query
            }

            if let urlComponentsURL = urlComponents.url {
                nextURL = urlComponentsURL
            } else {
                nextURL = URL(string: serverHost + next)
            }

        } else {
            nextURL = URL(string: serverHost + next)
        }

        return nextURL!.absoluteString
    }

}
