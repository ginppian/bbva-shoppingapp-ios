//
//  TransactionsModelEntity.swift
//  shoppingapp
//
//  Created by jesus.martinez on 5/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation
import HandyJSON

// Transactions
class TransactionsEntity: ModelEntity, HandyJSON {
    var status: Int!
    var data: [TransactionEntity]?
    var pagination: PaginationEntity?

    required init() {}
}

// Transaction
struct TransactionEntity: HandyJSON {
    var id: String!
    var localAmount: LocalAmountEntity!
    var originAmount: LocalAmountEntity?
    var moneyFlow: IdNameEntity?
    var concept: String?
    var transactionType: TransactionTypeEntity!
    var operationDate: String!
    var accountedDate: String!
    var valuationDate: String!
    var financingType: IdNameEntity!
    var status: IdNameEntity!
    var contract: ContractEntity!
    var tags: [String]?
    var category: IdNameEntity!
    var additionalInformation: AdditionalInformationEntity?
}

struct LocalAmountEntity: HandyJSON {
    var amount: NSNumber!
    var currency: String!
}

struct IdNameEntity: HandyJSON {
    var id: String?
    var name: String?
}

struct TransactionTypeEntity: HandyJSON {
    var id: String?
    var name: String?
    var internalCode: IdNameEntity?
}

struct ContractEntity: HandyJSON {
    var id: String!
    var number: String!
    var numberType: IdNameEntity!
    var product: IdNameEntity?
    var alias: String?
}

struct AdditionalInformationEntity: HandyJSON {
    var reference: String?
    var additionalData: String?
}
