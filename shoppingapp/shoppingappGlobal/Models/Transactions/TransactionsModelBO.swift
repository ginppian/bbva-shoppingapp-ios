//
//  TransactionsModelBO.swift
//  shoppingapp
//
//  Created by jesus.martinez on 5/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation

//Transactions Model
class TransactionsBO: ModelBO {

    let status: Int
    let transactions: [TransactionBO]
    var pagination: PaginationBO?

    init(transactionsEntity: TransactionsEntity) {

        self.status = transactionsEntity.status

        var transactions = [TransactionBO]()

        if let transactionEntities = transactionsEntity.data {

            for transactionEntity in transactionEntities {

                if let transaction = TransactionBO(transactionEntity: transactionEntity) {
                    transactions.append(transaction)
                }
            }

        }

        self.transactions = transactions

        self.pagination = PaginationBO(paginationEntity: transactionsEntity.pagination)

    }

}

class TransactionBO: ModelBO {

    private static let FINANCING_AVAILABLE = "FINANCING_AVAILABLE"
    private static let FINANCED_AMOUNT = "FINANCED_AMOUNT"

    private static let SETTLED = "SETTLED"
    private static let PENDING = "PENDING"

    private static let PURCHASE = "PURCHASE"
    private static let REFUND = "REFUND"
    private static let CASH_WITHDRAWAL = "CASH_WITHDRAWAL"
    private static let CASH_INCOME = "CASH_INCOME"
    private static let UNCATEGORIZED = "UNCATEGORIZED"

    let id: String
    let operationDate: Date
    let concept: String
    let financingType: FinancingType
    let status: TransactionStatus
    let transactionType: TransactionType
    let localAmount: LocalAmountBO
    let accountedDate: Date?
    var contract: ContractEntity
    let additionalInformation: AdditionalInformationEntity?

    init?(transactionEntity: TransactionEntity) {
        
        guard let operationDateFromEntity = transactionEntity.operationDate, let operationDateParsed = TransactionBO.parse(date: operationDateFromEntity) else {
            return nil
        }

        id = transactionEntity.id
        operationDate = operationDateParsed
        concept = transactionEntity.concept ?? ""
        financingType = TransactionBO.financingType(fromServerType: transactionEntity.financingType.id)
        status = TransactionBO.status(fromServerType: transactionEntity.status.id)
        transactionType = TransactionBO.transactionType(fromServerType: transactionEntity.transactionType.id)
        localAmount = LocalAmountBO(withLocalAmountEntity: transactionEntity.localAmount)
        accountedDate = TransactionBO.parse(date: transactionEntity.accountedDate)
        contract = transactionEntity.contract
        additionalInformation = transactionEntity.additionalInformation
    }
    
    private static func parse(date: String?) -> Date? {
        
        guard let stringDate = date else {
            return nil
        }
        
        var operationDate = Date.date(fromServerString: stringDate, withFormat: Date.DATE_FORMAT_LONG_SERVER)
        
        if operationDate == nil {
            
            operationDate = Date.date(fromServerString: stringDate, withFormat: Date.DATE_FORMAT_LONG_SERVER_ALT)
        }
        
        if operationDate == nil {
            
            operationDate = Date.date(fromServerString: stringDate, withFormat: Date.DATE_FORMAT_SERVER)
        }
        
        return operationDate
    }

    private static func financingType(fromServerType serverType: String?) -> FinancingType {

        switch serverType?.uppercased() {
        case FINANCING_AVAILABLE:
            return .financiable
        case FINANCED_AMOUNT:
            return .financed
        default:
            return .none
        }
    }

    private static func status(fromServerType serverType: String?) -> TransactionStatus {

        switch serverType?.uppercased() {
        case SETTLED:
            return .settled
        case PENDING:
            return .pending
        default:
            return .settled
        }
    }

    private static func transactionType(fromServerType serverType: String?) -> TransactionType {

        switch serverType?.uppercased() {
        case PURCHASE:
            return .purchase
        case REFUND:
            return .refund
        case CASH_WITHDRAWAL:
            return .cashWithdrawal
        case CASH_INCOME:
            return .cashIncome
        case UNCATEGORIZED:
            return .uncategorized
        default:
            return .uncategorized
        }
    }

}

class LocalAmountBO: ModelBO {

    var amount: NSDecimalNumber
    var currency: String

    init(withLocalAmountEntity localAmountEntity: LocalAmountEntity) {

        self.amount = NSDecimalNumber(decimal: localAmountEntity.amount.decimalValue)
        self.currency = localAmountEntity.currency

    }

}

enum FinancingType {
    case financiable
    case financed
    case none
}

enum TransactionStatus {
    case pending
    case settled
}

enum TransactionType {
    case purchase
    case refund
    case cashWithdrawal
    case cashIncome
    case uncategorized
}
