//
//  TransactionsTypeFilterEntity.swift
//  shoppingapp
//
//  Created by jesus.martinez on 4/8/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation

class TransactionsTypeFilterEntity: ModelEntity {

    let type: String
    let titleKey: String

    init(type: String, titleKey: String) {
        self.type = type
        self.titleKey = titleKey
    }

}
