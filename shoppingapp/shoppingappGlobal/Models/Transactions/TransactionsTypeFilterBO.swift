//
//  TransactionsTypeFilterBO.swift
//  shoppingapp
//
//  Created by jesus.martinez on 4/8/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation

enum TransactionFilterType {
    case all
    case income
    case expense
}

class TransactionsTypeFilterBO: ModelBO {

    private static let ALL = "ALL"
    private static let INCOMES = "INCOMES"
    private static let EXPENSES = "EXPENSES"

    let type: TransactionFilterType
    let titleKey: String

    init(type: String, titleKey: String) {
        self.type = TransactionsTypeFilterBO.transactionFilterType(fromFilterType: type)
        self.titleKey = titleKey
    }

    private static func transactionFilterType(fromFilterType filterType: String) -> TransactionFilterType {

        switch filterType.uppercased() {
        case ALL:
            return .all
        case INCOMES:
            return .income
        case EXPENSES:
            return .expense
        default:
            return .all
        }

    }

}
