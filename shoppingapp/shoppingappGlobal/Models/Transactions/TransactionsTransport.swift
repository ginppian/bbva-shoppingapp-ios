//
//  TransactionFilterTransport.swift
//  shoppingapp
//
//  Created by ruben.fernandez on 18/8/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation

class TransactionsTransport: ModelTransport { // Transport data structure (not Entity)

    var transactionDisplayItem: TransactionDisplayItem?
    var cardBO: CardBO?
}
