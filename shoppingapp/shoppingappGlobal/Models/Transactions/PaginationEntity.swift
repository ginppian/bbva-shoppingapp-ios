//
//  PaginationEntity.swift
//  shoppingapp
//
//  Created by jesus.martinez on 7/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation
import HandyJSON

struct PaginationEntity: HandyJSON {
    var links: LinksEntity?
    var page: Int?
    var totalPages: Int?
    var totalElements: Int?
    var pageSize: Int?
}

struct LinksEntity: HandyJSON {
    var first: String?
    var last: String?
    var previous: String?
    var next: String?
}
