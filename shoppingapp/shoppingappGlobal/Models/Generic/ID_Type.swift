//
//  ID_Type.swift
//  shoppingapp
//
//  Created by jesus.martinez on 9/7/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import HandyJSON

struct ID_Type: HandyJSON, Codable {
    var id: String!
    var name: String?
}
