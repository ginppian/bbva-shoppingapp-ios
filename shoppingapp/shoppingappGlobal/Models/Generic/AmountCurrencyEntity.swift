//
//  AmountCurrencyEntity.swift
//  shoppingapp
//
//  Created by jesus.martinez on 9/7/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import HandyJSON

struct AmountCurrencyEntity: HandyJSON, Codable {
    var amount: Double!
    var currency: String!
}
