//
//  Generic.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 11/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation
import HandyJSON

enum TypeError {
    case AuthenticationFailure
    case AuthenticationInfo

}

enum ServiceError: Error {
    case GenericErrorEntity(error: ErrorEntity)
    case GenericErrorBO(error: ErrorBO)
    case CouldNotDecodeJSON
    case BadStatus(status: Int)
    case Other(NSError)
}

protocol Model {
}

typealias ModelBO = Model
typealias ModelEntity = Model
typealias ModelDisplay = Model
typealias ModelTransport = Model

class EmptyModel: Model {}
class EmptyModelBO: ModelBO {}
class EmptyModelEntity: ModelEntity {}

struct ImagesEntity: HandyJSON, Codable {
    var id: String?
    var name: String?
    var url: String?
}

class ImagesBO: ModelBO {

    var id: String
    var name: String?
    var url: String

    init?(imageEntity: ImagesEntity?) {

        guard let imageURL = imageEntity?.url, let imageId = imageEntity?.id else {
            return nil
        }

        id = imageId
        name = imageEntity?.name
        url = imageURL
    }
}
