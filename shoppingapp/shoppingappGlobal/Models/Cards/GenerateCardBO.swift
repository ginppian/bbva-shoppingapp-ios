//
//  GenerateCardBO.swift
//  shoppingappGlobal
//
//  Created by Javier Dominguez on 30/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import HandyJSON

final class GenerateCardBO: ModelBO, HandyJSON {
    var cardId: String!
    var cardNumber: String!
    var cardHolder: String!
    var cardType: String!
    var imageList: [GenerateCardImageListBO]?
}

final class GenerateCardImageListBO: ModelBO, HandyJSON {
    var url: String!

}

final class GeneratedCardBO: ModelBO, Codable {

    let folio: String

    init(generatedCardDTO: GeneratedCardDTO) {
        folio = generatedCardDTO.data?.operationNumber ?? ""
    }
}
