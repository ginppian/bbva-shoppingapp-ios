//
//  CardsTransport.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 29/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class CardsTransport: ModelBO {

    var cardBO: CardBO?
    var flagNavigation: Bool?

    init(cardBO: CardBO, flag: Bool) {
        self.cardBO = cardBO
        self.flagNavigation = flag
    }

    convenience init(cardBO: CardBO) {
        self.init(cardBO: cardBO, flag: false)
    }

}
