//
//  CardsEntity.swift
//  shoppingapp
//
//  Created by ruben.fernandez on 12/9/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit
import HandyJSON

class CardsEntity: ModelEntity, HandyJSON {
    var status: Int!
    var data: [CardEntity]?

    required init() {}

}
class ActivationsEntity: ModelEntity, HandyJSON {
    var status: Int!
    var data: ActivationEntity?

    required init() {}

}

struct CardEntity: HandyJSON {

    var cardId: String!
    var number: String!
    var numberType: ID_Type!
    var cardType: ID_Type!
    var title: ID_Type!
    var alias: String!
    var brandAssociation: ID_Type?
    var physicalSupport: ID_Type?
    var expirationDate: String!
    var holderName: String!
    var currencies: [CurrenciesEntity]?
    var grantedCredits: [AmountCurrencyEntity]?
    var availableBalance: AvailableBalanceEntity!
    var disposedBalance: AvailableBalanceEntity!
    var status: ID_Type!
    var images: [ImagesEntity]?
    var companyEmbossingNameType: ID_Type?
    var indicators: [IndicatorEntity]?
    var activations: [ActivationEntity]?
    var relatedContracts: [RelatedContractEntity]?
    var rewards: [RewardEntity]?

}

struct CurrenciesEntity: HandyJSON, Codable {
    var currency: String!
    var isMajor: Bool!
}

struct AvailableBalanceEntity: HandyJSON {
    var currentBalances: [AmountCurrencyEntity]?
    var postedBalances: [AmountCurrencyEntity]?
    var pendingBalances: [AmountCurrencyEntity]?
}

struct IndicatorEntity: HandyJSON {
    var indicatorId: String!
    var name: String!
    var isActive: Bool!
}

struct ActivationEntity: HandyJSON {
    var activationId: String!
    var name: String!
    var isActive: Bool!
    var isActivationEnabledForUser: Bool!
    var startDate: String!
    var endDate: String!
    var limits: [ActivationLimitEntity]?
}

struct ActivationLimitEntity: HandyJSON {
    var id: String!
    var name: String!
    var amountLimit: AmountCurrencyEntity!
}

struct RelatedContractEntity: HandyJSON, Codable {
    var relatedContractId: String!
    var contractId: String!
    var number: String?
    var numberType: ID_Type?
    var product: ID_Type?
    var relationType: ID_Type?
}

struct RewardEntity: HandyJSON {
    var rewardId: String?
    var name: String?
    var nonMonetaryValue: Int?
    var unitType: ID_Type?
    var nonMonetaryValueCloseToExpire: NSNumber?
    var nonMonetaryValueExpirationDate: String?
}

struct EtagEntity {
    var data: Data?
    var etag: String?

    init(withData dataImage: Data, withEtag etagImage: String) {
        data = dataImage
        etag = etagImage
    }
}

struct UserAccessControlEntity: HandyJSON, Codable {
    var canOperate: Bool!
}
