//
//  BlockCardTransport.swift
//  shoppingapp
//
//  Created by Marcos on 30/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import HandyJSON

struct BlockCardTransport: HandyJSON {

    var id: String
    var cardNumber: String
    var alias: String
    var imageList: [CardImage]?

    init() {
        self.init(id: "", cardNumber: "", alias: "")
    }

    init(id: String, cardNumber: String, alias: String) {
        self.id = id
        self.cardNumber = cardNumber
        self.alias = alias
    }

    init(id: String, cardNumber: String, alias: String, imageList: [CardImage]) {
        self.init(id: id, cardNumber: cardNumber, alias: alias)
        self.imageList = imageList
    }

}

struct CardImage: HandyJSON {

    var url: String

    init() {
        self.init(url: "" )
    }

    init(url: String) {
        self.url = url
    }

}

struct BlockCardSuccess: HandyJSON {

    var cardNumber: String
    var date: String
    var folio: String

    init() {
        self.init(cardNumber: "", date: "", folio: "")
    }

    init(cardNumber: String, date: String, folio: String) {
        self.cardNumber = cardNumber
        self.date = date
        self.folio = folio
    }

}
