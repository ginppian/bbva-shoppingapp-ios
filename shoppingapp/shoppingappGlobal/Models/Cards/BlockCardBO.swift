//
//  BlockCardBO.swift
//  shoppingappMX
//
//  Created by ignacio.bonafonte on 21/01/2019.
//  Copyright © 2019 BBVA. All rights reserved.
//

import Foundation

public class BlockCardBO: ModelBO {

    var reference: String
    var blockDate: String

    init( blockCardDTO: BlockCardDTO?) {
        reference = blockCardDTO?.data?.reference ?? ""
        blockDate = blockCardDTO?.data?.blockDate ?? ""
    }

}
