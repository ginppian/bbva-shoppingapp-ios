//
//  OnOffResponseBO.swift
//  shoppingapp
//
//  Created by ignacio.bonafonte on 18/01/2019.
//  Copyright © 2019 BBVA. All rights reserved.
//

import Foundation

open class OnOffResponseBO: ModelBO {

    var isActive: Bool

    init(onOffResponseDTO: OnOffResponseDTO) {
        isActive = onOffResponseDTO.data?.isActive ?? false
    }

}
