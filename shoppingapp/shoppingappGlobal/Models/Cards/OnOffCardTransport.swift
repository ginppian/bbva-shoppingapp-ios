//
//  OnOffCardTransport.swift
//  shoppingapp
//
//  Created by AZIZEBULBUL on 03/08/2018.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import HandyJSON

struct OnOffCardTransport: HandyJSON {

    var id: String
    var cardNumber: String
    var alias: String
    var imageList: [OnOffCardImage]?
    var enabled: Bool

    init() {
        self.init(id: "", cardNumber: "", alias: "", enabled: false)
    }

    init(id: String, cardNumber: String, alias: String, enabled: Bool) {
        self.id = id
        self.cardNumber = cardNumber
        self.alias = alias
        self.enabled = enabled
    }

    init(id: String, cardNumber: String, alias: String, enabled: Bool, imageList: [OnOffCardImage]) {
        self.init(id: id, cardNumber: cardNumber, alias: alias, enabled: enabled)
        self.imageList = imageList
    }

}

struct OnOffCardImage: HandyJSON {

    var url: String

    init() {
        self.init(url: "" )
    }

    init(url: String) {
        self.url = url
    }

}

//struct BlockCardSuccess: HandyJSON {
//    
//    var cardNumber : String
//    var date: String
//    var folio : String
//    
//    init() {
//        self.init(cardNumber: "", date: "", folio: "")
//    }
//    
//    init(cardNumber : String, date: String, folio : String) {
//        self.cardNumber = cardNumber
//        self.date = date
//        self.folio = folio
//    }
//    
//}
