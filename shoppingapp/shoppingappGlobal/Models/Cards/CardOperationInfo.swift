//
//  CardOperationInfo.swift
//  shoppingapp
//
//  Created by Marcos on 10/10/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

enum CardOperation {

    case block
    case cancel
    case generate
    case activate
}

struct CardOperationInfo {

    let cardID: String
    let fisicalCardID: String?
    let cardType: CardDataType?
    let cardOperation: CardOperation
}

enum CardDataType {

    case physical
    case digital
    case sticker

    static func selectCardDataType(forCardBO cardBO: CardBO) -> CardDataType? {
        if cardBO.isNormalPlastic() {
            return CardDataType.physical
        } else if cardBO.isVirtual() {
            return CardDataType.digital
        } else if cardBO.isSticker() {
            return CardDataType.sticker
        }
        return nil
    }
}
