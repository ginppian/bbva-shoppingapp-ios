//
//  ActivateCardBO.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 8/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import HandyJSON

final class ActivateCardCVVTransport: CellsDTO, HandyJSON {

    var id: String!
    var alias: String!
    var cardNumber: String!
    var cardImageUrl: String!
}

final class ActivateCardTransport: CellsDTO, HandyJSON {
    var id: String!
    var cardNumber: String!
    var cardAlias: String!
    var imageList: [ActivateCardImageListTransport]?

}

final class ActivateCardImageListTransport: CellsDTO, HandyJSON {
    var url: String!

}
