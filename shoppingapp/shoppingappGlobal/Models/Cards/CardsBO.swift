//
//  CardsBO.swift
//  shoppingapp
//
//  Created by ruben.fernandez on 12/9/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit

class CardsBO: ModelBO {

    // MARK: - Properties

    let status: Int
    var cards = [CardBO]()
    let timestamp = Date()

    var userBO: UserBO?

    // MARK: - Inits

    init(cardsEntity: CardsEntity) {
        status = cardsEntity.status
        cards = cardsEntity.data?.map { CardBO(cardEntity: $0) } ?? [CardBO]()
        CardsImagesManager.sharedInstance.setupImageTasks(withCards: cards)
    }

    init(financialOverviewEntity: FinancialOverviewEntityProtocol, user: UserBO) {
        
        userBO = user
        status = financialOverviewEntity.status
        financialOverviewEntity.applyContractRelation()
        
        guard let contractEntities = financialOverviewEntity.data?.contracts else {
            return
        }
        
        let accountContracts: [(account: String?, number: String?)]? = contractEntities.filter { $0.productType == FinancialOverviewContractEntity.accountProductType }.map { ($0.id, $0.number) }
        
        let cardContractsEntities = contractEntities.filter { $0.productType == FinancialOverviewContractEntity.cardProductType }
        
        cards = cardContractsEntities.compactMap { cardEntity -> CardBO? in
            guard let card = CardBO(contractEntity: cardEntity, user: user) else {
                return nil
            }
            let accountData = accountContracts?.first { $0.number == cardEntity.relatedContracts?.first?.contractId }
            
            if card.isCreditCard(), !card.isNormalPlastic() {
                let relatedPhysicalCC = cardContractsEntities.first { $0.id == card.relatedContracts.first?.contractId }
                let bin = String(relatedPhysicalCC?.number?.prefix(6) ?? "")
                card.imageFront?.append("&type=\(bin)")
            }
            
            card.accountID = accountData?.account
            return card
        }
        CardsImagesManager.sharedInstance.setupImageTasks(withCards: cards)
    }
    // MARK: - Public methods

    func titleIds() -> [String]? {
        guard !cards.isEmpty else {
            return nil
        }

        let titles = cards.compactMap { $0.title?.id }.uniques()

        return titles.isEmpty ? nil : titles
    }
    
    func firstNfcCard() -> CardBO? {
        
        if let cardNfc = cards.first(where: { $0.physicalSupport?.id == .nfc }) {
            
            return cardNfc
        }
        
        return nil
    }
}

typealias CardID = String

// MARK: - CardBO

class CardBO: ModelBO {

    // MARK: - Properties
    var accountID: String?
    var cardId: CardID
    var number: String
    var cardType: CardTypeBO
    var title: ID_TypeBO?
    var alias: String?
    var physicalSupport: PysicalSupportBO?
    var expirationDate: Date?
    var holderName: String
    var currencies: [CurrencyCardBO]
    var grantedCredits: [AmountCurrencyBO]
    var availableBalance: AvailableBalanceBO
    var disposedBalance: AvailableBalanceBO
    var status: StatusBO
    var images: [ImagesBO]
    var imageFront: String?
    var imageBack: String?
    var indicators: [IndicatorBO]
    var activations: [ActivationBO]
    var relatedContracts: [RelatedContractBO]
    var imageSaved: UIImage?
    var currencyMajor: String?
    var transactions: TransactionsBO?
    var rewards: [RewardBO]

    // MARK: - Inits

    init(cardEntity: CardEntity) {

        cardId = cardEntity.cardId
        number = cardEntity.number.trimmingCharacters(in: .whitespaces)
        cardType = CardTypeBO(id_type: cardEntity.cardType)

        title = ID_TypeBO(id_type: cardEntity.title)
        alias = cardEntity.alias
        physicalSupport = PysicalSupportBO(id_type: cardEntity.physicalSupport)
        expirationDate = Date.date(fromLocalTimeZoneString: cardEntity.expirationDate, withFormat: Date.DATE_FORMAT_SERVER)
        holderName = cardEntity.holderName

        var currencies = [CurrencyCardBO]()

        if let currenciesEntity = cardEntity.currencies {
            for currencyEntity in currenciesEntity {
                if let currencyCard = CurrencyCardBO(currencyCard: currencyEntity) {
                    currencies.append(currencyCard)
                }
            }
        }

        self.currencies = currencies

        if !currencies.isEmpty {
            for currencyBO in currencies where currencyBO.isMajor {
                    currencyMajor = currencyBO.currency
            }
        }

        var grantedCredits = [AmountCurrencyBO]()
        if let amountCurrenciesEntity = cardEntity.grantedCredits {
            for amountCurrencyEntity in amountCurrenciesEntity {
                if let amountCurrency = AmountCurrencyBO(amountCurrency: amountCurrencyEntity) {
                    grantedCredits.append(amountCurrency)
                }
            }
        }

        self.grantedCredits = grantedCredits

        availableBalance = AvailableBalanceBO(availableBalanceEntity: cardEntity.availableBalance)
        disposedBalance = AvailableBalanceBO(availableBalanceEntity: cardEntity.disposedBalance)

        status = StatusBO(id_type: cardEntity.status)

        self.images = [ImagesBO]()

        if let imagesEntity = cardEntity.images {

            for imageEntity in imagesEntity {

                if let imageBO = ImagesBO(imageEntity: imageEntity) {
                    if imageBO.id == "FRONT_CARD" {
                        imageFront = imageBO.url
                        imageBack = imageBO.url + "&back=true"
                        let trimmedString = number.replacingOccurrences(of: " ", with: "")

                        let last5 = trimmedString.suffix(5)
                        let first6 = trimmedString.prefix(6)

                        let stringKey = "imageUrl" + "-" + last5 + first6

                        PreferencesManager.sharedInstance().saveValue(forValue: imageFront as AnyObject, withKey: stringKey)

                    } else if imageBO.id == "BACK_CARD" {
                        imageBack = imageBO.url
                    }

                    images.append(imageBO)
                }
            }
        }

        if let imageFrontString = imageFront, let dataImage = PreferencesManager.sharedInstance().getValue(forKey: imageFrontString) as? Data {

            imageSaved = UIImage(data: dataImage)
        }

        var indicators = [IndicatorBO]()

        if let indicatorsEntity = cardEntity.indicators {
            for indicatorEntity in indicatorsEntity {
                indicators.append(IndicatorBO(indicatorEntity: indicatorEntity))
            }
        }

        self.indicators = indicators

        var relatedContracts = [RelatedContractBO]()

        if let relatedContractsEntity = cardEntity.relatedContracts {
            for relatedContractEntity in relatedContractsEntity {
                relatedContracts.append(RelatedContractBO(relatedContractEntity: relatedContractEntity))
            }
        }

        self.relatedContracts = relatedContracts

        var activations = [ActivationBO]()

        if let activationsEntity = cardEntity.activations {
            for activation in activationsEntity {
                activations.append(ActivationBO(activationEntity: activation))
            }
        }

        self.activations = activations

        var rewards = [RewardBO]()

        if let rewardsEntity = cardEntity.rewards {
            for rewardEntity in rewardsEntity {
                rewards.append(RewardBO(rewardEntity: rewardEntity))
            }
        }

        self.rewards = rewards
    }

    init?(contractEntity: FinancialOverviewContractEntity, user: UserBO) {
        guard contractEntity.productType == FinancialOverviewContractEntity.cardProductType, let contractId = contractEntity.id, let contractNumber = contractEntity.number else {
            return nil
        }

        cardId = contractId
        alias = contractEntity.alias
        title = ID_TypeBO(id_type: contractEntity.product)
        number = contractNumber
        status = StatusBO(id_type: contractEntity.status)
        cardType = CardTypeBO(id_type: contractEntity.subProductType)
        holderName = user.completeUserName()
        currencies = [CurrencyCardBO]()

        if let currenciesEntity = contractEntity.currencies {
            for currencyEntity in currenciesEntity {
                if let currencyCard = CurrencyCardBO(currencyCard: currencyEntity) {
                    currencies.append(currencyCard)

                    if currencyCard.isMajor {
                        currencyMajor = currencyCard.currency
                    }
                }
            }
        }

        images = [ImagesBO]()
        rewards = [RewardBO]()
        indicators = [IndicatorBO]()
        activations = [ActivationBO]()

        if let contractDetail = contractEntity.detail {

            if let contractExpirationDetail = contractDetail.expirationDate {
                expirationDate = Date.date(fromServerString: contractExpirationDetail, withFormat: Date.DATE_FORMAT_SERVER)
            }

            if let activationsEntity = contractDetail.activations {
                for activationEntity in activationsEntity {
                    activations.append(ActivationBO(entity: activationEntity))
                }
            }

            if let indicatorsEntity = contractDetail.indicators {
                for indicatorEntity in indicatorsEntity {
                    indicators.append(IndicatorBO(entity: indicatorEntity))
                }
            }
            
            // Custom indicators imposed by PSS2-2028
            if let canOperate = contractEntity.userAccessControl?.first?.canOperate {
                
                let indicatorId = IndicatorsType.settableToOperative
                
                let hasIndicator = indicators.contains { indicator -> Bool in
                    indicator.indicatorId == indicatorId
                }
                
                let settableToOperativeIndicator = IndicatorBO(indicatorId: indicatorId, name: nil, isActive: canOperate)
                
                if hasIndicator {
                    indicators = indicators.map { $0.indicatorId == indicatorId ? settableToOperativeIndicator : $0 }
                } else {
                    indicators.append(settableToOperativeIndicator)
                }
            }
            
            let customizableActivationsIndicator = IndicatorBO(indicatorId: .customizableActivations, name: nil, isActive: true)
            indicators.append(customizableActivationsIndicator)
            // End custom indicators

            status = StatusBO(id_type: contractEntity.status)

            self.images = [ImagesBO]()

            if let imagesEntity = contractDetail.images {

                for imageEntity in imagesEntity {

                    if let imageBO = ImagesBO(imageEntity: imageEntity) {
                        if imageBO.id == "FRONT_CARD" {
                            imageFront = imageBO.url
                            imageBack = imageBO.url + "&back=true"

                            let trimmedString = number.replacingOccurrences(of: " ", with: "")

                            let last5 = trimmedString.suffix(5)
                            let first6 = trimmedString.prefix(6)

                            let stringKey = "imageUrl" + "-" + last5 + first6

                            PreferencesManager.sharedInstance().saveValue(forValue: imageFront as AnyObject, withKey: stringKey)

                        } else if imageBO.id == "BACK_CARD" {
                            imageBack = imageBO.url
                        }

                        images.append(imageBO)
                    }
                }
            }

            if let imageFrontString = imageFront, let dataImage = PreferencesManager.sharedInstance().getValue(forKey: imageFrontString) as? Data {

                imageSaved = UIImage(data: dataImage)
            }

            rewards = [RewardBO]()

            if let rewardsEntity = contractDetail.rewards {
                for rewardEntity in rewardsEntity {
                    rewards.append(RewardBO(entity: rewardEntity))
                }
            }

            if let physicalSupportEntity = contractDetail.physicalSupport {
                physicalSupport = PysicalSupportBO(id_type: physicalSupportEntity)
            }
        }

        grantedCredits = [AmountCurrencyBO]()
        
        let contractSpecificAmount = contractEntity.detail?.specificAmounts ?? [SpecificAmountsEntity]()

        if let currentBalances = contractSpecificAmount.first(where: { $0.id == SpecificAmountsEntity.currentBalance })?.amounts {
            grantedCredits = currentBalances.compactMap({ AmountCurrencyBO(amountCurrency: $0) })
        }
        
        availableBalance = AvailableBalanceBO(specificAmountsEntity: contractSpecificAmount.first { $0.id == SpecificAmountsEntity.availableBalance })
        
        disposedBalance = AvailableBalanceBO(specificAmountsEntity: contractSpecificAmount.first { $0.id == SpecificAmountsEntity.pendingChargeBalance })
        
        relatedContracts = [RelatedContractBO]()
        
        if let relatedContractsEntity = contractEntity.relatedContractsEntity {
            for relatedContractEntity in relatedContractsEntity {
                relatedContracts.append(RelatedContractBO(relatedContractEntity: relatedContractEntity))
            }
        }
    }

    // MARK: - Public methods

    func getActivator(withKeyActivation key: ActivationType) -> Bool {

        let results = activations.filter { $0.activationId == key }

        if !results.isEmpty {
            let activation: ActivationBO = results[0]
            return activation.isActive
        }

        return false
    }

    func activatorExist(withKeyActivation key: ActivationType) -> Bool {

        let results = activations.filter { $0.activationId == key }

        if !results.isEmpty {
            return true
        }

        return false
    }

    func isPrepaid() -> Bool {
        return cardType.id == .prepaid_card
    }

    func isCreditCard() -> Bool {
        return cardType.id == .credit_card
    }

    func isDebitCard() -> Bool {
        return cardType.id == .debit_card
    }

    func isInoperative() -> Bool {
        return status.id == .inoperative
    }
    
    func isCardOperative() -> Bool {
        return status.id == .operative
    }

    func isBlocked() -> Bool {
        return status.id == .blocked
    }
    
    func isCanceled() -> Bool {
        return status.id == .canceled
    }
    
    func isPendingEmbossing() -> Bool {
        return status.id == .pendingEmbossing
    }
    
    func isPendingDelivery() -> Bool {
        return status.id == .pendingDelivery
    }
    
    func isUnknown() -> Bool {
        return status.id == .unknown
    }

    func isCardOn() -> Bool {

        for activation in activations where activation.activationId == .onoff {
            return activation.isActive
        }
        return false
    }
    
    func isCardActivationsOnOff() -> Bool {
        
        return activations.contains { activation -> Bool in
            activation.activationId == .onoff
        }
    }

    func isPartialOff() -> Bool {

        guard !activations.isEmpty else {
            return false
        }

        if isCardOperative() {
            for activation in activations {
                if activation.activationId == .ecommerceActivation && activation.isActive == false {
                    return true
                }
                if activation.activationId == .cashwithdrawalActivation && activation.isActive == false {
                    return true
                }
                if activation.activationId == .purchasesActivation && activation.isActive == false {
                    return true
                }
                if activation.activationId == .foreignCashWithDrawalActivation && activation.isActive == false {
                    return true
                }
                if activation.activationId == .foreignPurchasesActivation && activation.isActive == false {
                    return true
                }
            }
        }

        return false
    }

    func isNormalPlastic() -> Bool {
        return physicalSupport?.id == .normalPlastic
    }

    func isSticker() -> Bool {
        return physicalSupport?.id == .sticker
    }

    func isVirtual() -> Bool {
        return physicalSupport?.id == .virtual || physicalSupport?.id == .nfc
    }

    func isAssociableToVirtualCards() -> Bool {

        return valueOfIndicator(.associableToVirtualCards)
    }

    func associatedCard(fronCardList cardList: [CardBO], type: PhysicalSupportType) -> CardBO? {

        for relatedContract in relatedContracts where relatedContract.relationType?.id == .linkedWith {
            let associated = cardList.first(where: { $0.cardId == relatedContract.contractId && $0.physicalSupport?.id == type })
            if let associated = associated {
                return associated
            }
        }

        return nil
    }

    func associatedVirtualCardBO(fromCards cards: [CardBO]) -> CardBO? {

        guard !cards.isEmpty else {
            return nil
        }

        for relatedContract in relatedContracts where relatedContract.relationType?.id == .linkedWith {

            let cardMatch = cards.filter { card -> Bool in

                return card.cardId == relatedContract.contractId && card.isVirtual()

            }

            if !cardMatch.isEmpty {
                return cardMatch[0]
            }
        }

        return nil
    }

    func associatedStickerCardBO(fromCards cards: [CardBO]) -> CardBO? {

        guard !cards.isEmpty else {
            return nil
        }

        for relatedContract in relatedContracts where relatedContract.relationType?.id == .linkedWith {

            let cardMatch = cards.filter { card -> Bool in

                return card.cardId == relatedContract.contractId && card.isSticker()

            }

            if !cardMatch.isEmpty {
                return cardMatch[0]
            }

        }

        return nil
    }

    func isSettableToOperative() -> Bool {

        return valueOfIndicator(.settableToOperative)
    }

    func isBlockable() -> Bool {

        return valueOfIndicator(.blockable)
    }

    func allowCustomizeActivations() -> Bool {

        return valueOfIndicator(.customizableActivations)
    }

    func isCancellable() -> Bool {

        return valueOfIndicator(.cancelable)
    }

    func isReadableSecurityData() -> Bool {
        
        return valueOfIndicator(.readableSecurityData)
    }

    func currentBalanceToShow(mainCurrencyCode: String) -> AmountCurrencyBO? {
        
        let referenceCurrencyCode = currencyMajor ?? mainCurrencyCode
        
        switch cardType.id {
            case .debit_card:
                return grantedCredits.first { $0.currency == referenceCurrencyCode }
            default:
                return availableBalance.currentBalances.first { $0.currency == referenceCurrencyCode }
        }
        
    }

    func updateActivations(activationBO: ActivationBO) {

        let hasActivationToModify = activations.contains(where: { activation -> Bool in
            activation.activationId == activationBO.activationId
        })

        if hasActivationToModify {
            activations = activations.map { $0.activationId == activationBO.activationId ? activationBO : $0 }

        } else {
            activations.append(activationBO)
        }
    }

    func cardAlias() -> String {

        if let alias = alias, !alias.isEmpty {
            return alias
        } else if let name = title?.name {
            return name
        }

        return ""
    }
}

// MARK: - Private methods
private extension CardBO {

    func valueOfIndicator(_ indicatorId: IndicatorsType) -> Bool {

        guard !indicators.isEmpty else {
            return false
        }

        for indicator in indicators where indicator.indicatorId == indicatorId {
                return indicator.isActive
        }

        return false
    }
}

// MARK: - Models
class ID_TypeBO: ModelBO {

    var id: String
    var name: String?

    init?(id_type: ID_Type?) {
        guard let idType = id_type?.id else {
            return nil
        }

        id = idType
        name = id_type?.name
    }
}

class CardTypeBO: ModelBO {

    var id: CardType
    var name: String?

    private static let CREDIT_CARD = "CREDIT_CARD"
    private static let DEBIT_CARD = "DEBIT_CARD"
    private static let PREPAID_CARD = "PREPAID_CARD"

    init(id_type: ID_Type?) {

        switch id_type?.id {

            case CardTypeBO.CREDIT_CARD?:
                id = .credit_card

            case CardTypeBO.DEBIT_CARD?:
                id = .debit_card

            case CardTypeBO.PREPAID_CARD?:
                id = .prepaid_card

            default:
                id = .unknown
        }

        name = id_type?.name
    }

    static func stringCardType(cardType: CardType) -> String {
        switch cardType {
            case .credit_card:
                return CardTypeBO.CREDIT_CARD
            case .debit_card:
                return  CardTypeBO.DEBIT_CARD
            case .prepaid_card:
                return CardTypeBO.PREPAID_CARD
            default:
                return ""
        }
    }
}

class BrandAssociationBO: ModelBO {

    var id: CommercialBrandIdentifier
    var name: String?

    private static let VISA = "VISA"
    private static let MASTERCARD = "MASTER_CARD"
    private static let AMERICANEXPRESS = "AMERICAN_EXPRESS"

    init(id_type: ID_Type?) {

        switch id_type?.id {

        case BrandAssociationBO.VISA?:
            id = .visa

        case BrandAssociationBO.MASTERCARD?:
            id = .masterCard

        case BrandAssociationBO.AMERICANEXPRESS?:
            id = .americanExpress

        default:
            id = .unknown
        }

        name = id_type?.name
    }
}

class PysicalSupportBO: ModelBO {

    var id: PhysicalSupportType
    var name: String?

    init(id_type: ID_Type?) {

        if let idType = id_type?.id {
            id = PhysicalSupportType(rawValue: idType) ?? .unknown
        } else {
            id = .unknown
        }

        name = id_type?.name
    }
}

class StatusBO: ModelBO {

    var id: StatusType
    var name: String?

    private static let INOPERATIVE = "INOPERATIVE"
    private static let OPERATIVE = "OPERATIVE"
    private static let BLOCKED = "BLOCKED"
    private static let CANCELED = "CANCELED"
    private static let PENDING_EMBOSSING = "PENDING_EMBOSSING"
    private static let PENDING_DELIVERY = "PENDING_DELIVERY"

    init(id_type: ID_Type?) {

        if let idType = id_type?.id {
            id = StatusType(rawValue: idType) ?? .unknown
        } else {
            id = .unknown
        }

        name = id_type?.name
    }
}

class CurrencyCardBO: ModelBO {

    var currency: String
    var isMajor: Bool

    init?(currencyCard: CurrenciesEntity?) {
        guard let currencyCard = currencyCard else {
            return nil
        }

        currency = currencyCard.currency
        isMajor = currencyCard.isMajor
    }
}

class AmountCurrencyBO: ModelBO {

    var amount: NSDecimalNumber
    var currency: String

    init?(amountCurrency: AmountCurrencyEntity?) {
        guard let amountFromEntity = amountCurrency?.amount, let currencyFromEntity = amountCurrency?.currency else {
            return nil
        }

        amount = NSDecimalNumber(decimal: NSNumber(value: amountFromEntity).decimalValue)
        currency = currencyFromEntity
    }
}

class AvailableBalanceBO: ModelBO {

    var currentBalances: [AmountCurrencyBO]
    var postedBalances: [AmountCurrencyBO]
    var pendingBalances: [AmountCurrencyBO]

    init(availableBalanceEntity: AvailableBalanceEntity?) {

        currentBalances = [AmountCurrencyBO]()

        if let amountCurrenciesEntity = availableBalanceEntity?.currentBalances {
            for amountCurrencyEntity in amountCurrenciesEntity {
                if let amountCurrency = AmountCurrencyBO(amountCurrency: amountCurrencyEntity) {
                    currentBalances.append(amountCurrency)
                }
            }
        }

        postedBalances = [AmountCurrencyBO]()

        if let amountCurrenciesEntity = availableBalanceEntity?.postedBalances {
            for amountCurrencyEntity in amountCurrenciesEntity {
                if let amountCurrency = AmountCurrencyBO(amountCurrency: amountCurrencyEntity) {
                    postedBalances.append(amountCurrency)
                }
            }
        }

        pendingBalances = [AmountCurrencyBO]()

        if let amountCurrenciesEntity = availableBalanceEntity?.pendingBalances {
            for amountCurrencyEntity in amountCurrenciesEntity {
                if let amountCurrency = AmountCurrencyBO(amountCurrency: amountCurrencyEntity) {
                    pendingBalances.append(amountCurrency)
                }
            }
        }
    }

    init(specificAmountsEntity: SpecificAmountsEntity?) {

        currentBalances = [AmountCurrencyBO]()

        if let amountCurrenciesEntity = specificAmountsEntity?.amounts {
            for amountCurrencyEntity in amountCurrenciesEntity {
                if let amountCurrency = AmountCurrencyBO(amountCurrency: amountCurrencyEntity) {
                    currentBalances.append(amountCurrency)
                }
            }
        }

        postedBalances = [AmountCurrencyBO]()

        if let amountCurrenciesEntity = specificAmountsEntity?.amounts {
            for amountCurrencyEntity in amountCurrenciesEntity {
                if let amountCurrency = AmountCurrencyBO(amountCurrency: amountCurrencyEntity) {
                    postedBalances.append(amountCurrency)
                }
            }
        }

        pendingBalances = [AmountCurrencyBO]()

        if let amountCurrenciesEntity = specificAmountsEntity?.amounts {
            for amountCurrencyEntity in amountCurrenciesEntity {
                if let amountCurrency = AmountCurrencyBO(amountCurrency: amountCurrencyEntity) {
                    pendingBalances.append(amountCurrency)
                }
            }
        }
    }
}

class IndicatorBO: ModelBO {

    var indicatorId: IndicatorsType
    var name: String?
    var isActive: Bool

    private static let ASSOCIABLE_TO_NFC = "ASSOCIABLE_TO_NFC"
    private static let ASSOCIABLE_TO_STICKER_CARDS = "ASSOCIABLE_TO_STICKER_CARDS"
    private static let ASSOCIABLE_TO_VIRTUAL_CARDS = "ASSOCIABLE_TO_VIRTUAL_CARDS"
    private static let BLOCKABLE = "BLOCKABLE"
    private static let CANCELABLE = "CANCELABLE"
    private static let CHANGEABLE_ACCOUNT_ASSOCIATION = "CHANGEABLE_ACCOUNT_ASSOCIATION"
    private static let CHANGEABLE_PIN_ATM = "CHANGEABLE_PIN_ATM"
    private static let CHANGEABLE_PIN_ONLINE = "CHANGEABLE_PIN_ONLINE"
    private static let CHANGEABLE_RELATED_CONTRACT_ASSOCIATONS = "CHANGEABLE_RELATED_CONTRACT_ASSOCIATONS"
    private static let CUSTOMIZABLE_ACTIVATIONS = "CUSTOMIZABLE_ACTIVATIONS"
    private static let CUSTOMIZABLE_PIN_BEFORE_EMBOSSED = "CUSTOMIZABLE_PIN_BEFORE_EMBOSSED"
    private static let DESTINATION_CARD_RECHARGE = "DESTINATION_CARD_RECHARGE"
    private static let DESTINATION_INTERNATIONAL_TRANSFER_FROM_ACCOUNT = "DESTINATION_INTERNATIONAL_TRANSFER_FROM_ACCOUNT"
    private static let DESTINATION_TRANSFER_FROM_ACCOUNT = "DESTINATION_TRANSFER_FROM_ACCOUNT"
    private static let FINANCEABLE_CREDIT_PAYMENT = "FINANCEABLE_CREDIT_PAYMENT"
    private static let ORIGIN_CARD_RECHARGE = "ORIGIN_CARD_RECHARGE"
    private static let ORIGIN_CASHOUT_TO_ACCOUNT = "ORIGIN_CASHOUT_TO_ACCOUNT"
    private static let ORIGIN_INTERNATIONAL_TRANSFER_TO_ACCOUNT = "ORIGIN_INTERNATIONAL_TRANSFER_TO_ACCOUNT"
    private static let ORIGIN_MOBILE_PHONE_RECHARGES = "ORIGIN_MOBILE_PHONE_RECHARGES"
    private static let ORIGIN_P2P = "ORIGIN_P2P"
    private static let ORIGIN_SERVICE_PAYMENT = "ORIGIN_SERVICE_PAYMENT"
    private static let ORIGIN_TRANSFER_TO_ACCOUNT = "ORIGIN_TRANSFER_TO_ACCOUNT"
    private static let READABLE_CARD_DETAILS = "READABLE_CARD_DETAILS"
    private static let READABLE_CARD_STATEMENTS = "READABLE_CARD_STATEMENTS"
    private static let READABLE_SECURITY_DATA = "READABLE_SECURITY_DATA"
    private static let SETTABLE_TO_OPERATIVE = "SETTABLE_TO_OPERATIVE"
    private static let ORIGIN_PAY_TERM_TO_LOAN = "ORIGIN_PAY_TERM_TO_LOAN"
    private static let CVV3_REQUIRED = "CVV3_REQUIRED"
    private static let SECURITY_DATA_READ = "SECURITY_DATA_READ"

    init(indicatorId: IndicatorsType, name: String?, isActive: Bool) {
        self.indicatorId = indicatorId
        self.name = name
        self.isActive = isActive
    }

    init(indicatorEntity: IndicatorEntity?) {

        switch indicatorEntity?.indicatorId {

        case IndicatorBO.ASSOCIABLE_TO_NFC:
            indicatorId = .associableToNFC

        case IndicatorBO.ASSOCIABLE_TO_STICKER_CARDS:
            indicatorId = .associableToStickerCards

        case IndicatorBO.ASSOCIABLE_TO_VIRTUAL_CARDS:
            indicatorId = .associableToVirtualCards

        case IndicatorBO.BLOCKABLE:
            indicatorId = .blockable

        case IndicatorBO.CANCELABLE:
            indicatorId = .cancelable

        case IndicatorBO.CHANGEABLE_ACCOUNT_ASSOCIATION:
            indicatorId = .changeableAccountAssociation

        case IndicatorBO.CHANGEABLE_PIN_ATM:
            indicatorId = .changeablePinAtm

        case IndicatorBO.CHANGEABLE_PIN_ONLINE:
            indicatorId = .changeablePinOnline

        case IndicatorBO.CHANGEABLE_RELATED_CONTRACT_ASSOCIATONS:
            indicatorId = .changeableRelatedContracAssociatons

        case IndicatorBO.CUSTOMIZABLE_ACTIVATIONS:
            indicatorId = .customizableActivations

        case IndicatorBO.CUSTOMIZABLE_PIN_BEFORE_EMBOSSED:
            indicatorId = .customizablePinBeforeEmbossed

        case IndicatorBO.DESTINATION_CARD_RECHARGE:
            indicatorId = .destinationCardRecharge

        case IndicatorBO.DESTINATION_INTERNATIONAL_TRANSFER_FROM_ACCOUNT:
            indicatorId = .destinationInternationalTransferFromAccount

        case IndicatorBO.DESTINATION_TRANSFER_FROM_ACCOUNT:
            indicatorId = .destinationTransferFromAccount

        case IndicatorBO.FINANCEABLE_CREDIT_PAYMENT:
            indicatorId = .financiableCreditPayment

        case IndicatorBO.ORIGIN_CARD_RECHARGE:
            indicatorId = .originCardRecharge

        case IndicatorBO.ORIGIN_CASHOUT_TO_ACCOUNT:
            indicatorId = .originCashoutToAccount

        case IndicatorBO.ORIGIN_INTERNATIONAL_TRANSFER_TO_ACCOUNT:
            indicatorId = .originInternationalTransferToAccount

        case IndicatorBO.ORIGIN_MOBILE_PHONE_RECHARGES:
            indicatorId = .originMobilePhoneRecharges

        case IndicatorBO.ORIGIN_P2P:
            indicatorId = .originP2p

        case IndicatorBO.ORIGIN_SERVICE_PAYMENT:
            indicatorId = .originServicePayment

        case IndicatorBO.ORIGIN_TRANSFER_TO_ACCOUNT:
            indicatorId = .originTransferToAccount

        case IndicatorBO.READABLE_CARD_DETAILS:
            indicatorId = .readableCardDetails

        case IndicatorBO.READABLE_CARD_STATEMENTS:
            indicatorId = .readableCardStatements

        case IndicatorBO.READABLE_SECURITY_DATA:
            indicatorId = .readableSecurityData

        case IndicatorBO.SETTABLE_TO_OPERATIVE:
            indicatorId = .settableToOperative

        case IndicatorBO.ORIGIN_PAY_TERM_TO_LOAN:
            indicatorId = .originPayTermToLoan

        case IndicatorBO.CVV3_REQUIRED:
            indicatorId = .cvv3

        default:
            indicatorId = .unknown
        }

        name = indicatorEntity?.name
        isActive = indicatorEntity?.isActive ?? false
    }

    init(entity: IDNameActiveEntity?) {

        switch entity?.id {

        case IndicatorBO.ASSOCIABLE_TO_NFC:
            indicatorId = .associableToNFC

        case IndicatorBO.ASSOCIABLE_TO_STICKER_CARDS:
            indicatorId = .associableToStickerCards

        case IndicatorBO.ASSOCIABLE_TO_VIRTUAL_CARDS:
            indicatorId = .associableToVirtualCards

        case IndicatorBO.BLOCKABLE:
            indicatorId = .blockable
            
        case IndicatorBO.CUSTOMIZABLE_ACTIVATIONS:
            indicatorId = .customizableActivations

        case IndicatorBO.SECURITY_DATA_READ:
            indicatorId = .readableSecurityData

        case IndicatorBO.READABLE_SECURITY_DATA:
            indicatorId = .readableSecurityData

        case IndicatorBO.SETTABLE_TO_OPERATIVE:
            indicatorId = .settableToOperative

        default:
            indicatorId = .unknown
        }

        name = entity?.name
        isActive = entity?.isActive ?? false
    }
}

class ActivationsBO: ModelBO {

    var status: Int?
    var data: ActivationBO?

    init(activationsEntity: ActivationsEntity) {

        if let activationsEntityStatus = activationsEntity.status {
            status = activationsEntityStatus
        }

        if let activationsEntity = activationsEntity.data {
            data = ActivationBO(activationEntity: activationsEntity)
        }
    }
}

class ActivationBO: ModelBO {

    var activationId: ActivationType
    var name: String?
    var isActive: Bool
    var isActivationEnabledForUser: Bool
    var startDate: String?
    var endDate: String?

    init(activationEntity: ActivationEntity?) {

        if let id = activationEntity?.activationId {
            activationId = ActivationType(rawValue: id) ?? .unknown
        } else {
            activationId = .unknown
        }

        name = activationEntity?.name
        isActive = activationEntity?.isActive ?? false
        isActivationEnabledForUser = activationEntity?.isActivationEnabledForUser ?? false
        startDate = activationEntity?.startDate
        endDate = activationEntity?.endDate
    }

    init(entity: IDNameActiveEntity?) {

        if let id = entity?.id {
            activationId = ActivationType(rawValue: id) ?? .unknown
        } else {
            activationId = .unknown
        }

        name = entity?.name
        isActive = entity?.isActive ?? false
        isActivationEnabledForUser = false
    }
}

class RelatedContractBO: ModelBO {
    var relatedContractId: String?
    var contractId: String?
    var number: String?
    var numberType: NumberBO?
    var product: ProductBO?
    var relationType: RelationTypeBO?

    init(relatedContractEntity: RelatedContractEntity?) {

        relatedContractId = relatedContractEntity?.relatedContractId
        contractId = relatedContractEntity?.contractId
        number = relatedContractEntity?.number

        if let relatedContractNumberType = relatedContractEntity?.numberType {
            numberType = NumberBO(idType: relatedContractNumberType)
        }

        if let relatedContractProduct = relatedContractEntity?.product {
            product = ProductBO(idType: relatedContractProduct)
        }

        if let relatedContractRelationType = relatedContractEntity?.relationType {
            relationType = RelationTypeBO(idType: relatedContractRelationType)
        }
    }

    init(relatedContractId: String, contractId: String, number: String, numberType: NumberBO, product: ProductBO, relationType: RelationTypeBO) {
        self.relatedContractId = relatedContractId
        self.contractId = contractId
        self.number = number
        self.numberType = numberType
        self.product = product
        self.relationType = relationType
    }
}

class NumberBO: ModelBO {

    var id: NumberType
    var name: String?

    private static let CCC = "CCC"
    private static let IBAN = "IBAN"
    private static let SWIFT = "SWIFT"
    private static let PAN = "PAN"
    private static let CLABE = "CLABE"
    private static let SPEI = "SPEI"
    private static let LIC = "LIC"

    init(type: NumberType) {
        self.id = type
    }

    init(idType: ID_Type?) {

        switch idType?.id {

        case NumberBO.CCC?:
            id = .ccc

        case NumberBO.IBAN?:
            id = .iban

        case NumberBO.SWIFT?:
            id = .swift

        case NumberBO.PAN?:
            id = .pan

        case NumberBO.CLABE?:
            id = .clabe

        case NumberBO.SPEI?:
            id = .spei

        case NumberBO.LIC?:
            id = .lic

        default:
            id = .unknown
        }

        name = idType?.name
    }
}

class ProductBO: ModelBO {

    var id: ProductType
    var name: String?

    private static let ACCOUNTS = "ACCOUNTS"
    private static let CARDS = "CARDS"

    init(type: ProductType) {
        id = type
    }

    init(idType: ID_Type?) {

        switch idType?.id {

        case ProductBO.ACCOUNTS?:
            id = .accounts

        case ProductBO.CARDS?:
            id = .cards

        default:
            id = .unknown
        }

        name = idType?.name
    }
}

class RelationTypeBO: ModelBO {

    var id: RelationType
    var name: String?

    private static let LINKED_WITH = "LINKED_WITH"
    private static let BLOCKED_DUE_TO_LOST = "BLOCKED_DUE_TO_LOST"
    private static let BLOCKED_DUE_TO_STOLEN = "BLOCKED_DUE_TO_STOLEN"
    private static let BLOCKED_DUE_TO_FRAUD = "BLOCKED_DUE_TO_FRAUD"
    private static let BLOCKED_DUE_TO_FORGERY = "BLOCKED_DUE_TO_FORGERY"
    private static let BLOCKED_DUE_TO_SECURITY = "BLOCKED_DUE_TO_SECURITY"
    private static let BLOCKED_DUE_TO_INTERNAL = "BLOCKED_DUE_TO_INTERNAL"
    private static let CANCELED_DUE_TO_DAMAGE = "CANCELED_DUE_TO_DAMAGE"
    private static let EXPIRED = "EXPIRED"
    private static let MIGRATED = "MIGRATED"
    private static let RENEWAL_DUE_TO_LOST = "RENEWAL_DUE_TO_LOST"
    private static let RENEWAL_DUE_TO_STOLEN = "RENEWAL_DUE_TO_STOLEN"
    private static let RENEWAL_DUE_TO_FRAUD = "RENEWAL_DUE_TO_FRAUD"
    private static let RENEWAL_DUE_TO_FORGERY = "RENEWAL_DUE_TO_FORGERY"
    private static let RENEWAL_DUE_TO_SECURITY = "RENEWAL_DUE_TO_SECURITY"
    private static let RENEWAL_DUE_TO_INTERNAL = "RENEWAL_DUE_TO_INTERNAL"
    private static let RENEWAL_DUE_TO_DAMAGE = "RENEWAL_DUE_TO_DAMAGE"
    private static let RENEWAL_DUE_TO_EXPIRATION = "RENEWAL_DUE_TO_EXPIRATION"
    private static let RENEWAL_DUE_TO_MIGRATION = "RENEWAL_DUE_TO_MIGRATION"

    init(type: RelationType) {
        id = type
    }

    init(idType: ID_Type?) {

        switch idType?.id {

        case RelationTypeBO.LINKED_WITH:
            id = .linkedWith

        case RelationTypeBO.BLOCKED_DUE_TO_LOST:
            id = .blockedDueToLost

        case RelationTypeBO.BLOCKED_DUE_TO_STOLEN:
            id = .blockedDueToStolen

        case RelationTypeBO.BLOCKED_DUE_TO_FRAUD:
            id = .blockedDueToFraud

        case RelationTypeBO.BLOCKED_DUE_TO_FRAUD:
            id = .blockedDueToForgery

        case RelationTypeBO.BLOCKED_DUE_TO_SECURITY:
            id = .blockedDueToSecurity

        case RelationTypeBO.BLOCKED_DUE_TO_INTERNAL:
            id = .blockedDueToInternal

        case RelationTypeBO.CANCELED_DUE_TO_DAMAGE:
            id = .canceledDueToDamage

        case RelationTypeBO.EXPIRED:
            id = .expired

        case RelationTypeBO.MIGRATED:
            id = .migrated

        case RelationTypeBO.RENEWAL_DUE_TO_LOST:
            id = .renewalDueToLost

        case RelationTypeBO.RENEWAL_DUE_TO_STOLEN:
            id = .renewalDueToStolen

        case RelationTypeBO.RENEWAL_DUE_TO_FRAUD:
            id = .renewalDueToFraud

        case RelationTypeBO.RENEWAL_DUE_TO_FORGERY:
            id = .renewalDueToForgery

        case RelationTypeBO.RENEWAL_DUE_TO_SECURITY:
            id = .renewalDueToSecurity

        case RelationTypeBO.RENEWAL_DUE_TO_INTERNAL:
            id = .renewalDueToInternal

        case RelationTypeBO.RENEWAL_DUE_TO_DAMAGE:
            id = .renewalDueToDamage

        case RelationTypeBO.RENEWAL_DUE_TO_EXPIRATION:
            id = .renewalDueToExpiration

        case RelationTypeBO.RENEWAL_DUE_TO_MIGRATION:
            id = .renewalDueToMigration

        default:
            id = .unknown
        }

        name = idType?.name
    }
}

class RewardBO: ModelBO {

    static let bancomer_points = "BANCOMER_POINTS"

    var rewardId: String?
    var name: String?
    var nonMonetaryValue: Int?
    var nonMonetaryValueCloseToExpire: NSNumber?
    var nonMonetaryValueExpirationDate: Date?

    init(rewardEntity: RewardEntity?) {

        if let rewardEntityReceive = rewardEntity {

            if let rewardIdEntity = rewardEntityReceive.rewardId {
                rewardId = rewardIdEntity
            }

            if let rewardNameEntity = rewardEntityReceive.name {
                name = rewardNameEntity
            }

            if let nonMonetaryValueEntity = rewardEntityReceive.nonMonetaryValue {
                nonMonetaryValue = nonMonetaryValueEntity
            }

            if let nonMonetaryValueCloseToExpire = rewardEntityReceive.nonMonetaryValueCloseToExpire {
                self.nonMonetaryValueCloseToExpire = nonMonetaryValueCloseToExpire
            }

            if let nonMonetaryValueExpirationDate = rewardEntityReceive.nonMonetaryValueExpirationDate {
                self.nonMonetaryValueExpirationDate = Date.date(fromServerString: nonMonetaryValueExpirationDate, withFormat: Date.DATE_FORMAT_SERVER)
            }
        }
    }

    init(entity: FinancialOverviewRewardEntity?) {

        if let rewardEntityReceive = entity {

            if let rewardIdEntity = rewardEntityReceive.id {
                rewardId = rewardIdEntity
            }

            if let rewardNameEntity = rewardEntityReceive.name {
                name = rewardNameEntity
            }

            if let nonMonetaryValueEntity = rewardEntityReceive.nonMonetaryValue {
                nonMonetaryValue = nonMonetaryValueEntity
            }
        }
    }
}

enum CardType {
    case credit_card
    case debit_card
    case prepaid_card
    case unknown
}

enum ProductType {
    case cards
    case accounts
    case unknown
}

enum CommercialBrandIdentifier {
    case visa
    case masterCard
    case americanExpress
    case unknown
}

enum PhysicalSupportType: String {
    case normalPlastic = "NORMAL_PLASTIC"
    case sticker = "STICKER"
    case virtual = "VIRTUAL"
    case nfc = "NFC"
    case unknown
}

enum StatusType: String {

    private static let INOPERATIVE = "INOPERATIVE"
    private static let OPERATIVE = "OPERATIVE"
    private static let BLOCKED = "BLOCKED"
    private static let CANCELED = "CANCELED"
    private static let PENDING_EMBOSSING = "PENDING_EMBOSSING"
    private static let PENDING_DELIVERY = "PENDING_DELIVERY"

    case inoperative = "INOPERATIVE"
    case operative = "OPERATIVE"
    case blocked = "BLOCKED"
    case canceled = "CANCELED"
    case pendingEmbossing = "PENDING_EMBOSSING"
    case pendingDelivery = "PENDING_DELIVERY"
    case unknown
}

enum BrandCompanyType {
    case legal
    case operative
    case unknown
}

enum ActivationType: String {
    case onoff = "ON_OFF"
    case pinlessActivation = "PINLESS_ACTIVATION"
    case ecommerceActivation = "ECOMMERCE_ACTIVATION"
    case cashwithdrawalActivation = "CASHWITHDRAWAL_ACTIVATION"
    case purchasesActivation = "PURCHASES_ACTIVATION"
    case foreignCashWithDrawalActivation = "FOREIGN_CASHWITHDRAWAL_ACTIVATION"
    case foreignPurchasesActivation = "FOREIGN_PURCHASES_ACTIVATION"
    case unknown
}

enum IndicatorsType {
    case associableToNFC
    case associableToStickerCards
    case associableToVirtualCards
    case blockable
    case cancelable
    case changeableAccountAssociation
    case changeablePinAtm
    case changeablePinOnline
    case changeableRelatedContracAssociatons
    case customizableActivations
    case customizablePinBeforeEmbossed
    case destinationCardRecharge
    case destinationInternationalTransferFromAccount
    case destinationTransferFromAccount
    case financiableCreditPayment
    case originCardRecharge
    case originCashoutToAccount
    case originInternationalTransferToAccount
    case originMobilePhoneRecharges
    case originP2p
    case originServicePayment
    case originTransferToAccount
    case readableCardDetails
    case readableCardStatements
    case readableSecurityData
    case settableToOperative
    case originPayTermToLoan
    case cvv3
    case unknown
}

enum NumberType {
    case ccc
    case iban
    case swift
    case pan
    case clabe
    case spei
    case lic
    case unknown
}

enum RelationType {
    case linkedWith
    case blockedDueToLost
    case blockedDueToStolen
    case blockedDueToFraud
    case blockedDueToForgery
    case blockedDueToSecurity
    case blockedDueToInternal
    case canceledDueToDamage
    case expired
    case migrated
    case renewalDueToLost
    case renewalDueToStolen
    case renewalDueToFraud
    case renewalDueToForgery
    case renewalDueToSecurity
    case renewalDueToInternal
    case renewalDueToDamage
    case renewalDueToExpiration
    case renewalDueToMigration
    case unknown
}

extension CardBO: Equatable {
    static func == (lhs: CardBO, rhs: CardBO) -> Bool {
        return lhs.cardId == rhs.cardId
    }
}

extension CardTypeBO: Equatable {
    static func == (lhs: CardTypeBO, rhs: CardTypeBO) -> Bool {
        return lhs.id == rhs.id
    }
    
}
