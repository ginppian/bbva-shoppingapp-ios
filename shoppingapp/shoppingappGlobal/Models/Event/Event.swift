//
//  Event.swift
//  shoppingapp
//
//  Created by jesus.martinez on 7/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol EventProtocol: Model {
    var type: TypeEvent { get }
}

enum TypeEvent {
    case login
    case showPromotions
    case startOption
    case cardsOption
    case promotionsOption
    case pointsOption
    case notificationsOption
    case promotionDetail
    case favoriteCategories
    case seeShoppingFilterResults
    case noPromotionContact
    case promotionPush
    case generateDigitalCard
    case seeCardCVV
}

class Event: EventProtocol {
    
    let type: TypeEvent
    
    init(type: TypeEvent) {
        self.type = type
    }
}
