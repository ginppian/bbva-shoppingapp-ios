//
//  BalanceRewardsBO.swift
//  shoppingapp
//
//  Created by jesus.martinez on 18/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class BalanceRewardsBO: ModelBO {

    var status: Int
    var startDate: Date?
    var endDate: Date?
    var rewardBreakDown = [RewardBreakDownBO]()
    var nonMonetaryValueCloseToExpired: Int?
    var nonMonetaryValueExpirationDate: Date?

    init(entity: BalanceRewardsEntity) {

        status = entity.status

        guard let data = entity.data?[0], let balance = data.balance else {
            return
        }

        nonMonetaryValueCloseToExpired = data.nonMonetaryValueCloseToExpire

        if let nonMonetaryValueExpirationDateRaw = data.nonMonetaryValueExpirationDate {
            
            nonMonetaryValueExpirationDate = Date.date(fromLocalTimeZoneString: nonMonetaryValueExpirationDateRaw, withFormat: Date.DATE_FORMAT_SERVER)
        }

        if let balanceStartDate = balance.startDate {

            startDate = Date.date(fromLocalTimeZoneString: balanceStartDate, withFormat: Date.DATE_FORMAT_SERVER)
        }

        if let balanceEndDate = balance.endDate {

            endDate = Date.date(fromLocalTimeZoneString: balanceEndDate, withFormat: Date.DATE_FORMAT_SERVER)
        }

        if let rewards = balance.rewardBreakDown {

            for reward in rewards {

                rewardBreakDown.append(RewardBreakDownBO(entity: reward))
            }
        }
    }

    func rewardBreakDown(_ id: RewardBreakDownId) -> RewardBreakDownBO? {

        guard id != .unknown && !rewardBreakDown.isEmpty else {
            return nil
        }

        let rewardFound = rewardBreakDown.first { rewardBreakDown -> Bool in
            rewardBreakDown.id == id
        }

        return rewardFound
    }

}

extension BalanceRewardsBO: Equatable {
    public static func == (lhs: BalanceRewardsBO, rhs: BalanceRewardsBO) -> Bool {

        return lhs.status == rhs.status
            && lhs.startDate == rhs.startDate
            && lhs.endDate == rhs.endDate
            && lhs.rewardBreakDown == rhs.rewardBreakDown

    }
}

class RewardBreakDownBO {
    var id: RewardBreakDownId
    var nonMonetaryValue: Int

    init(entity: RewardBreakDownEntity) {

        if let rewardId = entity.id {
            id = RewardBreakDownId(rawValue: rewardId) ?? .unknown
        } else {
            id = .unknown
        }

        nonMonetaryValue = entity.nonMonetaryValue ?? 0
    }
}

extension RewardBreakDownBO: Equatable {
    public static func == (lhs: RewardBreakDownBO, rhs: RewardBreakDownBO) -> Bool {

        return lhs.id == rhs.id
            && lhs.nonMonetaryValue == rhs.nonMonetaryValue

    }
}

enum RewardBreakDownId: String {

    case generated = "GENERATED"
    case applied = "APPLIED"
    case expired = "EXPIRED"
    case current = "CURRENT"
    case previous = "PREVIOUS"
    case unknown

}
