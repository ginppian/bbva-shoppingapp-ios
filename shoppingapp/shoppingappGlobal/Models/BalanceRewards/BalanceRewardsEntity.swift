//
//  BalanceRewardsEntity.swift
//  shoppingapp
//
//  Created by jesus.martinez on 18/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import HandyJSON

class BalanceRewardsEntity: ModelEntity, HandyJSON {
    var status: Int!
    var data: [BalanceRewardsDataEntity]?

    required init() {}
}

struct BalanceRewardsDataEntity: HandyJSON {

    var rewardId: String?
    var name: String?
    var nonMonetaryValue: Int?
    var unitType: ID_Type?
    var nonMonetaryValueCloseToExpire: Int?
    var nonMonetaryValueExpirationDate: String?
    var isRedeemable: Bool?
    var nonMonetaryRedemptionSavings: AmountCurrencyEntity?
    var balance: BalanceRewardsBalanceDataEntity?
}

struct BalanceRewardsBalanceDataEntity: HandyJSON {

    var startDate: String?
    var endDate: String?
    var rewardBreakDown: [RewardBreakDownEntity]?
}

struct RewardBreakDownEntity: HandyJSON {

    var id: String?
    var nonMonetaryValue: Int?

}
