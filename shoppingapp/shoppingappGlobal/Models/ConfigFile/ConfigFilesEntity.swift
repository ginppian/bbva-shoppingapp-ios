//
//  ConfigFilesEntity.swift
//  shoppingapp
//
//  Created by AZIZEBULBUL on 24/01/2018.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import HandyJSON

class ConfigFilesEntity: ModelEntity, HandyJSON {

    var status: Int! = 0
    var data: [ConfigFileEntity]?
    var version: String = ""

    required init() {}
}

struct ConfigFileEntity: HandyJSON {
    var id: String!
    var help: HelpEntity!
    var section: HelpEntity!
    var image: String?
    var platform: String?
    var texts: [TextsEntity]!
    var sections: [SectionsEntity]!

}

struct SectionsEntity: HandyJSON {
    var sectionId: String!
}

struct HelpEntity: HandyJSON {
    var backgroundColor: ColorEntity!
    var titleColor: ColorEntity!
    var descriptionColor: ColorEntity!
    var descriptionOnTapColor: ColorEntity?
    
}
