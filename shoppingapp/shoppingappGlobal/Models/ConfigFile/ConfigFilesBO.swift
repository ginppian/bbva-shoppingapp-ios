//
//  ConfigFilesBO.swift
//  shoppingapp
//
//  Created by AZIZEBULBUL on 24/01/2018.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class ConfigFilesBO: ModelBO {

    var status: Int
    var items = [ConfigFileBO]()
    var version: String

    init() {
        status = 0
        version = ""
    }

    init(configFilesEntity: ConfigFilesEntity) {

        status = configFilesEntity.status

        version = configFilesEntity.version

       var configFilesBo = [ConfigFileBO]()

        if let configFilesEntity = configFilesEntity.data {
            for configFileEntity in configFilesEntity {
                if configFileEntity.platform == "ios" || configFileEntity.platform == "both" {

                    if let idHelp = configFileEntity.id, !idHelp.isEmpty, configFileEntity.texts != nil {

                        var selectedLanguage = false

                        for text in configFileEntity.texts {

                            if let title = text.title, !title.isEmpty {

                                if text.language == ConstantsLanguage.DEFAULT_LANGUAGE || text.language == Settings.Global.default_language {
                                    selectedLanguage = true
                                    break
                                }
                            }
                        }

                        if selectedLanguage {
                            configFilesBo.append(ConfigFileBO(configFileEntity: configFileEntity))
                        }
                    }
                }
            }
        }
        items = configFilesBo
    }
}

class ConfigFileBO: ModelBO {

    var itemId: String!
    var help: HelpBO!
    var section: HelpBO!
    var image: String?
    var platform: String?
    var texts: [TextBO]!
    var sections: [SectionBO]!

    init(configFileEntity: ConfigFileEntity) {

        itemId = configFileEntity.id
        
        if let helpEntity = configFileEntity.help {
            help = HelpBO(helpEntity: helpEntity)
        }
        if let sectionEntity = configFileEntity.section {
            section = HelpBO(helpEntity: sectionEntity)
        }
        
        image = configFileEntity.image
        platform = configFileEntity.platform

        var textsBO = [TextBO]()

        if let textsEntity = configFileEntity.texts {

            for textEntity in textsEntity {

                textsBO.append(TextBO(textEntity: textEntity))
            }
        }

        texts = textsBO

        var sectionsBO = [SectionBO]()

        if let sectionsEntity = configFileEntity.sections {

            for sectionEntity in sectionsEntity {
                sectionsBO.append(SectionBO(sectionEntity: sectionEntity))
            }
        }
        sections = sectionsBO
    }

    func getSelectedLanguage(forKey key: String) -> TextBO? {

        var textBo: TextBO?

        if let textList = texts {
            for textBO in textList where textBO.language == key {
                textBo = textBO
            }

        }
        return textBo
    }
    func getImageURL(forUrl url: String) -> String {

        let imageUrlWithPlatform = url.replacingOccurrences(of: "%so%", with: Constants.PLATFORM)
        let imageUrl = imageUrlWithPlatform.replacingOccurrences(of: "%size%", with: ImagesScales.imageScale)

        return imageUrl
    }

}

class SectionBO: ModelBO {

    var sectionId: String!

    init(sectionEntity: SectionsEntity) {
        sectionId = sectionEntity.sectionId

    }
}

class HelpBO: ModelBO {
    
    var backgroundColor: ColorBO!
    var titleColor: ColorBO!
    var descriptionColor: ColorBO!
    var descriptionOnTapColor: ColorBO!
    
    init(helpEntity: HelpEntity) {
        
        if let backgroundEntity = helpEntity.backgroundColor {
            backgroundColor = ColorBO(colorEntity: backgroundEntity)
        }
        
        if let titleEntity = helpEntity.titleColor {
            titleColor = ColorBO(colorEntity: titleEntity)
        }
        
        if let descriptionEntity = helpEntity.descriptionColor {
            descriptionColor = ColorBO(colorEntity: descriptionEntity)
        }
        
        if let descriptionOnTapEntity = helpEntity.descriptionOnTapColor {
            descriptionOnTapColor = ColorBO(colorEntity: descriptionOnTapEntity)
        }
        
    }
}
