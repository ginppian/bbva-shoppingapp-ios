//
//  TicketEntity.swift
//  shoppingapp
//
//  Created by ruben.fernandez on 7/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

enum TicketType {
    case purchaseAccepted
    case purchaseDenied
    case unknown
}

class TicketModel: Model {

    var type: TicketType = TicketType.unknown
    var pan5: String = ""
    var bin: String = ""
    var concept: String = ""
    var transactionAmount: String = ""
    var transactionCurrency: String = ""
    var receiptDate: String = ""
    var deniedText: String = ""
    var authorizationNumber: String = ""

    init(withPayload payload: [AnyHashable: Any]) {

        var aps = payload["aps"] as? [AnyHashable: Any]

        if let alertReceive = aps?["alert"] as? [AnyHashable: Any] {

            switch alertReceive["loc-key"] as! String {
            case "00001":
                type = TicketType.purchaseAccepted
            case "00050":
                type = TicketType.purchaseDenied
            default:
                type = TicketType.unknown
            }

            if let locArgs = alertReceive["loc-args"] as? [Any], !locArgs.isEmpty {

                for i in 0..<locArgs.count {

                    switch i {
                    case 0:
                        pan5 = locArgs[0] as! String
                    case 1:
                        bin = locArgs[1] as! String
                    case 2:
                        concept = locArgs[2] as! String
                    case 3:
                        transactionAmount = locArgs[3] as! String
                    case 4:
                        transactionCurrency = locArgs[4] as! String
                    case 5:
                        receiptDate = locArgs[5] as! String
                    case 6:
                        authorizationNumber = locArgs[6] as! String
                    case 7:
                        deniedText = locArgs[7] as! String

                    default:
                        break

                    }

                }

            }
        }
    }
}

extension TicketModel: Equatable {
    
    static func == (lhs: TicketModel, rhs: TicketModel) -> Bool {
        
        return lhs.type == rhs.type &&
            lhs.pan5 == rhs.pan5 &&
            lhs.bin == rhs.bin &&
            lhs.concept == rhs.concept &&
            lhs.transactionAmount == rhs.transactionAmount &&
            lhs.transactionCurrency == rhs.transactionCurrency &&
            lhs.receiptDate == rhs.receiptDate &&
            lhs.authorizationNumber == rhs.authorizationNumber
    }
}
