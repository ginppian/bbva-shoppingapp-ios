//
//  LoginTransport.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 15/3/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation

class LoginTransport: ModelTransport {

    let username: String
    let password: String
    
    var userId: String {
        
        return username
    }

    init(username: String, password: String) {
        
        self.username = username
        self.password = password
    }
}
