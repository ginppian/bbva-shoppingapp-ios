//
//  LoginTransportA.swift
//  shoppingapp
//
//  Created by Javier Pino on 14/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation

class LoginTransportA: LoginTransport {

    let documentCode: String
    
    override var userId: String {
        
        return "\(documentCode)\(username)"
    }

    init(documentCode: String, username: String, password: String) {
        
        self.documentCode = documentCode
        super.init(username: username, password: password)
    }
}
