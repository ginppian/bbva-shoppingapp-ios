//
//  WebTransport.swift
//  shoppingapp
//
//  Created by Arturo Marzo de la Fuente on 29/10/2018.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class WebTransport: ModelTransport {
    
    var webPage: String?
    var webTitle: String?
    
    init(webPage: String? = nil, webTitle: String? = nil) {
        
        self.webPage = webPage
        self.webTitle = webTitle
    }
}

extension WebTransport: Equatable {
    
    static func == (lhs: WebTransport, rhs: WebTransport) -> Bool {
        
        return lhs.webPage == rhs.webPage
                && lhs.webTitle == rhs.webTitle
    }
}
