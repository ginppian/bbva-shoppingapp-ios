//
//  SettingsTransport.swift
//  shoppingapp
//
//  Created by Marcos on 7/6/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import HandyJSON

struct SettingsTransport: HandyJSON, Equatable {
    
    var isNotificationsAvailable: Bool
    var isBalancesAvailable: Bool
    var isVibrationEnabled: Bool
    var isTicketPromotionsEnabled: Bool
    
    init() {
        self.init(isNotificationsAvailable: false, isBalancesAvailable: false, isVibrationEnabled: false, isTicketPromotionsEnabled: false)
    }
    
    init(isNotificationsAvailable: Bool, isBalancesAvailable: Bool, isVibrationEnabled: Bool, isTicketPromotionsEnabled: Bool) {

        self.isNotificationsAvailable = isNotificationsAvailable
        self.isBalancesAvailable = isBalancesAvailable
        self.isVibrationEnabled = isVibrationEnabled
        self.isTicketPromotionsEnabled = isTicketPromotionsEnabled
    }
}
