//
//  AnonymousInformationEntity.swift
//  shoppingapp
//
//  Created by jesus.martinez on 9/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class AnonymousInformationEntity: ModelEntity {

    let consumerId: String
    let userId: String

    init(consumerId: String, userId: String) {
        self.consumerId = consumerId
        self.userId = userId
    }

}
