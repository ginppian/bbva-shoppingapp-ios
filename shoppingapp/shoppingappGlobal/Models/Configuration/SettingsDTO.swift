//
//  SettingsDTO.swift
//  shoppingapp
//
//  Created by Marcos on 11/6/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

final class ConfigurationPreferencesDTO: CellsDTO {
    let keyConfig: String
    let enabled: Bool

    init(enabled: Bool, keyConfig: String) {
        self.enabled = enabled
        self.keyConfig = keyConfig
    }
}
