//
//  AnonymousInformationBO.swift
//  shoppingapp
//
//  Created by jesus.martinez on 9/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class AnonymousInformationBO: ModelBO {

    let consumerId: String
    let userId: String

    init(entity: AnonymousInformationEntity) {
        consumerId = entity.consumerId
        userId = entity.userId
    }

}

extension AnonymousInformationBO: Equatable {

    public static func == (lhs: AnonymousInformationBO, rhs: AnonymousInformationBO) -> Bool {
        return lhs.consumerId == rhs.consumerId && lhs.userId == rhs.userId
    }

}
