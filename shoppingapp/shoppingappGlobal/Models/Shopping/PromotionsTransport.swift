//
//  TransactionFilterTransport.swift
//  shoppingapp
//
//  Created by ruben.fernandez on 18/8/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation

class PromotionsTransport: ModelTransport { // Transport data structure (not Entity)

    var promotionsBO: PromotionsBO?
    var promotionName: String?
    var category: CategoryBO?
    var trendingType: TrendingType?
    var categories: CategoriesBO?

}
