//
//  PromotionsBO.swift
//  shoppingapp
//
//  Created by ruben.fernandez on 7/11/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation

class PromotionsBO: ModelBO {

    var status: Int
    var promotions = [PromotionBO]()
    var pagination: PaginationBO?

    init() {
        status = 0
    }

    init(promotionsEntity: PromotionsEntity) {

        status = promotionsEntity.status

        var promotionsBO = [PromotionBO]()

        if let promotionsEntity = promotionsEntity.data {
            for promotionEntity in promotionsEntity {
                promotionsBO.append(PromotionBO(promotionEntity: promotionEntity))
            }
        }

        promotions = promotionsBO

        pagination = PaginationBO(paginationEntity: promotionsEntity.pagination)
    }
    
    init(promotionBO: PromotionBO) {
        
        status = 0
        
        var promotionsBO = [PromotionBO]()
        promotionsBO.append(promotionBO)
        
        promotions = promotionsBO
    }

    func groupByLatitudeLongitudeStoreAndPromotions() -> [DataPoi] {

        var dataPoi = [DataPoi]()
        let positionPrecision = DistanceConstants.positionPrecisionForGroupByLatitudeLongitude

        for promotion in promotions {

            if let stores = promotion.stores {

                for store in stores {

                    guard let location = store.location?.geolocation else {
                        break
                    }

                    let poi = dataPoi.first(where: { dataPoi -> Bool in
                        return location.latitude.rounded(toPlaces: positionPrecision) == dataPoi.latitude.rounded(toPlaces: positionPrecision) && location.longitude.rounded(toPlaces: positionPrecision) == dataPoi.longitude.rounded(toPlaces: positionPrecision)
                    })

                    if let poi = poi {
                        poi.append(storeId: store.id)
                        poi.append(promotion: promotion)
                    } else {
                        let poi = DataPoi(latitude: location.latitude.rounded(toPlaces: positionPrecision), longitude: location.longitude.rounded(toPlaces: positionPrecision), storeId: store.id)
                        poi.append(promotion: promotion)

                        dataPoi.append(poi)
                    }

                }
            }

        }

        return dataPoi

    }

}

extension Double {
    func rounded(toPlaces places: Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}

class PromotionBO: ModelBO {

    var id: String!
    var name: String!
    var startDate: Date!
    var endDate: Date!

    var commerceInformation: CommerceInformationBO!
    var images: [ImagesBO]?
    var imageDetail: String!
    var imageList: String!
    var imagePush: String!
    var benefit: String!
    var stores: [StoreBO]?
    var promotionType: PromotionTypeBO!
    var promotionUrl: String!
    var categories: [CategoryBO]?
    var descriptions: [PromotionDescriptionBO]?
    var legalConditions: LegalConditionsBO?
    var cards: [PromotionCardBO]?
    var usageTypes: [UsageTypeBO]?
    var contactDetails: [ContactDetailBO]?

    var imageDetailSaved: UIImage?
    var imageListSaved: UIImage?
    var imagePushSaved: UIImage?

    static let numberDays = 7

    static let imageDetail = "DETAIL"
    static let imageList = "LIST"
    static let imagePush = "PUSH"

    init(promotionEntity: PromotionEntity) {

        id = promotionEntity.id
        name = promotionEntity.name
        startDate = Date.date(fromLocalTimeZoneString: promotionEntity.startDate, withFormat: Date.DATE_FORMAT_SERVER)
        endDate = Date.date(fromLocalTimeZoneString: promotionEntity.endDate, withFormat: Date.DATE_FORMAT_SERVER)
        commerceInformation = CommerceInformationBO(commerceInformationEntity: promotionEntity.commerceInformation)

        var imagesBO = [ImagesBO]()

        if let imagesEntity = promotionEntity.images {

            for imageEntity in imagesEntity {

                if let imageBO = ImagesBO(imageEntity: imageEntity) {

                    if imageBO.id == PromotionBO.imageDetail {
                        imageDetail = imageBO.url
                    } else if imageBO.id == PromotionBO.imageList {
                        imageList = imageBO.url
                    } else if imageBO.id == PromotionBO.imagePush {
                        imagePush = imageBO.url
                    }

                    imagesBO.append(imageBO)
                }
            }

        }

        images = imagesBO

        if Tools.isStringNilOrEmpty(withString: promotionEntity.benefit) {
            benefit = ""
        } else {
            benefit = promotionEntity.benefit
        }

        var storesBO = [StoreBO]()

        if let storesEntity = promotionEntity.stores {

            for storeEntity in storesEntity {

                let storeBO = StoreBO(storeEntity: storeEntity)

                storesBO.append(storeBO)
            }
        }

        stores = storesBO
        
        var contactDetailBO = [ContactDetailBO]()
        
        if let contactDetailEntity = promotionEntity.contactDetails {
            
            for contactEntity in contactDetailEntity {
                
                let contactBO = ContactDetailBO(contactDetailEntity: contactEntity)
                
                contactDetailBO.append(contactBO)
            }
        }
        
        contactDetails = contactDetailBO
        
        promotionType = PromotionTypeBO(promotionTypeEntity: promotionEntity.promotionType)
        
        if Tools.isStringNilOrEmpty(withString: promotionEntity.promotionUrl) {
            promotionUrl = ""
        } else {
            promotionUrl = promotionEntity.promotionUrl
        }
        
        var categoriesBO = [CategoryBO]()

        if let categoriesEntity = promotionEntity.categories {

            for categoryEntity in categoriesEntity {

                let categoryBO = CategoryBO(categoryEntity: categoryEntity)

                categoriesBO.append(categoryBO)
            }
        }

        categories = categoriesBO

        var descriptionsBO = [PromotionDescriptionBO]()

        if let descriptionsEntity = promotionEntity.descriptions {

            for descriptionEntity in descriptionsEntity {

                let descriptionBO = PromotionDescriptionBO(promotionDescriptionEntity: descriptionEntity)

                descriptionsBO.append(descriptionBO)
            }
        }

        descriptions = descriptionsBO

        var cardsBO = [PromotionCardBO]()

        if let cardsEntity = promotionEntity.cards {

            for cardEntity in cardsEntity {

                let cardBO = PromotionCardBO(promotionCardEntity: cardEntity)

                cardsBO.append(cardBO)
            }
        }

        cards = cardsBO

        var usagesBO = [UsageTypeBO]()

        if let usagesEntity = promotionEntity.usageTypes {

            for usageEntity in usagesEntity {

                let usageBO = UsageTypeBO(promotionUsageIdType: usageEntity)

                usagesBO.append(usageBO)
            }
        }

        usageTypes = usagesBO

        if let legalEntity = promotionEntity.legalConditions {
            legalConditions = LegalConditionsBO(legalConditionsEntity: legalEntity)
        }

    }

    func isDiscountPromotionType() -> Bool {
        return promotionType.id == .discount
    }

    func isToMonthPromotionType() -> Bool {
        return promotionType.id == .toMonths
    }

    func isPointsPromotionType() -> Bool {
        return promotionType.id == .point
    }

    func isPromotionUsageType(usageType: PromotionUsageType) -> Bool {
        var isUsageType = false

        if let usages = usageTypes {

            for usage in usages where usage.usageId == usageType {
                isUsageType = true
                break
            }
        }

        return isUsageType
    }
    
    func groupByLatitudeLongitudeStore() -> [DataPoi] {
        
        var dataPoi = [DataPoi]()
        let positionPrecision = DistanceConstants.positionPrecisionForGroupByLatitudeLongitude
        
        guard let stores = self.stores else {
            return dataPoi
        }
        
        for store in stores {
            
            guard let location = store.location?.geolocation else {
                break
            }
            
            let poi = DataPoi(latitude: location.latitude.rounded(toPlaces: positionPrecision), longitude: location.longitude.rounded(toPlaces: positionPrecision), storeId: store.id)
            poi.append(promotion: self)
            poi.append(storeId: store.id)

            dataPoi.append(poi)
            
        }
        
        return dataPoi
        
    }
}

extension PromotionBO: Equatable {
    static func == (lhs: PromotionBO, rhs: PromotionBO) -> Bool {
        return lhs.id == rhs.id
    }
}

extension PromotionCardBO: Equatable {
    static func == (lhs: PromotionCardBO, rhs: PromotionCardBO) -> Bool {
        return lhs.cardTitle == rhs.cardTitle
    }
}

class CommerceInformationBO: ModelBO {

    var name: String!
    var description: String!
    var web: String!
    var logo: ImagesBO?

    init(commerceInformationEntity: CommerceInformationEntity) {
        name = commerceInformationEntity.name
        description = commerceInformationEntity.description
        web = commerceInformationEntity.web
        logo = ImagesBO(imageEntity: commerceInformationEntity.logo)
    }
}
class StoreBO: ModelBO {

    var id: String!
    var name: String!
    var location: LocationBO?
    var distance: DistanceBO?
    var contactDetails: [ContactDetailBO]?

    init(storeEntity: StoresEntity) {
        id = storeEntity.id
        name = storeEntity.name
        if let locationEntity = storeEntity.location {
            location = LocationBO(locationEntity: locationEntity)
        }

        if let distanceEntity = storeEntity.distance {
            distance = DistanceBO(distanceEntity: distanceEntity)
        }

        var contactDetailsBO = [ContactDetailBO]()

        if let contactDetailsEntity = storeEntity.contactDetails {

            for contactDetailEntity in contactDetailsEntity {

                let contactDetailBO = ContactDetailBO(contactDetailEntity: contactDetailEntity)

                contactDetailsBO.append(contactDetailBO)
            }
        }

        contactDetails = contactDetailsBO

    }

    func formattedDistance() -> String? {

        var distanceFormatted: String?

        if let kilometers = distance?.length, kilometers != 0, distance?.lengthType?.id == .kilometers {

            if kilometers < 1.0 {

                let kilometersToMetters = Double(kilometers) * Double(DistanceConstants.meters_to_kilometers)
                distanceFormatted = String(format: "%.0f %@", kilometersToMetters, Localizables.promotions.key_distance_meters)

            } else {

                distanceFormatted = String(format: "%.1f %@", kilometers, Localizables.promotions.key_distance_kilometers)
            }

            return distanceFormatted
        }

        if let metters = distance?.length, metters != 0, distance?.lengthType?.id == .metters {

            if metters >= Double(DistanceConstants.meters_to_kilometers) {

                let metersToKilometers = Double(metters) / Double(DistanceConstants.meters_to_kilometers)
                distanceFormatted = String(format: "%.1f %@", metersToKilometers, Localizables.promotions.key_distance_kilometers)

            } else {

                distanceFormatted = String(format: "%.0f %@", metters, Localizables.promotions.key_distance_meters)
            }
        }

        return distanceFormatted
    }
}

class ContactDetailBO: ModelBO {

    var contactType: ContactTypeBO?
    var contact: String?
    var description: String?

    init(contactDetailEntity: ContactDetailEntity) {

        if let contactTypeEntity = contactDetailEntity.contactType {
            contactType = ContactTypeBO(contactTypeEntity: contactTypeEntity)
        }

        if let contactDetails = contactDetailEntity.contact {
            contact = contactDetails
        }

        if let descriptionDetails = contactDetailEntity.description {
            description = descriptionDetails
        }
    }
}

class ContactTypeBO: ModelBO {

    var id: ContactType?
    var name: String?

    init(contactTypeEntity: ContactTypeEntity) {

        if let idContactType = contactTypeEntity.id {
            id = ContactType(rawValue: idContactType)
        }

        if let nameContactType = contactTypeEntity.name {
            name = nameContactType
        }
    }
}

class LocationBO: ModelBO {

    var addressName: String!
    var city: String!
    var state: String!
    var zipCode: String!
    var country: CountryBO?
    var geolocation: GeoLocationBO?
    var additionalInformation: String?

    init(locationEntity: LocationEntity) {
        addressName = locationEntity.addressName
        city = locationEntity.city
        state = locationEntity.state
        zipCode = locationEntity.zipCode

        if let countryEntity = locationEntity.country {
            country = CountryBO(countryEntity: countryEntity)
        }

        if let geolocationEntity = locationEntity.geolocation {
            geolocation = GeoLocationBO(geoLocationEntity: geolocationEntity)
        }

        if let additionalInfo = locationEntity.additionalInformation {
            additionalInformation = additionalInfo
        }

    }
}
class CountryBO: ModelBO {

    var id: String!
    var name: String!

    init(countryEntity: CountryEntity) {
        id = countryEntity.id
        name = countryEntity.name
    }
}
class GeoLocationBO: ModelBO {

    var latitude: Double
    var longitude: Double

    init(geoLocationEntity: GeoLocationEntity) {
        latitude = geoLocationEntity.latitude
        longitude = geoLocationEntity.longitude
    }

    convenience init(withLatitude latitude: Double, withLongitude longitude: Double) {
        let geoLocationEntity = GeoLocationEntity()
        geoLocationEntity.latitude = latitude
        geoLocationEntity.longitude = longitude
        self.init(geoLocationEntity: geoLocationEntity)
    }
}
extension GeoLocationBO: Equatable {

    static func == (lhs: GeoLocationBO, rhs: GeoLocationBO) -> Bool {

        return lhs.latitude == rhs.latitude &&
            lhs.longitude == rhs.longitude
    }
}

extension CardTitleBO: Equatable {
    static func == (lhs: CardTitleBO, rhs: CardTitleBO) -> Bool {
        return lhs.id == rhs.id &&
            lhs.name == rhs.name
    }
}

class PromotionTypeBO: ModelBO {

    var name: String!
    var id: PromotionType!

    private static let RATE = "RATE"
    private static let FIXED = "FIXED"
    private static let GIFT = "GIFT"
    private static let CASHBACK = "CASHBACK"
    private static let POST_PAYMENT = "POST_PAYMENT"
    private static let LOYALTY_POINTS = "LOYALTY_POINTS"

    init(promotionTypeEntity: PromotionTypeEntity?) {

        name = promotionTypeEntity?.name

        switch promotionTypeEntity?.id {

        case PromotionTypeBO.RATE?:
            id = .discount
        case PromotionTypeBO.FIXED?:
            id = .discount
        case PromotionTypeBO.GIFT?:
            id = .discount
        case PromotionTypeBO.CASHBACK?:
            id = .discount
        case PromotionTypeBO.POST_PAYMENT?:
            id = .toMonths
        case PromotionTypeBO.LOYALTY_POINTS?:
            id = .point
        default:
            id = .unknown
        }

    }

}
enum PromotionType: String {

    static let RATE_FOR_SERVICE = "FIXED,RATE,GIFT,CASHBACK"
    static let FIXED_FOR_SERVICE = "LOYALTY_POINTS"
    static let GIFT_FOR_SERVICE = "POST_PAYMENT"

    case discount = "DISCOUNT"
    case point = "POINT"
    case toMonths = "MONTHS"
    case unknown

    var toService: String {

        switch self {
        case .discount:
            return PromotionType.RATE_FOR_SERVICE
        case .point:
            return PromotionType.FIXED_FOR_SERVICE
        case .toMonths:
            return PromotionType.GIFT_FOR_SERVICE
        default:
            return ""
        }
    }

    var literal: String {

        switch self {
        case .discount:
            return Localizables.promotions.key_discount_text
        case .point:
            return Localizables.promotions.key_promotions_points_text
        case .toMonths:
            return Localizables.promotions.key_months_without_interest_text
        default:
            return ""
        }
    }

    var color: UIColor {

        switch self {
        case .discount:
            return .BBVADARKPINK
        case .point:
            return .DARKAQUA
        case .toMonths:
            return .BBVAMEDIUMYELLOW
        default:
            return .MEDIUMBLUE
        }
    }

}

public enum PromotionUsageType: String {
    case physical = "PHYSICAL"
    case online = "ONLINE"
    case unknown

    var literal: String {

        switch self {
        case .physical:
            return Localizables.promotions.key_physical_commerce_text
        case .online:
            return Localizables.promotions.key_online_commerce_text
        default:
            return ""
        }
    }
}

enum PromotionDescriptionType {
    case summary
    case extended
    case unknown
}

class PromotionDescriptionBO: ModelBO {

    var id: PromotionDescriptionType!
    var name: String!
    var value: String!

    private static let SUMMARY = "SUMMARY"
    private static let EXTENDED = "EXTENDED"

    init(promotionDescriptionEntity: PromotionDescriptionEntity?) {

        name = promotionDescriptionEntity?.name
        value = promotionDescriptionEntity?.value

        switch promotionDescriptionEntity?.id {

            case PromotionDescriptionBO.SUMMARY?:
            id = .summary
            case PromotionDescriptionBO.EXTENDED?:
            id = .extended
            default:
            id = .unknown
        }

    }

}

class PromotionCardBO: ModelBO {

    var cardId: String?
    var numberType: ID_TypeBO!
    var cardTitle: CardTitleBO?
    
    init(promotionCardEntity: PromotionCardEntity?) {

        cardId = promotionCardEntity?.id
        numberType = ID_TypeBO(id_type: promotionCardEntity?.numberType)
        cardTitle = CardTitleBO(id_type: promotionCardEntity?.title)

    }
}

class UsageTypeBO: ModelBO {

    var usageId: PromotionUsageType!
    var name: String?

    private static let PHYSICAL = "PHYSICAL"
    private static let ONLINE = "ONLINE"

    init(promotionUsageIdType: ID_Type?) {

        name = promotionUsageIdType?.name

        switch promotionUsageIdType?.id {

            case UsageTypeBO.PHYSICAL?:
            usageId = .physical
            case UsageTypeBO.ONLINE?:
            usageId = .online
            default:
            usageId = .unknown
        }

    }
}

class LegalConditionsBO: ModelBO {

    var text: String!
    var url: String!

    init(legalConditionsEntity: LegalConditionsEntity) {
        text = legalConditionsEntity.text
        url = nil

        if let urlString = legalConditionsEntity.url {
            url = urlString
        }

    }
}

class DistanceBO: ModelBO {
    var length: Double?
    var lengthType: LengthTypeBO?

    init(distanceEntity: DistanceEntity?) {

        length = distanceEntity?.length

        if let lengthTypeEntity = distanceEntity?.lengthType {
            lengthType = LengthTypeBO(lengthTypeEntity: lengthTypeEntity)
        }
    }
}

class LengthTypeBO: ModelBO {
    var id: LengthType?
    var name: String?

    init(lengthTypeEntity: LengthTypeEntity) {

        switch lengthTypeEntity.id {

        case LengthType.kilometers.rawValue?:
            id = .kilometers
        case LengthType.metters.rawValue?:
            id = .metters
        default:
            id = .unknown
        }

        name = lengthTypeEntity.name
    }
}

class CardTitleBO: ModelBO {
    
    var id: String?
    var name: String?
    
    init(id_type: ID_Type?) {
        
        id = id_type?.id
        name = id_type?.name
    }
}

enum LengthType: String {

    case kilometers = "KILOMETERS"
    case metters = "METERS"
    case unknown
}

enum ContactType: String {

    case email = "EMAIL"
    case phone = "PHONE_NUMBER"
    case mobile = "MOBILE_NUMBER"
    case web = "WEBSITE"
    case unknown
}
