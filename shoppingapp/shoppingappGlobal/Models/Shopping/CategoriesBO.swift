//
//  CardsBO.swift
//  shoppingapp
//
//  Created by ruben.fernandez on 12/9/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit

class CategoriesBO: ModelBO {

    var status: Int
    var categories: [CategoryBO]

    init() {
        status = 0
        categories = [CategoryBO]()
    }

    init(categoriesEntity: CategoriesEntity) {

        status = categoriesEntity.status
        var categoriesBO = [CategoryBO]()

        if let categoriesEntity = categoriesEntity.data {

            for categoryEntity in categoriesEntity {
                categoriesBO.append(CategoryBO(categoryEntity: categoryEntity))
            }
        }

        categories = categoriesBO
    }

    func sortAlphabeticallyByName() {

        categories = categories.sorted(by: { categoryLeft, categoryRight -> Bool in
            return categoryLeft.name < categoryRight.name
        })
    }

    func favoriteCategoryTypes() -> [CategoryType] {
        var favoriteCategoryTypes = [CategoryType]()

        for category in categories where category.isFavourite {
            favoriteCategoryTypes.append(category.id)
        }

        return favoriteCategoryTypes
    }
}

class CategoryBO: ModelBO {

    var id: CategoryType
    var name: String
    var isFavourite: Bool
    var image: ImagesBO?

    init(categoryEntity: CategoryEntity) {

        if let catId = CategoryType(rawValue: categoryEntity.id) {
            id = catId
        } else {
            id = .unknown
        }

        name = categoryEntity.name
        isFavourite = categoryEntity.isFavourite
        image = ImagesBO(imageEntity: categoryEntity.image)

    }
}

enum CategoryType: String {
    
    case activities = "ACTIVITIES"
    case auto = "AUTO"
    case beautyAndGragances = "BEAUTY_AND_FRAGANCES"
    case dietAndNutrition = "DIET_AND_NUTRITION"
    case foodAndGourmet = "FOOD_AND_GOURMET"
    case hobbies = "HOBBIES"
    case home = "HOME"
    case learning = "LEARNING"
    case other = "OTHER"
    case rent = "RENT"
    case restaurants = "RESTAURANTS"
    case shopping = "SHOPPING"
    case shows = "SHOWS"
    case technology = "TECHNOLOGY"
    case travel = "TRAVEL"
    case bar = "BAR"
    case bookStore = "BOOK_STORE"
    case boutique = "BOUTIQUE"
    case doctor = "DOCTOR"
    case flowerShop = "FLOWER_SHOP"
    case fuelStation = "FUEL_STATION"
    case health = "HEALTH"
    case jewelryStore = "JEWELRY_STORE"
    case pets = "PETS"
    case pharmacy = "PHARMACY"
    case services = "SERVICES"
    case stationery = "STATIONERY"
    case supermarket = "SUPERMARKET"
    case toyStore = "TOY_STORE"
    case ecommerce = "ECOMMERCE"
    case unknown

    func getCategoryName(fromCategories categories: CategoriesBO) -> String? {
        
        for category in categories.categories where category.id == self {
            return category.name
        }

        return nil
    }
    
    func imageNamed() -> String {

        var imageNamed = ConstantsImages.Promotions.promotion

        switch self {

        case .activities:
            imageNamed = ConstantsImages.Promotions.activities
        case .auto:
            imageNamed = ConstantsImages.Promotions.auto
        case .beautyAndGragances:
            imageNamed = ConstantsImages.Promotions.beautyAndFragances
        case .dietAndNutrition:
            imageNamed = ConstantsImages.Promotions.diet
        case .foodAndGourmet:
            imageNamed = ConstantsImages.Promotions.foodAndGourmet
        case .hobbies:
            imageNamed = ConstantsImages.Promotions.hobbies
        case .home:
            imageNamed = ConstantsImages.Promotions.home
        case .learning:
            imageNamed = ConstantsImages.Promotions.learning
        case .rent:
            imageNamed = ConstantsImages.Promotions.rent
        case .restaurants:
            imageNamed = ConstantsImages.Promotions.restaurant
        case .shopping:
            imageNamed = ConstantsImages.Promotions.shopping
        case .shows:
            imageNamed = ConstantsImages.Promotions.shows
        case .technology:
            imageNamed = ConstantsImages.Promotions.technology
        case .travel:
            imageNamed = ConstantsImages.Promotions.travel
        case .bar:
            imageNamed = ConstantsImages.Promotions.bar
        case .bookStore:
            imageNamed = ConstantsImages.Promotions.bookStore
        case .boutique:
            imageNamed = ConstantsImages.Promotions.boutique
        case .doctor:
            imageNamed = ConstantsImages.Promotions.doctor
        case .flowerShop:
            imageNamed = ConstantsImages.Promotions.flowerShop
        case .fuelStation:
            imageNamed = ConstantsImages.Promotions.fuelStation
        case .health:
            imageNamed = ConstantsImages.Promotions.health
        case .jewelryStore:
            imageNamed = ConstantsImages.Promotions.jewelryStore
        case .pets:
            imageNamed = ConstantsImages.Promotions.pets
        case .pharmacy:
            imageNamed = ConstantsImages.Promotions.pharmacy
        case .services:
            imageNamed = ConstantsImages.Promotions.services
        case .stationery:
            imageNamed = ConstantsImages.Promotions.stationery
        case .supermarket:
            imageNamed = ConstantsImages.Promotions.supermarket
        case .toyStore:
            imageNamed = ConstantsImages.Promotions.toyStore
        case .ecommerce:
            imageNamed = ConstantsImages.Promotions.ecommerce
        case .other:
            imageNamed = ConstantsImages.Promotions.other
        default:
            imageNamed = ConstantsImages.Promotions.promotion
        }

        return imageNamed

    }

}

enum TrendingType: String {
    
    case suggested = "SUGGESTED"
    case hot = "HOT"
}
