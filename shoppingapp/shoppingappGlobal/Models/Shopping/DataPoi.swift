//
//  DataPoi.swift
//  shoppingapp
//
//  Created by jesus.martinez on 6/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class DataPoi {

    var storeIds = [String]()
    let latitude: Double
    let longitude: Double
    var promotions = [PromotionBO]()

    init(latitude: Double, longitude: Double, storeId: String) {
        self.latitude = latitude
        self.longitude = longitude
        storeIds.append(storeId)
    }

    func append(storeId: String) {

        if !storeIds.contains(storeId) {
            storeIds.append(storeId)
        }

    }

    func append(promotion: PromotionBO) {

        let containsPromotion = promotions.contains { promotionSearch -> Bool in
            promotionSearch.id == promotion.id
        }

        if !containsPromotion {
            promotions.append(promotion)
        }

    }

    func categoriesEquals() -> [CategoryType]? {

        var categoriesTypes = [[CategoryType]]()

        for promotion in promotions {

            if let categories = promotion.categories {

                let categoryTypes = categories.map({ category -> CategoryType in
                    category.id
                })

                categoriesTypes.append(categoryTypes)

            } else {
                return nil
            }

        }

        var categoriesEquals = Set(categoriesTypes[0])

        for i in 1..<categoriesTypes.count {

            categoriesEquals = categoriesEquals.intersection(Set(categoriesTypes[i]))

        }

        return Array(categoriesEquals)

    }

    func hasStoresEqual() -> Bool {

        var storesIds = [[String]]()

        for promotion in promotions {

            if let stores = promotion.stores {

                let storeIds = stores.map({ store -> String in
                    store.id
                })

                storesIds.append(storeIds)

            } else {
                return false
            }

        }

        for i in 0..<storesIds.count - 1 {

            if !storesAreEquivalent(storesA: storesIds[i], storesB: storesIds[i + 1]) {
                return false
            }

        }

        return true

    }

    private func storesAreEquivalent(storesA: [String], storesB: [String]) -> Bool {
        return storesA.sorted() == storesB.sorted()
    }

}
