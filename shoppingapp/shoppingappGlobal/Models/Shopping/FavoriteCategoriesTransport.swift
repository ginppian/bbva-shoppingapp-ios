//
//  FavoriteCategoriesTransport.swift
//  shoppingappMX
//
//  Created by AZIZEBULBUL on 29/06/2018.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class FavoriteCategoriesTransport: ModelTransport {

    let categories: CategoriesBO

    init(categories: CategoriesBO) {

        self.categories = categories
    }
}
