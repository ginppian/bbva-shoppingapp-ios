//
//  ShoppingParamsTransport.swift
//  shoppingapp
//
//  Created by jesus.martinez on 30/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol QueryParametizable {
    func queryParams() -> String?
}

enum OrderBy: String {
    case endDate
}

struct ShoppingParamsTransport: QueryParametizable, Equatable {

    let categoryId: String?
    let trendingId: String?
    let location: ShoppingParamLocation?
    let pageSize: Int?
    let orderBy: OrderBy?
    let titleIds: [String]?
    
    init(categoryId: String? = nil, trendingId: String? = nil, location: ShoppingParamLocation? = nil, titleIds: [String]? = nil, pageSize: Int? = nil, orderBy: OrderBy? = nil) {
        self.categoryId = categoryId
        self.trendingId = trendingId
        self.location = location
        self.pageSize = pageSize
        self.orderBy = orderBy
        self.titleIds = titleIds
    }

    func queryParams() -> String? {

        var queryParams: String?
        
        if let locationParams = location?.queryParams() {
            let expand = ShoppingExpands(stores: true)
            queryParams = expand.expands() + "&" + locationParams
        }
        
        if let categoryId = categoryId {

            queryParams = appendParam(currentQueryParams: queryParams, newQueryParam: "categories.id=\(categoryId)")
        }

        if let trendingId = trendingId {

            queryParams = appendParam(currentQueryParams: queryParams, newQueryParam: "trending.id=\(trendingId)")
        }

        if let titleIds = titleIds, !titleIds.isEmpty {

            let titleIdsFormatted = titleIds.uniques().joined(separator: ",")

            queryParams = appendParam(currentQueryParams: queryParams, newQueryParam: "title.id=in=(\(titleIdsFormatted))")
        }

        if let pageSize = pageSize {

            queryParams = appendParam(currentQueryParams: queryParams, newQueryParam: "pageSize=\(pageSize)")
        }

        if let orderBy = orderBy {

            queryParams = appendParam(currentQueryParams: queryParams, newQueryParam: "orderBy=\(orderBy.rawValue)")
        }

        return queryParams
    }
    
    private func appendParam(currentQueryParams: String?, newQueryParam: String) -> String {

        guard let currentQueryParams = currentQueryParams else {
            return newQueryParam
        }

        return currentQueryParams + "&" + newQueryParam
    }
}

struct ShoppingParamLocation: QueryParametizable, Equatable {

    let geolocation: GeoLocationBO
    let distanceParam: ShoppingDistanceParam?

    init(geolocation: GeoLocationBO, distanceParam: ShoppingDistanceParam? = nil) {
        self.geolocation = geolocation
        self.distanceParam = distanceParam
    }

    func queryParams() -> String? {

        var geolocationParams = "stores.location.geolocation.latitude=\(geolocation.latitude)&stores.location.geolocation.longitude=\(geolocation.longitude)"

        if let distanceQueryParams = distanceParam?.queryParams() {
            geolocationParams += "&" + distanceQueryParams
        }

        return geolocationParams
    }
}

struct ShoppingDistanceParam: QueryParametizable, Equatable {

    let distance: Int
    let lengthType: String

    func queryParams() -> String? {

        return "stores.distance.length=\(distance)&stores.distance.lengthType.id=\(lengthType)"
    }
}

struct ShoppingExpands: Equatable {
    
    let stores: Bool
    let cards: Bool
    let contactDetails: Bool
    
    init(stores: Bool = false, cards: Bool = false, contactDetails: Bool = false) {
        self.stores = stores
        self.cards = cards
        self.contactDetails = contactDetails
    }
    
    func expands() -> String {
        
        var expands = "expand="
        
        if stores {
            
            expands = appendExpand(currentQueryExtend: expands, newQueryExtend: "stores")
        }
        
        if cards {
            
            expands = appendExpand(currentQueryExtend: expands, newQueryExtend: "cards")
        }
        
        if contactDetails {
            
            expands = appendExpand(currentQueryExtend: expands, newQueryExtend: "contact-details")
        }
        
        return expands
    }
    
    private func appendExpand(currentQueryExtend: String?, newQueryExtend: String) -> String {
        
        guard let currentQueryParams = currentQueryExtend, currentQueryParams != "expand=" else {
            return "expand=" + newQueryExtend
        }
        
        return currentQueryParams + "," + newQueryExtend
    }
}

struct ShoppingDetailParamsTransport: QueryParametizable, Equatable {
    
    let idPromo: String
    let expands: ShoppingExpands?
    let location: ShoppingParamLocation?
    let titleIds: [String]?
    
    init(idPromo: String, location: ShoppingParamLocation? = nil, expands: ShoppingExpands? = nil, titleIds: [String]? = nil) {
        self.idPromo = idPromo
        self.expands = expands
        self.location = location
        self.titleIds = titleIds
    }
    
    func queryParams() -> String? {
        
        guard var queryParams = expands?.expands() else {
            return nil
        }
        
        if let locationParam = location?.queryParams() {
            queryParams += "&" + locationParam
        }
        
        if let titleIds = titleIds, !titleIds.isEmpty {
            
            let titleIdsFormatted = titleIds.uniques().joined(separator: ",")
            
            queryParams = appendParam(currentQueryParams: queryParams, newQueryParam: "title.id=in=(\(titleIdsFormatted))")
        }
        
        return queryParams
    }
    
    private func appendParam(currentQueryParams: String?, newQueryParam: String) -> String {
        
        guard let currentQueryParams = currentQueryParams else {
            return newQueryParam
        }
        
        return currentQueryParams + "&" + newQueryParam
    }
}
