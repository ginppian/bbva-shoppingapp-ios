//
//  PromotionEntity.swift
//  shoppingapp
//
//  Created by ruben.fernandez on 7/11/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation
import HandyJSON

class PromotionsEntity: ModelEntity, HandyJSON {
    var status: Int! = 0
    var data: [PromotionEntity]?
    var pagination: PaginationEntity?

    required init() {}
}

struct PromotionEntity: HandyJSON {
    var id: String!
    var name: String!
    var startDate: String!
    var endDate: String!
    var benefit: String?
    var commerceInformation: CommerceInformationEntity!
    var images: [ImagesEntity]!
    var stores: [StoresEntity]?
    var promotionType: PromotionTypeEntity!
    var promotionUrl: String?
    var categories: [CategoryEntity]?
    var descriptions: [PromotionDescriptionEntity]?
    var legalConditions: LegalConditionsEntity?
    var cards: [PromotionCardEntity]?
    var usageTypes: [ID_Type]?
    var contactDetails: [ContactDetailEntity]?
}

class PromotionDetailEntity: ModelEntity, HandyJSON {
    var data: PromotionEntity?

    required init() {}
}

struct CommerceInformationEntity: HandyJSON {
    var name: String!
    var description: String!
    var web: String!
    var logo: ImagesEntity?
}

struct StoresEntity: HandyJSON {
    var id: String!
    var name: String!
    var location: LocationEntity?
    var distance: DistanceEntity?
    var contactDetails: [ContactDetailEntity]?

}

struct ContactDetailEntity: HandyJSON {

    var contactType: ContactTypeEntity?
    var contact: String?
    var description: String?
}

struct ContactTypeEntity: HandyJSON {

    var id: String?
    var name: String?
}

struct LocationEntity: HandyJSON {
    var addressName: String!
    var city: String!
    var state: String!
    var zipCode: String!
    var country: CountryEntity?
    var geolocation: GeoLocationEntity?
    var additionalInformation: String?

}

struct CountryEntity: HandyJSON {
    var id: String!
    var name: String!

}

class GeoLocationEntity: ModelEntity, HandyJSON {
    var latitude: Double!
    var longitude: Double!

    required init() {}

}

struct PromotionTypeEntity: HandyJSON {
    var id: String!
    var name: String!
}

struct PromotionDescriptionEntity: HandyJSON {
    var id: String!
    var name: String!
    var value: String!
}

struct LegalConditionsEntity: HandyJSON {
    var text: String?
    var url: String?
}

struct PromotionCardEntity: HandyJSON {
    var id: String!
    var numberType: ID_Type!
    var cardType: ID_Type!
    var title: ID_Type!
}

struct DistanceEntity: HandyJSON {
    var length: Double?
    var lengthType: LengthTypeEntity?

}

struct LengthTypeEntity: HandyJSON {
    var id: String?
    var name: String?

}
