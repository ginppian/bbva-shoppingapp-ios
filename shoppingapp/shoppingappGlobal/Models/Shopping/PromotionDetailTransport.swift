//
//  PromotionDetailTransport.swift
//  shoppingapp
//
//  Created by Luis Monroy on 08/01/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class PromotionDetailTransport: ModelTransport {

    var promotionBO: PromotionBO?
    var category: CategoryBO?
    var trendingType: TrendingType?
    var distanceAndDateToShow: String?
    var completion: (() -> Void)?
    var userLocation: GeoLocationBO?
    var shownOnMap: Bool = false
}
