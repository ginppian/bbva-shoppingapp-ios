//
//  ModelsBO.swift
//  shoppingapp
//
//  Created by ruben.fernandez on 9/6/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation
import HandyJSON

class CategoriesEntity: ModelEntity, Codable {
    var status: Int! = 0
    var data: [CategoryEntity]?
}

struct CategoryEntity: HandyJSON, Codable {

    var id: String!
    var name: String!
    var isFavourite: Bool = false
    var image: ImagesEntity?

    init(id: String, name: String, isFavourite: Bool? = nil, image: ImagesEntity? = nil) {
        self.id = id
        self.name = name
        self.isFavourite = isFavourite ?? false
        self.image = image
    }

    init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decode(String.self, forKey: .id)
        name = try values.decode(String.self, forKey: .name)
        isFavourite = (try? values.decode(Bool.self, forKey: .isFavourite)) ?? false
        image = try? values.decode(ImagesEntity.self, forKey: .image)
    }

    init() {
    }
}
