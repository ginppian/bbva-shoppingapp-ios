//
//  BBVAImageExtension.swift
//  shoppingapp
//
//  Created by jesus.martinez on 7/8/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

extension UIImage {

    open func tint(with color: UIColor?) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(size, false, UIScreen.main.scale)
        guard let color = color, let context = UIGraphicsGetCurrentContext() else {
            return nil
        }

        context.scaleBy(x: 1.0, y: -1.0)
        context.translateBy(x: 0.0, y: -size.height)

        context.setBlendMode(.multiply)

        let rect = CGRect(x: 0, y: 0, width: size.width, height: size.height)
        context.clip(to: rect, mask: cgImage!)
        color.setFill()
        context.fill(rect)

        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image?.withRenderingMode(.alwaysOriginal)
    }
}
