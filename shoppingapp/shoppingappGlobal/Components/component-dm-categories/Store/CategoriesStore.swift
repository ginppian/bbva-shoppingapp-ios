//
//  CategoriesStore.swift
//  shoppingapp
//
//  Created by jesus.martinez on 3/7/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import CellsNativeComponents
import BBVA_Network

public class CategoriesStore: NetworkStore {

    public static let ID: String = "component-data-manager-categories-store"

    public static let ON_SELECT_FAVOURITE_CATEGORIES_SUCCESS = ActionSpec<FavouriteCategoriesDTO>(id: "on-select_favourite_categories-success")
    static let ON_SELECT_FAVOURITE_CATEGORIES_ERROR = ActionSpec<ErrorBO>(id: "on-select_favourite_categories-error")

    var host: String?
    var countryCode: String?

    fileprivate var selectFavouriteCategoriesUseCase: SelectFavouriteCategoriesUseCase?
    fileprivate var worker: NetworkWorker

    public init(worker: NetworkWorker, host: String? = nil) {

        self.worker = worker
        self.host = host

        super.init(CategoriesStore.ID)

        guard host == nil || (host?.isEmpty)! else {
            return
        }

        configureStoreWithProperties()
    }

    public required init() {
        fatalError("init() has not been implemented")
    }

    fileprivate func configureStoreWithProperties() {

        newProperty(key: "host") { [weak self] (value: Any?) in

            if let value = value {
                self?.host = String(describing: value)
            }
        }
        newProperty(key: "country") { [weak self] (value: Any?) in

            if let value = value {
                self?.countryCode = String(describing: value)
            }
        }
    }

    public func selectFavouriteCategories(_ favouriteCategoriesDTO: FavouriteCategoriesDTO) {

        let useCaseCallback = UseCase<FavouriteCategoriesDTO, FavouriteCategoriesDTO>.UseCaseCallback<FavouriteCategoriesDTO>(onUseCaseComplete: { result in
            self.fire(actionSpec: CategoriesStore.ON_SELECT_FAVOURITE_CATEGORIES_SUCCESS, value: result)
        }, onUseCaseError: self.onUseCaseError)

        self.selectFavouriteCategoriesUseCase?.execute(params: favouriteCategoriesDTO, useCaseCallback: useCaseCallback)
    }

    override public func onPropertiesBounds() {

        guard let host = host else {
            return
        }

        let categoriesRepository: CategoriesRepository = CategoriesRepositoryImpl(baseURL: host, networkWorker: worker)
        self.selectFavouriteCategoriesUseCase = SelectFavouriteCategoriesUseCase(categoriesRepository: categoriesRepository)
    }

    override public func dispose() {
        selectFavouriteCategoriesUseCase?.dispose()
        super.dispose()
    }

    override public func onUseCaseError(error: Error) {
        print("onUseCaseError: \(error)")

        guard let evaluate = error as? ServiceError else {
            return
        }

        switch evaluate {
        case .GenericErrorEntity(let errorEntity):
            let errorBVABO = ErrorBO(error: errorEntity)
            self.fire(actionSpec: CategoriesStore.ON_SELECT_FAVOURITE_CATEGORIES_ERROR, value: errorBVABO)
        default:
            break
        }
    }

}
