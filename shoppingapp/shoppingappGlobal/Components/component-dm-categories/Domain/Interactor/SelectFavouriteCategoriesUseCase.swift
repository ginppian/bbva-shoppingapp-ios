//
//  SelectFavouriteCategoriesUseCase.swift
//  shoppingapp
//
//  Created by jesus.martinez on 3/7/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import CellsNativeComponents

class SelectFavouriteCategoriesUseCase: UseCaseAsync<FavouriteCategoriesDTO, FavouriteCategoriesDTO> {

    private let categoriesRepository: CategoriesRepository

    init(categoriesRepository: CategoriesRepository) {

        self.categoriesRepository = categoriesRepository
        super.init()
    }

    override func call(params: FavouriteCategoriesDTO?, success: @escaping (FavouriteCategoriesDTO) -> Void, error: @escaping (Error) -> Void) {

        guard let params = params else {
            error(ServiceError.GenericErrorEntity(error: ErrorEntity(message: "Error no params")))
            return
        }

        self.categoriesRepository.select(favouriteCategories: params, success: success, error: error)
    }

}
