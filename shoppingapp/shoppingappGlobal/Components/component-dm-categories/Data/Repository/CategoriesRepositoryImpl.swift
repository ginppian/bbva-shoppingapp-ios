//
//  CategoriesRepositoryImpl.swift
//  shoppingappMX
//
//  Created by jesus.martinez on 3/7/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

import ObjectMapper
import BBVA_Network
import HandyJSON

class CategoriesRepositoryImpl: BaseRepositoryImpl, CategoriesRepository {

    fileprivate static let CATEGORIES_URL_PREFIX = "/categories"

    func select(favouriteCategories: FavouriteCategoriesDTO, success: @escaping (_ result: FavouriteCategoriesDTO) -> Void, error: @escaping (_ error: Error) -> Void) {

        let url = baseURL + CategoriesRepositoryImpl.CATEGORIES_URL_PREFIX

        let bodyParser = favouriteCategories.data.toJSON()

        let request: NetworkRequest = createRequest(url, Headers.BASIC_HEADER, bodyParser, .useProtocolCachePolicy, 30, NetworkMethod.PATCH)

        _ = networkWorker.execute(request: request) { (response: Response) in

            if response.error == nil {

                if  let dataJson = response.body {

                    if let favouriteCategoriesDTO = FavouriteCategoriesDTO.deserialize(from: dataJson) {
                        success(favouriteCategoriesDTO)
                    } else {
                        error(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
                    }

                } else {
                    error(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
                }

            } else {

                if let serviceError = self.checkShowError(response: response, security: false) {
                    error(serviceError)
                }
            }

        }
    }
}
