//
//  CategoriesRepository.swift
//  shoppingappMX
//
//  Created by jesus.martinez on 3/7/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

public protocol CategoriesRepository {

    func select(favouriteCategories: FavouriteCategoriesDTO, success: @escaping (_ result: FavouriteCategoriesDTO) -> Void, error: @escaping (_ error: Error) -> Void)

}
