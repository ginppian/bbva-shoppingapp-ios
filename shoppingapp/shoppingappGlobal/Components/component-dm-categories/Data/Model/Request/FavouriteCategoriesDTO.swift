//
//  FavouriteCategoriesDTO.swift
//  shoppingapp
//
//  Created by jesus.martinez on 3/7/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import HandyJSON

public class FavouriteCategoriesDTO: CellsDTO, HandyJSON {

    let data: [FavouriteCategoryDTO]

    init(favouriteCategories: [FavouriteCategoryDTO]) {

        data = favouriteCategories
    }

    public required init() {
        data = [FavouriteCategoryDTO]()
    }
}

extension FavouriteCategoriesDTO: Equatable {
    public static func == (lhs: FavouriteCategoriesDTO, rhs: FavouriteCategoriesDTO) -> Bool {

        return lhs.data == rhs.data

    }
}

public class FavouriteCategoryDTO: HandyJSON {

    let id: String
    let isFavourite: Bool

    init(_ id: String, isFavourite: Bool) {
        self.id = id
        self.isFavourite = isFavourite
    }

    public required init() {
        id = ""
        isFavourite = false
    }
}

extension FavouriteCategoryDTO: Equatable {
    public static func == (lhs: FavouriteCategoryDTO, rhs: FavouriteCategoryDTO) -> Bool {

        return lhs.id == rhs.id && lhs.isFavourite == rhs.isFavourite

    }
}
