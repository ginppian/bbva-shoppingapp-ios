//
//  DowngradeRepositoryImpl.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 13/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import BBVA_Network

class DowngradeRepositoryImpl: BaseRepositoryImpl, DowngradeRepository {
    
    fileprivate static let downgradeUrl = "/downgrade-keys"
    
    func downgrade(params: DowngradeDTO, success: @escaping (Bool) -> Void, error: @escaping (Error) -> Void) {
        
        let url = baseURL + DowngradeRepositoryImpl.downgradeUrl
        
        let body = DowngradeBody(cardId: params.cardId, resetIndicator: params.resetIndicator)
        
        guard let bodyParser = body.toJSON() else {
            return
        }
        
        let request: NetworkRequest = createRequest(url, Headers.BASIC_HEADER, bodyParser, .useProtocolCachePolicy, 30, NetworkMethod.POST)
        
        _ = networkWorker.execute(request: request) { (response: Response) in
            
            if response.error == nil {
                success(true)
            } else {
                
                if let serviceError = self.checkShowError(response: response, security: false) {
                    error(serviceError)
                }
            }
        }
    }
}
