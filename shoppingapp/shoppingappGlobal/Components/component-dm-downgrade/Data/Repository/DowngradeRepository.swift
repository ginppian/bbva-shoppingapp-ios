//
//  DowngradeRepository.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 13/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

public protocol DowngradeRepository {
    
    func downgrade(params: DowngradeDTO, success: @escaping (_ result: Bool) -> Void, error: @escaping (_ error: Error) -> Void)
    
}
