//
//  DowngradeDTO.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 13/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import HandyJSON

open class DowngradeDTO: CellsDTO {
    
    var cardId: String
    var resetIndicator: Bool
    
    init(cardId: String, resetIndicator: Bool) {
        
        self.cardId = cardId
        self.resetIndicator = resetIndicator
    }
}

public class DowngradeBody: HandyJSON {
    
    let cardId: String
    let resetIndicator: Bool
    
    init(cardId: String, resetIndicator: Bool) {
        
        self.cardId = cardId
        self.resetIndicator = resetIndicator
    }
    
    public required init() {
        cardId = ""
        resetIndicator = false
    }
}
