//
//  DowngradeUseCase.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 13/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import CellsNativeComponents

class DowngradeUseCase: UseCaseAsync<Bool, DowngradeDTO> {
    
    private let downgradeRepository: DowngradeRepository
    
    init(downgradeRepository: DowngradeRepository) {
        
        self.downgradeRepository = downgradeRepository
        super.init()
    }
    
    override func call(params: DowngradeDTO?, success: @escaping (_ result: Bool) -> Void, error: @escaping (_ error: Error) -> Void) {
        
        if let params = params {
            self.downgradeRepository.downgrade(params: params, success: success, error: error)
        } else {
            DLog(message: "Error no params")
            error(ServiceError.GenericErrorEntity(error: ErrorEntity(message: "Error no params")))
        }
    }
}
