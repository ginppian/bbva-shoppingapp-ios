//
//  DowngradeUseCaseTest.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 16/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest
import BBVA_Network

#if TESTMX
@testable import shoppingappMX
#elseif TESTPE
@testable import shoppingappPE
#endif

class DowngradeUseCaseTest: XCTestCase {
    
    var sut: DowngradeUseCase!
    var mockDowngradeRepository = MockDowngradeRepository()
    
    override func setUp() {
        super.setUp()
        sut = DowngradeUseCase(downgradeRepository: mockDowngradeRepository)
    }
    
    func test_givenAResponse_whenCallDowngradeUseCase_thenIsCalledGrantingTicket() {
        
        //given
        let response = Response()
        response.body = "{}"
        
        let downgradeDTO = DowngradeDTO(cardId: "cardId", resetIndicator: true)
        
        //when
        sut.call(params: downgradeDTO, success: { _ in
            
            //then
            XCTAssertTrue(self.mockDowngradeRepository.isCalledDowngrade)
            
        }, error: { _ in
            
            XCTFail("Not expected error")
        })
    }
    
    func test_givenAError_whenCallDowngradeUseCaseWithWrongParameters_thenIsCompletionError() {
        
        //given
        let response = Response()
        response.body = "{}"
        
        //when
        sut.call(params: nil, success: { _ in
            
            XCTFail("Not expected success")
            
        }, error: { wrongParameters in
            
            //then
            XCTAssert((wrongParameters as? ServiceError) != nil)
        })
    }
}
