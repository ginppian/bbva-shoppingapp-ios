//
//  DowngradeUtilMock.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 16/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import BBVA_Network
import CellsNativeComponents

#if TESTMX
@testable import shoppingappMX
#elseif TESTPE
@testable import shoppingappPE
#endif

class MockDowngradeNetworkWorker: NetworkWorker {
    
    var response: Response?
    var isError = false
    var isBadBody = false
    
    override func execute(request: NetworkRequest, completion: @escaping (Response) -> Void) -> URLSessionDataTask? {
        
        if isError == true {
            response = Response()
            response?.error = ServiceError.Other(NSError())
            completion(response!)
        } else if isBadBody == true {
            response = Response()
            response?.body = "invalida_Json."
            completion(response!)
        } else {
            response = Response()
            response?.body = "{\"data\":{\"token\":\"12345\"}}"
            completion(response!)
        }
        return URLSessionDataTask()
    }
}

class MockDowngradeRepository: DowngradeRepository {
    
    var isCalledDowngrade = false
    
    func downgrade(params: DowngradeDTO, success: @escaping (Bool) -> Void, error: @escaping (Error) -> Void) {
        
        isCalledDowngrade = true
    }
}

class MockDowngradeUseCase: DowngradeUseCase {
    
    var requestSuccess = false
    var isCallExecute = false
    
    var downgradeDTO = DowngradeDTO(cardId: "cardId", resetIndicator: true)
    
    override func call(params: DowngradeDTO?, success: @escaping (Bool) -> Void, error: @escaping (Error) -> Void) {
        
        if requestSuccess == true {
            success(true)
        } else {
            error(ServiceError.Other(NSError()))
        }
    }
    
    override func execute(params: DowngradeDTO?, useCaseCallback: UseCase<Bool, DowngradeDTO>.UseCaseCallback<Bool>) {
        
        isCallExecute = true
        downgradeDTO = params!
        call(params: params, success: useCaseCallback.onUseCaseComplete!, error: useCaseCallback.onUseCaseError!)
    }
}
