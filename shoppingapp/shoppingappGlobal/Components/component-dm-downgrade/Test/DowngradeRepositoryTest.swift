//
//  DowngradeRepositoryTest.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 16/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
@testable import shoppingappMX
#elseif TESTPE
@testable import shoppingappPE
#endif

class DowngradeRepositoryTest: XCTestCase {
    
    var sut: DowngradeRepository!
    var networkWorkerMock = MockDowngradeNetworkWorker()
    
    override func setUp() {
        super.setUp()
        networkWorkerMock = MockDowngradeNetworkWorker()
        sut = DowngradeRepositoryImpl(baseURL: "", networkWorker: networkWorkerMock)
    }
    
    func test_givenAErrorInService_whenCallDowngrade_thenCompletionError() {
        
        let downgradeDTO = DowngradeDTO(cardId: "cardId", resetIndicator: true)
        networkWorkerMock.isError = true
        
        sut.downgrade(params: downgradeDTO, success: { _ in
            
            XCTFail("Not expected success")
            
        }, error: { error in
            
            XCTAssert((error as? ServiceError) != nil)
            
        })
    }
    
    func test_givenASuccessResponse_whenCallDowngrade_thenCompletionSuccess() {
        
        let downgradeDTO = DowngradeDTO(cardId: "cardId", resetIndicator: true)
        
        sut.downgrade(params: downgradeDTO, success: { response in
            
            XCTAssert(response == true)
            
        }, error: { _ in
            
            XCTFail("Not expected fail")
        })
    }
}
