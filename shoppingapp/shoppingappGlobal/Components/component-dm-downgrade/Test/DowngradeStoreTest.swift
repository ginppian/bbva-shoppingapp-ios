//
//  DowngradeStoreTest.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 16/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
@testable import shoppingappMX
#elseif TESTPE
@testable import shoppingappPE
#endif

class DowngradeStoreTest: XCTestCase {
    
    private var networkWorkerMock = MockDowngradeNetworkWorker()
    private var mockDowngradeUseCase: MockDowngradeUseCase!
    private var sut: DowngradeStore!
    
    override func setUp() {
        super.setUp()
        networkWorkerMock = MockDowngradeNetworkWorker()
        mockDowngradeUseCase = MockDowngradeUseCase(downgradeRepository: MockDowngradeRepository())
        
        sut = DowngradeStore(worker: networkWorkerMock, downgradeUseCase: mockDowngradeUseCase)
    }
    
    func test_givenADowngradeDTO_whenCallDowngrade_thenIsCalledExecute() {
        
        //given
        let downgradeDTO = DowngradeDTO(cardId: "cardId", resetIndicator: true)
        
        //when
        sut.downgrade(downgradeDTO: downgradeDTO)
        
        //then
        XCTAssertTrue(mockDowngradeUseCase.isCallExecute)
    }
    
    func test_givenADowngradeDTO_whenCallDowngrade_thenDowngradeDTOSameInBlockCardUse() {
        
        //given
        let downgradeDTO = DowngradeDTO(cardId: "cardId", resetIndicator: true)
        
        //when
        sut.downgrade(downgradeDTO: downgradeDTO)
        
        //then
        XCTAssertTrue(mockDowngradeUseCase.downgradeDTO === downgradeDTO)
    }
}
