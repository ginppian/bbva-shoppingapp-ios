//
//  DowngradeStore.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 13/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeComponents
import BBVA_Network

open class DowngradeStore: NetworkStore {
    
    public static let id: String = "component-data-manager-downgrade-store"
    
    public static let onDowngradeSuccess = ActionSpec<Bool>(id: "on-downgrade-success")
    static let onDowngradeError = ActionSpec<ErrorBO>(id: "on-downgrade-error")
    
    var host: String?
    var countryCode: String?
    
    fileprivate var downgradeUseCase: DowngradeUseCase?
    fileprivate var worker: NetworkWorker
    
    convenience init(worker: NetworkWorker, downgradeUseCase: DowngradeUseCase) {
        
        self.init(worker: worker)
        self.downgradeUseCase = downgradeUseCase
    }
    
    public init(worker: NetworkWorker, host: String? = nil) {
        
        self.worker = worker
        self.host = host
        
        super.init(DowngradeStore.id)
        
        guard host == nil || host!.isEmpty else {
            return
        }
        
        configureStoreWithProperties()
    }
    
    public required init() {
        fatalError("init() has not been implemented")
    }
    
    fileprivate func configureStoreWithProperties() {
        
        newProperty(key: "host") { [weak self] (value: Any?) in
            
            if let value = value {
                self?.host = String(describing: value)
            }
        }
        newProperty(key: "country") { [weak self] (value: Any?) in
            
            if let value = value {
                self?.countryCode = String(describing: value)
            }
        }
    }
    
    override open func onPropertiesBounds() {
        
        guard let host = host else {
            return
        }
        
        let downgradeRepository: DowngradeRepository = DowngradeRepositoryImpl(baseURL: host, networkWorker: worker)
        downgradeUseCase = DowngradeUseCase(downgradeRepository: downgradeRepository)
    }
    
    override open func dispose() {
        downgradeUseCase?.dispose()
        super.dispose()
    }
    
    override public func onUseCaseError(error: Error) {
        
        guard let evaluate = error as? ServiceError else {
            return
        }
        
        switch evaluate {
        case .GenericErrorEntity(let errorEntity):
            let errorBVABO = ErrorBO(error: errorEntity)
            self.fire(actionSpec: DowngradeStore.onDowngradeError, value: errorBVABO)
        default:
            break
        }
    }
    
    // MARK: External funcs
    
    public func downgrade(downgradeDTO: DowngradeDTO) {
        
        let useCaseCallback = UseCase<Bool, DowngradeDTO>.UseCaseCallback<Bool>(onUseCaseComplete: { result in
            self.fire(actionSpec: DowngradeStore.onDowngradeSuccess, value: result)
        }, onUseCaseError: self.onUseCaseError)
        
        downgradeUseCase?.execute(params: downgradeDTO, useCaseCallback: useCaseCallback)
    }
}
