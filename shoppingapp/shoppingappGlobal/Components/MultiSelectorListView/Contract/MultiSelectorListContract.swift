//
//  MultiSelectorListContract.swift
//  shoppingapp
//
//  Created by jesus.martinez on 26/2/18.
//  Copyright © 2017 BBVA. All rights reserved.
//

import RxSwift

protocol MultiSelectorListViewProtocol: ComponentViewProtocol {

    func showTitle(withText text: String)
    func showOptions(withDisplayData displayData: [OptionItemDisplayData])
    func reloadStatus(withDisplayData displayData: [OptionItemDisplayData])

}

protocol MultiSelectorListPresenterProtocol: ComponentPresenterProtocol {

    func onConfigureView()
    func showMoreInfoPressed()

}
