//
//  MultiSelectorListPresenter.swift
//  shoppingapp
//
//  Created by jesus.martinez on 26/2/18.
//  Copyright © 2017 BBVA. All rights reserved.
//

import RxSwift

class MultiSelectorListPresenter {

    weak var view: MultiSelectorListViewProtocol?

    var optionsShowed = false

    var displayData: MultiSelectorListDisplayData? {
        didSet {
            onConfigureView()
        }
    }

}

extension MultiSelectorListPresenter: MultiSelectorListPresenterProtocol {

    func onConfigureView() {

        if let displayData = displayData {
            view?.showTitle(withText: displayData.title)

            if optionsShowed {
                view?.reloadStatus(withDisplayData: displayData.options)
            } else {
                let optionsSelected = displayData.options.filter { $0.isSelected }

                if !optionsSelected.isEmpty {
                    view?.showOptions(withDisplayData: displayData.options)
                }
            }

        }

    }

    func showMoreInfoPressed() {

        if let displayData = displayData {
            view?.showOptions(withDisplayData: displayData.options)
        }

        optionsShowed = true
    }

}
