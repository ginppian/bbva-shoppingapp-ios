//
//  MultiSelectorListDisplayData.swift
//  shoppingapp
//
//  Created by jesus.martinez on 26/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit

class MultiSelectorListDisplayData {

    var title: String
    var options: [OptionItemDisplayData]

    init(withTitle title: String) {

        self.title = title
        options = [OptionItemDisplayData]()

    }

}

extension MultiSelectorListDisplayData {

    convenience init(withTitle title: String, andCategoriesBO categoriesBO: CategoriesBO, andCategoriesSelected categoriesSelected: [CategoryType]? = nil) {

        self.init(withTitle: title)

        for category in categoriesBO.categories {

            var imageName = ConstantsImages.Promotions.promotion

            imageName = category.id.imageNamed()

            let item = OptionItemDisplayData(imageName: imageName, title: category.name, isSelected: categoriesSelected?.contains(category.id) ?? false)
            options.append(item)

        }

    }

}

struct OptionItemDisplayData {

    let imageName: String?
    let title: String
    let accesoryImageName: String?
    var isSelected = false

    init(imageName: String, title: String, isSelected: Bool = false) {
        self.imageName = imageName
        self.title = title
        self.isSelected = isSelected
        self.accesoryImageName = nil
    }

}

extension OptionItemDisplayData: Equatable {

    public static func == (lhs: OptionItemDisplayData, rhs: OptionItemDisplayData) -> Bool {

        return lhs.imageName == rhs.imageName && lhs.title == rhs.title && lhs.accesoryImageName == rhs.accesoryImageName

    }

}
