//
//  MultiSelectorListView.swift
//  shoppingapp
//
//  Created by jesus.martinez on 26/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit

protocol MultiSelectorListViewDelegate: class {

    func multiSelectorListViewShowMoreInfo(_ multiSelectorListView: MultiSelectorListView, requiresHeight height: CGFloat)
    func multiSelectorListView(_ multiSelectorListView: MultiSelectorListView, selectedAtIndex index: Int)
    func multiSelectorListView(_ multiSelectorListView: MultiSelectorListView, deSelectedAtIndex index: Int)

}

class MultiSelectorListView: XibView {

    let presenter = MultiSelectorListPresenter()

    @IBOutlet weak var moreInfoContainerView: UIView!
    @IBOutlet weak var moreInfoButton: UIButton!

    @IBOutlet weak var optionsContainerView: UIView!
    @IBOutlet weak var optionsTableView: UITableView!

    weak var delegate: MultiSelectorListViewDelegate?

    var options: [OptionItemDisplayData]?

    override func configureView() {

        super.configureView()

        configureMoreInfoContainer()
        configureTableView()

        presenter.view = self
        presenter.onConfigureView()
    }

    private func configureMoreInfoContainer() {

        moreInfoContainerView.backgroundColor = .BBVAWHITE

        moreInfoButton.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Common.add_icon, color: .MEDIUMBLUE), width: 16, height: 16), for: .normal)

        moreInfoButton.titleLabel?.font = Tools.setFontBold(size: 14)
        moreInfoButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 6, bottom: 0, right: 0)

        moreInfoButton.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Common.add_icon, color: .COREBLUE), width: 16, height: 16), for: .highlighted)
        moreInfoButton.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Common.add_icon, color: .COREBLUE), width: 16, height: 16), for: .selected)

        moreInfoButton.setTitleColor(.MEDIUMBLUE, for: .normal)
        moreInfoButton.setTitleColor(.COREBLUE, for: .highlighted)
        moreInfoButton.setTitleColor(.COREBLUE, for: .selected)

    }

    private func configureTableView() {

        optionsContainerView.isHidden = true
        optionsTableView.backgroundColor = .clear

        optionsTableView.register(UINib(nibName: MultiSelectorListViewTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: MultiSelectorListViewTableViewCell.identifier)

        optionsTableView.tableHeaderView = MultiSelectorListHeaderView(frame: CGRect(x: 0, y: 0, width: frame.size.width, height: MultiSelectorListHeaderView.defaultHeight))

        optionsTableView.rowHeight = UITableViewAutomaticDimension
        optionsTableView.estimatedRowHeight = MultiSelectorListViewTableViewCell.height

        optionsTableView.isScrollEnabled = false
        optionsTableView.separatorInset = .zero
        optionsTableView.separatorColor = .BBVA300

    }

    @IBAction func moreInfoAction(_ sender: Any) {

        presenter.showMoreInfoPressed()

    }

    func clearSelected() {

        let selectedItems = optionsTableView?.indexPathsForSelectedRows

        if let selectedItems = selectedItems {
            for indexPath in selectedItems {
                optionsTableView?.deselectRow(at: indexPath, animated: true)
            }
        }

    }

    func updateOptionsStatus() {

        guard let options = options else {
            return
        }

        for i in 0..<options.count {

            if options[i].isSelected {
                optionsTableView.selectRow(at: IndexPath(row: i, section: 0), animated: false, scrollPosition: .none)
            } else {
                optionsTableView.deselectRow(at: IndexPath(row: i, section: 0), animated: false)
            }

        }

    }
}

extension MultiSelectorListView: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return options?.count ?? 0

    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if let optionItem = options?[indexPath.row], let cell = tableView.dequeueReusableCell(withIdentifier: MultiSelectorListViewTableViewCell.identifier, for: indexPath) as? MultiSelectorListViewTableViewCell {

            cell.configure(withDisplayData: optionItem)

            return cell

        }

        return UITableViewCell()

    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return MultiSelectorListViewTableViewCell.height
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        delegate?.multiSelectorListView(self, selectedAtIndex: indexPath.row)
    }

    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {

        delegate?.multiSelectorListView(self, deSelectedAtIndex: indexPath.row)
    }

}

extension MultiSelectorListView: MultiSelectorListViewProtocol {

    func showTitle(withText text: String) {

        moreInfoButton.setTitle(text, for: .normal)

        (optionsTableView.tableHeaderView as? MultiSelectorListHeaderView)?.titleLabel.text = text.uppercased()
    }

    func showOptions(withDisplayData displayData: [OptionItemDisplayData]) {

        moreInfoContainerView.isHidden = true
        optionsContainerView.isHidden = false

        options = displayData

        optionsTableView.reloadData()

        updateOptionsStatus()

        delegate?.multiSelectorListViewShowMoreInfo(self, requiresHeight: (CGFloat(displayData.count) * MultiSelectorListViewTableViewCell.height) + MultiSelectorListHeaderView.defaultHeight)

    }

    func reloadStatus(withDisplayData displayData: [OptionItemDisplayData]) {

        options = displayData

        updateOptionsStatus()

    }

}
