//
//  MultiSelectorListViewTableViewCell.swift
//  shoppingapp
//
//  Created by jesus.martinez on 26/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit

class MultiSelectorListViewTableViewCell: UITableViewCell {

    static let identifier = "MultiSelectorListViewTableViewCell"
    static let height: CGFloat = 73
    static let defaultAccessoryImageView = SVGCache.image(forSVGNamed: ConstantsImages.Common.checkmark_success, color: .COREBLUE)

    @IBOutlet weak var optionImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var accessoryImageView: UIImageView!

    var optionImageName: String?

    override func awakeFromNib() {
        super.awakeFromNib()

        selectionStyle = .none

        configureView()
        normalStatus()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {

        super.setSelected(selected, animated: animated)

        selected ? selectedStatus() : normalStatus()

    }

    private func configureView() {

        titleLabel.font = Tools.setFontBook(size: 16)

        accessoryImageView.contentMode = .scaleAspectFit
        accessoryImageView.image = MultiSelectorListViewTableViewCell.defaultAccessoryImageView

        optionImageView.contentMode = .scaleAspectFit
        optionImageView.image = MultiSelectorListViewTableViewCell.defaultAccessoryImageView
        optionImageView.isHidden = true

    }

    private func normalStatus() {

        titleLabel.textColor = .BBVA600

        if let imageName = optionImageName {
            optionImageView.image = SVGCache.image(forSVGNamed: imageName, color: .BBVA300)
        }

        accessoryImageView.isHidden = true

        backgroundColor = .BBVAWHITE

    }

    private func selectedStatus() {

        titleLabel.textColor = .COREBLUE

        if let imageName = optionImageName {
            optionImageView.image = SVGCache.image(forSVGNamed: imageName, color: .COREBLUE)
        }

        accessoryImageView.isHidden = false

        backgroundColor = .BBVA100

    }

    func configure(withDisplayData displayData: OptionItemDisplayData) {

        optionImageName = displayData.imageName

        titleLabel.text = displayData.title

        if let accesoryImageName = displayData.accesoryImageName {
            accessoryImageView.image = SVGCache.image(forSVGNamed: accesoryImageName, color: .COREBLUE)
        }

        if let imageName = optionImageName {

            let color: UIColor = self.isSelected ? .COREBLUE : .BBVA300
            DispatchQueue.global(qos: .userInitiated).async {

                let image = SVGCache.image(forSVGNamed: imageName, color: color)

                DispatchQueue.main.async {

                    self.optionImageView.image = image
                    self.optionImageView.isHidden = false

                }
            }

        }

    }

}
