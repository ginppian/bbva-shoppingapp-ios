//
//  MultiSelectorListHeaderView.swift
//  shoppingapp
//
//  Created by jesus.martinez on 26/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit

class MultiSelectorListHeaderView: XibView {

    static let defaultHeight: CGFloat = 88.0

    @IBOutlet weak var titleLabel: UILabel!

    override func configureView() {
        super.configureView()

        titleLabel.font = Tools.setFontBold(size: 13)
        titleLabel.textColor = .BBVA500

    }

    func configureTitle(withText text: String) {

        titleLabel.text = text

    }

}
