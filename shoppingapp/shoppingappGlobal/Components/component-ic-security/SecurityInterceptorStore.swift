//
//  SecurityInterceptorStore.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 5/6/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import CellsNativeComponents
import BBVA_Network

open class SecurityInterceptorStore: Store {

    public static let ID: String = "component-data-manager-security-interceptor-store"
    public static let SECURITY_TOKEN = ActionSpec<Any>(id: "security-token")
    public static let SOFTOKEN = ActionSpec<Any>(id: "softoken")

    private weak var securityInterceptor: SecurityInterceptor?
    private weak var worker: NetworkWorker?

    public init(worker: NetworkWorker) {

        self.worker = worker
        super.init(SecurityInterceptorStore.ID)

        let securityInterceptor = SecurityInterceptor(otp: self.onSecurityToken, softoken: self.onSoftoken)
        self.worker?.addInterceptor(interceptor: securityInterceptor)

        self.securityInterceptor = securityInterceptor

    }

    public required init() {
        fatalError("init() has not been implemented")
    }

    fileprivate func configureStoreWithProperties() {
        // All JSON properties will be defined here as current class properties.
    }

    func onSecurityToken() {

        DispatchQueue.main.async {
            self.fire(actionSpec: SecurityInterceptorStore.SECURITY_TOKEN, value: self.securityInterceptor?.responseHeader as Any)
        }
    }

    func onSoftoken() {

        DispatchQueue.main.async {
            self.fire(actionSpec: SecurityInterceptorStore.SOFTOKEN, value: self.securityInterceptor?.responseHeader as Any)
        }
    }

    open func unLock(headers: [String: String]) {
        
        securityInterceptor?.networkRequest?.headers["authenticationtype"] = headers["authenticationtype"]
        securityInterceptor?.networkRequest?.headers["authenticationdata"] = headers["authenticationdata"]
        securityInterceptor?.available.signal()
    }

    open func unLockStop() {
        
        securityInterceptor?.stop = true
        securityInterceptor?.available.signal()
    }

    override open func dispose() {
        super.dispose()
    }
}

private class SecurityInterceptor: NetworkInterceptor {

    typealias SecurityTokenClousure = () -> Void
    typealias SoftokenClousure = () -> Void
    
    var security: SecurityTokenClousure
    var softoken: SoftokenClousure
    var networkRequest: NetworkRequest?
    let available = DispatchSemaphore(value: 0)
    var stop = false
    var responseHeader = [String: String]()

    init(otp security:  @escaping SecurityTokenClousure, softoken: @escaping SoftokenClousure) {

        self.security = security
        self.softoken = softoken
    }

    func onRequest(request: NetworkRequest) {
        networkRequest = request
    }

    func onResponse(response: NetworkResponse) {

        if let body = response.body, let objectDeserialize = ErrorEntity.deserialize(from: body), let header = response.header, let authenticationtype = header["authenticationtype"] {

            let authenticationTypeSplit = authenticationtype.components(separatedBy: ";")
            
            print("\(#line): \(#function) - response.header: \(header)")
            responseHeader = header

            if objectDeserialize.httpStatus == 401 && objectDeserialize.code == "70" && authenticationtype == "05" {

                print("Fire Security")
                self.security()
                print("Waiting service")
                self.available.wait()
                print("Continue DiscardAndReplayRequest")
                if !stop {
                    response.discardAndReplay(request: response.request!)
                } else {
                    response.responseDataTask?.cancel()
                    stop = false
                }
            } else if objectDeserialize.httpStatus == 401 && objectDeserialize.code == "70" && (authenticationtype == "33" || authenticationTypeSplit.first == "33") {
                
                softoken()
                available.wait()
                
                if !stop {
                    
                    response.discardAndReplay(request: response.request!)
                    
                } else {
                    
                    response.responseDataTask?.cancel()
                    stop = false
                }
            }
        }
    }

}
