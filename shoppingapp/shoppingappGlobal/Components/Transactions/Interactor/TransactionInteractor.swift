//
//  TransactionInteractor.swift
//  shoppingapp
//
//  Created by Arturo Marzo de la Fuente on 26/9/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation
import RxSwift

class TransactionInteractor: TransactionInteractorProtocol {

    let disposeBag = DisposeBag()

    func provideTransaction(forCardId cardId: String) -> Observable <ModelBO> {

        return Observable.create { observer in

            TransactionsDataManager.sharedInstance.provideTransactions(forCardId: cardId).subscribe(
                onNext: { result in

                    if let transactionsEntity = result as? TransactionsEntity {

                        observer.onNext(TransactionsBO(transactionsEntity: transactionsEntity))
                        observer.onCompleted()
                    } else {

                        DLog(message: "Incorrect TransactionsEntity")
                        observer.onError(ServiceError.CouldNotDecodeJSON)
                    }

                },
                onError: { error in
                    observer.onError(Tools.evaluateError(with: error))
                }
            ).addDisposableTo(self.disposeBag)

            return Disposables.create()
        }
    }
}
