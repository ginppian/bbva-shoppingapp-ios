//
//  TransactionContract.swift
//  shoppingapp
//
//  Created by Arturo Marzo de la Fuente on 26/9/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import RxSwift

protocol TransactionViewProtocol: ComponentViewProtocol {

    func loadTransactions(forDisplayData displayData: TransactionDisplayData)
    func showTransactions(transactionsDisplayData: TransactionsDisplayData)
    func showSkeleton(rows: Int)
    func hideSkeleton()
    func show(message: TransactionMessageDisplayData)
    func showAllTransactions()
    func estimateHeightForTransactionView(withTransactionsDisplayData transactionsDisplayData: TransactionsDisplayData) -> CGFloat
    func showNoContentAction()
    func showDirectAccess()

}

protocol TransactionPresenterProtocol: ComponentPresenterProtocol {

    func loadTransactions(forDisplayData displayData: TransactionDisplayData)
    func showNoTransactions()
    func showError()
    func showAllTransactionsPressed()
    func messageButtonPressed()
}

protocol TransactionInteractorProtocol {

    func provideTransaction(forCardId cardId: String) -> Observable <ModelBO>
}
