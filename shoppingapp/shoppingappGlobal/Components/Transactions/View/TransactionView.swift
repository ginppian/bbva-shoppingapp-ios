//
//  TransactionView.swift
//  shoppingapp
//
//  Created by Arturo Marzo de la Fuente on 26/9/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit
import RxSwift
import Lottie
import Stellar
import IQKeyboardManagerSwift
import Material
import CellsNativeCore
import CellsNativeComponents

protocol TransactionViewDelegate: class {

    func showAllTransactionsPressed(forTransactionView transactionsView: TransactionView)
    func transactionViewWillLoadInfoWith(height: CGFloat)
    func noContentPressed(forTransactionView transactionsView: TransactionView)
    func showTransactionDetail(_ transactionView: TransactionView, andTransactionDisplayItem transactionDisplayItem: TransactionDisplayItem)

}

class TransactionView: XibView {

    // MARK: Constants
    static let defaultNumberOfSkeletons = 2

    static let headerViewHeight: CGFloat = 48
    static let messageViewTop: CGFloat = 21
    static let messageImageTop: CGFloat = 23
    static let messageTop: CGFloat = 14
    static let messageButtonTop: CGFloat = 20
    static let messageButtonHeight: CGFloat = 30
    static let messageButtonBottom: CGFloat = 30
    static let messageLabelLeadingTrailing: CGFloat = 30

    static let directMessageViewHeight: CGFloat = 200

    // MARK: IBOutlets

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var titleLastTransactionsLabel: UILabel!
    @IBOutlet weak var showAllTransactionsButton: UIButton!
    @IBOutlet weak var messageView: UIView!
    @IBOutlet weak var messageImageView: UIImageView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var messageButton: UIButton!
    @IBOutlet weak var transactionsView: UIView!
    @IBOutlet weak var transactionsTableView: UITableView!

    @IBOutlet weak var directAccessView: UIView!
    @IBOutlet weak var directAccessImageView: UIImageView!
    @IBOutlet weak var directAccessMessageLabel: UILabel!
    @IBOutlet weak var directAccessButton: UIButton!

    weak var delegate: TransactionViewDelegate?

    var presenter = TransactionPresenter()
    var transactionsDisplayData: TransactionsDisplayData?
    var needShowSkeleton = false
    var numberOfSkeletons = TransactionView.defaultNumberOfSkeletons

    // MARK: Custom Methods

    override func configureView() {

        super.configureView()

        backgroundColor = .BBVAWHITE

        titleLastTransactionsLabel.font = Tools.setFontBold(size: 14.0)
        titleLastTransactionsLabel.textColor = .BBVA500
        titleLastTransactionsLabel.backgroundColor = .clear
        titleLastTransactionsLabel.text = Localizables.transactions.key_transactions_last_text.uppercased()

        showAllTransactionsButton.titleLabel?.font = Tools.setFontMedium(size: 14.0)
        showAllTransactionsButton.titleLabel?.textColor = .MEDIUMBLUE
        showAllTransactionsButton.setTitleColor(.MEDIUMBLUE, for: .normal)
        showAllTransactionsButton.setTitleColor(.COREBLUE, for: .highlighted)
        showAllTransactionsButton.setTitleColor(.COREBLUE, for: .selected)
        showAllTransactionsButton.setTitle(Localizables.transactions.key_transactions_seeall_text, for: .normal)
        showAllTransactionsButton.isHidden = true

        transactionsView.backgroundColor = .clear
        transactionsView.isHidden = true

        messageView.backgroundColor = .clear
        messageView.isHidden = true

        messageImageView.backgroundColor = .clear

        messageLabel.font = Tools.setFontBook(size: 14.0)
        messageLabel.textColor = .BBVA600
        messageLabel.backgroundColor = .clear

        messageButton.setTitleColor(.MEDIUMBLUE, for: .normal)
        messageButton.setTitleColor(.COREBLUE, for: .highlighted)
        messageButton.setTitleColor(.COREBLUE, for: .selected)
        messageButton.titleLabel?.font = Tools.setFontMedium(size: 14)
        messageButton.backgroundColor = .clear

        transactionsTableView.register(UINib(nibName: TransactionTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: TransactionTableViewCell.identifier)
        transactionsTableView.register(UINib(nibName: SkeletonCell.identifier, bundle: nil), forCellReuseIdentifier: SkeletonCell.identifier)

        transactionsTableView.rowHeight = UITableViewAutomaticDimension
        transactionsTableView.estimatedRowHeight = 60

        self.transactionsTableView.separatorStyle = .none

        presenter.view = self

        if #available(iOS 11.0, *) {
            transactionsTableView.contentInsetAdjustmentBehavior = .never
        }

        configureDirectAccessView()

    }

    private func configureDirectAccessView() {

        directAccessView.isHidden = true

        directAccessView.backgroundColor = .BBVAWHITE

        directAccessImageView.image = SVGCache.image(forSVGNamed: ConstantsImages.Card.transfer_icon, color: .BBVA400)
        directAccessView.contentMode = .scaleAspectFit

        directAccessMessageLabel.numberOfLines = 2
        directAccessMessageLabel.text = Localizables.transactions.key_card_movement_last_three_movements_text
        directAccessMessageLabel.font = Tools.setFontBook(size: 14)
        directAccessMessageLabel.textColor = .BBVA600

        directAccessButton.setTitle(Localizables.transactions.key_card_movement_see_and_search_text, for: .normal)
        directAccessButton.setTitleColor(.MEDIUMBLUE, for: .normal)
        directAccessButton.setTitleColor(.COREBLUE, for: .highlighted)
        directAccessButton.setTitleColor(.COREBLUE, for: .selected)
        directAccessButton.titleLabel?.font = Tools.setFontMedium(size: 14)

    }

    @IBAction func showAllTransactionsPressed(_ sender: Any) {

        presenter.showAllTransactionsPressed()
    }

    @IBAction func messageButtonPressed(_ sender: Any) {

        presenter.messageButtonPressed()

    }

    @IBAction func directAccessButtonPressed(_ sender: Any) {
        presenter.showAllTransactionsPressed()
    }

}

extension TransactionView: TransactionViewProtocol {

    func loadTransactions(forDisplayData displayData: TransactionDisplayData) {

        presenter.loadTransactions(forDisplayData: displayData)

    }

    func showTransactions(transactionsDisplayData: TransactionsDisplayData) {

        if let delegate = delegate {
            let height = estimateHeightForTransactionView(withTransactionsDisplayData: transactionsDisplayData)
            delegate.transactionViewWillLoadInfoWith(height: height)
        }

        showAllTransactionsButton.isHidden = false
        transactionsView.isHidden = false
        showAllTransactionsButton.isHidden = false
        messageView.isHidden = true

        self.transactionsDisplayData = transactionsDisplayData

        transactionsTableView.reloadData()
    }

    func showSkeleton(rows: Int) {

        numberOfSkeletons = rows

        if let delegate = delegate {
            let height = estimateHeightForSkeleton()
            delegate.transactionViewWillLoadInfoWith(height: height)
        }

        transactionsView.isHidden = false
        showAllTransactionsButton.isHidden = true
        messageView.isHidden = true

        transactionsTableView.isScrollEnabled = false

        needShowSkeleton = true

        transactionsTableView.reloadData()
    }

    func hideSkeleton() {

        showAllTransactionsButton.isHidden = true
        transactionsView.isHidden = true
        showAllTransactionsButton.isHidden = true
        messageView.isHidden = false

        transactionsTableView.isScrollEnabled = false

        needShowSkeleton = false

        transactionsTableView.reloadData()
    }

    func show(message: TransactionMessageDisplayData) {

        if let delegate = delegate {
            let height = estimateHeightFor(message: message)
            delegate.transactionViewWillLoadInfoWith(height: height)
        }

        showAllTransactionsButton.isHidden = true
        transactionsView.isHidden = true
        showAllTransactionsButton.isHidden = true
        messageView.isHidden = false

        self.transactionsDisplayData = nil

        transactionsTableView.reloadData()

        messageImageView.image = message.messageImage
        messageLabel.text = message.messageText
        messageButton.setTitle(message.messageButton, for: .normal)
    }

    func showAllTransactions() {

        delegate?.showAllTransactionsPressed(forTransactionView: self)
    }

    func estimateHeightForTransactionView(withTransactionsDisplayData transactionsDisplayData: TransactionsDisplayData) -> CGFloat {

        var height: CGFloat = TransactionView.headerViewHeight

        for section in transactionsDisplayData.sections {

            height += TransactionListSectionHeader.defaultHeight
            var index = 0
            for item in section.items {

                let hideSeparator = (index == (section.items.count - 1))
                height += TransactionTableViewCell.heightForCell(withDisplayItem: item, hideSeparator: hideSeparator)
                index += 1
            }
        }

        return height
    }

    func estimateHeightFor(message: TransactionMessageDisplayData) -> CGFloat {

        var height: CGFloat = TransactionView.headerViewHeight + TransactionView.messageViewTop + TransactionView.messageImageTop + TransactionView.messageTop + TransactionView.messageButtonTop + TransactionView.messageButtonHeight + TransactionView.messageButtonBottom

        height += message.messageImage != nil ? (message.messageImage?.size.height)! : 0

        let messageLabelWidth = self.frame.size.width - 2 * TransactionView.messageLabelLeadingTrailing
        let messageLabelHeight = (message.messageText?.height(withConstrainedWidth: messageLabelWidth, font: messageLabel.font))!
        height += messageLabelHeight

        return height
    }

    func estimateHeightForSkeleton() -> CGFloat {

        return TransactionView.headerViewHeight + (SkeletonCell.cellHeight * CGFloat(numberOfSkeletons))

    }

    func showNoContentAction() {

        delegate?.noContentPressed(forTransactionView: self)

    }

    func showDirectAccess() {

        directAccessView.isHidden = false

        headerView.isHidden = true
        messageView.isHidden = true
        transactionsView.isHidden = true

        delegate?.transactionViewWillLoadInfoWith(height: TransactionView.directMessageViewHeight)

    }
}

extension TransactionView: UITableViewDelegate {

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        if !needShowSkeleton {

            let view = TransactionListSectionHeader(frame: CGRect(x: 0, y: 0, width: self.frame.size.width, height: TransactionListSectionHeader.defaultHeight))
            view.titleLabel.text = transactionsDisplayData?.sections[section].name
            view.backgroundColor = .clear

            return view
        }

        return UIView()

    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {

        if !needShowSkeleton {
            return  TransactionListSectionHeader.defaultHeight
        }

        return 0
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {

        let footer = UIView()

        if !needShowSkeleton {
            footer.backgroundColor = .BBVA200
        } else {
            footer.backgroundColor = .clear
        }

        return footer
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if !needShowSkeleton {
            if let displayItem = transactionsDisplayData?.sections[indexPath.section].items[indexPath.row] {
                delegate?.showTransactionDetail(self, andTransactionDisplayItem: displayItem)
            }
        }

    }
}

extension TransactionView: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {

        if !needShowSkeleton {
            return transactionsDisplayData?.sections.count ?? 0
        }

        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if !needShowSkeleton {
            return transactionsDisplayData?.sections[section].items.count ?? 0
        }

        return numberOfSkeletons
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if !needShowSkeleton {

            if let displayItem = transactionsDisplayData?.sections[indexPath.section].items[indexPath.row] {

                if let cell = tableView.dequeueReusableCell(withIdentifier: TransactionTableViewCell.identifier, for: indexPath) as? TransactionTableViewCell {

                    cell.configure(withDisplayItem: displayItem, hideSeparator: self.isLastRow(inPath: indexPath))
                    return cell
                }
            }

        } else {

            if let cell = tableView.dequeueReusableCell(withIdentifier: SkeletonCell.identifier, for: indexPath) as? SkeletonCell {
                return cell
            }
        }

        return UITableViewCell()
    }

    func isLastRow(inPath path: IndexPath) -> Bool {
        return path.row == (transactionsDisplayData!.sections[path.section].items.count - 1)
    }
}
