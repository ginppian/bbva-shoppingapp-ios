//
//  TransactionDisplayData.swift
//  shoppingapp
//
//  Created by jesus.martinez on 6/10/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation

struct TransactionDisplayData {

    var showDirectAccess = false
    var numberOfSkeletons: Int
    var maximumNumberOfTransactions: Int
    var cardId: String
    var transactionsBO: TransactionsBO?

    init(numberOfSkeletons: Int, maximumNumberOfTransactions: Int, cardId: String) {

        self.numberOfSkeletons = numberOfSkeletons
        self.maximumNumberOfTransactions = maximumNumberOfTransactions
        self.cardId = cardId

    }

}

extension TransactionDisplayData: Equatable {
    static func == (lhs: TransactionDisplayData, rhs: TransactionDisplayData) -> Bool {
        return lhs.maximumNumberOfTransactions == rhs.maximumNumberOfTransactions
                && lhs.cardId == rhs.cardId
    }
}
