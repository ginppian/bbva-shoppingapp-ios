//
//  TransactionMessageDisplayData.swift
//  shoppingapp
//
//  Created by Arturo Marzo de la Fuente on 27/9/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation

struct TransactionMessageDisplayData {

    var messageImage: UIImage?
    var messageText: String?
    var messageButton: String?

    init(withMessageImage messageImage: UIImage?, messageText: String?, andMessageButton messageButton: String?) {

        self.messageImage = messageImage
        self.messageText = messageText
        self.messageButton = messageButton
    }
}
