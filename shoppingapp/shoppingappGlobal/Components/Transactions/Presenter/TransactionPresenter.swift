//
//  TransactionPresenter.swift
//  shoppingapp
//
//  Created by Arturo Marzo de la Fuente on 26/9/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation
import RxSwift
import CellsNativeComponents
import CellsNativeCore

protocol TransactionPresenterDelegate: class {

    func transactionPresenter(_ transactionPresenter: TransactionPresenter, loadedTransactions transactions: TransactionsBO, forCardId cardId: String)
    func transactionPresenter(_ transactionPresenter: TransactionPresenter, error: ErrorBO)

}

class TransactionPresenter {

    weak var delegate: TransactionPresenterDelegate?

    weak var view: TransactionViewProtocol?
    var interactor: TransactionInteractorProtocol!
    var displayData = TransactionsDisplayData()
    var transactionDisplayData: TransactionDisplayData?

    let disposeBag = DisposeBag()
}

extension TransactionPresenter: TransactionPresenterProtocol {

    func loadTransactions(forDisplayData displayData: TransactionDisplayData) {

        transactionDisplayData = displayData

        if displayData.showDirectAccess {

            view?.showDirectAccess()

        } else {

            guard let transactions = displayData.transactionsBO else {

                loadTransacationFromServer(withDisplayData: displayData)

                return
            }

            self.show(transactions: transactions, withMaximumTransactions: displayData.maximumNumberOfTransactions)

        }

    }

    private func loadTransacationFromServer(withDisplayData displayData: TransactionDisplayData) {

        if interactor == nil {

            interactor = TransactionInteractor()
        }

        view?.showSkeleton(rows: displayData.numberOfSkeletons)

        interactor?.provideTransaction(forCardId: displayData.cardId).subscribe(
            onNext: { [weak self] result in
                guard let `self` = self else {
                    return
                }

                if let transactions = result as? TransactionsBO {

                    if let delegate = self.delegate {

                        delegate.transactionPresenter(self, loadedTransactions: transactions, forCardId: displayData.cardId)

                    }

                    self.transactionDisplayData?.transactionsBO = transactions

                    self.show(transactions: transactions, withMaximumTransactions: displayData.maximumNumberOfTransactions)

                } else {

                    DLog(message: "No transactionsBO")
                    self.showError()
                }
            },
            onError: { [weak self] error in
                guard let `self` = self else {
                    return
                }

                self.showError()

                let evaluate: ServiceError = (error as? ServiceError)!

                switch evaluate {

                case .GenericErrorBO(let errorBO):

                    self.delegate?.transactionPresenter(self, error: errorBO)

                default:
                    break

                }

            }).addDisposableTo(disposeBag)

    }

    func show(transactions: TransactionsBO, withMaximumTransactions maximumTransactions: Int) {

        if transactions.status == ConstantsHTTPCodes.NO_CONTENT {

            self.showNoTransactions()
        } else if transactions.status == ConstantsHTTPCodes.STATUS_OK {

            if !self.displayData.sections.isEmpty {

                self.displayData = TransactionsDisplayData()
            }

            var transactionsToDisplay = transactions.transactions

            if transactions.transactions.count > maximumTransactions {

                transactionsToDisplay = Array(transactions.transactions[0..<maximumTransactions])

            }

            self.displayData.add(transactions: transactionsToDisplay)

            self.view?.hideSkeleton()
            self.view?.showTransactions(transactionsDisplayData: self.displayData)
        } else {

            DLog(message: "Invalid status")
            self.showError()
        }

    }

    func showNoTransactions() {

        self.view?.hideSkeleton()

        let image = ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Common.info_icon, color: .BBVALIGHTBLUE),
                                         width: 24,
                                         height: 24)
        let message =
            TransactionMessageDisplayData(withMessageImage: image,
                                                 messageText: Localizables.transactions.key_transactions_noresults_text,
                                                 andMessageButton: Localizables.transactions.key_transactions_searchtransactions_text)
        self.view?.show(message: message)
    }

    func showError() {

        self.view?.hideSkeleton()

        let image = ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Common.alert_icon, color: .BBVALIGHTRED),
                                         width: 22,
                                         height: 22)
        let message =
            TransactionMessageDisplayData(withMessageImage: image,
                                                 messageText: Localizables.transactions.key_transactions_error_text,
                                                 andMessageButton: Localizables.transactions.key_transactions_reload_text)
        self.view?.show(message: message)
    }

    func showAllTransactionsPressed() {

        view?.showAllTransactions()
    }

    func messageButtonPressed() {

        if transactionDisplayData!.transactionsBO == nil {

            loadTransacationFromServer(withDisplayData: transactionDisplayData!)

        } else {

            view?.showNoContentAction()

        }

    }

}
