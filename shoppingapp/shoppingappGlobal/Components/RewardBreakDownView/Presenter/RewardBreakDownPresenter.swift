//
//  RewardBreakDownPresenter.swift
//  shoppingapp
//
//  Created by jesus.martinez on 17/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import RxSwift

protocol RewardBreakDownPresenterDelegate: class {
    func rewardBreakDownPresenter(_ rewardBreakDownPresenter: RewardBreakDownPresenter, willRetrieveBalanceForCardId cardId: String)
    func rewardBreakDownPresenter(_ rewardBreakDownPresenter: RewardBreakDownPresenter, cardId: String, retrieveError: ErrorBO)
    func rewardBreakDownPresenter(_ rewardBreakDownPresenter: RewardBreakDownPresenter, cardId: String, retrieveBalanceRewards: BalanceRewardsBO)
}

class RewardBreakDownPresenter {

    weak var delegate: RewardBreakDownPresenterDelegate?
    weak var view: RewardBreakDownViewProtocol?
    var interactor: RewardBreakDownInteractorProtocol = RewardBreakDownInteractor()
    var model: RewardBreakDownModelProtocol?

    let disposeBag = DisposeBag()

    fileprivate func showBalance(_ balance: BalanceRewardsBO) {
        let displayData = RewardBreakDownDisplayData(rewards: balance)

        if displayData.showInfoPoints {
            view?.showInfoPointsView()
        } else if displayData.showInfoPointsWithError {
            view?.showInfoPointsViewWithError()
        } else {
            view?.hiddenInfoPointsView()
        }

        view?.showRewardBreakDown(withDisplayData: displayData)
    }

    fileprivate func retrieveBalanceReward(withModel model: RewardBreakDownModelProtocol) {

        view?.showSkeleton()
        
        delegate?.rewardBreakDownPresenter(self, willRetrieveBalanceForCardId: model.cardId)

        interactor.retrieveBalanceRewards(cardId: model.cardId).subscribe(
            onNext: { [weak self] result in
                guard let `self` = self else {
                    return
                }
                self.view?.hideSkeleton()

                if let balance = result as? BalanceRewardsBO {

                    self.showBalance(balance)
                    self.delegate?.rewardBreakDownPresenter(self, cardId: model.cardId, retrieveBalanceRewards: balance)
                }

            },
            onError: { [weak self] error in
                guard let `self` = self else {
                    return
                }

                guard let serviceError = error as? ServiceError else {
                    self.view?.hideSkeleton()
                    return
                }

                switch serviceError {

                case .GenericErrorBO(let errorBO):

                    self.view?.showError(text: Localizables.points.key_points_error_loading_try_again_text)
                    self.delegate?.rewardBreakDownPresenter(self, cardId: model.cardId, retrieveError: errorBO)

                default:
                    break
                }

                self.view?.hideSkeleton()

            }).addDisposableTo(disposeBag)

    }
}

extension RewardBreakDownPresenter: RewardBreakDownPresenterProtocol {

    func setModel(_ model: RewardBreakDownModelProtocol) {

        self.model = model
        
        guard !model.showError else {
            view?.showError(text: Localizables.points.key_points_error_loading_try_again_text)
            return
        }

        if let balance = model.balanceRewards {

            showBalance(balance)

        } else {

            retrieveBalanceReward(withModel: model)

        }

    }

    func retryPressed() {

        view?.hideError()

        if let model = model {

            retrieveBalanceReward(withModel: model)

        }
    }

}
