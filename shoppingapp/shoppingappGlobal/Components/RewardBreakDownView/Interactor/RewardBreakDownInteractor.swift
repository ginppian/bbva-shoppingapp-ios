//
//  RewardBreakDownInteractor.swift
//  shoppingapp
//
//  Created by jesus.martinez on 17/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import RxSwift

class RewardBreakDownInteractor {

    let disposeBag = DisposeBag()

}

extension RewardBreakDownInteractor: RewardBreakDownInteractorProtocol {

    func retrieveBalanceRewards(cardId: String) -> Observable <ModelBO> {

        return Observable.create { observer in

            RewardsDataManager.sharedInstance.retrieveBalanceRewards(cardId: cardId).subscribe(
                onNext: { result in

                    if let balanceRewardsEntity = result as? BalanceRewardsEntity {

                        observer.onNext(BalanceRewardsBO(entity: balanceRewardsEntity))
                        observer.onCompleted()
                    } else {

                        DLog(message: "Incorrect balanceRewardsEntity")
                        observer.onError(ServiceError.CouldNotDecodeJSON)
                    }

                },
                onError: { error in

                    let evaluate : ServiceError = (error as? ServiceError)!

                    switch evaluate {
                    case .GenericErrorEntity(let errorEntity):
                        let errorBVABO = ErrorBO(error: errorEntity)
                        observer.onError(ServiceError.GenericErrorBO(error: errorBVABO))
                    default:
                        observer.onError(evaluate)
                    }
                },
                onCompleted: {
                }
                ).addDisposableTo(self.disposeBag)

            return Disposables.create()
        }

    }

}
