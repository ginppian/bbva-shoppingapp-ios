//
//  RewardBreakDownContract.swift
//  shoppingapp
//
//  Created by jesus.martinez on 17/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import RxSwift

protocol RewardBreakDownViewProtocol: ComponentViewProtocol {

    func showSkeleton()
    func hideSkeleton()
    func showError(text: String)
    func hideError()
    func showRewardBreakDown(withDisplayData displayData: RewardBreakDownDisplayDataProtocol)
    func hiddenInfoPointsView()
    func showInfoPointsView()
    func showInfoPointsViewWithError()

}

protocol RewardBreakDownPresenterProtocol: ComponentPresenterProtocol {

    func setModel(_ model: RewardBreakDownModelProtocol)
    func retryPressed()

}

protocol RewardBreakDownInteractorProtocol {

    func retrieveBalanceRewards(cardId: String) -> Observable <ModelBO>

}
