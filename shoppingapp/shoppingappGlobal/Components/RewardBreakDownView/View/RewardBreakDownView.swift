//
//  RewardBreakDownView.swift
//  shoppingapp
//
//  Created by jesus.martinez on 17/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit
import Shimmer

protocol RewardBreakDownViewDelegate: class {
    func rewardBreakDownView(_ rewardBreakDownView: RewardBreakDownView, changedHeight height: CGFloat)
}

class RewardBreakDownView: XibView {

    static let contentHeight: CGFloat = 368
    static let errorHeight: CGFloat = 140

    weak var delegate: RewardBreakDownViewDelegate?

    let presenter = RewardBreakDownPresenter()

    // MARK: - Data Container views
    @IBOutlet weak var pointsDataContainerView: UIView!

    @IBOutlet weak var fromToDateLabel: UILabel!
    @IBOutlet weak var pointsLabel: UILabel!
    @IBOutlet weak var pointsTitleLabel: UILabel!

    @IBOutlet weak var previousIcoView: UIView!
    @IBOutlet weak var previousTitleLabel: UILabel!
    @IBOutlet weak var previousValueLabel: UILabel!

    @IBOutlet weak var generatedIcoView: UIView!
    @IBOutlet weak var generatedTitleLabel: UILabel!
    @IBOutlet weak var generatedValueLabel: UILabel!

    @IBOutlet weak var separatorView: UIView!

    @IBOutlet weak var appliedIcoView: UIView!
    @IBOutlet weak var appliedTitleLabel: UILabel!
    @IBOutlet weak var appliedValueLabel: UILabel!

    @IBOutlet weak var expiredIcoView: UIView!
    @IBOutlet weak var expiredTitleLabel: UILabel!
    @IBOutlet weak var expiredValueLabel: UILabel!

    @IBOutlet weak var infoContainerStackView: UIStackView!
    @IBOutlet weak var infoPointsView: UIView!
    @IBOutlet weak var infoImageView: UIImageView!
    @IBOutlet weak var infoTitleLabel: UILabel!

    // MARK: - Skeleton views
    @IBOutlet weak var skeletonsContainerView: UIView!

    @IBOutlet weak var fromToDateSkeletonView: FBShimmeringView!
    @IBOutlet weak var pointsSkeletonView: FBShimmeringView!
    @IBOutlet weak var pointsTitleSkeletonView: FBShimmeringView!
    @IBOutlet weak var previousTitleSkeletonView: FBShimmeringView!
    @IBOutlet weak var previousValueSkeletonView: FBShimmeringView!
    @IBOutlet weak var generatedTitleSkeletonView: FBShimmeringView!
    @IBOutlet weak var generatedValueSkeletonView: FBShimmeringView!
    @IBOutlet weak var separatorSkeletonView: UIView!
    @IBOutlet weak var appliedTitleSkeletonView: FBShimmeringView!
    @IBOutlet weak var appliedValueSkeletonView: FBShimmeringView!
    @IBOutlet weak var expiredTitleSkeletonView: FBShimmeringView!
    @IBOutlet weak var expiredValueSkeletonView: FBShimmeringView!

    // MARK: - Error views
    @IBOutlet weak var errorView: InformationView!

    override func configureView() {

        super.configureView()

        presenter.view = self

        backgroundColor = .BBVAWHITE

        configureDataViews()

        configureSkeletonViews()

        configureErrorView()

    }

    private func configureDataViews() {

        pointsDataContainerView.backgroundColor = .BBVAWHITE

        fromToDateLabel.textColor = .DARKGOLD
        fromToDateLabel.font = Tools.setFontMediumItalic(size: 14)
        pointsLabel.textColor = .BBVA600
        pointsLabel.font = Tools.setFontBook(size: 36)
        pointsTitleLabel.textColor = .BROWNISHGREY
        pointsTitleLabel.text = Localizables.points.key_points_balance_text
        pointsTitleLabel.font = Tools.setFontBook(size: 14)

        previousIcoView.backgroundColor = .BBVAMEDIUMAQUA
        previousIcoView.layer.mask = rhomboidShapeLayer(rect: previousIcoView.bounds)
        previousTitleLabel.textColor = .BROWNISHGREY
        previousTitleLabel.font = Tools.setFontBook(size: 14)
        previousTitleLabel.text = Localizables.points.key_points_previous_balance_text
        previousValueLabel.textColor = .BBVA600
        previousValueLabel.font = Tools.setFontBook(size: 16)

        generatedIcoView.backgroundColor = .AQUA
        generatedIcoView.layer.mask = rhomboidShapeLayer(rect: generatedIcoView.bounds)
        generatedTitleLabel.textColor = .BROWNISHGREY
        generatedTitleLabel.font = Tools.setFontBook(size: 14)
        generatedTitleLabel.text = Localizables.points.key_points_generated_text
        generatedValueLabel.textColor = .BBVA600
        generatedValueLabel.font = Tools.setFontBook(size: 16)

        separatorView.backgroundColor = .BBVA300

        appliedIcoView.backgroundColor = .BBVACORAL
        appliedIcoView.layer.mask = rhomboidShapeLayer(rect: appliedIcoView.bounds)
        appliedTitleLabel.textColor = .BROWNISHGREY
        appliedTitleLabel.font = Tools.setFontBook(size: 14)
        appliedTitleLabel.text = Localizables.points.key_points_used_text
        appliedValueLabel.textColor = .BBVA600
        appliedValueLabel.font = Tools.setFontBook(size: 16)

        expiredIcoView.backgroundColor = .BBVALIGHTCORAL
        expiredIcoView.layer.mask = rhomboidShapeLayer(rect: expiredIcoView.bounds)
        expiredTitleLabel.textColor = .BROWNISHGREY
        expiredTitleLabel.font = Tools.setFontBook(size: 14)
        expiredTitleLabel.text = Localizables.points.key_points_expired_text
        expiredValueLabel.textColor = .BBVA600
        expiredValueLabel.font = Tools.setFontBook(size: 16)

        infoTitleLabel.font = Tools.setFontBook(size: 14)
        infoTitleLabel.textColor = .BBVA600

    }

    private func configureSkeletonViews() {

        skeletonsContainerView.isHidden = true
        skeletonsContainerView.backgroundColor = .BBVAWHITE

        var imageSkeleton = SVGCache.image(forSVGNamed: ConstantsImages.Skeleton.lines, color: .BBVA300)
        imageSkeleton = imageSkeleton.resizableImage(withCapInsets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 20))
        var imageReverseSkeleton = SVGCache.image(forSVGNamed: ConstantsImages.Skeleton.lines_reverse, color: .BBVA300)
        imageReverseSkeleton = imageReverseSkeleton.resizableImage(withCapInsets: UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 0))

        fromToDateSkeletonView.contentView = UIImageView(image: imageSkeleton)
        pointsSkeletonView.contentView = UIImageView(image: imageSkeleton)
        pointsTitleSkeletonView.contentView = UIImageView(image: imageSkeleton)
        previousTitleSkeletonView.contentView = UIImageView(image: imageSkeleton)
        previousValueSkeletonView.contentView = UIImageView(image: imageReverseSkeleton)
        generatedTitleSkeletonView.contentView = UIImageView(image: imageSkeleton)
        generatedValueSkeletonView.contentView = UIImageView(image: imageReverseSkeleton)
        separatorSkeletonView.backgroundColor = .BBVA300
        appliedTitleSkeletonView.contentView = UIImageView(image: imageSkeleton)
        appliedValueSkeletonView.contentView = UIImageView(image: imageReverseSkeleton)
        expiredTitleSkeletonView.contentView = UIImageView(image: imageSkeleton)
        expiredValueSkeletonView.contentView = UIImageView(image: imageReverseSkeleton)

    }

    private func configureErrorView() {

        errorView.isHidden = true
        errorView.delegate = self

    }

    private func rhomboidShapeLayer(rect: CGRect) -> CAShapeLayer {

        let inclination: CGFloat = 4.0
        let mask = CAShapeLayer()
        mask.frame = rect

        let rhomboidPath = UIBezierPath()
        rhomboidPath.move(to: CGPoint(x: 0, y: rect.height))
        rhomboidPath.addLine(to: CGPoint(x: inclination, y: 0))
        rhomboidPath.addLine(to: CGPoint(x: rect.width, y: 0))
        rhomboidPath.addLine(to: CGPoint(x: rect.width - inclination, y: rect.height))
        rhomboidPath.close()

        mask.path = rhomboidPath.cgPath

        return mask

    }
}

extension RewardBreakDownView: InformationViewDelegate {
    func informationViewActionButtonPressed(_ informationView: InformationView) {
        presenter.retryPressed()
    }
}

extension RewardBreakDownView: RewardBreakDownViewProtocol {

    func showSkeleton() {

        delegate?.rewardBreakDownView(self, changedHeight: RewardBreakDownView.contentHeight)

        skeletonsContainerView.isHidden = false
        errorView.isHidden = true
        pointsDataContainerView.isHidden = true

        fromToDateSkeletonView.isShimmering = true
        pointsSkeletonView.isShimmering = true
        pointsTitleSkeletonView.isShimmering = true
        previousTitleSkeletonView.isShimmering = true
        previousValueSkeletonView.isShimmering = true
        generatedTitleSkeletonView.isShimmering = true
        generatedValueSkeletonView.isShimmering = true
        appliedTitleSkeletonView.isShimmering = true
        appliedValueSkeletonView.isShimmering = true
        expiredTitleSkeletonView.isShimmering = true
        expiredValueSkeletonView.isShimmering = true

    }

    func hideSkeleton() {

        skeletonsContainerView.isHidden = true

        fromToDateSkeletonView.isShimmering = false
        pointsSkeletonView.isShimmering = false
        pointsTitleSkeletonView.isShimmering = false
        previousTitleSkeletonView.isShimmering = false
        previousValueSkeletonView.isShimmering = false
        generatedTitleSkeletonView.isShimmering = false
        generatedValueSkeletonView.isShimmering = false
        appliedTitleSkeletonView.isShimmering = false
        appliedValueSkeletonView.isShimmering = false
        expiredTitleSkeletonView.isShimmering = false
        expiredValueSkeletonView.isShimmering = false

    }

    func showError(text: String) {

        delegate?.rewardBreakDownView(self, changedHeight: RewardBreakDownView.errorHeight)

        let image = SVGCache.image(forSVGNamed: ConstantsImages.Common.alert_icon, color: .BBVALIGHTRED)
        let informationDiplayData = InformationViewDisplayData(iconImage: image, message: text, actionButtonTitle: Localizables.common.key_retry_text)
        errorView.configure(displayData: informationDiplayData)

        errorView.isHidden = false
        skeletonsContainerView.isHidden = true
        pointsDataContainerView.isHidden = true

    }

    func hideError() {
        errorView.isHidden = true
    }

    func showRewardBreakDown(withDisplayData displayData: RewardBreakDownDisplayDataProtocol) {

        let pointsDataContainerViewHeight = pointsDataContainerView.systemLayoutSizeFitting(UILayoutFittingCompressedSize).height
        delegate?.rewardBreakDownView(self, changedHeight: pointsDataContainerViewHeight)

        pointsDataContainerView.isHidden = false
        errorView.isHidden = true
        skeletonsContainerView.isHidden = true

        fromToDateLabel.text = displayData.dateFromToTitle
        pointsLabel.text = displayData.currentPoints
        previousValueLabel.text = displayData.previousPoints
        generatedValueLabel.text = displayData.generatedPoints
        appliedValueLabel.text = displayData.appliedPoints
        expiredValueLabel.text = displayData.expiredPoints

    }

    func hiddenInfoPointsView() {

        infoPointsView.isHidden = true
    }

    func showInfoPointsView() {

        infoPointsView.isHidden = false

        infoPointsView.backgroundColor = .WHITELIGHTBLUE
        infoImageView.image = SVGCache.image(forSVGNamed: ConstantsImages.Common.info_icon, color: .BBVALIGHTBLUE)
        infoTitleLabel.text = Localizables.points.key_points_info_title_text
    }

    func showInfoPointsViewWithError() {

        infoPointsView.isHidden = false

        infoPointsView.backgroundColor = .BBVAWHITERED30
        infoImageView.image = SVGCache.image(forSVGNamed: ConstantsImages.Common.alert_icon, color: .BBVALIGHTRED)
        infoTitleLabel.text = Localizables.points.key_points_info_title_error_text
    }
}
