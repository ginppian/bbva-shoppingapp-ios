//
//  RewardBreakDownModel.swift
//  shoppingapp
//
//  Created by jesus.martinez on 18/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol RewardBreakDownModelProtocol {
    var cardId: String { get }
    var balanceRewards: BalanceRewardsBO? { get }
    var showError: Bool { get }
}

struct RewardBreakDownModel: RewardBreakDownModelProtocol {
    let cardId: String
    let balanceRewards: BalanceRewardsBO?
    let showError: Bool
}

extension RewardBreakDownModel: Equatable {
    public static func == (lhs: RewardBreakDownModel, rhs: RewardBreakDownModel) -> Bool {

        return lhs.cardId == rhs.cardId
            && lhs.balanceRewards == rhs.balanceRewards

    }
}
