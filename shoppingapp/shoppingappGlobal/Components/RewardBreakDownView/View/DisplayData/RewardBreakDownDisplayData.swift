//
//  RewardBreakDownDisplayData.swift
//  shoppingapp
//
//  Created by jesus.martinez on 19/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol RewardBreakDownDisplayDataProtocol {
    var dateFromToTitle: String { get }
    var currentPoints: String { get }
    var previousPoints: String { get }
    var generatedPoints: String { get }
    var appliedPoints: String { get }
    var expiredPoints: String { get }
}

struct RewardBreakDownDisplayData: RewardBreakDownDisplayDataProtocol {

    let defaultValue = "0"
    let uninformedValue = "—"

    var dateFromToTitle: String
    var currentPoints: String
    var previousPoints: String
    var generatedPoints: String
    var appliedPoints: String
    var expiredPoints: String

    var showInfoPoints = false
    var showInfoPointsWithError = false

    init(rewards: BalanceRewardsBO) {

        if let startDate = rewards.startDate, let endDate = rewards.endDate {

            let startDateFormatted = startDate.string(format: Date.DATE_FORMAT_DAY_MONTH_SHORT)
            let endDateFormatted = endDate.string(format: Date.DATE_FORMAT_DAY_MONTH_YEAR_SHORT)
            dateFromToTitle = Localizables.common.key_from_text + " \(startDateFormatted) " + Localizables.common.key_to_text +  " \(endDateFormatted)"

        } else {
            dateFromToTitle = uninformedValue
        }

        if let reward = rewards.rewardBreakDown(.current) {
            currentPoints = NumberFormatter.stringDecimalToStringWithSeparator(withString: "\(reward.nonMonetaryValue)", withLocale: .current)
        } else {
            currentPoints = uninformedValue
        }

        if let reward = rewards.rewardBreakDown(.previous) {
            previousPoints = NumberFormatter.stringDecimalToStringWithSeparator(withString: "\(reward.nonMonetaryValue)", withLocale: .current)
            
        } else {
            previousPoints = uninformedValue
        }

        if let reward = rewards.rewardBreakDown(.generated) {
            generatedPoints = NumberFormatter.stringDecimalToStringWithSeparator(withString: "\(reward.nonMonetaryValue)", withLocale: .current)
            
        } else {
            generatedPoints = uninformedValue
        }

        if let reward = rewards.rewardBreakDown(.applied) {
            let appliedPointsValue = reward.nonMonetaryValue > 0 ? -reward.nonMonetaryValue : reward.nonMonetaryValue
            appliedPoints = NumberFormatter.stringDecimalToStringWithSeparator(withString: "\(appliedPointsValue)", withLocale: .current)
            
        } else {
            appliedPoints = uninformedValue
        }

        if let reward = rewards.rewardBreakDown(.expired) {
            let expiredPointsValue = reward.nonMonetaryValue > 0 ? -reward.nonMonetaryValue : reward.nonMonetaryValue
            expiredPoints = NumberFormatter.stringDecimalToStringWithSeparator(withString: "\(expiredPointsValue)", withLocale: .current)
            
        } else {
            expiredPoints = uninformedValue
        }

        if rewards.rewardBreakDown.isEmpty || rewards.status == 204 {

            dateFromToTitle = Date.currentYear() ?? uninformedValue
            currentPoints = defaultValue
            previousPoints = defaultValue
            generatedPoints = defaultValue
            appliedPoints = defaultValue
            expiredPoints = defaultValue

            showInfoPoints = true
        }

        if dateFromToTitle == uninformedValue || currentPoints == uninformedValue || previousPoints == uninformedValue || generatedPoints == uninformedValue || appliedPoints == uninformedValue || expiredPoints == uninformedValue {
            showInfoPointsWithError = true
        }
    }

}
