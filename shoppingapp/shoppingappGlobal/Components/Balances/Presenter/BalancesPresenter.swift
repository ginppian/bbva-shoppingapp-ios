//
//  BalancesPresenter.swift
//  shoppingapp
//
//  Created by ruben.fernandez on 21/9/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation
import RxSwift
import CellsNativeComponents
import CellsNativeCore

class BalancesPresenter {

    weak var view: BalancesViewProtocol?
    let disposeBag = DisposeBag()
    static let shadowLeftSpace: Float = 20.0
    var showCreditCardLimitInfo = Settings.CardsLanding.show_credit_card_limit_info
}

extension BalancesPresenter: BalancesPresenterProtocol {

    func showBalanceInfo(withDisplayBalanceCard displayBalanceCard: DisplayBalancesCard, withWidth width: Float) {

        view?.showBalanceInfo(withDisplayBalanceCard: displayBalanceCard)

        if displayBalanceCard.cardBO?.cardType.id == .credit_card && showCreditCardLimitInfo {

            view?.showMoreHeight()

            view?.showMoreInformation(withDisplayBalanceCard: displayBalanceCard)

            view?.showProgressBar(withPercentage: calculateProgressPercentage(withDisplayBalanceCard: displayBalanceCard, withWith: width - BalancesPresenter.shadowLeftSpace))

            view?.showCreditCardContentView()

        } else {

            view?.showLessHeight()

            view?.hideCreditCardContentView()
        }

    }

    func calculateProgressPercentage(withDisplayBalanceCard displayBalance: DisplayBalancesCard, withWith width: Float) -> Float {

        return (Float(width) * displayBalance.balanceDisposed / displayBalance.balanceGranted)

    }

}
