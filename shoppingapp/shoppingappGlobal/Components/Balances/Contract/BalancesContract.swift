//
//  BalancesContract.swift
//  shoppingapp
//
//  Created by ruben.fernandez on 21/9/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import RxSwift

protocol BalancesViewProtocol: ComponentViewProtocol {
    func showBalanceInfo(withDisplayBalanceCard displayBalance: DisplayBalancesCard)
    func receiveBalanceInfoInView(withDisplayBalanceCard displayBalance: DisplayBalancesCard)
    func showMoreHeight()
    func showMoreInformation(withDisplayBalanceCard displayBalance: DisplayBalancesCard)
    func showProgressBar(withPercentage percentage: Float)
    func showCreditCardContentView()
    func hideCreditCardContentView()
    func showLessHeight()
}

protocol BalancesPresenterProtocol: ComponentPresenterProtocol {
    func showBalanceInfo(withDisplayBalanceCard displayBalanceCard: DisplayBalancesCard, withWidth width: Float)
}
