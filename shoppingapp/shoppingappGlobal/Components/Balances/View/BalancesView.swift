//
//  BalancesViewController.swift
//  shoppingapp
//
//  Created by ruben.fernandez on 21/9/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit
import RxSwift
import Lottie
import Stellar
import IQKeyboardManagerSwift
import Material
import CellsNativeCore
import CellsNativeComponents

protocol BalancesViewDelegate: class {

    func displayMoreInformation(height: CGFloat)

}

class BalancesView: XibView {

    // MARK: Constants
    var presenter = BalancesPresenter()

    @IBOutlet weak var containerBalancesView: UIView!
    @IBOutlet weak var balanceAmountLabel: UILabel!
    @IBOutlet weak var balanceConsumedTitleLabel: UILabel!
    @IBOutlet weak var balanceConsumedValueLabel: UILabel!
    @IBOutlet weak var balanceLimitTitleLabel: UILabel!
    @IBOutlet weak var balanceLimitValueLabel: UILabel!
    @IBOutlet weak var creditaCardLimitInfoView: UIView!
    @IBOutlet weak var customProgressView: CustomProgressView!

    var stringAccesibilieBalanceText: String?
    var stringAccesibilieBalance2Text: String?
    var stringAccesibilieBalance3Text: String?
    var stringAccesibilieicon1Text: String?
    var stringAccesibilieicon2Text: String?

    weak var delegate: BalancesViewDelegate?

    static let contentMoreHeight: CGFloat = 250.0
    static let contentLessHeight: CGFloat = 150.0

    // MARK: Custom Methos

    override func configureView() {

        super.configureView()

        containerBalancesView.backgroundColor = .BBVAWHITE

        balanceAmountLabel.textColor = .BBVA600

        balanceConsumedTitleLabel.textColor = .DARKAQUA
        balanceConsumedValueLabel.textColor = .DARKAQUA
        balanceLimitTitleLabel.textColor = .BBVA500
        balanceLimitValueLabel.textColor = .BBVA500

        balanceLimitValueLabel.font = Tools.setFontBook(size: 16)
        balanceLimitTitleLabel.font = Tools.setFontBook(size: 14)

        balanceConsumedValueLabel.font = Tools.setFontBook(size: 16)
        balanceConsumedTitleLabel.font = Tools.setFontBook(size: 14)

        presenter.view = self

    }

    func removeSubviews() {
        for subview in subviews {

            if subview.tag == 99 || subview.tag == 98 || subview.tag == 97 || subview.tag == 96 || subview.tag == 95 || subview.tag == 94 {
                subview.removeFromSuperview()
            }
        }
    }

}

extension BalancesView: BalancesViewProtocol {

    func receiveBalanceInfoInView(withDisplayBalanceCard displayBalance: DisplayBalancesCard) {
        presenter.showBalanceInfo(withDisplayBalanceCard: displayBalance, withWidth: Float(customProgressView.bounds.size.width))
    }

    func showBalanceInfo(withDisplayBalanceCard displayBalance: DisplayBalancesCard) {

        if let amont = displayBalance.amount {
            balanceAmountLabel.attributedText = amont
        } else {
            balanceAmountLabel.text = "-"
        }

        removeSubviews()

        if Tools.isStringNilOrEmpty(withString: displayBalance.balance2Text) && Tools.isStringNilOrEmpty(withString: displayBalance.balance3Text) && Tools.isStringNilOrEmpty(withString: displayBalance.icon1Image) && Tools.isStringNilOrEmpty(withString: displayBalance.icon2Image) && !Tools.isStringNilOrEmpty(withString: displayBalance.balanceText) {

            let labelBalance = UILabel()
            labelBalance.textColor = .BBVA500
            labelBalance.backgroundColor = .clear
            labelBalance.font = Tools.setFontBook(size: 14)
            labelBalance.text = displayBalance.balanceText
            labelBalance.tag = 99
            labelBalance.sizeToFit()
            let screen = UIScreen.main.bounds
            let screenWidth = screen.size.width
            labelBalance.frame = CGRect(x: screenWidth / 2 - labelBalance.frame.size.width / 2, y: balanceAmountLabel.frame.origin.y + balanceAmountLabel.frame.size.height + 16, width: labelBalance.frame.size.width, height: labelBalance.frame.size.height)
            addSubview(labelBalance)

            setAccessibility(forView: labelBalance, withAccessibilityId: stringAccesibilieBalanceText ?? "")

          // Associated
        } else if !Tools.isStringNilOrEmpty(withString: displayBalance.balance2Text) && !Tools.isStringNilOrEmpty(withString: displayBalance.balance3Text) && !Tools.isStringNilOrEmpty(withString: displayBalance.icon1Image) && !Tools.isStringNilOrEmpty(withString: displayBalance.icon2Image) && !Tools.isStringNilOrEmpty(withString: displayBalance.balanceText) {

            let screen = UIScreen.main.bounds
            let screenWidth = screen.size.width

            let viewContainer = UIView(frame: CGRect(x: 0, y: 0, width: Int(screenWidth), height: 18))
            viewContainer.backgroundColor = UIColor.clear
            viewContainer.tag = 94

            let labelBalance = UILabel()
            labelBalance.textColor = .BBVA500
            labelBalance.backgroundColor = .clear
            labelBalance.font = Tools.setFontBook(size: 14)
            labelBalance.text = displayBalance.balanceText
            labelBalance.tag = 99
            labelBalance.sizeToFit()
            labelBalance.frame = CGRect(x: 0, y: viewContainer.frame.height / 2 - labelBalance.frame.size.height / 2, width: labelBalance.frame.size.width, height: labelBalance.frame.size.height)

            setAccessibility(forView: labelBalance, withAccessibilityId: stringAccesibilieBalanceText ?? "")

            viewContainer.addSubview(labelBalance)

            let icon1 = UIImageView(frame: CGRect(x: labelBalance.frame.width + 6, y: (viewContainer.frame.height / 2) - 7, width: 15, height: 15))

            icon1.image = ImageUtilities.image(with: SVGCache.image(forSVGNamed: displayBalance.icon1Image!, color: .BBVA500), width: 15, height: 15)
            icon1.tag = 98
            viewContainer.addSubview(icon1)

            setAccessibility(forView: icon1, withAccessibilityId: stringAccesibilieicon1Text ?? "")

            let labelBalance2 = UILabel()
            labelBalance2.textColor = .BBVA500
            labelBalance2.backgroundColor = .clear
            labelBalance2.font = Tools.setFontBook(size: 14)
            labelBalance2.text = displayBalance.balance2Text
            labelBalance2.tag = 97
            labelBalance2.sizeToFit()
            labelBalance2.frame = CGRect(x: labelBalance.frame.width + 6 + icon1.frame.width + 6, y: viewContainer.frame.height / 2 - labelBalance2.frame.size.height / 2, width: labelBalance2.frame.size.width, height: labelBalance2.frame.size.height)
            viewContainer.addSubview(labelBalance2)

            setAccessibility(forView: labelBalance2, withAccessibilityId: stringAccesibilieBalance2Text ?? "")

            let icon2 = UIImageView(frame: CGRect(x: labelBalance.frame.width + 6 + icon1.frame.width + 6 + labelBalance2.frame.width + 6, y: viewContainer.frame.height / 2 - (2), width: 4, height: 4))

            icon2.image = ImageUtilities.image(with: SVGCache.image(forSVGNamed: displayBalance.icon2Image!, color: .BBVA500), width: 2, height: 2)
            icon2.tag = 96
            viewContainer.addSubview(icon2)

            setAccessibility(forView: icon2, withAccessibilityId: stringAccesibilieicon2Text ?? "")

            let labelBalance3 = UILabel()
            labelBalance3.textColor = .BBVA500
            labelBalance3.backgroundColor = .clear
            labelBalance3.font = Tools.setFontBook(size: 14)
            labelBalance3.text = displayBalance.balance3Text
            labelBalance3.tag = 95
            labelBalance3.sizeToFit()
            labelBalance3.frame = CGRect(x: labelBalance.frame.width + 6 + icon1.frame.width + 6 + labelBalance2.frame.width + 6 + icon2.frame.width + 3, y: viewContainer.frame.height / 2 - labelBalance3.frame.size.height / 2, width: labelBalance3.frame.size.width, height: labelBalance3.frame.size.height)
            viewContainer.addSubview(labelBalance3)

            setAccessibility(forView: labelBalance3, withAccessibilityId: stringAccesibilieBalance3Text ?? "")

            viewContainer.frame = CGRect(x: screenWidth / 2 - (labelBalance.frame.width + 6 + icon1.frame.width + 6 + labelBalance2.frame.width + 6 + icon2.frame.width + 6 + labelBalance3.frame.size.width) / 2, y: balanceAmountLabel.frame.origin.y + balanceAmountLabel.frame.size.height + 16, width: labelBalance.frame.width + 6 + icon1.frame.width + 6 + labelBalance2.frame.width + 6 + icon2.frame.width + 6 + labelBalance3.frame.size.width, height: viewContainer.frame.size.height)

            viewContainer.isAccessibilityElement = false

            addSubview(viewContainer)

        }

    }
    func showMoreHeight() {
        // tell cardsView that should show more information
        delegate?.displayMoreInformation(height: BalancesView.contentMoreHeight)
    }

    func showMoreInformation(withDisplayBalanceCard displayBalance: DisplayBalancesCard) {

        if let balanceConsumendValue = displayBalance.balanceConsumedValue {
            balanceConsumedValueLabel.attributedText = balanceConsumendValue
        } else {
            balanceConsumedValueLabel.text = "-"
        }

        if let balanceLimitValue = displayBalance.balanceLimitValue {
            balanceLimitValueLabel.attributedText = balanceLimitValue
        } else {
            balanceLimitValueLabel.text = "-"
        }

        balanceConsumedTitleLabel.text = displayBalance.balanceConsumedTitle
        balanceLimitTitleLabel.text = displayBalance.balanceLimitTitle

    }

    func showProgressBar(withPercentage percentage: Float) {
        customProgressView.setTopProgress(progress: CGFloat(percentage))
    }

    func showCreditCardContentView() {
        creditaCardLimitInfoView.isHidden = false
    }

    func showLessHeight() {
        // tell cardsView that should show Less information
        delegate?.displayMoreInformation(height: BalancesView.contentLessHeight)
    }

    func hideCreditCardContentView() {
        creditaCardLimitInfoView.isHidden = true
    }

}

// MARK: - APPIUM

private extension BalancesView {

    func setAccessibility(forView view: UIView, withAccessibilityId accessibilityId: String) {

        #if APPIUM

            Tools.setAccessibility(view: view, identifier: accessibilityId)

        #endif
    }
}
