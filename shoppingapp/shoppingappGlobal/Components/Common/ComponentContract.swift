//
//  File.swift
//  shoppingapp
//
//  Created by jesus.martinez on 27/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

protocol ComponentPresenterProtocol: class {

}

protocol ComponentViewProtocol: class {

}
