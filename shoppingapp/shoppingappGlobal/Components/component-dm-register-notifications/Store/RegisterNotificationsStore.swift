//
//  RegisterNotificartionsStore.swift
//  shoppingapp
//
//  Created by Marcos on 25/9/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeComponents
import BBVA_Network

open class RegisterNotificationsStore: NetworkStore {

    public static let ID: String = "component-data-manager-register-notification-store"

    public static let ON_REGISTER_SUCCESS = ActionSpec<RegisterNotificationsResponseDTO>(id: "on-register-success")
    static let ON_REGISTER_ERROR = ActionSpec<ErrorBO>(id: "on-register-error")

    static let serviceId = "PUSH_NOTIFICATIONS"

    var host: String?
    var countryCode: String?

    fileprivate var registerNotificationsUseCase: RegisterNotificationsUseCase?
    fileprivate var worker: NetworkWorker

    public init(worker: NetworkWorker, host: String? = nil) {

        self.worker = worker
        self.host = host

        super.init(RegisterNotificationsStore.ID)

        guard host == nil || host!.isEmpty else {
            return
        }

        configureStoreWithProperties()
    }

    public required init() {
        fatalError("init() has not been implemented")
    }

    fileprivate func configureStoreWithProperties() {

        newProperty(key: "host") { [weak self] (value: Any?) in

            if let value = value {
                self?.host = String(describing: value)
            }
        }
        newProperty(key: "country") { [weak self] (value: Any?) in

            if let value = value {
                self?.countryCode = String(describing: value)
            }
        }
    }

    override open func onPropertiesBounds() {

        guard let host = host else {
            return
        }

        let registerNotificationRepository: RegisterNotificationsRepository = RegisterNotificationsRepositoryImpl(baseURL: host, networkWorker: worker)
        self.registerNotificationsUseCase = RegisterNotificationsUseCase(registerNotificationsRepository: registerNotificationRepository)
    }

    override open func dispose() {
        registerNotificationsUseCase?.dispose()
        super.dispose()
    }

    override public func onUseCaseError(error: Error) {
        print("onUseCaseError: \(error)")

        guard let evaluate = error as? ServiceError else {
            return
        }

        switch evaluate {
        case .GenericErrorEntity(let errorEntity):
            let errorBVABO = ErrorBO(error: errorEntity)
            self.fire(actionSpec: RegisterNotificationsStore.ON_REGISTER_ERROR, value: errorBVABO)
        default:
            break
        }
    }

    // MARK: External funcs

    public func registerNotificationsToken(registerNotificationsToken: RegisterNotificationsTokenDTO) {
        
        let useCaseCallback = UseCase<RegisterNotificationsResponseDTO, RegisterNotificationsTokenDTO>.UseCaseCallback<RegisterNotificationsResponseDTO>(onUseCaseComplete: { result in
            self.fire(actionSpec: RegisterNotificationsStore.ON_REGISTER_SUCCESS, value: result)
        }, onUseCaseError: self.onUseCaseError)
        
        self.registerNotificationsUseCase?.execute(params: registerNotificationsToken, useCaseCallback: useCaseCallback)
    }
}
