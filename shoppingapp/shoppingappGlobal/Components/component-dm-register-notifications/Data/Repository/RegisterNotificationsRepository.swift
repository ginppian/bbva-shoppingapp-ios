//
//  RegisterNotificartionsRepository.swift
//  shoppingapp
//
//  Created by Marcos on 25/9/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

public protocol RegisterNotificationsRepository {

    func registerNotificationToken(registerNotificationsTokenDTO: RegisterNotificationsTokenDTO, success: @escaping (_ result: RegisterNotificationsResponseDTO) -> Void, error: @escaping (_ error: Error) -> Void)
}
