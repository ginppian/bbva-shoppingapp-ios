//
//  RegisterNotificartionsRepositoryImpl.swift
//  shoppingapp
//
//  Created by Marcos on 25/9/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import BBVA_Network

class RegisterNotificationsRepositoryImpl: BaseRepositoryImpl, RegisterNotificationsRepository {

    fileprivate static let REGISTER_NOTIFICATION_URL = "/devices/"
    fileprivate static let REGISTER_NOTIFICATION_URL_APPLICATIONS = "/applications/"
    fileprivate static let REGISTER_NOTIFICATION_URL_SERVICES = "/services/"

    func registerNotificationToken(registerNotificationsTokenDTO: RegisterNotificationsTokenDTO, success: @escaping (_ result: RegisterNotificationsResponseDTO) -> Void, error: @escaping (Error) -> Void) {

        let url = baseURL +
            RegisterNotificationsRepositoryImpl.REGISTER_NOTIFICATION_URL +
                registerNotificationsTokenDTO.deviceId + RegisterNotificationsRepositoryImpl.REGISTER_NOTIFICATION_URL_APPLICATIONS + registerNotificationsTokenDTO.applicationId + RegisterNotificationsRepositoryImpl.REGISTER_NOTIFICATION_URL_SERVICES + registerNotificationsTokenDTO.serviceId

        let body = RegisterNotificationsTokenBody(token: registerNotificationsTokenDTO.token)

        guard let bodyParser = body.toJSON() else {
            return
        }

        let request: NetworkRequest = createRequest(url, Headers.BASIC_HEADER, bodyParser, .useProtocolCachePolicy, 30, NetworkMethod.PATCH)

        _ = networkWorker.execute(request: request) { (response: Response) in

            if response.error == nil {

                    if  let dataJson = response.body {

                        if let registerNotificationsResponseDTO = RegisterNotificationsResponseDTO.deserialize(from: dataJson) {
                            success(registerNotificationsResponseDTO)
                        } else {
                            error(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
                        }

                    } else {
                        error(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
                    }

            } else {

                if let serviceError = self.checkShowError(response: response, security: false) {
                    error(serviceError)
                }
            }
        }
    }
}
