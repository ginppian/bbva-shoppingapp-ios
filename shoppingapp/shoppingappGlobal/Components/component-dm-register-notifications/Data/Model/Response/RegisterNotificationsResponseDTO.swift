//
//  RegisterNotificationsRsponseDTO.swift
//  shoppingapp
//
//  Created by Marcos on 26/9/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import HandyJSON

open class RegisterNotificationsResponseDTO: CellsDTO, HandyJSON {

    var data: RegisterNotificationsResponse?

    public required init() {}
}

struct RegisterNotificationsResponse: HandyJSON {

    var token: String?
}
