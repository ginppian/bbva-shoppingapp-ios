//
//  RegisterNotificartionsDTO.swift
//  shoppingapp
//
//  Created by Marcos on 25/9/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import HandyJSON

open class RegisterNotificationsTokenDTO: CellsDTO {

    var token: String
    var deviceId: String
    var applicationId: String
    var serviceId: String

    init(token: String, deviceId: String, applicationId: String, serviceId: String) {

        self.token = token
        self.deviceId = deviceId
        self.applicationId = applicationId
        self.serviceId = serviceId
    }
}

public class RegisterNotificationsTokenBody: HandyJSON {

    let token: String

    init(token: String) {
        self.token = token
    }

    public required init() {
        token = ""
    }
}
