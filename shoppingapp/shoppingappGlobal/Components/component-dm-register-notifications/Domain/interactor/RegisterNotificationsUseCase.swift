//
//  RegisterNotificartionsUseCase.swift
//  shoppingapp
//
//  Created by Marcos on 25/9/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

import CellsNativeComponents

class RegisterNotificationsUseCase: UseCaseAsync<RegisterNotificationsResponseDTO, RegisterNotificationsTokenDTO> {

    private let registerNotificationsRepository: RegisterNotificationsRepository

    init(registerNotificationsRepository: RegisterNotificationsRepository) {

        self.registerNotificationsRepository = registerNotificationsRepository
        super.init()
    }

    override func call(params: RegisterNotificationsTokenDTO?, success: @escaping (RegisterNotificationsResponseDTO) -> Void, error: @escaping (_ error: Error) -> Void) {

        if let params = params {
            self.registerNotificationsRepository.registerNotificationToken(registerNotificationsTokenDTO: params, success: success, error: error)
        } else {
            DLog(message: "Error no params")
            error(ServiceError.GenericErrorEntity(error: ErrorEntity(message: "Error no params")))
        }
    }
}
