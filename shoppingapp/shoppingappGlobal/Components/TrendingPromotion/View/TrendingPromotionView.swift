//
//  TrendingPromotionView.swift
//  shoppingapp
//
//  Created by Javier Pino on 17/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import Shimmer

class TrendingPromotionView: XibView {

    @IBOutlet weak var headerLabel: UILabel!
    @IBOutlet weak var headerArrowImageView: UIImageView!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var promotionsCollectionView: UICollectionView!
    @IBOutlet weak var promotionsCollectionViewFlowLayout: UICollectionViewFlowLayout!
    @IBOutlet weak var connectionErrorView: UIView!

    @IBOutlet weak var skeletonsContainerView: UIView!
    @IBOutlet weak var imageSkeletonView: FBShimmeringView!
    @IBOutlet weak var commerceNameSkeletonView: FBShimmeringView!
    @IBOutlet weak var descriptionSkeletonView: FBShimmeringView!
    @IBOutlet weak var benefitSkeletonView: FBShimmeringView!
    @IBOutlet weak var distanceSkeletonView: FBShimmeringView!

    @IBOutlet weak var errorView: InformationView!

    let presenter = TrendingPromotionPresenter()

    var displayData: DisplayItemPromotion?

    override func configureView() {

        super.configureView()

        presenter.view = self

        backgroundColor = .BBVAWHITE

        backgroundImageView.image = UIImage(named: ConstantsImages.Common.city_promo)
        backgroundImageView.contentMode = .scaleAspectFill

        configureHeaderView(highlighted: false)
        configureSkeletonView()
        configureErrorView()
        configurePromotionsCollectionView()

    }

    // MARK: - IBAction methods

    @IBAction func headerButtonTouchDown(_ sender: Any) {

        configureHeaderView(highlighted: true)
    }

    @IBAction func headerButtonDragExit(_ sender: Any) {

        configureHeaderView(highlighted: false)
    }

    @IBAction func headerButtonDragEnter(_ sender: Any) {

        configureHeaderView(highlighted: true)
    }

    @IBAction func headerButtonTouchUpInside(_ sender: Any) {

        configureHeaderView(highlighted: false)
        presenter.headerViewPressed()
    }
}

// MARK: - Private methods
private extension TrendingPromotionView {

    func configureHeaderView(highlighted: Bool) {

        headerLabel.font = Tools.setFontBold(size: 13.0)
        headerLabel.text = Localizables.promotions.key_see_promotions

        if highlighted {

            headerLabel.textColor = .COREBLUE
            headerArrowImageView.image = SVGCache.image(forSVGNamed: ConstantsImages.Common.forward_icon, color: .COREBLUE)

        } else {

            headerLabel.textColor = .BBVA500
            headerArrowImageView.image = SVGCache.image(forSVGNamed: ConstantsImages.Common.forward_icon, color: .MEDIUMBLUE)
        }
    }

    func configureSkeletonView() {

        skeletonsContainerView.backgroundColor = .BBVAWHITE

        var skeletonImage = SVGCache.image(forSVGNamed: ConstantsImages.Skeleton.lines, color: .BBVA300)
        skeletonImage = skeletonImage.resizableImage(withCapInsets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 20))

        let skeletonContentView = UIView(frame: imageSkeletonView.frame)
        skeletonContentView.backgroundColor = .BBVA300
        imageSkeletonView.backgroundColor = .BBVAWHITE
        imageSkeletonView.contentView = skeletonContentView

        commerceNameSkeletonView.backgroundColor = .BBVAWHITE
        commerceNameSkeletonView.contentView = UIImageView(image: skeletonImage)
        descriptionSkeletonView.backgroundColor = .BBVAWHITE
        descriptionSkeletonView.contentView = UIImageView(image: skeletonImage)
        benefitSkeletonView.backgroundColor = .BBVAWHITE
        benefitSkeletonView.contentView = UIImageView(image: skeletonImage)
        distanceSkeletonView.backgroundColor = .BBVAWHITE
        distanceSkeletonView.contentView = UIImageView(image: skeletonImage)

        skeletonsContainerView.addShadow(color: .black, opacity: 0.33)
    }

    func configureErrorView() {

        errorView.delegate = self

        let displayData = InformationViewDisplayData(iconImage: SVGCache.image(forSVGNamed: ConstantsImages.Common.alert_icon, color: .BBVALIGHTRED),
                                                     message: Localizables.common.key_error_ocurred_try_again_text,
                                                     actionButtonTitle: Localizables.common.key_retry_text)
        errorView.configure(displayData: displayData)

        errorView.addShadow(color: .black, opacity: 0.33)
    }

    func configurePromotionsCollectionView() {

        promotionsCollectionView.dataSource = self
        promotionsCollectionView.delegate = self
        promotionsCollectionView.backgroundColor = .clear
        promotionsCollectionView.allowsMultipleSelection = false
        promotionsCollectionView.scrollsToTop = false
        promotionsCollectionView.bounces = false
        promotionsCollectionView.alwaysBounceVertical = false
        promotionsCollectionView.alwaysBounceHorizontal = false
        promotionsCollectionView.isPagingEnabled = false
        promotionsCollectionView.showsHorizontalScrollIndicator = false
        promotionsCollectionView.showsVerticalScrollIndicator = false
        promotionsCollectionView.delaysContentTouches = false
        promotionsCollectionView.decelerationRate = UIScrollViewDecelerationRateFast

        promotionsCollectionView.register(UINib(nibName: PromotionCollectionViewCell.identifier, bundle: nil), forCellWithReuseIdentifier: PromotionCollectionViewCell.identifier)

        let promotionsBetweenCellsSpacing = CGFloat(10.0)
        let promotionsSidesMargin = CGFloat(20.0)
        let promotionsCellWidth = UIScreen.main.bounds.width - promotionsSidesMargin * 2.0

        promotionsCollectionViewFlowLayout = promotionsCollectionView.collectionViewLayout as? UICollectionViewFlowLayout
        promotionsCollectionViewFlowLayout.scrollDirection = UICollectionViewScrollDirection.horizontal
        promotionsCollectionViewFlowLayout.sectionInset = UIEdgeInsets(top: 0, left: promotionsSidesMargin, bottom: 0, right: promotionsSidesMargin)
        promotionsCollectionViewFlowLayout.minimumLineSpacing = promotionsBetweenCellsSpacing
        promotionsCollectionViewFlowLayout.itemSize = CGSize(width: promotionsCellWidth, height: PromotionCollectionViewCell.height)

    }

    func startSkeletonAnimation() {

        imageSkeletonView.isShimmering = true
        commerceNameSkeletonView.isShimmering = true
        descriptionSkeletonView.isShimmering = true
        benefitSkeletonView.isShimmering = true
        distanceSkeletonView.isShimmering = true
    }

    func stopSkeletonAnimation() {

        imageSkeletonView.isShimmering = false
        commerceNameSkeletonView.isShimmering = false
        descriptionSkeletonView.isShimmering = false
        benefitSkeletonView.isShimmering = false
        distanceSkeletonView.isShimmering = false
    }
}

// MARK: - TrendingPromotionViewProtocol implementation
extension TrendingPromotionView: TrendingPromotionViewProtocol {

    func showSkeletonView() {

        skeletonsContainerView.isHidden = false
        startSkeletonAnimation()
        errorView.isHidden = true
        promotionsCollectionView.isHidden = true
    }

    func showConnectionErrorView() {

        errorView.isHidden = false
        skeletonsContainerView.isHidden = true
        stopSkeletonAnimation()
        promotionsCollectionView.isHidden = true
    }

    func showTrendingPromotionView(displayData: DisplayItemPromotion) {

        promotionsCollectionView.isHidden = false
        errorView.isHidden = true
        skeletonsContainerView.isHidden = true
        stopSkeletonAnimation()

        self.displayData = displayData
        promotionsCollectionView.reloadData()
    }
}

// MARK: - UICollectionViewDataSource implementation
extension TrendingPromotionView: UICollectionViewDelegate, UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        if displayData != nil {

            return 1
        }
        return 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        guard let promotionsDisplayData = displayData else {
            return UICollectionViewCell()

        }

        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PromotionCollectionViewCell.identifier, for: indexPath) as? PromotionCollectionViewCell {

            cell.configureCellWithDisplayPromotionData(withDisplayitemPromotion: promotionsDisplayData)

            return cell
        }

        return UICollectionViewCell()
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        presenter.trendingPromotionViewPressed()
    }
}

// MARK: - InformationViewDelegate implementation
extension TrendingPromotionView: InformationViewDelegate {

    func informationViewActionButtonPressed(_ informationView: InformationView) {

        presenter.connectionErrorRetryButtonPressed()
    }
}
