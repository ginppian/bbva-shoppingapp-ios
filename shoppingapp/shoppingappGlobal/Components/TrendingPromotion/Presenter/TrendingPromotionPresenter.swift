//
//  TrendingPromotionPresenter.swift
//  shoppingapp
//
//  Created by Javier Pino on 17/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import RxSwift

protocol TrendingPromotionPresenterDelegate: class {

    func trendingPromotionPresenter(_ trendingPromotionPresenter: TrendingPromotionPresenter, didReceiveError error: ErrorBO)
    func trendingPromotionHeaderPressed(_ trendingPromotionPresenter: TrendingPromotionPresenter)
    func trendingPromotionViewPressed(_ trendingPromotionPresenter: TrendingPromotionPresenter, promotion: PromotionBO)
    func trendingPromotionCardsForFilter(_ trendingPromotionPresenter: TrendingPromotionPresenter) -> CardsBO?
}

class TrendingPromotionPresenter {

    weak var delegate: TrendingPromotionPresenterDelegate?

    weak var view: TrendingPromotionViewProtocol?
    var interactor: TrendingPromotionInteractorProtocol = TrendingPromotionInteractor()

    let disposeBag = DisposeBag()
    let pageSize = 5
    var trendingPromotion: PromotionBO?
    var filterByCardsTitleIds = Settings.TrendingPromotionComponent.should_filter_by_card_title_id
}

// MARK: - Private methods
private extension TrendingPromotionPresenter {

    func loadTrendingPromotion() {

        view?.showSkeletonView()

        var titleIds: [String]?

        if filterByCardsTitleIds {

            let cards = delegate?.trendingPromotionCardsForFilter(self)

            titleIds = cards?.titleIds()
        }

        let params = ShoppingParamsTransport(trendingId: TrendingType.hot.rawValue, titleIds: titleIds, pageSize: pageSize, orderBy: .endDate)

        interactor.providePromotions(byParams: params).subscribe(
            onNext: { [weak self] result in

                if let promotion = result as? PromotionBO {
                    self?.trendingPromotion = promotion
                    self?.trendingPromotionLoadingSucceeded(promotion: promotion)
                }
            },
            onError: { [weak self] error in
                guard let `self` = self else {
                    return
                }

                guard let serviceError = error as? ServiceError else {
                    return
                }

                switch serviceError {

                case .GenericErrorBO(let errorBO):

                    self.trendingPromotionLoadingFailed()

                    if errorBO.sessionExpired() {
                        self.delegate?.trendingPromotionPresenter(self, didReceiveError: errorBO)
                    }

                default:
                    break
                }
            }).addDisposableTo(disposeBag)

    }

    func trendingPromotionLoadingFailed() {

        view?.showConnectionErrorView()
    }

    func trendingPromotionLoadingSucceeded(promotion: PromotionBO) {

        let displayData = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotion, withTypeCell: CellPromotionType.small)
        view?.showTrendingPromotionView(displayData: displayData)
    }
}

// MARK: - TrendingPromotionPresenterProtocol implementation
extension TrendingPromotionPresenter: TrendingPromotionPresenterProtocol {

    func viewLoaded() {

        loadTrendingPromotion()
    }

    func headerViewPressed() {

        delegate?.trendingPromotionHeaderPressed(self)
    }

    func connectionErrorRetryButtonPressed() {

        loadTrendingPromotion()
    }

    func trendingPromotionViewPressed() {

        if let trendingPromotion = trendingPromotion {
            delegate?.trendingPromotionViewPressed(self, promotion: trendingPromotion)
        }
    }
}
