//
//  TrendingPromotionInteractor.swift
//  shoppingapp
//
//  Created by Javier Pino on 17/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import RxSwift

class TrendingPromotionInteractor {

    var shoppingDataManager: ShoppingDataManager = ShoppingDataManager.sharedInstance
    var preferencesManager: PreferencesManager = PreferencesManager.sharedInstance()
    var randomGenerator: RandomGenerator = RandomGenerator()
    let disposeBag = DisposeBag()
}

// MARK: - Private methods
private extension TrendingPromotionInteractor {

    func randomPromotion(fromPromotions promotions: [PromotionBO]) -> PromotionBO {

        var filteredPromotions = promotions

        if let savedPromotionId = preferencesManager.getValueWithPreferencesKey(forKey: .kShowedTrendingPromotionId) as? String {

            filteredPromotions = promotions.filter { $0.id != savedPromotionId }
        }

        let randomPromotionIndex = randomGenerator.randomInt(fromZeroTo: filteredPromotions.count - 1)
        let randomPromotion = filteredPromotions[randomPromotionIndex]

        _ = preferencesManager.saveValueWithPreferencesKey(forValue: randomPromotion.id as AnyObject, withKey: .kShowedTrendingPromotionId)

        return randomPromotion
    }
}

// MARK: - TrendingPromotionInteractorProtocol implementation
extension TrendingPromotionInteractor: TrendingPromotionInteractorProtocol {

    func providePromotions(byParams params: ShoppingParamsTransport) -> Observable <ModelBO> {

        return Observable.create { observer in

            self.shoppingDataManager.providePromotions(byParams: params).subscribe(
                onNext: { result in

                    if let promotionsEntity = result as? PromotionsEntity {
                        let promotionsBO = PromotionsBO(promotionsEntity: promotionsEntity)

                        switch promotionsBO.promotions.count {

                        case ..<1:
                            // Production always returns promotions
                            observer.onNext(PromotionsBO())

                        case 1:
                            observer.onNext(promotionsBO.promotions[0])

                        default:
                            let promotion = self.randomPromotion(fromPromotions: promotionsBO.promotions)
                            observer.onNext(promotion)
                        }
                        observer.onCompleted()
                    }
                }, onError: { error in

                let evaluate: ServiceError = (error as? ServiceError)!

                switch evaluate {
                case .GenericErrorEntity(let errorEntity):
                    let errorBVABO = ErrorBO(error: errorEntity)
                    observer.onError(ServiceError.GenericErrorBO(error: errorBVABO))
                default:
                    observer.onError(evaluate)
                }

                }, onCompleted: {
                }).addDisposableTo(self.disposeBag)
            return Disposables.create()
        }
    }
}
