//
//  TrendingPromotionContract.swift
//  shoppingapp
//
//  Created by Javier Pino on 17/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import RxSwift

protocol TrendingPromotionViewProtocol: ComponentViewProtocol {

    func showSkeletonView()
    func showConnectionErrorView()
    func showTrendingPromotionView(displayData: DisplayItemPromotion)
}

protocol TrendingPromotionPresenterProtocol: ComponentPresenterProtocol {

    func viewLoaded()
    func headerViewPressed()
    func connectionErrorRetryButtonPressed()
    func trendingPromotionViewPressed()
}

protocol TrendingPromotionInteractorProtocol {

    func providePromotions(byParams params: ShoppingParamsTransport) -> Observable <ModelBO>
}
