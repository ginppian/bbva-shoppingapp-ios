//
//  ExpandableViewDisplayData.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 21/11/2017.
//  Copyright © 2017 BBVA. All rights reserved.
//

struct  ExpandableViewDisplayData {

    var pendingDescriptionText: String?
    var pendingIcon: String?
    var pendingTitle: String?

    static func generateExpandableDisplayData(withExpandableOption expandable: Bool, withButtonTitle title: String) -> ExpandableViewDisplayData {

        var expandableDisplayData = ExpandableViewDisplayData()

        expandableDisplayData.pendingTitle = title

        if expandable {
            expandableDisplayData.pendingDescriptionText = Localizables.transactions.key_card_movement_details_explanation_text
            expandableDisplayData.pendingIcon = ConstantsImages.Common.arrow_up
        } else {
            expandableDisplayData.pendingIcon = ConstantsImages.Common.arrow_down
        }

        return expandableDisplayData
    }
}
