//
//  ExpandableView.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 21/11/2017.
//  Copyright © 2017 BBVA. All rights reserved.
//
import UIKit

protocol ExpandableViewDelegate: class {
    func expandableView(_ expandableView: ExpandableView, withExpandable expandable: Bool)
}

class ExpandableView: XibView {

    @IBOutlet weak var pendingButton: UIButton!
    @IBOutlet weak var informationLabel: UILabel!
    @IBOutlet weak var pendingButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var expandableViewHeight: NSLayoutConstraint!
    static let pendingCollapsedHeight: CGFloat = 30.0
    static let padding: CGFloat = 10.0
    @IBOutlet weak var expandableView: UIView!

    weak var delegate: ExpandableViewDelegate?

    override func configureView() {
        super.configureView()
        configureViewItems()
    }

    func configureViewItems() {

        informationLabel.textColor = .BBVA600
        informationLabel.font = Tools.setFontBook(size: 14)
        informationLabel.textAlignment = .center
        informationLabel.numberOfLines = 0

        pendingButton.tintColor = .DARKAQUA
        pendingButton.contentMode = .scaleAspectFit
        pendingButton.setTitleColor(.DARKAQUA, for: .normal)
        pendingButton.semanticContentAttribute = .forceRightToLeft
        pendingButton.titleLabel?.font = Tools.setFontMediumItalic(size: 14.0)
        pendingButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: ExpandableView.padding, bottom: 0, right: 0)

    }

    @IBAction func expandViewAction(_ sender: Any) {
        pendingButton.isSelected = !pendingButton.isSelected
        if pendingButton.isSelected {
            self.expandableView.alpha = 0.0
            UIView.animate(withDuration: 1.0, animations: {
                self.expandableView.alpha = 1.0

            }, completion: nil)

        } else {
            self.expandableView.alpha = 1.0
            UIView.animate(withDuration: 1.0, animations: {
               self.expandableView.alpha = 0.0

            }, completion: nil)
        }
        delegate?.expandableView(self, withExpandable: pendingButton.isSelected)
    }

    func setPendingInformation(withPendingData  displayData: ExpandableViewDisplayData) {
        pendingButtonHeight.constant = ExpandableView.pendingCollapsedHeight
        pendingButton.setImage(UIImage(named: displayData.pendingIcon!), for: .normal)
        pendingButton.setTitle(displayData.pendingTitle, for: .normal)
        informationLabel.text = displayData.pendingDescriptionText
    }

}
