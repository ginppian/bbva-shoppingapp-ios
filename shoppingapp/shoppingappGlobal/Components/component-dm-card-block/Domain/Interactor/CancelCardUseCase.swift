//
//  CancelCardUseCase.swift
//  shoppingapp
//
//  Created by Marcos on 25/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import CellsNativeComponents

class CancelCardUseCase: UseCaseAsync<BlockCardDTO, CardCancelDTO> {

    private let cancelCardRepository: CancelCardRepository

    init(cancelCardRepository: CancelCardRepository) {

        self.cancelCardRepository = cancelCardRepository
        super.init()

    }

    override func call(params: CardCancelDTO?, success: @escaping (BlockCardDTO) -> Void, error: @escaping (Error) -> Void) {
        if let params = params {
            self.cancelCardRepository.cancelCard(params: params, success: success, error: error)
        } else {
            DLog(message: "Error no params")
            error(ServiceError.GenericErrorEntity(error: ErrorEntity(message: "Error no params")))
        }

    }

}
