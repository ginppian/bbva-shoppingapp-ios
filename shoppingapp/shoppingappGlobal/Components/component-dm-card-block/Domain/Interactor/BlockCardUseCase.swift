//
//  BlockCardUseCase.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 30/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import CellsNativeComponents

class BlockCardUseCase: UseCaseAsync<BlockCardDTO, CardCancelDTO> {

    private let cancelCardRepository: CancelCardRepository

    init(cancelCardRepository: CancelCardRepository) {

        self.cancelCardRepository = cancelCardRepository
        super.init()

    }

    override func call(params: CardCancelDTO?, success: @escaping (BlockCardDTO) -> Void, error: @escaping (Error) -> Void) {
        if let params = params {
            self.cancelCardRepository.blockCard(params: params, success: success, error: error)
        } else {
            DLog(message: "Error no params")
            error(ServiceError.GenericErrorEntity(error: ErrorEntity(message: "Error no params")))
        }

    }

}
