//
//  CardCancelDTO.swift
//  shoppingapp
//
//  Created by Marcos on 25/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

open class CardCancelDTO: CellsDTO {

    var cardId: String
    var blockType: BlockType = .lost

    init(cardId: String) {
        self.cardId = cardId
    }

    init(cardId: String, typeBlock: String) {
        self.cardId = cardId

        if let typeBlock = BlockType(rawValue: typeBlock) {
            blockType = typeBlock
        }
    }
}

enum BlockType: String {
    case stolen = "STOLEN"
    case lost = "LOST"
}
