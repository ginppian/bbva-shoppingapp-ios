//
//  BlockCardDTO.swift
//  shoppingapp
//
//  Created by Marcos on 30/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import HandyJSON

public class BlockCardDTO: CellsDTO, HandyJSON {

    var data: BlockCardData?

    public required init() {}

}

struct BlockCardData: HandyJSON {

    var blockId: String?
    var isActive: Bool?
    var reference: String?
    var blockDate: String?

}
