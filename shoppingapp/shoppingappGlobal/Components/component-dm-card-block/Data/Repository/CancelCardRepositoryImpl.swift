//
//  CancelCardRepositoryImpl.swift
//  shoppingapp
//
//  Created by Marcos on 25/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

import ObjectMapper
import BBVA_Network
import HandyJSON

class CancelCardRepositoryImpl: BaseRepositoryImpl, CancelCardRepository {

    fileprivate static let CANCEL_CARD_URL_PREFIX = "/cards/"
    fileprivate static let CANCEL_CARD_FINAL_URL_SUFFIX = "/blocks/"

    fileprivate static var CANCEL_CARD_HEADER = [Headers.CONTENT_TYPE: Headers.APP_JSON, Headers.UTF8: Headers.CHARSET_ACCEPTED]

    func cancelCard(params: CardCancelDTO, success: @escaping (BlockCardDTO) -> Void, error: @escaping (Error) -> Void) {

        let url = baseURL + CancelCardRepositoryImpl.CANCEL_CARD_URL_PREFIX + params.cardId + CancelCardRepositoryImpl.CANCEL_CARD_FINAL_URL_SUFFIX + "LOST/"
        let body = CancelCardBody()

        guard let bodyParser = body.toJSON() else {
            return
        }

        let request: NetworkRequest = createRequest(url, Headers.BASIC_HEADER, bodyParser, .useProtocolCachePolicy, 30, NetworkMethod.PUT)

        _ = networkWorker.execute(request: request) { (response: Response) in

            if response.error == nil {

                if let dataJson = response.body, let result = BlockCardDTO.deserialize(from: dataJson) {

                    success(result)

                } else {
                    error(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
                }

            } else {

                if let serviceError = self.checkShowError(response: response, security: false) {
                    error(serviceError)
                }
            }
        }
    }

    fileprivate static let BLOCK_CARD_URL_PREFIX = "/cards/"
    fileprivate static let BLOCK_CARD_FINAL_URL_SUFFIX = "/blocks/"

    func blockCard(params: CardCancelDTO, success: @escaping (BlockCardDTO) -> Void, error: @escaping (Error) -> Void) {

        let url = baseURL + CancelCardRepositoryImpl.BLOCK_CARD_URL_PREFIX + params.cardId + CancelCardRepositoryImpl.BLOCK_CARD_FINAL_URL_SUFFIX + params.blockType.rawValue + "/"

        let body = CancelCardBody()

        guard let bodyParser = body.toJSON() else {
            return
        }

        let request: NetworkRequest = createRequest(url, Headers.BASIC_HEADER, bodyParser, .useProtocolCachePolicy, 30, NetworkMethod.PUT)

        _ = networkWorker.execute(request: request) { (response: Response) in

            if response.error == nil {

                if let dataJson = response.body, let result = BlockCardDTO.deserialize(from: dataJson) {

                    success(result)

                } else {
                    error(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
                }

            } else {

                if let serviceError = self.checkShowError(response: response, security: false) {
                    error(serviceError)
                }
            }
        }
    }
}

struct CancelCardBody: HandyJSON {
    let isActive = true
}
