//
//  CancelCardRepository.swift
//  shoppingapp
//
//  Created by Marcos on 25/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

public protocol CancelCardRepository {

    func cancelCard(params: CardCancelDTO, success: @escaping (_ result: BlockCardDTO) -> Void, error: @escaping (_ error: Error) -> Void)
    func blockCard(params: CardCancelDTO, success: @escaping (_ result: BlockCardDTO) -> Void, error: @escaping (_ error: Error) -> Void)

}
