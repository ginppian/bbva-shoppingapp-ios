//
//  CancelCardStore.swift
//  shoppingapp
//
//  Created by Marcos on 25/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import CellsNativeComponents
import BBVA_Network

open class CancelCardStore: NetworkStore {

    // MARK: static references

    public static let ID: String = "component-data-manager-cancel-card-store"

    public static let ON_CANCEL_SUCCESS = ActionSpec<BlockCardDTO>(id: "on-cancel-success")
    public static let ON_BLOCK_SUCCESS = ActionSpec<BlockCardDTO>(id: "on-block-success")

    static let ON_CANCEL_ERROR = ActionSpec<ErrorBO>(id: "on-cancel-error")

    // MARK: public vars

    var host: String?
    var countryCode: String?

    // MARK: private vars

    fileprivate var cancelCardUseCase: CancelCardUseCase?
    fileprivate var blockCardUseCase: BlockCardUseCase?
    fileprivate var worker: NetworkWorker

    // MARK: init

    public init(worker: NetworkWorker, host: String? = nil) {

        self.worker = worker
        self.host = host

        super.init(CancelCardStore.ID)

        guard host == nil || (host?.isEmpty)! else {
            return
        }

        configureStoreWithProperties()
    }

    public required init() {
        fatalError("init() has not been implemented")
    }

    // MARK: override methods

    override open func onPropertiesBounds() {

        guard let host = host else {
            return
        }

        let cancelCardRepository: CancelCardRepository = CancelCardRepositoryImpl(baseURL: host, networkWorker: worker)

        cancelCardUseCase = CancelCardUseCase(cancelCardRepository: cancelCardRepository)

        blockCardUseCase = BlockCardUseCase(cancelCardRepository: cancelCardRepository)
    }

    override public func onUseCaseError(error: Error) {
        print("onUseCaseError: \(error)")

        guard let evaluate = error as? ServiceError else {
            return
        }

        switch evaluate {
        case .GenericErrorEntity(let errorEntity):
            let errorBVABO = ErrorBO(error: errorEntity)
            self.fire(actionSpec: CancelCardStore.ON_CANCEL_ERROR, value: errorBVABO)
        default:
            break
        }
    }

    override open func dispose() {
        cancelCardUseCase?.dispose()
        blockCardUseCase?.dispose()
        super.dispose()
    }

    // MARK: private methods

    fileprivate func configureStoreWithProperties() {

        newProperty(key: "host") { [weak self] (value: Any?) in

            if let value = value {
                self?.host = String(describing: value)
            }
        }
        newProperty(key: "country") { [weak self] (value: Any?) in

            if let value = value {
                self?.countryCode = String(describing: value)
            }
        }
    }

    // MARK: public methods

    public func cancelCardRequest(cardCancelDTO: CardCancelDTO) {

        let useCaseCallback = UseCase<BlockCardDTO, CardCancelDTO>.UseCaseCallback<BlockCardDTO>(onUseCaseComplete: { result in

                self.fire(actionSpec: CancelCardStore.ON_CANCEL_SUCCESS, value: result)
        }, onUseCaseError: self.onUseCaseError)

        cancelCardUseCase?.execute(params: cardCancelDTO, useCaseCallback: useCaseCallback)
    }

    public func blockCardRequest(cardCancelDTO: CardCancelDTO) {

        let useCaseCallback = UseCase<BlockCardDTO, CardCancelDTO>.UseCaseCallback<BlockCardDTO>(onUseCaseComplete: { result in

            self.fire(actionSpec: CancelCardStore.ON_BLOCK_SUCCESS, value: result)

        }, onUseCaseError: self.onUseCaseError)

        blockCardUseCase?.execute(params: cardCancelDTO, useCaseCallback: useCaseCallback)
    }
}
