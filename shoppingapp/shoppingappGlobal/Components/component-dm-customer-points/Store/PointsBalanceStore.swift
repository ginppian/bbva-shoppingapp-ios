//
//  PointsBalanceStore.swift
//  shoppingapp
//
//  Created by Javier Pino on 19/7/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import BBVA_Network
import CellsNativeComponents

public class PointsBalanceStore: NetworkStore {

    public static let ID: String = "component-data-manager-points-balance-store"

    public static let ON_GET_POINTS_BALANCE_SUCCESS = ActionSpec<PointsBalanceDTO>(id: "on-get-points-balance-success")
    static let ON_GET_POINTS_BALANCE_ERROR = ActionSpec<ErrorBO>(id: "on-get-points-balance-error")

    var host: String?
    var countryCode: String?

    fileprivate var getPointsBalanceUseCase: GetPointsBalanceUseCase?
    fileprivate var worker: NetworkWorker

    public init(worker: NetworkWorker, host: String? = nil) {

        self.worker = worker
        self.host = host

        super.init(PointsBalanceStore.ID)

        guard host == nil || (host?.isEmpty)! else {
            return
        }

        configureStoreWithProperties()
    }

    public required init() {
        fatalError("init() has not been implemented")
    }

    fileprivate func configureStoreWithProperties() {

        newProperty(key: "host") { [weak self] (value: Any?) in

            if let value = value {
                self?.host = String(describing: value)
            }
        }
        newProperty(key: "country") { [weak self] (value: Any?) in

            if let value = value {
                self?.countryCode = String(describing: value)
            }
        }
    }

    public func getPointsBalance() {

        let useCaseCallback = UseCase<PointsBalanceDTO, Void>.UseCaseCallback<PointsBalanceDTO>(onUseCaseComplete: { result in
            self.fire(actionSpec: PointsBalanceStore.ON_GET_POINTS_BALANCE_SUCCESS, value: result)
        }, onUseCaseError: self.onUseCaseError)

        getPointsBalanceUseCase?.execute(params: nil, useCaseCallback: useCaseCallback)
    }

    override public func onPropertiesBounds() {

        guard let host = host else {
            return
        }

        let pointsBalanceRepository = PointsBalanceRepositoryImpl(baseURL: host, networkWorker: worker)
        getPointsBalanceUseCase = GetPointsBalanceUseCase(pointsBalanceRepository: pointsBalanceRepository)
    }

    override public func dispose() {

        getPointsBalanceUseCase?.dispose()
        super.dispose()
    }

    override public func onUseCaseError(error: Error) {

        print("onUseCaseError: \(error)")

        guard let evaluate = error as? ServiceError else {
            return
        }

        switch evaluate {
        case .GenericErrorEntity(let errorEntity):
            let errorBVABO = ErrorBO(error: errorEntity)
            fire(actionSpec: PointsBalanceStore.ON_GET_POINTS_BALANCE_ERROR, value: errorBVABO)
        default:
            break
        }
    }

}
