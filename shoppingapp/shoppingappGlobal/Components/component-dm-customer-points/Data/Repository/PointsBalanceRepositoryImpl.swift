//
//  PointsBalanceRepositoryImpl.swift
//  shoppingapp
//
//  Created by Javier Pino on 18/7/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import BBVA_Network

class PointsBalanceRepositoryImpl: BaseRepositoryImpl, PointsBalanceRepository {

    fileprivate static let POINTS_BALANCE_URL = "/points/balances"

    func getPointsBalance(success: @escaping (PointsBalanceDTO) -> Void, error: @escaping (Error) -> Void) {

        let url = baseURL + PointsBalanceRepositoryImpl.POINTS_BALANCE_URL

        let request = createRequest(url, Headers.BASIC_HEADER, nil, .useProtocolCachePolicy, 30, NetworkMethod.GET)

        _ = networkWorker.execute(request: request) { (response: Response) in

            if response.error == nil {

                if  let dataJson = response.body, !dataJson.isEmpty {

                    if let pointsBalanceDTO = PointsBalanceDTO.deserialize(from: dataJson) {
                        success(pointsBalanceDTO)
                    } else {
                        error(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
                    }

                } else {
                    let pointsBalanceDTO = PointsBalanceDTO()
                    pointsBalanceDTO.data = PointsBalanceDataDTO()

                    success(pointsBalanceDTO)

                }

            } else {

                if let serviceError = self.checkShowError(response: response, security: false) {
                    error(serviceError)
                }
            }

        }
    }
}
