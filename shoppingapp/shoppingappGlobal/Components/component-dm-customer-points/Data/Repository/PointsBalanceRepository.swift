//
//  PointsBalanceRepository.swift
//  shoppingapp
//
//  Created by Javier Pino on 18/7/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

public protocol PointsBalanceRepository {

    func getPointsBalance(success: @escaping (_ result: PointsBalanceDTO) -> Void, error: @escaping (_ error: Error) -> Void)

}
