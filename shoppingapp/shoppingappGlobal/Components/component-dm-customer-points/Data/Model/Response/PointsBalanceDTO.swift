//
//  PointsBalanceDTO.swift
//  shoppingapp
//
//  Created by Javier Pino on 18/7/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import HandyJSON

open class PointsBalanceDTO: CellsDTO, HandyJSON {

    var data: PointsBalanceDataDTO?

    public required init() {}
}

struct PointsBalanceDataDTO: HandyJSON {

    var accumulatedPoints: PointsBalanceAccumulatedPointsDTO?
    var pointsToBeExpired: PointsBalancePointsToBeExpiredDTO?
    var expiredPoints: PointsBalanceExpiredPointsDTO?
    var totalSaved: PointsBalanceTotalSavedDTO?
    var areRedeemable: Bool?
}

struct PointsBalanceAccumulatedPointsDTO: HandyJSON {

    var amount: NSNumber?
    var exchangeUnit: PointsBalanceExchangeUnitDTO?
}

struct PointsBalanceExchangeUnitDTO: HandyJSON {

    var amount: NSNumber?
    var currency: String?
}

struct PointsBalancePointsToBeExpiredDTO: HandyJSON {

    var amount: NSNumber?
    var expirationDate: String?
}

struct PointsBalanceExpiredPointsDTO: HandyJSON {

    var amount: NSNumber?
}

struct PointsBalanceTotalSavedDTO: HandyJSON {

    var amount: NSNumber?
    var currency: String?
}
