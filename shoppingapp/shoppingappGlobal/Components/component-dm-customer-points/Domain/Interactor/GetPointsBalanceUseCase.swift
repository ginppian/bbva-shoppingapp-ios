//
//  GetPointsBalanceUseCase.swift
//  shoppingapp
//
//  Created by Javier Pino on 19/7/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeComponents

class GetPointsBalanceUseCase: UseCaseAsync<PointsBalanceDTO, Void> {

    private let pointsBalanceRepository: PointsBalanceRepository

    init(pointsBalanceRepository: PointsBalanceRepository) {

        self.pointsBalanceRepository = pointsBalanceRepository
        super.init()
    }

    override func call(params: Void?, success: @escaping (PointsBalanceDTO) -> Void, error: @escaping (Error) -> Void) {

        pointsBalanceRepository.getPointsBalance(success: success, error: error)
    }

}
