//
//  TransactionDetailComponentPresenter.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 20/11/2017.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation

class  TicketDetailComponentPresenter {
    var view: TicketDetailViewComponentProtocol?
}

extension TicketDetailComponentPresenter: TicketDetailComponentPresenterProtocol {

    func showTicketdata(withTicketData ticketData: TicketDisplayData) {
        view?.showTicketdata(withTicketData: ticketData)
    }

}
