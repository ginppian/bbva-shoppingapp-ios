//
//  TransactionDetailView.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 20/11/2017.
//  Copyright © 2017 BBVA. All rights reserved.
//
import Foundation

class TicketDetailView: XibView {

    @IBOutlet weak var topImageView: UIImageView!
    @IBOutlet weak var cardImageView: UIImageView!
    @IBOutlet weak var panLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var conceptLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!

    var presenter = TicketDetailComponentPresenter()

    override func configureView() {

        super.configureView()

        configureViewItems()

        presenter.view = self

        setAccessibilities()
    }

    func configureViewItems() {
        panLabel.font = Tools.setFontBookItalic(size: 14)
        panLabel.textColor = .BBVA600

        dateLabel.font = Tools.setFontBookItalic(size: 14)
        dateLabel.textColor = .BBVA600

        conceptLabel.font = Tools.setFontBook(size: 16)
        conceptLabel.textColor = .BBVA600

        amountLabel.font = Tools.setFontLight(size: 48)
        amountLabel.textColor = .BBVA600

        typeLabel.font = Tools.setFontMediumItalic(size: 14)
    }

    func getTicketDisplayData(withTicketDisplayData ticketDisplayData: TicketDisplayData) {
        presenter.showTicketdata(withTicketData: ticketDisplayData)
    }

}

extension TicketDetailView: TicketDetailViewComponentProtocol {

    func showTicketdata(withTicketData ticketData: TicketDisplayData) {
        panLabel.text = ticketData.pan
        dateLabel.text = ticketData.date
        conceptLabel.text = ticketData.concept
        amountLabel.attributedText = ticketData.amount
        typeLabel.text = ticketData.type
        typeLabel.textColor = ticketData.typeColor

        cardImageView.image = ticketData.image

    }

}

// MARK: - APPIUM
private extension TicketDetailView {

    func setAccessibilities() {
        #if APPIUM

        #endif
    }

    func setAccessibilities(forPendingStatus pendingStatus: Bool) {
        #if APPIUM

        #endif
    }

}
