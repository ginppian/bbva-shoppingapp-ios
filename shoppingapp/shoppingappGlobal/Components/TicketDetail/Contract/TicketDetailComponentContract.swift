//
//  TransactionDetailComponentContract.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 20/11/2017.
//  Copyright © 2017 BBVA. All rights reserved.
//

protocol TicketDetailViewComponentProtocol: ComponentViewProtocol {
    func showTicketdata(withTicketData ticketData: TicketDisplayData)
}

protocol TicketDetailComponentPresenterProtocol: ComponentPresenterProtocol {
    func showTicketdata(withTicketData ticketData: TicketDisplayData)
}
