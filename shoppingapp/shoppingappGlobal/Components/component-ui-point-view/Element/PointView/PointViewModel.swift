//
//  PointViewModel.swift
//  shoppingappGlobal
//
//  Created by ignacio.bonafonte on 18/07/2018.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit
import CellsNativeCore
import CellsNativeComponents

// MARK: - In model

public protocol PointsBalanceModelProtocol {

    var accumulatedPoints: NSNumber? { get }
    var pointsToBeExpired: NSNumber? { get }
    var expirationDate: String? { get }
    var areRedeemable: Bool? { get }
}

public struct PointsBalanceModel: PointsBalanceModelProtocol {

    public var accumulatedPoints: NSNumber?
    public var pointsToBeExpired: NSNumber?
    public var expirationDate: String?
    public var areRedeemable: Bool?
}

struct ExpiratingPoints {

    let pointsToBeExpired: NSNumber
    let expirationDate: String
    let accumulatedPoints: NSNumber
}

open class PointViewModel: ViewModel {

    private enum PointsBalanceStatus {

        case serviceFailed
        case noData
        case noPoints
        case redeemablePoints
        case noExpiringPoints
        case accumulatedAndExpiringPoints
    }

    private var pointsBalanceStatus: PointsBalanceStatus?

    static let TAG: String = String(describing: PointViewModel.self)

    public static let ATTR_LOADING: String = "loading"

    public static let ATTR_REDEEMABLE: String = "redeemable"
    public static let ATTR_EXPIRATING_POINTS: String = "expiratingPoints"
    public static let ATTR_NO_EXPIRATING_POINTS: String = "noExpiratingPoints"

    public static let ATTR_NO_POINTS: String = "noPoints"
    public static let ATTR_NO_DATA: String = "noData"
    public static let ATTR_ERROR: String = "error"

    public static let ON_RETRY_BUTTON_PRESSED: ActionSpec<Void> = ActionSpec<Void>(id: "retry-button-pressed")
    public static let ON_REDEEM_BUTTON_PRESSED: ActionSpec<Void> = ActionSpec<Void>(id: "redeem-button-pressed")
    public static let ON_NO_EXPIRANTING_POINTS_BUTTON_PRESSED: ActionSpec<Void> = ActionSpec<Void>(id: "no-expirating-points-button-pressed")
    public static let ON_NO_REMAINING_POINTS_BUTTON_PRESSED: ActionSpec<Void> = ActionSpec<Void>(id: "no-remaining-points-button-pressed")
    public static let ON_NO_POINTS_BUTTON_PRESSED: ActionSpec<Void> = ActionSpec<Void>(id: "no-points-button-pressed")
    public static let ON_VIEW_TOUCHED: ActionSpec<Void> = ActionSpec<Void>(id: "view-touched")

    public var isLoading = false {
        didSet {
            setProperty(key: PointViewModel.ATTR_LOADING, value: isLoading)
        }
    }

    public var pointsBalanceModel: PointsBalanceModelProtocol? {
        didSet {
            guard let pointsBalance = pointsBalanceModel else {
                pointsBalanceStatus = .noData
                setProperty(key: PointViewModel.ATTR_NO_DATA, value: Void())
                return
            }

            guard let acumulatedPoints = pointsBalance.accumulatedPoints else {
                pointsBalanceStatus = .noData
                setProperty(key: PointViewModel.ATTR_NO_DATA, value: Void())
                return
            }

            if acumulatedPoints == 0.0 {

                pointsBalanceStatus = .noPoints
                setProperty(key: PointViewModel.ATTR_NO_POINTS, value: Void())
            } else {

                if let areRedeemable = pointsBalance.areRedeemable, areRedeemable == false {

                    if let pointsToBeExpired = pointsBalance.pointsToBeExpired, let expirationDate = pointsBalance.expirationDate {

                        pointsBalanceStatus = .accumulatedAndExpiringPoints
                        let expiratingPoints = ExpiratingPoints(pointsToBeExpired: pointsToBeExpired, expirationDate: expirationDate, accumulatedPoints: acumulatedPoints)
                        setProperty(key: PointViewModel.ATTR_EXPIRATING_POINTS, value: expiratingPoints)

                    } else {

                        pointsBalanceStatus = .noExpiringPoints
                        setProperty(key: PointViewModel.ATTR_NO_EXPIRATING_POINTS, value: acumulatedPoints)
                    }
                } else {

                    pointsBalanceStatus = .redeemablePoints
                    setProperty(key: PointViewModel.ATTR_REDEEMABLE, value: acumulatedPoints)
                }
            }
        }
    }

    // MARK: - funcs

    open func genericError() {

        pointsBalanceStatus = .serviceFailed
        setProperty(key: PointViewModel.ATTR_ERROR, value: Void())
    }

    open func onButtonPressed() {
        
        switch pointsBalanceStatus {

        case .some(.serviceFailed):

            self.fire(actionSpec: PointViewModel.ON_RETRY_BUTTON_PRESSED)

        case .some(.noData):

            self.fire(actionSpec: PointViewModel.ON_NO_POINTS_BUTTON_PRESSED)

        case .some(.noPoints):

            self.fire(actionSpec: PointViewModel.ON_NO_REMAINING_POINTS_BUTTON_PRESSED)

        case .some(.redeemablePoints):

            self.fire(actionSpec: PointViewModel.ON_REDEEM_BUTTON_PRESSED)

        case .some(.noExpiringPoints):

            self.fire(actionSpec: PointViewModel.ON_NO_EXPIRANTING_POINTS_BUTTON_PRESSED)

        default:

            self.fire(actionSpec: PointViewModel.ON_VIEW_TOUCHED)
        }
    }

    open func onViewTouched() {

        self.fire(actionSpec: PointViewModel.ON_VIEW_TOUCHED)
    }
}
