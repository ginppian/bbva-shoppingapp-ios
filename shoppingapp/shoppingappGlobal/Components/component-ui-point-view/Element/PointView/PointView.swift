//
//  PointView.swift
//  shoppingappGlobal
//
//  Created by ignacio.bonafonte on 18/07/2018.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit
import CellsNativeComponents
import Shimmer

open class PointView: UIView {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var points: UILabel!
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var actionButton: UIButton!
    @IBOutlet weak var skeletonsContainerView: UIView!
    @IBOutlet weak var skeletonCircle: FBShimmeringView!
    @IBOutlet weak var skeletonLabel: FBShimmeringView!
    @IBOutlet weak var skeletonButton: FBShimmeringView!

    // MARK: - Constants
    private let skeletonBaseViewImage = UIImage(named: "skeleton_lines")
    private let skeletonImageEdgeInsetRight: CGFloat = 20

    // MARK: - Configuration Keys
    // MARK: Common
    fileprivate let VALUE_ID = "value"
    fileprivate let TYPE_ID = "type"
    fileprivate let TEXT_COLOR_ID = "text-color"
    fileprivate let TEXT_FONT_ID = "text-font"
    fileprivate let TEXT_SIZE_ID = "text-size"
    fileprivate let ERROR_LITERAL_ID = "error-literal"
    fileprivate let NO_POINTS_LITERAL_ID = "noPoints-literal"
    fileprivate let NO_MORE_POINTS_LITERAL_ID = "noMorePoints-literal"

    // MARK: Background
    fileprivate let BACKGROUND_COLOR_ID = "background-color"

    // MARK: Skeleton
    fileprivate let SKELETON_COLOR_ID = "skeleton-color"
    fileprivate let SKELETON__CONTENT_COLOR_ID = "skeleton-content-color"

    // MARK: Image
    fileprivate let IMAGEVIEW_ID = "imageView"
    fileprivate let POINTS_IMAGE_ID = "points-image"
    fileprivate let NO_POINTS_IMAGE_ID = "noPoints-image"

    // MARK: Points Label
    fileprivate let POINTS_LABEL_ID = "pointsLabel"

    // MARK: Points Label
    fileprivate let INFO_LABEL_ID = "infoLabel"
    fileprivate let POINTS_LITERAL_ID = "points-literal"

    // MARK: Action Label
    fileprivate let ACTION_LABEL_ID = "actionLabel"
    fileprivate let REDEEM_LITERAL_ID = "redeem-literal"
    fileprivate let EXPIRATING_POINTS_LITERAL_ID = "expiratingPoints-literal"

    // MARK: - Configuration Properties
    private var skeletonViewImage: UIImage?

    private var viewBackgroundColor: UIColor?
    private var skeletonBackgroundColor: UIColor?
    private var skeletonContentColor: UIColor?

    private var pointsImage: UIImage?
    private var noPointsImage: UIImage?

    private var pointsInfoLiteral: String?
    private var noPointsInfoLiteral: String?
    private var noMorePointsInfoLiteral: String?
    private var errorInfoLiteral: String?

    private var noPointsActionLiteral: String?
    private var expitaringPointsActionLiteral: String?
    private var noMorePointsActionLiteral: String?
    private var redeemActionLiteral: String?
    private var errorActionLiteral: String?

    open var viewModel: PointViewModel?

    public required init(coder aDecoder: NSCoder) {

        super.init(coder: aDecoder)!
        translatesAutoresizingMaskIntoConstraints = false
    }

    override open func removeFromSuperview() {

        viewModel?.onCleared()
        super.removeFromSuperview()
    }

    @IBAction func buttonPressed(_ sender: Any) {

        viewModel?.onButtonPressed()
    }

    override open func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {

        super.touchesEnded(touches, with: event)
        viewModel?.onViewTouched()
    }
}

    // MARK: - Configuration Component UI -
extension PointView {

    open func configureView(viewController: UIViewController) {

        actionButton.titleLabel?.font = UIFont.systemFont(ofSize: 14)

        viewModel = CellsViewModelProvider.of(id: self.tagNameUI, viewModelClass: PointViewModel.self, container: viewController)
        configureViewWithProperties()
    }

    fileprivate func configureViewWithProperties() {

        //Listening model properties
        viewModel?.newProperty(key: PointViewModel.ATTR_REDEEMABLE, consumer: showPointsRedeemables)
        viewModel?.newProperty(key: PointViewModel.ATTR_EXPIRATING_POINTS, consumer: showPointsExpirating)
        viewModel?.newProperty(key: PointViewModel.ATTR_NO_EXPIRATING_POINTS, consumer: showPointsNoExpirating)
        viewModel?.newProperty(key: PointViewModel.ATTR_NO_POINTS, consumer: showNoPoints)
        viewModel?.newProperty(key: PointViewModel.ATTR_NO_DATA, consumer: showNoData)
        viewModel?.newProperty(key: PointViewModel.ATTR_ERROR, consumer: showGenericError)
        viewModel?.newProperty(key: PointViewModel.ATTR_LOADING, consumer: showSkeleton)

        //Listening configuration json
        viewModel?.newProperty(key: BACKGROUND_COLOR_ID, consumer: setBackgroundColorFromProperty)
        viewModel?.newProperty(key: SKELETON_COLOR_ID, consumer: setSkeletonColorFromProperty)
        viewModel?.newProperty(key: SKELETON__CONTENT_COLOR_ID, consumer: setSkeletonContentColorFromProperty)
        viewModel?.newProperty(key: IMAGEVIEW_ID, consumer: setImagesFromProperty)
        viewModel?.newProperty(key: POINTS_LABEL_ID, consumer: setPointsLabelFromProperty)
        viewModel?.newProperty(key: INFO_LABEL_ID, consumer: setInfoLabelFromProperty)
        viewModel?.newProperty(key: ACTION_LABEL_ID, consumer: setActionLabelFromProperty)

        viewModel?.subscribeToFilledProperties(consumer: { _ in

            self.setupSkeleton()
            self.setupView()
        })
    }

}

// MARK: - Private methods

private extension PointView {

    private func setupView() {

        backgroundColor = viewBackgroundColor
        skeletonsContainerView.backgroundColor = viewBackgroundColor
    }

    private func setupSkeleton() {

        skeletonCircle.layer.cornerRadius = skeletonCircle.frame.size.height / 2
        skeletonCircle.backgroundColor = skeletonBackgroundColor

        let skeletonCircleContentView = UIView(frame: CGRect(x: 0, y: 0, width: skeletonCircle.frame.size.width, height: skeletonCircle.frame.size.height))
        skeletonCircleContentView.layer.cornerRadius = skeletonCircle.frame.size.height / 2
        skeletonCircleContentView.backgroundColor = skeletonContentColor

        skeletonCircle.contentView = skeletonCircleContentView

        skeletonViewImage = skeletonBaseViewImage?.resizableImage(withCapInsets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: skeletonImageEdgeInsetRight))

        skeletonLabel.backgroundColor = skeletonBackgroundColor
        skeletonButton.backgroundColor = skeletonBackgroundColor

        skeletonLabel.contentView = UIImageView(image: skeletonViewImage)
        skeletonButton.contentView = UIImageView(image: skeletonViewImage)

        startShimmering(false)
    }

    private func startShimmering(_ shimmering: Bool) {

        skeletonCircle.isShimmering = shimmering
        skeletonLabel.isShimmering = shimmering
        skeletonButton.isShimmering = shimmering
    }

    private func getValueOfProperty(_ params: Any?, id: String) -> String? {

        if let properties = params as? [String: Any] {

            for property in properties.keys {

                if property == id, let valueToChange = properties[property] as? [String: Any], let value = valueOfProperty(property: valueToChange) {

                    return value
                }
            }
        }

        return nil
    }

    private func valueOfProperty(property: [String: Any]) -> String? {

        if  let value = property[VALUE_ID] as? String {

            return value
        }
        return nil
    }

    private func setShowPointsMode(show: Bool) {

        if show {
            imageView.image = pointsImage
            points.isHidden = false
        } else {
            imageView.image = noPointsImage
            points.isHidden = true
        }
    }
}

// MARK: - Consumers for Properties

extension PointView {

    // MARK: Listening configuration json

    fileprivate func setBackgroundColorFromProperty (_ params: Any?) {

        if let property = params as? [String: Any] {
            let color = valueOfProperty(property: property)
            if let color = color {
                viewBackgroundColor = UIColor.hexStringToUIColor(hex: color)
            }
        }
    }

    fileprivate func setSkeletonColorFromProperty(_ params: Any?) {

        if let property = params as? [String: Any] {
            let color = valueOfProperty(property: property)
            if let color = color {
                skeletonBackgroundColor = UIColor.hexStringToUIColor(hex: color)
            }
        }
    }

    fileprivate func setSkeletonContentColorFromProperty(_ params: Any?) {

        if let property = params as? [String: Any] {
            let color = valueOfProperty(property: property)
            if let color = color {
                skeletonContentColor = UIColor.hexStringToUIColor(hex: color)
            }
        }
    }

    fileprivate func setImagesFromProperty (_ params: Any?) {

        if let imageName = getValueOfProperty(params, id: POINTS_IMAGE_ID), let image = UIImage(named: imageName) {
            pointsImage = image
        }

        if let imageName = getValueOfProperty(params, id: NO_POINTS_IMAGE_ID), let image = UIImage(named: imageName) {
            noPointsImage = image
        }
    }

    fileprivate func setPointsLabelFromProperty (_ params: Any?) {

        if let textColor = getValueOfProperty(params, id: TEXT_COLOR_ID) {
            let color = UIColor.hexStringToUIColor(hex: textColor)
            points.textColor = color
        }

        if let textFont = getValueOfProperty(params, id: TEXT_FONT_ID), let textSize = getValueOfProperty(params, id: TEXT_SIZE_ID) {
            guard let textSize = NumberFormatter().number(from: textSize) else {
                return
            }

            let font = UIFont(name: textFont, size: CGFloat(textSize.floatValue))

            points.font = font
        }
    }

    fileprivate func setInfoLabelFromProperty (_ params: Any?) {

        if let textColor = getValueOfProperty(params, id: TEXT_COLOR_ID) {
            let color = UIColor.hexStringToUIColor(hex: textColor)
            label.textColor = color
        }

        if let textFont = getValueOfProperty(params, id: TEXT_FONT_ID), let textSize = getValueOfProperty(params, id: TEXT_SIZE_ID) {
            guard let textSize = NumberFormatter().number(from: textSize) else {
                return
            }

            let font = UIFont(name: textFont, size: CGFloat(textSize.floatValue))

            label.font = font
        }

        pointsInfoLiteral = getValueOfProperty(params, id: POINTS_LITERAL_ID)
        noPointsInfoLiteral = getValueOfProperty(params, id: NO_POINTS_LITERAL_ID)
        noMorePointsInfoLiteral = getValueOfProperty(params, id: NO_MORE_POINTS_LITERAL_ID)
        errorInfoLiteral = getValueOfProperty(params, id: ERROR_LITERAL_ID)
    }

    fileprivate func setActionLabelFromProperty (_ params: Any?) {

        if let textColor = getValueOfProperty(params, id: TEXT_COLOR_ID) {
            let color = UIColor.hexStringToUIColor(hex: textColor)
            let selectedColor = color.withAlphaComponent(0.5)

            actionButton.setTitleColor(color, for: .normal)
            actionButton.setTitleColor(selectedColor, for: .selected)
            actionButton.setTitleColor(selectedColor, for: .highlighted)
        }

        if let textFont = getValueOfProperty(params, id: TEXT_FONT_ID), let textSize = getValueOfProperty(params, id: TEXT_SIZE_ID) {
            guard let textSize = NumberFormatter().number(from: textSize) else {
                return
            }

            let font = UIFont(name: textFont, size: CGFloat(textSize.floatValue))

            actionButton.titleLabel?.font = font
        }

        redeemActionLiteral = getValueOfProperty(params, id: REDEEM_LITERAL_ID)
        noPointsActionLiteral = getValueOfProperty(params, id: NO_POINTS_LITERAL_ID)
        noMorePointsActionLiteral = getValueOfProperty(params, id: NO_MORE_POINTS_LITERAL_ID)
        expitaringPointsActionLiteral = getValueOfProperty(params, id: EXPIRATING_POINTS_LITERAL_ID)
        errorActionLiteral = getValueOfProperty(params, id: ERROR_LITERAL_ID)
    }

    // MARK: Listening model properties

    fileprivate func showGenericError(_ params: Void?) {

        setShowPointsMode(show: false)

        if let errorInfoLiteral = errorInfoLiteral {
            label.text = NSLocalizedString(errorInfoLiteral, comment: "")
        }

        if let errorActionLiteral = errorActionLiteral {
            actionButton.setTitle(NSLocalizedString(errorActionLiteral, comment: ""), for: .normal)
        }
    }

    fileprivate func showNoPoints(_ params: Void?) {

        setShowPointsMode(show: false)

        if let literal = noMorePointsInfoLiteral {
            label.text = NSLocalizedString(literal, comment: "")
        }

        if let errorActionLiteral = noMorePointsActionLiteral {
            actionButton.setTitle(NSLocalizedString(errorActionLiteral, comment: ""), for: .normal)
        }

    }

    fileprivate func showNoData(_ params: Void?) {

        setShowPointsMode(show: false)

        if let literal = noPointsInfoLiteral {
            label.text = NSLocalizedString(literal, comment: "")
        }

        if let errorActionLiteral = noPointsActionLiteral {
            actionButton.setTitle(NSLocalizedString(errorActionLiteral, comment: ""), for: .normal)
        }

    }

    fileprivate func showPointsRedeemables(_ params: NSNumber?) {

        setShowPointsMode(show: true)

        if let pointsNumber = params {
            points.text = String(pointsNumber.intValue)
        }

        if let literal = pointsInfoLiteral {
            label.text = NSLocalizedString(literal, comment: "")
        }

        if let actionLiteral = redeemActionLiteral {
            actionButton.setTitle(NSLocalizedString(actionLiteral, comment: ""), for: .normal)
        }
    }

    fileprivate func showPointsExpirating(_ params: ExpiratingPoints?) {

        setShowPointsMode(show: true)

        if let pointsNumber = params?.accumulatedPoints {
            points.text = String(pointsNumber.intValue)
        }

        if let literal = pointsInfoLiteral {
            label.text = NSLocalizedString(literal, comment: "")
        }

        if let actionLiteral = expitaringPointsActionLiteral, let expirationDate = params?.expirationDate, let expirationAmount = params?.pointsToBeExpired {
            let literalText = NSLocalizedString(actionLiteral, comment: "")

            let literalInfo = String(format: literalText, String(expirationAmount.intValue), expirationDate)

            actionButton.setTitle(literalInfo, for: .normal)
        }
    }

    fileprivate func showPointsNoExpirating(_ params: NSNumber?) {

        setShowPointsMode(show: true)

        if let pointsNumber = params {
            points.text = String(pointsNumber.intValue)
        }

        if let literal = pointsInfoLiteral {
            label.text = NSLocalizedString(literal, comment: "")
        }

        if let actionLiteral = noPointsActionLiteral {
            actionButton.setTitle(NSLocalizedString(actionLiteral, comment: ""), for: .normal)
        }
    }

    fileprivate func showSkeleton(_ params: Bool?) {

        if let show = params {

            skeletonsContainerView.isHidden = !show
            startShimmering(show)
        }
    }
}
