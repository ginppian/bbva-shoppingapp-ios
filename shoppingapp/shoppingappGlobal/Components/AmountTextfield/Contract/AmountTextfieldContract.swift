//
//  AmountTextfieldContract.swift
//  shoppingapp
//
//  Created by jesus.martinez on 1/8/17.
//  Copyright © 2017 BBVA. All rights reserved.
//
import RxSwift

protocol AmountTextfieldViewProtocol: ComponentViewProtocol {

    func clearTextfield(withValue value: String)
    func configureNumberKeyboard()
    func configureDecimalKeyboard()
    func showError()
    func hideError()
    func setPlaceHolder(text: String)
    func updateView(withAmountText amountText: String)
    func informEndEditing(withAmount amount: NSDecimalNumber)
    func showRemoveButton()
    func hideRemoveButton()

}

protocol AmountTextfieldPresenterProtocol: ComponentPresenterProtocol {

    func onConfigureView()
    func crossAction()
    func textfieldStartEditing()
    func textfieldEndEditing(withText text: String)
    func shouldChangeCharacters(forText text: String, withNewText newText: String, inRange range: NSRange) -> Bool

}

protocol AmountTextfieldInteractorProtocol {

    func provideCurrency() -> Observable<ModelBO>
}
