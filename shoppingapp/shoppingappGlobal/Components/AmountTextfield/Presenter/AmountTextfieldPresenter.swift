//
//  FromToAmountFilterPresenter.swift
//  shoppingapp
//
//  Created by jesus.martinez on 1/8/17.
//  Copyright © 2017 BBVA. All rights reserved.
//
import RxSwift

class AmountTextfieldPresenter {

    var interactor: AmountTextfieldInteractorProtocol = AmountTextfieldInteractor()
    weak var view: AmountTextfieldViewProtocol?
    let disposeBag = DisposeBag()

    var formatter: AmountFormatter?
    var currencyBO: CurrencyBO!
    var amountText = ""

    var displayData: AmountTextfieldDisplayData? {
        didSet {
            onConfigureView()
        }
    }

    func setupData(currency: CurrencyBO) {

        self.currencyBO = currency
        self.formatter = AmountFormatter(codeCurrency: currency.code)

        (self.currencyBO?.needDecimals)! ? self.view?.configureDecimalKeyboard() : self.view?.configureNumberKeyboard()

        let placeHolder = self.displayData?.placeHolder ?? ""
        self.view?.setPlaceHolder(text: placeHolder)

        if let initialAmount = self.displayData?.initialAmount {
            self.amountText = initialAmount
            let formatted = self.formatter?.format(amount: NSDecimalNumber(string: self.amountText, locale: Locale.current), withSpace: true) ?? ""
            self.view?.updateView(withAmountText: formatted)
        }
    }

    func shouldHideErrorAutomatically() {

        guard let displayData = displayData else {
            return
        }

        if displayData.shouldHideErrorAutomatically {
            view?.hideError()
        }
    }
}

extension AmountTextfieldPresenter: AmountTextfieldPresenterProtocol {

    func onConfigureView() {

        view?.hideError()
        amountText = ""

        if let currency = displayData?.currency {

            setupData(currency: currency)

        } else {

            interactor.provideCurrency().subscribe(
                onNext: { [weak self] result in

                    if let currency = result as? CurrencyBO {
                        self?.setupData(currency: currency)
                    }
                }
                ).addDisposableTo(disposeBag)

        }
    }

    func crossAction() {

        view?.clearTextfield(withValue: "")

    }

    func textfieldStartEditing() {

        view?.showRemoveButton()

        let placeHolder = self.displayData?.placeHolder ?? ""

        view?.setPlaceHolder(text: placeHolder)

        view?.updateView(withAmountText: amountText)

    }

    func textfieldEndEditing(withText text: String) {

        view?.hideRemoveButton()

        amountText = text

        if text.isEmpty {

            let placeHolder = self.displayData?.placeHolder ?? ""
            view?.setPlaceHolder(text: placeHolder)
            view?.updateView(withAmountText: "")

        } else {

            let formatted = formatter?.format(amount: NSDecimalNumber(string: text, locale: Locale.current), withSpace: true) ?? text
            view?.updateView(withAmountText: formatted)

        }

        view?.informEndEditing(withAmount: NSDecimalNumber(string: text, locale: Locale.current))

    }

    func shouldChangeCharacters(forText currentText: String, withNewText newText: String, inRange range: NSRange) -> Bool {

        guard newText.isEmpty == false || currencyBO.needDecimals == false else {
            shouldHideErrorAutomatically()
            return true
        }

        let allowedCharacters = CharacterSet.decimalDigits//(self.currencyBO?.needDecimals)! ? CharacterSet.decimalDigits : CharacterSet(charactersIn: "0123456789")

        if newText.rangeOfCharacter(from: allowedCharacters.inverted) != nil {
            return false
        }

        if newText == AmountFormatter.decimalFormatter(locale: Locale.current).decimalSeparator {
            shouldHideErrorAutomatically()

            return currentText.components(separatedBy: newText).count <= 1

        } else {
            shouldHideErrorAutomatically()

            let replacementText = (currentText as NSString).replacingCharacters(in: range, with: newText)

            let numberOfDecimals = NSDecimalNumber(string: replacementText, locale: Locale.current).decimalValue.significantFractionalDecimalDigits

            return numberOfDecimals <= AmountFormatter.MAX_DECIMALS

        }

    }

}
