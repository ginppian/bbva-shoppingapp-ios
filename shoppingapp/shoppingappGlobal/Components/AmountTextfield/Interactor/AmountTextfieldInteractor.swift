//
//  AmountTextfieldInteractor.swift
//  shoppingapp
//
//  Created by jesus.martinez on 3/8/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation
import RxSwift

class AmountTextfieldInteractor {

    let disposeBag = DisposeBag()
}

extension AmountTextfieldInteractor: AmountTextfieldInteractorProtocol {

    func provideCurrency() -> Observable<ModelBO> {

        return Observable.create { observer in

            ConfigurationDataManager.sharedInstance.provideMainCurrency().subscribe(

                onNext: { result in

                    if let currencyEntity = result as? CurrencyEntity {

                        let currencyBO = Currencies.currency(forCode: currencyEntity.code)

                        observer.onNext(currencyBO)
                        observer.onCompleted()
                    } else {
                        DLog(message: "Incorrect CurrencyEntity")

                    }

                }).addDisposableTo(self.disposeBag)

            return Disposables.create()
        }

    }
}
