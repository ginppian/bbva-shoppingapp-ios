//
//  AmountTextfield.swift
//  
//
//  Created by jesus.martinez on 31/7/17.
//
//

import UIKit

protocol AmountTextfieldDelegate: class {
    func amountTextfield(_ amountTextfield: AmountTextfield, didEndEditing amount: NSDecimalNumber)
}

class AmountTextfield: XibView {

    var presenter = AmountTextfieldPresenter()

    weak var delegate: AmountTextfieldDelegate?

    @IBOutlet weak var textfield: CustomTextField!
    @IBOutlet weak var errorLineView: UIView!
    @IBOutlet weak var crossButton: UIButton!

    override func configureView() {

        super.configureView()

        backgroundColor = .BBVAWHITE

        textfield.placeholderNormalColor = .BBVA500
        textfield.placeholderActiveColor = .BBVA500
        textfield.placeholderVerticalOffset = textfield.frame.size.height - textfield.verticalOffset
        textfield.contentVerticalAlignment = UIControlContentVerticalAlignment.bottom
        textfield.isDividerHidden = true
        textfield.textColor = .BBVA600
        textfield.font = Tools.setFontBook(size: 16)
        textfield.delegate = self

        errorLineView.isHidden = true
        errorLineView.backgroundColor = UIColor.BBVADARKRED.withAlphaComponent(0.4)

        crossButton.isHidden = true

        crossButton.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Common.close_icon, color: .BBVA300), width: 14, height: 14), for: .normal)
        crossButton.addTarget(self, action: #selector(removeText), for: .touchUpInside)
        crossButton.contentMode = .scaleAspectFit
        crossButton.tintColor = .BBVA300
        crossButton.imageEdgeInsets = UIEdgeInsets(top: 13, left: 17, bottom: 17, right: 13)

        presenter.view = self
        presenter.onConfigureView()

    }

    @objc func removeText() {

        presenter.crossAction()

    }

}

extension AmountTextfield: AmountTextfieldViewProtocol {

    func configureNumberKeyboard() {

        textfield.keyboardType = .numberPad

    }

    func configureDecimalKeyboard() {

        textfield.keyboardType = .decimalPad

    }

    func setPlaceHolder(text: String) {

        textfield.placeholder = text

    }

    func clearTextfield(withValue value: String) {

        textfield.text = value

    }

    func showError() {

        backgroundColor = .BBVAWHITECORAL50
        errorLineView.isHidden = false

    }

    func hideError() {

        backgroundColor = .BBVAWHITE
        errorLineView.isHidden = true

    }

    func informEndEditing(withAmount amount: NSDecimalNumber) {

        delegate?.amountTextfield(self, didEndEditing: amount)

    }

    func updateView(withAmountText amountText: String) {

        textfield.text = amountText

    }

    func showRemoveButton() {
        crossButton.isHidden = false

    }

    func hideRemoveButton() {
        crossButton.isHidden = true
    }

}

extension AmountTextfield: UITextFieldDelegate {

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString newString: String) -> Bool {

        return presenter.shouldChangeCharacters(forText: textField.text ?? "", withNewText: newString, inRange: range)

    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        dynamicAccessibility(forTextfield: textField, withVisibility: false)
        presenter.textfieldStartEditing()
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        dynamicAccessibility(forTextfield: textField, withVisibility: true)
        presenter.textfieldEndEditing(withText: textField.text!)
    }

}

// MARK: - APPIUM

private extension AmountTextfield {

    func dynamicAccessibility(forTextfield textfield: UITextField, withVisibility visibility: Bool) {
        #if APPIUM
            textfield.isAccessibilityElement = visibility
        #endif
    }

}
