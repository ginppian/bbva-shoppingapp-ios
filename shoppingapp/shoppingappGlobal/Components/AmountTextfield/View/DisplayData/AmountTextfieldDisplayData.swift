//
//  AmountTextfieldDisplayData.swift
//  shoppingapp
//
//  Created by jesus.martinez on 31/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation

struct AmountTextfieldDisplayData: Equatable {

    let placeHolder: String
    var initialAmount: String?
    var currency: CurrencyBO?
    var shouldHideErrorAutomatically = true

}
