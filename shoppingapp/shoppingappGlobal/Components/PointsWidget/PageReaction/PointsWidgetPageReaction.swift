//
//  PointsWidgetPageReaction.swift
//  shoppingapp
//
//  Created by Javier Pino on 20/7/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

class PointsWidgetPageReaction: PageReaction {

    static let ROUTER_TAG: String = "points-widget-tag"
    static let ON_TSEC_EXPIRED = ActionSpec<ErrorBO>(id: "on-tsec-expired")

    override func configure() {

        pointsBalanceSucceed()
        pointsBalanceFailed()
        retryButtonPressed()
        redeemButtonPressed()
        noExpiratingPointsButtonPressed()
        noRemainingPointsButtonPressed()
        noPointsButtonPressed()
        viewTouched()
    }

    func pointsBalanceSucceed() {

        _ = when(id: PointsBalanceStore.ID, type: PointsBalanceDTO.self)
            .doAction(actionSpec: PointsBalanceStore.ON_GET_POINTS_BALANCE_SUCCESS)
            .then()
            .inside(componentID: PointsWidget.ID, component: PointsWidget.self)
            .doReaction { widget, pointsBalance in

                if let pointsBalance = pointsBalance {
                    widget.pointsBalanceSucceed(pointsBalance: pointsBalance)
                } else {
                    widget.pointsBalanceFailed(error: nil)
                }

        }
    }

    func pointsBalanceFailed() {

        _ = when(id: PointsBalanceStore.ID, type: ErrorBO.self)
            .doAction(actionSpec: PointsBalanceStore.ON_GET_POINTS_BALANCE_ERROR)
            .then()
            .inside(componentID: PointsWidget.ID, component: PointsWidget.self)
            .doReaction { widget, error in

                widget.pointsBalanceFailed(error: error)
        }
    }

    func retryButtonPressed() {

        _ = when(id: PointsWidget.POINTVIEW_COMPONENT, type: Void.self)
            .doAction(actionSpec: PointViewModel.ON_RETRY_BUTTON_PRESSED)
            .then()
            .inside(componentID: PointsWidget.ID, component: PointsWidget.self)
            .doReaction { widget, _ in
                widget.retryButtonPressed()
        }
    }

    func redeemButtonPressed() {

        _ = when(id: PointsWidget.POINTVIEW_COMPONENT, type: Void.self)
            .doAction(actionSpec: PointViewModel.ON_REDEEM_BUTTON_PRESSED)
            .then()
            .inside(componentID: PointsWidget.ID, component: PointsWidget.self)
            .doReaction { widget, _ in
                widget.redeemButtonPressed()
        }
    }

    func noExpiratingPointsButtonPressed() {

        _ = when(id: PointsWidget.POINTVIEW_COMPONENT, type: Void.self)
            .doAction(actionSpec: PointViewModel.ON_NO_EXPIRANTING_POINTS_BUTTON_PRESSED)
            .then()
            .inside(componentID: PointsWidget.ID, component: PointsWidget.self)
            .doReaction { widget, _ in
                widget.noExpiratingPointsButtonPressed()
        }
    }

    func noRemainingPointsButtonPressed() {

        _ = when(id: PointsWidget.POINTVIEW_COMPONENT, type: Void.self)
            .doAction(actionSpec: PointViewModel.ON_NO_REMAINING_POINTS_BUTTON_PRESSED)
            .then()
            .inside(componentID: PointsWidget.ID, component: PointsWidget.self)
            .doReaction { widget, _ in
                widget.noRemainingPointsButtonPressed()
        }
    }

    func noPointsButtonPressed() {

        _ = when(id: PointsWidget.POINTVIEW_COMPONENT, type: Void.self)
            .doAction(actionSpec: PointViewModel.ON_NO_POINTS_BUTTON_PRESSED)
            .then()
            .inside(componentID: PointsWidget.ID, component: PointsWidget.self)
            .doReaction { widget, _ in
                widget.noPointsButtonPressed()
        }
    }

    func viewTouched() {

        _ = when(id: PointsWidget.POINTVIEW_COMPONENT, type: Void.self)
            .doAction(actionSpec: PointViewModel.ON_VIEW_TOUCHED)
            .then()
            .inside(componentID: PointsWidget.ID, component: PointsWidget.self)
            .doReaction { widget, _ in
                widget.viewTouched()
        }
    }

}
