//
//  PointsWidgetswift
//  shoppingapp
//
//  Created by ignacio.bonafonte on 19/07/2018.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit
import CellsNativeCore
import CellsNativeComponents

class PointsWidget: CellsBaseViewController {

    static let ID: String = String(describing: PointsWidget.self)
    static let POINTVIEW_COMPONENT: String = "pointView"

    @IBOutlet weak var pointView: UIView!
    var pointViewUIComponent: PointView?
    var pointsBalanceStore: PointsBalanceStore?

    override func viewDidLoad() {

        pointViewUIComponent = UIView.fromNib(superView: self.pointView, tagName: PointsWidget.POINTVIEW_COMPONENT) as PointView
        pointViewUIComponent?.configureView(viewController: self)

        if let appLifeCycle = (UIApplication.shared.delegate as! AppDelegate).appLifeCycle {
            configure(appLifecycle: appLifeCycle, id: PointsWidget.ID)
        }

        if let networkWorker = BusManager.sessionDataInstance.networkWorker {
            pointsBalanceStore = PointsBalanceStore(worker: networkWorker, host: ServerHostURL.getLoyaltyURL())
        }

        let pageReactionManager = BusManager.sessionDataInstance.pageReactionManager
        pageReactionManager.add(id: PointsWidget.ID) { () -> PageReaction in
            PointsWidgetPageReaction()
        }

        super.viewDidLoad()

        requestPointsBalance()
    }

    func pointsBalanceSucceed(pointsBalance: PointsBalanceDTO) {

        var pointsBalanceModel = PointsBalanceModel()

        pointsBalanceModel.accumulatedPoints = pointsBalance.data?.accumulatedPoints?.amount
        pointsBalanceModel.pointsToBeExpired = pointsBalance.data?.pointsToBeExpired?.amount

        if let expirationDateString = pointsBalance.data?.pointsToBeExpired?.expirationDate,
            let expirationDate = Date.date(fromServerString: expirationDateString, withFormat: Date.DATE_FORMAT_SERVER) {

            let date = Date.string(fromDate: expirationDate, withFormat: Date.DATE_FORMAT_DAY_MONTH_COMPLETE_YEAR_COMPACT)
            pointsBalanceModel.expirationDate = date
        }

        pointsBalanceModel.areRedeemable = pointsBalance.data?.areRedeemable ?? true

        pointViewUIComponent?.viewModel?.pointsBalanceModel = pointsBalanceModel

        pointViewUIComponent?.viewModel?.isLoading = false
    }

    func pointsBalanceFailed(error: ErrorBO?) {

        if let error = error, error.sessionExpired() {
            BusManager.sessionDataInstance.publishData(tag: PointsWidgetPageReaction.ROUTER_TAG, PointsWidgetPageReaction.ON_TSEC_EXPIRED, error)

            pointViewUIComponent?.viewModel?.genericError()
        } else {
            pointViewUIComponent?.viewModel?.genericError()
        }

        pointViewUIComponent?.viewModel?.isLoading = false
    }

    func retryButtonPressed() {

        requestPointsBalance()
    }

    func redeemButtonPressed() {

        navigateToPointsScreen()
    }

    func noExpiratingPointsButtonPressed() {

        navigateToPointsScreen()
    }

    func noRemainingPointsButtonPressed() {

        navigateToPointsScreen()
    }

    func noPointsButtonPressed() {

        navigateToPointsScreen()
    }

    func viewTouched() {

        navigateToPointsScreen()
    }
}

// MARK: - Private methods
private extension PointsWidget {

    func requestPointsBalance() {

        pointViewUIComponent?.viewModel?.isLoading = true
        pointsBalanceStore?.getPointsBalance()
    }

    func navigateToPointsScreen() {

        BusManager.sessionDataInstance.navigateScreen(tag: HomePageReaction.ROUTER_TAG, HomePageReaction.EVENT_CHANGE_TAB_TO_POINTS, Void())
    }
}
