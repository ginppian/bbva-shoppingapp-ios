//
//  PublicConfigurationStore.swift
//  shoppingapp
//
//  Created by Javier Pino on 5/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeComponents
import BBVA_Network

open class PublicConfigurationStore: NetworkStore {
    
    public static let identifier: String = "component-data-manager-public-configuration-store"
    
    public static let onGetPublicConfigurationSuccess = ActionSpec<PublicConfigurationDTO>(id: "on-get-public-configuration-success")
    static let onGetPublicConfigurationError = ActionSpec<ErrorBO>(id: "on-get-public-configuration-error")
        
    // MARK: public vars
    
    var url: String?
    var countryCode: String?
    
    // MARK: private vars
    
    fileprivate var publicConfigurationUseCase: PublicConfigurationUseCase?
    fileprivate var worker: NetworkWorker
    
    // MARK: init
    
    convenience init(worker: NetworkWorker, publicConfigurationUseCase: PublicConfigurationUseCase) {
        
        self.init(worker: worker)
        self.publicConfigurationUseCase = publicConfigurationUseCase
    }
    
    public init(worker: NetworkWorker, url: String? = nil) {
        
        self.worker = worker
        self.url = url
        
        super.init(PublicConfigurationStore.identifier)
        
        guard let url = url, !url.isEmpty else {
            return
        }
        
        let publicConfigurationRepository: PublicConfigurationRepository = PublicConfigurationRepositoryImpl(baseURL: url, networkWorker: worker)
        publicConfigurationUseCase = PublicConfigurationUseCase(publicConfigurationRepository: publicConfigurationRepository)
    }
    
    public required init() {
        
        fatalError("init() has not been implemented")
    }
    
    // MARK: override methods
    
    override open func dispose() {
        
        publicConfigurationUseCase?.dispose()
        super.dispose()
    }
    
    override public func onUseCaseError(error: Error) {
        
        if let serviceError = error as? ServiceError {
            
            switch serviceError {
            case .GenericErrorEntity(let errorEntity):
                let errorBVABO = ErrorBO(error: errorEntity)
                self.fire(actionSpec: PublicConfigurationStore.onGetPublicConfigurationError, value: errorBVABO)
            default:
                break
            }
        }
    }

    // MARK: Public methods
    
    public func getPublicConfiguration() {
        
        let useCaseCallback = UseCase<PublicConfigurationDTO, Void>.UseCaseCallback<PublicConfigurationDTO>(onUseCaseComplete: { [weak self] result in
            self?.fire(actionSpec: PublicConfigurationStore.onGetPublicConfigurationSuccess, value: result)
            }, onUseCaseError: onUseCaseError)
        
        publicConfigurationUseCase?.execute(params: nil, useCaseCallback: useCaseCallback)
    }
}
