//
//  PublicConfigurationUseCaseTests.swift
//  shoppingapp
//
//  Created by Arturo Marzo de la Fuente on 13/11/2018.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import BBVA_Network
import XCTest

#if TESTMX
@testable import shoppingappMX
#elseif TESTPE
@testable import shoppingappPE
#endif

class PublicConfigurationUseCaseTests: XCTestCase {
    
    var sut: PublicConfigurationUseCase!
    var mockPublicConfigurationRepository = MockPublicConfigurationRepository()
    
    override func setUp() {
        
        super.setUp()
        sut = PublicConfigurationUseCase(publicConfigurationRepository: mockPublicConfigurationRepository)
    }
    
    func test_givenAResponse_whenCallPublicConfigurationUseCase_thenIsCalledGetPublicConfiguration() {
        
        let response = Response()
        response.body = "{}"
        
        //when
        sut.call(params: nil, success: { _ in
            //then
            XCTAssertTrue(self.mockPublicConfigurationRepository.isCalledGetPublicConfiguration)
            
        }, error: { _ in
            
            XCTFail("Not expected error")
        })
    }
}
