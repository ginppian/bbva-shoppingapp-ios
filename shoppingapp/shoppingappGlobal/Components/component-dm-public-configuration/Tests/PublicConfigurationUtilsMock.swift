//
//  UtilsMock.swift
//  shoppingapp
//
//  Created by Arturo Marzo de la Fuente on 13/11/2018.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import BBVA_Network
import CellsNativeComponents

#if TESTMX
@testable import shoppingappMX
#elseif TESTPE
@testable import shoppingappPE
#endif

class MockPublicConfigurationNetworkWorker: NetworkWorker {
    
    var response: Response?
    var isError = false
    var isBadBody = false
    
    override func execute(request: NetworkRequest, completion: @escaping (Response) -> Void) -> URLSessionDataTask? {
        
        if isError == true {
            
            response = Response()
            response?.error = ServiceError.Other(NSError())
            completion(response!)
        } else if isBadBody == true {
            
            response = Response()
            response?.body = "{\"bad\":\"test\"}"
            completion(response!)
        } else {
            
            response = Response()
            response?.body = "{ \"data\": [ { \"key\": \"app_settings\", \"value\": \"{\\\"data\\\":{\\\"lastUpdate\\\":1519835704,\\\"publicConfig\\\":{\\\"hasRateApp\\\":true,\\\"mobileBankingSchema\\\":\\\"com.bancomer.bbva.bancomermovil://\\\",\\\"update\\\":{\\\"iOS\\\":{\\\"message\\\":\\\"Es necesario actualizar la aplicaciÃ³n para seguir disfrutando de todos sus servicios.\\\",\\\"isMandatory\\\":true,\\\"URL\\\":\\\"https://itunes.apple.com/us/app/bbva-wallet-mexico/id970582311?mt=8\\\"},\\\"Android\\\":{\\\"message\\\":\\\"Es necesario actualizar la aplicaciÃ³n para seguir disfrutando de todos sus servicios.\\\",\\\"isMandatory\\\":true,\\\"URL\\\":\\\"https://itunes.apple.com/us/app/bbva-wallet-mexico/id970582311?mt=8\\\"}},\\\"generalTermsUrl\\\":\\\"https://m.bbva.es/politica-de-privacidad-apps/BBVA_Wallet_condiciones.jsp\\\",\\\"traceability\\\":{\\\"level\\\":0},\\\"maintenance\\\":{\\\"iOS\\\":{\\\"message\\\":\\\"BBVA se encuentra realizando tareas de mantenimiento para continuar ofreciÃ©ndote el mejor servicio.\\\",\\\"maintenancePeriod\\\":{\\\"endTime\\\":\\\"2018-01-20T05:55:22.000+0100\\\",\\\"initTime\\\":\\\"2018-01-20T00:55:22.000+0100\\\"}},\\\"Android\\\":{\\\"message\\\":\\\"BBVA se encuentra realizando tareas de mantenimiento para continuar ofreciÃ©ndote el mejor servicio.\\\",\\\"maintenancePeriod\\\":{\\\"endTime\\\":\\\"2018-01-20T05:55:22.000+0100\\\",\\\"initTime\\\":\\\"2018-01-20T00:55:22.000+0100\\\"}}},\\\"callCenterPhone\\\":\\\"(+52) 55 5226 2663\\\",\\\"mobileBankingUrl\\\":\\\"https://itunes.apple.com/es/app/bancomer-movil/id374824226?mt=8\\\"}}}\" } ] }"
            completion(response!)
        }
        
        return URLSessionDataTask()
    }
}

class MockPublicConfigurationRepository: PublicConfigurationRepository {
    
    var isCalledGetPublicConfiguration = false
    
    init() {
        
    }
    
    func getPublicConfiguration(success: @escaping (_ result: PublicConfigurationDTO) -> Void, error: @escaping (_ error: Error) -> Void) {
        
        isCalledGetPublicConfiguration = true
    }
    
}

class MockPublicConfigurationUseCase: PublicConfigurationUseCase {
    
    var requestSuccess = false
    var isCalledExecute = false
    
    override func call(params: Void?, success: @escaping (PublicConfigurationDTO) -> Void, error: @escaping (Error) -> Void) {
        
        if requestSuccess == true {
            
            success(PublicConfigurationDTO())
        } else {
            
            error(ServiceError.Other(NSError()))
        }
    }
    
    override func execute(params: Void?, useCaseCallback: UseCase<PublicConfigurationDTO, Void>.UseCaseCallback<PublicConfigurationDTO>) {
        
        isCalledExecute = true
        call(params: nil, success: useCaseCallback.onUseCaseComplete!, error: useCaseCallback.onUseCaseError!)
    }
}
