//
//  PublicConfigurationRepositoryTests.swift
//  shoppingapp
//
//  Created by Arturo Marzo de la Fuente on 13/11/2018.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import BBVA_Network
import XCTest

#if TESTMX
@testable import shoppingappMX
#elseif TESTPE
@testable import shoppingappPE
#endif

class PublicConfigurationRepositoryTests: XCTestCase {
    
    var sut: PublicConfigurationRepository!
    var networkWorkerMock: MockPublicConfigurationNetworkWorker!
    
    override func setUp() {
        
        super.setUp()
        networkWorkerMock = MockPublicConfigurationNetworkWorker()
        sut = PublicConfigurationRepositoryImpl(baseURL: "", networkWorker: networkWorkerMock)
    }
    
    func test_givenAErrorInService_whenCallGetPublicConfiguration_thenCompletionError() {
        
        networkWorkerMock.isError = true
        
        sut.getPublicConfiguration(success: { _ in
            
            XCTFail("Not expected success")
        }, error: { error in
            
            XCTAssert((error as? ServiceError) != nil)
        })
    }
    
    func test_givenAErrorInParseBody_whenCallGetPublicConfiguration_thenCompletionError() {
        
        networkWorkerMock.isBadBody = true
        
        sut.getPublicConfiguration(success: { _ in
            
            XCTFail("Not expected success")
        }, error: { error in
            
            XCTAssert((error as? ServiceError) != nil)
        })
    }
    
    func test_givenASuccessResponse_whenCallGetPublicConfiguration_thenCompletionSuccess() {
        
        sut.getPublicConfiguration(success: { response in
        
            XCTAssertNotNil(response)
        }, error: { _ in
            
            XCTFail("Not expected fail")
        })
    }
}
