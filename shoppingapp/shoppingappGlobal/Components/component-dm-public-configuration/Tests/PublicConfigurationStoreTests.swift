//
//  PublicConfigurationStoreTests.swift
//  shoppingapp
//
//  Created by Arturo Marzo de la Fuente on 13/11/2018.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import BBVA_Network
import CellsNativeComponents
import XCTest

#if TESTMX
@testable import shoppingappMX
#elseif TESTPE
@testable import shoppingappPE
#endif

class PublicConfigurationStoreTests: XCTestCase {
    
    private var mockNetworkWorker: MockPublicConfigurationNetworkWorker!
    private var mockPublicConfigurationUseCase: MockPublicConfigurationUseCase!
    private var sut: PublicConfigurationStore!
    private var publisher = VerifcablePublisher()
    
    override func setUp() {
        
        super.setUp()
        
        mockNetworkWorker = MockPublicConfigurationNetworkWorker()
        mockPublicConfigurationUseCase = MockPublicConfigurationUseCase(publicConfigurationRepository: MockPublicConfigurationRepository())
        sut = PublicConfigurationStore(worker: mockNetworkWorker, publicConfigurationUseCase: mockPublicConfigurationUseCase)
    }
    
    override func tearDown() {
        
        super.tearDown()
        publisher.reset()
    }
    
    func test_givenAny_whenCallGetPublicConfigurationAndCompletionSuccess_thenFireOnBlockSuccess() {

        //Given
        mockPublicConfigurationUseCase.requestSuccess = true

        //When
        sut.getPublicConfiguration()

        //Then
        XCTAssertTrue(publisher.verify(viewModelId: PublicConfigurationStore.identifier, action: PublicConfigurationStore.onGetPublicConfigurationSuccess))
    }
    
    func test_givenAny_whenCallGetPublicConfigurationAndCompletionError_thenIsCalledExecute() {
        
        //Given
        
        //When
        sut.getPublicConfiguration()
        
        //Then
        XCTAssertTrue(mockPublicConfigurationUseCase.isCalledExecute)
    }
}
