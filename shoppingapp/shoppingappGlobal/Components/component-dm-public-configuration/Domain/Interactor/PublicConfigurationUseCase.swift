//
//  PublicConfigurationUseCase.swift
//  shoppingapp
//
//  Created by Javier Pino on 5/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

import CellsNativeComponents

class PublicConfigurationUseCase: UseCaseAsync<PublicConfigurationDTO, Void> {
    
    private let publicConfigurationRepository: PublicConfigurationRepository
    
    init(publicConfigurationRepository: PublicConfigurationRepository) {
        
        self.publicConfigurationRepository = publicConfigurationRepository
        super.init()
    }
    
    override func call(params: Void?, success: @escaping (PublicConfigurationDTO) -> Void, error: @escaping (Error) -> Void) {
        
        self.publicConfigurationRepository.getPublicConfiguration(success: success, error: error)
    }
}
