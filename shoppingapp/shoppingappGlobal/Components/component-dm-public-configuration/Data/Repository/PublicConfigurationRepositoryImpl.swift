//
//  PublicConfigurationRepositoryImpl.swift
//  shoppingapp
//
//  Created by Javier Pino on 5/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import BBVA_Network

class PublicConfigurationRepositoryImpl: BaseRepositoryImpl, PublicConfigurationRepository {
    
    fileprivate static var getPublicConfigurationHeaders = [Headers.CONTENT_TYPE: Headers.APP_JSON, Headers.UTF8: Headers.CHARSET_ACCEPTED]
    
    func getPublicConfiguration(success: @escaping (_ result: PublicConfigurationDTO) -> Void, error: @escaping (_ error: Error) -> Void) {
    
        let request: NetworkRequest = createRequest(baseURL, Headers.BASIC_HEADER, nil, .useProtocolCachePolicy, 30, NetworkMethod.GET)
        
        _ = networkWorker.execute(request: request) { (response: Response) in
            
            if response.error == nil {
                
                let settingsResponse = AppSettingsResponse.deserialize(from: response.body)
                
                if let arrayOfData = settingsResponse?.data, !arrayOfData.isEmpty, let escapedJson = arrayOfData[0].value, let publicConfiguration = PublicConfiguration.deserialize(from: escapedJson), let data = publicConfiguration.data {
                    
                    success(data)
                } else {

                    error(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
                }
            } else {

                if let serviceError = self.checkShowError(response: response, security: false) {
                    
                    error(serviceError)
                }
            }
        }
    }
}
