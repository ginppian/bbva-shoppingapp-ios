//
//  PublicConfigurationRepository.swift
//  shoppingapp
//
//  Created by Javier Pino on 5/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

public protocol PublicConfigurationRepository {
    
    func getPublicConfiguration(success: @escaping (_ result: PublicConfigurationDTO) -> Void, error: @escaping (_ error: Error) -> Void)
}
