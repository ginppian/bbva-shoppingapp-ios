//
//  PublicConfigurationDTO.swift
//  shoppingapp
//
//  Created by Javier Pino on 5/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import HandyJSON

struct AppSettingsResponse: HandyJSON {
    
    var data: [AppSettingsData]?
}

struct AppSettingsData: HandyJSON {
    
    var key: String?
    var value: String?
}

struct PublicConfiguration: HandyJSON {
    
    var data: PublicConfigurationDTO?
}

open class PublicConfigurationDTO: CellsDTO, HandyJSON {
    
    var publicConfig: PublicConfigData?
    var lastUpdate: NSNumber?
    
    public required init() {}
}

struct PublicConfigData: Equatable, HandyJSON {
    
    var update: UpdateData?
    var maintenance: MaintenanceData?
    var traceability: TraceabilityData?
    var characterLimits: CharacterLimitsData?
    var help: HelpData?
    var terms: TermsData?
    var mobileBancomer: MobileAppsData?
    var mobileGlomo: MobileAppsData?
    var hasRateApp: Bool?
    var callCenterPhone: String?
    var promotionDetailContactPhone: String?
    var patternSMS: String?
}

struct UpdateData: Equatable, HandyJSON {
    
    var Android: UpdateOSData?
    var iOS: UpdateOSData?
}

struct MaintenanceData: Equatable, HandyJSON {
    
    var Android: MaintenanceOSData?
    var iOS: MaintenanceOSData?
}

struct TraceabilityData: Equatable, HandyJSON {
    
    var level: NSNumber?
}

struct CharacterLimitsData: Equatable, HandyJSON {
    
    var movementsConcept: NSNumber?
    var movementsAmount: NSNumber?
    var limitsAmount: NSNumber?
    var searchPromotionsName: NSNumber?
    var searchPromotionsLocation: NSNumber?
}

struct HelpData: Equatable, HandyJSON {
    
    var contextHelp: String?
    var faq: FAQData?
    var didacticArea: String?
}

struct TermsData: Equatable, HandyJSON {
    
    var cud: String?
    var tyc: String?
    var privacy: String?
}

struct MobileAppsData: Equatable, HandyJSON {
    
    var Android: MobileAppOSData?
    var iOS: MobileAppOSData?
}

struct UpdateOSData: Equatable, HandyJSON {
    
    var isMandatory: Bool?
    var URL: String?
    var message: String?
    var version: String?
    
    func bundleVersionIsSmaller() -> Bool {
        guard let version = version else {
            return false
        }
        
        let bundleVersion = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String
        
        if let bundleVersion = bundleVersion, bundleVersion.compare(version, options: .numeric) == .orderedAscending {
            return true
        }
        
        return false
    }
}

struct MaintenanceOSData: Equatable, HandyJSON {
    
    var maintenancePeriod: MaintenancePeriodData?
    var message: String?
}

struct MaintenancePeriodData: Equatable, HandyJSON {
    
    var initTime: String?
    var endTime: String?
}

struct FAQData: Equatable, HandyJSON {
    
    var helpAbout: String?
    var helpDigitalCard: String?
    var helpHce: String?
    var helpOnOff: String?
    var helpPoints: String?
    var helpPromotionsDetail: String?
    var helpSticker: String?
}

struct MobileAppOSData: Equatable, HandyJSON {
    
    var schema: String?
    var url: String?
}
