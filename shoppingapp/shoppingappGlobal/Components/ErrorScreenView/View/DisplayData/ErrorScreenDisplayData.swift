//
//  ErrorScreenDisplayData.swift
//  shoppingapp
//
//  Created by Javier Pino on 30/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

struct ErrorScreenDisplayData {

    let title: String
    let message: String
    let hasRetryButton: Bool
}
