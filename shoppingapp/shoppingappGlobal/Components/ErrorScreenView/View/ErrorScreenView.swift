//
//  ErrorScreenView.swift
//  shoppingapp
//
//  Created by Javier Pino on 30/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol ErrorScreenViewDelegate: class {

    func errorScreenViewRetryButtonPressed(_ errorScreenView: ErrorScreenView)
}

class ErrorScreenView: XibView {

    weak var delegate: ErrorScreenViewDelegate?

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var retryButton: UIButton!
    @IBOutlet weak var bottomImageView: UIImageView!

    override func configureView() {

        setDefaultBackgroundColor()

        configureTitleLabel()
        configureMessageLabel()
        configureRetryButton()

        setDefaultRetryButtonTitle()
    }

    func configureError(displayData: ErrorScreenDisplayData) {

        setDefaultErrorImage()
        configure(displayData: displayData)
    }

    func configureNoContent(displayData: ErrorScreenDisplayData) {

        setDefaultNoContentImage()
        configure(displayData: displayData)
    }

    @IBAction func retryButtonPressed(_ sender: Any) {

        delegate?.errorScreenViewRetryButtonPressed(self)
    }
}

// MARK: - Private methods
private extension ErrorScreenView {

    func configureTitleLabel() {

        titleLabel.font = Tools.setFontBook(size: 22.0)
        titleLabel.textColor = .BBVA600
    }

    func configureMessageLabel() {

        messageLabel.font = Tools.setFontBook(size: 14.0)
        messageLabel.textColor = .BBVA600
    }

    func configureRetryButton() {

        retryButton.titleLabel?.font = Tools.setFontMedium(size: 14.0)
        retryButton.setTitleColor(.MEDIUMBLUE, for: .normal)
        retryButton.setTitleColor(.COREBLUE, for: .highlighted)
        retryButton.setTitleColor(.COREBLUE, for: .selected)
    }

    func setDefaultBackgroundColor() {

        backgroundColor = .BBVAWHITELIGHTBLUE
    }

    func setDefaultErrorImage() {

        bottomImageView.image = UIImage(named: ConstantsImages.Common.desert_rocket)
    }

    func setDefaultNoContentImage() {

        bottomImageView.image = UIImage(named: ConstantsImages.Common.deser_ovni)
    }

    func setDefaultRetryButtonTitle() {
        retryButton.setTitle(Localizables.common.key_retry_text, for: .normal)
    }

    func configure(displayData: ErrorScreenDisplayData) {

        titleLabel.text = displayData.title
        messageLabel.text = displayData.message
        retryButton.isHidden = !displayData.hasRetryButton
    }
}
