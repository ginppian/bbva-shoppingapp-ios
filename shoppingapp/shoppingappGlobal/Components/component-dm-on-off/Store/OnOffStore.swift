//
//  OnOffStore.swift
//  shoppingapp
//
//  Created by AZIZEBULBUL on 01/08/2018.
//  Copyright © 2018 BBVA. All rights reserved.
//

import BBVA_Network
import CellsNativeComponents

open class OnOffStore: NetworkStore {

    public static let ID: String = "component-data-manager-on-off-store"

    public static let ON_UPDATE_SUCCESS = ActionSpec<OnOffResponseDTO>(id: "on-update-success")
    static let ON_UPDATE_ERROR = ActionSpec<ErrorBO>(id: "on-update-error")

    var host: String?
    var countryCode: String?

    fileprivate var onOffUseCase: OnOffUseCase?
    fileprivate var worker: NetworkWorker

    public init(worker: NetworkWorker, host: String? = nil) {

        self.worker = worker
        self.host = host

        super.init(OnOffStore.ID)

        guard host == nil || (host?.isEmpty)! else {
            return
        }

        configureStoreWithProperties()
    }

    public required init() {
        fatalError("init() has not been implemented")
    }

    fileprivate func configureStoreWithProperties() {

        newProperty(key: "host") { [weak self] (value: Any?) in

            if let value = value {
                self?.host = String(describing: value)
            }
        }
        newProperty(key: "country") { [weak self] (value: Any?) in

            if let value = value {
                self?.countryCode = String(describing: value)
            }
        }
    }

    public func updateCardStatusRequest(_ updateCard: OnOffDTO) {

        let useCaseCallback = UseCase<OnOffResponseDTO, OnOffDTO>.UseCaseCallback<OnOffResponseDTO>(onUseCaseComplete: { result in

            self.fire(actionSpec: OnOffStore.ON_UPDATE_SUCCESS, value: result)
        }, onUseCaseError: self.onUseCaseError)

        onOffUseCase?.execute(params: updateCard, useCaseCallback: useCaseCallback)
    }

    override open func onPropertiesBounds() {

        guard let host = host else {
            return
        }

        let onOffRepository: OnOffRepository = OnOffRepositoryImpl(baseURL: host, networkWorker: worker)

        onOffUseCase = OnOffUseCase(onOffRepository: onOffRepository)
    }

    override open func dispose() {
        onOffUseCase?.dispose()
        super.dispose()
    }

    override public func onUseCaseError(error: Error) {
        print("onUseCaseError: \(error)")

        guard let evaluate = error as? ServiceError else {
            return
        }

        switch evaluate {
        case .GenericErrorEntity(let errorEntity):
            let errorBVABO = ErrorBO(error: errorEntity)
            self.fire(actionSpec: OnOffStore.ON_UPDATE_ERROR, value: errorBVABO)
        default:
            break
        }
    }
}
