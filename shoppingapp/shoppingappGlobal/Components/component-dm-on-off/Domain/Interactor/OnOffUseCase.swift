//
//  OnOffUseCase.swift
//  shoppingapp
//
//  Created by AZIZEBULBUL on 01/08/2018.
//  Copyright © 2018 BBVA. All rights reserved.
//

import CellsNativeComponents

class OnOffUseCase: UseCaseAsync<OnOffResponseDTO, OnOffDTO> {

    private let onOffRepository: OnOffRepository

    init(onOffRepository: OnOffRepository) {

        self.onOffRepository = onOffRepository
        super.init()

    }

    override func call(params: OnOffDTO?, success: @escaping (OnOffResponseDTO) -> Void, error: @escaping (_ error: Error) -> Void) {

        if let params = params {
            self.onOffRepository.changeCardStatus(params: params, success: success, error: error)
        } else {
            DLog(message: "Error no params")
            error(ServiceError.GenericErrorEntity(error: ErrorEntity(message: "Error no params")))
        }
    }
}
