//
//  OnOffResponseDTO.swift
//  shoppingapp
//
//  Created by AZIZEBULBUL on 01/08/2018.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import HandyJSON

public class OnOffResponseDTO: CellsDTO, HandyJSON {

    var data: OnOffResponse?
    var apiInfo: ApiInfoDTO?

    public required init() {}
}

struct OnOffResponse: HandyJSON {

    var activationId: String!
    var name: String!
    var isActive: Bool!

}

struct ApiInfoDTO: HandyJSON {

    var category: String!
    var name: String!
    var version: String!

}
