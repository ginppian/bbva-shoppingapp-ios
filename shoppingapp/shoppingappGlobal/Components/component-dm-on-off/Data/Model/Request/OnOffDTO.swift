//
//  OnOffDTO.swift
//  shoppingapp
//
//  Created by AZIZEBULBUL on 01/08/2018.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import HandyJSON

public class OnOffDTO: CellsDTO {

    var id: String
    var enabled: Bool

    init?(cardId: String?, enabled: Bool?) {
        if let cardId = cardId, let enabled = enabled {
            self.id = cardId
            self.enabled = enabled
        } else {
            return nil
        }

    }
}

public class OnOffBody: HandyJSON {

    let isActive: Bool

    init(_ isActive: Bool) {
        self.isActive = isActive
    }

    public required init() {
        isActive = false
    }
}
