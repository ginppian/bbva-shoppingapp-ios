//
//  OnOffRepositoryImpl.swift
//  shoppingapp
//
//  Created by AZIZEBULBUL on 01/08/2018.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

import ObjectMapper
import BBVA_Network
import HandyJSON

class OnOffRepositoryImpl: BaseRepositoryImpl, OnOffRepository {

    fileprivate static let ON_OFF_CARD_URL_PREFIX = "/cards/"
    fileprivate static let ON_OFF_CARD_FINAL_URL_SUFFIX = "/activations/ON_OFF"

    func changeCardStatus(params: OnOffDTO, success: @escaping (OnOffResponseDTO) -> Void, error: @escaping (Error) -> Void) {

        let url = baseURL + OnOffRepositoryImpl.ON_OFF_CARD_URL_PREFIX + params.id + OnOffRepositoryImpl.ON_OFF_CARD_FINAL_URL_SUFFIX

        let body = OnOffBody(params.enabled)

        guard let bodyParser = body.toJSON() else {
            return
        }

        let request: NetworkRequest = createRequest(url, Headers.BASIC_HEADER, bodyParser, .useProtocolCachePolicy, 30, NetworkMethod.PATCH)

        _ = networkWorker.execute(request: request) { (response: Response) in

            if response.error == nil {

                if  let dataJson = response.body {

                    if let updatedCardDTO = OnOffResponseDTO.deserialize(from: dataJson) {
                        success(updatedCardDTO)
                    } else {
                        error(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
                    }

                } else {
                    error(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
                }

            } else {

                if let serviceError = self.checkShowError(response: response, security: false) {
                    error(serviceError)
                }
            }

        }
    }
}
