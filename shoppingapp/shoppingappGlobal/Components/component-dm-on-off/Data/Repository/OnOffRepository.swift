//
//  OnOffRepository.swift
//  shoppingapp
//
//  Created by AZIZEBULBUL on 01/08/2018.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol OnOffRepository {

    func changeCardStatus(params: OnOffDTO, success: @escaping (_ result: OnOffResponseDTO) -> Void, error: @escaping (_ error: Error) -> Void)

}
