//
//  CustomProgressView.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 11/10/2017.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit

class CustomProgressView: XibView {

    static let animationKey = "AnimationProgress"

    static let leadingShadow: CGFloat = 20.0
    static let positionX: CGFloat = 12.0
    static let positionY: CGFloat = 12.0

    static let minProgress: CGFloat = 0.0
    static let maxProgress: CGFloat = 1.0

    var xWidth: CGFloat = 0.0
    var topProgress: CGFloat = 0.0 {
        didSet {
            drawTopProgress()
        }
    }

    var middleProgress: CGFloat = 0.0 {
        didSet {
            drawMiddleProgress()
        }
    }

    let topLayerProgress = CAShapeLayer()
    let middleLayerProgress = CAShapeLayer()

    override func configureView() {

        super.configureView()

        backgroundColor = .clear

    }

    override func draw(_ rect: CGRect) {
        super.draw(rect)

        xWidth = bounds.size.width

        let bottomLayer = CAShapeLayer()

        bottomLayer.path = createTrackingPath().cgPath

        bottomLayer.fillColor = UIColor.BBVA200.cgColor

        layer.addSublayer(bottomLayer)

        layer.addSublayer(middleLayerProgress)

        layer.addSublayer(topLayerProgress)

        drawTopProgress()

        drawMiddleProgress()

    }

    func setTopProgress(progress endPoint: CGFloat) {

        topLayerProgress.path = createProgressPath(endPoint: endPoint).cgPath

        topLayerProgress.fillColor = UIColor.AQUA.cgColor

    }

    func drawTopProgress() {

        topLayerProgress.path = createProgressPath(toProgress: topProgress).cgPath

        topLayerProgress.fillColor = UIColor.AQUA.cgColor

    }

    func setMiddleProgress(_ progress: CGFloat, withAnimation animation: Bool) {

        let currentPath = middleLayerProgress.path
        let newPath = createProgressPath(toProgress: progress).cgPath

        if animation {

            let morph = CABasicAnimation(keyPath: "path")
            morph.duration = 1
            morph.fromValue = currentPath
            morph.toValue = newPath
            morph.delegate = self
            morph.setValue(progress, forKey: CustomProgressView.animationKey)
            middleLayerProgress.add(morph, forKey: nil)

        } else {
            middleProgress = progress
        }

        middleLayerProgress.path = newPath

        middleLayerProgress.fillColor = UIColor.BBVA300.cgColor

    }

    func drawMiddleProgress() {

        middleLayerProgress.path = createProgressPath(toProgress: middleProgress).cgPath

        middleLayerProgress.fillColor = UIColor.BBVA300.cgColor

    }

    private func createTrackingPath() -> UIBezierPath {

        let pathTracking = UIBezierPath()

        pathTracking.move(to: CGPoint(x: CustomProgressView.positionX, y: CustomProgressView.positionY))
        pathTracking.addLine(to: CGPoint(x: CustomProgressView.leadingShadow, y: 0.0))
        pathTracking.addLine(to: CGPoint(x: xWidth, y: 0.0))
        pathTracking.addLine(to: CGPoint(x: xWidth - 8, y: 12.0))

        pathTracking.close()

        return pathTracking

    }

    private func createProgressPath(toProgress progress: CGFloat) -> UIBezierPath {

        let progressLimited = min(max(CustomProgressView.minProgress, progress), CustomProgressView.maxProgress)

        let endPoint = progressLimited * (xWidth - CustomProgressView.leadingShadow)

        return createProgressPath(endPoint: endPoint)

    }

    private func createProgressPath(endPoint: CGFloat) -> UIBezierPath {

        let pathProgress = UIBezierPath()

        pathProgress.move(to: CGPoint(x: CustomProgressView.positionX, y: CustomProgressView.positionY))

        pathProgress.addLine(to: CGPoint(x: CustomProgressView.leadingShadow, y: 0.0))

        pathProgress.addLine(to: CGPoint(x: endPoint + CustomProgressView.leadingShadow, y: 0.0))

        pathProgress.addLine(to: CGPoint(x: endPoint + 12, y: 12.0))

        pathProgress.close()

        return pathProgress

    }

}

extension CustomProgressView: CAAnimationDelegate {

    func animationDidStop(_ anim: CAAnimation, finished: Bool) {

        let progress = anim.value(forKeyPath: CustomProgressView.animationKey) as? CGFloat

        if finished, let progress = progress {

            middleProgress = progress

        }

    }

}
