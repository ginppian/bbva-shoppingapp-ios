//
//  InformationViewDisplayData.swift
//  shoppingapp
//
//  Created by Javier Pino on 18/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

struct InformationViewDisplayData {

    var iconImage: UIImage
    var message: String
    var actionButtonTitle: String
}
