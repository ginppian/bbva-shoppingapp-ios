//
//  InformationView.swift
//  shoppingapp
//
//  Created by Javier Pino on 18/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol InformationViewDelegate: class {

    func informationViewActionButtonPressed(_ informationView: InformationView)
}

class InformationView: XibView {

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var actionButton: UIButton!

    weak var delegate: InformationViewDelegate?

    override func configureView() {

        super.configureView()

        backgroundColor = .BBVAWHITE

        messageLabel.font = Tools.setFontBook(size: 14.0)
        messageLabel.textColor = .BBVA600

        actionButton.titleLabel?.font = Tools.setFontBold(size: 14.0)
        actionButton.setTitleColor(.MEDIUMBLUE, for: .normal)
        actionButton.setTitleColor(.COREBLUE, for: .highlighted)
        actionButton.setTitleColor(.COREBLUE, for: .selected)
    }

    func configure(displayData: InformationViewDisplayData) {

        iconImageView.image = displayData.iconImage
        messageLabel.text = displayData.message
        actionButton.setTitle(displayData.actionButtonTitle, for: .normal)
    }

    @IBAction func actionButtonPressed(_ sender: Any) {

        delegate?.informationViewActionButtonPressed(self)
    }

}
