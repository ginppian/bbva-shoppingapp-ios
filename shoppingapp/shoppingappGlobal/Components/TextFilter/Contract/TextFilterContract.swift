//
//  TextFilterContract.swift
//  shoppingapp
//
//  Created by jesus.martinez on 27/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

protocol TextFilterViewProtocol: ComponentViewProtocol {

    func clearInput(withValue value: String)
    func clearPlaceHolderText()
    func setPlaceHolder(withText text: String)
    func setMaximumNumberOfCharacters(withMaximum maximum: Int)
    func setValidCharacters(withCharactersSet charactersSet: CharacterSet)
    func setText(withText text: String)

}

protocol TextFilterPresenterProtocol: ComponentPresenterProtocol {

    func onConfigureView()
    func removeAction()
    func textFieldDidBeginEditing()
    func textFieldDidEndEditing()

}
