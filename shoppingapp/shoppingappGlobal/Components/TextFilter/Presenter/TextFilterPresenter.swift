//
//  TextFilterPresenter.swift
//  shoppingapp
//
//  Created by jesus.martinez on 27/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

class TextFilterPresenter {

    weak var view: TextFilterViewProtocol?

    var displayData: TextFilterDisplayData? {
        didSet {
            onConfigureView()
        }
    }

    func setPlacerHolderIfPlaceHolderTitle() {

        if let placeHolderTitle = displayData?.placeHolderTitle {
            view?.setPlaceHolder(withText: placeHolderTitle)
        }
    }
}

extension TextFilterPresenter: TextFilterPresenterProtocol {

    func onConfigureView() {

        setPlacerHolderIfPlaceHolderTitle()

        if let maxLength = displayData?.maximumLength {
            view?.setMaximumNumberOfCharacters(withMaximum: maxLength)
        }

        if let validCharacters = displayData?.validCharacterSet {
            view?.setValidCharacters(withCharactersSet: validCharacters)
        }

        if let defaultText = displayData?.defaultValue {
            view?.setText(withText: defaultText)
        } else {
            view?.clearInput(withValue: "")
        }

    }

    func removeAction() {

         view?.clearInput(withValue: "")

    }

    func textFieldDidBeginEditing() {

        view?.clearPlaceHolderText()

    }

    func textFieldDidEndEditing() {

        setPlacerHolderIfPlaceHolderTitle()

    }

}
