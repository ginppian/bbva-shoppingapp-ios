//
//  ConceptFilterView.swift
//  shoppingapp
//
//  Created by jesus.martinez on 25/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit

protocol TextFilterViewDelegate: class {

    func textFilterUpdate(textFilterView: TextFilterView)

}

class TextFilterView: XibView {

    var presenter = TextFilterPresenter()

    weak var delegate: TextFilterViewDelegate?

    static let defaultHeight: CGFloat = 60.0

    var maximumCharacters: Int?
    var validCharacters: CharacterSet?

    // MARK: @IBOutlet's
    @IBOutlet weak var textField: UITextField!
    var removeButton: UIButton!

    // MARK: - Private Helper Methods

    override func configureView() {
        super.configureView()

        textField.backgroundColor = .COREBLUE
        textField.tintColor = .BBVAWHITE
        textField.font = Tools.setFontBook(size: 16)
        textField.delegate = self

        let rightView = UIView(frame: CGRect(x: 0, y: textField.frame.size.height / 2 - 8, width: 44, height: 24))
        removeButton = UIButton(frame: CGRect(x: 20, y: 0, width: 24, height: 24))
        removeButton.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Common.close_icon, color: .BBVAWHITE), width: 16, height: 16), for: .normal)
        removeButton.addTarget(self, action: #selector(removeConceptAction), for: .touchUpInside)
        rightView.addSubview(removeButton)

        textField.rightView = rightView
        textField.rightViewMode = .whileEditing

        presenter.view = self
        presenter.onConfigureView()

    }

    @objc func removeConceptAction() {

        presenter.removeAction()

    }

}

extension TextFilterView: TextFilterViewProtocol {

    func setText(withText text: String) {

        textField.text = text

    }

    func clearInput(withValue value: String) {

        textField.text = value

    }

    func clearPlaceHolderText() {

        textField.placeholder = ""

    }

    func setPlaceHolder(withText text: String) {

        let attributes = [
            NSAttributedStringKey.foregroundColor: UIColor.BBVAWHITE,
            NSAttributedStringKey.font: Tools.setFontBook(size: 16)
        ]

        textField.attributedPlaceholder = NSAttributedString(string: text, attributes: attributes)

    }

    func setMaximumNumberOfCharacters(withMaximum maximumCharacters: Int) {
        self.maximumCharacters = maximumCharacters
    }

    func setValidCharacters(withCharactersSet charactersSet: CharacterSet) {
        validCharacters = charactersSet
    }

}

extension TextFilterView: UITextFieldDelegate {

    func textFieldDidBeginEditing(_ textField: UITextField) {
        presenter.textFieldDidBeginEditing()
        delegate?.textFilterUpdate(textFilterView: self)
        dynamicAccessibility(forTextfield: textField, withVisibility: false)

    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        presenter.textFieldDidEndEditing()
        delegate?.textFilterUpdate(textFilterView: self)
        dynamicAccessibility(forTextfield: textField, withVisibility: true)

    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        var shouldChangeCharactersInRange = true

        if let validCharactersSet = validCharacters {
            shouldChangeCharactersInRange = shouldChangeCharactersIn(string: string, forCharacterSet: validCharactersSet)
        }

        if let maxCharacters = maximumCharacters, shouldChangeCharactersInRange {
            shouldChangeCharactersInRange = shouldChangeCharactersInRange && self.textField(textField, shouldChangeCharactersIn: range, replacementString: string, withNumberMaximumCharacters: maxCharacters)
        }

        return shouldChangeCharactersInRange
    }

    private func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String, withNumberMaximumCharacters maximumLenth: Int) -> Bool {

        let currentString = textField.text ?? ""
        let castedNSString = currentString as NSString?
        let newString = castedNSString?.replacingCharacters(in: range, with: string)

        return (newString?.count)! <= maximumLenth
    }

    private func shouldChangeCharactersIn(string: String, forCharacterSet characterSet: CharacterSet) -> Bool {

        let aSet = characterSet.inverted
        let compSepByCharInSet = string.components(separatedBy: aSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")

        return string == numberFiltered
    }

}

// MARK: - APPIUM

private extension TextFilterView {

    func dynamicAccessibility(forTextfield textfield: UITextField, withVisibility visibility: Bool) {
        #if APPIUM
            textfield.isAccessibilityElement = visibility
        #endif
    }

}
