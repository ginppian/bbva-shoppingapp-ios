//
//  TextFilterDisplayData.swift
//  shoppingapp
//
//  Created by jesus.martinez on 20/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

struct TextFilterDisplayData {

    let placeHolderTitle: String
    let maximumLength: Int?
    let validCharacterSet: CharacterSet?
    let defaultValue: String?

    init(placeHolderTitle: String, maximumLength: Int? = nil, validCharacterSet: CharacterSet? = nil, defaultValue: String? = nil) {
        self.placeHolderTitle = placeHolderTitle
        self.maximumLength = maximumLength
        self.validCharacterSet = validCharacterSet
        self.defaultValue = defaultValue
    }

}
