//
//  FiltersScrollContract.swift
//  shoppingapp
//
//  Created by Arturo Marzo de la Fuente on 30/8/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

protocol FiltersScrollViewProtocol: ComponentViewProtocol {

    func loadFilters(filters: [FilterScrollDisplayData]?)
    func reloadFilters(filters: [FilterScrollDisplayData]?)
    func removeFilter(filter: FilterScrollDisplayData)
    func removeFilterLonglyPushed()
    func removeAllFilters()

}

protocol FiltersScrollPresenterProtocol: ComponentPresenterProtocol {

    func receivedNewFilters(filters: [FilterScrollDisplayData]?)
    func removeFilterPushed(filter: FilterScrollDisplayData)
    func removeFilterLonglyPushed()
    func removeAllFiltersPushed()

}
