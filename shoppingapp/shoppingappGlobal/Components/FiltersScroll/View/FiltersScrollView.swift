//
//  FiltersScrollView.swift
//  shoppingapp
//
//  Created by Arturo Marzo de la Fuente on 30/8/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit

protocol FiltersScrollViewDelegate: class {

    func removeFilterPushed(filter: FilterScrollDisplayData, inFiltersScrollView filtersScrollView: FiltersScrollView)
    func removeFilterLonglyPushed(inFiltersScrollView filtersScrollView: FiltersScrollView)
    func removeAllFiltersPushed(inFiltersScrollView filtersScrollView: FiltersScrollView)
}

class FiltersScrollView: XibView {

    static let default_height: CGFloat = 56.0

    var presenter = FiltersScrollPresenter()

    weak var delegate: FiltersScrollViewDelegate?

    var filters: [FilterScrollDisplayData]?

    @IBOutlet weak var filtersScrollCollectionView: UICollectionView!

    override func configureView() {

        super.configureView()

        presenter.view = self

        configureFiltersScrollView()

    }

    private func configureFiltersScrollView() {

        backgroundColor = .NAVY

        filtersScrollCollectionView.backgroundColor = .clear
        filtersScrollCollectionView.register(UINib(nibName: FiltersScrollCollectionViewCell.identifier, bundle: nil), forCellWithReuseIdentifier: FiltersScrollCollectionViewCell.identifier)
    }

}

extension FiltersScrollView: FiltersScrollViewProtocol {

    func loadFilters(filters: [FilterScrollDisplayData]?) {

        presenter.receivedNewFilters(filters: filters)
    }

    func reloadFilters(filters: [FilterScrollDisplayData]?) {

        self.filters = filters
        filtersScrollCollectionView.reloadData()
    }

    func removeFilter(filter: FilterScrollDisplayData) {

        delegate?.removeFilterPushed(filter: filter, inFiltersScrollView: self)

    }

    func removeFilterLonglyPushed() {

        delegate?.removeFilterLonglyPushed(inFiltersScrollView: self)

    }

    func removeAllFilters() {

        delegate?.removeAllFiltersPushed(inFiltersScrollView: self)

    }

}

extension FiltersScrollView: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        if let filters = filters {
            return filters.count
        } else {
            return 0
        }
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FiltersScrollCollectionViewCell.identifier, for: indexPath) as? FiltersScrollCollectionViewCell {

            cell.configure(withFilter: filters![indexPath.row])
            cell.delegate = self

            cell.preservesSuperviewLayoutMargins = false
            cell.layoutMargins = UIEdgeInsets.zero
            cell.layoutMargins = UIEdgeInsets.zero

            return cell
        }

        return UICollectionViewCell()
    }

}

extension FiltersScrollView: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        if indexPath.row >= (filters?.count)! {
            return CGSize(width: 0, height: 0)
        }

        return FiltersScrollCollectionViewCell.sizeForCellFor(string: filters![indexPath.row].title)
    }
}

extension FiltersScrollView: FiltersScrollCollectionViewCellDelegate {

    func removeFilterButtonPushed(filter: FilterScrollDisplayData, filtersScrollCollectionViewCell: FiltersScrollCollectionViewCell) {
        presenter.removeFilterPushed(filter: filter)
    }

    func removeFilterButtonLonglyPushed(filtersScrollCollectionViewCell: FiltersScrollCollectionViewCell) {
        presenter.removeFilterLonglyPushed()
    }

    func removeAllFiltersButtonPushed(filtersScrollCollectionViewCell: FiltersScrollCollectionViewCell) {
        presenter.removeAllFiltersPushed()
    }
}
