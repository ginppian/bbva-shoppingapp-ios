//
//  FiltersScrollCollectionViewCell.swift
//  shoppingapp
//
//  Created by Arturo Marzo de la Fuente on 30/8/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit

protocol FiltersScrollCollectionViewCellDelegate: class {
    func removeFilterButtonPushed(filter: FilterScrollDisplayData, filtersScrollCollectionViewCell: FiltersScrollCollectionViewCell)
    func removeFilterButtonLonglyPushed(filtersScrollCollectionViewCell: FiltersScrollCollectionViewCell)
    func removeAllFiltersButtonPushed(filtersScrollCollectionViewCell: FiltersScrollCollectionViewCell)

}

class FiltersScrollCollectionViewCell: UICollectionViewCell {

    static let identifier = "FiltersScrollCollectionViewCell"
    static let height: CGFloat = 28
    static let closeButtonLeftMargin: CGFloat = 20
    static let closeButtonRightMargin: CGFloat = 10
    static let closeButtonIconSize: CGFloat = 16
    static let titleRightMargin: CGFloat = 20
    var isRemoveAllFiltersButton = false
    var filter: FilterScrollDisplayData?
    weak var delegate: FiltersScrollCollectionViewCellDelegate?

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var closeButton: UIButton!

    class func sizeForCellFor(string: String) -> CGSize {

        let width = closeButtonLeftMargin + closeButtonIconSize + closeButtonRightMargin + titleRightMargin + string.width(withConstraintedHeight: CGFloat(height), font: Tools.setFontMediumItalic(size: 14))
        return CGSize(width: width, height: CGFloat(height))
    }

    override func awakeFromNib() {

        super.awakeFromNib()

        closeButton.setImage(
            ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Common.close_icon, color: .BBVAWHITE),
                                 width: Float(FiltersScrollCollectionViewCell.closeButtonIconSize),
                                 height: Float(FiltersScrollCollectionViewCell.closeButtonIconSize)), for: .normal)

        closeButton.setImage(
            ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Common.close_icon, color: .BBVAWHITE30),
                                 width: Float(FiltersScrollCollectionViewCell.closeButtonIconSize),
                                 height: Float(FiltersScrollCollectionViewCell.closeButtonIconSize)), for: .highlighted)

        let buttonHeightInset = closeButton.bounds.height - FiltersScrollCollectionViewCell.closeButtonIconSize
        closeButton.imageEdgeInsets = UIEdgeInsets(top: buttonHeightInset / 2,
                                                   left: FiltersScrollCollectionViewCell.closeButtonLeftMargin,
                                                   bottom: buttonHeightInset / 2,
                                                   right: FiltersScrollCollectionViewCell.closeButtonRightMargin)

        let longPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: #selector(closeButtonLongPress(_:)))
        longPressGestureRecognizer.minimumPressDuration = 2.0
        longPressGestureRecognizer.delaysTouchesBegan = false
        addGestureRecognizer(longPressGestureRecognizer)

        normalStatus()

    }

    func configure(withFilter filter: FilterScrollDisplayData) {

        self.filter = filter
        title.text = filter.title
        isRemoveAllFiltersButton = filter.isRemoveAllFiltersButton

        setAccessibilities(withFilter: filter)
    }

    private func normalStatus() {

        title.font = Tools.setFontMediumItalic(size: 14)
        title.textColor = .BBVAWHITE

        backgroundColor = .COREBLUE

    }

    @objc private func closeButtonLongPress(_ gestureRecognizer: UILongPressGestureRecognizer) {

        if let delegate = delegate {
            delegate.removeFilterButtonLonglyPushed(filtersScrollCollectionViewCell: self)
        }
    }

    @IBAction func closeButtonPressed(_ sender: Any) {

        if isRemoveAllFiltersButton, let delegate = delegate {
            delegate.removeAllFiltersButtonPushed(filtersScrollCollectionViewCell: self)

        } else if let delegate = delegate {
            delegate.removeFilterButtonPushed(filter: filter!, filtersScrollCollectionViewCell: self)
        }

    }

}

// MARK: - APPIUM

private extension FiltersScrollCollectionViewCell {

    func setAccessibilities(withFilter filter: FilterScrollDisplayData) {

        #if APPIUM
            Tools.setAccessibility(view: title, identifier: filter.labelAccessibilityId)
            Tools.setAccessibility(view: closeButton, identifier: filter.buttonAccessibilityId)
        #endif

    }
}
