//
//  FilterScrollDisplayData.swift
//  shoppingapp
//
//  Created by Arturo Marzo de la Fuente on 6/9/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation

struct FilterScrollDisplayData {

    let title: String
    let filterType: String
    let isRemoveAllFiltersButton: Bool
    let labelAccessibilityId: String
    let buttonAccessibilityId: String

    init(title: String, filterType: String) {

        self.init(title: title, filterType: filterType, isRemoveAllFiltersButton: false, labelAccessibilityId: "", buttonAccessibilityId: "")

    }

    init(title: String, filterType: String, isRemoveAllFiltersButton: Bool) {

        self.init(title: title, filterType: filterType, isRemoveAllFiltersButton: isRemoveAllFiltersButton, labelAccessibilityId: "", buttonAccessibilityId: "")

    }

    init(title: String, filterType: String, isRemoveAllFiltersButton: Bool, labelAccessibilityId: String, buttonAccessibilityId: String) {

        self.title = title
        self.filterType = filterType
        self.isRemoveAllFiltersButton = isRemoveAllFiltersButton
        self.labelAccessibilityId = labelAccessibilityId
        self.buttonAccessibilityId = buttonAccessibilityId
    }
}

extension FilterScrollDisplayData: Equatable {

    public static func == (lhs: FilterScrollDisplayData, rhs: FilterScrollDisplayData) -> Bool {

        return lhs.title == rhs.title
            && lhs.filterType == rhs.filterType
            && lhs.isRemoveAllFiltersButton == rhs.isRemoveAllFiltersButton

    }

}
