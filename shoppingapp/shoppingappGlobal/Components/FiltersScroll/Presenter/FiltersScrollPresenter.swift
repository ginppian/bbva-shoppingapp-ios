//
//  FiltersScrollPresenter.swift
//  shoppingapp
//
//  Created by Arturo Marzo de la Fuente on 30/8/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

class FiltersScrollPresenter {

    static let type_filter_remove_all = "TYPE_FILTER_REMOVE_ALL"

    weak var view: FiltersScrollViewProtocol?
    var displayData: [FilterScrollDisplayData]?

    func generateFilterRemoveAll() -> [FilterScrollDisplayData] {

        return [FilterScrollDisplayData(title: Localizables.common.key_delete_all_filters_text,
                                   filterType: FiltersScrollPresenter.type_filter_remove_all,
                     isRemoveAllFiltersButton: true)]

    }

}

extension FiltersScrollPresenter: FiltersScrollPresenterProtocol {

    func receivedNewFilters(filters: [FilterScrollDisplayData]?) {

        displayData = filters

        view?.reloadFilters(filters: filters)
    }

    func removeFilterPushed(filter: FilterScrollDisplayData) {

        let indexToRemove = displayData?.index(of: filter)

        if let indexToRemove = indexToRemove {
            displayData?.remove(at: indexToRemove)
            view?.reloadFilters(filters: displayData)
        }

        view?.removeFilter(filter: filter)

    }

    func removeFilterLonglyPushed() {

        displayData = generateFilterRemoveAll()
        view?.reloadFilters(filters: displayData)

        view?.removeFilterLonglyPushed()

    }

    func removeAllFiltersPushed() {

        view?.removeAllFilters()
    }

}
