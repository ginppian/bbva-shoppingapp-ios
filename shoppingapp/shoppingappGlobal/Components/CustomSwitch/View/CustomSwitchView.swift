//
//  CustomSwitchView.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 19/10/2017.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit

protocol CustomSwitchDelegate: class {

    func customSwitchView(_ customSwitchView: CustomSwitchView, changedValue newValue: Bool)

}

class CustomSwitchView: UIView {

    weak var delegate: CustomSwitchDelegate?

    var switchButton: UIButton = UIButton()

    var rightConstraints: [NSLayoutConstraint]!
    var leftConstraints: [NSLayoutConstraint]!

    private var switchValue: Bool = false

    var switchBackgroundColor: UIColor = UIColor.clear {
        willSet {
            backgroundColor = newValue
        }
    }

    var switchToggleBackgroundImage: UIImage! {
        willSet {
            thumbButton.setImage(newValue, for: .normal)
        }
    }

    var switchEnabled: Bool = false {
        willSet {
            switchButton.isEnabled = newValue
            thumbButton.isEnabled = newValue

        }
    }

    var switchIsOn: Bool {
        get {
            return switchValue
        }
        set {
            switchValue = newValue
            setOn(newValue)
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        configureView()
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!

        configureView()
    }

    private var thumbButton: UIButton = {

        let thumbButton = UIButton()
        thumbButton.translatesAutoresizingMaskIntoConstraints = false
        thumbButton.layer.masksToBounds = false
        return thumbButton
    }()

    func configureView() {

        switchButton.translatesAutoresizingMaskIntoConstraints = false
        switchButton.layer.masksToBounds = false
        switchButton.addTarget(self, action: #selector(CustomSwitchView.cardOnOffTouchUpInside), for: .touchUpInside)

        layer.cornerRadius = frame.size.height * 0.5

        configureConstraints()
    }

    func configureConstraints() {

        addSubview(thumbButton)
        addSubview(switchButton)

        let thumbViews = ["thumbButton": thumbButton, "switchButton": switchButton]

        addConstraint(NSLayoutConstraint(item: thumbButton,
                                         attribute: .width,
                                         relatedBy: .equal,
                                         toItem: self,
                                         attribute: .width,
                                         multiplier: 0.6,
                                         constant: 0))

        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-1-[thumbButton]-1-|",
                                                      options: NSLayoutFormatOptions(rawValue: 0),
                                                      metrics: nil,
                                                      views: thumbViews))
        leftConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|-0-[thumbButton]-(>=0)-|",
                                                         options: NSLayoutFormatOptions(rawValue: 0),
                                                         metrics: nil,
                                                         views: thumbViews)
        rightConstraints = NSLayoutConstraint.constraints(withVisualFormat: "H:|-(>=0)-[thumbButton]-0-|",
                                                          options: NSLayoutFormatOptions(rawValue: 0),
                                                          metrics: nil,
                                                          views: thumbViews)
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|[switchButton]|",
                                                      options: NSLayoutFormatOptions(rawValue: 0),
                                                      metrics: nil,
                                                      views: thumbViews))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|[switchButton]|",
                                                      options: NSLayoutFormatOptions(rawValue: 0),
                                                      metrics: nil,
                                                      views: thumbViews))
    }

    @objc func cardOnOffTouchUpInside() {
        delegate?.customSwitchView(self, changedValue: switchValue)
    }

    func switchOff() {
        NSLayoutConstraint.deactivate(rightConstraints)
        NSLayoutConstraint.activate(leftConstraints)
    }

    func switchOn() {
        NSLayoutConstraint.deactivate(leftConstraints)
        NSLayoutConstraint.activate(rightConstraints)
    }

    func setOn(_ isOn: Bool) {

        switchValue = isOn

        if switchIsOn {
            switchOn()
            thumbButton.setImage(UIImage(named: ConstantsImages.Card.on_off_switch_on), for: .normal)

        } else {
            switchOff()
            thumbButton.setImage(UIImage(named: ConstantsImages.Card.on_off_switch_off), for: .normal)
        }
    }

}
