//
//  TransactionDetailComponentPresenter.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 20/11/2017.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation

class  TransactionDetailComponentPresenter {
    weak var view: TransactionDetailViewComponentProtocol?
}

extension TransactionDetailComponentPresenter: TransactionDetailComponentPresenterProtocol {

    func showTransactionDetail(withDisplayData displayTransactionDetail: TransactionDetailDisplayData, withExpandable expandable: Bool) {

        if displayTransactionDetail.pending == Localizables.transactions.key_transactions_pending_text {

            if expandable {
                view?.setTopTransactionDetailWithPendingExpanded(withExpandableData: createExpandableDisplayData(withExpandableOption: expandable, withButtonTitle: Localizables.transactions.key_transactions_pending_text))
            } else {
                view?.setTopTransactionDetailWithPendingCollapsed(withExpandableData: createExpandableDisplayData(withExpandableOption: expandable, withButtonTitle: Localizables.transactions.key_transactions_pending_text))
            }

        }

        view?.setTransactionDetailHeight()
    }
    func createExpandableDisplayData(withExpandableOption expandableOption: Bool, withButtonTitle buttonTitle: String) -> ExpandableViewDisplayData {

        return ExpandableViewDisplayData.generateExpandableDisplayData(withExpandableOption: expandableOption, withButtonTitle: buttonTitle)
    }

}
