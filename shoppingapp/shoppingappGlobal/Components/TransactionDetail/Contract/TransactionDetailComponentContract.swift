//
//  TransactionDetailComponentContract.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 20/11/2017.
//  Copyright © 2017 BBVA. All rights reserved.
//

protocol TransactionDetailViewComponentProtocol: ComponentViewProtocol {
    func setTransactionDetailHeight()
    func setTransactionDetailData(withDisplayData displayTransactionDetail: TransactionDetailDisplayData)
    func setTopTransactionDetailWithPendingExpanded(withExpandableData displayData: ExpandableViewDisplayData)
    func setTopTransactionDetailWithPendingCollapsed(withExpandableData displayData: ExpandableViewDisplayData)

}

protocol TransactionDetailComponentPresenterProtocol: ComponentPresenterProtocol {
    func showTransactionDetail(withDisplayData displayTransactionDetail: TransactionDetailDisplayData, withExpandable expandable: Bool)
}
