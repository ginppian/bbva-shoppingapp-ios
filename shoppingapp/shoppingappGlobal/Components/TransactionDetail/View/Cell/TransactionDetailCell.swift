//
//  TransactionDetailCell.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 21/11/2017.
//  Copyright © 2017 BBVA. All rights reserved.
//

class TransactionDetailCell: UITableViewCell {

    static let identifier = "TransactionDetailCell"
    static let height: CGFloat = 60.0

    // MARK: @IBOutlet's

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!

    override func awakeFromNib() {

        super.awakeFromNib()

        selectionStyle = .none

        valueLabel.textColor = .BBVA600
        valueLabel?.textAlignment = .left

        titleLabel?.font = Tools.setFontBook(size: 14)
        titleLabel.textColor = .BBVA500
        titleLabel?.textAlignment = .left

    }

    func configure(withDisplayItem displayItem: TransactionDetailDisplayBottomSection) {

        if displayItem.title == Localizables.transactions.key_card_movement_details_card_text {
            valueLabel.font = Tools.setFontBookItalic(size: 16)
        } else {
            valueLabel.font = Tools.setFontBook(size: 16)
        }

        titleLabel.text = displayItem.title
        valueLabel.text = displayItem.value

        setAccessibilities(WithAccessibilityTitle: displayItem.accessibiltyTitle, andaccessibilityValue: displayItem.accessibiltyValue)
    }

}

// MARK: - APPIUM

private extension TransactionDetailCell {

    func setAccessibilities(WithAccessibilityTitle accessibilityTitle: String, andaccessibilityValue accessibilityValue: String) {
        #if APPIUM
            Tools.setAccessibility(view: titleLabel, identifier: accessibilityTitle)
            Tools.setAccessibility(view: valueLabel, identifier: accessibilityValue)
        #endif
    }

}
