//
//  TransactionDetailView.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 20/11/2017.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation

protocol TransactionDetailViewDelegate: class {

    func setTransactionDetailHeight(withHeight height: CGFloat)

}
class TransactionDetailView: XibView {

    @IBOutlet weak var conceptLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var expandableContainerView: ExpandableView!
    @IBOutlet weak var tableView: UITableView!

    @IBOutlet weak var bottomTransactionDetailHeight: NSLayoutConstraint!
    @IBOutlet weak var pendingAreaHeight: NSLayoutConstraint!
    @IBOutlet weak var lineView: UIView!

    static let pendingExpandedHeight: CGFloat = 100.0
    static let pendingCollapsedHeight: CGFloat = 30.0
    static let tableCellHeight: Int = 70
    static let topSectionHeight: CGFloat = 194.0

    weak var delegate: TransactionDetailViewDelegate?

    var presenter = TransactionDetailComponentPresenter()
    var transactionDetailDisplayData: TransactionDetailDisplayData?

    override func configureView() {

        super.configureView()

        configureViewItems()

        presenter.view = self

        expandableContainerView.delegate = self

        setAccessibilities()
    }

    func configureViewItems() {

        conceptLabel.textColor = UIColor.BBVA600
        conceptLabel.font = Tools.setFontBook(size: 16)
        conceptLabel.textAlignment = .center

        amountLabel.textColor = UIColor.BBVA600
        amountLabel.textAlignment = .center

        dateLabel.textColor = UIColor.BBVA500
        dateLabel.font = Tools.setFontBook(size: 14)
        dateLabel.textAlignment = .center

        lineView.backgroundColor = .BBVA200

        tableView.separatorStyle = UITableViewCellSeparatorStyle.none
        tableView.register(UINib(nibName: TransactionDetailCell.identifier, bundle: nil), forCellReuseIdentifier: TransactionDetailCell.identifier)

    }

    func configureTopView(withTopDisplayData displayData: TransactionDetailDisplayData) {
        transactionDetailDisplayData = displayData
        conceptLabel.text = displayData.concept
        amountLabel.attributedText = displayData.amount
        dateLabel.text = displayData.operationDate

        setAccessibilities(forPendingStatus: !displayData.pending.isEmpty)
    }

}

extension TransactionDetailView: TransactionDetailViewComponentProtocol {

     func setTransactionDetailData(withDisplayData displayTransactionDetail: TransactionDetailDisplayData) {

        configureTopView(withTopDisplayData: displayTransactionDetail)
        bottomTransactionDetailHeight.constant = CGFloat(displayTransactionDetail.bottomItems.count * TransactionDetailView.tableCellHeight)
        tableView.reloadData()
        presenter.showTransactionDetail(withDisplayData: displayTransactionDetail, withExpandable: false)

    }

    func setTransactionDetailHeight() {
        delegate?.setTransactionDetailHeight(withHeight: bottomTransactionDetailHeight.constant + pendingAreaHeight.constant + TransactionDetailView.topSectionHeight)
    }

    func setTopTransactionDetailWithPendingCollapsed(withExpandableData displayData: ExpandableViewDisplayData) {

        pendingAreaHeight.constant = TransactionDetailView.pendingCollapsedHeight
        expandableContainerView.setPendingInformation(withPendingData: displayData)

    }

    func setTopTransactionDetailWithPendingExpanded(withExpandableData displayData: ExpandableViewDisplayData) {

        pendingAreaHeight.constant = TransactionDetailView.pendingExpandedHeight
        expandableContainerView.setPendingInformation(withPendingData: displayData)

    }

}

extension TransactionDetailView: ExpandableViewDelegate {
    func expandableView(_ expandableView: ExpandableView, withExpandable expandable: Bool) {
        presenter.showTransactionDetail(withDisplayData: transactionDetailDisplayData!, withExpandable: expandable)

    }
}

extension TransactionDetailView: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return transactionDetailDisplayData?.bottomItems.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if let displayItem = transactionDetailDisplayData?.bottomItems[indexPath.row] {

            if let cell = tableView.dequeueReusableCell(withIdentifier: TransactionDetailCell.identifier, for: indexPath) as? TransactionDetailCell {

                cell.configure(withDisplayItem: displayItem)
                return cell
            }
        }
        return UITableViewCell()
    }
}
extension TransactionDetailView: UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return TransactionDetailCell.height
    }
}

extension TransactionDetailView: ButtonsBarViewDelegate {

    func buttonBarPressed(_ buttonsBarView: ButtonsBarView, withDisplayItem displayItem: ButtonBarDisplayData) {

    }
}

// MARK: - APPIUM

private extension TransactionDetailView {

    func setAccessibilities() {
        #if APPIUM
            Tools.setAccessibility(view: conceptLabel, identifier: ConstantsAccessibilities.TRANSACTION_DETAIL_TRANSACTION_NAME)
            Tools.setAccessibility(view: amountLabel, identifier: ConstantsAccessibilities.TRANSACTION_DETAIL_TRANSACTION_AMOUNT)
            Tools.setAccessibility(view: dateLabel, identifier: ConstantsAccessibilities.TRANSACTION_DETAIL_OPERATION_DATE)

        #endif
    }

    func setAccessibilities(forPendingStatus pendingStatus: Bool) {

        #if APPIUM

            if pendingStatus {

                Tools.setAccessibility(view: expandableContainerView.pendingButton, identifier: ConstantsAccessibilities.TRANSACTION_DETAIL_PENDING_TITLE)
                Tools.setAccessibility(view: expandableContainerView.informationLabel, identifier: ConstantsAccessibilities.TRANSACTION_DETAIL_PENDING_DESCRIPTION)

            }

        #endif

    }

}
