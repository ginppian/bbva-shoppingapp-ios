//
//  TransactionDetailDisplayData.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 20/11/2017.
//  Copyright © 2017 BBVA. All rights reserved.
//

struct TransactionDetailDisplayData {

    let concept: String
    let amount: NSAttributedString
    let pending: String
    let operationDate: String
    var bottomItems = [TransactionDetailDisplayBottomSection]()

    init(withTransactionDetailDisplayData displayData: TransactionDisplayItem) {

        if let contractNumber = displayData.contract.number {

            bottomItems.append(TransactionDetailDisplayBottomSection(title: Localizables.transactions.key_card_movement_details_card_text, value: contractNumber, accessibiltyTitle: ConstantsAccessibilities.TRANSACTION_DETAIL_CARD_TITLE, accessibiltyValue: ConstantsAccessibilities.TRANSACTION_DETAIL_CONTRACT_NUMBER))

        }

        if let operationDate = displayData.operationDate {
            bottomItems.append(TransactionDetailDisplayBottomSection(title: Localizables.transactions.key_card_movement_details_date_operation_text, value: String(format: "%@h", arguments: [operationDate]), accessibiltyTitle: ConstantsAccessibilities.TRANSACTION_DETAIL_OPERATION_DATE_TITLE, accessibiltyValue: ConstantsAccessibilities.TRANSACTION_DETAIL_OPERATION_DATE_VALUE))

        }

        if let accountedDate = displayData.accountedDate {
            bottomItems.append(TransactionDetailDisplayBottomSection(title: Localizables.transactions.key_card_movement_details_date_application_text, value: String(format: "%@h", arguments: [accountedDate]), accessibiltyTitle: ConstantsAccessibilities.TRANSACTION_DETAIL_VALUE_DATE_TITLE, accessibiltyValue: ConstantsAccessibilities.TRANSACTION_DETAIL_VALUE_DATE_VALUE))
        }

        if let referenceString = displayData.additionalInformation?.reference {
            bottomItems.append(TransactionDetailDisplayBottomSection(title: Localizables.transactions.key_card_movement_details_folio_text, value: referenceString, accessibiltyTitle: ConstantsAccessibilities.TRANSACTION_DETAIL_FOLIO_TITLE, accessibiltyValue: ConstantsAccessibilities.TRANSACTION_DETAIL_FOLIO_VALUE))
        }

        concept = displayData.concept
        amount = displayData.amountTransactionDetail
        pending = displayData.pending
        operationDate = String(format: "%@h", arguments: [displayData.operationDate!])
    }
}
