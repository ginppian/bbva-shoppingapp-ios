//
//  GenerateCardUseCase.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 6/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import CellsNativeComponents

class GenerateCardUseCase: UseCaseAsync<GeneratedCardDTO, GenerateCardDTO> {

    private let generateCardRepository: GenerateCardRepository

    init(generateCardRepository: GenerateCardRepository) {

        self.generateCardRepository = generateCardRepository
        super.init()

    }

    override func call(params: GenerateCardDTO?, success: @escaping (GeneratedCardDTO) -> Void, error: @escaping (_ error: Error) -> Void) {

        if let params = params {
            self.generateCardRepository.generateCard(params: params, success: success, error: error)
        } else {
            DLog(message: "Error no params")
            error(ServiceError.GenericErrorEntity(error: ErrorEntity(message: "Error no params")))
        }
    }

}
