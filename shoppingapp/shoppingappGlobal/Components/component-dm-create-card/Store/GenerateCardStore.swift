//
//  GenerateCardStore.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 6/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import CellsNativeComponents
import BBVA_Network

open class GenerateCardStore: NetworkStore {

    public static let identifier: String = "component-data-manager-generate-card-store"

    public static let onGenerateSuccess = ActionSpec<GeneratedCardDTO>(id: "on-generate-success")
    static let onGenerateError = ActionSpec<ErrorBO>(id: "on-generate-error")

    var host: String?
    var countryCode: String?

    fileprivate var generateCardUseCase: GenerateCardUseCase?
    fileprivate var worker: NetworkWorker

    public init(worker: NetworkWorker, host: String? = nil) {

        self.worker = worker
        self.host = host

        super.init(GenerateCardStore.identifier)

        guard host == nil || (host?.isEmpty)! else {
            return
        }

        configureStoreWithProperties()
    }

    public required init() {
        fatalError("init() has not been implemented")
    }

    fileprivate func configureStoreWithProperties() {

        newProperty(key: "host") { [weak self] (value: Any?) in

            if let value = value {
                self?.host = String(describing: value)
            }
        }
        newProperty(key: "country") { [weak self] (value: Any?) in

            if let value = value {
                self?.countryCode = String(describing: value)
            }
        }
    }

    public func generateCardRequest(generateCardDTO: GenerateCardDTO) {

        let useCaseCallback = UseCase<GeneratedCardDTO, GenerateCardDTO>.UseCaseCallback<GeneratedCardDTO>(onUseCaseComplete: { result in
            self.fire(actionSpec: GenerateCardStore.onGenerateSuccess, value: result)
        }, onUseCaseError: self.onUseCaseError)

        self.generateCardUseCase?.execute(params: generateCardDTO, useCaseCallback: useCaseCallback)
    }

    override open func onPropertiesBounds() {

        guard let host = host else {
            return
        }

        let generateCardRepository: GenerateCardRepository = GenerateCardRepositoryImpl(baseURL: host, networkWorker: worker)
        self.generateCardUseCase = GenerateCardUseCase(generateCardRepository: generateCardRepository)
    }

    override open func dispose() {
        generateCardUseCase?.dispose()
        super.dispose()
    }

    override public func onUseCaseError(error: Error) {
        print("onUseCaseError: \(error)")

        guard let evaluate = error as? ServiceError else {
            return
        }

        switch evaluate {
        case .GenericErrorEntity(let errorEntity):
            let errorBVABO = ErrorBO(error: errorEntity)
            self.fire(actionSpec: GenerateCardStore.onGenerateError, value: errorBVABO)
        default:
            break
        }
    }

}
