//
//  GenerateCardRepository.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 6/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

public protocol GenerateCardRepository {

    func generateCard (params: GenerateCardDTO, success: @escaping (_ result: GeneratedCardDTO) -> Void, error: @escaping (_ error: Error) -> Void)

}
