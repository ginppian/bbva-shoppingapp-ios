//
//  GenerateCardRepositoryImpl.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 6/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import ObjectMapper
import BBVA_Network
import HandyJSON

class GenerateCardRepositoryImpl: BaseRepositoryImpl, GenerateCardRepository {

    fileprivate static let operativeId = "OPERATIVE"

    fileprivate static let generateCardUrl = "/cards/" //NON-NLS

    func generateCard (params: GenerateCardDTO, success: @escaping (GeneratedCardDTO) -> Void, error: @escaping (_ error: Error) -> Void) {

        let url = baseURL + GenerateCardRepositoryImpl.generateCardUrl

        let body = GenerateCardBody(params: params)

        guard let bodyParser = body.toJSON() else {
            return
        }

        let request: NetworkRequest = createRequest(url, Headers.BASIC_HEADER, bodyParser, .useProtocolCachePolicy, 30, NetworkMethod.POST)

        _ = networkWorker.execute(request: request) { (response: Response) in

            if response.error == nil {

                if  let dataJson = response.body {

                    if let generatedCardDTO = GeneratedCardDTO.deserialize(from: dataJson) {
                        success(generatedCardDTO)
                    } else {
                        error(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
                    }

                } else {
                    error(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
                }

            } else {

                if let serviceError = self.checkShowError(response: response, security: false) {
                    error(serviceError)
                }
            }

        }
    }

}

/*
{
   "cardType":{
      "id":"DEBIT_CARD"
   },
   "physicalSupport":{
      "id":"VIRTUAL"
   },
   "title":{
      "id":"WALLET_VIRTUAL"
   },
   "expirationDate":"2021-11-01",
   "relatedContracts":[
      {
         "contractId":"1234",
         "number":"4555103000605264",
         "product":{
            "id":"CARDS"
         },
         "relationType":{
            "id":"LINKED_WITH"
         }
      }
   ]
}
*/

class GenerateCardBody: HandyJSON {

    var cardType: GenerateCardBodyCardType?
    var physicalSupport: GenerateCardBodyPhysicalSupport?
    var title: GenerateCardBodyTitle?
    var relatedContracts: [GenerateCardBodyRelatedContract]?

    required init() {

    }

    init(params: GenerateCardDTO) {
        var cardType = GenerateCardBodyCardType()
        cardType.id = params.cardType

        var physicalSupport = GenerateCardBodyPhysicalSupport()
        physicalSupport.id = "VIRTUAL"

        var title = GenerateCardBodyTitle()
        title.id = "WALLET_VIRTUAL"

        var product = GenerateCardBodyProduct()
        product.id = "CARDS"

        var relationType = GenerateCardBodyRelationType()
        relationType.id = "LINKED_WITH"

        var relatedContract = GenerateCardBodyRelatedContract()
        relatedContract.product = product
        relatedContract.relationType = relationType
        relatedContract.contractId = params.cardId
        relatedContract.number = params.cardNumber

        var relatedContracts = [GenerateCardBodyRelatedContract]()
        relatedContracts.append(relatedContract)

        self.cardType = cardType
        self.physicalSupport = physicalSupport
        self.title = title
        self.relatedContracts = relatedContracts
    }

}

struct GenerateCardBodyCardType: HandyJSON {
    var id: String?
}

struct GenerateCardBodyPhysicalSupport: HandyJSON {
    var id: String?
}

struct GenerateCardBodyTitle: HandyJSON {
    var id: String?
}

struct GenerateCardBodyRelatedContract: HandyJSON {
    var contractId: String?
    var number: String?
    var product: GenerateCardBodyProduct?
    var relationType: GenerateCardBodyRelationType?
}

struct GenerateCardBodyProduct: HandyJSON {
    var id: String?
}

struct GenerateCardBodyRelationType: HandyJSON {
    var id: String?
}
