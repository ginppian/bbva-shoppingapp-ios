//
//  BlockCardDTO.swift
//  shoppingapp
//
//  Created by Marcos on 30/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import HandyJSON

public class GeneratedCardDTO: CellsDTO, HandyJSON {

    var data: GeneratedCardDataDTO?
    var apiInfo: GeneratedCardApiInfoDTO?

    public required init() {}
}

struct GeneratedCardDataDTO: HandyJSON {

    var cardId: String?
    var number: String?
    var numberType: GeneratedCardDataNumberTypeDTO?
    var cardType: GeneratedCardDataCardTypeDTO?
    var title: GeneratedCardDataTitleDTO?
    var physicalSupport: GeneratedCardDataPhysicalSupportDTO?
    var expirationDate: String?
    var currencies: [GeneratedCardDataCurrencyDTO]?
    var availableBalance: GeneratedCardDataAvailableBalanceDTO?
    var status: GeneratedCardDataStatusDTO?
    var issueDate: String?
    var operationNumber: String?
    var relatedContracts: [GeneratedCardDataRelatedContractDTO]?
}

struct GeneratedCardDataNumberTypeDTO: HandyJSON {

    var id: String?
    var name: String?
}

struct GeneratedCardDataCardTypeDTO: HandyJSON {

    var id: String?
    var name: String?
}

struct GeneratedCardDataTitleDTO: HandyJSON {

    var id: String?
    var name: String?
}

struct GeneratedCardDataPhysicalSupportDTO: HandyJSON {

    var id: String?
    var name: String?
}

struct GeneratedCardDataStatusDTO: HandyJSON {

    var id: String?
    var name: String?
}

struct GeneratedCardDataCurrencyDTO: HandyJSON {

    var currency: String?
    var isMajor: Bool?
}

struct GeneratedCardDataBalanceDTO: HandyJSON {

    var amount: String?
    var currency: String?
}

struct GeneratedCardDataAvailableBalanceDTO: HandyJSON {

    var currentBalances: [GeneratedCardDataBalanceDTO]?
    var postedBalances: [GeneratedCardDataBalanceDTO]?
    var pendingBalances: [GeneratedCardDataBalanceDTO]?
}

struct GeneratedCardDataRelatedContractDTO: HandyJSON {

    var relatedContractId: String?
    var contractId: String?
    var product: GeneratedCardDataProductDTO?
    var relationType: GeneratedCardDataRelationTypeDTO?
}

struct GeneratedCardDataProductDTO: HandyJSON {

    var id: String?
    var name: String?
}

struct GeneratedCardDataRelationTypeDTO: HandyJSON {

    var id: String?
    var name: String?
}

struct GeneratedCardApiInfoDTO: HandyJSON {

    var category: String?
    var name: String?
    var version: String?
}

/*
{
  "data": {
    "cardId": "1528278731464",
    "number": " 3537 1668 8126 2471",
    "numberType": {
      "id": "PAN",
      "name": "Primary Account Number"
    },
    "cardType": {
      "id": "DEBIT_CARD",
      "name": "DEBIT_CARD"
    },
    "title": {
      "id": "WALLET_VIRTUAL",
      "name": "WALLET_VIRTUAL"
    },
    "physicalSupport": {
      "id": "VIRTUAL",
      "name": "VIRTUAL"
    },
    "expirationDate": "2017-10-09",
    "currencies": [
      {
        "currency": "EUR",
        "isMajor": true
      }
    ],
    "availableBalance": {
      "currentBalances": [
        {
          "amount": 0,
          "currency": "EUR"
        }
      ],
      "postedBalances": [
        {
          "amount": 0,
          "currency": "EUR"
        }
      ],
      "pendingBalances": [
        {
          "amount": 0,
          "currency": "EUR"
        }
      ]
    },
    "status": {
      "id": "OPERATIVE",
      "name": "Operative"
    },
    "issueDate": "2018-06-06T09:52:11.464Z",
    "operationNumber": "12345678",
    "relatedContracts": [
      {
        "relatedContractId": "1528278731464",
        "contractId": "0005",
        "product": {
          "id": "CARDS",
          "name": "CARDS"
        },
        "relationType": {
          "id": "LINKED_WITH",
          "name": "LINKED_WITH"
        }
      }
    ]
  },
  "apiInfo": {
    "category": "products",
    "name": "cards",
    "version": "0.19.0"
  }
}
*/
