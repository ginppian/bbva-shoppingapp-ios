//
//  GenerateCardDTO.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 6/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

open class GenerateCardDTO: CellsDTO {

    var cardId: String
    var cardNumber: String
    var cardType: String

    init(cardId: String, cardNumber: String, cardType: String) {

        self.cardId = cardId
        self.cardNumber = cardNumber
        self.cardType = cardType
    }

}
