//
//  TransactionTypeFilterContract.swift
//  shoppingapp
//
//  Created by jesus.martinez on 3/8/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import RxSwift

protocol TransactionTypeFilterViewProtocol: ComponentViewProtocol {

    func showMoreOptionsView()
    func showTransaction(filterTypes: [String])
    func informMoreFilters()
    func informSelectedType(_ type: TransactionFilterType)
    func setSelectedOption(atIndex index: Int)

}

protocol TransactionTypeFilterPresenterProtocol: ComponentPresenterProtocol {

    func onConfigureView()
    func showMoreOptionsAction()
    func setToDefaultValue()
    func didSelectedOption(atIndex index: Int)

}

protocol TransactionTypeFilterInteractorProtocol {

    func provideTransactionFilterTypes() -> Observable<[ModelBO]>
}
