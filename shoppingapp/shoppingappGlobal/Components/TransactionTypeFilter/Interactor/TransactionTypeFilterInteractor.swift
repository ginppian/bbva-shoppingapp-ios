//
//  TransactionTypeFilterInteractor.swift
//  shoppingapp
//
//  Created by jesus.martinez on 7/8/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation
import RxSwift

class TransactionTypeFilterInteractor {

    let disposeBag = DisposeBag()
}

extension TransactionTypeFilterInteractor: TransactionTypeFilterInteractorProtocol {

    func provideTransactionFilterTypes() -> Observable<[ModelBO]> {

        return Observable.create { observer in

            ConfigurationDataManager.sharedInstance.provideTransactionFilterTypes().subscribe(

                onNext: { result in

                    if let filterEntities = result as? [TransactionsTypeFilterEntity] {

                        var filtersBOs: [TransactionsTypeFilterBO] = []

                        for filterEntity in filterEntities {
                            filtersBOs.append(TransactionsTypeFilterBO(type: filterEntity.type, titleKey: filterEntity.titleKey))
                        }

                        observer.onNext(filtersBOs)
                        observer.onCompleted()
                    } else {
                        DLog(message: "Incorrect IdentificationDocumentEntity")

                    }

                }).addDisposableTo(self.disposeBag)

            return Disposables.create()
        }

    }
}
