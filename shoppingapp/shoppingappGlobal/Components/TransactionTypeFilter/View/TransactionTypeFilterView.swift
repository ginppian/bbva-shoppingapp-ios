//
//  TransactionTypeFilterView.swift
//  shoppingapp
//
//  Created by jesus.martinez on 3/8/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit

protocol TransactionTypeFilterViewDelegate: class {

    func showMoreFilters(transactionTypeFilterView: TransactionTypeFilterView, withHeight height: CGFloat)

    func modifiedType(_ transactionTypeFilterView: TransactionTypeFilterView, typeSelected type: TransactionFilterType)

}

class TransactionTypeFilterView: XibView {

    var presenter = TransactionTypeFilterPresenter()

    weak var delegate: TransactionTypeFilterViewDelegate?

    var filterTypes: [String] = []

    // MARK: @IBOutlet's

    @IBOutlet weak var moreFiltersView: UIView!
    @IBOutlet weak var moreFiltersButton: UIButton!

    @IBOutlet weak var transactionOptionsView: UIView!
    @IBOutlet weak var transactionFiltersTableView: UITableView!

    // MARK: - Private Helper Methods

    override func configureView() {
        super.configureView()

        moreFiltersButton.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Common.add_icon, color: .MEDIUMBLUE), width: 16, height: 16), for: .normal)

        moreFiltersButton.setTitle(Localizables.transactions.key_transactions_filters_text, for: .normal)
        moreFiltersButton.titleLabel?.font = Tools.setFontBold(size: 14)
        moreFiltersButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 6, bottom: 0, right: 0)

        moreFiltersButton.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Common.add_icon, color: .COREBLUE), width: 16, height: 16), for: .highlighted)
        moreFiltersButton.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Common.add_icon, color: .COREBLUE), width: 16, height: 16), for: .selected)

        moreFiltersButton.setTitleColor(.MEDIUMBLUE, for: .normal)
        moreFiltersButton.setTitleColor(.COREBLUE, for: .highlighted)
        moreFiltersButton.setTitleColor(.COREBLUE, for: .selected)

        presenter.view = self
        presenter.onConfigureView()

        configureTransactionFiltersView()

    }

    private func configureTransactionFiltersView() {

        transactionFiltersTableView.backgroundColor = .clear

        transactionFiltersTableView.register(UINib(nibName: TransactionTypeFilterTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: TransactionTypeFilterTableViewCell.identifier)

        transactionFiltersTableView.tableHeaderView = TransactionTypeFilterHeaderView(frame: CGRect(x: 0, y: 0, width: frame.size.width, height: TransactionTypeFilterHeaderView.defaultHeight))

        transactionFiltersTableView.rowHeight = UITableViewAutomaticDimension
        transactionFiltersTableView.estimatedRowHeight = TransactionTypeFilterTableViewCell.height

        transactionFiltersTableView.isScrollEnabled = false
        transactionFiltersTableView.separatorInset = .zero
        transactionFiltersTableView.separatorColor = .BBVA100

    }

    @IBAction func moreFiltersAction(_ sender: Any) {

        presenter.showMoreOptionsAction()

    }

}

extension TransactionTypeFilterView: TransactionTypeFilterViewProtocol {

    func showMoreOptionsView() {

        moreFiltersView.isHidden = false
        transactionOptionsView.isHidden = true

    }

    func showTransaction(filterTypes: [String]) {

        self.filterTypes = filterTypes

        transactionFiltersTableView.reloadData()

        let indexPath = IndexPath(row: 0, section: 0)
        transactionFiltersTableView.selectRow(at: indexPath, animated: true, scrollPosition: .top)

        transactionOptionsView.isHidden = false
        moreFiltersView.isHidden = true

    }

    func informMoreFilters() {

        let height = CGFloat(filterTypes.count) * CGFloat(TransactionTypeFilterTableViewCell.height) + TransactionTypeFilterHeaderView.defaultHeight

        delegate?.showMoreFilters(transactionTypeFilterView: self, withHeight: height)

    }

    func informSelectedType(_ type: TransactionFilterType) {

        delegate?.modifiedType(self, typeSelected: type)
    }

    func setSelectedOption(atIndex index: Int) {

        if self.filterTypes.count <= index {

            return
        }

        let indexPath = IndexPath(row: 0, section: 0)
        transactionFiltersTableView.selectRow(at: indexPath, animated: true, scrollPosition: .top)
    }

}

extension TransactionTypeFilterView: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return filterTypes.count

    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if let cell = tableView.dequeueReusableCell(withIdentifier: TransactionTypeFilterTableViewCell.identifier, for: indexPath) as? TransactionTypeFilterTableViewCell {
            cell.configure(withTitle: filterTypes[indexPath.row])

            cell.preservesSuperviewLayoutMargins = false
            cell.separatorInset = UIEdgeInsets.zero
            cell.layoutMargins = UIEdgeInsets.zero

            return cell
        }

        return UITableViewCell()

    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.didSelectedOption(atIndex: indexPath.row)
    }

}
