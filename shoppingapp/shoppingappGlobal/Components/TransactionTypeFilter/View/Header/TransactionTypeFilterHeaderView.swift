//
//  TransactionTypeFilterHeaderView.swift
//  shoppingapp
//
//  Created by jesus.martinez on 8/8/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit

class TransactionTypeFilterHeaderView: XibView {

    static let defaultHeight: CGFloat = 76.0

    @IBOutlet weak var titleLabel: UILabel!

    override func configureView() {
        super.configureView()

        titleLabel.font = Tools.setFontBold(size: 16)
        titleLabel.textColor = .BBVA500

        titleLabel.text = Localizables.transactions.key_transactions_type_text

    }

}
