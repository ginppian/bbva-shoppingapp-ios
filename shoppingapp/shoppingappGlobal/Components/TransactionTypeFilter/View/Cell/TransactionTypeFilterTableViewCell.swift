//
//  TransactionTypeFilterTableViewCell.swift
//  shoppingapp
//
//  Created by jesus.martinez on 7/8/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit

class TransactionTypeFilterTableViewCell: UITableViewCell {

    static let identifier = "TransactionTypeFilterTableViewCell"
    static let height: CGFloat = 76

    @IBOutlet weak var title: UILabel!

    override func awakeFromNib() {

        super.awakeFromNib()

        selectionStyle = .none

        normalStatus()

        setAccessibilities()

    }

    override func setSelected(_ selected: Bool, animated: Bool) {

        super.setSelected(selected, animated: animated)

        selected ? selectedStatus() : normalStatus()

    }

    func configure(withTitle title: String) {

        self.title.text = title

    }

    private func normalStatus() {

        title.font = Tools.setFontBook(size: 16)
        title.textColor = .BBVA600

        backgroundColor = .BBVAWHITE

    }

    private func selectedStatus() {

        title.font = Tools.setFontMedium(size: 16)
        title.textColor = .COREBLUE

        backgroundColor = .BBVAWHITE

    }

}

// MARK: - APPIUM

private extension TransactionTypeFilterTableViewCell {

    func setAccessibilities() {

        #if APPIUM

            accessibilityIdentifier = ConstantsAccessibilities.TRANSACTIONS_FILTER_CELL_ID

            Tools.setAccessibility(view: self.title, identifier: ConstantsAccessibilities.TRANSACTIONS_FILTER_TITLE_CELL_TYPE_TEXT_ID)

        #endif

    }
}
