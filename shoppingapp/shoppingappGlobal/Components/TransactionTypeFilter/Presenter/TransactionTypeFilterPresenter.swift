//
//  TransactionTypeFilterPresenter.swift
//  shoppingapp
//
//  Created by jesus.martinez on 3/8/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import RxSwift

class TransactionTypeFilterPresenter {

    var interactor: TransactionTypeFilterInteractorProtocol = TransactionTypeFilterInteractor()
    weak var view: TransactionTypeFilterViewProtocol?
    var transactionTypes: [TransactionsTypeFilterBO]?
    let disposeBag = DisposeBag()

}

extension TransactionTypeFilterPresenter: TransactionTypeFilterPresenterProtocol {

    func onConfigureView() {

        view?.showMoreOptionsView()

        interactor.provideTransactionFilterTypes().subscribe(
            onNext: { [weak self] result in

                if let result = result as? [TransactionsTypeFilterBO] {

                    self?.transactionTypes = result

                }

            }
        ).addDisposableTo(disposeBag)

    }

    func showMoreOptionsAction() {

        if let types = transactionTypes {

            var filterTitles = [String]()

            for filterType in types {
                filterTitles.append(filterType.titleKey)
            }

            view?.showTransaction(filterTypes: filterTitles)

            view?.informMoreFilters()
        }

    }

    func setToDefaultValue() {

        view?.setSelectedOption(atIndex: 0)
        self.didSelectedOption(atIndex: 0)
    }

    func didSelectedOption(atIndex index: Int) {

        if let types = transactionTypes {
            let selectedType = types[index]
            view?.informSelectedType(selectedType.type)
        }
    }

}
