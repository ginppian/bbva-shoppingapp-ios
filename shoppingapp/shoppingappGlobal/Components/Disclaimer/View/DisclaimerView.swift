//
//  DisclaimerView.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 17/11/2017.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit

protocol DisclaimerViewDelegate: class {

    func disclaimerViewClickTextDisclaimer(_ disclaimerView: DisclaimerView)

}

class DisclaimerView: XibView {

    @IBOutlet weak var informationButton: UIButton!

    @IBAction func informationButtonPressed(_ sender: Any) {
        delegate?.disclaimerViewClickTextDisclaimer(self)
    }

    weak var delegate: DisclaimerViewDelegate?

    override func configureView() {
        super.configureView()
        backgroundColor = .BBVAWHITELIGHTBLUE
        configureViewItems()
    }

    func configureViewItems() {
        informationButton.titleEdgeInsets = UIEdgeInsets(top: 2, left: 6, bottom: 0, right: 0)
        informationButton.setTitleColor(.MEDIUMBLUE, for: .normal)
        informationButton.setTitleColor(.COREBLUE, for: .highlighted)
        informationButton.setTitleColor(.COREBLUE, for: .selected)
        informationButton.titleLabel?.font = Tools.setFontMedium(size: 14)
    }

    func setDisclaimerInformation(withDisclaimerData  displayData: DisclaimerDisplayData) {

        informationButton.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: displayData.disclaimerIcon, color: .MEDIUMBLUE), width: 16, height: 16), for: .normal)
        informationButton.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: displayData.disclaimerIcon, color: .COREBLUE), width: 16, height: 16), for: .highlighted)
        informationButton.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: displayData.disclaimerIcon, color: .COREBLUE), width: 16, height: 16), for: .selected)

        informationButton.setTitle(displayData.disclaimerText, for: .normal)
    }

}
