//
//  DisclaimerDisplayData.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 17/11/2017.
//  Copyright © 2017 BBVA. All rights reserved.
//

struct  DisclaimerDisplayData {

    var disclaimerText: String
    var disclaimerIcon: String

    init() {
        disclaimerIcon = ConstantsImages.Common.info_icon
        disclaimerText = Localizables.common.key_not_you_done_operation_text
    }

}
