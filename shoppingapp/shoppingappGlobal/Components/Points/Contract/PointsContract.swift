//
// PointsContract.swift
//  shoppingapp
//
//
//  Created by Ruben Fernandez on 21/01/2018.
//  Copyright © 2017 BBVA. All rights reserved.
//

protocol PointsViewProtocol: ComponentViewProtocol {

    func showDisplayData(withPointsDisplayData pointsDisplayData: PointsDisplayData)
}

protocol PointsPresenterProtocol: ComponentPresenterProtocol {

    func receivePoinsDisplayData(withPointsDisplayData pointsDisplayData: PointsDisplayData)

}
