//
//  TransactionDetailDisplayData.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 20/11/2017.
//  Copyright © 2017 BBVA. All rights reserved.
//

struct PointsDisplayData {

    var points: NSAttributedString?
    var descriptionPoints: NSAttributedString?
    var needShowPoinst = false
    var imageBackgroundName = ConstantsImages.Card.backgroundPoints_image

    static func generatePointsDisplayData(withCardBO cardBO: CardBO) -> PointsDisplayData {

        var pointsDisplayData = PointsDisplayData()

        if cardBO.isCreditCard() {

            let results = cardBO.rewards.filter { $0.rewardId == RewardBO.bancomer_points }

            if !results.isEmpty {
                let reward = results[0] as RewardBO
                pointsDisplayData.needShowPoinst = true

                if let nonmonetaryValue = reward.nonMonetaryValue, nonmonetaryValue == 0 {

                    pointsDisplayData.descriptionPoints = NSAttributedString(string: Localizables.cards.key_cards_accumulate_points_text, attributes: [
                            NSAttributedStringKey.foregroundColor: UIColor.BBVA600,
                            NSAttributedStringKey.font: Tools.setFontBookItalic(size: 14)
                            ])

                } else if let name = reward.name, let nonmonetaryValue = reward.nonMonetaryValue {

                    pointsDisplayData.points = NSAttributedString(string: NumberFormatter.stringDecimalToStringWithSeparator(withString: "\(nonmonetaryValue)", withLocale: Locale.current), attributes: [
                        NSAttributedStringKey.foregroundColor: UIColor.BBVA600,
                        NSAttributedStringKey.font: Tools.setFontBook(size: 24)
                        ])

                    pointsDisplayData.descriptionPoints = NSAttributedString(string: name, attributes: [
                        NSAttributedStringKey.foregroundColor: UIColor.BBVA600,
                        NSAttributedStringKey.font: Tools.setFontBookItalic(size: 14)
                        ])

                } else {
                    pointsDisplayData.descriptionPoints = NSAttributedString(string: Localizables.cards.key_cards_accumulate_points_text, attributes: [
                        NSAttributedStringKey.foregroundColor: UIColor.BBVA600,
                        NSAttributedStringKey.font: Tools.setFontBookItalic(size: 14)
                        ])
                }

            }

        }

        return pointsDisplayData
    }
}
