//
//  PointsView
//  shoppingapp
//
//  Created by Ruben Fernandez on 21/01/2018.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation

class PointsView: XibView {

    @IBOutlet weak var pointsLabel: UILabel!
    @IBOutlet weak var descriptionPointsLabel: UILabel!
    @IBOutlet weak var backgroundPointsImage: UIImageView!

    var presenter = PointsPresenter()

    override func configureView() {
        super.configureView()
        presenter.view = self
    }

    func showDisplayDataPoints(withPointsDisplayData pointsDisplayData: PointsDisplayData) {
        presenter.receivePoinsDisplayData(withPointsDisplayData: pointsDisplayData)
    }

}

extension PointsView: PointsViewProtocol {

    func showDisplayData(withPointsDisplayData pointsDisplayData: PointsDisplayData) {

        if let points = pointsDisplayData.points {
            pointsLabel.isHidden = false
            pointsLabel.attributedText = points

        } else {
            pointsLabel.isHidden = true
        }

        descriptionPointsLabel.attributedText = pointsDisplayData.descriptionPoints
        backgroundPointsImage.image = UIImage(named: pointsDisplayData.imageBackgroundName)

    }

}
