//
// PointsPresenter.swift
//  shoppingapp
//
//
//  Created by Ruben Fernandez on 21/01/2018.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation

class  PointsPresenter {
    weak var view: PointsViewProtocol?
}

extension PointsPresenter: PointsPresenterProtocol {

    func receivePoinsDisplayData(withPointsDisplayData pointsDisplayData: PointsDisplayData) {
        view?.showDisplayData(withPointsDisplayData: pointsDisplayData)
    }

}
