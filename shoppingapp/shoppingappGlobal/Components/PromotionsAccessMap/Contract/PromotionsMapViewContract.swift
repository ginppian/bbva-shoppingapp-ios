//
//  PromotionsMapViewContract.swift
//  shoppingapp
//
//  Created by jesus.martinez on 20/3/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit

protocol PromotionsAccessMapViewProtocol: ComponentViewProtocol {

    func showLoading()
    func hideLoading()

}

protocol PromotionsAccessMapPresenterProtocol: ComponentPresenterProtocol {

    func startLoading()
    func finishLoading()

}
