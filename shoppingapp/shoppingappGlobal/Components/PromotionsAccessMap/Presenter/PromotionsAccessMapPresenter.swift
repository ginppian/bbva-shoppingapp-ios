//
//  PromotionsAccessMapPresenter.swift
//  shoppingapp
//
//  Created by jesus.martinez on 20/3/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit

class PromotionsAccessMapPresenter {

    weak var view: PromotionsAccessMapViewProtocol?
    var isLoading = false

}

extension PromotionsAccessMapPresenter: PromotionsAccessMapPresenterProtocol {

    func startLoading() {

        if !isLoading {
            view?.showLoading()
        }

        isLoading = true
    }

    func finishLoading() {

        if isLoading {
            view?.hideLoading()
        }

        isLoading = false

    }

}
