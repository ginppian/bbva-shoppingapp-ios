//
//  PromotionsAccessMapView.swift
//  shoppingapp
//
//  Created by jesus.martinez on 4/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit
import Shimmer

protocol PromotionsAccessMapViewDelegate: class {
    func promotionsAccessMapViewSelected(_ promotionsAccessMapView: PromotionsAccessMapView)
}

class PromotionsAccessMapView: XibView {

    let presenter = PromotionsAccessMapPresenter()

    static let height: CGFloat = 54.0

    static let imageSize: Float = 22.0
    static let padding: CGFloat = 20.0

    weak var delegate: PromotionsAccessMapViewDelegate?

    @IBOutlet weak var loadingViewContainer: UIView!
    @IBOutlet weak var shimmeringView: FBShimmeringView!

    @IBOutlet weak var contentViewContainer: UIView!
    @IBOutlet weak var accessMapButton: UIButton!
    @IBOutlet weak var separatorLineView: UIView!

    // MARK: - Private Helper Methods

    override func configureView() {
        super.configureView()

        presenter.view = self

        backgroundColor = .BBVAWHITE
        separatorLineView.backgroundColor = .BBVA200

        accessMapButton.setTitle(Localizables.promotions.key_promotions_see_promotions_near_me_text, for: .normal)
        accessMapButton.titleLabel?.font = Tools.setFontBook(size: 14)
        accessMapButton.setTitleColor(.BBVA600, for: .normal)
        accessMapButton.setTitleColor(.COREBLUE, for: .highlighted)
        accessMapButton.setTitleColor(.COREBLUE, for: .selected)

        accessMapButton.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Promotions.place_icon, color: .MEDIUMBLUE), width: PromotionsAccessMapView.imageSize, height: PromotionsAccessMapView.imageSize), for: .normal)
        accessMapButton.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Promotions.place_icon, color: .COREBLUE), width: PromotionsAccessMapView.imageSize, height: PromotionsAccessMapView.imageSize), for: .normal)

        accessMapButton.imageView?.contentMode = .scaleAspectFit

        let buttonWidth = UIScreen.main.bounds.width - (PromotionsAccessMapView.padding * 2)

        accessMapButton.imageEdgeInsets = UIEdgeInsets(top: 0.0, left: buttonWidth - CGFloat(PromotionsAccessMapView.imageSize), bottom: 0.0, right: 0.0)
        accessMapButton.titleEdgeInsets = UIEdgeInsets(top: 0.0, left: -CGFloat(PromotionsAccessMapView.imageSize), bottom: 0.0, right: 0.0)

        loadingViewContainer.backgroundColor = .BBVAWHITE
        loadingViewContainer.isHidden = true

        var imageSkeleton = SVGCache.image(forSVGNamed: ConstantsImages.Skeleton.lines, color: .BBVA300)
        imageSkeleton = imageSkeleton.resizableImage(withCapInsets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 20))
        shimmeringView.contentView = UIImageView(image: imageSkeleton)

    }

    @IBAction func accessMapAction(_ sender: Any) {

        delegate?.promotionsAccessMapViewSelected(self)

    }
}

extension PromotionsAccessMapView: PromotionsAccessMapViewProtocol {

    func showLoading() {

        shimmeringView.isShimmering = true
        loadingViewContainer.isHidden = false

    }

    func hideLoading() {

        loadingViewContainer.isHidden = true
        shimmeringView.isShimmering = false

    }

}
