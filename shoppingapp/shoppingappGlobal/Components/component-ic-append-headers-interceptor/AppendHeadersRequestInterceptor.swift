//
//  AppendHeadersRequestInterceptor.swift
//  shoppingappMX
//
//  Created by jesus.martinez on 30/10/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import CellsNativeComponents
import BBVA_Network

class AppendHeadersRequestInterceptor: NetworkInterceptor {
    
    private let acceptLanguageMX = "es-MX;q=1"
    
    func onRequest(request: NetworkRequest) {
        
        request.headers[Headers.ACCEPT_LANGUAGE] = acceptLanguageMX
        
        // TODO: Remove check selectedEnvironment when development environment work properly and accept deviceid
        // This TODO should be removed it when all environments have to send deviceID in headers
        if urlRequireDeviceId(request.url) && ServerHostURL.environmentNeedsExtraSecurity() {
            
            request.headers["authenticationtype"] = "58"
            if let device = DeviceManager.instance.deviceUID {
                DLog(message: "deviceID: \(device)")
                request.headers["authenticationdata"] = "deviceid=\(device)"
            }
        }
    }
    
    fileprivate func urlRequireDeviceId(_ url: String) -> Bool {
        
        return ServerHostURL.getCustomerURL() + "/customers" == url
    }
    
    func onResponse(response: NetworkResponse) {
        
    }
    
}
