//
//  DidacticAreaDisplayData.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 15/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

struct DidacticAreaDisplayData {

    var isTypeOffer = false
    var imageBackground = ConstantsImages.DidacticArea.imageBackgroundDefault
    var imageBackgroundOffer = ConstantsImages.DidacticArea.imageBackgroundOfferDefault
    var imageBackgroundURL: String = ""
    var imageThumb = ConstantsImages.DidacticArea.imageThumbDefault
    var imageThumbURL: String = ""
    var title: String = ""
    var description: String = ""
    var categoryName: String = ""

    var titleColor: UIColor = .BBVAWHITE
    var descriptionColor: UIColor = .BBVALIGHTBLUE
    var descriptionOnTapColor: UIColor = .MEDIUMBLUE

    var helpId: String = ""
    var categoryId = ""
    var url: URL?

    var omnitureCampaignName = ""
    static let errorCampaignName = "Hola persona"
    
    static func generateDidacticAreaDisplayData(withDidacticAreaBO didacticAreaBO: DidacticAreaBO?, andDidacticCategoryBO didacticCategoryBO: DidacticCategoryBO?) -> DidacticAreaDisplayData {

        var didacticAreaDisplayData = DidacticAreaDisplayData()
        var isIdSpecial = false

        let name = KeychainManager.shared.getFirstname()
        let firstname = (!name.isEmpty) ? " \(name)" : ""

        didacticAreaDisplayData.title = Localizables.didactica.key_didactic_hello_text + "\(firstname)" + Localizables.didactica.key_didactic_exclamation_mark_text
        didacticAreaDisplayData.omnitureCampaignName = errorCampaignName
        didacticAreaDisplayData.description = Localizables.didactica.key_didactic_explore_text

        if let didacticArea = didacticAreaBO {

            if didacticArea.startDate != nil, didacticArea.endDate != nil {
                isIdSpecial = true
            }

            if didacticArea.type == .unknowm {
                return didacticAreaDisplayData.showDefaultDidacticAreaTypeDidactic()
            }

            if didacticArea.type == .offer {
                didacticAreaDisplayData.isTypeOffer = true
            }
            
            didacticAreaDisplayData.url = didacticArea.url
            
            if let imageBackground = didacticArea.imageBackground, !imageBackground.isEmpty {
                didacticAreaDisplayData.imageBackgroundURL = imageBackground
            }

            if let imageThumb = didacticArea.imageThumb, !imageThumb.isEmpty {
                didacticAreaDisplayData.imageThumbURL = didacticArea.getImageURL(forUrl: imageThumb)
            }

            if let idDidacticArea = didacticArea.helpId, !idDidacticArea.isEmpty {

                didacticAreaDisplayData.helpId = idDidacticArea

                if let selectedLanguageData = didacticArea.getSelectedLanguage(forKey: ConstantsLanguage.DEFAULT_LANGUAGE), !selectedLanguageData.title.isEmpty && !selectedLanguageData.description.isEmpty {

                    didacticAreaDisplayData.title = selectedLanguageData.title
                    didacticAreaDisplayData.omnitureCampaignName = didacticAreaDisplayData.title
                    didacticAreaDisplayData.description = selectedLanguageData.description
                } else {

                    if didacticArea.type == .didactic {
                        return didacticAreaDisplayData.showDefaultDidacticAreaTypeDidactic()
                    } else if didacticArea.type == .offer && isIdSpecial {
                        didacticAreaDisplayData.title = Localizables.didactica.key_didactic_promotions_text
                        didacticAreaDisplayData.omnitureCampaignName = didacticAreaDisplayData.title
                        didacticAreaDisplayData.description = Localizables.didactica.key_didactic_discover_text
                    }
                }

            } else {

                if didacticArea.type == .didactic {
                    return didacticAreaDisplayData.showDefaultDidacticAreaTypeDidactic()
                }
            }

            if let titleColor = didacticArea.titleColor, let hexStringTitleColor = titleColor.hexString, !hexStringTitleColor.isEmpty, let alphaTitleColor = titleColor.alpha, let descriptionColor = didacticArea.descriptionColor, let hexStringDescriptionColor = descriptionColor.hexString, !hexStringDescriptionColor.isEmpty, let alphaDescriptionColor = descriptionColor.alpha, let descriptionOnTapColor = didacticArea.descriptionOnTapColor, let hexStringDescriptionOnTapColor = descriptionOnTapColor.hexString, !hexStringDescriptionOnTapColor.isEmpty, let alphaDescriptionOnTapColor = descriptionOnTapColor.alpha {

                didacticAreaDisplayData.titleColor = Colors.hexStringToUIColor(hex: hexStringTitleColor, alpha: alphaTitleColor)
                didacticAreaDisplayData.descriptionColor = Colors.hexStringToUIColor(hex: hexStringDescriptionColor, alpha: alphaDescriptionColor)
                didacticAreaDisplayData.descriptionOnTapColor = Colors.hexStringToUIColor(hex: hexStringDescriptionOnTapColor, alpha: alphaDescriptionOnTapColor)
            } else {

                if didacticArea.type == .offer && isIdSpecial {

                    didacticAreaDisplayData = didacticAreaDisplayData.setDefaultColorsAndBackgroundDidacticAreaTypeOffer(withDidacticAreaDisplayData: didacticAreaDisplayData)
                } else {
                    didacticAreaDisplayData = didacticAreaDisplayData.setDefaultColorsAndBackgroundDidacticAreaTypeDidactic(withDidacticAreaDisplayData: didacticAreaDisplayData)
                }
            }

            if didacticAreaDisplayData.imageBackgroundURL.isEmpty && didacticArea.type == .offer {
                didacticAreaDisplayData = didacticAreaDisplayData.setDefaultColorsAndBackgroundDidacticAreaTypeOffer(withDidacticAreaDisplayData: didacticAreaDisplayData)
            }
        }

        //Didactic Area type == Offer
        if let didacticCategory = didacticCategoryBO {

            if let selectedLanguageData = didacticCategory.getSelectedLanguage(forKey: ConstantsLanguage.DEFAULT_LANGUAGE), !selectedLanguageData.title.isEmpty && !selectedLanguageData.description.isEmpty {

                didacticAreaDisplayData.title = selectedLanguageData.title
                 didacticAreaDisplayData.omnitureCampaignName = didacticAreaDisplayData.title
                didacticAreaDisplayData.description = selectedLanguageData.description
            } else {
                didacticAreaDisplayData.title = Localizables.didactica.key_didactic_promotions_text
                didacticAreaDisplayData.omnitureCampaignName = didacticAreaDisplayData.title
                didacticAreaDisplayData.description = Localizables.didactica.key_didactic_discover_text
            }
            
            if let categoryName = didacticCategory.getCategoryName(forLanguageKey: ConstantsLanguage.DEFAULT_LANGUAGE), !categoryName.name.isEmpty {
                
                didacticAreaDisplayData.categoryName = categoryName.name
            }

            if let titleColor = didacticCategory.titleColor, let hexStringTitleColor = titleColor.hexString, !hexStringTitleColor.isEmpty, let alphaTitleColor = titleColor.alpha, let descriptionColor = didacticCategory.descriptionColor, let hexStringDescriptionColor = descriptionColor.hexString, !hexStringDescriptionColor.isEmpty, let alphaDescriptionColor = descriptionColor.alpha, let descriptionOnTapColor = didacticCategory.descriptionOnTapColor, let hexStringDescriptionOnTapColor = descriptionOnTapColor.hexString, !hexStringDescriptionOnTapColor.isEmpty, let alphaDescriptionOnTapColor = descriptionOnTapColor.alpha, let imageBackground = didacticCategory.imageBackground, !imageBackground.isEmpty {

                didacticAreaDisplayData.imageBackgroundURL = imageBackground

                didacticAreaDisplayData.titleColor = Colors.hexStringToUIColor(hex: hexStringTitleColor, alpha: alphaTitleColor)
                didacticAreaDisplayData.descriptionColor = Colors.hexStringToUIColor(hex: hexStringDescriptionColor, alpha: alphaDescriptionColor)
                didacticAreaDisplayData.descriptionOnTapColor = Colors.hexStringToUIColor(hex: hexStringDescriptionOnTapColor, alpha: alphaDescriptionOnTapColor)
            } else {
                didacticAreaDisplayData = didacticAreaDisplayData.setDefaultColorsAndBackgroundDidacticAreaTypeOffer(withDidacticAreaDisplayData: didacticAreaDisplayData)
            }

            if let category = didacticCategory.categoryId, !category.isEmpty {

                didacticAreaDisplayData.categoryId = category

            } else {
                return didacticAreaDisplayData.showDefaultDidacticAreaTypeDidactic()
            }

        } else {

            if let didacticArea = didacticAreaBO, didacticArea.type == .offer, !isIdSpecial {
                return didacticAreaDisplayData.showDefaultDidacticAreaTypeDidactic()
            }
        }

        return didacticAreaDisplayData
    }

    func showDefaultDidacticAreaTypeDidactic() -> DidacticAreaDisplayData {

        var didacticAreaDisplayData = DidacticAreaDisplayData()

        didacticAreaDisplayData.helpId = ""

        let name = KeychainManager.shared.getFirstname()
        let firstname = (!name.isEmpty) ? " \(name)" : ""

        didacticAreaDisplayData.title = Localizables.didactica.key_didactic_hello_text + "\(firstname)" + Localizables.didactica.key_didactic_exclamation_mark_text
        didacticAreaDisplayData.omnitureCampaignName = DidacticAreaDisplayData.errorCampaignName
        didacticAreaDisplayData.description = Localizables.didactica.key_didactic_explore_text

        didacticAreaDisplayData.imageBackgroundURL = ""
        didacticAreaDisplayData.imageThumbURL = ""

        didacticAreaDisplayData.imageBackground = ConstantsImages.DidacticArea.imageBackgroundDefault
        didacticAreaDisplayData.imageThumb = ConstantsImages.DidacticArea.imageThumbDefault

        didacticAreaDisplayData.titleColor = .BBVAWHITE
        didacticAreaDisplayData.descriptionColor = .BBVALIGHTBLUE
        didacticAreaDisplayData.descriptionOnTapColor = .MEDIUMBLUE
        
        didacticAreaDisplayData.categoryId = ""
        didacticAreaDisplayData.categoryName = ""
        didacticAreaDisplayData.url = nil

        return didacticAreaDisplayData
    }

    func setDefaultColorsAndBackgroundDidacticAreaTypeDidactic(withDidacticAreaDisplayData didacticAreaDisplayData: DidacticAreaDisplayData) -> DidacticAreaDisplayData {

        var didacticAreaDisplayDataCopy = DidacticAreaDisplayData()

        didacticAreaDisplayDataCopy = didacticAreaDisplayData

        didacticAreaDisplayDataCopy.imageBackgroundURL = ""
        didacticAreaDisplayDataCopy.imageBackground = ConstantsImages.DidacticArea.imageBackgroundDefault

        didacticAreaDisplayDataCopy.titleColor = .BBVAWHITE
        didacticAreaDisplayDataCopy.descriptionColor = .BBVALIGHTBLUE
        didacticAreaDisplayDataCopy.descriptionOnTapColor = .MEDIUMBLUE

        return didacticAreaDisplayDataCopy
    }

    func setDefaultColorsAndBackgroundDidacticAreaTypeOffer(withDidacticAreaDisplayData didacticAreaDisplayData: DidacticAreaDisplayData) -> DidacticAreaDisplayData {

        var didacticAreaDisplayDataCopy = DidacticAreaDisplayData()

        didacticAreaDisplayDataCopy = didacticAreaDisplayData

        didacticAreaDisplayDataCopy.imageBackgroundURL = ""
        didacticAreaDisplayDataCopy.imageBackground = ConstantsImages.DidacticArea.imageBackgroundOfferDefault

        didacticAreaDisplayDataCopy.titleColor = .BBVAWHITE
        didacticAreaDisplayDataCopy.descriptionColor = .BBVAWHITE
        didacticAreaDisplayDataCopy.descriptionOnTapColor = UIColor.BBVAWHITE.withAlphaComponent(0.3)

        return didacticAreaDisplayDataCopy
    }
}

extension DidacticAreaDisplayData: Equatable {

    public static func == (lhs: DidacticAreaDisplayData, rhs: DidacticAreaDisplayData) -> Bool {
        return lhs.isTypeOffer == rhs.isTypeOffer
            && lhs.imageBackground == rhs.imageBackground
            && lhs.imageBackgroundOffer == rhs.imageBackgroundOffer
            && lhs.imageBackgroundURL == rhs.imageBackgroundURL
            && lhs.imageThumb == rhs.imageThumb
            && lhs.imageThumbURL == rhs.imageThumbURL
            && lhs.title == rhs.title
            && lhs.description == rhs.description
            && lhs.titleColor == rhs.titleColor
            && lhs.descriptionColor == rhs.descriptionColor
            && lhs.descriptionOnTapColor == rhs.descriptionOnTapColor
            && lhs.helpId == rhs.helpId
            && lhs.categoryId == rhs.categoryId
    }

}
