//
//  DidacticAreaView.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 26/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class DidacticAreaView: XibView {

    let heightPath: CGFloat = 74.0

    @IBOutlet weak var backgroundImageView: UIImageView!

    @IBOutlet weak var typeOneView: UIView!
    @IBOutlet weak var titleOneLabel: UILabel!
    @IBOutlet weak var linkOneButton: UIButton!
    @IBOutlet weak var typeOneViewTopConstraint: NSLayoutConstraint!

    @IBOutlet weak var typeTwoView: UIView!
    @IBOutlet weak var titleTwoLabel: UILabel!
    @IBOutlet weak var linkTwoButton: UIButton!
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var typeTwoViewTopConstraint: NSLayoutConstraint!

    var currentDisplayData: DidacticAreaDisplayData?

    var presenter = DidacticAreaPresenter()

    var maskLayer: CAShapeLayer!

    override func configureView() {
        super.configureView()
        presenter.view = self
    }

    func configureViewItems() {

        backgroundColor = .clear
        backgroundImageView.image = nil
        backgroundImageView.backgroundColor = .NAVY
        backgroundImageView.contentMode = .scaleAspectFill
        backgroundImageView.clipsToBounds = true
        backgroundImageView.image = UIImage(named: ConstantsImages.DidacticArea.imageBackgroundDefault)

        configureTypeOneView()
        configureTypeTwoView()

        typeOneView.isHidden = true

        addShapeLayerToView()
    }

    func addShapeLayerToView() {

        if let layer = maskLayer {
            layer.removeFromSuperlayer()
        }

        maskLayer = CAShapeLayer()

        maskLayer.path = createPath().cgPath

        maskLayer.fillColor = UIColor.BBVA100.cgColor

        layer.addSublayer(maskLayer)
        backgroundImageView.layer.addSublayer(maskLayer)
    }

    func createPath() -> UIBezierPath {

        let pathArea = UIBezierPath()

        pathArea.move(to: CGPoint(x: 0.0, y: bounds.size.height))
        pathArea.addLine(to: CGPoint(x: bounds.size.width, y: bounds.size.height))
        pathArea.addLine(to: CGPoint(x: bounds.size.width, y: bounds.size.height - heightPath))

        pathArea.close()

        return pathArea

    }

    func configureTypeOneView() {

        typeOneView.backgroundColor = .clear

        titleOneLabel.font = Tools.setFontBook(size: 22)
        titleOneLabel.textColor = .BBVAWHITE
        titleOneLabel.textAlignment = .center
        titleOneLabel.numberOfLines = 2
        titleOneLabel.minimumScaleFactor = 14 / 22
        titleOneLabel.adjustsFontSizeToFitWidth = true

        linkOneButton.setTitleColor(.BBVAWHITE, for: .normal)
        linkOneButton.setTitleColor(UIColor.BBVAWHITE.withAlphaComponent(0.3), for: .highlighted)
        linkOneButton.setTitleColor(UIColor.BBVAWHITE.withAlphaComponent(0.3), for: .selected)
        linkOneButton.titleLabel?.font = Tools.setFontMedium(size: 14)
        linkOneButton.titleLabel?.lineBreakMode = .byTruncatingTail
    }

    func configureTypeTwoView() {

        typeTwoView.backgroundColor = .clear

        titleTwoLabel.font = Tools.setFontBook(size: 20)
        titleTwoLabel.textColor = .BBVAWHITE
        titleTwoLabel.textAlignment = .left
        titleTwoLabel.numberOfLines = 3
        titleTwoLabel.minimumScaleFactor = 14 / 20
        titleTwoLabel.adjustsFontSizeToFitWidth = true

        linkTwoButton.setTitleColor(.BBVALIGHTBLUE, for: .normal)
        linkTwoButton.setTitleColor(.MEDIUMBLUE, for: .highlighted)
        linkTwoButton.setTitleColor(.MEDIUMBLUE, for: .selected)
        linkTwoButton.titleLabel?.font = Tools.setFontMedium(size: 14)
        linkTwoButton.contentHorizontalAlignment = .left
        linkTwoButton.titleLabel?.lineBreakMode = .byTruncatingTail
        linkTwoButton.titleLabel?.numberOfLines = 2

        thumbnailImageView.image = nil
        thumbnailImageView.contentMode = .scaleAspectFill
    }

    @IBAction func didacticAreaOfferButtonPressed(_ sender: Any) {
        
        if let url = currentDisplayData?.url {
            
            presenter.didacticAreaSpecialOfferButtonPressed(withURL: url, andTitle: url.domain)
        } else if currentDisplayData?.categoryId != nil {

            presenter.didacticAreaOfferButtonPressed(withCategoryId: currentDisplayData?.categoryId, andTitle: currentDisplayData?.categoryName)
        }
        
        TrackerHelper.sharedInstance().trackHomeBannerClick(withCustomerID: SessionDataManager.sessionDataInstance().customerID,
                                                            campaignName: currentDisplayData?.omnitureCampaignName.removeSpecialCharsAndAccentFromString)
    }

    @IBAction func didacticAreaButtonPressed(_ sender: Any) {

        presenter.didacticAreaButtonPressed(withHelpId: currentDisplayData?.helpId)
        TrackerHelper.sharedInstance().trackHomeBannerClick(withCustomerID: SessionDataManager.sessionDataInstance().customerID,
                                                        campaignName: currentDisplayData?.omnitureCampaignName.removeSpecialCharsAndAccentFromString)
    }

    func configureViewToStatusBarBigHeight(withHeightConstraint heightConstraint: NSLayoutConstraint) -> CGFloat {

        let statusHeight = UIApplication.shared.statusBarFrame.height
        let space = statusHeight - 20
        typeOneViewTopConstraint.constant = space
        typeTwoViewTopConstraint.constant = space

        heightConstraint.constant = frame.size.height + space
        layoutIfNeeded()
        return space
    }

    func setDidacticAreaDisplayData(withDisplayData displayData: DidacticAreaDisplayData) {

        currentDisplayData = displayData
        presenter.receiveDidacticAreaDisplayData(withDisplayData: displayData)
    }

    func animationBackgroundImage(toImageView imageView: UIImageView, andImage image: UIImage) {

        DispatchQueue.main.async {
            
            UIView.transition(with: imageView, duration: 1.0, options: .transitionCrossDissolve, animations: {
                imageView.image = image
            }, completion: { (_: Bool) in
                
            })
        }
    }
}

extension DidacticAreaView: DidacticAreaViewProtocol {

    func showDidacticAreaOffer(withDisplayData displayData: DidacticAreaDisplayData) {

        typeOneView.isHidden = false
        typeTwoView.isHidden = true

        titleOneLabel.text = displayData.title
        titleOneLabel.textColor = displayData.titleColor
        linkOneButton.setTitle(displayData.description, for: .normal)

        linkOneButton.setTitleColor(displayData.descriptionColor, for: .normal)
        linkOneButton.setTitleColor(displayData.descriptionOnTapColor, for: .highlighted)
        linkOneButton.setTitleColor(displayData.descriptionOnTapColor, for: .selected)

        if !displayData.imageBackgroundURL.isEmpty {
            let url = URL(string: displayData.imageBackgroundURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")
            UIImage.load(fromUrl: url, completion: { loadedImage in
                guard let image = loadedImage else {
                    self.animationBackgroundImage(toImageView: self.backgroundImageView, andImage: UIImage(named: displayData.imageBackgroundOffer)!)
                    return
                }
                
                self.animationBackgroundImage(toImageView: self.backgroundImageView, andImage: image)
            })

        } else {

            self.animationBackgroundImage(toImageView: self.backgroundImageView, andImage: UIImage(named: displayData.imageBackgroundOffer)!)
        }
    }

    func showDidacticArea(withDisplayData displayData: DidacticAreaDisplayData) {

        typeOneView.isHidden = true
        typeTwoView.isHidden = false

        titleTwoLabel.text = displayData.title
        titleTwoLabel.textColor = displayData.titleColor
        linkTwoButton.setTitle(displayData.description, for: .normal)

        linkTwoButton.setTitleColor(displayData.descriptionColor, for: .normal)
        linkTwoButton.setTitleColor(displayData.descriptionOnTapColor, for: .highlighted)
        linkTwoButton.setTitleColor(displayData.descriptionOnTapColor, for: .selected)

        if !displayData.imageThumbURL.isEmpty {

            let url = URL(string: displayData.imageThumbURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")
            UIImage.load(fromUrl: url, completion: { loadedImage in
                guard let image = loadedImage else {
                    self.animationBackgroundImage(toImageView: self.thumbnailImageView, andImage: UIImage(named: displayData.imageThumb)!)
                    return
                }
                
                self.animationBackgroundImage(toImageView: self.thumbnailImageView, andImage: image)
            })
        } else {

            self.animationBackgroundImage(toImageView: self.thumbnailImageView, andImage: UIImage(named: displayData.imageThumb)!)
        }

        if !displayData.imageBackgroundURL.isEmpty {

            let url = URL(string: displayData.imageBackgroundURL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")
            UIImage.load(fromUrl: url, completion: { loadedImage in
                guard let image = loadedImage else {
                    self.animationBackgroundImage(toImageView: self.backgroundImageView, andImage: UIImage(named: displayData.imageBackground)!)
                    return
                }
                
                self.animationBackgroundImage(toImageView: self.backgroundImageView, andImage: image)
            })
        } else {

            self.animationBackgroundImage(toImageView: self.backgroundImageView, andImage: UIImage(named: displayData.imageBackground)!)
        }
    }

}
