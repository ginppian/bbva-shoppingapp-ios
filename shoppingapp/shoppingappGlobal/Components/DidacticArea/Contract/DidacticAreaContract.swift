//
//  DidacticAreaContract.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 20/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol DidacticAreaViewProtocol: ComponentViewProtocol {

    func showDidacticAreaOffer(withDisplayData displayData: DidacticAreaDisplayData)
    func showDidacticArea(withDisplayData displayData: DidacticAreaDisplayData)

}

protocol DidacticAreaPresenterProtocol: ComponentPresenterProtocol {

    func receiveDidacticAreaDisplayData(withDisplayData displayData: DidacticAreaDisplayData)
    func didacticAreaOfferButtonPressed(withCategoryId categoryId: String?, andTitle title: String?)
    func didacticAreaButtonPressed(withHelpId helpId: String?)
    func didacticAreaSpecialOfferButtonPressed(withURL url: URL?, andTitle title: String?)
}
