//
//  DidacticAreaPresenter.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 20/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol DidacticAreaPresenterDelegate: class {

    func didacticAreaPresenterDidacticButtonPressed(_ didacticAreaPresenter: DidacticAreaPresenter, andHelpId helpId: String)
    func didacticAreaPresenterOfferButtonPressed(_ didacticAreaPresenter: DidacticAreaPresenter, categoryId: String, andTitle title: String)
    func didacticAreaPresenterSpecialOfferButtonPressed(_ didacticAreaPresenter: DidacticAreaPresenter, _ url: URL, andTitle title: String)
}

class  DidacticAreaPresenter {

    weak var view: DidacticAreaViewProtocol?
    weak var delegate: DidacticAreaPresenterDelegate?
}

extension DidacticAreaPresenter: DidacticAreaPresenterProtocol {

    func receiveDidacticAreaDisplayData(withDisplayData displayData: DidacticAreaDisplayData) {

        if displayData.isTypeOffer {
            view?.showDidacticAreaOffer(withDisplayData: displayData)
        } else {
            view?.showDidacticArea(withDisplayData: displayData)
        }
    }
    
    func didacticAreaOfferButtonPressed(withCategoryId categoryId: String?, andTitle title: String?) {

        if let categoryId = categoryId {
            delegate?.didacticAreaPresenterOfferButtonPressed(self, categoryId: categoryId, andTitle: title ?? "")
        }
    }
    
    func didacticAreaButtonPressed(withHelpId helpId: String?) {
        
        if let helpId = helpId {
            delegate?.didacticAreaPresenterDidacticButtonPressed(self, andHelpId: helpId)
        }
    }
    
    func didacticAreaSpecialOfferButtonPressed(withURL url: URL?, andTitle title: String?) {
        
        if let url = url, let title = title {
            delegate?.didacticAreaPresenterSpecialOfferButtonPressed(self, url, andTitle: title)
        }
    }
}
