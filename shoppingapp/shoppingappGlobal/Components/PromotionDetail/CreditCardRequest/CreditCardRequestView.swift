//
//  CreditCardRequestView.swift
//  shoppingapp
//
//  Created by Rubén Jacobo on 2/21/19.
//  Copyright © 2019 BBVA. All rights reserved.
//

import UIKit

protocol CreditCardRequestViewDelegate: class {
    func showCreditCardRequestLinkOnWebPage(_ creditCardRequest: CreditCardRequestView)
}

class CreditCardRequestView: XibView {
    
    weak var delegate: CreditCardRequestViewDelegate?
    
    @IBOutlet weak var cardImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var descriptionButton: UIButton!

    override func configureView() {
        
        super.configureView()
        configureViewItems()
    }
    
    private func configureViewItems() {
    
        backgroundColor = .COREBLUE
        
        cardImageView.image = UIImage(named: ConstantsImages.Card.creditCardRequestIcon)
        
        titleLabel.font = Tools.setFontMedium(size: 14.0)
        titleLabel.textColor = .BBVAWHITE
        titleLabel.text = Localizables.promotions.key_promotions_link_credit_card
        
        descriptionLabel.font = Tools.setFontBook(size: 14.0)
        descriptionLabel.textColor = .BBVAWHITE
        descriptionLabel.text = Localizables.promotions.key_promotions_link_credit_card_message
        
        descriptionButton.contentMode = .scaleAspectFit
        descriptionButton.titleLabel?.font = Tools.setFontMedium(size: 14.0)
        descriptionButton.setTitle(Localizables.promotions.key_promotions_more_info_text, for: .normal)
        descriptionButton.setTitleColor(.BBVAWHITE, for: .normal)
        descriptionButton.setTitleColor(.BBVAWHITE30, for: .highlighted)
        descriptionButton.setTitleColor(.BBVAWHITE30, for: .selected)
    }
    
    @IBAction func descriptionButtonAction(_ sender: Any) {
        
        delegate?.showCreditCardRequestLinkOnWebPage(self)
    }
    
    // MARK: Touch logic
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        
        descriptionButton.setTitleColor(.BBVAWHITE30, for: .normal)
    }
    
    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesCancelled(touches, with: event)
        
        descriptionButton.setTitleColor(.BBVAWHITE, for: .normal)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        
        descriptionButton.setTitleColor(.BBVAWHITE, for: .normal)
        descriptionButton.sendActions(for: .touchUpInside)
    }
}
