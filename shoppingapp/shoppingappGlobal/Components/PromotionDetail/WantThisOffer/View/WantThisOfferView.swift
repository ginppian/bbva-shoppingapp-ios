//
//  WantThisOfferView.swift
//  shoppingapp
//
//  Created by Rubén Jacobo on 7/26/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit

protocol WantThisOfferViewDelegate: class {
    func wantThisOfferShowStoreWebPage(_ WantThisOffer: WantThisOfferView)
}

class WantThisOfferView: XibView {
    
    // MARK: Constants
    
    weak var delegate: WantThisOfferViewDelegate?
    
    @IBOutlet weak var wantThisOfferDescriptionLabel: UILabel!
    
    @IBOutlet weak var wantThisOfferButton: UIButton!
    
    override func configureView() {
        
        super.configureView()
        wantThisOfferDescriptionLabel.font = Tools.setFontBook(size: 14)
        wantThisOfferDescriptionLabel.textColor = .BBVA600
        wantThisOfferDescriptionLabel.text = Localizables.promotions.key_use_promotion_link_text
       
        wantThisOfferButton.setBackgroundColor(color: UIColor.MEDIUMBLUE, forUIControlState: .normal)
        wantThisOfferButton.setBackgroundColor(color: UIColor.DARKMEDIUMBLUE, forUIControlState: .highlighted)
        
        wantThisOfferButton.titleLabel?.font = Tools.setFontBold(size: 14)
        
        wantThisOfferButton.setTitleColor(.BBVAWHITE, for: .normal)
        
        wantThisOfferButton.setTitle(Localizables.promotions.key_want_this_promotion_text, for: .normal)
        
        layer.shadowColor = UIColor.BBVA300.cgColor
        layer.shadowOpacity = 1
        layer.shadowOffset = CGSize.zero
        layer.shadowRadius = 2
    }
    
    @IBAction func wantThisOfferButtonPressed(_ sender: Any) {
        delegate?.wantThisOfferShowStoreWebPage(self)
    }
}
