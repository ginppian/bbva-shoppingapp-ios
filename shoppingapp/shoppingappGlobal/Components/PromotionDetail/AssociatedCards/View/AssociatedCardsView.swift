//
//  AssociatedCardsView.swift
//  shoppingappMX
//
//  Created by Rubén Jacobo on 9/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit

class AssociatedCardsView: XibView {
    
    // MARK: @IBOutlet's
    @IBOutlet weak var pagerView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var validCardsLabel: UILabel!
    
    @IBOutlet weak var cardsCollectionView: UICollectionView!
    @IBOutlet weak var cardsCollectionViewFlowLayout: UICollectionViewFlowLayout!
    
    var cards: [AssociatedCardsDisplayData]!
    
    override func configureView() {
        
        super.configureView()
        configurePromotionsCollectionView()
        cardsCollectionView.reloadData()
        
    }
    
    func configurePromotionsCollectionView() {
        
        backgroundColor = .BBVA100
        validCardsLabel.font = Tools.setFontMedium(size: 14)
        validCardsLabel.textColor = .BBVA600
        validCardsLabel.text = Localizables.promotions.key_promotions_valid_card_for_the_promotion_text
        
        cardsCollectionView.dataSource = self
        cardsCollectionView.delegate = self
        cardsCollectionView.backgroundColor = .clear
        cardsCollectionView.allowsMultipleSelection = false
        cardsCollectionView.scrollsToTop = false
        cardsCollectionView.bounces = false
        cardsCollectionView.alwaysBounceVertical = false
        cardsCollectionView.alwaysBounceHorizontal = false
        cardsCollectionView.isPagingEnabled = false
        cardsCollectionView.showsHorizontalScrollIndicator = false
        cardsCollectionView.showsVerticalScrollIndicator = false
        cardsCollectionView.delaysContentTouches = false
        cardsCollectionView.decelerationRate = UIScrollViewDecelerationRateFast
        
        cardsCollectionView.register(UINib(nibName: AssociatedCardsCell.identifier, bundle: nil), forCellWithReuseIdentifier: AssociatedCardsCell.identifier)
        
        let associatedCardsBetweenCellsSpacing = CGFloat(20.0)
        let associatedCardsLeftSideMargin = CGFloat(20.0)
        let associatedCardsRightSideMargin = CGFloat(20.0)
        cardsCollectionViewFlowLayout = cardsCollectionView.collectionViewLayout as? UICollectionViewFlowLayout
        cardsCollectionViewFlowLayout.scrollDirection = .horizontal
        cardsCollectionViewFlowLayout.sectionInset = UIEdgeInsets(top: 0, left: associatedCardsLeftSideMargin, bottom: 0, right: associatedCardsRightSideMargin)
        cardsCollectionViewFlowLayout.minimumLineSpacing = associatedCardsBetweenCellsSpacing
        cardsCollectionViewFlowLayout.minimumInteritemSpacing = associatedCardsBetweenCellsSpacing
        cardsCollectionViewFlowLayout.estimatedItemSize = CGSize(width: AssociatedCardsCell.defaultWidth, height: AssociatedCardsCell.defaultHeight)
    }
}

// MARK: - UICollectionViewDataSource implementation
extension AssociatedCardsView: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return cards?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: AssociatedCardsCell.identifier, for: indexPath) as? AssociatedCardsCell {
            
            let card = cards[indexPath.row]
            cell.displayContent(displayData: card)
            
            return cell
        }
        
        return UICollectionViewCell()
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}
