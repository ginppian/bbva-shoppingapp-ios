//
//  AssociatedCardsDisplayData.swift
//  shoppingapp
//
//  Created by Rubén Jacobo on 5/10/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

struct AssociatedCardsDisplayData {
    var cardTitleName: String
    var imageUrl: String?
    var imageNamed: UIImage?
}

extension AssociatedCardsDisplayData: Equatable {
    public static func == (lhs: AssociatedCardsDisplayData, rhs: AssociatedCardsDisplayData) -> Bool {
        
        return lhs.cardTitleName == rhs.cardTitleName && lhs.imageUrl == rhs.imageUrl && lhs.imageNamed == rhs.imageNamed
        
    }
}
