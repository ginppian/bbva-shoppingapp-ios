//
//  AssociatedCardsCell.swift
//  shoppingappMX
//
//  Created by Rubén Jacobo on 9/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit

class AssociatedCardsCell: UICollectionViewCell {
    
    static let identifier = "AssociatedCardsCell"
    static let defaultHeight = 112.0
    static let defaultWidth = 150.0
    
    @IBOutlet weak var financialProductNameLabel: UILabel?
    @IBOutlet weak var cardImageView: UIImageView?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
       if #available(iOS 12, *) {
        
            setupSelfSizingForiOS12(contentView: contentView)
        }
    }
    
    private func setupSelfSizingForiOS12(contentView: UIView) {
        
        contentView.translatesAutoresizingMaskIntoConstraints = false
        
        let leftConstraint = contentView.leftAnchor.constraint(equalTo: leftAnchor)
        let rightConstraint = contentView.rightAnchor.constraint(equalTo: rightAnchor)
        let topConstraint = contentView.topAnchor.constraint(equalTo: topAnchor)
        let bottomConstraint = contentView.bottomAnchor.constraint(equalTo: bottomAnchor)
        
        NSLayoutConstraint.activate([leftConstraint, rightConstraint, topConstraint, bottomConstraint])
    }
    func displayContent(displayData: AssociatedCardsDisplayData) {
        financialProductNameLabel?.font = Tools.setFontBook(size: 16)
        financialProductNameLabel?.textColor = .BBVA600
        backgroundColor = .clear
        financialProductNameLabel?.text = displayData.cardTitleName
        
        if let publicImage = displayData.imageNamed {
            cardImageView?.image = publicImage
        } else {
            if let url = displayData.imageUrl {
                cardImageView?.getCardImage(withUrl: url)
            } else {
                cardImageView?.image = UIImage(named: ConstantsImages.Card.blank_card)
            }
        }
    }
    
}
