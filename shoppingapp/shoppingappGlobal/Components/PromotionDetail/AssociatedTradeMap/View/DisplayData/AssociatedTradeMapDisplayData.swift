//
//  AssociatedTradeMapDisplayData.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 20/6/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

struct AssociatedTradeMapDisplayData: Equatable {

    var distance: String?
    var address: String?
    var additionalInfo: String?
    var latitude: Double?
    var longitude: Double?
    var web: String?
    var email: String?
    var phone: String?
    var mobile: String?
    var promotionTypeColor: UIColor?

    init() {
    }

    init(promotion: PromotionBO, userLocation: GeoLocationBO? = nil, shownOnMap: Bool? = false) {

        if let promotionType = promotion.promotionType, let id = promotionType.id {
            promotionTypeColor = id.color
        }

        if let storesBO = promotion.stores, !storesBO.isEmpty, let storeBO = storesBO.first {

            if userLocation != nil {

                let formattedDistance = storeBO.formattedDistance()

                if let formattedDistance = formattedDistance, !formattedDistance.isEmpty {
                    distance = String(format: Localizables.promotions.key_promotions_near_you_text, formattedDistance)
                }
            }

            if let location = storeBO.location {

                var addressList = [String]()

                if let addressName = location.addressName, !addressName.isEmpty {
                    addressList.append(addressName.capitalized)
                }

                if let zipCode = location.zipCode, !zipCode.isEmpty {
                    addressList.append(zipCode.capitalized)
                }

                if let city = location.city, !city.isEmpty {
                    addressList.append(city.capitalized)
                }

                if let state = location.state, !state.isEmpty {
                    addressList.append(state.capitalized)
                }

                if !addressList.isEmpty {
                    address = addressList.joined(separator: ", ")
                }

                if let additionalInformation = location.additionalInformation, !additionalInformation.isEmpty {
                    additionalInfo = additionalInformation
                }

                if let geoLocation = location.geolocation {
                    latitude = geoLocation.latitude
                    longitude = geoLocation.longitude
                }
            }

            if let contactDetails = storeBO.contactDetails {

                for contactDetail in contactDetails {

                    if let contactType = contactDetail.contactType, let id = contactType.id {

                        switch id {

                        case .email:
                            if let contact = contactDetail.contact, !contact.isEmpty {
                                email = contact
                            }
                        case .phone:
                            if let contact = contactDetail.contact, !contact.isEmpty {
                                phone = contact
                            }
                        case .mobile:
                            if let contact = contactDetail.contact, !contact.isEmpty {
                                mobile = contact
                            }
                        case .web:
                            if let contact = contactDetail.contact, !contact.isEmpty {
                                web = contact
                            }
                        case .unknown:
                            break
                        }
                    }
                }
            }
        }
    }

    func getPhoneContact() -> String {

        if let phone = phone, !phone.isEmpty, let mobile = mobile, !mobile.isEmpty {

            return phone

        } else if let phone = phone, !phone.isEmpty {

            return phone

        } else if let mobile = mobile, !mobile.isEmpty {

            return mobile
        }

        return ""
    }

}
