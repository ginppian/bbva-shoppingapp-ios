//
//  AssociatedTradeMapView.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 20/6/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import GoogleMaps

protocol AssociatedTradeMapViewDelegate: class {

    func associatedTradeMapViewPressedWebsite(_ associatedTradeMapView: AssociatedTradeMapView)
    func associatedTradeMapViewPressedCallToPhone(_ associatedTradeMapView: AssociatedTradeMapView)
    func associatedTradeMapViewPressedSendEmail(_ associatedTradeMapView: AssociatedTradeMapView)
    func associatedTradeMapViewPressedLaunchMap(_ associatedTradeMapView: AssociatedTradeMapView)
}

class AssociatedTradeMapView: XibView {

    @IBOutlet weak var mapView: GMSMapView!

    @IBOutlet weak var iconView: UIView!
    @IBOutlet weak var iconImageView: UIImageView!

    @IBOutlet weak var contentStackView: UIStackView!
    @IBOutlet weak var infoPromotionView: UIView!

    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var additionalInfoLabel: UILabel!

    @IBOutlet weak var infoLineView: UIView!

    @IBOutlet weak var routeButton: UIButton!
    @IBOutlet weak var webButton: UIButton!
    @IBOutlet weak var emailButton: UIButton!
    @IBOutlet weak var phoneButton: UIButton!

    weak var delegate: AssociatedTradeMapViewDelegate?

    var associatedTradeMapDisplayData: AssociatedTradeMapDisplayData?

    let trackerCustomerID = SessionDataManager.sessionDataInstance().customerID

    var moveCameraToMarker = false

    let defaultZoom = Settings.Promotions.default_zoom_of_associated_trade_map

    override func configureView() {

        super.configureView()

        configurationInfoPromotionView()

    }

    // MARK: Configuration

    func configurationInfoPromotionView() {

        configurationMapView()

        iconView.backgroundColor = .MEDIUMBLUE
        iconView.layer.cornerRadius = min(iconView.frame.size.height, iconView.frame.size.width) / 2.0
        iconView.clipsToBounds = true

        iconImageView.image = SVGCache.image(forSVGNamed: ConstantsImages.Promotions.tab_promotions, color: .BBVAWHITE)

        distanceLabel.font = Tools.setFontMedium(size: 14)
        distanceLabel.textColor = .BBVA600

        addressLabel.font = Tools.setFontBook(size: 14)
        addressLabel.textColor = .BBVA600

        additionalInfoLabel.font = Tools.setFontBook(size: 14)
        additionalInfoLabel.textColor = .BBVA500

        infoLineView.backgroundColor = .BBVA300

        routeButton.setTitle(Localizables.promotions.key_how_to_get_text, for: .normal)
        routeButton.titleLabel?.font = Tools.setFontMedium(size: 14)
        routeButton.setTitleColor(.MEDIUMBLUE, for: .normal)
        routeButton.setTitleColor(.COREBLUE, for: .highlighted)
        routeButton.setTitleColor(.COREBLUE, for: .selected)

        webButton.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Promotions.ecommerce, color: .MEDIUMBLUE), width: Float(webButton.frame.size.width), height: Float(webButton.frame.size.height)), for: .normal)
        webButton.tintColor = .MEDIUMBLUE

        emailButton.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Common.email, color: .MEDIUMBLUE), width: Float(emailButton.frame.size.width), height: Float(emailButton.frame.size.height)), for: .normal)
        emailButton.tintColor = .MEDIUMBLUE

        phoneButton.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Common.telephone_bbva_line, color: .MEDIUMBLUE), width: Float(phoneButton.frame.size.width), height: Float(phoneButton.frame.size.height)), for: .normal)
        phoneButton.tintColor = .MEDIUMBLUE
    }

    func configurationMapView() {

        mapView.settings.myLocationButton = false
        mapView.settings.compassButton = false
        mapView.settings.setAllGesturesEnabled(false)
        mapView.delegate = self
    }

    // MARK: Actions Methods

    @IBAction func routeButtonPressed(_ sender: Any) {

        TrackerHelper.sharedInstance().trackPromotionsDetailScreenButtonHowToGet(withCustomerID: trackerCustomerID)

        delegate?.associatedTradeMapViewPressedLaunchMap(self)
    }

    @IBAction func webButtonPressed(_ sender: Any) {

        TrackerHelper.sharedInstance().trackPromotionsDetailScreenButtonWebSite(withCustomerID: trackerCustomerID)

        delegate?.associatedTradeMapViewPressedWebsite(self)
    }

    @IBAction func emailButtonPressed(_ sender: Any) {

        TrackerHelper.sharedInstance().trackPromotionsDetailScreenButtonEmail(withCustomerID: trackerCustomerID)

        delegate?.associatedTradeMapViewPressedSendEmail(self)
    }

    @IBAction func phoneButtonPressed(_ sender: Any) {

        TrackerHelper.sharedInstance().trackPromotionsDetailScreenButtonPhone(withCustomerID: trackerCustomerID)

        delegate?.associatedTradeMapViewPressedCallToPhone(self)
    }

    // MARK: Setup

    func setupAssociatedTradeMapView(withDisplayData displayData: AssociatedTradeMapDisplayData) {

        associatedTradeMapDisplayData = displayData

        if let latitude = displayData.latitude, let longitude = displayData.longitude {

            let target = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
            if CLLocationCoordinate2DIsValid(target), latitude != 0, longitude != 0 {

                mapView.camera = GMSCameraPosition.camera(withTarget: target, zoom: defaultZoom)
            }
        }

        if let promotionTypeColor = displayData.promotionTypeColor {
            iconView.backgroundColor = promotionTypeColor
        }

        setupLabelText(withText: displayData.distance, onLabel: distanceLabel)
        setupLabelText(withText: displayData.address, onLabel: addressLabel)
        setupLabelText(withText: displayData.additionalInfo, onLabel: additionalInfoLabel)

        setupButtonContact(withContact: displayData.web, onButton: webButton)
        setupButtonContact(withContact: displayData.email, onButton: emailButton)

        let phone = displayData.getPhoneContact()
        setupButtonContact(withContact: phone, onButton: phoneButton)
    }

    func setupLabelText(withText text: String?, onLabel label: UILabel) {

        if let text = text, !text.isEmpty {

            label.isHidden = false
            label.text = text

        } else {
            label.isHidden = true
        }
    }

    func setupButtonContact(withContact contact: String?, onButton button: UIButton) {

        if let contact = contact, !contact.isEmpty {
            button.isHidden = false
        } else {
            button.isHidden = true
        }
    }
}

extension AssociatedTradeMapView: GMSMapViewDelegate {

    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {

        if position.zoom == defaultZoom {

            if !moveCameraToMarker {

                moveCameraToMarker = true

                let downwards = GMSCameraUpdate.scrollBy(x: 0, y: (iconView.frame.origin.y + iconView.frame.size.height))
                mapView.animate(with: downwards)
            }
        }
    }
}
