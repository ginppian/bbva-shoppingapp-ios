//
//  AssociateCommercePresenter.swift
//  shoppingapp
//
//  Created by Luis Monroy on 22/01/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class AssociateCommercePresenter {

    weak var view: AssociateCommerceViewProtocol?
}

extension AssociateCommercePresenter: AssociateCommercePresenterProtocol {

    func receiveforShowAssociateCommerceDisplay(withAssociateCommerceDisplay displayAssociateCommerce: AssociateCommerceDisplayData) {
        view?.showDisplayData(withAssociateCommerceDisplay: displayAssociateCommerce)
    }
}
