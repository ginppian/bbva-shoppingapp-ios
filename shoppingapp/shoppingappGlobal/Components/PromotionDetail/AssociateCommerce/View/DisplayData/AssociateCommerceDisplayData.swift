//
//  AssociateCommerceDisplayData.swift
//  shoppingapp
//
//  Created by Luis Monroy on 27/05/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

struct AssociateCommerceDisplayData {

    var commerceAboutName: String?
    var commerceDescription: String?
    var commerceWebPage: String?
    var stores: [StoreDisplay]?
    var storesDescription: String?
    var showStores: Bool = false

    static func generateAssociateCommerceDisplay(fromPromotion promotion: PromotionBO) -> AssociateCommerceDisplayData {

        var associateCommerceDisplay = AssociateCommerceDisplayData()

        if let commerceInfo = promotion.commerceInformation {

            associateCommerceDisplay.commerceAboutName = Localizables.promotions.key_promotions_about_text + " " + commerceInfo.name

            if let commerceDescription = commerceInfo.description {

                associateCommerceDisplay.commerceDescription = commerceDescription
            }

            if let commerceWeb = commerceInfo.web {

                associateCommerceDisplay.commerceWebPage = commerceWeb
            }
        }

        var storesArray = [StoreDisplay]()

        if let storesBO = promotion.stores {

            for storeBO in storesBO {

                let store = StoreDisplay.generateStoreDisplay(fromStore: storeBO)
                storesArray.append(store)
            }
        }

        associateCommerceDisplay.stores = storesArray

        if storesArray.count > 1 {

            associateCommerceDisplay.storesDescription = Localizables.promotions.key_promotions_see_establishment_text + " (\(storesArray.count))"
        }

        var isPhysical = false

        if let usageTypes = promotion.usageTypes {

            for usageType in usageTypes where usageType.usageId == .physical {

                isPhysical = true
                break
            }
        }

        associateCommerceDisplay.showStores = isPhysical && storesArray.count > 1

        return associateCommerceDisplay
    }
}

struct StoreDisplay {

    var storeName: String?
    var address: String?
    var city: String?
    var state: String?
    var country: String?
    var zipCode: String?

    static func generateStoreDisplay(fromStore store: StoreBO) -> StoreDisplay {

        var storeDisplay = StoreDisplay()

        storeDisplay.storeName = store.name

        if let locationBO = store.location {

            storeDisplay.address = locationBO.addressName
            storeDisplay.city = locationBO.city
            storeDisplay.state = locationBO.state
            storeDisplay.zipCode = locationBO.zipCode

            if let countryBO = locationBO.country {

                storeDisplay.country = countryBO.name
            }
        }

        return storeDisplay
    }
}
