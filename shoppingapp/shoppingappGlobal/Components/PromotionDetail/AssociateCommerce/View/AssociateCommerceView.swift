//
//  AssociateCommerceView.swift
//  shoppingapp
//
//  Created by Luis Monroy on 27/05/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol AssociateCommerceViewDelegate: class {
    func associateCommerceShowStoreWebPage(_ associateCommerce: AssociateCommerceView)
    func associateCommerceShowStoreList(_ associateCommerce: AssociateCommerceView)
}

class AssociateCommerceView: XibView {

    @IBOutlet weak var storesAboutLabel: UILabel!
    @IBOutlet weak var storesDescriptionLabel: UILabel!
    @IBOutlet weak var storeWebPageButton: UIButton!
    @IBOutlet weak var totalStoresButton: UIButton!

    @IBOutlet weak var storesDescriptionLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var storesDescriptionLabelBottomSpace: NSLayoutConstraint!
    @IBOutlet weak var storeWebPageButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var storeWebPageButtonBottomSpace: NSLayoutConstraint!
    @IBOutlet weak var totalStoresButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var totalStoresButtonBottomSpace: NSLayoutConstraint!

    var presenter = AssociateCommercePresenter()

    weak var delegate: AssociateCommerceViewDelegate?

    let trackerCustomerID = SessionDataManager.sessionDataInstance().customerID

    override func configureView() {

        super.configureView()
        presenter.view = self

        storesAboutLabel.font = Tools.setFontMedium(size: 14)
        storesAboutLabel.textColor = .BBVA600

        storesDescriptionLabel.font = Tools.setFontBook(size: 14)
        storesDescriptionLabel.textColor = .BBVA600

        storeWebPageButton.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Promotions.ecommerce, color: .MEDIUMBLUE), width: Float(storeWebPageButton.frame.size.width), height: Float(storeWebPageButton.frame.size.height)), for: .normal)
        storeWebPageButton.tintColor = .MEDIUMBLUE

        totalStoresButton.titleLabel?.font = Tools.setFontMedium(size: 14)
        totalStoresButton.setTitleColor(.MEDIUMBLUE, for: .normal)
        totalStoresButton.setTitleColor(.COREBLUE, for: .highlighted)
        totalStoresButton.setTitleColor(.COREBLUE, for: .selected)
    }

    func showAssociateCommerceDisplay(withAssociateCommerceDisplay displayAssociateCommerce: AssociateCommerceDisplayData) {

        presenter.receiveforShowAssociateCommerceDisplay(withAssociateCommerceDisplay: displayAssociateCommerce)
    }

    // MARK: actions

    @IBAction func showStoreWebPagePressed(_ sender: Any) {

        TrackerHelper.sharedInstance().trackPromotionsDetailScreenButtonWebSite(withCustomerID: trackerCustomerID)

        delegate?.associateCommerceShowStoreWebPage(self)
    }
    
    @IBAction func showStoreListPressed(_ sender: Any) {
        
        delegate?.associateCommerceShowStoreList(self)
    }
}

extension AssociateCommerceView: AssociateCommerceViewProtocol {

    func showDisplayData(withAssociateCommerceDisplay displayAssociateCommerce: AssociateCommerceDisplayData) {

        storesAboutLabel.text = displayAssociateCommerce.commerceAboutName

        if let storeDescription = displayAssociateCommerce.commerceDescription {

            storesDescriptionLabel.text = storeDescription
        } else {

            storesDescriptionLabelHeight.constant = 0.0
            storesDescriptionLabelBottomSpace.constant = 0.0
        }

        if displayAssociateCommerce.commerceWebPage == nil {

            storeWebPageButtonHeight.constant = 0.0
            storeWebPageButtonBottomSpace.constant = 0.0
        }

        if displayAssociateCommerce.showStores {
            totalStoresButton.isHidden = false
            totalStoresButton.setTitle(displayAssociateCommerce.storesDescription, for: .normal)
        } else {
            totalStoresButton.isHidden = true
            totalStoresButtonHeight.constant = 0.0
            totalStoresButtonBottomSpace.constant = 0.0
        }
    }
}
