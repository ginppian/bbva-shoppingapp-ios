//
//  AssociateCommerceContract.swift
//  shoppingapp
//
//  Created by Luis Monroy on 22/01/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

protocol AssociateCommerceViewProtocol: ComponentViewProtocol {

    func showDisplayData(withAssociateCommerceDisplay displayAssociateCommerce: AssociateCommerceDisplayData)
}

protocol AssociateCommercePresenterProtocol: ComponentPresenterProtocol {

    func receiveforShowAssociateCommerceDisplay(withAssociateCommerceDisplay displayAssociateCommerce: AssociateCommerceDisplayData)
}
