//
//  BasicDetailView.swift
//  shoppingapp
//
//  Created by Luis Monroy on 29/04/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol BasicDetailViewDelegate: class {
    func basicDetailShowMoreInformation(_ basicDetail: BasicDetailView)
    func basicDetailShowTermsAndConditions(_ basicDetail: BasicDetailView)
}

class BasicDetailView: XibView {

    @IBOutlet weak var promoDetailLabel: UILabel!
    @IBOutlet weak var attentionLabel: UILabel!
    @IBOutlet weak var legalConditionsLabel: UILabel!
    @IBOutlet weak var moreInformationButton: UIButton!
    @IBOutlet weak var termsAndConditionsButton: UIButton!

    @IBOutlet weak var attentionLabelBottomSpace: NSLayoutConstraint!
    @IBOutlet weak var attentionLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var legalConditionsLabelBottomSpace: NSLayoutConstraint!

    @IBOutlet weak var termsAndConditionsButtonTopSpace: NSLayoutConstraint!
    @IBOutlet weak var termsAndConditionsButtonHeight: NSLayoutConstraint!
    @IBOutlet weak var moreInformationButtonBottomSpace: NSLayoutConstraint!
    @IBOutlet weak var moreInformationButtonHeight: NSLayoutConstraint!

    static let spaceSize: CGFloat = 20.0

    var presenter = BasicDetailPresenter()

    weak var delegate: BasicDetailViewDelegate?

    override func configureView() {

        super.configureView()
        presenter.view = self

        promoDetailLabel.font = Tools.setFontBook(size: 14)
        promoDetailLabel.textColor = .BBVA600

        attentionLabel.font = Tools.setFontMedium(size: 14)
        attentionLabel.textColor = .BBVA600

        legalConditionsLabel.font = Tools.setFontBook(size: 14)
        legalConditionsLabel.textColor = .BBVA600

        moreInformationButton.titleLabel?.font = Tools.setFontMedium(size: 14)
        moreInformationButton.setTitleColor(.MEDIUMBLUE, for: .normal)
        moreInformationButton.setTitleColor(.COREBLUE, for: .highlighted)
        moreInformationButton.setTitleColor(.COREBLUE, for: .selected)
        moreInformationButton.setTitle(Localizables.promotions.key_promotions_more_info_text, for: .normal)

        termsAndConditionsButton.titleLabel?.font = Tools.setFontMedium(size: 14)
        termsAndConditionsButton.setTitleColor(.MEDIUMBLUE, for: .normal)
        termsAndConditionsButton.setTitleColor(.COREBLUE, for: .highlighted)
        termsAndConditionsButton.setTitleColor(.COREBLUE, for: .selected)
        termsAndConditionsButton.setTitle(Localizables.promotions.key_promotions_consider_this_info_text, for: .normal)
    }

    func showDisplayItemPromotionDetail(withDisplayItemPromotionDetail displayItemPromotionDetail: DisplayItemPromotionDetail) {

        presenter.receiveforShowDisplayItemPromotionDetail(withDisplayItemPromotionDetail: displayItemPromotionDetail)
    }

    func updateDisplayItemPromotionDetail(withDisplayItemPromotionDetail displayItemPromotionDetail: DisplayItemPromotionDetail) {

        presenter.receiveforUpdateDisplayItemPromotionDetail(withDisplayItemPromotionDetail: displayItemPromotionDetail)
    }

    // MARK: actions

    @IBAction func showMoreInformationPressed(_ sender: Any) {

        delegate?.basicDetailShowMoreInformation(self)
    }

    @IBAction func showTermsAndConditionsPressed(_ sender: Any) {

        delegate?.basicDetailShowTermsAndConditions(self)
    }

}

extension BasicDetailView: BasicDetailViewProtocol {
    func showDisplayData(withDisplayItemPromotionDetail displayItemPromotionDetail: DisplayItemPromotionDetail) {

        promoDetailLabel.text = displayItemPromotionDetail.summaryDescription

        attentionLabelHeight.constant = 0.0
        attentionLabelBottomSpace.constant = 0.0
        legalConditionsLabelBottomSpace.constant = 0.0
        termsAndConditionsButtonHeight.constant = 0.0
        termsAndConditionsButtonTopSpace.constant = 0.0

        self.sizeToFit()
    }

    func updateDisplayData(withDisplayItemPromotionDetail displayItemPromotionDetail: DisplayItemPromotionDetail) {

        promoDetailLabel.text = displayItemPromotionDetail.extendedDescription
        promoDetailLabel.sizeToFit()

        attentionLabelBottomSpace.constant = BasicDetailView.spaceSize
        legalConditionsLabelBottomSpace.constant = BasicDetailView.spaceSize

        attentionLabel.text = Localizables.promotions.key_promotions_consider_text
        attentionLabelHeight.constant = BasicDetailView.spaceSize

        legalConditionsLabel.text = displayItemPromotionDetail.legalConditions
        legalConditionsLabel.sizeToFit()

        if displayItemPromotionDetail.legalConditionsURL != nil {

            termsAndConditionsButton.setTitle(Localizables.promotions.key_promotions_terms_and_conditions_text, for: .normal)
            termsAndConditionsButtonTopSpace.constant = BasicDetailView.spaceSize
            termsAndConditionsButtonHeight.constant = moreInformationButtonHeight.constant

            moreInformationButton.setTitle("", for: .normal)
        } else {

            moreInformationButtonHeight.constant = 0.0
            moreInformationButtonBottomSpace.constant = 0.0
        }

        self.sizeToFit()
    }

}
