//
//  BasicDetailContract.swift
//  shoppingapp
//
//  Created by Luis Monroy on 29/04/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

protocol BasicDetailViewProtocol: ComponentViewProtocol {

    func showDisplayData(withDisplayItemPromotionDetail displayItemPromotionDetail: DisplayItemPromotionDetail)
    func updateDisplayData(withDisplayItemPromotionDetail displayItemPromotionDetail: DisplayItemPromotionDetail)
}

protocol BasicDetailPresenterProtocol: ComponentPresenterProtocol {

    func receiveforShowDisplayItemPromotionDetail(withDisplayItemPromotionDetail displayItemPromotionDetail: DisplayItemPromotionDetail)

    func receiveforUpdateDisplayItemPromotionDetail(withDisplayItemPromotionDetail displayItemPromotionDetail: DisplayItemPromotionDetail)

}
