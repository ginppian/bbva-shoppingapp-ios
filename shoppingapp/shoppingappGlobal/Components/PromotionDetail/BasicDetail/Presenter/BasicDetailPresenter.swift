//
//  BasicDetailPresenter.swift
//  shoppingapp
//
//  Created by Luis Monroy on 29/04/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class  BasicDetailPresenter {

    weak var view: BasicDetailViewProtocol?
}

extension BasicDetailPresenter: BasicDetailPresenterProtocol {

    func receiveforShowDisplayItemPromotionDetail(withDisplayItemPromotionDetail displayItemPromotionDetail: DisplayItemPromotionDetail) {

        view?.showDisplayData(withDisplayItemPromotionDetail: displayItemPromotionDetail)
    }

    func receiveforUpdateDisplayItemPromotionDetail(withDisplayItemPromotionDetail displayItemPromotionDetail: DisplayItemPromotionDetail) {

        view?.updateDisplayData(withDisplayItemPromotionDetail: displayItemPromotionDetail)
    }

}
