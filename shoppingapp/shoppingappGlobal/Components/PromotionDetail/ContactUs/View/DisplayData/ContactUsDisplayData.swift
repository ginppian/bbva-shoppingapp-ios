//
//  ContactUsDisplayData.swift
//  shoppingapp
//
//  Created by Jose Luis Mendoza Espinosa on 30/07/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

struct ContactUsDisplayData {
    
    var description: String = ""
    var showMoreInfo: Bool = false
    
    static func generateContactUsDisplay(fromPromotion promotion: PromotionBO) -> ContactUsDisplayData {
     
        var contactUsDescription = Localizables.promotions.key_any_questions_need_a_quick_response_text
        var showMoreInfo = false
        
        if let contacts = promotion.contactDetails {
            
            for contactDetail in contacts where contactDetail.description != nil && contactDetail.contactType?.id != .email {
                
                showMoreInfo = true
                contactUsDescription = Localizables.promotions.key_didnt_give_promotion_we_guarantee_it_text
                break
            }
        }
        
        return ContactUsDisplayData(description: contactUsDescription, showMoreInfo: showMoreInfo)
    }
}
