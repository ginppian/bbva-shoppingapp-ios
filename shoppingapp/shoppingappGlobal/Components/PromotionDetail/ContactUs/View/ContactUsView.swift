//
//  ContactUsView.swift
//  shoppingapp
//
//  Created by Jose Luis Mendoza Espinosa on 30/07/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol ContactUsViewDelegate: class {
    func contactUsShowGuaranteePressed(_ contactUs: ContactUsView)
    func contactUsShowContactDetailsPressed(_ contactUs: ContactUsView)
}

class ContactUsView: XibView {
    
    @IBOutlet weak var contactImage: UIImageView!
    @IBOutlet weak var contactLabel: UILabel!
    @IBOutlet weak var contactButton: UIButton!
    @IBOutlet weak var moreInfoButton: UIButton!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var contentView: UIView!
    
    var presenter = ContactUsPresenter()
    
    weak var delegate: ContactUsViewDelegate?
    
    override func configureView() {
        
        super.configureView()
        presenter.view = self
        
        contactLabel.font = Tools.setFontBook(size: 14)
        
        moreInfoButton.setTitle(Localizables.promotions.key_more_information_text, for: .normal)
        moreInfoButton.titleLabel?.font = Tools.setFontMedium(size: 14)
        moreInfoButton.setTitleColor(.MEDIUMBLUE, for: .normal)
        moreInfoButton.setTitleColor(.COREBLUE, for: .highlighted)
        moreInfoButton.setTitleColor(.COREBLUE, for: .selected)
        
        contactButton.setTitle(Localizables.promotions.key_contact_us_text, for: .normal)
        contactButton.titleLabel?.font = Tools.setFontMedium(size: 14)
        contactButton.setTitleColor(.MEDIUMBLUE, for: .normal)
        contactButton.setTitleColor(.COREBLUE, for: .highlighted)
        contactButton.setTitleColor(.COREBLUE, for: .selected)
        
        lineView.backgroundColor = .BBVA400
        contentView.backgroundColor = UIColor.WHITELIGHTBLUE.withAlphaComponent(0.5)
        
        self.backgroundColor = .BBVAWHITE
        
    }
    
    func showContactUsDisplay(withContactUsDisplay displayContactUs: ContactUsDisplayData) {
        
        presenter.receiveforShowContactUsDisplay(withContactUsDisplay: displayContactUs)
    }
    
    // MARK: actions
    
    @IBAction func showGuaranteePressed(_ sender: Any) {
        
        delegate?.contactUsShowGuaranteePressed(self)
    }
    
    @IBAction func showContactsPressed(_ sender: Any) {
        
        delegate?.contactUsShowContactDetailsPressed(self)
    }
}

extension ContactUsView: ContactUsViewProtocol {
    
    func showDisplayData(withContactUsDisplay displayContactUs: ContactUsDisplayData) {
    
        contactLabel.text = displayContactUs.description
        
        if !displayContactUs.showMoreInfo {
            
            moreInfoButton.isHidden = true
            separatorView.isHidden = true
        }
    }
}
