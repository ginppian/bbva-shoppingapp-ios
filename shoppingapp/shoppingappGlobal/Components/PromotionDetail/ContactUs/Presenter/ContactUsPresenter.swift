//
//  ContactUsPresenter.swift
//  shoppingapp
//
//  Created by Jose Luis Mendoza Espinosa on 30/07/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class ContactUsPresenter {
    
    weak var view: ContactUsViewProtocol?
}

extension ContactUsPresenter: ContactUsPresenterProtocol {
    
    func receiveforShowContactUsDisplay(withContactUsDisplay displayContactUs: ContactUsDisplayData) {
        view?.showDisplayData(withContactUsDisplay: displayContactUs)
    }
}
