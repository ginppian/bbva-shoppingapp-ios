//
//  ContactUsContract.swift
//  shoppingapp
//
//  Created by Jose Luis Mendoza Espinosa on 30/07/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

protocol ContactUsViewProtocol: ComponentViewProtocol {
    
    func showDisplayData(withContactUsDisplay displayContactUs: ContactUsDisplayData)
}

protocol ContactUsPresenterProtocol: ComponentPresenterProtocol {
    
    func receiveforShowContactUsDisplay(withContactUsDisplay displayContactUs: ContactUsDisplayData)
}
