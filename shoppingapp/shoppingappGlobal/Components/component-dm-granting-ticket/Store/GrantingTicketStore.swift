//
//  GrantingTicketStore.swift
//  shoppingapp
//
//  Created by Marcos on 5/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import CellsNativeComponents
import BBVA_Network

open class GrantingTicketStore: NetworkStore {

    public static let identifier: String = "component-data-manager-granting-ticket-store"

    public static let onGrantingSuccess = ActionSpec<GrantingTicketResponseDTO>(id: "on-granting-success")
    static let onGrantingError = ActionSpec<ErrorBO>(id: "on-granting-error")
    
    // MARK: public vars
    
    var host: String?
    var countryCode: String?
    
    // MARK: private vars
    
    fileprivate var grantingTicketAnonymousUseCase: GrantingTicketAnonymousUseCase?
    fileprivate var worker: NetworkWorker
    
    // MARK: init
    
    convenience init(worker: NetworkWorker, grantingTicketUseCase: GrantingTicketAnonymousUseCase) {

        self.init(worker: worker)
        grantingTicketAnonymousUseCase = grantingTicketUseCase
    }

    public init(worker: NetworkWorker, host: String? = nil) {

        self.worker = worker
        self.host = host

        super.init(GrantingTicketStore.identifier)

        guard let host = host, !host.isEmpty else {
            return
        }
        
        let grantingTicketRepository: GrantingTicketRepository = GrantingTicketRepositoryImpl(baseURL: host, networkWorker: worker)
        grantingTicketAnonymousUseCase = GrantingTicketAnonymousUseCase(grantingTicketRepository: grantingTicketRepository)
    }

    public required init() {

        fatalError("init() has not been implemented")
    }
    
    // MARK: override methods
    
    override open func dispose() {

        grantingTicketAnonymousUseCase?.dispose()
        super.dispose()
    }

    override public func onUseCaseError(error: Error) {

        if let serviceError = error as? ServiceError {

            switch serviceError {
            case .GenericErrorEntity(let errorEntity):
                let errorBVABO = ErrorBO(error: errorEntity)
                fire(actionSpec: GrantingTicketStore.onGrantingError, value: errorBVABO)
            default:
                break
            }
        }
    }
    
    // MARK: Public methods
    
    public func grantingTicketAnonymous(grantingTicket: GrantingTicketDTO) {
        
        let useCaseCallback = UseCase<GrantingTicketResponseDTO, GrantingTicketDTO>.UseCaseCallback<GrantingTicketResponseDTO>(onUseCaseComplete: { result in
            self.fire(actionSpec: GrantingTicketStore.onGrantingSuccess, value: result)
        }, onUseCaseError: onUseCaseError)
        
        grantingTicketAnonymousUseCase?.execute(params: grantingTicket, useCaseCallback: useCaseCallback)
    }
}
