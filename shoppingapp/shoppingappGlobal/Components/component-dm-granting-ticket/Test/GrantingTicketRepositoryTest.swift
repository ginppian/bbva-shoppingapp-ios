//
//  GrantingTicketRepositoryTest.swift
//  shoppingapp
//
//  Created by Marcos on 6/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import BBVA_Network
import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class GrantingTicketRepositoryTests: XCTestCase {

    var sut: GrantingTicketRepository!
    var networkWorkerMock = MockNetworkWorker()

    override func setUp() {
        
        super.setUp()
        networkWorkerMock = MockNetworkWorker()
        sut = GrantingTicketRepositoryImpl(baseURL: "", networkWorker: networkWorkerMock)
    }

    func test_givenAErrorInService_whenCallGrantingTicket_thenCompletionError() {

        let grantingTicketDTO = GrantingTicketDTO(consumerID: "1",
                                                  userID: "1",
                                                  authenticationType: "61",
                                                  password: nil,
                                                  idAuthenticationData: nil,
                                                  backendUserRequestUserID: "",
                                                  accessCode: "1",
                                                  dialogId: "")
        networkWorkerMock.isError = true

        sut.grantingTicketAnonymous(params: grantingTicketDTO, success: { _ in

            XCTFail("Not expected success")

        }, error: { error in
            XCTAssert((error as? ServiceError) != nil)
        })
    }

    func test_givenAErrorInParseBody_whenCallGrantingTicket_thenCompletionError() {

        let grantingTicketDTO = GrantingTicketDTO(consumerID: "1",
                                                  userID: "1",
                                                  authenticationType: "61",
                                                  password: nil,
                                                  idAuthenticationData: nil,
                                                  backendUserRequestUserID: "",
                                                  accessCode: "1",
                                                  dialogId: "")
        networkWorkerMock.isBadBody = true

        sut.grantingTicketAnonymous(params: grantingTicketDTO, success: { _ in

            XCTFail("Not expected success")

        }, error: { error in
            XCTAssert((error as? ServiceError) != nil)
        })
    }

    func test_givenASuccessResponse_whenCallGrantingTicket_thenCompletionSuccess() {

        let grantingTicketDTO = GrantingTicketDTO(consumerID: "1",
                                                  userID: "1",
                                                  authenticationType: "61",
                                                  password: nil,
                                                  idAuthenticationData: nil,
                                                  backendUserRequestUserID: "",
                                                  accessCode: "1",
                                                  dialogId: "")

        sut.grantingTicketAnonymous(params: grantingTicketDTO, success: { response in
            
            XCTAssertNotNil(response)
        }, error: { _ in

            XCTFail("Not expected fail")
        })
    }
}
