//
//  GrantingTicketStoreTest.swift
//  shoppingapp
//
//  Created by Marcos on 6/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import BBVA_Network
import CellsNativeComponents
import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class GrantingTicketStoreTests: XCTestCase {

    private var networkWorkerMock = MockNetworkWorker()
    private var mockGrantingTicketAnonymousUseCase: MockGrantingTicketAnonymousUseCase!
    private var sut: GrantingTicketStore!
    private var publisher = VerifcablePublisher()

    override func setUp() {
        
        super.setUp()

        networkWorkerMock = MockNetworkWorker()
        mockGrantingTicketAnonymousUseCase = MockGrantingTicketAnonymousUseCase(grantingTicketRepository: MockGrantingTicketRepository())

        sut = GrantingTicketStore(worker: networkWorkerMock, grantingTicketUseCase: mockGrantingTicketAnonymousUseCase)
    }

    func test_givenACardDTO_whenCallGrantingTicketAndCompletionSuccess_thenFireOnBlockSuccess() {

        //Given
        let grantingTicketDTO = GrantingTicketDTO(consumerID: "1",
                                                  userID: "1",
                                                  authenticationType: "61",
                                                  password: nil,
                                                  idAuthenticationData: nil,
                                                  backendUserRequestUserID: "",
                                                  accessCode: "1",
                                                  dialogId: "")
        mockGrantingTicketAnonymousUseCase.requestSuccess = true

        //When
        sut.grantingTicketAnonymous(grantingTicket: grantingTicketDTO)

        //Then
        XCTAssertTrue(publisher.verify(viewModelId: GrantingTicketStore.identifier, action: GrantingTicketStore.onGrantingSuccess))
    }

    func test_givenACardDTO_whenCallGrantingTicketAndCompletionError_thenIsCalledExecute() {

        //Given
        let grantingTicketDTO = GrantingTicketDTO(consumerID: "1",
                                                  userID: "1",
                                                  authenticationType: "61",
                                                  password: nil,
                                                  idAuthenticationData: nil,
                                                  backendUserRequestUserID: "",
                                                  accessCode: "1",
                                                  dialogId: "")

        //When
        sut.grantingTicketAnonymous(grantingTicket: grantingTicketDTO)

        //Then
        XCTAssertTrue(mockGrantingTicketAnonymousUseCase.isCalledExecute)

    }

    func test_givenACardDTO_whenCallGrantingTicketAndCompletionError_thenCancelCardDTOSameInBlockCardUse() {

        //Given
        let grantingTicketDTO = GrantingTicketDTO(consumerID: "1",
                                                  userID: "1",
                                                  authenticationType: "61",
                                                  password: nil,
                                                  idAuthenticationData: nil,
                                                  backendUserRequestUserID: "",
                                                  accessCode: "1",
                                                  dialogId: "")

        //When
        sut.grantingTicketAnonymous(grantingTicket: grantingTicketDTO)

        //Then
        XCTAssertTrue(mockGrantingTicketAnonymousUseCase.grantingTicketDTO === grantingTicketDTO)

    }
}
