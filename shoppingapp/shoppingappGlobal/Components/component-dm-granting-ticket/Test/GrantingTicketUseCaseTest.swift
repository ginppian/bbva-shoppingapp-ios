//
//  GrantingTicketUseCaseTest.swift
//  shoppingapp
//
//  Created by Marcos on 6/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import BBVA_Network
import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class GrantingTicketUseCaseTests: XCTestCase {

    var sut: GrantingTicketAnonymousUseCase!
    var mockGrantingTicketRepository = MockGrantingTicketRepository()

    override func setUp() {
        
        super.setUp()
        sut = GrantingTicketAnonymousUseCase(grantingTicketRepository: mockGrantingTicketRepository)
    }

    func test_givenAResponse_whenCallGrantingTicketUseCase_thenIsCalledGrantingTicket() {

        let response = Response()
        response.body = "{}"
        let grantingTicketDTO = GrantingTicketDTO(consumerID: "1",
                                                  userID: "1",
                                                  authenticationType: "61",
                                                  password: nil,
                                                  idAuthenticationData: nil,
                                                  backendUserRequestUserID: "",
                                                  accessCode: "1",
                                                  dialogId: "")
        
        //when
        sut.call(params: grantingTicketDTO, success: { _ in
            //then
            XCTAssertTrue(self.mockGrantingTicketRepository.isCalledGrantingTicketAnonymous)

        }, error: { _ in

            XCTFail("Not expected error")
        })
    }

    func test_givenAError_whenCallGrantingTicketUseCaseWithWrongParameters_thenIsCompletionError() {

        let response = Response()
        response.body = "{}"

        //when
        sut.call(params: nil, success: { _ in

            XCTFail("Not expected success")

        }, error: { wrongParameters in
            
            XCTAssert((wrongParameters as? ServiceError) != nil)
        })
    }

}
