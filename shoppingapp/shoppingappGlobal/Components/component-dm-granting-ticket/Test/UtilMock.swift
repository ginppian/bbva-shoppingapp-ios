//
//  UtilMock.swift
//  shoppingapp
//
//  Created by Marcos on 6/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import BBVA_Network
import CellsNativeComponents

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class MockNetworkWorker: NetworkWorker {

    var response: Response?
    var isError = false
    var isBadBody = false

    override func execute(request: NetworkRequest, completion: @escaping (Response) -> Void) -> URLSessionDataTask? {
        
        if isError == true {
            response = Response()
            response?.error = ServiceError.Other(NSError())
            completion(response!)
        } else if isBadBody == true {
            response = Response()
            response?.body = "invalid_Json."
            completion(response!)
        } else {
            response = Response()
            response?.body = "{}"
            completion(response!)
        }
        return URLSessionDataTask()
    }
}

class MockGrantingTicketRepository: GrantingTicketRepository {

    var isCalledGrantingTicketAnonymous = false
    
    func grantingTicketAnonymous(params: GrantingTicketDTO, success: @escaping (_ result: GrantingTicketResponseDTO) -> Void, error: @escaping (_ error: Error) -> Void) {

        isCalledGrantingTicketAnonymous = true
    }
}

class MockGrantingTicketAnonymousUseCase: GrantingTicketAnonymousUseCase {

    var requestSuccess = false
    var isCalledExecute = false
    
    var grantingTicketDTO = GrantingTicketDTO(consumerID: "1",
                                              userID: "1",
                                              authenticationType: "61",
                                              password: nil,
                                              idAuthenticationData: nil,
                                              backendUserRequestUserID: "",
                                              accessCode: "1",
                                              dialogId: "")
    
    override func call(params: GrantingTicketDTO?, success: @escaping (GrantingTicketResponseDTO) -> Void, error: @escaping (Error) -> Void) {

        if requestSuccess == true {
            success(GrantingTicketResponseDTO())
        } else {
            error(ServiceError.Other(NSError()))
        }
    }

    override func execute(params: GrantingTicketDTO?, useCaseCallback: UseCase<GrantingTicketResponseDTO, GrantingTicketDTO>.UseCaseCallback<GrantingTicketResponseDTO>) {

        isCalledExecute = true
        grantingTicketDTO = params!
        call(params: params, success: useCaseCallback.onUseCaseComplete!, error: useCaseCallback.onUseCaseError!)
    }
}
