//
//  GrantingTicketDTO.swift
//  shoppingapp
//
//  Created by Marcos on 5/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

open class GrantingTicketDTO: CellsDTO {
    
    var consumerID: String
    var userID: String
    var authenticationType: String
    var password: String?
    var idAuthenticationData: String?
    var backendUserRequestUserID: String
    var accessCode: String
    var dialogId: String
    
    init(consumerID: String, userID: String, authenticationType: String, password: String?, idAuthenticationData: String?, backendUserRequestUserID: String, accessCode: String, dialogId: String) {

        self.consumerID = consumerID
        self.userID = userID
        self.authenticationType = authenticationType
        self.password = password
        self.idAuthenticationData = idAuthenticationData
        self.backendUserRequestUserID = backendUserRequestUserID
        self.accessCode = accessCode
        self.dialogId = dialogId
    }
}
