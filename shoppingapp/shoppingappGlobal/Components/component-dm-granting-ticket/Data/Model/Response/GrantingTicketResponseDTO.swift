//
//  GrantingTicketResponseDTO.swift
//  shoppingapp
//
//  Created by Marcos on 5/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import HandyJSON

open class GrantingTicketResponseDTO: CellsDTO, HandyJSON {

    var authenticationResponse: AutenticationResponseEntityDTO?
    var backendUserResponse: BackendUserResponseDTO?

    public required init() {}
}

struct AutenticationResponseEntityDTO: HandyJSON {

    var authenticationState: String?
    var authenticationData: [Any]?

    init() {}
}

struct BackendUserResponseDTO: HandyJSON {
    
    var userType: String?
    var clientId: String?
    var clientStatus: String?
    var accountingTerminal: String?
    var multistepProcessId: String?

    init() {}
}
