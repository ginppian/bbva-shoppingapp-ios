//
//  GrantingTicketRepository.swift
//  shoppingapp
//
//  Created by Marcos on 5/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

public protocol GrantingTicketRepository {

    func grantingTicketAnonymous(params: GrantingTicketDTO, success: @escaping (_ result: GrantingTicketResponseDTO) -> Void, error: @escaping (_ error: Error) -> Void)
}
