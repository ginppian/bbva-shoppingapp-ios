//
//  GrantingTicketRepositoryImpl.swift
//  shoppingapp
//
//  Created by Marcos on 5/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import BBVA_Network
import HandyJSON

class GrantingTicketRepositoryImpl: BaseRepositoryImpl, GrantingTicketRepository {

    func grantingTicketAnonymous(params: GrantingTicketDTO, success: @escaping (GrantingTicketResponseDTO) -> Void, error: @escaping (Error) -> Void) {
        
        var authenticationData: [[String: Any]] = []
        
        if let password = params.password {
            authenticationData.append(["authenticationData": password])
        }
        
        if let idAuthenticationData = params.idAuthenticationData {
            authenticationData.append(["idAuthenticationData": idAuthenticationData])
        }
        
        let body = ["authentication": ["consumerID": params.consumerID, "userID": params.userID, "authenticationType": params.authenticationType, "authenticationData": authenticationData], "backendUserRequest": ["userId": params.backendUserRequestUserID, "accessCode": params.accessCode, "dialogId": params.dialogId]]
        
        let request = createRequest(baseURL, Headers.BASIC_HEADER, body, .useProtocolCachePolicy, 30, NetworkMethod.POST)

        _ = networkWorker.execute(request: request) { (response: Response) in

            if response.error == nil {
                
                if let dataJson = response.body {
                    
                    if let grantingTicketResponseDTO = GrantingTicketResponseDTO.deserialize(from: dataJson) {
                        
                        success(grantingTicketResponseDTO)
                    } else {
                        
                        error(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
                    }
                } else {
                    
                    error(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
                }
                
            } else {
                
                if let serviceError = self.checkShowError(response: response, security: false) {
                    error(serviceError)
                }
            }
        }
    }
}
