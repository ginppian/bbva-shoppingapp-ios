//
//  GrantingTicketAnonymousUseCase.swift
//  shoppingapp
//
//  Created by Marcos on 5/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeComponents

class GrantingTicketAnonymousUseCase: UseCaseAsync<GrantingTicketResponseDTO, GrantingTicketDTO> {

    private let grantingTicketRepository: GrantingTicketRepository

    init(grantingTicketRepository: GrantingTicketRepository) {

        self.grantingTicketRepository = grantingTicketRepository
        super.init()
    }

    override func call(params: GrantingTicketDTO?, success: @escaping (_ result: GrantingTicketResponseDTO) -> Void, error: @escaping (_ error: Error) -> Void) {

        if let params = params {
            grantingTicketRepository.grantingTicketAnonymous(params: params, success: success, error: error)
        } else {
            DLog(message: "Error no params")
            error(ServiceError.GenericErrorEntity(error: ErrorEntity(message: "Error no params")))
        }
    }
}
