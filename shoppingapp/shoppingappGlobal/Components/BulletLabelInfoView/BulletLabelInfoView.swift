//
//  BulletLabelInfoView.swift
//  shoppingapp
//
//  Created by Magdali Grajales on 11/01/19.
//  Copyright © 2019 BBVA. All rights reserved.
//

import UIKit

class BulletLabelInfoView: XibView {
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var bulletImageView: UIImageView!
    @IBOutlet weak var labelText: UILabel!

    override func configureView() {
        super.configureView()
        
        labelText.font = Tools.setFontBook(size: 14)
        labelText.textColor = .BBVA600
    }
    
    func configureWithTitle(_ text: String, bulletImageName: String) {
        labelText.text = text
        let bulletImage = SVGCache.image(forSVGNamed: bulletImageName, color: .MEDIUMBLUE)
        bulletImageView.image = bulletImage
    }

}
