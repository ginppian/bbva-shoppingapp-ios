//
//  CardListModel.swift
//  shoppingapp
//
//  Created by Marcos on 21/3/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol CardListModelProtocol {
    var cardBO: CardsBO { get }
    var currencyCode: String { get }
    var cardTypesToShow: [PhysicalSupportType] { get }
    var showPoints: Bool { get }
}

struct CardListModel: CardListModelProtocol {
    let cardBO: CardsBO
    let currencyCode: String
    let cardTypesToShow: [PhysicalSupportType]
    let showPoints: Bool
}

struct CardModel {
    let cardBO: CardBO
}
