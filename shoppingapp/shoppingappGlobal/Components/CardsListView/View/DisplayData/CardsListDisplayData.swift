//
//  CardsListDisplayData.swift
//  shoppingapp
//
//  Created by Marcos on 14/3/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

struct CardsListDisplayData {
    let cardsDisplayData: [CardDisplayDataProtocol]

    init(cardsBO: CardsBO, mainCurrencyCode: String, cardTypesToShow: [PhysicalSupportType], showPoints: Bool) {
        var cardDisplays = [CardDisplayDataProtocol]()
        let normalPlasticCards = cardsBO.cards.filter { $0.isNormalPlastic() }
        for cardBO in normalPlasticCards {
            var firstAssociatedCardSetted = false
            for type in cardTypesToShow {
                if type == .normalPlastic {
                    //Normal Cards big Cell
                    cardDisplays.append(BigCardDisplayData(cardBO: cardBO, mainCurrencyCode: mainCurrencyCode, physicalSupportType: type, showPoints: showPoints))

                } else {
                    //Other Cards small Cell
                    let associatedCard = cardBO.associatedCard(fronCardList: cardsBO.cards, type: type)

                    if let associatedCard = associatedCard {

                        if !firstAssociatedCardSetted {
                            // Add show two decorative lines if is the first associated card
                            firstAssociatedCardSetted = true
                            cardDisplays.append(SmallCardDisplayData(cardBO: associatedCard, mainCurrencyCode: mainCurrencyCode, physicalSupportType: type, showLeftLines: true))

                        } else {
                            cardDisplays.append(SmallCardDisplayData(cardBO: associatedCard, mainCurrencyCode: mainCurrencyCode, physicalSupportType: type))
                        }
                    }
                }
            }
            if firstAssociatedCardSetted {
                // Add some extra space below if is the last associated card
                var lastAssociatedDisplay = cardDisplays.last
                lastAssociatedDisplay?.extraButtonSpace = 10
                cardDisplays.remove(at: cardDisplays.count - 1)
                cardDisplays.append(lastAssociatedDisplay!)
            }
        }
        cardsDisplayData = cardDisplays
    }
}

protocol CardDisplayDataProtocol {

    var cardBO: CardBO { get }
    var cardId: String { get }
    var title: String { get }
    var state: String? { get }
    var image: UIImage? { get }
    var stateImage: String? { get }
    var amount: NSAttributedString { get }
    var pan4: String { get }
    var stateLabelColor: UIColor { get }
    var points: NSAttributedString? { get }

    var cellIdentifier: String { get }
    var showLeftLines: Bool { get }
    var extraButtonSpace: CGFloat { get set }
}

struct BigCardDisplayData: CardDisplayDataProtocol {

    var cardBO: CardBO
    let cardId: String
    let title: String
    let image: UIImage?
    let state: String?
    let stateImage: String?
    let stateLabelColor: UIColor
    let amount: NSAttributedString
    let pan4: String
    let points: NSAttributedString?

    let cellIdentifier = BigCardsListTableViewCell.identifier
    let showLeftLines = false
    var extraButtonSpace: CGFloat = 0

    init(cardBO: CardBO, mainCurrencyCode: String, physicalSupportType: PhysicalSupportType, showPoints: Bool) {

        self.cardBO = cardBO
        cardId = cardBO.cardId
        title = BigCardDisplayData.getTitle(cardBO: cardBO)
        image = BigCardDisplayData.getImage(cardBO: cardBO)

        let stateInfo = BigCardDisplayData.getStatus(cardBO: cardBO, physicalSupportType: physicalSupportType)
        state = stateInfo.state
        stateImage = stateInfo.stateImage
        stateLabelColor = stateInfo.stateLabelColor

        pan4 = String(cardBO.number.suffix(4))

        amount = BigCardDisplayData.getAmount(cardBO: cardBO, mainCurrencyCode: mainCurrencyCode)

        if showPoints {
            points = BigCardDisplayData.getPoints(cardBO: cardBO)
        } else {
            points = nil
        }
    }
}

struct SmallCardDisplayData: CardDisplayDataProtocol {

    var cardBO: CardBO
    let cardId: String
    let title: String
    let image: UIImage?
    let state: String?
    let stateImage: String?
    let stateLabelColor: UIColor
    let amount: NSAttributedString
    let pan4: String
    let points: NSAttributedString?

    let cellIdentifier = SmallCardsListTableViewCell.identifier
    let showLeftLines: Bool
    var extraButtonSpace: CGFloat

    init(cardBO: CardBO, mainCurrencyCode: String, physicalSupportType: PhysicalSupportType, showLeftLines: Bool = false, extraButtonSpace: CGFloat = 0) {

        self.cardBO = cardBO
        self.showLeftLines = showLeftLines
        self.extraButtonSpace = extraButtonSpace

        cardId = cardBO.cardId
        title = SmallCardDisplayData.getTitle(cardBO: cardBO)
        image = SmallCardDisplayData.getImage(cardBO: cardBO)

        let stateInfo = SmallCardDisplayData.getStatus(cardBO: cardBO, physicalSupportType: physicalSupportType)
        state = stateInfo.state
        stateImage = stateInfo.stateImage
        stateLabelColor = stateInfo.stateLabelColor

        pan4 = String(cardBO.number.suffix(4))

        amount = SmallCardDisplayData.getAmount(cardBO: cardBO, mainCurrencyCode: mainCurrencyCode)

        points = nil
    }
}

extension CardDisplayDataProtocol {

    static func getTitle(cardBO: CardBO) -> String {
        return cardBO.cardAlias()
    }

    static func getStatus(cardBO: CardBO, physicalSupportType: PhysicalSupportType) -> (state: String?, stateImage: String?, stateLabelColor: UIColor) {

        var state: String?
        var stateImage: String?
        var stateLabelColor: UIColor
        
        if cardBO.isBlocked() || cardBO.isCanceled() || cardBO.isUnknown() {
            
            state = Localizables.cards.key_card_status_blocked_text
            stateImage = physicalSupportType == .normalPlastic ? ConstantsImages.Card.card_bloqued_state : ConstantsImages.Card.card_bloqued_state_small
            stateLabelColor = .BBVACORAL
            
        } else if cardBO.isPendingEmbossing() || cardBO.isPendingDelivery() {
            
            state = Localizables.cards.key_card_status_delivery_text
            stateImage = physicalSupportType == .normalPlastic ? ConstantsImages.Card.card_delivery_state : ConstantsImages.Card.card_delivery_state_small
            stateLabelColor = .DARKAQUA
            
        } else if cardBO.isInoperative() {

            state = Localizables.cards.key_card_status_pending_activation_text
            stateImage = physicalSupportType == .normalPlastic ? ConstantsImages.Card.card_inoperative_state : ConstantsImages.Card.card_inoperative_state_small
            stateLabelColor = .NAVY

        } else if cardBO.isCardOperative() && !cardBO.isCardOn() {

            state = Localizables.cards.key_card_status_off_text
            stateImage = physicalSupportType == .normalPlastic ? ConstantsImages.Card.card_off_state : ConstantsImages.Card.card_off_state_small
            stateLabelColor = .BBVA500

        } else if cardBO.isPartialOff() {

            state = Localizables.cards.key_card_status_partial_off_text
            stateImage = physicalSupportType == .normalPlastic ? ConstantsImages.Card.card_off_state : ConstantsImages.Card.card_off_state_small
            stateLabelColor = .BBVA500

        } else {

            state = nil
            stateImage = nil
            stateLabelColor = .BBVA500
        }

        return (state, stateImage, stateLabelColor)
    }

    static func getImage(cardBO: CardBO) -> UIImage? {
        var image: UIImage?

        if let imageSaved = cardBO.imageSaved {
            image = imageSaved
        } else {
            if cardBO.isSticker() {
                image = UIImage(named: ConstantsImages.Card.blank_sticker)
            } else {
                image = UIImage(named: ConstantsImages.Card.blank_card)
            }
        }
        return image
    }

    static func getAmount(cardBO: CardBO, mainCurrencyCode: String) -> NSAttributedString {

        var amount: NSAttributedString = NSAttributedString(string: "-")
        let currencyBalanceToShow = cardBO.currentBalanceToShow(mainCurrencyCode: mainCurrencyCode)

        if let currencyBalanceToShow = currencyBalanceToShow {

            var isNegative: Bool = false

            if currencyBalanceToShow.amount.compare(NSDecimalNumber(value: 0)) == ComparisonResult.orderedAscending {
                isNegative = true
            }

            let formatter = (cardBO.currencyMajor != nil) ? AmountFormatter(codeCurrency: cardBO.currencyMajor!) : AmountFormatter(codeCurrency: mainCurrencyCode)

            amount = formatter.attributtedText(forAmount: currencyBalanceToShow.amount, isNegative: isNegative, bigFontSize: 20, smallFontSize: 14)
        }

        return amount
    }

    static func getPoints(cardBO: CardBO) -> NSAttributedString? {

        var pointAttributtedText: NSMutableAttributedString?

        if cardBO.isCreditCard() {

            let rewards = cardBO.rewards.first(where: { $0.rewardId == RewardBO.bancomer_points })

            if let rewards = rewards, let nonMonetaryValue = rewards.nonMonetaryValue {

                let numberFormatter = NumberFormatter()
                numberFormatter.numberStyle = .decimal
                numberFormatter.locale = Locale.current

                let pointsValue = numberFormatter.string(from: NSNumber(value: nonMonetaryValue))

                let pointText = pointsValue! + " " + Localizables.points.key_points_card_list_text

                pointAttributtedText = NSMutableAttributedString(string: pointText)
                pointAttributtedText?.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 14), range: (pointText as NSString).range(of: pointsValue!))

                pointAttributtedText?.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 14), range: (pointText as NSString).range(of: Localizables.points.key_points_card_list_text))

            }
        }
        return pointAttributtedText
    }

}
