//
//  CardsListTableViewCell.swift
//  shoppingapp
//
//  Created by Marcos on 14/3/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit

protocol CardsListTableViewCellDelegate: class {
    func cardsListTableViewCellImagePressed(_ cardsListTableViewCell: CardsListTableViewCell)
}

class CardsListTableViewCell: UITableViewCell {

    weak var delegate: CardsListTableViewCellDelegate?

    let touchOffset: CGFloat = 10.0

    var touchAlpha: CGFloat = 0.7
    var endAlpha: CGFloat = 1.0

    var index: Int?

    @IBOutlet weak var cardImageView: UIImageView!
    
    static func cellFor(nibName: String) -> CardsListTableViewCell? {
             
        return Bundle.main.loadNibNamed(nibName, owner: nil, options: nil)?.first as? CardsListTableViewCell
    }

    func configure(withDisplayData displayData: CardDisplayDataProtocol) {}

    fileprivate func frameTouch() -> CGRect? {
        return CGRect(x: cardImageView.frame.origin.x - touchOffset, y: cardImageView.frame.origin.y - touchOffset, width: cardImageView.frame.size.width + touchOffset * 2, height: cardImageView.frame.size.height + touchOffset * 2)
    }

    fileprivate func manageTouch(_ touch: UITouch, with alpha: CGFloat) {

        let position = touch.location(in: self)
        let frameTouch = self.frameTouch()

        if let frameTouch = frameTouch, frameTouch.contains(position) {
            cardImageView.alpha = alpha
        }

    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {

        if let touch = touches.first {

            manageTouch(touch, with: touchAlpha)

        }

    }

    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {

        cardImageView.alpha = endAlpha

    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {

            manageTouch(touch, with: endAlpha)

            let position = touch.location(in: self)
            let frameTouch = self.frameTouch()

            if let frameTouch = frameTouch, frameTouch.contains(position) {
                delegate?.cardsListTableViewCellImagePressed(self)
            }

        }
    }

}
