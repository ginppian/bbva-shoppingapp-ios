//
//  SmallCardsListTableViewCell.swift
//  shoppingapp
//
//  Created by Marcos on 22/3/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit

class SmallCardsListTableViewCell: CardsListTableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var stateLabel: UILabel!

    @IBOutlet weak var pointPanImageView: UIImageView!
    @IBOutlet weak var stateImageView: UIImageView!

    @IBOutlet weak var horizontalLine: UIView!
    @IBOutlet weak var verticalLine: UIView!
    @IBOutlet weak var panLabel: UILabel!

    static let identifier = "SmallCardsListTableViewCell"
    static let height: CGFloat = 80

    override func awakeFromNib() {
        super.awakeFromNib()
        configureView()

    }

    private func configureView() {

        titleLabel.font = Tools.setFontBook(size: 16)
        titleLabel.textColor = .MEDIUMBLUE

        stateLabel.font = Tools.setFontBookItalic(size: 14)
        stateLabel.textColor = .BBVA500

        panLabel.font = Tools.setFontBookItalic(size: 14)
        panLabel.textColor = .BBVA500

        pointPanImageView.backgroundColor = .BBVA500
        pointPanImageView.layer.cornerRadius = 2

        horizontalLine.backgroundColor = .BBVA400
        verticalLine.backgroundColor = .BBVA400

    }

    override func configure(withDisplayData displayData: CardDisplayDataProtocol) {
        titleLabel.text = displayData.title
        stateLabel.text = displayData.state
        stateLabel.textColor = displayData.stateLabelColor
        panLabel.text = displayData.pan4
        cardImageView.image = displayData.image
        if let stateImage = displayData.stateImage {
            stateImageView.isHidden = false
            stateImageView.image = UIImage(named: stateImage)
            endAlpha = 0.5
            cardImageView.alpha = endAlpha
        } else {
            stateImageView.isHidden = true
            stateImageView.image = nil
            cardImageView.alpha = endAlpha
        }

        if displayData.showLeftLines {
            horizontalLine.isHidden = false
            verticalLine.isHidden = false
        } else {
            horizontalLine.isHidden = true
            verticalLine.isHidden = true
        }

        let urlImage = displayData.cardBO.imageFront ?? ""

        cardImageView.getCardImage(withUrl: urlImage) { newImage, _ in

            self.cardImageView.image = newImage
        }
    }

}
