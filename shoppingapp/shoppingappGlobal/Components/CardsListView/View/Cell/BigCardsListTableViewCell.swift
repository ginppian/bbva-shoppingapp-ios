//
//  BigCardsListTableViewCell.swift
//  shoppingapp
//
//  Created by Marcos on 22/3/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit

class BigCardsListTableViewCell: CardsListTableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var stateLabel: UILabel!

    @IBOutlet weak var pointPanImageView: UIImageView!
    @IBOutlet weak var stateImageView: UIImageView!

    @IBOutlet weak var divisorView: UIView!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var panLabel: UILabel!
    @IBOutlet weak var pointsLabel: UILabel!

    @IBOutlet weak var amountToPanVerticalDistanceConstraint: NSLayoutConstraint!
    @IBOutlet weak var pointsButtonConstraint: NSLayoutConstraint!
    @IBOutlet weak var cardToStateVerticalSpaceConstraint: NSLayoutConstraint!
    @IBOutlet weak var cardToTitelVerticalSpaceConstraint: NSLayoutConstraint!

    static let identifier = "BigCardsListTableViewCell"
    static let height: CGFloat = 215

    override func awakeFromNib() {
        super.awakeFromNib()
        configureView()

    }

    private func configureView() {

        titleLabel.font = Tools.setFontBook(size: 16)
        titleLabel.textColor = .MEDIUMBLUE

        stateLabel.font = Tools.setFontBookItalic(size: 14)
        stateLabel.textColor = .BBVA500

        panLabel.font = Tools.setFontBookItalic(size: 14)
        panLabel.textColor = .BBVA500

        pointPanImageView.backgroundColor = .BBVA500
        pointPanImageView.layer.cornerRadius = 2

        divisorView.backgroundColor = .BBVA200

        pointsLabel.textColor = .BBVA500

    }

    override func configure(withDisplayData displayData: CardDisplayDataProtocol) {
        titleLabel.text = displayData.title
        stateLabel.textColor = displayData.stateLabelColor
        amountLabel.attributedText = displayData.amount
        panLabel.text = displayData.pan4
        cardImageView.image = displayData.image

        if let stateImage = displayData.stateImage {
            stateImageView.isHidden = false
            stateImageView.image = UIImage(named: stateImage)
            endAlpha = 0.5
            cardImageView.alpha = endAlpha
        } else {
            stateImageView.isHidden = true
            stateImageView.image = nil
            endAlpha = 1.0
            cardImageView.alpha = endAlpha
        }
        if let points = displayData.points {
            pointsLabel.attributedText = points
            pointsLabel.isHidden = false
            amountToPanVerticalDistanceConstraint.priority = UILayoutPriority(rawValue: 250)
            pointsButtonConstraint.priority = UILayoutPriority(rawValue: 900)
        } else {
            pointsLabel.text = ""
            pointsLabel.isHidden = true
            amountToPanVerticalDistanceConstraint.priority = UILayoutPriority(rawValue: 900)
            pointsButtonConstraint.priority = UILayoutPriority(rawValue: 250)
        }
        if let state = displayData.state {
            stateLabel.text = state
            cardToStateVerticalSpaceConstraint.priority = UILayoutPriority(rawValue: 900)
            cardToTitelVerticalSpaceConstraint.priority = UILayoutPriority(rawValue: 250)
        } else {
            stateLabel.text = ""
            cardToStateVerticalSpaceConstraint.priority = UILayoutPriority(rawValue: 250)
            cardToTitelVerticalSpaceConstraint.priority = UILayoutPriority(rawValue: 900)
        }

        let urlImage = displayData.cardBO.imageFront ?? ""

        cardImageView.getCardImage(withUrl: urlImage) { newImage, _ in

            self.cardImageView.image = newImage
        }
    }

}
