//
//  CardsListView.swift
//  shoppingapp
//
//  Created by Marcos on 14/3/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit

protocol CardsListViewDelegate: class {
    func cardsListViewImagePressed(_ cardsListView: CardsListView, imagePressed: UIImageView)
    func cardsListViewWillDisplay(_ cardsListView: CardsListView, estimatedHeight: CGFloat)
    func cardsListViewLoaded(_ cardsListView: CardsListView, height: CGFloat)
}

class CardsListView: XibView {

    weak var delegate: CardsListViewDelegate?

    // MARK: Outlets
    @IBOutlet weak var tableView: UITableView!

    let presenter = CardsListPresenter()

    var displayCards: CardsListDisplayData? {
        didSet {
            tableView.reloadData()
        }
    }

    fileprivate var preferredContentSize: CGSize {
        tableView.setNeedsLayout()
        tableView.layoutIfNeeded()
        return tableView.contentSize
    }

    override func configureView() {

        super.configureView()

        configureTableView()

        presenter.view = self

    }

    private func configureTableView() {

        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = BigCardsListTableViewCell.height
        tableView.isScrollEnabled = false

        tableView.dataSource = self
        tableView.delegate = self

        layer.cornerRadius = 1
        tableView.layer.cornerRadius = 1

    }
    
    private func estimatedHeight(cardsDisplayData: [CardDisplayDataProtocol]) -> CGFloat {
        
        var estimatedHeight: CGFloat = 0
        
        for cardDisplay in cardsDisplayData {
            if cardDisplay.cellIdentifier == SmallCardsListTableViewCell.identifier {
                estimatedHeight += SmallCardsListTableViewCell.height + cardDisplay.extraButtonSpace
            } else {
                estimatedHeight += BigCardsListTableViewCell.height
            }
        }
        
        return estimatedHeight
    }

}

extension CardsListView: CardsListViewProtocol {
    func showDisplayCards(displayCards: CardsListDisplayData) {
        
        delegate?.cardsListViewWillDisplay(self, estimatedHeight: estimatedHeight(cardsDisplayData: displayCards.cardsDisplayData))

        self.displayCards = displayCards
    }
}

extension CardsListView: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return displayCards?.cardsDisplayData.count ?? 0
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        if let cardDisplayItem = displayCards?.cardsDisplayData[indexPath.row] {
            if cardDisplayItem.cellIdentifier == SmallCardsListTableViewCell.identifier {
                return SmallCardsListTableViewCell.height + cardDisplayItem.extraButtonSpace
            } else {
                return UITableViewAutomaticDimension
            }
        }

        return UITableViewAutomaticDimension
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if let cardDisplayItem = displayCards?.cardsDisplayData[indexPath.row], let cell = CardsListTableViewCell.cellFor(nibName: cardDisplayItem.cellIdentifier) {

            cell.delegate = self

            cell.index = indexPath.row

            cell.configure(withDisplayData: cardDisplayItem)

            return cell
        }

        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if indexPath.row == tableView.indexPathsForVisibleRows?.last?.row {
            delegate?.cardsListViewLoaded(self, height: preferredContentSize.height)
        }
    }
}

extension CardsListView: CardsListTableViewCellDelegate {
    func cardsListTableViewCellImagePressed(_ cardsListTableViewCell: CardsListTableViewCell) {

        if let indexPath = tableView.indexPath(for: cardsListTableViewCell), let displayData = displayCards?.cardsDisplayData[indexPath.row] {
            delegate?.cardsListViewImagePressed(self, imagePressed: cardsListTableViewCell.cardImageView)
            presenter.cardPressed(withDisplayData: displayData)
        }

    }
}
