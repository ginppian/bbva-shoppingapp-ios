//
//  CardsListContract.swift
//  shoppingapp
//
//  Created by Marcos on 14/3/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol CardsListViewProtocol: ComponentViewProtocol {
    func showDisplayCards(displayCards: CardsListDisplayData)
}

protocol CardsListPresenterProtocol: ComponentPresenterProtocol {

    func setModel(model: CardListModelProtocol)
    func cardPressed(withDisplayData displayData: CardDisplayDataProtocol)

}
