//
//  CardsListPresenter.swift
//  shoppingapp
//
//  Created by Marcos on 14/3/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol CardsListPresenterDelegate: class {
    func cardsListPresenter(_ cardsListPresenter: CardsListPresenter, didSelectCardId cardId: String)
}

class CardsListPresenter {

    weak var view: CardsListViewProtocol?
    weak var delegate: CardsListPresenterDelegate?

}

extension CardsListPresenter: CardsListPresenterProtocol {

    func setModel(model: CardListModelProtocol) {

        let display = CardsListDisplayData(cardsBO: model.cardBO, mainCurrencyCode: model.currencyCode, cardTypesToShow: model.cardTypesToShow, showPoints: model.showPoints)
        view?.showDisplayCards(displayCards: display)

    }

     func cardPressed(withDisplayData displayData: CardDisplayDataProtocol) {
        delegate?.cardsListPresenter(self, didSelectCardId: displayData.cardId)
    }
}
