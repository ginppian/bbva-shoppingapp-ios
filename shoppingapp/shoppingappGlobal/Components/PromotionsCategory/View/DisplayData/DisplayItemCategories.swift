//
//  DisplayItemsCards.swift
//  shoppingapp
//
//  Created by ruben.fernandez on 27/9/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation

class DisplayItemCategories {

    var items: [DisplayItemCategory]?
    var showSelectFavoriteCategories = false
    var showAllCategories = false

    init() {

        self.items = Array()
    }

}
