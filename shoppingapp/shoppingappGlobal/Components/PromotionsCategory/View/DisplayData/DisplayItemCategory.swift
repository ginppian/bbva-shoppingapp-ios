//
//  File.swift
//  shoppingapp
//
//  Created by ruben.fernandez on 8/11/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation

struct DisplayItemCategory: Equatable {

    var categoryName: String?
    var categoryImage: String
    var categoryImageColor: UIColor

    init(fromCategory category: CategoryBO) {

        categoryName = category.name
        categoryImage = category.id.imageNamed()
        categoryImageColor = .BBVA400
    }

}
