//
//  PromotionsCategoryTableViewCell.swift
//  shoppingapp
//
//  Created by ruben.fernandez on 14/11/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit

class PromotionsCategoryTableViewCell: UITableViewCell {

    static let identifier = "PromotionsCategoryTableViewCell"
    static let height: CGFloat = 73
    static let separatorHeight: CGFloat = 2

    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var categoryImage: UIImageView!

    override func awakeFromNib() {

        super.awakeFromNib()

        selectionStyle = .none

        normalStatus()

        setAccessibilities()

        let screenSize = UIScreen.main.bounds
        let additionalSeparator = UIView(frame: CGRect(x: 0, y: PromotionsCategoryTableViewCell.height - PromotionsCategoryTableViewCell.separatorHeight, width: screenSize.width, height: PromotionsCategoryTableViewCell.separatorHeight))
        additionalSeparator.backgroundColor = .BBVA100
        self.addSubview(additionalSeparator)

    }

    func configure(withDisplayItemCategory displayItemCategory: DisplayItemCategory) {

        title.text = displayItemCategory.categoryName
        categoryImage.image = ImageUtilities.image(with: SVGCache.image(forSVGNamed: displayItemCategory.categoryImage, color: displayItemCategory.categoryImageColor), width: 24, height: 24)

    }

    private func normalStatus() {

        title.font = Tools.setFontBook(size: 16)
        title.textColor = .BBVA600

        backgroundColor = .BBVAWHITE
        contentView.backgroundColor = .BBVAWHITE
    }

    override func setHighlighted(_ highlighted: Bool, animated: Bool) {

        super.setHighlighted(highlighted, animated: animated)

        if highlighted {
            backgroundColor = .BBVA100
            contentView.backgroundColor = .BBVA100

        } else {
            backgroundColor = .BBVAWHITE
            contentView.backgroundColor = .BBVAWHITE
        }

    }

}

// MARK: - APPIUM

private extension PromotionsCategoryTableViewCell {

    func setAccessibilities() {

        #if APPIUM

            accessibilityIdentifier = ConstantsAccessibilities.OTHER_CATEGORY_CELL

            Tools.setAccessibility(view: self.title, identifier: ConstantsAccessibilities.OTHER_CATEGORY_TITLE)

        #endif

    }
}
