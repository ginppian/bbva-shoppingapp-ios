//
//  TransactionTypeFilterView.swift
//  shoppingapp
//
//  Created by ruben.fernandez on 14/11/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit

protocol PromotionsCategoryViewDelegate: class {

    func promotionsCategoryViewConfig(_ promotionsCategoryView: PromotionsCategoryView, withHeight height: CGFloat)
    func promotionsCategoryViewUpdate(_ promotionsCategoryView: PromotionsCategoryView, withHeight height: CGFloat)
    func promotionsCategoryViewSelected(_ promotionsCategoryView: PromotionsCategoryView, withCategory category: CategoryBO)
    func promotionsCategoryViewSelectFavoriteCategoriesPressed(_ promotionsCategoryView: PromotionsCategoryView)
    func promotionsCategoryViewScrollForAllCategories()
}

class PromotionsCategoryView: XibView {

    var presenter = PromotionsCategoryPresenter()
    var displayItemsToShow: DisplayItemCategories?
    weak var delegate: PromotionsCategoryViewDelegate?
    var promotionsCategoryFooterView: PromotionsCategoryFooterView?

    // MARK: @IBOutlet's

    @IBOutlet weak var promotionsCategoryOptionsView: UIView!
    @IBOutlet weak var promotionsCategoryTableView: UITableView!

    // MARK: - Private Helper Methods

    override func configureView() {

        super.configureView()

        presenter.view = self

        configurePromotionsCategoryView()

        setAccessibilities()

    }

    private func configurePromotionsCategoryView() {

        promotionsCategoryTableView.backgroundColor = .clear

        promotionsCategoryTableView.register(UINib(nibName: PromotionsCategoryTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: PromotionsCategoryTableViewCell.identifier)

        promotionsCategoryTableView.tableHeaderView = PromotionsCategoryHeaderView(frame: CGRect(x: 0, y: 0, width: frame.size.width, height: PromotionsCategoryHeaderView.defaultHeight))

        promotionsCategoryTableView.isScrollEnabled = false
        promotionsCategoryTableView.separatorStyle = .none

    }

    private func calculateHeight() -> CGFloat {

        guard let displayItemsToShow = displayItemsToShow else {

            return 0.0
        }

        let heightFooter = PromotionsCategoryFooterView.heightFooter(showSelectFavoriteCategories: displayItemsToShow.showSelectFavoriteCategories)
        let height = (CGFloat((displayItemsToShow.items?.count)!) * PromotionsCategoryTableViewCell.height) + PromotionsCategoryHeaderView.defaultHeight + heightFooter

        return height
    }

    func configurePromotionsCategory(withDisplayModel displayModel: PromotionsCategoryDisplayModel) {

        presenter.onConfigureView(withDisplayModel: displayModel)

        let height = calculateHeight()

        delegate?.promotionsCategoryViewConfig(self, withHeight: height)
    }
}

extension PromotionsCategoryView: PromotionsCategoryViewProtocol {

    func prepareTable(withDisplayItems displayItems: DisplayItemCategories) {

        displayItemsToShow = displayItems
        promotionsCategoryTableView.reloadData()
        
        if displayItems.showAllCategories {
            delegate?.promotionsCategoryViewScrollForAllCategories()
        }
    }

    func configureFooter(showSelectFavoriteCategories: Bool, showAllCategories: Bool) {

        let heightFooter = PromotionsCategoryFooterView.heightFooter(showSelectFavoriteCategories: showSelectFavoriteCategories)
        let footerView = PromotionsCategoryFooterView(frame: CGRect(x: 0, y: 0, width: frame.size.width, height: heightFooter))

        if !showSelectFavoriteCategories {

            footerView.hideSelectFavoriteCategories()
        }

        footerView.delegate = self
        
        promotionsCategoryFooterView = footerView

        promotionsCategoryTableView.tableFooterView = footerView
    }
    
    func configureCategoriesButtonForLessCategories() {
        promotionsCategoryFooterView?.configureShowAllCategoriesButtonForLessCategories()
    }
    
    func configureCategoriesButtonForSeeAllCategories() {
        promotionsCategoryFooterView?.configureShowAllCategoriesButtonForSeeAllCategories()
    }

    func informCategorySelected(withCategory category: CategoryBO) {

        delegate?.promotionsCategoryViewSelected(self, withCategory: category)
    }
    
    func trackPromotionsSeeAllCategories() {
        TrackerHelper.sharedInstance().trackPromotionsSeeAllCategories(withCustomerID: SessionDataManager.sessionDataInstance().customerID)
    }
}

extension PromotionsCategoryView: PromotionsCategoryFooterViewDelegate {

    func promotionsCategoryFooterViewSelectFavoriteCategoriesPressed(_ promotionsCategoryFooterView: PromotionsCategoryFooterView) {

        delegate?.promotionsCategoryViewSelectFavoriteCategoriesPressed(self)
    }

    func promotionsCategoryFooterViewShowAllCategoriesPressed(_ promotionsCategoryFooterView: PromotionsCategoryFooterView) {
        
        presenter.showCategoriesByButtonPressed()
        let height = calculateHeight()
        delegate?.promotionsCategoryViewUpdate(self, withHeight: height)
    }
}

extension PromotionsCategoryView: UITableViewDataSource, UITableViewDelegate {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if displayItemsToShow != nil && displayItemsToShow!.items?.count ?? 0 > 0 {
            return displayItemsToShow!.items!.count
        }

        return 0
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        return PromotionsCategoryTableViewCell.height
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if let cell = tableView.dequeueReusableCell(withIdentifier: PromotionsCategoryTableViewCell.identifier, for: indexPath) as? PromotionsCategoryTableViewCell {
            cell.configure(withDisplayItemCategory: (displayItemsToShow?.items![indexPath.row])!)

            return cell
        }

        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        presenter.itemSelected(atIndex: indexPath.row)
    }
}

// MARK: - APPIUM

private extension PromotionsCategoryView {

    func setAccessibilities() {

        #if APPIUM
            promotionsCategoryTableView.accessibilityIdentifier = ConstantsAccessibilities.OTHER_CATEGORIES_LIST
        #endif
    }
}
