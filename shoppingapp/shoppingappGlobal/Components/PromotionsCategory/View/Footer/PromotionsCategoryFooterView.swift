//
//  TransactionTypeFilterHeaderView.swift
//  shoppingapp
//
//  Created by ruben.fernandez on 14/11/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit

protocol PromotionsCategoryFooterViewDelegate: class {

    func promotionsCategoryFooterViewSelectFavoriteCategoriesPressed(_ promotionsCategoryFooterView: PromotionsCategoryFooterView)
    func promotionsCategoryFooterViewShowAllCategoriesPressed(_ promotionsCategoryFooterView: PromotionsCategoryFooterView)
}

class PromotionsCategoryFooterView: UIView {

    static let topBottomVerticalSeparation: CGFloat = 22
    static let showAllCategoriesButtonHeight: CGFloat = 30
    static let selectFavoriteCategoriesButtonHeight: CGFloat = 50
    static let buttonsSeparation: CGFloat = 32

    weak var delegate: PromotionsCategoryFooterViewDelegate?

    // MARK: @IBOutlet's

    @IBOutlet weak var showAllCategoriesButton: UIButton!
    @IBOutlet weak var selectFavoriteCategoriesButton: UIButton!

    // MARK: - Initializers

    override init(frame: CGRect) {

        super.init(frame: frame)
        setupView()
    }

    required init?(coder aDecoder: NSCoder) {

        super.init(coder: aDecoder)
        setupView()
    }

    // MARK: - Private Helper Methods

    private func setupView() {

        let view = viewFromNibForClass()
        view.frame = bounds
        view.backgroundColor = .white

        configureButtons()
        setAccessibilities()

        addSubview(view)
    }

    private func configureButtons() {

        showAllCategoriesButton.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Common.add_icon, color: .MEDIUMBLUE), width: 20, height: 20), for: .normal)
        showAllCategoriesButton.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Common.add_icon, color: .COREBLUE), width: 20, height: 20), for: .highlighted)
        showAllCategoriesButton.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Common.add_icon, color: .COREBLUE), width: 20, height: 20), for: .selected)

        showAllCategoriesButton.setTitle(Localizables.promotions.key_promotions_see_all_fem_text, for: .normal)
        showAllCategoriesButton.titleLabel?.font = Tools.setFontBold(size: 14)
        showAllCategoriesButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: 5, bottom: 0, right: 0)

        showAllCategoriesButton.setTitleColor(.MEDIUMBLUE, for: .normal)
        showAllCategoriesButton.setTitleColor(.COREBLUE, for: .highlighted)
        showAllCategoriesButton.setTitleColor(.COREBLUE, for: .selected)

        selectFavoriteCategoriesButton.setBackgroundColor(color: .MEDIUMBLUE, forUIControlState: .normal)
        selectFavoriteCategoriesButton.setBackgroundColor(color: .DARKMEDIUMBLUE, forUIControlState: .highlighted)
        selectFavoriteCategoriesButton.titleLabel?.font = Tools.setFontBold(size: 14)
        selectFavoriteCategoriesButton.setTitleColor(.BBVAWHITE, for: .normal)
        selectFavoriteCategoriesButton.setTitle(Localizables.promotions.key_promotions_configure_favourites_title, for: .normal)
    }

    private func viewFromNibForClass() -> UIView {

        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView

        return view
    }
    
    func hideSelectFavoriteCategories() {

        selectFavoriteCategoriesButton.isHidden = true
    }
    
    func configureShowAllCategoriesButtonForSeeAllCategories() {
        
        showAllCategoriesButton.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Common.add_icon, color: .MEDIUMBLUE), width: 20, height: 20), for: .normal)
        showAllCategoriesButton.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Common.add_icon, color: .COREBLUE), width: 20, height: 20), for: .highlighted)
        showAllCategoriesButton.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Common.add_icon, color: .COREBLUE), width: 20, height: 20), for: .selected)
        
        showAllCategoriesButton.setTitle(Localizables.promotions.key_promotions_see_all_fem_text, for: .normal)
    }
    
    func configureShowAllCategoriesButtonForLessCategories() {
        
        showAllCategoriesButton.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Common.substract_icon, color: .MEDIUMBLUE), width: 20, height: 20), for: .normal)
        showAllCategoriesButton.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Common.substract_icon, color: .COREBLUE), width: 20, height: 20), for: .highlighted)
        showAllCategoriesButton.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Common.substract_icon, color: .COREBLUE), width: 20, height: 20), for: .selected)
        
        showAllCategoriesButton.setTitle(Localizables.promotions.key_promotions_see_less_text, for: .normal)
    }

    class func heightFooter(showSelectFavoriteCategories: Bool) -> CGFloat {
        
        var height = PromotionsCategoryFooterView.topBottomVerticalSeparation * 2
        height += PromotionsCategoryFooterView.showAllCategoriesButtonHeight

        if showSelectFavoriteCategories {

            height += PromotionsCategoryFooterView.selectFavoriteCategoriesButtonHeight
            height += PromotionsCategoryFooterView.buttonsSeparation
        }
        
        return height
    }

    @IBAction func selectFavoriteCategoriesPressed(_ sender: AnyObject?) {

        delegate?.promotionsCategoryFooterViewSelectFavoriteCategoriesPressed(self)
    }

    @IBAction func showAllCategoriesPressed(_ sender: AnyObject?) {

        delegate?.promotionsCategoryFooterViewShowAllCategoriesPressed(self)
    }
}

// MARK: - APPIUM

private extension PromotionsCategoryFooterView {

    func setAccessibilities() {

        #if APPIUM
            Tools.setAccessibility(view: showAllCategoriesButton, identifier: ConstantsAccessibilities.SHOW_ALL_CATEGORIES_BUTTON)
        #endif
    }
}
