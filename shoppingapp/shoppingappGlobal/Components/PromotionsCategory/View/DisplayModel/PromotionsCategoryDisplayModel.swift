//
//  PromotionsCategoryDisplayModel.swift
//  shoppingapp
//
//  Created by jesus.martinez on 4/7/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

struct PromotionsCategoryDisplayModel {

    let categories: CategoriesBO
    let shouldShowSelectFavoriteCategories: Bool

}
