//
//  TransactionTypeFilterHeaderView.swift
//  shoppingapp
//
//  Created by ruben.fernandez on 14/11/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit

class PromotionsCategoryHeaderView: UIView {

    static let defaultHeight: CGFloat = 66.0

    // MARK: @IBOutlet's

    @IBOutlet weak var titleLabel: UILabel!

    // MARK: - Initializers

    override init(frame: CGRect) {

        super.init(frame: frame)
        setupView()
    }

    required init?(coder aDecoder: NSCoder) {

        super.init(coder: aDecoder)
        setupView()
    }

    // MARK: - Private Helper Methods

    private func setupView() {

        let view = viewFromNibForClass()
        view.frame = bounds
        view.backgroundColor = .BBVA200

        configureLabels()
        setAccessibilities()

        addSubview(view)
    }

    private func configureLabels() {

        titleLabel.font = Tools.setFontBold(size: 13)
        titleLabel.textColor = .BBVA500
        titleLabel.text = Localizables.promotions.key_promotions_others_promotions_text
    }

    private func viewFromNibForClass() -> UIView {

        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView

        return view
    }

}

// MARK: - APPIUM

private extension PromotionsCategoryHeaderView {

    func setAccessibilities() {

        #if APPIUM
            Tools.setAccessibility(view: titleLabel, identifier: ConstantsAccessibilities.OTHER_CATEGORY_HEADER_TITLE)
        #endif

    }
}
