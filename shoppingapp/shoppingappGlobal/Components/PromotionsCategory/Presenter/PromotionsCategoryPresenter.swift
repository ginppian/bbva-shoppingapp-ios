//
//  PromotionsCategoryPresenter.swift
//  shoppingapp
//
//  Created by ruben.fernandez on 14/11/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import RxSwift

class PromotionsCategoryPresenter {

    static let maximumNumberOfCategoriesToShow = Settings.PromotionsGlobal.maximum_number_of_categories_to_show

    weak var view: PromotionsCategoryViewProtocol?
    var displayItemsToShow: DisplayItemCategories?
    var displayItems: DisplayItemCategories?
    var categoriesBO: CategoriesBO?
}

extension PromotionsCategoryPresenter: PromotionsCategoryPresenterProtocol {

    func onConfigureView(withDisplayModel displayModel: PromotionsCategoryDisplayModel) {

        displayModel.categories.sortAlphabeticallyByName()

        categoriesBO = displayModel.categories

        displayItems = DisplayItemCategories()
        displayItemsToShow = DisplayItemCategories()
        displayItems?.showSelectFavoriteCategories = displayModel.shouldShowSelectFavoriteCategories
        displayItemsToShow?.showSelectFavoriteCategories = displayModel.shouldShowSelectFavoriteCategories

        for category in displayModel.categories.categories {

            displayItems?.items?.append(DisplayItemCategory(fromCategory: category))
        }

        if (displayItems?.items?.count)! <= PromotionsCategoryPresenter.maximumNumberOfCategoriesToShow {

            showAllCategories()
            
        } else {

            for i in 0 ..< PromotionsCategoryPresenter.maximumNumberOfCategoriesToShow {

                displayItemsToShow?.items?.append((displayItems?.items![i])!)
            }

            view?.configureFooter(showSelectFavoriteCategories: displayModel.shouldShowSelectFavoriteCategories, showAllCategories: false)
            view?.prepareTable(withDisplayItems: displayItemsToShow!)
        }
    }
    
    func showCategoriesByButtonPressed() {
        
        guard let displayItems = displayItems else {
            return
        }
        
        if displayItems.showAllCategories {
            
            showLessCategories()
            
        } else {
            
            showAllCategories()
            view?.trackPromotionsSeeAllCategories()
        }
    }
    
    func itemSelected(atIndex index: Int) {

        if let category = categoriesBO?.categories[index] {

            view?.informCategorySelected(withCategory: category)
        }
    }
}

// MARK: - Private methods
private extension PromotionsCategoryPresenter {
    
    func showAllCategories() {
        
        guard let displayItems = displayItems else {
            return
        }
        
        displayItemsToShow?.items?.removeAll()
        
        let total: Int = (displayItems.items?.count)!
        
        for i in 0 ..< total {
            
            displayItemsToShow?.items?.append((displayItems.items![i]))
        }
        
        displayItems.showAllCategories = true
        displayItemsToShow?.showAllCategories = true
        
        view?.configureFooter(showSelectFavoriteCategories: displayItems.showSelectFavoriteCategories, showAllCategories: true)
        view?.configureCategoriesButtonForLessCategories()
        view?.prepareTable(withDisplayItems: displayItemsToShow!)
    }
    
    func showLessCategories() {
        
        guard let displayItems = displayItems else {
            return
        }
        
        displayItemsToShow?.items?.removeAll()
        
        for i in 0 ..< PromotionsCategoryPresenter.maximumNumberOfCategoriesToShow {
            
            displayItemsToShow?.items?.append((displayItems.items![i]))
        }
        
        displayItems.showAllCategories = false
        displayItemsToShow?.showAllCategories = false
        
        view?.configureFooter(showSelectFavoriteCategories: displayItems.showSelectFavoriteCategories, showAllCategories: false)
        view?.configureCategoriesButtonForSeeAllCategories()
        view?.prepareTable(withDisplayItems: displayItemsToShow!)
    }
}
