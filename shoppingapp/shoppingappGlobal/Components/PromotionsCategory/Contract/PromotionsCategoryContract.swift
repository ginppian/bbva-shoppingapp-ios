//
//  PromotionsCategoryContract.swift
//  shoppingapp
//
//  Created by ruben.fernandez on 14/11/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import RxSwift

protocol PromotionsCategoryViewProtocol: ComponentViewProtocol {

    func prepareTable(withDisplayItems displayItems: DisplayItemCategories)
    func configureFooter(showSelectFavoriteCategories: Bool, showAllCategories: Bool)
    func informCategorySelected(withCategory category: CategoryBO)
    func configureCategoriesButtonForLessCategories()
    func configureCategoriesButtonForSeeAllCategories()
    func trackPromotionsSeeAllCategories()
}

protocol PromotionsCategoryPresenterProtocol: ComponentPresenterProtocol {

    func onConfigureView(withDisplayModel displayModel: PromotionsCategoryDisplayModel)
    func showCategoriesByButtonPressed()
    func itemSelected(atIndex index: Int)
}
