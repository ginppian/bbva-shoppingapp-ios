//
//  OptionsSelectionableContract.swift
//  shoppingapp
//
//  Created by jesus.martinez on 21/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol OptionsSelectionableViewProtocol: ComponentViewProtocol {

    func configureTitle(withText text: String)
    func showOptions(withDisplayData displayData: [OptionSelectionableDisplayData])

}

protocol OptionsSelectionablePresenterProtocol: ComponentPresenterProtocol {

    func onConfigureView()

}
