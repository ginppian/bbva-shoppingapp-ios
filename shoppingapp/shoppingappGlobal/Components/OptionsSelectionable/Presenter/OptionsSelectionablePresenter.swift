//
//  OptionsSelectionablePresenter.swift
//  shoppingapp
//
//  Created by jesus.martinez on 21/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit

class OptionsSelectionablePresenter {

    weak var view: OptionsSelectionableViewProtocol?

    var displayData: OptionsSelectionableDisplayData? {
        didSet {
            onConfigureView()
        }
    }

}

extension OptionsSelectionablePresenter: OptionsSelectionablePresenterProtocol {

    func onConfigureView() {

        if let display = displayData {
            view?.configureTitle(withText: display.title)
            view?.showOptions(withDisplayData: display.options)
        }

    }

}
