//
//  OptionsSelectionableDisplayData.swift
//  shoppingapp
//
//  Created by jesus.martinez on 21/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit

struct OptionsSelectionableDisplayData {

    var title: String
    var options: [OptionSelectionableDisplayData]

    init(title: String) {

        self.title = title
        options = [OptionSelectionableDisplayData]()
    }

    init(title: String, promotionTypes: [PromotionType], promotionTypesSelected: [PromotionType]? = nil) {

        self.init(title: title)

        for promotionType in promotionTypes {

            var newOption: OptionSelectionableDisplayData?

            switch promotionType {
            case .discount:
                newOption = OptionSelectionableDisplayData(imageName: ConstantsImages.Promotions.discount_icon, title: promotionType.literal)
            case .toMonths:
                newOption = OptionSelectionableDisplayData(imageName: ConstantsImages.Common.calendar_icon, title: promotionType.literal)
            case .point:
                newOption = OptionSelectionableDisplayData(imageName: ConstantsImages.Promotions.promotion, title: promotionType.literal)
            default:
                break
            }

            if var newOption = newOption {
                newOption.isSelected = promotionTypesSelected?.contains(promotionType) ?? false
                options.append(newOption)
            }

        }

    }

    init(title: String, promotionUsageTypes: [PromotionUsageType], promotionUsageTypesSelected: [PromotionUsageType]? = nil) {

        self.init(title: title)

        for promotionUsageType in promotionUsageTypes {

            var newOption: OptionSelectionableDisplayData?

            switch promotionUsageType {
            case .physical:
                newOption = OptionSelectionableDisplayData(imageName: ConstantsImages.Promotions.shopping, title: promotionUsageType.literal)
            case .online:
                newOption = OptionSelectionableDisplayData(imageName: ConstantsImages.Promotions.ecommerce, title: promotionUsageType.literal)
            default:
                break
            }

            if var newOption = newOption {
                newOption.isSelected = promotionUsageTypesSelected?.contains(promotionUsageType) ?? false
                options.append(newOption)
            }

        }

    }

}

struct OptionSelectionableDisplayData {

    var imageName: String
    var title: String
    var isSelected = false

    init(imageName: String, title: String) {

        self.imageName = imageName
        self.title = title

    }

}
