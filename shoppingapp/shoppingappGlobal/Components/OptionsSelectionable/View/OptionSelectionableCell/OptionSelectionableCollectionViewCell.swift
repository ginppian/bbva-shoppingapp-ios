//
//  OptionSelectionableCollectionViewCell.swift
//  shoppingapp
//
//  Created by jesus.martinez on 22/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit

class OptionSelectionableCollectionViewCell: UICollectionViewCell {

    static let identifier = "OptionSelectionableCollectionViewCell"
    static let defaultSize = CGSize(width: 80, height: 140)

    @IBOutlet weak var optionButton: UIButton!
    @IBOutlet weak var optionTitleLabel: UILabel!

    var optionImage: String?

    override var isSelected: Bool {
        didSet {

            let textColor: UIColor = isSelected ? .COREBLUE : .BBVA600
            let colorBackgroundButton: UIColor = isSelected ? .COREBLUE : .BBVAWHITE
            let colorImageButton: UIColor = isSelected ? .BBVAWHITE : .BBVA500

            optionButton.setBackgroundColor(color: colorBackgroundButton, forUIControlState: .normal)
            optionTitleLabel.textColor = textColor

            if let image = optionImage {
                optionButton.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: image, color: colorImageButton), width: 24, height: 24), for: .normal)
            }

        }
    }

    override var isHighlighted: Bool {
        willSet {

            optionButton.isHighlighted = newValue

        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()

        optionButton.layer.cornerRadius = 0.5 * optionButton.bounds.size.width
        optionButton.clipsToBounds = true

        optionButton.setBackgroundColor(color: .BBVAWHITE, forUIControlState: .normal)
        optionButton.setBackgroundColor(color: .COREBLUE, forUIControlState: .highlighted)
        optionButton.setTitle("", for: .normal)
        optionButton.isUserInteractionEnabled = false

        optionTitleLabel.font = Tools.setFontBook(size: 14)
        optionTitleLabel.textColor = .BBVA600
        optionTitleLabel.numberOfLines = 2

    }

    func configure(withDisplayData displayData: OptionSelectionableDisplayData) {

        optionImage = displayData.imageName

        let textColor: UIColor = isSelected ? .COREBLUE : .BBVA600
        let colorImageButton: UIColor = isSelected ? .BBVAWHITE : .BBVA500
        let colorBackgroundButton: UIColor = isSelected ? .COREBLUE : .BBVAWHITE

        optionButton.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: displayData.imageName, color: colorImageButton), width: 24, height: 24), for: .normal)
        optionButton.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: displayData.imageName, color: .BBVAWHITE), width: 24, height: 24), for: .highlighted)

        optionTitleLabel.text = displayData.title

        optionTitleLabel.textColor = textColor

        optionButton.setBackgroundColor(color: colorBackgroundButton, forUIControlState: .normal)

    }

}
