//
//  OptionsSelectionableView.swift
//  shoppingapp
//
//  Created by jesus.martinez on 21/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit

protocol OptionsSelectionableViewDelegate: class {
    func optionsSelectionableView(_ optionsSelectionableView: OptionsSelectionableView, didSelectOptionAtIndex index: Int)
    func optionsSelectionableView(_ optionsSelectionableView: OptionsSelectionableView, didDeselectOptionAtIndex index: Int)

}

class OptionsSelectionableView: XibView {

    static let padding: CGFloat = 20.0
    static let itemPadding: CGFloat = 15.0

    let itemSizeCell = OptionSelectionableCollectionViewCell.defaultSize
    let itemSectionInset = UIEdgeInsets(top: 0.0, left: OptionsSelectionableView.padding, bottom: 0.0, right: OptionsSelectionableView.padding)

    let presenter = OptionsSelectionablePresenter()

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var optionsCollectionView: UICollectionView!
    @IBOutlet weak var optionsCollectionViewFlowLayout: UICollectionViewFlowLayout!

    var options: [OptionSelectionableDisplayData]?
    weak var delegate: OptionsSelectionableViewDelegate?

    override func configureView() {

        super.configureView()

        configureTitleLabel()
        configureOptionsCollectionView()

        presenter.view = self
        presenter.onConfigureView()
    }

    private func configureTitleLabel() {

        titleLabel.textColor = .BBVA500
        titleLabel.font = Tools.setFontBold(size: 13)

    }

    private func configureOptionsCollectionView() {

        optionsCollectionView.dataSource = self
        optionsCollectionView.delegate = self
        optionsCollectionView.backgroundColor = .clear
        optionsCollectionView.allowsMultipleSelection = true
        optionsCollectionView.scrollsToTop = false
        optionsCollectionView.alwaysBounceVertical = false
        optionsCollectionView.alwaysBounceHorizontal = false
        optionsCollectionView.isPagingEnabled = false
        optionsCollectionView.showsHorizontalScrollIndicator = false
        optionsCollectionView.showsVerticalScrollIndicator = false
        optionsCollectionView.delaysContentTouches = false

        optionsCollectionView.register(UINib(nibName: OptionSelectionableCollectionViewCell.identifier, bundle: nil), forCellWithReuseIdentifier: OptionSelectionableCollectionViewCell.identifier)

        optionsCollectionViewFlowLayout.scrollDirection = .horizontal
        optionsCollectionViewFlowLayout.sectionInset = itemSectionInset
        optionsCollectionViewFlowLayout.minimumLineSpacing = OptionsSelectionableView.itemPadding
        optionsCollectionViewFlowLayout.itemSize = itemSizeCell

    }

    func deselectAll() {

        if let selectedIndextPaths = optionsCollectionView.indexPathsForSelectedItems {
            for indexPath in selectedIndextPaths {
                optionsCollectionView.deselectItem(at: indexPath, animated: false)
            }
        }

    }

}

// MARK: - UICollectionView DataSource & Delegate

extension OptionsSelectionableView: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return options?.count ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        if let displayData = options?[indexPath.row] {
            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: OptionSelectionableCollectionViewCell.identifier, for: indexPath) as? OptionSelectionableCollectionViewCell {

                cell.configure(withDisplayData: displayData)

                return cell
            }
        }

        return UICollectionViewCell()
    }
}

extension OptionsSelectionableView: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.optionsSelectionableView(self, didSelectOptionAtIndex: indexPath.row)
    }

    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        delegate?.optionsSelectionableView(self, didDeselectOptionAtIndex: indexPath.row)
    }

}

extension OptionsSelectionableView: OptionsSelectionableViewProtocol {

    func configureTitle(withText text: String) {

        titleLabel.text = text

    }

    func showOptions(withDisplayData displayData: [OptionSelectionableDisplayData]) {

        options = displayData
        optionsCollectionView.reloadData()

        for i in 0..<displayData.count {

            if displayData[i].isSelected {
                optionsCollectionView.selectItem(at: IndexPath(row: i, section: 0), animated: false, scrollPosition: .top)
            } else {
                optionsCollectionView.deselectItem(at: IndexPath(row: i, section: 0), animated: false)
            }

        }

    }

}
