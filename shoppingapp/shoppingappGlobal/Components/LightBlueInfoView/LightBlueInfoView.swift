//
//  LightBlueInfoView.swift
//  shoppingapp
//
//  Created by ignacio.bonafonte on 06/07/2018.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit

class LightBlueInfoView: XibView {

    @IBOutlet var contentView: UIView!
    @IBOutlet weak var labelText: UILabel!
    @IBOutlet weak var imageView: UIImageView!

    override func configureView() {
        super.configureView()

        contentView.backgroundColor = .WHITELIGHTBLUE

        imageView.image = SVGCache.image(forSVGNamed: ConstantsImages.Common.info_icon, color: .BBVALIGHTBLUE)

        labelText.font = Tools.setFontBook(size: 14)
        labelText.textColor = .BBVA600
        labelText.textAlignment = .center
    }

    func configure(text: String) {
        labelText.text = text
    }
}
