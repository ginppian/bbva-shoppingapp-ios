//
//  TransactionTypeFilterView.swift
//  shoppingapp
//
//  Created by ruben.fernandez on 14/11/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit

protocol PromotionsTrendingViewDelegate: class {
    func promotionsTrendingShowAll(_ promotionsTrending: PromotionsTrendingView)
    func promotionsTrending(_ promotionsTrending: PromotionsTrendingView, didSelectItem displayItem: DisplayItemPromotion?)
    func promotionsTrendingWillBeginDragging(_ promotionsTrending: PromotionsTrendingView)
}

class PromotionsTrendingView: XibView {

    var presenter = PromotionsTrendingPresenter()

    // MARK: @IBOutlet's

    @IBOutlet weak var pagerView: FSPagerView!

    var displayItemsToShow: DisplayItemsPromotions?

    static let screenWidth = UIScreen.main.bounds.size.width
    static let minimumScaleCards: CGFloat = 0.725
    static let widthLabel: CGFloat = 170.0
    static let heightLabel: CGFloat = 14.0
    static let widthButton: CGFloat = 88.0
    static let heightButton: CGFloat = 11.0
    static let paddingMargin: CGFloat = 8.0
    static let padding: CGFloat = 15.0
    static let paddingTop: CGFloat = 24.0
    static let widthItemCell = screenWidth - (PromotionsTrendingView.padding * 2)
    static let marginTop: CGFloat = 55.0

    static var height: CGFloat {
        return PromotionTrendingViewCell.heightForWidth(widthItemCell) + marginTop
    }

    @IBOutlet weak var leadingConstraint: NSLayoutConstraint!

    var trendingLabel: UILabel!
    var seeAllButton = UIButton()

    weak var delegate: PromotionsTrendingViewDelegate?

    // MARK: - Private Helper Methods

    override func configureView() {
        super.configureView()

        presenter.view = self
        configurePromotionsTrendingView()
        setAccessibilities()

    }

    private func configurePromotionsTrendingView() {

        pagerView.backgroundColor = .BBVA200
        backgroundColor = .BBVA200

        pagerView.register(UINib(nibName: PromotionTrendingViewCell.identifier, bundle: nil), forCellWithReuseIdentifier: PromotionTrendingViewCell.identifier)

        pagerView.isInfinite = false
        pagerView.transformer = FSPagerViewTransformer(type: .linear)
        pagerView.transformer?.minimumScale = PromotionsTrendingView.minimumScaleCards
        pagerView.itemSize = CGSize(width: PromotionsTrendingView.widthItemCell, height: PromotionTrendingViewCell.heightForWidth(PromotionsTrendingView.widthItemCell))
        pagerView.delegate = self
        pagerView.dataSource = self

        trendingLabel = UILabel(frame: CGRect(x: ((PromotionsTrendingView.screenWidth / 2) - (pagerView.itemSize.width / 2)) + PromotionsTrendingView.paddingMargin, y: pagerView.frame.origin.y - PromotionsTrendingView.paddingTop, width: PromotionsTrendingView.widthLabel, height: PromotionsTrendingView.heightLabel))
        trendingLabel.text = Localizables.promotions.key_promotions_featured_text.uppercased()
        trendingLabel.font = Tools.setFontBold(size: 13)
        trendingLabel.textColor = .BBVA500

        addSubview(trendingLabel)

    }

    func configurePromotionsTrending(withPromotions promotions: [DisplayItemPromotion]) {
        presenter.onConfigureView(withPromotions: promotions)
    }

}

extension PromotionsTrendingView: PromotionsTrendingViewProtocol {

    func addButtonSeeAll() {

        let screen = UIScreen.main.bounds
        let screenWidth = screen.size.width

        seeAllButton = UIButton(frame: CGRect(x: ((screenWidth / 2) + (pagerView.itemSize.width / 2)) - PromotionsTrendingView.widthButton, y: pagerView.frame.origin.y - PromotionsTrendingView.paddingTop, width: PromotionsTrendingView.widthButton, height: PromotionsTrendingView.heightButton))

        seeAllButton.setTitle(Localizables.promotions.key_promotions_see_all_text, for: .normal)
        seeAllButton.titleLabel?.font = Tools.setFontMedium(size: 14)
        seeAllButton.setTitleColor(.MEDIUMBLUE, for: .normal)
        seeAllButton.setTitleColor(.COREBLUE, for: .highlighted)
        seeAllButton.setTitleColor(.COREBLUE, for: .selected)

        seeAllButton.addTarget(self, action: #selector(showAllAction), for: .touchUpInside)

        addSubview(seeAllButton)
    }

    func prepareTable(withDisplayItems displayItems: DisplayItemsPromotions) {
        displayItemsToShow = displayItems
        pagerView.reloadData()
    }

    func changeConstraintAnimation() {
        leadingConstraint.constant = 30
    }

    func restoreConstraintAnimation () {
        layoutIfNeeded()
        UIView.animate(withDuration: 1.0) { [weak self] in
            self?.leadingConstraint.constant = 0
            self?.layoutIfNeeded()
            self?.presenter.setAnimationDone()
        }
    }

    @objc func showAllAction() {
        delegate?.promotionsTrendingShowAll(self)
    }

}

extension PromotionsTrendingView: FSPagerViewDataSource, FSPagerViewDelegate {

    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        return displayItemsToShow?.items!.count ?? 0
    }

    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {

        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: PromotionTrendingViewCell.identifier, at: index) as! PromotionTrendingViewCell

        let displayItem: DisplayItemPromotion! = displayItemsToShow?.items![index]

        cell.configureCellWithDisplayPromotionData(withDisplayitemPromotion: displayItem)

        return cell

    }

    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        delegate?.promotionsTrending(self, didSelectItem: displayItemsToShow?.items![index])

        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)
    }

    func pagerViewWillBeginDragging(_ pagerView: FSPagerView) {
        delegate?.promotionsTrendingWillBeginDragging(self)
    }
}

// MARK: - APPIUM

private extension PromotionsTrendingView {

    func setAccessibilities() {
        #if APPIUM

             pagerView.isAccessibilityElement = false
             pagerView.accessibilityIdentifier = ConstantsAccessibilities.PROMOTION_HOT_CARUSEL_TABLE

        #endif
    }
}
