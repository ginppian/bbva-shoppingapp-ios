//
//  PromotionTrendingViewCell.swift
//  shoppingapp
//
//  Created by ruben.fernandez on 29/11/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit

class PromotionTrendingViewCell: FSPagerViewCell {

    static let identifier = "PromotionTrendingViewCell"

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var promotionImage: UIImageView!
    @IBOutlet weak var commerceNameLabel: UILabel!
    @IBOutlet weak var descriptionOfferLabel: UILabel!
    @IBOutlet weak var diffDateLabel: UILabel!
    @IBOutlet weak var benefitLabel: UILabel!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var promotionTypeView: PromotionTypeView!
    @IBOutlet weak var promotionTypeViewWidth: NSLayoutConstraint!

    static let imageAspectRatio: CGFloat = 75 / 142
    static let spaceBetweenImageAndCommerceName: CGFloat = 20.0
    static let commerceNameHeight: CGFloat = 12.0
    static let spaceBetweenCommerceNameAndDescriptionOffer: CGFloat = 10.0
    static let descriptionOfferHeight: CGFloat = 34.0
    static let spaceBetweenDescriptionOfferAndBenefit: CGFloat = 10.0
    static let benefitHeight: CGFloat = 14.0
    static let spaceBetweenBenefitAndDiffDate: CGFloat = 48.0
    static let diffDateHeight: CGFloat = 14.0
    static let bottomSpace: CGFloat = 9.5

    static func heightForWidth(_ width: CGFloat) -> CGFloat {

        let imageHeight = width * imageAspectRatio

        return imageHeight
            + spaceBetweenImageAndCommerceName + commerceNameHeight
            + spaceBetweenCommerceNameAndDescriptionOffer + descriptionOfferHeight
            + spaceBetweenDescriptionOfferAndBenefit + benefitHeight
            + spaceBetweenBenefitAndDiffDate + diffDateHeight
            + bottomSpace

    }

    override func awakeFromNib() {
        super.awakeFromNib()

        containerView.backgroundColor = .BBVAWHITE
        promotionImage.backgroundColor = .BBVA200
        separatorView.backgroundColor = .BBVA200

        commerceNameLabel.font = Tools.setFontBook(size: 12)
        commerceNameLabel.textColor = .BBVA600

        descriptionOfferLabel.font = Tools.setFontMedium(size: 14)
        descriptionOfferLabel.textColor = .BBVA600

        diffDateLabel.font = Tools.setFontBookItalic(size: 14)
        diffDateLabel.textColor = .BBVA500

        benefitLabel.font = Tools.setFontMedium(size: 14)
        benefitLabel.textColor = .MEDIUMBLUE

        setAccessibilities()

    }

    func configureCellWithDisplayPromotionData(withDisplayitemPromotion displayItemPromotion: DisplayItemPromotion) {

        commerceNameLabel.text = displayItemPromotion.commerceName
        descriptionOfferLabel.text = displayItemPromotion.descriptionPromotion

        diffDateLabel.text = displayItemPromotion.dateDiff

        benefitLabel.text = displayItemPromotion.benefit

        if !Tools.isStringNilOrEmpty(withString: displayItemPromotion.imageDetail) {
            configureImage(withDisplayitemPromotion: displayItemPromotion)
        } else {
            promotionImage.image = UIImage(named: ConstantsImages.Promotions.default_promotion_image)
        }
        promotionTypeView.configurePromotionType(withPromotionTypeDisplayData: createPromotionTypeDisplayData(withPromotion: displayItemPromotion.promotionBO!))

        setAccessibilities()

    }

    private func createPromotionTypeDisplayData(withPromotion promotion: PromotionBO) -> PromotionTypeDisplayData {
        let promotionTypeDisplayData = PromotionTypeDisplayData(fromPromotion: promotion)
        promotionTypeViewWidth.constant = promotionTypeDisplayData.promotionTypeViewWidth

        return promotionTypeDisplayData

    }

    private func configureImage(withDisplayitemPromotion displayItemPromotion: DisplayItemPromotion) {
        
        if let imageSaved = displayItemPromotion.promotionBO?.imageDetailSaved {

            promotionImage.image = imageSaved
            promotionImage.backgroundColor = .BBVAWHITE
        } else {

            promotionImage.image = nil
            promotionImage.backgroundColor = .BBVA200
            
            let url = URL(string: displayItemPromotion.imageDetail?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")

            promotionImage.setImage(fromUrl: url,
                                    placeholderImage: UIImage(named: ConstantsImages.Promotions.default_promotion_image)) {  [weak self] successful in
                                        if successful {
                                            displayItemPromotion.promotionBO?.imageDetailSaved = self?.promotionImage.image
                                            DispatchQueue.main.async {
                                                self?.promotionImage.backgroundColor = .BBVAWHITE
                                            }
                                        }
            }
        }
    }
}

// MARK: - APPIUM

private extension PromotionTrendingViewCell {

    func setAccessibilities() {

        #if APPIUM

            accessibilityIdentifier = ConstantsAccessibilities.PROMOTION_HOT_CELL

            Tools.setAccessibility(view: commerceNameLabel, identifier: ConstantsAccessibilities.PROMOTION_PROMOTIONS_HOT_COMMERCE)
            Tools.setAccessibility(view: descriptionOfferLabel, identifier: ConstantsAccessibilities.PROMOTION_PROMOTIONS_HOT_NAME)
            Tools.setAccessibility(view: benefitLabel, identifier: ConstantsAccessibilities.PROMOTION_PROMOTIONS_HOT_BENEFIT)

        #endif

    }

}
