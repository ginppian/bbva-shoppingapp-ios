//
//  PromotionsCategoryContract.swift
//  shoppingapp
//
//  Created by ruben.fernandez on 14/11/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import RxSwift

protocol PromotionsTrendingViewProtocol: ComponentViewProtocol {
    func prepareTable(withDisplayItems displayItems: DisplayItemsPromotions)
    func addButtonSeeAll()
    func changeConstraintAnimation()
    func restoreConstraintAnimation()

}

protocol PromotionsTrendingPresenterProtocol: ComponentPresenterProtocol {
    func onConfigureView(withPromotions promotions: [DisplayItemPromotion])
    func setAnimationDone()
}
