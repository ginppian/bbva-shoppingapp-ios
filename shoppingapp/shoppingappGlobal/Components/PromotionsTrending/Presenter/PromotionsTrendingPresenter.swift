//
//  PromotionsCategoryPresenter.swift
//  shoppingapp
//
//  Created by ruben.fernandez on 14/11/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import RxSwift

class PromotionsTrendingPresenter {

    weak var view: PromotionsTrendingViewProtocol?
    var displayItems: DisplayItemsPromotions!
    let maxSmallPromotions: Int = 5

}

extension PromotionsTrendingPresenter: PromotionsTrendingPresenterProtocol {

    func onConfigureView(withPromotions promotions: [DisplayItemPromotion]) {

        if !SessionDataManager.sessionDataInstance().isShowTrendingAnimation && promotions.count > 1 {
            view?.changeConstraintAnimation()
        }

        displayItems = DisplayItemsPromotions()

        if promotions.count > maxSmallPromotions {
            for i in 0 ... maxSmallPromotions - 1 {
                displayItems.items?.append(promotions[i])
            }

            view?.addButtonSeeAll()
        } else {
            for promotionBO in promotions {
                displayItems.items?.append(promotionBO)
            }
        }

        if !SessionDataManager.sessionDataInstance().isShowTrendingAnimation && promotions.count > 1 {
            view?.restoreConstraintAnimation()
        }

        view?.prepareTable(withDisplayItems: displayItems)
    }

    func setAnimationDone() {
        SessionDataManager.sessionDataInstance().isShowTrendingAnimation = true
    }

}
