//
//  HelpView.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 23/01/2018.
//  Copyright © 2018 BBVA. All rights reserved.
//
import UIKit

protocol HelpViewDelegate: class {
    func helpButtonPressed(_ helpView: HelpView, andHelpId helpId: String)
}

class HelpView: XibView {

    // MARK: IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionButton: UIButton!
    @IBOutlet weak var helpImageView: UIImageView!
    @IBOutlet weak var containerView: UIView!

    private var displayData: HelpDisplayData?

    weak var delegate: HelpViewDelegate?

    override func configureView() {
        super.configureView()

        configureViewItems()
    }

    func configureViewItems() {

        titleLabel.font = Tools.setFontBold(size: 16)
        titleLabel.textAlignment = .center
        titleLabel.numberOfLines = 2

        descriptionButton.contentMode = .scaleAspectFit
        descriptionButton.titleLabel?.font = Tools.setFontMedium(size: 14.0)

    }

    @IBAction func helpInformationAction(_ sender: Any) {

        if let helpId = displayData?.helpId, !helpId.isEmpty {
            delegate?.helpButtonPressed(self, andHelpId: helpId)
        }
    }

    func setHelpDisplayData(forHelpDisplayData displayData: HelpDisplayData, withAnimation animation: Bool = true) {

        self.displayData = displayData

        backgroundColor = .clear
        containerView.backgroundColor = displayData.backgroundColor

        titleLabel.textColor = displayData.titleColor
        titleLabel.text = displayData.titleText

        descriptionButton.setTitle(displayData.descriptionText, for: .normal)
        descriptionButton.setTitleColor(displayData.descriptionTextColor, for: .normal)
        descriptionButton.setTitleColor(displayData.descriptionOnTapColor, for: .highlighted)
        descriptionButton.setTitleColor(displayData.descriptionOnTapColor, for: .selected)
        
        let imageDefault = UIImage(named: displayData.image)!
        
        guard let imageUrl = displayData.imageUrl, !imageUrl.isEmpty else {

            setImage(toImageView: helpImageView, withImage: imageDefault, andAnimated: animation)
            return
        }
        
        if let url = URL(string: imageUrl) {
            
            UIImage.load(fromUrl: url, completion: { image in
                DispatchQueue.main.async {
                    if let image = image {
                        self.setImage(toImageView: self.helpImageView, withImage: image, andAnimated: animation)
                    } else {
                        self.setImage(toImageView: self.helpImageView, withImage: imageDefault, andAnimated: animation)
                    }
                }
            })
        }
    }

    func setImage(toImageView imageView: UIImageView, withImage image: UIImage, andAnimated animated: Bool) {

        if animated {
            UIView.transition(with: imageView, duration: 1.0, options: .transitionCrossDissolve, animations: {
                imageView.image = image
            }, completion: { (_: Bool) in

            })
        } else {
            imageView.image = image
        }
    }

    // MARK: Touch logic

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)

        descriptionButton.setTitleColor(displayData?.descriptionOnTapColor, for: .normal)
    }

    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesCancelled(touches, with: event)

        descriptionButton.setTitleColor(displayData?.descriptionTextColor, for: .normal)
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)

        descriptionButton.setTitleColor(displayData?.descriptionTextColor, for: .normal)

        descriptionButton.sendActions(for: .touchUpInside)
    }

}
