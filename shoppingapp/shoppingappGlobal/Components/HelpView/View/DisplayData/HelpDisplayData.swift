//
//  HelpDisplayData.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 23/01/2018.
//  Copyright © 2018 BBVA. All rights reserved.
//

struct  HelpDisplayData {

    var helpId: String?
    var titleText: String?
    var descriptionText: String?
    var image = ConstantsImages.Help.imageThumbDefault
    var imageUrl: String?
    var backgroundColor: UIColor?
    var titleColor: UIColor?
    var descriptionTextColor: UIColor?
    var descriptionOnTapColor: UIColor?

    static func generateHelpDisplayData(withConfigBO configBO: ConfigFileBO, isFromSection: Bool) -> HelpDisplayData {

        var helpDisplayData = HelpDisplayData()

        helpDisplayData.helpId = configBO.itemId

        if let selectedLanguageData = configBO.getSelectedLanguage(forKey: ConstantsLanguage.DEFAULT_LANGUAGE) {
            helpDisplayData.titleText = selectedLanguageData.title
            helpDisplayData.descriptionText = selectedLanguageData.description
        }

        if let imageThumb = configBO.image, !imageThumb.isEmpty {
            helpDisplayData.imageUrl = configBO.getImageURL(forUrl: imageThumb)
        } else {
            helpDisplayData = helpDisplayData.setDefaultColorsAndBackground(withHelpDisplayData: helpDisplayData)
            return helpDisplayData
        }

        var configType: HelpBO?
        
        if isFromSection {
            configType = configBO.section
        } else {
            configType = configBO.help
        }
        
        if let configType = configType, let titleColor = configType.titleColor, let hexStringTitleColor = titleColor.hexString, !hexStringTitleColor.isEmpty, let alphaTitleColor = titleColor.alpha, let descriptionColor = configType.descriptionColor, let hexStringDescriptionColor = descriptionColor.hexString, !hexStringDescriptionColor.isEmpty, let alphaDescriptionColor = descriptionColor.alpha, let descriptionOnTapColor = configType.descriptionOnTapColor, let hexStringDescriptionOnTapColor = descriptionOnTapColor.hexString, !hexStringDescriptionOnTapColor.isEmpty, let alphaDescriptionOnTapColor = descriptionOnTapColor.alpha, let backgroundColor = configType.backgroundColor, let hexStringBackgroundColor = backgroundColor.hexString, !hexStringBackgroundColor.isEmpty, let alphaBackgroundColor = backgroundColor.alpha {

            helpDisplayData.titleColor = Colors.hexStringToUIColor(hex: hexStringTitleColor, alpha: alphaTitleColor)
            helpDisplayData.descriptionTextColor = Colors.hexStringToUIColor(hex: hexStringDescriptionColor, alpha: alphaDescriptionColor)
            helpDisplayData.descriptionOnTapColor = Colors.hexStringToUIColor(hex: hexStringDescriptionOnTapColor, alpha: alphaDescriptionOnTapColor)

            helpDisplayData.backgroundColor = Colors.hexStringToUIColor(hex: hexStringBackgroundColor, alpha: alphaBackgroundColor)

        } else {
            helpDisplayData = helpDisplayData.setDefaultColorsAndBackground(withHelpDisplayData: helpDisplayData)
            return helpDisplayData
        }

        return helpDisplayData
    }

    func setDefaultColorsAndBackground(withHelpDisplayData helpDisplayData: HelpDisplayData) -> HelpDisplayData {

        var helpDisplayDataCopy = HelpDisplayData()

        helpDisplayDataCopy = helpDisplayData

        helpDisplayDataCopy.backgroundColor = .BBVAWHITE

        helpDisplayDataCopy.titleColor = .BBVA600
        helpDisplayDataCopy.descriptionTextColor = .MEDIUMBLUE
            helpDisplayDataCopy.descriptionOnTapColor = .COREBLUE

        return helpDisplayDataCopy
    }
}
