//
//  OnOffDisplayData.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 18/10/2017.
//  Copyright © 2017 BBVA. All rights reserved.
//

struct OnOffDisplayData {

    var cardBO: CardBO
    var switchText: String
    var switchImage: String
    var switchStatus = false
    var switchValue = false
    var accesibilityValue: String

    init(fromCardBO cardBO: CardBO) {

        self.cardBO = cardBO
        
        switchValue = cardBO.isCardOn()
        switchText = cardBO.isCardOn() ? Localizables.cards.key_cards_off_text : Localizables.cards.key_cards_on_text
        switchImage = cardBO.isCardOn() ? ConstantsImages.Card.on_off_switch_on : ConstantsImages.Card.on_off_switch_off
        accesibilityValue = cardBO.isCardOn() ? "1" : "0"
        
        switchStatus = !cardBO.isCardActivationsOnOff() ? false : cardBO.isCardOperative()
    }

}
