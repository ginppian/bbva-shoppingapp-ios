//
//  OnOffView.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 17/10/2017.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation

protocol OnOffViewDelegate: class {

    func configureSwitchStatus(withStatus status: Bool, withCard card: CardBO)
    func onOffViewHelpButtonPressed(_ onOffView: OnOffView)

}
class OnOffView: XibView {

    // MARK: IBOutlets

    @IBOutlet weak var onOffTitleLabel: UILabel!
    @IBOutlet weak var customSwitchButton: CustomSwitchView!
    @IBOutlet weak var helpButton: ExtraTouchAreaButton!

    var presenter = OnOffPresenter()

    weak var delegate: OnOffViewDelegate?

    private var switchValue: Bool = false

    var cardBO: CardBO!

    override func configureView() {

        super.configureView()

        presenter.view = self

        customSwitchButton.delegate = self
    }

    func configureSwitch(withOnOffData displayData: OnOffDisplayData) {

        cardBO = displayData.cardBO

        customSwitchButton.switchIsOn = displayData.switchValue

        onOffTitleLabel.text = displayData.switchText
        onOffTitleLabel.font = Tools.setFontMedium(size: 14)
        onOffTitleLabel.numberOfLines = 2
        onOffTitleLabel.lineBreakMode = .byWordWrapping

        customSwitchButton.accessibilityValue = displayData.accesibilityValue

        helpButton.setImage(SVGCache.image(forSVGNamed: ConstantsImages.Common.help_icon_on_off, color: .BBVALIGHTBLUE), for: .normal)
        helpButton.setImage(SVGCache.image(forSVGNamed: ConstantsImages.Common.help_icon_on_off, color: .MEDIUMBLUE), for: .highlighted)
        helpButton.setImage(SVGCache.image(forSVGNamed: ConstantsImages.Common.help_icon_on_off, color: .MEDIUMBLUE), for: .disabled)

    }

    @IBAction func helpButtonPressed(_ sender: Any) {
        delegate?.onOffViewHelpButtonPressed(self)
    }
}

extension OnOffView: OnOffViewProtocol {

    func showSwitchValue(onOffDisplayData displayOnOffData: OnOffDisplayData) {

        configureSwitch(withOnOffData: displayOnOffData)

        presenter.showViewStatus(onOffDisplayData: displayOnOffData)

    }

    func viewDisable(withSwitchValue switchValue: Bool) {

        onOffTitleLabel.textColor = UIColor.BBVAWHITE.withAlphaComponent(0.3)

        backgroundColor = .COREBLUE

        customSwitchButton.switchBackgroundColor = UIColor.NAVY.withAlphaComponent(0.3)

        customSwitchButton.switchEnabled = false
        customSwitchButton.switchIsOn = switchValue

        helpButton.isEnabled = false

    }

    func viewEnable() {

        onOffTitleLabel.textColor = .BBVAWHITE

        backgroundColor = .DARKCOREBLUE

        customSwitchButton.switchBackgroundColor = UIColor.NAVY.withAlphaComponent(1.0)

        customSwitchButton.switchEnabled = true

        helpButton.isEnabled = true

    }

}

extension OnOffView: CustomSwitchDelegate {

    func customSwitchView(_ customSwitchView: CustomSwitchView, changedValue newValue: Bool) {

        delegate?.configureSwitchStatus(withStatus: newValue, withCard: cardBO)

    }

}
