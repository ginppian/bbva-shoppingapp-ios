//
//  OnOffPresenter.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 18/10/2017.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation

class OnOffPresenter {

    weak var view: OnOffViewProtocol?

}

extension OnOffPresenter: OnOffPresenterProtocol {

    func showViewStatus(onOffDisplayData displayOnOffData: OnOffDisplayData) {

        if displayOnOffData.switchStatus {

            view?.viewEnable()

        } else {

            view?.viewDisable(withSwitchValue: displayOnOffData.switchValue)
        }

    }

}
