//
//  OnOffContract.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 17/10/2017.
//  Copyright © 2017 BBVA. All rights reserved.
//

protocol OnOffViewProtocol: ComponentViewProtocol {
    func showSwitchValue(onOffDisplayData displayOnOffData: OnOffDisplayData)
    func viewEnable()
    func viewDisable(withSwitchValue switchValue: Bool)
}

protocol OnOffPresenterProtocol: ComponentPresenterProtocol {
    func showViewStatus(onOffDisplayData displayOnOffData: OnOffDisplayData)
}
