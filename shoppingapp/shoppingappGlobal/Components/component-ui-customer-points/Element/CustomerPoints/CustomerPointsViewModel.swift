//
//  CustomerPointsViewModel.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 20/7/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit
import CellsNativeCore
import CellsNativeComponents

// MARK: - Model

public protocol CustomerPointsModelProtocol {
    var accumulatedPointsAmount: NSNumber? { get }
    var exchangeUnit: String? { get }
    var pointsToBeExpiredAmount: NSNumber? { get }
    var expirationDate: String? { get }
    var areRedeemable: Bool? { get }
}

public struct CustomerPointsModel: CustomerPointsModelProtocol {

    public var accumulatedPointsAmount: NSNumber?
    public var exchangeUnit: String?
    public var pointsToBeExpiredAmount: NSNumber?
    public var expirationDate: String?
    public var areRedeemable: Bool?

    init(accumulatedPointsAmount: NSNumber? = nil, exchangeUnit: String? = nil, pointsToBeExpiredAmount: NSNumber? = nil, expirationDate: String? = nil, areRedeemable: Bool? = nil) {
        self.accumulatedPointsAmount = accumulatedPointsAmount
        self.exchangeUnit = exchangeUnit
        self.pointsToBeExpiredAmount = pointsToBeExpiredAmount
        self.expirationDate = expirationDate
        self.areRedeemable = areRedeemable
    }
}

// MARK: - Display Model

public struct CustomerPoints {

    public let availablePoints: String?
    public let exchangeUnit: String?
    public let pointsToBeExpired: String?
    public let expirationDate: String?
    public let areRedeemable: Bool?
    public let uninformedData: Bool?

}

open class CustomerPointsViewModel: ViewModel {

    static let TAG: String = String(describing: CustomerPointsViewModel.self)

    static let ATTR_LOADING: String = "loading"
    static let ATTR_CUSTOMER_POINTS: String = "customerPoints"
    static let ATTR_NO_POINTS: String = "noPoints"
    static let ATTR_NO_DATA: String = "noData"
    public static let ACTION_SHOW_ERROR: ActionSpec<Void> = ActionSpec<Void>(id: "action-showError")
    public static let ACTION_REDEEM_BUTTON_PRESSED: ActionSpec<Void> = ActionSpec<Void>(id: "action-redeemButtonPressed")
    public static let ACTION_INFO_BUTTON_PRESSED: ActionSpec<Void> = ActionSpec<Void>(id: "action-infoButtonPressed")

    public var isLoading = false {
        didSet {
            setProperty(key: CustomerPointsViewModel.ATTR_LOADING, value: isLoading)
        }
    }

    public var customerPointsModel: CustomerPointsModelProtocol = CustomerPointsModel() {
        didSet {

            guard let points = customerPointsModel.accumulatedPointsAmount else {
                setProperty(key: CustomerPointsViewModel.ATTR_NO_DATA, value: Void())
                return
            }

            if points == 0.0 {
                setProperty(key: CustomerPointsViewModel.ATTR_NO_POINTS, value: Void())

            } else {

                let customerPoints = generateCustomerPoints(customerPointsModel)

                setProperty(key: CustomerPointsViewModel.ATTR_CUSTOMER_POINTS, value: customerPoints)
            }
        }
    }

    fileprivate func generateCustomerPoints(_ customerPointsModel: CustomerPointsModelProtocol) -> CustomerPoints {

        var uninformedData = false

        var availablePoints = ""

        if let points = customerPointsModel.accumulatedPointsAmount {
            availablePoints = formattingNumberToString(number: points)
        }

        var exchangeUnit = ""

        if let amount = customerPointsModel.exchangeUnit, !amount.isEmpty {
            exchangeUnit = amount
        } else {
            uninformedData = true
        }

        var pointsToBeExpired = ""

        if let amount = customerPointsModel.pointsToBeExpiredAmount {
            pointsToBeExpired = formattingNumberToString(number: amount)
        } else {
            uninformedData = true
        }

        let customerPoints = CustomerPoints(availablePoints: availablePoints, exchangeUnit: exchangeUnit, pointsToBeExpired: pointsToBeExpired, expirationDate: customerPointsModel.expirationDate, areRedeemable: customerPointsModel.areRedeemable, uninformedData: uninformedData)

        return customerPoints
    }

    fileprivate func formattingNumberToString(number: NSNumber) -> String {

        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        return formatter.string(from: number) ?? ""
    }

    // MARK: - Fire Actions

    open func showError() {

        fire(actionSpec: CustomerPointsViewModel.ACTION_SHOW_ERROR)
    }

    open func redeemButtonPressed() {

        fire(actionSpec: CustomerPointsViewModel.ACTION_REDEEM_BUTTON_PRESSED)
    }

    open func infoButtonPressed() {

        fire(actionSpec: CustomerPointsViewModel.ACTION_INFO_BUTTON_PRESSED)
    }

}
