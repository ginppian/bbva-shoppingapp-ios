//
//  CustomerPointsView.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 19/7/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit
import CellsNativeComponents
import Shimmer

open class CustomerPointsView: UIView {

    // MARK: IBOutlets
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var contentStackView: UIStackView!
    @IBOutlet weak var contentStackViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var contentStackViewBottomConstraint: NSLayoutConstraint!

    // MARK: IBOutlets Skeleton
    @IBOutlet weak var contentSkeletonStackView: UIStackView!

    @IBOutlet weak var skeletonView: UIView!

    @IBOutlet weak var circleShimmeringView: FBShimmeringView!
    @IBOutlet weak var oneShimmeringView: FBShimmeringView!
    @IBOutlet weak var twoShimmeringView: FBShimmeringView!
    @IBOutlet weak var threeShimmeringView: FBShimmeringView!
    @IBOutlet weak var fourShimmeringView: FBShimmeringView!

    // MARK: IBOutlets Points
    @IBOutlet weak var contentPointsStackView: UIStackView!

    @IBOutlet weak var pointsTopView: UIView!
    @IBOutlet weak var pointsImageView: UIImageView!
    @IBOutlet weak var availablePointsLabel: UILabel!
    @IBOutlet weak var exchangeUnitLabel: UILabel!
    @IBOutlet weak var exchangeUnitSkeletonImageView: UIImageView!
    @IBOutlet weak var pointsExpireView: UIView!
    @IBOutlet weak var pointsExpireLabel: UILabel!
    @IBOutlet weak var pointsExpireSkeletonImageView: UIImageView!
    @IBOutlet weak var pointsSeparatorView: UIView!
    @IBOutlet weak var pointsLineView: UIView!
    @IBOutlet weak var pointsInfoView: UIView!
    @IBOutlet weak var pointsInfoLabel: UILabel!
    @IBOutlet weak var pointsRedeemView: UIView!
    @IBOutlet weak var pointsRedeemButton: UIButton!

    // MARK: IBOutlets No Points
    @IBOutlet weak var contentNoPointsStackView: UIStackView!

    @IBOutlet weak var noPointsContentImageView: UIView!
    @IBOutlet weak var noPointsImageView: UIImageView!

    @IBOutlet weak var noPointsTitleView: UIView!
    @IBOutlet weak var noPointsTitleLabel: UILabel!

    @IBOutlet weak var noPointsSubtitleView: UIView!
    @IBOutlet weak var noPointsSubtitleLabel: UILabel!

    @IBOutlet weak var noPointsInfoView: UIView!
    @IBOutlet weak var noPointsInfoButton: UIButton!

    // MARK: Variables
    open var viewModel: CustomerPointsViewModel?
    private var isShowSkeleton = false

    // MARK: Constants

    private let skeletonImageEdgeInsetRight: CGFloat = 20
    private let contentStackViewTopConstraintDefaultValue: CGFloat = 20
    private let contentStackViewBottomConstraintDefaultValue: CGFloat = 30

    // MARK: Configuration Properties

    private var defaultBackgroundColor: UIColor = UIColor(red: 2.0 / 255.0, green: 132.0 / 255.0, blue: 132.0 / 255.0, alpha: 1.0)
    private var defaultContentColor: UIColor = UIColor(red: 0.0 / 255.0, green: 120.0 / 255.0, blue: 120.0 / 255.0, alpha: 1.0)
    private var defaultTextColor: UIColor = .white
    private var defaultTextFont: UIFont = UIFont.systemFont(ofSize: 16.0)
    private var defaultText: String = "Default Text"

    fileprivate let VALUE_ID: String = "value"
    fileprivate let TYPE_ID: String = "type"
    fileprivate let TYPE_RESOURCE_ID: String = "resource"

    fileprivate let BACKGROUND_COLOR_ID: String = "background-color"
    fileprivate let CONTENT_COLOR_ID: String = "content-color"
    fileprivate let SECONDARY_COLOR_ID: String = "secondary-color"
    fileprivate let TEXT_COLOR_ID: String = "text-color"
    fileprivate let IMAGES_ID: String = "images"
    fileprivate let FONTS_ID: String = "fonts"
    fileprivate let TEXTS_ID: String = "texts"

    //-----------------------------------------

    fileprivate let POINTS_IMAGE_ID: String = "points-image"
    fileprivate let NO_POINTS_IMAGE_ID: String = "noPoints-image"
    fileprivate let SKELETON_IMAGE_ID: String = "skeleton-image"
    fileprivate let INFO_IMAGE_ID: String = "info-image"

    fileprivate let LIGHT_FONT_ID: String = "light-font"
    fileprivate let MEDIUM_FONT_ID: String = "medium-font"
    fileprivate let BOOK_ITALIC_FONT_ID: String = "book-italic-font"
    fileprivate let BOLD_FONT_ID: String = "bold-font"
    fileprivate let BOOK_FONT_ID: String = "book-font"

    fileprivate let EXCHANGE_UNIT_LITERAL_ID: String = "exchangeUnit-literal"
    fileprivate let POINTS_EXPIRE_LITERAL_ID: String = "pointsExpire-literal"
    fileprivate let REDEEM_POINTS_INFO_LITERAL_ID: String = "redeemPointsInfo-literal"
    fileprivate let NO_REDEEM_POINTS_INFO_LITERAL_ID: String = "noRedeemPointsInfo-literal"
    fileprivate let REDEEM_BUTTON_LITERAL_ID: String = "redeemButton-literal"
    fileprivate let NO_POINTS_TITLE_LITERAL_ID: String = "noPointsTitle-literal"
    fileprivate let NO_POINTS_SUBTITLE_LITERAL_ID: String = "noPointsSubtitle-literal"
    fileprivate let NO_POINTS_BUTTON_LITERAL_ID: String = "noPointsButton-literal"

    //-----------------------------------------

    // MARK: Variables Properties -

    private var viewBackgroundColor: UIColor?
    private var viewContentColor: UIColor?
    private var viewSecondaryColor: UIColor?
    private var textColor: UIColor?

    private var pointsImage: UIImage?
    private var noPointsImage: UIImage?
    private var skeletonImage: UIImage?
    private var infoImage: UIImage?

    private var lightFont: UIFont?
    private var mediumFont: UIFont?
    private var bookItalicFont: UIFont?
    private var boldFont: UIFont?
    private var bookFont: UIFont?

    private var exchangeUnitLiteral: String?
    private var pointsExpireLiteral: String?
    private var redeemPointsInfoLiteral: String?
    private var noRedeemPointsInfoLiteral: String?
    private var redeemButtonLiteral: String?
    private var noPointsTitleLiteral: String?
    private var noPointsSubtitleLiteral: String?
    private var noPointsButtonLiteral: String?

    //-----------------------------------------

    public required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        translatesAutoresizingMaskIntoConstraints = false
    }

    override open func removeFromSuperview() {

        viewModel?.onCleared()
        super.removeFromSuperview()
    }

    override open func layoutSubviews() {

        contentView.frame.size.height = getHeightCustomerPointsView()
        frame.size = contentView.frame.size
    }

    @IBAction func pointsRedeemButtonPressed(_ sender: Any) {

        viewModel?.redeemButtonPressed()
    }

    @IBAction func noPointsInfoButtonPressed(_ sender: Any) {

        viewModel?.infoButtonPressed()
    }
}

// MARK: - Configuration Component UI -
extension CustomerPointsView {

    open func configureView(viewController: UIViewController) {

        backgroundColor = .clear
        contentView.backgroundColor = .clear

        configureViewAttributesDefault()

        viewModel = CellsViewModelProvider.of(id: tagNameUI, viewModelClass: CustomerPointsViewModel.self, container: viewController)
        configureViewWithProperties()
    }

    fileprivate func configureViewWithProperties() {

        viewModel?.newProperty(key: CustomerPointsViewModel.ATTR_LOADING, consumer: showSkeleton)
        viewModel?.newProperty(key: CustomerPointsViewModel.ATTR_CUSTOMER_POINTS, consumer: showCustomerPoints)
        viewModel?.newProperty(key: CustomerPointsViewModel.ATTR_NO_POINTS, consumer: showCustomerNoPoints)
        viewModel?.newProperty(key: CustomerPointsViewModel.ATTR_NO_DATA, consumer: showNoData)

        viewModel?.newProperty(key: BACKGROUND_COLOR_ID, consumer: configureBackgroundColorProperties)
        viewModel?.newProperty(key: CONTENT_COLOR_ID, consumer: configureContentColorProperties)
        viewModel?.newProperty(key: SECONDARY_COLOR_ID, consumer: configureSecondaryColorProperties)
        viewModel?.newProperty(key: TEXT_COLOR_ID, consumer: configureTextColorProperties)

        viewModel?.newProperty(key: IMAGES_ID, consumer: configureImagesProperties)
        viewModel?.newProperty(key: FONTS_ID, consumer: configureFontsProperties)
        viewModel?.newProperty(key: TEXTS_ID, consumer: configureTextsProperties)

        viewModel?.subscribeToFilledProperties(consumer: { _ in

            self.configureSkeleton()
            self.configurePointsView()
            self.configureNoPointsView()
        })
    }

    // MARK: Consumers for Properties

    private func getValueOfProperty(_ params: Any?, id: String) -> String? {

        if let properties = params as? [String: Any] {

            for property in properties.keys {

                if property == id, let valueToChange = properties[property] as? [String: Any], let value = valueOfProperty(params: valueToChange) {

                    return value
                }
            }
        }
        return nil
    }

    private func valueOfProperty(params: Any?) -> String? {

        if let property = params as? [String: Any], let value = property[VALUE_ID] as? String {

            return value
        }
        return nil
    }

    private func getTypeOfProperty(_ params: Any?, id: String) -> String? {

        if let properties = params as? [String: Any] {

            for property in properties.keys {

                if property == id, let valueToChange = properties[property] as? [String: Any], let value = typeOfProperty(params: valueToChange) {

                    return value
                }
            }
        }
        return nil
    }

    private func typeOfProperty(params: Any?) -> String? {

        if let property = params as? [String: Any], let value = property[TYPE_ID] as? String {

            return value
        }
        return nil
    }

    fileprivate func getBackgroundColor(_ params: Any?) -> UIColor {

        guard let backgroundColor = valueOfProperty(params: params) else {
            return defaultBackgroundColor
    }

        return UIColor.hexStringToUIColor(hex: backgroundColor)
    }

    fileprivate func getContentColor(_ params: Any?) -> UIColor {

        guard let contentColor = valueOfProperty(params: params) else {
            return defaultContentColor
        }

        return UIColor.hexStringToUIColor(hex: contentColor)
    }

    fileprivate func getSecondaryColor(_ params: Any?) -> UIColor {

        guard let secondaryColor = valueOfProperty(params: params) else {
            return defaultContentColor
    }

        return UIColor.hexStringToUIColor(hex: secondaryColor)
    }

    fileprivate func getTextColor(_ params: Any?) -> UIColor {

        guard let textColor = valueOfProperty(params: params) else {
            return defaultTextColor
    }

        return UIColor.hexStringToUIColor(hex: textColor)
    }

    fileprivate func getImage(_ params: Any?, _ idProperty: String) -> UIImage {

        guard let imageName = getValueOfProperty(params, id: idProperty), let image = UIImage(named: imageName) else {
            return UIImage()
                }

        return image
    }

    fileprivate func getTextFont(_ params: Any?, _ idProperty: String, _ fontSize: CGFloat) -> UIFont {

        guard let textFont = getValueOfProperty(params, id: idProperty), let font = UIFont(name: textFont, size: fontSize) else {
            return defaultTextFont
    }

        return font
    }

    fileprivate func getText(_ params: Any?, _ idProperty: String) -> String {

        guard let literal = getValueOfProperty(params, id: idProperty), let type = getTypeOfProperty(params, id: idProperty) else {
            return String()
    }

        return type == TYPE_RESOURCE_ID ? NSLocalizedString(literal, comment: "") : literal
    }

    // MARK: - Configure Properties

    fileprivate func configureBackgroundColorProperties(_ params: Any?) {

        viewBackgroundColor = getBackgroundColor(params)
    }

    fileprivate func configureContentColorProperties(_ params: Any?) {

        viewContentColor = getContentColor(params)
    }

    fileprivate func configureSecondaryColorProperties(_ params: Any?) {

        viewSecondaryColor = getSecondaryColor(params)
            }

    fileprivate func configureTextColorProperties(_ params: Any?) {

        textColor = getTextColor(params)
    }

    fileprivate func configureImagesProperties(_ params: Any?) {

        pointsImage = getImage(params, POINTS_IMAGE_ID)
        noPointsImage = getImage(params, NO_POINTS_IMAGE_ID)
        skeletonImage = getImage(params, SKELETON_IMAGE_ID)
        infoImage = getImage(params, INFO_IMAGE_ID)
    }

    fileprivate func configureFontsProperties(_ params: Any?) {

        lightFont = getTextFont(params, LIGHT_FONT_ID, 70.0)
        mediumFont = getTextFont(params, MEDIUM_FONT_ID, 14.0)
        bookItalicFont = getTextFont(params, BOOK_ITALIC_FONT_ID, 14.0)
        boldFont = getTextFont(params, BOLD_FONT_ID, 14.0)
        bookFont = getTextFont(params, BOOK_FONT_ID, 14.0)
    }

    fileprivate func configureTextsProperties(_ params: Any?) {

        exchangeUnitLiteral = getText(params, EXCHANGE_UNIT_LITERAL_ID)
        pointsExpireLiteral = getText(params, POINTS_EXPIRE_LITERAL_ID)
        redeemPointsInfoLiteral = getText(params, REDEEM_POINTS_INFO_LITERAL_ID)
        noRedeemPointsInfoLiteral = getText(params, NO_REDEEM_POINTS_INFO_LITERAL_ID)
        redeemButtonLiteral = getText(params, REDEEM_BUTTON_LITERAL_ID)
        noPointsTitleLiteral = getText(params, NO_POINTS_TITLE_LITERAL_ID)
        noPointsSubtitleLiteral = getText(params, NO_POINTS_SUBTITLE_LITERAL_ID)
        noPointsButtonLiteral = getText(params, NO_POINTS_BUTTON_LITERAL_ID)
    }
}

// MARK: - Private methods -
extension CustomerPointsView {

    private func getHeightCustomerPointsView() -> CGFloat {

        if isShowSkeleton == true {

            contentStackViewTopConstraint.constant = 0
            contentStackViewBottomConstraint.constant = 0

        } else {

            contentStackViewTopConstraint.constant = contentStackViewTopConstraintDefaultValue
            contentStackViewBottomConstraint.constant = contentStackViewBottomConstraintDefaultValue
        }

        contentStackView.setNeedsLayout()
        contentStackView.layoutIfNeeded()

        guard isShowSkeleton == false else {
            return contentStackView.frame.size.height
        }

        let topConstraint = contentStackViewTopConstraint.constant
        let bottomConstraint = contentStackViewBottomConstraint.constant

        return contentStackView.frame.size.height + topConstraint + bottomConstraint
    }

    private func configureViewAttributesDefault() {

        viewBackgroundColor = defaultBackgroundColor
        viewContentColor = defaultContentColor
        viewSecondaryColor = defaultContentColor
        textColor = defaultTextColor

        pointsImage = UIImage()
        noPointsImage = UIImage()
        skeletonImage = UIImage()
        infoImage = UIImage()

        lightFont = defaultTextFont
        mediumFont = defaultTextFont
        bookItalicFont = defaultTextFont
        boldFont = defaultTextFont
        bookFont = defaultTextFont

        exchangeUnitLiteral = defaultText
        pointsExpireLiteral = defaultText
        redeemPointsInfoLiteral = defaultText
        noRedeemPointsInfoLiteral = defaultText
        redeemButtonLiteral = defaultText
        noPointsTitleLiteral = defaultText
        noPointsSubtitleLiteral = defaultText
        noPointsButtonLiteral = defaultText
    }

    private func configureSkeleton() {

        circleShimmeringView.layer.cornerRadius = circleShimmeringView.frame.size.height / 2
        circleShimmeringView.backgroundColor = viewBackgroundColor

        let shimmeringCircleContentView = UIView(frame: CGRect(x: 0, y: 0, width: circleShimmeringView.frame.size.width, height: circleShimmeringView.frame.size.height))
        shimmeringCircleContentView.layer.cornerRadius = circleShimmeringView.frame.size.height / 2
        shimmeringCircleContentView.backgroundColor = viewContentColor

        circleShimmeringView.contentView = shimmeringCircleContentView

        skeletonImage = skeletonImage?.resizableImage(withCapInsets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: skeletonImageEdgeInsetRight))

        oneShimmeringView.backgroundColor = viewBackgroundColor
        twoShimmeringView.backgroundColor = viewBackgroundColor
        threeShimmeringView.backgroundColor = viewBackgroundColor
        fourShimmeringView.backgroundColor = viewBackgroundColor

        oneShimmeringView.contentView = UIImageView(image: skeletonImage)
        twoShimmeringView.contentView = UIImageView(image: skeletonImage)
        threeShimmeringView.contentView = UIImageView(image: skeletonImage)
        fourShimmeringView.contentView = UIImageView(image: skeletonImage)

        startShimmering(shimmering: false)
    }

    private func startShimmering(shimmering: Bool) {

        circleShimmeringView.isShimmering = shimmering
        oneShimmeringView.isShimmering = shimmering
        twoShimmeringView.isShimmering = shimmering
        threeShimmeringView.isShimmering = shimmering
        fourShimmeringView.isShimmering = shimmering
    }

    private func hideSkeleton() {

        isShowSkeleton = false
        contentSkeletonStackView.isHidden = true
        startShimmering(shimmering: false)
    }

    private func configurePointsView() {

        pointsImageView.image = pointsImage

        availablePointsLabel.textColor = textColor
        availablePointsLabel.font = lightFont

        exchangeUnitLabel.textColor = textColor
        exchangeUnitLabel.font = mediumFont
        exchangeUnitLabel.text = exchangeUnitLiteral

        exchangeUnitSkeletonImageView.isHidden = true
        exchangeUnitSkeletonImageView.image = skeletonImage

        pointsExpireLabel.textColor = textColor
        pointsExpireLabel.font = bookItalicFont
        pointsExpireLabel.text = pointsExpireLiteral

        pointsExpireSkeletonImageView.isHidden = true
        pointsExpireSkeletonImageView.image = skeletonImage

        pointsLineView.backgroundColor = viewSecondaryColor

        pointsInfoLabel.textColor = textColor
        pointsInfoLabel.font = bookItalicFont
        pointsInfoLabel.text = redeemPointsInfoLiteral

        pointsRedeemButton.setTitleColor(textColor, for: .normal)
        pointsRedeemButton.titleLabel?.font = boldFont
        pointsRedeemButton.setTitle(redeemButtonLiteral, for: .normal)

        pointsRedeemButton.setBackgroundColor(color: viewContentColor, forUIControlState: .normal)
        pointsRedeemButton.setBackgroundColor(color: viewContentColor?.withAlphaComponent(0.5), forUIControlState: .selected)
        pointsRedeemButton.setBackgroundColor(color: viewContentColor?.withAlphaComponent(0.5), forUIControlState: .highlighted)
    }

    private func configureNoPointsView() {

        noPointsImageView.image = noPointsImage

        noPointsTitleLabel.textColor = textColor
        noPointsTitleLabel.font = mediumFont?.withSize(16.0)
        noPointsTitleLabel.text = noPointsTitleLiteral

        noPointsSubtitleLabel.textColor = textColor
        noPointsSubtitleLabel.font = bookFont
        noPointsSubtitleLabel.text = noPointsSubtitleLiteral

        noPointsInfoButton.setTitleColor(textColor, for: .normal)
        noPointsInfoButton.setTitleColor(textColor?.withAlphaComponent(0.3), for: .selected)
        noPointsInfoButton.setTitleColor(textColor?.withAlphaComponent(0.3), for: .highlighted)
        noPointsInfoButton.titleLabel?.font = mediumFont
        noPointsInfoButton.setTitle(noPointsButtonLiteral, for: .normal)

        if let buttonImage = infoImage {

            let buttonTintableImage = buttonImage.withRenderingMode(.alwaysTemplate)
            let buttonSelectedImage = buttonTintableImage.tint(with: textColor?.withAlphaComponent(0.3))

            noPointsInfoButton.setImage(buttonTintableImage, for: .normal)
            noPointsInfoButton.setImage(buttonSelectedImage, for: .highlighted)
            noPointsInfoButton.setImage(buttonSelectedImage, for: .selected)

            noPointsInfoButton.tintColor = textColor

            let spacing = 6.0
            noPointsInfoButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: CGFloat(spacing))
            noPointsInfoButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: CGFloat(spacing), bottom: 0, right: 0)
        }
    }
}

// MARK: - Open methods -
extension CustomerPointsView {

    var preferredContentHeight: CGFloat {
            return getHeightCustomerPointsView()
        }

    open func showSkeleton(_ params: Bool?) {

        isShowSkeleton = true
        contentSkeletonStackView.isHidden = false
        startShimmering(shimmering: true)

        contentPointsStackView.isHidden = true
        contentNoPointsStackView.isHidden = true

        contentView.backgroundColor = viewBackgroundColor
    }

    open func showCustomerPoints(_ params: CustomerPoints?) {

        hideSkeleton()

        contentPointsStackView.isHidden = false
        contentNoPointsStackView.isHidden = true

        contentView.backgroundColor = viewBackgroundColor

        availablePointsLabel.text = params?.availablePoints

        if let exchangeUnit = params?.exchangeUnit, !exchangeUnit.isEmpty, let exchangeUnitLiteral = exchangeUnitLiteral, !exchangeUnitLiteral.isEmpty {

            exchangeUnitSkeletonImageView.isHidden = true
            exchangeUnitLabel.isHidden = false
            exchangeUnitLabel.text = String(format: exchangeUnitLiteral, exchangeUnit)

        } else {

            exchangeUnitSkeletonImageView.isHidden = false
            exchangeUnitLabel.isHidden = true
        }

        if let pointsExpire = params?.pointsToBeExpired, !pointsExpire.isEmpty, let expirationDate = params?.expirationDate, !expirationDate.isEmpty, let pointsExpireLiteral = pointsExpireLiteral, !pointsExpireLiteral.isEmpty {

            pointsExpireSkeletonImageView.isHidden = true
            pointsExpireLabel.isHidden = false
            pointsExpireLabel.text = String(format: pointsExpireLiteral, pointsExpire, expirationDate)

        } else {

            pointsExpireSkeletonImageView.isHidden = false
            pointsExpireLabel.isHidden = true
        }

        if let areRedeemable = params?.areRedeemable, areRedeemable {

            pointsRedeemView.isHidden = false
            pointsInfoLabel.text = redeemPointsInfoLiteral

        } else {

            pointsRedeemView.isHidden = true
            pointsInfoLabel.text = noRedeemPointsInfoLiteral
        }

        if let uninformedData = params?.uninformedData, uninformedData {

            viewModel?.showError()
        }
    }

    open func showCustomerNoPoints(_ params: Void?) {

        hideSkeleton()

        contentNoPointsStackView.isHidden = false
        contentPointsStackView.isHidden = true

        contentView.backgroundColor = viewBackgroundColor
    }

    open func showNoData(_ params: Void?) {

        hideSkeleton()

        contentNoPointsStackView.isHidden = false
        contentPointsStackView.isHidden = true

        contentView.backgroundColor = viewBackgroundColor
    }
}
