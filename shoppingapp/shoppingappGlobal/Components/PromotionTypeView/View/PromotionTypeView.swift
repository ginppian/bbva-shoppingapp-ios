//
//  PromotionTypeView.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 09/01/2018.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit

class PromotionTypeView: XibView {

    static let inclinationMargin: CGFloat = 10.0
    static let inclinedViewHeight: CGFloat = 24.0
    var inclinedViewWidth: CGFloat = 0.0

    var shapeLayer = CAShapeLayer()

    // MARK: IBOutlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var titleLabelWidth: NSLayoutConstraint!

    override func configureView() {

        super.configureView()

        configureTagView()

    }

    func configureTagView() {

        backgroundColor = .clear

        layer.addSublayer(shapeLayer)

        titleLabel.textColor = .BBVAWHITE
        titleLabel.font = Tools.setFontMedium(size: 12)
        titleLabel.textAlignment = .center
        titleLabel.lineBreakMode = .byTruncatingTail

    }
    func configureTagView(withDisplayData displayData: PromotionTypeDisplayData) {

        inclinedViewWidth = displayData.promotionTypeViewWidth
        titleLabelWidth.constant = displayData.promotionTypeLabelWidth
        shapeLayer.path = createTrackingPath().cgPath
        shapeLayer.fillColor = displayData.promotionTypeBackgroundColor.cgColor

        titleLabel.text = displayData.promotionTypeText

        if inclinedViewWidth <= 0 {
            shapeLayer.path = UIBezierPath().cgPath
            titleLabel.text = ""
        }
    }

    func createTrackingPath() -> UIBezierPath {

        let pathTracking = UIBezierPath()
        pathTracking.move(to: CGPoint(x: 0.0, y: PromotionTypeView.inclinedViewHeight))
        pathTracking.addLine(to: CGPoint(x: PromotionTypeView.inclinationMargin, y: 0.0))
        pathTracking.addLine(to: CGPoint(x: inclinedViewWidth, y: 0.0))
        pathTracking.addLine(to: CGPoint(x: inclinedViewWidth, y: PromotionTypeView.inclinedViewHeight))
        pathTracking.close()

        return pathTracking

    }

    func configurePromotionType(withPromotionTypeDisplayData displayData: PromotionTypeDisplayData) {
        configureTagView(withDisplayData: displayData)

    }

}
