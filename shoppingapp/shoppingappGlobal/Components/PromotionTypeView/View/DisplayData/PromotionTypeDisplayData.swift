//
//  PromotionTypeDisplayData.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 09/01/2018.
//  Copyright © 2018 BBVA. All rights reserved.
//

struct PromotionTypeDisplayData {

    var promotionTypeText: String?
    var promotionTypeBackgroundColor: UIColor
    var promotionTypeViewWidth: CGFloat
    var promotionTypeLabelWidth: CGFloat

    let widthForDiscount: CGFloat = 98.0
    let widthForPointsAndMonths: CGFloat = 77.0

    init(fromPromotion promotionBO: PromotionBO) {

        promotionTypeText = promotionBO.promotionType.name.uppercased()

        if promotionBO.isDiscountPromotionType() {

            promotionTypeBackgroundColor = .BBVADARKPINK
            promotionTypeViewWidth = widthForDiscount
            promotionTypeLabelWidth = widthForDiscount - 20

        } else if promotionBO.isToMonthPromotionType() {

            promotionTypeBackgroundColor = .BBVAMEDIUMYELLOW
            promotionTypeViewWidth = widthForPointsAndMonths
            promotionTypeLabelWidth = widthForPointsAndMonths - 20

        } else if promotionBO.isPointsPromotionType() {

            promotionTypeBackgroundColor = .DARKAQUA
            promotionTypeViewWidth = widthForPointsAndMonths
            promotionTypeLabelWidth = widthForPointsAndMonths - 20

        } else {
            promotionTypeBackgroundColor = .clear
            promotionTypeText = nil
            promotionTypeViewWidth = 0.0
            promotionTypeLabelWidth = 0.0

        }
    }

}
