//
//  ButtonsBarView.swift
//  shoppingapp
//
//  Created by jesus.martinez on 11/10/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation

protocol ButtonsBarViewDelegate: class {

    func buttonBarPressed(_ buttonsBarView: ButtonsBarView, withDisplayItem displayItem: ButtonBarDisplayData)
}

class ButtonsBarView: XibView {

    @IBOutlet weak var buttonsBarCollectionView: UICollectionView!

    var collectionViewLayout: UICollectionViewFlowLayout!
    var displayButtonsBarDisplayData: ButtonsBarDisplayData?

    //Configuration Collection
    let itemSizeCell = CGSize(width: 70, height: 104)
    var itemSectionInsetDefaultMaxCellPerRow = UIEdgeInsets(top: 0.0, left: 25.0, bottom: 0.0, right: 25.0)
    var itemSectionInset = UIEdgeInsets(top: 0.0, left: 25.0, bottom: 0.0, right: 25.0)
    var itemMinimumLineSpacing: CGFloat = 10.0
    var cellsPerRow: Int = 0
    var dynamicWidthByScreen = true
    let maxCellsPerScreenWidth: Int = 4

    weak var delegate: ButtonsBarViewDelegate?

    override func configureView() {

        super.configureView()

        setupCollectionView()
        setupCollectionViewFlowLayout()
    }

    func setupCollectionView() {

        buttonsBarCollectionView.dataSource = self
        buttonsBarCollectionView.delegate = self
        buttonsBarCollectionView.backgroundColor = .MEDIUMBLUE
        buttonsBarCollectionView.allowsMultipleSelection = false
        buttonsBarCollectionView.scrollsToTop = false
        buttonsBarCollectionView.alwaysBounceVertical = false
        buttonsBarCollectionView.alwaysBounceHorizontal = false
        buttonsBarCollectionView.isPagingEnabled = false
        buttonsBarCollectionView.showsHorizontalScrollIndicator = false
        buttonsBarCollectionView.showsVerticalScrollIndicator = false
        buttonsBarCollectionView.delaysContentTouches = false

        buttonsBarCollectionView.register(UINib(nibName: ButtonBarCellView.identifier, bundle: nil), forCellWithReuseIdentifier: ButtonBarCellView.identifier)
    }

    func setupCollectionViewFlowLayout() {

        collectionViewLayout = buttonsBarCollectionView.collectionViewLayout as? UICollectionViewFlowLayout
        // Set scrollDirection horizontal produces crash sometimes in iOS9, avoid to set it.
        collectionViewLayout.sectionInset = itemSectionInset
        collectionViewLayout.minimumLineSpacing = itemMinimumLineSpacing
        collectionViewLayout.itemSize = itemSizeCell
    }

    func getItemSize(_ collectionView: UICollectionView, flowLayout: UICollectionViewFlowLayout) -> CGSize {

        var itemSize = CGSize.zero
        let marginsAndInsets = itemSectionInset.left + itemSectionInset.right + itemMinimumLineSpacing * CGFloat(cellsPerRow - 1)
        let itemWidth = ((collectionView.bounds.size.width - marginsAndInsets) / CGFloat(cellsPerRow)).rounded(.down)
        itemSize = CGSize(width: itemWidth, height: flowLayout.itemSize.height)
        return itemSize
    }

    @objc func collectionViewCellButtonPressed(_ button: UIButton) {

        if let displayItem = displayButtonsBarDisplayData?.buttons[button.tag] {
            delegate?.buttonBarPressed(self, withDisplayItem: displayItem)
        }
    }
}

extension ButtonsBarView: ButtonsBarViewProtocol {

    func show(displayButtonsBarDisplayData: ButtonsBarDisplayData) {

        self.displayButtonsBarDisplayData = displayButtonsBarDisplayData
        cellsPerRow = displayButtonsBarDisplayData.buttons.count

        itemSectionInsetDefaultMaxCellPerRow = UIEdgeInsets(top: 0.0, left: itemSizeCell.width / 1.5, bottom: 20.0, right: itemSizeCell.width / 1.5)

        cellsPerRow > maxCellsPerScreenWidth ? (dynamicWidthByScreen = false) : (dynamicWidthByScreen = true)

        buttonsBarCollectionView.reloadData()
    }
}

// MARK: - UICollectionView DataSource & Delegate

extension ButtonsBarView: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return displayButtonsBarDisplayData?.buttons.count ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        if let displayItem = displayButtonsBarDisplayData?.buttons[indexPath.row] {

            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: ButtonBarCellView.identifier, for: indexPath) as? ButtonBarCellView {

                cell.configure(withDisplayItem: displayItem)
                cell.optionButton.tag = indexPath.row
                cell.optionButton.addTarget(self, action: #selector(collectionViewCellButtonPressed(_:)), for: .touchUpInside)

                return cell
            }
        }
        return UICollectionViewCell()
    }
}

extension ButtonsBarView: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let flowLayout: UICollectionViewFlowLayout = (collectionViewLayout as? UICollectionViewFlowLayout)!
        var itemSize = CGSize.zero

        if !dynamicWidthByScreen {

            itemSize = CGSize(width: flowLayout.itemSize.width, height: flowLayout.itemSize.height)

            itemSectionInset = itemSectionInsetDefaultMaxCellPerRow
            flowLayout.sectionInset = itemSectionInset

        } else {

            itemSize = getItemSize(collectionView, flowLayout: flowLayout)

            if itemSize.width < itemSizeCell.width {

                let sizeScreen = UIScreen.main.bounds
                let spacing = (sizeScreen.width - (CGFloat(cellsPerRow) * itemSizeCell.width)) / (CGFloat(cellsPerRow) + 1.0)

                itemSectionInset.left = spacing
                itemSectionInset.right = spacing
                flowLayout.sectionInset = itemSectionInset
                itemMinimumLineSpacing = spacing
                flowLayout.minimumLineSpacing = itemMinimumLineSpacing

                itemSize = getItemSize(collectionView, flowLayout: flowLayout)
            }
        }

        return itemSize
    }
}
