//
//  File.swift
//  shoppingapp
//
//  Created by jesus.martinez on 13/10/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

struct ButtonBarDisplayData {

    let imageNamed: String
    let titleKey: String
    let optionKey: ButtonBarDisplayOption
}

public enum ButtonBarDisplayOption {
    case callBBVA
    case limits
    case block
    case cancel
}

extension ButtonBarDisplayData: Equatable {

    public static func == (lhs: ButtonBarDisplayData, rhs: ButtonBarDisplayData) -> Bool {

        return lhs.imageNamed == rhs.imageNamed
            && lhs.titleKey == rhs.titleKey

    }

}
