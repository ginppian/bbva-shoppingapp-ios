//
//  ButtonsBarDisplayData.swift
//  shoppingapp
//
//  Created by jesus.martinez on 13/10/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

struct ButtonsBarDisplayData {

    var buttons: [ButtonBarDisplayData]

    init() {

        buttons = [ButtonBarDisplayData]()

    }

    mutating func add(button: ButtonBarDisplayData) {

        buttons.append(button)

    }

    static func generateButtonsBarDisplayData(fromCardBO cardBO: CardBO) -> ButtonsBarDisplayData {

        var buttonsBarDisplayData = ButtonsBarDisplayData()

        if cardBO.isCardOperative() && cardBO.isBlockable() {
            buttonsBarDisplayData.add(button: ButtonBarDisplayData(imageNamed: ConstantsImages.Card.blockcard, titleKey: Localizables.cards.key_card_block_card_text, optionKey: .block))
        }

        if !cardBO.isInoperative() && !cardBO.isPendingEmbossing() && !cardBO.isPendingDelivery() {
            buttonsBarDisplayData.add(button: ButtonBarDisplayData(imageNamed: ConstantsImages.Card.limits, titleKey: Localizables.cards.key_cards_set_up_limits_text, optionKey: .limits))
        }

        if !cardBO.isBlocked() && cardBO.isCancellable() {
            buttonsBarDisplayData.add(button: ButtonBarDisplayData(imageNamed: ConstantsImages.Card.blockcard, titleKey: Localizables.cards.key_card_cancel, optionKey: .cancel))
        }

        return buttonsBarDisplayData

    }

    static func generateButtonsBarOnTransactionDisplayData() -> ButtonsBarDisplayData {

        var buttonsBarDisplayData = ButtonsBarDisplayData()

        buttonsBarDisplayData.add(button: ButtonBarDisplayData(imageNamed: ConstantsImages.Common.telephone_bbva_line, titleKey: Localizables.common.key_bbva_line_text, optionKey: .callBBVA))

        return buttonsBarDisplayData
    }
}
