//
//  ButtonBarCellView.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 10/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class ButtonBarCellView: UICollectionViewCell {

    static let identifier = "ButtonBarCellView"
    @IBOutlet weak var optionButton: CircularButton!

    let normalColor = UIColor.BBVAWHITE
    let highlightedColor = UIColor.COREBLUE
    let backgroundNormalColor = UIColor.BBVAWHITE.withAlphaComponent(0.1)
    let backgroundHighlightedColor = UIColor.BBVA600.withAlphaComponent(0.1)

    override func awakeFromNib() {

        super.awakeFromNib()
    }

    func configure(withDisplayItem displayItem: ButtonBarDisplayData) {

        contentView.backgroundColor = UIColor.clear
        configure(circularButton: optionButton, withImageNamed: displayItem.imageNamed, andTitle: displayItem.titleKey)
    }

    func configure(circularButton: CircularButton, withImageNamed imageNamed: String, andTitle title: String) {

        if !imageNamed.isEmpty {

            circularButton.image = ImageUtilities.image(with: SVGCache.image(forSVGNamed: imageNamed, color: normalColor), width: 36, height: 36)
            circularButton.imageHighlighted = SVGCache.image(forSVGNamed: imageNamed, color: highlightedColor)
        }

        circularButton.normalColor = normalColor
        circularButton.highlightedColor = highlightedColor
        circularButton.backgroundNormalColor = backgroundNormalColor
        circularButton.backgroundHighlightedColor = backgroundHighlightedColor

        circularButton.numberOfLines = 2
        circularButton.bottomTitleText = title

        circularButton.bottomTitleLabel?.font = Tools.setFontMedium(size: 12)
    }
}
