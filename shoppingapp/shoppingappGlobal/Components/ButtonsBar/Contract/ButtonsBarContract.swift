//
//  ButtonsBarContract.swift
//  shoppingapp
//
//  Created by jesus.martinez on 13/10/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

protocol ButtonsBarViewProtocol: ComponentViewProtocol {
    func show(displayButtonsBarDisplayData: ButtonsBarDisplayData)
}

protocol ButtonsBarPresenterProtocol: ComponentPresenterProtocol {
}
