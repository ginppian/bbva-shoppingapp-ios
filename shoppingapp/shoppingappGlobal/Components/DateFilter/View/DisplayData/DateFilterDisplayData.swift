//
//  DateFilterDisplayData.swift
//  shoppingapp
//
//  Created by jesus.martinez on 28/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation

struct DateFilterDisplayData {

    var minDate: Date?
    var maxDate: Date?
    var title: String
    var dateSelectedTitle: String
    var dateSelected: String?

}

extension DateFilterDisplayData: Equatable {

    public static func == (lhs: DateFilterDisplayData, rhs: DateFilterDisplayData) -> Bool {

        return lhs.minDate == rhs.minDate
            && lhs.maxDate == rhs.maxDate
            && lhs.title == rhs.title
            && lhs.dateSelectedTitle == rhs.dateSelectedTitle
            && lhs.dateSelected == rhs.dateSelected

    }

}
