//
//  DateFilterView.swift
//  shoppingapp
//
//  Created by jesus.martinez on 26/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit

protocol DateFilterViewDelegate: class {

    func dateFilterView(_ dateFilterView: DateFilterView, didChangeDate newDate: Date)
    func dateFilterViewStartEditing(_ dateFilterView: DateFilterView)
    func dateFilterViewFinishEditing(_ dateFilterView: DateFilterView)

}

class DateFilterView: XibView {

    // MARK: Constants

    static let defaultHeight = 78

    private static let widthPicker = Int(UIScreen.main.bounds.width)
    private static let heightPicker = 206

    // MARK: variables

    var presenter = DateFilterPresenter()

    weak var delegate: DateFilterViewDelegate?

    // MARK: @IBOutlet's
    @IBOutlet weak var calendarImageView: UIImageView!
    @IBOutlet weak var titleDateLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!

    let hiddenTextField: UITextField! = UITextField(frame: .zero)
    let datePicker: UIDatePicker! = UIDatePicker(frame: CGRect(x: 0, y: 0, width: DateFilterView.widthPicker, height: DateFilterView.heightPicker))

    // MARK: - Private Helper Methods

    override func configureView() {

        super.configureView()

        titleDateLabel.font = Tools.setFontBook(size: 16)
        dateLabel.font = Tools.setFontBold(size: 14)

        addTapGesture()

        addSubview(hiddenTextField)

        hiddenTextField.delegate = self

        configurePicker()

        presenter.view = self
        presenter.onConfigureView()

    }

    private func configurePicker() {

        datePicker.datePickerMode = .date
        datePicker.backgroundColor = .BBVAWHITE
        datePicker.addTarget(self, action: #selector(datePickerValueChanged), for: .valueChanged)

        hiddenTextField.inputView = datePicker
        hiddenTextField.addDoneOnKeyboardWithTarget(self, action: #selector(doneAction))

    }

    private func addTapGesture() {

        let tapGesture = UITapGestureRecognizer()

        tapGesture.addTarget(self, action: #selector(showCalendarAction))
        tapGesture.numberOfTouchesRequired = 1
        tapGesture.numberOfTapsRequired = 1
        addGestureRecognizer(tapGesture)

    }

    @objc func showCalendarAction() {

        presenter.onShowCalendarAction()

    }

    @objc func datePickerValueChanged(sender: UIDatePicker) {

        presenter.onSelect(date: sender.date)

    }

    @objc func doneAction(_ sender: AnyObject?) {

        presenter.onSelect(date: datePicker.date)
        presenter.endEditing()
        hiddenTextField.resignFirstResponder()

    }

}

extension DateFilterView: DateFilterViewProtocol {

    func showNormalState() {

        calendarImageView.image = SVGCache.image(forSVGNamed: ConstantsImages.Common.calendar_icon, color: .BBVA400)
        titleDateLabel.textColor = .BBVA500
        dateLabel.textColor = .MEDIUMBLUE

    }

    func showSelectedState() {

        calendarImageView.image = SVGCache.image(forSVGNamed: ConstantsImages.Common.calendar_icon, color: .COREBLUE)
        titleDateLabel.textColor = .COREBLUE
        dateLabel.textColor = .COREBLUE

    }

    func updateView(withDisplayData displayData: DateFilterDisplayData) {

        datePicker.minimumDate = displayData.minDate
        datePicker.maximumDate = displayData.maxDate
        titleDateLabel.text = displayData.title
        dateLabel.text = displayData.dateSelected ?? displayData.dateSelectedTitle

    }

    func updateDateLabel(withDate date: String) {

        dateLabel.text = date

    }

    func showCalendar() {

        hiddenTextField.becomeFirstResponder()

    }

    func update(witMinimumDate minimumDate: Date) {

        datePicker.minimumDate = minimumDate

    }

    func update(witMaximum maximumDate: Date) {

        datePicker.maximumDate = maximumDate

    }

    func informDelegateStartEditing() {

        delegate?.dateFilterViewStartEditing(self)

    }

    func informDelegateEndEditing() {

        delegate?.dateFilterViewFinishEditing(self)

    }

    func informDelegate(newDateSelected: Date) {

        delegate?.dateFilterView(self, didChangeDate: newDateSelected)

    }

}

extension DateFilterView: UITextFieldDelegate {

    func textFieldDidBeginEditing(_ textField: UITextField) {

        presenter.startEditing()

    }

    func textFieldDidEndEditing(_ textField: UITextField) {

        presenter.endEditing()

    }

}
