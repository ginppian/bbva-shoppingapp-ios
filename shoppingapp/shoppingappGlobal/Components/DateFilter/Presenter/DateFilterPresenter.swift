//
//  DateFilterPresenter.swift
//  shoppingapp
//
//  Created by jesus.martinez on 28/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit

class DateFilterPresenter {

    weak var view: DateFilterViewProtocol?

    var displayData: DateFilterDisplayData? {

        didSet {

            onConfigureView()

        }

    }

}

extension DateFilterPresenter: DateFilterPresenterProtocol {

    func onConfigureView() {

        view?.showNormalState()

        if let displayData = displayData {
            view?.updateView(withDisplayData: displayData)

        }

    }

    func onShowCalendarAction() {

        view?.showCalendar()

    }

    func onSelect(date: Date) {

        view?.updateDateLabel(withDate: date.string(format: Date.DATE_FORMAT_DAY_MONTH_YEAR).capitalized)
        view?.informDelegate(newDateSelected: date)

    }

    func startEditing() {

        view?.showSelectedState()
        view?.informDelegateStartEditing()

    }

    func endEditing() {

        view?.showNormalState()
        view?.informDelegateEndEditing()

    }

    func present(minimumDate: Date) {

        view?.update(witMinimumDate: minimumDate)

    }

    func present(maximumDate: Date) {

        view?.update(witMaximum: maximumDate)

    }

}
