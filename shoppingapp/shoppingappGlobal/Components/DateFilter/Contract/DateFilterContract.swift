//
//  DateFilterContract.swift
//  shoppingapp
//
//  Created by jesus.martinez on 28/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

protocol DateFilterViewProtocol: ComponentViewProtocol {

    func showNormalState()
    func showSelectedState()
    func showCalendar()
    func updateView(withDisplayData displayData: DateFilterDisplayData)
    func updateDateLabel(withDate date: String)
    func update(witMinimumDate minimumDate: Date)
    func update(witMaximum maximumDate: Date)

    func informDelegateStartEditing()
    func informDelegateEndEditing()
    func informDelegate(newDateSelected: Date)

}

protocol DateFilterPresenterProtocol: ComponentPresenterProtocol {

    func onConfigureView()
    func onShowCalendarAction()
    func onSelect(date: Date)
    func startEditing()
    func endEditing()
    func present(minimumDate: Date)
    func present(maximumDate: Date)

}
