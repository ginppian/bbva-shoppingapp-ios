//
//  ActivateCardUseCase.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 10/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import CellsNativeComponents

class ActivateCardUseCase: UseCaseAsync<Bool, CardActivateDTO> {

    private let activateCardRepository: ActivateCardRepository

    init(activateCardRepository: ActivateCardRepository) {

        self.activateCardRepository = activateCardRepository
        super.init()
    }

    override func call(params: CardActivateDTO?, success: @escaping (_ result: Bool) -> Void, error: @escaping (_ error: Error) -> Void) {

        if let params = params {
            self.activateCardRepository.activateCard(params: params, success: success, error: error)
        } else {
            DLog(message: "Error no params")
            error(ServiceError.GenericErrorEntity(error: ErrorEntity(message: "Error no params")))
        }
    }
}
