//
//  ActivateCardStore.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 9/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import CellsNativeComponents
import BBVA_Network

open class ActivateCardStore: NetworkStore {

    public static let ID: String = "component-data-manager-activate-card-store"

    public static let ON_ACTIVATE_SUCCESS = ActionSpec<Bool>(id: "on-activate-success")
    static let ON_ACTIVATE_ERROR = ActionSpec<ErrorBO>(id: "on-activate-error")
    static let ON_ACTIVATE_WRONG_CVV_ERROR = ActionSpec<Void>(id: "on-activate-wrong-cvv-error")

    var host: String?
    var countryCode: String?

    fileprivate var activateCardUseCase: ActivateCardUseCase?
    fileprivate var activateCardWithCVVUseCase: ActivateCardWithCVVUseCase?
    fileprivate var worker: NetworkWorker

    public init(worker: NetworkWorker, host: String? = nil) {

        self.worker = worker
        self.host = host

        super.init(ActivateCardStore.ID)

        guard host == nil || (host?.isEmpty)! else {
            return
        }

        configureStoreWithProperties()
    }

    public required init() {
        
        fatalError("init() has not been implemented")
    }

    fileprivate func configureStoreWithProperties() {

        newProperty(key: "host") { [weak self] (value: Any?) in

            if let value = value {
                self?.host = String(describing: value)
            }
        }
        newProperty(key: "country") { [weak self] (value: Any?) in

            if let value = value {
                self?.countryCode = String(describing: value)
            }
        }
    }

    public func updateCardActivation(cardActivateDTO: CardActivateDTO) {

        let useCaseCallback = UseCase<Bool, CardActivateDTO>.UseCaseCallback<Bool>(onUseCaseComplete: { result in
            self.fire(actionSpec: ActivateCardStore.ON_ACTIVATE_SUCCESS, value: result)
        }, onUseCaseError: self.onUseCaseError)

        self.activateCardUseCase?.execute(params: cardActivateDTO, useCaseCallback: useCaseCallback)
    }
    
    public func updateCardActivationWithCVV(cardActivateWithCVVDTO: CardActivateWithCVVDTO) {
        
        let useCaseCallback = UseCase<Bool, CardActivateWithCVVDTO>.UseCaseCallback<Bool>(onUseCaseComplete: { result in
            self.fire(actionSpec: ActivateCardStore.ON_ACTIVATE_SUCCESS, value: result)
        }, onUseCaseError: self.onUseCaseError)
        
        self.activateCardWithCVVUseCase?.execute(params: cardActivateWithCVVDTO, useCaseCallback: useCaseCallback)
    }

    override open func onPropertiesBounds() {

        guard let host = host else {
            return
        }

        let activateCardRepository: ActivateCardRepository = ActivateCardRepositoryImpl(baseURL: host, networkWorker: worker)
        self.activateCardUseCase = ActivateCardUseCase(activateCardRepository: activateCardRepository)
        self.activateCardWithCVVUseCase = ActivateCardWithCVVUseCase(activateCardRepository: activateCardRepository)
    }

    override open func dispose() {
        
        activateCardUseCase?.dispose()
        activateCardWithCVVUseCase?.dispose()
        super.dispose()
    }

    override public func onUseCaseError(error: Error) {
        
        if let serviceError = error as? ServiceError {

            switch serviceError {
            case .GenericErrorEntity(let errorEntity):
                let errorBVABO = ErrorBO(error: errorEntity)
                self.fire(actionSpec: ActivateCardStore.ON_ACTIVATE_ERROR, value: errorBVABO)
            default:
                break
            }
        } else if let activateError = error as? ActivateCardWithCVVWError {

            switch activateError {
            case .WrongCVVError:
                self.fire(actionSpec: ActivateCardStore.ON_ACTIVATE_WRONG_CVV_ERROR, value: Void())
            }
        }
    }

}
