//
//  NetworkStore.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 9/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import CellsNativeComponents
import BBVA_Network

open class NetworkStore: Store {

    public static let CONNECTION_ERROR = ActionSpec<Response>(id: "connection-error-event")
    public static let NOT_CONNECTION_AVAILABLE = ActionSpec<Void>(id: "not-connection-available")
    public static let UNKNOWN_EXCEPTION = ActionSpec<Error>(id: "unknown-exception-event")

    public func onUseCaseError(error: Error) {

        print("onUseCaseError: \(error)")

        let networkResponse = Response()
        networkResponse.error = error

        self.fire(actionSpec: NetworkStore.CONNECTION_ERROR, value: networkResponse)

    }

}
