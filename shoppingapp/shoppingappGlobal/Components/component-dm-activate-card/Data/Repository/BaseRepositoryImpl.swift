//
//  BaseRepositoryImpl.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 10/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import ObjectMapper
import BBVA_Network

class BaseRepositoryImpl {

    var networkWorker: NetworkWorker
    var baseURL: String
    var countryCode: String?

    init(baseURL: String, countryCode: String? = nil, networkWorker: NetworkWorker) {

        self.baseURL = baseURL
        self.countryCode = countryCode
        self.networkWorker = networkWorker

    }

    func serialize<T: Mappable>(type: T.Type, body: String) -> T {

        return Mapper<T>().map(JSONString: body)!

    }

    func createRequest(_ url: String, _ headers: [String: String], _ body: Any?, _ cachePolicy: NSURLRequest.CachePolicy, _ timeout: Int32, _ method: NetworkMethod) -> NetworkRequest {

        return NetworkRequest(url: url, headers: headers, parameters: body, cachePolicy: cachePolicy, timeout: timeout, method: method)

    }

    // TODO: Refactor para mostrar los diferentes errores
    // Se ha sacado una deuda técnica para refactorizar este método en los DM's

    func checkShowError(response: Response, security: Bool) -> ServiceError? {

        if let body = response.body, let objectDeserialize = ErrorEntity.deserialize(from: body) {

            if security {

                if let headerDict = response.headerDict, let authenticationtype = headerDict["authenticationtype"] as? String {

                    if objectDeserialize.httpStatus != 401 && objectDeserialize.code != "70" && authenticationtype != "05" {

                        return ServiceError.GenericErrorEntity(error: objectDeserialize)
                    }
                }

                return nil

            } else {
                return ServiceError.GenericErrorEntity(error: objectDeserialize)
            }

        } else {

            print("\(#line): \(#function) - Error parsing")
            return ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text))
        }
    }
}
