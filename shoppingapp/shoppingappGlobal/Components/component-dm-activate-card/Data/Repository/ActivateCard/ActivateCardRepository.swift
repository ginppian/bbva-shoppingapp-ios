//
//  ActivateCardRepository.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 10/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

public protocol ActivateCardRepository {

    func activateCard(params: CardActivateDTO, success: @escaping (_ result: Bool) -> Void, error: @escaping (_ error: Error) -> Void)
    func activateCardWithCVV(params: CardActivateWithCVVDTO, success: @escaping (_ result: Bool) -> Void, error: @escaping (_ error: Error) -> Void)

}
