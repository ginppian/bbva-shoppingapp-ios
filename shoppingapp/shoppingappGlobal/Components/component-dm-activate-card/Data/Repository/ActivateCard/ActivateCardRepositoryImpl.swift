//
//  ActivateCardRepositoryImpl.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 10/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import ObjectMapper
import BBVA_Network
import HandyJSON

enum ActivateCardWithCVVWError: Error {

    case WrongCVVError
}

class ActivateCardRepositoryImpl: BaseRepositoryImpl, ActivateCardRepository {

    fileprivate static let OPERATIVE_ID = "OPERATIVE"

    fileprivate static let ACTIVATE_CARD_URL = "/cards/" //NON-NLS
    fileprivate static let ACTIVATE_CARD_WITH_CVV_URL = "/cards/%@/activate"
    
    func activateCard(params: CardActivateDTO, success: @escaping (_ result: Bool) -> Void, error: @escaping (_ error: Error) -> Void) {

        let url = baseURL + ActivateCardRepositoryImpl.ACTIVATE_CARD_URL + params.cardId
        let body = ActivateCardBody()
        var content = ActivateCardContent()
        content.id = ActivateCardRepositoryImpl.OPERATIVE_ID
        body.status = content

        guard let bodyParser = body.toJSON() else {
            return
        }

        let request: NetworkRequest = createRequest(url, Headers.BASIC_HEADER, bodyParser, .useProtocolCachePolicy, 30, NetworkMethod.PATCH)

        _ = networkWorker.execute(request: request) { (response: Response) in

            if response.error == nil {
                success(true)
            } else {

                if let serviceError = self.checkShowError(response: response, security: false) {
                    error(serviceError)
                }
            }
        }
    }
    
    func activateCardWithCVV(params: CardActivateWithCVVDTO, success: @escaping (_ result: Bool) -> Void, error: @escaping (_ error: Error) -> Void) {
        
        let url = baseURL + String(format: ActivateCardRepositoryImpl.ACTIVATE_CARD_WITH_CVV_URL, params.cardId)
        let body = ActivateCardWithCVVBody()
        body.id = params.id
        body.value = params.pin
        
        guard let bodyParser = body.toJSON() else {
            return
        }
        
        let request: NetworkRequest = createRequest(url, Headers.BASIC_HEADER, bodyParser, .useProtocolCachePolicy, 30, NetworkMethod.POST)
        
        _ = networkWorker.execute(request: request) { (response: Response) in
            
            if response.error == nil {
                success(true)
            } else {

                if let body = response.body, let objectDeserialize = ErrorEntity.deserialize(from: body) {
                    if objectDeserialize.httpStatus == 409 && objectDeserialize.code == "MCE0329"{
                        error(ActivateCardWithCVVWError.WrongCVVError)
                    } else {
                        if let serviceError = self.checkShowError(response: response, security: false) {
                            error(serviceError)
                        }
                    }
                }
                
            }
        }
    }
}

class ActivateCardBody: HandyJSON {

    var status: ActivateCardContent?

    required init() {

    }
}

class ActivateCardWithCVVBody: HandyJSON {
    
    var id: String?
    var value: String?
    
    required init() {
        
    }
}

struct ActivateCardContent: HandyJSON {
    var id: String?
}
