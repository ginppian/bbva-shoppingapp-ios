//
//  Headers.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 10/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

open class Headers {

    // MARK: Keys

    public static let CONTENT_TYPE      = "Content-Type"
    public static let ACCEPT            = "Accept"
    public static let SET_COOKIE        = "Set-Cookie"
    public static let ACCEPT_CHARSET    = "Accept-Charset"
    public static let ACCEPT_LANGUAGE   = "Accept-Language"
    public static let UTF8              = "Charset"
    public static let TSEC              = "TSEC"

    // MARK: Values

    public static let APP_URL_ENCODED_CHARSET   = "application/x-www-form-urlencoded;charset=UTF-8"
    public static let APP_JSON                  = "application/json"
    public static let CHARSET_ACCEPTED          = "UTF-8"
    public static let ACCEPTED_LANGUAGE         = "es-ES,es;q=0.8"

    // MARK: Implementation

    static var BASIC_HEADER = [Headers.CONTENT_TYPE: Headers.APP_JSON, Headers.UTF8: Headers.CHARSET_ACCEPTED]

}
