//
//  CardActivateWithCVVDTO.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 10/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

open class CardActivateWithCVVDTO: CellsDTO {

    var id: String
    var cardId: String
    var pin: String

    init(id: String, cardId: String, pin: String) {
        
        self.id = id
        self.cardId = cardId
        self.pin = pin
    }

}
