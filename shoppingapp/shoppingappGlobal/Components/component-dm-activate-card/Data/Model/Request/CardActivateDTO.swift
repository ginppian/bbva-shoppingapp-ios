//
//  CardActivateDTO.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 10/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

open class CardActivateDTO: CellsDTO {

    var cardId: String

    init(cardId: String) {
        
        self.cardId = cardId
    }
}
