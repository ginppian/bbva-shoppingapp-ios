//
//  WalletCellsViewController.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 13/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit
import CellsNativeCore
import CellsNativeComponents
import WebKit

protocol CellsProtocol: class {
    var presenter: CellsPresenterProtocol! { get set }

    func finishDoRequestSuccess(modelBO: ModelBO?)
    func finishDoRequestError(error: ErrorBO)
    func startDoRequest(cellsDTO: CellsDTO)
    func registerOmniture(page: String)
    func showOtp()
}

extension CellsProtocol {

    func showOtp() {
        presenter.hideLoading(completion: {
            let viewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: SecurityViewController.ID)! as! SecurityViewController
            let navigationController = UINavigationController(rootViewController: viewController)
            BusManager.sessionDataInstance.present(viewController: navigationController, animated: true, completion: nil)
        })
    }

    func startDoRequest(cellsDTO: CellsDTO) {

    }

    func finishDoRequestSuccess(modelBO: ModelBO? = nil) {
        presenter.hideLoading(completion: {
            self.presenter.showPageSuccess(modelBO: modelBO)
        })
    }

    func finishDoRequestError(error: ErrorBO) {
        presenter.hideLoading(completion: {
            self.presenter.setModel(model: error)
            self.presenter.checkError()
        })
    }

    func registerOmniture(page: String) {
        presenter.registerOmniture(page: page)
    }
}

class WalletCellsViewController: BaseViewController {

    var presenter: CellsPresenterProtocol!

    // MARK: Outlets

    @IBOutlet weak var cellsWebView: CellsWebView!

    // MARK: life cycle

    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }

    required convenience init?(coder aDecoder: NSCoder) {
        self.init(nibName: String(describing: WalletCellsViewController.self), bundle: nil)
    }

    override func viewDidLoad() {

        super.viewDidLoad()

        setNavigationBarTransparent()

        self.navigationController?.navigationBar.isHidden = true

        if #available(iOS 11.0, *) {

            self.cellsWebView.webView?.scrollView.contentInsetAdjustmentBehavior = .never

        } else {

            self.automaticallyAdjustsScrollViewInsets = false

        }

        if let webController: WebController = ComponentManager.get(id: WebController.ID) {
            webController.webView.scrollView.bounces = false
            webController.webView.navigationDelegate = self
        }
        
        if #available(iOS 12.0, *) {
            
            NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        }
    }

    override func viewWillAppear(_ animated: Bool) {

        super.viewWillAppear(animated)

        BusManager.sessionDataInstance.disableSwipeLateralMenu()

    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationItem.setHidesBackButton(true, animated: false)
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    deinit {
        DLog(message: "Deinitialized WalletCellsViewController")
    }

    func back() {

        self.navigationController?.navigationBar.isHidden = false
        BusManager.sessionDataInstance.back()

    }

    func dismiss(completion: (() -> Void)? = nil) {

        self.navigationController?.navigationBar.isHidden = false

        BusManager.sessionDataInstance.dismissViewController(animated: true, completion: completion )

    }
    
    @available(iOS 12.0, *)
    @objc func keyboardWillHide(notification: NSNotification) {
        if let webController: WebController = ComponentManager.get(id: WebController.ID) {
            for view in webController.webView.subviews {
                if let scrollView: UIScrollView = view as? UIScrollView {
                    scrollView.setContentOffset(CGPoint(x: 0.0, y: 0.0), animated: false)
                }
            }
        }
    }

}

extension WalletCellsViewController: TransitionAnimation {

    func nextPageLoadedNotification(webPage: String) {

    }

    func animateFinish() {
        if isModal() {
            dismiss()
        } else {
            back()
        }
    }

}

extension WalletCellsViewController: CloseModalDelegate {

    func errorViewControllerAcceptPressed(_ errorViewController: ErrorViewController) {
        presenter?.checkError()
    }

    func errorViewControllerCancelPressed(_ errorViewController: ErrorViewController) {
    }

}

extension WalletCellsViewController: ViewProtocol {
    func showError(error: ModelBO) {
        presenter?.showModal()
    }

    func changeColorEditTexts() {

    }
}

extension UIViewController {
    func isModal() -> Bool {

        if let navigationController = self.navigationController {
            if navigationController.viewControllers.first != self {
                return false
            }
        }

        if self.presentingViewController != nil {
            return true
        }

        if self.navigationController?.presentingViewController?.presentedViewController == self.navigationController {
            return true
        }

        if self.tabBarController?.presentingViewController is UITabBarController {
            return true
        }

        return false
    }
}

extension WalletCellsViewController: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        switch navigationAction.request.url?.scheme {
        case "tel"?:
            if let url = navigationAction.request.url {
                URLOpener().openURL(url)
            }
            decisionHandler(.cancel)
        case "mailto"?:
            if let url = navigationAction.request.url {
                URLOpener().openURL(url)
            }
            decisionHandler(.cancel)
        default:
            decisionHandler(.allow)
        }
    }
    
}
