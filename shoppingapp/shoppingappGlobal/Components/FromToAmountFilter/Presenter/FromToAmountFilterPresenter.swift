//
//  FromToAmountFilterPresenter.swift
//  shoppingapp
//
//  Created by jesus.martinez on 1/8/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

class FromToAmountFilterPresenter {

    var view: FromToAmountFilterViewProtocol?

    var displayData: FromToAmountFilterDisplayData! {
        didSet {
            onConfigureView()
        }
    }

    var fromAmount: NSDecimalNumber?
    var toAmount: NSDecimalNumber?

}

extension FromToAmountFilterPresenter: FromToAmountFilterPresenterProtocol {

    func onConfigureView() {

        view?.showNormal()

    }

    func fromChanged(amount: NSDecimalNumber) {

        guard amount.doubleValue.isNaN == false else {
            fromAmount = nil
            view?.showNormal()
            return
        }

        fromAmount = amount

        if let toAmount = toAmount {
            showError(fromAmount: fromAmount!, toAmount: toAmount)
        } else {
            view?.showNormal()
        }

    }

    func toChanged(amount: NSDecimalNumber) {

        guard amount.doubleValue.isNaN == false else {
            toAmount = nil
            view?.showNormal()
            return
        }

        toAmount = amount

        if let fromAmount = fromAmount {
            showError(fromAmount: fromAmount, toAmount: toAmount!)
        } else {
            view?.showNormal()
        }

    }

    func removeAmountValues() {
        view?.removeAmountValues()
    }

    private func showError(fromAmount: NSDecimalNumber, toAmount: NSDecimalNumber) {

        if toAmount.compare(fromAmount) == .orderedAscending {
            view?.show(error: displayData.error)
        } else {
            view?.showNormal()
        }

    }

}
