//
//  FromToAmountFilterView.swift
//  shoppingapp
//
//  Created by jesus.martinez on 1/8/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit

protocol FromToAmountFilterViewDelegate: class {

    func amounteFilterView(_ fromToAmountFilterView: FromToAmountFilterView, selectedFromAmount fromAmountText: String?, selectedToAmount toAmountText: String?)

}

class FromToAmountFilterView: XibView {

    var presenter = FromToAmountFilterPresenter()

    var fromAmountTextfielPresenter: AmountTextfieldPresenter?
    @IBOutlet weak var fromAmountTextfieldView: AmountTextfield!

    var toAmountTextfielPresenter: AmountTextfieldPresenter?
    @IBOutlet weak var toAmountTextfieldView: AmountTextfield!

    @IBOutlet weak var errorView: UIView!
    @IBOutlet weak var errorImageView: UIImageView!
    @IBOutlet weak var errorLabel: UILabel!

    weak var delegate: FromToAmountFilterViewDelegate?

    override func configureView() {

        super.configureView()

        errorImageView.image = ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Common.alert_icon, color: .BBVALIGHTRED), width: 15, height: 15)
        errorLabel.textColor = .BBVADARKRED
        errorLabel.font = Tools.setFontBook(size: 14)

        presenter.view = self
        presenter.onConfigureView()

        configureViews()

    }

    func configureViews() {

        fromAmountTextfieldView.presenter.displayData = AmountTextfieldDisplayData(placeHolder: Localizables.transactions.key_transactions_from_text, initialAmount: nil, currency: nil, shouldHideErrorAutomatically: true)
        toAmountTextfieldView.presenter.displayData = AmountTextfieldDisplayData(placeHolder: Localizables.transactions.key_transactions_to_text, initialAmount: nil, currency: nil, shouldHideErrorAutomatically: true)

        fromAmountTextfieldView.delegate = self
        toAmountTextfieldView.delegate = self

    }

}

extension FromToAmountFilterView: FromToAmountFilterViewProtocol {

    func show(error: String) {

        errorLabel.text = error
        errorView.isHidden = false

        fromAmountTextfieldView.showError()
        toAmountTextfieldView.showError()

        delegate?.amounteFilterView(self, selectedFromAmount: fromAmountTextfieldView.presenter.amountText, selectedToAmount: toAmountTextfieldView.presenter.amountText)
    }

    func showNormal() {

        errorView.isHidden = true

        fromAmountTextfieldView.hideError()
        toAmountTextfieldView.hideError()

        delegate?.amounteFilterView(self, selectedFromAmount: fromAmountTextfieldView.presenter.amountText, selectedToAmount: toAmountTextfieldView.presenter.amountText)
    }

    func removeAmountValues() {

        fromAmountTextfieldView.presenter.crossAction()
        toAmountTextfieldView.presenter.crossAction()

        fromAmountTextfieldView.presenter.onConfigureView()
        toAmountTextfieldView.presenter.onConfigureView()

        errorView.isHidden = true

    }

}

extension FromToAmountFilterView: AmountTextfieldDelegate {

    func amountTextfield(_ amountTextfield: AmountTextfield, didEndEditing amount: NSDecimalNumber) {

        if fromAmountTextfieldView == amountTextfield {

            presenter.fromChanged(amount: amount)

        } else {

            presenter.toChanged(amount: amount)

        }

    }

}
