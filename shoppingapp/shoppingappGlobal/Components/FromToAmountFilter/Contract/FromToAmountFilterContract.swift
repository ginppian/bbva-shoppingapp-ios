//
//  FromToAmountFilterContract.swift
//  shoppingapp
//
//  Created by jesus.martinez on 1/8/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

protocol FromToAmountFilterViewProtocol: ComponentViewProtocol {

    func showNormal()
    func show(error: String)
    func removeAmountValues()

}

protocol FromToAmountFilterPresenterProtocol: ComponentPresenterProtocol {

    func onConfigureView()
    func fromChanged(amount: NSDecimalNumber)
    func toChanged(amount: NSDecimalNumber)
    func removeAmountValues()

}
