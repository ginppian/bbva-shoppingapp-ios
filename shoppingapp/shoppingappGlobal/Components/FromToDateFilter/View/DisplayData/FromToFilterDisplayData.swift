//
//  FromToFilterDisplayData.swift
//  shoppingapp
//
//  Created by jesus.martinez on 29/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

struct FromToFilterDisplayData {

    var fromDisplayData: DateFilterDisplayData
    var toDisplayData: DateFilterDisplayData

}

extension FromToFilterDisplayData: Equatable {

    public static func == (lhs: FromToFilterDisplayData, rhs: FromToFilterDisplayData) -> Bool {

        return lhs.fromDisplayData == rhs.fromDisplayData
            && lhs.toDisplayData == rhs.toDisplayData

    }

}
