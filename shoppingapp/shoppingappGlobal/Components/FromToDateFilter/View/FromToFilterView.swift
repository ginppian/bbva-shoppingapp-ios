//
//  FromToFilterView.swift
//  shoppingapp
//
//  Created by jesus.martinez on 28/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit

enum SeparatorPosition {
    case center
    case left
    case right
}

protocol FromToFilterViewDelegate: class {

    func dateFilterView(_ fromToFilterView: FromToFilterView, selectedFromDate fromDate: String?, selectedToDate toDate: String?)

}

class FromToFilterView: XibView {

    fileprivate static let rotationAngle: CGFloat = .pi / 9

    var presenter = FromToDateFilterPresenter()

    // MARK: - IBOutlets

    @IBOutlet weak var fromDateFilterView: DateFilterView!

    @IBOutlet weak var separatorView: UIView!

    @IBOutlet weak var toDateFilterView: DateFilterView!

    weak var delegate: FromToFilterViewDelegate?

    // MARK: - Private Helper Methods

    override func configureView() {

        super.configureView()

        separatorView.backgroundColor = .BBVA200

        presenter.view = self
        presenter.onConfigureView()

        fromDateFilterView.delegate = self
        toDateFilterView.delegate = self
    }

}

extension FromToFilterView: FromToDateFilterViewProtocol {

    func updateFromDateFilter(withMaximumDate maximumDate: Date) {
        fromDateFilterView.presenter.present(maximumDate: maximumDate)
    }

    func updateToDateFilter(withMiniumDate minimumDate: Date) {
        toDateFilterView.presenter.present(minimumDate: minimumDate)
    }

    func rotateSeparatorView(to: SeparatorPosition) {

        var affineTransform = CGAffineTransform(rotationAngle: 0)

        switch to {
            case .center:
                affineTransform = CGAffineTransform(rotationAngle: 0)
            case .left:
                affineTransform = CGAffineTransform(rotationAngle: FromToFilterView.rotationAngle)
            case .right:
                affineTransform = CGAffineTransform(rotationAngle: -FromToFilterView.rotationAngle)
        }

        UIView.animate(withDuration: 0.5,
                       delay: 0,
                       options: .curveLinear,
                       animations: { () -> Void in
                        self.separatorView.transform = affineTransform
        }, completion: nil)

    }

}

extension FromToFilterView: DateFilterViewDelegate {

    func dateFilterView(_ dateFilterView: DateFilterView, didChangeDate newDate: Date) {

        if dateFilterView == fromDateFilterView {

            presenter.fromDateFilterChanged(date: newDate)

        } else {

            presenter.toDateFilterChanged(date: newDate)

        }

        delegate?.dateFilterView(self, selectedFromDate: presenter.fromDate, selectedToDate: presenter.toDate)

    }

    func dateFilterViewStartEditing(_ dateFilterView: DateFilterView) {

        if dateFilterView == fromDateFilterView {

            presenter.startEditingFromFilter()

        } else {

            presenter.startEditingToFilter()

        }

    }

    func dateFilterViewFinishEditing(_ dateFilterView: DateFilterView) {

        if dateFilterView == fromDateFilterView {

            presenter.endEditingFromFilter()

        } else {

            presenter.endEditingToFilter()

        }
    }

    func updateView(withDisplayData displayData: FromToFilterDisplayData) {

        fromDateFilterView.presenter.displayData = displayData.fromDisplayData
        toDateFilterView.presenter.displayData = displayData.toDisplayData
//        delegate?.dateFilterView(self, selectedFromDate: fromDateFilterView.presenter.displayData?.dateSelected, selectedToDate: toDateFilterView.presenter.displayData?.dateSelected)

    }

}
