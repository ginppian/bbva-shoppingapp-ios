//
//  FromToDateFilter.swift
//  shoppingapp
//
//  Created by jesus.martinez on 28/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

protocol FromToDateFilterViewProtocol: ComponentViewProtocol {

    func rotateSeparatorView(to: SeparatorPosition)
    func updateFromDateFilter(withMaximumDate maximumDate: Date)
    func updateToDateFilter(withMiniumDate minimumDate: Date)
    func updateView(withDisplayData displayData: FromToFilterDisplayData)

}

protocol FromToDateFilterPresenterProtocol: ComponentPresenterProtocol {

    func onConfigureView()
    func fromDateFilterChanged(date: Date)
    func toDateFilterChanged(date: Date)
    func startEditingFromFilter()
    func endEditingFromFilter()
    func startEditingToFilter()
    func endEditingToFilter()

}
