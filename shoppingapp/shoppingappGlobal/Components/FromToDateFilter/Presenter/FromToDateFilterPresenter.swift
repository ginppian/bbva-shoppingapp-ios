//
//  FromToDateFilterPresenter.swift
//  shoppingapp
//
//  Created by jesus.martinez on 28/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

class FromToDateFilterPresenter {

    weak var view: FromToDateFilterViewProtocol?
    var fromDate: String?
    var toDate: String?

    var displayData: FromToFilterDisplayData? {

        didSet {

            onConfigureView()

        }

    }

}

extension FromToDateFilterPresenter: FromToDateFilterPresenterProtocol {

    func onConfigureView() {

        view?.rotateSeparatorView(to: .center)

        if let displayData = displayData {
            view?.updateView(withDisplayData: displayData)
        }

    }

    func toDateFilterChanged(date: Date) {
        toDate = date.string(format: Date.DATE_FORMAT_SERVER)
        view?.updateFromDateFilter(withMaximumDate: date)
    }

    func fromDateFilterChanged(date: Date) {
        fromDate = date.string(format: Date.DATE_FORMAT_SERVER)
        view?.updateToDateFilter(withMiniumDate: date)
    }

    func startEditingFromFilter() {

        view?.rotateSeparatorView(to: .left)

    }

    func endEditingFromFilter() {

        view?.rotateSeparatorView(to: .center)

    }

    func startEditingToFilter() {

        view?.rotateSeparatorView(to: .right)

    }

    func endEditingToFilter() {

        view?.rotateSeparatorView(to: .center)

    }

}
