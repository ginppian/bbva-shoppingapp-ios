//
//  BridgingHeader.h
//  shoppingapp
//
//  Created by Javier Dominguez on 23/3/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

#ifndef BridgingHeader_h
    #define BridgingHeader_h
#endif /* BridgingHeader_h */

#import "LGSideMenuController.h"
#import "UIViewController+LGSideMenuController.h"
#import <Lottie/Lottie.h>
#import <ToolkitLib/ImageUtilities.h>
#import "TrackerHelper.h"
#import <Shimmer/FBShimmering.h>
#import <Shimmer/FBShimmeringLayer.h>
#import <Shimmer/FBShimmeringView.h>
#import <CommonCrypto/CommonHMAC.h>

#import "SmiSdk.h"
#import <SVGKit/SVGKit.h>
#import <SVGKit/SVGKImage.h>

#import "BGMSecuritySDK/LibMCrypt.h"

#ifndef LibMCrypt_Bridging_Header_h
    #define LibMCrypt_Bridging_Header_h
#endif /* LibMCrypt_Bridging_Header_h */

