//
//  LoginLoaderContract.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 15/3/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit
import RxSwift

protocol LoginLoaderViewProtocol: ViewProtocol {

    func startUnlockAnimation()
    func showUnlockTitle(_ title: String)

}

protocol LoginLoaderPresenterProtocol: PresenterProtocol {

    func viewDidLoad()
    func didFinishUnlockAnimation()

}

protocol LoginLoaderInteractorProtocol {

    func provideConsumerId() -> Observable<String>
    func provideAnonymousInformation() -> Observable<ModelBO>
    func provideLoginData(loginTransport: ModelTransport, consumerId: String) -> Observable<ModelBO>
    func provideCustomerData() -> Observable<ModelBO>
    func provideCards() -> Observable<ModelBO>
    func requestGrantingTicket(withAnonymousInformation anonymousInformation: AnonymousInformationBO) -> Observable<ModelBO>

}
