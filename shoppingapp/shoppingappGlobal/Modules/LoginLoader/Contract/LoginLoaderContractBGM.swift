//
//  LoginLoaderContractBGM.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 15/3/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit
import RxSwift

protocol LoginLoaderViewProtocolBGM: LoginLoaderViewProtocolA {

    func checkDeviceActivation(loginTransport: LoginTransport, grantingTicketResponse: GrandTingTicketBO, tsec: String)
}

protocol LoginLoaderPresenterProtocolBGM: LoginLoaderPresenterProtocolA {
    
    func deviceActivationSuccess()
}
