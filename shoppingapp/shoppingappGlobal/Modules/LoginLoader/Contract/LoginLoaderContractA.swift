//
//  LoginLoaderContractA.swift
//  shoppingappMX
//
//  Created by Jesús Ángel Sánchez Sánchez on 14/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import RxSwift

protocol LoginLoaderViewProtocolA: LoginLoaderViewProtocol {
    
    func downgradeService(withCardId cardId: String, andResetIndicator resetIndicator: Bool)
}

protocol LoginLoaderPresenterProtocolA: LoginLoaderPresenterProtocol {
    
}

protocol LoginLoaderInteractorProtocolA: LoginLoaderInteractorProtocol {
    
}
