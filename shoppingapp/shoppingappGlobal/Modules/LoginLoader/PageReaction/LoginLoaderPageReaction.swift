//
//  LoginLoaderPageReaction.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 25/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

class LoginLoaderPageReaction: PageReaction {

    static let ROUTER_TAG: String = "login-loader-tag"

    static let EVENT_LOGIN_FINISHED: ActionSpec<Void> = ActionSpec<Void>(id: "EVENT_LOGIN_FINISHED")
    static let EVENT_ANONYMOUS_SESSION: ActionSpec<Void> = ActionSpec<Void>(id: "EVENT_ANONYMOUS_SESSION")

    static let EVENT_ERROR_LOGIN: ActionSpec<Model> = ActionSpec<Model>(id: "EVENT_ERROR_LOGIN")
    
    override func configure() {

        navigateWhenSessionFromLoginLoader()

        navigateWhenAnonymousSessionFromLoginLoader()

        navigateErrorFromLoginLoader()
    
    }

    func navigateWhenSessionFromLoginLoader() {

        _ = when(id: LoginLoaderPageReaction.ROUTER_TAG, type: Void.self)
            .doAction(actionSpec: LoginLoaderPageReaction.EVENT_LOGIN_FINISHED)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, _ in

                router.navigateWhenSessionFromLoginLoader()

            }

    }

    func navigateWhenAnonymousSessionFromLoginLoader() {

        _ = when(id: LoginLoaderPageReaction.ROUTER_TAG, type: Void.self)
            .doAction(actionSpec: LoginLoaderPageReaction.EVENT_ANONYMOUS_SESSION)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, _ in

                router.navigateWhenAnonymousSessionFromLoginLoader()

            }
    }

    func navigateErrorFromLoginLoader() {
        _ = when(id: LoginLoaderPageReaction.ROUTER_TAG, type: Model.self)
            .doAction(actionSpec: LoginLoaderPageReaction.EVENT_ERROR_LOGIN)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, model in

                router.navigateErrorFromLoginLoader(withModel: model!)

            }
    }
    
}
