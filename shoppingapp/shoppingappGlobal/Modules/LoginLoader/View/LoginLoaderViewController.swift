//
//  LoginLoaderViewController.swift
//  shoppingapp
//
//  Created by ruben.fernandez on 14/6/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit
import RxSwift
import Lottie
import Stellar
import CellsNativeCore
import CellsNativeComponents

class LoginLoaderViewController: BaseViewController {

    // MARK: Constants

    static let ID: String = String(describing: LoginLoaderViewController.self)

    // MARK: Vars

    var presenter: LoginLoaderPresenterProtocol!
    var lottiUnlock: LOTAnimationView!
    var lottiLoaderPlaying = false

    // MARK: Outlets

    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var descriptionLabel: UILabel!

    override func viewDidLoad() {
        configure(appLifecycle: (UIApplication.shared.delegate as! AppDelegate).appLifeCycle!, id: LoginLoaderViewController.ID)

        super.viewDidLoad()

        setAccessibilities()

        navigationItem.setHidesBackButton(true, animated: false)

        descriptionLabel.textColor = .BBVAWHITE
        descriptionLabel.font = Tools.setFontMedium(size: 14)

        descriptionLabel.frame = CGRect(x: descriptionLabel.frame.origin.x, y: descriptionLabel.frame.origin.y + 90, width: descriptionLabel.frame.size.width, height: descriptionLabel.frame.size.height)

        bottomView.frame = CGRect(x: 0, y: UIScreen.main.bounds.height - 380, width: UIScreen.main.bounds.width, height: 380)

        lottiUnlock = LOTAnimationView(name: ConstantsLottieAnimations.lock)
        lottiUnlock?.frame = CGRect(x: (UIScreen.main.bounds.width / 2), y: UIScreen.main.bounds.height - 140, width: 0, height: 0)
        lottiUnlock?.contentMode = .scaleAspectFit
        view.addSubview(lottiUnlock!)

        presenter.viewDidLoad()

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        BusManager.sessionDataInstance.disableSwipeLateralMenu()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        if !lottiLoaderPlaying {
            lottiLoaderPlaying = true

            bottomView.makeFrame(CGRect(x: 0, y: UIScreen.main.bounds.height - 180, width: UIScreen.main.bounds.width, height: 180)).duration(0.52).easing(.custom(0.6, 0, 0.2, 1)).animate()
            
            lottiUnlock.makeFrame(CGRect(x: (UIScreen.main.bounds.width / 2) - 15, y: UIScreen.main.bounds.height - 140, width: 30, height: 30)).duration(0.48).easing(.custom(0.3, 0, 0.2, 1)).animate()
            
            descriptionLabel.makeFrame(CGRect(x: (UIScreen.main.bounds.width / 2) - descriptionLabel.frame.size.width / 2, y: UIScreen.main.bounds.height - 140 + 30 + descriptionLabel.frame.size.height / 2, width: descriptionLabel.frame.size.width, height: descriptionLabel.frame.size.height)).duration(0.48).easing(.custom(0.3, 0, 0.2, 1)).animate()
            
            let lottiLoader = LOTAnimationView(name: ConstantsLottieAnimations.loader)
            let yPosition: Double = Double(((UIScreen.main.bounds.height - 180) / 4) + 40)
            let xPosition: Double = Double((UIScreen.main.bounds.width / 2) - 40)
            lottiLoader.frame = CGRect(x: xPosition, y: yPosition, width: 80, height: 80)
            lottiLoader.contentMode = .scaleAspectFit
            view.insertSubview(lottiLoader, belowSubview: bottomView)
            
            lottiLoader.play()
            lottiLoader.loopAnimation = true
        }
    }

    override func initModel(withModel model: Model) {
        presenter.setModel(model: model)
    }

    deinit {
        ComponentManager.remove(id: presenter.routerTag)
        DLog(message: "Deinitialized LoginLoaderViewController")
    }

}

extension LoginLoaderViewController: LoginLoaderViewProtocol {
    
    func startUnlockAnimation() {
        DLog(message: "Start animation")

        lottiUnlock?.play(completion: { _ in

            self.descriptionLabel.makeFrame(CGRect(x: (UIScreen.main.bounds.width / 2) - self.descriptionLabel.frame.size.width / 2, y: self.descriptionLabel.frame.origin.y - 60, width: self.descriptionLabel.frame.size.width, height: self.descriptionLabel.frame.size.height)).duration(0.52).easing(.custom(0.3, 0, 0.2, 1)).makeAlpha(0).animate()

            self.bottomView.makeFrame(CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)).duration(0.52).easing(.custom(0.6, 0, 0.2, 1)).completion({

                self.presenter.didFinishUnlockAnimation()

            }).animate()
        })
    }

    func showUnlockTitle(_ title: String) {
        descriptionLabel.text = title
    }

    func showError(error: ModelBO) {
    }

    func changeColorEditTexts() {
    }

}

// MARK: - APPIUM

private extension LoginLoaderViewController {

    func setAccessibilities() {
        #if APPIUM
        Tools.setAccessibility(view: descriptionLabel, identifier: ConstantsAccessibilities.LOGIN_LOADER_PASS_TEXTFIELD)
        #endif
    }

}
