//
//  LoginLoaderViewControllerBGM.swift
//  shoppingapp
//
//  Created by ruben.fernandez on 14/6/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit
import BGMConnectManager
import BGMConnectSDK
import BGMCore
import BGMUICatalog
import RxSwift
import Lottie
import Stellar
import CellsNativeCore
import CellsNativeComponents

class LoginLoaderViewControllerBGM: LoginLoaderViewControllerA, GCSSecurityUIDelegate, GCSPhoneCaller, GCSUILoader, GCSErrorModel, GCSUILogoutPresenter, GCSOnCriticalFlowStatusChange {
    
    // MARK: vars
    
    private var handlerAndRequestHelper: GCSHandlerAndRequestHelper!
    private var serverError500Handler: GCSServerError500Handler!
    private var uiErrorHandler: GCSUIErrorHandler!
    private var serverExpirationHandler: GCSServerExpirationHandler!
    private var authenticationHandler: GCSAuthenticationHandler!
    
    // MARK: life cycle
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initialize()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: glomoconnect
    
    private func initialize() {
        self.handlerAndRequestHelper = GCSHandlerAndRequestHelper()
        self.serverError500Handler = GCSServerError500Handler(context: self, _handlerAndRequestHelper: self.handlerAndRequestHelper, uiLoader: self)
        self.uiErrorHandler = GCSUIErrorHandler(context: self, handlerAndRequestHelper: self.handlerAndRequestHelper, uiLoader: self, uiErrorViewPresenter: self, phoneCaller: self)
        self.serverExpirationHandler = GCSServerExpirationHandler(context: self, _handlerAndRequestHelper: self.handlerAndRequestHelper, uiLoader: self, logoutPresenter: self, uiErrorHandler: self.uiErrorHandler)
        self.authenticationHandler = GCSAuthenticationHandler(context: self, _handlerAndRequestHelper: self.handlerAndRequestHelper, uiLoader: self, uiErrorHandler: self.uiErrorHandler, security: self)
    }
    
    func handleError(error: GCSError, requestInfo: GCSRequestModel, extra: [String: Any?]? = nil, isDigitalActivation: Bool = false) -> Bool {
        
        return self.uiErrorHandler.handleError(error: error, requestInfo: requestInfo, extra: extra, isDigitalActivation: isDigitalActivation)
    }
    
    func checkForError(httpUrlResponse: HTTPURLResponse) -> Bool {
        if httpUrlResponse.statusCode >= GCSHTTPCodes.KNOWNERROR.rawValue {
            return true
        }
        
        return false
    }
    
    func loginWithCheckDevice(_ loginTransport: LoginTransport, grantingTicketResponse: GrandTingTicketBO, tsec: String) {
                
        let connectDispatcher: BGMConnectDispatcher = BGMConnectDispatcher()
        self.baseHandler = connectDispatcher.getConnectHandler()

        let connectController = GCSController.getCore()
        connectController.criticalFlowDelegate = self
        connectController.saveUsername(username: loginTransport.username)
        connectController.saveAutheticationData(userStatus: grantingTicketResponse.clientStatus, autheticationState: grantingTicketResponse.authenticationState, multiStepProcssID: grantingTicketResponse.multistepProcessId)

        connectController.setTsec(tsec: tsec)
        connectController.checkDeviceStatus(vc: self)
    
    }
    
    // MARK: GCSSecurityUIDelegate
    
    // Hides the loading view
    func finishLoading() {
        DLog(message: "finishLoading")
    }
    
    func startLoading() {
        DLog(message: "startLoading")
    }
    
    //OVERWRITE
    func showLogoutMessage(onCompletion: (() -> Void)?) {
        DLog(message: "showLogoutMessage")
    }
    
    func sendLogoutTag() {
        DLog(message: "sendLogoutTag")
    }
    
    func callForbidden403(error: GCSError, requestInfo: GCSRequestModel) -> Bool {
        return self.serverExpirationHandler.handle(error: error, requestInfo: requestInfo)
    }
    
    func callUnauthorized401(error: GCSError, requestInfo: GCSRequestModel, headers: [AnyHashable: Any]) -> Bool {
        return self.authenticationHandler.handle(error: error, requestInfo: requestInfo, headers: headers)
    }
    
    func callServerError500(error: GCSError, requestInfo: GCSRequestModel) -> Bool {
        return self.serverError500Handler.handle(error: error, requestInfo: requestInfo)
    }
    
    func callServerError40X(error: GCSError, requestInfo: GCSRequestModel) -> Bool {
        return handleError(error: error, requestInfo: requestInfo)
    }
    
    func callServerError50X(error: GCSError, requestInfo: GCSRequestModel) -> Bool {
        return handleError(error: error, requestInfo: requestInfo)
    }
    
    var baseHandler: BGMRequestHandler? {
        get {
            return self.handlerAndRequestHelper.baseHandler
        }
        set {
            self.handlerAndRequestHelper.baseHandler = newValue
        }
    }
    
    // MARK: GCSOnCriticalFlowStatusChange
    
    func onDeviceActivationSuccessful(tsec: String, notificationTitle: NSAttributedString?, notificationMessage: NSAttributedString?, hadErrorDownloadingToken: Bool) {
        
        DLog(message: "onDeviceActivationSuccessful")
        
        deviceActivationSuccess(tsec: nil)
    }
    
    func onDeviceActivationFailure(error: Int, serverError: GCSError?, errorMessage: String?, requestModel: GCSRequestModel?) {
        
        DLog(message: "onDeviceActivationFailure error: \(error)")
        
        let errorBO = ErrorBO(message: Localizables.glomo_connect.key_device_activation_error, errorType: ErrorType.warning, allowCancel: false)
        
        if error == GCSDeviceActivationError.ivrPending.rawValue {
            errorBO.isErrorShown = true
        }
        
        BusManager.sessionDataInstance.navigateScreen(tag: LoginLoaderPageReaction.ROUTER_TAG, LoginLoaderPageReaction.EVENT_ERROR_LOGIN, errorBO)
    }
    
    func onDigitalActivationSuccessful(tsec: String, notificationTitle: NSAttributedString?, notificationMessage: NSAttributedString?, hadErrorDownloadingToken: Bool) {
        
        DLog(message: "onDigitalActivationSuccessful")
    }
    
    func validateLevelRaiseOnDeviceIdError(error: GCSError, requestInfo: GCSRequestModel, headers: [AnyHashable: Any]) -> Bool {
        
        DLog(message: "validateLevelRaiseOnDeviceIdError")
        
        return callUnauthorized401(error: error, requestInfo: requestInfo, headers: headers)
    }
    
    func onPasswordResetSuccessful(tsec: String, message: String) {
        DLog(message: "onPasswordResetSuccessful")
    }
    
    func startDigitalActivation() {
        DLog(message: "startDigitalActivation")
    }
    
    func onIVRActivationComplete() {
        DLog(message: "onIVRActivationComplete")
        
        let errorBO = ErrorBO(message: Localizables.glomo_connect.key_device_activation_error, errorType: ErrorType.warning, allowCancel: false)
        
        errorBO.isErrorShown = true
        
        BusManager.sessionDataInstance.navigateScreen(tag: LoginLoaderPageReaction.ROUTER_TAG, LoginLoaderPageReaction.EVENT_ERROR_LOGIN, errorBO)
    }
    
    func interceptAndHandleError(error: GCSError, requestInfo: GCSRequestModel, headers: [AnyHashable: Any]) {
        DLog(message: "interceptAndHandleError")
    }
    
    func onNewPasswordSuccessfull() {
        DLog(message: "onNewPasswordSuccessfull")
    }
    
    func handleUnknownError() {
        DLog(message: "handleUnknownError")
        let errorBO = ErrorBO(message: Localizables.common.key_error_ocurred_try_again_text, errorType: ErrorType.warning, allowCancel: false)
        DispatchQueue.main.async {
            BusManager.sessionDataInstance.navigateScreen(tag: LoginLoaderPageReaction.ROUTER_TAG, LoginLoaderPageReaction.EVENT_ERROR_LOGIN, errorBO)
        }
    }
    
    // // MARK: custom methods
    
    func deviceActivationSuccess(tsec: String?) {
        if let tsec = tsec {
            BusManager.sessionDataInstance.headerInterceptorStore?.setTsec(tsec: tsec)
        }
        
        (presenter as? LoginLoaderPresenterProtocolBGM)?.deviceActivationSuccess()
    }
    
}

extension LoginLoaderViewControllerBGM: LoginLoaderViewProtocolBGM {
    
    func checkDeviceActivation(loginTransport: LoginTransport, grantingTicketResponse: GrandTingTicketBO, tsec: String) {
        loginWithCheckDevice(loginTransport, grantingTicketResponse: grantingTicketResponse, tsec: tsec)
    }

}
