//
//  LoginLoaderViewControllerA.swift
//  shoppingappMX
//
//  Created by Jesús Ángel Sánchez Sánchez on 14/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit
import CellsNativeCore
import CellsNativeComponents

class LoginLoaderViewControllerA: LoginLoaderViewController {
    
    // MARK: Constants
    
    var downgradeStore: DowngradeStore?
    
    override func viewDidLoad() {
        
        if let networkWorker = BusManager.sessionDataInstance.networkWorker {
            downgradeStore = DowngradeStore(worker: networkWorker, host: ServerHostURL.getDowngradeURL())
        }
        
        super.viewDidLoad()
    }
}

// MARK: - LoginLoaderViewProtocolA

extension LoginLoaderViewControllerA: LoginLoaderViewProtocolA {
    
    func downgradeService(withCardId cardId: String, andResetIndicator resetIndicator: Bool) {
        
        let downgradeDTO = DowngradeDTO(cardId: cardId, resetIndicator: resetIndicator)
        downgradeStore?.downgrade(downgradeDTO: downgradeDTO)
    }
}
