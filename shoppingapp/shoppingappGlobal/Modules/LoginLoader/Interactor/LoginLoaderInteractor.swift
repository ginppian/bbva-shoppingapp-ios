//
//  BVALoginInteractor.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 15/3/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation
import RxSwift

class LoginLoaderInteractor: LoginLoaderInteractorProtocol {

    let disposeBag = DisposeBag()
    var userBO: UserBO?

    func provideConsumerId() -> Observable<String> {

        return Observable.create { observer in

            ConfigurationDataManager.sharedInstance.provideConsumerId().subscribe(
                onNext: { result in

                    observer.onNext(result)
                    observer.onCompleted()
                }).addDisposableTo(self.disposeBag)

            return Disposables.create()
        }
    }

    func provideLoginData(loginTransport: ModelTransport, consumerId: String) -> Observable<ModelBO> {

        return Observable.create { observer in

            LoginLoaderDataManager.sharedInstance.serviceGrantingTicket(loginTransport: loginTransport, consumerId: consumerId).subscribe(

                onNext: { result in

                    if let granTingTicketEntity = result as? GrandTingTicketEntity {
                        observer.onNext(GrandTingTicketBO(grantingTicket: granTingTicketEntity))
                        observer.onCompleted()
                    } else {
                        DLog(message: "Incorrect GrandTingTicketEntity")

                    }

                },

                onError: { error in

                    let evaluate : ServiceError = (error as? ServiceError)!

                    switch evaluate {

                    case .GenericErrorEntity(let errorEntity):
                        let errorBVABO = ErrorBO(error: errorEntity)
                        observer.onError(ServiceError.GenericErrorBO(error: errorBVABO))
                    default:
                        observer.onError(evaluate)

                    }

                },
                onCompleted: {
                    DLog(message: "nothing to do")
                }

                ).addDisposableTo(self.disposeBag)

            return Disposables.create()
        }

    }

    func provideCustomerData() -> Observable<ModelBO> {
        return Observable.create { observer in

            LoginLoaderDataManager.sharedInstance.serviceCustomer().subscribe(
                onNext: { result in

                    if let userEntity = result as? UserEntity {

                        let user = UserBO(user: userEntity)

                        self.userBO = user

                        observer.onNext(user)
                        observer.onCompleted()
                    } else {
                        DLog(message: "Incorrect UserEntity")
                    }

                },
                onError: { error in

                    let evaluate : ServiceError = (error as? ServiceError)!

                    switch evaluate {

                    case .GenericErrorEntity(let errorEntity):
                        let errorBVABO = ErrorBO(error: errorEntity)
                        observer.onError(ServiceError.GenericErrorBO(error: errorBVABO))
                    default:
                        observer.onError(evaluate)
                    }

                },
                onCompleted: {
                }
                ).addDisposableTo(self.disposeBag)
            return Disposables.create()
        }
    }

    func provideCards() -> Observable <ModelBO> {

        return Observable.create { observer in

            CardsDataManager.sharedInstance.provideCards().subscribe(
                onNext: { result in

                    if let cardsEntity = result as? CardsEntity {
                        observer.onNext(CardsBO(cardsEntity: cardsEntity))
                        observer.onCompleted()
                    } else {
                        DLog(message: "Incorrect CardsEntity")
                    }
                },

                onError: { error in

                    let evaluate: ServiceError = (error as? ServiceError)!

                    switch evaluate {

                    case .GenericErrorEntity(let errorEntity):
                        let errorBVABO = ErrorBO(error: errorEntity)
                        observer.onError(ServiceError.GenericErrorBO(error: errorBVABO))
                    default:
                        observer.onError(evaluate)
                    }

                },
                onCompleted: {
                }).addDisposableTo(self.disposeBag)

            return Disposables.create()
        }
    }

    func provideAnonymousInformation() -> Observable<ModelBO> {

        return Observable.create { observer in

            ConfigurationDataManager.sharedInstance.provideAnonymousInformation().subscribe(
                onNext: { result in

                    if let anonymousEntity = result as? AnonymousInformationEntity {
                        observer.onNext(AnonymousInformationBO(entity: anonymousEntity))
                        observer.onCompleted()
                    }

                }).addDisposableTo(self.disposeBag)

            return Disposables.create()
        }

    }

    func requestGrantingTicket(withAnonymousInformation anonymousInformation: AnonymousInformationBO) -> Observable<ModelBO> {

        return Observable.create { observer in

            LoginLoaderDataManager.sharedInstance.requestGrantingTicket(withAnonymousInformation: anonymousInformation).subscribe(

                onNext: { result in

                    if let granTingTicketEntity = result as? GrandTingTicketEntity {
                        observer.onNext(GrandTingTicketBO(grantingTicket: granTingTicketEntity))
                        observer.onCompleted()
                    } else {
                        DLog(message: "Incorrect GrandTingTicketEntity")

                    }

                },

                onError: { error in

                    let evaluate : ServiceError = (error as? ServiceError)!

                    switch evaluate {

                    case .GenericErrorEntity(let errorEntity):
                        let errorBVABO = ErrorBO(error: errorEntity)
                        observer.onError(ServiceError.GenericErrorBO(error: errorBVABO))
                    default:
                        observer.onError(evaluate)
                    }

                },
                onCompleted: {
                    DLog(message: "nothing to do")
                }

                ).addDisposableTo(self.disposeBag)

            return Disposables.create()
        }

    }
}
