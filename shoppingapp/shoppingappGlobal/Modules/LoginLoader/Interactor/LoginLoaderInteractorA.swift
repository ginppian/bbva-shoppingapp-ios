//
//  LoginLoaderInteractorA.swift
//  shoppingappMX
//
//  Created by jesus.martinez on 9/7/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import RxSwift
import BGMConnectSDK

class LoginLoaderInteractorA: LoginLoaderInteractor {

    override func provideCards() -> Observable <ModelBO> {

        return Observable.create { observer in
            
            CardsDataManager.sharedInstance.retrieveFinancialOverview().subscribe(
                onNext: { [weak self] result in

                    if let financialOverviewEntity = result as? FinancialOverviewEntity {

                        guard let `self` = self, let user = self.userBO else {
                            observer.onError(ServiceError.GenericErrorBO(error: ErrorBO(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text))))
                            return
                        }
                        observer.onNext(CardsBO(financialOverviewEntity: financialOverviewEntity, user: user))
                        observer.onCompleted()
                    } else {
                        observer.onError(ServiceError.GenericErrorBO(error: ErrorBO(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text))))
                        DLog(message: "Incorrect FinancialOverviewEntity")
                    }
                },
                onError: { error in

                    let evaluate: ServiceError = (error as? ServiceError)!

                    switch evaluate {

                    case .GenericErrorEntity(let errorEntity):
                        let errorBVABO = ErrorBO(error: errorEntity)
                        observer.onError(ServiceError.GenericErrorBO(error: errorBVABO))
                    default:
                        observer.onError(evaluate)
                    }

                },
                onCompleted: {
                }).addDisposableTo(self.disposeBag)

            return Disposables.create()
        }
    }

}
