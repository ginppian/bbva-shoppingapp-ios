//
//  LoginLoaderPresenterBGM.swift
//  shoppingappMX
//
//  Created by Javier Dominguez on 13/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit
import BGMConnectSDK

class LoginLoaderPresenterBGM<T: LoginLoaderViewProtocolBGM>: LoginLoaderPresenterA<T> {
    
    var environmentNeedsExtraSecurity = ServerHostURL.environmentNeedsExtraSecurity()
    
    override func requestGrantingTicket(loginTransport: ModelTransport, consumerId: String) {
        
        interactor.provideLoginData(loginTransport: loginTransport, consumerId: consumerId).subscribe(
            onNext: { [weak self] result in
                
                if let grandTingTicketBO = result as? GrandTingTicketBO {
                    self?.grantingTicketBO = grandTingTicketBO
                    
                    if let environmentNeedsExtraSecurity = self?.environmentNeedsExtraSecurity, !environmentNeedsExtraSecurity {
                        
                        self?.requestCustomerData()
                        
                    } else if let tsec = BusManager.sessionDataInstance.headerInterceptorStore?.getTsec(), let grantingTicketResponse = self?.grantingTicketBO, let loginTransport = loginTransport as? LoginTransport {
                      
                        self?.view?.checkDeviceActivation(loginTransport: loginTransport, grantingTicketResponse: grantingTicketResponse, tsec: tsec)
                   
                    }
                    
                }
                
            },
            onError: {[ weak self] error in
                
                DLog(message: "\(error)")
                guard let `self` = self else {
                    return
                }
                let evaluate: ServiceError = (error as? ServiceError)!
                
                switch evaluate {
                    
                case .GenericErrorBO(let errorBO):
                    
                    if errorBO.status == 403, let errorCode = errorBO.code, (errorCode == "160" || errorCode == "161") {
                        
                        let error = ErrorBO(error: ErrorEntity(message: Localizables.login.key_login_user_pass_error_message))
                        error.status = errorBO.status
                        self.busManager.navigateScreen(tag: self.routerTag, LoginLoaderPageReaction.EVENT_ERROR_LOGIN, error)
                        
                    } else {
                        self.busManager.navigateScreen(tag: self.routerTag, LoginLoaderPageReaction.EVENT_ERROR_LOGIN, errorBO)
                    }
                default:
                    break
                    
                }
        }).addDisposableTo(disposeBag)
    }
}

extension LoginLoaderPresenterBGM: LoginLoaderPresenterProtocolBGM {

    func deviceActivationSuccess() {
        
        requestCustomerData()
    }
    
}
