//
//  LoginPresenter.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 7/3/17.
//  Copyright © 2017 daniel.petrovic. All rights reserved.
//

import Foundation
import RxSwift
import CellsNativeComponents
import CellsNativeCore

class LoginLoaderPresenter<T: LoginLoaderViewProtocol>: BasePresenter<T> {
    
    var interactor: LoginLoaderInteractorProtocol!
    
    var cards: CardsBO?
    
    var loginTransport: LoginTransport?

    private var recognizerUser = false

    var grantingTicketBO: GrandTingTicketBO?
    
    let disposeBag = DisposeBag()
    
    override init(busManager: BusManager) {
        
        super.init(busManager: busManager)
        routerTag = LoginLoaderPageReaction.ROUTER_TAG
    }
    
    required init() {
        
        super.init(tag: LoginLoaderPageReaction.ROUTER_TAG)

        recognizerUser = KeychainManager.shared.isUserSaved()

    }
    
    func viewDidLoad() {
        
        var loadingTitle = Localizables.login.key_login_loading_text
        
        if loginTransport != nil {
            requestConsumerId()
        } else {
            loadingTitle = Localizables.login.key_loading_promotions
            requestAnonymousInformation()
        }
        
        view?.showUnlockTitle(loadingTitle)
    
    }
    
    func requestConsumerId() {
        
        interactor.provideConsumerId().subscribe(
            onNext: { [weak self] result in
                
                guard let `self` = self else {
                    return
                }
                self.requestGrantingTicket(loginTransport: self.loginTransport!, consumerId: result)
            },
            onError: { [weak self] error in
                
                DLog(message: "\(error)")
                
                guard let `self` = self else {
                    return
                }
                
                let evaluate: ServiceError = (error as? ServiceError)!
                
                switch evaluate {
                    
                case .GenericErrorBO(let errorBO):
                    self.busManager.navigateScreen(tag: self.routerTag, LoginLoaderPageReaction.EVENT_ERROR_LOGIN, errorBO)
                default:
                    break
                    
                }
                
        }).addDisposableTo(disposeBag)
    }
    
    func requestAnonymousInformation() {
        
        interactor.provideAnonymousInformation().subscribe(
            onNext: { [weak self] result in
                
                if let anonymousInformation = result as? AnonymousInformationBO {
                    self?.requestGrantingTicket(withAnonymousInformation: anonymousInformation)
                }
            },
            onError: { [weak self] error in
                
                DLog(message: "\(error)")
                
                guard let `self` = self else {
                    return
                }
                let evaluate: ServiceError = (error as? ServiceError)!
                
                switch evaluate {
                    
                case .GenericErrorBO(let errorBO):
                    self.busManager.navigateScreen(tag: self.routerTag, LoginLoaderPageReaction.EVENT_ERROR_LOGIN, errorBO)
                default:
                    break
                }
                
        }).addDisposableTo(disposeBag)
    }
    
    func requestGrantingTicket(loginTransport: ModelTransport, consumerId: String) {
        
        interactor.provideLoginData(loginTransport: loginTransport, consumerId: consumerId).subscribe(
            onNext: { [weak self] result in
                
                if let grandTingTicketBO = result as? GrandTingTicketBO {
                    self?.grantingTicketBO = grandTingTicketBO
                    self?.requestCustomerData()
                }
                
            },
            onError: {[ weak self] error in
                
                DLog(message: "\(error)")
                guard let `self` = self else {
                    return
                }
                let evaluate: ServiceError = (error as? ServiceError)!
                
                switch evaluate {
                    
                case .GenericErrorBO(let errorBO):
                    
                    if errorBO.status == 403, let errorCode = errorBO.code, (errorCode == "160" || errorCode == "161") {

                        let error = ErrorBO(error: ErrorEntity(message: Localizables.login.key_login_user_pass_error_message))
                        error.status = errorBO.status
                        self.busManager.navigateScreen(tag: self.routerTag, LoginLoaderPageReaction.EVENT_ERROR_LOGIN, error)
                        
                    } else {
                        self.busManager.navigateScreen(tag: self.routerTag, LoginLoaderPageReaction.EVENT_ERROR_LOGIN, errorBO)
                    }
                default:
                    break
                    
                }
        }).addDisposableTo(disposeBag)
    }
    
    fileprivate func requestGrantingTicket(withAnonymousInformation anomymousInformation: AnonymousInformationBO) {
        
        interactor.requestGrantingTicket(withAnonymousInformation: anomymousInformation).subscribe(
            onNext: { [weak self] _ in
                
                self?.view?.startUnlockAnimation()
                
            },
            onError: { [weak self] error in
                
                DLog(message: "\(error)")
                
                guard let `self` = self else {
                    return
                }
                
                let evaluate: ServiceError = (error as? ServiceError)!
                
                switch evaluate {
                    
                case .GenericErrorBO(let errorBO):
                    self.busManager.navigateScreen(tag: self.routerTag, LoginLoaderPageReaction.EVENT_ERROR_LOGIN, errorBO)
                default:
                    break
                    
                }
        }).addDisposableTo(disposeBag)
    }
    
    func requestCustomerData() {
        
        interactor.provideCustomerData().subscribe(
            onNext: { [weak self] result in
                
                if let user = (result as? UserBO) {
                    self?.getCards(withUser: user)
                }
            },
            onError: { [weak self] error in
                
                DLog(message: "\(error)")
                guard let `self` = self else {
                    return
                }
                let evaluate: ServiceError = (error as? ServiceError)!
                
                switch evaluate {
                    
                case .GenericErrorBO(let errorBO):
                    self.busManager.navigateScreen(tag: self.routerTag, LoginLoaderPageReaction.EVENT_ERROR_LOGIN, errorBO)
                default:
                    break
                    
                }
                
        }).addDisposableTo(disposeBag)
    }
    
    func getCards(withUser user: UserBO) {
        
        interactor.provideCards().subscribe(
            onNext: { [weak self] result in
                
                guard let `self` = self else {
                    return
                }
                
                self.configLoginNumber()
                
                self.view?.startUnlockAnimation()
                
                if let cardsBO = result as? CardsBO {
                    
                    self.cards = cardsBO
                    
                    SessionDataManager.sessionDataInstance().customerID = user.customerId
                    if !KeychainManager.shared.isUserSaved() {
                        KeychainManager.shared.saveUserId(forUserId: self.loginTransport?.userId ?? "")
                        KeychainManager.shared.saveFirstname(forFirstname: user.firstName ?? "")
                    }
                }
            },
            onError: { [weak self] error in
                
                DLog(message: "\(error)")
                guard let `self` = self else {
                    return
                }
                let evaluate: ServiceError = (error as? ServiceError)!
                
                switch evaluate {
                    
                case .GenericErrorBO(let errorBO):
                    self.busManager.navigateScreen(tag: self.routerTag, LoginLoaderPageReaction.EVENT_ERROR_LOGIN, errorBO)
                default:
                    break
                    
                }
        }).addDisposableTo(disposeBag)
    }
    
    override func setModel(model: Model) {
        
        if let loginTransport = model as? LoginTransport {
            self.loginTransport = loginTransport
        }
    }
    
    func configLoginNumber() {
        
        let loginNumber = PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kLoginNumber) as? NSNumber
        if let loginNumber = loginNumber {
            let newLoginNumber = loginNumber.intValue + 1
            PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: newLoginNumber), withKey: PreferencesManagerKeys.kLoginNumber)
        } else {
            PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: 1 as AnyObject, withKey: PreferencesManagerKeys.kLoginNumber)
        }
        
        let numberLoginsRateApp = PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kNumberLoginsRateApp) as? NSNumber
        if let numberLoginsRateApp = numberLoginsRateApp {
            let newNumberLoginsRateApp = numberLoginsRateApp.intValue + 1
            PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: newNumberLoginsRateApp), withKey: PreferencesManagerKeys.kNumberLoginsRateApp)
        } else {
            PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: 1 as AnyObject, withKey: PreferencesManagerKeys.kNumberLoginsRateApp)
        }
    }
}

extension LoginLoaderPresenter: LoginLoaderPresenterProtocol {
    
    func didFinishUnlockAnimation() {
        
        if let cards = cards {

            if recognizerUser {

                TrackerHelper.sharedInstance().trackLoginSuccessEvent(true)
            } else {

                TrackerHelper.sharedInstance().trackLoginSuccessEvent(false)
            }
            SessionDataManager.sessionDataInstance().isUserLogged = true
            
            busManager.publishData(tag: ShoppingCrossReaction.TAG, PageReactionConstants.PARAMS_CARDS, cards)
            
            busManager.navigateScreen(tag: routerTag, LoginLoaderPageReaction.EVENT_LOGIN_FINISHED, Void())
            
            PreferencesManager.sharedInstance().saveValue(forValue: true as AnyObject, withKey: PreferencesManagerKeys.kWelcomeShowed.rawValue)
            
        } else {
            
            SessionDataManager.sessionDataInstance().isUserAnonymous = true

            busManager.navigateScreen(tag: routerTag, LoginLoaderPageReaction.EVENT_ANONYMOUS_SESSION, Void())
            
        }
        
    }
    
}
