//
//  LoginLoaderPresenterA.swift
//  shoppingappMX
//
//  Created by Jesús Ángel Sánchez Sánchez on 14/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import RxSwift
import BGMConnectSDK

class LoginLoaderPresenterA<T: LoginLoaderViewProtocolA>: LoginLoaderPresenter<T> {
    
    override func viewDidLoad() {
        
        if let userId = loginTransport?.userId, let password = loginTransport?.password {
            
            let loginSha256 = (userId + password).sha256()
            
            if loginSha256 == Settings.Login.userMock {
                
                ServerHostURL.setEnvironmentAndHostForUserMock(activate: true)
            } else {
                ServerHostURL.setEnvironmentAndHostForUserMock(activate: false)
            }
        }
        
        super.viewDidLoad()
    }
    
    override func getCards(withUser user: UserBO) {
        
        interactor.provideCards().subscribe(
            onNext: { [weak self] result in
                
                guard let `self` = self else {
                    return
                }
                
                self.configLoginNumber()
                
                self.view?.startUnlockAnimation()
                
                if let cardsBO = result as? CardsBO {
                    
                    self.cards = cardsBO
                    
                    if let nfcCard = self.cards?.firstNfcCard() {
                        self.view?.downgradeService(withCardId: nfcCard.cardId, andResetIndicator: true)
                    }
                    
                    SessionDataManager.sessionDataInstance().customerID = user.customerId
                   
                    KeychainManager.shared.saveUserId(forUserId: self.loginTransport?.userId ?? "")
                    KeychainManager.shared.saveFirstname(forFirstname: user.firstName ?? "")
                    KeychainManager.shared.saveLastLoggedUserId(forUserId: self.loginTransport?.userId ?? "")
                    
                    if ServerHostURL.getSelectedEnvironment() == ServerHostURL.Environment.artichoke {
                        GCSController.getCore().getkeyChainManager()?.saveTokenType(tokenType: "S2")
                        GCSController.getCore().getkeyChainManager()?.saveUrlScheme(urlScheme: "bbvawalletiphonemx") // this is the url scheme of the centro app, must be the same when initializing connect in AppDelegate: GCSController.getCore().setUrlScheme(urlScheme: "bbvawalletiphonemx")
                    }
                }
                
            },
            onError: { [weak self] error in
                
                DLog(message: "\(error)")
                guard let `self` = self else {
                    return
                }
                let evaluate: ServiceError = (error as? ServiceError)!
                
                switch evaluate {
                    
                case .GenericErrorBO(let errorBO):
                    self.busManager.navigateScreen(tag: self.routerTag, LoginLoaderPageReaction.EVENT_ERROR_LOGIN, errorBO)
                default:
                    break
                    
                }
        }).addDisposableTo(disposeBag)
    }
}
