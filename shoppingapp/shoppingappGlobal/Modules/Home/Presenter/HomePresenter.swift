//
//  HomePresenter.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 7/3/17.
//  Copyright © 2017 daniel.petrovic. All rights reserved.
//

import Foundation
import RxSwift
import CellsNativeComponents

class HomePresenter<T: HomeViewProtocol>: BasePresenter<T> {

    var interactor: HomeInteractorProtocol!
    let disposeBag = DisposeBag()

    var cards: CardsBO?

    var notificationManager: NotificationManager?
    
    lazy var publicConfiguration = PublicConfigurationManager.sharedInstance().localPublicConfiguration()

    override init(busManager: BusManager) {

        super.init(busManager: busManager)
        routerTag = HomePageReaction.ROUTER_TAG
    }

    required init() {
        
        super.init(tag: HomePageReaction.ROUTER_TAG)
    }

    func receive(model: Model) {

        if let cardsReceived = model as? CardsBO {
            if cardsReceived.timestamp != cards?.timestamp {
                cards = cardsReceived
                showCardList()
                view?.showTrendingPromotion()
            }
        }
    }

    func didFailToRegisterForRemoteNotifications() {
        //
    }

    func didSuccessRegisterForRemoteNotificationsWithDeviceToken(_ token: String) {

        view?.registerNotificationsToken(token)
    }

    func registerTokenSuccess(withToken token: String) {

        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: token as AnyObject, withKey: PreferencesManagerKeys.kRegisterDeviceToken)
    }

    func registerTokenFailed() {
        //
    }
    
    private func checkNotificationStatus(_ loginNumber: NSNumber?, _ isFirstLogin: Bool) {
        
        let userCloseNotificationsAdvise = PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kUserCloseNotificationsAdvise) as? Bool

        let isLessThanFourLoginAndUserCloseNotificationAdvise = (loginNumber != nil && loginNumber!.intValue <= 3) && (userCloseNotificationsAdvise != nil && userCloseNotificationsAdvise! == true)

        if let notificationManager = notificationManager, isFirstLogin || isLessThanFourLoginAndUserCloseNotificationAdvise {
            notificationManager.checkAuthorizedStatus { [weak self] determined in
                if !determined {
                    DispatchQueue.main.async {
                        self?.busManager.navigateScreen(tag: HomePageReaction.ROUTER_TAG, HomePageReaction.EVENT_NAV_TO_ACTIVATE_DEVICE_PERMISSIONS_SCREEN, Void())
                    }
                } else {
                    
                    notificationManager.isAuthorizedStatus({ [weak self] authorized in

                        DispatchQueue.main.async {

                            guard let `self` = self else {
                                return
                            }

                            let configureNotifications = ConfigureNotificationsTransport(isPermissionAccepted: authorized)

                            if let configureNotificationsJSON = configureNotifications.toJSON() {
                                
                                self.busManager.publishData(tag: HomePageReaction.ROUTER_TAG, HomePageReaction.PARAMS_CONFIGURE_DEVICE_PERMISSIONS, configureNotificationsJSON)
                                self.busManager.navigateScreen(tag: HomePageReaction.ROUTER_TAG, HomePageReaction.EVENT_NAV_TO_CONFIGURE_DEVICE_PERMISSIONS_SCREEN, Void())
                            }

                        }
                        
                    })
                }
            }
        } else {
            let apnsToken = PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kApnsToken) as? String
            let registerToken = PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kRegisterDeviceToken) as? String

            if let apnsToken = apnsToken, apnsToken != registerToken {
                view?.registerNotificationsToken(apnsToken)
            }
        }
    }
    
    private func checkAppRateStatus() {
        
        guard let hasRateApp = publicConfiguration.publicConfig?.hasRateApp, hasRateApp else {
            return
        }
        
        if #available(iOS 10.3, *) {
            
            view?.showRateApp()
        } else {
            
            let numberLoginsRateApp = PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kNumberLoginsRateApp) as? NSNumber
            
            let appRated = PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kAppRated) as? Bool ?? false
            
            let userSkipRateApp = PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kUserSkipRateApp) as? Bool ?? false
            
            if let numberLoginsRateApp = numberLoginsRateApp, numberLoginsRateApp.intValue % Settings.Global.number_logins_to_show_rate_app == 0, appRated == false, userSkipRateApp == false {
                
                view?.showRateApp()
            }
        }
    }
}

extension HomePresenter: HomePresenterProtocol {

    func viewDidLoad() {

        let isFirstLogin = PreferencesManager.sharedInstance().isFirstLogin()
        let loginNumber = PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kLoginNumber) as? NSNumber

        checkNotificationStatus(loginNumber, isFirstLogin)
        checkAppRateStatus()
    }

    func getDidacticArea() {

        let didacticArea = ConfigPublicManager.sharedInstance().getDidacticAreaToShow()

        let didacticAreaDisplayData = DidacticAreaDisplayData.generateDidacticAreaDisplayData(withDidacticAreaBO: didacticArea.didacticAreaBO, andDidacticCategoryBO: didacticArea.didacticCategoryBO)

        view?.showDidacticAreaDisplayData(withDisplayData: didacticAreaDisplayData)
    }

    func cardStatusUpdated(card: CardModel) {
        guard let cards = cards else {
            return
        }

        for index in 0..<cards.cards.count where cards.cards[index].cardId == card.cardBO.cardId {
            cards.cards[index] = card.cardBO
        }

        showCardList()
    }

    private func showCardList() {
        if let cards = cards {

            interactor.provideCurrency().subscribe(
                onNext: { [weak self] result in

                    if let currency = result as? CurrencyBO {
                        self?.view?.setCardModel(model: CardListModel(cardBO: cards, currencyCode: currency.code, cardTypesToShow: Settings.Home.cardTypesToShowInHome, showPoints: Settings.Home.showPointsInHome))
                    }
                }
                ).addDisposableTo(disposeBag)
        }
    }

    func showNavigationBarAnimation(byScrollViewHeight scrollViewHeight: CGFloat, withContentSize contentSize: CGSize, andContentOffset contentOffset: CGPoint) {

        let offsetAnimation: CGFloat = 30.0
        var offset = contentOffset.y / offsetAnimation

        if contentSize.height > scrollViewHeight + offsetAnimation {

            if offset > 1 {
                offset = 1
                let color = UIColor.NAVY.withAlphaComponent(offset)
                view?.showNavigationTitle()
                view?.changeNavigationBarColor(withColor: color)

            } else {

                let color = UIColor.NAVY.withAlphaComponent(offset)
                view?.hideNavigationTitle()
                view?.changeNavigationBarColor(withColor: color)
            }
        }
    }

    func rateAppDialogUserChoiceSkip() {
        
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: true as AnyObject, withKey: PreferencesManagerKeys.kUserSkipRateApp)
    }

    func rateAppDialogUserChoiceRate() {
        
        guard let url = publicConfiguration.publicConfig?.update?.iOS?.URL else {
            return
        }
        
        view?.openURL(url)
        
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: true as AnyObject, withKey: PreferencesManagerKeys.kAppRated)
    }
}

extension HomePresenter: CardsListPresenterDelegate {

    func cardsListPresenter(_ cardsListPresenter: CardsListPresenter, didSelectCardId cardId: String) {

        if let cardBO = cards?.cards.first(where: { cardBO -> Bool in
            cardBO.cardId == cardId
        }) {
            let model = CardsTransport(cardBO: cardBO)
            busManager.navigateScreen(tag: HomePageReaction.ROUTER_TAG, HomePageReaction.EVENT_CHANGE_TAB_TO_CARDS, model)
        }

    }

}

extension HomePresenter: TrendingPromotionPresenterDelegate {

    func trendingPromotionCardsForFilter(_ trendingPromotionPresenter: TrendingPromotionPresenter) -> CardsBO? {
        return cards
    }

    func trendingPromotionPresenter(_ trendingPromotionPresenter: TrendingPromotionPresenter, didReceiveError error: ErrorBO) {

        errorBO = error
        checkError()
    }

    func trendingPromotionHeaderPressed(_ trendingPromotionPresenter: TrendingPromotionPresenter) {

        busManager.navigateScreen(tag: HomePageReaction.ROUTER_TAG, HomePageReaction.EVENT_CHANGE_TAB_TO_PROMOTIONS, Void())
    }

    func trendingPromotionViewPressed(_ trendingPromotionPresenter: TrendingPromotionPresenter, promotion: PromotionBO) {

        let promotionDetailTransport = PromotionDetailTransport()
        promotionDetailTransport.promotionBO = promotion

        busManager.navigateScreen(tag: HomePageReaction.ROUTER_TAG, HomePageReaction.EVENT_NAV_TO_PROMOTION_DETAIL_SCREEN, promotionDetailTransport)
    }
}

extension HomePresenter: DidacticAreaPresenterDelegate {

    func didacticAreaPresenterDidacticButtonPressed(_ didacticAreaPresenter: DidacticAreaPresenter, andHelpId helpId: String) {

        if helpId.isEmpty {
            busManager.navigateScreen(tag: HomePageReaction.ROUTER_TAG, HomePageReaction.EVENT_NAV_TO_HELP, EmptyModel())
        } else {
            busManager.navigateScreen(tag: HomePageReaction.ROUTER_TAG, HomePageReaction.EVENT_NAV_TO_DETAIL_HELP, HelpDetailTransport(helpId))
        }
    }

    func didacticAreaPresenterOfferButtonPressed(_ didacticAreaPresenter: DidacticAreaPresenter, categoryId: String, andTitle title: String) {
        
        guard !categoryId.isEmpty, categoryId != CategoryType.unknown.rawValue, !title.isEmpty else {
            return
        }
        
        let promotionsTransport = PromotionsTransport()
        promotionsTransport.promotionName = title
        let categoryEntity = CategoryEntity(id: categoryId, name: title)
        let category = CategoryBO(categoryEntity: categoryEntity)
        promotionsTransport.category = category
        busManager.navigateScreen(tag: HomePageReaction.ROUTER_TAG, HomePageReaction.EVENT_NAV_TO_PROMOTIONS_CATEGORY_SCREEN, promotionsTransport)
    }
    
    func didacticAreaPresenterSpecialOfferButtonPressed(_ didacticAreaPresenter: DidacticAreaPresenter, _ url: URL, andTitle title: String) {
        
        guard !url.absoluteString.isEmpty, !title.isEmpty else {
            return
        }
        
        let transport = WebTransport()
        transport.webPage = url.absoluteString
        transport.webTitle = title
        busManager.navigateScreen(tag: HomePageReaction.ROUTER_TAG, HomePageReaction.EVENT_NAV_TO_WEBPAGE_SCREEN, transport)
    }
}
