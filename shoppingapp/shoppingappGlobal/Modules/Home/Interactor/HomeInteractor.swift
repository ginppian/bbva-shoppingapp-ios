//
//  HomeInteractor.swift
//  shoppingapp
//
//  Created by jesus.martinez on 5/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation
import RxSwift

class HomeInteractor: HomeInteractorProtocol {

    let disposeBag = DisposeBag()

    func provideTransaction(forCardId cardId: String) -> Observable <ModelBO> {

        return Observable.create { observer in

            TransactionsDataManager.sharedInstance.provideTransactions(forCardId: cardId).subscribe(
                onNext: { result in

                    if let transactionsEntity = result as? TransactionsEntity {
                        observer.onNext(TransactionsBO(transactionsEntity: transactionsEntity))
                        observer.onCompleted()
                    } else {
                        DLog(message: "Incorrect TransactionsEntity")
                    }
                },

                onError: { error in

                    let evaluate : ServiceError = (error as? ServiceError)!

                    switch evaluate {
                    case .GenericErrorEntity(let errorEntity):
                        let errorBVABO = ErrorBO(error: errorEntity)
                        observer.onError(ServiceError.GenericErrorBO(error: errorBVABO))
                    default:
                        observer.onError(evaluate)
                    }

                },
                onCompleted: {
                }
                ).addDisposableTo(self.disposeBag)
            return Disposables.create()
        }
    }

    func provideCurrency() -> Observable<ModelBO> {

        return Observable.create { observer in

            ConfigurationDataManager.sharedInstance.provideMainCurrency().subscribe(

                onNext: { result in

                    if let currencyEntity = result as? CurrencyEntity {

                        let currencyBO = Currencies.currency(forCode: currencyEntity.code)

                        observer.onNext(currencyBO)
                        observer.onCompleted()
                    } else {
                        DLog(message: "Incorrect CurrencyEntity")

                    }

                }).addDisposableTo(self.disposeBag)

            return Disposables.create()
        }
    }
}
