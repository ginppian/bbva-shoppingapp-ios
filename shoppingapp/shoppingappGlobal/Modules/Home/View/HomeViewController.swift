//
//  HomeViewController.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 8/3/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit
import CellsNativeCore
import CellsNativeComponents
import StoreKit

class HomeViewController: BaseViewController {

    // MARK: Constants

    static let ID: String = String(describing: HomeViewController.self)

    // MARK: outlets
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var didacticAreaView: DidacticAreaView!
    @IBOutlet weak var didacticAreaViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var cardsViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var navigationBarView: UIView!
    @IBOutlet weak var navigationBarViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var cardListHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomStack: UIStackView!
    @IBOutlet weak var trendingPromotionView: TrendingPromotionView! {
        didSet {
            trendingPromotionView.presenter.delegate = presenter as? TrendingPromotionPresenterDelegate
        }
    }
    @IBOutlet weak var cardListView: CardsListView! {
        didSet {
            cardListView.presenter.delegate = presenter as? CardsListPresenterDelegate
            cardListView.delegate = self
        }
    }

    // MARK: vars

    var presenter: HomePresenterProtocol!

    var registerNotificationsStore: RegisterNotificationsStore?

    let statusHeight = UIApplication.shared.statusBarFrame.height

    var transitionImageView: UIImageView?

    // MARK: life cycle

    override func viewDidLoad() {

        self.configure(appLifecycle: (UIApplication.shared.delegate as! AppDelegate).appLifeCycle!, id: HomeViewController.ID)

        if let networkWorker = BusManager.sessionDataInstance.networkWorker {
            registerNotificationsStore = RegisterNotificationsStore(worker: networkWorker, host: ServerHostURL.getDevicesURL())
        }

        addPointsWidget()

        super.viewDidLoad()

        edgesForExtendedLayout = .top

        self.view.backgroundColor = .BBVA100

        if #available(iOS 11.0, *) {
            scrollView.contentInsetAdjustmentBehavior = .never
        } else {
            self.automaticallyAdjustsScrollViewInsets = false
        }

        scrollView.bounces = false
        scrollView.delegate = self

        navigationBarView.backgroundColor = .clear
        navigationBarViewHeightConstraint.constant = 44.0 + statusHeight

        if UIDevice.current.hasNotch {
            let space = didacticAreaView.configureViewToStatusBarBigHeight(withHeightConstraint: didacticAreaViewHeightConstraint)
            cardsViewTopConstraint.constant += space
        }
        didacticAreaView.configureViewItems()
        didacticAreaView.presenter.delegate = presenter as? DidacticAreaPresenterDelegate

        presenter.getDidacticArea()

        setNavigationTitle(withText: Localizables.common.key_tabbar_start_button)
        hideNavigationTitle()

        presenter.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        setNavigationBarTransparent()
        addNavigationBarMenuButton()
        setAccessibilities()

        trackScreen()
    }
    
    override func viewWillAppearAfterDismiss() {
        
        trackScreen()
    }
    
    fileprivate func trackScreen() {
        
        TrackerHelper.sharedInstance().trackShowHomeBanner(withCustomerID: SessionDataManager.sessionDataInstance().customerID,
                                                           campaignName: didacticAreaView.currentDisplayData?.omnitureCampaignName.removeSpecialCharsAndAccentFromString)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        cardListView.dropShadow(color: UIColor(white: 0.0, alpha: 0.6), opacity: 1, offSet: CGSize(width: 0, height: 0))

    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        didacticAreaView.addShapeLayerToView()
    }

    deinit {
        ComponentManager.remove(id: presenter.routerTag)
        ComponentManager.remove(id: RegisterNotificationsStore.ID)
        DLog(message: "HomeViewController deinitialized")
    }

    func cardStatusUpdated(card: CardModel) {
        presenter.cardStatusUpdated(card: card)
        view.setNeedsLayout()
    }

    func addPointsWidget() {

        if let pointsWidget = Settings.Home.viewControllerForCustomerPoints {

            addChildViewController(pointsWidget)
            bottomStack.insertArrangedSubview(pointsWidget.view, at: 0)
            pointsWidget.didMove(toParentViewController: self)
        }
    }

    func tsecExpiredPointsWidgetError(_ error: ErrorBO) {
        presenter.errorBO = error
        presenter.checkError()
    }
    
    private func showRateAppiOSVersionsBefore10_3() {
        
        let rateAction = UIAlertAction(title: Localizables.others.key_rate_this_app_rate_text, style: .default) { [weak self]  _ in
            
            self?.presenter.rateAppDialogUserChoiceRate()
        }
        
        let delayRateAction = UIAlertAction(title: Localizables.others.key_rate_this_app_remind_me_text, style: .default)
        
        let skipRateAction = UIAlertAction(title: Localizables.others.key_rate_this_app_dismiss_text, style: .default) { [weak self] _ in
            
            self?.presenter.rateAppDialogUserChoiceSkip()
        }
        
        let rateAppAlert = UIAlertController(title: Localizables.others.key_rate_this_app_header_text, message: Localizables.others.key_rate_this_app_description_ios_text, preferredStyle: .alert)
        
        rateAppAlert.addAction(rateAction)
        rateAppAlert.addAction(delayRateAction)
        rateAppAlert.addAction(skipRateAction)
        
        present(rateAppAlert, animated: true, completion: nil)
    }
}

extension HomeViewController: HomeViewProtocol {

    func showError(error: ModelBO) {
        presenter.showModal()
    }

    func changeColorEditTexts() {

    }

    func showDidacticAreaDisplayData(withDisplayData displayData: DidacticAreaDisplayData) {
        didacticAreaView.setDidacticAreaDisplayData(withDisplayData: displayData)
    }

    func changeNavigationBarColor(withColor color: UIColor) {
        navigationBarView.backgroundColor = color
    }

    func setCardModel(model: CardListModelProtocol) {
        cardListView.presenter.setModel(model: model)
    }

    func showTrendingPromotion() {
        trendingPromotionView.presenter.viewLoaded()
    }

    func showNavigationTitle() {
        navigationItem.titleView?.isHidden = false

    }

    func hideNavigationTitle() {
        navigationItem.titleView?.isHidden = true
    }
    
    func registerNotificationsToken(_ registerToken: String) {
        
        if let device = DeviceManager.instance.deviceUID {
            registerNotificationsStore?.registerNotificationsToken(registerNotificationsToken: RegisterNotificationsTokenDTO(token: registerToken, deviceId: device, applicationId: DeviceManager.instance.applicationId, serviceId: RegisterNotificationsStore.serviceId))
        }
    }

    func showRateApp() {
        
        if #available(iOS 10.3, *) {
            
            SKStoreReviewController.requestReview()
        } else {
            
            showRateAppiOSVersionsBefore10_3()
        }
    }
    
    func openURL(_ url: String) {
        
        URLOpener().openURL(url)
    }
}

extension HomeViewController: UIScrollViewDelegate {

    func scrollViewDidScroll(_ scrollView: UIScrollView) {

        presenter.showNavigationBarAnimation(byScrollViewHeight: scrollView.bounds.height, withContentSize: scrollView.contentSize, andContentOffset: scrollView.contentOffset)
    }
}

extension HomeViewController: CardsListViewDelegate {
    func cardsListViewImagePressed(_ cardsListView: CardsListView, imagePressed: UIImageView) {
        transitionImageView = imagePressed
    }
    
    func cardsListViewWillDisplay(_ cardsListView: CardsListView, estimatedHeight: CGFloat) {
        
        cardListHeightConstraint.constant = estimatedHeight
    }
    
    func cardsListViewLoaded(_ cardsListView: CardsListView, height: CGFloat) {
        
        cardListHeightConstraint.constant = height
    }
}

extension HomeViewController: TransitionerOutAnimationProtocol {

    func transitionFrame() -> CGRect? {

        guard let transitionImageView = transitionImageView else {
            return nil
        }

        let convertedPoint = transitionImageView.convert(CGPoint.zero, to: view)

        return CGRect(x: convertedPoint.x, y: convertedPoint.y, width: transitionImageView.frame.size.width, height: transitionImageView.frame.size.height)
    }

}

extension HomeViewController: CloseModalDelegate {

    func errorViewControllerAcceptPressed(_ errorViewController: ErrorViewController) {
        presenter.checkError()
    }

    func errorViewControllerCancelPressed(_ errorViewController: ErrorViewController) {
    }

}

// MARK: - APPIUM

private extension HomeViewController {

    func setAccessibilities() {

        #if APPIUM

            self.view.accessibilityIdentifier = ConstantsAccessibilities.HOME_VIEW

        #endif
    }

}
