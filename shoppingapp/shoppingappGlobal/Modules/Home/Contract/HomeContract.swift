//
//  HomeContract.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 15/3/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit
import RxSwift

protocol HomeViewProtocol: ViewProtocol {

    func showDidacticAreaDisplayData(withDisplayData displayData: DidacticAreaDisplayData)
    func showNavigationTitle()
    func hideNavigationTitle()
    func changeNavigationBarColor(withColor color: UIColor)
    func setCardModel(model: CardListModelProtocol)
    func showTrendingPromotion()
    func registerNotificationsToken(_ registerToken: String)
    func showRateApp()
    func openURL(_ url: String)
}

protocol HomePresenterProtocol: PresenterProtocol {

    func viewDidLoad()
    func getDidacticArea()
    func cardStatusUpdated(card: CardModel)
    func showNavigationBarAnimation(byScrollViewHeight scrollViewHeight: CGFloat, withContentSize contentSize: CGSize, andContentOffset contentOffset: CGPoint)
    func rateAppDialogUserChoiceRate()
    func rateAppDialogUserChoiceSkip()
}

protocol HomeInteractorProtocol {
    func provideCurrency() -> Observable<ModelBO>
}
