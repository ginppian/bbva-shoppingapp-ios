//
//  HomePageReactionA.swift
//  shoppingapp
//
//  Created by Marcos on 30/7/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class HomePageReactionA: HomePageReaction {

    override func configure() {
        super.configure()

        tsecExpiredPointsWidgetError()
    }

    func tsecExpiredPointsWidgetError() {

        _ = when(id: PointsWidgetPageReaction.ROUTER_TAG, type: ErrorBO.self)
            .doAction(actionSpec: PointsWidgetPageReaction.ON_TSEC_EXPIRED)
            .then()
            .inside(componentID: HomeViewController.ID, component: HomeViewController.self)
            .doReaction { viewController, error in

                if let error = error {
                    viewController.tsecExpiredPointsWidgetError(error)
                }

            }
    }
}
