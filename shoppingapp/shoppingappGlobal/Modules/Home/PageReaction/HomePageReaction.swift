//
//  HomePageReaction.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 26/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

class HomePageReaction: PageReaction {

    static let ROUTER_TAG: String = "home-tag"
    static let EVENT_CHANGE_TAB_TO_CARDS: ActionSpec<Model> = ActionSpec<Model>(id: "EVENT_CHANGE_TAB_TO_CARDS")
    static let EVENT_CHANGE_TAB_TO_PROMOTIONS: ActionSpec<Void> = ActionSpec<Void>(id: "EVENT_CHANGE_TAB_TO_PROMOTIONS")
    static let EVENT_CHANGE_TAB_TO_POINTS: ActionSpec<Void> = ActionSpec<Void>(id: "EVENT_CHANGE_TAB_TO_POINTS")
    static let EVENT_NAV_TO_DETAIL_HELP: ActionSpec<Model> = ActionSpec<Model>(id: "EVENT_NAV_TO_DETAIL_HELP")
    static let EVENT_NAV_TO_HELP: ActionSpec<Model> = ActionSpec<Model>(id: "EVENT_NAV_TO_HELP")
    static let EVENT_NAV_TO_PROMOTION_DETAIL_SCREEN: ActionSpec<Model> = ActionSpec<Model>(id: "EVENT_NAV_TO_PROMOTION_DETAIL_SCREEN")
    static let EVENT_NAV_TO_PROMOTIONS_CATEGORY_SCREEN: ActionSpec<Model> = ActionSpec<Model>(id: "EVENT_NAV_TO_PROMOTIONS_CATEGORY_SCREEN")
    static let EVENT_NAV_TO_WEBPAGE_SCREEN: ActionSpec<Model> = ActionSpec<Model>(id: "EVENT_NAV_TO_WEBPAGE_SCREEN")
    static let EVENT_NAV_TO_ACTIVATE_DEVICE_PERMISSIONS_SCREEN: ActionSpec<Void> = ActionSpec<Void>(id: "EVENT_NAV_TO_ACTIVATE_DEVICE_PERMISSIONS_SCREEN")
    static let EVENT_NAV_TO_CONFIGURE_DEVICE_PERMISSIONS_SCREEN: ActionSpec<Void> = ActionSpec<Void>(id: "EVENT_NAV_TO_CONFIGURE_DEVICE_PERMISSIONS_SCREEN")
    static let EVENT_CLOSE_CELL_STACK: ActionSpec<Void> = ActionSpec<Void>(id: "close_cell_stack")
    static let PARAMS_CONFIGURE_DEVICE_PERMISSIONS: ActionSpec<Any> = ActionSpec<Any>(id: "PARAMS_CONFIGURE_DEVICE_PERMISSIONS")

    static let CONFIGURE_DEVICE_PERMISSIONS_CHANNEL = "cdp_info_channel"
    
    override func configure() {

        cardUpdated()
        changeTabToCards()
        changeTabToPromotions()
        changeTabToPoints()
        navigateToHelpDetail()
        navigateToHelp()
        navigateToPromotionDetailScreen()
        navigateToPromotionsCategoryScreen()
        navigateToWebPageScreen()
        registerToNavigateToActivateDevicePermissionScreen()
        registerToNavigateToConfigureDevicePermissionScreen()
        reactCards()
        reactTokenChanged()
        reactTokenFailed()
        registerToLisentRegisterNotificationTokenSuccessRequest()
        registerToLisentRegisterNotificationTokenErrorRequest()
        reactToCloseCellEvent()
        paramsConfigureNotification()
    }

    func reactCards() {

        _ = reactTo(channel: PageReactionConstants.CHANNEL_CARDS)?
            .then()
            .inside(componentID: HomePageReaction.ROUTER_TAG, component: HomePresenter<HomeViewController>.self)
            .doReaction { component, param in

                if let param = param {
                    component.receive(model: param)
                }
            }
    }

    func cardUpdated() {

        let channel: Channel = Channel<Any>(name: CardsPageReaction.CARD_UPDATED_CHANNEL)
        _ = reactTo(channel: channel)?
            .then()
            .inside(componentID: HomeViewController.ID, component: HomeViewController.self)
            .doReactionAndClearChannel(reactionExecution: { component, param in
                if let card = param as? CardModel {
                    component.cardStatusUpdated(card: card)
                }
            })
    }

    func changeTabToCards() {

        _ = when(id: HomePageReaction.ROUTER_TAG, type: Model.self)
            .doAction(actionSpec: HomePageReaction.EVENT_CHANGE_TAB_TO_CARDS)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, model in

                if let model = model {
                    router.changeTabToCards(withModel: model)
                }

            }
    }

    func changeTabToPromotions() {

        _ = when(id: HomePageReaction.ROUTER_TAG, type: Void.self)
            .doAction(actionSpec: HomePageReaction.EVENT_CHANGE_TAB_TO_PROMOTIONS)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, _ in

                router.changeTabToPromotions()

            }
    }

    func changeTabToPoints() {

        _ = when(id: HomePageReaction.ROUTER_TAG, type: Void.self)
            .doAction(actionSpec: HomePageReaction.EVENT_CHANGE_TAB_TO_POINTS)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, _ in

                router.changeTabToPoints()

            }
    }

    func navigateToHelpDetail() {

        _ = when(id: HomePageReaction.ROUTER_TAG, type: Model.self)
            .doAction(actionSpec: HomePageReaction.EVENT_NAV_TO_DETAIL_HELP)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, model in

                if let model = model {
                    router.navigateToDetailHelp(withModel: model)
                }
            }
    }

    func navigateToHelp() {

        _ = when(id: HomePageReaction.ROUTER_TAG, type: Model.self)
            .doAction(actionSpec: HomePageReaction.EVENT_NAV_TO_HELP)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, model in

                if let model = model {
                    router.navigateToHelp(withModel: model)
                }

            }
    }

    func navigateToPromotionDetailScreen() {

        _ = when(id: HomePageReaction.ROUTER_TAG, type: Model.self)
            .doAction(actionSpec: HomePageReaction.EVENT_NAV_TO_PROMOTION_DETAIL_SCREEN)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, model in

                if let model = model {
                    router.navigateToShoppingDetail(withModel: model)
                }

            }
    }
    
    func navigateToPromotionsCategoryScreen() {
        
        _ = when(id: HomePageReaction.ROUTER_TAG, type: Model.self)
            .doAction(actionSpec: HomePageReaction.EVENT_NAV_TO_PROMOTIONS_CATEGORY_SCREEN)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, model in
                
                if let model = model {
                    router.navigateToShoppingPromotionsCategory(withModel: model)
                }
        }
    }
    
    func navigateToWebPageScreen() {
        
        _ = when(id: HomePageReaction.ROUTER_TAG, type: Model.self)
            .doAction(actionSpec: HomePageReaction.EVENT_NAV_TO_WEBPAGE_SCREEN)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, model in
                
                router.navigateToShoppingWebPage(withModel: model!)
        }
    }
    
    func registerToNavigateToActivateDevicePermissionScreen() {

        _ = when(id: HomePageReaction.ROUTER_TAG, type: Void.self)
            .doAction(actionSpec: HomePageReaction.EVENT_NAV_TO_ACTIVATE_DEVICE_PERMISSIONS_SCREEN)
            .then()
            .doWebNavigation(webRoute: ActivateDevicePermissionsPageReaction.CELLS_PAGE_ACTIVATE_DEVICE_PERMISSIONS, nativeRoute: Route(viewController: { () -> UIViewController in
                return (BusManager.sessionDataInstance.routerFactory?.getViewController(route: ActivateDevicePermissionsViewController.ID))!
            }, animationToNext: { _, destinationViewController in

                destinationViewController?.modalPresentationStyle = .overCurrentContext

                if let destinationViewController = destinationViewController {
                    let navigationController = UINavigationController(rootViewController: destinationViewController)
                    BusManager.sessionDataInstance.present(viewController: navigationController, animated: true, completion: nil)
                }

            }, animationToPrevious: { _, _ in

            }
            ))
    }
    
    func registerToNavigateToConfigureDevicePermissionScreen() {
        
        _ = when(id: HomePageReaction.ROUTER_TAG, type: Void.self)
            .doAction(actionSpec: HomePageReaction.EVENT_NAV_TO_CONFIGURE_DEVICE_PERMISSIONS_SCREEN)
            .then()
            .doWebNavigation(webRoute: ActivateDevicePermissionsPageReaction.CELLS_PAGE_CONFIGURE_DEVICE_PERMISSIONS, nativeRoute: Route(viewController: { () -> UIViewController in
                return (BusManager.sessionDataInstance.routerFactory?.getViewController(route: ActivateDevicePermissionsViewController.ID))!
            }, animationToNext: { _, destinationViewController in
                
                destinationViewController?.modalPresentationStyle = .overCurrentContext
                
                if let destinationViewController = destinationViewController {
                    let navigationController = UINavigationController(rootViewController: destinationViewController)
                    BusManager.sessionDataInstance.present(viewController: navigationController, animated: true, completion: nil)
                }
                
            }, animationToPrevious: { _, _ in
                
            }
            ))
    }

    func reactTokenChanged() {

        _ = reactTo(channel: PageReactionConstants.CHANNEL_TOKEN_CHANGED)?
            .then()
            .inside(componentID: HomePageReaction.ROUTER_TAG, component: HomePresenter<HomeViewController>.self)
            .doReactionAndClearChannel { component, param in

                if let param = param {
                    component.didSuccessRegisterForRemoteNotificationsWithDeviceToken(param)
                }
        }
    }

    func reactTokenFailed() {

        _ = reactTo(channel: PageReactionConstants.CHANNEL_TOKEN_FAILED)?
            .then()
            .inside(componentID: HomePageReaction.ROUTER_TAG, component: HomePresenter<HomeViewController>.self)
            .doReactionAndClearChannel { component, _ in
                component.didFailToRegisterForRemoteNotifications()
        }
    }
    
    func paramsConfigureNotification() {
        
        let channel: JsonChannel = JsonChannel(HomePageReaction.CONFIGURE_DEVICE_PERMISSIONS_CHANNEL)
        
        _ = writeOn(channel: channel)
            .when(componentID: HomePageReaction.ROUTER_TAG)
            .doAction(actionSpec: HomePageReaction.PARAMS_CONFIGURE_DEVICE_PERMISSIONS)
    }

    // MARK: - Store component Response

    func registerToLisentRegisterNotificationTokenSuccessRequest() {

        _ = when(id: RegisterNotificationsStore.ID, type: RegisterNotificationsResponseDTO.self)
            .doAction(actionSpec: RegisterNotificationsStore.ON_REGISTER_SUCCESS)
            .then()
            .inside(componentID: HomePageReaction.ROUTER_TAG, component: HomePresenter<HomeViewController>.self)
            .doReaction { homePresenter, registerNotificationsResponseDTO in

                if let registerNotificationsResponseDTO = registerNotificationsResponseDTO, let data = registerNotificationsResponseDTO.data, let token = data.token {
                    
                    homePresenter.registerTokenSuccess(withToken: token)
                }
        }
    }

    func registerToLisentRegisterNotificationTokenErrorRequest() {

        _ = when(id: RegisterNotificationsStore.ID, type: ErrorBO.self)
            .doAction(actionSpec: RegisterNotificationsStore.ON_REGISTER_ERROR)
            .then()
            .inside(componentID: HomePageReaction.ROUTER_TAG, component: HomePresenter<HomeViewController>.self)
            .doReaction { homePresenter, _ in

                homePresenter.registerTokenFailed()

        }
    }

    func reactToCloseCellEvent() {

        _ = when(id: HomePageReaction.ROUTER_TAG, type: Void.self)
            .doAction(actionSpec: HomePageReaction.EVENT_CLOSE_CELL_STACK)
            .then()
            .inside(componentID: WebController.ID, component: WebController.self)
            .doReaction { webController, _ in

                webController.sendOnBack()
        }
    }
}
