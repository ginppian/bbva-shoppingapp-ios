//
//  OnOffHelpContract.swift
//  shoppingapp
//
//  Created by Javier Pino on 5/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol OnOffHelpViewProtocol: ViewProtocol {

    func showHelpInformation(displayData: OnOffHelpDisplayData)
}

protocol OnOffHelpPresenterProtocol: PresenterProtocol {

    func viewLoaded()
    func acceptButtonPressed()
}
