//
//  OnOffHelpDisplayData.swift
//  shoppingapp
//
//  Created by Javier Pino on 5/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

struct OnOffHelpDisplayData: Equatable {

    let headerImageName: String
    let bulletImageName: String
    let titleOne: String
    let textsOne: [String]
    let titleTwo: String
    let textsTwo: [String]
    var acceptButtonTitle: String
    let disclaimerText: String
    var screenTitleText: String?
    var titleThree: String?
    var textsThree: [String]?

    init(cardBO: CardBO) {
        let rootKey = Settings.CardHelpOnOff.localizationKeyForHelpOnOffAndCard(card: cardBO)

        let titleOneKey = rootKey + "TITLE_1"
        let titleFirst = NSLocalizedString(titleOneKey, comment: "")
        let titleTwoKey = rootKey + "TITLE_2"
        let titleSecond = NSLocalizedString(titleTwoKey, comment: "")

        let disclaimerKey = rootKey + "DISCLAIMER"
        let disclaimer = NSLocalizedString(disclaimerKey, comment: "")
        
        let textsOneArray = Tools.localizedStringsForKeyAndStartIndex(key: rootKey + "TEXT_", startIndex: 1)
        let textstwoArray = Tools.localizedStringsForKeyAndStartIndex(key: rootKey + "TEXT_TWO_", startIndex: 1)
        let textsThreeArray = Tools.localizedStringsForKeyAndStartIndex(key: rootKey + "TEXT_THREE_", startIndex: 1)

        headerImageName = ConstantsImages.Card.ilu_turn_on_off
        bulletImageName = ConstantsImages.Common.bullet_icon
        titleOne = titleFirst
        textsOne = textsOneArray
        titleTwo = titleSecond
        textsTwo = textstwoArray
        textsThree = textsThreeArray
        acceptButtonTitle = Localizables.common.key_common_accept_text
        disclaimerText = disclaimer
    }
    
}
