//
//  OnOffHelpViewController.swift
//  shoppingapp
//
//  Created by Javier Pino on 5/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit

class OnOffHelpViewController: BaseViewController {

    static let ID: String = String(describing: OnOffHelpViewController.self)

    var presenter: OnOffHelpPresenterProtocol!

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!

    @IBOutlet weak var headerImageView: UIImageView!
    @IBOutlet weak var titleOneLabel: UILabel!
    @IBOutlet weak var stackOne: UIStackView!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var titleTwoLabel: UILabel!
    @IBOutlet weak var stackTwo: UIStackView!
    @IBOutlet weak var titleThree: UILabel!
    @IBOutlet weak var stackThree: UIStackView!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var scrollHeight: NSLayoutConstraint!
    
    // MARK: - View life cycle

    override func viewDidLoad() {
        
        configure(appLifecycle: (UIApplication.shared.delegate as! AppDelegate).appLifeCycle!, id: OnOffHelpViewController.ID)
        
        super.viewDidLoad()

        configureView()

        presenter?.viewLoaded()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        configureNavigationBar()

        BusManager.sessionDataInstance.disableSwipeLateralMenu()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        scrollView.flashScrollIndicators()
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        updateShadowView()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func initModel(withModel model: Model) {
        presenter.setModel(model: model)
    }

    // MARK: - Custom methods

    func configureNavigationBar() {

        setNavigationBarDefault()

        let image = SVGCache.image(forSVGNamed: ConstantsImages.Common.close_icon, color: .BBVAWHITE)
        setNavigationLeftButton(withImage: image, action: #selector(dismissAction))
    }

    func configureView() {

        titleOneLabel.font = Tools.setFontMedium(size: 14)
        titleOneLabel.textColor = .BBVA600
        titleTwoLabel.font = Tools.setFontMedium(size: 14)
        titleTwoLabel.textColor = .BBVA600
        separatorView.backgroundColor = .BBVA200

        acceptButton.backgroundColor = .MEDIUMBLUE
        acceptButton.titleLabel?.font = Tools.setFontBold(size: 14)
        acceptButton.setTitleColor(.BBVAWHITE, for: .normal)

    }

    func updateShadowView() {
        if contentView.bounds.height > scrollView.bounds.height {
            acceptButton.superview?.layer.shadowColor = UIColor.black.cgColor
            acceptButton.superview?.layer.shadowOpacity = 0.5
            acceptButton.superview?.layer.shadowOffset = CGSize.zero
            acceptButton.superview?.layer.shadowRadius = 2
        } else {
            acceptButton.superview?.layer.shadowOpacity = 0
        }
    }

    // MARK: - IBAction

    @IBAction func acceptButtonPressed(_ sender: Any) {
        presenter?.acceptButtonPressed()
    }
}

extension OnOffHelpViewController: OnOffHelpViewProtocol {

    func showError(error: ModelBO) {
    }

    func changeColorEditTexts() {
    }

    func showHelpInformation(displayData: OnOffHelpDisplayData) {
        
        let titleThreeText = displayData.titleThree ?? ""
        let textsThree = displayData.textsThree ?? [String]()
        let screenTitle = displayData.screenTitleText ?? Localizables.cards.key_card_turn_on_off
        
        titleOneLabel.text = displayData.titleOne
        titleTwoLabel.text = displayData.titleTwo
        titleThree.text = titleThreeText

        for text in displayData.textsOne {
            let view = BulletLabelInfoView(frame: CGRect(x: 0, y: 0, width: 200, height: 20) )
            view.translatesAutoresizingMaskIntoConstraints = false
            view.configureWithTitle(text, bulletImageName: displayData.bulletImageName)
            stackOne.addArrangedSubview (view)
        }

        for text in displayData.textsTwo {
            let view = BulletLabelInfoView(frame: CGRect(x: 0, y: 0, width: 200, height: 20) )
            view.translatesAutoresizingMaskIntoConstraints = false
            view.configureWithTitle(text, bulletImageName: displayData.bulletImageName)
            stackTwo.addArrangedSubview(view)
        }
        
        if !titleThreeText.isEmpty {
            
            for text in textsThree {
                let view = BulletLabelInfoView(frame: CGRect(x: 0, y: 0, width: 200, height: 20) )
                view.translatesAutoresizingMaskIntoConstraints = false
                view.configureWithTitle(text, bulletImageName: displayData.bulletImageName)
                stackThree.addArrangedSubview(view)
            }
            
            if !displayData.disclaimerText.isEmpty {
                let view = LightBlueInfoView(frame: CGRect(x: 0, y: 0, width: 200, height: 20) )
                view.translatesAutoresizingMaskIntoConstraints = false
                view.configure(text: displayData.disclaimerText)
                stackThree.addArrangedSubview(view)
            }
            
            let extendedViewConstraint = contentView.constraints.first { $0.identifier == "StackTwoBottom" }
            extendedViewConstraint?.constant = 215
            scrollHeight.constant = 205
            contentView.setNeedsUpdateConstraints()

        } else {
            
            if !displayData.disclaimerText.isEmpty {
                let view = LightBlueInfoView(frame: CGRect(x: 0, y: 0, width: 200, height: 20) )
                view.translatesAutoresizingMaskIntoConstraints = false
                view.configure(text: displayData.disclaimerText)
                stackTwo.addArrangedSubview(view)
            }
            
        }
        
        scrollView.layoutSubviews()
        acceptButton.setTitle(displayData.acceptButtonTitle, for: .normal)
        setNavigationTitle(withText: screenTitle)
        headerImageView.image = UIImage(named: displayData.headerImageName)

        updateShadowView()
    }
}
