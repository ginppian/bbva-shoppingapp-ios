//
//  OnOffHelpPageReaction.swift
//  shoppingapp
//
//  Created by Javier Pino on 6/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

class OnOffHelpPageReaction: PageReaction {
    
    let routerTagValue = "on-off-help-tag"
    
    override func configure() {
        
    }
    
    func getRouterTag() -> String {
        
        return routerTagValue
    }
}
