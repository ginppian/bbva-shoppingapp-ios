//
//  OnOffHelperA.swift
//  shoppingappMX
//
//  Created by Armando Vazquez on 11/16/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

final class OnOffHelpPageReactionA: OnOffHelpPageReaction {

    static let ROUTER_TAG = "on-off-help-tag"
    
    static let SWITCH_CARD_UP_ON_SUCCES: ActionSpec<Void> = ActionSpec<Void>(id: "SWITCH_CARD_UP_ON_SUCCES")
    
    static let INFO_SWITCH_CARD_UP_CHANNEL: Channel<Void> = Channel(name: "info_switch_card_up")
    
    override func configure() {
        super.configure()
        switchCardUpOnSuccess()
    }
    
    func switchCardUpOnSuccess() {
        
        _ = writeOn(channel: OnOffHelpPageReactionA.INFO_SWITCH_CARD_UP_CHANNEL)
            .when(componentID: OnOffHelpPageReactionA.ROUTER_TAG)
            .doAction(actionSpec: OnOffHelpPageReactionA.SWITCH_CARD_UP_ON_SUCCES)
        
    }
    
}
