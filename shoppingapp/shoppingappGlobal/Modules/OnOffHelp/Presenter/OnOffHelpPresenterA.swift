//
//  OnOffHelpPresenterA.swift
//  shoppingappMX
//
//  Created by Armando Vazquez on 11/14/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

final class OnOffHelpPresenterA <T: OnOffHelpViewProtocol>: OnOffHelpPresenter<T> {
    
    var isCalledFromSwitchCardUp: Bool?
    
    override func setModel(model: Model) {
        if let cardsTransport = model as? CardsTransport {
            cardBO = cardsTransport.cardBO
            isCalledFromSwitchCardUp = cardsTransport.flagNavigation
        }
    }
    
    override func viewLoaded() {
        if let card = cardBO, let calledFromSwitch = isCalledFromSwitchCardUp {
            
            displayData = OnOffHelpDisplayData(cardBO: card)
            
            guard var display = displayData else {
                return
            }
            
            let key = Settings.CardHelpOnOff.localizationKeyForHelpOnOffAndCard(card: card)
            display.acceptButtonTitle = calledFromSwitch ?  NSLocalizedString((key + "ACCEPT"), comment: "") : Localizables.common.key_common_accept_text

            let titleThreeKey = key + "TITLE_3"
            let titleThird = NSLocalizedString(titleThreeKey, comment: "")
            let screenTitleKey = key + "TITLE"
            let screenTitle = NSLocalizedString(screenTitleKey, comment: "")
            
            display.screenTitleText = screenTitle
            display.titleThree = titleThird
            displayData = display
            view?.showHelpInformation(displayData: display)
        }
        
    }
    
    override func acceptButtonPressed() {
        
        if let calledFromSwitch = isCalledFromSwitchCardUp, calledFromSwitch == true, let cardBO = cardBO {
                
           if cardBO.isCardOn() {
                PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: true as AnyObject, withKey: PreferencesManagerKeys.kSwitchCardOff)
            } else {
                PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: true as AnyObject, withKey: PreferencesManagerKeys.kSwitchCardOn)
            }
            
            busManager.publishData(tag: OnOffHelpPageReactionA.ROUTER_TAG, OnOffHelpPageReactionA.SWITCH_CARD_UP_ON_SUCCES, Void())
            
        }
        
        busManager.dismissViewController(animated: true)
    }
    
}
