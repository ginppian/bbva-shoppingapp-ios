//
//  OnOffHelpPresenter.swift
//  shoppingapp
//
//  Created by Javier Pino on 5/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class OnOffHelpPresenter <T: OnOffHelpViewProtocol>: BasePresenter<T>, OnOffHelpPresenterProtocol {

    let routerTagValue = "on-off-help-tag"

    var displayData: OnOffHelpDisplayData?
    var cardBO: CardBO?

    required init() {
        super.init(tag: routerTagValue)
    }

    override init(busManager: BusManager) {
        super.init(busManager: busManager)
        routerTag = routerTagValue
    }

    override func setModel(model: Model) {
        if let cardsTransport = model as? CardsTransport {
            cardBO = cardsTransport.cardBO
        }
    }
    func viewLoaded() {
        if let card = cardBO {
            displayData = OnOffHelpDisplayData(cardBO: card)
            view?.showHelpInformation(displayData: displayData!)
        }
    }
    
    func acceptButtonPressed() {
        
        busManager.dismissViewController(animated: true)
    }

}
