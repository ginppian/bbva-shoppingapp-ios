//
//  CardsContractA.swift
//  shoppingappMX
//
//  Created by jesus.martinez on 13/12/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

protocol CardsViewProtocolA: CardsViewProtocol {

    func showCardDataInfo()
    func hideCardDataInfo()

    func showPointsView(withDisplayData displayData: PointsDisplayData)
    func hidePointsView()
}

protocol CardsPresenterProtocolA: CardsPresenterProtocol {
    func switchCardStatus()
    func tabPressed(withDisplayData displayData: DisplayCardData?)
    func obtainDigitalCardButtonPressed(atIndex index: Int)
}
