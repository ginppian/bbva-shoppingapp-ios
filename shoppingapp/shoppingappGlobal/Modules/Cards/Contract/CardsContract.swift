//
//  BVACardsContract.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 15/3/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit
import RxSwift

protocol CardsViewProtocol: ViewProtocol {

    func showInfoCards (withDisplayItems displayItems: DisplayItemsCards)
    func showTransactions(withDisplayData displayData: TransactionDisplayData)
    func showToastSuccess(withSuccessMessage message: String, andBackgroundColor color: UIColor)
    func switchInitialStateAfterReceiveError (withSwitchStatus status: Bool)

    func showNavigation(withTitle title: String)
    func showBalancesInfo(withDisplayData displayData: DisplayBalancesCard)
    func showOnOffInfo(withDisplayData displayData: OnOffDisplayData)
    func showOperativeBarInfo(withDisplayData displayData: ButtonsBarDisplayData)
    func hideOperativesBar()
    func copyPan(withNumber number: String)
    func showHelp(withDisplayData displayData: HelpDisplayData)

    func doAnimationDetail()
    func doAnimationCards()

    func showCard(atIndex index: Int, hideCardInformationFromVisibleCell: Bool)

    func sendOmnitureScreenCardOffSuccess()
    
    func refreshPagerView(atIndex index: Int)
}

protocol CardsPresenterProtocol: PresenterProtocol {

    func viewWillAppear()

    func cardShowed(atIndex index: Int)
    func goToTransactionsForCard(atIndex index: Int)
    func goToSearchTransactionsForCard(atIndex index: Int)
    func changeCardStatus(withCardStatus status: Bool, andCard card: CardBO)
    func goToTransactionDetail(withTransactionDisplayItem transactionDisplayItem: TransactionDisplayItem, forCardAtIndex index: Int)
    func leftCardButtonPressed(atIndex index: Int)
    func rightCardButtonPressed(atIndex index: Int)

    func onOffViewHelpButtonPressed(forCard: CardBO?)
    func pressedOperativeButton(atIndex index: Int, withDisplayData displayData: ButtonBarDisplayData)

    func getHelp()
    func helpButtonPressed(withHelpId helpId: String)
    func updateCard(withCardStatus status: Bool, andCard card: CardBO)

}

protocol CardsInteractorProtocol {
    func provideUpdatedCard(withCardId cardId: String, withStatus status: Bool) -> Observable <ModelBO>
    func provideCards(user: UserBO?) -> Single<ModelBO>
}
