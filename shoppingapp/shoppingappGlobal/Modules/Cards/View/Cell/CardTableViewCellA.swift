//
//  CardTableViewCellA.swift
//  shoppingappMX
//
//  Created by jesus.martinez on 13/12/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit

protocol CardTableViewCellADelegate: CardTableViewCellDelegate {

    func tableViewCell(_ cardTableViewCell: CardTableViewCell, didSelectDisplayItem displayItem: DisplayCardData?)
    func obtainDigitalCardButtonPressedTableViewCell(_ cardTableViewCell: CardTableViewCell)

}

class CardTableViewCellA: CardTableViewCell {

    // Tabs
    @IBOutlet weak var physicalButton: UIButton!
    @IBOutlet weak var digitalButton: UIButton!
    @IBOutlet weak var stickerButton: UIButton!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var selectedLineView: UIView!
    @IBOutlet weak var selectedLineLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var tabView: UIView!

    // Card container
    @IBOutlet weak var cardInfoContainerView: UIView!

    // Digital info container
    @IBOutlet weak var digitalInfoContainerView: UIView!
    @IBOutlet weak var digitalinfoImageView: UIImageView!
    @IBOutlet weak var descriptionDigitalInfoLabel: UILabel!
    @IBOutlet weak var obtainDigitalCardButton: UIButton!

    override class var cardImageVerticalSpacing: CGFloat {
        return 80
    }

    override class var nibName: String {
        return "CardTableViewCellA"
    }

    override func awakeFromNib() {
        super.awakeFromNib()

        configureTabs()

        configureDigitalContainer()

    }

    private func configureTabs() {

        physicalButton.setTitle(Localizables.cards.key_cards_physics_type_text, for: .normal)
        physicalButton.titleLabel?.font = Tools.setFontMedium(size: 14)
        configureForSelectedButton(physicalButton, animated: false)

        digitalButton.setTitle(Localizables.cards.key_cards_digital_type_text, for: .normal)
        digitalButton.titleLabel?.font = Tools.setFontMedium(size: 14)
        configureForNormalStateButton(digitalButton)

        stickerButton.setTitle(Localizables.cards.key_cards_sticker_type_text, for: .normal)
        stickerButton.titleLabel?.font = Tools.setFontMedium(size: 14)
        configureForNormalStateButton(stickerButton)

        separatorView.backgroundColor = .BBVA300
        selectedLineView.backgroundColor = .COREBLUE

    }

    private func configureDigitalContainer() {

        digitalInfoContainerView.backgroundColor = .BBVAWHITE

        if let card = displayData?.cardBO, card.isSticker() {
            digitalinfoImageView.image = UIImage(named: ConstantsImages.Card.blank_sticker)
        } else {
            digitalinfoImageView.image = UIImage(named: ConstantsImages.Card.blank_card)
        }

        descriptionDigitalInfoLabel.font = Tools.setFontBook(size: 14)
        descriptionDigitalInfoLabel.textColor = .BBVA500

        obtainDigitalCardButton.setTitleColor(.MEDIUMBLUE, for: .normal)
        obtainDigitalCardButton.setTitleColor(.COREBLUE, for: .highlighted)
        obtainDigitalCardButton.setTitleColor(.COREBLUE, for: .selected)
        obtainDigitalCardButton.titleLabel?.font = Tools.setFontMedium(size: 14)
        obtainDigitalCardButton.tintColor = .MEDIUMBLUE
        obtainDigitalCardButton.titleLabel?.textAlignment = .center

    }

    private func configureForNormalStateButton(_ button: UIButton) {

        button.setTitleColor(.BBVA500, for: .normal)
        button.setTitleColor(.BBVA600, for: .highlighted)
        button.setTitleColor(.BBVA600, for: .selected)
        button.isUserInteractionEnabled = true

    }

    private func configureForSelectedButton(_ button: UIButton, animated: Bool) {

        button.setTitleColor(.COREBLUE, for: .normal)
        button.setTitleColor(.COREBLUE, for: .highlighted)
        button.setTitleColor(.COREBLUE, for: .selected)
        button.isUserInteractionEnabled = false

        if animated {
            UIView.animate(withDuration: 0.2,
                           delay: 0.0,
                           options: .curveLinear,
                           animations: {
                            self.selectedLineLeadingConstraint.constant = button.frame.origin.x
                            self.layoutIfNeeded()
            }, completion: nil)
        } else {
            selectedLineLeadingConstraint.constant = button.frame.origin.x
        }

    }

    func showCardInfoContainer(withCardDisplayData cardDisplayData: DisplayCardData?) {

        digitalInfoContainerView.isHidden = true
        cardInfoContainerView.isHidden = false

        if let displayData = cardDisplayData {

            configureImage(withDisplayCardData: displayData)

            configureStatus(withImageStatus: displayData.imageStatus)

            configureUserInfo(withDisplayCardData: displayData)

            configureActionButtons(withDisplayCardData: displayData)

            setAccessibilities()
            setAccessibilitiesMultiTab()

        }

    }

    private func showDigitalInfoContainer() {

        cardInfoContainerView.isHidden = true
        digitalInfoContainerView.isHidden = false

        if let virtualInfo = (displayData as! DisplayCardDataA).virtualInfoDisplayData {

            descriptionDigitalInfoLabel.text = virtualInfo.description

            if let link = virtualInfo.link {

                obtainDigitalCardButton.setTitle(link, for: .normal)
                obtainDigitalCardButton.isHidden = false

            } else {
                obtainDigitalCardButton.isHidden = true
            }

            digitalinfoImageView.alpha = virtualInfo.alpha
        }

    }

    private func configureCardFor(selectedCard: SelectDisplayCardDataAType?, animated: Bool) {

        guard let selectedCard = selectedCard else {
            return
        }

        switch selectedCard {
        case .physical:
            configureForSelectedButton(physicalButton, animated: animated)
            configureForNormalStateButton(digitalButton)
            configureForNormalStateButton(stickerButton)

            showCardInfoContainer(withCardDisplayData: displayData)
        case .digital:
            configureForSelectedButton(digitalButton, animated: animated)
            configureForNormalStateButton(physicalButton)
            configureForNormalStateButton(stickerButton)

            (displayData as! DisplayCardDataA).virtualCardDisplayData == nil ? showDigitalInfoContainer() : showCardInfoContainer(withCardDisplayData: (displayData as! DisplayCardDataA).virtualCardDisplayData)
        case .sticker:
            configureForSelectedButton(stickerButton, animated: animated)
            configureForNormalStateButton(physicalButton)
            configureForNormalStateButton(digitalButton)

            showCardInfoContainer(withCardDisplayData: (displayData as! DisplayCardDataA).stickerCardDisplayData)
        }

    }

    override func configureCell (withDisplayCardData displayCardData: DisplayCardData) {

        cardImageView.isHidden = false
        panLabel.isHidden = false
        cardHolderLabel.isHidden = false

        if let displayCardDataA = displayCardData as? DisplayCardDataA {

            displayData = displayCardDataA

            stickerButton.isHidden = displayCardDataA.stickerCardDisplayData == nil ? true : false

            setNeedsLayout()
            layoutIfNeeded()

            configureCardFor(selectedCard: displayCardDataA.selectedCard, animated: false)

        }

    }

    @IBAction func physicalButtonAction(_ sender: Any) {

        (displayData as! DisplayCardDataA).selectedCard = .physical

        configureCardFor(selectedCard: (displayData as! DisplayCardDataA).selectedCard, animated: true)

        (delegate as? CardTableViewCellADelegate)?.tableViewCell(self, didSelectDisplayItem: (displayData as! DisplayCardDataA))

    }

    @IBAction func digitalButtonAction(_ sender: Any) {

        (displayData as! DisplayCardDataA).selectedCard = .digital

        configureCardFor(selectedCard: (displayData as! DisplayCardDataA).selectedCard, animated: true)

        (delegate as? CardTableViewCellADelegate)?.tableViewCell(self, didSelectDisplayItem: (displayData as! DisplayCardDataA).virtualCardDisplayData)

    }

    @IBAction func stickerButtonAction(_ sender: Any) {

        (displayData as! DisplayCardDataA).selectedCard = .sticker

        configureCardFor(selectedCard: (displayData as! DisplayCardDataA).selectedCard, animated: true)

        (delegate as? CardTableViewCellADelegate)?.tableViewCell(self, didSelectDisplayItem: (displayData as! DisplayCardDataA).stickerCardDisplayData)

    }

    @IBAction func obtainDigitalCardButtonAction(_ sender: Any) {
        
        (delegate as? CardTableViewCellADelegate)?.obtainDigitalCardButtonPressedTableViewCell(self)
    }

}
// MARK: - APPIUM

extension CardTableViewCellA {

     func setAccessibilitiesMultiTab() {

        #if APPIUM
            Tools.setAccessibility(view: tabView, identifier: ConstantsAccessibilities.CARDS_CARD_MULTI_TAB)
            tabView.isAccessibilityElement = false

            Tools.setAccessibility(view: physicalButton, identifier: ConstantsAccessibilities.CARDS_CARD_MULTI_TAB_BUTTON)
            Tools.setAccessibility(view: digitalButton, identifier: ConstantsAccessibilities.CARDS_CARD_MULTI_TAB_BUTTON)
            Tools.setAccessibility(view: stickerButton, identifier: ConstantsAccessibilities.CARDS_CARD_MULTI_TAB_BUTTON)

            Tools.setAccessibility(view: obtainDigitalCardButton, identifier: ConstantsAccessibilities.CARDS_CARD_MULTI_TAB_DIGITAL_LINK)

            Tools.setAccessibility(view: descriptionDigitalInfoLabel, identifier: ConstantsAccessibilities.CARDS_CARD_MULTI_TAB_DIGITAL_DESCRIPTION)

            Tools.setAccessibility(view: digitalinfoImageView, identifier: ConstantsAccessibilities.CARDS_CARD_MULTI_TAB_DIGITAL_SMALL_COVER)

            if let displayCardDataA = displayData as? DisplayCardDataA {
                if displayCardDataA.selectedCard == .sticker {
                    Tools.setAccessibility(view: expirationDateLabel, identifier: "")
                    Tools.setAccessibility(view: cardHolderLabel, identifier: "")
                    Tools.setAccessibility(view: panLabel, identifier: "")

                }
            }

        #endif

    }

}
