//
//  CardTableViewCell.swift
//  FSPagerViewExample
//
//  Created by ruben.fernandez on 12/9/17.
//  Copyright © 2017 Wenchao Ding. All rights reserved.
//

import UIKit

protocol CardTableViewCellDelegate: class {
    func leftCardButtonPressedTableViewCell(_ cardTableViewCell: CardTableViewCell)
    func rightCardButtonPressedTableViewCell(_ cardTableViewCell: CardTableViewCell)
    func imageCardDownloadedTableViewCell(_ cardTableViewCell: CardTableViewCell, atIndex index: Int)
}

class CardTableViewCell: FSPagerViewCell {

    class var cardImageVerticalSpacing: CGFloat {
        return 50
    }

    class var nibName: String {
        return "CardTableViewCell"
    }

    static let identifier = "cardcell"

    static let spacingBetweenTitleAndImageButton: CGFloat = 10.0
    static let shadowColor = UIColor.BBVA600.withAlphaComponent(0.8)

    weak var delegate: CardTableViewCellDelegate?

    @IBOutlet weak var containerView: UIView!

    @IBOutlet weak var cardImageView: UIImageView!
    @IBOutlet weak var statusImageView: UIImageView!

    @IBOutlet weak var panLabel: ShadowedLabel!
    @IBOutlet weak var expirationDateLabel: ShadowedLabel!
    @IBOutlet weak var cardHolderLabel: ShadowedLabel!

    @IBOutlet weak var leftButton: UIButton!
    @IBOutlet weak var rightButton: UIButton!

    var displayData: DisplayCardData?

    var index: Int?

    override func awakeFromNib() {
        super.awakeFromNib()

        contentView.backgroundColor = .clear
        backgroundColor = .clear
        containerView.backgroundColor = .BBVAWHITE

        panLabel.font = Tools.setFontBook(size: 15)
        panLabel.textColor = .BBVAWHITE
        panLabel.colorShadow = CardTableViewCell.shadowColor.cgColor
        expirationDateLabel.font = Tools.setFontBook(size: 14)
        expirationDateLabel.textColor = .BBVAWHITE
        expirationDateLabel.colorShadow = CardTableViewCell.shadowColor.cgColor
        cardHolderLabel.font = Tools.setFontBook(size: 12)
        cardHolderLabel.textColor = .BBVAWHITE
        cardHolderLabel.colorShadow = CardTableViewCell.shadowColor.cgColor

        configureAction(button: leftButton)
        configureAction(button: rightButton)

    }

    private func configureAction(button: UIButton) {
        button.setTitleColor(.MEDIUMBLUE, for: .normal)
        button.setTitleColor(.COREBLUE, for: .highlighted)
        button.setTitleColor(.COREBLUE, for: .selected)
        button.titleLabel?.font = Tools.setFontMedium(size: 14)
        button.contentMode = .scaleAspectFit
        button.clipsToBounds = true
        button.imageView?.contentMode = .scaleAspectFit
    }

    func configureCell (withDisplayCardData displayCardData: DisplayCardData) {

        cardImageView.isHidden = false
        panLabel.isHidden = false
        cardHolderLabel.isHidden = false

        displayData = displayCardData

        configureImage(withDisplayCardData: displayCardData)

        configureStatus(withImageStatus: displayCardData.imageStatus)

        configureUserInfo(withDisplayCardData: displayCardData)

        configureActionButtons(withDisplayCardData: displayCardData)

        setAccessibilities()

    }

    func configureImage(withDisplayCardData displayCardData: DisplayCardData) {

        let urlImage = displayCardData.cardBO?.imageFront ?? ""

        cardImageView.getCardImage(withUrl: urlImage) { _, isNewImage in

            guard let index = self.index else {
                return
            }

            if isNewImage {
                self.delegate?.imageCardDownloadedTableViewCell(self, atIndex: index)
            }
        }
    }

    func configureStatus(withImageStatus imageStatus: String?) {

        if !Tools.isStringNilOrEmpty(withString: imageStatus) {

            statusImageView.isHidden = false
            statusImageView.image = UIImage(named: imageStatus!)
            cardImageView.alpha = 0.5

        } else {

            statusImageView.isHidden = true
            statusImageView.image = nil
            cardImageView.alpha = 1.0

        }

    }

    func configureExpirationDateLabel(withExpirationDate expirationDate: String?) {
        if !Tools.isStringNilOrEmpty(withString: expirationDate) {

            expirationDateLabel.isHidden = false
            expirationDateLabel.addShadowAttributeToString(substring: expirationDate!, font: Tools.setFontBook(size: 14), shadowColor: CardTableViewCell.shadowColor)

        } else {

            expirationDateLabel.isHidden = true
            expirationDateLabel.text = nil

        }
    }

    func configureUserInfo(withDisplayCardData displayCardData: DisplayCardData) {

        panLabel.addShadowAttributeToString(substring: displayCardData.pan, font: Tools.setFontBook(size: 15), shadowColor: CardTableViewCell.shadowColor)
        cardHolderLabel.addShadowAttributeToString(substring: displayCardData.holderName, font: Tools.setFontBook(size: 12), shadowColor: CardTableViewCell.shadowColor)

        configureExpirationDateLabel(withExpirationDate: displayCardData.expirationDate)

    }

    func configureActionButtons(withDisplayCardData displayCardData: DisplayCardData) {

        leftButton.contentHorizontalAlignment = .left
        rightButton.contentHorizontalAlignment = .right
        initializeAction(button: leftButton)
        initializeAction(button: rightButton)

        if let rightButtonDisplayData = displayCardData.rightButtonDisplayData {

            rightButton.setTitle(rightButtonDisplayData.title, for: .normal)

            if let imageName = rightButtonDisplayData.imageNamed {
                setImage(forButton: rightButton, withImageName: imageName)
            }

            if let leftButtonDisplayData = displayCardData.leftButtonDisplayData {

                leftButton.setTitle(leftButtonDisplayData.title, for: .normal)

                if let imageName = leftButtonDisplayData.imageNamed {
                    setImage(forButton: leftButton, withImageName: imageName)
                }

                leftButton.isHidden = false

            } else {

                rightButton.contentHorizontalAlignment = .center
                leftButton.isHidden = true

            }

        } else if let  leftButtonDisplayData = displayCardData.leftButtonDisplayData {

            leftButton.setTitle(leftButtonDisplayData.title, for: .normal)

            if let imageName = leftButtonDisplayData.imageNamed {
                setImage(forButton: leftButton, withImageName: imageName)
            }

            leftButton.contentHorizontalAlignment = .center

            leftButton.isHidden = false
            rightButton.isHidden = true

        } else {

            leftButton.isHidden = true
            rightButton.isHidden = true

        }

    }

    func initializeAction(button: UIButton) {

        button.setImage(nil, for: .normal)
        button.setImage(nil, for: .highlighted)
        button.setImage(nil, for: .selected)

        button.isHidden = false
        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)

    }

    func setImage(forButton button: UIButton, withImageName imageName: String) {

        button.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: imageName, color: .MEDIUMBLUE), width: 16, height: 16), for: .normal)
        button.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: imageName, color: .COREBLUE), width: 16, height: 16), for: .highlighted)
        button.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: imageName, color: .COREBLUE), width: 16, height: 16), for: .selected)

        button.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: CardTableViewCell.spacingBetweenTitleAndImageButton)
        button.titleEdgeInsets = UIEdgeInsets(top: 0, left: CardTableViewCell.spacingBetweenTitleAndImageButton, bottom: 0, right: 0)

    }

    @IBAction func leftButtonAction(_ sender: Any) {

        delegate?.leftCardButtonPressedTableViewCell(self)

    }
    @IBAction func rightButtonAction(_ sender: Any) {

        delegate?.rightCardButtonPressedTableViewCell(self)

    }

    func cardInformationVisible() {

        if let displayData = displayData {
            configureCell(withDisplayCardData: displayData)
        }

    }

    func cardInformationHidden() {
        cardImageView.isHidden = true
        statusImageView.isHidden = true

        panLabel.isHidden = true
        expirationDateLabel.isHidden = true
        cardHolderLabel.isHidden = true
    }

}

// MARK: - APPIUM

extension CardTableViewCell {

    func setAccessibilities() {

        #if APPIUM

            accessibilityIdentifier = ConstantsAccessibilities.CARD_CELL

            Tools.setAccessibility(view: cardImageView, identifier: ConstantsAccessibilities.CARDS_CARD_IMAGE)

            Tools.setAccessibility(view: panLabel, identifier: ConstantsAccessibilities.CARD_PAN_OUTPUT_TEXT)
            Tools.setAccessibility(view: expirationDateLabel, identifier: ConstantsAccessibilities.CARD_EXPIRATION_DATE_OUTPUT_TEXT)
            Tools.setAccessibility(view: cardHolderLabel, identifier: ConstantsAccessibilities.CARD_HOLDER_NAME_OUTPUT_TEXT)

            if statusImageView.image != nil {
                Tools.setAccessibility(view: statusImageView, identifier: ConstantsAccessibilities.CARDS_STATUS_IMAGE)
            } else {
                Tools.setAccessibility(view: statusImageView, identifier: "")
            }

            if let accesibility = displayData?.leftButtonDisplayData?.accesibility {
                Tools.setAccessibility(view: leftButton, identifier: accesibility)
            }

            if let accesibility = displayData?.rightButtonDisplayData?.accesibility {
                Tools.setAccessibility(view: rightButton, identifier: accesibility)
            }

        #endif

    }

}
