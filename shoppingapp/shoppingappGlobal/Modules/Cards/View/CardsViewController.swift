//
//  CardsViewController.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 8/3/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit
import CellsNativeCore
import CellsNativeComponents

class CardsViewController: BaseViewController {

    // MARK: Variables
    static let ID: String = String(describing: CardsViewController.self)

    static let minimumScaleCards: CGFloat = 0.725
    static let paddingCards: CGFloat = 15.0
    static let heightToastView: CGFloat = 100.0
    static let timeOutToastView: CGFloat = Constants.default_toast_time_in_seconds

    static let durationForAnimationCards = 0.7
    static let durationForAnimationDetail = 0.8

    var presenter: CardsPresenterProtocol!

    var displayItems: DisplayItemsCards?
    var itemDisplayedIndex: Int = 0

     // MARK: outlets
    @IBOutlet weak var cardDataInfoContainerStackView: UIStackView!

    @IBOutlet weak var optionsBarView: ButtonsBarView!
    @IBOutlet weak var balancesView: BalancesView!
    @IBOutlet weak var cardOnOffView: OnOffView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var helpView: HelpView!

    @IBOutlet weak var transactionView: TransactionView! {
        didSet {
            transactionView.delegate = self
            transactionView.presenter.delegate = presenter as? TransactionPresenterDelegate
        }
    }

    @IBOutlet weak var transactionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var creditCardViewMoreHeight: NSLayoutConstraint!
    @IBOutlet weak var pagerView: FSPagerView!

    var durationToTransition = CardsViewController.durationForAnimationCards
    var indexOfCardTransition: Int?
    var cardImageTransitionFrame: CGRect?

    // MARK: life cycle

    override func viewDidLoad() {

        configure(appLifecycle: (UIApplication.shared.delegate as! AppDelegate).appLifeCycle!, id: CardsViewController.ID)

        super.viewDidLoad()

        edgesForExtendedLayout = UIRectEdge()

        configureView()

        balancesView.delegate = self

        cardOnOffView.delegate = self

        optionsBarView.delegate = self

        presenter.getHelp()

        if #available(iOS 11.0, *) {
            scrollView.contentInsetAdjustmentBehavior = .never
        }
    }

    override func viewWillAppear(_ animated: Bool) {

        super.viewWillAppear(animated)

        configurePagerView()

        pagerView.layoutIfNeeded()

        presenter.viewWillAppear()

        setNavigationBarDefault ()
        addNavigationBarMenuButton()

        self.tabBarController?.tabBar.isHidden = false

        BusManager.sessionDataInstance.enableSwipeLateralMenu()
        
        if let indexPath = visibleIndexPath() {
            trackCardOmniture(forIndex: indexPath.row)
        }
    }
    
    override func viewWillAppearAfterDismiss() {
        
        if let indexPath = visibleIndexPath() {
            trackCardOmniture(forIndex: indexPath.row)
        }
    }

    func setTitleNavigationBar(withTitle title: String) {

        setNavigationTitle(withText: title)

        tabBarItem.title = Localizables.common.key_tabbar_cards_button

    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    deinit {
        if let presenter = presenter {
            ComponentManager.remove(id: presenter.routerTag)
        }
        DLog(message: "CardsViewController Deinitialized")
    }

    // MARK: custom methods

    func configureView () {
        view.backgroundColor = .BBVA200
        optionsBarView.backgroundColor = .MEDIUMBLUE
    }

    func configurePagerView() {

        pagerView.register(UINib(nibName: CardTableViewCell.nibName, bundle: nil), forCellWithReuseIdentifier: CardTableViewCell.identifier)

        pagerView.isInfinite = false
        pagerView.transformer = FSPagerViewTransformer(type: .linear)
        pagerView.transformer?.minimumScale = CardsViewController.minimumScaleCards
        pagerView.itemSize = CGSize(width: view.frame.size.width - (CardsViewController.paddingCards * 2), height: pagerView.frame.size.height)
        pagerView.delegate = self
        pagerView.dataSource = self

    }

    func doAnimationDetail() {

        cardDataInfoContainerStackView.layer.frame = CGRect(x: cardDataInfoContainerStackView.frame.origin.x, y: -310, width: cardDataInfoContainerStackView.frame.size.width, height: cardDataInfoContainerStackView.frame.size.height)

        cardDataInfoContainerStackView.isHidden = false

        UIView.animate(withDuration: CardsViewController.durationForAnimationDetail) {

            self.cardDataInfoContainerStackView.layer.frame = CGRect(x: (self.cardDataInfoContainerStackView.frame.origin.x), y: (self.pagerView.frame.size.height), width: (self.cardDataInfoContainerStackView.frame.size.width), height: (self.cardDataInfoContainerStackView.frame.size.height))
        }
    }

    func doAnimationCards() {

        pagerView.layer.frame = CGRect(x: 30, y: pagerView.frame.origin.y, width: pagerView.frame.size.width, height: pagerView.frame.size.height)
        pagerView.alpha = 0.0

        UIView.animate(withDuration: CardsViewController.durationForAnimationCards) { [weak self] in
            self?.pagerView.layer.frame = CGRect(x: 0, y: (self?.pagerView.frame.origin.y)!, width: (self?.pagerView.frame.size.width)!, height: (self?.pagerView.frame.size.height)!)
            self?.pagerView.alpha = 1.0

        }
    }

    override func initModel(withModel model: Model) {
        presenter.setModel(model: model)
    }

    func loadCardCell() -> CardTableViewCell? {
        return Bundle.main.loadNibNamed(CardTableViewCell.nibName, owner: self, options: nil)?.first as? CardTableViewCell
    }

    func cardImageVerticalSpacing() -> CGFloat {
        return CardTableViewCell.cardImageVerticalSpacing
    }

    func showCard(atIndex index: Int, hideCardInformationFromVisibleCell: Bool = true) {

        scrollView.setContentOffset(.zero, animated: false)

        if !hideCardInformationFromVisibleCell {
            indexOfCardTransition = index
        }
        
        itemDisplayedIndex = index

        let visibleIndexPath = IndexPath(row: itemDisplayedIndex, section: 0)

        if let visibleCell = pagerView.collectionView.cellForItem(at: visibleIndexPath) as? CardTableViewCell, hideCardInformationFromVisibleCell {

            if hideCardInformationFromVisibleCell {
                visibleCell.cardInformationHidden()
            }

        }

        self.pagerView.scrollToItem(at: index, animated: true)

    }

    func changeCardStatusFromCells(withCardStatus status: Bool, andCard card: CardBO) {
        presenter.updateCard(withCardStatus: status, andCard: card)

    }

    func rightCardButtonPressedTableViewCell(_ cardTableViewCell: CardTableViewCell) {

        presenter.rightCardButtonPressed(atIndex: itemDisplayedIndex)

    }
    
    func onOffViewHelpButtonPressed(_ onOffView: OnOffView) {
        let card = displayItems?.items?[safeElement: itemDisplayedIndex]?.displayCardData?.cardBO
        presenter.onOffViewHelpButtonPressed(forCard: card)
    }
    
    func trackCardOmniture(forIndex index: Int) {
        if let titleID = displayItems?.items?[index].displayCardData?.cardBO?.title?.id {
            TrackerHelper.sharedInstance().trackCardsScreen(forTitleId: titleID, andCustomerID: SessionDataManager.sessionDataInstance().customerID)
        }
    }

}

extension CardsViewController: FSPagerViewDataSource, FSPagerViewDelegate {

    public func numberOfItems(in pagerView: FSPagerView) -> Int {

        return displayItems?.items!.count ?? 0
    }

    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {

        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: CardTableViewCell.identifier, at: index) as! CardTableViewCell

        let displayItem = displayItems?.items?[index]

        cell.delegate = self

        cell.index = index

        cell.configureCell(withDisplayCardData: (displayItem?.displayCardData)!)

        if let indexOfCardTransition = indexOfCardTransition, indexOfCardTransition == index {
            cell.cardInformationHidden()
        }

        return cell

    }

    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)
    }

    func pagerViewDidEndDecelerating(_ pagerView: FSPagerView) {

        guard let visibleIndex = visibleIndexPath(), itemDisplayedIndex != visibleIndex.row else {
            return
        }
        
        itemDisplayedIndex = visibleIndex.row
        trackCardOmniture(forIndex: itemDisplayedIndex)
        presenter.cardShowed(atIndex: itemDisplayedIndex)

    }

    func pagerViewDidEndScrollAnimation(_ pagerView: FSPagerView) {

        if let visibleRow = visibleIndexPath()?.row {
            itemDisplayedIndex = visibleRow
            presenter.cardShowed(atIndex: itemDisplayedIndex)
        }

    }
    
    fileprivate func visibleIndexPath() -> IndexPath? {
        
        var visibleRect = CGRect()
        visibleRect.origin = pagerView.collectionView.contentOffset
        visibleRect.size = pagerView.collectionView.bounds.size
        let visiblePoint = CGPoint(x: CGFloat(visibleRect.midX), y: CGFloat(visibleRect.midY))
        
        return pagerView.collectionView.indexPathForItem(at: visiblePoint)
    }

}

extension CardsViewController: CardsViewProtocol {

    func showHelp(withDisplayData displayData: HelpDisplayData) {
        helpView.delegate = self

        helpView.isHidden = false

        helpView.setHelpDisplayData(forHelpDisplayData: displayData)
    }

    func switchInitialStateAfterReceiveError(withSwitchStatus status: Bool) {
        cardOnOffView.customSwitchButton.setOn(status)
    }

    func showTransactions(withDisplayData displayData: TransactionDisplayData) {
        transactionView.loadTransactions(forDisplayData: displayData)
    }

    func showError(error: ModelBO) {
        presenter.showModal()
    }

    func changeColorEditTexts() {

    }

    func showInfoCards(withDisplayItems displayItems: DisplayItemsCards) {

        setAccessibilities()

        self.displayItems = displayItems
        presenter.cardShowed(atIndex: itemDisplayedIndex)
        pagerView.reloadData()

    }

    func showToastSuccess(withSuccessMessage message: String, andBackgroundColor color: UIColor) {

        ToastManager.shared().showToast(withText: message, backgroundColor: color, height: CardsViewController.heightToastView)
    }

    func showNavigation(withTitle title: String) {
        setTitleNavigationBar(withTitle: title)
    }

    func showBalancesInfo(withDisplayData displayData: DisplayBalancesCard) {
        balancesView.receiveBalanceInfoInView(withDisplayBalanceCard: displayData)
    }

    func showOnOffInfo(withDisplayData displayData: OnOffDisplayData) {
        cardOnOffView.showSwitchValue(onOffDisplayData: displayData)
    }

    func showOperativeBarInfo(withDisplayData displayData: ButtonsBarDisplayData) {
        optionsBarView.show(displayButtonsBarDisplayData: displayData)
        optionsBarView.isHidden = false
    }

    func hideOperativesBar() {
        optionsBarView.isHidden = true
    }

    func copyPan(withNumber number: String) {

        let pasteboard = UIPasteboard.general
        pasteboard.string = number
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: Constants.TIMEOUT_COPY_PAN as AnyObject, withKey: PreferencesManagerKeys.kClearCopyPan)

    }

    func sendOmnitureScreenCardOffSuccess() {
        TrackerHelper.sharedInstance().trackCardOffSuccess(withCustomerID: SessionDataManager.sessionDataInstance().customerID)
    }

    func refreshPagerView(atIndex index: Int) {
        
        itemDisplayedIndex = index
        trackCardOmniture(forIndex: itemDisplayedIndex)
        presenter.cardShowed(atIndex: itemDisplayedIndex)
    }
}

extension CardsViewController: TransactionViewDelegate {

    func showAllTransactionsPressed(forTransactionView transactionsView: TransactionView) {
        presenter.goToTransactionsForCard(atIndex: itemDisplayedIndex)
    }

    func transactionViewWillLoadInfoWith(height: CGFloat) {
        transactionViewHeight.constant = height
    }

    func noContentPressed(forTransactionView transactions: TransactionView) {
        presenter.goToSearchTransactionsForCard(atIndex: itemDisplayedIndex)
    }

    func showTransactionDetail(_ transactionView: TransactionView, andTransactionDisplayItem transactionDisplayItem: TransactionDisplayItem) {

        presenter.goToTransactionDetail(withTransactionDisplayItem: transactionDisplayItem, forCardAtIndex: itemDisplayedIndex)

    }
}

extension CardsViewController: CloseModalDelegate {

    func errorViewControllerAcceptPressed(_ errorViewController: ErrorViewController) {
        presenter.checkError()
    }

    func errorViewControllerCancelPressed(_ errorViewController: ErrorViewController) {
    }

}

extension CardsViewController: BalancesViewDelegate {

    func displayMoreInformation(height: CGFloat) {
        creditCardViewMoreHeight.constant = height
    }
}

extension CardsViewController: CardTableViewCellDelegate {

    func leftCardButtonPressedTableViewCell(_ cardTableViewCell: CardTableViewCell) {

        presenter.leftCardButtonPressed(atIndex: itemDisplayedIndex)

    }

    func imageCardDownloadedTableViewCell(_ cardTableViewCell: CardTableViewCell, atIndex index: Int) {

        UIView.performWithoutAnimation({
            pagerView.collectionView.reloadItems(at: [IndexPath(row: index, section: 0)])
        })
    }

}

extension CardsViewController: OnOffViewDelegate {

    func configureSwitchStatus(withStatus status: Bool, withCard card: CardBO) {
        presenter.changeCardStatus(withCardStatus: status, andCard: card)
    }

}

extension CardsViewController: ButtonsBarViewDelegate {

    func buttonBarPressed(_ buttonsBarView: ButtonsBarView, withDisplayItem displayItem: ButtonBarDisplayData) {
        presenter.pressedOperativeButton(atIndex: itemDisplayedIndex, withDisplayData: displayItem)
    }
}

extension CardsViewController: HelpViewDelegate {

    func helpButtonPressed(_ helpView: HelpView, andHelpId helpId: String) {
        presenter.helpButtonPressed(withHelpId: helpId)
    }
}

extension CardsViewController: TransitionerInAnimationProtocol {

    func transitionFrame() -> CGRect? {

        guard cardImageTransitionFrame == nil else {
            return cardImageTransitionFrame
        }

        guard let cellForFrame = loadCardCell() else {
            return nil
        }

        let y = cardImageVerticalSpacing() + topbarHeight
        let x = (UIScreen.main.bounds.size.width - cellForFrame.cardImageView.frame.size.width) / 2

        cardImageTransitionFrame = CGRect(x: x, y: y, width: cellForFrame.cardImageView.frame.size.width, height: cellForFrame.cardImageView.frame.size.height)

        return cardImageTransitionFrame

    }

    func finishTransition() {

        indexOfCardTransition = nil

        let visibleIndexPath = IndexPath(row: itemDisplayedIndex, section: 0)

        if let visibleCell = pagerView.collectionView.cellForItem(at: visibleIndexPath) as? CardTableViewCell {

            visibleCell.cardInformationVisible()

        }

    }

    func doAnimationWhileTransition(withDuration duration: Double) {

        pagerView.collectionView.setContentOffset(CGPoint(x: pagerView.collectionView.contentOffset.x - CardsViewController.paddingCards, y: pagerView.collectionView.contentOffset.y), animated: false)

        UIView.animate(withDuration: duration) {
            self.pagerView.collectionView.setContentOffset(CGPoint(x: self.pagerView.collectionView.contentOffset.x + CardsViewController.paddingCards, y: self.pagerView.collectionView.contentOffset.y), animated: false)
        }

    }

}

// MARK: - APPIUM

 extension CardsViewController {

    func setAccessibilities() {

        #if APPIUM

            navigationController?.navigationItem.titleView?.isAccessibilityElement = false
            navigationController?.navigationBar.isAccessibilityElement = false
            balancesView.isAccessibilityElement = false

            pagerView.accessibilityIdentifier = ConstantsAccessibilities.CARD_TABLE

            Tools.setAccessibility(view: balancesView.balanceAmountLabel!, identifier: ConstantsAccessibilities.CARD_AMOUNT_TEXT_ID)

            balancesView.stringAccesibilieBalanceText = ConstantsAccessibilities.CARD_AVAILABLE_OUTPUTTEXT
            balancesView.stringAccesibilieicon1Text = ConstantsAccessibilities.CARD_LINK_IMAGE
            balancesView.stringAccesibilieicon2Text = ConstantsAccessibilities.CARD_BULLET_IMAGE
            balancesView.stringAccesibilieBalance2Text = ConstantsAccessibilities.CARD_PRODUCT_TYPE_OUTPUTTEXT
            balancesView.stringAccesibilieBalance3Text = ConstantsAccessibilities.CARD_LAST_DIGITS_OUTPUTTEXT

            transactionView.isAccessibilityElement = false
            transactionView.transactionsView.isAccessibilityElement = false
            transactionView.messageView.isAccessibilityElement = false
            transactionView.transactionsTableView.isAccessibilityElement = false

            Tools.setAccessibility(view: transactionView.showAllTransactionsButton!, identifier: ConstantsAccessibilities.CARD_SHOW_TRANSACTIONS_BUTTON)

            Tools.setAccessibility(view: transactionView.titleLastTransactionsLabel!, identifier: ConstantsAccessibilities.CARD_TRANSACTION_VIEW_LAST_MOVEMENTS)

            Tools.setAccessibility(view: transactionView.messageButton!, identifier: ConstantsAccessibilities.CARD_TRANSACTION_VIEW_SEARCH_MOVEMENTS)

            transactionView.transactionsTableView.accessibilityIdentifier = ConstantsAccessibilities.TRANSACTIONS_LIST_ID
            Tools.setAccessibility(view: transactionView.directAccessButton!, identifier: ConstantsAccessibilities.CARD_DIRECT_ACCESS_BUTTON)

            Tools.setAccessibility(view: transactionView.messageLabel!, identifier: ConstantsAccessibilities.CARD_LAST_MOVEMENT_EMPTY_TEXT)
            Tools.setAccessibility(view: transactionView.messageImageView!, identifier: ConstantsAccessibilities.CARD_NO_MOVEMENT_IMAGE)

            Tools.setAccessibility(view: transactionView.directAccessMessageLabel, identifier: ConstantsAccessibilities.CARD_INFO_LAST_MOVEMENTS_OUTPUT_TEXT)

            optionsBarView.accessibilityIdentifier = ConstantsAccessibilities.CARD_OPERATIVES_BAR

            Tools.setAccessibility(view: optionsBarView.buttonsBarCollectionView, identifier: ConstantsAccessibilities.CARD_BUTTON_BAR)

            Tools.setAccessibility(view: balancesView.balanceLimitTitleLabel, identifier: ConstantsAccessibilities.CARD_CREDIT_CARD_GRANTED_OUTPUTTEXT)
            Tools.setAccessibility(view: balancesView.balanceConsumedTitleLabel, identifier: ConstantsAccessibilities.CARD_CREDIT_CARD_DISPOSED_OUTPUTTEXT)

            Tools.setAccessibility(view: balancesView.balanceLimitValueLabel, identifier: ConstantsAccessibilities.CARD_CREDIT_CARD_GRANTED_COMPONENT)
            Tools.setAccessibility(view: balancesView.balanceConsumedValueLabel, identifier: ConstantsAccessibilities.CARD_CREDIT_CARD_DISPOSED_COMPONENT)

            Tools.setAccessibility(view: balancesView.customProgressView, identifier: ConstantsAccessibilities.CARD_CREDIT_CARD_BAR)

            cardOnOffView.accessibilityIdentifier = ConstantsAccessibilities.CARD_ON_OFF_COMPONENT

            cardOnOffView.customSwitchButton.accessibilityIdentifier = ConstantsAccessibilities.CARD_SWITCH_BUTTON

            Tools.setAccessibility(view: cardOnOffView.customSwitchButton.switchButton, identifier: ConstantsAccessibilities.CARD_ON_OFF_BUTTON)

            Tools.setAccessibility(view: cardOnOffView.onOffTitleLabel, identifier: ConstantsAccessibilities.CARD_ON_OFF_OUTPUT_TEXT)

        #endif

    }

}
