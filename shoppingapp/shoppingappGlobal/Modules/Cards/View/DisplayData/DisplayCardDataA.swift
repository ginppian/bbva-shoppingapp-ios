//
//  DisplayCardDataA.swift
//  shoppingappMX
//
//  Created by jesus.martinez on 14/12/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation

class DisplayCardDataA: DisplayCardData {

    var selectedCard: SelectDisplayCardDataAType

    var virtualCardDisplayData: DisplayCardData?
    var virtualInfoDisplayData: VirtualInforDisplayData?
    var stickerCardDisplayData: DisplayCardData?

    override init(withPan pan: String, andHolderName holderName: String) {
        selectedCard = .physical
        super.init(withPan: pan, andHolderName: holderName)
    }

    static func generateDisplayCardDataA(fromCardBO cardBO: CardBO, withVirtualCardBO virtualCardBO: CardBO?, withStickerCardBO stickerCardBO: CardBO?) -> DisplayCardDataA {

        let formattedPan = formatTextUsingSeparator(text: cardBO.number)
        
        let displayDataCard = DisplayCardDataA(withPan: formattedPan, andHolderName: cardBO.holderName.uppercased())

        displayDataCard.fillDisplayCardData(withCardBO: cardBO)
        displayDataCard.fillButtonActionsDiplayData(withCardBO: cardBO)

        if let virtualCard = virtualCardBO {

            displayDataCard.virtualCardDisplayData = DisplayCardData.generateDisplayCardData(fromCardBO: virtualCard)

            if virtualCard.isBlocked() {

                displayDataCard.virtualCardDisplayData?.leftButtonDisplayData = DisplayCardButton(imageNamed: ConstantsImages.Card.creditcard_icon,
                                                                                                  title: Localizables.cards.key_card_generate_digital_card_text,
                                                                                                  actionType: .generateDigitalCard,
                                                                                                  accesibility: ConstantsAccessibilities.CARD_GENERATE_CARD_BUTTON)

                displayDataCard.virtualCardDisplayData?.rightButtonDisplayData = nil
            }

        } else {

            var link: String?
            var description = Localizables.cards.key_cards_digitalize_disable_for_this_card_text
            var alpha: CGFloat = 0.5

            if cardBO.isAssociableToVirtualCards() {
                description = Localizables.cards.key_cards_create_digital_card_text
                link = Localizables.cards.key_cards_get_digital_card_text
                alpha = 1.0
            }

            displayDataCard.virtualInfoDisplayData = VirtualInforDisplayData(title: Localizables.cards.key_card_digital_text, description: description, link: link, alpha: alpha)

        }

        if let stickerCard = stickerCardBO {

            displayDataCard.stickerCardDisplayData = DisplayCardData.generateDisplayCardData(fromCardBO: stickerCard)

        }

        return displayDataCard

    }

}

enum SelectDisplayCardDataAType {
    case physical
    case digital
    case sticker
}
