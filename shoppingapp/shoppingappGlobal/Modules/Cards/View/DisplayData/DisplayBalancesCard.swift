//
//  DisplayBalancesCard.swift
//  shoppingapp
//
//  Created by ruben.fernandez on 25/9/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation

struct DisplayBalancesCard {

    var cardBO: CardBO?
    var amount: NSAttributedString?
    var balanceText: String?
    var icon1Image: String?
    var balance2Text: String?
    var icon2Image: String?
    var balance3Text: String?
    var balanceConsumedTitle: String?
    var balanceConsumedValue: NSAttributedString?
    var balanceLimitValue: NSAttributedString?
    var balanceLimitTitle: String?
    var balanceDisposed: Float = 0.0
    var balanceGranted: Float = 0.0

    static func generateDisplayBalanceCard(fromCardBO cardBO: CardBO, andCurrencyBO currencyBO: CurrencyBO, andFormatter formatter: AmountFormatter) -> DisplayBalancesCard {

        var displayBalanceCard = DisplayBalancesCard()

        var disposedCurrentBalance: Float = 0.0
        var grantCreditBalance: Float = 0.0

        displayBalanceCard.cardBO = cardBO
        
        var balanceCurrency: String?

        if cardBO.isCreditCard() {

            var isDisposedValue: Bool = false
            
            displayBalanceCard.balanceText = Localizables.cards.key_cards_availablecredit_text
            displayBalanceCard.balanceLimitTitle = Localizables.cards.key_cards_limit_text
            displayBalanceCard.balanceConsumedTitle = Localizables.cards.key_cards_consumed_text

            for grantedBalance in cardBO.grantedCredits {

                if (cardBO.currencyMajor != nil && cardBO.currencyMajor == grantedBalance.currency) || (currencyBO.code == grantedBalance.currency) {

                    displayBalanceCard.balanceLimitValue = formatterAmountWith(withAmountDecimal: grantedBalance.amount, withBigSize: 16, withSmallSize: 10, withFormatter: formatter)

                    grantCreditBalance = grantedBalance.amount.floatValue
                }
            }

            for currentBalances in cardBO.disposedBalance.currentBalances {

                if (cardBO.currencyMajor != nil && cardBO.currencyMajor == currentBalances.currency) || cardBO.currencyMajor == nil && (currencyBO.code == currentBalances.currency) {

                    displayBalanceCard.balanceConsumedValue = formatterAmountWith(withAmountDecimal: currentBalances.amount, withBigSize: 16, withSmallSize: 10, withFormatter: formatter)

                    disposedCurrentBalance = currentBalances.amount.floatValue

                    isDisposedValue = true
                }

            }

            if disposedCurrentBalance < 0 || cardBO.disposedBalance.currentBalances.isEmpty || !isDisposedValue {
                displayBalanceCard.balanceDisposed = 0.0
                displayBalanceCard.balanceGranted = 1.0
            } else if disposedCurrentBalance > grantCreditBalance || disposedCurrentBalance == grantCreditBalance {
                displayBalanceCard.balanceDisposed = 1.0
                displayBalanceCard.balanceGranted = 1.0
            } else if disposedCurrentBalance < grantCreditBalance {
                displayBalanceCard.balanceGranted = grantCreditBalance
                displayBalanceCard.balanceDisposed = disposedCurrentBalance
            }

        } else if cardBO.isDebitCard() {
            
            displayBalanceCard.balanceText = Localizables.cards.key_cards_availablebalance_text
            
            balanceCurrency = cardBO.grantedCredits.first { $0.currency == cardBO.currencyMajor || $0.currency == currencyBO.code }?.currency

        } else if cardBO.isPrepaid() && !cardBO.relatedContracts.isEmpty {
            displayBalanceCard.balanceText = Localizables.cards.key_cards_linkedbalance_text
            displayBalanceCard.icon1Image = ConstantsImages.Card.link_card_icon

            for relatedContract in cardBO.relatedContracts {

                if relatedContract.relationType?.id == .linkedWith, let number = relatedContract.number {

                    if relatedContract.product?.id == .accounts {
                        displayBalanceCard.balance2Text = Localizables.cards.key_cards_account_text
                    } else if relatedContract.product?.id == .cards {
                        displayBalanceCard.balance2Text = Localizables.cards.key_cards_card_text
                    }

                    displayBalanceCard.icon2Image = ConstantsImages.Common.bullet
                    let last4 = number.suffix(4)
                    displayBalanceCard.balance3Text = String(last4)

                }
            }
        } else if cardBO.isPrepaid() && cardBO.relatedContracts.isEmpty {
            displayBalanceCard.balanceText = Localizables.cards.key_cards_prepaidbalance_text
        } else {
            displayBalanceCard.balanceText = Localizables.cards.key_cards_availablebalance_text
        }
        
        if !cardBO.isDebitCard() {
            balanceCurrency = cardBO.availableBalance.currentBalances.first { $0.currency == cardBO.currencyMajor || $0.currency == currencyBO.code }?.currency
        }
        
        if let balanceCurrency = balanceCurrency, let balance = cardBO.currentBalanceToShow(mainCurrencyCode: balanceCurrency)?.amount {
            displayBalanceCard.amount = formatterAmountWith(withAmountDecimal: balance, withBigSize: 36, withSmallSize: 22, withFormatter: formatter)
        }

        return displayBalanceCard

    }

    private static func formatterAmountWith(withAmountDecimal amount: NSDecimalNumber, withBigSize bigSize: Int, withSmallSize smallSize: Int, withFormatter formatter: AmountFormatter) -> NSAttributedString {

        var isNegative: Bool = false

        if amount.compare(NSDecimalNumber(value: 0)) == ComparisonResult.orderedAscending {
            isNegative = true
        }

        return formatter.attributtedText(forAmount: amount, isNegative: isNegative, bigFontSize: bigSize, smallFontSize: smallSize)
    }

}
