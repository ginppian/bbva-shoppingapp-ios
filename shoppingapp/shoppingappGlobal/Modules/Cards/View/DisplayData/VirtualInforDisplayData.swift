//
//  VirtualInforDisplayData.swift
//  shoppingappMX
//
//  Created by jesus.martinez on 15/12/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

struct VirtualInforDisplayData {

    let title: String
    let description: String
    let link: String?
    let alpha: CGFloat

}
