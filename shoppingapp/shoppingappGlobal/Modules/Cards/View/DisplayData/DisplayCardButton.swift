//
//  DisplayCardButton.swift
//  shoppingapp
//
//  Created by jesus.martinez on 19/12/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

struct DisplayCardButton {

    var imageNamed: String?
    var title: String
    var actionType: DisplayCardButtonActionType
    var accesibility: String?

}

enum DisplayCardButtonActionType {
    case copyPan
    case seeCVV
    case activateCard
    case activateCardExplanation
    case generateDigitalCard
}
