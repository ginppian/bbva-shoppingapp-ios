//
//  DisplayCardData.swift
//  shoppingapp
//
//  Created by ruben.fernandez on 14/9/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit

class DisplayCardData {

    var cardBO: CardBO?

    var imageCard: String?
    var backImageCard: String?
    var imageStatus: String?
    var titleCard: String?

    var pan: String
    var expirationDate: String?
    var holderName: String

    var bottomTitleText: String?
    var bottomImageNamed: String?

    var leftButtonDisplayData: DisplayCardButton?
    var rightButtonDisplayData: DisplayCardButton?

    init(withPan pan: String, andHolderName holderName: String) {

        self.pan = pan
        self.holderName = holderName
    }

    static func generateDisplayCardData(fromCardBO cardBO: CardBO) -> DisplayCardData {

        let formattedPan = formatTextUsingSeparator(text: cardBO.number)
        
        let displayDataCard = DisplayCardData(withPan: formattedPan, andHolderName: cardBO.holderName.uppercased())

        displayDataCard.fillDisplayCardData(withCardBO: cardBO)
        displayDataCard.fillButtonActionsDiplayData(withCardBO: cardBO)

        return displayDataCard

    }

    func fillDisplayCardData(withCardBO cardBO: CardBO) {

        expirationDate = cardBO.expirationDate?.string(format: Date.DATE_FORMAT_MONTH_SHORT_YEAR)

        self.cardBO = cardBO

        imageCard = cardBO.imageFront
        backImageCard = cardBO.imageBack

        if cardBO.isBlocked() || cardBO.isCanceled() || cardBO.isUnknown() {
            
            imageStatus = ConstantsImages.Card.card_blocked
            
        } else if cardBO.isPendingEmbossing() || cardBO.isPendingDelivery() {

            imageStatus = ConstantsImages.Card.card_delivery

        } else if cardBO.isInoperative() {

            imageStatus = ConstantsImages.Card.card_activate

        } else if cardBO.isCardOperative() && !cardBO.isCardOn() {
            
            imageStatus = ConstantsImages.Card.card_off
            
        } else if cardBO.isCardOperative() && ((cardBO.activatorExist(withKeyActivation: .onoff) && !cardBO.getActivator(withKeyActivation: .onoff))) {
            imageStatus = ConstantsImages.Card.card_off
        } else if cardBO.isCardOperative() && ((cardBO.activatorExist(withKeyActivation: .ecommerceActivation) && !cardBO.getActivator(withKeyActivation: .ecommerceActivation)) || (cardBO.activatorExist(withKeyActivation: .cashwithdrawalActivation) && !cardBO.getActivator(withKeyActivation: .cashwithdrawalActivation)) || (cardBO.activatorExist(withKeyActivation: .purchasesActivation) && !cardBO.getActivator(withKeyActivation: .purchasesActivation)) || (cardBO.activatorExist(withKeyActivation: .foreignCashWithDrawalActivation) && !cardBO.getActivator(withKeyActivation: .foreignCashWithDrawalActivation)) || (cardBO.activatorExist(withKeyActivation: .foreignPurchasesActivation) && !cardBO.getActivator(withKeyActivation: .foreignPurchasesActivation))) {

            imageStatus = ConstantsImages.Card.card_off

        } else if cardBO.isCardOperative() && (cardBO.getActivator(withKeyActivation: .onoff) || cardBO.getActivator(withKeyActivation: .ecommerceActivation) || cardBO.getActivator(withKeyActivation: .cashwithdrawalActivation) || cardBO.getActivator(withKeyActivation: .purchasesActivation) || cardBO.getActivator(withKeyActivation: .foreignCashWithDrawalActivation) || cardBO.getActivator(withKeyActivation: .foreignPurchasesActivation)) {

            imageStatus = ""

        } else {
            imageStatus = ""
        }

        if !Tools.isStringNilOrEmpty(withString: cardBO.alias) {
            titleCard = cardBO.alias
        } else {
            titleCard = cardBO.title?.name ?? ""
        }

        if cardBO.isSticker() {
            pan = ""
            holderName = ""
            expirationDate = nil
        }

    }

    func fillButtonActionsDiplayData(withCardBO cardBO: CardBO) {

        if cardBO.isInoperative() && cardBO.isSettableToOperative() {

            leftButtonDisplayData = DisplayCardButton(imageNamed: ConstantsImages.Card.creditcard_icon,
                                                      title: Localizables.cards.key_cards_activate_card_text,
                                                      actionType: .activateCard,
                                                      accesibility: ConstantsAccessibilities.CARD_BOTTOM_ACTION_BUTTON)

        } else if cardBO.isInoperative() && !cardBO.isSettableToOperative() {

            leftButtonDisplayData = DisplayCardButton(imageNamed: ConstantsImages.Common.help_icon_on_off,
                                                      title: Localizables.cards.key_cards_how_activate_card_text,
                                                      actionType: .activateCardExplanation,
                                                      accesibility: ConstantsAccessibilities.CARD_BOTTOM_ACTION_BUTTON)

        } else if cardBO.isCardOperative() && cardBO.isReadableSecurityData() {

            leftButtonDisplayData = DisplayCardButton(imageNamed: nil,
                                                      title: Localizables.cards.key_cards_copy_text,
                                                      actionType: .copyPan,
                                                      accesibility: ConstantsAccessibilities.CARD_COPY_PAN_ACTION_BUTTON)

            rightButtonDisplayData = DisplayCardButton(imageNamed: nil,
                                                       title: Localizables.cards.key_card_see_cvv_text,
                                                       actionType: .seeCVV,
                                                       accesibility: ConstantsAccessibilities.CARD_BOTTOM_ACTION_BUTTON_VER_CVV)

        } else {
            leftButtonDisplayData = DisplayCardButton(imageNamed: nil,
                                                      title: Localizables.cards.key_cards_copy_text,
                                                      actionType: .copyPan,
                                                      accesibility: ConstantsAccessibilities.CARD_COPY_PAN_ACTION_BUTTON)
        }

    }
    
    static func formatTextUsingSeparator(text: String, groupSize: Int = 4, separator: Character = " ") -> String {
        
        let formatString = text.enumerated().map({ (offset: Int, element: Character) -> [Character] in
            
            return (offset > 0) && (offset % groupSize == 0) ? [separator, element] : [element]
        }).joined()
        
        return String(formatString)
    }
}
