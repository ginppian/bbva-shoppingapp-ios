//
//  CardsViewController.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 8/3/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit
import CellsNativeCore
import CellsNativeComponents

class CardsViewControllerA: CardsViewController {

    @IBOutlet weak var pointsView: PointsView!

    override func configurePagerView() {

        pagerView.register(UINib(nibName: CardTableViewCellA.nibName, bundle: nil), forCellWithReuseIdentifier: CardTableViewCellA.identifier)

        pagerView.isInfinite = false
        pagerView.transformer = FSPagerViewTransformer(type: .linear)
        pagerView.transformer?.minimumScale = CardsViewControllerA.minimumScaleCards
        pagerView.itemSize = CGSize(width: view.frame.size.width - (CardsViewControllerA.paddingCards * 2), height: pagerView.frame.size.height)
        pagerView.delegate = self
        pagerView.dataSource = self

    }

    override func doAnimationDetail() {

        cardDataInfoContainerStackView.layer.frame = CGRect(x: cardDataInfoContainerStackView.frame.origin.x, y: -310, width: cardDataInfoContainerStackView.frame.size.width, height: cardDataInfoContainerStackView.frame.size.height)

        showCardDataInfo()

        UIView.animate(withDuration: CardsViewController.durationForAnimationDetail) {

            self.cardDataInfoContainerStackView.layer.frame = CGRect(x: (self.cardDataInfoContainerStackView.frame.origin.x), y: (self.pagerView.frame.size.height), width: (self.cardDataInfoContainerStackView.frame.size.width), height: (self.cardDataInfoContainerStackView.frame.size.height))

        }

    }

    override func loadCardCell() -> CardTableViewCell? {
        return Bundle.main.loadNibNamed(CardTableViewCellA.nibName, owner: self, options: nil)?.first as? CardTableViewCell
    }

    override func cardImageVerticalSpacing() -> CGFloat {
        return CardTableViewCellA.cardImageVerticalSpacing
    }

    override func showCard(atIndex index: Int, hideCardInformationFromVisibleCell: Bool = true) {

        scrollView.setContentOffset(.zero, animated: false)

        if hideCardInformationFromVisibleCell {
            indexOfCardTransition = index
        }
        itemDisplayedIndex = index

        let visibleIndexPath = IndexPath(row: itemDisplayedIndex, section: 0)

        if let visibleCell = pagerView.collectionView.cellForItem(at: visibleIndexPath) as? CardTableViewCellA, let displayDataForVisibleCell = displayItems?.items?[itemDisplayedIndex].displayCardData {

            visibleCell.configureCell(withDisplayCardData: displayDataForVisibleCell)

            if hideCardInformationFromVisibleCell {
                visibleCell.cardInformationHidden()
            }
        }

        self.pagerView.scrollToItem(at: index, animated: true)

    }
    
    override func onOffViewHelpButtonPressed(_ onOffView: OnOffView) {
        
        guard let displayItem = displayItems?.items?[safeElement: itemDisplayedIndex]?.displayCardData as? DisplayCardDataA else {
            super.onOffViewHelpButtonPressed(onOffView)
            return
        }
        
        let currentCardSelected = cardSelected(forDisplayData: displayItem)
        
        presenter.onOffViewHelpButtonPressed(forCard: currentCardSelected)
    }
    
    override func trackCardOmniture(forIndex index: Int) {
        
        guard let displayItem = displayItems?.items?[safeElement: itemDisplayedIndex]?.displayCardData as? DisplayCardDataA else {
            super.trackCardOmniture(forIndex: index)
            return
        }
        
        let currentCardSelected = cardSelected(forDisplayData: displayItem)
        
        if let titleID = currentCardSelected?.title?.id {
            TrackerHelper.sharedInstance().trackCardsScreen(forTitleId: titleID, andCustomerID: SessionDataManager.sessionDataInstance().customerID)
        }
    }
    
    fileprivate func trackCardOmniture(ForTabPressedWithDisplayData displayData: DisplayCardData?) {
        guard let displayData = displayData else {
            return
        }
        
        if let titleID = displayData.cardBO?.title?.id {
            TrackerHelper.sharedInstance().trackCardsScreen(forTitleId: titleID, andCustomerID: SessionDataManager.sessionDataInstance().customerID)
        }
    }
    
    fileprivate func cardSelected(forDisplayData displayData: DisplayCardDataA) -> CardBO? {
        
        switch displayData.selectedCard {
        case .digital:
            return displayData.virtualCardDisplayData?.cardBO
        case .sticker:
            return displayData.stickerCardDisplayData?.cardBO
        case .physical:
            return displayData.cardBO
        }
    }
}

extension CardsViewControllerA: CardTableViewCellADelegate {

    func tableViewCell(_ cardTableViewCell: CardTableViewCell, didSelectDisplayItem displayItem: DisplayCardData?) {

        scrollView.setContentOffset(.zero, animated: true)
        (presenter as! CardsPresenterProtocolA).tabPressed(withDisplayData: displayItem)
        trackCardOmniture(ForTabPressedWithDisplayData: displayItem)
    }

}

// MARK: - APPIUM

extension CardsViewControllerA {

    func setAccessibilitiesA() {

        #if APPIUM

            pointsView.accessibilityIdentifier = ConstantsAccessibilities.POINTS_COMPONENT

            Tools.setAccessibility(view: pointsView.pointsLabel, identifier: ConstantsAccessibilities.POINTS_OUTPUTTEXT)

            Tools.setAccessibility(view: pointsView.descriptionPointsLabel, identifier: ConstantsAccessibilities.POINTS_DESCRIPTIONTEXT)

        #endif
    }

}

extension CardsViewControllerA: CardsViewProtocolA {

    func showCardDataInfo() {

        cardDataInfoContainerStackView.isHidden = false
        scrollView.isScrollEnabled = true
        pagerView.alpha = 1.0

        setAccessibilities()

    }

    func hideCardDataInfo() {
        cardDataInfoContainerStackView.isHidden = true
        scrollView.setContentOffset(.zero, animated: true)
        scrollView.isScrollEnabled = false
    }

    func showPointsView(withDisplayData displayData: PointsDisplayData) {
        pointsView.isHidden = false
        pointsView.showDisplayDataPoints(withPointsDisplayData: displayData)
        setAccessibilitiesA()
}

    func hidePointsView() {

        pointsView.isHidden = true
    }

}

// MARK: - FlipCardAnimatorStarterProtocol
extension CardsViewControllerA: FlipCardAnimatorStarterProtocol {
    
    var backCardImageURL: String {
        return displayItems?.items?[itemDisplayedIndex].displayCardData?.backImageCard ?? ""
    }
    
    func obtainDigitalCardButtonPressedTableViewCell(_ cardTableViewCell: CardTableViewCell) {
        
        (presenter as? CardsPresenterProtocolA)?.obtainDigitalCardButtonPressed(atIndex: itemDisplayedIndex)
    }

    private func visibleCardCell() -> CardTableViewCell? {
        let visibleIndexPath = IndexPath(row: itemDisplayedIndex, section: 0)
        return pagerView.collectionView.cellForItem(at: visibleIndexPath) as? CardTableViewCell
    }

    private func visibleImageView() -> UIImageView? {
        guard let visibleCell = visibleCardCell(), let cardImageView = visibleCell.cardImageView else {
            return nil
        }
        return cardImageView
    }

    func prepareForTransition() {
        UIView.animate(withDuration: 0.3, animations: {
            let desiredOffset = CGPoint(x: 0, y: -self.scrollView.contentInset.top)
            self.scrollView.setContentOffset(desiredOffset, animated: false)
        }, completion: { _ in
            return
        })
    }

    func frame() -> CGRect {
        guard let cardImageView = visibleImageView() else {
            return .zero
        }
        return cardImageView.convert(cardImageView.bounds, to: nil)
    }

    func frontImage() -> UIImage? {
        guard let cardImageView = visibleImageView() else {
            return CardImageUtils.placeholderImage
        }
        return cardImageView.image
    }

    func backImage() -> UIImage? {
        return CardImageUtils.placeholderBackImage
    }

    func startAnimation() {
        
        visibleCardCell()?.cardInformationHidden()
    }
    
    func finishAnimation() {
        
        visibleCardCell()?.cardInformationVisible()
    }
}
