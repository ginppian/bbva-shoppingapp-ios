//
//  FlipTransitioner.swift
//  shoppingappMX
//
//  Created by jesus.martinez on 9/8/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol FlipCardAnimatorStarterProtocol: class {
    var backCardImageURL: String { get }
    
    func frame() -> CGRect
    func frontImage() -> UIImage?
    func backImage() -> UIImage?
    func prepareForTransition()
    func startAnimation()
    func finishAnimation()
}

protocol FlipCardAnimatorFinisherProtocol: class {
    func prepareFinishCardImageView(withFrame frame: CGRect, from imageURL: String)
}

class FlipAnimatorDelegate: NSObject, UIViewControllerTransitioningDelegate {

    func animationController(forPresented presented: UIViewController, presenting: UIViewController, source: UIViewController) -> UIViewControllerAnimatedTransitioning? {

        let currentNVC = ((presenting as? MainViewController)?.rootViewController as? UITabBarController)?.selectedViewController as? UINavigationController ?? source as? UINavigationController

        guard let sourceFlip = currentNVC?.viewControllers.first as? FlipCardAnimatorStarterProtocol, let presentedFlip = (presented as? UINavigationController)?.viewControllers.first as? FlipCardAnimatorFinisherProtocol else {
            return nil
        }
        sourceFlip.prepareForTransition()

        return FlipCardAnimator(flipStarter: sourceFlip, flipFinisher: presentedFlip)
    }
}

class FlipCardAnimator: NSObject, UIViewControllerAnimatedTransitioning {
    var duration: TimeInterval = 0.5
    weak var flipStarter: FlipCardAnimatorStarterProtocol?
    weak var flipFinisher: FlipCardAnimatorFinisherProtocol?

    init(flipStarter: FlipCardAnimatorStarterProtocol, flipFinisher: FlipCardAnimatorFinisherProtocol) {
        self.flipStarter = flipStarter
        self.flipFinisher = flipFinisher
    }

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {

        guard let toViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to),
            let toView = toViewController.view, let imageBack = flipStarter?.backImage(), let cgImageBack = imageBack.cgImage else {
            return
        }
        
        //frames
        guard let flipingFrame = flipStarter?.frame() else {
            return
        }

        let screenBounds = UIScreen.main.bounds

        let container = transitionContext.containerView
        toViewController.view.frame = CGRect(x: 0, y: screenBounds.height, width: screenBounds.width, height: screenBounds.height)

        toView.backgroundColor = .MEDIUMBLUE
        container.addSubview(toView)
        toView.layoutIfNeeded()

        let cardBackImage = UIImage(cgImage: cgImageBack, scale: imageBack.scale, orientation: .upMirrored)
        let cardImageView = UIImageView(frame: CGRect(x: flipingFrame.minX, y: flipingFrame.minY, width: flipingFrame.width, height: flipingFrame.height))
        cardImageView.image = flipStarter?.frontImage()
        cardImageView.layer.zPosition = 200
        container.addSubview(cardImageView)

        flipFinisher?.prepareFinishCardImageView(withFrame: flipingFrame, from: (flipStarter?.backCardImageURL ?? ""))
        flipStarter?.startAnimation()

        cardImageView.rotateY(CGFloat(Double.pi / 2)).duration(duration / 2).completion {
            cardImageView.image = cardBackImage
        }.then().rotateY(CGFloat(Double.pi / 2)).animate()
        
        cardImageView.rotateY(CGFloat(Double.pi / 2)).duration(duration / 2).completion {
            cardImageView.image = cardBackImage
            UIView.animate(withDuration: self.duration, animations: {
                toView.frame = CGRect(x: 0, y: 0.0, width: screenBounds.width, height: screenBounds.height)
            }, completion: { _ in
                transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
                self.flipStarter?.finishAnimation()
                cardImageView.removeFromSuperview()
            })
        }.then().rotateY(CGFloat(Double.pi / 2)).animate()
        
    }

    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return duration
    }
}
