//
//  CardsPresenterA.swift
//  shoppingappMX
//
//  Created by jesus.martinez on 13/12/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

final class CardsPresenterA<T: CardsViewProtocolA>: CardsPresenter<T> {
    
    var currentCard: CardBO?
    var cardStatus: Bool?
    var preferences = PreferencesManager.sharedInstance()

    override func prepareSelectedCard(showCard: Bool = true) {

        if var showCardId = showCardId, let displayDataItems = displayItems.items {

            if let cardOperationInfo = cardOperationInfo,
                (cardOperationInfo.cardOperation == .block || cardOperationInfo.cardOperation == .cancel),
                (cardOperationInfo.cardType == .sticker || cardOperationInfo.cardType == .digital) {

                showCardId = cardOperationInfo.fisicalCardID ?? showCardId
            }

            var findId = 0
            var displayData: DisplayCardData?

            for index in 0..<displayDataItems.count {

                if displayItems.items![index].displayCardData?.cardBO?.cardId == showCardId {
                    findId = index
                    (displayItems.items![index].displayCardData as? DisplayCardDataA)?.selectedCard = .physical
                    displayData = displayItems.items![index].displayCardData
                    break

                } else if (displayItems.items![index].displayCardData as? DisplayCardDataA)?.virtualCardDisplayData?.cardBO?.cardId == showCardId {
                    findId = index
                    (displayItems.items![index].displayCardData as? DisplayCardDataA)?.selectedCard = .digital
                    displayData = (displayItems.items![index].displayCardData as? DisplayCardDataA)?.virtualCardDisplayData
                    break

                } else if (displayItems.items![index].displayCardData as? DisplayCardDataA)?.stickerCardDisplayData?.cardBO?.cardId == showCardId {
                    findId = index
                    (displayItems.items![index].displayCardData as? DisplayCardDataA)?.selectedCard = .sticker
                    displayData = (displayItems.items![index].displayCardData as? DisplayCardDataA)?.stickerCardDisplayData
                    break
                }

            }

            if let cardOperationInfo = cardOperationInfo, cardOperationInfo.cardType == .digital {
                (displayItems.items![findId].displayCardData as? DisplayCardDataA)?.selectedCard = .digital
                 displayData = (displayItems.items![findId].displayCardData as? DisplayCardDataA)?.virtualCardDisplayData
            }

            if let cardOperationInfo = cardOperationInfo, cardOperationInfo.cardType == .sticker, (displayItems.items![findId].displayCardData as? DisplayCardDataA)?.stickerCardDisplayData != nil {
                (displayItems.items![findId].displayCardData as? DisplayCardDataA)?.selectedCard = .sticker
                displayData = (displayItems.items![findId].displayCardData as? DisplayCardDataA)?.stickerCardDisplayData
            }

            if showCard {
                view?.showCard(atIndex: findId, hideCardInformationFromVisibleCell: !needRefreshPagerView)
            }
            
            tabPressed(withDisplayData: displayData)
            
            if needRefreshPagerView {
                view?.refreshPagerView(atIndex: findId)
            }
        }

        showCardId = nil
        cardOperationInfo = nil
        needRefreshPagerView = false
    }

    override func showInfo(forCardBO cardBO: CardBO) {
        super.showInfo(forCardBO: cardBO)

        if cardBO.isCreditCard() {
            let poinstDisplayData = PointsDisplayData.generatePointsDisplayData(withCardBO: cardBO)

            if poinstDisplayData.needShowPoinst {
                view?.showPointsView(withDisplayData: poinstDisplayData)
            } else {
                view?.hidePointsView()
            }
        } else {
            view?.hidePointsView()
        }
    }

    override func createDisplayDataItems() -> DisplayItemsCards {

        displayItems.items?.removeAll()
        
        for cardBO in (cards?.cards)! {

            formatter = (cardBO.currencyMajor != nil) ? AmountFormatter(codeCurrency: cardBO.currencyMajor!) : AmountFormatter(codeCurrency: currencyBO.code)

            if cardBO.isNormalPlastic() {
                var displayItemCard = DisplayItemCard()
                displayItemCard.displayCardData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: cardBO.associatedVirtualCardBO(fromCards: cards!.cards), withStickerCardBO: cardBO.associatedStickerCardBO(fromCards: cards!.cards))
                displayItems.items?.append(displayItemCard)
            }

        }

        return displayItems
    }

    func checkIfshowData() {

        if SessionDataManager.sessionDataInstance().isShowCardAnimation {
            view?.showCardDataInfo()
        }

    }

    override func cardShowed(atIndex index: Int) {

        if let displayCardData = displayItems.items?[index].displayCardData as? DisplayCardDataA {

            if displayCardData.selectedCard == .physical {

                showInfo(forCardBO: displayCardData.cardBO!)
                checkIfshowData()

            } else if displayCardData.selectedCard == .digital {

                if let virtualCardBO = displayCardData.virtualCardDisplayData?.cardBO {

                    showInfo(forCardBO: virtualCardBO)
                    checkIfshowData()

                } else {

                    view?.showNavigation(withTitle: Localizables.cards.key_card_digital_text)
                    view?.hideCardDataInfo()

                }

            } else {

                if let stickerCardBO = displayCardData.stickerCardDisplayData?.cardBO {

                    showInfo(forCardBO: stickerCardBO)
                    checkIfshowData()

                }

            }

        }

    }

    override func getUpdatedCard(withCard card: CardBO, withStatus status: Bool, fromCells isFromCell: Bool) {

        if let data = activations?.data {
            card.updateActivations(activationBO: data)
        }

        updateDisplayCard(withCardBO: card)

        view?.showInfoCards(withDisplayItems: displayItems)

        if !isFromCell {
            
            view?.showToastSuccess(withSuccessMessage: (status ? Localizables.cards.key_cards_off_successful_text : Localizables.cards.key_cards_on_successful_text), andBackgroundColor: .BBVADARKGREEN)
            
            if status {
                view?.sendOmnitureScreenCardOffSuccess()
            }
        }
    }

    func updateDisplayCard(withCardBO cardBO: CardBO) {

        let countArray: Int = (displayItems.items?.count)!

        for i in 0 ..< countArray {

            let tempDictionary = displayItems.items?[i]

            if tempDictionary?.displayCardData?.cardBO?.cardId == cardBO.cardId {

                displayItems.items?[i].displayCardData?.fillDisplayCardData(withCardBO: cardBO)

            } else if (tempDictionary?.displayCardData as! DisplayCardDataA).virtualCardDisplayData?.cardBO?.cardId == cardBO.cardId {

                (displayItems.items?[i].displayCardData as! DisplayCardDataA).virtualCardDisplayData?.fillDisplayCardData(withCardBO: cardBO)

            } else if (tempDictionary?.displayCardData as! DisplayCardDataA).stickerCardDisplayData?.cardBO?.cardId == cardBO.cardId {

                (displayItems.items?[i].displayCardData as! DisplayCardDataA).stickerCardDisplayData?.fillDisplayCardData(withCardBO: cardBO)

            }

        }

    }

    override func goToSearchTransactionsForCard(atIndex index: Int) {

        if let displayCardData = displayItems.items?[index].displayCardData as? DisplayCardDataA {

            switch displayCardData.selectedCard {
            case .physical:
                navigateToSearchTransactions(withCardBO: displayCardData.cardBO!)

            case .digital:
                navigateToSearchTransactions(withCardBO: displayCardData.virtualCardDisplayData!.cardBO!)

            case .sticker:
                navigateToSearchTransactions(withCardBO: displayCardData.stickerCardDisplayData!.cardBO!)

            }

        }

    }

    override func goToTransactionsForCard(atIndex index: Int) {

        if let displayCardData = displayItems.items?[index].displayCardData as? DisplayCardDataA {

            switch displayCardData.selectedCard {
            case .physical:
                navigateToTransactionsList(withCardBO: displayCardData.cardBO!)

            case .digital:
                navigateToTransactionsList(withCardBO: displayCardData.virtualCardDisplayData!.cardBO!)

            case .sticker:
                navigateToTransactionsList(withCardBO: displayCardData.stickerCardDisplayData!.cardBO!)

            }

        }

    }

    override func goToTransactionDetail(withTransactionDisplayItem transactionDisplayItem: TransactionDisplayItem, forCardAtIndex index: Int) {

        if let displayCardData = displayItems.items?[index].displayCardData as? DisplayCardDataA {

            switch displayCardData.selectedCard {
            case .physical:
                navigateToTransactionDetail(withCardBO: displayCardData.cardBO!, andTransactionDisplayItem: transactionDisplayItem)

            case .digital:
                navigateToTransactionDetail(withCardBO: displayCardData.virtualCardDisplayData!.cardBO!, andTransactionDisplayItem: transactionDisplayItem)

            case .sticker:
                navigateToTransactionDetail(withCardBO: displayCardData.stickerCardDisplayData!.cardBO!, andTransactionDisplayItem: transactionDisplayItem)
            }

        }

    }

    override func leftCardButtonPressed(atIndex index: Int) {

        if let displayCardData = displayItems.items?[index].displayCardData as? DisplayCardDataA {

            switch displayCardData.selectedCard {
            case .physical:
                if displayCardData.leftButtonDisplayData?.actionType == .copyPan {
                    copyPan(withNumber: displayCardData.cardBO?.number)
                } else if displayCardData.leftButtonDisplayData?.actionType == .activateCard {
                    activateCard(withCardBO: displayCardData.cardBO)
                } else if displayCardData.leftButtonDisplayData?.actionType == .activateCardExplanation {
                    
                    busManager.navigateScreen(tag: CardsPageReaction.ROUTER_TAG, CardsPageReaction.EVENT_NAV_TO_DETAIL_HELP, HelpDetailTransport(idHelpOnOff))
                }
            case .digital:
                if displayCardData.virtualCardDisplayData?.leftButtonDisplayData?.actionType == .copyPan {
                    copyPan(withNumber: displayCardData.virtualCardDisplayData?.cardBO?.number)
                } else if displayCardData.virtualCardDisplayData?.leftButtonDisplayData?.actionType == .activateCard {
                    activateCard(withCardBO: displayCardData.cardBO)
                } else if displayCardData.virtualCardDisplayData?.leftButtonDisplayData?.actionType == .generateDigitalCard {
                    generateDigitalCard(withCardBO: displayCardData.cardBO)
                }
            case .sticker:
                if displayCardData.stickerCardDisplayData?.leftButtonDisplayData?.actionType == .copyPan {
                    copyPan(withNumber: displayCardData.stickerCardDisplayData?.cardBO?.number)
                } else if displayCardData.stickerCardDisplayData?.leftButtonDisplayData?.actionType == .activateCard {
                    activateCard(withCardBO: displayCardData.cardBO)
                }
            }

        }

    }
    
    func obtainDigitalCardButtonPressed(atIndex index: Int) {
        
        if let displayCardData = displayItems.items?[index].displayCardData as? DisplayCardDataA {

            generateDigitalCard(withCardBO: displayCardData.cardBO)
        }
    }

    override func pressedOperativeButton(atIndex index: Int, withDisplayData displayData: ButtonBarDisplayData) {

        if let displayCardData = displayItems.items?[index].displayCardData as? DisplayCardDataA {

            switch displayCardData.selectedCard {
            case .physical:

                if let cardBO = displayCardData.cardBO {
                    pressedOperativeButton(withDisplayData: displayData, cardBO: cardBO)
                }

            case .digital:

                if let cardBO = displayCardData.virtualCardDisplayData?.cardBO {
                    
                    if displayData.optionKey == .block {
                        cancelCard(withCardBO: cardBO)
                    } else {
                        pressedOperativeButton(withDisplayData: displayData, cardBO: cardBO)
                    }
                    
                }

            case .sticker:

                if let cardBO = displayCardData.stickerCardDisplayData?.cardBO {
                    pressedOperativeButton(withDisplayData: displayData, cardBO: cardBO)
                }

            }
        }
    }

    override func rightCardButtonPressed(atIndex index: Int) {
        if let items = displayItems.items {
            if index >= 0 && index < items.count {
                
                if let displayCardData = displayItems.items?[index].displayCardData as? DisplayCardDataA {
                    
                    busManager.publishData(tag: EventTracker.id, PageReactionConstants.NEW_TRACK_EVENT, Event(type: .seeCardCVV))
                    switch displayCardData.selectedCard {
                    case .physical:
                        navigateToCVVScreen(withCardId: displayCardData.cardBO!.cardId)
                        
                    case .digital:
                        navigateToCVVScreen(withCardId: displayCardData.virtualCardDisplayData!.cardBO!.cardId)
                        
                    case .sticker:
                        navigateToCVVScreen(withCardId: displayCardData.stickerCardDisplayData!.cardBO!.cardId)
                    }
                    
                }
            } else {
                errorBO = ErrorBO(message: Localizables.common.key_common_generic_error_text, errorType: .warning, allowCancel: false)
                checkError()
            }
        }
    }

    override func activateCard(withCardBO cardBO: CardBO?) {
        
        guard let cardBO = cardBO else {
            return
        }

        let activateCardBO = ActivateCardCVVTransport()
        activateCardBO.cardNumber = cardBO.number
        activateCardBO.cardImageUrl = cardBO.imageFront
        activateCardBO.alias = cardBO.alias

        var activateCardSuccessObjectCells = ActivateCardSuccessObjectCells()
        activateCardSuccessObjectCells.cardNumber = String(activateCardBO.cardNumber.suffix(4))

        if let activateCardJSON = activateCardBO.toJSON() {

            let cardOperationInfo = createCardOperationInfo(forCardBO: cardBO, operation: .activate)

            if let cardOperationInfo = cardOperationInfo {
                busManager.publishData(tag: CardsPageReactionA.ROUTER_TAG, CardsPageReaction.PARAMS_OPERATION_CARD_INFO, cardOperationInfo)
            }

            busManager.publishData(tag: CardsPageReactionA.ROUTER_TAG, CardsPageReactionA.PARAMS_ACTIVATE_WITH_CVV_CARD, activateCardJSON)

            if let activateCardSuccessObjectCellsJSON = activateCardSuccessObjectCells.toJSON() {
                busManager.publishData(tag: CardsPageReactionA.ROUTER_TAG, CardsPageReaction.PARAMS_SELECT_CARD_ACTIVATE, activateCardSuccessObjectCellsJSON)
            }

            busManager.navigateScreen(tag: CardsPageReactionA.ROUTER_TAG, CardsPageReactionA.EVENT_NAV_TO_ACTIVATE_WITH_CVV_CARD, Void())
        }
    }
    
    func switchCardStatus() {
        if let status = cardStatus, let card = currentCard {
            switchCardUp(with: status, and: card)
        }
    }
    
    override func changeCardStatus(withCardStatus status: Bool, andCard card: CardBO) {
        cardStatus = status
        currentCard = card
        
        if (card.isCardOn() && preferences.isFirstTimeTurningCardOff()) || (!card.isCardOn() && preferences.isFirstTimeTuringCardOn()) {
            cardStatusNavigation(with: card)
        } else {
            switchCardUp(with: status, and: card)
        }
    }

    func cardStatusNavigation(with card: CardBO) {
        let cardTransport = CardsTransport(cardBO: card)
        cardTransport.flagNavigation = true
        busManager.navigateScreen(tag: CardsPageReaction.ROUTER_TAG, CardsPageReaction.EVENT_NAV_TO_ON_OFF_HELP, cardTransport)
    }

}

extension CardsPresenterA: CardsPresenterProtocolA {

    func tabPressed(withDisplayData displayData: DisplayCardData?) {

        if let displayData = displayData, let cardBO = displayData.cardBO {

            if cardBO.isCreditCard() {
                let poinstDisplayData = PointsDisplayData.generatePointsDisplayData(withCardBO: cardBO)

                if poinstDisplayData.needShowPoinst {
                    view?.showPointsView(withDisplayData: poinstDisplayData)
                } else {
                    view?.hidePointsView()
                }
            } else {
                view?.hidePointsView()
            }

            var transactionDisplayData = TransactionDisplayData(numberOfSkeletons: numberOfSkeletons, maximumNumberOfTransactions: maximumTransactionsInTransactionView, cardId: cardBO.cardId)
            transactionDisplayData.showDirectAccess = showDirectAccessToTransactions

            transactionDisplayData.transactionsBO = cardBO.transactions

            formatter = (cardBO.currencyMajor != nil) ? AmountFormatter(codeCurrency: cardBO.currencyMajor!) : AmountFormatter(codeCurrency: currencyBO.code)

            let operativeBarInfo = ButtonsBarDisplayData.generateButtonsBarDisplayData(fromCardBO: cardBO)

            view?.showNavigation(withTitle: displayData.titleCard!)
            view?.showBalancesInfo(withDisplayData: DisplayBalancesCard.generateDisplayBalanceCard(fromCardBO: cardBO, andCurrencyBO: currencyBO, andFormatter: formatter!))
            view?.showOnOffInfo(withDisplayData: OnOffDisplayData(fromCardBO: cardBO))

            if operativeBarInfo.buttons.isEmpty {
                view?.hideOperativesBar()
            } else {
                view?.showOperativeBarInfo(withDisplayData: operativeBarInfo)
            }

            view?.showTransactions(withDisplayData: transactionDisplayData)
            view?.showCardDataInfo()

        } else {

            if displayData == nil {
                view?.showNavigation(withTitle: Localizables.cards.key_card_digital_text)
            }

            view?.hideCardDataInfo()

        }

    }

}
