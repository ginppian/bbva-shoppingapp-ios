//
//  CardsPresenter.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 7/3/17.
//  Copyright © 2017 daniel.petrovic. All rights reserved.
//

import Foundation
import RxSwift
import CellsNativeComponents

class CardsPresenter<T: CardsViewProtocol>: BasePresenter<T> {

    let disposeBag = DisposeBag()

    let maximumTransactionsInTransactionView = 7
    let numberOfSkeletons = 2
    let showDirectAccessToTransactions = Settings.CardsLanding.show_direct_access_to_transactions
    let idHelpOnOff = Constants.HelpId.helpOnOff

    var interactor: CardsInteractorProtocol!

    var cards: CardsBO?
    var currencyBO: CurrencyBO!
    var formatter: AmountFormatter?
    var activations: ActivationsBO?
    var displayItems = DisplayItemsCards()
    var helpDisplayItems = HelpDisplayData()
    var showCardId: String?
    var cardOperationInfo: CardOperationInfo? {
        didSet {
            showCardId = cardOperationInfo?.cardID
        }
    }
    var needRefreshPagerView = false

    required init() {
        super.init(tag: CardsPageReaction.ROUTER_TAG)
        getCurrencyBO()
    }

    override init(busManager: BusManager) {
        super.init(busManager: busManager)
        routerTag = CardsPageReaction.ROUTER_TAG
        getCurrencyBO()
    }

    func receive(model: Model) {

        if let cardsReceived = model as? CardsBO {
            if cardsReceived.timestamp != cards?.timestamp {
                cards = cardsReceived
                if let cards = self.cards?.cards, !cards.isEmpty {
                    self.view?.showInfoCards(withDisplayItems: self.createDisplayDataItems())
                    if needRefreshPagerView {
                        prepareSelectedCard(showCard: true)
                    }
                }
            }
        }
    }

    func getCurrencyBO() {
        ConfigurationDataManager.sharedInstance.provideMainCurrency().subscribe(
            onNext: { [weak self] result in
                if let currencyEntity = result as? CurrencyEntity {
                    let currencyBO = Currencies.currency(forCode: currencyEntity.code)
                    self?.currencyBO = currencyBO
                }
            }).addDisposableTo(disposeBag)
    }

    func createDisplayDataItems() -> DisplayItemsCards {

        displayItems.items?.removeAll()

        for cardBO in (cards?.cards)! {
            formatter = (cardBO.currencyMajor != nil) ? AmountFormatter(codeCurrency: cardBO.currencyMajor!) : AmountFormatter(codeCurrency: currencyBO.code)
            var displayItemCard = DisplayItemCard()
            displayItemCard.displayCardData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)
            displayItems.items?.append(displayItemCard)
        }
        return displayItems
    }

    func cardShowed(atIndex index: Int) {
        let cardBO = cards!.cards[index]
        showInfo(forCardBO: cardBO)
    }

    func manageInitialAnimation() {

        if let cardsSaved = cards {
            if !SessionDataManager.sessionDataInstance().isShowCardAnimation && cardsSaved.cards.count > 1 {

                view?.doAnimationDetail()
                view?.doAnimationCards()

            } else if !SessionDataManager.sessionDataInstance().isShowCardAnimation && cardsSaved.cards.count == 1 {
                view?.doAnimationDetail()
            }
        }

        SessionDataManager.sessionDataInstance().isShowCardAnimation = true

    }

    func viewWillAppear() {

        manageInitialAnimation()

        prepareSelectedCard()
    }

    func prepareSelectedCard(showCard: Bool = true) {

        if let showCardId = showCardId, let cards = cards?.cards {

            let index = cards.index(where: { cardBO -> Bool in
                cardBO.cardId == showCardId
            })

            if let indexOfCard = index, showCard {
                view?.showCard(atIndex: indexOfCard, hideCardInformationFromVisibleCell: needRefreshPagerView)
            }
        }

        showCardId = nil
        cardOperationInfo = nil
        needRefreshPagerView = false
    }

    func createHelpDisplayData() -> HelpDisplayData {

        helpDisplayItems = HelpDisplayData.generateHelpDisplayData(withConfigBO: ConfigPublicManager.sharedInstance().getConfigBO(forSection: Constants.SECTION_TO_SHOW_CARDS, withKey: PreferencesManagerKeys.kIndexToShowCards)!, isFromSection: true)

        return helpDisplayItems
    }

    func showInfo(forCardBO cardBO: CardBO) {
        var transactionDisplayData = TransactionDisplayData(numberOfSkeletons: numberOfSkeletons, maximumNumberOfTransactions: maximumTransactionsInTransactionView, cardId: cardBO.cardId)
        transactionDisplayData.showDirectAccess = showDirectAccessToTransactions
        transactionDisplayData.transactionsBO = cardBO.transactions
        formatter = (cardBO.currencyMajor != nil) ? AmountFormatter(codeCurrency: cardBO.currencyMajor!) : AmountFormatter(codeCurrency: currencyBO.code)

        let title = cardBO.cardAlias()

        let operativeBarInfo = ButtonsBarDisplayData.generateButtonsBarDisplayData(fromCardBO: cardBO)

        view?.showNavigation(withTitle: title)
        view?.showBalancesInfo(withDisplayData: DisplayBalancesCard.generateDisplayBalanceCard(fromCardBO: cardBO, andCurrencyBO: currencyBO, andFormatter: formatter!))
        view?.showOnOffInfo(withDisplayData: OnOffDisplayData(fromCardBO: cardBO))

        if operativeBarInfo.buttons.isEmpty {
            view?.hideOperativesBar()
        } else {
            view?.showOperativeBarInfo(withDisplayData: operativeBarInfo)
        }

        view?.showTransactions(withDisplayData: transactionDisplayData)
    }
    
    func getUpdatedCard(withCard card: CardBO, withStatus status: Bool, fromCells isFromCell: Bool) {

        if let data = activations?.data {
            card.updateActivations(activationBO: data)
        }
        updateDisplayItems(withCardData: DisplayCardData.generateDisplayCardData(fromCardBO: card))
        view?.showInfoCards(withDisplayItems: displayItems)

        if !isFromCell {
        view?.showToastSuccess(withSuccessMessage: (status ? Localizables.cards.key_cards_off_successful_text : Localizables.cards.key_cards_on_successful_text), andBackgroundColor: .BBVADARKGREEN)

        }

        if status {
            view?.sendOmnitureScreenCardOffSuccess()
        }
    }

    func goToSearchTransactionsForCard(atIndex index: Int) {
        let cardBO = cards!.cards[index]
        navigateToSearchTransactions(withCardBO: cardBO)
    }

    func navigateToSearchTransactions(withCardBO cardBO: CardBO) {
        busManager.navigateScreen(tag: routerTag, CardsPageReaction.EVENT_NAV_TO_SECOND_SCREEN, CardsTransport(cardBO: cardBO, flag: true))
    }

    func goToTransactionsForCard(atIndex index: Int) {
        if let cards = cards?.cards {
            let cardBO = cards[index]
            navigateToTransactionsList(withCardBO: cardBO)
        }
    }

    func navigateToTransactionsList(withCardBO cardBO: CardBO) {
        busManager.navigateScreen(tag: routerTag, CardsPageReaction.EVENT_NAV_TO_NEXT_SCREEN, cardBO)
    }

    func goToTransactionDetail(withTransactionDisplayItem transactionDisplayItem: TransactionDisplayItem, forCardAtIndex index: Int) {
        let cardBO = cards!.cards[index]
        navigateToTransactionDetail(withCardBO: cardBO, andTransactionDisplayItem: transactionDisplayItem)
    }

    func navigateToTransactionDetail(withCardBO cardBO: CardBO, andTransactionDisplayItem transactionDisplayItem: TransactionDisplayItem) {

        let transactionsTransport = TransactionsTransport()
        transactionsTransport.transactionDisplayItem = transactionDisplayItem
        transactionsTransport.cardBO = cardBO

        busManager.navigateScreen(tag: routerTag, CardsPageReaction.EVENT_NAV_TO_THIRD_SCREEN, transactionsTransport)

    }

    func leftCardButtonPressed(atIndex index: Int) {
        if let leftDisplayCardButton = displayItems.items?[index].displayCardData?.leftButtonDisplayData {
            if leftDisplayCardButton.actionType == .copyPan {
                copyPan(withNumber: cards?.cards[index].number)
            } else if leftDisplayCardButton.actionType == .activateCard {
                activateCard(withCardBO: cards?.cards[index])
            } else if leftDisplayCardButton.actionType == .generateDigitalCard {
                generateDigitalCard(withCardBO: cards?.cards[index])
            } else if leftDisplayCardButton.actionType == .activateCardExplanation {
                
                busManager.navigateScreen(tag: CardsPageReaction.ROUTER_TAG, CardsPageReaction.EVENT_NAV_TO_DETAIL_HELP, HelpDetailTransport(idHelpOnOff))
            }
        }
    }

    func copyPan(withNumber number: String?) {
        if let pan = number {
            view?.copyPan(withNumber: pan.replacingOccurrences(of: " ", with: ""))
            view?.showToastSuccess(withSuccessMessage: Localizables.cards.key_cards_copy_success_text, andBackgroundColor: .BBVADARKGREEN)
        }
    }

    func activateCard(withCardBO cardBO: CardBO?) {
        guard let cardBO = cardBO else {
            return
        }

        let imageListBO = ActivateCardImageListTransport()
        imageListBO.url = cardBO.imageFront

        let activateCardBO = ActivateCardTransport()
        activateCardBO.cardNumber = cardBO.number
        activateCardBO.imageList = [imageListBO]
        activateCardBO.id = cardBO.cardId

        var activateCardSuccessObjectCells = ActivateCardSuccessObjectCells()
        activateCardSuccessObjectCells.cardNumber = String(activateCardBO.cardNumber.suffix(4))

        if let activateCardJSON = activateCardBO.toJSON() {

            let cardOperationInfo = createCardOperationInfo(forCardBO: cardBO, operation: .activate)

            if let cardOperationInfo = cardOperationInfo {
                busManager.publishData(tag: CardsPageReaction.ROUTER_TAG, CardsPageReaction.PARAMS_OPERATION_CARD_INFO, cardOperationInfo)
            }

            busManager.publishData(tag: CardsPageReaction.ROUTER_TAG, CardsPageReaction.PARAMS_ACTIVATE_CARD, activateCardJSON)

            if let activateCardSuccessObjectCellsJSON = activateCardSuccessObjectCells.toJSON() {
                busManager.publishData(tag: CardsPageReaction.ROUTER_TAG, CardsPageReaction.PARAMS_SELECT_CARD_ACTIVATE, activateCardSuccessObjectCellsJSON)
            }

            busManager.navigateScreen(tag: CardsPageReaction.ROUTER_TAG, CardsPageReaction.EVENT_NAV_TO_ACTIVATE_CARD, Void())
        }

    }

    func cancelCard(withCardBO cardBO: CardBO) {

        var blockCardTranport = BlockCardTransport(id: cardBO.cardId, cardNumber: String(cardBO.number.suffix(4)), alias: cardBO.cardAlias())

        if let image = cardBO.imageFront {
            blockCardTranport.imageList = [CardImage(url: image)]
        }

        if let blockCardJSON = blockCardTranport.toJSON() {

            let cardOperationInfo = createCardOperationInfo(forCardBO: cardBO, operation: .cancel)

            if let cardOperationInfo = cardOperationInfo {
                busManager.publishData(tag: CardsPageReaction.ROUTER_TAG, CardsPageReaction.PARAMS_OPERATION_CARD_INFO, cardOperationInfo)
            }

            busManager.publishData(tag: CardsPageReaction.ROUTER_TAG, CardsPageReaction.PARAMS_CANCEL_CARD, blockCardJSON)
            busManager.publishData(tag: CardsPageReaction.ROUTER_TAG, CardsPageReaction.PARAMS_CANCEL_CARD_BO, cardBO)
            busManager.navigateScreen(tag: CardsPageReaction.ROUTER_TAG, CardsPageReaction.EVENT_NAV_TO_CANCEL_CARD, Void())

        }
    }

    private func blockCard(withCardBO cardBO: CardBO) {

        var blockCardTranport = BlockCardTransport(id: cardBO.cardId, cardNumber: String(cardBO.number.suffix(4)), alias: cardBO.cardAlias())

        if let url = cardBO.imageFront {
            blockCardTranport.imageList = [CardImage(url: url)]
        }

        if let blockCardJSON = blockCardTranport.toJSON() {

            let cardOperationInfo = createCardOperationInfo(forCardBO: cardBO, operation: .block)

            if let cardOperationInfo = cardOperationInfo {
                busManager.publishData(tag: CardsPageReaction.ROUTER_TAG, CardsPageReaction.PARAMS_OPERATION_CARD_INFO, cardOperationInfo)
            }

            busManager.publishData(tag: CardsPageReaction.ROUTER_TAG, CardsPageReaction.PARAMS_BLOCK_CARD, blockCardJSON)
            busManager.publishData(tag: CardsPageReaction.ROUTER_TAG, CardsPageReaction.PARAMS_CANCEL_CARD_BO, cardBO)
            busManager.navigateScreen(tag: CardsPageReaction.ROUTER_TAG, CardsPageReaction.EVENT_NAV_TO_BLOCK_CARD, Void())
        }
    }

    func generateDigitalCard(withCardBO cardBO: CardBO?) {

        guard let cardBO = cardBO, let cardsList = cards?.cards else {
            return
        }

        var cardOperationInfo: CardOperationInfo?

        let withVirtualCardBO = cardBO.associatedVirtualCardBO(fromCards: cardsList)

        if let withVirtualCardBO = withVirtualCardBO {

            cardOperationInfo = CardOperationInfo(cardID: withVirtualCardBO.cardId, fisicalCardID: cardBO.cardId, cardType: .digital, cardOperation: .generate)
        } else {

            cardOperationInfo = CardOperationInfo(cardID: cardBO.cardId, fisicalCardID: cardBO.isNormalPlastic() ? cardBO.cardId : nil, cardType: .digital, cardOperation: .generate)
        }

        let imageListBO = GenerateCardImageListBO()
        imageListBO.url = cardBO.imageFront

        let generateCardBO = GenerateCardBO()
        generateCardBO.cardId = cardBO.cardId
        generateCardBO.cardNumber = cardBO.number
        generateCardBO.cardHolder = cardBO.holderName
        generateCardBO.cardType = CardTypeBO.stringCardType(cardType: cardBO.cardType.id)
        generateCardBO.imageList = Array()
        generateCardBO.imageList?.append(imageListBO)

        if let cardOperationInfo = cardOperationInfo {
            busManager.publishData(tag: CardsPageReaction.ROUTER_TAG, CardsPageReaction.PARAMS_OPERATION_CARD_INFO, cardOperationInfo)
        }

        busManager.publishData(tag: CardsPageReaction.ROUTER_TAG, CardsPageReaction.PARAMS_GENERATE_CARD, generateCardBO.toJSON()!)

        busManager.navigateScreen(tag: CardsPageReaction.ROUTER_TAG, CardsPageReaction.EVENT_NAV_TO_GENERATE_CARD, Void())
    }

    func createCardOperationInfo(forCardBO cardBO: CardBO?, operation: CardOperation) -> CardOperationInfo? {

        guard let cardBO = cardBO, let cardsList = cards?.cards else {
            return nil
        }
        var asociatedCardBO: CardBO?
        if !cardBO.isNormalPlastic() {
            asociatedCardBO = cardBO.associatedCard(fronCardList: cardsList, type: .normalPlastic)
        }

        let cardOperationInfo = CardOperationInfo(cardID: cardBO.cardId, fisicalCardID: asociatedCardBO?.cardId, cardType: CardDataType.selectCardDataType(forCardBO: cardBO), cardOperation: operation)

        return cardOperationInfo
    }

    func pressedOperativeButton(withDisplayData displayData: ButtonBarDisplayData, cardBO: CardBO) {

        if displayData.optionKey == .limits {
            busManager.navigateScreen(tag: routerTag, CardsPageReaction.EVENT_NAV_TO_FOUR_SCREEN, cardBO)
        } else if displayData.optionKey == .cancel {
            cancelCard(withCardBO: cardBO)
        } else if displayData.optionKey == .block {
            blockCard(withCardBO: cardBO)
        }
    }

    func pressedOperativeButton(atIndex index: Int, withDisplayData displayData: ButtonBarDisplayData) {

        if let cardBO = cards?.cards[index] {
            pressedOperativeButton(withDisplayData: displayData, cardBO: cardBO)
        }

    }

    override func setModel(model: Model) {
        if let cards = model as? CardsBO {
            self.cards = cards
        } else if let cardsTransport = model as? CardsTransport, let cardBO = cardsTransport.cardBO {
            showCardId = cardBO.cardId
        }
    }

    func navigateToCVVScreen(withCardId cardId: String) {
        busManager.navigateScreen(tag: routerTag, CardsPageReaction.EVENT_NAV_TO_CARD_CVV_SCREEN, CVVTransport(with: cardId))
    }

    func rightCardButtonPressed(atIndex index: Int) {
        if let cardsConut = cards?.cards.count, let inboundIndex = (index < cardsConut) ? index : nil, let cardID = cards?.cards[inboundIndex].cardId {
            self.navigateToCVVScreen(withCardId: cardID)
        } else {
            errorBO = ErrorBO(message: Localizables.common.key_common_generic_error_text, errorType: .warning, allowCancel: false)
            checkError()
        }
    }

    func neededCardsRefresh() {

        showLoading()

        needRefreshPagerView = true

        interactor.provideCards(user: cards?.userBO)
            .subscribe(onSuccess: { [weak self]  cards in
                self?.busManager.publishData(tag: ShoppingCrossReaction.TAG, PageReactionConstants.PARAMS_CARDS, cards)
                self?.hideLoading()
                },
                       onError: { [weak self] _ in
                        self?.hideLoading()
            })
            .disposed(by: disposeBag)
    }

    func cardOperationSucess(info: CardOperationInfo) {
        cardOperationInfo = info
    }

    func changeCardStatus(withCardStatus status: Bool, andCard card: CardBO) {
        switchCardUp(with: status, and: card)
    }
    
    func switchCardUp(with status: Bool, and card: CardBO) {
        showLoading()
        interactor.provideUpdatedCard(withCardId: card.cardId, withStatus: !status).subscribe(
            onNext: { [weak self] result in

                if let activationsBO = result as? ActivationsBO {
                    self?.activations = activationsBO
                    self?.hideLoading(completion: {
                        self?.getUpdatedCard(withCard: card, withStatus: status, fromCells: false)
                        let cardModel = CardModel(cardBO: card)
                        BusManager.sessionDataInstance.publishData(tag: CardsPageReaction.ROUTER_TAG, CardsPageReaction.EVENT_CARD_UPDATED, cardModel)
                    })
                }

            },
            onError: { [weak self] error in
                DLog(message: "\(error)")

                self?.view?.switchInitialStateAfterReceiveError(withSwitchStatus: status)
                if let evaluate = error as? ServiceError {
                    switch evaluate {

                    case .GenericErrorBO(let errorBO):
                        self?.errorBO = errorBO
                    default:
                        break
                    }
                }

                self?.hideLoading(completion: {
                    if self?.errorBO?.status != 401 && self?.errorBO?.code != "70" {
                        self?.checkError()
                    }
                })
            }).addDisposableTo(disposeBag)
    }
    
    func onOffViewHelpButtonPressed(forCard cardBO: CardBO?) {
        if let cardBO = cardBO {
            let cardTransport = CardsTransport(cardBO: cardBO)
            busManager.navigateScreen(tag: CardsPageReaction.ROUTER_TAG, CardsPageReaction.EVENT_NAV_TO_ON_OFF_HELP, cardTransport)
        } else {
            busManager.navigateScreen(tag: CardsPageReaction.ROUTER_TAG, CardsPageReaction.EVENT_NAV_TO_ON_OFF_HELP, EmptyModel() )
        }
    }

}

extension CardsPresenter: CardsPresenterProtocol {
    func updateCard(withCardStatus status: Bool, andCard card: CardBO) {
        getUpdatedCard(withCard: card, withStatus: status, fromCells: true)

    }

    func getHelp() {
        if !ConfigPublicManager.sharedInstance().getConfigBOBySection(forKey: Constants.SECTION_TO_SHOW_CARDS).isEmpty {
            view?.showHelp(withDisplayData: createHelpDisplayData())
        }
    }

    func helpButtonPressed(withHelpId helpId: String) {
        busManager.navigateScreen(tag: CardsPageReaction.ROUTER_TAG, CardsPageReaction.EVENT_NAV_TO_DETAIL_HELP, HelpDetailTransport(helpId))
    }

    func updateDisplayItems(withCardData cardData: DisplayCardData) {

        let countArray: Int = (displayItems.items?.count)!

        for i in 0 ..< countArray {
            let tempDictionary = displayItems.items?[i]
            if tempDictionary?.displayCardData?.cardBO?.cardId == cardData.cardBO?.cardId {
                displayItems.items?[i].displayCardData = cardData
            }
        }

    }
}

extension CardsPresenter: TransactionPresenterDelegate {

    func transactionPresenter(_ transactionPresenter: TransactionPresenter, error: ErrorBO) {
        errorBO = error
        if error.sessionExpired() {
            checkError()
        }
    }

    func transactionPresenter(_ transactionPresenter: TransactionPresenter, loadedTransactions transactions: TransactionsBO, forCardId cardId: String) {
        guard let cards = cards?.cards else {
            return
        }

        let cardBOFiltered = cards.filter { $0.cardId == cardId }
        if cardBOFiltered.count == 1 {
            cardBOFiltered[0].transactions = transactions
        }
    }

}
