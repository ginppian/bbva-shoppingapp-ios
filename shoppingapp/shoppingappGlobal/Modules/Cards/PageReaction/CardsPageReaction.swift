//
//  CardsPageReaction.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 26/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

import CellsNativeCore
import CellsNativeComponents

class CardsPageReaction: PageReaction {

    static let ROUTER_TAG: String = "cards-tag"

    //Channels

    static let CARD_UPDATED_CHANNEL = "CARD_UPDATED_CHANNEL"

    static let PAGE_LOADED_CHANNEL = "on_page_loaded_channel"

    static let ACTIVATE_CARD_NUMBER_CHANNEL = "ac_card_number_channel"

    static let TERMS_CONDITIONS_CARD_NUMBER_CHANNEL = "tcc_card_number_channel"

    static let CANCEL_CARD_INFORMATION_CHANNEL = "cci_card_information_channel"

    static let BLOCK_CARD_INFORMATION_CHANNEL = "cbi_card_information_channel"

    static let BLOCK_TYPE_INFORMATION_CHANNEL = "cbt_card_information_channel"

    static let CARD_BO_CHANNEL = "card_bo_channel"

    static let NEEDED_CARDS_REFRESH_CHANNEL: Channel<Void> = Channel(name: "NEEDED_CARDS_REFRESH_CHANNEL")

    static let OPERATION_CARD_INFO_CHANNEL: Channel<CardOperationInfo> = Channel(name: "OPERATION_CARD_INFO_CHANNEL")

    static let OPERATION_CARD_INFO_SUCCESS_CHANNEL: Channel<CardOperationInfo> = Channel(name: "OPERATION_CARD_INFO_SUCCESS_CHANNEL")

    // CELLS PAGES

    static let CELLS_PAGE_ACTIVATE_CARD = "cardActivate"

    static let CELLS_PAGE_GENERATE_CARD = "cardGenerate"

    static let CELLS_PAGE_CARD_CANCEL_INFO = "cardBlockDigital"

    static let CELLS_PAGE_CARD_BLCOK_INFO =  "cardBlockNormal"

    //Events

    //----Native

    static let EVENT_NAV_TO_NEXT_SCREEN: ActionSpec<Model> = ActionSpec<Model>(id: "EVENT_NAV_TO_NEXT_SCREEN")

    static let EVENT_NAV_TO_SECOND_SCREEN: ActionSpec<Model> = ActionSpec<Model>(id: "EVENT_NAV_TO_SECOND_SCREEN")

    static let EVENT_NAV_TO_THIRD_SCREEN: ActionSpec<Model> = ActionSpec<Model>(id: "EVENT_NAV_TO_THIRD_SCREEN")

    static let EVENT_NAV_TO_FOUR_SCREEN: ActionSpec<Model> = ActionSpec<Model>(id: "EVENT_NAV_TO_FOUR_SCREEN")

    static let EVENT_NAV_TO_ON_OFF_HELP: ActionSpec<Model> = ActionSpec<Model>(id: "EVENT_NAV_TO_ON_OFF_HELP")

    static let EVENT_NAV_TO_CARD_CVV_SCREEN: ActionSpec<Model> = ActionSpec<Model>(id: "EVENT_NAV_TO_CARD_CVV_SCREEN")

    static let EVENT_NAV_TO_DETAIL_HELP: ActionSpec<Model> = ActionSpec<Model>(id: "EVENT_NAV_TO_DETAIL_HELP")

    static let EVENT_CARD_UPDATED: ActionSpec<Any> = ActionSpec<Any>(id: "EVENT_CARD_UPDATED")

    static let PARAMS_CANCEL_CARD_BO: ActionSpec<Model> = ActionSpec<Model>(id: "PARAMS_CANCEL_CARD_BO")

    static let NEEDED_CARDS_REFRESH: ActionSpec<Void> = ActionSpec<Void>(id: "NEEDED_CARDS_REFRESH")

    static let PARAMS_OPERATION_CARD_INFO: ActionSpec<CardOperationInfo> = ActionSpec<CardOperationInfo>(id: "PARAMS_OPERATION_CARD_INFO")

    static let PARAMS_OPERATION_CARD_INFO_SUCCESS: ActionSpec<CardOperationInfo> = ActionSpec<CardOperationInfo>(id: "PARAMS_OPERATION_CARD_INFO_SUCCESS")

    //----Cells

    static let PARAMS_ACTIVATE_CARD: ActionSpec<Any> = ActionSpec<Any>(id: "PARAMS_ACTIVATE_CARD")

    static let EVENT_NAV_TO_ACTIVATE_CARD: ActionSpec<Void> = ActionSpec<Void>(id: "EVENT_NAV_TO_ACTIVATE_CARD")

    static let PARAMS_GENERATE_CARD: ActionSpec<Any> = ActionSpec<Any>(id: "PARAMS_GENERATE_CARD")

    static let EVENT_NAV_TO_GENERATE_CARD: ActionSpec<Void> = ActionSpec<Void>(id: "EVENT_NAV_TO_GENERATE_CARD")

    static let PARAMS_CANCEL_CARD: ActionSpec<Any> = ActionSpec<Any>(id: "PARAMS_CANCEL_CARD")

    static let EVENT_NAV_TO_CANCEL_CARD: ActionSpec<Void> = ActionSpec<Void>(id: "EVENT_NAV_TO_CANCEL_CARD")

    static let PARAMS_BLOCK_CARD: ActionSpec<Any> = ActionSpec<Any>(id: "PARAMS_BLOCK_CARD")

    static let EVENT_NAV_TO_BLOCK_CARD: ActionSpec<Void> = ActionSpec<Void>(id: "EVENT_NAV_TO_BLOCK_CARD")

    static let CHANNEL_SELECT_CARD_ACTIVATE: Channel<Any> = Channel(name: "channel-select-card-activate")

    static let PARAMS_SELECT_CARD_ACTIVATE: ActionSpec<Any> = ActionSpec<Any>(id: "PARAMS_SELECT_CARD_ACTIVATE")

    override func configure() {

        //----Native

        navigateFirstScreenFromCards()

        navigateSecondScreenFromCards()

        navigateThirdScreenFromCards()

        navigateFourScreenFromCards()

        navigateOnOffCard()

        navigateCardCVVScreenFromCards()

        registerForNavigateToHelpDetail()

        updateCardChannel()

        updateCurrentCard()

        //----Cells

        paramsActivateCard()

        navigateActivateCard()

        paramsGenerateCard()

        navigateGenerateCard()

        paramsCancelCard()

        navigateCancelCard()

        paramsBlockCard()

        navigateBlockCard()

        onPageCellsLoaded()

        reactCards()

        updatedCard()

        registerToReactToNeededCardsRefreshChannel()
        registerToWriteWhenCardsNeedRefresh()

        paramsOperationBlockCard()

        registerToReactToCardOperationInfoSuccess()
    }

    // MARK: - Native -
    func updatedCard() {
        let channel: Channel = Channel<Any>(name: CardsPageReaction.CARD_UPDATED_CHANNEL)
        _ = reactTo(channel: channel)?
            .then()
            .inside(componentID: CardsViewController.ID, component: CardsViewController.self)
            .doReaction(reactionExecution: { component, param in
                if let card = param as? CardModel {
                    component.changeCardStatusFromCells(withCardStatus: !card.cardBO.isCardOn(), andCard: card.cardBO)
    }
            })
    }

    func reactCards() {

        _ = reactTo(channel: PageReactionConstants.CHANNEL_CARDS)?
            .then()
            .inside(componentID: CardsPageReaction.ROUTER_TAG, component: CardsPresenter<CardsViewController>.self)
            .doReaction { component, param in

                if let param = param {
                    component.receive(model: param)
                }
            }
    }

    func navigateFirstScreenFromCards() {

        _ = when(id: CardsPageReaction.ROUTER_TAG, type: Model.self)
            .doAction(actionSpec: CardsPageReaction.EVENT_NAV_TO_NEXT_SCREEN)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, model in

                if let model = model {
                    router.navigateFirstScreenFromCards(withModel: model)
                }

            }
    }

    func navigateSecondScreenFromCards() {

        _ = when(id: CardsPageReaction.ROUTER_TAG, type: Model.self)
            .doAction(actionSpec: CardsPageReaction.EVENT_NAV_TO_SECOND_SCREEN)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, model in

                if let model = model {
                    router.navigateSecondScreenFromCards(withModel: model)
                }

            }
    }

    func navigateThirdScreenFromCards() {

        _ = when(id: CardsPageReaction.ROUTER_TAG, type: Model.self)
            .doAction(actionSpec: CardsPageReaction.EVENT_NAV_TO_THIRD_SCREEN)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, model in

                if let model = model {
                    router.navigateThirdScreenFromCards(withModel: model)
                }

            }
    }

    func navigateFourScreenFromCards() {

        _ = when(id: CardsPageReaction.ROUTER_TAG, type: Model.self)
            .doAction(actionSpec: CardsPageReaction.EVENT_NAV_TO_FOUR_SCREEN)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, model in

                if let model = model {
                    router.navigateFourScreenFromCards(withModel: model)
                }

            }
    }

    func navigateOnOffCard() {

        _ = when(id: CardsPageReaction.ROUTER_TAG, type: Model.self)
            .doAction(actionSpec: CardsPageReaction.EVENT_NAV_TO_ON_OFF_HELP)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, model in
                if let model = model {
                    router.navigateToOnOffHelp(model: model)
                }
            }
    }

    func navigateCardCVVScreenFromCards() {

        _ = when(id: CardsPageReaction.ROUTER_TAG, type: Model.self)
            .doAction(actionSpec: CardsPageReaction.EVENT_NAV_TO_CARD_CVV_SCREEN)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, model in

                if let model = model {
                    router.navigateCardCVVScreenFromCards(model: model)
                }
            }
    }

    func registerForNavigateToHelpDetail() {

        _ = when(id: CardsPageReaction.ROUTER_TAG, type: Model.self)
            .doAction(actionSpec: CardsPageReaction.EVENT_NAV_TO_DETAIL_HELP)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, model in

                if let model = model {
                    router.navigateToDetailHelp(withModel: model)
                }
            }
    }

    func updateCardChannel() {

        let channel: Channel = Channel<Any>(name: CardsPageReaction.CARD_UPDATED_CHANNEL)

        _ = writeOn(channel: channel)
            .when(componentID: CardsPageReaction.ROUTER_TAG)
            .doAction(actionSpec: CardsPageReaction.EVENT_CARD_UPDATED)
    }

    func updateCurrentCard() {

        let channel: Channel = Channel<Model>(name: CardsPageReaction.CARD_BO_CHANNEL)

        _ = writeOn(channel: channel)
            .when(componentID: CardsPageReaction.ROUTER_TAG)
            .doAction(actionSpec: CardsPageReaction.PARAMS_CANCEL_CARD_BO)
    }

    // MARK: - Cells -

    func onPageCellsLoaded() {

        let channelOnPageLoaded: Channel = Channel<String>(name: CardsPageReaction.PAGE_LOADED_CHANNEL)

        _ = writeOn(channel: channelOnPageLoaded)
            .when(componentID: WebController.ID)
            .doAction(actionSpec: WebController.ON_PAGE_LOADED)
    }

    // MARK: - Need Refresh Cards
    func registerToReactToNeededCardsRefreshChannel () {

        _ = reactTo(channel: CardsPageReaction.NEEDED_CARDS_REFRESH_CHANNEL)?
            .then()
            .inside(componentID: CardsPageReaction.ROUTER_TAG, component: CardsPresenter<CardsViewController>.self)
            .doReactionAndClearChannel(reactionExecution: { presenter, _ in
                presenter.neededCardsRefresh()
            })
    }

    func registerToWriteWhenCardsNeedRefresh () {

        _ = writeOn(channel: CardsPageReaction.NEEDED_CARDS_REFRESH_CHANNEL)
            .when(componentID: CardsPageReaction.ROUTER_TAG)
            .doAction(actionSpec: CardsPageReaction.NEEDED_CARDS_REFRESH)
    }
    
    func paramsOperationBlockCard() {
        
        _ = writeOn(channel: CardsPageReaction.OPERATION_CARD_INFO_CHANNEL)
            .when(componentID: CardsPageReaction.ROUTER_TAG)
            .doAction(actionSpec: CardsPageReaction.PARAMS_OPERATION_CARD_INFO)
    }
    
    func registerToReactToCardOperationInfoSuccess () {
        
        _ = reactTo(channel: CardsPageReaction.OPERATION_CARD_INFO_SUCCESS_CHANNEL)?
            .then()
            .inside(componentID: CardsPageReaction.ROUTER_TAG, component: CardsPresenter<CardsViewController>.self)
            .doReactionAndClearChannel(reactionExecution: { presenter, cardOperationInfo in
                if let cardOperationInfo = cardOperationInfo {
                    presenter.cardOperationSucess(info: cardOperationInfo)
                }
            })
    }

    // MARK: - Activate Card

    func paramsActivateCard() {

        let channel: JsonChannel = JsonChannel(CardsPageReaction.ACTIVATE_CARD_NUMBER_CHANNEL)

        _ = writeOn(channel: channel)
            .when(componentID: CardsPageReaction.ROUTER_TAG)
            .doAction(actionSpec: CardsPageReaction.PARAMS_ACTIVATE_CARD)

        _ = writeOn(channel: CardsPageReaction.CHANNEL_SELECT_CARD_ACTIVATE)
            .when(componentID: CardsPageReaction.ROUTER_TAG)
            .doAction(actionSpec: CardsPageReaction.PARAMS_SELECT_CARD_ACTIVATE)

    }

    func navigateActivateCard() {

        _ = when(id: CardsPageReaction.ROUTER_TAG, type: Void.self)
            .doAction(actionSpec: CardsPageReaction.EVENT_NAV_TO_ACTIVATE_CARD)
            .then()
            .doWebNavigation(webRoute: CardsPageReaction.CELLS_PAGE_ACTIVATE_CARD, nativeRoute: Route(viewController: { () -> UIViewController in
                return (BusManager.sessionDataInstance.routerFactory?.getViewController(route: CardActivationViewController.ID))!
            }, animationToNext: { _, destinationViewController in

                destinationViewController?.modalPresentationStyle = .overCurrentContext

                if let destinationViewController = destinationViewController {
                    let navigationController = UINavigationController(rootViewController: destinationViewController)
                    BusManager.sessionDataInstance.present(viewController: navigationController, animated: true, completion: nil)
                }

            }, animationToPrevious: { _, _ in

            }
            ))
    }

    func paramsGenerateCard() {

        let channel: JsonChannel = JsonChannel(CardsPageReaction.TERMS_CONDITIONS_CARD_NUMBER_CHANNEL)

        _ = writeOn(channel: channel)
            .when(componentID: CardsPageReaction.ROUTER_TAG)
            .doAction(actionSpec: CardsPageReaction.PARAMS_GENERATE_CARD)
    }

    func navigateGenerateCard() {

        _ = when(id: CardsPageReaction.ROUTER_TAG, type: Void.self)
            .doAction(actionSpec: CardsPageReaction.EVENT_NAV_TO_GENERATE_CARD)
            .then()
            .doWebNavigation(webRoute: CardsPageReaction.CELLS_PAGE_GENERATE_CARD, nativeRoute: Route(viewController: { () -> UIViewController in
                return (BusManager.sessionDataInstance.routerFactory?.getViewController(route: CardGenerationViewController.ID))!
            }, animationToNext: { _, destinationViewController in

                destinationViewController?.modalPresentationStyle = .overCurrentContext

                if let destinationViewController = destinationViewController {
                    BusManager.sessionDataInstance.present(viewController: destinationViewController, animated: true, completion: nil)
                }

            }, animationToPrevious: { _, _ in

            }
            ))
    }

    // MARK: - Cancel Card

    func paramsCancelCard() {

        let channel: JsonChannel = JsonChannel(CardsPageReaction.CANCEL_CARD_INFORMATION_CHANNEL)

        _ = writeOn(channel: channel)
            .when(componentID: CardsPageReaction.ROUTER_TAG)
            .doAction(actionSpec: CardsPageReaction.PARAMS_CANCEL_CARD)
    }

    func navigateCancelCard() {

        _ = when(id: CardsPageReaction.ROUTER_TAG, type: Void.self)
            .doAction(actionSpec: CardsPageReaction.EVENT_NAV_TO_CANCEL_CARD)
            .then()
            .doWebNavigation(webRoute: CardsPageReaction.CELLS_PAGE_CARD_CANCEL_INFO, nativeRoute: Route(viewController: { () -> UIViewController in
                return (BusManager.sessionDataInstance.routerFactory?.getViewController(route: CardCancelationViewController.ID))!
            }, animationToNext: { _, destinationViewController in

                destinationViewController?.modalPresentationStyle = .overCurrentContext

                if let destinationViewController = destinationViewController {
                    let navigationController = UINavigationController(rootViewController: destinationViewController)
                    BusManager.sessionDataInstance.present(viewController: navigationController, animated: true, completion: nil)
                }

            }, animationToPrevious: { _, _ in

            }
            ))
    }

    // MARK: - Block Card

    func paramsBlockCard() {

        let channelBlockInfo: JsonChannel = JsonChannel(CardsPageReaction.BLOCK_CARD_INFORMATION_CHANNEL)

        _ = writeOn(channel: channelBlockInfo)
            .when(componentID: CardsPageReaction.ROUTER_TAG)
            .doAction(actionSpec: CardsPageReaction.PARAMS_BLOCK_CARD)

        let channelBlockType: JsonChannel = JsonChannel(CardsPageReaction.BLOCK_TYPE_INFORMATION_CHANNEL)

        _ = writeOn(channel: channelBlockType)
            .when(componentID: CardsPageReaction.ROUTER_TAG)
            .doAction(actionSpec: CardsPageReaction.PARAMS_BLOCK_CARD)
    }

    func navigateBlockCard() {

        _ = when(id: CardsPageReaction.ROUTER_TAG, type: Void.self)
            .doAction(actionSpec: CardsPageReaction.EVENT_NAV_TO_BLOCK_CARD)
            .then()
            .doWebNavigation(webRoute: CardsPageReaction.CELLS_PAGE_CARD_BLCOK_INFO, nativeRoute: Route(viewController: { () -> UIViewController in
                return (BusManager.sessionDataInstance.routerFactory?.getViewController(route: CardBlockViewController.ID))!
            }, animationToNext: { _, destinationViewController in

                destinationViewController?.modalPresentationStyle = .overCurrentContext

                if let destinationViewController = destinationViewController {
                    let navigationController = UINavigationController(rootViewController: destinationViewController)
                    BusManager.sessionDataInstance.present(viewController: navigationController, animated: true, completion: nil)
                }

            }, animationToPrevious: { _, _ in

            }
            ))
    }
}
