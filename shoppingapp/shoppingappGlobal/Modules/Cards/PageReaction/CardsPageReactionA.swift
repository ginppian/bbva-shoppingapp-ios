//
//  CardsPageReactionA.swift
//  shoppingappMX
//
//  Created by daniel.petrovic on 29/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeComponents
import CellsNativeCore

final class CardsPageReactionA: CardsPageReaction {
    
    override func configure() {
        super.configure()
        switchCardUpCallBack()
    }

    //EVENTS

    static let PARAMS_ACTIVATE_WITH_CVV_CARD: ActionSpec<Any> = ActionSpec<Any>(id: "PARAMS_ACTIVATE_WITH_CVV_CARD")
    static let EVENT_NAV_TO_ACTIVATE_WITH_CVV_CARD: ActionSpec<Void> = ActionSpec<Void>(id: "EVENT_NAV_TO_ACTIVATE_WITH_CVV_CARD")

    //Channels

    static let ACTIVATE_WITH_CVV_CARD_NUMBER_CHANNEL = "acbc_info_channel"

    // CELLS PAGES

    static let CELLS_PAGE_ACTIVATE_CARD_BY_CVV = "cardActivateByCvv"

    override func navigateCardCVVScreenFromCards() {
        _ = when(id: CardsPageReactionA.ROUTER_TAG, type: Model.self)
            .doAction(actionSpec: CardsPageReactionA.EVENT_NAV_TO_CARD_CVV_SCREEN)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, model in
                guard let `model` = model else {
                    return
                }
                router.navigateCardCVVScreenFromCards(model: model)
            }
    }
    
    func switchCardUpCallBack() {
        
        _ = reactTo(channel: OnOffHelpPageReactionA.INFO_SWITCH_CARD_UP_CHANNEL)?
            .then()
            .inside(componentID: CardsPageReactionA.ROUTER_TAG, component: CardsPresenterA<CardsViewControllerA>.self)
            .doReactionAndClearChannel { component, _ in
                component.switchCardStatus()
        }
    }

    override func reactCards() {

        _ = reactTo(channel: PageReactionConstants.CHANNEL_CARDS)?
            .then()
            .inside(componentID: CardsPageReaction.ROUTER_TAG, component: CardsPresenterA<CardsViewControllerA>.self)
            .doReaction { component, param in

                if let param = param {
                    component.receive(model: param)
                }
            }
    }

    override func registerToReactToNeededCardsRefreshChannel () {

        _ = reactTo(channel: CardsPageReaction.NEEDED_CARDS_REFRESH_CHANNEL)?
            .then()
            .inside(componentID: CardsPageReaction.ROUTER_TAG, component: CardsPresenterA<CardsViewControllerA>.self)
            .doReactionAndClearChannel(reactionExecution: { presenter, _ in
                presenter.neededCardsRefresh()
            })
    }

    override func registerToReactToCardOperationInfoSuccess () {

        _ = reactTo(channel: CardsPageReaction.OPERATION_CARD_INFO_SUCCESS_CHANNEL)?
            .then()
            .inside(componentID: CardsPageReaction.ROUTER_TAG, component: CardsPresenterA<CardsViewControllerA>.self)
            .doReactionAndClearChannel(reactionExecution: { controller, cardOperationInfo in
                if let cardOperationInfo = cardOperationInfo {
                    controller.cardOperationSucess(info: cardOperationInfo)
                }
            })
    }

    override func navigateActivateCard() {

        _ = when(id: CardsPageReactionA.ROUTER_TAG, type: Void.self)
            .doAction(actionSpec: CardsPageReactionA.EVENT_NAV_TO_ACTIVATE_WITH_CVV_CARD)
            .then()
            .doWebNavigation(webRoute: CardsPageReactionA.CELLS_PAGE_ACTIVATE_CARD_BY_CVV, nativeRoute: Route(viewController: { () -> UIViewController in
                return (BusManager.sessionDataInstance.routerFactory?.getViewController(route: CardActivationByCVVViewController.id))!
            }, animationToNext: { _, destinationViewController in

                destinationViewController?.modalPresentationStyle = .overCurrentContext

                if let destinationViewController = destinationViewController {
                    let navigationController = UINavigationController(rootViewController: destinationViewController)
                    BusManager.sessionDataInstance.present(viewController: navigationController, animated: true, completion: nil)
                }

            }, animationToPrevious: { _, _ in

            }
            ))
    }

    override func paramsActivateCard() {

        let channel: JsonChannel = JsonChannel(CardsPageReactionA.ACTIVATE_WITH_CVV_CARD_NUMBER_CHANNEL)

        _ = writeOn(channel: channel)
            .when(componentID: CardsPageReaction.ROUTER_TAG)
            .doAction(actionSpec: CardsPageReactionA.PARAMS_ACTIVATE_WITH_CVV_CARD)

        _ = writeOn(channel: CardsPageReaction.CHANNEL_SELECT_CARD_ACTIVATE)
            .when(componentID: CardsPageReaction.ROUTER_TAG)
            .doAction(actionSpec: CardsPageReaction.PARAMS_SELECT_CARD_ACTIVATE)

    }
}
