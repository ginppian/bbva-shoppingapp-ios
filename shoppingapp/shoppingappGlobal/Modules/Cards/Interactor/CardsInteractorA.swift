//
//  CardsInteractorA.swift
//  shoppingappMX
//
//  Created by jesus.martinez on 13/12/17.
//  Copyright © 2017 BBVA. All rights reserved.
//
import RxSwift

class CardsInteractorA: CardsInteractor {

    override func provideCards(user: UserBO?) -> Single<ModelBO> {

        return Single<ModelBO>.create { single in

            CardsDataManager.sharedInstance.retrieveFinancialOverview().subscribe(
                onNext: { result in

                    if let financialOverviewEntity = result as? FinancialOverviewEntity {

                        guard let user = user else {
                            single(.error(ServiceError.GenericErrorBO(error: ErrorBO(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))))
                            return
                        }

                        single(.success(CardsBO(financialOverviewEntity: financialOverviewEntity, user: user)))
                    } else {
                        single(.error(ServiceError.GenericErrorBO(error: ErrorBO(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))))
                        DLog(message: "Incorrect FinancialOverviewEntity")
                    }
                },
                onError: { error in

                    let evaluate: ServiceError = (error as? ServiceError)!

                    switch evaluate {

                    case .GenericErrorEntity(let errorEntity):
                        let errorBVABO = ErrorBO(error: errorEntity)
                        single(.error(ServiceError.GenericErrorBO(error: errorBVABO)))
                    default:
                        single(.error(evaluate))
                    }

            },
                onCompleted: {
            }).addDisposableTo(self.disposeBag)

            return Disposables.create()
        }
    }
}
