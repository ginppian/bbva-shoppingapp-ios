//
//  BVATransactionsListInteractor.swift
//  shoppingapp
//
//  Created by jesus.martinez on 7/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation
import RxSwift

class CardsInteractor: CardsInteractorProtocol {

    var onOffDataManager = OnOffDataManager.sharedInstance
    var cryptoDataManager = LibCryptoDataManager.sharedInstance

    let disposeBag = DisposeBag()

    func provideUpdatedCard(withCardId cardId: String, withStatus status: Bool) -> Observable <ModelBO> {
        return Observable.create { observer in
            self.onOffDataManager.provideUpdatedCardStatus(forCardId: cardId, andStatus: status).subscribe(
                onNext: { result in
                    if let activationsEntity = result as? ActivationsEntity {
                        observer.onNext(ActivationsBO(activationsEntity: activationsEntity))
                        observer.onCompleted()
                    } else { DLog(message: "Incorrect ActivationsEntity") }
                },
                onError: { error in
                    observer.onError(Tools.evaluateError(with: error))
                }).addDisposableTo(self.disposeBag)
            return Disposables.create()
        }
    }

    func provideCards(user: UserBO? = nil) -> Single<ModelBO> {

        return Single<ModelBO>.create { single in

            CardsDataManager.sharedInstance.provideCards().subscribe(
                onNext: { result in

                    if let cardsEntity = result as? CardsEntity {
                        single(.success(CardsBO(cardsEntity: cardsEntity)))
                    }
                },
                onError: { error in
                    single(.error(Tools.evaluateError(with: error)))
                },
                onCompleted: {
            }).addDisposableTo(self.disposeBag)

            return Disposables.create()
        }
    }
}
