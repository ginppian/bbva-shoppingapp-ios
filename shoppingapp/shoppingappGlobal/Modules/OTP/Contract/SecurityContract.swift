//
//  SecurityContract.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 27/12/2017.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit
import RxSwift

protocol SecurityViewProtocol: ViewProtocol {

    func showSecurityData(withSecurityDisplayItems displayItems: SecurityDisplayData)
    func clearSecurityCodeInput(value: String)

    func updateTimer(withText text: String)
    func startTimer()
    func finishTimer()

    func enableConfirm()
    func disableConfirm()

}
protocol SecurityPresenterProtocol: PresenterProtocol {

    func getSecurityData()
    func clearText()
    func shouldChangeCharacters(textValue text: String, withNewText newText: String, inRange range: NSRange) -> Bool
    func updateTimer()
    func codeWrote(text: String)
    func requestNewPin()
    func replayServiceSecurity(withHeader header: [String: String], andSecurityCode securityCode: String)

}
