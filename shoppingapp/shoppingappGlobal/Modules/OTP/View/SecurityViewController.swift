//
//  SecurityViewController.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 27/12/2017.
//  Copyright © 2017 BBVA. All rights reserved.
//
import CellsNativeCore
import CellsNativeComponents
import UIKit
import IQKeyboardManagerSwift

class SecurityViewController: BaseViewController {

    static let ID: String = String(describing: SecurityViewController.self)

    static let top_height_opaque: CGFloat = 88
    static let top_height_translucent: CGFloat = 256

    static let request_pin_bottom_without_keyboard: CGFloat = 20
    static let request_pin_bottom_with_keyboard: CGFloat = 0

    var presenter: SecurityPresenterProtocol!

    // MARK: Outlets

    @IBOutlet weak var securityCodeView: UIView!
    @IBOutlet weak var separatorLineView: UIView!

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var requestSmsDescriptionLabel: UILabel!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var timerImageView: UIImageView!
    @IBOutlet weak var informationImageView: UIImageView!
    @IBOutlet weak var securityCodeTextField: CustomTextField!
    @IBOutlet weak var confirmButton: UIButton!
    @IBOutlet weak var requestNewPinButton: UIButton!

    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var topHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var topHeaderBackgroundImageView: UIImageView!
    @IBOutlet weak var topHeaderIlustrationImageView: UIImageView!

    @IBOutlet weak var requestPinBottomConstraint: NSLayoutConstraint!

    var timer = Timer()
    let letterSpacing = 17

    var responseHeader = [String: String]()

    override func viewDidLoad() {

        self.configure(appLifecycle: (UIApplication.shared.delegate as! AppDelegate).appLifeCycle!, id: SecurityViewController.ID)

        super.viewDidLoad()

        presenter.getSecurityData()

        configureView()

        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        notificationCenter.addObserver(self, selector: #selector(keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }

    override func viewWillAppear(_ animated: Bool) {

        super.viewWillAppear(animated)

        configureNavigationBar()

        BusManager.sessionDataInstance.disableSwipeLateralMenu()

        IQKeyboardManager.shared.enable = false

    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        timer.invalidate()

        IQKeyboardManager.shared.enable = true
    }

    func configureNavigationBar() {

        let image = SVGCache.image(forSVGNamed: ConstantsImages.Common.close_icon, color: .BBVAWHITE)

        setNavigationLeftButton(withImage: image, action: #selector(backAction))

        setNavigationTitle(withText: Localizables.common.key_security_title_text)

        setNavigationBarTransparent()
    }

    override func backAction() {

        BusManager.sessionDataInstance.securityInterceptorStore?.unLockStop()
        dismissAction()

        self.navigationController?.navigationBar.isTranslucent = true
    }

    @IBAction func confirmButtonClick(_ sender: UIButton) {

        if let securityCode = securityCodeTextField.text {
            presenter.replayServiceSecurity(withHeader: responseHeader, andSecurityCode: securityCode)
        }
    }

    func configureView() {

        securityCodeTextField.delegate = self
        securityCodeTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        securityCodeTextField.keyboardType = Settings.SecurityControllerGlobal.security_code_keyboard_type
        securityCodeTextField.delegate = self

        let clearTextButton = UIButton(frame: CGRect(x: 8, y: securityCodeTextField.frame.size.height / 2 - 8, width: 16, height: 16))
        clearTextButton.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Common.close_icon, color: .MEDIUMBLUE), width: 16, height: 16), for: .normal)
        clearTextButton.addTarget(self, action: #selector(clearCodeInput), for: .touchUpInside)

        let clearTextView = UIView(frame: CGRect(x: -clearTextButton.frame.size.width - 15, y: 0, width: clearTextButton.frame.size.width * 2, height: securityCodeTextField.frame.size.height))

        clearTextView.addSubview(clearTextButton)

        securityCodeView.backgroundColor = .BBVA100
        securityCodeTextField.rightView = clearTextView
        securityCodeTextField.rightViewMode = .whileEditing
        securityCodeTextField.configure()
        securityCodeTextField.placeholderActiveColor = .BBVA500
        securityCodeTextField.placeholderNormalColor = .BBVA500

        separatorLineView.backgroundColor = .BBVA500

        requestSmsDescriptionLabel.textColor = .BBVA600
        requestSmsDescriptionLabel.font = Tools.setFontBook(size: 14)
        requestSmsDescriptionLabel.textAlignment = .left

        timerLabel.textColor = .MEDIUMBLUE
        timerLabel.font = Tools.setFontBold(size: 14)
        timerLabel.textAlignment = .left

        confirmButton.isEnabled = false
        confirmButton.setBackgroundColor(color: .MEDIUMBLUE, forUIControlState: .normal)
        confirmButton.setBackgroundColor(color: .COREBLUE, forUIControlState: .highlighted)
        confirmButton.setBackgroundColor(color: .BBVA200, forUIControlState: .disabled)
        confirmButton.setTitleColor(.BBVAWHITE, for: .normal)
        confirmButton.titleLabel?.font = Tools.setFontBold(size: 14)

        requestNewPinButton.contentMode = .scaleAspectFit
        requestNewPinButton.setTitleColor(.MEDIUMBLUE, for: .normal)
        requestNewPinButton.setTitleColor(.COREBLUE, for: .highlighted)
        requestNewPinButton.setTitleColor(.BBVA200, for: .disabled)
        requestNewPinButton.titleLabel?.font = Tools.setFontBold(size: 14)
        requestNewPinButton.isEnabled = false
    }

    @objc func keyboardWillShow(_ notification: Notification) {

        showOpaqueNavigationBar()
        
        var keyboardHeight = CGRect.zero
        
        if let value = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            keyboardHeight = value.cgRectValue
        }
        
        requestPinBottomConstraint.constant = SecurityViewController.request_pin_bottom_with_keyboard + keyboardHeight.size.height
        
        animationWithKeyBoard(withNotification: notification)
    }

    @objc func keyboardWillHide(_ notification: Notification) {

        showTranslucentNavigationBar()
        
        requestPinBottomConstraint.constant = SecurityViewController.request_pin_bottom_without_keyboard
        
        animationWithKeyBoard(withNotification: notification)
    }

    func showOpaqueNavigationBar() {

        topHeightConstraint.constant = SecurityViewController.top_height_opaque
        topView.isHidden = true

        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.barTintColor = .DARKAQUA

    }

    func showTranslucentNavigationBar() {

        topHeightConstraint.constant = SecurityViewController.top_height_translucent
        topView.isHidden = false

        self.navigationController?.navigationBar.isTranslucent = true

    }

    func animationWithKeyBoard(withNotification notification: Notification) {

        let duration = (notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.doubleValue ?? 0.0
        let curve = (notification.userInfo?[UIKeyboardAnimationDurationUserInfoKey] as? NSNumber)?.uintValue ?? 0

        UIView.animate(withDuration: TimeInterval(duration), delay: 0, options: [UIViewAnimationOptions(rawValue: UInt(curve))], animations: {
            self.view.layoutIfNeeded()
        }, completion: nil)

    }

    @objc func clearCodeInput() {
        presenter.clearText()
    }

    @objc func timerShouldChange() {
        presenter.updateTimer()
    }

    @IBAction func requestNewPinButtonAction(_ sender: Any) {
        presenter.requestNewPin()
    }

    deinit {
        ComponentManager.remove(id: presenter.routerTag)
        DLog(message: "Deinitialized SecurityViewController")
    }
}

// MARK: UITextFieldDelegate

extension SecurityViewController: UITextFieldDelegate {

    func textFieldDidBeginEditing(_ textField: UITextField) {

        separatorLineView.backgroundColor = .BBVA600

    }

    func textFieldDidEndEditing(_ textField: UITextField) {

        separatorLineView.backgroundColor = .BBVA500

    }

    @objc func textFieldDidChange(_ textField: UITextField) {

        let text = textField.text ?? ""

        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedStringKey.kern, value: letterSpacing, range: NSRange(location: 0, length: text.count))
        attributedString.addAttribute(NSAttributedStringKey.font, value: textField.font!, range: NSRange(location: 0, length: text.count))
        attributedString.addAttribute(NSAttributedStringKey.foregroundColor, value: textField.textColor!, range: NSRange(location: 0, length: text.count))

        textField.attributedText = attributedString

        presenter.codeWrote(text: text)

    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

      return presenter.shouldChangeCharacters(textValue: textField.text ?? "", withNewText: string, inRange: range)

    }
}

extension SecurityViewController: ViewProtocol {

    func showError(error: ModelBO) {

    }

    func changeColorEditTexts() {

    }

}

extension SecurityViewController: SecurityViewProtocol {

    func clearSecurityCodeInput(value: String) {
        securityCodeTextField.text = value
    }

    func showSecurityData(withSecurityDisplayItems displayItems: SecurityDisplayData) {

        titleLabel.attributedText = displayItems.introducePinTitle
        requestSmsDescriptionLabel.text = displayItems.requestPinDescription
        requestNewPinButton.setTitle(displayItems.requestPinTitle, for: .normal)
        securityCodeTextField.placeholder = displayItems.placeHolderText

        informationImageView.image = SVGCache.image(forSVGNamed: displayItems.informationIcon!, color: .BBVALIGHTBLUE)
        timerImageView.image = SVGCache.image(forSVGNamed: displayItems.timerIcon!, color: .MEDIUMBLUE)

    }

    func updateTimer(withText text: String) {

        timerLabel.text = text

    }

    func startTimer() {

        requestNewPinButton.isEnabled = false

        timer.invalidate()

        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: (#selector(SecurityViewController.timerShouldChange)), userInfo: nil, repeats: true)

    }

    func finishTimer() {

        requestNewPinButton.isEnabled = true

        timer.invalidate()

    }

    func enableConfirm() {

        confirmButton.isEnabled = true

    }

    func disableConfirm() {

        confirmButton.isEnabled = false

    }
}
