//
//  SecurityDisplayData.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 27/12/2017.
//  Copyright © 2017 BBVA. All rights reserved.
//

struct SecurityDisplayData {

    var introducePinTitle: NSAttributedString?
    var requestPinDescription: String?
    var informationIcon: String?
    var timerIcon: String?
    var requestPinTitle: String?
    var placeHolderText: String?

    static func generateSecurityDisplayData() -> SecurityDisplayData {

        var securityDisplayData = SecurityDisplayData()

        securityDisplayData.informationIcon = ConstantsImages.Common.info_icon
        securityDisplayData.timerIcon = ConstantsImages.Common.clock_icon
        securityDisplayData.introducePinTitle = CustomAttributedText.attributedBoldText(forFullText: Localizables.common.key_introduce_key_from_sms, withBoldText: Localizables.common.key_sms_text, withFont: Tools.setFontBook(size: 14), withBoldFont: Tools.setFontBold(size: 14), withAlignment: .left, andForegroundColor: .BBVA600)

        securityDisplayData.requestPinDescription = Localizables.common.key_request_new_sms_description
        securityDisplayData.requestPinTitle = Localizables.common.key_request_new_sms
        securityDisplayData.placeHolderText = Localizables.common.key_placeholder_text

        return securityDisplayData

    }

}
