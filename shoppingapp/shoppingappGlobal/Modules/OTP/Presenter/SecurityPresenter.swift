//
//  SecurityPresenter.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 27/12/2017.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation
import RxSwift
import CellsNativeComponents

class SecurityPresenter<T: SecurityViewProtocol>: BasePresenter<T> {

    var timeForTimerInSeconds = 180
    var remainingTime = 0
    var code = ""

    let EVENT_NAV_TO_SECURITY_SCREEN: String = "nav-to-second-next-login-bind"

    let AUTHENTICATION_TYPE = "authenticationtype"
    let AUTHENTICATION_DATA = "authenticationdata"

    var securityData: SecurityDisplayData?

    required init() {
        super.init(tag: SecurityPageReaction.ROUTER_TAG)
    }

    override init(busManager: BusManager) {
        super.init(busManager: busManager)
        routerTag = SecurityPageReaction.ROUTER_TAG
    }

    func calculateMinSecond(forSeconds seconds: Int) -> String {
        let minutes = seconds / 60 % 60
        let second = seconds % 60
        return String(format: "%02i:%02i %@", minutes, second, Localizables.common.key_minute_text)
    }

    func createSecurityData() -> SecurityDisplayData {
        securityData = SecurityDisplayData.generateSecurityDisplayData()

        return securityData!
    }
}
extension SecurityPresenter: SecurityPresenterProtocol {

    func replayServiceSecurity(withHeader header: [String: String], andSecurityCode securityCode: String) {

        self.showLoading()

        busManager.dismissViewController(animated: true) { [weak self] in

            guard let `self` = self else {
                return
            }

            var headers = [String: String]()

            if let authenticationtype = header[self.AUTHENTICATION_TYPE], let authenticationdata = header[self.AUTHENTICATION_DATA] {

                headers[self.AUTHENTICATION_TYPE] = authenticationtype
                headers[self.AUTHENTICATION_DATA] = "\(authenticationdata)=\(securityCode)"
            }

            self.busManager.securityInterceptorStore?.unLock(headers: headers)
        }
    }

    func shouldChangeCharacters(textValue text: String, withNewText newText: String, inRange range: NSRange) -> Bool {
        var shouldChangeCharacter = false

        let allowedCharacters = Settings.SecurityControllerGlobal.security_code_valid_character_set

        if newText.rangeOfCharacter(from: allowedCharacters.inverted) != nil {
            shouldChangeCharacter = false
        } else {
            let length = text.count + newText.count - range.length

            if length > Settings.SecurityControllerGlobal.security_code_maximum_length && range.length == 0 {

                shouldChangeCharacter = false
            } else {

                shouldChangeCharacter = true
            }
        }
        return shouldChangeCharacter
    }

    func getSecurityData() {

        remainingTime = timeForTimerInSeconds

        view?.showSecurityData(withSecurityDisplayItems: createSecurityData())
        view?.updateTimer(withText: calculateMinSecond(forSeconds: timeForTimerInSeconds))
        view?.startTimer()

    }

    func updateTimer() {

        if remainingTime >= 1 {
            remainingTime -= 1
        }

        if remainingTime == 0 {

            view?.finishTimer()
            view?.disableConfirm()
        }

        view?.updateTimer(withText: String(describing: calculateMinSecond(forSeconds: remainingTime)))

    }

    func clearText() {
        view?.clearSecurityCodeInput(value: "")
        view?.disableConfirm()
    }

    func codeWrote(text: String) {

        code = text

        if code.count < Settings.SecurityControllerGlobal.security_code_maximum_length {
            view?.disableConfirm()
        } else {
            view?.enableConfirm()
        }

    }

    func requestNewPin() {

        view?.clearSecurityCodeInput(value: "")

        remainingTime = timeForTimerInSeconds

        view?.updateTimer(withText: calculateMinSecond(forSeconds: timeForTimerInSeconds))
        view?.startTimer()
    }

}
