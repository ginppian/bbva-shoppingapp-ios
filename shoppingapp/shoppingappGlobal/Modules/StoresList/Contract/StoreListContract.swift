//
//  StoreListContract.swift
//  shoppingapp
//
//  Created by Developer on 18/07/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import RxSwift

protocol StoreListViewProtocol: ViewProtocol {
    
    func configureView(withTitle title: String)
    func showStores(withStoresDisplayData storesDisplayData: StoresDisplayData)
}

protocol StoreListPresenterProtocol: PresenterProtocol {
    
    func viewWillAppear()
    func itemSelected(atIndex index: Int)
}
