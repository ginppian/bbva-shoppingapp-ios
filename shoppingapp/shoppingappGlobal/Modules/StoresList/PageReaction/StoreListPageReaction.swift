//
//  StoreListPageReaction.swift
//  shoppingapp
//
//  Created by Developer on 18/07/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

class StoreListPageReaction: PageReaction {
    
    static let ROUTER_TAG: String = "shopping-storeList-tag"
    
    static let EVENT_NAV_TO_STORE_MAP_SCREEN: ActionSpec<Model> = ActionSpec<Model>(id: "EVENT_NAV_TO_STORE_MAP_SCREEN")

    override func configure() {
        navigateToStoreMapScreen()
    }
    
    func navigateToStoreMapScreen() {
        _ = when(id: StoreListPageReaction.ROUTER_TAG, type: Model.self)
            .doAction(actionSpec: StoreListPageReaction.EVENT_NAV_TO_STORE_MAP_SCREEN)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, model in
                guard let modelForNavigation = model else {
                    return
                }
                
                router.navigateToShoppingStoreMap(withModel: modelForNavigation)
        }
    }
}
