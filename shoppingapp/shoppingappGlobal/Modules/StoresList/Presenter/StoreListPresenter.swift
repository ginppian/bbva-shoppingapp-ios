//
//  StoreListPresenter.swift
//  shoppingapp
//
//  Created by Developer on 18/07/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import RxSwift
import CellsNativeComponents

class StoreListPresenter<T: StoreListViewProtocol>: BasePresenter<T> {
    
    var stores = [StoreBO]()
    var title = String()
    var displayItems = StoresDisplayData()
    var userLocation: GeoLocationBO?
    var categoryBO: CategoryBO?
    var promotionBO: PromotionBO?

    let ROUTER_TAG: String = "shopping-storeList-tag"
    
    required init() {
        super.init(tag: ROUTER_TAG)
    }
    
    override init(busManager: BusManager) {
        super.init(busManager: busManager)
        routerTag = "shopping-storeList-tag"
    }
    
    override func setModel(model: Model) {
        
        if let promotionDetailTransport = model as? PromotionDetailTransport {
            
            if let transportPromotionBO = promotionDetailTransport.promotionBO {
                title = transportPromotionBO.commerceInformation.name
                userLocation = promotionDetailTransport.userLocation
                categoryBO = promotionDetailTransport.category
                promotionBO = transportPromotionBO

                if let listStores = transportPromotionBO.stores {
                    stores = listStores
                }
            }
        }
    }
    
    func showStoresMap(withTransport transport: StoresMapTransport) {
        
        busManager.navigateScreen(tag: routerTag, StoreListPageReaction.EVENT_NAV_TO_STORE_MAP_SCREEN, transport)
    }
}

extension StoreListPresenter: StoreListPresenterProtocol {

    func viewWillAppear() {
        view?.configureView(withTitle: String(format: "%@ (%d)", title, stores.count))
        view?.showStores(withStoresDisplayData: StoresDisplayData(withStores: stores, userLocation: userLocation))
    }
    
    func itemSelected(atIndex index: Int) {
        
        let selectedStore = stores[index]
        let promotionDetailTransport = StoresMapTransport()
        promotionDetailTransport.promotionBO = promotionBO
        promotionDetailTransport.selectedStore = selectedStore
        promotionDetailTransport.userLocation = userLocation
        promotionDetailTransport.category = categoryBO
        showStoresMap(withTransport: promotionDetailTransport)
    }
}
