//
//  StoreListViewController.swift
//  shoppingapp
//
//  Created by Developer on 13/07/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit

class StoreListViewController: BaseViewController {

    static let ID: String = String(describing: StoreListViewController.self)
    
    var presenter: StoreListPresenterProtocol!
    var displayItems: StoresDisplayData?

    @IBOutlet weak var storesTableView: UITableView!
    
    override func viewDidLoad() {
        configure(appLifecycle: (UIApplication.shared.delegate as! AppDelegate).appLifeCycle!, id: StoreListViewController.ID)

        super.viewDidLoad()
        
        configureContentView()
        
        if #available(iOS 11.0, *) {
            storesTableView.contentInsetAdjustmentBehavior = .never
        }
    }

    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        setNavigationBarDefault()
        
        setNavigationBackButtonDefault()
        
        BusManager.sessionDataInstance.disableSwipeLateralMenu()
        
        presenter.viewWillAppear()
        
    }
    
    func configureContentView() {
        
        storesTableView.backgroundColor = .BBVAWHITE
        
        storesTableView.register(UINib(nibName: StoreListTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: StoreListTableViewCell.identifier)

        storesTableView.rowHeight = UITableViewAutomaticDimension
        storesTableView.separatorStyle = .none
        storesTableView.reloadData()
    }
    
    override func initModel(withModel model: Model) {
        presenter.setModel(model: model)
    }
}

extension StoreListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        presenter.itemSelected(atIndex: indexPath.row)
    }
}

extension StoreListViewController: UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return displayItems?.items.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: StoreListTableViewCell.identifier, for: indexPath) as? StoreListTableViewCell {
            if let displayItems = displayItems {
                let displayItem = displayItems.items[indexPath.row]
                cell.configureCell(withDisplayItemStore: displayItem)
            }
            return cell
        }
        
        return UITableViewCell()
    }
    
}

extension StoreListViewController: StoreListViewProtocol {
    
    func showStores(withStoresDisplayData storesDisplayData: StoresDisplayData) {
        displayItems = storesDisplayData
        storesTableView.reloadData()
    }
    
    func configureView(withTitle title: String) {
        setNavigationTitle(withText: title)
    }
    
    func showError(error: ModelBO) { }
    
    func changeColorEditTexts() { }
}
