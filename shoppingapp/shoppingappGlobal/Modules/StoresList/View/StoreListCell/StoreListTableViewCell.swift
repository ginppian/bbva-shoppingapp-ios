//
//  StoreListTableViewCell.swift
//  shoppingapp
//
//  Created by Developer on 13/07/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit

class StoreListTableViewCell: UITableViewCell {

    static let identifier = "StoreListTableViewCell"
    private let distanceKmIdentifier = "StoreListTableViewCell"

    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var streetLabel: UILabel!
    @IBOutlet weak var distanceLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var addressTopSpaceHeigth: NSLayoutConstraint!
    @IBOutlet weak var rightArrowDistanceImage: UIImageView!
    @IBOutlet weak var rightArrowAddressImage: UIImageView!

    override func awakeFromNib() {
        super.awakeFromNib()
        
        selectionStyle = .none
        
        distanceLabel.font = Tools.setFontMedium(size: 16)
        distanceLabel.textColor = .MEDIUMBLUE
        
        streetLabel.font = Tools.setFontBookItalic(size: 16)
        streetLabel.textColor = .BBVA600
    }
    
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        
        super.setHighlighted(highlighted, animated: animated)
        
        if highlighted {
            backgroundColor = .BBVA100
            contentView.backgroundColor = .BBVA100
            
        } else {
            backgroundColor = .BBVAWHITE
            contentView.backgroundColor = .BBVAWHITE
        }
    }
    
    func configureCell(withDisplayItemStore displayItemStore: StoreDisplayData) {
        
        setupDistanceLabel(withText: displayItemStore.formattedDistance, onLabel: distanceLabel)
        streetLabel.text = displayItemStore.address?.capitalized
    }
    
    func setupDistanceLabel(withText text: String?, onLabel label: UILabel) {
        
        if let text = text, !text.isEmpty {
            
            label.isHidden = false
            label.text = text
            distanceLabelHeight.constant = 14
            addressTopSpaceHeigth.constant = 20
            rightArrowDistanceImage.isHidden = false
            rightArrowAddressImage.isHidden = true
        } else {
            label.isHidden = true
            distanceLabelHeight.constant = 0
            addressTopSpaceHeigth.constant = 0
            rightArrowDistanceImage.isHidden = true
            rightArrowAddressImage.isHidden = false
        }
    }
}
