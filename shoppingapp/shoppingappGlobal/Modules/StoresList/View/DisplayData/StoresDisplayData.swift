//
//  StoresDisplayData.swift
//  shoppingapp
//
//  Created by Developer on 23/07/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

struct StoresDisplayData: Equatable {
    
    var items: [StoreDisplayData]

    init() {
        items = [StoreDisplayData]()
    }
    
    init(withStores stores: [StoreBO], userLocation: GeoLocationBO? = nil) {
        
        items = [StoreDisplayData]()
        
        for store in stores {
            let displayItem = StoreDisplayData(fromStore: store, userLocation: userLocation)
            items.append(displayItem)
        }
    }
}

struct StoreDisplayData: Equatable {
    
    var formattedDistance: String = ""
    var address: String?
    var userLocation: GeoLocationBO?
    
    init(fromStore store: StoreBO, userLocation: GeoLocationBO? = nil) {
        
        if let distance = store.formattedDistance(), !distance.isEmpty, userLocation != nil {
            formattedDistance = Localizables.promotions.key_from_distance_text + " " + distance
        }
        
        if let location = store.location {
            
            var addressList = [String]()
            
            if let addressName = location.addressName, !addressName.isEmpty {
                addressList.append(addressName.capitalized)
            }
            
            if let zipCode = location.zipCode, !zipCode.isEmpty {
                addressList.append(zipCode.capitalized)
            }
            
            if let city = location.city, !city.isEmpty {
                addressList.append(city.capitalized)
            }
            
            if let state = location.state, !state.isEmpty {
                addressList.append(state.capitalized)
            }
            
            if !addressList.isEmpty {
                address = addressList.joined(separator: ", ")
            }
        }
    }
}
