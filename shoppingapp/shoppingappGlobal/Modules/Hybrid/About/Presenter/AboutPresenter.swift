//
//  AboutPresenter.swift
//  shoppingapp
//
//  Created by Daniel on 23/7/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import RxSwift
import CellsNativeComponents

final class AboutPresenter<T: AboutViewProtocol>: BasePresenter<T> {

    lazy var publicConfiguration = PublicConfigurationManager.sharedInstance().localPublicConfiguration()

    required init() {
        super.init(tag: AboutPageReaction.routerTag)
    }

    override init(busManager: BusManager) {

        super.init(busManager: busManager)
        routerTag = AboutPageReaction.routerTag
    }
    
    private func navigateTo(web: String?, withTitle title: String?) {
        busManager.navigateScreen(tag: AboutPageReaction.routerTag, AboutPageReaction.eventNavigateToWeb, WebTransport(webPage: web, webTitle: title))
    }
}

extension AboutPresenter: AboutPresenterProtocol {
    
    func contractPressed() {
        
        navigateTo(web: publicConfiguration.publicConfig?.terms?.cud, withTitle: Localizables.menu_lateral.key_menu_about_cud_text)
    }
    
    func legalAdvicePressed() {
        
        navigateTo(web: publicConfiguration.publicConfig?.terms?.privacy, withTitle: Localizables.menu_lateral.key_menu_about_privacy_text)
    }
}
