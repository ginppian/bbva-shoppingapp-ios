//
//  AboutPageReaction.swift
//  shoppingapp
//
//  Created by Daniel on 23/7/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

final class AboutPageReaction: PageReaction {

    // MARK: Constants

    static let routerTag = "about-tag"

    static let id = String(describing: AboutPageReaction.self)
    
    static let eventNavigateToWeb: ActionSpec<Model> = ActionSpec<Model>(id: "EVENT_NAV_WEB")

    static let menuAboutChannel = "cvi_version_info_input"
    static let menuAboutOptionsChannel = "am_visible_options_channel"
    static let menuAboutShareChannel = "am_open_legal_share_channel"
    static let menuAboutContractChannel = "cla_contract_button_clicked"
    static let menuAboutLegalAdviceChannel = "cla_legal_advice_button_clicked"

    override func configure() {

        pageLoaded()
        shareAction()
        contractAction()
        legalAdviceAction()
        navigateToWeb()
    }

    func pageLoaded() {

        _ = reactTo(channel: PageReactionConstants.CHANNEL_HIDDEN_VIEW_NATIVE)?
            .then()
            .inside(componentID: AboutViewController.ID, component: AboutViewController.self)
            .doReactionAndClearChannel { controller, page in

                if let page = page {
                    controller.registerOmniture(page: page)
                }
            }
    }

    func shareAction() {

        let channel: JsonChannel = JsonChannel(AboutPageReaction.menuAboutShareChannel)

        _ = reactTo(channel: channel)?
            .then()
            .inside(componentID: AboutViewController.ID, component: AboutViewController.self)
            .doReactionAndClearChannel(reactionExecution: { component, _ in

                component.shareContent()
            })
    }
    
    func contractAction() {
        
        let channel: JsonChannel = JsonChannel(AboutPageReaction.menuAboutContractChannel)
        
        _ = reactTo(channel: channel)?
            .then()
            .inside(componentID: AboutPageReaction.routerTag, component: AboutPresenter<AboutViewController>.self)
            .doReactionAndClearChannel(reactionExecution: { component, _ in
                component.contractPressed()
            })
    }
    
    func legalAdviceAction() {
        
        let channel: JsonChannel = JsonChannel(AboutPageReaction.menuAboutLegalAdviceChannel)
        
        _ = reactTo(channel: channel)?
            .then()
            .inside(componentID: AboutPageReaction.routerTag, component: AboutPresenter<AboutViewController>.self)
            .doReactionAndClearChannel(reactionExecution: { component, _ in
                component.legalAdvicePressed()
            })
    }
    
    func navigateToWeb() {
        
        _ = when(id: AboutPageReaction.routerTag, type: Model.self)
            .doAction(actionSpec: AboutPageReaction.eventNavigateToWeb)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, model in
                guard let modelForNavigation = model else {
                    return
                }
                
                router.navigateToShoppingWebPage(withModel: modelForNavigation)
        }
    }
}
