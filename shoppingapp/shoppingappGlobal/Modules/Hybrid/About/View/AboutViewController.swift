//
//  AboutViewController.swift
//  shoppingapp
//
//  Created by Daniel on 23/7/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit
import CellsNativeCore
import CellsNativeComponents

final class AboutViewController: WalletCellsViewController {

    // MARK: Constants

    static let ID = String(describing: AboutViewController.self)

    override func viewDidLoad() {

        configure(appLifecycle: (UIApplication.shared.delegate as! AppDelegate).appLifeCycle!, id: AboutViewController.ID)

        super.viewDidLoad()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        navigationController?.navigationBar.isHidden = true
        
        super.viewWillAppear(animated)
    }

    deinit {
        
        DLog(message: "Deinitialized AboutViewController")
    }
}
extension AboutViewController: AboutViewProtocol {

    func shareContent() {

        let urlMarket = Configuration.provideUrlMarketFromConfiguration()
        
        let viewShareController = ShareContentManager.sharedInstance().shareContent(values: [Localizables.menu_lateral.key_menu_lateral_share_text, urlMarket])

        present(viewShareController, animated: true, completion: nil)
    }
}

extension AboutViewController: CellsProtocol {
}
