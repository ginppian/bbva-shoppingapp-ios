//
//  AboutContract.swift
//  shoppingapp
//
//  Created by Daniel on 23/7/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol AboutViewProtocol: ViewProtocol {

    func shareContent()

}

protocol AboutPresenterProtocol: CellsPresenterProtocol {
    
    func contractPressed()
    func legalAdvicePressed()
}
