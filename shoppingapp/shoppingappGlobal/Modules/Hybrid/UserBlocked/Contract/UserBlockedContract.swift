//
//  UserBlockedContract.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 19/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol UserBlockedViewProtocol: ViewProtocol {
    
    func showBankingMobile()
}

protocol UserBlockedPresenterProtocol: CellsPresenterProtocol {
    
}
