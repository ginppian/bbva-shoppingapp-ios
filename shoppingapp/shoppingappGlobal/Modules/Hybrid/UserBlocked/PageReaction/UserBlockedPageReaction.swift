//
//  UserBlockedPageReaction.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 19/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

class UserBlockedPageReaction: PageReaction {
    
    static let routerTag: String = "user-bloqued-tag"
    static let id = String(describing: UserBlockedPageReaction.self)
    
    // Channels
    
    static let resultGoToBancomer = "ub_go_to_bancomer"
    
    override func configure() {
        
        registerToReactToCellResultGoToBancomer()
    }
    
    func registerToReactToCellResultGoToBancomer() {
        
        let channel: JsonChannel = JsonChannel(UserBlockedPageReaction.resultGoToBancomer)
        
        _ = reactTo(channel: channel)?
            .then()
            .inside(componentID: UserBlockedViewController.id, component: UserBlockedViewController.self)
            .doReactionAndClearChannel { userBlockedViewController, _ in
                
                userBlockedViewController.showBankingMobile()
        }
    }
}
