//
//  UserBlockedViewController.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 19/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class UserBlockedViewController: WalletCellsViewController {
    
    // MARK: Constants
    
    static let id: String = String(describing: UserBlockedViewController.self)
    
    // MARK: Life cycle
    
    override func viewDidLoad() {
        
        configure(appLifecycle: (UIApplication.shared.delegate as! AppDelegate).appLifeCycle!, id: UserBlockedViewController.id)
        
        super.viewDidLoad()
    }
    
    deinit {
        DLog(message: "Deinitialized UserBlockedViewController")
    }
}

// MARK: - UserBlockedViewProtocol
extension UserBlockedViewController: UserBlockedViewProtocol {
    
    func showBankingMobile() {
        
        let urlSchema = Configuration.provideUrlSchemaFromConfiguration()
        let urlBBVAMovilMarket = Configuration.provideUrlBBVAMovilMarketFromConfiguration()
        
        let urlOpener = URLOpener()
        
        if !urlOpener.openURL(urlSchema) {
            _ = urlOpener.openURL(urlBBVAMovilMarket)
        }
    }
}

// MARK: - CellsProtocol
extension UserBlockedViewController: CellsProtocol {
}
