//
//  UserBlockedPresenter.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 19/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import RxSwift
import CellsNativeComponents

class UserBlockedPresenter<T: UserBlockedViewProtocol>: BasePresenter<T> {
    
    required init() {
        
        super.init(tag: UserBlockedPageReaction.routerTag)
    }
    
    override init(busManager: BusManager) {
        
        super.init(busManager: busManager)
        routerTag = UserBlockedPageReaction.routerTag
    }
}

// MARK: - UserBlockedPresenterProtocol
extension UserBlockedPresenter: UserBlockedPresenterProtocol {
    
}

// MARK: - CellsPresenterProtocol
extension UserBlockedPresenter: CellsPresenterProtocol {
    
}
