//
//  CardBlockViewController.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 30/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

class CardBlockViewController: WalletCellsViewController {

    // MARK: Constants

    static let ID: String = String(describing: CardBlockViewController.self)

    let trackerCustomerID = SessionDataManager.sessionDataInstance().customerID

    // MARK: Vars

    var cancelCardStore: CancelCardStore?
    
    // MARK: Life cycle

    override func viewDidLoad() {

        configure(appLifecycle: (UIApplication.shared.delegate as! AppDelegate).appLifeCycle!, id: CardBlockViewController.ID)

        if let networkWorker = BusManager.sessionDataInstance.networkWorker {
            cancelCardStore = CancelCardStore(worker: networkWorker, host: ServerHostURL.getCardsURL())
        }

        super.viewDidLoad()
        
//        TicketUtils.openDummyTicket()
    }
    
    override func initModel(withModel model: Model) {
        presenter.setModel(model: model)
    }

    override func dismiss(completion: (() -> Void)? = nil) {

        super.dismiss(completion: { [weak self] in
            if let presenter = self?.presenter as? CardBlockPresenter<CardBlockViewController> {
                presenter.viewWasDismissed()
            }
        })
    }

    deinit {
        DLog(message: "Deinitialized CardBlockViewController")
    }

    func updateCurrentCardBO(card: CardBO) {

        if let presenter = presenter as? CardBlockPresenter<CardBlockViewController> {
            presenter.setCurrentCard(card: card)
        }
    }
}

extension CardBlockViewController: CardBlockViewProtocol {
    
    func sendTrackCellsPageCardBlockInfo() {

        TrackerHelper.sharedInstance().trackCellsPage(withCustomerID: trackerCustomerID, pageName: TRACKER_HELPER_PAGE_NAME_CARD_BLOCK_INFO)
    }

    func sendTrackCellsPageCardBlockType() {

        TrackerHelper.sharedInstance().trackCellsPage(withCustomerID: trackerCustomerID, pageName: TRACKER_HELPER_PAGE_NAME_CARD_BLOCK_TYPE)
    }

    func sendTrackCellsPageCardBlockSuccess() {

        TrackerHelper.sharedInstance().trackCellsPage(withCustomerID: trackerCustomerID, pageName: TRACKER_HELPER_PAGE_NAME_CARD_BLOCK_SUCCESS)
    }
}

extension CardBlockViewController: CellsProtocol {

    func startDoRequest(cellsDTO: CellsDTO) {

        guard let cardCancelDTO = cellsDTO as? CardCancelDTO else {
            return
        }

        presenter?.showLoading(completion: nil)
        cancelCardStore?.blockCardRequest(cardCancelDTO: cardCancelDTO)
    }
}
