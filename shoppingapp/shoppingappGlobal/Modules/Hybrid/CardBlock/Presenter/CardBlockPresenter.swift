//
//  CardBlockPresenter.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 30/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import RxSwift
import CellsNativeComponents

class CardBlockPresenter<T: CardBlockViewProtocol>: BasePresenter<T> {

    let disposeBag = DisposeBag()

    var blockCardObjectCells: RequestBlockCardObjectCells?
    var cardBO: CardBO?
    var cardOperationInfo: CardOperationInfo?
    var successResult: BlockCardSuccess?

    required init() {
        super.init(tag: CardBlockPageReaction.ROUTER_TAG)
    }

    override init(busManager: BusManager) {

        super.init(busManager: busManager)
        routerTag = CardBlockPageReaction.ROUTER_TAG
    }

    override func setModel(model: Model) {
        if let error = model as? ErrorBO {
            self.errorBO = error
        } else if let cardBlock = model as? RequestBlockCardObjectCells {
            self.blockCardObjectCells = cardBlock
        }
    }

}

extension CardBlockPresenter: CardBlockPresenterProtocol {
    
    func showPageSuccess(modelBO: ModelBO?) {

        guard let blockCardsBO = modelBO as? BlockCardBO, let cardNumber = cardBO?.number.suffix(4) else {
            return
        }

        let date = Date.getDateFormatForCellsModule(withDate: blockCardsBO.blockDate)
        let folio = blockCardsBO.reference

        let successResult = BlockCardSuccess(cardNumber: String(cardNumber), date: date, folio: folio)
        self.successResult = successResult

        if let blockCardJSON = successResult.toJSON() {

            busManager.publishData(tag: CardBlockPageReaction.ROUTER_TAG, CardBlockPageReaction.PARAMS_BLOCK_CARD_SUCCESS, blockCardJSON)

            busManager.navigateScreen(tag: CardBlockPageReaction.ROUTER_TAG, CardBlockPageReaction.EVENT_BLOCK_CARD_SUCCESS, Void())

            if let cardOperationInfo = cardOperationInfo {
                busManager.publishData(tag: CardBlockPageReaction.ROUTER_TAG, CardsPageReaction.PARAMS_OPERATION_CARD_INFO_SUCCESS, cardOperationInfo)
            }
        }
    }

    func registerOmniture(page: String) {

        if page == CardBlockPageReaction.CELLS_PAGE_CARD_BLOCK_INFO {

            view?.sendTrackCellsPageCardBlockInfo()

        } else if page == CardBlockPageReaction.CELLS_PAGE_CARD_BLOCK_TYPE {

            view?.sendTrackCellsPageCardBlockType()

        } else if page == CardBlockPageReaction.CELLS_PAGE_BLOCK_CARD_SUCCESS {

            view?.sendTrackCellsPageCardBlockSuccess()
        }
    }

    func setCurrentCard(card: CardBO) {
        cardBO = card
    }

    func viewWasDismissed() {

        if successResult != nil {
            busManager.notify(tag: CardsPageReaction.ROUTER_TAG, CardsPageReaction.NEEDED_CARDS_REFRESH)
        }

    }
}
