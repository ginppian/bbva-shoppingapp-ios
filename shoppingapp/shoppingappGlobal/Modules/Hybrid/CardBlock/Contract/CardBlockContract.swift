//
//  CardBlockContract.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 30/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol CardBlockViewProtocol: ViewProtocol {

    func sendTrackCellsPageCardBlockInfo()
    func sendTrackCellsPageCardBlockType()
    func sendTrackCellsPageCardBlockSuccess()
}

protocol CardBlockPresenterProtocol: CellsPresenterProtocol {
    
    func viewWasDismissed()
}
