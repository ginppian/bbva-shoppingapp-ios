//
//  CardBlockPageReaction.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 30/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

class CardBlockPageReaction: CommonPageReaction {

    static let ROUTER_TAG: String = "card-block-tag"

    static let ID = String(describing: CardBlockPageReaction.self)

    static let EVENT_CARD_BLOCK_TYPE: ActionSpec<Void> = ActionSpec<Void>(id: "EVENT_CARD_BLOCK_TYPE")

    static let PARAMS_BLOCK_CARD_SUCCESS: ActionSpec<Any> = ActionSpec<Any>(id: "PARAMS_BLOCK_CARD_SUCCESS")

    static let EVENT_BLOCK_CARD_SUCCESS: ActionSpec<Void> = ActionSpec<Void>(id: "EVENT_BLOCK_CARD_SUCCESS")

    // Channels

    static let REQUEST_CHANNEL = "cbt_cells_request_channel"

    static let INFO_CONFIRMATION_CHANNEL = "bcs_info_confirmation_channel"

    // Cells pages

    static let CELLS_PAGE_BLOCK_CARD_SUCCESS = "cardBlockSuccess"

    static let CELLS_PAGE_CARD_BLOCK_TYPE = "cardBlockNormalReason"

    static let CELLS_PAGE_CARD_BLOCK_INFO = "cardBlockNormal"

    override func configure() {

        super.configure()
        
        pageLoaded()
        cardBlockTypeAction()
        finishedCardBlockRequest()
        finishedCardBlockErrorRequest()
        paramsBlockCardSuccess()
        navigateBlockCardSuccess()
        registerToReadCurrentCard()
        registerToReadOperationCardInfoChannel()
        paramsOperationBlockCardSuccess()
        skipNavigation()
    }

    func pageLoaded() {

        _ = reactTo(channel: PageReactionConstants.CHANNEL_HIDDEN_VIEW_NATIVE)?
            .then()
            .inside(componentID: CardBlockViewController.ID, component: CardBlockViewController.self)
            .doReactionAndClearChannel { controller, page in

                if let page = page {
                    controller.registerOmniture(page: page)
                }
            }

    }

    func cardBlockTypeAction() {

        let channel: JsonChannel = JsonChannel(CardBlockPageReaction.REQUEST_CHANNEL)

        _ = reactTo(channel: channel)?
            .then()
            .inside(componentID: CardBlockViewController.ID, component: CardBlockViewController.self)
            .doReactionAndClearChannel(reactionExecution: { component, param in

                let cardBlockCells = CellsRequestBO.deserialize(from: param as? [String: Any])
                let requestObjectCells = RequestBlockCardObjectCells.deserialize(from: cardBlockCells?.requestObject)
                if let requestObjectCells = requestObjectCells, let cardId = requestObjectCells.id, let typeBlock = requestObjectCells.typeBlock {
                    component.initModel(withModel: requestObjectCells as Model)
                    component.startDoRequest(cellsDTO: CardCancelDTO(cardId: cardId, typeBlock: typeBlock))
                }
            })
    }

    // MARK: - Block Card Request

    func finishedCardBlockRequest() {

        _ = when(id: CancelCardStore.ID, type: BlockCardDTO.self)
            .doAction(actionSpec: CancelCardStore.ON_BLOCK_SUCCESS)
            .then()
            .inside(componentID: CardBlockViewController.ID, component: CardBlockViewController.self)
            .doReaction { controller, param in
                if let blockCardDTO = param {
                    controller.finishDoRequestSuccess(modelBO: BlockCardBO(blockCardDTO: blockCardDTO))
                }
            }

    }

    func finishedCardBlockErrorRequest() {

        _ = when(id: CancelCardStore.ID, type: ErrorBO.self)
            .doAction(actionSpec: CancelCardStore.ON_CANCEL_ERROR)
            .then()
            .inside(componentID: CardBlockViewController.ID, component: CardBlockViewController.self)
            .doReaction { controller, param in

                controller.finishDoRequestError(error: param!)
            }
    }

    // MARK: - Block Card Success

    func paramsBlockCardSuccess() {

        let channel: JsonChannel = JsonChannel(CardBlockPageReaction.INFO_CONFIRMATION_CHANNEL)

        _ = writeOn(channel: channel)
            .when(componentID: CardBlockPageReaction.ROUTER_TAG)
            .doAction(actionSpec: CardBlockPageReaction.PARAMS_BLOCK_CARD_SUCCESS)
    }

    func navigateBlockCardSuccess() {

        _ = when(id: CardBlockPageReaction.ROUTER_TAG, type: Void.self)
            .doAction(actionSpec: CardBlockPageReaction.EVENT_BLOCK_CARD_SUCCESS)
            .then()
            .inside(componentID: WebController.ID, component: WebController.self)
            .doReaction { controller, _ in

                controller.navigateTo(route: CardBlockPageReaction.CELLS_PAGE_BLOCK_CARD_SUCCESS, payload: "")

            }
    }

    private func skipNavigation() {

        if let controller: WebController = ComponentManager.get(id: WebController.ID) {
            controller.skipNavigations(routes: [SkipTransitionDTO(from: CardBlockPageReaction.CELLS_PAGE_BLOCK_CARD_SUCCESS, to: CardBlockPageReaction.CELLS_PAGE_CARD_BLOCK_TYPE, skipHistory: true), SkipTransitionDTO(from: CardBlockPageReaction.CELLS_PAGE_CARD_BLOCK_TYPE, to: CardBlockPageReaction.CELLS_PAGE_CARD_BLOCK_INFO, skipHistory: true)])
        }
    }

    func registerToReadCurrentCard() {

        let channel: Channel = Channel<Any>(name: CardsPageReaction.CARD_BO_CHANNEL)

        _ = reactTo(channel: channel)?
            .then()
            .inside(componentID: CardBlockViewController.ID, component: CardBlockViewController.self)
            .doReactionAndClearChannel(reactionExecution: { component, param in
                if let card = param as? CardBO {
                    component.updateCurrentCardBO(card: card)
                }
            })
    }

    func registerToReadOperationCardInfoChannel() {

        _ = reactTo(channel: CardsPageReaction.OPERATION_CARD_INFO_CHANNEL)?
            .then()
            .inside(componentID: CardBlockPageReaction.ROUTER_TAG, component: CardBlockPresenter<CardBlockViewController>.self)
            .doReactionAndClearChannel(reactionExecution: { component, cardOperationInfo in
                component.cardOperationInfo = cardOperationInfo
            })
    }

    func paramsOperationBlockCardSuccess() {

        _ = writeOn(channel: CardsPageReaction.OPERATION_CARD_INFO_SUCCESS_CHANNEL)
            .when(componentID: CardBlockPageReaction.ROUTER_TAG)
            .doAction(actionSpec: CardsPageReaction.PARAMS_OPERATION_CARD_INFO_SUCCESS)
    }
}
