//
//  MobileTokenPageReaction.swift
//  shoppingappMX
//
//  Created by jesus.martinez on 26/12/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore

class MobileTokenPageReaction: CommonPageReaction {
    
    static let routerTag: String = "mobile-token-tag"
    
    static let menuMobileTokenChannel = "mt_info_channel"
    
    static let openAppChannel = "mt_primary_click"
    
    override func configure() {
        
        reactToOpenApp()
    }
    
    func reactToOpenApp() {
        
        let channel: JsonChannel = JsonChannel(MobileTokenPageReaction.openAppChannel)
        
        _ = reactTo(channel: channel)?
            .then()
            .inside(componentID: MobileTokenPageReaction.routerTag, component: MobileTokenPresenter<MobileTokenViewController>.self)
            .doReactionAndClearChannel(reactionExecution: { presenter, _ in
                
                presenter.openAppPressed()
            })
    }
}
