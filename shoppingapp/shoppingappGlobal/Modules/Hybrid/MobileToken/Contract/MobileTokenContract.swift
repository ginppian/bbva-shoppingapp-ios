//
//  MobileTokenContract.swift
//  shoppingapp
//
//  Created by jesus.martinez on 26/12/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol MobileTokenViewProtocol: ViewProtocol {
}

protocol MobileTokenPresenterProtocol: CellsPresenterProtocol {
    
    func openAppPressed()
}
