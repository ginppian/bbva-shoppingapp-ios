//
//  MobileTokenPresenter.swift
//  shoppingappMX
//
//  Created by jesus.martinez on 26/12/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class MobileTokenPresenter<T: MobileTokenViewProtocol>: BasePresenter<T> {
    
    lazy var publicConfiguration = PublicConfigurationManager.sharedInstance().localPublicConfiguration()
    lazy var urlOpener: URLOpenerProtocol = URLOpener()
    
    required init() {
        super.init(tag: MobileTokenPageReaction.routerTag)
    }
    
    override init(busManager: BusManager) {
        
        super.init(busManager: busManager)
        routerTag = MobileTokenPageReaction.routerTag
    }
}

extension MobileTokenPresenter: MobileTokenPresenterProtocol {

    func openAppPressed() {
        
        var url = URL(string: publicConfiguration.publicConfig?.mobileGlomo?.iOS?.url ?? PublicConfigurationManager.DefaultPublicConfigurationDTO.glomoMarketURL)
        
        if publicConfiguration.isGlomoInstalled() {
            
            url = URL(string: publicConfiguration.publicConfig?.mobileGlomo?.iOS?.schema ?? PublicConfigurationManager.DefaultPublicConfigurationDTO.glomoScheme)
        } else if publicConfiguration.isBancomerInstalled() {
            
            url = URL(string: publicConfiguration.publicConfig?.mobileBancomer?.iOS?.schema ?? PublicConfigurationManager.DefaultPublicConfigurationDTO.bancomerScheme)
        }
        
        if let url = url {
            urlOpener.openURL(url)
        }
    }
}
