//
//  MobileTokenViewController.swift
//  shoppingappMX
//
//  Created by jesus.martinez on 26/12/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class MobileTokenViewController: WalletCellsViewController {
    
    // MARK: Constants
    
    static let id: String = String(describing: MobileTokenViewController.self)
    
    // MARK: Life cycle
    
    override func viewDidLoad() {
        
        configure(appLifecycle: (UIApplication.shared.delegate as! AppDelegate).appLifeCycle!, id: MobileTokenViewController.id)
        
        super.viewDidLoad()
    }
    
    deinit {
        DLog(message: "Deinitialized MobileTokenViewController")
    }
}

extension MobileTokenViewController: MobileTokenViewProtocol {
    
}
