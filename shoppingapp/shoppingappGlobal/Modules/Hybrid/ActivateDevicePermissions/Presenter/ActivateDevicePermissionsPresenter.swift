//
//  ActivateDevicePermissionsPresenter.swift
//  shoppingapp
//
//  Created by Marcos on 20/9/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import RxSwift
import CellsNativeComponents

class ActivateDevicePermissionsPresenter<T: ActivateDevicePermissionsViewProtocol>: BasePresenter<T> {

    let disposeBag = DisposeBag()
    
    var notificationManager: NotificationManager = NotificationManagerType()

    required init() {
        
        super.init(tag: ActivateDevicePermissionsPageReaction.ROUTER_TAG)
    }

    override init(busManager: BusManager) {

        super.init(busManager: busManager)
        routerTag = ActivateDevicePermissionsPageReaction.ROUTER_TAG
    }

    func showRequestNotificationAutorization() {
        
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: false as AnyObject, withKey: PreferencesManagerKeys.kUserCloseNotificationsAdvise)
        
        notificationManager.showRequestNotificationAutorization { isGranted in
            self.didRequestAuthorizationNotifications(granted: isGranted)
        }
    }

    func userClosedNotificationAdvise() {
        
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: true as AnyObject, withKey: PreferencesManagerKeys.kUserCloseNotificationsAdvise)
        
        dismissAdvise()
    }
    
    func configurePermissionsAdviseClosed() {
        
        notificationManager.isAuthorizedStatus {  [weak self] authorized in
            
            if authorized, let apnsToken = PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kApnsToken) as? String {
                
                self?.view?.registerDeviceOnNotificationsService(withToken: apnsToken)
            }
            
            self?.dismissAdvise()
        }
    }
    
    func configurePermissionsAdviseButtonPressed() {
        
        notificationManager.isAuthorizedStatus { [weak self] authorized in
            
            if authorized {
                
                if let apnsToken = PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kApnsToken) as? String {
                    
                    self?.view?.registerDeviceOnNotificationsService(withToken: apnsToken)
                }
                
                self?.dismissAdvise()
            } else {
                
                self?.view?.openAppSettings()
            }
        }
    }
    
    private func dismissAdvise() {
        
        busManager.dismissViewController(animated: true) { [weak self] in
            guard let `self` = self else {
                return
            }
            self.busManager.notify(tag: HomePageReaction.ROUTER_TAG, HomePageReaction.EVENT_CLOSE_CELL_STACK)
        }
    }
    
    fileprivate func didRequestAuthorizationNotifications(granted: Bool) {
        
        busManager.dismissViewController(animated: true) { [weak self] in
            guard let `self` = self else {
                return
            }
            self.busManager.notify(tag: HomePageReaction.ROUTER_TAG, HomePageReaction.EVENT_CLOSE_CELL_STACK)
            if granted {
                self.view?.showToastAceptedNotifications()
            } else {
                self.view?.showToastRejectedNotifications()
            }
        }
    }
}

extension ActivateDevicePermissionsPresenter: ActivateDevicePermissionsPresenterProtocol {
    
    func didPermissionsChanged(granted: Bool) {
        
        didRequestAuthorizationNotifications(granted: granted)
    }
    
    func applicationDidBecomeActive() {
        
        notificationManager.isAuthorizedStatus { [weak self] authorized in
            
            if authorized {
                
                if let apnsToken = PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kApnsToken) as? String {
                    
                    self?.view?.registerDeviceOnNotificationsService(withToken: apnsToken)
                }
                
                self?.dismissAdvise()
                self?.view?.showToastAceptedNotifications()
            } else {
                
                self?.dismissAdvise()
                self?.view?.showToastRejectedNotifications()
            }
        }
    }
}

extension ActivateDevicePermissionsPresenter: CellsPresenterProtocol {

}
