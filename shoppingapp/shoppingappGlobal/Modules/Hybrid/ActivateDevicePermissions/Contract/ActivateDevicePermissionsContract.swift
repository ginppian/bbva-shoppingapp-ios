//
//  ActivateDevicePermissionsContract.swift
//  shoppingapp
//
//  Created by Marcos on 20/9/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol ActivateDevicePermissionsViewProtocol: ViewProtocol {

    func openAppSettings()
    func registerDeviceOnNotificationsService(withToken token: String)
    func showToastRejectedNotifications()
    func showToastAceptedNotifications()
}

protocol ActivateDevicePermissionsPresenterProtocol: CellsPresenterProtocol {
    
    func didPermissionsChanged(granted: Bool)
    func applicationDidBecomeActive()
}
