//
//  ActivateDevicePermissionsPageReaction.swift
//  shoppingapp
//
//  Created by Marcos on 20/9/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

class ActivateDevicePermissionsPageReaction: PageReaction {

    static let ROUTER_TAG: String = "activate_device_permissions-tag"
    static let ID = String(describing: ActivateDevicePermissionsPageReaction.self)
    
    // Pages
    static let CELLS_PAGE_ACTIVATE_DEVICE_PERMISSIONS =  "notificationsConfiguration"
    static let CELLS_PAGE_CONFIGURE_DEVICE_PERMISSIONS =  "notificationsConfigurationPermissions"
    
    // Channels
    static let RESULT_CHANNEL = "adp_result_channel"
    static let CLOSE_CHANNEL = "adp_close_channel"
    static let CONFIGURATION_RESULT_CHANNEL = "cdp_result_channel"
    static let CONFIGURATION_CLOSE_CHANNEL = "cdp_close_channel"
    
    override func configure() {
        
        pageLoaded()
        registerToReactToCellResultChannel()
        registerToReactToCellCloseChannel()
        registerToReactToConfigurationCellResultChannel()
        registerToReactToConfigurationCellCloseChannel()
    }

    func pageLoaded() {
        
        _ = reactTo(channel: PageReactionConstants.CHANNEL_HIDDEN_VIEW_NATIVE)?
            .then()
            .inside(componentID: ActivateDevicePermissionsViewController.ID, component: ActivateDevicePermissionsViewController.self)
            .doReactionAndClearChannel { controller, page in
                
                if let page = page {
                    
                    controller.registerOmniture(page: page)
                }
        }
    }

    func registerToReactToCellResultChannel() {

        let channel: JsonChannel = JsonChannel(ActivateDevicePermissionsPageReaction.RESULT_CHANNEL)

        _ = reactTo(channel: channel)?
            .then()
            .inside(componentID: ActivateDevicePermissionsPageReaction.ROUTER_TAG, component: ActivateDevicePermissionsPresenter<ActivateDevicePermissionsViewController>.self)
            .doReactionAndClearChannel { activateDevicePermissionsPresenter, _ in

                activateDevicePermissionsPresenter.showRequestNotificationAutorization()
        }
    }

    func registerToReactToCellCloseChannel() {

        let channel: JsonChannel = JsonChannel(ActivateDevicePermissionsPageReaction.CLOSE_CHANNEL)

        _ = reactTo(channel: channel)?
            .then()
            .inside(componentID: ActivateDevicePermissionsPageReaction.ROUTER_TAG, component: ActivateDevicePermissionsPresenter<ActivateDevicePermissionsViewController>.self)
            .doReactionAndClearChannel { activateDevicePermissionsPresenter, _ in

                activateDevicePermissionsPresenter.userClosedNotificationAdvise()
        }
    }
    
    func registerToReactToConfigurationCellResultChannel() {
        
        let channel: JsonChannel = JsonChannel(ActivateDevicePermissionsPageReaction.CONFIGURATION_RESULT_CHANNEL)
        
        _ = reactTo(channel: channel)?
            .then()
            .inside(componentID: ActivateDevicePermissionsPageReaction.ROUTER_TAG, component: ActivateDevicePermissionsPresenter<ActivateDevicePermissionsViewController>.self)
            .doReactionAndClearChannel { activateDevicePermissionsPresenter, _ in
                
                activateDevicePermissionsPresenter.configurePermissionsAdviseButtonPressed()
        }
    }
    
    func registerToReactToConfigurationCellCloseChannel() {
        
        let channel: JsonChannel = JsonChannel(ActivateDevicePermissionsPageReaction.CONFIGURATION_CLOSE_CHANNEL)
        
        _ = reactTo(channel: channel)?
            .then()
            .inside(componentID: ActivateDevicePermissionsPageReaction.ROUTER_TAG, component: ActivateDevicePermissionsPresenter<ActivateDevicePermissionsViewController>.self)
            .doReactionAndClearChannel { activateDevicePermissionsPresenter, _ in
                
                activateDevicePermissionsPresenter.configurePermissionsAdviseClosed()
        }
    }
}
