//
//  ActivateDevicePermissionsViewController.swift
//  shoppingapp
//
//  Created by Marcos on 20/9/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents
import UserNotifications

class ActivateDevicePermissionsViewController: WalletCellsViewController {

    // MARK: Constants

    static let ID: String = String(describing: ActivateDevicePermissionsViewController.self)
    var registerNotificationsStore: RegisterNotificationsStore?
    var observerDidBecomeActive: Any?

    // MARK: Life cycle
    
    override func viewDidLoad() {
        
        configure(appLifecycle: (UIApplication.shared.delegate as! AppDelegate).appLifeCycle!, id: ActivateDevicePermissionsViewController.ID)
        
        if let networkWorker = BusManager.sessionDataInstance.networkWorker {
            registerNotificationsStore = RegisterNotificationsStore(worker: networkWorker, host: ServerHostURL.getDevicesURL())
        }
        
        super.viewDidLoad()
    }
    
    private func addApplicationsObservers() {
        
        if observerDidBecomeActive == nil {
            
            observerDidBecomeActive = NotificationCenter.default.addObserver(forName: NSNotification.Name.UIApplicationDidBecomeActive, object: nil, queue: nil) { [weak self] _ in
                self?.applicationDidBecomeActive()
            }
        }
    }
    
    private func removeApplicationsObservers() {
        
        if let observer = observerDidBecomeActive {
            NotificationCenter.default.removeObserver(observer)
            observerDidBecomeActive = nil
        }
    }
    
    private func applicationDidBecomeActive() {
        
        removeApplicationsObservers()
        
        if let presenter = presenter as? ActivateDevicePermissionsPresenter<ActivateDevicePermissionsViewController> {
            presenter.applicationDidBecomeActive()
        }
    }
}

extension ActivateDevicePermissionsViewController: ActivateDevicePermissionsViewProtocol {

    func openAppSettings() {
        
        addApplicationsObservers()
        
        if let url = URL(string: UIApplicationOpenSettingsURLString) {
            
            DispatchQueue.main.async {
                URLOpener().openURL(url)
            }
        }
    }
    
    func registerDeviceOnNotificationsService(withToken token: String) {
        
        if let device = DeviceManager.instance.deviceUID {
            registerNotificationsStore?.registerNotificationsToken(registerNotificationsToken: RegisterNotificationsTokenDTO(token: token, deviceId: device, applicationId: DeviceManager.instance.applicationId, serviceId: RegisterNotificationsStore.serviceId))
        }
    }
    
    func showToastAceptedNotifications() {

        DispatchQueue.main.async {
            ToastManager.shared().showToast(withText: Localizables.notifications.key_ios_permission_accepted_text, backgroundColor: .BBVADARKGREEN, height: self.defaultHeightToastView)
        }
    }

    func showToastRejectedNotifications() {

        DispatchQueue.main.async {
            ToastManager.shared().showToast(withText: Localizables.notifications.key_ios_permission_denied_text, backgroundColor: .DARKMEDIUMBLUE, height: self.defaultHeightToastView)
        }
    }
}

extension ActivateDevicePermissionsViewController: CellsProtocol {
}
