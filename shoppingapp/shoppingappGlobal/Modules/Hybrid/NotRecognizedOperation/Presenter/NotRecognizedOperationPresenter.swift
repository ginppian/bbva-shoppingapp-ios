//
//  NotRecognizedOperationPresenter.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 18/7/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import RxSwift
import CellsNativeCore
import CellsNativeComponents

class NotRecognizedOperationPresenter<T: NotRecognizedOperationViewProtocol>: BasePresenter<T> {

    let disposeBag = DisposeBag()

    var blockCardObjectCells: RequestBlockCardObjectCells?
    var cardOnOffObjectCells: OnOffCardObjectCells?
    var cardBO: CardBO?

    required init() {
        super.init(tag: NotRecognizedOperationPageReaction.ROUTER_TAG)
    }

    override init(busManager: BusManager) {

        super.init(busManager: busManager)
        routerTag = NotRecognizedOperationPageReaction.ROUTER_TAG
    }

    override func setModel(model: Model) {
        if let error = model as? ErrorBO {
            self.errorBO = error
        } else if let cardBlock = model as? RequestBlockCardObjectCells {
            self.blockCardObjectCells = cardBlock
        } else if let cardOnOff = model as? OnOffCardObjectCells {
            self.cardOnOffObjectCells = cardOnOff
        }
    }

    func navigateToWallet() {

        if SessionDataManager.sessionDataInstance().isUserLogged {

            busManager.notify(tag: NotRecognizedOperationPageReaction.ROUTER_TAG, NotRecognizedOperationPageReaction.EVENT_NAV_TO_ROOT_CARDS)

        } else {

            BusManager.sessionDataInstance.hideCellsControllerIfNeeded()
            BusManager.sessionDataInstance.forceDismissAllPresentedViewControllers(completion: nil)
            if let webController: WebController = ComponentManager.get(id: WebController.ID) {
                webController.logoutWebView()
            }
            
            busManager.setRootWhenNoSession()
        }
    }
}

extension NotRecognizedOperationPresenter: NotRecognizedOperationPresenterProtocol {

    func onOffFailed(error: ErrorBO) {

        guard let cardBO = cardBO else {
            return
        }

        errorBO = error
        var onOffCardTransport = OnOffCardTransport(id: cardBO.cardId, cardNumber: String(cardBO.number.suffix(4)), alias: cardBO.cardAlias(), enabled: cardBO.isCardOn())

        if let url = cardBO.imageFront {
            onOffCardTransport.imageList = [OnOffCardImage(url: url)]
        }

        if let onOffCardJSON = onOffCardTransport.toJSON() {
             busManager.publishData(tag: TransactionDetailPageReaction.ROUTER_TAG, TransactionDetailPageReaction.PARAMS_ON_OFF_CARD, onOffCardJSON)

        }

        hideLoading(completion: { [weak self] in

            if error.status != 401 && error.code != "70" {
                self?.checkError()
            }

        })
    }

    func showPageSuccess(modelBO: ModelBO?) {

        guard let cardBO = cardBO else {
            return
        }
        
        if let blockCardsBO = modelBO as? BlockCardBO {
            
            let date = Date.getDateFormatForCellsModule(withDate: blockCardsBO.blockDate)
            let folio = blockCardsBO.reference

            let blockCardSuccess = BlockCardSuccess(cardNumber: String(cardBO.number.suffix(4)), date: date, folio: folio)

            if let blockCardJSON = blockCardSuccess.toJSON() {

                busManager.publishData(tag: NotRecognizedOperationPageReaction.ROUTER_TAG, NotRecognizedOperationPageReaction.PARAMS_BLOCK_CARD_SUCCESS, blockCardJSON)

                busManager.navigateScreen(tag: NotRecognizedOperationPageReaction.ROUTER_TAG, NotRecognizedOperationPageReaction.EVENT_BLOCK_CARD_SUCCESS, Void())
            }
        } else if let onOffResponseBO = modelBO as? OnOffResponseBO {

            for activation in cardBO.activations where activation.activationId == .onoff {
                activation.isActive = onOffResponseBO.isActive
            }
            let cardModel = CardModel(cardBO: cardBO)
                busManager.publishData(tag: NotRecognizedOperationPageReaction.ROUTER_TAG, CardsPageReaction.EVENT_CARD_UPDATED, cardModel)

            view?.showToastSuccess(withSuccessMessage: (onOffResponseBO.isActive ? Localizables.cards.key_cards_on_successful_text : Localizables.cards.key_cards_off_successful_text), andBackgroundColor: .BBVADARKGREEN)

        } else {
            return

        }

    }

    func registerOmniture(page: String) {

        if page == NotRecognizedOperationPageReaction.CELLS_PAGE_NOT_RECOGNIZED_OPERATION {

            view?.sendTrackCellsPageNotRecognizedOperation()

        } else if page == CardBlockPageReaction.CELLS_PAGE_CARD_BLOCK_INFO {

            view?.sendTrackCellsPageCardBlockInfo()

        } else if page == CardBlockPageReaction.CELLS_PAGE_CARD_BLOCK_TYPE {

            view?.sendTrackCellsPageCardBlockType()

        } else if page == CardBlockPageReaction.CELLS_PAGE_BLOCK_CARD_SUCCESS {

            view?.sendTrackCellsPageCardBlockSuccess()
        }
    }

    func setCurrentCard(card: CardBO) {
        cardBO = card
    }

    func getCurrentCard() -> CardBO? {
        return cardBO
    }

}
