//
//  NotRecognizedOperationViewController.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 18/7/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

class NotRecognizedOperationViewController: WalletCellsViewController {

    // MARK: Constants

    static let ID: String = String(describing: NotRecognizedOperationViewController.self)

    let trackerCustomerID = SessionDataManager.sessionDataInstance().customerID

    // MARK: Vars

    var cancelCardStore: CancelCardStore?

    var onOffStore: OnOffStore?

    static let heightToastView: CGFloat = 100.0

    // MARK: Life cycle

    override func viewDidLoad() {

        configure(appLifecycle: (UIApplication.shared.delegate as! AppDelegate).appLifeCycle!, id: NotRecognizedOperationViewController.ID)

        if let networkWorker = BusManager.sessionDataInstance.networkWorker {
            cancelCardStore = CancelCardStore(worker: networkWorker, host: ServerHostURL.getCardsURL())
            onOffStore = OnOffStore(worker: networkWorker, host: ServerHostURL.getCardsURL())
        }

        super.viewDidLoad()
        
//        TicketUtils.openDummyTicket()
    }

    override func initModel(withModel model: Model) {
        presenter.setModel(model: model)
    }

    deinit {
        DLog(message: "Deinitialized NotRecognizedOperationViewController")
    }

    func updateCurrentCardBO(card: CardBO) {

        if let presenter = presenter as? NotRecognizedOperationPresenter<NotRecognizedOperationViewController> {
            presenter.setCurrentCard(card: card)
        }
    }

    func onOffFinished(withError error: ErrorBO) {
        if let presenter = presenter as? NotRecognizedOperationPresenter<NotRecognizedOperationViewController> {
            presenter.onOffFailed(error: error)
        }
    }
    
    override func dismiss(completion: (() -> Void)? = nil) {
        
        super.dismiss(completion: completion)
        
    }

}

extension NotRecognizedOperationViewController: NotRecognizedOperationViewProtocol {

    func sendTrackCellsPageNotRecognizedOperation() {

        TrackerHelper.sharedInstance().trackCellsPage(withCustomerID: trackerCustomerID, pageName: TRACKER_HELPER_PAGE_NAME_NOT_RECOGNIZED_OPERATION)
    }

    func sendTrackCellsPageCardBlockInfo() {

        TrackerHelper.sharedInstance().trackCellsPage(withCustomerID: trackerCustomerID, pageName: TRACKER_HELPER_PAGE_NAME_CARD_BLOCK_INFO)
    }

    func sendTrackCellsPageCardBlockType() {

        TrackerHelper.sharedInstance().trackCellsPage(withCustomerID: trackerCustomerID, pageName: TRACKER_HELPER_PAGE_NAME_CARD_BLOCK_TYPE)
    }

    func sendTrackCellsPageCardBlockSuccess() {

        TrackerHelper.sharedInstance().trackCellsPage(withCustomerID: trackerCustomerID, pageName: TRACKER_HELPER_PAGE_NAME_CARD_BLOCK_SUCCESS)
    }

    func showToastSuccess(withSuccessMessage message: String, andBackgroundColor color: UIColor) {

        ToastManager.shared().showToast(withText: message, backgroundColor: .BBVADARKGREEN, height: CardsViewController.heightToastView)
    }
}

extension NotRecognizedOperationViewController: CellsProtocol {

    func startDoRequest(cellsDTO: CellsDTO) {

        if let cardCancelDTO = cellsDTO as? CardCancelDTO {

            presenter?.showLoading(completion: nil)
            cancelCardStore?.blockCardRequest(cardCancelDTO: cardCancelDTO)
        } else if let cardOnOffDTO = cellsDTO as? OnOffDTO {

            if let presenter = presenter as? NotRecognizedOperationPresenter<NotRecognizedOperationViewController> {
                if cardOnOffDTO.enabled != presenter.cardBO?.isCardOn() {
                    presenter.showLoading(completion: nil)
                    onOffStore?.updateCardStatusRequest(cardOnOffDTO)
                }
            }

        } else {

            return
        }
    }
}
