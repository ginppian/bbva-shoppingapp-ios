//
//  NotRecognizedOperationPageReaction.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 18/7/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

class NotRecognizedOperationPageReaction: CommonPageReaction {

    static let ROUTER_TAG: String = "not-recognized-operation-tag"
    static let ID = String(describing: NotRecognizedOperationPageReaction.self)
    static let EVENT_CARD_BLOCK_TYPE: ActionSpec<Void> = ActionSpec<Void>(id: "EVENT_CARD_BLOCK_TYPE")
    static let PARAMS_BLOCK_CARD_SUCCESS: ActionSpec<Any> = ActionSpec<Any>(id: "PARAMS_BLOCK_CARD_SUCCESS")
    static let EVENT_BLOCK_CARD_SUCCESS: ActionSpec<Void> = ActionSpec<Void>(id: "EVENT_BLOCK_CARD_SUCCESS")
    static let EVENT_NAV_TO_ROOT_CARDS: ActionSpec<Void> = ActionSpec<Void>(id: "EVENT_NAV_TO_ROOT_CARDS")

    // Channels
    static let REQUEST_CHANNEL = "cbt_cells_request_channel"
    static let INFO_CONFIRMATION_CHANNEL = "bcs_info_confirmation_channel"
    static let ON_OFF_CHANNEL_INFO = "poc_result_channel"
    static let onLoginChannel = "nro_login_channel"
    static let closePageChannel = "nro_navigation_channel_header"

    // Cells pages
    static let CELLS_PAGE_NOT_RECOGNIZED_OPERATION = "cardUnknowTransaction"
    static let CELLS_PAGE_BLOCK_CARD_SUCCESS = "cardBlockSuccess"
    static let CELLS_PAGE_CARD_BLOCK_TYPE = "cardBlockNormalReason"
    static let CELLS_PAGE_CARD_BLOCK_INFO = "cardBlockNormal"

    let notRecognizedOperationUtils = NotRecognizedOperationUtils()
    
    override func configure() {

        super.configure()
        
        pageLoaded()

        cardBlockTypeAction()

        registerReactionToReadChannelOnOffInformation()

        finishedCardBlockRequest()

        finishedCardBlockErrorRequest()

        paramsBlockCardSuccess()

        navigateBlockCardSuccess()

        skipNavigation()

        cardOnOffSuccessRequest()

        cardOnOffErrorRequest()

        registerToReadReactionForCurrentCard()

        updateCardChannel()

        registerToReactToCellLoginChannel()

        registerToReactToNavigateToCardsNotification()

        closePage()
        
    }

    func updateCardChannel() {

        let channel: Channel = Channel<Any>(name: CardsPageReaction.CARD_UPDATED_CHANNEL)

        _ = writeOn(channel: channel)
            .when(componentID: NotRecognizedOperationPageReaction.ROUTER_TAG)
            .doAction(actionSpec: CardsPageReaction.EVENT_CARD_UPDATED)
    }

    func pageLoaded() {

        _ = reactTo(channel: PageReactionConstants.CHANNEL_HIDDEN_VIEW_NATIVE)?
            .then()
            .inside(componentID: NotRecognizedOperationViewController.ID, component: NotRecognizedOperationViewController.self)
            .doReactionAndClearChannel { controller, page in

                if let page = page {
                    controller.registerOmniture(page: page)
                }
            }
    }

    func cardBlockTypeAction() {

        let channel: JsonChannel = JsonChannel(NotRecognizedOperationPageReaction.REQUEST_CHANNEL)

        _ = reactTo(channel: channel)?
            .then()
            .inside(componentID: NotRecognizedOperationViewController.ID, component: NotRecognizedOperationViewController.self)
            .doReactionAndClearChannel(reactionExecution: { component, param in

                let cardBlockCells = CellsRequestBO.deserialize(from: param as? [String: Any])
                let requestObjectCells = RequestBlockCardObjectCells.deserialize(from: cardBlockCells?.requestObject)
                if let requestObjectCells = requestObjectCells, let cardId = requestObjectCells.id, let typeBlock = requestObjectCells.typeBlock {
                    component.initModel(withModel: requestObjectCells as Model)
                    component.startDoRequest(cellsDTO: CardCancelDTO(cardId: cardId, typeBlock: typeBlock))
                }
            })
    }

    // MARK: - Block Card Request

    func finishedCardBlockRequest() {

        _ = when(id: CancelCardStore.ID, type: BlockCardDTO.self)
            .doAction(actionSpec: CancelCardStore.ON_BLOCK_SUCCESS)
            .then()
            .inside(componentID: NotRecognizedOperationViewController.ID, component: NotRecognizedOperationViewController.self)
            .doReaction { controller, param in

                if let blockCardDTO = param {
                    controller.finishDoRequestSuccess(modelBO: BlockCardBO(blockCardDTO: blockCardDTO))
                }
            }
    }

    func finishedCardBlockErrorRequest() {

        _ = when(id: CancelCardStore.ID, type: ErrorBO.self)
            .doAction(actionSpec: CancelCardStore.ON_CANCEL_ERROR)
            .then()
            .inside(componentID: NotRecognizedOperationViewController.ID, component: NotRecognizedOperationViewController.self)
            .doReaction { controller, param in

                controller.finishDoRequestError(error: param!)
            }
    }

    // MARK: - Block Card Success

    func paramsBlockCardSuccess() {

        let channel: JsonChannel = JsonChannel(NotRecognizedOperationPageReaction.INFO_CONFIRMATION_CHANNEL)

        _ = writeOn(channel: channel)
            .when(componentID: NotRecognizedOperationPageReaction.ROUTER_TAG)
            .doAction(actionSpec: NotRecognizedOperationPageReaction.PARAMS_BLOCK_CARD_SUCCESS)
    }

    func navigateBlockCardSuccess() {

        _ = when(id: NotRecognizedOperationPageReaction.ROUTER_TAG, type: Void.self)
            .doAction(actionSpec: NotRecognizedOperationPageReaction.EVENT_BLOCK_CARD_SUCCESS)
            .then()
            .inside(componentID: WebController.ID, component: WebController.self)
            .doReaction { controller, _ in

                controller.navigateTo(route: NotRecognizedOperationPageReaction.CELLS_PAGE_BLOCK_CARD_SUCCESS, payload: "")

            }
    }

    private func skipNavigation() {

        if let controller: WebController = ComponentManager.get(id: WebController.ID) {
            controller.skipNavigations(routes: [SkipTransitionDTO(from: NotRecognizedOperationPageReaction.CELLS_PAGE_BLOCK_CARD_SUCCESS, to: NotRecognizedOperationPageReaction.CELLS_PAGE_CARD_BLOCK_TYPE, skipHistory: true),
                                                SkipTransitionDTO(from: NotRecognizedOperationPageReaction.CELLS_PAGE_CARD_BLOCK_TYPE, to: NotRecognizedOperationPageReaction.CELLS_PAGE_CARD_BLOCK_INFO, skipHistory: true)
                ])
        }
    }

    // MARK: - Read From OnOff Channel

    func  registerReactionToReadChannelOnOffInformation() {

        let channel: JsonChannel = JsonChannel(NotRecognizedOperationPageReaction.ON_OFF_CHANNEL_INFO)

        _ = reactTo(channel: channel)?
            .then()
            .inside(componentID: NotRecognizedOperationViewController.ID, component: NotRecognizedOperationViewController.self)
            .doReactionAndClearChannel(reactionExecution: { component, param in

                if let jsonData = try? JSONSerialization.data(withJSONObject: param as Any, options: []) {
                    if let onOffObjectCells = try? JSONDecoder().decode(OnOffCardObjectCells.self, from: jsonData) {
                        component.initModel(withModel: onOffObjectCells as Model)
                        if let onOffDTO = OnOffDTO(cardId: onOffObjectCells.id, enabled: onOffObjectCells.enabled) {
                            component.startDoRequest(cellsDTO: onOffDTO)
                        }
                    }
                }
            })
    }

    // MARK: - OnOff Response

    func cardOnOffSuccessRequest() {

        _ = when(id: OnOffStore.ID, type: OnOffResponseDTO.self)
            .doAction(actionSpec: OnOffStore.ON_UPDATE_SUCCESS)
            .then()
            .inside(componentID: NotRecognizedOperationViewController.ID, component: NotRecognizedOperationViewController.self)
            .doReaction { controller, param in
                if let onOffResponseDTO = param {
                    controller.finishDoRequestSuccess(modelBO: OnOffResponseBO(onOffResponseDTO: onOffResponseDTO))
                }
            }
    }

    func cardOnOffErrorRequest() {

        _ = when(id: OnOffStore.ID, type: ErrorBO.self)
            .doAction(actionSpec: OnOffStore.ON_UPDATE_ERROR)
            .then()
            .inside(componentID: NotRecognizedOperationViewController.ID, component: NotRecognizedOperationViewController.self)
            .doReaction { controller, param in
                controller.onOffFinished(withError: param!)

            }
    }

    func registerToReadReactionForCurrentCard() {

        let channel: Channel = Channel<Any>(name: TransactionDetailPageReaction.CURRENT_CARD_INFORMATION_CHANNEL)

        _ = reactTo(channel: channel)?
            .then()
            .inside(componentID: NotRecognizedOperationViewController.ID, component: NotRecognizedOperationViewController.self)
            .doReactionAndClearChannel(reactionExecution: { component, param in
                if let card = param as? CardBO {
                    component.updateCurrentCardBO(card: card)
                }
            })
    }

    func registerToReactToCellLoginChannel() {

        let channel: JsonChannel = JsonChannel(NotRecognizedOperationPageReaction.onLoginChannel)

        _ = reactTo(channel: channel)?
            .then()
            .inside(componentID: NotRecognizedOperationPageReaction.ROUTER_TAG, component: NotRecognizedOperationPresenter<NotRecognizedOperationViewController>.self)
            .doReactionAndClearChannel { notRecogizedPresenter, _ in

                notRecogizedPresenter.navigateToWallet()
        }
    }

    func registerToReactToNavigateToCardsNotification() {

        _ = when(id: NotRecognizedOperationPageReaction.ROUTER_TAG, type: Void.self)
            .doAction(actionSpec: NotRecognizedOperationPageReaction.EVENT_NAV_TO_ROOT_CARDS)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, _ in

                router.navigateToRootCards()
        }
    }
    
    func closePage() {
        
        let channel: JsonChannel = JsonChannel(NotRecognizedOperationPageReaction.closePageChannel)

        _ = reactTo(channel: channel)?
            .then()
            .inside(componentID: NotRecognizedOperationViewController.ID, component: NotRecognizedOperationViewController.self)
            .doReactionAndClearChannel { controller, _ in
                
                self.notRecognizedOperationUtils.startFindNotRecognizedOperationViewController(close: true)

                if !self.notRecognizedOperationUtils.foundCellsViewControllerPresented {
                    
                    if self.notRecognizedOperationUtils.foundCellsViewControllerPushed {
                        controller.dismiss(completion: nil)
                    }
                    
                    if let webController: WebController = ComponentManager.get(id: WebController.ID) {
                        webController.sendOnBack()
                    }
                }
            }
    }
}
