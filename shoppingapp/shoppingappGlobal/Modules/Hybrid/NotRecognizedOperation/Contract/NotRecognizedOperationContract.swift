//
//  NotRecognizedOperationContract.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 18/7/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol NotRecognizedOperationViewProtocol: ViewProtocol {

    func sendTrackCellsPageNotRecognizedOperation()
    func sendTrackCellsPageCardBlockInfo()
    func sendTrackCellsPageCardBlockType()
    func sendTrackCellsPageCardBlockSuccess()
    func showToastSuccess(withSuccessMessage message: String, andBackgroundColor color: UIColor)

}

protocol NotRecognizedOperationPresenterProtocol: CellsPresenterProtocol {

    func onOffFailed(error: ErrorBO)
    func setCurrentCard(card: CardBO)
    func getCurrentCard() -> CardBO?
}
