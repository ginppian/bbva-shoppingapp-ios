//
//  CardActivationPresenter.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 17/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import RxSwift
import CellsNativeComponents

class CardActivationPresenter<T: CardActivationViewProtocol>: BasePresenter<T> {

    let disposeBag = DisposeBag()

    var activateCardSuccessObjectCells: ActivateCardSuccessObjectCells?
    var cardOperationInfo: CardOperationInfo?

    var success = false

    required init() {
        super.init(tag: CardActivationPageReaction.ROUTER_TAG)
    }

    override init(busManager: BusManager) {

        super.init(busManager: busManager)
        routerTag = CardActivationPageReaction.ROUTER_TAG
    }

    override func setModel(model: Model) {
        if let error = model as? ErrorBO {
            self.errorBO = error
        }
    }

}

extension CardActivationPresenter: CellsPresenterProtocol {
    func showPageSuccess(modelBO: ModelBO? = nil) {

        if activateCardSuccessObjectCells != nil {

            success = true

            if let activateCardSuccessObjectCellsJson = activateCardSuccessObjectCells?.toJSON() {
                busManager.publishData(tag: CardActivationPageReaction.ROUTER_TAG, CardActivationPageReaction.PARAMS_ACTIVATE_CARD_SUCCESS, activateCardSuccessObjectCellsJson)
            }

        }
        busManager.navigateScreen(tag: CardActivationPageReaction.ROUTER_TAG, CardActivationPageReaction.EVENT_CARD_ACTIVATED_SUCCESS, Void())

        if let cardOperationInfo = cardOperationInfo {
            busManager.publishData(tag: CardActivationPageReaction.ROUTER_TAG, CardsPageReaction.PARAMS_OPERATION_CARD_INFO_SUCCESS, cardOperationInfo)
        }
    }

    func registerOmniture(page: String) {
        if page == CardActivationPageReaction.CELLS_PAGE_ACTIVATE_CARD {
            TrackerHelper.sharedInstance().trackCellsPageActivateCard(withCustomerID: SessionDataManager.sessionDataInstance().customerID, pageName: "tarjetas:activacion tarjeta:1 activar")

        } else if page == CardActivationPageReaction.CELLS_PAGE_ACTIVATE_CARD_SUCCESS {
            TrackerHelper.sharedInstance().trackCellsPageSuccessActivation(withCustomerID: SessionDataManager.sessionDataInstance().customerID, pageName: "tarjetas:activacion tarjeta:pagina exitosa")
        }
    }

    func viewWasDismissed() {

        if success {
            busManager.notify(tag: CardsPageReaction.ROUTER_TAG, CardsPageReaction.NEEDED_CARDS_REFRESH)
        }

    }
}
