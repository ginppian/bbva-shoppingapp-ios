//
//  CardActivationViewController.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 13/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit
import CellsNativeCore
import CellsNativeComponents

class CardActivationViewController: WalletCellsViewController {

    // MARK: Constants

    static let ID = String(describing: CardActivationViewController.self)

    // MARK: Vars

    var activateCardStore: ActivateCardStore?

    // MARK: life cycle

    override func viewDidLoad() {
        configure(appLifecycle: (UIApplication.shared.delegate as! AppDelegate).appLifeCycle!, id: CardActivationViewController.ID)

        if let networkWorker = BusManager.sessionDataInstance.networkWorker {
            activateCardStore = ActivateCardStore(worker: networkWorker, host: ServerHostURL.getCardsURL())
        }

        super.viewDidLoad()
    }

    override func dismiss(completion: (() -> Void)? = nil) {

        super.dismiss(completion: { [weak self] in
            if let presenter = self?.presenter as? CardActivationPresenter<CardActivationViewController> {
                presenter.viewWasDismissed()
            }
        })
    }
}

extension CardActivationViewController: CellsProtocol {

    func startDoRequest(cellsDTO: CellsDTO) {

        guard let cardActivatedCTO = cellsDTO as? CardActivateDTO else {
            return
        }

        presenter?.showLoading(completion: nil)
        activateCardStore?.updateCardActivation(cardActivateDTO: cardActivatedCTO)
    }

}

extension CardActivationViewController: CardActivationViewProtocol {

}
