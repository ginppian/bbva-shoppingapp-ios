//
//  CardActivationContract.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 17/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit

protocol CardActivationViewProtocol: ViewProtocol {

}

protocol CardActivationPresenterProtocol: CellsProtocol {

    func viewWasDismissed()
}
