//
//  CardActivationPageReaction.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 13/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

class CardActivationPageReaction: CommonPageReaction {

    static let ROUTER_TAG: String = "card-activation-tag"

    static let EVENT_CARD_ACTIVATED_SUCCESS: ActionSpec<Void> = ActionSpec<Void>(id: "EVENT_CARD_ACTIVATED_SUCCESS")

    static let PARAMS_ACTIVATE_CARD_SUCCESS: ActionSpec<Any> = ActionSpec<Any>(id: "PARAMS_ACTIVATE_CARD_SUCCESS")

    // Channels

    static let REQUEST_CHANNEL = "ac_cells_request_channel"

    static let INFO_CONFIRMATION_CHANNEL = "acs_info_confirmation_channel"

    // Cells pages

    static let CELLS_PAGE_ACTIVATE_CARD = "cardActivate"

    static let CELLS_PAGE_ACTIVATE_CARD_SUCCESS = "cardActivateSuccess"

    override func configure() {

        super.configure()
        
        paramsCardActivation()

        navigateCardActivationSuccess()

        navigateCardActivationError()

        finishCardActivationRequest()

        paramsActivateCardSuccess()

        skipNavigation()

        pageLoaded()

        cardDataSelected()
        registerToReadOperationCardInfoChannel()
        paramsOperationBlockCardSuccess()

    }

    func cardDataSelected() {

        _ = reactTo(channel: CardsPageReaction.CHANNEL_SELECT_CARD_ACTIVATE)?
            .then()
            .inside(componentID: CardActivationPageReaction.ROUTER_TAG, component: CardActivationPresenter<CardActivationViewController>.self)
            .doReactionAndClearChannel { component, param in

                let activateCardSuccessObjectCells = ActivateCardSuccessObjectCells.deserialize(from: param as? [String: Any])
                component.activateCardSuccessObjectCells = activateCardSuccessObjectCells

            }

    }

    func pageLoaded() {

        _ = reactTo(channel: PageReactionConstants.CHANNEL_HIDDEN_VIEW_NATIVE)?
            .then()
            .inside(componentID: CardActivationViewController.ID, component: CardActivationViewController.self)
            .doReactionAndClearChannel { controller, page in

                if let page = page {
                    controller.registerOmniture(page: page)
                }
            }

    }

    func paramsCardActivation() {

        let channel: JsonChannel = JsonChannel(CardActivationPageReaction.REQUEST_CHANNEL)

        _ = reactTo(channel: channel)?
            .then()
            .inside(componentID: CardActivationViewController.ID, component: CardActivationViewController.self)
            .doReactionAndClearChannel(reactionExecution: { component, param in
                let cardActivationCells = CellsRequestBO.deserialize(from: param as? [String: Any])
                let requestObjectCells = RequestActivateCardObjectCells.deserialize(from: cardActivationCells?.requestObject)
                if let requestObjectCells = requestObjectCells {
                    component.startDoRequest(cellsDTO: CardActivateDTO(cardId: requestObjectCells.id))
                }
            })
    }

    func finishCardActivationRequest() {

        _ = when(id: ActivateCardStore.ID, type: Bool.self)
            .doAction(actionSpec: ActivateCardStore.ON_ACTIVATE_SUCCESS)
            .then()
            .inside(componentID: CardActivationViewController.ID, component: CardActivationViewController.self)
            .doReaction { controller, _ in

                controller.finishDoRequestSuccess()

            }

    }

    func navigateCardActivationSuccess() {

        _ = when(id: CardActivationPageReaction.ROUTER_TAG, type: Void.self)
            .doAction(actionSpec: CardActivationPageReaction.EVENT_CARD_ACTIVATED_SUCCESS)
            .then()
            .inside(componentID: WebController.ID, component: WebController.self)
            .doReaction { controller, _ in

                controller.navigateTo(route: CardActivationPageReaction.CELLS_PAGE_ACTIVATE_CARD_SUCCESS, payload: "")

            }

    }

    func navigateCardActivationError() {

        _ = when(id: ActivateCardStore.ID, type: ErrorBO.self)
            .doAction(actionSpec: ActivateCardStore.ON_ACTIVATE_ERROR)
            .then()
            .inside(componentID: CardActivationViewController.ID, component: CardActivationViewController.self)
            .doReaction { controller, param in

                controller.finishDoRequestError(error: param!)
            }
    }

    func paramsActivateCardSuccess() {

        let channel: JsonChannel = JsonChannel(CardActivationPageReaction.INFO_CONFIRMATION_CHANNEL)

        _ = writeOn(channel: channel)
            .when(componentID: CardActivationPageReaction.ROUTER_TAG)
            .doAction(actionSpec: CardActivationPageReaction.PARAMS_ACTIVATE_CARD_SUCCESS)
    }

    fileprivate func skipNavigation() {

        if let controller: WebController = ComponentManager.get(id: WebController.ID) {
            controller.skipNavigations(routes: [SkipTransitionDTO(from: CardActivationPageReaction.CELLS_PAGE_ACTIVATE_CARD_SUCCESS, to: CardActivationPageReaction.CELLS_PAGE_ACTIVATE_CARD, skipHistory: true)])
        }
    }
    func registerToReadOperationCardInfoChannel() {

        _ = reactTo(channel: CardsPageReaction.OPERATION_CARD_INFO_CHANNEL)?
            .then()
            .inside(componentID: CardActivationPageReaction.ROUTER_TAG, component: CardActivationPresenter<CardActivationViewController>.self)
            .doReactionAndClearChannel(reactionExecution: { component, cardOperationInfo in
                component.cardOperationInfo = cardOperationInfo
            })
    }

    func paramsOperationBlockCardSuccess() {

        _ = writeOn(channel: CardsPageReaction.OPERATION_CARD_INFO_SUCCESS_CHANNEL)
            .when(componentID: CardActivationPageReaction.ROUTER_TAG)
            .doAction(actionSpec: CardsPageReaction.PARAMS_OPERATION_CARD_INFO_SUCCESS)
    }
}
