//
//  CardCancelationViewController.swift
//  shoppingapp
//
//  Created by Marcos on 24/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit
import CellsNativeCore
import CellsNativeComponents

class CardCancelationViewController: WalletCellsViewController {

    // MARK: Constants

    static let ID: String = String(describing: CardCancelationViewController.self)

    let trackerCustomerID = SessionDataManager.sessionDataInstance().customerID

    // MARK: Vars

    var cancelCardStore: CancelCardStore?

    // MARK: life cycle

    override func viewDidLoad() {
        configure(appLifecycle: (UIApplication.shared.delegate as! AppDelegate).appLifeCycle!, id: CardCancelationViewController.ID)

        if let networkWorker = BusManager.sessionDataInstance.networkWorker {
            cancelCardStore = CancelCardStore(worker: networkWorker, host: ServerHostURL.getCardsURL())
        }

        super.viewDidLoad()

    }

    override func initModel(withModel model: Model) {
        presenter.setModel(model: model)
    }

    deinit {
        DLog(message: "Deinitialized CardCancelationViewController")
    }

    func updateCurrentCardBO(card: CardBO) {

        if let presenter = presenter as? CardCancelationPresenter<CardCancelationViewController> {
            presenter.setCurrentCard(card: card)
        }
    }

    override func dismiss(completion: (() -> Void)? = nil) {

        super.dismiss(completion: { [weak self] in
            if let presenter = self?.presenter as? CardCancelationPresenter<CardCancelationViewController> {
                presenter.viewWasDismissed()
            }
        })
    }

}

extension CardCancelationViewController: CardCancelationViewProtocol {

    func sendTrackCellsPageCardCancelationInfo() {

        TrackerHelper.sharedInstance().trackCellsPage(withCustomerID: trackerCustomerID, pageName: TRACKER_HELPER_PAGE_NAME_CARD_CANCELATION_INFO)
    }

    func sendTrackCellsPageCardCancelationSuccess() {

        TrackerHelper.sharedInstance().trackCellsPage(withCustomerID: trackerCustomerID, pageName: TRACKER_HELPER_PAGE_NAME_CARD_CANCELATION_SUCCESS)
    }

}

extension CardCancelationViewController: CellsProtocol {

    func startDoRequest(cellsDTO: CellsDTO) {
        guard let cardCancelDTO = cellsDTO as? CardCancelDTO else {
            return
        }

        presenter?.showLoading(completion: nil)
        cancelCardStore?.cancelCardRequest(cardCancelDTO: cardCancelDTO)
    }

    func finishDoRequestError(error: ErrorBO) {

        presenter.hideLoading(completion: { [weak self] in
            self?.presenter.setModel(model: error)
            self?.presenter.checkError()
        })
    }
}
