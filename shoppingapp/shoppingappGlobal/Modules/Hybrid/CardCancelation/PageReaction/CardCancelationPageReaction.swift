//
//  CardCancelationPageReaction.swift
//  shoppingapp
//
//  Created by Marcos on 24/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

final class CardCancelationPageReaction: CommonPageReaction {

    // MARK: Constants

    static let ROUTER_TAG: String = "card-cancelation-tag"

    static let ID = String(describing: CardCancelationPageReaction.self)

    static let EVENT_CARD_CANCEL_SUCCESS: ActionSpec<Void> = ActionSpec<Void>(id: "EVENT_CARD_CANCEL_SUCCESS")
    static let PARAMS_CANCEL_CARD_SUCCESS: ActionSpec<Any> = ActionSpec<Any>(id: "PARAMS_CANCEL_CARD_SUCCESS")

    // Channels

    static let REQUEST_CHANNEL = "cci_cells_request_channel"

    static let INFO_CONFIRMATION_CHANNEL = "ccs_info_confirmation_channel"

    // Cells pages

    static let CELLS_PAGE_CANCEL_CARD_SUCCESS = "cardCancelSuccess"

    static let CELLS_PAGE_CARD_CANCEL_INFO = "cardBlockDigital"

    override func configure() {

        super.configure()
        
        pageLoaded()
        cancelCardAction()
        navigateCardCancelationSuccess()
        finishCardCancelationRequest()
        navigateCardCancelationError()
        paramsCancelCardSuccess()
        registerToReadCurrentCard()
        registerToReadOperationCardInfoChannel()
        paramsOperationBlockCardSuccess()
        skipNavigation()

    }

    func pageLoaded() {

        _ = reactTo(channel: PageReactionConstants.CHANNEL_HIDDEN_VIEW_NATIVE)?
            .then()
            .inside(componentID: CardCancelationViewController.ID, component: CardCancelationViewController.self)
            .doReactionAndClearChannel { controller, page in

                if let page = page {
                    controller.registerOmniture(page: page)
                }
            }

    }

    func  cancelCardAction() {
        let channel: JsonChannel = JsonChannel(CardCancelationPageReaction.REQUEST_CHANNEL)

        _ = reactTo(channel: channel)?
            .then()
            .inside(componentID: CardCancelationViewController.ID, component: CardCancelationViewController.self)
            .doReactionAndClearChannel(reactionExecution: { component, param in
                let cardCancelationCells = CellsRequestBO.deserialize(from: param as? [String: Any])

                if let requestObject = cardCancelationCells?.requestObject, let json = requestObject.data(using: .utf8) {

                    let requestCancelCardObjectCells = try? JSONDecoder().decode(RequestCancelCardObjectCells.self, from: json)

                    if let requestCancelCardObjectCells = requestCancelCardObjectCells {
                        component.initModel(withModel: requestCancelCardObjectCells as Model)
                        component.startDoRequest(cellsDTO: CardCancelDTO(cardId: requestCancelCardObjectCells.id))
                    }
                }
            })
    }

    func finishCardCancelationRequest() {
        _ = when(id: CancelCardStore.ID, type: BlockCardDTO.self)
            .doAction(actionSpec: CancelCardStore.ON_CANCEL_SUCCESS)
            .then()
            .inside(componentID: CardCancelationViewController.ID, component: CellsProtocol.self)
            .doReaction { controller, param in
                if let blockCardDTO = param {
                    controller.finishDoRequestSuccess(modelBO: BlockCardBO(blockCardDTO: blockCardDTO))
                }
            }

    }

    func navigateCardCancelationSuccess() {
        _ = when(id: CardCancelationPageReaction.ROUTER_TAG, type: Void.self)
            .doAction(actionSpec: CardCancelationPageReaction.EVENT_CARD_CANCEL_SUCCESS)
            .then()
            .inside(componentID: WebController.ID, component: WebController.self)
            .doReaction { controller, _ in

                controller.navigateTo(route: CardCancelationPageReaction.CELLS_PAGE_CANCEL_CARD_SUCCESS, payload: "")

            }
    }

    func navigateCardCancelationError() {
        _ = when(id: CancelCardStore.ID, type: ErrorBO.self)
            .doAction(actionSpec: CancelCardStore.ON_CANCEL_ERROR)
            .then()
            .inside(componentID: CardCancelationViewController.ID, component: CellsProtocol.self)
            .doReaction { controller, param in
                controller.finishDoRequestError(error: param!)
            }
    }

    func paramsCancelCardSuccess() {
        let channel: JsonChannel = JsonChannel(CardCancelationPageReaction.INFO_CONFIRMATION_CHANNEL)

        _ = writeOn(channel: channel)
            .when(componentID: CardCancelationPageReaction.ROUTER_TAG)
            .doAction(actionSpec: CardCancelationPageReaction.PARAMS_CANCEL_CARD_SUCCESS)
    }

    private func skipNavigation() {
        if let controller: WebController = ComponentManager.get(id: WebController.ID) {
            controller.skipNavigations(routes: [SkipTransitionDTO(from: CardCancelationPageReaction.CELLS_PAGE_CANCEL_CARD_SUCCESS, to: CardCancelationPageReaction.CELLS_PAGE_CARD_CANCEL_INFO, skipHistory: true)])
        }
    }

    func registerToReadCurrentCard() {

        let channel: Channel = Channel<Any>(name: CardsPageReaction.CARD_BO_CHANNEL)

        _ = reactTo(channel: channel)?
            .then()
            .inside(componentID: CardCancelationViewController.ID, component: CardCancelationViewController.self)
            .doReactionAndClearChannel(reactionExecution: { component, param in
                if let card = param as? CardBO {
                    component.updateCurrentCardBO(card: card)
                }
            })
    }

    func registerToReadOperationCardInfoChannel() {

        _ = reactTo(channel: CardsPageReaction.OPERATION_CARD_INFO_CHANNEL)?
            .then()
            .inside(componentID: CardCancelationPageReaction.ROUTER_TAG, component: CardCancelationPresenter<CardCancelationViewController>.self)
            .doReactionAndClearChannel(reactionExecution: { component, cardOperationInfo in
                component.cardOperationInfo = cardOperationInfo
            })
    }

    func paramsOperationBlockCardSuccess() {

        _ = writeOn(channel: CardsPageReaction.OPERATION_CARD_INFO_SUCCESS_CHANNEL)
            .when(componentID: CardCancelationPageReaction.ROUTER_TAG)
            .doAction(actionSpec: CardsPageReaction.PARAMS_OPERATION_CARD_INFO_SUCCESS)
    }

}
