//
//  CardCancelationContract.swift
//  shoppingapp
//
//  Created by Marcos on 24/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol CardCancelationViewProtocol: ViewProtocol {

    func sendTrackCellsPageCardCancelationInfo()
    func sendTrackCellsPageCardCancelationSuccess()
}

protocol CardCancelationPresenterProtocol: CellsPresenterProtocol {

    func viewWasDismissed()
}
