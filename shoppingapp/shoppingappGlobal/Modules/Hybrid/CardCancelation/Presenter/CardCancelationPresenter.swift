//
//  CardCancelationPresenter.swift
//  shoppingapp
//
//  Created by Marcos on 24/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

import RxSwift
import CellsNativeComponents

class CardCancelationPresenter<T: CardCancelationViewProtocol>: BasePresenter<T> {

    let disposeBag = DisposeBag()

    var cancelCardObjectCells: RequestCancelCardObjectCells?
    var cardBO: CardBO?
    var cardOperationInfo: CardOperationInfo?

    private var successResult: BlockCardSuccess?

    required init() {
        super.init(tag: CardCancelationPageReaction.ROUTER_TAG)
    }

    override init(busManager: BusManager) {

        super.init(busManager: busManager)
        routerTag = CardCancelationPageReaction.ROUTER_TAG
    }

    override func setModel(model: Model) {
        if let error = model as? ErrorBO {
            self.errorBO = error
        } else if let cardCancel = model as? RequestCancelCardObjectCells {
            self.cancelCardObjectCells = cardCancel
        }
    }

}

extension CardCancelationPresenter: CardCancelationPresenterProtocol {

    func showPageSuccess(modelBO: ModelBO?) {

        if let blockCardsBO = modelBO as? BlockCardBO, let cardNumber = cardBO?.number.suffix(4) {

            let date = Date.getDateFormatForCellsModule(withDate: blockCardsBO.blockDate)
            let folio = blockCardsBO.reference

            let successResult = BlockCardSuccess(cardNumber: String(cardNumber), date: date, folio: folio)
            self.successResult = successResult

            if let blockCardJSON = successResult.toJSON() {

                busManager.publishData(tag: CardCancelationPageReaction.ROUTER_TAG, CardCancelationPageReaction.PARAMS_CANCEL_CARD_SUCCESS, blockCardJSON)

                busManager.navigateScreen(tag: CardCancelationPageReaction.ROUTER_TAG, CardCancelationPageReaction.EVENT_CARD_CANCEL_SUCCESS, Void())

                if let cardOperationInfo = cardOperationInfo {
                    busManager.publishData(tag: CardCancelationPageReaction.ROUTER_TAG, CardsPageReaction.PARAMS_OPERATION_CARD_INFO_SUCCESS, cardOperationInfo)
                }
            }

        }
    }

    func registerOmniture(page: String) {

        if page == CardCancelationPageReaction.CELLS_PAGE_CARD_CANCEL_INFO {

            view?.sendTrackCellsPageCardCancelationInfo()

        } else if page == CardCancelationPageReaction.CELLS_PAGE_CANCEL_CARD_SUCCESS {

            view?.sendTrackCellsPageCardCancelationSuccess()
        }
    }

    func setCurrentCard(card: CardBO) {
        cardBO = card
    }

    func viewWasDismissed() {

        if successResult != nil {
            busManager.notify(tag: CardsPageReaction.ROUTER_TAG, CardsPageReaction.NEEDED_CARDS_REFRESH)
        }

    }
}
