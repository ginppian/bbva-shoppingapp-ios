//
//  CardGenerationPageReaction.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 13/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

class CardGenerationPageReaction: CommonPageReaction {

    static let ROUTER_TAG: String = "card-generation-tag"

    static let EVENT_CARD_GENERATED_SUCCESS: ActionSpec<Void> = ActionSpec<Void>(id: "EVENT_CARD_GENERATED_SUCCESS")
    static let PARAMS_GENERATE_CARD_SUCCESS: ActionSpec<Any> = ActionSpec<Any>(id: "PARAMS_GENERATE_CARD_SUCCESS")

    // Channels

    static let TERMS_CONDITIONS_CHANNEL = "tcc_termsconditions_generation_channel"
    static let INFO_CONFIRMATION_CHANNEL = "gcs_info_confirmation_channel"

    // Cells pages

    static let CELLS_PAGE_GENERATE_CARD = "cardGenerate"
    static let CELLS_PAGE_TERMS_CONDITIONS = "cardGenerateTerms"
    static let CELLS_PAGE_GENERATE_CARD_SUCCESS = "cardGenerateSuccess"

    override func configure() {

        super.configure()
        
        pageLoaded()

        paramsCardGeneration()

        navigateCardGenerationSuccess()

        navigateCardGenerationError()

        finishCardGenerationRequest()

        paramsGenerateCardSuccess()

        registerToReadOperationCardInfoChannel()
        paramsOperationBlockCardSuccess()

        skipNavigation()

    }

    func pageLoaded() {

        _ = reactTo(channel: PageReactionConstants.CHANNEL_HIDDEN_VIEW_NATIVE)?
            .then()
            .inside(componentID: CardGenerationViewController.ID, component: CardGenerationViewController.self)
            .doReactionAndClearChannel { controller, page in

                if let page = page {
                    controller.registerOmniture(page: page)
                }
            }

    }

    func paramsCardGeneration() {

        let channel: JsonChannel = JsonChannel(CardGenerationPageReaction.TERMS_CONDITIONS_CHANNEL)

        _ = reactTo(channel: channel)?
            .then()
            .inside(componentID: CardGenerationViewController.ID, component: CardGenerationViewController.self)
            .doReactionAndClearChannel(reactionExecution: { component, param in
                let card = CellsRequestBO.deserialize(from: param as? [String: Any])
                let requestObjectCells = RequestGenerateCardObjectCells.deserialize(from: card?.requestObject)
                if let requestObjectCells = requestObjectCells {
                    component.startDoRequest(cellsDTO: GenerateCardDTO(cardId: requestObjectCells.cardId, cardNumber: requestObjectCells.cardNumber, cardType: requestObjectCells.cardType))
                }
            })
    }

    func finishCardGenerationRequest() {

        _ = when(id: GenerateCardStore.identifier, type: GeneratedCardDTO.self)
            .doAction(actionSpec: GenerateCardStore.onGenerateSuccess)
            .then()
            .inside(componentID: CardGenerationViewController.ID, component: CardGenerationViewController.self)
            .doReaction { controller, param in

                if let generatedCardDTO = param {
                    controller.finishDoRequestSuccess(modelBO: GeneratedCardBO(generatedCardDTO: generatedCardDTO))
                }

            }

    }

    func navigateCardGenerationSuccess() {

        _ = when(id: CardGenerationPageReaction.ROUTER_TAG, type: Void.self)
            .doAction(actionSpec: CardGenerationPageReaction.EVENT_CARD_GENERATED_SUCCESS)
            .then()
            .inside(componentID: WebController.ID, component: WebController.self)
            .doReaction { controller, _ in

                controller.navigateTo(route: CardGenerationPageReaction.CELLS_PAGE_GENERATE_CARD_SUCCESS, payload: "")

            }

    }

    func navigateCardGenerationError() {

        _ = when(id: GenerateCardStore.identifier, type: ErrorBO.self)
            .doAction(actionSpec: GenerateCardStore.onGenerateError)
            .then()
            .inside(componentID: CardGenerationViewController.ID, component: CardGenerationViewController.self)
            .doReaction { controller, param in

                controller.finishDoRequestError(error: param!)
            }
    }

    func paramsGenerateCardSuccess() {

        let channel: JsonChannel = JsonChannel(CardGenerationPageReaction.INFO_CONFIRMATION_CHANNEL)

        _ = writeOn(channel: channel)
            .when(componentID: CardGenerationPageReaction.ROUTER_TAG)
            .doAction(actionSpec: CardGenerationPageReaction.PARAMS_GENERATE_CARD_SUCCESS)
    }

    fileprivate func skipNavigation() {

        if let controller: WebController = ComponentManager.get(id: WebController.ID) {
            controller.skipNavigations(routes: [SkipTransitionDTO(from: CardGenerationPageReaction.CELLS_PAGE_GENERATE_CARD_SUCCESS, to: CardGenerationPageReaction.CELLS_PAGE_TERMS_CONDITIONS, skipHistory: true), SkipTransitionDTO(from: CardGenerationPageReaction.CELLS_PAGE_TERMS_CONDITIONS, to: CardGenerationPageReaction.CELLS_PAGE_GENERATE_CARD, skipHistory: true)])
        }
    }

    func registerToReadOperationCardInfoChannel() {

        _ = reactTo(channel: CardsPageReaction.OPERATION_CARD_INFO_CHANNEL)?
            .then()
            .inside(componentID: CardGenerationPageReaction.ROUTER_TAG, component: CardGenerationPresenter<CardGenerationViewController>.self)
            .doReactionAndClearChannel(reactionExecution: { component, cardOperationInfo in
                component.cardOperationInfo = cardOperationInfo
            })
    }

    func paramsOperationBlockCardSuccess() {

        _ = writeOn(channel: CardsPageReaction.OPERATION_CARD_INFO_SUCCESS_CHANNEL)
            .when(componentID: CardGenerationPageReaction.ROUTER_TAG)
            .doAction(actionSpec: CardsPageReaction.PARAMS_OPERATION_CARD_INFO_SUCCESS)
    }
}
