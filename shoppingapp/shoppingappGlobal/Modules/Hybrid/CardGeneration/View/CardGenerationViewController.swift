//
//  CardGenerationViewController.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 13/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit
import CellsNativeCore
import CellsNativeComponents

class CardGenerationViewController: WalletCellsViewController {

    // MARK: Constants

    static let ID: String = String(describing: CardGenerationViewController.self)

    let trackerCustomerID = SessionDataManager.sessionDataInstance().customerID

    // MARK: Vars

    var generateCardStore: GenerateCardStore?

    // MARK: life cycle

    override func viewDidLoad() {
        configure(appLifecycle: (UIApplication.shared.delegate as! AppDelegate).appLifeCycle!, id: CardGenerationViewController.ID)

        if let networkWorker = BusManager.sessionDataInstance.networkWorker {
            generateCardStore = GenerateCardStore(worker: networkWorker, host: ServerHostURL.getCardsURL())
        }

        super.viewDidLoad()
    }

    override func dismiss(completion: (() -> Void)? = nil) {

        super.dismiss(completion: { [weak self] in
            if let presenter = self?.presenter as? CardGenerationPresenter<CardGenerationViewController> {
                presenter.viewWasDismissed()
            }
        })
    }

}

extension CardGenerationViewController: CardGenerationViewProtocol {

    func sendTrackCellsPageGenerateCard() {

        TrackerHelper.sharedInstance().trackCellsPage(withCustomerID: trackerCustomerID, pageName: TRACKER_HELPER_PAGE_NAME_GENERATE_CARD)
    }

    func sendTrackCellsPageTermsConditions() {

        TrackerHelper.sharedInstance().trackCellsPage(withCustomerID: trackerCustomerID, pageName: TRACKER_HELPER_PAGE_NAME_GENERATE_CARD_TERMS)
    }

    func sendTrackCellsPageGenerateCardSuccess() {

        TrackerHelper.sharedInstance().trackCellsPage(withCustomerID: trackerCustomerID, pageName: TRACKER_HELPER_PAGE_NAME_GENERATE_CARD_SUCCESS)
    }

}

extension CardGenerationViewController: CellsProtocol {

    func startDoRequest(cellsDTO: CellsDTO) {

        guard let generateCardDTO = cellsDTO as? GenerateCardDTO else {
            return
        }

        presenter?.showLoading(completion: nil)
        generateCardStore?.generateCardRequest(generateCardDTO: generateCardDTO)
    }

}
