//
//  CardGenerationPresenter.swift
//  shoppingappMX
//
//  Created by Javier Dominguez on 28/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import RxSwift
import CellsNativeComponents

class CardGenerationPresenter<T: CardGenerationViewProtocol>: BasePresenter<T> {

    let disposeBag = DisposeBag()

    var generatedCardBO: GeneratedCardBO?
    var cardOperationInfo: CardOperationInfo?

    required init() {
        super.init(tag: CardGenerationPageReaction.ROUTER_TAG)
    }

    override init(busManager: BusManager) {

        super.init(busManager: busManager)
        routerTag = CardGenerationPageReaction.ROUTER_TAG
    }

    override func setModel(model: Model) {
        if let error = model as? ErrorBO {
            self.errorBO = error
        }
    }

}

extension CardGenerationPresenter: CellsPresenterProtocol {

    func showPageSuccess(modelBO: ModelBO?) {

        generatedCardBO = modelBO as? GeneratedCardBO

        if let data = try? JSONEncoder().encode(generatedCardBO) {
            
            if let auxJSON = try? JSONSerialization.jsonObject(with: data, options: []), let generateCardJSON = auxJSON as? [String: Any] {
                
                busManager.publishData(tag: CardGenerationPageReaction.ROUTER_TAG, CardGenerationPageReaction.PARAMS_GENERATE_CARD_SUCCESS, generateCardJSON)
                
                busManager.navigateScreen(tag: CardGenerationPageReaction.ROUTER_TAG, CardGenerationPageReaction.EVENT_CARD_GENERATED_SUCCESS, Void())
                
                if let cardOperationInfo = cardOperationInfo {
                    busManager.publishData(tag: CardGenerationPageReaction.ROUTER_TAG, CardsPageReaction.PARAMS_OPERATION_CARD_INFO_SUCCESS, cardOperationInfo)
                }
            }
        }
    }

    func registerOmniture(page: String) {

        if page == CardGenerationPageReaction.CELLS_PAGE_GENERATE_CARD {

            view?.sendTrackCellsPageGenerateCard()

        } else if page == CardGenerationPageReaction.CELLS_PAGE_TERMS_CONDITIONS {

            view?.sendTrackCellsPageTermsConditions()

        } else if page == CardGenerationPageReaction.CELLS_PAGE_GENERATE_CARD_SUCCESS {
            
            busManager.publishData(tag: EventTracker.id, PageReactionConstants.NEW_TRACK_EVENT, Event(type: .generateDigitalCard))

            view?.sendTrackCellsPageGenerateCardSuccess()
        }
    }

    func viewWasDismissed() {

        if generatedCardBO != nil {
            busManager.notify(tag: CardsPageReaction.ROUTER_TAG, CardsPageReaction.NEEDED_CARDS_REFRESH)
        }

    }
}
