//
//  CardGenerationContract.swift
//  shoppingappMX
//
//  Created by Javier Dominguez on 28/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol CardGenerationViewProtocol: ViewProtocol {

    func sendTrackCellsPageGenerateCard()
    func sendTrackCellsPageTermsConditions()
    func sendTrackCellsPageGenerateCardSuccess()
}

protocol CardGenerationPresenterProtocol: CellsProtocol {

    func viewWasDismissed()
}
