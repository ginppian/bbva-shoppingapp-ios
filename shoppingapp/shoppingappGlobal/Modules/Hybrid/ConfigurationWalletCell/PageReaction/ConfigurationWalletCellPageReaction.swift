//
//  SideMenuWalletCellPageReaction.swift
//  shoppingapp
//
//  Created by Marcos on 7/6/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

final class ConfigurationWalletCellPageReaction: PageReaction {

    // MARK: Constants

    static let ROUTER_TAG = "configuration-wallet-cell-tag"

    static let ID = String(describing: ConfigurationWalletCellPageReaction.self)
    
    // MARK: Channels
    
    static let BALANCES_AVAILABLE_CHANNEL = "cmsb_result_channel"
    
    static let NOTIFICATIONS_AVAILABLE_CHANNEL = "cmsn_result_channel"

    static let VIBRATION_ENABLED_CHANNEL = "cmsn_vibration_enabled"
    
    static let MENU_INFORMATION_CHANNEL = "cm_info_channel"
    
    static let CONFIG_MENU_RESULT_CHANNEL = "cm_result_channel"
    
    static let CONFIG_DEVICE_NOTIFICATIONS_CHANNEL = "cdn_result_channel"
    
    static let CLOSE_CONFIG_DEVICE_NOTIFICATIONS_CHANNEL = "cdn_close_channel"
    
    // MARK: Events

    static let EVENT_CONFIGURATION_CHANGED: ActionSpec<Void> = ActionSpec<Void>(id: "EVENT_CONFIGURATION_CHANGED")

    static let PARAMS_CONFIGURATION_CHANGED: ActionSpec<Any> = ActionSpec<Any>(id: "PARAMS_CONFIGURATION_CHANGED")
    
    static let EVENT_NAV_TO_CONFIG_DEVICE_NOTIFICATIONS: ActionSpec<Void> = ActionSpec<Void>(id: "EVENT_NAV_TO_CONFIG_DEVICE_NOTIFICATIONS")
    
    static let EVENT_NAV_TO_CONFIG_MENU_SECTION_NOTIFICATIONS: ActionSpec<Void> = ActionSpec<Void>(id: "EVENT_NAV_TO_CONFIG_MENU_SECTION_NOTIFICATIONS")
    
    static let EVENT_CLOSE_CONFIG_DEVICE_NOTIFICATIONS: ActionSpec<Void> = ActionSpec<Void>(id: "EVENT_CLOSE_CONFIG_DEVICE_NOTIFICATIONS")

    // MARK: Cells pages
    
    static let CELLS_PAGE_CONFIG_DEVICE_NOTIFICATIONS = "notificationsPermissions"
    
    static let CELLS_PAGE_CONFIG_MENU_SECTION_NOTIFICATIONS = "settingsNotifications"
    
    enum ConfigItemIds: String {
        
        case kConfigBalances = "balances"
        case kConfigNotifications = "notifications"
    }

    override func configure() {

        pageLoaded()
        pageUnLoaded()
        registerReactionForCloseConfigDeviceNotifications()
        
        registerReactionForReadChannelConfigMenu()
        registerReactionForReadChannelBalancesAvailable()
        registerReactionForReadChannelNotificationsAvailable()
        registerReactionForReadChannelVibrationEnabled()
        registerReactionForReadChannelConfigDeviceNotifications()
        registerReactionForReadChannelCloseConfigDeviceNotifications()
        
        registerReactionForWriteInChannelParamsConfiguration()
        
        registerReactionForNavigateToConfigDeviceNotifications()
        registerReactionForNavigateToConfigMenuSectionNotifications()
        
        reactTokenChanged()
        reactTokenFailed()
        
        registerToLisenRegisterNotificationTokenSuccessRequest()
        registerToLisenRegisterNotificationTokenErrorRequest()
    }

    // MARK: - General
    
    func pageLoaded() {

        _ = reactTo(channel: PageReactionConstants.CHANNEL_HIDDEN_VIEW_NATIVE)?
            .then()
            .inside(componentID: ConfigurationWalletCellViewController.ID, component: ConfigurationWalletCellViewController.self)
            .doReactionAndClearChannel { controller, page in

                if let page = page {
                    controller.registerOmniture(page: page)
                }
            }
    }
    
    func pageUnLoaded() {
        
        _ = reactTo(channel: WebController.ON_PAGE_UNLOADED_CHANNEL)?
            .then()
            .inside(componentID: ConfigurationWalletCellPageReaction.ROUTER_TAG, component: ConfigurationWalletCellPresenter<ConfigurationWalletCellViewController>.self)
            .doReactionAndClearChannel { configurationWalletCellPresenter, page in
                
                if let page = page {
                    configurationWalletCellPresenter.pageCellUnload(withPage: page)
                }
        }
    }
    
    func registerReactionForCloseConfigDeviceNotifications() {
        
        _ = when(id: ConfigurationWalletCellPageReaction.ROUTER_TAG, type: Void.self)
            .doAction(actionSpec: ConfigurationWalletCellPageReaction.EVENT_CLOSE_CONFIG_DEVICE_NOTIFICATIONS)
            .then()
            .inside(componentID: WebController.ID, component: WebController.self)
            .doReactionAndClearChannel(reactionExecution: { controller, _ in
                controller.sendOnBack()
            })
    }
    
    // MARK: - ReactionsForReadChannel
    
    func registerReactionForReadChannelConfigMenu() {
        
        let channel: JsonChannel = JsonChannel(ConfigurationWalletCellPageReaction.CONFIG_MENU_RESULT_CHANNEL)
        
        _ = reactTo(channel: channel)?
            .then()
            .inside(componentID: ConfigurationWalletCellPageReaction.ROUTER_TAG, component: ConfigurationWalletCellPresenter<ConfigurationWalletCellViewController>.self)
            .doReactionAndClearChannel(reactionExecution: { configurationWalletCellPresenter, param in
                
                let configureObjectCells = ConfigureObjectCells.deserialize(from: param as? [String: Any])
                
                guard let configuration = configureObjectCells, let itemId = configuration.itemId, let enabled = configuration.enabled else {
                    return
                }
                
                configurationWalletCellPresenter.reactionForConfigMenu(withItemId: itemId, andEnabled: enabled)
            })
    }

    func registerReactionForReadChannelBalancesAvailable() {

        let channel: JsonChannel = JsonChannel(ConfigurationWalletCellPageReaction.BALANCES_AVAILABLE_CHANNEL)

        reactToConfiguration(channel: channel)
    }
    
    func registerReactionForReadChannelNotificationsAvailable() {
        
        let channel: JsonChannel = JsonChannel(ConfigurationWalletCellPageReaction.NOTIFICATIONS_AVAILABLE_CHANNEL)
        
        reactToConfiguration(channel: channel)
    }
    
    func registerReactionForReadChannelVibrationEnabled() {
        
        let channel: JsonChannel = JsonChannel(ConfigurationWalletCellPageReaction.VIBRATION_ENABLED_CHANNEL)
        
        _ = reactTo(channel: channel)?
            .then()
            .inside(componentID: ConfigurationWalletCellPageReaction.ROUTER_TAG, component: ConfigurationWalletCellPresenter<ConfigurationWalletCellViewController>.self)
            .doReactionAndClearChannel(reactionExecution: { configurationWalletCellPresenter, param in
                
                let notificationsVibrationCells = NotificationsVibrationCells.deserialize(from: param as? [String: Any])
                
                if let notificationsVibrationCells = notificationsVibrationCells, let enabled = notificationsVibrationCells.value {
                    
                    let configurationPreferencesDTO = ConfigurationPreferencesDTO(enabled: enabled, keyConfig: PreferencesManagerKeys.kConfigVibrationEnabled.rawValue)
                    configurationWalletCellPresenter.configurationChanged(configurationPreferencesDTO)
                }
            })
    }
    
    func registerReactionForReadChannelConfigDeviceNotifications() {
        
        let channel: JsonChannel = JsonChannel(ConfigurationWalletCellPageReaction.CONFIG_DEVICE_NOTIFICATIONS_CHANNEL)
        
        _ = reactTo(channel: channel)?
            .then()
            .inside(componentID: ConfigurationWalletCellPageReaction.ROUTER_TAG, component: ConfigurationWalletCellPresenter<ConfigurationWalletCellViewController>.self)
            .doReactionAndClearChannel { configurationWalletCellPresenter, _ in
                
                configurationWalletCellPresenter.showAppSettings()
        }
    }
    
    func registerReactionForReadChannelCloseConfigDeviceNotifications() {
        
        let channel: JsonChannel = JsonChannel(ConfigurationWalletCellPageReaction.CLOSE_CONFIG_DEVICE_NOTIFICATIONS_CHANNEL)
        
        _ = reactTo(channel: channel)?
            .then()
            .inside(componentID: WebController.ID, component: WebController.self)
            .doReactionAndClearChannel { controller, _ in
                
                controller.sendOnBack()
        }
    }
    
    // MARK: - ReactionsForWriteChannel

    func registerReactionForWriteInChannelParamsConfiguration() {

        let channel: JsonChannel = JsonChannel(ConfigurationWalletCellPageReaction.MENU_INFORMATION_CHANNEL)

        _ = writeOn(channel: channel)
            .when(componentID: ConfigurationWalletCellPageReaction.ROUTER_TAG)
            .doAction(actionSpec: ConfigurationWalletCellPageReaction.PARAMS_CONFIGURATION_CHANGED)
    }
    
    // MARK: - ReactionsForNavigate
    
    func registerReactionForNavigateToConfigDeviceNotifications() {
        
        _ = when(id: ConfigurationWalletCellPageReaction.ROUTER_TAG, type: Void.self)
            .doAction(actionSpec: ConfigurationWalletCellPageReaction.EVENT_NAV_TO_CONFIG_DEVICE_NOTIFICATIONS)
            .then()
            .inside(componentID: WebController.ID, component: WebController.self)
            .doReactionAndClearChannel(reactionExecution: { controller, _ in
                controller.navigateTo(route: ConfigurationWalletCellPageReaction.CELLS_PAGE_CONFIG_DEVICE_NOTIFICATIONS, payload: "")
            })
    }
    
    func registerReactionForNavigateToConfigMenuSectionNotifications() {
        
        _ = when(id: ConfigurationWalletCellPageReaction.ROUTER_TAG, type: Void.self)
            .doAction(actionSpec: ConfigurationWalletCellPageReaction.EVENT_NAV_TO_CONFIG_MENU_SECTION_NOTIFICATIONS)
            .then()
            .inside(componentID: WebController.ID, component: WebController.self)
            .doReactionAndClearChannel(reactionExecution: { controller, _ in
                controller.navigateTo(route: ConfigurationWalletCellPageReaction.CELLS_PAGE_CONFIG_MENU_SECTION_NOTIFICATIONS, payload: "")
            })
    }
    
    // MARK: - ReactionsToToken
    
    func reactTokenChanged() {
        
        _ = reactTo(channel: PageReactionConstants.CHANNEL_TOKEN_CHANGED)?
            .then()
            .inside(componentID: ConfigurationWalletCellPageReaction.ROUTER_TAG, component: ConfigurationWalletCellPresenter<ConfigurationWalletCellViewController>.self)
            .doReactionAndClearChannel { configurationWalletCellPresenter, param in
                
                if let param = param {
                    configurationWalletCellPresenter.registerToken(param)
                }
        }
    }
    
    func reactTokenFailed() {
        
        _ = reactTo(channel: PageReactionConstants.CHANNEL_TOKEN_FAILED)?
            .then()
            .inside(componentID: ConfigurationWalletCellPageReaction.ROUTER_TAG, component: ConfigurationWalletCellPresenter<ConfigurationWalletCellViewController>.self)
            .doReactionAndClearChannel { configurationWalletCellPresenter, _ in
                
                DLog(message: "routerTag: \(configurationWalletCellPresenter.routerTag)")
        }
    }
    
    // MARK: - ReactionsToStoreComponent
    
    func registerToLisenRegisterNotificationTokenSuccessRequest() {
        
        _ = when(id: RegisterNotificationsStore.ID, type: RegisterNotificationsResponseDTO.self)
            .doAction(actionSpec: RegisterNotificationsStore.ON_REGISTER_SUCCESS)
            .then()
            .inside(componentID: ConfigurationWalletCellPageReaction.ROUTER_TAG, component: ConfigurationWalletCellPresenter<ConfigurationWalletCellViewController>.self)
            .doReaction { configurationWalletCellPresenter, registerNotificationsResponseDTO in
                
                if let registerNotificationsResponseDTO = registerNotificationsResponseDTO, let data = registerNotificationsResponseDTO.data, let token = data.token {
                    configurationWalletCellPresenter.registerDeviceTokenSuccess(withToken: token)
                }
        }
    }
    
    func registerToLisenRegisterNotificationTokenErrorRequest() {
        
        _ = when(id: RegisterNotificationsStore.ID, type: ErrorBO.self)
            .doAction(actionSpec: RegisterNotificationsStore.ON_REGISTER_ERROR)
            .then()
            .inside(componentID: ConfigurationWalletCellPageReaction.ROUTER_TAG, component: ConfigurationWalletCellPresenter<ConfigurationWalletCellViewController>.self)
            .doReaction { configurationWalletCellPresenter, param in
                
                if let param = param {
                    configurationWalletCellPresenter.registerDeviceTokenFailure(error: param)
                }
        }
    }
}

extension ConfigurationWalletCellPageReaction {
    
    func createConfigurationPreferences(configureObjectCells: ConfigureObjectCells, channel: JsonChannel) -> ConfigurationPreferencesDTO {
        
        var isEnabled = false
        
        if let enabled = configureObjectCells.enabled {
            isEnabled = enabled
        }
        
        var keyConfig: String
        
        switch channel.getName() {
        case ConfigurationWalletCellPageReaction.BALANCES_AVAILABLE_CHANNEL:
            keyConfig = PreferencesManagerKeys.kConfigBalancesAvailable.rawValue
        case ConfigurationWalletCellPageReaction.NOTIFICATIONS_AVAILABLE_CHANNEL:
            keyConfig = PreferencesManagerKeys.kConfigNotificationsAvailable.rawValue
        case ConfigurationWalletCellPageReaction.VIBRATION_ENABLED_CHANNEL:
            keyConfig = PreferencesManagerKeys.kConfigVibrationEnabled.rawValue
        default:
            keyConfig = ""
        }
        
        return ConfigurationPreferencesDTO(enabled: isEnabled, keyConfig: keyConfig)
    }
    
    func reactToConfiguration(channel: JsonChannel) {
        
        _ = reactTo(channel: channel)?
            .then()
            .inside(componentID: ConfigurationWalletCellPageReaction.ROUTER_TAG, component: ConfigurationWalletCellPresenter<ConfigurationWalletCellViewController>.self)
            .doReactionAndClearChannel(reactionExecution: { configurationWalletCellPresenter, param in
                
                let configureObjectCells = ConfigureObjectCells.deserialize(from: param as? [String: Any])
                
                switch channel.getName() {
                case ConfigurationWalletCellPageReaction.NOTIFICATIONS_AVAILABLE_CHANNEL:
                    configurationWalletCellPresenter.showAppSettings()
                default:
                    
                    if let configureObjectCells = configureObjectCells {
                        
                        let configurationPreferencesDTO = self.createConfigurationPreferences(configureObjectCells: configureObjectCells, channel: channel)
                        configurationWalletCellPresenter.configurationChanged(configurationPreferencesDTO)
                    }
                }
            })
    }
}
