//
//  SideMenuWalletCellContract.swift
//  shoppingapp
//
//  Created by Marcos on 7/6/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol ConfigurationWalletCellViewProtocol: ViewProtocol {

    func openAppSettings()
    func registerDeviceOnNotificationsService(withToken token: String)
    func showToastSuccess(withSuccessMessage message: String, andBackgroundColor color: UIColor)
}

protocol ConfigurationWalletCellPresenterProtocol: CellsPresenterProtocol {
    
    func configurationChanged(_ configurationPreferencesDTO: ConfigurationPreferencesDTO)
    func showAppSettings()
    func reactionForConfigMenu(withItemId itemId: String, andEnabled enabled: Bool)
    func applicationWillEnterForeground()
    func registerDeviceTokenSuccess(withToken token: String)
    func registerDeviceTokenFailure(error: ErrorBO)
    func showRequestNotificationAutorization()
    func pageCellUnload(withPage page: String)
}
