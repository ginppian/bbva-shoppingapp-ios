//
//  SideMenuWalletCellPresenter.swift
//  shoppingapp
//
//  Created by Marcos on 7/6/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeComponents

class ConfigurationWalletCellPresenter<T: ConfigurationWalletCellViewProtocol>: BasePresenter<T> {

    var settingsTransport = SettingsTransport()
    var notificationManager: NotificationManager = NotificationManagerType()
    var isNotifyCloseCellsPageDeviceNotifications = false
    var isCallTokenRegisterTransparentlyForTheUser = false
    var isRegisteringToken = false

    required init() {
        super.init(tag: ConfigurationWalletCellPageReaction.ROUTER_TAG)
    }

    override init(busManager: BusManager) {

        super.init(busManager: busManager)
        routerTag = ConfigurationWalletCellPageReaction.ROUTER_TAG
    }
    
    override func setModel(model: Model) {
        
        if let error = model as? ErrorBO {
            errorBO = error
        }
    }

    override func checkError() {

        let settings = createSettings(withSettingsTransport: settingsTransport)

        if let settingsJson = settings.toJSON() {

            busManager.publishData(tag: ConfigurationWalletCellPageReaction.ROUTER_TAG, ConfigurationWalletCellPageReaction.PARAMS_CONFIGURATION_CHANGED, settingsJson)
        }
    }
    
    // MARK: - Public Methods
    
    func registerToken(_ token: String) {
        
        registerToken(token, transparently: false)
    }
}

// MARK: - ConfigurationWalletCellPresenterProtocol
extension ConfigurationWalletCellPresenter: ConfigurationWalletCellPresenterProtocol {

    func configurationChanged(_ configurationPreferencesDTO: ConfigurationPreferencesDTO) {

        let keyConfig = configurationPreferencesDTO.keyConfig
        let enabled = configurationPreferencesDTO.enabled
        
        switch keyConfig {
        case PreferencesManagerKeys.kConfigBalancesAvailable.rawValue:
            
            settingsTransport.isBalancesAvailable = !enabled
            saveConfig(withKeyConfig: keyConfig, andValue: enabled)
            
        case PreferencesManagerKeys.kConfigNotificationsAvailable.rawValue:
            
            settingsTransport.isNotificationsAvailable = !enabled
            saveConfig(withKeyConfig: keyConfig, andValue: enabled)
            
        case PreferencesManagerKeys.kConfigVibrationEnabled.rawValue:
            
            settingsTransport.isVibrationEnabled = !enabled
            saveConfig(withKeyConfig: keyConfig, andValue: enabled)
            
        case PreferencesManagerKeys.kConfigTicketPromotionsEnabled.rawValue:
            
            settingsTransport.isTicketPromotionsEnabled = !enabled
            saveConfig(withKeyConfig: keyConfig, andValue: enabled)
            
        default:
            break
        }
    }
    
    func showAppSettings() {
        
        notificationsPermissionDetermined { [weak self] determined in
            
            if !determined {
                self?.showRequestNotificationAutorization()
            } else {
                self?.view?.openAppSettings()
            }
        }
    }
    
    func reactionForConfigMenu(withItemId itemId: String, andEnabled enabled: Bool) {
        
        guard itemId == ConfigurationWalletCellPageReaction.ConfigItemIds.kConfigNotifications.rawValue else {
            return
        }
        
        let isRegisterDeviceToken = checkIfDeviceTokenIsRegister()
        
        notificationManager.isAuthorizedStatus { isAuthorized in
            
            if !isAuthorized || !isRegisterDeviceToken {
                
                DispatchQueue.main.async {
                    self.busManager.notify(tag: ConfigurationWalletCellPageReaction.ROUTER_TAG, ConfigurationWalletCellPageReaction.EVENT_NAV_TO_CONFIG_DEVICE_NOTIFICATIONS)
                }
                
                if !isRegisterDeviceToken {
                    
                    // Register device on notifications service (transparently for the user)
                    let apnsToken = PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kApnsToken) as? String
                    
                    if let token = apnsToken, isAuthorized, SessionDataManager.sessionDataInstance().isUserLogged {
                        self.isCallTokenRegisterTransparentlyForTheUser = true
                        self.view?.registerDeviceOnNotificationsService(withToken: token)
                    }
                }
                
            } else if isAuthorized && isRegisterDeviceToken {
                
                DispatchQueue.main.async {
                    self.publishDataOfConfiguration()
                    self.busManager.notify(tag: ConfigurationWalletCellPageReaction.ROUTER_TAG, ConfigurationWalletCellPageReaction.EVENT_NAV_TO_CONFIG_MENU_SECTION_NOTIFICATIONS)
                }
            }
        }
    }
    
    func applicationWillEnterForeground() {
        
        publishDataOfConfiguration()
        
        needRegisterToken { [weak self] needRegister in
            
            let apnsToken = PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kApnsToken) as? String
            let registerToken = PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kRegisterDeviceToken) as? String
            if needRegister, let token = apnsToken {
                
                self?.registerToken(token, transparently: false)
            } else if apnsToken != nil, registerToken != nil {
                
                self?.currentNotificationsPermission({ [weak self] authorized in
                    
                    if authorized {
                        self?.closeConfigurationDeviceNotificationsScreen()
                    }
                })
            }
            
        }
    }
    
    func registerDeviceTokenSuccess(withToken token: String) {
        
        registerSucceed(token: token)
    }
    
    func registerDeviceTokenFailure(error: ErrorBO) {
        
        registerFailed(error: error)
    }
    
    func showRequestNotificationAutorization() {
        
        notificationManager.showRequestNotificationAutorization { _ in
        }
    }
    
    func pageCellUnload(withPage page: String) {
        
        if page == ConfigurationWalletCellPageReaction.CELLS_PAGE_CONFIG_DEVICE_NOTIFICATIONS {
            
            if isNotifyCloseCellsPageDeviceNotifications {
                
                isNotifyCloseCellsPageDeviceNotifications = false
                
                navigateToConfigurationNotificationsSection()
            }
        }
    }
}

// MARK: - private methods
extension ConfigurationWalletCellPresenter {
    
    private func needRegisterToken(_ completion: @escaping (Bool) -> Void) {
        
        if checkIfDeviceTokenIsRegister() {
            return completion(false)
        }
        
        currentNotificationsPermission { authorized in
            
            completion(authorized)
        }
    }
    
    private func notificationsPermissionDetermined(_ determined: @escaping (Bool) -> Void) {
        
        notificationManager.checkAuthorizedStatus { isDetermined in
            determined(isDetermined)
        }
    }
    
    private func currentNotificationsPermission(_ authorized: @escaping (Bool) -> Void) {
        
        notificationManager.isAuthorizedStatus { isAuthorized in
            authorized(isAuthorized)
        }
    }
    
    private func registerToken(_ token: String, transparently: Bool) {
        
        guard SessionDataManager.sessionDataInstance().isUserLogged else {
            return
        }
        
        isCallTokenRegisterTransparentlyForTheUser = transparently
        
        if !transparently {
            DispatchQueue.main.async {
                
                self.showLoading()
            }
        }
        
        if !isRegisteringToken {
            
            isRegisteringToken = true
            view?.registerDeviceOnNotificationsService(withToken: token)
        }
    }
    
    private func registerSucceed(token: String) {
        
        saveRegisteredToken(token)
        publishDataOfConfiguration()
        
        if !isCallTokenRegisterTransparentlyForTheUser {
            
            hideLoading(completion: nil)
            closeConfigurationDeviceNotificationsScreen()
        }
        
        isRegisteringToken = false
    }
    
    private func registerFailed(error: ErrorBO) {
        
        publishDataOfConfiguration()
        if !isCallTokenRegisterTransparentlyForTheUser {
            
            hideLoading(completion: nil)
            setModel(model: error)
            view?.showError(error: error)
        }
        
        isRegisteringToken = false
    }
    
    private func saveRegisteredToken(_ token: String) {
        
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: token as AnyObject, withKey: PreferencesManagerKeys.kRegisterDeviceToken)
    }
    
    private func closeConfigurationDeviceNotificationsScreen() {
        
        if busManager.currentCellsPage == ConfigurationWalletCellPageReaction.CELLS_PAGE_CONFIG_DEVICE_NOTIFICATIONS {
            
            DispatchQueue.main.async {
                self.busManager.notify(tag: ConfigurationWalletCellPageReaction.ROUTER_TAG, ConfigurationWalletCellPageReaction.EVENT_CLOSE_CONFIG_DEVICE_NOTIFICATIONS)
                self.isNotifyCloseCellsPageDeviceNotifications = true
            }
        }
    }
    
    private func navigateToConfigurationNotificationsSection() {
        
        DispatchQueue.main.async {
            self.busManager.notify(tag: ConfigurationWalletCellPageReaction.ROUTER_TAG, ConfigurationWalletCellPageReaction.EVENT_NAV_TO_CONFIG_MENU_SECTION_NOTIFICATIONS)
            self.view?.showToastSuccess(withSuccessMessage: Localizables.notifications.key_ios_permission_accepted_text, andBackgroundColor: .BBVADARKGREEN)
        }
    }
    
    private func createSettings(withSettingsTransport settingsTransport: SettingsTransport) -> SettingsTransport {
        
        return SettingsTransport(isNotificationsAvailable: settingsTransport.isNotificationsAvailable, isBalancesAvailable: settingsTransport.isBalancesAvailable, isVibrationEnabled: settingsTransport.isVibrationEnabled, isTicketPromotionsEnabled: settingsTransport.isTicketPromotionsEnabled)
    }
    
    private func saveConfig(withKeyConfig keyConfig: String, andValue value: Bool) {
        
        let saved = PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: value as AnyObject, withKey: PreferencesManagerKeys(rawValue: keyConfig)!)
        
        if !saved {
            
            errorBO = ErrorBO(message: Localizables.common.key_common_generic_error_text, errorType: .warning, allowCancel: false)
            view?.showError(error: errorBO!)
        }
    }
    
    private func createConfigurationData(_ settings: @escaping (SettingsTransport) -> Void) {
        
        var areNotificationsEnabled = false
        
        notificationManager.isAuthorizedStatus { [weak self] isAuthorized in
            
            areNotificationsEnabled = isAuthorized
            
            let isRegisterDeviceToken = self?.checkIfDeviceTokenIsRegister() ?? false
            
            let balancesEnabled = PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kConfigBalancesAvailable)
            let isBalancesAvailable = (balancesEnabled as? Bool) ?? false
            
            let vibrationEnabled = PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kConfigVibrationEnabled)
            let isVibrationEnabled = (vibrationEnabled as? Bool) ?? true
            
            let ticketPromotionsEnabled = PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kConfigTicketPromotionsEnabled)
            let isTicketPromotionsEnabled = (ticketPromotionsEnabled as? Bool) ?? false
            
            let settingsTransport = SettingsTransport(isNotificationsAvailable: (isRegisterDeviceToken && areNotificationsEnabled), isBalancesAvailable: isBalancesAvailable, isVibrationEnabled: isVibrationEnabled, isTicketPromotionsEnabled: isTicketPromotionsEnabled)
            
            settings(settingsTransport)
        }
    }
    
    private func checkIfDeviceTokenIsRegister() -> Bool {
        
        let apnsToken = PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kApnsToken) as? String
        let registerDeviceToken = PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kRegisterDeviceToken) as? String
        
        if let apnsToken = apnsToken, apnsToken == registerDeviceToken {
            return true
        }
        
        return false
    }
    
    private func publishDataOfConfiguration() {
        
        createConfigurationData { [weak self] settingsTransport in
            
            if let settingsTransportJson = settingsTransport.toJSON() {
                
                DispatchQueue.main.async {
                    
                    self?.busManager.publishData(tag: ConfigurationWalletCellPageReaction.ROUTER_TAG, ConfigurationWalletCellPageReaction.PARAMS_CONFIGURATION_CHANGED, settingsTransportJson)
                }
            }
        }
    }
}
