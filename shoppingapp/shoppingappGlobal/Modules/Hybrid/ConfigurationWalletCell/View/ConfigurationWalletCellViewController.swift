//
//  ConfigurationWalletCellViewController.swift
//  shoppingapp
//
//  Created by Marcos on 7/6/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit
import CellsNativeCore
import CellsNativeComponents

final class ConfigurationWalletCellViewController: WalletCellsViewController {

    // MARK: Constants

    static let ID = String(describing: ConfigurationWalletCellViewController.self)
    static let heightToastView: CGFloat = 100.0
    
    var observerWillEnterForeground: Any?
    
    var registerNotificationsStore: RegisterNotificationsStore?
    
    override func viewDidLoad() {

        configure(appLifecycle: (UIApplication.shared.delegate as! AppDelegate).appLifeCycle!, id: ConfigurationWalletCellViewController.ID)
        
        if let networkWorker = BusManager.sessionDataInstance.networkWorker {
            registerNotificationsStore = RegisterNotificationsStore(worker: networkWorker, host: ServerHostURL.getDevicesURL())
        }

        super.viewDidLoad()
        
        addApplicationsObservers()
        
//        TicketUtils.openDummyTicket()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    deinit {
        DLog(message: "Deinitialized ConfigurationWalletCellViewController")
        
        removeApplicationsObservers()
    }

}

extension ConfigurationWalletCellViewController: ConfigurationWalletCellViewProtocol {

    func openAppSettings() {
        
        if let url = URL(string: UIApplicationOpenSettingsURLString) {
            
            DispatchQueue.main.async {
                URLOpener().openURL(url)
            }
        }
    }
    
    func registerDeviceOnNotificationsService(withToken token: String) {
        
        if let device = DeviceManager.instance.deviceUID {
            registerNotificationsStore?.registerNotificationsToken(registerNotificationsToken: RegisterNotificationsTokenDTO(token: token, deviceId: device, applicationId: DeviceManager.instance.applicationId, serviceId: RegisterNotificationsStore.serviceId))
        }
        
    }
    
    func showToastSuccess(withSuccessMessage message: String, andBackgroundColor color: UIColor) {
        
        ToastManager.shared().showToast(withText: message, backgroundColor: color, height: ConfigurationWalletCellViewController.heightToastView)
    }
}

extension ConfigurationWalletCellViewController: CellsProtocol {
}

extension ConfigurationWalletCellViewController {
    
    private func addApplicationsObservers() {
        
        if observerWillEnterForeground == nil {
            
            observerWillEnterForeground = NotificationCenter.default.addObserver(forName: NSNotification.Name.UIApplicationWillEnterForeground, object: nil, queue: nil) { [weak self] _ in
                self?.applicationWillEnterForeground()
            }
        }
    }
    
    private func removeApplicationsObservers() {
        
        if let observer = observerWillEnterForeground {
            NotificationCenter.default.removeObserver(observer)
            observerWillEnterForeground = nil
        }
    }
    
    private func applicationWillEnterForeground() {
        
        if let presenter = presenter as? ConfigurationWalletCellPresenter<ConfigurationWalletCellViewController> {
            presenter.applicationWillEnterForeground()
        }
    }
}
