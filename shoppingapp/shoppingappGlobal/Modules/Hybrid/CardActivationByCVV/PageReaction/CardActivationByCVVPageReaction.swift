//
//  CardActivationByCVVPageReaction.swift
//  shoppingappMX
//
//  Created by Marcos on 17/10/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

class CardActivationByCVVPageReaction: CommonPageReaction {

    static let routerTag: String = "card-activation-cvv-tag"

    static let eventCardActivatedSuccess: ActionSpec<Void> = ActionSpec<Void>(id: "EVENT_CARD_ACTIVATED_SUCCESS")

    static let paramsActivateCardSuccess: ActionSpec<Any> = ActionSpec<Any>(id: "PARAMS_ACTIVATE_CARD_SUCCESS")
    static let paramsActivateCardError: ActionSpec<Any> = ActionSpec<Any>(id: "PARAMS_ACTIVATE_CARD_ERROR")

    // Channels

    static let cellResultCVVChannel = "acbc_result_channel"

    static let infoConfirmationChannel = "acs_info_confirmation_channel"

    static let infoErrorChannel = "acbc_response_channel"

    // Cells pages

    static let cellPageActivateCard = "cardActivateByCvv"

    static let cellPageActivateCardSuccess = "cardActivateSuccess"

    override func configure() {
        
        super.configure()

        paramsCardActivation()

        navigateCardActivationSuccess()

        cardActivationError()
        cardActivationWrongCVVError()
        finishCardActivationRequest()

        paramsActivateCardSuccess()
        paramsActivateCardError()

        skipNavigation()
        pageLoaded()

        cardDataSelected()
        registerToReadOperationCardInfoChannel()
        paramsOperationActivationCardSuccess()

    }

    func cardDataSelected() {

        _ = reactTo(channel: CardsPageReaction.CHANNEL_SELECT_CARD_ACTIVATE)?
            .then()
            .inside(componentID: CardActivationByCVVPageReaction.routerTag, component: CardActivationByCVVPresenter<CardActivationByCVVViewController>.self)
            .doReactionAndClearChannel { component, param in

                let activateCardSuccessObjectCells = ActivateCardSuccessObjectCells.deserialize(from: param as? [String: Any])
                component.activateCardSuccessObjectCells = activateCardSuccessObjectCells

        }

    }

    func pageLoaded() {

        _ = reactTo(channel: PageReactionConstants.CHANNEL_HIDDEN_VIEW_NATIVE)?
            .then()
            .inside(componentID: CardActivationByCVVViewController.id, component: CardActivationByCVVViewController.self)
            .doReactionAndClearChannel { controller, page in

                if let page = page {
                    controller.registerOmniture(page: page)
                }
        }

    }

    func paramsCardActivation() {

        let channel: JsonChannel = JsonChannel(CardActivationByCVVPageReaction.cellResultCVVChannel)

        _ = reactTo(channel: channel)?
            .then()
            .inside(componentID: CardActivationByCVVPageReaction.routerTag, component: CardActivationByCVVPresenter<CardActivationByCVVViewController>.self)
            .doReactionAndClearChannel(reactionExecution: { presenter, param in
                let requestObjectCells = RequestActivateCardByCVVObjectCells.deserialize(from: param as? [String: Any])
                if let requestObjectCells = requestObjectCells {
                    presenter.setCVV(cvv: requestObjectCells.value)
                }
            })
    }

    func finishCardActivationRequest() {

        _ = when(id: ActivateCardStore.ID, type: Bool.self)
            .doAction(actionSpec: ActivateCardStore.ON_ACTIVATE_SUCCESS)
            .then()
            .inside(componentID: CardActivationByCVVViewController.id, component: CardActivationByCVVViewController.self)
            .doReaction { controller, _ in

                controller.finishDoRequestSuccess()

        }
    }

    func navigateCardActivationSuccess() {

        _ = when(id: CardActivationByCVVPageReaction.routerTag, type: Void.self)
            .doAction(actionSpec: CardActivationByCVVPageReaction.eventCardActivatedSuccess)
            .then()
            .inside(componentID: WebController.ID, component: WebController.self)
            .doReaction { controller, _ in

                controller.navigateTo(route: CardActivationByCVVPageReaction.cellPageActivateCardSuccess, payload: "")

        }

    }

    func cardActivationError() {

        _ = when(id: ActivateCardStore.ID, type: ErrorBO.self)
            .doAction(actionSpec: ActivateCardStore.ON_ACTIVATE_ERROR)
            .then()
            .inside(componentID: CardActivationByCVVViewController.id, component: CardActivationByCVVViewController.self)
            .doReaction { controller, param in

                controller.finishDoRequestError(error: param!)
        }
    }

    func cardActivationWrongCVVError() {

        _ = when(id: ActivateCardStore.ID, type: Void.self)
            .doAction(actionSpec: ActivateCardStore.ON_ACTIVATE_WRONG_CVV_ERROR)
            .then()
            .inside(componentID: CardActivationByCVVViewController.id, component: CardActivationByCVVViewController.self)
            .doReaction { controller, _ in

                controller.wrongCVVError()
        }
    }

    func paramsActivateCardSuccess() {

        let channel: JsonChannel = JsonChannel(CardActivationByCVVPageReaction.infoConfirmationChannel)

        _ = writeOn(channel: channel)
            .when(componentID: CardActivationByCVVPageReaction.routerTag)
            .doAction(actionSpec: CardActivationByCVVPageReaction.paramsActivateCardSuccess)
    }

    func paramsActivateCardError() {

        let channel: JsonChannel = JsonChannel(CardActivationByCVVPageReaction.infoErrorChannel)

        _ = writeOn(channel: channel)
            .when(componentID: CardActivationByCVVPageReaction.routerTag)
            .doAction(actionSpec: CardActivationByCVVPageReaction.paramsActivateCardError)
    }

    fileprivate func skipNavigation() {

        if let controller: WebController = ComponentManager.get(id: WebController.ID) {
            controller.skipNavigations(routes: [SkipTransitionDTO(from: CardActivationByCVVPageReaction.cellPageActivateCardSuccess, to: CardActivationByCVVPageReaction.cellPageActivateCard, skipHistory: true)])
        }
    }

    func registerToReadOperationCardInfoChannel() {

        _ = reactTo(channel: CardsPageReaction.OPERATION_CARD_INFO_CHANNEL)?
            .then()
            .inside(componentID: CardActivationByCVVPageReaction.routerTag, component: CardActivationByCVVPresenter<CardActivationByCVVViewController>.self)
            .doReactionAndClearChannel(reactionExecution: { component, cardOperationInfo in
                component.cardOperationInfo = cardOperationInfo
            })
    }

    func paramsOperationActivationCardSuccess() {

        _ = writeOn(channel: CardsPageReaction.OPERATION_CARD_INFO_SUCCESS_CHANNEL)
            .when(componentID: CardActivationByCVVPageReaction.routerTag)
            .doAction(actionSpec: CardsPageReaction.PARAMS_OPERATION_CARD_INFO_SUCCESS)
    }

}
