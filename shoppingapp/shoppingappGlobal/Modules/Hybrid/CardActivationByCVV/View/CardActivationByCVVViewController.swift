//
//  CardActivationByCVVViewController.swift
//  shoppingappMX
//
//  Created by Marcos on 17/10/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit
import CellsNativeCore
import CellsNativeComponents

class CardActivationByCVVViewController: WalletCellsViewController {

    // MARK: Constants

    static let id = String(describing: CardActivationByCVVViewController.self)

    // MARK: Vars

    var activateCardStore: ActivateCardStore?

    // MARK: life cycle

    override func viewDidLoad() {

        configure(appLifecycle: (UIApplication.shared.delegate as! AppDelegate).appLifeCycle!, id: CardActivationByCVVViewController.id)

        if let networkWorker = BusManager.sessionDataInstance.networkWorker {
            activateCardStore = ActivateCardStore(worker: networkWorker, host: ServerHostURL.getCardsURL())
        }

        super.viewDidLoad()
    }

    override func viewWillDisappear(_ animated: Bool) {

        super.viewWillDisappear(animated)
        if let presenter = presenter as? CardActivationByCVVPresenter<CardActivationByCVVViewController> {

            presenter.viewWillDisappear()
        }
    }

    override func dismiss(completion: (() -> Void)? = nil) {

        super.dismiss(completion: { [weak self] in
            if let presenter = self?.presenter as? CardActivationByCVVPresenter<CardActivationByCVVViewController> {

                presenter.viewWasDismissed()
            }
        })
    }

    func activateCardWithCVV(id: String, cardId: String, cvv: String) {
        
        presenter?.showLoading(completion: nil)
        let cardActivateWithCVVDTO = CardActivateWithCVVDTO(id: id, cardId: cardId, pin: cvv)
        activateCardStore?.updateCardActivationWithCVV(cardActivateWithCVVDTO: cardActivateWithCVVDTO)
    }

    func wrongCVVError() {

        presenter.hideLoading {
            let activateByCVVCardErrorObjectCells = ActivateByCVVCardErrorObjectCells()

            if let activateByCVVCardErrorObjectCellsJSon = activateByCVVCardErrorObjectCells.toJSON() {

                BusManager.sessionDataInstance.publishData(tag: CardActivationByCVVPageReaction.routerTag, CardActivationByCVVPageReaction.paramsActivateCardError, activateByCVVCardErrorObjectCellsJSon)

            }
        }
    }
}

extension CardActivationByCVVViewController: CellsProtocol {

}

extension CardActivationByCVVViewController: CardActivationByCVVViewProtocol {

}
