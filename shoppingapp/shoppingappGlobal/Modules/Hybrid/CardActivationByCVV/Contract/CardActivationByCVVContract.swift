//
//  CardActivationByCVVContract.swift
//  shoppingappMX
//
//  Created by Marcos on 17/10/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol CardActivationByCVVViewProtocol: ViewProtocol {

    func activateCardWithCVV(id: String, cardId: String, cvv: String)
}

protocol CardActivationByCVVContractPresenterProtocol: CellsProtocol {

    func viewWasDismissed()
    func viewWillDisappear()
}
