//
//  CardActivationByCVVPresenter.swift
//  shoppingappMX
//
//  Created by Marcos on 17/10/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeComponents
import RxSwift

class CardActivationByCVVPresenter<T: CardActivationByCVVViewProtocol>: BasePresenter<T> {
    
    let disposeBag = DisposeBag()
    
    var cryptoDataManager = LibCryptoDataManager.sharedInstance

    var activateCardSuccessObjectCells: ActivateCardSuccessObjectCells?
    var cardOperationInfo: CardOperationInfo?

    var success = false

    required init() {
        super.init(tag: CardActivationByCVVPageReaction.routerTag)
    }

    override init(busManager: BusManager) {

        super.init(busManager: busManager)
        routerTag = CardActivationByCVVPageReaction.routerTag
    }

    override func setModel(model: Model) {
        if let error = model as? ErrorBO {
            errorBO = error
        }
    }

    func viewWillDisappear() {

        busManager.clearData(tag: CardActivationByCVVPageReaction.routerTag, CardActivationByCVVPageReaction.paramsActivateCardError)

    }

    func setCVV(cvv: String) {

        guard let cardID = cardOperationInfo?.cardID else {
            return
        }
        
        let hexCVVString = Settings.CardCvv.hexadecimalEncodingForCVV(cvv: cvv)
        let encryptedCVV = cryptoDataManager.encryptDataForHost(data: hexCVVString, keyTag: ServerHostURL.getKeyTagForEncryption())
        
        encryptedCVV.subscribe(
            onNext: { [weak self] hostDataEntity in
                self?.view?.activateCardWithCVV(id: Settings.CardCvv.cvvId, cardId: cardID, cvv: hostDataEntity.data)
            }, onError: { [weak self]  error in
                self?.errorBO = ErrorBO(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text))
                self?.checkError()
        }).addDisposableTo(self.disposeBag)
    }
}

extension CardActivationByCVVPresenter: CellsPresenterProtocol {

    func showPageSuccess(modelBO: ModelBO? = nil) {

        if let activateCardSuccessObjectCells = activateCardSuccessObjectCells {

            success = true

            if let activateCardSuccessObjectCellsJson = activateCardSuccessObjectCells.toJSON() {
                busManager.publishData(tag: CardActivationByCVVPageReaction.routerTag, CardActivationByCVVPageReaction.paramsActivateCardSuccess, activateCardSuccessObjectCellsJson)
            }

        }
        busManager.navigateScreen(tag: CardActivationByCVVPageReaction.routerTag, CardActivationByCVVPageReaction.eventCardActivatedSuccess, Void())

        if let cardOperationInfo = cardOperationInfo {
            busManager.publishData(tag: CardActivationByCVVPageReaction.routerTag, CardsPageReaction.PARAMS_OPERATION_CARD_INFO_SUCCESS, cardOperationInfo)
        }
    }

    func registerOmniture(page: String) {
        
        if page == CardActivationPageReaction.CELLS_PAGE_ACTIVATE_CARD {
            
            TrackerHelper.sharedInstance().trackCellsPageActivateCard(withCustomerID: SessionDataManager.sessionDataInstance().customerID, pageName: "tarjetas:activacion tarjeta:1 activar")
            
        } else if page == CardActivationPageReaction.CELLS_PAGE_ACTIVATE_CARD_SUCCESS {
            
            TrackerHelper.sharedInstance().trackCellsPageSuccessActivation(withCustomerID: SessionDataManager.sessionDataInstance().customerID, pageName: "tarjetas:activacion tarjeta:pagina exitosa")
        }
    }

    func viewWasDismissed() {

        if success {
            busManager.notify(tag: CardsPageReaction.ROUTER_TAG, CardsPageReaction.NEEDED_CARDS_REFRESH)
        }

    }
}
