//
//  GPSHelpContract.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 23/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import RxSwift

protocol GPSHelpViewProtocol: ViewProtocol {

    func dismiss()
}

protocol GPSHelpPresenterProtocol: PresenterProtocol {

    func acceptButtonPressed()
    func cancelButtonPressed()
}
