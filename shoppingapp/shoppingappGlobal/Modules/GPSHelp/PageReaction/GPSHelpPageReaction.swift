//
//  GPSHelpPageReaction.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 25/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import CellsNativeCore
import CellsNativeComponents

class GPSHelpPageReaction: PageReaction {

    static let routerTag: String = "gps-help-tag"
    
    static let channelDismissGpsHelpScreen: Channel<Void> = Channel(name: "channel-dismiss-gps-help-screen")
    
    static let eventDismissScreen: ActionSpec<Void> = ActionSpec<Void>(id: "event-dismiss-screen")

    override func configure() {

        registerForDismiss()
    }
    
    func registerForDismiss() {
        
        _ = writeOn(channel: GPSHelpPageReaction.channelDismissGpsHelpScreen)
            .when(componentID: GPSHelpPageReaction.routerTag)
            .doAction(actionSpec: GPSHelpPageReaction.eventDismissScreen)
    }
}
