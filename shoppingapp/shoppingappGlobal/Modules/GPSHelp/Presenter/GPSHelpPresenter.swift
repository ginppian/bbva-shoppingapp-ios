//
//  GPSHelpPresenter.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 23/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import RxSwift
import CellsNativeComponents

class GPSHelpPresenter<T: GPSHelpViewProtocol>: BasePresenter<T> {

    required init() {
        super.init(tag: GPSHelpPageReaction.routerTag)
    }

    override init(busManager: BusManager) {
        super.init(busManager: busManager)
        routerTag = GPSHelpPageReaction.routerTag
    }
    
    private func publishDismissEvent() {
        
        busManager.publishData(tag: GPSHelpPageReaction.routerTag, GPSHelpPageReaction.eventDismissScreen, Void())
    }
}

extension GPSHelpPresenter: GPSHelpPresenterProtocol {

    func acceptButtonPressed() {
        
        publishDismissEvent()
        view?.dismiss()
    }
    
    func cancelButtonPressed() {
        
        publishDismissEvent()
        view?.dismiss()
    }
}
