//
//  GPSHelpViewController.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 19/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

protocol GPSHelpViewControllerDelegate: class {

    func cancelGPSHelp(_ gpsHelpViewController: GPSHelpViewController)
}

class GPSHelpViewController: BaseViewController {

    var presenter: GPSHelpPresenterProtocol!

    static let id: String = String(describing: GPSHelpViewController.self)

    @IBOutlet weak var gpsImageView: UIImageView!
    @IBOutlet weak var infoLabel: UILabel!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var settingsImageView: UIImageView!
    @IBOutlet weak var settingsLabel: UILabel!
    @IBOutlet weak var privacyImageView: UIImageView!
    @IBOutlet weak var privacyLabel: UILabel!
    @IBOutlet weak var nearmeImageView: UIImageView!
    @IBOutlet weak var nearmeLabel: UILabel!
    @IBOutlet weak var locationImageView: UIImageView!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var understoodButton: UIButton!

    weak var delegate: GPSHelpViewControllerDelegate?

    override func viewDidLoad() {
        
        self.configure(appLifecycle: (UIApplication.shared.delegate as! AppDelegate).appLifeCycle!, id: GPSHelpViewController.id)
        super.viewDidLoad()
        configureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {

        super.viewWillAppear(animated)

        configureNavigationBar()

        BusManager.sessionDataInstance.disableSwipeLateralMenu()

    }

    override func viewWillDisappear(_ animated: Bool) {

        super.viewWillDisappear(animated)

    }

    func configureNavigationBar() {

        let image = SVGCache.image(forSVGNamed: ConstantsImages.Common.close_icon, color: .NAVY)

        self.navigationController?.navigationBar.barTintColor = .white

        setNavigationLeftButton(withImage: image, action: #selector(dismissAction))

        setNavigationTitle(withText: Localizables.promotions.key_enable_gps_text, color: .NAVY)

    }

    func configureView() {

        let font = Tools.setFontBook(size: 14)
        let boldFont = Tools.setFontMedium(size: 14)

        infoLabel.attributedText = CustomAttributedText.attributedBoldText(forFullText: Localizables.promotions.key_gps_disabled_info_text, withBoldText: Localizables.promotions.key_gps_text, withFont: font, withBoldFont: boldFont, andForegroundColor: .BBVA600)
        infoLabel.textAlignment = .center

        lineView.backgroundColor = .BBVA200

        settingsImageView.image = SVGCache.image(forSVGNamed: ConstantsImages.GpsHelp.settings, color: .BBVA500)
        settingsImageView.contentMode = .scaleAspectFit
        settingsLabel.attributedText = CustomAttributedText.attributedBoldText(forFullText: Localizables.promotions.key_gps_disabled_info_option_settings_text, withBoldText: Localizables.promotions.key_settings_text, withFont: font, withBoldFont: boldFont, andForegroundColor: .BBVA500)

        privacyImageView.image = SVGCache.image(forSVGNamed: ConstantsImages.GpsHelp.lock_icon, color: .BBVA500)
        privacyImageView.contentMode = .scaleAspectFit
        privacyLabel.attributedText = CustomAttributedText.attributedBoldText(forFullText: Localizables.promotions.key_gps_disabled_info_option_privacity_text, withBoldText: Localizables.promotions.key_privacy_text, withFont: font, withBoldFont: boldFont, andForegroundColor: .BBVA500)

        nearmeImageView.image = SVGCache.image(forSVGNamed: ConstantsImages.GpsHelp.nearme_icon, color: .BBVA500)
        nearmeImageView.contentMode = .scaleAspectFit
        nearmeLabel.attributedText = CustomAttributedText.attributedBoldText(forFullText: Localizables.promotions.key_gps_disabled_info_option_location_text, withBoldText: Localizables.promotions.key_location_services_text, withFont: font, withBoldFont: boldFont, andForegroundColor: .BBVA500)

        locationImageView.image = SVGCache.image(forSVGNamed: ConstantsImages.GpsHelp.correct_location_icon, color: .BBVA500)
        locationImageView.contentMode = .scaleAspectFit
        locationLabel.attributedText = CustomAttributedText.attributedBoldText(forFullText: Localizables.promotions.key_gps_disabled_info_option_activate_text, withBoldText: Localizables.promotions.key_location_services_text, withFont: font, withBoldFont: boldFont, andForegroundColor: .BBVA500)

        understoodButton.setBackgroundColor(color: .MEDIUMBLUE, forUIControlState: .normal)
        understoodButton.setBackgroundColor(color: .DARKMEDIUMBLUE, forUIControlState: .highlighted)
        understoodButton.setTitle(Localizables.promotions.key_understood_text, for: .normal)
        understoodButton.titleLabel?.font = Tools.setFontBold(size: 14.0)
        understoodButton.setTitleColor(.BBVAWHITE, for: .normal)
    }

    @IBAction func understoodButtonPressed(_ sender: Any) {

        presenter.acceptButtonPressed()
    }

    override func dismissAction() {

        presenter.cancelButtonPressed()
    }

    deinit {
        ComponentManager.remove(id: presenter.routerTag)
        DLog(message: "GPSHelpViewController deinitialized")
    }
}

extension GPSHelpViewController: GPSHelpViewProtocol {

    func dismiss() {
        
        super.dismissAction()
    }
    
    func showError(error: ModelBO) {
    }

    func changeColorEditTexts() {
    }
}
