//
//  ShoppingMapViewController.swift
//  shoppingapp
//
//  Created by jesus.martinez on 4/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit
import CellsNativeCore
import CellsNativeComponents
import GoogleMaps
import Lottie

class ShoppingMapViewController: BaseViewController {

    // MARK: Constants
    static let heightToastView: CGFloat = 100.0
    static let promotionPadding: CGFloat = 20.0
    static let minimumZIndex: Int32 = 0
    static let maximumZIndex: Int32 = 1000
    static let timeToHideFilterAnimation = 0.5
    static let pinBorderWidth: CGFloat = 4

    // Map
    static let sizeMapIconSelected: CGFloat = 48.0
    static let sizeMapIcon: CGFloat = 32.0
    static let sizeMapImage: CGFloat = 16.0
    static let animationTapPromotionDuration = 0.5

    // Timer
    static let timeToWaitBeforeToInformNewLocationInSeconds = 1

    // Promotions collection view
    static let automaticFlickMinimumSpeed = CGFloat(0.2)
    let promotionsBetweenCellsSpacing = CGFloat(10.0)
    let promotionsSidesMargin = CGFloat(20.0)
    var promotionsCellWidth = CGFloat(0)
    var currentCellIndex = 0
    var scrollToOffset = false
    var lastCalculatedOffsetX = CGFloat(0)

    static let ID: String = String(describing: ShoppingMapViewController.self)

    // Map
    @IBOutlet weak var mapView: GMSMapView!

    // Promotions
    @IBOutlet weak var promotionsContainerView: UIView!
    @IBOutlet weak var promotionsCollectionView: UICollectionView!
    @IBOutlet weak var promotionsCollectionViewFlowLayout: UICollectionViewFlowLayout!

    @IBOutlet weak var promotionsCollectionViewLeadingToLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var promotionsCollectionViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var promotionsCollectionViewTranilingEqualsToLeadingMap: NSLayoutConstraint!

    // Footer
    @IBOutlet weak var footerView: UIView!
    @IBOutlet weak var footerViewHeightConstraint: NSLayoutConstraint!
    static let footerViewHeight = CGFloat(70)
    static let footerLottieWidth = CGFloat(33.8)
    static let footerLottieHeight = CGFloat(30.0)

    // Filter
    @IBOutlet weak var filtersScrollView: FiltersScrollView!
    @IBOutlet weak var filtersScrollViewHeightConstraint: NSLayoutConstraint!

    var lottieLoader: LOTAnimationView!
    var presenter: ShoppingMapPresenterProtocol!
    var selectedMarker: GMSMarker?
    var promotionsDisplayData: MapDisplayDataPromotions?
    var storesCircle: GMSCircle?

    var timer: Timer?
    var isUserAction = true
    var hiddingPromotions = false

    override func viewDidLoad() {

        self.configure(appLifecycle: (UIApplication.shared.delegate as! AppDelegate).appLifeCycle!, id: ShoppingMapViewController.ID)
        super.viewDidLoad()

        configureFooterLoader()
        presenter?.viewDidLoad()

        filtersScrollView.delegate = self
        mapView.delegate = self

        configurePromotionsCollectionView()

        promotionsContainerView.isUserInteractionEnabled = false

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        setNavigationBarDefault()
        setNavigationBackButtonDefault()
        setNavigationTitle(withText: Localizables.promotions.key_promotions_near_me_text)
        BusManager.sessionDataInstance.disableSwipeLateralMenu()

        trackScreen()

        presenter?.viewWillAppear()
        
        _ = NotificationCenter.default.addObserver(forName: NSNotification.Name.UIApplicationWillEnterForeground, object: nil, queue: nil) { [weak self] _ in
            if self?.footerView.isHidden == false {
                self?.lottieLoader.play()
            }
        }

    }

    override func viewWillAppearAfterDismiss() {
        
        trackScreen()
    }
    
    fileprivate func trackScreen() {
        TrackerHelper.sharedInstance().trackPromotionsMapScreen(withCustomerID: SessionDataManager.sessionDataInstance().customerID)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        NotificationCenter.default.removeObserver(self)
    }

    @objc func filterAction(_ sender: AnyObject?) {

        presenter.filterPressed()

    }

    func configurePromotionsCollectionView() {

        promotionsCollectionView.dataSource = self
        promotionsCollectionView.delegate = self
        promotionsCollectionView.backgroundColor = .clear
        promotionsCollectionView.allowsMultipleSelection = false
        promotionsCollectionView.scrollsToTop = false
        promotionsCollectionView.bounces = false
        promotionsCollectionView.alwaysBounceVertical = false
        promotionsCollectionView.alwaysBounceHorizontal = false
        promotionsCollectionView.isPagingEnabled = false
        promotionsCollectionView.showsHorizontalScrollIndicator = false
        promotionsCollectionView.showsVerticalScrollIndicator = false
        promotionsCollectionView.delaysContentTouches = false
        promotionsCollectionView.decelerationRate = UIScrollViewDecelerationRateFast
        promotionsCollectionView.isHidden = true

        promotionsCollectionView.register(UINib(nibName: PromotionCollectionViewCell.identifier, bundle: nil), forCellWithReuseIdentifier: PromotionCollectionViewCell.identifier)
        promotionsCollectionView.register(UINib(nibName: MorePromotionsCollectionViewCell.identifier, bundle: nil), forCellWithReuseIdentifier: MorePromotionsCollectionViewCell.identifier)

        promotionsCellWidth = UIScreen.main.bounds.width - promotionsSidesMargin * 2.0

        promotionsCollectionViewFlowLayout = promotionsCollectionView.collectionViewLayout as? UICollectionViewFlowLayout
        promotionsCollectionViewFlowLayout.scrollDirection = UICollectionViewScrollDirection.horizontal
        promotionsCollectionViewFlowLayout.sectionInset = UIEdgeInsets(top: 0, left: promotionsSidesMargin, bottom: 0, right: promotionsSidesMargin)
        promotionsCollectionViewFlowLayout.minimumLineSpacing = promotionsBetweenCellsSpacing
        promotionsCollectionViewFlowLayout.itemSize = CGSize(width: promotionsCellWidth, height: PromotionCollectionViewCell.height)
    }

    func createCircle(forCoordinate coordinate: CLLocationCoordinate2D, andRadius radius: CLLocationDistance) -> GMSCircle {

        let circle = GMSCircle(position: coordinate, radius: radius)
        circle.fillColor = UIColor.BBVADARKLIGHTBLUE.withAlphaComponent(0.1)
        circle.strokeColor = .clear

        return circle

    }

    func configureFooterLoader() {

        footerViewHeightConstraint.constant = ShoppingMapViewController.footerViewHeight + UIDevice.current.bottomMargin

        footerView.isHidden = true
        footerView.backgroundColor = UIColor.BBVAWHITE.withAlphaComponent(0.9)

        lottieLoader = LOTAnimationView(name: ConstantsLottieAnimations.loader)
        let lottieFooterX = (UIScreen.main.bounds.width - ShoppingMapViewController.footerLottieWidth) / 2
        let lottieFooterY = (ShoppingMapViewController.footerViewHeight - ShoppingMapViewController.footerLottieHeight) / 2
        lottieLoader.frame = CGRect(x: lottieFooterX,
                                    y: lottieFooterY,
                                    width: ShoppingMapViewController.footerLottieWidth,
                                    height: ShoppingMapViewController.footerLottieHeight)
        lottieLoader.contentMode = .scaleAspectFit
        lottieLoader.loopAnimation = true

        footerView.addSubview(lottieLoader)

    }

    func hideLoader() {

        footerView.isHidden = true
        lottieLoader.pause()
    }

    func showLoader() {

        footerView.isHidden = false
        lottieLoader.play()
    }

    func createIcon(mapDisplayDataItem: MapDisplayDataItem) -> UIView {

        let view = UIView(frame: CGRect(x: 0, y: 0, width: ShoppingMapViewController.sizeMapIcon, height: ShoppingMapViewController.sizeMapIcon))
        view.layer.cornerRadius = min(view.frame.size.height, view.frame.size.width) / 2.0
        view.clipsToBounds = true
        view.backgroundColor = mapDisplayDataItem.backgroundColor

        if let imageName = mapDisplayDataItem.imageNamed {
            let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: ShoppingMapViewController.sizeMapImage, height: ShoppingMapViewController.sizeMapImage))

            imageView.image = SVGCache.image(forSVGNamed: imageName, color: mapDisplayDataItem.imageColor)
            imageView.backgroundColor = mapDisplayDataItem.backgroundColor

            view.addSubview(imageView)
            imageView.center = view.center
        }

        if let title = mapDisplayDataItem.title {
            let titleLabel = UILabel(frame: CGRect(x: 0, y: 0, width: view.bounds.width, height: view.bounds.height))
            titleLabel.font = Tools.setFontMedium(size: 14)
            titleLabel.textAlignment = .center
            titleLabel.text = title
            titleLabel.backgroundColor = mapDisplayDataItem.backgroundColor
            view.addSubview(titleLabel)
            titleLabel.center = view.center

        }

        if let colors = mapDisplayDataItem.borderColors {
            let degreesForColor = 360.0 / Float(colors.count)
            for index in 0..<colors.count {
                var startAngle = degreesForColor * Float(index)
                if colors.count == 3 {
                    startAngle += 90
                }
                let endAngle = startAngle + degreesForColor
                let colorBorderLayer = createArcShapeLayer(bounds: view.bounds, startAngle: CGFloat(startAngle * Float.pi / 180), endAngle: CGFloat(endAngle * Float.pi / 180), color: colors[index], lineWidth: ShoppingMapViewController.pinBorderWidth)
                view.layer.addSublayer(colorBorderLayer)
            }
        }

        let contentView = UIView(frame: CGRect(x: 0, y: 0, width: ShoppingMapViewController.sizeMapIcon, height: ShoppingMapViewController.sizeMapIcon))

        contentView.layer.cornerRadius = ShoppingMapViewController.sizeMapIcon / 2
        contentView.addSubview(view)

        return contentView
    }

    private func createArcShapeLayer(bounds: CGRect, startAngle: CGFloat, endAngle: CGFloat, color: UIColor, lineWidth: CGFloat) -> CAShapeLayer {

        let position = CGPoint(x: bounds.width / 2, y: bounds.height / 2 )

        let semiCircleLayer = CAShapeLayer()

        let semiCirclePath = UIBezierPath(arcCenter: position, radius: bounds.width / 2 - ( lineWidth / 2 ), startAngle: startAngle, endAngle: endAngle, clockwise: true)

        semiCircleLayer.path = semiCirclePath.cgPath
        semiCircleLayer.strokeColor = color.cgColor
        semiCircleLayer.lineWidth = lineWidth
        semiCircleLayer.fillColor = UIColor.clear.cgColor

        return semiCircleLayer
    }

    override func initModel(withModel model: Model) {
        presenter.setModel(model: model)
    }

    func deselectMarker() {

        if let markerBeforeSelected = selectedMarker, let markerView = markerBeforeSelected.iconView {

            markerBeforeSelected.tracksViewChanges = true
            let contentView = markerView.subviews[0]

            UIView.animate(withDuration: ShoppingMapViewController.animationTapPromotionDuration, animations: {

                contentView.transform = .identity
            }, completion: { _ in

                markerBeforeSelected.tracksViewChanges = false

                markerView.frame = CGRect(center: markerView.center, size: CGSize(width: ShoppingMapViewController.sizeMapIcon, height: ShoppingMapViewController.sizeMapIcon))
                contentView.center = CGPoint(x: ShoppingMapViewController.sizeMapIcon / 2, y: ShoppingMapViewController.sizeMapIcon / 2)
                markerView.layer.cornerRadius = ShoppingMapViewController.sizeMapIcon / 2
            })

        }

        selectedMarker?.zIndex = ShoppingMapViewController.minimumZIndex

        selectedMarker = nil

    }

    @objc func cameraMoved(timer: Timer) {

        if let coordinate = timer.userInfo as? CLLocationCoordinate2D {
            DLog(message: "Timer expired then camera moved to \(coordinate.latitude) \(coordinate.longitude)")

            presenter?.movePosition(withLatitude: coordinate.latitude, andLongitude: coordinate.longitude)
        }

        self.timer?.invalidate()
        self.timer = nil

    }

    fileprivate func numberOfCollectionViewCells() -> Int {

        guard let promotionsDisplayData = promotionsDisplayData else {
            return 0
        }

        return promotionsDisplayData.shouldShowMorePromotions ? promotionsDisplayData.itemsToShow.count + 1 : promotionsDisplayData.itemsToShow.count

    }

    deinit {
        DLog(message: "Deinitialized ShoppingMapViewController")
    }

}

extension ShoppingMapViewController: ShoppingMapViewProtocol {

    func showError(error: ModelBO) {
        presenter?.showModal()
    }

    func changeColorEditTexts() {

    }

    func showToast(withInfoText text: String) {

        ToastManager.shared().showToast(withText: text, backgroundColor: .DARKMEDIUMBLUE, height: ShoppingMapViewController.heightToastView)
    }

    func centerMap(withLatitude latitude: Double, andLongitude longitude: Double, withRadiousInMeters radiousInMeters: Double) {

        let location = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)

        if storesCircle == nil {
            storesCircle = self.createCircle(forCoordinate: location, andRadius: radiousInMeters)
        } else if storesCircle?.position.latitude != latitude || storesCircle?.position.longitude != longitude {
            storesCircle?.map = nil
            storesCircle = self.createCircle(forCoordinate: location, andRadius: radiousInMeters)
        }

        if let circle = storesCircle {

            if storesCircle?.map == nil {
                storesCircle?.map = mapView
            }
            let cameraUpdate = GMSCameraUpdate.fit(circle.bounds())

            mapView.animate(with: cameraUpdate)

        }

        isUserAction = false

    }

    func goToLocationSettings() {

        if let url = URL(string: UIApplicationOpenSettingsURLString) {

            URLOpener().openURL(url)
        }

    }

    func showUserLocation() {

        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true

    }

    func hideUserLocation() {
        mapView.settings.myLocationButton = false
    }

    func showStores(withDisplayData displayData: MapDisplayData) {

        mapView.clear()

        let deadlineTime = DispatchTime.now() + .milliseconds(100)
        DispatchQueue.main.asyncAfter(deadline: deadlineTime) { [weak self] in

            for displayDataItem in displayData.items {

                let location = CLLocationCoordinate2D(latitude: displayDataItem.latitude, longitude: displayDataItem.longitude)

                let marker = GMSMarker(position: location)
                marker.iconView = self?.createIcon(mapDisplayDataItem: displayDataItem)
                marker.map = self?.mapView
                marker.userData = displayDataItem
                marker.tracksViewChanges = false
                marker.groundAnchor = CGPoint(x: 0.5, y: 0.5)
            }

        }

        storesCircle?.map = mapView

    }

    func animateShowPromotions() {

        NSLayoutConstraint.deactivate([promotionsCollectionViewLeadingConstraint])
        NSLayoutConstraint.deactivate([promotionsCollectionViewTranilingEqualsToLeadingMap])
        NSLayoutConstraint.activate([promotionsCollectionViewLeadingToLeadingConstraint])

        UIView.animate(withDuration: ShoppingMapViewController.animationTapPromotionDuration / 2, animations: {
            self.promotionsCollectionView.isHidden = false
            self.promotionsContainerView.layoutIfNeeded()
        })

    }

    func showPromotions(withDisplayData displayData: MapDisplayDataPromotions) {

        self.promotionsContainerView.isUserInteractionEnabled = true

        if promotionsCollectionViewLeadingToLeadingConstraint.isActive {

            NSLayoutConstraint.deactivate([promotionsCollectionViewLeadingConstraint])
            NSLayoutConstraint.deactivate([promotionsCollectionViewLeadingToLeadingConstraint])
            NSLayoutConstraint.activate([promotionsCollectionViewTranilingEqualsToLeadingMap])

            UIView.animate(withDuration: ShoppingMapViewController.animationTapPromotionDuration / 2, animations: {
                self.promotionsContainerView.layoutIfNeeded()
            }, completion: { _  in

                self.promotionsDisplayData = displayData
                self.promotionsCollectionView.reloadData()
                self.promotionsCollectionView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)

                NSLayoutConstraint.deactivate([self.promotionsCollectionViewTranilingEqualsToLeadingMap])
                NSLayoutConstraint.activate([self.promotionsCollectionViewLeadingConstraint])
                self.promotionsContainerView.layoutIfNeeded()

                self.animateShowPromotions()

            })

        } else {

            self.promotionsDisplayData = displayData
            self.promotionsCollectionView.reloadData()

            self.animateShowPromotions()

        }

    }

    func hidePromotions() {

        currentCellIndex = 0

        if !hiddingPromotions {
            hiddingPromotions = true

            deselectMarker()

            NSLayoutConstraint.deactivate([promotionsCollectionViewLeadingConstraint])
            NSLayoutConstraint.deactivate([promotionsCollectionViewLeadingToLeadingConstraint])
            NSLayoutConstraint.activate([promotionsCollectionViewTranilingEqualsToLeadingMap])

            UIView.animate(withDuration: ShoppingMapViewController.animationTapPromotionDuration / 2, animations: {
                self.promotionsContainerView.layoutIfNeeded()
            }, completion: { _  in

                self.hiddingPromotions = false

                self.promotionsCollectionView.isHidden = true
                NSLayoutConstraint.deactivate([self.promotionsCollectionViewTranilingEqualsToLeadingMap])
                NSLayoutConstraint.activate([self.promotionsCollectionViewLeadingConstraint])

                self.promotionsDisplayData = nil
                self.promotionsCollectionView.reloadData()

                self.promotionsContainerView.isUserInteractionEnabled = false

            })
        }

    }

    func showFilterButton() {

        let image = SVGCache.image(forSVGNamed: ConstantsImages.Common.search_icon, color: .BBVAWHITE)
        setNavigationRightButton(withImage: image, action: #selector(filterAction))

    }

    func showFilters(withDisplayData displayData: [FilterScrollDisplayData]) {

        filtersScrollView.loadFilters(filters: displayData)
        filtersScrollViewHeightConstraint.constant = FiltersScrollView.default_height

    }

    func hideFilters() {

        filtersScrollView.loadFilters(filters: nil)
        filtersScrollViewHeightConstraint.constant = 0

        UIView.animate(withDuration: ShoppingMapViewController.timeToHideFilterAnimation) {
            self.view.layoutIfNeeded()
        }

    }

}

extension ShoppingMapViewController: CloseModalDelegate {

    func errorViewControllerAcceptPressed(_ errorViewController: ErrorViewController) {
        presenter?.errorAcceptPressed()
    }

    func errorViewControllerCancelPressed(_ errorViewController: ErrorViewController) {
        presenter?.errorCancelPressed()
    }

}

extension ShoppingMapViewController: GMSMapViewDelegate {

    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {

        if selectedMarker != marker {

            guard let iconView = marker.iconView else {
                return false
            }
            
            let contentView = iconView.subviews[0]
            
            deselectMarker()

            view.isUserInteractionEnabled = false

            iconView.frame = CGRect(center: iconView.center, size: CGSize(width: ShoppingMapViewController.sizeMapIconSelected, height: ShoppingMapViewController.sizeMapIconSelected))
            contentView.center = CGPoint(x: ShoppingMapViewController.sizeMapIconSelected / 2, y: ShoppingMapViewController.sizeMapIconSelected / 2)
            iconView.layer.cornerRadius = ShoppingMapViewController.sizeMapIconSelected / 2

            marker.tracksViewChanges = true
            
            UIView.animate(withDuration: ShoppingMapViewController.animationTapPromotionDuration, animations: {

                contentView.transform = CGAffineTransform(scaleX: ShoppingMapViewController.sizeMapIconSelected / ShoppingMapViewController.sizeMapIcon, y: ShoppingMapViewController.sizeMapIconSelected / ShoppingMapViewController.sizeMapIcon)
            }, completion: { _ in

                marker.tracksViewChanges = false
                self.view.isUserInteractionEnabled = true
            })

            selectedMarker = marker
            marker.zIndex = ShoppingMapViewController.maximumZIndex

            if let displayData = selectedMarker?.userData as? MapDisplayDataItem {

                isUserAction = false

                presenter?.didTapInStore(withDisplayData: displayData)
            }

        }

        return false
    }

    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        presenter?.didTapInMap()
        isUserAction = true
    }

    func mapView(_ mapView: GMSMapView, didChange position: GMSCameraPosition) {

        timer?.invalidate()
        timer = nil

        if isUserAction {

            presenter?.didTapInMap()

            storesCircle?.position = position.target
            if storesCircle?.map == nil {
                storesCircle?.map = mapView
            }
        }

    }

    func mapView(_ mapView: GMSMapView, idleAt position: GMSCameraPosition) {

        timer?.invalidate()
        timer = nil

        if isUserAction {

            storesCircle?.position = position.target
            if storesCircle?.map == nil {
                storesCircle?.map = mapView
            }

            timer = Timer.scheduledTimer(timeInterval: TimeInterval(ShoppingMapViewController.timeToWaitBeforeToInformNewLocationInSeconds), target: self, selector: (#selector(ShoppingMapViewController.cameraMoved(timer:))), userInfo: position.target, repeats: false)
        }

        DLog(message: "Camera idleAt to \(position.target.latitude) \(position.target.longitude)")
        isUserAction = view.isUserInteractionEnabled

    }
}

extension ShoppingMapViewController: UICollectionViewDelegate {

    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {

        let pageWidth = promotionsCellWidth + promotionsBetweenCellsSpacing

        var targetIndex = Int(0)

        if velocity.x == 0 {

            let nearestIndex = floor((targetContentOffset.pointee.x - pageWidth / 2) / pageWidth) + 1.0

            targetIndex = Int(nearestIndex)
            targetContentOffset.pointee.x = CGFloat(currentCellIndex) * pageWidth
            scrollToOffset = false

        } else {

            let targetIndexFloat = targetContentOffset.pointee.x / pageWidth

            targetIndex = Int(round(targetIndexFloat))

            if targetIndex == currentCellIndex {

                if velocity.x < -ShoppingMapViewController.automaticFlickMinimumSpeed && targetIndex > 0 {
                    targetIndex -= 1
                } else if velocity.x > ShoppingMapViewController.automaticFlickMinimumSpeed && targetIndex < (numberOfCollectionViewCells() - 1) {
                    targetIndex += 1
                }
            }

            targetContentOffset.pointee.x = scrollView.contentOffset.x
            scrollToOffset = true
            lastCalculatedOffsetX = CGFloat(targetIndex) * pageWidth
        }

        currentCellIndex = targetIndex
    }

    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {

        if scrollToOffset {
            UIView.animate(withDuration: 0.25, animations: {
                scrollView.contentOffset = CGPoint(x: self.lastCalculatedOffsetX, y: 0)
            })
            scrollToOffset = false
        }
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let promotionsDisplayData = promotionsDisplayData else {
            return
        }
        let showMoreDisplayed = promotionsDisplayData.itemsToShow.count == indexPath.row && promotionsDisplayData.shouldShowMorePromotions
        if showMoreDisplayed {
            presenter.didTapInPromotionPlusCell()
        } else {
            presenter.didTapInPromotionCell(atIndex: indexPath.row)
        }
    }
}

extension ShoppingMapViewController: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return numberOfCollectionViewCells()
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        guard let promotionsDisplayData = promotionsDisplayData else {
            return UICollectionViewCell()

        }

        if promotionsDisplayData.itemsToShow.count == indexPath.row && promotionsDisplayData.shouldShowMorePromotions {

            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: MorePromotionsCollectionViewCell.identifier, for: indexPath) as? MorePromotionsCollectionViewCell {

                cell.configureCell(promotionsCount: promotionsDisplayData.totalItemsCount)

                return cell
            }

        } else {

            let promotionItem = promotionsDisplayData.itemsToShow[indexPath.row]

            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: PromotionCollectionViewCell.identifier, for: indexPath) as? PromotionCollectionViewCell {

                cell.configureCellWithDisplayPromotionData(withDisplayitemPromotion: promotionItem)

                return cell
            }

        }

        return UICollectionViewCell()

    }

}

extension GMSCircle {

    func bounds() -> GMSCoordinateBounds {
        func locationMinMax(positive: Bool) -> CLLocationCoordinate2D {
            let sign: Double = positive ? 1 : -1
            let dx = sign * self.radius / 6378000 * (180 / Double.pi)
            let lat = position.latitude + dx
            let lon = position.longitude + dx / cos(position.latitude * Double.pi / 180)
            return CLLocationCoordinate2D(latitude: lat, longitude: lon)
        }

        return GMSCoordinateBounds(coordinate: locationMinMax(positive: true),
                                   coordinate: locationMinMax(positive: false))
    }

}

extension ShoppingMapViewController: FiltersScrollViewDelegate {

    func removeFilterPushed(filter: FilterScrollDisplayData, inFiltersScrollView filtersScrollView: FiltersScrollView) {

        presenter.removedFilter(withDisplayData: filter)

    }

    func removeFilterLonglyPushed(inFiltersScrollView filtersScrollView: FiltersScrollView) {
    }

    func removeAllFiltersPushed(inFiltersScrollView filtersScrollView: FiltersScrollView) {

        presenter.removedAllFilters()

    }
}
