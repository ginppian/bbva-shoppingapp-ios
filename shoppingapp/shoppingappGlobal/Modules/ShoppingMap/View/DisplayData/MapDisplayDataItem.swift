//
//  MapDisplayDataItem.swift
//  shoppingapp
//
//  Created by jesus.martinez on 11/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class MapDisplayDataItem {

    static let defaultTitle = "+1"

    let borderColors: [UIColor]?
    let backgroundColor: UIColor
    let imageColor: UIColor
    let imageNamed: String?
    let title: String?
    let latitude: Double
    let longitude: Double
    let idStores: [String]
    var selectedItem: Bool = false

    init?(withDataPoi dataPoi: DataPoi, andCategory category: CategoryBO?) {

        idStores = dataPoi.storeIds
        latitude = dataPoi.latitude
        longitude = dataPoi.longitude

        // Only 1 Promotion --> Normal Poi
        if dataPoi.promotions.count == 1, let promotion = dataPoi.promotions.first {

            imageColor = .BBVAWHITE

            title = nil
            borderColors = nil

            if let color = MapDisplayDataItem.color(forPromotion: promotion) {
                backgroundColor = color
            } else {
                return nil
            }

            var categoryId = promotion.categories?[0].id

            if let category = category {
                categoryId = category.id
            }

            if let categoryId = categoryId {
                imageNamed = categoryId.imageNamed()
            } else {
                imageNamed = ConstantsImages.Promotions.promotion
            }
                        
        } else {

            // Multiples promotions
            backgroundColor = .BBVAWHITE
            imageColor = .BBVA600

            var colors = [UIColor]()

            for promotion in dataPoi.promotions {

                if let color = MapDisplayDataItem.color(forPromotion: promotion), !colors.contains(color) {

                    colors.append(color)

                }

            }

            guard !colors.isEmpty else {
                return nil
            }

            borderColors = colors

            let categoriesEquals = dataPoi.categoriesEquals()

            if let categoriesEquals = categoriesEquals, !categoriesEquals.isEmpty {

                if dataPoi.hasStoresEqual() {

                    var categoryId = categoriesEquals[0]

                    if let category = category {
                        categoryId = category.id
                    }

                    imageNamed = categoryId.imageNamed()
                    title = nil

                } else {

                    imageNamed = nil
                    title = MapDisplayDataItem.defaultTitle

                }

            } else {

                imageNamed = nil
                title = MapDisplayDataItem.defaultTitle

            }

        }

    }

    private static func color(forPromotion promotion: PromotionBO) -> UIColor? {

        if promotion.isDiscountPromotionType() {

            return .BBVADARKPINK

        } else if promotion.isToMonthPromotionType() {

            return .BBVAMEDIUMYELLOW

        } else if promotion.isPointsPromotionType() {

            return .DARKAQUA

        } else {

            return nil

        }

    }

}
