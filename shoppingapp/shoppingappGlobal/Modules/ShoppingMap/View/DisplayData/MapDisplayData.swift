//
//  MapDisplayData.swift
//  shoppingapp
//
//  Created by jesus.martinez on 11/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class MapDisplayData {

    var items: [MapDisplayDataItem]

    init() {
        self.items = [MapDisplayDataItem]()
    }

    convenience init(withDataPois dataPois: [DataPoi], forCategory category: CategoryBO?) {

        self.init()

        for dataPoi in dataPois {

            if let mapDisplayDataItem = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: category) {
                items.append(mapDisplayDataItem)
            }

        }

    }

}
