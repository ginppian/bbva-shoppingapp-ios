//
//  MapDisplayItemsPromotions.swift
//  shoppingapp
//
//  Created by jesus.martinez on 16/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

struct MapDisplayDataPromotions {

    var shouldShowMorePromotions = false
    var totalItemsCount = 0
    var itemsToShow = [DisplayItemPromotion]()

}
