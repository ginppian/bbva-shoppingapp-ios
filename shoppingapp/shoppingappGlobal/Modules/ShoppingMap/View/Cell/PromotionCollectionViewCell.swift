//
//  PromotionCollectionViewCell.swift
//  shoppingapp
//
//  Created by jesus.martinez on 31/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit

class PromotionCollectionViewCell: UICollectionViewCell {

    static let identifier = "PromotionCollectionViewCell"
    static let height: CGFloat = 144

    @IBOutlet weak var promotionImageView: UIImageView!
    @IBOutlet weak var promotionTypeView: PromotionTypeView!

    @IBOutlet weak var commerceNameLabel: UILabel!
    @IBOutlet weak var descriptionOfferLabel: UILabel!
    @IBOutlet weak var benefitLabel: UILabel!
    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var promotionTypeViewWidth: NSLayoutConstraint!
    @IBOutlet weak var containerView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()

        backgroundColor = .clear

        promotionImageView.backgroundColor = .BBVA200

        commerceNameLabel.font = Tools.setFontBook(size: 12)
        commerceNameLabel.textColor = .BBVA600

        descriptionOfferLabel.font = Tools.setFontMedium(size: 14)
        descriptionOfferLabel.textColor = .BBVA600

        benefitLabel.font = Tools.setFontMedium(size: 14)
        benefitLabel.textColor = .MEDIUMBLUE

        distanceLabel.font = Tools.setFontBookItalic(size: 14)
        distanceLabel.textColor = .BBVA500

        addShadow()
    }

    private func addShadow() {
        
          addShadow(color: .black, opacity: 0.33)
          layer.masksToBounds = false
    }

    func configureCellWithDisplayPromotionData(withDisplayitemPromotion displayItemPromotion: DisplayItemPromotion) {

        commerceNameLabel.text = displayItemPromotion.commerceName
        descriptionOfferLabel.text = displayItemPromotion.descriptionPromotion
        benefitLabel.text = displayItemPromotion.benefit

        if !Tools.isStringNilOrEmpty(withString: displayItemPromotion.imageList) {
            configureImage(withDisplayitemPromotion: displayItemPromotion)
        } else {
            promotionImageView.image = UIImage(named: ConstantsImages.Promotions.default_promotion_list_image)
            promotionImageView.contentMode = .scaleAspectFill
        }

        distanceLabel.text = displayItemPromotion.dateDiffPlusDistance ?? displayItemPromotion.dateDiff

        promotionTypeView.configurePromotionType(withPromotionTypeDisplayData: createPromotionTypeDisplayData(withPromotion: displayItemPromotion.promotionBO!))

        setAccessibilities()
    }

    private func createPromotionTypeDisplayData(withPromotion promotion: PromotionBO) -> PromotionTypeDisplayData {
        let promotionTypeDisplayData = PromotionTypeDisplayData(fromPromotion: promotion)
        promotionTypeViewWidth.constant = promotionTypeDisplayData.promotionTypeViewWidth

        return promotionTypeDisplayData
    }

    private func configureImage(withDisplayitemPromotion displayItemPromotion: DisplayItemPromotion) {

        if let imageSaved = displayItemPromotion.promotionBO?.imageListSaved {

            promotionImageView.image = imageSaved
            promotionImageView.backgroundColor = UIColor.white
        } else {

            let url = URL(string: displayItemPromotion.imageList?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")
            promotionImageView.backgroundColor = .BBVA200
            promotionImageView.contentMode = .scaleAspectFit

            promotionImageView.setImage(fromUrl: url,
                                        placeholderImage: UIImage(named: ConstantsImages.Promotions.default_promotion_list_image)) {  [weak self] successful in
                                            if successful {
                                                displayItemPromotion.promotionBO?.imageDetailSaved = self?.promotionImageView.image
                                                DispatchQueue.main.async {
                                                    self?.promotionImageView.backgroundColor = .BBVAWHITE
                                                }
                                            } else {
                                                self?.promotionImageView.contentMode = .scaleAspectFill
                                            }
            }
        }
    }
}

// MARK: - APPIUM

private extension PromotionCollectionViewCell {

    func setAccessibilities() {

        #if APPIUM

            accessibilityIdentifier = ConstantsAccessibilities.PROMOTION_SECONDARY_PROMOTION_CELL

            Tools.setAccessibility(view: commerceNameLabel, identifier: ConstantsAccessibilities.PROMOTION_PROMOTION_COMMERCE)
            Tools.setAccessibility(view: descriptionOfferLabel, identifier: ConstantsAccessibilities.PROMOTION_PROMOTION_DESCRIPTION)

            Tools.setAccessibility(view: benefitLabel, identifier: ConstantsAccessibilities.PROMOTION_PROMOTIONS_HOT_BENEFIT)

        #endif

    }

}
