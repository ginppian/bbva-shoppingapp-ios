//
//  MorePromotionsCollectionViewCell.swift
//  shoppingapp
//
//  Created by Javier Pino on 13/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit

class MorePromotionsCollectionViewCell: UICollectionViewCell {

    static let identifier = "MorePromotionsCollectionViewCell"
    static let height: CGFloat = 144

    @IBOutlet weak var iconImageView: UIImageView!
    @IBOutlet weak var morePromotionslabel: UILabel!
    @IBOutlet weak var containerView: UIView!

    override var isHighlighted: Bool {
        willSet {

            let color: UIColor = newValue ? .COREBLUE : .MEDIUMBLUE

            iconImageView.image = SVGCache.image(forSVGNamed: ConstantsImages.Common.add_icon, color: color)
            morePromotionslabel.textColor = color

        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()

        backgroundColor = .clear

        morePromotionslabel.font = Tools.setFontMedium(size: 14)
        morePromotionslabel.textColor = .MEDIUMBLUE
        morePromotionslabel.text = Localizables.promotions.key_promotions_see_all_fem_text

        iconImageView.image = SVGCache.image(forSVGNamed: ConstantsImages.Common.add_icon, color: .MEDIUMBLUE)

        addShadow()
    }

    private func addShadow() {
        
        addShadow(color: .black, opacity: 0.33)
        layer.masksToBounds = false
    }

    func configureCell(promotionsCount: Int) {

        morePromotionslabel.text = Localizables.promotions.key_promotions_see_all_fem_text + " (\(promotionsCount))"
    }
}
