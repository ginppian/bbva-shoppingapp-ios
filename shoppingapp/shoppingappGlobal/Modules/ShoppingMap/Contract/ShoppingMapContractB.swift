//
//  ShoppingMapContractB.swift
//  shoppingappMX
//
//  Created by jesus.martinez on 15/6/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol ShoppingMapViewProtocolB: ShoppingMapViewProtocol {

}

protocol ShoppingMapPresenterProtocolB: ShoppingMapPresenterProtocol {
}
