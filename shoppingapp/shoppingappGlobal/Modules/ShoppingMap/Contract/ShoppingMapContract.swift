//
//  ShoppingMapContract.swift
//  shoppingapp
//
//  Created by jesus.martinez on 4/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import RxSwift

protocol ShoppingMapViewProtocol: ViewProtocol {

    func showToast(withInfoText text: String)
    func centerMap(withLatitude latitude: Double, andLongitude longitude: Double, withRadiousInMeters radiousInMeters: Double)
    func goToLocationSettings()
    func showStores(withDisplayData displayData: MapDisplayData)

    func showUserLocation()
    func hideUserLocation()

    func showLoader()
    func hideLoader()

    func showPromotions(withDisplayData displayData: MapDisplayDataPromotions)
    func hidePromotions()

    func showFilterButton()

    func showFilters(withDisplayData displayData: [FilterScrollDisplayData])
    func hideFilters()

}

protocol ShoppingMapPresenterProtocol: PresenterProtocol {

    func viewDidLoad()
    func viewWillAppear()

    func errorAcceptPressed()
    func errorCancelPressed()

    func didTapInStore(withDisplayData displayData: MapDisplayDataItem)
    func didTapInMap()

    func movePosition(withLatitude latitude: Double, andLongitude longitude: Double)
    func filterPressed()

    func removedFilter(withDisplayData displayData: FilterScrollDisplayData)
    func removedAllFilters()

    func didTapInPromotionPlusCell()
    func didTapInPromotionCell(atIndex index: Int)
    
    func filter(withModel model: Model)
}

protocol ShoppingMapInteractorProtocol {

    func providePromotions(byParams params: ShoppingParamsTransport) -> Observable <ModelBO>
    func providePromotions(byParams params: ShoppingParamsTransport, withFilters filters: ShoppingFilter?) -> Observable<ModelBO>

}
