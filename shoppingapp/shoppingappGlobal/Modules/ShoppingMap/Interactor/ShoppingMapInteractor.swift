//
//  ShoppingMapInteractor.swift
//  shoppingapp
//
//  Created by jesus.martinez on 4/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import RxSwift

class ShoppingMapInteractor: ShoppingMapInteractorProtocol {

    let disposeBag = DisposeBag()

    func providePromotions(byParams params: ShoppingParamsTransport) -> Observable <ModelBO> {

        return Observable.create { observer in

            ShoppingDataManager.sharedInstance.providePromotionsPhysical(byParams: params, cancellable: true).subscribe(
                onNext: { result in

                    if let promotionsEntity = result as? PromotionsEntity {
                        observer.onNext(PromotionsBO(promotionsEntity: promotionsEntity))
                        observer.onCompleted()
                    } else {
                        DLog(message: "Incorrect Promotions Entity")
                    }
                }, onError: { error in

                let evaluate: ServiceError = (error as? ServiceError)!

                switch evaluate {
                case .GenericErrorEntity(let errorEntity):
                    let errorBVABO = ErrorBO(error: errorEntity)
                    observer.onError(ServiceError.GenericErrorBO(error: errorBVABO))
                default:
                    observer.onError(evaluate)
                }

                }, onCompleted: {
                }).addDisposableTo(self.disposeBag)
            return Disposables.create()
        }

    }

    func providePromotions(byParams params: ShoppingParamsTransport, withFilters filters: ShoppingFilter?) -> Observable<ModelBO> {

        return Observable.create { observer in

            ShoppingDataManager.sharedInstance.providePromotions(byParams: params, withFilters: filters, cancellable: true).subscribe(

                onNext: { result in

                    if let promotionsEntity = result as? PromotionsEntity {
                        observer.onNext(PromotionsBO(promotionsEntity: promotionsEntity))
                        observer.onCompleted()
                    } else {
                        DLog(message: "Incorrect Promotions Entity")
                    }

                }, onError: { error in

                let evaluate: ServiceError = (error as? ServiceError)!

                switch evaluate {
                case .GenericErrorEntity(let errorEntity):
                    let errorBVABO = ErrorBO(error: errorEntity)
                    observer.onError(ServiceError.GenericErrorBO(error: errorBVABO))
                default:
                    observer.onError(evaluate)
                }

                }, onCompleted: {
                }).addDisposableTo(self.disposeBag)
            return Disposables.create()
        }

    }

}
