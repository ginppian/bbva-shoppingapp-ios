//
//  ShoppingMapPresenter.swift
//  shoppingapp
//
//  Created by jesus.martinez on 4/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import RxSwift
import CellsNativeComponents

class ShoppingMapPresenter<T: ShoppingMapViewProtocol>: BasePresenter<T> {

    let disposeBag = DisposeBag()

    var interactor: ShoppingMapInteractorProtocol?

    var categoryBO: CategoryBO?
    var trendingType: TrendingType?
    var categoriesBO: CategoriesBO?
    var dataPois = [DataPoi]()

    var maxPromotionsInCarrousel = Settings.PromotionsMap.max_promotions_in_carrousel
    var defaultDistance = Settings.PromotionsMap.default_distance_in_map
    var lengthType = Settings.PromotionsMap.default_length_type
    var pageSize = Settings.PromotionsMap.page_size
    var isGPSDisabled = false

    var defaultLocation = Settings.Global.default_location
    var userLocation: GeoLocationBO?
    var lastLocationRetrieve: GeoLocationBO?

    var displayData = MapDisplayData()
    var displayDataPromotions = MapDisplayDataPromotions()

    var promotionsInCarrousel = [PromotionBO]()

    var filters: ShoppingFilter?

    var locationManager: LocationManagerProtocol?

    required init() {
        super.init(tag: ShoppingMapPageReaction.ROUTER_TAG)
    }

    override init(busManager: BusManager) {
        super.init(busManager: busManager)
        routerTag = ShoppingMapPageReaction.ROUTER_TAG
    }

    override func setModel(model: Model) {

        if let promotionsTransport = model as? PromotionsTransport {

            categoryBO = promotionsTransport.category
            trendingType = promotionsTransport.trendingType
            categoriesBO = promotionsTransport.categories

        }
    }

    private func manageSuccess(withResult result: ModelBO) {

        if let resultPromotionsBO = result as? PromotionsBO {

            self.dataPois = resultPromotionsBO.groupByLatitudeLongitudeStoreAndPromotions()

            self.displayData = MapDisplayData(withDataPois: self.dataPois, forCategory: self.categoryBO)

            self.view?.showStores(withDisplayData: self.displayData)
            self.view?.hidePromotions()

            if self.displayData.items.isEmpty {
                self.view?.showToast(withInfoText: Localizables.promotions.key_promotions_stores_zero_result_text)
            }

        }

        self.view?.hideLoader()

    }

    private func manage(error: Error) {

        self.view?.hideLoader()
        self.view?.hidePromotions()

        let evaluate: ServiceError = (error as? ServiceError)!

        switch evaluate {

        case .GenericErrorBO(let errorBO):
            self.errorBO = errorBO
            self.checkError()
        default:
            break
        }

    }

    func shoppingParams(forGeolocation geolocation: GeoLocationBO, withCategoryId categoryId: CategoryType? = nil, andTrendingType trendingId: TrendingType? = nil) -> ShoppingParamsTransport {

        let locationTranport = ShoppingParamLocation(geolocation: GeoLocationBO(withLatitude: geolocation.latitude, withLongitude: geolocation.longitude), distanceParam: ShoppingDistanceParam(distance: defaultDistance, lengthType: lengthType))
        return ShoppingParamsTransport(categoryId: categoryId?.rawValue, trendingId: trendingId?.rawValue, location: locationTranport, pageSize: pageSize)
    }

    func retrieveStores(withLatitude latitude: Double, andLongitude longitude: Double) {

        view?.showLoader()
        let geolocation = GeoLocationBO(withLatitude: latitude, withLongitude: longitude)
        lastLocationRetrieve = geolocation

        let transport = shoppingParams(forGeolocation: geolocation, withCategoryId: categoryBO?.id, andTrendingType: trendingType)

        interactor?.providePromotions(byParams: transport).subscribe(
            onNext: { [weak self] result in

                self?.manageSuccess(withResult: result)

            },
            onError: { [weak self] error in

                self?.manage(error: error)

            }).addDisposableTo(disposeBag)

    }

    func retrieveStores(withFilters filters: ShoppingFilter?, andGeolocalization geolocation: GeoLocationBO) {

        view?.showLoader()

        let filtersToInteractor = ShoppingFilter()
        filtersToInteractor.textFilter = filters?.textFilter
        filtersToInteractor.promotionTypeFilter = filters?.promotionTypeFilter
        filtersToInteractor.promotionUsageTypeFilter = [.physical]
        filtersToInteractor.categoriesFilter = filters?.categoriesFilter

        let transport = shoppingParams(forGeolocation: geolocation)

        interactor?.providePromotions(byParams: transport, withFilters: filtersToInteractor).subscribe(
            onNext: { [weak self] result in

                self?.manageSuccess(withResult: result)

            },
            onError: { [weak self] error in

                self?.manage(error: error)

            }).addDisposableTo(disposeBag)

    }

    func showErrorLocationPermission() {

        if errorBO == nil {

            let attributtedText = CustomAttributedText.attributedBoldText(forFullText: Localizables.promotions.key_activate_gps_advise_text, withBoldText: Localizables.promotions.key_activate_location_text, withFont: Tools.setFontBook(size: 14), withBoldFont: Tools.setFontBold(size: 14), withAlignment: .center, andForegroundColor: .BBVA600)

            errorBO = ErrorBO(messageAttributed: attributtedText, errorType: .info, allowCancel: true, okTitle: Localizables.common.key_common_accept_text)
            view?.showError(error: errorBO!)

        }
    }

    func showGpsAdviceTextAndRetrieveStoresOnDefaultLocation() {

        errorBO = nil

        view?.showToast(withInfoText: Localizables.promotions.key_enable_gps_advise_text)
        
        centerMapInDefaultLocation()
    }
    
    private func centerMapInDefaultLocation() {
        
        view?.centerMap(withLatitude: defaultLocation.latitude, andLongitude: defaultLocation.longitude, withRadiousInMeters: Double(defaultDistance * DistanceConstants.meters_to_kilometers))
        view?.hideUserLocation()
        retrieveStores(withLatitude: defaultLocation.latitude, andLongitude: defaultLocation.longitude)
    }

    func promotions(ofDataPois dataPois: [DataPoi], forLatitude latitude: Double, andLongitude longitude: Double) -> [PromotionBO] {

        for dataPoi in dataPois {

            if dataPoi.latitude == latitude && dataPoi.longitude == longitude {
                return dataPoi.promotions
            }

        }

        return [PromotionBO]()

    }

    func getLocation() {
        locationManager = locationManager ?? LocationManager()
        locationManager!.startLocationObserverAndCheckStatus()
        locationManager?.getLocation(onSuccess: { [weak self] location -> Void in
            if let `self` = self {
                if self.userLocation == nil {

                    let latitude = location.coordinate.latitude
                    let longitude = location.coordinate.longitude
                    self.userLocation = GeoLocationBO(withLatitude: latitude, withLongitude: longitude)

                    self.view?.showUserLocation()
                    self.view?.centerMap(withLatitude: latitude, andLongitude: longitude, withRadiousInMeters: Double(self.defaultDistance * DistanceConstants.meters_to_kilometers))

                    if self.lastLocationRetrieve == nil || DistanceTools.calculateDistanceInMetersBetween(latitue: self.lastLocationRetrieve!.latitude, longitude: self.lastLocationRetrieve!.longitude, andLatiudeToCompare: self.userLocation!.latitude, andLongitudeToCompare: self.userLocation!.longitude) >= (Double(self.defaultDistance * DistanceConstants.meters_to_kilometers) / 2) {
                        self.retrieveStores(withLatitude: latitude, andLongitude: longitude)
                    }

                }
            }
            }, onFail: { [weak self] locationErrorType -> Void in
                if let `self` = self {

                    switch locationErrorType {
                    case .gpsOff:
                        self.isGPSDisabled = true
                        self.showErrorLocationPermission()
                    case.permissionDenied:
                        self.isGPSDisabled = false
                        self.showErrorLocationPermission()
                    case .gpsFail:
                        self.errorBO = nil
                        self.userLocation = nil

                        let defaultLat = self.defaultLocation.latitude
                        let defaultLon = self.defaultLocation.longitude

                        self.view?.centerMap(withLatitude: defaultLat, andLongitude: defaultLon, withRadiousInMeters: Double(self.defaultDistance * DistanceConstants.meters_to_kilometers))
                        self.view?.hideUserLocation()

                        self.retrieveStores(withLatitude: defaultLat, andLongitude: defaultLon)
                    }
                }
        })
    }
    
    func dismissedGPSHelpScreen() {
        
        centerMapInDefaultLocation()
    }
}

extension ShoppingMapPresenter: ShoppingMapPresenterProtocol {

    func viewDidLoad() {

        if categoryBO == nil && trendingType == nil {
            view?.showFilterButton()
        }
    }

    func viewWillAppear() {
        
        if lastLocationRetrieve == nil {
            if isGPSDisabled {
                showGpsAdviceTextAndRetrieveStoresOnDefaultLocation()
                
            } else {
                self.getLocation()
            }
        }
    }

    func errorAcceptPressed() {

        if let errorBO = errorBO, errorBO.errorType == .info {
            if isGPSDisabled {
                busManager.navigateScreen(tag: routerTag, ShoppingMapPageReaction.EVENT_NAV_TO_NEXT_SCREEN, Void())
            } else {
                centerMapInDefaultLocation()
                view?.goToLocationSettings()
            }

        } else {
            checkError()
        }

        errorBO = nil

    }

    func errorCancelPressed() {

        showGpsAdviceTextAndRetrieveStoresOnDefaultLocation()

    }

    func didTapInStore(withDisplayData displayData: MapDisplayDataItem) {

        if !dataPois.isEmpty {

            displayDataPromotions = MapDisplayDataPromotions()

            promotionsInCarrousel = promotions(ofDataPois: dataPois, forLatitude: displayData.latitude, andLongitude: displayData.longitude)

            for i in 0..<min(maxPromotionsInCarrousel, promotionsInCarrousel.count) {

                let promotionInPosition = promotionsInCarrousel[i]

                displayDataPromotions.itemsToShow.append(DisplayItemPromotion.generateDisplayItemPromotionInMap(withPromotion: promotionInPosition, withStoreLatitude: displayData.latitude, andStoreLongitude: displayData.longitude, andWithUserLatitude: userLocation?.latitude, andUserLongitude: userLocation?.longitude))

            }

            displayDataPromotions.shouldShowMorePromotions = promotionsInCarrousel.count > maxPromotionsInCarrousel
            displayDataPromotions.totalItemsCount = promotionsInCarrousel.count

            view?.hideUserLocation()
            view?.showPromotions(withDisplayData: displayDataPromotions)

        }
    }

    func didTapInMap() {

        if userLocation != nil {
            view?.showUserLocation()
        }

        view?.hidePromotions()
    }

    func movePosition(withLatitude latitude: Double, andLongitude longitude: Double) {

        if let lastLocationRetrieve = lastLocationRetrieve, DistanceTools.calculateDistanceInMetersBetween(latitue: lastLocationRetrieve.latitude, longitude: lastLocationRetrieve.longitude, andLatiudeToCompare: latitude, andLongitudeToCompare: longitude) >= (Double(defaultDistance * DistanceConstants.meters_to_kilometers) / 2) {

            if let filters = filters {

                self.lastLocationRetrieve = GeoLocationBO(withLatitude: latitude, withLongitude: longitude)

                retrieveStores(withFilters: filters, andGeolocalization: self.lastLocationRetrieve!)

            } else {
                retrieveStores(withLatitude: latitude, andLongitude: longitude)
            }

        }

    }

    func filterPressed() {

        let model = ShoppingFilterTransport()
        model.shouldAllowSelectUsageType = false
        model.categoriesBO = categoriesBO
        model.filters = filters
        model.shouldFilterInPreviousScreen = true

        busManager.navigateScreen(tag: routerTag, ShoppingMapPageReaction.EVENT_NAV_TO_FILTER_SCREEN, model)

    }

    func removedFilter(withDisplayData displayData: FilterScrollDisplayData) {

        guard let filters = filters else {
            view?.hideFilters()
            return
        }

        filters.remove(byFilterType: displayData.filterType)

        if filters.isEmpty() {
            view?.hideFilters()
            self.filters = nil
        }

        if let lastPosition = lastLocationRetrieve {
            retrieveStores(withFilters: filters, andGeolocalization: lastPosition)
        }

    }

    func removedAllFilters() {

        filters = nil

        view?.hideFilters()

        if let lastPosition = lastLocationRetrieve {
            retrieveStores(withFilters: filters, andGeolocalization: lastPosition)
        }
    }

    func didTapInPromotionPlusCell() {
        let transport = ShoppingMapListTransport()
        transport.promotions = promotionsInCarrousel
        transport.userLocation = userLocation
        transport.storeLocation = lastLocationRetrieve
        busManager.navigateScreen(tag: routerTag, ShoppingMapPageReaction.EVENT_NAV_TO_SHOPPING_MAP_LIST, transport)
    }

    func didTapInPromotionCell(atIndex index: Int) {

        if let promotion = promotionsInCarrousel[safeElement: index] {
            let promotionDetailTransport = PromotionDetailTransport()
            promotionDetailTransport.promotionBO = promotion
            promotionDetailTransport.category = categoryBO
            promotionDetailTransport.distanceAndDateToShow = displayDataPromotions.itemsToShow[safeElement:index]?.dateDiffPlusDistance ?? displayDataPromotions.itemsToShow[safeElement:index]?.dateDiff
            promotionDetailTransport.userLocation = userLocation
            promotionDetailTransport.shownOnMap = true

            busManager.navigateScreen(tag: ShoppingMapPageReaction.ROUTER_TAG, ShoppingMapPageReaction.EVENT_NAV_TO_PROMOTION_DETAIL_SCREEN, promotionDetailTransport)
        }
    }
    
    func filter(withModel model: Model) {
        
        guard let lastLocationRetrieve = lastLocationRetrieve, let shoppingFilterTransport = model as? ShoppingFilterTransport, let newFilters = shoppingFilterTransport.filters else {
            return
        }
        
        newFilters.textFilter = shoppingFilterTransport.filters?.textFilter
        newFilters.promotionTypeFilter = shoppingFilterTransport.filters?.promotionTypeFilter
        newFilters.promotionUsageTypeFilter = shoppingFilterTransport.filters?.promotionUsageTypeFilter
        newFilters.categoriesFilter = shoppingFilterTransport.filters?.categoriesFilter
        
        if newFilters != filters {
            
            filters = newFilters
            
            if let categories = categoriesBO {
                
                var displayFilters = [FilterScrollDisplayData]()
                
                for titleKey in newFilters.retrieveLiterals(withCategories: categories) {
                    
                    displayFilters.append(FilterScrollDisplayData(title: titleKey.title, filterType: titleKey.key))
                }
                
                if !newFilters.isEmpty() {
                    view?.showFilters(withDisplayData: displayFilters)
                } else {
                    view?.hideFilters()
                }
                
                retrieveStores(withFilters: filters, andGeolocalization: lastLocationRetrieve)
                
            } else {
                
                view?.hideFilters()
            }
        }
    }
}
