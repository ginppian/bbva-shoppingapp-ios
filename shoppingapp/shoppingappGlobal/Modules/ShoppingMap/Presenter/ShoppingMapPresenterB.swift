//
//  ShoppingMapPresenterB.swift
//  shoppingappMX
//
//  Created by jesus.martinez on 15/6/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class ShoppingMapPresenterB<T: ShoppingMapViewProtocolB>: ShoppingMapPresenter<T> {

    var cardTitleIds: [String]?

    func receive(model: Model) {

        if let cards = model as? CardsBO, cardTitleIds != cards.titleIds() {
            cardTitleIds = cards.titleIds()
        }
    }

    override func shoppingParams(forGeolocation geolocation: GeoLocationBO, withCategoryId categoryId: CategoryType? = nil, andTrendingType trendingId: TrendingType? = nil) -> ShoppingParamsTransport {

        guard let cardTitleIds = cardTitleIds else {
            return super.shoppingParams(forGeolocation: geolocation, withCategoryId: categoryId, andTrendingType: trendingId)
        }

        let locationTranport = ShoppingParamLocation(geolocation: GeoLocationBO(withLatitude: geolocation.latitude, withLongitude: geolocation.longitude), distanceParam: ShoppingDistanceParam(distance: defaultDistance, lengthType: lengthType))
        return ShoppingParamsTransport(categoryId: categoryId?.rawValue, trendingId: trendingId?.rawValue, location: locationTranport, titleIds: cardTitleIds, pageSize: pageSize)
    }

}

extension ShoppingMapPresenterB: ShoppingMapPresenterProtocolB {

}
