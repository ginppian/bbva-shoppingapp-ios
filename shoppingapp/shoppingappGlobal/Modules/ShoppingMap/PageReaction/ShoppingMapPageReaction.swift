//
//  ShoppingMapPageReaction.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 26/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

class ShoppingMapPageReaction: PageReaction {

   static let ROUTER_TAG: String = "shopping-map-tag"

   static let EVENT_NAV_TO_NEXT_SCREEN: ActionSpec<Void> = ActionSpec<Void>(id: "EVENT_NAV_TO_NEXT_SCREEN")
   static let EVENT_NAV_TO_FILTER_SCREEN: ActionSpec<Model> = ActionSpec<Model>(id: "EVENT_NAV_TO_FILTER_SCREEN")
   static let EVENT_NAV_TO_SHOPPING_MAP_LIST: ActionSpec<Model> = ActionSpec<Model>(id: "EVENT_NAV_TO_SHOPPING_MAP_LIST")
   static let EVENT_NAV_TO_PROMOTION_DETAIL_SCREEN: ActionSpec<Model> = ActionSpec<Model>(id: "EVENT_NAV_TO_PROMOTION_DETAIL_SCREEN")

    override func configure() {

        navigateFirstScreenFromShoppingMap()
        navigateToFilterScreen()
        registerNavigateShoppingMapListFromShoppingMap()
        navigateToPromotionDetailScreen()
        reactToFilters()
        reactToDismiss()
    }

    func navigateFirstScreenFromShoppingMap() {

        _ = when(id: ShoppingMapPageReaction.ROUTER_TAG, type: Void.self)
            .doAction(actionSpec: ShoppingMapPageReaction.EVENT_NAV_TO_NEXT_SCREEN)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, _ in

                router.navigateToLocationSettingHelp()

            }
    }

    func navigateToFilterScreen() {

        _ = when(id: ShoppingMapPageReaction.ROUTER_TAG, type: Model.self)
            .doAction(actionSpec: ShoppingMapPageReaction.EVENT_NAV_TO_FILTER_SCREEN)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, model in

                router.navigateToShoppingFilter(withModel: model!)

            }
    }

    func registerNavigateShoppingMapListFromShoppingMap() {

        _ = when(id: ShoppingMapPageReaction.ROUTER_TAG, type: Model.self)
            .doAction(actionSpec: ShoppingMapPageReaction.EVENT_NAV_TO_SHOPPING_MAP_LIST)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, model in

                router.navigateShoppingMapListFromShoppingMap(withModel: model!)

            }
    }

    func navigateToPromotionDetailScreen() {

        _ = when(id: ShoppingMapPageReaction.ROUTER_TAG, type: Model.self)
            .doAction(actionSpec: ShoppingMapPageReaction.EVENT_NAV_TO_PROMOTION_DETAIL_SCREEN)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, model in

                router.navigateToShoppingDetail(withModel: model!)

            }
    }
    
    func reactToFilters() {
        
        _ = reactTo(channel: ShoppingFilterPageReaction.SHOPPING_FILTERS)?
            .then()
            .inside(componentID: ShoppingMapPageReaction.ROUTER_TAG, component: ShoppingMapPresenter<ShoppingMapViewController>.self)
            .doReactionAndClearChannel { component, model in
                if let model = model {
                    component.filter(withModel: model)
                }
        }
    }
    
    func reactToDismiss() {
        
        _ = reactTo(channel: GPSHelpPageReaction.channelDismissGpsHelpScreen)?
            .then()
            .inside(componentID: ShoppingMapPageReaction.ROUTER_TAG, component: ShoppingMapPresenter<ShoppingMapViewController>.self)
            .doReactionAndClearChannel(reactionExecution: { presenter, _ in
                
                presenter.dismissedGPSHelpScreen()
            })
    }
}
