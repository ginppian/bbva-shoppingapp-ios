//
//  ShoppingMapPageReactionB.swift
//  shoppingappMX
//
//  Created by jesus.martinez on 15/6/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class ShoppingMapPageReactionB: ShoppingMapPageReaction {

    override func configure() {
        super.configure()

        reactCards()
    }

    func reactCards() {

        _ = reactTo(channel: PageReactionConstants.CHANNEL_CARDS)?
            .then()
            .inside(componentID: ShoppingMapPageReaction.ROUTER_TAG, component: ShoppingMapPresenterB<ShoppingMapViewControllerB>.self)
            .doReaction { component, param in

                if let param = param {
                    component.receive(model: param)
                }
            }
    }
    
    override func reactToFilters() {
        
        _ = reactTo(channel: ShoppingFilterPageReaction.SHOPPING_FILTERS)?
            .then()
            .inside(componentID: ShoppingMapPageReaction.ROUTER_TAG, component: ShoppingMapPresenterB<ShoppingMapViewControllerB>.self)
            .doReactionAndClearChannel { component, model in
                if let model = model {
                    component.filter(withModel: model)
                }
        }
    }
    
    override func reactToDismiss() {
        
        _ = reactTo(channel: GPSHelpPageReaction.channelDismissGpsHelpScreen)?
            .then()
            .inside(componentID: ShoppingMapPageReaction.ROUTER_TAG, component: ShoppingMapPresenterB<ShoppingMapViewControllerB>.self)
            .doReactionAndClearChannel(reactionExecution: { presenter, _ in
                
                presenter.dismissedGPSHelpScreen()
            })
    }

}
