//
//  HelpDetailPresenter.swift
//  shoppingapp
//
//  Created by Marcos on 13/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class HelpDetailPresenter<T: HelpDetailViewProtocol>: BasePresenter<T> {

    var helpId: String?
    var fileProvider = FileProvider()

    override init(busManager: BusManager) {
        super.init(busManager: busManager)
        routerTag = HelpDetailPageReaction.ROUTER_TAG
    }

    override func setModel(model: Model) {

        if let helpDetailTransport = model as? HelpDetailTransport {
            helpId = helpDetailTransport.helpId
        }
    }

    required init() {
        super.init(tag: HelpDetailPageReaction.ROUTER_TAG)
    }

}

extension HelpDetailPresenter: HelpDetailPresenterProtocol {

    func getHelpDetail() {

        if let resource = helpId, !resource.isEmpty {

            let contentsOfFile = fileProvider.readResourceFromBundleAsString(forResource: resource, ofType: "json")

            if !contentsOfFile.isEmpty {

                if let helpDetailEntity = HelpDetailEntity.deserialize(from: contentsOfFile) {

                    let helpDetailBO = HelpDetailBO(helpDetailEntity: helpDetailEntity)

                    let helpDetailDisplayData = HelpDetailDisplayData(helpDetailBO)
                    view?.showHelpDetail(withDisplayData: helpDetailDisplayData)
                } else {
                    view?.showEmptyView()
                }
            } else {
                view?.showEmptyView()
            }
        } else {
            view?.showEmptyView()
        }
    }

    func navigateToWebPageScreen(withURL URL: URL) {
        
        let webTransport = WebTransport(webPage: URL.absoluteString, webTitle: URL.domain)
        busManager.navigateScreen(tag: routerTag, HelpDetailPageReaction.EVENT_NAV_TO_WEBPAGE_SCREEN, webTransport)
    }
}
