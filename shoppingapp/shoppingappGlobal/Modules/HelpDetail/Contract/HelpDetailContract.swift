//
//  HelpDetailContract.swift
//  shoppingapp
//
//  Created by Marcos on 13/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol HelpDetailViewProtocol: ViewProtocol {

    func showHelpDetail(withDisplayData displayData: HelpDetailDisplayData)
    func showEmptyView()

}

protocol HelpDetailPresenterProtocol: PresenterProtocol {

    func getHelpDetail()
    func navigateToWebPageScreen(withURL URL: URL)
}

protocol HelpDetailInteractorProtocol {
}
