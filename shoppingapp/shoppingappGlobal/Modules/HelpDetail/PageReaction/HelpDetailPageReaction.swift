//
//  HelpDetailPageReaction.swift
//  shoppingapp
//
//  Created by Marcos on 13/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

class HelpDetailPageReaction: PageReaction {

    static let ROUTER_TAG: String = "help_detail-tag"
    static let EVENT_NAV_TO_WEBPAGE_SCREEN: ActionSpec<Model> = ActionSpec<Model>(id: "EVENT_NAV_TO_WEBPAGE_SCREEN")

    override func configure() {

        navigateToWebPageScreen()
    }
    
    func navigateToWebPageScreen() {
        
        _ = when(id: HelpDetailPageReaction.ROUTER_TAG, type: Model.self)
            .doAction(actionSpec: HelpDetailPageReaction.EVENT_NAV_TO_WEBPAGE_SCREEN)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, model in
                
                router.navigateToShoppingWebPage(withModel: model!)
        }
    }
}
