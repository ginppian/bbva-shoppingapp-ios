//
//  HelpDetailViewController.swift
//  shoppingapp
//
//  Created by Marcos on 13/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit
import AVKit

class HelpDetailViewController: BaseViewController {

    // MARK: Static Constants

    static let ID: String = String(describing: HelpDetailViewController.self)

    fileprivate static let defaultImageViewCell: CGFloat = 200
    fileprivate static let defaultImageViewMovieCell: CGFloat = 170

    // MARK: Outlets

    @IBOutlet weak var helpTableView: UITableView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var headerImageView: UIImageView!
    @IBOutlet weak var headerViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var lottiePlayView: UIView!
    @IBOutlet weak var headerPlayButton: UIButton!

    // No Content
    
    @IBOutlet weak var noContentView: ErrorScreenView!
    
    // MARK: Public Vars
    
    var presenter: HelpDetailPresenterProtocol!
    
    // MARK: Private Vars

    fileprivate var displayData: HelpDetailDisplayData? {
        didSet {
            helpTableView.reloadData()
        }
    }
    
    fileprivate var movieHeaderURL: URL?
    fileprivate var expandedCellsIndex = [Int]()
    fileprivate var imageForIndex = [IndexPath: UIImage]()
    fileprivate var imageViewCellWith: CGFloat?
    fileprivate var previewMovieViewCellWith: CGFloat?
    fileprivate var lottiePlay: LOTAnimationView?
    
    // MARK: Private Constants
    
    fileprivate let defaultHeaderHeight: CGFloat = 266
    fileprivate let marginBounceHeader: CGFloat = 50
    
    fileprivate let playAction: (URL) -> Void = { movieHeaderURL in
        
        let player = AVPlayer(url: movieHeaderURL)
        let vc = AVPlayerViewController()
        vc.player = player
        
        BusManager.sessionDataInstance.present(viewController: vc, animated: true, completion: {
            vc.player?.play()
        })
    }
    
    // MARK: Initialization
    
    override func viewDidLoad() {
        
        configure(appLifecycle: (UIApplication.shared.delegate as! AppDelegate).appLifeCycle!, id: HelpDetailViewController.ID)
        
        super.viewDidLoad()

        configureView()
        configureNoContentView()

        presenter.getHelpDetail()
        NotificationCenter.default.addObserver(self, selector: #selector(applicationWillEnterForeground), name: .UIApplicationWillEnterForeground, object: nil)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        showPlayButton(true)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        
        super.viewDidDisappear(animated)
    }
    
    @objc func applicationWillEnterForeground() {
        
        lottiePlay?.play()
    }
    
    override func initModel(withModel model: Model) {
        
        presenter.setModel(model: model)
    }

    // MARK: Configuration

    private func configureView () {

        setNavigationBarDefault()
        
        let image = SVGCache.image(forSVGNamed: ConstantsImages.Common.close_icon, color: .BBVAWHITE)
        setNavigationLeftButton(withImage: image, action: #selector(dismissAction))
        
        headerImageView.backgroundColor = .BBVA300
        
        helpTableView.register(UINib(nibName: HelpSectionTableViewHeader.identifier, bundle: nil),
                           forHeaderFooterViewReuseIdentifier: HelpSectionTableViewHeader.identifier)

        helpTableView.register(UINib(nibName: HelpTextTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: HelpTextTableViewCell.identifier)
        helpTableView.register(UINib(nibName: HelpImageTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: HelpImageTableViewCell.identifier)
        helpTableView.register(UINib(nibName: HelpMovieTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: HelpMovieTableViewCell.identifier)
        
        helpTableView.backgroundColor = .BBVAWHITE
        helpTableView.rowHeight = UITableViewAutomaticDimension
        helpTableView.estimatedRowHeight = 100
        
        helpTableView.contentInset = UIEdgeInsets(top: defaultHeaderHeight, left: 0, bottom: 0, right: 0)
        
        headerView.clipsToBounds = true
        if #available(iOS 11.0, *) {
            helpTableView.contentInsetAdjustmentBehavior = .never
        } else {
            self.automaticallyAdjustsScrollViewInsets = false
        }
    }

    private func configureNoContentView() {

        let displayData = ErrorScreenDisplayData(title: Localizables.common.key_error_ups_text, message: "", hasRetryButton: false)
        noContentView.configureNoContent(displayData: displayData)
    }
    
    private func showPlayButton(_ show: Bool) {
        
        if movieHeaderURL != nil && show {
            lottiePlay?.play()
            lottiePlayView.isHidden = false
        } else {
            lottiePlay?.pause()
            lottiePlayView.isHidden = true
        }
    }
    
    @IBAction func playButtonPressed(_ sender: Any) {
        
        guard let movieHeaderURL = movieHeaderURL else {
            return
        }

        playAction(movieHeaderURL)
    }
}

extension HelpDetailViewController: UIScrollViewDelegate {

    fileprivate func resizeHeader(_ scrollView: UIScrollView) {

        let yOffset = defaultHeaderHeight - ( scrollView.contentOffset.y + defaultHeaderHeight)
        let height = max(0, min(yOffset, defaultHeaderHeight + marginBounceHeader))
        headerViewHeightConstraint.constant = height
    }

    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        resizeHeader(scrollView)
    }
}

extension HelpDetailViewController: HelpDetailViewProtocol {

    func showError(error: ModelBO) {
    }

    func changeColorEditTexts() {
    }

    func showHelpDetail(withDisplayData displayData: HelpDetailDisplayData) {
        
        self.displayData = displayData
        if let titleHeader = displayData.titleHeader {
            setNavigationTitle(withText: titleHeader)
        }
        
        if let imageHeader = displayData.imageHeaderURL {
            let url = URL(string: imageHeader)
            if let url = url {
                UIImage.load(fromUrl: url, completion: { image in
                    DispatchQueue.main.async {
                        if let image = image {
                            self.headerImageView.image = image
                        }
                    }
                })
            }
        } else {
            let imageDefault = UIImage(named: ConstantsImages.Help.imageThumbDefault)
            self.headerImageView.image = imageDefault
        }
        
        if let movieHeaderURLName = displayData.videoHeaderURL {
            movieHeaderURL = URL(string: movieHeaderURLName)
            
            let lottieAnimationView = LOTAnimationView(name: ConstantsLottieAnimations.play)
            lottieAnimationView.frame = lottiePlayView.bounds
            lottieAnimationView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            lottieAnimationView.contentMode = .scaleAspectFit
            lottieAnimationView.loopAnimation = true
            lottiePlayView.addSubview(lottieAnimationView)
            lottiePlayView.bringSubview(toFront: headerPlayButton)
            showPlayButton(false)
            lottiePlay = lottieAnimationView
        }
    }

    func showEmptyView() {
        
        noContentView.isHidden = false

        helpTableView.isHidden = true
        headerView.isHidden = true
    }
}

extension HelpDetailViewController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        
        guard let displayData = displayData else {
            return 0
        }

        return displayData.questions.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard let displayData = displayData, displayData.questions.count > section, expandedCellsIndex.contains(section) else {
            return 0
        }

        let question = displayData.questions[section]

        return question.answers.count
    }

    fileprivate func loadImage(atUrl url: URL, _ indexPath: IndexPath) {
        
        UIImage.load(fromUrl: url, completion: { image in
            self.imageForIndex[indexPath] = image
            DispatchQueue.main.async {
                self.helpTableView.reloadRows(at: [indexPath], with: UITableViewRowAnimation.none)
            }
        })
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        guard let displayData = displayData else {
            return UITableViewCell()
        }

        let question = displayData.questions[indexPath.section]
        let answer = question.answers[indexPath.row]

        switch answer.type {
        case .text:
            let cell = tableView.dequeueReusableCell(withIdentifier: HelpTextTableViewCell.identifier) as? HelpTextTableViewCell
            if let cell = cell {
                cell.setupCell(withAnswer: answer)
                cell.delegate = self
                return cell
            }
            return UITableViewCell()

        case .image:
            let cell = tableView.dequeueReusableCell(withIdentifier: HelpImageTableViewCell.identifier) as? HelpImageTableViewCell
            if let cell = cell, let urlString = answer.data {
                imageViewCellWith = cell.helpImageView.bounds.width
                let image = imageForIndex[indexPath]
                if let image = image {
                    cell.setupCell(withImage: image)
                } else {
                    if let url = URL(string: urlString) {
                        loadImage(atUrl: url, indexPath)
                    }
                }
                return cell
            }
            return UITableViewCell()

        case .video:
            let cell = tableView.dequeueReusableCell(withIdentifier: HelpMovieTableViewCell.identifier) as? HelpMovieTableViewCell
            if let cell = cell, let urlString = answer.extraData, let urlMoviewString = answer.data {
                previewMovieViewCellWith = cell.previewMovieImageView.bounds.width
                let image = imageForIndex[indexPath]
                if let image = image {
                    cell.setupCell(withImage: image, urlMovie: URL(string: urlMoviewString), playAction: playAction )
                } else {
                    if let url = URL(string: urlString) {
                        loadImage(atUrl: url, indexPath)
                    }
                }
                return cell
            }
            return UITableViewCell()

        default:
            return UITableViewCell()
        }
    }
}

extension HelpDetailViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        guard let displayData = displayData,
            displayData.questions.count > section,
            let headerCell = tableView.dequeueReusableHeaderFooterView(withIdentifier: HelpSectionTableViewHeader.identifier) as? HelpSectionTableViewHeader else { return nil }

        headerCell.delegate = self

        let question = displayData.questions[section]

        if expandedCellsIndex.contains(section) {
            headerCell.configure(question: question, expanded: true, sectionIndex: section)
        } else {
            headerCell.configure(question: question, expanded: false, sectionIndex: section)
        }

        return headerCell
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        return HelpSectionTableViewHeader.height
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        guard let displayData = displayData else {
            return UITableViewAutomaticDimension
        }

        let question = displayData.questions[indexPath.section]
        let answer = question.answers[indexPath.row]

        if answer.type == .image {
            return HelpDetailViewController.defaultImageViewCell
        }

        if answer.type == .video {
            return HelpDetailViewController.defaultImageViewMovieCell
        }
        return UITableViewAutomaticDimension
    }

}

extension HelpDetailViewController: HelpSectionTableViewHeaderDelegate {
    
    func sectionTouched(sectionHeaderView: HelpSectionTableViewHeader) {
        
        guard let sectionIndex = sectionHeaderView.sectionIndex else {
            return
        }

        if let index = expandedCellsIndex.index(of: sectionIndex) {
            expandedCellsIndex.remove(at: index)
        } else {
            expandedCellsIndex.append(sectionIndex)
        }

        helpTableView.reloadData()
    }
}

extension HelpDetailViewController: HelpTextTableViewCellDelegate {
    
    func helpTextTableViewCellPressedLink(_ helpTextTableViewCell: HelpTextTableViewCell, _ URL: URL) {
        
        presenter.navigateToWebPageScreen(withURL: URL)
    }
}
