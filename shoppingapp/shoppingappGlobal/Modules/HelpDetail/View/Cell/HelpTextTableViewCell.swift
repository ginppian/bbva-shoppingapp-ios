//
//  HelpTextTableViewCell.swift
//  shoppingapp
//
//  Created by Marcos on 16/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit

protocol HelpTextTableViewCellDelegate: class {
    
    func helpTextTableViewCellPressedLink(_ helpTextTableViewCell: HelpTextTableViewCell, _ URL: URL)
}

class HelpTextTableViewCell: UITableViewCell {
    
    @IBOutlet weak var helpTextView: HelpCustomTextView!
    
    static let identifier =  "HelpTextTableViewCell"
    
    weak var delegate: HelpTextTableViewCellDelegate?
    
    override func awakeFromNib() {
        // Initialization code
        super.awakeFromNib()
        configureCell()
    }
    
    func configureCell() {
        
        helpTextView.font = Tools.setFontBook(size: 14)
        helpTextView.textColor = .BBVA500
        helpTextView.tintColor = .MEDIUMBLUE
        helpTextView.isScrollEnabled = false
        helpTextView.isEditable = false
        helpTextView.isSelectable = true
        helpTextView.textContainerInset = .zero
        helpTextView.contentInset = UIEdgeInsets(top: 0, left: -5, bottom: 0, right: 0)
        helpTextView.delegate = self
    }
    
    func setupCell(withAnswer answer: AnswerDisplayData) {
        
        helpTextView.text = answer.data
        helpTextView.attributedText = answer.data?.htmlAttributed(withFont: Tools.setFontBook(size: 14), andColor: .BBVA500)
    }
}

extension HelpTextTableViewCell: UITextViewDelegate {
    
    func textView(_ textView: UITextView, shouldInteractWith URL: URL, in characterRange: NSRange, interaction: UITextItemInteraction) -> Bool {
        
        delegate?.helpTextTableViewCellPressedLink(self, URL)
        
        return false
    }
}

class HelpCustomTextView: UITextView {
    
    override func canPerformAction(_ action: Selector, withSender sender: Any?) -> Bool {
        
        return false
    }
    
    override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        
        if gestureRecognizer is UILongPressGestureRecognizer {
            return false
        } else if gestureRecognizer is UITapGestureRecognizer, let tapGesture = gestureRecognizer as? UITapGestureRecognizer, tapGesture.numberOfTapsRequired > 1 {
            return false
        } else {
            return super.gestureRecognizerShouldBegin(gestureRecognizer)
        }
    }
}
