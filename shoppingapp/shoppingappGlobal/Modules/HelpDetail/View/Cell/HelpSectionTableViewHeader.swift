//
//  HelpSectionTableViewHeader.swift
//  shoppingapp
//
//  Created by Marcos on 16/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit

protocol HelpSectionTableViewHeaderDelegate: class {
    func sectionTouched(sectionHeaderView: HelpSectionTableViewHeader)
}

class HelpSectionTableViewHeader: UITableViewHeaderFooterView {

    // MARK: Outlets

    @IBOutlet weak var questionLabel: UILabel!
    @IBOutlet weak var expandIconImageView: UIImageView!
    @IBOutlet weak var divisorLine: UIView!

    // MARK: Vars

    weak var delegate: HelpSectionTableViewHeaderDelegate?
    var sectionIndex: Int?

    // MARK: Constants
    static let identifier =  "HelpSectionTableViewHeader"

    static let height: CGFloat = 81.0

    override func awakeFromNib() {
        super.awakeFromNib()
        configureCell()
    }

    private func configureCell() {
        questionLabel.font = Tools.setFontBook(size: 16)
        questionLabel.textColor = .BBVA600
        questionLabel.textAlignment = .left

        divisorLine.backgroundColor = .BBVA300

        addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(tapInSection(gestureRecognizer:))))
    }

    @objc func tapInSection(gestureRecognizer: UITapGestureRecognizer) {
        delegate?.sectionTouched(sectionHeaderView: self)
    }

    func configure(question: QuestionDisplayData, expanded isExpanded: Bool, sectionIndex: Int) {

        self.sectionIndex = sectionIndex

        if let titleSection = question.title {
            questionLabel.text = titleSection
        } else {
            questionLabel.text = ""
        }

        if isExpanded {
            questionLabel.textColor = .MEDIUMBLUE
            expandIconImageView.image = SVGCache.image(forSVGNamed: ConstantsImages.Help.contract_icon, color: .MEDIUMBLUE)
            questionLabel.numberOfLines = 0
        } else {
            questionLabel.textColor = .BBVA600
            expandIconImageView.image = SVGCache.image(forSVGNamed: ConstantsImages.Common.expand_icon, color: .MEDIUMBLUE)
            questionLabel.numberOfLines = 2

        }
    }

}
