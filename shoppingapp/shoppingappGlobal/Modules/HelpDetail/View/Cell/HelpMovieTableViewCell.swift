//
//  HelpMovieTableViewCell.swift
//  shoppingapp
//
//  Created by Marcos on 16/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit

class HelpMovieTableViewCell: UITableViewCell {

    @IBOutlet weak var previewMovieImageView: UIImageView!
    @IBOutlet weak var playButton: UIButton!

    // MARK: Constants
    static let identifier =  "HelpMovieTableViewCell"

    // MARK: Private Vars

    private var movieURL: URL?
    private var playAction: ((URL) -> Void)?

    override func awakeFromNib() {
        // Initialization code
        super.awakeFromNib()
        configureCell()
    }

    func configureCell() {
        playButton.setImage(SVGCache.image(forSVGNamed: ConstantsImages.Common.play_icon, color: .BBVAWHITE), for: .normal)
    }

    func setupCell(withImage image: UIImage, urlMovie: URL?, playAction: @escaping (URL) -> Void) {
        movieURL = urlMovie
        if movieURL != nil {
            playButton.isHidden = false
        } else {
            playButton.isHidden = true
        }
        self.playAction = playAction
        previewMovieImageView.image = image
    }

    @IBAction func playButtonPressed(_ sender: Any) {
        guard let movieURL = movieURL else {
            return
        }
        playAction!(movieURL)
    }

}
