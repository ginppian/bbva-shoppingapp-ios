//
//  HelpImageTableViewCell.swift
//  shoppingapp
//
//  Created by Marcos on 16/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit

class HelpImageTableViewCell: UITableViewCell {

    @IBOutlet weak var helpImageView: UIImageView!

    // MARK: Constants
    static let identifier =  "HelpImageTableViewCell"

    func setupCell(withImage image: UIImage) {
        helpImageView.image = image
    }

}
