//
//  HelpDetailDisplayData.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 16/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

struct HelpDetailDisplayData {

    static var language = ConstantsLanguage.DEFAULT_LANGUAGE
    var titleHeader: String?
    var imageHeaderURL: String?
    var videoHeaderURL: String?
    var questions = [QuestionDisplayData]()

    init(_ helpDetailBO: HelpDetailBO) {

        if let titleContentBO = helpDetailBO.getTitleByLanguage(forKey: HelpDetailDisplayData.language), let titleContent = titleContentBO.content, !titleContent.isEmpty {

            titleHeader = titleContent
        }

        if let imageContentBO = helpDetailBO.getImageByLanguage(forKey: HelpDetailDisplayData.language), let imageContent = imageContentBO.content, !imageContent.isEmpty {

            imageHeaderURL = helpDetailBO.getImageURL(forUrl: imageContent)
        }

        if let videoContentBO = helpDetailBO.getVideoByLanguage(forKey: HelpDetailDisplayData.language), let videoContent = videoContentBO.content, !videoContent.isEmpty {

            videoHeaderURL = videoContent
        }

        if let questionsBO = helpDetailBO.questions {

            for question in questionsBO {

                questions.append(QuestionDisplayData(question))
            }
        }

    }
}

struct QuestionDisplayData {

    var title: String?
    var answers = [AnswerDisplayData]()

    init(_ questionBO: QuestionBO) {

        if let titleContentBO = questionBO.getTitleByLanguage(forKey: HelpDetailDisplayData.language), let titleContent = titleContentBO.content, !titleContent.isEmpty {

            title = titleContent
        }

        if let answersBO = questionBO.answers {

            for answer in answersBO {

                answers.append(AnswerDisplayData(answer))
            }
        }

    }
}

struct AnswerDisplayData {

    var type: AnswerType
    var data: String?
    var extraData: String?

    init(_ answerBO: AnswerBO) {

        type = answerBO.type
        if let dataContentBO = answerBO.getDataByLanguage(forKey: HelpDetailDisplayData.language), let dataContent = dataContentBO.content, !dataContent.isEmpty {

            data = dataContent

            if type == .image {
                data = dataContentBO.getImageURL(forUrl: dataContent)
            }
        }

        if let extraDataContentBO = answerBO.getExtraDataByLanguage(forKey: HelpDetailDisplayData.language), let extraDataContent = extraDataContentBO.content, !extraDataContent.isEmpty {

            extraData = extraDataContentBO.getImageURL(forUrl: extraDataContent)
        }
    }
}
