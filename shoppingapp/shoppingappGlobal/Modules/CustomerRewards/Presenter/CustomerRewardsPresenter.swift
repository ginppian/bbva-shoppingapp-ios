//
//  CustomerRewardsPresenter.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 23/7/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

final class CustomerRewardsPresenter <T: CustomerRewardsViewProtocol>: BasePresenter<T> {

    required init() {
        super.init(tag: CustomerRewardsPageReaction.ROUTER_TAG)
    }

    override init(busManager: BusManager) {
        super.init(busManager: busManager)
        routerTag = CustomerRewardsPageReaction.ROUTER_TAG
    }
}

extension CustomerRewardsPresenter: CustomerRewardsPresenterProtocol {

    func pointsBalanceFailed(error: ErrorBO) {

        view?.showErrorView()
        errorBO = error
        checkError()
    }

    func customerPointsShowError() {

        let error = ErrorBO(message: Localizables.points.key_points_info_title_error_text, errorType: .warning, allowCancel: false)
        errorBO = error
        view?.showError(error: error)
    }
}
