//
//  CustomerRewardsViewController.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 23/7/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit
import CellsNativeCore
import CellsNativeComponents

final class CustomerRewardsViewController: BaseViewController {

    // MARK: Static Constants

    static let ID: String = String(describing: CustomerRewardsViewController.self)
    static let CUSTOMER_POINTS_COMPONENT: String = "customerPoints"

    // MARK: IBOutlets

    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var pointsContainerStackView: UIStackView!
    @IBOutlet weak var contentCustomerPointsView: UIView!
    @IBOutlet weak var customerPointsHeightConstraint: NSLayoutConstraint!

    @IBOutlet weak var errorScreenView: ErrorScreenView! {
        didSet {
            errorScreenView.delegate = self
        }
    }

    // MARK: Vars

    var presenter: CustomerRewardsPresenterProtocol!

    var customerPointsView: CustomerPointsView?
    var pointsBalanceStore: PointsBalanceStore?

    override func viewDidLoad() {

        customerPointsView = UIView.fromNib(superView: contentCustomerPointsView, tagName: CustomerRewardsViewController.CUSTOMER_POINTS_COMPONENT) as CustomerPointsView
        customerPointsView?.configureView(viewController: self)

        configure(appLifecycle: (UIApplication.shared.delegate as! AppDelegate).appLifeCycle!, id: CustomerRewardsViewController.ID)
        if let networkWorker = BusManager.sessionDataInstance.networkWorker {
            pointsBalanceStore = PointsBalanceStore(worker: networkWorker, host: ServerHostURL.getLoyaltyURL())
        }

        super.viewDidLoad()

        configureView()

        requestGetPointsBalance()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        setNavigationBarWithColor(withColor: .GREEN132)
        addNavigationBarMenuButton()
        setTitleNavigationBar(withTitle: Localizables.points.key_points_text)
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        if let height = customerPointsView?.preferredContentHeight {
            customerPointsHeightConstraint.constant = height
        }
    }

    override func initModel(withModel model: Model) {
        presenter.setModel(model: model)
    }

    deinit {
        ComponentManager.remove(id: presenter.routerTag)
        DLog(message: "Deinitialized CustomerRewardsViewController")
    }
}

// MARK: - Private methods -
private extension CustomerRewardsViewController {

    func setTitleNavigationBar(withTitle title: String) {

        setNavigationTitle(withText: title)
        tabBarItem.title = Localizables.common.key_tabbar_points_button
    }

    func configureView() {

        view.backgroundColor = .GREEN132
        backView.backgroundColor = .GREEN132
        scrollView.backgroundColor = .clear
        errorScreenView.isHidden = true

    }

    func requestGetPointsBalance() {

        errorScreenView.isHidden = true
        customerPointsView?.isHidden = false
        customerPointsView?.viewModel?.isLoading = true
        pointsBalanceStore?.getPointsBalance()
    }

    private func showErrorScreenView() {

        errorScreenView.isHidden = false

        let errorMessage = Localizables.common.key_common_error_nodata_text
        let displayData = ErrorScreenDisplayData(title: Localizables.common.key_error_ups_text, message: errorMessage, hasRetryButton: true)
        errorScreenView.configureError(displayData: displayData)
    }

    private func formattingAmount(amount: NSNumber, currency: String) -> String {

        var amountFormatter = AmountFormatter(codeCurrency: SessionDataManager.sessionDataInstance().mainCurrency)

        if !currency.isEmpty {
            amountFormatter = AmountFormatter(codeCurrency: currency)
        }

        return amountFormatter.format(amount: NSDecimalNumber(decimal: amount.decimalValue), withSpace: true)
    }
}

// MARK: - Page Reactions -
extension CustomerRewardsViewController {

    func pointsBalanceSucceed(pointsBalance: PointsBalanceDTO) {

        let accumulatedPointsAmount = pointsBalance.data?.accumulatedPoints?.amount
        var exchangeUnit: String?
        let pointsToBeExpiredAmount = pointsBalance.data?.pointsToBeExpired?.amount
        var expirationDate: String?
        let areRedeemable = pointsBalance.data?.areRedeemable

        if let amount = pointsBalance.data?.accumulatedPoints?.exchangeUnit?.amount, let currency = pointsBalance.data?.accumulatedPoints?.exchangeUnit?.currency {

            exchangeUnit = formattingAmount(amount: amount, currency: currency)
        }

        if let dateString = pointsBalance.data?.pointsToBeExpired?.expirationDate, let date = Date.date(fromServerString: dateString, withFormat: Date.DATE_FORMAT_SERVER) {

            expirationDate = Date.string(fromDate: date, withFormat: Date.DATE_FORMAT_DAY_MONTH_COMPLETE_YEAR_COMPACT)
        }

        let customerPointsModel = CustomerPointsModel(accumulatedPointsAmount: accumulatedPointsAmount, exchangeUnit: exchangeUnit, pointsToBeExpiredAmount: pointsToBeExpiredAmount, expirationDate: expirationDate, areRedeemable: areRedeemable)

        customerPointsView?.viewModel?.customerPointsModel = customerPointsModel
    }

    func pointsBalanceFailed(error: ErrorBO) {

        customerPointsView?.isHidden = true
        presenter.pointsBalanceFailed(error: error)
    }

    func customerPointsShowError() {

        presenter.customerPointsShowError()
    }

    func customerPointsRedeemButtonPressed() {

    }

    func customerPointsInfoButtonPressed() {

    }
}

// MARK: - CustomerRewardsViewProtocol -
extension CustomerRewardsViewController: CustomerRewardsViewProtocol {

    func showError(error: ModelBO) {
        presenter.showModal()
    }

    func changeColorEditTexts() {
    }

    func showErrorView() {
        showErrorScreenView()
    }
}

// MARK: - ErrorScreenViewDelegate -
extension CustomerRewardsViewController: ErrorScreenViewDelegate {

    func errorScreenViewRetryButtonPressed(_ errorScreenView: ErrorScreenView) {

        requestGetPointsBalance()
    }
}

// MARK: - CloseModalDelegate
extension CustomerRewardsViewController: CloseModalDelegate {

    func errorViewControllerAcceptPressed(_ errorViewController: ErrorViewController) {
        presenter.checkError()
    }

    func errorViewControllerCancelPressed(_ errorViewController: ErrorViewController) {
    }

}
