//
//  CustomerRewardsPageReaction.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 23/7/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

class CustomerRewardsPageReaction: PageReaction {

    static let ROUTER_TAG: String = "customer-rewards-tag"

    override func configure() {

        pointsBalanceSucceed()
        pointsBalanceFailed()
        customerPointsShowError()
        customerPointsRedeemButtonPressed()
        customerPointsInfoButtonPressed()
    }

    func pointsBalanceSucceed() {

        _ = when(id: PointsBalanceStore.ID, type: PointsBalanceDTO.self)
            .doAction(actionSpec: PointsBalanceStore.ON_GET_POINTS_BALANCE_SUCCESS)
            .then()
            .inside(componentID: CustomerRewardsViewController.ID, component: CustomerRewardsViewController.self)
            .doReaction { component, pointsBalance in

                if let pointsBalance = pointsBalance {
                    component.pointsBalanceSucceed(pointsBalance: pointsBalance)
                } else {
                    component.pointsBalanceFailed(error: ErrorBO(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
                }
            }
    }

    func pointsBalanceFailed() {

        _ = when(id: PointsBalanceStore.ID, type: ErrorBO.self)
            .doAction(actionSpec: PointsBalanceStore.ON_GET_POINTS_BALANCE_ERROR)
            .then()
            .inside(componentID: CustomerRewardsViewController.ID, component: CustomerRewardsViewController.self)
            .doReaction { component, error in

                if let error = error {
                    component.pointsBalanceFailed(error: error)
                } else {
                    component.pointsBalanceFailed(error: ErrorBO(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
                }
            }
    }

    func customerPointsShowError() {

        _ = when(id: CustomerRewardsViewController.CUSTOMER_POINTS_COMPONENT, type: Void.self)
            .doAction(actionSpec: CustomerPointsViewModel.ACTION_SHOW_ERROR)
            .then()
            .inside(componentID: CustomerRewardsViewController.ID, component: CustomerRewardsViewController.self)
            .doReaction(reactionExecution: { component, _ in

                component.customerPointsShowError()
            })
    }

    func customerPointsRedeemButtonPressed() {

        _ = when(id: CustomerRewardsViewController.CUSTOMER_POINTS_COMPONENT, type: Void.self)
            .doAction(actionSpec: CustomerPointsViewModel.ACTION_REDEEM_BUTTON_PRESSED)
            .then()
            .inside(componentID: CustomerRewardsViewController.ID, component: CustomerRewardsViewController.self)
            .doReaction(reactionExecution: { component, _ in

                component.customerPointsRedeemButtonPressed()
            })
    }

    func customerPointsInfoButtonPressed() {

        _ = when(id: CustomerRewardsViewController.CUSTOMER_POINTS_COMPONENT, type: Void.self)
            .doAction(actionSpec: CustomerPointsViewModel.ACTION_INFO_BUTTON_PRESSED)
            .then()
            .inside(componentID: CustomerRewardsViewController.ID, component: CustomerRewardsViewController.self)
            .doReaction(reactionExecution: { component, _ in

                component.customerPointsInfoButtonPressed()
            })
    }
}
