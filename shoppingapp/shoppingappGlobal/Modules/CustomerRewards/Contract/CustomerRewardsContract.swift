//
//  CustomerRewardsContract.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 23/7/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol CustomerRewardsViewProtocol: ViewProtocol {

    func showErrorView()
}

protocol CustomerRewardsPresenterProtocol: PresenterProtocol {

    func pointsBalanceFailed(error: ErrorBO)
    func customerPointsShowError()
}
