//
//  HelpPageReaction.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 24/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

class HelpPageReaction: PageReaction {

    static let ROUTER_TAG: String = "help-tag"

    static let EVENT_NAV_TO_DETAIL_HELP: ActionSpec<Model> = ActionSpec<Model>(id: "EVENT_NAV_TO_DETAIL_HELP")

    override func configure() {

        registerForNavigateToHelpDetail()
    }

    func registerForNavigateToHelpDetail() {

        _ = when(id: HelpPageReaction.ROUTER_TAG, type: Model.self)
            .doAction(actionSpec: HelpPageReaction.EVENT_NAV_TO_DETAIL_HELP)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, model in

                router.navigateToDetailHelp(withModel: model!)
            }
    }
}
