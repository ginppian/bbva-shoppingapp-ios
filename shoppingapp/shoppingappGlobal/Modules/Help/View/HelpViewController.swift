//
//  HelpViewController.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 24/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

class HelpViewController: BaseViewController {

    // MARK: Static Constants

    static let ID: String = String(describing: HelpViewController.self)

    static let tableFooterViewHeight: CGFloat = 10.0

    // MARK: Outlets

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var helpTableView: UITableView!

    // MARK: Public Vars

    var presenter: HelpPresenterProtocol!

    var displayDataItems = [HelpDisplayData]()

    // MARK: Initialization

    override func viewDidLoad() {

        configure(appLifecycle: (UIApplication.shared.delegate as! AppDelegate).appLifeCycle!, id: HelpViewController.ID)
        super.viewDidLoad()

        configureView()
        configureFooter()

        presenter.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        configureNavigationBar()

        BusManager.sessionDataInstance.disableSwipeLateralMenu()
    }

    deinit {
        ComponentManager.remove(id: presenter.routerTag)
        DLog(message: "Deinitialized HelpViewController")
    }

    override func initModel(withModel model: Model) {

    }

    // MARK: Configuration

    func configureNavigationBar() {

        setNavigationBarDefault()
        setNavigationBackButtonDefault()

        setNavigationTitle(withText: Localizables.common.key_help_menu_text)
    }

    func configureView() {

        edgesForExtendedLayout = .top

        if #available(iOS 11.0, *) {
            helpTableView.contentInsetAdjustmentBehavior = .never
        } else {
            automaticallyAdjustsScrollViewInsets = false
        }

        contentView.backgroundColor = .BBVA100

        helpTableView.dataSource = self

        helpTableView.backgroundColor = .BBVA100

        helpTableView.register(UINib(nibName: HelpTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: HelpTableViewCell.identifier)

        helpTableView.rowHeight = UITableViewAutomaticDimension
        helpTableView.estimatedRowHeight = HelpTableViewCell.height
        helpTableView.separatorStyle = .none
    }

    func configureFooter() {

        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: helpTableView.frame.size.width, height: HelpViewController.tableFooterViewHeight))
        footerView.backgroundColor = .BBVA100
        helpTableView.tableFooterView = footerView
    }
}

extension HelpViewController: HelpViewProtocol {

    func showError(error: ModelBO) {
    }

    func changeColorEditTexts() {
    }

    func showHelp(withDisplayDataItems items: [HelpDisplayData]) {

        displayDataItems = items
        helpTableView.reloadData()
    }
}

extension HelpViewController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        return displayDataItems.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let displayItem = displayDataItems[indexPath.row]

        if let cell = tableView.dequeueReusableCell(withIdentifier: HelpTableViewCell.identifier, for: indexPath) as? HelpTableViewCell {

            cell.configureCellWithHelpDisplayData(withHelpDisplayData: displayItem)
            cell.helpView.delegate = self

            return cell
        }

        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return HelpTableViewCell.height
    }
}

extension HelpViewController: HelpViewDelegate {

    func helpButtonPressed(_ helpView: HelpView, andHelpId helpId: String) {

        presenter.helpButtonPressed(withHelpId: helpId)
    }
}
