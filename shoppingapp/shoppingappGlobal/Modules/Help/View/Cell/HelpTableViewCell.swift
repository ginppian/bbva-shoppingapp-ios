//
//  HelpTableViewCell.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 25/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class HelpTableViewCell: UITableViewCell {

    static let identifier = "HelpTableViewCell"

    static let height: CGFloat = 260.0

    @IBOutlet weak var helpView: HelpView!

    override func awakeFromNib() {
        super.awakeFromNib()

        selectionStyle = .none

        contentView.backgroundColor = .BBVA100

    }

    func configureCellWithHelpDisplayData(withHelpDisplayData helpDisplayData: HelpDisplayData) {

        helpView.setHelpDisplayData(forHelpDisplayData: helpDisplayData, withAnimation: false)
    }
    
    override func prepareForReuse() {
        
        super.prepareForReuse()
        helpView.helpImageView.image = nil
    }
}
