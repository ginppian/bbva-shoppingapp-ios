//
//  HelpPresenter.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 24/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class HelpPresenter<T: HelpViewProtocol>: BasePresenter<T> {

    override init(busManager: BusManager) {
        super.init(busManager: busManager)
        routerTag = HelpPageReaction.ROUTER_TAG
    }

    override func setModel(model: Model) {

    }

    required init() {
        super.init(tag: HelpPageReaction.ROUTER_TAG)
    }

}

extension HelpPresenter: HelpPresenterProtocol {

    func viewDidLoad() {

        var helpDisplayItems = [HelpDisplayData]()

        if let configsBO = ConfigPublicManager.sharedInstance().configs?.items {

            for configBO in configsBO {

                let helpDisplayItem = HelpDisplayData.generateHelpDisplayData(withConfigBO: configBO, isFromSection: false)
                helpDisplayItems.append(helpDisplayItem)
            }
        }

        view?.showHelp(withDisplayDataItems: helpDisplayItems)
    }

    func helpButtonPressed(withHelpId helpId: String) {
        busManager.navigateScreen(tag: HelpPageReaction.ROUTER_TAG, HelpPageReaction.EVENT_NAV_TO_DETAIL_HELP, HelpDetailTransport(helpId))
    }
}
