//
//  HelpContract.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 24/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol HelpViewProtocol: ViewProtocol {

    func showHelp(withDisplayDataItems items: [HelpDisplayData])

}

protocol HelpPresenterProtocol: PresenterProtocol {

    func viewDidLoad()
    func helpButtonPressed(withHelpId helpId: String)

}
