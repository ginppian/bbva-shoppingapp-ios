//
//  NotificationsPresenter.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 7/3/17.
//  Copyright © 2017 daniel.petrovic. All rights reserved.
//

import Foundation

class NotificationsPresenter<T: NotificationsViewProtocol>: BasePresenter<T>, NotificationsPresenterProtocol {

    override init(busManager: BusManager) {
        super.init(busManager: busManager)
    }

    required init() {
        super.init(tag: NotificationsPageReaction.ROUTER_TAG)
    }

}
