//
//  NotificationsViewController.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 8/3/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit
import CellsNativeComponents

class NotificationsViewController: BaseViewController {

    // MARK: Constants

    static let ID: String = String(describing: NotificationsViewController.self)

    var presenter: NotificationsPresenterProtocol!

    // MARK: outlets

    // MARK: life cycle

    override func viewDidLoad() {

        self.configure(appLifecycle: (UIApplication.shared.delegate as! AppDelegate).appLifeCycle!, id: NotificationsViewController.ID)
        super.viewDidLoad()

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        setNavigationBarTransparent()
        addNavigationBarLogo()
        addNavigationBarMenuButton()
    }

    deinit {
        ComponentManager.remove(id: presenter.routerTag)
        DLog(message: "Deinitialized NotificationsViewController")
    }

    // MARK: custom methods

}

extension NotificationsViewController: NotificationsViewProtocol {

    func showError(error: ModelBO) {

    }

    func changeColorEditTexts() {

    }

}
