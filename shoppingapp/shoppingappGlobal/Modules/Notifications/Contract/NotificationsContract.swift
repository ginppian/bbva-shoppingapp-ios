//
//  NotificationsContract.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 15/3/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit

protocol NotificationsViewProtocol: ViewProtocol {

}

protocol NotificationsPresenterProtocol: PresenterProtocol {

}
