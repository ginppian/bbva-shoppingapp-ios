//
//  ShoppingContract.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 15/3/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit
import RxSwift

protocol ShoppingViewProtocol: ViewProtocol {

    func showErrorEmptyTsec()
    func showNoContentView()
    func showSkeleton()
    func hideSkeleton()
    func showPromotions(withDisplayItemsPromotions displayItemPromotions: DisplayItemsPromotions)
    func showCategories(withDisplayModel displayModel: PromotionsCategoryDisplayModel)
    func showFilterButton()
    
    func trackScreen()
    func trackErrorScreen(withError error: String)
}

protocol ShoppingPresenterProtocol: PresenterProtocol {

    func viewWillAppear()
    func viewLoaded()
    func showTrendingPromotions()
    func itemSelected(withDisplayItem displayItem: DisplayItemPromotion?)
    func categorySelected(withCategory category: CategoryBO)
    func filterPressed()
    func retryButtonPressed()
}

protocol ShoppingInteractorProtocol {

    func provideCategories() -> Observable <ModelBO>
    func providePromotions(byParams params: ShoppingParamsTransport) -> Observable <ModelBO>
}
