//
//  ShoppingContractB.swift
//  shoppingappMX
//
//  Created by jesus.martinez on 4/6/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol ShoppingViewProtocolB: ShoppingViewProtocolA {

    func scrollToTop()
    func isVisibleOnTop() -> Bool
}

protocol ShoppingPresenterProtocolB: ShoppingPresenterProtocolA {

    func selectFavoriteCategoriesPressed()
    func promotionsWillBeShown()
    func checkCountAccessInNextApparition()
    func favoriteCategoriesNotChanged()
    func userScrollsToBottom()
    func toastViewDidDissapearAndViewIsVisible()
    func toastViewDidDissapearButViewIsNotVisible()
    func tabPressed()
}
