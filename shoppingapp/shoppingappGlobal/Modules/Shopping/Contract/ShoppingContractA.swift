//
//  ShoppingContractA.swift
//  shoppingapp
//
//  Created by jesus.martinez on 10/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol ShoppingViewProtocolA: ShoppingViewProtocol {

    func showInfoToastNotGPS()
    func showInfoToastNotPermissions()
    func showToastUpdatedPromotions()

    func openAppSettings()
}

protocol ShoppingPresenterProtocolA: ShoppingPresenterProtocol {

    func viewDidAppear()
    func viewDidDisappear()
    func toastPressed()
    func accessMapSelected()
}

protocol ShoppingInteractorProtocolA: ShoppingInteractorProtocol {

}
