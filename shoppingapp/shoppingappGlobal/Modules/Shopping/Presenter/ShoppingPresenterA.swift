//
//  ShoppingPresenterA.swift
//  shoppingapp
//
//  Created by jesus.martinez on 10/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class ShoppingPresenterA<T: ShoppingViewProtocolA>: ShoppingPresenter<T> {

    var defaultLocation = Settings.Global.default_location
    var userLocation: GeoLocationBO?
    var defaultDistance = Settings.PromotionsMap.default_distance
    var lengthType = Settings.PromotionsMap.default_length_type
    var locationManager: LocationManagerProtocol?
    var lastLocationErrorType: LocationErrorType?

    override func viewLoaded() {

        view?.showSkeleton()

        locationManager = locationManager ?? LocationManager()

        locationManager?.getLocation(onSuccess: { [weak self] location -> Void in
            
            if self?.userLocation == nil && self?.lastLocationErrorType != nil {
                self?.view?.showToastUpdatedPromotions()
            }

            self?.obtainedLocation(withLatitude: location.coordinate.latitude, andLongitude: location.coordinate.longitude)

            self?.lastLocationErrorType = nil

            }, onFail: { [weak self] locationErrorType -> Void in

                self?.lastLocationErrorType = locationErrorType
                self?.obtainLocationFail()

                switch locationErrorType {

                case .permissionDenied:

                    self?.view?.showInfoToastNotPermissions()

                case .gpsOff:

                    self?.view?.showInfoToastNotGPS()

                case .gpsFail:

                    DLog(message: "gpsFail")
                }
        })
    }

    override func shoppingParamsTransport(withCategory category: CategoryBO) -> ShoppingParamsTransport {

        let location = userLocation ?? defaultLocation

        let locationParam = ShoppingParamLocation(geolocation: location, distanceParam: ShoppingDistanceParam(distance: defaultDistance, lengthType: lengthType))

        return ShoppingParamsTransport(categoryId: category.id.rawValue, location: locationParam)
    }

    private func obtainedLocation(withLatitude latitude: Double, andLongitude longitude: Double) {

        if userLocation == nil {

            userLocation = GeoLocationBO(withLatitude: latitude, withLongitude: longitude)
            loadData()
        }
    }

    private func obtainLocationFail() {

        if displayItems.items.isEmpty {
            loadData()
        }
    }

    override func navigateToDetailScreen(withTransport transport: PromotionDetailTransport) {

        if let locationM = locationManager {
            locationM.stopLocationObserver()
        }
        
        transport.completion = blockForCompletion()
        
        transport.userLocation = userLocation

        busManager.navigateScreen(tag: routerTag, ShoppingPageReaction.EVENT_NAV_TO_DETAIL_SCREEN, transport)
    }
    
    func blockForCompletion() -> (() -> Void)? {
        
        return { [weak self] () -> Void in
            if let locationM = self?.locationManager {
                locationM.startLocationObserverAndCheckStatus()
            }
        }
    }

    override func createDisplayitemWithPromotion(withPromotion promotion: PromotionBO, withNumber number: Int, withCategoryType categoryType: CategoryType) -> DisplayItemPromotion {

        var displayItemPromotion: DisplayItemPromotion

        if number == 0 {
            displayItemPromotion = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotion, withTypeCell: .high, withCategoryType: categoryType, userLocation: userLocation)
        } else {
            displayItemPromotion = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotion, withTypeCell: .small, withCategoryType: categoryType, userLocation: userLocation)
        }

        return displayItemPromotion
    }

    override func headerSelected(forCategory categorySelected: CategoryBO, andDisplayItem item: DisplayItemPromotion) {

        let promotionsTrasport = PromotionsTransportA()
        promotionsTrasport.promotionsBO = promotionsAll[categorySelected.id.rawValue]
        promotionsTrasport.promotionName = item.categoryString
        promotionsTrasport.category = categorySelected
        promotionsTrasport.userLocation = userLocation

        navigateToNextScreen(withTransport: promotionsTrasport)
    }
}

extension ShoppingPresenterA: ShoppingPresenterProtocolA {

    func accessMapSelected() {

        let model = PromotionsTransport()
        model.categories = categories

        busManager.navigateScreen(tag: routerTag, ShoppingPageReactionA.EVENT_NAV_TO_SECOND_SCREEN, model)
    }

    func toastPressed() {

        switch lastLocationErrorType {
        case .some(.permissionDenied):
            view?.openAppSettings()
        case .some(.gpsOff):
            busManager.navigateScreen(tag: routerTag, ShoppingPageReactionA.EVENT_NAV_TO_LOCATION_SETTING_HELP, Void())
        default:
            break
        }
    }

    func viewDidAppear() {

        if let locationM = locationManager {
            locationM.startLocationObserverAndCheckStatus()
        }
    }

    func viewDidDisappear() {

        if let locationM = locationManager {
            locationM.stopLocationObserver()
        }
    }
}
