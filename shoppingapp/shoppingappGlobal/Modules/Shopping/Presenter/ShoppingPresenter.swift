//
//  ShoppingPresenter.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 7/3/17.
//  Copyright © 2017 daniel.petrovic. All rights reserved.
//

import Foundation
import RxSwift
import CellsNativeComponents

class ShoppingPresenter<T: ShoppingViewProtocol>: BasePresenter<T> {

    let disposeBag = DisposeBag()
    var interactor: ShoppingInteractorProtocol!

    var categories: CategoriesBO?
    var promotionsAll = [String: PromotionsBO]()

    var promotionsTrending: PromotionsBO?
    var errorsTrackData = ErrorTrackData()

    var displayItems = DisplayItemsPromotions()

    let categoriesNumberItems = 3
    let maxSmallPromotions = 3
    let promotionsForScreen = 3
    var promotionsCalled = 0

    var trendingFinish = false
    var promotionsFinish = false
    var errorNoTsec = false
    var allowConfigureFavoriteCategories = Settings.Promotions.should_show_configure_favorite_categories
    lazy var isUserLogged = SessionDataManager.sessionDataInstance().isUserLogged
    
    required init() {
        super.init(tag: ShoppingPageReaction.ROUTER_TAG)
    }

    override init(busManager: BusManager) {
        super.init(busManager: busManager)
        routerTag = ShoppingPageReaction.ROUTER_TAG
    }

    func viewLoaded() {

        loadData()
    }

    func navigateToNextScreen(withTransport transport: PromotionsTransport) {

        busManager.navigateScreen(tag: routerTag, ShoppingPageReaction.EVENT_NAV_TO_NEXT_SCREEN, transport)

    }

    func navigateToDetailScreen(withTransport transport: PromotionDetailTransport) {

        busManager.navigateScreen(tag: routerTag, ShoppingPageReaction.EVENT_NAV_TO_DETAIL_SCREEN, transport)

    }

    func getPromotionsForNextCategory() {

        if let categoriesBO = categories, !categoriesBO.categories.isEmpty {

            if promotionsCalled < promotionsForScreen && promotionsCalled < categoriesBO.categories.count {

                getPromotionByCategory(withCategory: (categoriesBO.categories[promotionsCalled]))

            }

        }

    }

    func shoppingTrendingParamsTransport() -> ShoppingParamsTransport {
        return ShoppingParamsTransport(trendingId: TrendingType.hot.rawValue, orderBy: .endDate)
    }

    func getPromotionTrending() {

        let params = shoppingTrendingParamsTransport()

        interactor.providePromotions(byParams: params).subscribe(
            onNext: { [weak self] result in
                guard let `self` = self else {
                    return
                }
                self.trendingFinish = true

                if let promotionsBO = result as? PromotionsBO {
                    self.promotionsTrending = promotionsBO
                }

                if self.promotionsTrending?.status != ConstantsHTTPCodes.NO_CONTENT {
                    self.createTrendingDisplayData()
                    self.checkFinishService()
                }

            },
            onError: { [weak self] error in
                guard let `self` = self else {
                    return
                }
                DLog(message: "\(error)")
                let evaluate: ServiceError = (error as? ServiceError)!

                self.trendingFinish = true

                switch evaluate {

                case .GenericErrorBO(let errorBO):

                    self.errorsTrackData.append(errorCode: errorBO.status, errorName: Promotions.trending)
                    
                    if self.errorBO == nil {
                        self.errorBO = errorBO
                        self.checkError()
                    } else {
                        self.errorBO?.status = errorBO.status
                    }

                default:
                    break

                }

            }).addDisposableTo(disposeBag)
    }

    func createDisplayDataItems(withCategory categoryType: CategoryType, withCategoryString categoryString: String) {

        displayItems.items?.append(DisplayItemPromotion.generateDisplayItemPromotion(withCategoryString: categoryString, withCategoryType: categoryType))

        var number = 0

        if let promotionsByCategory = promotionsAll[categoryType.rawValue] {
            if promotionsByCategory.promotions.count > maxSmallPromotions + 1 {

                for i in 0 ... maxSmallPromotions {

                    let displayItemPromotion = createDisplayitemWithPromotion(withPromotion: promotionsByCategory.promotions[i], withNumber: number, withCategoryType: categoryType)

                    number += 1

                    displayItems.items?.append(displayItemPromotion)
                }

            } else {
                for promotionBO in promotionsByCategory.promotions {

                    let displayItemPromotion = createDisplayitemWithPromotion(withPromotion: promotionBO, withNumber: number, withCategoryType: categoryType)

                    number += 1

                    displayItems.items?.append(displayItemPromotion)
                }
            }
        }

    }

    func checkFinishService() {

        if trendingFinish && promotionsFinish {

            if !displayItems.items.isEmpty {

                view?.hideSkeleton()
                view?.showFilterButton()
                view?.showPromotions(withDisplayItemsPromotions: displayItems)

            } else {

                if errorNoTsec {
                    showErrorNoTsec()
                } else {
                    showNoContentView()
                }
            }
        }
    }

    func shoppingParamsTransport(withCategory category: CategoryBO) -> ShoppingParamsTransport {
        return ShoppingParamsTransport(categoryId: category.id.rawValue)
    }

    func categorySelected(withCategory category: CategoryBO) {

        let promotionsTrasport = PromotionsTransport()
        promotionsTrasport.category = category
        promotionsTrasport.promotionName = category.name

        navigateToNextScreen(withTransport: promotionsTrasport)

    }

    func headerSelected(forCategory categorySelected: CategoryBO, andDisplayItem item: DisplayItemPromotion) {

        let promotionsTrasport = PromotionsTransport()
        promotionsTrasport.promotionsBO = promotionsAll[categorySelected.id.rawValue]
        promotionsTrasport.promotionName = item.categoryString
        promotionsTrasport.category = categorySelected

        navigateToNextScreen(withTransport: promotionsTrasport)
    }

    func createDisplayitemWithPromotion(withPromotion promotion: PromotionBO, withNumber number: Int, withCategoryType categoryType: CategoryType) -> DisplayItemPromotion {

        var displayItemPromotion: DisplayItemPromotion

        if number == 0 {
            displayItemPromotion = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotion, withTypeCell: .high, withCategoryType: categoryType)
        } else {
            displayItemPromotion = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotion, withTypeCell: .small, withCategoryType: categoryType)
        }

        return displayItemPromotion
    }
    
    func categoriesLoaded() {
        
    }
}

// MARK: - Private methods
extension ShoppingPresenter {

    func loadData() {

        errorNoTsec = false
        errorsTrackData.reset()

        view?.showSkeleton()

        interactor.provideCategories().subscribe(
            onNext: { [weak self] result in
                guard let `self` = self, let categoriesBO = result as? CategoriesBO else {
                    return
                }

                self.trendingFinish = false
                self.promotionsFinish = false
                self.promotionsCalled = 0
                self.displayItems = DisplayItemsPromotions()
                self.promotionsAll = [String: PromotionsBO]()
                
                self.categories = categoriesBO

                if self.categories?.status == ConstantsHTTPCodes.NO_CONTENT {
                    self.view?.hideSkeleton()
                    self.showNoContentView()
                } else {

                    self.getPromotionTrending()

                    if categoriesBO.categories.count > self.categoriesNumberItems {

                        let total : Int = categoriesBO.categories.count

                        let categoriesToComponent = CategoriesBO()

                        for i in self.categoriesNumberItems ..< total {
                            categoriesToComponent.categories.append(categoriesBO.categories[i])
                        }

                        let displayModel = PromotionsCategoryDisplayModel(categories: categoriesToComponent, shouldShowSelectFavoriteCategories: self.allowConfigureFavoriteCategories && self.isUserLogged)
                        self.view?.showCategories(withDisplayModel: displayModel)
                        
                        self.categoriesLoaded()
                    }

                    self.getPromotionsForNextCategory()
                }

            },
            onError: { [weak self] error in
                DLog(message: "\(error)")
                guard let `self` = self else {
                    return
                }

                guard let serviceError = error as? ServiceError else {
                    return
                }

                switch serviceError {

                case .GenericErrorBO(let errorBO):

                    self.errorBO = errorBO

                    self.view?.hideSkeleton()

                    self.checkError()
                    
                    self.errorsTrackData.append(errorCode: errorBO.status, errorName: Promotions.categories)

                    if !errorBO.sessionExpired() {
                        self.showErrorNoTsec()
                    }

                default:
                    break
                }
            }).addDisposableTo(disposeBag)
    }

    func getPromotionByCategory(withCategory category: CategoryBO) {

        let params = shoppingParamsTransport(withCategory: category)

        interactor.providePromotions(byParams: params).subscribe(
            onNext: { [weak self] result in
                guard let `self` = self else {
                    return
                }
                self.promotionsCalled += 1

                var promotionsReceive = PromotionsBO()

                self.errorNoTsec = false

                self.getPromotionsForNextCategory()

                if let promotionsBO = result as? PromotionsBO {
                    promotionsReceive = promotionsBO
                }

                if self.promotionsCalled == self.promotionsForScreen || self.promotionsCalled == self.categories!.categories.count {
                    self.promotionsFinish = true
                    if promotionsReceive.status != ConstantsHTTPCodes.NO_CONTENT {
                        self.promotionsAll[category.id.rawValue] = promotionsReceive
                        self.createDisplayDataItems(withCategory: category.id, withCategoryString: category.name)
                    }
                    self.checkFinishService()
                } else {
                    if promotionsReceive.status != ConstantsHTTPCodes.NO_CONTENT {
                        self.promotionsAll[category.id.rawValue] = promotionsReceive
                        self.createDisplayDataItems(withCategory: category.id, withCategoryString: category.name)
                    }
                }

            },
            onError: { [weak self] error in
                DLog(message: "\(error)")
                guard let `self` = self else {
                    return
                }
                self.promotionsCalled += 1

                self.getPromotionsForNextCategory()

                guard let serviceError = error as? ServiceError else {
                    return
                }

                switch serviceError {

                case .GenericErrorBO(let errorBO):

                    if self.errorBO == nil {
                        self.errorBO = errorBO

                        self.checkError()
                    } else {
                        self.errorBO?.status = errorBO.status
                    }
                    
                    self.errorsTrackData.append(errorCode: errorBO.status, errorName: Promotions.promotionByCategory)

                    if !errorBO.sessionExpired() {
                        self.errorNoTsec = true
                    }

                default:
                    break
                }
                
                if self.promotionsCalled == self.promotionsForScreen || self.promotionsCalled == self.categories!.categories.count {
                    self.promotionsFinish = true
                    self.checkFinishService()
                }

            }).addDisposableTo(disposeBag)
    }

    func showErrorNoTsec() {
        
        if !errorsTrackData.isEmpty() {
            view?.trackErrorScreen(withError: errorsTrackData.format())
        }
        
        view?.showErrorEmptyTsec()
    }

    func showNoContentView() {
        view?.showNoContentView()
    }

    func createTrendingDisplayData() {

        let displayItemPromotion = DisplayItemPromotion.generateDisplayItemPromotionTrending(withTrendingPromotions: promotionsTrending!)
        if !displayItems.items.contains(displayItemPromotion) {
            displayItems.items?.insert(displayItemPromotion, at: 0)
        }
        trendingFinish = true
        checkFinishService()

    }
}

extension ShoppingPresenter: ShoppingPresenterProtocol {
        
    func viewWillAppear() {
        
        if errorsTrackData.isEmpty() {
            view?.trackScreen()
        } else {
            view?.trackErrorScreen(withError: errorsTrackData.format())
        }
    }

    func showTrendingPromotions() {

        let promotionsTrasport = PromotionsTransport()
        promotionsTrasport.promotionsBO = promotionsTrending
        promotionsTrasport.promotionName = Localizables.promotions.key_promotions_featured_text.capitalized
        promotionsTrasport.trendingType = .hot

        navigateToNextScreen(withTransport: promotionsTrasport)

    }

    func itemSelected(withDisplayItem displayItem: DisplayItemPromotion?) {

        guard let item = displayItem, let typeCell = item.typeCell else {
            return
        }

        let categoryType = item.categoryType
        var categorySelected: CategoryBO?

        if categoryType != nil {

            if let categoriesBO = categories {

                for i in 0 ..< categoriesBO.categories.count where categoriesBO.categories[i].id == categoryType {
                        categorySelected = categoriesBO.categories[i]
                        break
                }
            }

        }

        switch typeCell {
        case .header:
            guard let categorySelected = categorySelected else {
                return
            }

            headerSelected(forCategory: categorySelected, andDisplayItem: item)

        case .high, .trending, .small:
            guard let promotionBO = item.promotionBO else {
                return
            }

            let promotionDetailTransport = PromotionDetailTransport()
            promotionDetailTransport.promotionBO = promotionBO
            promotionDetailTransport.category = categorySelected

            navigateToDetailScreen(withTransport: promotionDetailTransport)
        }

    }

    func filterPressed() {

        let model = ShoppingFilterTransport()
        model.shouldAllowSelectUsageType = true
        model.categoriesBO = categories

        busManager.navigateScreen(tag: routerTag, ShoppingPageReaction.EVENT_NAV_TO_FILTER_SCREEN, model)

    }

    func retryButtonPressed() {

        loadData()
    }
}
