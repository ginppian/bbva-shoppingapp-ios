//
//  ShoppingPresenterB.swift
//  shoppingappMX
//
//  Created by jesus.martinez on 4/6/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

class ShoppingPresenterB<T: ShoppingViewProtocolB>: ShoppingPresenterA<T> {
    
    let maximumTimesToSuggestFavoriteSelection = 5
    let firstAccess = 1
    let accessToShowFirstSuggestion = 2
    let accessToControlLastSuggestion = 3
    let pageSizeForPromotionsForCategories = 5
    
    var cardTitleIds: [String]?
    var countAccessInNextApparition = false
    
    override func shoppingParamsTransport(withCategory category: CategoryBO) -> ShoppingParamsTransport {
        
        let location = userLocation ?? defaultLocation

        let locationParam = ShoppingParamLocation(geolocation: location, distanceParam: ShoppingDistanceParam(distance: defaultDistance, lengthType: lengthType))
        
        return ShoppingParamsTransport(categoryId: category.id.rawValue, location: locationParam, titleIds: cardTitleIds, pageSize: pageSizeForPromotionsForCategories)
    }

    override func shoppingTrendingParamsTransport() -> ShoppingParamsTransport {

        guard let cardTitleIds = cardTitleIds else {

            return super.shoppingTrendingParamsTransport()
        }

        return ShoppingParamsTransport(trendingId: TrendingType.hot.rawValue, titleIds: cardTitleIds, orderBy: .endDate)
    }
    
    private func navigateToFavoriteCategoriesSelector() {
        
        guard let categories = categories else {
            return
        }
        
        let model = FavoriteCategoriesTransport(categories: categories)
        busManager.navigateScreen(tag: routerTag, ShoppingPageReactionB.EVENT_NAV_TO_FAVORITE_CATEGORIES, model)
    }
    
    override func categoriesLoaded() {
        
        if view?.isVisibleOnTop() ?? false {
            
            promotionsWillBeShown()
        }
    }
    
    override func blockForCompletion() -> (() -> Void)? {
        
        return { [weak self] () -> Void in
            if let locationM = self?.locationManager {
                locationM.startLocationObserverAndCheckStatus()
                self?.checkCountAccessInNextApparition()
            }
        }
    }
}

extension ShoppingPresenterB: ShoppingPresenterProtocolB {
    
    func tabPressed() {
        promotionsWillBeShown()
    }

    func selectFavoriteCategoriesPressed() {
        
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: true as AnyObject, withKey: PreferencesManagerKeys.kDontSuggestFavoriteSelection)
        navigateToFavoriteCategoriesSelector()
    }
    
    func promotionsWillBeShown() {
        
        guard categories != nil, !PreferencesManager.sharedInstance().getDontSuggestFavoriteSelection(), !ToastManager.shared().isThereAToastVisible(), SessionDataManager.sessionDataInstance().isUserLogged else {
            
            return
        }
        
        PreferencesManager.sharedInstance().incrementPromotionsSectionAccessCounter()
        
        let accessCounter = PreferencesManager.sharedInstance().getPromotionsSectionAccessCounter()
        let value = PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kWaitToAccessToSuggestFavoriteSelection) as? NSNumber ?? NSNumber(value: maximumTimesToSuggestFavoriteSelection - 1)
        
        switch accessCounter {
        case firstAccess:
            PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: maximumTimesToSuggestFavoriteSelection - 1), withKey: PreferencesManagerKeys.kWaitToAccessToSuggestFavoriteSelection)
        case accessToShowFirstSuggestion, value.intValue:
            navigateToFavoriteCategoriesSelector()
        default:
            break
        }
    }
    
    func checkCountAccessInNextApparition() {
        
        if countAccessInNextApparition {
            
            countAccessInNextApparition = false
            promotionsWillBeShown()
        }
    }
    
    func favoriteCategoriesNotChanged() {
        
        view?.scrollToTop()
    }
    
    func userScrollsToBottom() {
        
        let accessCounter = PreferencesManager.sharedInstance().getPromotionsSectionAccessCounter()

        if accessCounter == accessToControlLastSuggestion {
            PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: maximumTimesToSuggestFavoriteSelection), withKey: PreferencesManagerKeys.kWaitToAccessToSuggestFavoriteSelection)
        }
    }
    
    func toastViewDidDissapearAndViewIsVisible() {
        
        promotionsWillBeShown()
    }
    
    func toastViewDidDissapearButViewIsNotVisible() {
        
        countAccessInNextApparition = true
    }
}

// MARK: - reactions
extension ShoppingPresenterB {

    func receive(model: Model) {

        if let cards = model as? CardsBO, cardTitleIds != cards.titleIds() {
            cardTitleIds = cards.titleIds()
        }
    }

    func favoriteCategoriesChanged() {
        
        loadData()
    }
}
