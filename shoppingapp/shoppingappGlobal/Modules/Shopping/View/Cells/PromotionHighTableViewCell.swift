//
//  PromotionHightTableViewCell.swift
//  shoppingapp
//
//  Created by ruben.fernandez on 7/11/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit

class PromotionHighTableViewCell: UITableViewCell {

    static let identifier = "PromotionHighTableViewCell"

    @IBOutlet weak var promotionImage: UIImageView!

    @IBOutlet weak var commerceNameLabel: UILabel!
    @IBOutlet weak var descriptionOfferLabel: UILabel!
    @IBOutlet weak var diffDateLabel: UILabel!
    @IBOutlet weak var benefitLabel: UILabel!
    @IBOutlet weak var promotionTypeView: PromotionTypeView!
    @IBOutlet weak var promotionTypeViewWidth: NSLayoutConstraint!

    static let screenWidth = UIScreen.main.bounds.size.width
    static let padding: CGFloat = 20.0
    static let imageAspectRatio: CGFloat = 151 / 304
    static let spaceBetweenImageAndCommerceName: CGFloat = 20.0
    static let commerceNameHeight: CGFloat = 12.0
    static let spaceBetweenCommerceNameAndDescriptionOffer: CGFloat = 10.0
    static let descriptionOfferHeight: CGFloat = 34.0
    static let spaceBetweenDescriptionOfferAndBenefit: CGFloat = 10.0
    static let benefitHeight: CGFloat = 17.0
    static let spaceBetweenBenefitAndDiffDate: CGFloat = 15.0
    static let diffDateHeight: CGFloat = 17.0
    static let bottomSpace: CGFloat = 32.0

    static var height: CGFloat {

        let imageHeight = (screenWidth - (PromotionHighTableViewCell.padding * 2)) * imageAspectRatio

        return imageHeight
            + spaceBetweenImageAndCommerceName + commerceNameHeight
            + spaceBetweenCommerceNameAndDescriptionOffer + descriptionOfferHeight
            + spaceBetweenDescriptionOfferAndBenefit + benefitHeight
            + spaceBetweenBenefitAndDiffDate + diffDateHeight
            + bottomSpace

    }

    override func awakeFromNib() {
        super.awakeFromNib()

        selectionStyle = .none

        promotionImage.backgroundColor = .BBVA200

        commerceNameLabel.font = Tools.setFontBook(size: 12)
        commerceNameLabel.textColor = .BBVA600

        descriptionOfferLabel.font = Tools.setFontMedium(size: 14)
        descriptionOfferLabel.textColor = .BBVA600

        diffDateLabel.font = Tools.setFontBookItalic(size: 14)
        diffDateLabel.textColor = .BBVA500

        benefitLabel.font = Tools.setFontMedium(size: 14)
        benefitLabel.textColor = .MEDIUMBLUE

    }

    func configureCellWithDisplayPromotionData(withDisplayitemPromotion displayItemPromotion: DisplayItemPromotion) {

        commerceNameLabel.text = displayItemPromotion.commerceName
        descriptionOfferLabel.text = displayItemPromotion.descriptionPromotion

        diffDateLabel.text = displayItemPromotion.dateDiffPlusDistance ?? displayItemPromotion.dateDiff

        benefitLabel.text = displayItemPromotion.benefit

        if !Tools.isStringNilOrEmpty(withString: displayItemPromotion.imageDetail) {
            configureImage(withDisplayitemPromotion: displayItemPromotion)
        } else {
            promotionImage.image = UIImage(named: ConstantsImages.Promotions.default_promotion_image)
        }

        promotionTypeView.configurePromotionType(withPromotionTypeDisplayData: createPromotionTypeDisplayData(withPromotion: displayItemPromotion.promotionBO!))

        setAccessibilities()

    }

    private func createPromotionTypeDisplayData(withPromotion promotion: PromotionBO) -> PromotionTypeDisplayData {
        let promotionTypeDisplayData = PromotionTypeDisplayData(fromPromotion: promotion)
        promotionTypeViewWidth.constant = promotionTypeDisplayData.promotionTypeViewWidth

       return promotionTypeDisplayData

    }

    private func configureImage(withDisplayitemPromotion displayItemPromotion: DisplayItemPromotion) {
        
        if let imageSaved = displayItemPromotion.promotionBO?.imageDetailSaved {

            promotionImage.image = imageSaved
            promotionImage.backgroundColor = .BBVAWHITE
        } else {

            promotionImage.image = nil
            promotionImage.backgroundColor = .BBVA200
            
            let url = URL(string: displayItemPromotion.imageDetail?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")

            promotionImage.setImage(fromUrl: url,
                                    placeholderImage: UIImage(named: ConstantsImages.Promotions.default_promotion_image)) {  [weak self] successful in
                                        if successful {
                                            displayItemPromotion.promotionBO?.imageDetailSaved = self?.promotionImage.image
                                            DispatchQueue.main.async {
                                                self?.promotionImage.backgroundColor = .BBVAWHITE
                                            }
                                        }
            }
        }
    }

}

// MARK: - APPIUM

private extension PromotionHighTableViewCell {

    func setAccessibilities() {

        #if APPIUM

            accessibilityIdentifier = ConstantsAccessibilities.PROMOTION_MAIN_PROMOTION_CELL

            Tools.setAccessibility(view: commerceNameLabel, identifier: ConstantsAccessibilities.PROMOTION_PROMOTION_COMMERCE)
            Tools.setAccessibility(view: descriptionOfferLabel, identifier: ConstantsAccessibilities.PROMOTION_PROMOTION_DESCRIPTION)

            Tools.setAccessibility(view: benefitLabel, identifier: ConstantsAccessibilities.PROMOTION_PROMOTIONS_HOT_BENEFIT)

        #endif

    }

}
