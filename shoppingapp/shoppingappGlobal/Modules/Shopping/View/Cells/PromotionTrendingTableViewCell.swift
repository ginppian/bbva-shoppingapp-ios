//
//  PromotionTrendingTableViewCell.swift
//  shoppingapp
//
//  Created by ruben.fernandez on 29/11/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit

class PromotionTrendingTableViewCell: UITableViewCell {

    static let identifier = "PromotionTrendingTableViewCell"

    @IBOutlet weak var promotionTrendingView: PromotionsTrendingView!

    static var height: CGFloat {
        return PromotionsTrendingView.height
    }

    override func awakeFromNib() {
        super.awakeFromNib()

        selectionStyle = .none

    }

    func configureCellWithDisplayPromotionData(withDisplayitemPromotion displayItemPromotion: DisplayItemPromotion, withDelegate delegate: PromotionsTrendingViewDelegate) {

        promotionTrendingView.configurePromotionsTrending(withPromotions: displayItemPromotion.itemsTrending)
        promotionTrendingView.delegate = delegate
        setAccessibilities()

    }

}
// MARK: - APPIUM

private extension PromotionTrendingTableViewCell {

    func setAccessibilities() {

        #if APPIUM

            accessibilityIdentifier = ConstantsAccessibilities.PROMOTION_HOT_COMPONENT

            promotionTrendingView.isAccessibilityElement = false

            Tools.setAccessibility(view: promotionTrendingView.trendingLabel, identifier: ConstantsAccessibilities.PROMOTION_HOT_HEADER_TITLE)

            Tools.setAccessibility(view: promotionTrendingView.seeAllButton, identifier: ConstantsAccessibilities.SHOW_ALL_PROMOTIONS_HOT)

        #endif

    }

}
