//
//  PromotionSmallTableViewCell.swift
//  shoppingapp
//
//  Created by ruben.fernandez on 7/11/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit

class PromotionSmallTableViewCell: UITableViewCell {

    static let identifier = "PromotionSmallTableViewCell"

    @IBOutlet weak var promotionImage: UIImageView!
    @IBOutlet weak var promotionTypeView: PromotionTypeView!

    @IBOutlet weak var commerceNameLabel: UILabel!
    @IBOutlet weak var descriptionOfferLabel: UILabel!
    @IBOutlet weak var diffDateLabel: UILabel!
    @IBOutlet weak var benefitLabel: UILabel!
    @IBOutlet weak var promotionTypeViewWidth: NSLayoutConstraint!

    static let height: CGFloat = 159.0

    var mAddSeparator: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()

        selectionStyle = .none

        promotionImage.backgroundColor = .BBVA200

        let mScreenSize = UIScreen.main.bounds
        let mSeparatorHeight = CGFloat(1.0)
        mAddSeparator = UIView(frame: CGRect(x: 20, y: 0, width: mScreenSize.width, height: mSeparatorHeight))
        mAddSeparator.backgroundColor = .BBVA200
        self.addSubview(mAddSeparator)

        commerceNameLabel.font = Tools.setFontBook(size: 12)
        commerceNameLabel.textColor = .BBVA600

        descriptionOfferLabel.font = Tools.setFontMedium(size: 14)
        descriptionOfferLabel.textColor = .BBVA600

        diffDateLabel.font = Tools.setFontBookItalic(size: 14)
        diffDateLabel.textColor = .BBVA500

        benefitLabel.font = Tools.setFontMedium(size: 14)
        benefitLabel.textColor = .MEDIUMBLUE

    }

    func configureCellWithDisplayPromotionData(withDisplayitemPromotion displayItemPromotion: DisplayItemPromotion) {

        commerceNameLabel.text = displayItemPromotion.commerceName
        descriptionOfferLabel.text = displayItemPromotion.descriptionPromotion
        benefitLabel.text = displayItemPromotion.benefit

        diffDateLabel.text = displayItemPromotion.dateDiffPlusDistance ?? displayItemPromotion.dateDiff

        if !Tools.isStringNilOrEmpty(withString: displayItemPromotion.imageList) {
            configureImage(withDisplayitemPromotion: displayItemPromotion)
        } else {
            promotionImage.image = UIImage(named: ConstantsImages.Promotions.default_promotion_list_image)
            promotionImage.contentMode = .scaleAspectFill
        }

        promotionTypeView.configurePromotionType(withPromotionTypeDisplayData: createPromotionTypeDisplayData(withPromotion: displayItemPromotion.promotionBO!))

        setAccessibilities()
    }

    private func createPromotionTypeDisplayData(withPromotion promotion: PromotionBO) -> PromotionTypeDisplayData {
        let promotionTypeDisplayData = PromotionTypeDisplayData(fromPromotion: promotion)
        promotionTypeViewWidth.constant = promotionTypeDisplayData.promotionTypeViewWidth

        return promotionTypeDisplayData
    }

    private func configureImage(withDisplayitemPromotion displayItemPromotion: DisplayItemPromotion) {
        
        if let imageSaved = displayItemPromotion.promotionBO?.imageListSaved {

            promotionImage.image = imageSaved
            promotionImage.backgroundColor = .BBVAWHITE
        } else {

            promotionImage.image = nil
            promotionImage.backgroundColor = .BBVA200
            promotionImage.contentMode = .scaleAspectFill
            
            let url = URL(string: displayItemPromotion.imageList?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")

            promotionImage.setImage(fromUrl: url,
                                    placeholderImage: UIImage(named: ConstantsImages.Promotions.default_promotion_list_image)) {  [weak self] successful in
                                        if successful {
                                            displayItemPromotion.promotionBO?.imageListSaved = self?.promotionImage.image
                                            DispatchQueue.main.async {
                                                self?.promotionImage.backgroundColor = .BBVAWHITE
                                            }
                                        }
            }
        }
    }

}

// MARK: - APPIUM

private extension PromotionSmallTableViewCell {

    func setAccessibilities() {

        #if APPIUM

            accessibilityIdentifier = ConstantsAccessibilities.PROMOTION_SECONDARY_PROMOTION_CELL

            Tools.setAccessibility(view: commerceNameLabel, identifier: ConstantsAccessibilities.PROMOTION_PROMOTION_COMMERCE)
            Tools.setAccessibility(view: descriptionOfferLabel, identifier: ConstantsAccessibilities.PROMOTION_PROMOTION_DESCRIPTION)

            Tools.setAccessibility(view: benefitLabel, identifier: ConstantsAccessibilities.PROMOTION_PROMOTIONS_HOT_BENEFIT)

        #endif

    }

}
