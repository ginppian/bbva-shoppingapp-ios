//
//  HeaderPromotionTableViewCell.swift
//  shoppingapp
//
//  Created by ruben.fernandez on 10/11/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit

class HeaderPromotionTableViewCell: UITableViewCell {

    static let identifier = "HeaderPromotionTableViewCell"

    @IBOutlet weak var headerImage: UIImageView!
    @IBOutlet weak var arrowImage: UIImageView!
    @IBOutlet weak var categoryLabel: UILabel!

    var categoryImageCell: String?

    static let height: CGFloat = 72.0

    override func awakeFromNib() {
        super.awakeFromNib()

        selectionStyle = .none

        contentView.backgroundColor = .BBVA200

        categoryImageCell = ConstantsImages.Promotions.promotion

        arrowImage.image = ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Common.forward_icon, color: .BBVA400), width: Float(arrowImage.frame.size.width), height: Float(arrowImage.frame.size.height))

        categoryLabel.font = Tools.setFontBook(size: 18)
        categoryLabel.textColor = .BBVA600

        setAccessibilities()

    }

    func configureCellWithDisplayPromotionData(withDisplayitemPromotion displayItemPromotion: DisplayItemPromotion) {
        categoryLabel.text = displayItemPromotion.categoryString

        categoryImageCell = displayItemPromotion.categoryImage

        headerImage.image = ImageUtilities.image(with: SVGCache.image(forSVGNamed: displayItemPromotion.categoryImage!, color: displayItemPromotion.categoryImageColor!), width: Float(headerImage.frame.size.width), height: Float(headerImage.frame.size.height))

    }

    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)

        if highlighted {

            headerImage.image = ImageUtilities.image(with: SVGCache.image(forSVGNamed: categoryImageCell!, color: .COREBLUE), width: Float(headerImage.frame.size.width), height: Float(headerImage.frame.size.height))

            arrowImage.image = ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Common.forward_icon, color: .COREBLUE), width: Float(arrowImage.frame.size.width), height: Float(arrowImage.frame.size.height))

            categoryLabel.textColor = .COREBLUE

        } else {

            headerImage.image = ImageUtilities.image(with: SVGCache.image(forSVGNamed: categoryImageCell!, color: .BBVA400), width: Float(headerImage.frame.size.width), height: Float(headerImage.frame.size.height))

            arrowImage.image = ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Common.forward_icon, color: .BBVA400), width: Float(arrowImage.frame.size.width), height: Float(arrowImage.frame.size.height))

            categoryLabel.textColor = .BBVA600

        }

    }

}

// MARK: - APPIUM

private extension HeaderPromotionTableViewCell {

    func setAccessibilities() {

        #if APPIUM

            accessibilityIdentifier = ConstantsAccessibilities.PROMOTION_SECONDARY_PROMOTION_CELL

            Tools.setAccessibility(view: categoryLabel, identifier: ConstantsAccessibilities.PROMOTION_HEADER_TITLE)

        #endif

    }

}
