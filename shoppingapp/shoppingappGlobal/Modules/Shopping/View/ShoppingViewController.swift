//
//  ShoppingViewController.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 8/3/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit
import CellsNativeCore
import CellsNativeComponents

class ShoppingViewController: BaseViewController {

    // MARK: Constants

    static let ID: String = String(describing: ShoppingViewController.self)
    
    var presenter: ShoppingPresenterProtocol!

    var needShowSkeleton = false
    var needCompleteSkeleton = false
    var shouldShowSelectFavoriteCategories = Settings.Promotions.should_show_configure_favorite_categories
    static let numberOfRowsCompleteSkeleton = 2
    static let numberOfRowsForSkeletoon = 1

    var footerView: PromotionsCategoryView?

    // MARK: outlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var errorScreenView: ErrorScreenView! {
        didSet {
            errorScreenView.delegate = self
        }
    }

    var displayItems: DisplayItemsPromotions?

    static let showSkeletonCompleteFirstCellHeight: CGFloat = 414.0
    static let showSkeletonCompleteSecondCellHeight: CGFloat = 700.0

    static let skeletonCarouselPromotionCellIndex = 0
    static let skeletonBigPromotionCellIndex = 1
    
    static let heightOfFooterSeparator: CGFloat = 10.0

    // MARK: life cycle

    override func viewDidLoad() {

        self.configure(appLifecycle: (UIApplication.shared.delegate as! AppDelegate).appLifeCycle!, id: ShoppingViewController.ID)

        super.viewDidLoad()

        edgesForExtendedLayout = UIRectEdge()

        configureContentView()

        presenter.viewLoaded()

    }

    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        setNavigationBarDefault ()
        addNavigationBarMenuButton()

        setNavigationTitle(withText: Localizables.common.key_tabbar_offers_button)

        presenter.viewWillAppear()
        
        setAccessibilities()

        self.tabBarController?.tabBar.isHidden = false
    }
    
    override func viewWillAppearAfterDismiss() {
        
        presenter.viewWillAppear()
    }
    
    deinit {
        
        ComponentManager.remove(id: presenter.routerTag)
        DLog(message: "Deinitialized ShoppingViewController")
    }

    // MARK: custom methods

    func configureContentView() {

        tableView.backgroundColor = .BBVA200

        tableView.register(UINib(nibName: SkeletonCarouselPromotionCell.identifier, bundle: nil), forCellReuseIdentifier: SkeletonCarouselPromotionCell.identifier)
        tableView.register(UINib(nibName: SkeletonBigPromotionCell.identifier, bundle: nil), forCellReuseIdentifier: SkeletonBigPromotionCell.identifier)
        tableView.register(UINib(nibName: PromotionHighTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: PromotionHighTableViewCell.identifier)
        tableView.register(UINib(nibName: PromotionSmallTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: PromotionSmallTableViewCell.identifier)
        tableView.register(UINib(nibName: HeaderPromotionTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: HeaderPromotionTableViewCell.identifier)
        tableView.register(UINib(nibName: PromotionTrendingTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: PromotionTrendingTableViewCell.identifier)

        tableView.separatorStyle = .none
    }

    @objc func filterAction(_ sender: AnyObject?) {

        presenter.filterPressed()

    }

    func showCategories(withDisplayModel displayModel: PromotionsCategoryDisplayModel) {

        footerView = PromotionsCategoryView()
        footerView?.delegate = self
        footerView?.configurePromotionsCategory(withDisplayModel: displayModel)
    }

    func promotionsCategoryViewSelectFavoriteCategoriesPressed(_ promotionsCategoryView: PromotionsCategoryView) {
    }

    func promotionsTrendingWillBeginDragging(_ promotionsTrending: PromotionsTrendingView) {
    }
}

extension ShoppingViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        presenter.itemSelected(withDisplayItem: displayItems?.items[indexPath.row])
    }
}

extension ShoppingViewController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var rows = 0
        if  needShowSkeleton {
            if needCompleteSkeleton {
                rows = ShoppingViewController.numberOfRowsCompleteSkeleton
            } else {
                rows = ShoppingViewController.numberOfRowsForSkeletoon
            }
        } else {
            rows = displayItems?.items?.count ?? 0
        }

        return rows
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        var height: CGFloat = 0

        if  needShowSkeleton {
            if  needCompleteSkeleton {
                if  indexPath.row == ShoppingViewController.skeletonCarouselPromotionCellIndex {
                    height = ShoppingViewController.showSkeletonCompleteFirstCellHeight
                } else if indexPath.row == ShoppingViewController.skeletonBigPromotionCellIndex {
                    height = ShoppingViewController.showSkeletonCompleteSecondCellHeight
                }
            } else {
                height = ShoppingViewController.showSkeletonCompleteSecondCellHeight
            }
        } else {

            let displayItem: DisplayItemPromotion! = displayItems?.items![indexPath.row]

            switch displayItem.typeCell {
            case .high?:
                height = PromotionHighTableViewCell.height
            case .small?:
                height = PromotionSmallTableViewCell.height
            case .header?:
                height = HeaderPromotionTableViewCell.height
            case .trending?:
                height = PromotionTrendingTableViewCell.height
            default:
                height = 0
            }

        }

        return height
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if needShowSkeleton {
            if needCompleteSkeleton {
                if  indexPath.row == ShoppingViewController.skeletonCarouselPromotionCellIndex {
                    if let cell = tableView.dequeueReusableCell(withIdentifier: SkeletonCarouselPromotionCell.identifier, for: indexPath) as? SkeletonCarouselPromotionCell {
                        return cell
                    }
                } else if indexPath.row == ShoppingViewController.skeletonBigPromotionCellIndex {
                    if let cell = tableView.dequeueReusableCell(withIdentifier: SkeletonBigPromotionCell.identifier, for: indexPath) as? SkeletonBigPromotionCell {
                        return cell
                    }
                }
            } else {
                if let cell = tableView.dequeueReusableCell(withIdentifier: SkeletonBigPromotionCell.identifier, for: indexPath) as? SkeletonBigPromotionCell {
                    return cell
                }
            }
        } else {

            let displayItem: DisplayItemPromotion! = displayItems?.items![indexPath.row]

            if displayItem.typeCell == .high {
                if let cell = tableView.dequeueReusableCell(withIdentifier: PromotionHighTableViewCell.identifier, for: indexPath) as? PromotionHighTableViewCell {

                    cell.configureCellWithDisplayPromotionData(withDisplayitemPromotion: displayItem)

                    return cell
                }
            } else if displayItem.typeCell == .small {
                if let cell = tableView.dequeueReusableCell(withIdentifier: PromotionSmallTableViewCell.identifier, for: indexPath) as? PromotionSmallTableViewCell {

                    cell.configureCellWithDisplayPromotionData(withDisplayitemPromotion: displayItem)

                    return cell
                }
            } else if displayItem.typeCell == .header {
                if let cell = tableView.dequeueReusableCell(withIdentifier: HeaderPromotionTableViewCell.identifier, for: indexPath) as? HeaderPromotionTableViewCell {

                    cell.configureCellWithDisplayPromotionData(withDisplayitemPromotion: displayItem)

                    return cell
                }
            } else if displayItem.typeCell == .trending {
                if let cell = tableView.dequeueReusableCell(withIdentifier: PromotionTrendingTableViewCell.identifier, for: indexPath) as? PromotionTrendingTableViewCell {

                    cell.configureCellWithDisplayPromotionData(withDisplayitemPromotion: displayItem, withDelegate: self)

                    return cell
                }
            }

        }

        return UITableViewCell()
    }
}

extension ShoppingViewController: CloseModalDelegate {

    func errorViewControllerAcceptPressed(_ errorViewController: ErrorViewController) {
        presenter.checkError()
    }

    func errorViewControllerCancelPressed(_ errorViewController: ErrorViewController) {
    }

}

extension ShoppingViewController: PromotionsTrendingViewDelegate {

    func promotionsTrendingShowAll(_ promotionsTrending: PromotionsTrendingView) {

        presenter.showTrendingPromotions()
    }

    func promotionsTrending(_ promotionsTrending: PromotionsTrendingView, didSelectItem displayItem: DisplayItemPromotion?) {
        presenter.itemSelected(withDisplayItem: displayItem)
    }

}

extension ShoppingViewController: PromotionsCategoryViewDelegate {

    func promotionsCategoryViewConfig(_ promotionsCategoryView: PromotionsCategoryView, withHeight height: CGFloat) {
        footerView?.frame = CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: height)

    }

    func promotionsCategoryViewUpdate(_ promotionsCategoryView: PromotionsCategoryView, withHeight height: CGFloat) {
        footerView?.frame = CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: height)
        tableView.tableFooterView = footerView
    }

    func promotionsCategoryViewSelected(_ promotionsCategoryView: PromotionsCategoryView, withCategory category: CategoryBO) {
        presenter.categorySelected(withCategory: category)
    }
    
    func promotionsCategoryViewScrollForAllCategories() {
        
        DispatchQueue.main.async {
            
            guard let tableFooterView = self.tableView.tableFooterView, let footerView = self.footerView else {
                return
            }
            
            let footerViewHeight = footerView.frame.size.height
            let tableViewHeight = self.tableView.frame.size.height
            
            if footerViewHeight > tableViewHeight {
                
                let contentOffset = CGPoint(x: 0, y: tableFooterView.frame.origin.y + ShoppingViewController.heightOfFooterSeparator)
                self.tableView.setContentOffset(contentOffset, animated: true)
                
            } else {
                
                let footerRectInTable = self.tableView.convert(tableFooterView.bounds, from: tableFooterView)
                self.tableView.scrollRectToVisible(footerRectInTable, animated: true)
            }
        }
    }
}

extension ShoppingViewController: ShoppingViewProtocol {
    
    func trackScreen() {
        
        TrackerHelper.sharedInstance().trackPromotionsScreen(withCustomerID: SessionDataManager.sessionDataInstance().customerID)
    }
    
    func trackErrorScreen(withError error: String) {
        
        TrackerHelper.sharedInstance().trackPromotionsScreenError(error, andWithCustomerID: SessionDataManager.sessionDataInstance().customerID)
    }

    func showPromotions(withDisplayItemsPromotions displayItemPromotions: DisplayItemsPromotions) {

        displayItems = displayItemPromotions
        tableView.reloadData()
        tableView.isScrollEnabled = true

        if tableView.tableFooterView != footerView {
            tableView.tableFooterView = footerView
        }

    }

    @objc func showSkeleton() {

        needShowSkeleton = true
        needCompleteSkeleton = true

        tableView.isHidden = false
        tableView.isScrollEnabled = false

        UIView.animate(withDuration: 0.1, animations: {
            self.tableView.contentOffset = .zero
        }, completion: { _  in
            self.tableView.reloadData()
        })

    }

    @objc func hideSkeleton() {

        needShowSkeleton = false
    }

    func showError(error: ModelBO) {

        presenter.showModal()
    }

    func changeColorEditTexts() {

    }

    func showErrorEmptyTsec() {
        tableView.isHidden = true

        let errorMessage = Localizables.common.key_common_error_nodata_text
        let displayData = ErrorScreenDisplayData(title: Localizables.common.key_error_ups_text, message: errorMessage, hasRetryButton: true)
        errorScreenView.configureError(displayData: displayData)
    }

    func showNoContentView() {

        tableView.isHidden = true

        let errorMessage = Localizables.promotions.key_promotions_no_promotions_text + " " + Localizables.common.key_try_later_text
        let displayData = ErrorScreenDisplayData(title: Localizables.common.key_error_ups_text, message: errorMessage, hasRetryButton: false)
        errorScreenView.configureNoContent(displayData: displayData)
    }

    func showFilterButton() {

        let image = SVGCache.image(forSVGNamed: ConstantsImages.Common.search_icon, color: .BBVAWHITE)
        setNavigationRightButton(withImage: image, action: #selector(filterAction))

    }
}

// MARK: - ErrorScreenViewDelegate

extension ShoppingViewController: ErrorScreenViewDelegate {

    func errorScreenViewRetryButtonPressed(_ errorScreenView: ErrorScreenView) {

        presenter.retryButtonPressed()
    }
}

// MARK: - APPIUM

private extension ShoppingViewController {

    func setAccessibilities() {

        #if APPIUM

            tableView.accessibilityIdentifier = ConstantsAccessibilities.PROMOTIONS_LIST
            errorScreenView.accessibilityIdentifier = ConstantsAccessibilities.PROMOTIONS_ERROR_VIEW

            Tools.setAccessibility(view: errorScreenView.titleLabel, identifier: ConstantsAccessibilities.PROMOTIONS_TITLE_ERROR)
            Tools.setAccessibility(view: errorScreenView.messageLabel, identifier: ConstantsAccessibilities.PROMOTIONS_DESCRIPTION_ERROR)

        #endif

    }

}
