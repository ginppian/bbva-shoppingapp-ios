//
//  ShoppingViewControllerB.swift
//  shoppingappMX
//
//  Created by jesus.martinez on 4/6/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit

class ShoppingViewControllerB: ShoppingViewControllerA {
    
    override func viewDidAppear(_ animated: Bool) {

        super.viewDidAppear(animated)
        (presenter as? ShoppingPresenterProtocolB)?.checkCountAccessInNextApparition()
    }

    override func viewDidDisappear(_ animated: Bool) {

        super.viewDidDisappear(animated)
        (presenter as? ShoppingPresenterProtocolB)?.viewDidDisappear()
    }

    override func promotionsCategoryViewSelectFavoriteCategoriesPressed(_ promotionsCategoryView: PromotionsCategoryView) {

        (presenter as? ShoppingPresenterProtocolB)?.selectFavoriteCategoriesPressed()
    }
    
    override func toastViewDidDissapear() {
        
        if isVisibleOnTop() {
            (presenter as? ShoppingPresenterProtocolB)?.toastViewDidDissapearAndViewIsVisible()
        } else {
            (presenter as? ShoppingPresenterProtocolB)?.toastViewDidDissapearButViewIsNotVisible()
        }
    }
}

extension ShoppingViewControllerB: ShoppingViewProtocolB {

    func scrollToTop() {

        tableView.setContentOffset(.zero, animated: false)
    }
    
    func isVisibleOnTop() -> Bool {
        
        return isVisible()
    }
}

extension ShoppingViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        let height = scrollView.frame.size.height
        let contentYoffset = scrollView.contentOffset.y
        let distanceFromBottom = scrollView.contentSize.height - contentYoffset
        
        if distanceFromBottom < height {
            (presenter as? ShoppingPresenterProtocolB)?.userScrollsToBottom()
        }
    }
}
