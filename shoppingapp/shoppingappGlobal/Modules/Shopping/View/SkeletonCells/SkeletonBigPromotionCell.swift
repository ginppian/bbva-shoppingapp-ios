//
//  SkeletonCell.swift
//  Skeleton
//
//  Created by Gonzalo Nunez on 2/15/17.
//  Copyright © 2017 CocoaPods. All rights reserved.
//

import UIKit
import Shimmer

class SkeletonBigPromotionCell: UITableViewCell {

    static let identifier = "SkeletonBigPromotionCell"

    @IBOutlet var shimmeringView1: FBShimmeringView!
    @IBOutlet var shimmeringView2: FBShimmeringView!
    @IBOutlet var shimmeringView3: FBShimmeringView!
    @IBOutlet var shimmeringView4: FBShimmeringView!
    @IBOutlet var shimmeringView5: FBShimmeringView!

    @IBOutlet var shimmeringViewImage: FBShimmeringView!
    @IBOutlet var shimmeringViewImage2: FBShimmeringView!
    @IBOutlet var shimmeringViewImage3: FBShimmeringView!

    @IBOutlet var shimmeringView6: FBShimmeringView!
    @IBOutlet var shimmeringView7: FBShimmeringView!
    @IBOutlet var shimmeringView8: FBShimmeringView!
    @IBOutlet var shimmeringView9: FBShimmeringView!
    @IBOutlet var shimmeringView10: FBShimmeringView!
    @IBOutlet var shimmeringView11: FBShimmeringView!

    override func awakeFromNib() {

        super.awakeFromNib()

        contentView.backgroundColor = .white

        shimmeringView1.contentView = UIImageView(image: ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Skeleton.lines, color: .BBVA300), width: Float(shimmeringView1.frame.size.width), height: Float(shimmeringView1.frame.size.height)))
        shimmeringView1.isShimmering = true

        let viewImage = UIView(frame: shimmeringViewImage.frame)
        viewImage.backgroundColor = .BBVA300
        shimmeringViewImage.contentView = viewImage
        shimmeringViewImage.isShimmering = true

        shimmeringView2.contentView = UIImageView(image: ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Skeleton.lines, color: .BBVA300), width: Float(shimmeringView2.frame.size.width), height: Float(shimmeringView2.frame.size.height)))
        shimmeringView2.isShimmering = true

        shimmeringView3.contentView = UIImageView(image: ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Skeleton.lines, color: .BBVA300), width: Float(shimmeringView3.frame.size.width), height: Float(shimmeringView3.frame.size.height)))
        shimmeringView3.isShimmering = true

        shimmeringView4.contentView = UIImageView(image: ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Skeleton.lines, color: .BBVA300), width: Float(shimmeringView4.frame.size.width), height: Float(shimmeringView4.frame.size.height)))
        shimmeringView4.isShimmering = true

        shimmeringView5.contentView = UIImageView(image: ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Skeleton.lines, color: .BBVA300), width: Float(shimmeringView5.frame.size.width), height: Float(shimmeringView5.frame.size.height)))
        shimmeringView5.isShimmering = true

        let viewImage2 = UIView(frame: shimmeringViewImage2.frame)
        viewImage2.backgroundColor = .BBVA300
        shimmeringViewImage2.contentView = viewImage2
        shimmeringViewImage2.isShimmering = true

        let viewImage3 = UIView(frame: shimmeringViewImage3.frame)
        viewImage3.backgroundColor = .BBVA300
        shimmeringViewImage3.contentView = viewImage3
        shimmeringViewImage3.isShimmering = true

        shimmeringView6.contentView = UIImageView(image: ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Skeleton.lines, color: .BBVA300), width: Float(shimmeringView6.frame.size.width), height: Float(shimmeringView6.frame.size.height)))
        shimmeringView6.isShimmering = true

        shimmeringView7.contentView = UIImageView(image: ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Skeleton.lines, color: .BBVA300), width: Float(shimmeringView7.frame.size.width), height: Float(shimmeringView7.frame.size.height)))
        shimmeringView7.isShimmering = true

        shimmeringView8.contentView = UIImageView(image: ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Skeleton.lines, color: .BBVA300), width: Float(shimmeringView8.frame.size.width), height: Float(shimmeringView8.frame.size.height)))
        shimmeringView8.isShimmering = true

        shimmeringView9.contentView = UIImageView(image: ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Skeleton.lines, color: .BBVA300), width: Float(shimmeringView9.frame.size.width), height: Float(shimmeringView9.frame.size.height)))
        shimmeringView9.isShimmering = true

        shimmeringView10.contentView = UIImageView(image: ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Skeleton.lines, color: .BBVA300), width: Float(shimmeringView10.frame.size.width), height: Float(shimmeringView10.frame.size.height)))
        shimmeringView10.isShimmering = true

        shimmeringView11.contentView = UIImageView(image: ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Skeleton.lines, color: .BBVA300), width: Float(shimmeringView11.frame.size.width), height: Float(shimmeringView11.frame.size.height)))
        shimmeringView11.isShimmering = true

    }

}
