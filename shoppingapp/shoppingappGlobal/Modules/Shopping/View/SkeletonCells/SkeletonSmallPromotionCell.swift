//
//  SkeletonCell.swift
//  Skeleton
//
//  Created by Gonzalo Nunez on 2/15/17.
//  Copyright © 2017 CocoaPods. All rights reserved.
//

import UIKit
import Shimmer

class SkeletonSmallPromotionCell: UITableViewCell {

    static let identifier = "SkeletonSmallPromotionCell"
    static let height: CGFloat = 159.0

    @IBOutlet var shimmeringView1: FBShimmeringView!
    @IBOutlet var shimmeringViewImage: FBShimmeringView!
    @IBOutlet var shimmeringView2: FBShimmeringView!
    @IBOutlet var shimmeringView3: FBShimmeringView!

    override func awakeFromNib() {

        super.awakeFromNib()

        contentView.backgroundColor = .white

        shimmeringView1.contentView = UIImageView(image: ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Skeleton.lines, color: .BBVA300), width: Float(shimmeringView1.frame.size.width), height: Float(shimmeringView1.frame.size.height)))
        shimmeringView1.isShimmering = true

        let viewImage = UIView(frame: shimmeringViewImage.frame)
        viewImage.backgroundColor = .BBVA300
        shimmeringViewImage.contentView = viewImage
        shimmeringViewImage.isShimmering = true

        shimmeringView2.contentView = UIImageView(image: ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Skeleton.lines, color: .BBVA300), width: Float(shimmeringView2.frame.size.width), height: Float(shimmeringView2.frame.size.height)))
        shimmeringView2.isShimmering = true

        shimmeringView3.contentView = UIImageView(image: ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Skeleton.lines, color: .BBVA300), width: Float(shimmeringView3.frame.size.width), height: Float(shimmeringView3.frame.size.height)))
        shimmeringView3.isShimmering = true

    }

}
