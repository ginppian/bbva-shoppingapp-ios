//
//  SkeletonCell.swift
//  Skeleton
//
//  Created by Gonzalo Nunez on 2/15/17.
//  Copyright © 2017 CocoaPods. All rights reserved.
//

import UIKit
import Shimmer

class SkeletonCarouselPromotionCell: UITableViewCell {

    static let identifier = "SkeletonCarouselPromotionCell"

    @IBOutlet var shimmeringView1: FBShimmeringView!
    @IBOutlet var shimmeringViewImage: FBShimmeringView!
    @IBOutlet var shimmeringView2: FBShimmeringView!
    @IBOutlet var shimmeringView3: FBShimmeringView!
    @IBOutlet var shimmeringView4: FBShimmeringView!
    @IBOutlet var shimmeringView5: FBShimmeringView!

    override func awakeFromNib() {

        super.awakeFromNib()

        contentView.backgroundColor = .BBVA200

        shimmeringView1.contentView = UIImageView(image: ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Skeleton.lines, color: .BBVA300), width: Float(shimmeringView1.frame.size.width), height: Float(shimmeringView1.frame.size.height)))
        shimmeringView1.isShimmering = true

        let viewImage = UIView(frame: shimmeringViewImage.frame)
        viewImage.backgroundColor = .BBVA300
        shimmeringViewImage.contentView = viewImage
        shimmeringViewImage.isShimmering = true

        shimmeringView2.contentView = UIImageView(image: ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Skeleton.lines, color: .BBVA300), width: Float(shimmeringView2.frame.size.width), height: Float(shimmeringView2.frame.size.height)))
        shimmeringView2.isShimmering = true

        shimmeringView3.contentView = UIImageView(image: ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Skeleton.lines, color: .BBVA300), width: Float(shimmeringView3.frame.size.width), height: Float(shimmeringView3.frame.size.height)))
        shimmeringView3.isShimmering = true

        shimmeringView4.contentView = UIImageView(image: ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Skeleton.lines, color: .BBVA300), width: Float(shimmeringView4.frame.size.width), height: Float(shimmeringView4.frame.size.height)))
        shimmeringView4.isShimmering = true

        shimmeringView5.contentView = UIImageView(image: ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Skeleton.lines, color: .BBVA300), width: Float(shimmeringView5.frame.size.width), height: Float(shimmeringView5.frame.size.height)))
        shimmeringView5.isShimmering = true

    }

}
