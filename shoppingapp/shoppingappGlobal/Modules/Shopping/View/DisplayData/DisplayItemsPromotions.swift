//
//  DisplayItemsCards.swift
//  shoppingapp
//
//  Created by ruben.fernandez on 27/9/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation

class DisplayItemsPromotions {

    var shouldShowMorePromotions = false
    var items: [DisplayItemPromotion]!

    init() {
       items = Array()
    }

}

extension DisplayItemsPromotions: Equatable {

    static func == (lhs: DisplayItemsPromotions, rhs: DisplayItemsPromotions) -> Bool {

        return lhs.shouldShowMorePromotions == rhs.shouldShowMorePromotions &&
            lhs.items == rhs.items
    }
}
