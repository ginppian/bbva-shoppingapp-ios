//
//  File.swift
//  shoppingapp
//
//  Created by ruben.fernandez on 8/11/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation
import CoreLocation

struct DisplayItemPromotion {

    typealias EffectiveDateTextInfo = (date: String, isInterval: Bool)

    var commerceName: String?
    var descriptionPromotion: String?
    var dateDiff: String?
    var benefit: String?
    var promotionUrl: String?
    var imageDetail: String?
    var imageList: String?
    var imagePush: String?
    var typeCell: CellPromotionType!
    var categoryType: CategoryType?
    var categoryString: String?
    var promotionBO: PromotionBO?
    var dateDiffPlusDistance: String?
    var usageTypeDescription: String?
    var usageTypePhysical: Bool = false
    var usageTypeOnline: Bool = false

    var categoryImage: String?
    var categoryImageColor: UIColor?

    var itemsTrending = [DisplayItemPromotion]()

    static func generateDisplayItemPromotion(fromPromotion promotion: PromotionBO, withTypeCell typeCell: CellPromotionType, userLocation: GeoLocationBO? = nil) -> DisplayItemPromotion {

        return generateDisplayItemPromotion(fromPromotion: promotion, withTypeCell: typeCell, withCategoryType: .unknown, userLocation: userLocation)
    }

    static func generateDisplayItemPromotion(fromPromotion promotion: PromotionBO, withTypeCell typeCell: CellPromotionType, withCategoryType categoryType: CategoryType, userLocation: GeoLocationBO? = nil) -> DisplayItemPromotion {

        var displayItemPromotion = DisplayItemPromotion()
        displayItemPromotion.commerceName = promotion.commerceInformation.name.uppercased()
        displayItemPromotion.descriptionPromotion = promotion.name
        displayItemPromotion.benefit = promotion.benefit
        displayItemPromotion.promotionUrl = promotion.promotionUrl
        
        let effectiveDateTextCompactSize = typeCell == CellPromotionType.small ? true : false

        let effectiveDateTextInfo = Settings.Promotions.effectiveDateText(forPromotionBO: promotion, compactSize: effectiveDateTextCompactSize)

        let displayItemPromotionDatInfo = getDateDiffAndDateDiffPlus(effectiveDateTextInfo: effectiveDateTextInfo, distance: promotion.stores?.first?.formattedDistance())
        displayItemPromotion.dateDiff = displayItemPromotionDatInfo.dateDiff

        if userLocation != nil {
            displayItemPromotion.dateDiffPlusDistance = displayItemPromotionDatInfo.dateDiffPlusDistance
        }

        displayItemPromotion.typeCell = typeCell

        displayItemPromotion.categoryType = categoryType

        displayItemPromotion.promotionBO = promotion

        displayItemPromotion.imageDetail = promotion.imageDetail
        displayItemPromotion.imageList = promotion.imageList
        displayItemPromotion.imagePush = promotion.imagePush

        return displayItemPromotion

    }

    static func generateDisplayItemPromotion(fromPromotion promotion: PromotionBO, userLocation: GeoLocationBO? = nil) -> DisplayItemPromotion {

        var displayPromotion = generateDisplayItemPromotion(fromPromotion: promotion, withTypeCell: CellPromotionType.header, withCategoryType: .unknown, userLocation: userLocation)

        if let usageTypes = promotion.usageTypes {

            for usage in usageTypes {

                switch usage.usageId {

                case .physical?:
                    displayPromotion.usageTypePhysical = true
                case .online?:
                    displayPromotion.usageTypeOnline = true
                case .unknown?:
                    break
                default:
                    break
                }
            }

            if  displayPromotion.usageTypePhysical && displayPromotion.usageTypeOnline {

                displayPromotion.usageTypeDescription = Localizables.promotions.key_promotions_online_and_facetoface_text
            } else if displayPromotion.usageTypePhysical {

                displayPromotion.usageTypeDescription = Localizables.promotions.key_promotions_facetoface_text
            } else if displayPromotion.usageTypeOnline {

                displayPromotion.usageTypeDescription = Localizables.promotions.key_promotions_online_text
            } else {

                displayPromotion.usageTypeDescription = ""
            }
        }

        return displayPromotion
    }

    static func generateDisplayItemPromotion(withCategoryString categoryString: String, withCategoryType categoryType: CategoryType) -> DisplayItemPromotion {

        var displayItemPromotion = DisplayItemPromotion()
        displayItemPromotion.categoryType = categoryType
        displayItemPromotion.categoryString = categoryString
        displayItemPromotion.typeCell = .header
        displayItemPromotion.categoryImage = categoryType.imageNamed()
        displayItemPromotion.categoryImageColor = .BBVA400

        return displayItemPromotion

    }

    static func generateDisplayItemPromotionTrending(withTrendingPromotions trendingPromotions: PromotionsBO) -> DisplayItemPromotion {

        var displayItemPromotion = DisplayItemPromotion()
        displayItemPromotion.categoryString = TrendingType.hot.rawValue
        displayItemPromotion.typeCell = .trending

        for promotion in trendingPromotions.promotions {

            var displayItemPromotionTrending = DisplayItemPromotion()
            displayItemPromotionTrending.commerceName = promotion.commerceInformation.name.uppercased()
            displayItemPromotionTrending.descriptionPromotion = promotion.name
            displayItemPromotionTrending.benefit = promotion.benefit
            displayItemPromotionTrending.promotionUrl = promotion.promotionUrl
            displayItemPromotionTrending.typeCell = .trending

            let effectiveDateTextInfo = Settings.Promotions.effectiveDateText(forPromotionBO: promotion, compactSize: false)
            displayItemPromotionTrending.dateDiff = effectiveDateTextInfo?.date

            //trending promotions never show distance
            displayItemPromotionTrending.dateDiffPlusDistance = nil

            displayItemPromotionTrending.promotionBO = promotion

            displayItemPromotionTrending.imageDetail = promotion.imageDetail
            displayItemPromotionTrending.imageList = promotion.imageList
            displayItemPromotionTrending.imagePush = promotion.imagePush

            displayItemPromotion.itemsTrending.append(displayItemPromotionTrending)
        }

        return displayItemPromotion

    }

    static func generateDisplayItemPromotionInMap(withPromotion promotion: PromotionBO, withStoreLatitude storeLatitude: Double, andStoreLongitude storeLongitude: Double, andWithUserLatitude userLatitude: Double?, andUserLongitude userLongitude: Double?) -> DisplayItemPromotion {

        var displayItemPromotion = DisplayItemPromotion()
        displayItemPromotion.commerceName = promotion.commerceInformation.name.uppercased()
        displayItemPromotion.descriptionPromotion = promotion.name
        displayItemPromotion.benefit = promotion.benefit
        displayItemPromotion.promotionBO = promotion
        displayItemPromotion.promotionUrl = promotion.promotionUrl
        
        displayItemPromotion.imageDetail = promotion.imageDetail
        displayItemPromotion.imageList = promotion.imageList
        displayItemPromotion.imagePush = promotion.imagePush

        let effectiveDateTextInfo = Settings.Promotions.effectiveDateText(forPromotionBO: promotion, compactSize: true)

       let distance = calculateDistanteBetween(storeLatitue: storeLatitude, andStoreLongitude: storeLongitude, withUserLatiude: userLatitude, andUserLongitude: userLongitude)

        let displayItemPromotionDatInfo = getDateDiffAndDateDiffPlus(effectiveDateTextInfo: effectiveDateTextInfo, distance: distance)
        displayItemPromotion.dateDiff = displayItemPromotionDatInfo.dateDiff
        displayItemPromotion.dateDiffPlusDistance = displayItemPromotionDatInfo.dateDiffPlusDistance

        return displayItemPromotion
    }

    static func calculateDistanteBetween(storeLatitue: Double, andStoreLongitude storeLongitude: Double, withUserLatiude userLatitude: Double?, andUserLongitude userLongitude: Double?) -> String? {

        guard let userLatitude = userLatitude, let userLongitude = userLongitude else {
            return nil
        }

        let distanceInMeters = DistanceTools.calculateDistanceInMetersBetween(latitue: storeLatitue, longitude: storeLongitude, andLatiudeToCompare: userLatitude, andLongitudeToCompare: userLongitude)

        let distanceFormatted: String?

        let numberFormatter = NumberFormatter()
        numberFormatter.minimumFractionDigits = 0

        if distanceInMeters >= Double(DistanceConstants.meters_to_kilometers) {

            numberFormatter.maximumFractionDigits = 1

            let kilomteres = numberFormatter.string(from: NSDecimalNumber(value: distanceInMeters / Double(DistanceConstants.meters_to_kilometers)))!

            distanceFormatted = String(format: "%@ %@", kilomteres, Localizables.promotions.key_distance_kilometers)

        } else {

            numberFormatter.maximumFractionDigits = 0

            let meters = numberFormatter.string(from: NSDecimalNumber(value: distanceInMeters))!

            distanceFormatted = String(format: "%@ %@", meters, Localizables.promotions.key_distance_meters)

        }

        return distanceFormatted

    }

    private static func getDateDiffAndDateDiffPlus(effectiveDateTextInfo: EffectiveDateTextInfo?, distance: String?) ->(dateDiff: String, dateDiffPlusDistance: String?) {

        var dateDiff = String()
        var dateDiffPlusDistance: String?

        if let dateTextInfo = effectiveDateTextInfo {
            dateDiff = dateTextInfo.date
            if let distance = distance {
                dateDiffPlusDistance = Settings.Promotions.dateDiffPlusDistance(distance: distance, effectiveDateText: dateDiff, isIntervalDate: dateTextInfo.isInterval)
            }
        } else {
            if let distance = distance {
                dateDiffPlusDistance = distance
            }
        }

        return (dateDiff, dateDiffPlusDistance)
    }

}

enum CellPromotionType {
    case trending
    case high
    case small
    case header
}

extension DisplayItemPromotion: Equatable {

    public static func == (lhs: DisplayItemPromotion, rhs: DisplayItemPromotion) -> Bool {

        return (lhs.commerceName == rhs.commerceName &&
            lhs.descriptionPromotion == rhs.descriptionPromotion &&
            lhs.dateDiff == rhs.dateDiff &&
            lhs.benefit == rhs.benefit &&
            lhs.promotionUrl == rhs.promotionUrl &&
            lhs.imageDetail == rhs.imageDetail &&
            lhs.imageList == rhs.imageList &&
            lhs.imagePush == rhs.imagePush &&
            lhs.typeCell == rhs.typeCell &&
            lhs.categoryType == rhs.categoryType &&
            lhs.categoryString == rhs.categoryString &&
            lhs.dateDiffPlusDistance == rhs.dateDiffPlusDistance &&
            lhs.categoryImage == rhs.categoryImage &&
            lhs.categoryImageColor == rhs.categoryImageColor &&
            lhs.itemsTrending == rhs.itemsTrending)
    }
}
