//
//  ShoppingPageReactionA.swift
//  shoppingappMX
//
//  Created by daniel.petrovic on 26/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

class ShoppingPageReactionA: ShoppingPageReaction {

    static let EVENT_NAV_TO_SECOND_SCREEN: ActionSpec<Model> = ActionSpec<Model>(id: "EVENT_NAV_TO_SECOND_SCREEN")
    static let EVENT_NAV_TO_LOCATION_SETTING_HELP: ActionSpec<Void> = ActionSpec<Void>(id: "EVENT_NAV_TO_LOCATION_SETTING_HELP")

    override func configure() {
        super.configure()
        navigateSecondScreenFromShopping()
        navigateToLocationSettingHelp()
    }

    func navigateSecondScreenFromShopping() {

        _ = when(id: ShoppingPageReactionA.ROUTER_TAG, type: Model.self)
            .doAction(actionSpec: ShoppingPageReactionA.EVENT_NAV_TO_SECOND_SCREEN)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, model in

                router.navigateSecondScreenFromShopping(withModel: model!)
            }
    }

    func navigateToLocationSettingHelp() {

        _ = when(id: ShoppingPageReactionA.ROUTER_TAG, type: Void.self)
            .doAction(actionSpec: ShoppingPageReactionA.EVENT_NAV_TO_LOCATION_SETTING_HELP)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, _ in

                router.navigateToLocationSettingHelp()
            }
    }
}
