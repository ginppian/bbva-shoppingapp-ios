//
//  ShoppingPageReaction.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 26/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

class ShoppingPageReaction: PageReaction {

    static let ROUTER_TAG: String = "shopping-tag"

    static let EVENT_NAV_TO_NEXT_SCREEN: ActionSpec<Model> = ActionSpec<Model>(id: "EVENT_NAV_TO_NEXT_SCREEN")
    static let EVENT_NAV_TO_DETAIL_SCREEN: ActionSpec<Model> = ActionSpec<Model>(id: "EVENT_NAV_TO_DETAIL_SCREEN")
    static let EVENT_NAV_TO_FILTER_SCREEN: ActionSpec<Model> = ActionSpec<Model>(id: "EVENT_NAV_TO_FILTER_SCREEN")

    override func configure() {

        navigateFirstScreenFromShopping()
        navigateToDetailScreen()
        navigateToFilterScreen()
    }

    func navigateFirstScreenFromShopping() {

        _ = when(id: ShoppingPageReaction.ROUTER_TAG, type: Model.self)
            .doAction(actionSpec: ShoppingPageReaction.EVENT_NAV_TO_NEXT_SCREEN)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, model in

                if let model = model {
                    router.navigateFirstScreenFromShopping(withModel: model)
                }

            }
    }

    func navigateToDetailScreen() {

        _ = when(id: ShoppingPageReaction.ROUTER_TAG, type: Model.self)
        .doAction(actionSpec: ShoppingPageReaction.EVENT_NAV_TO_DETAIL_SCREEN)
        .then()
        .inside(componentID: RouterApp.ID, component: RouterApp.self)
        .doReaction { router, model in

            if let model = model {
                router.navigateToShoppingDetail(withModel: model)
            }

        }
    }

    func navigateToFilterScreen() {

        _ = when(id: ShoppingPageReaction.ROUTER_TAG, type: Model.self)
            .doAction(actionSpec: ShoppingPageReaction.EVENT_NAV_TO_FILTER_SCREEN)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, model in

                if let model = model {
                    router.navigateToShoppingFilter(withModel: model)
                }

            }
    }

}
