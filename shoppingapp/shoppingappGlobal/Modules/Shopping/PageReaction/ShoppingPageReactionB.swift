//
//  ShoppingPageReactionB.swift
//  shoppingapp
//
//  Created by jesus.martinez on 14/6/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

class ShoppingPageReactionB: ShoppingPageReactionA {

    static let EVENT_NAV_TO_FAVORITE_CATEGORIES: ActionSpec<Model> = ActionSpec<Model>(id: "EVENT_NAV_TO_FAVORITE_CATEGORIES")

    override func configure() {
        super.configure()

        reactCards()
        navigateToFavoriteCategoriesFromShopping()
        newFavoriteCategories()
        favoriteCategoriesDoNotChange()
        filterPageWasDismissed()
    }

    func reactCards() {

        _ = reactTo(channel: PageReactionConstants.CHANNEL_CARDS)?
            .then()
            .inside(componentID: ShoppingPageReaction.ROUTER_TAG, component: ShoppingPresenterB<ShoppingViewControllerB>.self)
            .doReaction { component, param in

                if let param = param {
                    component.receive(model: param)
                }
            }
    }

    func newFavoriteCategories() {

        _ = reactTo(channel: SelectFavoriteCategoryPageReaction.INFO_CONFIRMATION_CHANNEL)?
            .then()
            .inside(componentID: ShoppingPageReaction.ROUTER_TAG, component: ShoppingPresenterB<ShoppingViewControllerB>.self)
            .doReactionAndClearChannel { component, _ in

                component.favoriteCategoriesChanged()
            }
    }

    func favoriteCategoriesDoNotChange() {

        _ = reactTo(channel: SelectFavoriteCategoryPageReaction.INFO_CATEGORIES_NOT_CHANGE_CHANNEL)?
            .then()
            .inside(componentID: ShoppingPageReaction.ROUTER_TAG, component: ShoppingPresenterB<ShoppingViewControllerB>.self)
            .doReactionAndClearChannel { component, _ in

                component.favoriteCategoriesNotChanged()
            }
    }

    func navigateToFavoriteCategoriesFromShopping() {

        _ = when(id: ShoppingPageReactionB.ROUTER_TAG, type: Model.self)
            .doAction(actionSpec: ShoppingPageReactionB.EVENT_NAV_TO_FAVORITE_CATEGORIES)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, model in

                router.navigateFavoriteCategoriesFromShopping(withModel: model!)
            }

    }

    func filterPageWasDismissed() {

        _ = when(id: ShoppingFilterPageReaction.ROUTER_TAG, type: Void.self)
            .doAction(actionSpec: ShoppingFilterPageReaction.EVENT_FILTERVIEW_DISMISSED)
            .then()
            .inside(componentID: ShoppingPageReaction.ROUTER_TAG, component: ShoppingPresenterB<ShoppingViewControllerB>.self)
            .doReaction { component, _ in

                component.checkCountAccessInNextApparition()
                
        }

    }
}
