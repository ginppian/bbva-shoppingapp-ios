//
//  TransactionsListInteractor.swift
//  shoppingapp
//
//  Created by jesus.martinez on 7/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import RxSwift

class TransactionsListInteractor {
    let disposeBag = DisposeBag()
    var transactionDataManager = TransactionsDataManager.sharedInstance
    
    deinit {
        DLog(message: "Deinit TransactionsListInteractor")
    }
}

// MARK: TransactionListInteractorProtocol
extension TransactionsListInteractor: TransactionListInteractorProtocol {
    
    func provideTransactions(fromNextPage nextPage: String) -> Observable <ModelBO> {
        
        let transactions = transactionDataManager
        
        return Observable.create {observer in
            transactions.provideTransactions(forNextPage: nextPage).subscribe(
                onNext: { result in
                    if let transactionsEntity = result as? TransactionsEntity {
                        observer.onNext(TransactionsBO(transactionsEntity: transactionsEntity))
                        observer.onCompleted()
                    } else {
                        DLog(message: "Incorrect TransactionsEntity")
                    }
                },
                onError: { error in
                    observer.onError(Tools.evaluateError(with: error))
                }).addDisposableTo(self.disposeBag)
            return Disposables.create()
        }
    }

    func provideTransaction(withTransactionFilterTransport transactionFilterTransport: TransactionFilterTransport?, forCardId cardId: String) -> Observable<ModelBO> {

        let transactions = transactionDataManager
        return Observable.create { observer in

            transactions.provideTransactions(withTransactionFilterTransport: transactionFilterTransport, forCardId: cardId).subscribe(
                onNext: { result in
                    if let transactionsEntity = result as? TransactionsEntity {
                        observer.onNext(TransactionsBO(transactionsEntity: transactionsEntity))
                        observer.onCompleted()
                    } else {
                        DLog(message: "Incorrect TransactionsEntity")
                    }
                },
                onError: { error in
                    observer.onError(Tools.evaluateError(with: error))
                }).addDisposableTo(self.disposeBag)
            return Disposables.create()
        }
    }
}
