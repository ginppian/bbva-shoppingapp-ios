//
//  TransactionsListInteractorA.swift
//  shoppingappMX
//
//  Created by Armando Vazquez on 8/24/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import RxSwift

final class TransactionsListInteractorA: TransactionsListInteractor {
    
    let transactionsListRangesDebit = 2
    let pageSize = 20
    
    deinit {
        DLog(message: "TransactionsListInteractorA Deinit")
    }
}

// MARK: TransactionListInteractorProtocolA

extension TransactionsListInteractorA: TransactionListInteractorProtocolA {
    
    func provideTransactionsForAccount(filteredBy transactionTransport: TransactionFilterTransport?, with accountId: String) -> Observable<ModelBO> {
        
        let filter: TransactionFilterTransport = transactionTransport ?? TransactionFilterTransport()
        
        if transactionTransport == nil {
            
            let date = Date()
            let calendar = Calendar.current
            
            var components = calendar.dateComponents([.day, .month, .year], from: date)
            let monthsBack = transactionsListRangesDebit
            let rangeMonth = (components.month ?? monthsBack) - monthsBack
            components.month = rangeMonth
            
            let threeMonthsAgo = calendar.date(from: components)?.string(format: Date.DATE_FORMAT_SERVER)
            let currentDate = date.string(format: Date.DATE_FORMAT_SERVER)
            
            filter.fromDateText = threeMonthsAgo
            filter.toDateText = currentDate
            filter.pageSize = pageSize
            
        }
        
        let transactions = transactionDataManager
        
        return Observable.create { observer in
            transactions.provideAccountTransactions(filteredBy: filter, for: accountId).subscribe(
                onNext: { result in
                    if let transactionsEntity = result as? TransactionsEntity {
                        observer.onNext(TransactionsBO(transactionsEntity: transactionsEntity))
                        observer.onCompleted()
                    } else {
                        DLog(message: "Incorrect TransactionsEntity")
                    }
                },
                onError: { error in
                    observer.onError(Tools.evaluateError(with: error))
                }).addDisposableTo(self.disposeBag)
            return Disposables.create()
        }
    }

}
