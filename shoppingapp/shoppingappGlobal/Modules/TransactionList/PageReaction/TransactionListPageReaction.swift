//
//  TransactionListPageReaction.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 26/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

class TransactionListPageReaction: PageReaction {

    static let ROUTER_TAG: String = "transactions-list-tag"

    static let EVENT_NAV_TO_NEXT_SCREEN: ActionSpec<Model> = ActionSpec<Model>(id: "EVENT_NAV_TO_NEXT_SCREEN")

    static let EVENT_NAV_TO_TRANSACTION_DETAIL_SCREEN: ActionSpec<Model> = ActionSpec<Model>(id: "EVENT_NAV_TO_TRANSACTION_DETAIL_SCREEN")

    override func configure() {

        navigateSecondScreenFromTransactionList()
        navigateFirstScreenFromTransactionList()
        reloadFiltersFromTransactionsFilter()
    }

    func navigateSecondScreenFromTransactionList() {

        _ = when(id: TransactionListPageReaction.ROUTER_TAG, type: Model.self)
            .doAction(actionSpec: TransactionListPageReaction.EVENT_NAV_TO_TRANSACTION_DETAIL_SCREEN)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, model in

                router.navigateSecondScreenFromTransactionList(withModel: model!)

            }

    }

    func navigateFirstScreenFromTransactionList() {

        _ = when(id: TransactionListPageReaction.ROUTER_TAG, type: Model.self)
            .doAction(actionSpec: TransactionListPageReaction.EVENT_NAV_TO_NEXT_SCREEN)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, model in

                router.navigateFirstScreenFromTransactionList(withModel: model!)

            }

    }

    func reloadFiltersFromTransactionsFilter() {

        _ = reactTo(channel: TransactionsFilterPageReaction.CHANNEL_TRANSACTIONS_FILTER)?
            .then()
            .inside(componentID: TransactionListPageReaction.ROUTER_TAG, component: TransactionsListPresenter<TransactionListViewController>.self)
            .doReactionAndClearChannel { component, param in

                if let model = param {
                    component.reloadTransactionsList(model: model)
                }
            }
    }
}
