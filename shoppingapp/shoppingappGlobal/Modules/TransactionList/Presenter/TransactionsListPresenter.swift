//
//  TransactionsListPresenter.swift
//  shoppingapp
//
//  Created by jesus.martinez on 7/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit
import RxSwift
import CellsNativeComponents

class TransactionsListPresenter<T: TransactionListViewProtocol>: BasePresenter<T> {

    let FILTER_TYPE_CONCEPT: String = "concept"
    let FILTER_TYPE_DATE: String = "date"
    let FILTER_TYPE_AMOUNT: String = "amount"
    let FILTER_TYPE: String = "type"
    let FILTER_TYPE_REMOVE_ALL: String = "removeAll"

    var interactor: TransactionListInteractorProtocol!
    var transactionsBO: TransactionsBO?
    var displayData = TransactionsDisplayData()
    var isLoadingNextPage = false
    let disposeBag = DisposeBag()
    var formatter: AmountFormatter?
    var currencyBO: CurrencyBO!
    var cardBO: CardBO?
    var transactionFilterTransport: TransactionFilterTransport?
    var shouldLoadFilters = true

    required init() {
        super.init(tag: TransactionListPageReaction.ROUTER_TAG)
        getCurrencyBO()
    }

    override init(busManager: BusManager) {

        super.init(busManager: busManager)

        getCurrencyBO()

        routerTag = TransactionListPageReaction.ROUTER_TAG
    }

    func loadDataFromService() {
        
        guard let cardBO = cardBO else {
            return
        }
        
        interactor.provideTransaction(withTransactionFilterTransport: transactionFilterTransport, forCardId: cardBO.cardId).subscribe(

            onNext: { [weak self] result in
                self?.manageOKTransactionService(with: result)
            },
            onError: { [weak self] error in
                self?.manageBadTransactionService(with: error)
            }
        ).addDisposableTo(disposeBag)
    }
    
    func getCurrencyBO() {

        _ = ConfigurationDataManager.sharedInstance.provideMainCurrency().subscribe(

            onNext: { [weak self] result in

                if let currencyEntity = result as? CurrencyEntity {
                    let currencyBO = Currencies.currency(forCode: currencyEntity.code)
                    self?.currencyBO = currencyBO
                    self?.formatter = AmountFormatter(codeCurrency: currencyBO.code)
                }
            })
    }

    func generateFiltersArray(withTransactionFilterTransport transactionFilterTransport: TransactionFilterTransport) -> [FilterScrollDisplayData] {

        var filters: [FilterScrollDisplayData] = []

        if let conceptText = transactionFilterTransport.conceptText, !conceptText.isEmpty {

            filters.append(FilterScrollDisplayData(title: conceptText,
                                                   filterType: FILTER_TYPE_CONCEPT,
                                                   isRemoveAllFiltersButton: false,
                                                   labelAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_CONCEPT_FILTER_DETAIL_LABEL_ID,
                                                   buttonAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_CONCEPT_FILTER_DETAIL_CLOSE_BUTTON_ID))
        }

        if let fromDateString = transactionFilterTransport.fromDateText, !fromDateString.isEmpty {

            let format = Date.DATE_FORMAT_SERVER
            let fromDate = Date.date(fromLocalTimeZoneString: fromDateString, withFormat: format)
            var toDate: Date?

            if transactionFilterTransport.toDateText == nil {
                toDate = Date()
            } else {
                toDate = Date.date(fromLocalTimeZoneString: transactionFilterTransport.toDateText!, withFormat: format)
            }

            if let fromDate = fromDate, let toDate = toDate {

                var fromDateText = fromDate.string(format: Date.DATE_FORMAT_DAY_MONTH_YEAR_SHORT).capitalized
                var toDateText = toDate.string(format: Date.DATE_FORMAT_DAY_MONTH_YEAR_SHORT).capitalized

                let calendar = NSCalendar.current
                let isYesterdayFrom = calendar.isDateInYesterday(fromDate)
                let isTodayFrom = calendar.isDateInToday(fromDate)

                if isTodayFrom {
                    fromDateText = Localizables.transactions.key_card_movement_today_text
                } else if isYesterdayFrom {
                    fromDateText = Localizables.transactions.key_card_movement_yesterday_text
                }

                let isYesterdayTo = calendar.isDateInYesterday(toDate)
                let isTodayTo = calendar.isDateInToday(toDate)

                if isTodayTo {
                    toDateText = Localizables.transactions.key_card_movement_today_text
                } else if isYesterdayTo {
                    toDateText = Localizables.transactions.key_card_movement_yesterday_text
                }

                filters.append(FilterScrollDisplayData(title: fromDateText + " - " + toDateText,
                                                       filterType: FILTER_TYPE_DATE,
                                                       isRemoveAllFiltersButton: false,
                                                       labelAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_DATE_FILTER_DETAIL_LABEL_ID,
                                                       buttonAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_DATE_FILTER_DETAIL_CLOSE_BUTTON_ID))
            }
        } else if let toDateString = transactionFilterTransport.toDateText, !toDateString.isEmpty {

            let format = Date.DATE_FORMAT_SERVER
            let date = Date.date(fromLocalTimeZoneString: toDateString, withFormat: format)
            var toDateText = date?.string(format: Date.DATE_FORMAT_DAY_MONTH_YEAR_SHORT).capitalized

            let calendar = NSCalendar.current
            let isYesterday = calendar.isDateInYesterday(date!)

            if isYesterday {
                toDateText = Localizables.transactions.key_card_movement_yesterday_text.lowercased()
            }

            if let toDateText = toDateText {

                filters.append(FilterScrollDisplayData(title: Localizables.transactions.key_transactions_to_text + " " + toDateText,
                                                       filterType: FILTER_TYPE_DATE,
                                                       isRemoveAllFiltersButton: false,
                                                       labelAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_DATE_FILTER_DETAIL_LABEL_ID,
                                                       buttonAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_DATE_FILTER_DETAIL_CLOSE_BUTTON_ID))
            }
        }

        if let fromAmountText = transactionFilterTransport.fromAmountText, !fromAmountText.isEmpty,
            let toAmountText = transactionFilterTransport.toAmountText, !toAmountText.isEmpty {

            let fromAmountTextFormatted = formatter?.format(amount: NSDecimalNumber(string: fromAmountText, locale: Locale.current), withSpace: false)
            let toAmountTextFormatted = formatter?.format(amount: NSDecimalNumber(string: toAmountText, locale: Locale.current), withSpace: false)

            filters.append(FilterScrollDisplayData(title: fromAmountTextFormatted! + " - " + toAmountTextFormatted!,
                                                   filterType: FILTER_TYPE_AMOUNT,
                                                   isRemoveAllFiltersButton: false,
                                                   labelAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_AMOUNT_FILTER_DETAIL_LABEL_ID,
                                                   buttonAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_AMOUNT_FILTER_DETAIL_CLOSE_BUTTON_ID))
        } else if let fromAmountText = transactionFilterTransport.fromAmountText, !fromAmountText.isEmpty {

            let fromAmountTextFormatted = formatter?.format(amount: NSDecimalNumber(string: fromAmountText, locale: Locale.current), withSpace: false)
            filters.append(FilterScrollDisplayData(title: Localizables.transactions.key_transactions_from_text + " " + fromAmountTextFormatted!,
                                                   filterType: FILTER_TYPE_AMOUNT,
                                                   isRemoveAllFiltersButton: false,
                                                   labelAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_AMOUNT_FILTER_DETAIL_LABEL_ID,
                                                   buttonAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_AMOUNT_FILTER_DETAIL_CLOSE_BUTTON_ID))

        } else if let toAmountText = transactionFilterTransport.toAmountText, !toAmountText.isEmpty {

            let toAmountTextFormatted = formatter?.format(amount: NSDecimalNumber(string: toAmountText, locale: Locale.current), withSpace: false)
            filters.append(FilterScrollDisplayData(title: Localizables.transactions.key_transactions_to_text + " " + toAmountTextFormatted!,
                                                   filterType: FILTER_TYPE_AMOUNT,
                                                   isRemoveAllFiltersButton: false,
                                                   labelAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_AMOUNT_FILTER_DETAIL_LABEL_ID,
                                                   buttonAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_AMOUNT_FILTER_DETAIL_CLOSE_BUTTON_ID))
        }

        if transactionFilterTransport.filtertype == .income {
            filters.append(FilterScrollDisplayData(title: Localizables.transactions.key_transactions_income_text,
                                                   filterType: FILTER_TYPE,
                                                   isRemoveAllFiltersButton: false,
                                                   labelAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_TRANSACTION_TYPE_FILTER_DETAIL_LABEL_ID,
                                                   buttonAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_TRANSACTION_TYPE_FILTER_DETAIL_CLOSE_BUTTON_ID))
        } else if transactionFilterTransport.filtertype == .expense {
            filters.append(FilterScrollDisplayData(title: Localizables.transactions.key_transactions_expenses_text,
                                                   filterType: FILTER_TYPE,
                                                   isRemoveAllFiltersButton: false,
                                                   labelAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_TRANSACTION_TYPE_FILTER_DETAIL_LABEL_ID,
                                                   buttonAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_TRANSACTION_TYPE_FILTER_DETAIL_CLOSE_BUTTON_ID))
        }

        return filters
    }

    func removeFilterInTransactionFilterTransport(forFilter filter: (FilterScrollDisplayData)) -> Bool {

        if transactionFilterTransport == nil {
            return false
        }

        switch filter.filterType {
        case FILTER_TYPE_CONCEPT:
            if transactionFilterTransport?.conceptText != nil {
                transactionFilterTransport?.conceptText = nil
            } else {
                return false
            }
        case FILTER_TYPE_DATE:
            if transactionFilterTransport?.toDateText != nil || transactionFilterTransport?.fromDateText != nil {
                transactionFilterTransport?.toDateText = nil
                transactionFilterTransport?.fromDateText = nil
            } else {
                return false
            }
        case FILTER_TYPE_AMOUNT:
            if transactionFilterTransport?.toAmountText != nil || transactionFilterTransport?.fromAmountText != nil {
                transactionFilterTransport?.toAmountText = nil
                transactionFilterTransport?.fromAmountText = nil
            } else {
                return false
            }
        case FILTER_TYPE:
            if transactionFilterTransport?.filtertype != TransactionFilterType.all {
                transactionFilterTransport?.filtertype = TransactionFilterType.all
            } else {
                return false
            }
        default:
            return false
        }

        return true
    }

    func generateFilterRemoveAll() -> [FilterScrollDisplayData] {

        var filters: [FilterScrollDisplayData] = []

        filters.append(FilterScrollDisplayData(title: Localizables.transactions.key_transactions_deleteall_text,
                                               filterType: FILTER_TYPE_REMOVE_ALL,
                                               isRemoveAllFiltersButton: true,
                                               labelAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_REMOVE_ALL_FILTERS_DETAIL_LABEL_ID,
                                               buttonAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_REMOVE_ALL_FILTERS_DETAIL_CLOSE_BUTTON_ID))

        return filters
    }

    func cleanTransactionFilterTransportValues() {

        transactionFilterTransport?.conceptText = nil
        transactionFilterTransport?.fromDateText = nil
        transactionFilterTransport?.toDateText = nil
        transactionFilterTransport?.filtertype = .all
        transactionFilterTransport?.fromAmountText = nil
        transactionFilterTransport?.toAmountText = nil
    }

    override func setModel(model: Model) {

        if let cardBO = model as? CardBO {
            self.cardBO = cardBO
        } else if let newTransactionsFilterTransport = model as? TransactionFilterTransport {

            if let beforeTransactionsFilterTransport = transactionFilterTransport {
                shouldLoadFilters = beforeTransactionsFilterTransport != newTransactionsFilterTransport
            }

            transactionFilterTransport = newTransactionsFilterTransport
            cardBO = transactionFilterTransport?.cardBO

        }
    }
    
    // MARK: Utils
    
    func manageOKTransactionService(with response: ModelBO) {
        if let responseTransactionsBO = response as? TransactionsBO {
            displayData = TransactionsDisplayData()
            transactionsBO = responseTransactionsBO
            managePagination()
            showTransactions()
        }
    }
    
    func manageBadTransactionService(with response: Error = ServiceError.GenericErrorBO(error: ErrorBO(message: Localizables.common.key_common_error_text, errorType: .warning, allowCancel: false))) {
        
        transactionsBO = nil
        view?.showErrorContent()
        
        let evaluate: ServiceError = (response as? ServiceError)!
        
        switch evaluate {
        case .GenericErrorBO(let genericErrorBO):
            errorBO = genericErrorBO
            checkError()
        default:
            break
        }
    }

}

extension TransactionsListPresenter: TransactionListPresenterProtocol {
    
    func viewWillAppearAfterDismiss() {
        
        view?.sendOmnitureScreen()
    }

    func checkFilterData() {

        if transactionFilterTransport != nil {

            if shouldLoadFilters {

                view?.showSkeleton()

                let filters = generateFiltersArray(withTransactionFilterTransport: transactionFilterTransport!)

                if filters.isEmpty {
                    view?.hideFiltersScrollView()
                } else {
                    view?.showFiltersScrollView(withFilters: filters)
                }

                loadDataFromService()
                shouldLoadFilters = false
            }
        } else {
            view?.hideFiltersScrollView()
        }
    }

    func loadData() {

        if viewDidLoad == false {

            if self.transactionsBO != nil {
                self.managePagination()
                showTransactions()
            } else {

                if self.cardBO != nil {

                    view?.showSkeleton()

                    loadDataFromService()

                }

            }

        }

        view?.sendOmnitureScreen()

        viewDidLoad = true
    }

    func managePagination() {

        if let nextPage = transactionsBO?.pagination?.links?.next {
            if nextPage.isEmpty {
                view?.updateForNoMoreContent()
            } else {
                view?.updateForMoreContent()
            }
        } else {
            view?.updateForNoMoreContent()
        }

    }

    func showTransactions() {

        if let transactions = self.transactionsBO {

            if transactions.status == ConstantsHTTPCodes.NO_CONTENT {

                if let transactionFilterTransport = transactionFilterTransport, !transactionFilterTransport.isUnitialized() {

                    view?.showNoResults()
                } else {

                    view?.showNoContent()
                }
            } else if transactions.status == ConstantsHTTPCodes.STATUS_OK {

                view?.hideSkeleton()
                
                if transactions.transactions.count != 0 {
                    
                    displayData.add(transactions: transactions.transactions)
                    view?.showTransactions(transactionsDisplayData: self.displayData)
                    
                } else {
                    
                    view?.showNoContent()
                }

            } else {
                DLog(message: "Invalid status")
            }
        } else {
            DLog(message: "No transactionsBO")
        }
    }

    func loadDataNextPage() {

        if isLoadingNextPage == false, let links = transactionsBO?.pagination?.links, let next = links.next, !next.isEmpty {

            view?.showNextPageAnimation()

            self.isLoadingNextPage = true

            let nextURL = links.generateNextURL(isFinancial: true)

            self.interactor.provideTransactions(fromNextPage: nextURL).subscribe(
                onNext: { [weak self] result in
                    guard let `self` = self else {
                        return
                    }
                    self.isLoadingNextPage = false
                    self.view?.hideNextPageAnimation()

                    if let transactionsBO = result as? TransactionsBO {

                        self.transactionsBO?.pagination = transactionsBO.pagination
                        self.managePagination()

                        if transactionsBO.status == ConstantsHTTPCodes.STATUS_OK {
                            self.displayData.add(transactions: transactionsBO.transactions)
                            self.view?.showTransactions(transactionsDisplayData: self.displayData)
                        }
                    }
                },
                onError: { [weak self] error in
                    guard let `self` = self else {
                        return
                    }
                    self.isLoadingNextPage = false

                    self.view?.updateForNoMoreContent()
                    self.view?.hideNextPageAnimation()

                    DLog(message: "\(error)")
                    let evaluate: ServiceError = (error as? ServiceError)!

                    switch evaluate {

                    case .GenericErrorBO(let errorBO):
                        self.errorBO = errorBO
                        self.checkError()
                    default:
                        break
                    }
            }).addDisposableTo(self.disposeBag)
        }
    }

    func presentTransactionsFilter() {

        if let cardBO = self.cardBO {
            busManager.navigateScreen(tag: routerTag, TransactionListPageReaction.EVENT_NAV_TO_NEXT_SCREEN, cardBO)
        }

    }

    func removeFilter(filter: FilterScrollDisplayData, inFiltersScrollView filtersScrollView: FiltersScrollView ) {

        if self.removeFilterInTransactionFilterTransport(forFilter: filter) {

            view?.showSkeleton()

            let filters = generateFiltersArray(withTransactionFilterTransport: transactionFilterTransport!)
            if filters.isEmpty {
                self.cleanTransactionFilterTransportValues()
                view?.hideFiltersScrollView()
            } else {
                view?.showFiltersScrollView(withFilters: filters)
            }

            loadDataFromService()
        }
    }

    func removeFilterLonglyPushed(inFiltersScrollView filtersScrollView: FiltersScrollView) {

        let filters = generateFilterRemoveAll()
        view?.showFiltersScrollView(withFilters: filters)
    }

    func removeAllFilters(inFiltersScrollView filtersScrollView: FiltersScrollView) {

        view?.showSkeleton()

        self.cleanTransactionFilterTransportValues()
        view?.hideFiltersScrollView()

        loadDataFromService()
    }

    func goToTransactionDetail(withTransactionDisplayItem transactionDisplayItem: TransactionDisplayItem) {

        let transactionsTransport = TransactionsTransport()
        transactionsTransport.transactionDisplayItem = transactionDisplayItem
        transactionsTransport.cardBO = cardBO

        busManager.navigateScreen(tag: routerTag, TransactionListPageReaction.EVENT_NAV_TO_TRANSACTION_DETAIL_SCREEN, transactionsTransport)
    }

    func retryButtonPressed() {

        view?.showSkeleton()
        loadDataFromService()
    }
}

// MARK: - Reactions
extension TransactionsListPresenter {

    func reloadTransactionsList(model: Model) {

        setModel(model: model)
        loadData()
        checkFilterData()
    }

}
