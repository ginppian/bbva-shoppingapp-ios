//
//  TransactionListPresenterA.swift
//  shoppingappMX
//
//  Created by Armando Vazquez on 8/24/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

final class TransactionsListPresenterA<T: TransactionListViewProtocol>: TransactionsListPresenter<T> {
    
    override func loadDataFromService() {
        
        switch cardBO?.cardType.id {
        case .debit_card?:
            guard let accountID = cardBO?.accountID else {
                DLog(message: "Bad CardBO")
                manageBadTransactionService()
                return
        }
            (interactor as! TransactionListInteractorProtocolA).provideTransactionsForAccount(filteredBy: transactionFilterTransport, with: accountID).subscribe(
                onNext: { [weak self] result in
                    self?.manageOKTransactionService(with: result)
                },
                onError: { [weak self] error in
                    self?.manageBadTransactionService(with: error)
                }).addDisposableTo(disposeBag)
        default:
            super.loadDataFromService()
        }
    }
    
}
