//
//  SkeletonCell.swift
//  Skeleton
//
//  Created by Gonzalo Nunez on 2/15/17.
//  Copyright © 2017 CocoaPods. All rights reserved.
//

import UIKit
import Shimmer

class SkeletonCell: UITableViewCell {

    static let identifier = "SkeletonCell"
    static let cellHeight: CGFloat = 70.0

    @IBOutlet var shimmeringViewTop: FBShimmeringView!
    @IBOutlet var shimmeringViewLeft: FBShimmeringView!
    @IBOutlet var shimmeringViewRight: FBShimmeringView!

  override func layoutSubviews() {
    super.layoutSubviews()
  }

    override func awakeFromNib() {

        super.awakeFromNib()

        shimmeringViewTop.contentView = UIImageView(image: ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Skeleton.lines, color: .BBVA300), width: Float(shimmeringViewTop.frame.size.width), height: Float(shimmeringViewTop.frame.size.height)))
        shimmeringViewTop.isShimmering = true

        shimmeringViewLeft.contentView = UIImageView(image: ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Skeleton.lines, color: .BBVA300), width: Float(shimmeringViewLeft.frame.size.width), height: Float(shimmeringViewLeft.frame.size.height)))
        shimmeringViewLeft.isShimmering = true

        let imageRotated = UIImageView(image: ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Skeleton.lines_reverse, color: .BBVA300), width: Float(shimmeringViewRight.frame.size.width), height: Float(shimmeringViewRight.frame.size.height)))

        shimmeringViewRight.contentView = imageRotated

        shimmeringViewRight.isShimmering = true
    }

}
