//
//  TransactionTableViewCell.swift
//  shoppingapp
//
//  Created by jesus.martinez on 4/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit

class TransactionTableViewCell: UITableViewCell {

    static let identifier = "TransactionTableViewCell"
    static let conceptLabelTop: CGFloat = 15
    static let conceptLabelHeight: CGFloat = 20
    static let pendingLabelTop: CGFloat = 8
    static let pendingLabelHeight: CGFloat = 14
    static let pendingLabelBottom: CGFloat = 8
    static let financingViewHeight: CGFloat = 16
    static let financingViewBottom: CGFloat = 15
    static let financingImageWidth: CGFloat = 14
    static let financingImageTrailing: CGFloat = 8

    // MARK: @IBOutlet's

    @IBOutlet weak var conceptLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!

    @IBOutlet weak var pendingLabel: UILabel!
    @IBOutlet weak var pendingTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var pendingLabelBottomConstraint: NSLayoutConstraint!

    @IBOutlet weak var financingImageView: UIImageView!
    @IBOutlet weak var financingLabel: UILabel!
    @IBOutlet weak var horizontalSpacingFinancingImageViewAndLabelConstraint: NSLayoutConstraint!
    @IBOutlet weak var financingImageViewWidthConstraint: NSLayoutConstraint!

    @IBOutlet weak var separatorLineView: UIView!

    override func awakeFromNib() {

        super.awakeFromNib()

        selectionStyle = .none

        conceptLabel.font = Tools.setFontBook(size: 16)
        conceptLabel.textColor = .BBVA600

        amountLabel.font = Tools.setFontBook(size: 16)
        amountLabel.textColor = .BBVA600

        pendingLabel.font = Tools.setFontBookItalic(size: 14)
        pendingLabel.textColor = .DARKAQUA

        let image = ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Common.dollar_icon, color: .DARKAQUA), width: 16.1, height: 15.6)
        financingImageView.image = image
        financingLabel.font = Tools.setFontBookItalic(size: 14)
        financingLabel.textColor = .DARKAQUA

        separatorLineView.backgroundColor = .BBVA200

        setAccessibilities()

    }

    func configure(withDisplayItem displayItem: TransactionDisplayItem, hideSeparator: Bool) {

        conceptLabel.text = displayItem.concept
        amountLabel.attributedText = displayItem.amount

        pendingLabel.text = displayItem.pending
        pendingTopConstraint.constant = displayItem.pending.isEmpty ? 0 : TransactionTableViewCell.pendingLabelTop

        financingLabel.text = displayItem.financing
        pendingLabelBottomConstraint.constant = displayItem.financing.isEmpty ? 0 : TransactionTableViewCell.pendingLabelBottom

        let image = ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Common.dollar_icon, color: .DARKAQUA), width: 16.1, height: 15.6)
        financingImageView.image = displayItem.isFinanciable ? image : nil
        financingImageViewWidthConstraint.constant = displayItem.isFinanciable ? TransactionTableViewCell.financingImageWidth : 0
        horizontalSpacingFinancingImageViewAndLabelConstraint.constant = displayItem.isFinanciable ? TransactionTableViewCell.financingImageTrailing : 0

        separatorLineView.isHidden = hideSeparator

        dynamicAccessibilities(forDisplayItem: displayItem)

    }

    class func heightForCell(withDisplayItem displayItem: TransactionDisplayItem, hideSeparator: Bool) -> CGFloat {

        var heightCell = TransactionTableViewCell.conceptLabelTop + TransactionTableViewCell.financingViewBottom + TransactionTableViewCell.conceptLabelHeight + 0.5

        if !displayItem.pending.isEmpty {

            heightCell += TransactionTableViewCell.pendingLabelHeight + TransactionTableViewCell.pendingLabelTop
        }

        if !displayItem.financing.isEmpty {

            heightCell += TransactionTableViewCell.pendingLabelBottom + TransactionTableViewCell.financingViewHeight
        }

        return heightCell
    }
    override func setHighlighted(_ highlighted: Bool, animated: Bool) {
        super.setHighlighted(highlighted, animated: animated)

        if highlighted {
            backgroundColor = .BBVA100
        } else {
            backgroundColor = .BBVAWHITE
        }

    }

}

// MARK: - APPIUM

private extension TransactionTableViewCell {

    func setAccessibilities() {

        #if APPIUM

        accessibilityIdentifier = ConstantsAccessibilities.TRANSACTIONS_LIST_CELL_AND_SECTION_ID

        Tools.setAccessibility(view: conceptLabel, identifier: ConstantsAccessibilities.TRANSACTIONS_LIST_CONCEPT_TEXT_ID)
        Tools.setAccessibility(view: amountLabel, identifier: ConstantsAccessibilities.TRANSACTIONS_LIST_AMOUNT_TEXT_ID)

        #endif

    }

    func dynamicAccessibilities(forDisplayItem displayItem: TransactionDisplayItem) {

        #if APPIUM

            Tools.setAccessibility(view: pendingLabel, identifier: "")
            Tools.setAccessibility(view: financingImageView, identifier: "")
            Tools.setAccessibility(view: financingLabel, identifier: "")

        if !displayItem.pending.isEmpty {
            Tools.setAccessibility(view: pendingLabel, identifier: ConstantsAccessibilities.TRANSACTIONS_LIST_PENDING_TEXT_ID)
        }

        if displayItem.isFinanciable {
            Tools.setAccessibility(view: financingImageView, identifier: ConstantsAccessibilities.TRANSACTIONS_LIST_FINANCIABLE_IMAGE_ID)

        }

        if !displayItem.financing.isEmpty {
            Tools.setAccessibility(view: financingLabel, identifier: ConstantsAccessibilities.TRANSACTIONS_LIST_FINANCIABLE_TEXT_ID)
        }

        #endif

    }

}
