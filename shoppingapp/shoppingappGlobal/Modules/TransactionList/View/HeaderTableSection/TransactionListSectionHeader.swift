//
//  TransactionListSectionHeader.swift
//  shoppingapp
//
//  Created by jesus.martinez on 4/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit

class TransactionListSectionHeader: UIView {

    static let defaultHeight: CGFloat = 49.0

    // MARK: @IBOutlet's

    @IBOutlet weak var titleLabel: UILabel!

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }

    // MARK: - Private Helper Methods

    private func setupView() {
        let view = viewFromNibForClass()
        view.frame = bounds
        view.backgroundColor = .BBVAWHITE

        configureLabels()
        setAccessibilities()

        addSubview(view)
    }

    private func configureLabels() {

        titleLabel.font = Tools.setFontMediumItalic(size: 14)
        titleLabel.textColor = .DARKGOLD

    }

    private func viewFromNibForClass() -> UIView {

        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView

        return view
    }

}

// MARK: - APPIUM

private extension TransactionListSectionHeader {

    func setAccessibilities() {

        #if APPIUM

        accessibilityIdentifier = ConstantsAccessibilities.TRANSACTIONS_LIST_CELL_AND_SECTION_ID
        Tools.setAccessibility(view: titleLabel, identifier: ConstantsAccessibilities.TRANSACTIONS_LIST_TABLE_HEADER_SECTION_DATE_ID)

        #endif

    }

}
