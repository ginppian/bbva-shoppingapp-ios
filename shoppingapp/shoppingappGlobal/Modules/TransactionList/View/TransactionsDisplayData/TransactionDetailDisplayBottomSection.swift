//
//  TransactionDetailDisplayBottomSection.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 16/11/2017.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit

struct  TransactionDetailDisplayBottomSection {

    var title: String
    var value: String
    var accessibiltyTitle: String
    var accessibiltyValue: String

}
