//
//  TransactionDisplayItem.swift
//  shoppingapp
//
//  Created by jesus.martinez on 7/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit

struct TransactionDisplayItem {

    let id: String
    let concept: String
    let amount: NSAttributedString
    let pending: String
    let financing: String
    let isFinanciable: Bool
    let contract: ContractEntity
    let additionalInformation: AdditionalInformationEntity?
    let accountedDate: String?
    let operationDate: String?
    let amountTransactionDetail: NSAttributedString

    init(fromTransactionBO transactionBO: TransactionBO) {

        id = transactionBO.id
        concept = transactionBO.concept

        let amountFormatter = AmountFormatter(codeCurrency: transactionBO.localAmount.currency)
        amount = amountFormatter.attributtedText(forAmount: transactionBO.localAmount.amount, bigFontSize: 16, smallFontSize: 12)
        
        pending = transactionBO.status == .pending ? Localizables.transactions.key_transactions_pending_text : ""
        financing = TransactionDisplayItem.financingLiteral(forFincancingType: transactionBO.financingType)
        isFinanciable = transactionBO.financingType == .financiable
        contract = transactionBO.contract
        additionalInformation = transactionBO.additionalInformation

        if let tempAccountedDate = transactionBO.accountedDate {
            accountedDate = Date.string(fromDate: tempAccountedDate, withFormat: Date.DATE_FORMAT_MONTH_NAME_YEAR_HOUR)
        } else {
            accountedDate = nil
        }
        operationDate = Date.string(fromDate: transactionBO.operationDate, withFormat: Date.DATE_FORMAT_MONTH_NAME_YEAR_HOUR)
        amountTransactionDetail = amountFormatter.attributtedText(forAmount: transactionBO.localAmount.amount, bigFontSize: 48, smallFontSize: 23)
    }

    private static func financingLiteral(forFincancingType fincinancingType: FinancingType) -> String {

        var literal: String = ""

        if fincinancingType == .financiable {

            literal = Localizables.transactions.key_transactions_financiable_text

        } else if fincinancingType == .financed {

            literal = Localizables.transactions.key_transactions_financed_text

        }

        return literal
    }

}
