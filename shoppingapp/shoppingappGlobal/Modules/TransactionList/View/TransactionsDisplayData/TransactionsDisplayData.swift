//
//  TransactionsDisplayData.swift
//  shoppingapp
//
//  Created by jesus.martinez on 7/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

class TransactionsDisplayData {

    var sections: [TransactionDisplaySection]

    init() {

        self.sections = [TransactionDisplaySection]()

    }

    func add(transactions: [TransactionBO]) {

        var transactions = transactions

        transactions.sort(by: { $0.operationDate > $1.operationDate })

        for transaction in transactions {

            let keySection = transaction.operationDate.string(format: Calendar.gregorianCalendar().isDate(Date(), duringSameYearAsDate: transaction.operationDate) ? Date.DATE_FORMAT_DAY_MONTH : Date.DATE_FORMAT_DAY_MONTH_YEAR)

            let displayTransaction = TransactionDisplayItem(fromTransactionBO: transaction)

            if let sectionIndex = self.indexSection(forKey: keySection) {

                self.sections[sectionIndex].items.append(displayTransaction)

            } else {

                let newSection = TransactionDisplaySection(name: keySection, items: [displayTransaction])
                self.sections.append(newSection)

            }

        }

    }

    private func indexSection(forKey key: String) -> Int? {

        for (index, section) in self.sections.enumerated() where section.name == key {
                return index
        }

        return nil
    }

}
