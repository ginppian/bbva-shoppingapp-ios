//
//  TransactionsDisplaySection.swift
//  shoppingapp
//
//  Created by jesus.martinez on 7/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit

struct TransactionDisplaySection {

    var name: String
    var items: [TransactionDisplayItem] = []

}
