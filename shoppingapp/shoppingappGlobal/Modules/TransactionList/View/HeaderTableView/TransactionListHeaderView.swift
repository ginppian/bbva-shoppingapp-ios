//
//  TransactionListHeaderView.swift
//  shoppingapp
//
//  Created by jesus.martinez on 4/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit

class TransactionListHeaderView: UIView {

    static let defaultHeight: CGFloat = 44.0

    // MARK: @IBOutlet's

    @IBOutlet weak var dateTitleLabel: UILabel!
    @IBOutlet weak var amountTitleLabel: UILabel!

    // MARK: - Initializers

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupView()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupView()
    }

    // MARK: - Private Helper Methods

    private func setupView() {
        let view = viewFromNibForClass()
        view.frame = bounds
        view.backgroundColor = .BBVAWHITE

        configureLabels()

        addSubview(view)

        setAccessibilities()

    }

    private func configureLabels() {

        dateTitleLabel.font = Tools.setFontBold(size: 14)
        dateTitleLabel.textColor = .BBVA500
        dateTitleLabel.text = Localizables.transactions.key_transactions_date_text

        amountTitleLabel.font = Tools.setFontBold(size: 14)
        amountTitleLabel.textColor = .BBVA500
        amountTitleLabel.text = Localizables.transactions.key_transactions_amount_text

    }

    private func viewFromNibForClass() -> UIView {

        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView

        return view
    }

}

// MARK: - APPIUM

private extension TransactionListHeaderView {

    func setAccessibilities() {

        #if APPIUM

        Tools.setAccessibility(view: dateTitleLabel, identifier: ConstantsAccessibilities.TRANSACTIONS_LIST_TABLE_HEADER_DATE_ID)
        Tools.setAccessibility(view: amountTitleLabel, identifier: ConstantsAccessibilities.TRANSACTIONS_LIST_TABLE_HEADER_AMOUNT_ID)

        #endif

    }

}
