//
//  TransactionListViewController.swift
//  shoppingapp
//
//  Created by jesus.martinez on 3/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//
import Lottie
import CellsNativeCore
import CellsNativeComponents

class TransactionListViewController: BaseViewController {

    // MARK: Constants

    static let ID: String = String(describing: TransactionListViewController.self)

    static let EVENT_NAV_TO_NEXT_SCREEN: ActionSpec<CardBO> = ActionSpec<CardBO>(id: "EVENT_NAV_TO_NEXT_SCREEN")

    #if APPIUM
    var headersForAccesibility = [UIView]()
    #endif

    // MARK: vars

    var presenter: TransactionListPresenterProtocol!

    // MARK: @IBOutlet's

    // FiltersScrollView
    @IBOutlet weak var filtersScrollView: FiltersScrollView!
    @IBOutlet weak var filtersScrollViewHeightConstraint: NSLayoutConstraint!

    // Error Screen
    @IBOutlet weak var errorScreenView: ErrorScreenView! {
        didSet {
            errorScreenView.delegate = self
        }
    }

    // Transactions table
    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var transactionsTableView: UITableView!

    var footerView: UIView!
    var lottieLoader: LOTAnimationView!

    var displayData: TransactionsDisplayData?
    var moreContent = true
    var needShowSkeleton = false

    var filters: [FilterScrollDisplayData]? = []

    override func viewDidLoad() {

        self.configure(appLifecycle: (UIApplication.shared.delegate as! AppDelegate).appLifeCycle!, id: TransactionListViewController.ID)
        super.viewDidLoad()

        configureContentView()
        setAccessibilities()

        filtersScrollView.delegate = self

        if #available(iOS 11.0, *) {
            transactionsTableView.contentInsetAdjustmentBehavior = .never
        }
    }

    override func viewWillAppear(_ animated: Bool) {

        super.viewWillAppear(animated)

        configureNavigationBar()

        presenter.loadData()
        presenter.checkFilterData()

        BusManager.sessionDataInstance.disableSwipeLateralMenu()
    }
    
    override func viewWillAppearAfterDismiss() {
        
        presenter.viewWillAppearAfterDismiss()
    }

    override func viewWillDisappear(_ animated: Bool) {

        super.viewWillDisappear(animated)

    }

    override func viewDidAppear(_ animated: Bool) {

        super.viewDidAppear(animated)

        configureFooterLoader()
    }

    deinit {
        ComponentManager.remove(id: presenter.routerTag)
        DLog(message: "Deinitialized TransactionListViewController")
    }

    func configureContentView() {

        contentView.isHidden = true

        transactionsTableView.backgroundColor = .BBVAWHITE

        transactionsTableView.register(UINib(nibName: TransactionTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: TransactionTableViewCell.identifier)

        transactionsTableView.register(UINib(nibName: SkeletonCell.identifier, bundle: nil), forCellReuseIdentifier: SkeletonCell.identifier)

        transactionsTableView.rowHeight = UITableViewAutomaticDimension
        transactionsTableView.estimatedRowHeight = 60

        transactionsTableView.tableHeaderView = TransactionListHeaderView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: TransactionListHeaderView.defaultHeight))

        transactionsTableView.separatorStyle = .none

    }

    func configureFooterLoader() {

        footerView = UIView(frame: CGRect(x: 0, y: 0, width: transactionsTableView.frame.size.width, height: 130))
        footerView.backgroundColor = .clear

        lottieLoader = LOTAnimationView(name: ConstantsLottieAnimations.loader)
        lottieLoader.frame = CGRect(x: (UIScreen.main.bounds.width / 2) - 33.8 / 2, y: 50, width: 33.8, height: 30)
        lottieLoader.contentMode = .scaleAspectFit
        lottieLoader.loopAnimation = true

        footerView.addSubview(lottieLoader)

    }

    func configureNavigationBar() {

        setNavigationBarDefault()
        setNavigationBackButtonDefault()
        
        if !Settings.TransactionsListGlobal.hideFilteringOfTransactions {
            
            let image = SVGCache.image(forSVGNamed: ConstantsImages.Common.search_icon, color: .BBVAWHITE)
            setNavigationRightButton(withImage: image, action: #selector(filterAction))
        }

        setNavigationTitle(withText: Localizables.transactions.key_transactions_transactions_text)

    }

    @objc func filterAction(_ sender: AnyObject?) {

        presenter.presentTransactionsFilter()

    }

    override func backAction() {

        if navigationController?.viewControllers.first is TransactionsFilterViewController {
            BusManager.sessionDataInstance.dismissViewController(animated: true)
        } else {
            super.backAction()
        }
    }

    override func initModel(withModel model: Model) {
        presenter.setModel(model: model)
    }
}

extension TransactionListViewController: TransactionListViewProtocol {

    func showNoContent() {

        let title = Localizables.transactions.key_transactions_oups_text
        let message = Localizables.transactions.key_transactions_noresults_text
        let displayData = ErrorScreenDisplayData(title: title, message: message, hasRetryButton: false)
        errorScreenView.configureNoContent(displayData: displayData)

        errorScreenView.isHidden = false
        contentView.isHidden = true

    }

    func showNoResults() {

        let title = Localizables.transactions.key_transactions_oups_text
        let message = Localizables.transactions.key_not_movements_text
        let displayData = ErrorScreenDisplayData(title: title, message: message, hasRetryButton: false)
        errorScreenView.configureNoContent(displayData: displayData)

        errorScreenView.isHidden = false
        contentView.isHidden = true
    }

    func showErrorContent() {

        let title = Localizables.common.key_error_ups_text
        let message = Localizables.common.key_conexion_error_text + " " + Localizables.common.key_try_later_text
        let displayData = ErrorScreenDisplayData(title: title, message: message, hasRetryButton: true)
        errorScreenView.configureError(displayData: displayData)

        errorScreenView.isHidden = false
        contentView.isHidden = true
        view.backgroundColor = .WHITELIGHTBLUE

    }

    func showFiltersScrollView(withFilters filters: [FilterScrollDisplayData]?) {

        self.filters = filters
        filtersScrollView.loadFilters(filters: filters)
        filtersScrollViewHeightConstraint.constant = FiltersScrollView.default_height

    }

    func hideFiltersScrollView() {

        filters = nil
        filtersScrollView.loadFilters(filters: nil)
        filtersScrollViewHeightConstraint.constant = 0

    }

    func showTransactions(transactionsDisplayData: TransactionsDisplayData) {

        contentView.isHidden = false
        errorScreenView.isHidden = true

        displayData = transactionsDisplayData

        transactionsTableView.reloadData()
    }

    func updateForNoMoreContent() {

        moreContent = false
    }

    func updateForMoreContent() {

        moreContent = true
    }

    func showNextPageAnimation() {

        footerView.isHidden = false

        transactionsTableView.tableFooterView = footerView

        lottieLoader.play()

        transactionsTableView.reloadData()

    }

    func hideNextPageAnimation() {

        footerView.isHidden = true

        transactionsTableView.tableFooterView = nil

        lottieLoader.pause()

        transactionsTableView.reloadData()
    }

    func showSkeleton() {

        contentView.isHidden = false
        errorScreenView.isHidden = true

        transactionsTableView.isScrollEnabled = false

        needShowSkeleton = true

        transactionsTableView.tableHeaderView?.isHidden = true
        transactionsTableView.contentInset = UIEdgeInsets(top: -36, left: 0, bottom: 0, right: 0)

        transactionsTableView.tableFooterView = nil
        transactionsTableView.reloadData()

    }

    func hideSkeleton() {

        transactionsTableView.isScrollEnabled = true
        needShowSkeleton = false
        transactionsTableView.tableHeaderView?.isHidden = false
        transactionsTableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
        transactionsTableView.setContentOffset(CGPoint(x: 0, y: 0), animated: false)

        transactionsTableView.reloadData()
    }

    func sendOmnitureScreen() {
        TrackerHelper.sharedInstance().trackTransactionsScreen(withCustomerID: SessionDataManager.sessionDataInstance().customerID)
    }

}

extension TransactionListViewController: ViewProtocol {

    func showError(error: ModelBO) {
        presenter.showModal()
    }

    func changeColorEditTexts() {
    }
}

extension TransactionListViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        if !needShowSkeleton {

            let view = TransactionListSectionHeader(frame: CGRect(x: 0, y: 0, width: self.view.frame.size.width, height: TransactionListSectionHeader.defaultHeight))
            view.titleLabel.text = displayData?.sections[section].name
            view.backgroundColor = .clear

            appendHeaderForAccessiblity(view: view)
            return view
        }

        return UIView()
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        var height: CGFloat = UITableViewAutomaticDimension

        if  needShowSkeleton {
            height = SkeletonCell.cellHeight
        }
        return height
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {

        if !needShowSkeleton {
            return  TransactionListSectionHeader.defaultHeight
        }

        return 0
    }

    func tableView(_ tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {

        let footer = UIView()

        if !needShowSkeleton {
            footer.backgroundColor = .BBVA200

        } else {
            footer.backgroundColor = .clear
        }

        return footer
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 1
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if !needShowSkeleton {

            if let displayItem = displayData?.sections[indexPath.section].items[indexPath.row] {
                presenter.goToTransactionDetail(withTransactionDisplayItem: displayItem)

            }
        }
    }
}

extension TransactionListViewController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {

        if !needShowSkeleton {
            return displayData?.sections.count ?? 0
        }

        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if !needShowSkeleton {
            return displayData?.sections[section].items.count ?? 0
        }

        let screen = UIScreen.main.bounds
        let screenHeight = screen.size.height
        let numberCells = (screenHeight / SkeletonCell.cellHeight)

        return Int(numberCells)
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if !needShowSkeleton {

            if let displayItem = displayData?.sections[indexPath.section].items[indexPath.row] {

                if let cell = tableView.dequeueReusableCell(withIdentifier: TransactionTableViewCell.identifier, for: indexPath) as? TransactionTableViewCell {
                    cell.configure(withDisplayItem: displayItem, hideSeparator: self.isLastRow(inPath: indexPath))
                    return cell
                }
            }
        } else {
            if let cell = tableView.dequeueReusableCell(withIdentifier: SkeletonCell.identifier, for: indexPath) as? SkeletonCell {
                return cell
            }
        }
        return UITableViewCell()
    }

    func isLastRow(inPath path: IndexPath) -> Bool {
        return path.row == (displayData!.sections[path.section].items.count - 1)
    }

}

extension TransactionListViewController: UIScrollViewDelegate {

    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {

        let offset = scrollView.contentOffset
        let bounds = scrollView.bounds
        let size = scrollView.contentSize
        let inset = scrollView.contentInset
        let y = offset.y + bounds.size.height - inset.bottom
        let h = size.height

        let reload_distance: CGFloat = -300.0
        if y > h + reload_distance && moreContent {
            presenter.loadDataNextPage()
        }
    }
}

extension TransactionListViewController: CloseModalDelegate {
    func errorViewControllerAcceptPressed(_ errorViewController: ErrorViewController) {
        presenter.checkError()
    }

    func errorViewControllerCancelPressed(_ errorViewController: ErrorViewController) {
    }
}

extension TransactionListViewController: FiltersScrollViewDelegate {

    func removeFilterPushed(filter: FilterScrollDisplayData, inFiltersScrollView filtersScrollView: FiltersScrollView) {
        presenter.removeFilter(filter: filter, inFiltersScrollView: filtersScrollView)
    }

    func removeFilterLonglyPushed(inFiltersScrollView filtersScrollView: FiltersScrollView) {
        presenter.removeFilterLonglyPushed(inFiltersScrollView: filtersScrollView)
    }

    func removeAllFiltersPushed(inFiltersScrollView filtersScrollView: FiltersScrollView) {
        presenter.removeAllFilters(inFiltersScrollView: filtersScrollView)
    }
}

extension TransactionListViewController: ErrorScreenViewDelegate {

    func errorScreenViewRetryButtonPressed(_ errorScreenView: ErrorScreenView) {

        presenter.retryButtonPressed()
    }
}

// MARK: - APPIUM

private extension TransactionListViewController {

    func appendHeaderForAccessiblity(view: UIView) {
        #if APPIUM
        headersForAccesibility.append(view)
        #endif
    }

    func setAccessibilities() {

        #if APPIUM
        transactionsTableView.accessibilityIdentifier = ConstantsAccessibilities.TRANSACTIONS_LIST_ID
        Tools.setAccessibility(view: errorScreenView.titleLabel, identifier: ConstantsAccessibilities.NO_CONTENT_TITTLE_TRANSACTION_LIST_ID)
        Tools.setAccessibility(view: errorScreenView.messageLabel, identifier: ConstantsAccessibilities.NO_CONTENT_DESC_TRANSACTION_LIST_ID)

        filtersScrollView.isAccessibilityElement = false
        filtersScrollView.filtersScrollCollectionView.isAccessibilityElement = false
        #endif

    }

}
