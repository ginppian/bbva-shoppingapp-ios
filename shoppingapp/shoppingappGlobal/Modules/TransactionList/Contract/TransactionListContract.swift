//
//  TransactionListContract.swift
//  shoppingapp
//
//  Created by jesus.martinez on 3/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import RxSwift

protocol TransactionListViewProtocol: ViewProtocol {

    func showNoContent()
    func showNoResults()
    func showErrorContent()
    func showTransactions(transactionsDisplayData: TransactionsDisplayData)
    func updateForMoreContent()
    func updateForNoMoreContent()
    func showNextPageAnimation()
    func hideNextPageAnimation()
    func showSkeleton()
    func hideSkeleton()
    func showFiltersScrollView(withFilters filters: [FilterScrollDisplayData]?)
    func hideFiltersScrollView()
    func sendOmnitureScreen()

}

protocol TransactionListPresenterProtocol: PresenterProtocol {

    func viewWillAppearAfterDismiss()
    func checkFilterData()
    func loadData()
    func loadDataNextPage()
    func presentTransactionsFilter()
    func removeFilter(filter: FilterScrollDisplayData, inFiltersScrollView filtersScrollView: FiltersScrollView )
    func removeFilterLonglyPushed(inFiltersScrollView filtersScrollView: FiltersScrollView)
    func removeAllFilters(inFiltersScrollView filtersScrollView: FiltersScrollView)
    func goToTransactionDetail(withTransactionDisplayItem transactionDisplayItem: TransactionDisplayItem)
    func retryButtonPressed()

}

protocol TransactionListInteractorProtocol {

    func provideTransactions(fromNextPage nextPage: String) -> Observable <ModelBO>
    func provideTransaction(withTransactionFilterTransport transactionFilterTransport: TransactionFilterTransport?, forCardId cardId: String) -> Observable <ModelBO>

}
