//
//  TransactionsListContractA.swift
//  shoppingappMX
//
//  Created by Armando Vazquez on 8/24/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import RxSwift

protocol TransactionListInteractorProtocolA: TransactionListInteractorProtocol {
    func provideTransactionsForAccount(filteredBy transactionTransport: TransactionFilterTransport?, with accountId: String) -> Observable<ModelBO>
}
