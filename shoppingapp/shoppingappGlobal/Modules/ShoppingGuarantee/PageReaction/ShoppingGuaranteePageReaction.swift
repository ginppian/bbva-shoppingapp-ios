//
//  ShoppingGuaranteePageReaction.swift
//  shoppingapp
//
//  Created by Jose Luis Mendoza Espinosa on 30/07/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

class ShoppingGuaranteePageReaction: PageReaction {
    
    let ROUTER_TAG: String = "shopping-guarantee-tag"
    
    override func configure() {
        
    }
    
    func getRouterTag() -> String {
        
        return ROUTER_TAG
    }
}
