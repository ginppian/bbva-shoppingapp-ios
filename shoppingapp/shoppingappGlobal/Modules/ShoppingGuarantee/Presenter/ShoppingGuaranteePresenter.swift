//
//  ShoppingGuaranteePresenter.swift
//  shoppingapp
//
//  Created by Jose Luis Mendoza Espinosa on 30/07/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import RxSwift
import CellsNativeComponents

class ShoppingGuaranteePresenter<T: ShoppingGuaranteeViewProtocol>: BasePresenter<T> {

    var promotionBO: PromotionBO?
    
    let ROUTER_TAG: String = "shopping-guarantee-tag"
    
    required init() {
        super.init(tag: ROUTER_TAG)
    }
    
    override init(busManager: BusManager) {
        super.init(busManager: busManager)
        routerTag = "shopping-guarantee-tag"
    }
    
    override func setModel(model: Model) {
        
        if let promotionDetailTransport = model as? PromotionDetailTransport {
            
            promotionBO = promotionDetailTransport.promotionBO
        }
    }
    
}

extension ShoppingGuaranteePresenter: ShoppingGuaranteePresenterProtocol {
    
    func viewLoaded() {
        
        if let promotion = promotionBO, let contactDetails = promotion.contactDetails {
            
            var guaranteeString = ""
            
            for contact in contactDetails {
                
                if let descriptionString = contact.description {
                    
                    if descriptionString.count > guaranteeString.count {
                        
                        guaranteeString = descriptionString
                    }
                }
            }
            
            view?.showPromotionGuarantee(guarantee: guaranteeString, withTitle: Localizables.promotions.key_satisfaction_guarantee_text)
        }
    }
    
}
