//
//  ShoppingGuaranteeView.swift
//  shoppingapp
//
//  Created by Jose Luis Mendoza Espinosa on 30/07/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

class ShoppingGuaranteeViewController: BaseViewController {

    // MARK: Constants
    static let ID: String = String(describing: ShoppingGuaranteeViewController.self)
    
    var presenter: ShoppingGuaranteePresenterProtocol!
    
    // MARK: outlets
    @IBOutlet weak var textView: UITextView!
    
    // MARK: life cycle
    override func viewDidLoad() {
        
        self.configure(appLifecycle: (UIApplication.shared.delegate as! AppDelegate).appLifeCycle!, id: ShoppingGuaranteeViewController.ID)
        super.viewDidLoad()
        
        configureView()
        
        edgesForExtendedLayout = UIRectEdge()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        setNavigationBarDefault()
        
        setNavigationBackButtonDefault()
        
        BusManager.sessionDataInstance.disableSwipeLateralMenu()
        
        presenter.viewLoaded()
        
    }
    
    func configureView() {
        
        textView.font = Tools.setFontBook(size: 14)
        textView.textColor = .BBVA600
        textView.textAlignment = .left
        
        self.view.backgroundColor = .BBVAWHITE
    }
    
    override func initModel(withModel model: Model) {
        presenter.setModel(model: model)
    }
    
    deinit {
        if presenter != nil {
            ComponentManager.remove(id: presenter.routerTag)
        }
        DLog(message: "Deinitialized ShoppingGuaranteeViewController")
    }
    
}

extension ShoppingGuaranteeViewController: ShoppingGuaranteeViewProtocol {
    
    func showPromotionGuarantee(guarantee: String, withTitle title: String) {
        
        self.title = title
        textView.text = guarantee
    }
    
    func showError(error: ModelBO) {
        
    }
    
    func changeColorEditTexts() {
    
    }
}
