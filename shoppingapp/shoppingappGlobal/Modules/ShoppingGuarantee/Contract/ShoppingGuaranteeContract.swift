//
//  ShoppingGuaranteeContract.swift
//  shoppingapp
//
//  Created by Jose Luis Mendoza Espinosa on 30/07/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol ShoppingGuaranteeViewProtocol: ViewProtocol {
    
    func showPromotionGuarantee(guarantee: String, withTitle title: String)
}

protocol ShoppingGuaranteePresenterProtocol: PresenterProtocol {
    
    func viewLoaded()
}
