//
//  ShoppingWebPageReaction.swift
//  shoppingapp
//
//  Created by Luis Monroy on 18/03/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

class ShoppingWebPageReaction: PageReaction {

    let routerTag = "shopping-web-tag"

    override func configure() {

    }

    func getRouterTag() -> String {

        return routerTag
    }
}
