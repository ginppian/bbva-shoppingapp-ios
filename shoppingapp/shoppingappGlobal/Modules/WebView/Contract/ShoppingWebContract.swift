//
//  ShoppingWebContract.swift
//  shoppingapp
//
//  Created by Luis Monroy on 18/03/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol ShoppingWebViewProtocol: ViewProtocol {

    func showWebPage(ofUrl url: String, withTitle title: String)
}

protocol ShoppingWebPresenterProtocol: PresenterProtocol {

    func viewDidAppear()
    func contentLoadedSuccess()
    func contentLoadedWithError()
}
