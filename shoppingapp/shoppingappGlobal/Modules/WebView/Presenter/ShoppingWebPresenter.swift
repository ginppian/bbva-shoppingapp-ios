//
//  ShoppingWebPresenter.swift
//  shoppingapp
//
//  Created by Luis Monroy on 18/03/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import RxSwift
import CellsNativeComponents

class ShoppingWebPresenter<T: ShoppingWebViewProtocol>: BasePresenter<T> {

    var webPageUrl: String?
    var webTitle: String?

    let ROUTER_TAG: String = "shopping-web-tag"

    required init() {
        
        super.init(tag: ROUTER_TAG)
    }

    override init(busManager: BusManager) {
        
        super.init(busManager: busManager)
        routerTag = "shopping-web-tag"
    }

    override func setModel(model: Model) {

        if let webTransport = model as? WebTransport {
            
            webPageUrl = webTransport.webPage
            webTitle = webTransport.webTitle
        }
    }
}

extension ShoppingWebPresenter: ShoppingWebPresenterProtocol {

    func viewDidAppear() {

        if let webPageUrl = webPageUrl, let webTitle = webTitle {
            
            showLoading()
            view?.showWebPage(ofUrl: webPageUrl, withTitle: webTitle)
        }
    }
    
    func contentLoadedSuccess() {
        
        hideLoading()
    }
    
    func contentLoadedWithError() {
        
        hideLoading()
    }
}
