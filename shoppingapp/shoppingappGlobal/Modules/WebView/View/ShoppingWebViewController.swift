//
//  ShoppingWebViewController.swift
//  shoppingapp
//
//  Created by Luis Monroy on 18/03/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents
import WebKit

class ShoppingWebViewController: BaseViewController, WKUIDelegate {

    // MARK: Constants
    static let ID: String = String(describing: ShoppingWebViewController.self)

    var presenter: ShoppingWebPresenterProtocol!
    
    var webView: WKWebView!

    override func loadView() {
        
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.uiDelegate = self
        webView.navigationDelegate = self
        view = webView
    }
    
    override func viewDidLoad() {

        navigationController?.navigationBar.isHidden = false

        self.configure(appLifecycle: (UIApplication.shared.delegate as! AppDelegate).appLifeCycle!, id: ShoppingWebViewController.ID)
        super.viewDidLoad()

        edgesForExtendedLayout = UIRectEdge()
    }

    override func viewWillAppear(_ animated: Bool) {

        super.viewWillAppear(animated)

        setNavigationBarDefault()
        setNavigationBackButtonDefault()

        BusManager.sessionDataInstance.disableSwipeLateralMenu()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        presenter.viewDidAppear()
    }

    override func initModel(withModel model: Model) {
        
        presenter.setModel(model: model)
    }
}

extension ShoppingWebViewController: ShoppingWebViewProtocol {

    func showWebPage(ofUrl url: String, withTitle title: String) {
        
        if let url = URL(string: url) {
            
            let urlRequest = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalCacheData, timeoutInterval: 30)
            
            self.title = title
            webView.load(urlRequest)
        }
    }

    func showError(error: ModelBO) {
    }

    func changeColorEditTexts() {
    }
}

extension ShoppingWebViewController: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
        presenter.contentLoadedSuccess()
    }
    
    func webView(_ webView: WKWebView,
                 didFailProvisionalNavigation navigation: WKNavigation!,
                 withError error: Error) {
        
        presenter.contentLoadedWithError()
    }
}
