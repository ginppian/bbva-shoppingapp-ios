//
//  PickerContract.swift
//  shoppingapp
//
//  Created by Javier Pino on 18/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation

protocol PickerViewProtocol: ViewProtocol {

    func configureView()
    func showPickerOptions(options: PickerOptionsDisplay)
    func dismiss()
}

protocol PickerPresenterProtocol: PresenterProtocol {

    func viewDidLoad()
    func didSelectRow(atIndex index: Int)
}
