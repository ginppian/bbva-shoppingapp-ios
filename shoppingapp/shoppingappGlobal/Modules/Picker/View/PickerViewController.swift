//
//  PickerViewController.swift
//  shoppingapp
//
//  Created by Javier Pino on 18/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit
import CellsNativeCore
import CellsNativeComponents

class PickerViewController: BaseViewController {

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var containerViewHeightConstraint: NSLayoutConstraint!

    var presenter: PickerPresenterProtocol!

    var optionTitles: [String]?
    var selectedCellIndex: Int?

    static let reuseCellIdentifier = "PickerTableViewCell"
    static let cellHeight = 66.0

    override func viewDidLoad() {

        super.viewDidLoad()

        presenter.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        UIView.animate(withDuration: 0.28,
                       delay: 0.28,
                       options: UIViewAnimationOptions.curveEaseInOut,
                       animations: { () -> Void in
                        self.view.backgroundColor = .clear
                        self.view.backgroundColor = UIColor.NAVY.withAlphaComponent(0.9)

        }, completion: nil)

        BusManager.sessionDataInstance.disableSwipeLateralMenu()

    }

    override func viewWillLayoutSubviews() {

        super.viewWillLayoutSubviews()

        updateTableViewContainerHeight()
    }

    func updateTableViewContainerHeight() {

        var newHeight = 0.0

        if let optionTitles = optionTitles {

            let cellsHeight = Double(optionTitles.count) * PickerViewController.cellHeight
            let maxHeight = Double(view.frame.size.height)

            newHeight = (cellsHeight < maxHeight) ? cellsHeight : maxHeight
        }

        containerViewHeightConstraint.constant = CGFloat(newHeight)
    }

    deinit {
        ComponentManager.remove(id: presenter.routerTag)
        DLog(message: "Deinitialized PickerViewController")
    }

}

extension PickerViewController: PickerViewProtocol {

    func configureView() {

        setNavigationBarTransparent()
        navigationItem.setHidesBackButton(true, animated: false)
        view.backgroundColor = .clear
        containerView.backgroundColor = .BBVAWHITE

        tableView.register(UINib(nibName: "PickerTableViewCell", bundle: nil), forCellReuseIdentifier: PickerViewController.reuseCellIdentifier)
        tableView.rowHeight = CGFloat(PickerViewController.cellHeight)
        tableView.separatorStyle = .none
        tableView.delegate = self
        tableView.dataSource = self

        setAccessibilities()
    }

    func showPickerOptions(options: PickerOptionsDisplay) {

        optionTitles = options.titles
        selectedCellIndex = options.selectedIndex

        tableView.reloadData()

        updateTableViewContainerHeight()
    }

    func dismiss() {

        view.backgroundColor = .clear
        BusManager.sessionDataInstance.dismissViewController(animated: true, completion: nil)
    }

    func showError(error: ModelBO) {

    }

    func changeColorEditTexts() {

    }

}

extension PickerViewController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {

        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if let optionTitles = optionTitles {
            return optionTitles.count
        }
        return 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tableView.dequeueReusableCell(withIdentifier: PickerViewController.reuseCellIdentifier) as! PickerTableViewCell

        if let optionTitles = optionTitles {

            let row = indexPath.row

            var selected = false

            if let selectedIndex = selectedCellIndex {

                selected = (selectedIndex == row)
            }

            cell.checkImageView.isHidden = !selected
            cell.titleLabel.text = optionTitles[row]

            cell.separatorView.isHidden = false

            if row == (optionTitles.count - 1) {

                cell.separatorView.isHidden = true
            }
        }

        return cell
    }
}

extension PickerViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        let cell = tableView.cellForRow(at: indexPath) as! PickerTableViewCell

        cell.checkImageView.isHidden = false

        presenter.didSelectRow(atIndex: indexPath.row)

    }
}

// MARK: - APPIUM

private extension PickerViewController {

    func setAccessibilities() {

        #if APPIUM

            tableView.isAccessibilityElement = false
            tableView.accessibilityIdentifier = ConstantsAccessibilities.PICKER_TABLEVIEW_ID

        #endif
    }
}
