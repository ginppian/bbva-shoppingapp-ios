//
//  PickerTableViewCell.swift
//  shoppingapp
//
//  Created by Javier Pino on 19/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit

class PickerTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var checkImageView: UIImageView!
    @IBOutlet weak var separatorView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

        titleLabel.font = Tools.setFontBook(size: 16)
        titleLabel.textColor = .BBVA600

        separatorView.backgroundColor = .BBVA300

        checkImageView.image = SVGCache.image(forSVGNamed: ConstantsImages.Common.checkmark_success, color: .COREBLUE)

        setAccessibilities()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}

// MARK: - APPIUM

private extension PickerTableViewCell {

    func setAccessibilities() {

        #if APPIUM

            accessibilityIdentifier = ConstantsAccessibilities.PICKER_TABLEVIEW_CELL_ID

            Tools.setAccessibility(view: titleLabel, identifier: ConstantsAccessibilities.PICKER_TABLEVIEW_CELL_TITLE_ID)

        #endif
    }
}
