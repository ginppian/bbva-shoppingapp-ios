//
//  PickerPresenter.swift
//  shoppingapp
//
//  Created by Javier Pino on 18/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation

protocol PickerPresenterDelegate: class {

    func didSelectPickerOption(atIndex index: Int)
}

class PickerPresenter<T: PickerViewProtocol>: BasePresenter<T> {

    weak var delegate: PickerPresenterDelegate?

    var options: PickerOptionsDisplay?

    required init() {
        super.init(tag: PickerPageReaction.ROUTER_TAG)
    }

}

extension PickerPresenter: PickerPresenterProtocol {

    func viewDidLoad() {

        view?.configureView()

        if let options = options {
            view?.showPickerOptions(options: options)
        }
    }

    func didSelectRow(atIndex index: Int) {

        //BusManager.sessionDataInstance.publishData(tag: "picker", event: "picker-content-bind", values: ["result": "aceptar" as AnyObject])
        delegate?.didSelectPickerOption(atIndex: index)
        view?.dismiss()
    }
}
