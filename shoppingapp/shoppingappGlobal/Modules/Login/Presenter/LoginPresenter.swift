//
//  LoginPresenter.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 7/3/17.
//  Copyright © 2017 daniel.petrovic. All rights reserved.
//

import Foundation
import CellsNativeComponents
import RxSwift

class LoginPresenter<T: LoginViewProtocol>: BasePresenter<T> {

    var loginDisplayData: LoginDisplayData?
    var changeUserDisplayData: LoginDisplayData?

    let disposeBag = DisposeBag()

    var animationFinished = false
    var changeUserPressed = false

    override init(busManager: BusManager) {
        super.init(busManager: busManager)
        routerTag = LoginPageReaction.ROUTER_TAG
    }

    required init() {
        super.init(tag: LoginPageReaction.ROUTER_TAG)
    }

    override func checkError() {
        if let errorBO = errorBO {
            if !errorBO.isErrorShown {
                
                view?.showError(error: errorBO)
                
            } else if let status = errorBO.status {

                if status >= 400 && status < 500 {
                    view?.changeColorEditTexts()
                }
            }
        }
    }

    func viewDidAppear() {

        guard errorBO == nil && !animationFinished else {
            return
        }

        if !KeychainManager.shared.isUserSaved() {
            TrackerHelper.sharedInstance().trackLoginScreen(false)

            view?.showAnimation()

        } else {
            TrackerHelper.sharedInstance().trackLoginScreen(true)
            view?.showLoggedAnimation()
        }

        animationFinished = true

    }

    func canDoLoginWithLoginTransport(_ loginTransport: LoginTransport) -> Bool {

        if loginTransport.username.isEmpty || loginTransport.password.isEmpty {
            return false
        } else {
            return true
        }
    }

    func doLoginWithLoginTransport(_ loginTransport: LoginTransport) {

        busManager.navigateScreen(tag: routerTag, LoginPageReaction.ACTION_NAVIGATE_TO_LOGIN_LOADER, loginTransport)
    }

    func createLoggedViewData() -> LoginDisplayData {
        loginDisplayData = LoginDisplayData.generateLoginDisplayData()
        return loginDisplayData!
    }

    func createChangeUserViewData() -> LoginDisplayData {
        return LoginDisplayData.generateChangeUserDisplayData()
    }

    func removeAllLocalData() {
        KeychainManager.shared.resetKeyChainItems()
    }

    override func setModel(model: Model) {
        if let error = model as? ErrorBO {
            self.errorBO = error
        }
    }

    func ticketReceivedInLaunch(ticket: TicketModel) {

        busManager.showTicketDetail(withModel: ticket)
    }
}

extension LoginPresenter: LoginPresenterProtocol {

    func acceptButtonPressed() {

        if changeUserPressed {
            removeAllLocalData()
            SessionDataManager.sessionDataInstance().clearTabBarSelected()
            busManager.setRootWhenNoSession()
            SessionDataManager.sessionDataInstance().requestLogout()
            SessionDataManager.sessionDataInstance().closeSession()
            PreferencesManager.sharedInstance().resetPreferencesData()
            ConfigPublicManager.sharedInstance().resetCounters()

        } else {
            checkError()
        }

        changeUserPressed = false

    }

    func cancelButtonPressed() {
        changeUserPressed = false
    }

    func removeTextUser() {

        view?.removeTextInputUser(value: "")
        view?.showUserViewStatusNormal()

    }

    func removeTextPass() {

        view?.removeTextInputPass(value: "")
        view?.showPassViewStatusNormal()
    }

    func checkStateSecure(boolValue: Bool) {
        if boolValue == true {
            view?.showSecureHideIcon(state: false)
        } else {
            view?.showSecureVisualizeIcon(state: true)
        }
    }

    func checkEnterButtonState(loginTransport: ModelTransport) {

        let loginTransport: LoginTransport = loginTransport as! LoginTransport

        if canDoLoginWithLoginTransport(loginTransport) {
            view?.showCheckEnterButtonState(state: true)
        } else {
            view?.showCheckEnterButtonState(state: false)
        }
    }

    func startLogin() {

        if !KeychainManager.shared.isUserSaved() {
            view?.showLogin()
        } else {
             view?.showLoggedView(withDisplayData: createLoggedViewData())
        }
    }

    func checkLocationPrivileges() {
        view?.askLocationPrivileges()
    }

    func userTextfieldDidChanged() {
        view?.showUserViewStatusNormal()
    }

    func passTextfieldDidChanged() {
        view?.showPassViewStatusNormal()
    }

    func userTextInputIntroPressed() {
        view?.moveFocusToPasswordTextInput()
    }

    func loginButtonPressed(loginTransport: ModelTransport) {
        let loginTransport: LoginTransport = loginTransport as! LoginTransport
        doLoginWithLoginTransport(loginTransport)
    }

    func passwordTextInputIntroPressed(loginTransport: ModelTransport) {

        let loginTransport: LoginTransport = loginTransport as! LoginTransport

        if canDoLoginWithLoginTransport(loginTransport) {
            view?.dismissKeyboard()
            view?.showLoginAnimation()
        } else {
            view?.dismissKeyboard()
        }
    }

    func loginAnimationFinished(loginTransport: ModelTransport) {

        let loginTransport: LoginTransport = loginTransport as! LoginTransport
        
        busManager.publishData(tag: EventTracker.id, PageReactionConstants.NEW_TRACK_EVENT, Event(type: .login))
        
        doLoginWithLoginTransport(loginTransport)
    }

    func changeUserConfirmation() {

        changeUserPressed = true

        changeUserDisplayData = createChangeUserViewData()
        errorBO = ErrorBO(messageAttributed: (changeUserDisplayData?.loginDisconnectMessage!)!, errorType: .info, allowCancel: true, okTitle: (changeUserDisplayData?.okButtonTitle!)!)

        view?.showError(error: errorBO!)
    }

    func showPromotionsPressed() {
                
        busManager.publishData(tag: EventTracker.id, PageReactionConstants.NEW_TRACK_EVENT, Event(type: .showPromotions))
        
        busManager.navigateScreen(tag: routerTag, LoginPageReaction.ACTION_NAVIGATE_TO_LOGIN_LOADER, EmptyModel())
    }
}
