//
//  LoginPresenterA.swift
//  shoppingapp
//
//  Created by Javier Pino on 17/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation
import RxSwift

class LoginPresenterA<T: LoginViewProtocolA>: LoginPresenter<T> {

    var identificationDocuments: [IdentificationDocumentBO]?

    var pickerOptions: PickerOptionsDisplay?

    var interactor: LoginInteractorProtocolA?

    var selectedIdentificationDocument: IdentificationDocumentBO? {
        if let selectedIndex = pickerOptions?.selectedIndex {
            return identificationDocuments?[selectedIndex]
        }
        return nil
    }

    override func canDoLoginWithLoginTransport(_ loginTransport: LoginTransport) -> Bool {

        if KeychainManager.shared.isUserSaved() {
            if loginTransport.username.isEmpty || loginTransport.password.isEmpty {
                return false
            } else {
                return true
            }
        } else {
            if selectedIdentificationDocument == nil || loginTransport.username.isEmpty || loginTransport.password.isEmpty {
                return false
            } else {
                return true
            }
        }
    }

    override func doLoginWithLoginTransport(_ loginTransport: LoginTransport) {

        var loginTransportA: ModelTransport

        if KeychainManager.shared.isUserSaved() {
            loginTransportA = loginTransport
        } else {
            loginTransportA = LoginTransportA(documentCode: (selectedIdentificationDocument?.code)!, username: loginTransport.username, password: loginTransport.password)
        }

        busManager.navigateScreen(tag: routerTag, LoginPageReactionA.ACTION_NAVIGATE_TO_LOGIN_LOADER, loginTransportA)
    }

}

extension LoginPresenterA: LoginPresenterProtocolA {

    func viewDidLoad() {
        if let interactor = interactor {
            interactor.provideIdentificationDocuments().subscribe(
                onNext: { [weak self] result in

                    if let result = result as? [IdentificationDocumentBO] {
                        guard let `self` = self else {
                            return
                        }
                        self.identificationDocuments = result
                        self.pickerOptions = PickerOptionsDisplay()
                        self.pickerOptions?.addIdentificationDocuments(documents: self.identificationDocuments!)
                    }

                }
            ).addDisposableTo(disposeBag)
        }

    }

    func identificationDocumentPickerPressed() {

        if let pickerOptions = pickerOptions {
            busManager.showPicker(options: pickerOptions, delegate: self)
        }
    }
}

extension LoginPresenterA: PickerPresenterDelegate {

    func didSelectPickerOption(atIndex index: Int) {

        pickerOptions?.selectedIndex = index

        let identificationDocumentTitle = pickerOptions?.titles?[index]

        view?.updateIndentificationDocumentText(text: identificationDocumentTitle!)
    }
}

fileprivate extension PickerOptionsDisplay {

    func addIdentificationDocuments(documents: [IdentificationDocumentBO]) {

        titles = []

        for identificationDocument in documents {

            titles?.append(identificationDocument.title)
        }
    }
}
