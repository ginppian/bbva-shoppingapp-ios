//
//  LoginPresenterBGM.swift
//  shoppingappMX
//
//  Created by Jesús Ángel Sánchez Sánchez on 19/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import RxSwift

class LoginPresenterBGM<T: LoginViewProtocolBGM>: LoginPresenter<T> {
    
    fileprivate let userBlockedErrorCode = "162"
    var launchRegisterInGlomo = false

    override func viewDidAppear() {

        guard errorBO == nil && !animationFinished else {
            return
        }

        if !KeychainManager.shared.isUserSaved() {
            TrackerHelper.sharedInstance().trackLoginScreen(false)

            if launchRegisterInGlomo {

                launchRegisterInGlomo = false
                view?.launchRegisterInGlomo()
            } else {

                view?.showAnimation()
            }

        } else {
            TrackerHelper.sharedInstance().trackLoginScreen(true)
            view?.showLoggedAnimation()
        }

        animationFinished = true
    }

    override func checkError() {
        
        if let errorBO = errorBO {
            
            if !errorBO.isErrorShown {
                
                if errorBO.status == ConstantsHTTPCodes.STATUS_403, let errorCode = errorBO.code, errorCode == userBlockedErrorCode {
                    
                    errorBO.isErrorShown = true
                    busManager.navigateScreen(tag: LoginPageReactionB.ROUTER_TAG, LoginPageReactionB.eventNavToUserBlocked, Void())
                    
                } else {
                    view?.showError(error: errorBO)
                }
                
            } else if let status = errorBO.status {
                
                if status >= 400 && status < 500 {
                    view?.changeColorEditTexts()
                }
            }
        }
    }
}

extension LoginPresenterBGM: LoginPresenterProtocolBGM {
    
    func digitalActivationSuccess(loginTransport: LoginTransport) {
        
        doLoginWithLoginTransport(loginTransport)
    
    }

    func configureToRegisterInGlomo() {

        view?.configureToRegisterInGlomo()
        launchRegisterInGlomo = true
    }
}
