//
//  LoginInteractorA.swift
//  shoppingapp
//
//  Created by Javier Pino on 21/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation
import RxSwift

class LoginInteractorA: LoginInteractor {

    let disposeBag = DisposeBag()
}

extension LoginInteractorA: LoginInteractorProtocolA {

    func provideIdentificationDocuments() -> Observable<[ModelBO]> {

        return Observable.create { observer in

            ConfigurationDataManager.sharedInstance.provideIdentificationDocuments().subscribe(

                onNext: { result in

                    if let documentEntityArray = result as? [IdentificationDocumentEntity] {

                        var documentsArray: [IdentificationDocumentBO] = []

                        for documentEntity in documentEntityArray {
                            documentsArray.append(IdentificationDocumentBO(entity: documentEntity))
                        }

                        observer.onNext(documentsArray)
                        observer.onCompleted()
                    } else {
                        DLog(message: "Incorrect IdentificationDocumentEntity")
                    }

                }).addDisposableTo(self.disposeBag)
            return Disposables.create()
        }
    }
}

extension IdentificationDocumentBO {

    convenience init(entity: IdentificationDocumentEntity) {
        self.init(code: entity.code, title: entity.title)
    }
}
