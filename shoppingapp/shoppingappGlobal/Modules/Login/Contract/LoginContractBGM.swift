//
//  LoginContractA.swift
//  shoppingapp
//
//  Created by Javier Pino on 17/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation

protocol LoginViewProtocolBGM: LoginViewProtocol {

    func configureToRegisterInGlomo()
    func launchRegisterInGlomo()
}

protocol LoginPresenterProtocolBGM: LoginPresenterProtocol {

    func digitalActivationSuccess(loginTransport: LoginTransport)
    
}
