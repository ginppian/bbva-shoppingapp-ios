//
//  LoginContractA.swift
//  shoppingapp
//
//  Created by Javier Pino on 17/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation
import RxSwift

protocol LoginViewProtocolA: LoginViewProtocol {

    func updateIndentificationDocumentText(text: String)

}

protocol LoginPresenterProtocolA: LoginPresenterProtocol {

    func viewDidLoad()
    func identificationDocumentPickerPressed()
}

protocol LoginInteractorProtocolA: LoginInteractorProtocol {

    func provideIdentificationDocuments() -> Observable<[ModelBO]>
}
