//
//  LoginContract.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 15/3/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit
import RxSwift

protocol LoginViewProtocol: ViewProtocol {

    func showLogin()
    func showLoggedView(withDisplayData displayData: LoginDisplayData)
    func showAnimation()
    func removeTextInputUser(value: String)
    func removeTextInputPass(value: String)
    func showCheckEnterButtonState(state: Bool)
    func showSecureVisualizeIcon(state: Bool)
    func showSecureHideIcon(state: Bool)
    func askLocationPrivileges()
    func showUserViewStatusNormal()
    func showPassViewStatusNormal()
    func moveFocusToPasswordTextInput()
    func dismissKeyboard()
    func showLoginAnimation()
    func showLoggedAnimation()

}

protocol LoginPresenterProtocol: PresenterProtocol {

    func checkStateSecure(boolValue: Bool)
    func checkEnterButtonState(loginTransport: ModelTransport)
    func viewDidAppear()
    func startLogin()
    func removeTextUser()
    func removeTextPass()
    func checkLocationPrivileges()
    func userTextfieldDidChanged()
    func passTextfieldDidChanged()
    func userTextInputIntroPressed()
    func passwordTextInputIntroPressed(loginTransport: ModelTransport)
    func loginButtonPressed(loginTransport: ModelTransport)
    func loginAnimationFinished(loginTransport: ModelTransport)
    func changeUserConfirmation()
    func showPromotionsPressed()
    func acceptButtonPressed()
    func cancelButtonPressed()

}

protocol LoginInteractorProtocol {

}
