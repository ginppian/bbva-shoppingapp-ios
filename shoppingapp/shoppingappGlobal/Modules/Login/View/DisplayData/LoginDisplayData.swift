//
//  LoginDisplayData.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 10/01/2018.
//  Copyright © 2018 BBVA. All rights reserved.
//

struct LoginDisplayData {

    var greetingText: String?
    var changeUserText: String?
    var userSavedImage: String?
    var userSavedName: String?
    var okButtonTitle: String?
    var backgroundHeight: CGFloat?

    static let backgroundViewHeight: CGFloat = 440.0

    var loginDisconnectMessage: NSAttributedString?

    static func generateLoginDisplayData() -> LoginDisplayData {

        var loginDisplayData = LoginDisplayData()

        loginDisplayData.greetingText = Localizables.login.key_login_hello_text
        loginDisplayData.changeUserText = Localizables.login.key_login_disconnect_change_user_text
        loginDisplayData.userSavedImage = ConstantsImages.Login.user_saved_image
        
        if !Tools.isStringNilOrEmpty(withString: KeychainManager.shared.getFirstname()) {
            loginDisplayData.userSavedName = KeychainManager.shared.getFirstname().capitalized
        } else if KeychainManager.shared.getUserId().count > 5 {
            loginDisplayData.userSavedName = "\u{25CF}" + KeychainManager.shared.getUserId().suffix(5)
        } else {
            loginDisplayData.userSavedName = ""
        }
        
        loginDisplayData.backgroundHeight = backgroundViewHeight

        return loginDisplayData

    }
    static func generateChangeUserDisplayData() -> LoginDisplayData {

        var loginDisplayData = LoginDisplayData()

        let fullText = String(format: "%@%@ %@", KeychainManager.shared.getFirstname().capitalized, Localizables.login.key_login_disconnect_confirmation_text, Localizables.login.key_login_disconnect_message_info2_text )

        loginDisplayData.loginDisconnectMessage = CustomAttributedText.attributedBoldText(forFullText: fullText, withBoldText: Localizables.login.key_login_disconnect_message_info2_text, withFont: Tools.setFontBook(size: 14), withBoldFont: Tools.setFontBold(size: 14), withAlignment: .center, andForegroundColor: .BBVA600)
        loginDisplayData.okButtonTitle = Localizables.login.key_login_disconnect_understood_text

        return loginDisplayData

    }
}
