//
//  LoginViewControllerA.swift
//  shoppingapp
//
//  Created by Javier Pino on 13/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit
import CellsNativeCore
import CellsNativeComponents

class LoginViewControllerA: LoginViewController {

    @IBOutlet weak var documentView: UIView!
    @IBOutlet weak var documentButton: UIButton!
    @IBOutlet weak var documentTf: CustomTextField!
    @IBOutlet weak var documentArrowImageView: UIImageView!
    let backgroundViewHeight = 480.0

    override func viewDidLoad() {
        super.viewDidLoad()

        (self.presenter as! LoginPresenterProtocolA).viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - IBAction
    @IBAction func documentButtonPressed(_ sender: Any) {
        (presenter as! LoginPresenterProtocolA).identificationDocumentPickerPressed()
    }

    // MARK: - LoginViewController methods override
    override func configureView() {

        super.configureView()

        documentArrowImageView.image = SVGCache.image(forSVGNamed: ConstantsImages.Common.unfold_icon, color: .BBVAWHITE)

        setAccesibilitiesForDocument()

        documentTf.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)

        documentTf.delegate = self

        documentTf.placeholder = Localizables.login.key_login_selector_text
        documentTf.configure()
    }
}

// MARK: - LoginViewProtocolA Implementation

extension LoginViewControllerA: LoginViewProtocolA {

    func updateIndentificationDocumentText(text: String) {
        documentTf.text = text
        documentTf.sendActions(for: .editingChanged)
        documentTf.placeholderNormalColor = UIColor.white.withAlphaComponent(0.6)
    }
}

// MARK: - APPIUM

extension LoginViewControllerA {

    func setAccesibilitiesForDocument() {

        #if APPIUM

            self.view.accessibilityIdentifier = ConstantsAccessibilities.LOGIN_VIEW

            documentButton.isAccessibilityElement = false
            documentView.isAccessibilityElement = false
            documentTf.isAccessibilityElement = false

            documentTf.accessibilityIdentifier = ConstantsAccessibilities.LOGIN_DOCUMENT_INPUT
            documentButton.accessibilityIdentifier = ConstantsAccessibilities.LOGIN_DOCUMENT_BUTTON

            Tools.setAccessibility(view: documentTf.placeholderLabel, identifier: ConstantsAccessibilities.LOGIN_DOCUMENT_PLACEHOLDER)

        #endif
    }
}
