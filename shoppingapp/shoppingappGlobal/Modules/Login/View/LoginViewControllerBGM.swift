//
//  LoginViewControllerBGM.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 28/7/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit
import BGMConnectManager
import BGMConnectSDK
import BGMCore
import BGMUICatalog
import CellsNativeCore
import RxSwift

class LoginViewControllerBGM: LoginViewController, GCSSecurityUIDelegate, GCSPhoneCaller, GCSUILoader, GCSErrorModel, GCSUILogoutPresenter, GCSOnCriticalFlowStatusChange {
    
    // MARK: vars
    
    private var handlerAndRequestHelper: GCSHandlerAndRequestHelper!
    private var serverError500Handler: GCSServerError500Handler!
    private var uiErrorHandler: GCSUIErrorHandler!
    private var serverExpirationHandler: GCSServerExpirationHandler!
    private var authenticationHandler: GCSAuthenticationHandler!

    private var configureViewToRegisterInGlomo = false
    
    // MARK: life cycle
    
    override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
        self.initialize()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.initialize()
    }
    
    override func setExtraButtonTitleText() {
        
        let attributtedText = CustomAttributedText.attributedBoldText(forFullText: Localizables.login.key_login_use_bbva_wallet_text + " " + Localizables.login.key_login_register_text, withBoldText: Localizables.login.key_login_register_text, withFont: Tools.setFontBook(size: 14), withBoldFont: Tools.setFontBold(size: 14), andForegroundColor: .BBVAWHITE)
        let attributtedTextHighlighted = CustomAttributedText.attributedBoldText(forFullText: Localizables.login.key_login_use_bbva_wallet_text + " " + Localizables.login.key_login_register_text, withBoldText: Localizables.login.key_login_register_text, withFont: Tools.setFontBook(size: 14), withBoldFont: Tools.setFontBold(size: 14), andForegroundColor: .BBVAWHITE30)
        
        extraButton.setAttributedTitle(attributtedText, for: .normal)
        extraButton.setAttributedTitle(attributtedTextHighlighted, for: .highlighted)
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()

        if configureViewToRegisterInGlomo {

            animationView.frame = CGRect(x: 0, y: topImage.bounds.size.height - 49, width: 49, height: 49)
        }
    }

    // MARK: IBAction
    
    @IBAction override func extraButtonAction(_ sender: Any) {
        loginWithDigitalActivation()
    }

    // MARK: glomoconnect
    
    private func initialize() {
        
        self.handlerAndRequestHelper = GCSHandlerAndRequestHelper()
        self.serverError500Handler = GCSServerError500Handler(context: self, _handlerAndRequestHelper: self.handlerAndRequestHelper, uiLoader: self)
        self.uiErrorHandler = GCSUIErrorHandler(context: self, handlerAndRequestHelper: self.handlerAndRequestHelper, uiLoader: self, uiErrorViewPresenter: self, phoneCaller: self)
        self.serverExpirationHandler = GCSServerExpirationHandler(context: self, _handlerAndRequestHelper: self.handlerAndRequestHelper, uiLoader: self, logoutPresenter: self, uiErrorHandler: self.uiErrorHandler)
        self.authenticationHandler = GCSAuthenticationHandler(context: self, _handlerAndRequestHelper: self.handlerAndRequestHelper, uiLoader: self, uiErrorHandler: self.uiErrorHandler, security: self)
    }
    
    func handleError(error: GCSError, requestInfo: GCSRequestModel, extra: [String: Any?]? = nil, isDigitalActivation: Bool = false) -> Bool {
        
        return self.uiErrorHandler.handleError(error: error, requestInfo: requestInfo, extra: extra, isDigitalActivation: isDigitalActivation)
    }

    func checkForError(httpUrlResponse: HTTPURLResponse) -> Bool {
        if httpUrlResponse.statusCode >= GCSHTTPCodes.KNOWNERROR.rawValue {
            return true
        }
        
        return false
    }
    
    func loginWithDigitalActivation() {
        startDigitalActivation()
    }
    
    func startDigitalActivation() {
        DLog(message: "startDigitalActivation")
        
        let securityUISearcher = GCSSecurityUISearcherImpl()
        GCSSecurityUISearcher.setInstance(instance: securityUISearcher)
        
        var validations: GCSDigitalActivationValidationModel = GCSDigitalActivationValidationModel()
        validations.usernameValidationRegex = "^[0-9]+$"
        validations.usernameFormat = "2 8"
        validations.usernameLength = 10
        validations.passwordValidationRegex = "^([0-9](?<!1234|2345|3456|4567|5678|6789|0123|1111|2222|3333|4444|5555|6666|7777|8888|9999|0000))+$"
        validations.passwordMaxLength = 6
        validations.passwordMinLength = 6
        GCSController.getCore().criticalFlowDelegate = self
        GCSController.getCore().startUserActivation(validations: validations)
    }
    
    // MARK: GCSSecurityUIDelegate
    
    // Hides the loading view
    func finishLoading() {
        DLog(message: "finishLoading")
    }
    
    func startLoading() {
        DLog(message: "startLoading")
    }
    
    //OVERWRITE
    func showLogoutMessage(onCompletion: (() -> Void)?) {
        DLog(message: "showLogoutMessage")
    }
    
    func sendLogoutTag() {
        DLog(message: "sendLogoutTag")
    }
    
    func callForbidden403(error: GCSError, requestInfo: GCSRequestModel) -> Bool {
        return self.serverExpirationHandler.handle(error: error, requestInfo: requestInfo)
    }
    
    func callUnauthorized401(error: GCSError, requestInfo: GCSRequestModel, headers: [AnyHashable: Any]) -> Bool {
        return self.authenticationHandler.handle(error: error, requestInfo: requestInfo, headers: headers)
    }
    
    func callServerError500(error: GCSError, requestInfo: GCSRequestModel) -> Bool {
        return self.serverError500Handler.handle(error: error, requestInfo: requestInfo)
    }
    
    func callServerError40X(error: GCSError, requestInfo: GCSRequestModel) -> Bool {
        return handleError(error: error, requestInfo: requestInfo)
    }
    
    func callServerError50X(error: GCSError, requestInfo: GCSRequestModel) -> Bool {
        return handleError(error: error, requestInfo: requestInfo)
    }
    
    var baseHandler: BGMRequestHandler? {
        get {
            return self.handlerAndRequestHelper.baseHandler
        }
        set {
            self.handlerAndRequestHelper.baseHandler = newValue
        }
    }
    
    // MARK: GCSOnCriticalFlowStatusChange
    
    func onDeviceActivationSuccessful(tsec: String, notificationTitle: NSAttributedString?, notificationMessage: NSAttributedString?, hadErrorDownloadingToken: Bool) {
        
        DLog(message: "onDeviceActivationSuccessful")
    }
    
    func onDeviceActivationFailure(error: Int, serverError: GCSError?, errorMessage: String?, requestModel: GCSRequestModel?) {
        
        DLog(message: "onDeviceActivationFailure error: \(error)")
        
        let errorBO = ErrorBO(message: Localizables.glomo_connect.key_device_activation_error, errorType: ErrorType.warning, allowCancel: false)
        
        BusManager.sessionDataInstance.navigateScreen(tag: LoginLoaderPageReaction.ROUTER_TAG, LoginLoaderPageReaction.EVENT_ERROR_LOGIN, errorBO)
    }
    
    func onDigitalActivationSuccessful(tsec: String, notificationTitle: NSAttributedString?, notificationMessage: NSAttributedString?, hadErrorDownloadingToken: Bool) {
        
        DLog(message: "onDigitalActivationSuccessful")
        
        digitalActivationSuccess(tsec: tsec)
    }
    
    func validateLevelRaiseOnDeviceIdError(error: GCSError, requestInfo: GCSRequestModel, headers: [AnyHashable: Any]) -> Bool {
        
        DLog(message: "validateLevelRaiseOnDeviceIdError")
        
        return callUnauthorized401(error: error, requestInfo: requestInfo, headers: headers)
    }
    
    func onPasswordResetSuccessful(tsec: String, message: String) {
        DLog(message: "onPasswordResetSuccessful")
    }
    
    func onIVRActivationComplete() {
        DLog(message: "onIVRActivationComplete")
        
        let errorBO = ErrorBO(message: Localizables.glomo_connect.key_device_activation_error, errorType: ErrorType.warning, allowCancel: false)
        
        errorBO.isErrorShown = true
        
        BusManager.sessionDataInstance.navigateScreen(tag: LoginLoaderPageReaction.ROUTER_TAG, LoginLoaderPageReaction.EVENT_ERROR_LOGIN, errorBO)
    }
    
    func onNewPasswordSuccessfull() {
        DLog(message: "onNewPasswordSuccessfull")
    }
    
    func handleUnknownError() {
        DLog(message: "handleUnknownError")
        let errorBO = ErrorBO(message: Localizables.common.key_error_ocurred_try_again_text, errorType: ErrorType.warning, allowCancel: false)
        DispatchQueue.main.async {
            BusManager.sessionDataInstance.navigateScreen(tag: LoginLoaderPageReaction.ROUTER_TAG, LoginLoaderPageReaction.EVENT_ERROR_LOGIN, errorBO)
        }
    }
    
    func interceptAndHandleError(error: GCSError, requestInfo: GCSRequestModel, headers: [AnyHashable: Any]) {
        DLog(message: "interceptAndHandleError")
    }
    
    // // MARK: custom methods
    
    func digitalActivationSuccess(tsec: String?) {
        if let tsec = tsec {
            BusManager.sessionDataInstance.headerInterceptorStore?.setTsec(tsec: tsec)
        }
    }
}

extension LoginViewControllerBGM: LoginViewProtocolBGM {

    func configureToRegisterInGlomo() {

        configureViewToRegisterInGlomo = true

        configureFrames()
        showTopView(withImage: ConstantsImages.Login.welcome_image)

        inputsView.alpha = 1
        inputsView.isHidden = false
        extraButton.isHidden = false

        inputsView.layer.zPosition = 1

        sideMenuController?.isLeftViewEnabled = true
        addNavigationBarMenuButton()
    }

    func launchRegisterInGlomo() {
        
        configureViewToRegisterInGlomo = false
        loginWithDigitalActivation()
    }
}
