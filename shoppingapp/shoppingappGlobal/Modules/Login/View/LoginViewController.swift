//
//  LoginViewController.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 14/3/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit
import RxSwift
import Lottie
import Stellar
import IQKeyboardManagerSwift
import Material
import CellsNativeCore
import CellsNativeComponents
import CoreLocation

class LoginViewController: BaseViewController {

    // MARK: Constants

    static let ID: String = String(describing: LoginViewController.self)

    // MARK: Vars

    var presenter: LoginPresenterProtocol!

    var animationView = UIView()

    var locationManager: CLLocationManager?

    // MARK: Outlets

    @IBOutlet weak var welcomeContainer: UIView!
    @IBOutlet weak var imageContainer: UIView!
    @IBOutlet weak var bottomContainer: UIView!
    @IBOutlet weak var topImage: UIImageView!
    @IBOutlet weak var helloView: UIView!
    @IBOutlet weak var promotionsView: UIView!
    @IBOutlet weak var loggedPromotionsView: UIView?
    @IBOutlet weak var helloLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var accessView: UIView!
    @IBOutlet weak var accessButton: UIButton!
    @IBOutlet weak var showPromotionsButton: UIButton!
    @IBOutlet weak var showLoggedPromotionsButton: UIButton?
    @IBOutlet weak var bottomImage: UIImageView!
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var inputContainerView: UIView!

    @IBOutlet weak var userTextField: CustomTextField!
    @IBOutlet weak var passTextField: CustomTextField!
    @IBOutlet weak var userAnimationView: UIView!
    @IBOutlet weak var passAnimationView: UIView!

    @IBOutlet weak var userView: UIView!
    @IBOutlet weak var passView: UIView!
    @IBOutlet weak var inputsView: UIView!
    @IBOutlet weak var insputsImage: UIImageView!
    @IBOutlet weak var enterButton: UIButton!
    @IBOutlet weak var extraButton: UIButton!
    @IBOutlet weak var loginContainerView: UIView!

    @IBOutlet weak var loggedContainerView: UIView!
    @IBOutlet weak var changeUserButton: UIButton!
    @IBOutlet weak var greetingLabel: UILabel!
    @IBOutlet weak var rememberedUserName: UILabel!
    @IBOutlet weak var backgroundViewHeightConstraint: NSLayoutConstraint!

    var showSecureTextButton: UIButton!

    var isEditingTextfields = false

    // MARK: Life cycle

    override func viewDidLoad() {
        self.configure(appLifecycle: (UIApplication.shared.delegate as! AppDelegate).appLifeCycle!, id: LoginViewController.ID)
        
        super.viewDidLoad()

        sideMenuController?.isLeftViewEnabled = false
        presenter.startLogin()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNavigationBarTransparent()
        addNavigationBarLogo()
        presenter.checkError()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.viewDidAppear()
    }

    deinit {
        ComponentManager.remove(id: presenter.routerTag)
        DLog(message: "Deinitialized LoginViewController")
    }

    // MARK: IBAction
    
    @IBAction func extraButtonAction(_ sender: Any) {

    }
    
    @IBAction func doLoginAction(_ sender: AnyObject?) {

        userTextField.resignFirstResponder()
        passTextField.resignFirstResponder()
        
        loginAnimationWithCompletionHandler {
            self.presenter.loginAnimationFinished(loginTransport: self.getLoginTransport())
        }
        
    }

    @IBAction func enterAction(_ sender: AnyObject) {

        animateEnterImputsView {
            
            self.inputsView.layer.zPosition = 1
        }
    }

    // MARK: Custom methods
    func getLoginTransport() -> LoginTransport {

        if KeychainManager.shared.isUserSaved() {
            userTextField.text = KeychainManager.shared.getUserId()
        }

        return LoginTransport(username: userTextField.text!, password: passTextField.text!)
    }

    @objc func removeTextUser() {
        presenter.removeTextUser()
        presenter.checkEnterButtonState(loginTransport: getLoginTransport())
    }

    @objc func removeTextPass() {
        self.presenter.removeTextPass()
        self.presenter.checkEnterButtonState(loginTransport: getLoginTransport())
    }

    @objc func showTextSecure() {
        self.presenter.checkStateSecure(boolValue: passTextField.isSecureTextEntry)
    }

    func animateEnterImputsView(completion: (() -> Void)? = nil) {

        var distanceToUp = CGRect()
        distanceToUp = inputsView.frame

        inputsView.frame = CGRect(x: inputsView.frame.origin.x, y: -inputsView.frame.size.height, width: inputsView.frame.size.width, height: inputsView.frame.size.height)
        inputsView.alpha = 0
        inputsView.isHidden = false
        extraButton.isHidden = false

        inputsView.makeFrame(distanceToUp).duration(0.6).easing(.custom(0.6, 0, 0.2, 1)).makeAlpha(1.0).animate()
        bottomContainer.moveY(bottomContainer.frame.size.height).duration(0.6
            ).easing(.custom(0.6, 0, 0.2, 1)).makeAlpha(0.0).completion ({
                if let completion = completion {

                    completion()
                }
            }).animate()
    }

    @discardableResult
    func configureFrames() -> UIView {

        animationView = UIView()

        animationView.frame = CGRect(x: topImage.bounds.size.width, y: 0, width: topImage.bounds.size.width, height: topImage.frame.size.height)
        animationView.backgroundColor = UIColor.AQUA
        imageContainer.addSubview(animationView)

        let animation2View = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: 49))
        animation2View.backgroundColor = UIColor.COREBLUE
        backgroundView.addSubview(animation2View)

        return animation2View
    }

    func startAnimation() {

        let animation2View = configureFrames()

        animationView.moveX(-topImage.bounds.size.width).duration(0.52).easing(.custom(0.6, 0, 0.2, 1)).completion ({
            self.showTopView(withImage: ConstantsImages.Login.welcome_image)
        }).then().makeWidth(49).duration(0.52).easing(.custom(0.6, 0, 0.2, 1)).then().makeFrame(CGRect(x: 0, y: self.topImage.frame.size.height - 49, width: 49, height: 49)).duration(0.48).easing(.custom(0.6, 0, 0.2, 1)).animate()

        animation2View.makeWidth(backgroundView.frame.size.width).duration(0.52).easing(.custom(0.6, 0, 0.2, 1)).delay(1.3).completion({

            let rectOriginal = self.helloView.frame

            self.helloView.frame = CGRect(x: self.helloView.frame.origin.x, y: -self.helloView.frame.size.height + 49, width: self.helloView.frame.size.width, height: self.helloView.frame.size.height)

            self.bottomContainer.isHidden = false
            self.loggedPromotionsView?.isHidden = true

            self.helloView.backgroundColor = UIColor.COREBLUE

            self.view.bringSubview(toFront: self.imageContainer)

            self.helloView.makeFrame(rectOriginal).duration( 0.52).easing(.custom(0.6, 0, 0.2, 1)).animate()

            UIView.animate(withDuration: 0.52,
                           delay: 0,
                           options: UIViewAnimationOptions.curveEaseInOut,
                           animations: { () -> Void in
                            self.helloView.backgroundColor = UIColor.clear
                            self.bottomImage.alpha = 1.0

                            self.sideMenuController?.isLeftViewEnabled = true
                            self.addNavigationBarMenuButton()

            }, completion: nil)

            let rectPromotionsOriginal = self.promotionsView.frame

            self.promotionsView.frame = CGRect(x: self.promotionsView.frame.origin.x, y: self.promotionsView.frame.origin.y + self.promotionsView.frame.size.height, width: self.promotionsView.frame.size.width, height: self.promotionsView.frame.size.height)

            self.promotionsView.makeFrame(rectPromotionsOriginal).duration(0.52).easing(.custom(0.3, 0, 0.2, 1)).animate()

            let rectAccessViewOriginal = self.accessView.frame
            let rectAccessButtonOriginal = self.enterButton.frame

            self.accessView.frame = CGRect(x: self.accessView.frame.origin.x - self.accessView.frame.size.width, y: 0, width: 0, height: self.accessView.frame.size.height)

            self.enterButton.frame = CGRect(x: self.enterButton.frame.origin.x - self.enterButton.frame.size.width, y: 0, width: 0, height: self.enterButton.frame.size.height)

            self.accessView.backgroundColor = UIColor.BBVADARKLIGHTBLUE

            self.accessView.makeFrame(rectAccessViewOriginal).delay(0.2).duration(0.48).easing(.custom(0.6, 0, 0.2, 1)).animate()
            self.enterButton.makeFrame(rectAccessButtonOriginal).delay(0.4).duration(0.48).easing(.custom(0.6, 0, 0.2, 1)).completion {
                self.presenter.checkLocationPrivileges()
            }.animate()

            let rectHelloLabel = self.helloLabel.frame

            self.helloLabel.frame = CGRect(x: self.helloLabel.frame.origin.x, y: self.helloLabel.frame.origin.y + self.helloLabel.frame.size.height, width: self.helloLabel.frame.size.width, height: self.helloLabel.frame.size.height)

            self.helloLabel.makeFrame(rectHelloLabel).delay(0.2).duration(0.52).easing(.custom(0.6, 0, 0.2, 1)).animate()

            let rectDescriptionLabel = self.descriptionLabel.frame

            self.descriptionLabel.frame = CGRect(x: self.descriptionLabel.frame.origin.x, y: self.descriptionLabel.frame.origin.y - self.descriptionLabel.frame.size.height, width: self.descriptionLabel.frame.size.width, height: self.descriptionLabel.frame.size.height)

            self.descriptionLabel.makeFrame(rectDescriptionLabel).delay(0.2).duration(0.52).easing(.custom(0.6, 0, 0.2, 1)).animate()

            animation2View .removeFromSuperview()

        }).animate()

    }
    func startAnimationLoggedView() {

        let animation2View = configureFrames()

        animationView.moveX(-topImage.bounds.size.width).duration(0.52).easing(.custom(0.6, 0, 0.2, 1)).completion ({
            self.showTopView(withImage: ConstantsImages.Login.user_saved_image)
        }).then().makeWidth(49).duration(0.52).easing(.custom(0.6, 0, 0.2, 1)).then().makeFrame(CGRect(x: 0, y: self.topImage.frame.size.height - 49, width: 49, height: 49)).duration(0.48).easing(.custom(0.6, 0, 0.2, 1)).animate()

        animation2View.makeWidth(backgroundView.frame.size.width).duration(0.52).easing(.custom(0.6, 0, 0.2, 1)).delay(1.3).completion({

            let rectOriginal = self.inputContainerView.frame

            self.inputContainerView.frame = CGRect(x: self.inputContainerView.frame.origin.x, y: -self.inputContainerView.frame.size.height + 49, width: self.inputContainerView.frame.size.width, height: self.inputContainerView.frame.size.height)

            self.bottomContainer.isHidden = true
            self.loggedPromotionsView?.isHidden = false
            self.inputsView.isHidden = false
            self.loggedContainerView.isHidden = false
            self.inputContainerView.backgroundColor = UIColor.COREBLUE

            self.view.bringSubview(toFront: self.imageContainer)

            self.inputContainerView.makeFrame(rectOriginal).duration(0.52).easing(.custom(0.6, 0, 0.2, 1)).animate()

            UIView.animate(withDuration: 0.52,
                           delay: 0,
                           options: UIViewAnimationOptions.curveEaseInOut,
                           animations: { () -> Void in
                            self.inputContainerView.backgroundColor = UIColor.clear
                            self.insputsImage.alpha = 1.0

                            self.sideMenuController?.isLeftViewEnabled = true
                            self.addNavigationBarMenuButton()

            }, completion: nil)

            if let loggedPromotionsView = self.loggedPromotionsView {
                
                let rectLoggedPromotionsOriginal = loggedPromotionsView.frame
                
                loggedPromotionsView.frame = CGRect(x: loggedPromotionsView.frame.origin.x, y: loggedPromotionsView.frame.origin.y + loggedPromotionsView.frame.size.height, width: loggedPromotionsView.frame.size.width, height: loggedPromotionsView.frame.size.height)
                
                loggedPromotionsView.makeFrame(rectLoggedPromotionsOriginal).duration(0.52).easing(.custom(0.3, 0, 0.2, 1)).animate()
            }
            
            let rectAccessButtonOriginal = self.accessButton.frame

            self.accessButton.frame = CGRect(x: self.accessButton.frame.origin.x - self.accessButton.frame.size.width, y: 0, width: 0, height: self.accessButton.frame.size.height)
            self.accessButton.makeFrame(rectAccessButtonOriginal).delay(0.4).duration(0.48).easing(.custom(0.6, 0, 0.2, 1)).animate()

            let rectHelloLabel = self.greetingLabel.frame

            self.greetingLabel.frame = CGRect(x: self.greetingLabel.frame.origin.x, y: self.greetingLabel.frame.origin.y + self.greetingLabel.frame.size.height, width: self.greetingLabel.frame.size.width, height: self.greetingLabel.frame.size.height)

            self.greetingLabel.makeFrame(rectHelloLabel).delay(0.2).duration(0.52).easing(.custom(0.6, 0, 0.2, 1)).animate()

            let rectUserNameLabel = self.rememberedUserName.frame

            self.rememberedUserName.frame = CGRect(x: self.rememberedUserName.frame.origin.x, y: self.rememberedUserName.frame.origin.y - self.rememberedUserName.frame.size.height, width: self.rememberedUserName.frame.size.width, height: self.rememberedUserName.frame.size.height)

            self.rememberedUserName.makeFrame(rectUserNameLabel).delay(0.2).duration(0.52).easing(.custom(0.6, 0, 0.2, 1)).animate()

            let rectChangeUserButton = self.changeUserButton.frame

            self.changeUserButton.frame = CGRect(x: self.changeUserButton.frame.origin.x, y: self.changeUserButton.frame.origin.y - self.changeUserButton.frame.size.height, width: self.changeUserButton.frame.size.width, height: self.changeUserButton.frame.size.height)

            self.changeUserButton.makeFrame(rectChangeUserButton).delay(0.2).duration(0.52).easing(.custom(0.6, 0, 0.2, 1)).animate()

            animation2View .removeFromSuperview()

        }).animate()

    }

    func showTopView(withImage imagename: String) {
        topImage.image = UIImage(named: imagename)
    }

    func setBackgroundImage() {
        bottomImage.image = UIImage(named: ConstantsImages.Login.background)
        bottomImage.alpha = 0.0
        insputsImage.image = UIImage(named: ConstantsImages.Login.background)
    }

    func setInputsBackgroundImage() {
        insputsImage.image = UIImage(named: ConstantsImages.Login.background)
        insputsImage.alpha = 0.0
    }

    func configureView() {

        //Config Buttons
        showPromotionsButton.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Promotions.promotion, color: .BBVAWHITE), width: 16, height: 16), for: .normal)

        showPromotionsButton.contentMode = .scaleAspectFit
        showPromotionsButton.clipsToBounds = true

        let spacing = 10; // the amount of spacing to appear between image and title
        showPromotionsButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: CGFloat(spacing))
        showPromotionsButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: CGFloat(spacing), bottom: 0, right: 0)
        showPromotionsButton.setTitle(Localizables.login.key_login_promotions_text, for: .normal)

        showLoggedPromotionsButton?.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Promotions.promotion, color: .BBVAWHITE), width: 16, height: 16), for: .normal)
        showLoggedPromotionsButton?.contentMode = .scaleAspectFit
        showLoggedPromotionsButton?.clipsToBounds = true
        showLoggedPromotionsButton?.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: CGFloat(spacing))
        showLoggedPromotionsButton?.titleEdgeInsets = UIEdgeInsets(top: 0, left: CGFloat(spacing), bottom: 0, right: 0)
        showLoggedPromotionsButton?.setTitle(Localizables.login.key_login_promotions_text, for: .normal)

        showCheckEnterButtonState(state: false)

        accessButton.setBackgroundColor(color: UIColor.MEDIUMBLUE, forUIControlState: .normal)
        accessButton.setBackgroundColor(color: UIColor.DARKMEDIUMBLUE, forUIControlState: .highlighted)
        accessButton.setTitle(Localizables.login.key_login_access_button, for: .normal)

        enterButton.setBackgroundColor(color: UIColor.MEDIUMBLUE, forUIControlState: .normal)
        enterButton.setBackgroundColor(color: UIColor.DARKMEDIUMBLUE, forUIControlState: .highlighted)

        enterButton.layer.borderWidth = 1.0
        enterButton.layer.borderColor = UIColor.MEDIUMBLUE.cgColor
        enterButton.setTitle(Localizables.login.key_login_access_button, for: .normal)

        //Config TextFields
        userTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)
        passTextField.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)

        userTextField.delegate = self
        passTextField.delegate = self

        userTextField.keyboardType = Settings.Login.user_keyboard_type
        passTextField.keyboardType = Settings.Login.password_keyboard_type

        let sideSize: CGFloat = 20.0
        let padding: CGFloat = 8.0

        let removeTextButtonPass = UIButton(frame: CGRect(x: padding, y: passTextField.frame.size.height / 2 - (sideSize / 2), width: sideSize, height: sideSize))
        removeTextButtonPass.setImage(SVGCache.image(forSVGNamed: ConstantsImages.Common.close_icon, color: .BBVAWHITE), for: .normal)
        removeTextButtonPass.addTarget(self, action: #selector(removeTextPass), for: .touchUpInside)

        showSecureTextButton = UIButton(frame: CGRect(x: removeTextButtonPass.frame.origin.y + removeTextButtonPass.frame.width + padding, y: passTextField.frame.size.height / 2 - padding, width: 22, height: 16))
        showSecureTextButton.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Common.visualize_icon_image, color: .BBVAWHITE), width: 22, height: 16), for: .normal)
        showSecureTextButton.addTarget(self, action: #selector(showTextSecure), for: .touchUpInside)

        setAccessibilitiesForPass(view: removeTextButtonPass)

        let rightTextViewPass = UIView(frame: CGRect(x: -removeTextButtonPass.frame.size.width, y: 0, width: removeTextButtonPass.frame.size.width + showSecureTextButton.frame.size.width + padding * 3, height: passTextField.frame.size.height))

        rightTextViewPass.addSubview(removeTextButtonPass)
        rightTextViewPass.addSubview(showSecureTextButton)

        passTextField.rightView = rightTextViewPass
        passTextField.rightViewMode = UITextFieldViewMode.whileEditing

        let removeTextButton = UIButton(frame: CGRect(x: padding, y: userTextField.frame.size.height / 2 - (sideSize / 2), width: sideSize, height: sideSize))
        removeTextButton.setImage(SVGCache.image(forSVGNamed: ConstantsImages.Common.close_icon, color: .BBVAWHITE), for: .normal)
        removeTextButton.addTarget(self, action: #selector(removeTextUser), for: .touchUpInside)

        let removeTextView = UIView(frame: CGRect(x: -removeTextButton.frame.size.width - 15, y: 0, width: removeTextButton.frame.size.width * 2, height: userTextField.frame.size.height))

        setAccessibilitiesForUser(view: removeTextButton)

        removeTextView.addSubview(removeTextButton)

        userTextField.rightView = removeTextView
        userTextField.rightViewMode = UITextFieldViewMode.whileEditing

        userTextField.placeholder = Localizables.login.key_login_user_text
        userTextField.configure()

        passTextField.placeholder = Localizables.login.key_login_password_text
        passTextField.configure()

        helloLabel.text = Localizables.login.key_login_hello_text
        descriptionLabel.text = Localizables.login.key_login_usewallet_text
        setExtraButtonTitleText()

        greetingLabel.textColor = .BBVAWHITE
        greetingLabel.font = Tools.setFontBold(size: 40)
        greetingLabel.textAlignment = .center

        rememberedUserName.textColor = .BBVAWHITE
        rememberedUserName.font = Tools.setFontBook(size: 16)
        rememberedUserName.textAlignment = .center

        changeUserButton.tintColor = .BBVAWHITE
        changeUserButton.contentMode = .scaleAspectFit
        changeUserButton.setTitleColor(.BBVAWHITE, for: .normal)
        changeUserButton.titleLabel?.font = Tools.setFontMedium(size: 14.0)

    }
    
    func setExtraButtonTitleText() {
        
        extraButton.setTitle(Localizables.login.key_login_recover_text, for: .normal)
        extraButton.setTitle(Localizables.login.key_login_recover_text, for: .highlighted)
    }

    func loginAnimationWithCompletionHandler(_ completionHandler: @escaping () -> Void) {

        insputsImage.makeFrame(CGRect(x: 0, y: insputsImage.frame.origin.y - imageContainer.frame.size.height, width: insputsImage.frame.size.width, height: welcomeContainer.frame.size.height)).duration(0.52).easing(.custom(0.6, 0, 0.2, 1)).animate()

       imageContainer.moveY(-imageContainer.frame.size.height).duration(0.52).easing(.custom(0.6, 0, 0.2, 1)).completion({

            self.imageContainer.frame = CGRect(x: 0, y: 0, width: self.imageContainer.frame.size.width, height: self.imageContainer.frame.size.height)
            self.insputsImage.frame = CGRect(x: 0, y: 0, width: self.insputsImage.frame.size.width, height: self.inputsView.frame.size.height)

            completionHandler()
        }).animate()
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String, withNumberMaximumCharacters maximumLenth: Int) -> Bool {

        let currentString = textField.text ?? ""
        let castedNSString = currentString as NSString?
        let newString = castedNSString?.replacingCharacters(in: range, with: string)

        return (newString?.count)! <= maximumLenth
    }

    func shouldChangeCharactersIn(string: String, forCharacterSet characterSet: CharacterSet) -> Bool {

        let aSet = characterSet.inverted
        let compSepByCharInSet = string.components(separatedBy: aSet)
        let numberFiltered = compSepByCharInSet.joined(separator: "")

        return string == numberFiltered
    }

    @IBAction func showPromotions(_ sender: AnyObject) {
        presenter.showPromotionsPressed()
    }

    @IBAction func changeUserAction(_ sender: AnyObject) {
        presenter.changeUserConfirmation()
    }

    override func initModel(withModel model: Model) {
        presenter.setModel(model: model)
    }

}

// MARK: UITextFieldDelegate implementation

extension LoginViewController: UITextFieldDelegate {

    func textFieldDidBeginEditing(_ textField: UITextField) {

        if userTextField == textField {
            dynamicAccessibility(forTextfield: userTextField, withVisibility: false)
            textFieldStartEditingAnimation(forAnimationView: userAnimationView, inParentView: userView)
        } else if passTextField == textField {
            dynamicAccessibility(forTextfield: passTextField, withVisibility: false)
            textFieldStartEditingAnimation(forAnimationView: passAnimationView, inParentView: passView)
        }
    }

    func textFieldDidEndEditing(_ textField: UITextField) {

        if userTextField == textField {
            dynamicAccessibility(forTextfield: userTextField, withVisibility: true)
            textEndEditingAnimation(forAnimationView: userAnimationView, inParentView: userView)
            userTextField.didEndEditing()

        } else if passTextField == textField {
            dynamicAccessibility(forTextfield: passTextField, withVisibility: true)
            textEndEditingAnimation(forAnimationView: passAnimationView, inParentView: passView)
            passTextField.didEndEditing()
        }
    }

    @objc func textFieldDidChange(_ textField: UITextField) {

        presenter.checkEnterButtonState(loginTransport: getLoginTransport())

        if userTextField == textField {
            presenter.userTextfieldDidChanged()
        } else if passTextField == textField {
            presenter.passTextfieldDidChanged()
        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

        if userTextField == textField {
            presenter.userTextInputIntroPressed()
        } else if passTextField == textField {
            presenter.passwordTextInputIntroPressed(loginTransport: getLoginTransport())
        }

        return true
    }

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        var shouldChangeCharactersInRange = shouldChangeCharactersIn(string: string, forCharacterSet: Settings.Login.user_valid_character_set) && shouldChangeCharactersIn(string: string, forCharacterSet: Settings.Login.password_valid_character_set)

        if textField == userTextField, let maximumLength = Settings.Login.user_maximum_length {

            shouldChangeCharactersInRange = self.textField(textField, shouldChangeCharactersIn: range, replacementString: string, withNumberMaximumCharacters: maximumLength) && shouldChangeCharactersIn(string: string, forCharacterSet: Settings.Login.user_valid_character_set)

        } else if textField == passTextField, let maximumLength = Settings.Login.password_maximum_length {

            shouldChangeCharactersInRange = self.textField(textField, shouldChangeCharactersIn: range, replacementString: string, withNumberMaximumCharacters: maximumLength) && shouldChangeCharactersIn(string: string, forCharacterSet: Settings.Login.password_valid_character_set)
        }

        return shouldChangeCharactersInRange
    }

}

// MARK: Keyboard management

extension LoginViewController {

    func textFieldStartEditingAnimation(forAnimationView anitmationView: UIView, inParentView parentView: UIView) {

        anitmationView.isHidden = false

        let rectViewAnimate = CGRect(x: 0,
                                          y: anitmationView.frame.origin.y,
                                          width: parentView.frame.size.width,
                                          height: anitmationView.frame.size.height)

        let deadlineTime = DispatchTime.now() + .milliseconds(300)
        DispatchQueue.main.asyncAfter(deadline: deadlineTime) {

            anitmationView.makeFrame(rectViewAnimate).duration(0.3).easing(TimingFunctionType.easeInEaseOut).animate()

        }

    }

    func textEndEditingAnimation(forAnimationView anitmationView: UIView, inParentView parentView: UIView) {

        let rectViewAnimate = CGRect(x: parentView.frame.size.width / 2,
                                          y: anitmationView.frame.origin.y,
                                          width: 0,
                                          height: anitmationView.frame.size.height)

        let deadlineTime = DispatchTime.now() + .milliseconds(300)
        DispatchQueue.main.asyncAfter(deadline: deadlineTime) {

            anitmationView.makeFrame(rectViewAnimate).duration(0.3).easing(TimingFunctionType.easeInEaseOut).completion ({
                anitmationView.isHidden = true
            }).animate()

        }
    }

}

// MARK: LoginViewProtocol implementation

extension LoginViewController: LoginViewProtocol {

    func showLoggedView(withDisplayData displayData: LoginDisplayData) {

        setAccessibilities()
        extraButton.isHidden = !Settings.Login.showRememberPassword
        loginContainerView.isHidden = true
        setInputsBackgroundImage()
        configureView()

        greetingLabel.text = displayData.greetingText
        changeUserButton.setTitle(displayData.changeUserText, for: .normal)
        rememberedUserName.text = displayData.userSavedName
        backgroundViewHeightConstraint.constant = CGFloat(displayData.backgroundHeight!)
    }

    func showLogin() {
        
        setAccessibilities()
        setBackgroundImage()
        configureView()
    }

    func showAnimation() {
        startAnimation()
    }
    func showLoggedAnimation() {
        startAnimationLoggedView()
    }

    func removeTextInputUser(value: String) {
        userTextField.text = value
    }

    func removeTextInputPass(value: String) {
        passTextField.text = value
    }

    func showCheckEnterButtonState(state: Bool) {
        accessButton.isEnabled = state
        if state {
            accessButton.alpha = 1
        } else {
            accessButton.alpha = 0.3
        }
    }

    func showSecureVisualizeIcon(state: Bool) {
        passTextField.isSecureTextEntry = state
        showSecureTextButton.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Common.visualize_icon_image, color: .BBVAWHITE), width: 22, height: 16), for: .normal)
    }

    func showSecureHideIcon(state: Bool) {
        passTextField.isSecureTextEntry = state
        showSecureTextButton.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Login.hide_icon, color: .BBVAWHITE), width: 22, height: 16), for: .normal)
    }

    func askLocationPrivileges() {
        askGeolocationPrivileges()
    }

    func showUserViewStatusNormal() {
        userView.backgroundColor = .COREBLUE
    }

    func showPassViewStatusNormal() {
        passView.backgroundColor = .COREBLUE
    }

    func moveFocusToPasswordTextInput() {
        userTextField.resignFirstResponder()
        passTextField.becomeFirstResponder()
    }

    func dismissKeyboard() {
        userTextField.resignFirstResponder()
        passTextField.resignFirstResponder()
    }

    func showLoginAnimation() {

        loginAnimationWithCompletionHandler {
            self.presenter.loginAnimationFinished(loginTransport: self.getLoginTransport())
        }

    }

    func askGeolocationPrivileges() {
        if CLLocationManager.locationServicesEnabled() {
            if CLLocationManager.authorizationStatus() == .notDetermined {
                locationManager = CLLocationManager()
                locationManager?.delegate = self
                locationManager?.requestWhenInUseAuthorization()
            }
        }
    }
}

extension LoginViewController: ViewProtocol {

    func showError(error: ModelBO) {
        self.presenter.showModal()
    }

    func changeColorEditTexts() {
        self.userView.backgroundColor = .BBVALIGHTRED
        self.passView.backgroundColor = .BBVALIGHTRED
    }

}

extension LoginViewController: CLLocationManagerDelegate {

    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if status == .authorizedWhenInUse {
            TrackerHelper.sharedInstance().startFindCountry()
        }
    }

}

extension LoginViewController: CloseModalDelegate {

    func errorViewControllerAcceptPressed(_ errorViewController: ErrorViewController) {
        presenter.acceptButtonPressed()
    }

    func errorViewControllerCancelPressed(_ errorViewController: ErrorViewController) {
        presenter.cancelButtonPressed()
    }

}

// MARK: - APPIUM

private extension LoginViewController {

    func setAccessibilities() {

        #if APPIUM
        self.userView.isAccessibilityElement = false
        self.passView.isAccessibilityElement = false

        self.view.accessibilityIdentifier = ConstantsAccessibilities.LOGIN_VIEW
        Tools.setAccessibility(view: helloLabel, identifier: ConstantsAccessibilities.LOGIN_HELLO_LABEL)
        Tools.setAccessibility(view: descriptionLabel, identifier: ConstantsAccessibilities.LOGIN_DESCRIPTION_LABEL)
        Tools.setAccessibility(view: accessButton, identifier: ConstantsAccessibilities.LOGIN_ACCESS_BUTTON)
        Tools.setAccessibility(view: showPromotionsButton, identifier: ConstantsAccessibilities.LOGIN_SHOW_PROMOTIONS_BUTTON)
        if let showLoggedPromotionsButton = showLoggedPromotionsButton {
            Tools.setAccessibility(view: showLoggedPromotionsButton, identifier: ConstantsAccessibilities.LOGIN_SHOW_PROMOTIONS_BUTTON)
        }
        Tools.setAccessibility(view: enterButton, identifier: ConstantsAccessibilities.LOGIN_ENTER_BUTTON)
        Tools.setAccessibility(view: extraButton, identifier: ConstantsAccessibilities.LOGIN_REMEMBER_PASSWORD)

        Tools.setAccessibility(view: userTextField, identifier: ConstantsAccessibilities.LOGIN_USER_TEXTFIELD)
        Tools.setAccessibility(view: passTextField, identifier: ConstantsAccessibilities.LOGIN_PASS_TEXTFIELD)

        Tools.setAccessibility(view: userTextField.placeholderLabel, identifier: ConstantsAccessibilities.LOGIN_USER_PLACEHOLDER)
        Tools.setAccessibility(view: passTextField.placeholderLabel, identifier: ConstantsAccessibilities.LOGIN_PASS_PLACEHOLDER)

        Tools.setAccessibility(view: greetingLabel, identifier: ConstantsAccessibilities.LOGIN_HELLO_LABEL)
        Tools.setAccessibility(view: rememberedUserName, identifier: ConstantsAccessibilities.LOGIN_USER_NAME_LABEL)
        Tools.setAccessibility(view: changeUserButton, identifier: ConstantsAccessibilities.LOGIN_CHANGE_USER_BUTTON)

        #endif

    }

    func setAccessibilitiesForPass(view: UIView) {

        #if APPIUM
        Tools.setAccessibility(view: view, identifier: ConstantsAccessibilities.LOGIN_REMOVE_TEXT_PASS)
        Tools.setAccessibility(view: showSecureTextButton, identifier: ConstantsAccessibilities.LOGIN_SHOW_SECURE_TEXT)
        #endif

    }

    func setAccessibilitiesForUser(view: UIView) {

        #if APPIUM
            Tools.setAccessibility(view: view, identifier: ConstantsAccessibilities.LOGIN_REMOVE_TEXT_USER)
        #endif

    }

    func dynamicAccessibility(forTextfield textfield: UITextField, withVisibility visibility: Bool) {
         #if APPIUM
        textfield.isAccessibilityElement = visibility
        #endif
    }

}
