//
//  LoginPageReactionB.swift
//  shoppingappMX
//
//  Created by Jesús Ángel Sánchez Sánchez on 19/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

class LoginPageReactionB: LoginPageReaction {
    
    static let eventNavToUserBlocked: ActionSpec<Void> = ActionSpec<Void>(id: "eventNavToUserBlocked")
    
    static let cellsPageUserBlocked = "userBlocked"
    
    override func configure() {
        
        super.configure()
        
        registerToNavigateToUserBloqued()
        registerToReactToRegisterInGlomoChannel()
    }
    
    func registerToNavigateToUserBloqued() {
        
        _ = when(id: LoginPageReactionB.ROUTER_TAG, type: Void.self)
            .doAction(actionSpec: LoginPageReactionB.eventNavToUserBlocked)
            .then()
            .doWebNavigation(webRoute: LoginPageReactionB.cellsPageUserBlocked, nativeRoute: Route(viewController: { () -> UIViewController in
                return (BusManager.sessionDataInstance.routerFactory?.getViewController(route: UserBlockedViewController.id))!
            }, animationToNext: { _, destinationViewController in
                
                destinationViewController?.modalPresentationStyle = .overCurrentContext
                
                if let destinationViewController = destinationViewController {
                    let navigationController = UINavigationController(rootViewController: destinationViewController)
                    BusManager.sessionDataInstance.present(viewController: navigationController, animated: true, completion: nil)
                }
                
            }, animationToPrevious: { _, _ in
                
            }
            ))
    }

    override func reacToTicketReceivedChannel() {

        _ = reactTo(channel: PageReactionConstants.CHANNEL_TICKET_RECEIVED_IN_BACKGROUND)?
            .then()
            .inside(componentID: LoginPageReaction.ROUTER_TAG, component: LoginPresenterBGM<LoginViewControllerBGM>.self)
            .doReactionAndClearChannel(reactionExecution: { presenter, ticketModel in

                if let ticket = ticketModel as? TicketModel {
                    presenter.ticketReceivedInLaunch(ticket: ticket)
                }
            })
    }

    func registerToReactToRegisterInGlomoChannel () {

        _ = reactTo(channel: AnonymousTutorialPageReactionB.CONFIGURE_LOGIN_TO_REGISTER_IN_GLOMO_CHANNEL)?
            .then()
            .inside(componentID: LoginPageReaction.ROUTER_TAG, component: LoginPresenterBGM<LoginViewControllerBGM>.self)
            .doReactionAndClearChannel(reactionExecution: { presenter, _ in
                
                presenter.configureToRegisterInGlomo()
            })
    }
    
}
