//
//  LoginPageReaction.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 25/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import CellsNativeCore
import CellsNativeComponents

class LoginPageReaction: PageReaction {

    static let ROUTER_TAG: String = "login-tag"

    static let ACTION_NAME: ActionSpec<String> = ActionSpec<String>(id: "Button tapped")
    static let ACTION_NAVIGATE_TO_LOGIN_LOADER: ActionSpec<Model> = ActionSpec<Model>(id: "ACTION_NAVIGATE_TO_LOGIN_LOADER")

    static let EVENT_ERROR_LOGIN: ActionSpec<String> = ActionSpec<String>(id: "EVENT_ERROR_LOGIN")

    override func configure() {

        navigateFirstScreenFromLogin()
        reacToTicketReceivedChannel()
    }

    func navigateFirstScreenFromLogin() {

        _ = when(id: LoginPageReaction.ROUTER_TAG, type: Model.self)
            .doAction(actionSpec: LoginPageReaction.ACTION_NAVIGATE_TO_LOGIN_LOADER)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, model in

                router.navigateFirstScreenFromLogin(withModel: model!)

            }
    }

    func reacToTicketReceivedChannel() {

        _ = reactTo(channel: PageReactionConstants.CHANNEL_TICKET_RECEIVED_IN_BACKGROUND)?
            .then()
            .inside(componentID: LoginPageReaction.ROUTER_TAG, component: LoginPresenter<LoginViewController>.self)
            .doReactionAndClearChannel(reactionExecution: { presenter, ticketModel in

                if let ticket = ticketModel as? TicketModel {
                    presenter.ticketReceivedInLaunch(ticket: ticket)
                }
            })
    }
}
