//
//  ViewController.swift
//  CellsPagesDemo
//
//  Created by Intel on 3/3/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit
import CellsNativeCore
import CellsNativeComponents

class SplashViewController: BaseViewController {

    @IBOutlet weak var logoContainerView: UIView!
    
    // MARK: Constants

    static let ID: String = String(describing: SplashViewController.self)

    var presenter: SplashPresenterProtocol!
    var animationView: LOTAnimationView?

    override func viewDidLoad() {
        self.configure(appLifecycle: (UIApplication.shared.delegate as! AppDelegate).appLifeCycle!, id: SplashViewController.ID)
        super.viewDidLoad()

        loadAnimationViewInContainer()
        
        presenter.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setNavigationBarTransparent()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        presenter.viewDidAppear()
    }

    deinit {
        ComponentManager.remove(id: presenter.routerTag)
        DLog(message: "Deinitialized SplashViewController")
    }
    
    private func loadAnimationViewInContainer() {
        
        let lottieAnimationView = LOTAnimationView(name: ConstantsLottieAnimations.walletSplashLogo)
        lottieAnimationView.frame = logoContainerView.bounds
        lottieAnimationView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        lottieAnimationView.contentMode = .scaleAspectFit
        logoContainerView.addSubview(lottieAnimationView)
        
        animationView = lottieAnimationView
    }
}

extension SplashViewController: SplashViewProtocol {
    
    func startAnimation() {
        
        animationView?.play(completion: { [weak self] animationFinished in
            
            if animationFinished {
                
                self?.presenter.animationFinished()
            }
        })
    }

    func showError(error: ModelBO) {
    }

    func changeColorEditTexts() {
    }
}
