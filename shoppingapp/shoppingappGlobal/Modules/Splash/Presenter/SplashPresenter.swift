//
//  SplashPresenter.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 24/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import RxSwift
import CellsNativeComponents

class SplashPresenter<T: SplashViewProtocol>: BasePresenter<T> {

    let disposeBag = DisposeBag()
    var splashAnimationFinished = false
    var publicConfiguration: PublicConfigurationDTO?
    var webIsReady = false

    required init() {
        
        super.init(tag: SplashPageReaction.ROUTER_TAG)
    }

    override init(busManager: BusManager) {

        super.init(busManager: busManager)
        routerTag = SplashPageReaction.ROUTER_TAG
    }
    
    private func goToNextScreen() {
        
        guard let publicConfiguration = publicConfiguration, splashAnimationFinished, webIsReady else {
            return
        }
        
        VersionControlManager.sharedInstance().checkVersion(withPublicConfigurationDTO: publicConfiguration, shouldShowNoMandatoryUpdate: true)
        
        if (PreferencesManager.sharedInstance().getValue(forKey: PreferencesManagerKeys.kWelcomeShowed.rawValue) as? Bool) == true {
            
            busManager.navigateScreen(tag: routerTag, SplashPageReaction.EVENT_NAV_TO_LOGIN_SCREEN, Void())
        } else {
            
            busManager.navigateScreen(tag: routerTag, SplashPageReaction.EVENT_NAV_TO_WELCOME, Void())
        }
    }
    
    private func getPublicConfiguration() {
        
        let publicConfigurationObservable = PublicConfigurationManager.sharedInstance().getPublicConfiguration()
        
        publicConfigurationObservable
            .take(1)
            .subscribe( onNext: { [weak self] publicConfigurationDTO in
                
                self?.publicConfiguration = publicConfigurationDTO
                self?.goToNextScreen()
                
            }).addDisposableTo(disposeBag)
    }
    
    func notifyWebIsReady() {
        
        webIsReady = true
        goToNextScreen()
    }
}

extension SplashPresenter: SplashPresenterProtocol {

    func viewDidLoad() {
        
        getPublicConfiguration()
    }
    
    func viewDidAppear() {
        
        view?.startAnimation()
    }
    
    func animationFinished() {
        
        splashAnimationFinished = true
        goToNextScreen()
    }
}
