//
//  SplashPageReaction.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 23/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

class SplashPageReaction: PageReaction {

    static let ROUTER_TAG: String = "splash-tag"

    static let EVENT_NAV_TO_WELCOME: ActionSpec<Void> = ActionSpec<Void>(id: "EVENT_NAV_TO_WELCOME")

    static let EVENT_NAV_TO_LOGIN_SCREEN: ActionSpec<Void> = ActionSpec<Void>(id: "EVENT_NAV_TO_LOGIN_SCREEN")

    override func configure() {

        navigateWelcome()
        navigateLogin()
        reactToWhenWebReady()
    }

    func navigateLogin() {

        _ = when(id: SplashPageReaction.ROUTER_TAG, type: Void.self)
            .doAction(actionSpec: SplashPageReaction.EVENT_NAV_TO_LOGIN_SCREEN)
            .then()
            .inside(componentID: SplashViewController.ID, component: SplashViewController.self)
            .doReaction { _, _ in
                
                BusManager.sessionDataInstance.setRootWhenNoSession()
        }
    }

    func navigateWelcome() {

        _ = when(id: SplashPageReaction.ROUTER_TAG, type: Void.self)
            .doAction(actionSpec: SplashPageReaction.EVENT_NAV_TO_WELCOME)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, _ in
                
                router.navigateToWelcome()
        }
    }
    
    func reactToWhenWebReady() {
        
        _ = reactTo(channel: WebController.ON_WEB_READY_CHANNEL)?
            .then()
            .inside(componentID: SplashPageReaction.ROUTER_TAG, component: SplashPresenter<SplashViewController>.self)
            .doReactionAndClearChannel { presenter, _ in
                
                presenter.notifyWebIsReady()
        }
    }
}
