//
//  SplashContract.swift
//  mybusiness
//
//  Created by daniel.petrovic on 26/10/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation

protocol SplashViewProtocol: ViewProtocol {

    func startAnimation()
}

protocol SplashPresenterProtocol: PresenterProtocol {

    func viewDidLoad()
    func viewDidAppear()
    func animationFinished()
}
