//
//  BVARewardsContract.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 15/3/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit

protocol RewardsViewProtocol: ViewProtocol {

    func showDataWithRewards(withRewardsDisplayItems rewardsDisplayItems: [RewardDisplayData])
    func doAnimationRewards()
    func sendScreenOmniture()
    func showEmptyView()
    func showHelp(withDisplayData displayData: HelpDisplayData)
    func showRewardBreakDown(withModel model: RewardBreakDownModelProtocol)
    func showExpiredSkeleton(atIndex index: Int)
    func hideExpiredSkeleton(atIndex index: Int)
    func showExpiredInfo(points: Int, date: Date, atIndex index: Int)

}

protocol RewardsPresenterProtocol: PresenterProtocol {

    func viewWillAppear()
    func viewWillAppearAfterDismiss()
    func setAnimationDone()
    func moreInfoButtonPressed(atIndex index: Int)
    func emptyMoreInfoButtonPressed()
    func showedRewards(atIndex index: Int)
    func helpButtonPressed(withHelpId helpId: String)
}
