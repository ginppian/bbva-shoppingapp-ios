//
//  RewardsPageReaction.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 26/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

class RewardsPageReaction: PageReaction {

    static let ROUTER_TAG: String = "rewards-tag"

    static let EVENT_NAV_TO_DETAIL_HELP: ActionSpec<Model> = ActionSpec<Model>(id: "EVENT_NAV_TO_DETAIL_HELP")
    static let EVENT_CHANGE_TAB_TO_PROMOTIONS: ActionSpec<Void> = ActionSpec<Void>(id: "EVENT_CHANGE_TAB_TO_PROMOTIONS")

    override func configure() {

        registerForNavigateToHelpDetail()
        reactCards()
        registerForChangeTabToPromotions()
    }

    func registerForNavigateToHelpDetail() {

        _ = when(id: RewardsPageReaction.ROUTER_TAG, type: Model.self)
            .doAction(actionSpec: RewardsPageReaction.EVENT_NAV_TO_DETAIL_HELP)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, model in

                router.navigateToDetailHelp(withModel: model!)
            }
    }

    func reactCards() {

        _ = reactTo(channel: PageReactionConstants.CHANNEL_CARDS)?
            .then()
            .inside(componentID: RewardsPageReaction.ROUTER_TAG, component: RewardsPresenter<RewardsViewController>.self)
            .doReaction { component, param in

                if let param = param {
                    component.receive(model: param)
                }
            }
    }

    func registerForChangeTabToPromotions() {

        _ = when(id: RewardsPageReaction.ROUTER_TAG, type: Void.self)
            .doAction(actionSpec: RewardsPageReaction.EVENT_CHANGE_TAB_TO_PROMOTIONS)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, _ in

                router.changeTabToPromotions()
        }
    }

}
