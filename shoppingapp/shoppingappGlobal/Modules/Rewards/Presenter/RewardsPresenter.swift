//
//  BVACardPresenter.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 7/3/17.
//  Copyright © 2017 daniel.petrovic. All rights reserved.
//

import Foundation
import RxSwift
import CellsNativeComponents

class RewardsPresenter<T: RewardsViewProtocol>: BasePresenter<T> {

    let EVENT_IMAGE_CARD_DOWNLOAD = "card-to-reload-image"

    var cards: CardsBO?
    var displayDataItems = [RewardDisplayData]()
    var balanceRewardsByCardId = [String: BalanceRewardsBO]()
    var cardIdsOfBalanceRewardsWithError = [String]()
    let helpPointsId = "helpPoints"

    override init(busManager: BusManager) {

        super.init(busManager: busManager)
    }

    required init() {

        super.init(tag: RewardsPageReaction.ROUTER_TAG)
    }

    func receive(model: Model) {

        if let cardsReceived = model as? CardsBO {
            if cardsReceived.timestamp != cards?.timestamp {
                cards = cardsReceived
                showRewards()
            }
        }

        if cards == nil {
            view?.showEmptyView()
        }
    }

    fileprivate func showRewards() {

        guard let cards = cards?.cards, !cards.isEmpty else {
            view?.showEmptyView()
            return
        }

        displayDataItems.removeAll()
        
        for card in cards {
            if let reward = RewardDisplayData(withCardBO: card) {
                displayDataItems.append(reward)
            }
        }

        if !displayDataItems.isEmpty {

            view?.showDataWithRewards(withRewardsDisplayItems: displayDataItems)

            if !SessionDataManager.sessionDataInstance().isShowRewardsAnimation && displayDataItems.count > 1 {
                view?.doAnimationRewards()
            }

            if !ConfigPublicManager.sharedInstance().getConfigBOBySection(forKey: Constants.SECTION_TO_SHOW_REWARDS).isEmpty {

                let helpDisplayItems = HelpDisplayData.generateHelpDisplayData(withConfigBO: ConfigPublicManager.sharedInstance().getConfigBO(forSection: Constants.SECTION_TO_SHOW_REWARDS, withKey: PreferencesManagerKeys.kIndexToShowRewards)!, isFromSection: true)

                view?.showHelp(withDisplayData: helpDisplayItems)

            }

        } else {

            view?.showEmptyView()

        }
    }

}

extension RewardsPresenter: RewardsPresenterProtocol {

    func viewWillAppear() {

        view?.sendScreenOmniture()
    }
    
    func viewWillAppearAfterDismiss() {
        
        view?.sendScreenOmniture()
    }

    func setAnimationDone() {

        SessionDataManager.sessionDataInstance().isShowRewardsAnimation = true
    }

    func showedRewards(atIndex index: Int) {
        
        if index < displayDataItems.count, let cardId = displayDataItems[index].cardBO?.cardId {
            let rewardModel = RewardBreakDownModel(cardId: cardId, balanceRewards: balanceRewardsByCardId[cardId], showError: cardIdsOfBalanceRewardsWithError.contains(cardId))
            view?.showRewardBreakDown(withModel: rewardModel)
        }
    }

    func helpButtonPressed(withHelpId helpId: String) {

        busManager.navigateScreen(tag: RewardsPageReaction.ROUTER_TAG, RewardsPageReaction.EVENT_NAV_TO_DETAIL_HELP, HelpDetailTransport(helpId))
    }

    func moreInfoButtonPressed(atIndex index: Int) {
        if index < displayDataItems.count {
            let displayData = displayDataItems[index]
            if displayData.emptyNonMonetaryValue {

                helpButtonPressed(withHelpId: helpPointsId)
            } else {

                busManager.navigateScreen(tag: RewardsPageReaction.ROUTER_TAG, RewardsPageReaction.EVENT_CHANGE_TAB_TO_PROMOTIONS, Void())
            }
        }
    }

    func emptyMoreInfoButtonPressed() {
        helpButtonPressed(withHelpId: helpPointsId)
    }

}

extension RewardsPresenter: RewardBreakDownPresenterDelegate {
    
    func rewardBreakDownPresenter(_ rewardBreakDownPresenter: RewardBreakDownPresenter, willRetrieveBalanceForCardId cardId: String) {
        
        if let index = cardIdsOfBalanceRewardsWithError.index(of: cardId) {
            cardIdsOfBalanceRewardsWithError.remove(at: index)
        }
        
        for (index, displayDataItem) in displayDataItems.enumerated() {
            
            if let cardBO = displayDataItem.cardBO,
                cardBO.cardId == cardId {
                view?.showExpiredSkeleton(atIndex: index)
            }
        }
    }

    func rewardBreakDownPresenter(_ rewardBreakDownPresenter: RewardBreakDownPresenter, cardId: String, retrieveError: ErrorBO) {

        cardIdsOfBalanceRewardsWithError.append(cardId)
        
        errorBO = retrieveError

        if retrieveError.sessionExpired() {
            checkError()
        }
        
        for (index, displayDataItem) in displayDataItems.enumerated() {

            if let cardBO = displayDataItem.cardBO,
                cardBO.cardId == cardId {
                view?.hideExpiredSkeleton(atIndex: index)
            }
        }
    }

    func rewardBreakDownPresenter(_ rewardBreakDownPresenter: RewardBreakDownPresenter, cardId: String, retrieveBalanceRewards: BalanceRewardsBO) {

        balanceRewardsByCardId[cardId] = retrieveBalanceRewards
        
        if let index = cardIdsOfBalanceRewardsWithError.index(of: cardId) {
            cardIdsOfBalanceRewardsWithError.remove(at: index)
        }
        
        for (index, displayDataItem) in displayDataItems.enumerated() {

            if let cardBO = displayDataItem.cardBO,
                cardBO.cardId == cardId {

                if let nonMonetaryValueExpirationDate = retrieveBalanceRewards.nonMonetaryValueExpirationDate,
                let nonMonetaryValueCloseToExpired = retrieveBalanceRewards.nonMonetaryValueCloseToExpired, nonMonetaryValueCloseToExpired > 0 {
                    view?.showExpiredInfo(points: nonMonetaryValueCloseToExpired, date: nonMonetaryValueExpirationDate, atIndex: index)
                }
                view?.hideExpiredSkeleton(atIndex: index)
                return
            }
        }
    }
}
