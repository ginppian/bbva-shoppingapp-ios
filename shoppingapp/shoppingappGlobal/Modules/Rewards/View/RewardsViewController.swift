//
//  RewardsViewController.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 8/3/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit
import CellsNativeComponents

class RewardsViewController: BaseViewController {

    // MARK: Constants

    static let ID: String = String(describing: RewardsViewController.self)

    var presenter: RewardsPresenterProtocol!

    static let minimumScaleCells: CGFloat = 0.725
    static let paddingCPoints: CGFloat = 15.0
    var itemDisplayedIndex: Int = 0

    // MARK: outlets
    @IBOutlet weak var pointsInfoContainerStackView: UIStackView!
    @IBOutlet weak var pagerView: FSPagerView!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var backView: UIView!
    @IBOutlet weak var moreInfoButton: UIButton!

    @IBOutlet weak var rewardBreakDownContainerView: UIView!
    @IBOutlet weak var rewardBreakDownView: RewardBreakDownView! {
        didSet {
            rewardBreakDownView.delegate = self
            rewardBreakDownView.presenter.delegate = presenter as? RewardBreakDownPresenterDelegate
        }
    }

    @IBOutlet weak var rewardBreakDownHeightConstraint: NSLayoutConstraint!

    @IBOutlet weak var helpView: HelpView!

    @IBOutlet weak var emptyScrollView: UIScrollView!
    @IBOutlet weak var emptyTitleLabel: UILabel!
    @IBOutlet weak var emptyDescriptionLabel: UILabel!
    @IBOutlet weak var emptyMoreInfoButton: UIButton!

    var displayDataItems = [RewardDisplayData]()

    static let durationForAnimation = 1.0

    // MARK: life cycle

    override func viewDidLoad() {

        self.configure(appLifecycle: (UIApplication.shared.delegate as! AppDelegate).appLifeCycle!, id: RewardsViewController.ID)

        super.viewDidLoad()

        edgesForExtendedLayout = UIRectEdge()

        if #available(iOS 11.0, *) {
            scrollView.contentInsetAdjustmentBehavior = .never
            emptyScrollView.contentInsetAdjustmentBehavior = .never
        }

        emptyScrollView.isHidden = true
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        setNavigationBarWithColor(withColor: .GREEN120)
        addNavigationBarMenuButton()
        setTitleNavigationBar(withTitle: Localizables.points.key_points_text)

        configureView()
        configureEmptyView()

        presenter.viewWillAppear()
    }
    
    override func viewWillAppearAfterDismiss() {
        presenter.viewWillAppearAfterDismiss()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

    deinit {
        ComponentManager.remove(id: presenter.routerTag)
        DLog(message: "Deinitialized RewardsViewController")
    }

    func setTitleNavigationBar(withTitle title: String) {

        setNavigationTitle(withText: title)
        tabBarItem.title = Localizables.common.key_tabbar_points_button

    }

    // MARK: custom methods
    func configureView() {

        view.backgroundColor = .BBVA100

        pagerView.register(UINib(nibName: RewardsTableViewCell.nibName, bundle: nil), forCellWithReuseIdentifier: RewardsTableViewCell.identifier)

        pagerView.isInfinite = false
        pagerView.transformer = FSPagerViewTransformer(type: .linear)
        pagerView.transformer?.minimumScale = RewardsViewController.minimumScaleCells
        pagerView.itemSize = CGSize(width: view.frame.size.width - (RewardsViewController.paddingCPoints * 2), height: pagerView.frame.size.height)
        pagerView.delegate = self
        pagerView.dataSource = self
        pagerView.backgroundColor = .GREEN120
        scrollView.backgroundColor = .clear
        pointsInfoContainerStackView.backgroundColor = .GREEN120
        backView.backgroundColor = .GREEN120

        moreInfoButton.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Common.info_icon, color: .BBVAWHITE), width: 20, height: 20), for: .normal)

         moreInfoButton.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Common.info_icon, color: UIColor.BBVAWHITE.withAlphaComponent(0.3)), width: 20, height: 20), for: .highlighted)

         moreInfoButton.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Common.info_icon, color: UIColor.BBVAWHITE.withAlphaComponent(0.3)), width: 20, height: 20), for: .selected)

        moreInfoButton.setTitleColor(.BBVAWHITE, for: .normal)
        moreInfoButton.setTitleColor(UIColor.BBVAWHITE.withAlphaComponent(0.3), for: .highlighted)
        moreInfoButton.setTitleColor(UIColor.BBVAWHITE.withAlphaComponent(0.3), for: .selected)

        moreInfoButton.contentMode = .scaleAspectFit
        moreInfoButton.clipsToBounds = true

        let spacing = 6
        moreInfoButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: CGFloat(spacing))
        moreInfoButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: CGFloat(spacing), bottom: 0, right: 0)

        moreInfoButton.titleLabel?.font = Tools.setFontMedium(size: 14)

        rewardBreakDownContainerView.backgroundColor = .BBVA100

        if let firstItem = displayDataItems.first {
            if firstItem.emptyNonMonetaryValue {

                showGetMorePoints()
            } else {

                showExchangePoints()
            }
        }
    }

    func configureEmptyView() {

        emptyScrollView.backgroundColor = .GREEN120

        emptyTitleLabel.textColor = .BBVAWHITE
        emptyTitleLabel.text = Localizables.points.key_points_get_points_bbva
        emptyTitleLabel.font = Tools.setFontMedium(size: 16)

        emptyDescriptionLabel.textColor = .BBVAWHITE
        emptyDescriptionLabel.text = Localizables.points.key_points_win_points
        emptyDescriptionLabel.font = Tools.setFontBook(size: 14)

        emptyMoreInfoButton.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Common.info_icon, color: .BBVAWHITE), width: 20, height: 20), for: .normal)
        emptyMoreInfoButton.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Common.info_icon, color: UIColor.BBVAWHITE.withAlphaComponent(0.3)), width: 18, height: 20), for: .highlighted)
        emptyMoreInfoButton.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Common.info_icon, color: UIColor.BBVAWHITE.withAlphaComponent(0.3)), width: 18, height: 20), for: .selected)
        emptyMoreInfoButton.setTitleColor(.BBVAWHITE, for: .normal)
        emptyMoreInfoButton.setTitleColor(UIColor.BBVAWHITE.withAlphaComponent(0.3), for: .highlighted)
        emptyMoreInfoButton.setTitleColor(UIColor.BBVAWHITE.withAlphaComponent(0.3), for: .selected)
        emptyMoreInfoButton.contentMode = .scaleAspectFit
        emptyMoreInfoButton.clipsToBounds = true
        let spacing = 6
        emptyMoreInfoButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: CGFloat(spacing))
        emptyMoreInfoButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: CGFloat(spacing), bottom: 0, right: 0)

        emptyMoreInfoButton.setTitle(Localizables.points.key_points_how_to_get_points_text, for: .normal)
        emptyMoreInfoButton.titleLabel?.font = Tools.setFontMedium(size: 14)

    }

    @IBAction func moreInfoButtonPressed(_ sender: AnyObject?) {

        presenter.moreInfoButtonPressed(atIndex: pagerView.currentIndex)
    }

    @IBAction func emptyMoreInfoButtonPressed(_ sender: Any) {

        presenter.emptyMoreInfoButtonPressed()
    }
}

extension RewardsViewController: FSPagerViewDataSource, FSPagerViewDelegate {

    public func numberOfItems(in pagerView: FSPagerView) -> Int {
        return  displayDataItems.count
    }

    public func pagerView(_ pagerView: FSPagerView, cellForItemAt index: Int) -> FSPagerViewCell {

        let cell = pagerView.dequeueReusableCell(withReuseIdentifier: RewardsTableViewCell.identifier, at: index) as! RewardsTableViewCell
        cell.delegate = self
        cell.index = index
        cell.configureCell(withRewardDisplayData: displayDataItems[index])
        return cell

    }

    func pagerView(_ pagerView: FSPagerView, didSelectItemAt index: Int) {
        pagerView.deselectItem(at: index, animated: true)
        pagerView.scrollToItem(at: index, animated: true)
    }

    func pagerViewDidEndDecelerating(_ pagerView: FSPagerView) {

        var visibleRect = CGRect()
        visibleRect.origin = pagerView.collectionView.contentOffset
        visibleRect.size = pagerView.collectionView.bounds.size
        let visiblePoint = CGPoint(x: CGFloat(visibleRect.midX), y: CGFloat(visibleRect.midY))
        let visibleIndexPath: IndexPath? = pagerView.collectionView.indexPathForItem(at: visiblePoint)

        itemDisplayedIndex = (visibleIndexPath?.row)!

        if displayDataItems[itemDisplayedIndex].emptyNonMonetaryValue {
            showGetMorePoints()

        } else {
            showExchangePoints()

        }
        presenter.showedRewards(atIndex: itemDisplayedIndex)
    }

    func showExchangePoints() {

        UIView.transition(with: moreInfoButton, duration: 0.3, options: .transitionCrossDissolve, animations: {
            self.moreInfoButton.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Common.shopping_icon, color: .BBVAWHITE), width: 20, height: 20), for: .normal)
            self.moreInfoButton.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Common.shopping_icon, color: UIColor.BBVAWHITE.withAlphaComponent(0.3)), width: 20, height: 20), for: .highlighted)
            self.moreInfoButton.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Common.shopping_icon, color: UIColor.BBVAWHITE.withAlphaComponent(0.3)), width: 20, height: 20), for: .selected)
            self.moreInfoButton.setTitle(Localizables.points.key_points_see_offers_to_redeem_points, for: .normal)
        }, completion: nil)
    }

    func showGetMorePoints() {

        UIView.transition(with: moreInfoButton, duration: 0.3, options: .transitionCrossDissolve, animations: {
            self.moreInfoButton.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Common.info_icon, color: .BBVAWHITE), width: 20, height: 20), for: .normal)
            self.moreInfoButton.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Common.info_icon, color: UIColor.BBVAWHITE.withAlphaComponent(0.3)), width: 20, height: 20), for: .highlighted)
            self.moreInfoButton.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Common.info_icon, color: UIColor.BBVAWHITE.withAlphaComponent(0.3)), width: 20, height: 20), for: .selected)
            self.moreInfoButton.setTitle(Localizables.points.key_points_how_to_get_more_points_text, for: .normal)
        }, completion: nil)
    }
}

extension RewardsViewController: RewardsViewProtocol {

    func showError(error: ModelBO) {
        presenter.showModal()
    }

    func changeColorEditTexts() {

    }

    func showExpiredSkeleton(atIndex index: Int) {
        displayDataItems[index].showExpiredSkeleton = true
        pagerView.reloadData()
    }

    func hideExpiredSkeleton(atIndex index: Int) {
        displayDataItems[index].showExpiredSkeleton = false
        pagerView.reloadData()
    }

    func showExpiredInfo(points: Int, date: Date, atIndex index: Int) {
        displayDataItems[index].nonMonetaryValueCloseToExpire = RewardDisplayData.generateExpiredData(nonMonetaryValueCloseToExpire: points, nonMonetaryValueExpirationDate: date)
        pagerView.reloadData()
    }

    func showDataWithRewards(withRewardsDisplayItems rewardsDisplayItems: [RewardDisplayData]) {
        displayDataItems = rewardsDisplayItems
        presenter.showedRewards(atIndex: itemDisplayedIndex)
        pagerView.reloadData()
    }

    func doAnimationRewards () {
        pagerView.layer.frame = CGRect(x: 30, y: pagerView.frame.origin.y, width: pagerView.frame.size.width, height: pagerView.frame.size.height)

        UIView.animate(withDuration: RewardsViewController.durationForAnimation) { [weak self] in
            self?.pagerView.layer.frame = CGRect(x: 0, y: (self?.pagerView.frame.origin.y)!, width: (self?.pagerView.frame.size.width)!, height: (self?.pagerView.frame.size.height)!)
            self?.presenter.setAnimationDone()
        }
    }

    func sendScreenOmniture() {
        TrackerHelper.sharedInstance().trackRewardsScreen(withCustomerID: SessionDataManager.sessionDataInstance().customerID)
    }

    func showEmptyView() {

        scrollView.isHidden = true
        emptyScrollView.isHidden = false

    }

    func showHelp(withDisplayData displayData: HelpDisplayData) {

        helpView.delegate = self
        helpView.isHidden = false
        helpView.setHelpDisplayData(forHelpDisplayData: displayData)
    }

    func showRewardBreakDown(withModel model: RewardBreakDownModelProtocol) {
        rewardBreakDownView.presenter.setModel(model)
    }
}

extension RewardsViewController: RewardBreakDownViewDelegate {
    func rewardBreakDownView(_ rewardBreakDownView: RewardBreakDownView, changedHeight height: CGFloat) {
        rewardBreakDownHeightConstraint.constant = height
    }
}

extension RewardsViewController: CloseModalDelegate {

    func errorViewControllerAcceptPressed(_ errorViewController: ErrorViewController) {
        presenter.checkError()
    }

    func errorViewControllerCancelPressed(_ errorViewController: ErrorViewController) {
    }

}

extension RewardsViewController: HelpViewDelegate {

    func helpButtonPressed(_ helpView: HelpView, andHelpId helpId: String) {
        presenter.helpButtonPressed(withHelpId: helpId)
    }
}

extension RewardsViewController: RewardsTableViewCellDelegate {

    func imageCardDownloadedTableViewCell(_ rewardsTableViewCell: RewardsTableViewCell, atIndex index: Int) {

        UIView.performWithoutAnimation({
            pagerView.collectionView.reloadItems(at: [IndexPath(row: index, section: 0)])
        })
    }

}

// MARK: - APPIUM

extension RewardsViewController {

    func setAccessibilities() {

        #if APPIUM

            navigationController?.navigationItem.titleView?.isAccessibilityElement = false
            navigationController?.navigationBar.isAccessibilityElement = false

            pagerView.accessibilityIdentifier = ConstantsAccessibilities.REWARDS_TABLE

            Tools.setAccessibility(view: moreInfoButton, identifier: ConstantsAccessibilities.REWARDS_MORE_INFO_BUTTON)

        #endif

    }

}
