//
//  RewardsDisplayData.swift
//  shoppingapp
//
//  Created by ruben.fernandez on 1/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

struct RewardDisplayData {

    var cardBO: CardBO?

    var alias: String
    var aliasFont: UIFont
    var aliasColor: UIColor
    var panFour: String
    var panFourFont: UIFont
    var panFourColor: UIColor
    var imageCard: String?
    var points: String?
    var pointsColor: UIColor
    var pointsFont: UIFont
    var emptyPointsString: String?
    var emptyPointsStringFont: UIFont
    var emptyPointsStringColor: UIColor = .BBVAWHITE
    var backgroundImage: String
    var emptyNonMonetaryValue: Bool
    var nonMonetaryValueCloseToExpire: String?
    var nonMonetaryValueCloseToExpireFont: UIFont
    var nonMonetaryValueCloseToExpireColor: UIColor
    var showExpiredSkeleton = false

    init?(withCardBO cardBO: CardBO) {

        if cardBO.isCreditCard(), cardBO.isNormalPlastic() {

            let results = cardBO.rewards.filter { $0.rewardId == RewardBO.bancomer_points }

            if !results.isEmpty {

                showExpiredSkeleton = true

                self.cardBO = cardBO

                let reward = results[0] as RewardBO

                alias = cardBO.alias ?? (cardBO.title?.name ?? "")

                aliasColor = .BBVAWHITE
                aliasFont = Tools.setFontBook(size: 16)

                let number = cardBO.number
                let last4 = number.suffix(4)

                panFour = "•" + last4
                panFourColor = .BBVAWHITE
                panFourFont = Tools.setFontBookItalic(size: 14)

                imageCard = cardBO.imageFront

                if let nonmonetaryValue = reward.nonMonetaryValue, nonmonetaryValue > 0 {

                    points = NumberFormatter.stringDecimalToStringWithSeparator(withString: "\(reward.nonMonetaryValue!)", withLocale: Locale.current)
                    backgroundImage = ConstantsImages.Rewards.rewardsPointsBackground
                    emptyNonMonetaryValue = false
                } else {

                    emptyPointsString = Localizables.points.key_points_card_without_points_text
                    backgroundImage = ConstantsImages.Rewards.rewardsNoPointsBackground
                    emptyNonMonetaryValue = true
                }

                emptyPointsStringFont = Tools.setFontMediumItalic(size: 14)
                emptyPointsStringColor = .BBVAWHITE
                pointsColor = .BBVAWHITE
                pointsFont = Tools.setFontLight(size: 48)

                nonMonetaryValueCloseToExpireFont = Tools.setFontMediumItalic(size: 14)
                nonMonetaryValueCloseToExpireColor = .BBVAWHITE

                if let nonMonetaryValueCloseToExpire = reward.nonMonetaryValueCloseToExpire,
                    let nonMonetaryValueExpirationDate = reward.nonMonetaryValueExpirationDate {

                    self.nonMonetaryValueCloseToExpire = RewardDisplayData.generateExpiredData(nonMonetaryValueCloseToExpire: nonMonetaryValueCloseToExpire.intValue, nonMonetaryValueExpirationDate: nonMonetaryValueExpirationDate)

                }
                return
            }
        }
        return nil
    }

    static func generateExpiredData(nonMonetaryValueCloseToExpire: Int, nonMonetaryValueExpirationDate: Date) -> String {

        let expiratedDateString = Date.string(fromDate: nonMonetaryValueExpirationDate, withFormat: Date.DATE_FORMAT_DAY_MONTH_COMPLETE_YEAR_COMPACT)

        let nonMonetaryValueCloseToExpireString = String(format: Localizables.points.key_points_expire_text, NSNumber(value: nonMonetaryValueCloseToExpire), expiratedDateString)

        return nonMonetaryValueCloseToExpireString
    }
}

extension RewardDisplayData: Equatable {

    public static func == (lhs: RewardDisplayData, rhs: RewardDisplayData) -> Bool {

        return lhs.cardBO?.cardId == rhs.cardBO?.cardId

    }

}
