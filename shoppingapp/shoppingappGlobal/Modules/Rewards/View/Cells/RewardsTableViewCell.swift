//
//  CardTableViewCell.swift
//  FSPagerViewExample
//
//  Created by ruben.fernandez on 12/9/17.
//  Copyright © 2017 Wenchao Ding. All rights reserved.
//

import UIKit
import Shimmer

protocol RewardsTableViewCellDelegate: class {

    func imageCardDownloadedTableViewCell(_ rewardsTableViewCell: RewardsTableViewCell, atIndex index: Int)
}

class RewardsTableViewCell: FSPagerViewCell {

    class var nibName: String {
        return "RewardsTableViewCell"
    }

    static let identifier = "rewardsCell"

    weak var delegate: RewardsTableViewCellDelegate?

    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var cardImageView: UIImageView!
    @IBOutlet weak var aliasLabel: UILabel!
    @IBOutlet weak var panfourLabel: UILabel!
    @IBOutlet weak var emptyPointsLabel: UILabel!
    @IBOutlet weak var pointsLabel: UILabel!
    @IBOutlet weak var backgroundImage: UIImageView!
    @IBOutlet weak var expiredInfoShimmeringView: FBShimmeringView!
    @IBOutlet weak var UpdatedInfoLabel: UILabel!
    
    var index: Int?

    override func awakeFromNib() {
        super.awakeFromNib()

        contentView.backgroundColor = .clear
        backgroundColor = .clear
        containerView.backgroundColor = .GREEN132

        setupSkeleton()
    }

    func configureCell(withRewardDisplayData rewardDisplayData: RewardDisplayData) {

        configureCardAndAlias(withRewardDisplayData: rewardDisplayData)
        configurePoints(withRewardDisplayData: rewardDisplayData)

        setAccessibilities()

    }

    func configureCardAndAlias(withRewardDisplayData rewardDisplayData: RewardDisplayData) {

        let urlImage = rewardDisplayData.cardBO?.imageFront ?? ""

        cardImageView.getCardImage(withUrl: urlImage) { _, isNewImage in

            guard let index = self.index else {
                return
            }

            if isNewImage {
                self.delegate?.imageCardDownloadedTableViewCell(self, atIndex: index)
            }
        }

        aliasLabel.font = rewardDisplayData.aliasFont
        aliasLabel.textColor = rewardDisplayData.aliasColor
        aliasLabel.text = rewardDisplayData.alias

        panfourLabel.font = rewardDisplayData.panFourFont
        panfourLabel.textColor = rewardDisplayData.panFourColor
        panfourLabel.text = rewardDisplayData.panFour

    }

    func configurePoints(withRewardDisplayData rewardDisplayData: RewardDisplayData) {

        if let points = rewardDisplayData.points {

            emptyPointsLabel.text = ""

            pointsLabel.font = rewardDisplayData.pointsFont
            pointsLabel.textColor = rewardDisplayData.pointsColor
            pointsLabel.text = points
            backgroundImage.image = UIImage(named: rewardDisplayData.backgroundImage)

            if rewardDisplayData.showExpiredSkeleton {
                expiredInfoShimmeringView.isHidden = false
                expiredInfoShimmeringView.isShimmering = true
            } else {
                expiredInfoShimmeringView.isHidden = true
                expiredInfoShimmeringView.isShimmering = false
            }

            if let nonMonetaryValueCloseToExpire = rewardDisplayData.nonMonetaryValueCloseToExpire {

                emptyPointsLabel.text = nonMonetaryValueCloseToExpire
                emptyPointsLabel.font = rewardDisplayData.nonMonetaryValueCloseToExpireFont
                emptyPointsLabel.textColor = rewardDisplayData.nonMonetaryValueCloseToExpireColor

            }

        } else if let emptyPoints = rewardDisplayData.emptyPointsString {

            pointsLabel.text = ""
            emptyPointsLabel.font = rewardDisplayData.emptyPointsStringFont
            emptyPointsLabel.textColor = rewardDisplayData.emptyPointsStringColor
            emptyPointsLabel.text = emptyPoints
            backgroundImage.image = UIImage(named: rewardDisplayData.backgroundImage)
            expiredInfoShimmeringView.isHidden = true
            expiredInfoShimmeringView.isShimmering = false

        }

        UpdatedInfoLabel.font = Tools.setFontBook(size: 14)
        UpdatedInfoLabel.textColor = .BBVAWHITE
        UpdatedInfoLabel.text = Localizables.points.key_points_info_updated_yesterday

    }

    private func setupSkeleton() {

        var skeletonImage = SVGCache.image(forSVGNamed: ConstantsImages.Skeleton.lines, color: .GREEN120)
        skeletonImage = skeletonImage.resizableImage(withCapInsets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 20))

        expiredInfoShimmeringView.backgroundColor = .GREEN132
        expiredInfoShimmeringView.contentView = UIImageView(image: skeletonImage)
    }
}

// MARK: - APPIUM

extension RewardsTableViewCell {

    func setAccessibilities() {

        #if APPIUM

            accessibilityIdentifier = ConstantsAccessibilities.REWARDS_CELL

            containerView.accessibilityIdentifier = ConstantsAccessibilities.REWARDS_CONTAINER_CELL

            Tools.setAccessibility(view: cardImageView, identifier: ConstantsAccessibilities.REWARDS_IMAGE_CELL)

            Tools.setAccessibility(view: aliasLabel, identifier: ConstantsAccessibilities.REWARDS_ALIAS_CELL)

            Tools.setAccessibility(view: panfourLabel, identifier: ConstantsAccessibilities.REWARDS_PAN_FOUR_CELL)

            Tools.setAccessibility(view: emptyPointsLabel, identifier: ConstantsAccessibilities.REWARDS_EMPTY_POINTS_CELL)

            Tools.setAccessibility(view: emptyPointsLabel, identifier: ConstantsAccessibilities.REWARDS_POINTS_CELL)

             Tools.setAccessibility(view: backgroundImage, identifier: ConstantsAccessibilities.REWARDS_BACKGROUND_IMAGE_CELL)

        #endif

    }

}
