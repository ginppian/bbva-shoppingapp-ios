//
//  LimitsPageReaction.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 26/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

class LimitsPageReaction: PageReaction {

    static let ROUTER_TAG: String = "limits-tag"

    static let EVENT_NAV_TO_EDIT_LIMITS: ActionSpec<Model> = ActionSpec<Model>(id: "EVENT_NAV_TO_EDIT_LIMITS")

    override func configure() {

        navigateEditLimitsFromLimits()
    }

    func navigateEditLimitsFromLimits() {

        _ = when(id: LimitsPageReaction.ROUTER_TAG, type: Model.self)
            .doAction(actionSpec: LimitsPageReaction.EVENT_NAV_TO_EDIT_LIMITS)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, model in

                router.navigateEditLimitsFromLimits(withModel: model!)

            }
    }
}
