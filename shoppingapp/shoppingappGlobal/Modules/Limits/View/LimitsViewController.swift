//
//  LimitsViewController.swift
//  shoppingapp
//
//  Created by jesus.martinez on 24/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit
import CellsNativeCore
import CellsNativeComponents

class LimitsViewController: BaseViewController {

    // MARK: Constants

    static let ID: String = String(describing: LimitsViewController.self)
    static let heightToastView: CGFloat = 100.0

    // MARK: Vars

    var presenter: LimitsPresenterProtocol!

    var displayData: LimitsDisplayData?

    var needShowSkeleton = false

    // MARK: Outlets

    @IBOutlet weak var limitsTableView: UITableView!

    // Error Screen
    @IBOutlet weak var errorScreenView: ErrorScreenView! {
        didSet {
            errorScreenView.delegate = self
        }
    }

    override func viewDidLoad() {
        self.configure(appLifecycle: (UIApplication.shared.delegate as! AppDelegate).appLifeCycle!, id: LimitsViewController.ID)
        super.viewDidLoad()

        configureContentView()

        setAccessibilities()

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        configureNavigationBar()

        presenter.viewWillAppear()

        BusManager.sessionDataInstance.disableSwipeLateralMenu()

    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        presenter.viewDidAppear()
    }

    // MARK: - Life cycle

    override func initModel(withModel model: Model) {
        presenter.setModel(model: model)
    }

    deinit {
        ComponentManager.remove(id: presenter.routerTag)
        DLog(message: "Deinitialized LimitsViewController")
    }

    // MARK: - Setup views

    func configureContentView() {

        limitsTableView.isHidden = false
        limitsTableView.alwaysBounceVertical = false

        limitsTableView.backgroundColor = .BBVAWHITE

        limitsTableView.register(UINib(nibName: LimitTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: LimitTableViewCell.identifier)
        limitsTableView.register(UINib(nibName: SkeletonLimitsCell.identifier, bundle: nil), forCellReuseIdentifier: SkeletonLimitsCell.identifier)

        limitsTableView.estimatedRowHeight = LimitTableViewCell.height
        limitsTableView.rowHeight = UITableViewAutomaticDimension

        limitsTableView.separatorStyle = .none

    }

    func configureNavigationBar() {

        setNavigationBarDefault()
        setNavigationBackButtonDefault()

        setNavigationTitle(withText: Localizables.cards.key_cards_limits_text)

    }

    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationControllerOperation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {

        switch operation {
            
        case .push:
            
            if toVC as? EditLimitViewController != nil {
                return FaceOutFaceInAnimator(belowNavigationBar: false, fadeOutTimeFromViewController: FaceOutFaceInAnimator.defaultFadeOutTimeFromViewController, slideUpInTimeToViewController: 0)
            } else {
                return nil
            }
            
        default:
            return nil
        }
    }
}

extension LimitsViewController: UITableViewDelegate {

}

extension LimitsViewController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {

        return 1

    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if needShowSkeleton {

            let screen = UIScreen.main.bounds
            let screenHeight = screen.size.height
            let numberCells = (screenHeight / SkeletonLimitsCell.height)

            return Int(numberCells)
        }

        return displayData?.items.count ?? 0

    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if needShowSkeleton {
            if let cell = tableView.dequeueReusableCell(withIdentifier: SkeletonLimitsCell.identifier, for: indexPath) as? SkeletonLimitsCell {
                return cell
            }
        } else {
            if let displayItem = displayData?.items[indexPath.row] {

                if let cell = tableView.dequeueReusableCell(withIdentifier: LimitTableViewCell.identifier, for: indexPath) as? LimitTableViewCell {
                    cell.configure(withDisplayData: displayItem, andShowSeparator: !isLastRow(inPath: indexPath))
                    cell.delegate = self

                    displayData!.items[indexPath.row].animateConsumedLimit = false

                    return cell
                }

            }
        }

        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        var height: CGFloat = LimitTableViewCell.height

        if  needShowSkeleton {
            height = SkeletonLimitsCell.height
        }
        return height
    }

    func isLastRow(inPath path: IndexPath) -> Bool {
        return path.row == ((displayData?.items.count ?? 0) - 1)
    }

}

extension LimitsViewController: LimitTableViewCellDelegate {
    func editButtonPressed(_ limitTableViewCell: LimitTableViewCell) {
        let indexPath = limitsTableView.indexPath(for: limitTableViewCell)
        if let indexPath = indexPath, let limitsDisplayData = displayData?.items {
            presenter.editLimitButtonPressed(atIndex: indexPath.row, withLimitDisplayData: limitsDisplayData[indexPath.row])
        }
    }
}

extension LimitsViewController: CloseModalDelegate {

    func errorViewControllerAcceptPressed(_ errorViewController: ErrorViewController) {
        presenter.checkError()
    }

    func errorViewControllerCancelPressed(_ errorViewController: ErrorViewController) {
    }

}

extension LimitsViewController: LimitsViewProtocol {

    func showError(error: ModelBO) {
        presenter.showModal()
    }

    func changeColorEditTexts() {

    }

    func sendScreenOmniture(withTitleId titleId: String) {
        TrackerHelper.sharedInstance().trackLimitsScreen(withCustomerID: SessionDataManager.sessionDataInstance().customerID)
    }

    func showLimits(withDisplayData displayData: LimitsDisplayData) {

        limitsTableView.isHidden = false

        self.displayData = displayData

        limitsTableView.reloadData()
    }

    func showSkeleton() {

        limitsTableView.isHidden = false
        limitsTableView.isScrollEnabled = false

        needShowSkeleton = true

        errorScreenView.isHidden = true

        limitsTableView.reloadData()

    }

    func hideSkeleton() {

        limitsTableView.isScrollEnabled = true

        needShowSkeleton = false

        limitsTableView.reloadData()

    }

    func showErrorView() {

        limitsTableView.isHidden = true
        errorScreenView.isHidden = false

        let errorMessage = Localizables.common.key_common_error_text
        let displayData = ErrorScreenDisplayData(title: Localizables.common.key_error_ups_text, message: errorMessage, hasRetryButton: true)
        errorScreenView.configureError(displayData: displayData)
    }

    func showNoContentView() {

        limitsTableView.isHidden = true
        errorScreenView.isHidden = false

        let errorMessage = Localizables.common.key_common_error_text
        let displayData = ErrorScreenDisplayData(title: Localizables.common.key_error_ups_text, message: errorMessage, hasRetryButton: false)
        errorScreenView.configureNoContent(displayData: displayData)
    }

    func sendUpdateLimitSuccessfulOmniture(withLimitName limitName: String) {

        TrackerHelper.sharedInstance().trackUpdateLimitSucessful(forLimitName: limitName, andCustomerID: SessionDataManager.sessionDataInstance().customerID)
    }

    func showSuccessfulToast(withText text: String) {

        ToastManager.shared().showToast(withText: text, backgroundColor: .BBVADARKGREEN, height: CardsViewController.heightToastView)
    }
}

// MARK: - ErrorScreenViewDelegate

extension LimitsViewController: ErrorScreenViewDelegate {

    func errorScreenViewRetryButtonPressed(_ errorScreenView: ErrorScreenView) {

        presenter.retryButtonPressed()
    }
}

private extension LimitsViewController {

    func setAccessibilities() {

        #if APPIUM
            limitsTableView.accessibilityIdentifier = ConstantsAccessibilities.LIMITS_TABLE
        #endif

    }
}
