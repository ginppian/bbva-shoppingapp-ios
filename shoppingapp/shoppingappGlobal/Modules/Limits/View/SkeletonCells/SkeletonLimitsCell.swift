//
//  SkeletonLimitsCell.swift
//  shoppingapp
//
//  Created by Marcos on 9/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit
import Shimmer

class SkeletonLimitsCell: UITableViewCell {

    static let identifier = "SkeletonLimitsCell"
    static let height: CGFloat = 180.0

    @IBOutlet var shimmeringViewTop: FBShimmeringView!
    @IBOutlet var shimmeringViewLeft: FBShimmeringView!
    @IBOutlet var shimmeringViewRight: FBShimmeringView!
    @IBOutlet var shimmeringCenterRight: FBShimmeringView!
    @IBOutlet var shimmeringViewLeftDown: FBShimmeringView!
    @IBOutlet var shimmeringViewRightCenter: FBShimmeringView!
    @IBOutlet var shimmeringViewRightDown: FBShimmeringView!

    @IBOutlet weak var separatorView: UIView!

    override func layoutSubviews() {
        super.layoutSubviews()
    }

    override func awakeFromNib() {

        super.awakeFromNib()

        shimmeringViewTop.contentView = UIImageView(image: ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Skeleton.lines, color: .BBVA300), width: Float(shimmeringViewTop.frame.size.width), height: Float(shimmeringViewTop.frame.size.height)))
        shimmeringViewTop.isShimmering = true

        shimmeringViewLeft.contentView = UIImageView(image: ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Skeleton.lines, color: .BBVA300), width: Float(shimmeringViewLeft.frame.size.width), height: Float(shimmeringViewLeft.frame.size.height)))
        shimmeringViewLeft.isShimmering = true

        let imageRotated = UIImageView(image: ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Skeleton.lines_reverse, color: .BBVA300), width: Float(shimmeringViewRight.frame.size.width), height: Float(shimmeringViewRight.frame.size.height)))

        shimmeringViewRight.contentView = imageRotated
        shimmeringViewRight.isShimmering = true

        let shimmeringCenterRightContentView = UIView(frame: CGRect(x: 0, y: 0, width: shimmeringCenterRight.frame.size.width, height: shimmeringCenterRight.frame.size.height))
        shimmeringCenterRightContentView.backgroundColor = .BBVA300

        shimmeringCenterRight.contentView = shimmeringCenterRightContentView
        shimmeringCenterRight.isShimmering = true

        shimmeringViewLeftDown.contentView = UIImageView(image: ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Skeleton.lines, color: .BBVA300), width: Float(shimmeringViewLeftDown.frame.size.width), height: Float(shimmeringViewLeftDown.frame.size.height)))
        shimmeringViewLeftDown.isShimmering = true

        let imageRotatedCenter = UIImageView(image: ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Skeleton.lines_reverse, color: .BBVA300), width: Float(shimmeringViewRightCenter.frame.size.width), height: Float(shimmeringViewRightCenter.frame.size.height)))

        shimmeringViewRightCenter.contentView = imageRotatedCenter
        shimmeringViewRightCenter.isShimmering = true

        let imageRotatedDown = UIImageView(image: ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Skeleton.lines_reverse, color: .BBVA300), width: Float(shimmeringViewRightDown.frame.size.width), height: Float(shimmeringViewRightDown.frame.size.height)))

        shimmeringViewRightDown.contentView = imageRotatedDown
        shimmeringViewRightDown.isShimmering = true

        separatorView.backgroundColor = .BBVA200
    }
}
