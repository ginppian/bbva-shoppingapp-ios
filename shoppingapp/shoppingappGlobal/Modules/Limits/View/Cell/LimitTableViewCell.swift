//
//  LimitTableViewCell.swift
//  shoppingapp
//
//  Created by jesus.martinez on 26/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit

protocol LimitTableViewCellDelegate: class {
    func editButtonPressed(_ limitTableViewCell: LimitTableViewCell)
}

class LimitTableViewCell: UITableViewCell {

    weak var delegate: LimitTableViewCellDelegate?

    static let identifier = "LimitTableViewCell"
    static let height: CGFloat = 197

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var editButton: UIButton!

    @IBOutlet weak var consumedAmountLabel: UILabel!
    @IBOutlet weak var consumedTitleLabel: UILabel!

    @IBOutlet weak var currentLimitAmountLabel: UILabel!
    @IBOutlet weak var currentLimiTitleLabel: UILabel!

    @IBOutlet weak var limitProgressView: CustomProgressView!

    @IBOutlet weak var minimumAmountLabel: UILabel!
    @IBOutlet weak var maximumAmountLabel: UILabel!

    @IBOutlet weak var separatorLineView: UIView!

    override func awakeFromNib() {
        super.awakeFromNib()

        selectionStyle = .none

        titleLabel.font = Tools.setFontMedium(size: 16)
        titleLabel.textColor = .BBVA600

        editButton.setImage(SVGCache.image(forSVGNamed: ConstantsImages.Common.edit_icon, color: .MEDIUMBLUE), for: .normal)
        editButton.setImage(SVGCache.image(forSVGNamed: ConstantsImages.Common.edit_icon, color: .COREBLUE), for: .highlighted)
        editButton.setImage(SVGCache.image(forSVGNamed: ConstantsImages.Common.edit_icon, color: .COREBLUE), for: .selected)
        editButton.setImage(SVGCache.image(forSVGNamed: ConstantsImages.Common.edit_icon, color: .BBVA300), for: .disabled)

        consumedAmountLabel.font = Tools.setFontBook(size: 16)
        consumedAmountLabel.textColor = .DARKAQUA
        consumedTitleLabel.font = Tools.setFontBook(size: 14)
        consumedTitleLabel.textColor = .DARKAQUA

        currentLimitAmountLabel.font = Tools.setFontBook(size: 16)
        currentLimitAmountLabel.textColor = .BBVA600
        currentLimiTitleLabel.font = Tools.setFontBook(size: 14)
        currentLimiTitleLabel.textColor = .BBVA600

        minimumAmountLabel.font = Tools.setFontMediumItalic(size: 12)
        minimumAmountLabel.textColor = .BBVA500
        maximumAmountLabel.font = Tools.setFontMediumItalic(size: 12)
        maximumAmountLabel.textColor = .BBVA500

        separatorLineView.backgroundColor = .BBVA200

        setAccessibilities()

    }

    func configure(withDisplayData displayData: LimitDisplayData, andShowSeparator showSeparator: Bool) {

        titleLabel.text = displayData.title

        editButton.isHidden = displayData.hideEditable

        drawEditButton(withAllowEdit: displayData.allowsEdit)

        consumedAmountLabel.attributedText = displayData.consumedAmount
        consumedTitleLabel.text = displayData.consumedLimitTitle

        currentLimitAmountLabel.attributedText = displayData.currentLimitAmount
        currentLimiTitleLabel.text = displayData.currentLimiTitle

        limitProgressView.topProgress = CGFloat(displayData.consumedPercentage)
        limitProgressView.setMiddleProgress(CGFloat(displayData.currentLimitPercentage), withAnimation: displayData.animateConsumedLimit)

        minimumAmountLabel.attributedText = displayData.minimumLimitAmount
        maximumAmountLabel.attributedText = displayData.maximumLimitAmount

        separatorLineView.isHidden = !showSeparator
    }

    func drawEditButton(withAllowEdit allowEdit: Bool) {

        if allowEdit {

            editButton.setImage(SVGCache.image(forSVGNamed: ConstantsImages.Common.edit_icon, color: .MEDIUMBLUE), for: .normal)
            editButton.setImage(SVGCache.image(forSVGNamed: ConstantsImages.Common.edit_icon, color: .COREBLUE), for: .highlighted)
            editButton.setImage(SVGCache.image(forSVGNamed: ConstantsImages.Common.edit_icon, color: .COREBLUE), for: .selected)
            editButton.setImage(SVGCache.image(forSVGNamed: ConstantsImages.Common.edit_icon, color: .BBVA300), for: .disabled)

        } else {

            editButton.setImage(SVGCache.image(forSVGNamed: ConstantsImages.Common.edit_icon, color: .BBVA300), for: .normal)
            editButton.setImage(SVGCache.image(forSVGNamed: ConstantsImages.Common.edit_icon, color: .BBVA300), for: .highlighted)
            editButton.setImage(SVGCache.image(forSVGNamed: ConstantsImages.Common.edit_icon, color: .BBVA300), for: .selected)
            editButton.setImage(SVGCache.image(forSVGNamed: ConstantsImages.Common.edit_icon, color: .BBVA300), for: .disabled)
        }
    }

    // MARK: - User Actions

    @IBAction func editButtonPressed(_ sender: UIButton) {
        delegate?.editButtonPressed(self)
    }

}

private extension LimitTableViewCell {

    func setAccessibilities() {

        #if APPIUM

            accessibilityIdentifier = ConstantsAccessibilities.LIMIT_CELL

            Tools.setAccessibility(view: titleLabel, identifier: ConstantsAccessibilities.LIMIT_TITLE)
            Tools.setAccessibility(view: editButton, identifier: ConstantsAccessibilities.EDIT_LIMIT_BUTTON)
            Tools.setAccessibility(view: consumedAmountLabel, identifier: ConstantsAccessibilities.LIMIT_CONSUMED_AMOUNT)
            Tools.setAccessibility(view: consumedTitleLabel, identifier: ConstantsAccessibilities.LIMIT_CONSUMED_TITLE)
            Tools.setAccessibility(view: currentLimitAmountLabel, identifier: ConstantsAccessibilities.LIMIT_CURRENT_LIMIT_AMOUNT)
            Tools.setAccessibility(view: currentLimiTitleLabel, identifier: ConstantsAccessibilities.LIMIT_CURRENT_LIMIT_TITLE)
            Tools.setAccessibility(view: limitProgressView, identifier: ConstantsAccessibilities.LIMIT_PROGRESS_LIMIT_BAR)
            Tools.setAccessibility(view: minimumAmountLabel, identifier: ConstantsAccessibilities.LIMIT_MINIMUM_LIMIT_AMOUNT)
            Tools.setAccessibility(view: maximumAmountLabel, identifier: ConstantsAccessibilities.LIMIT_MAXIMUM_LIMIT_AMOUNT)

        #endif

    }

}
