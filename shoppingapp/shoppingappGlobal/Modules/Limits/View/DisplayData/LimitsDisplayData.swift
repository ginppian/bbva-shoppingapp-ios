//
//  LimitsDisplayData.swift
//  shoppingapp
//
//  Created by jesus.martinez on 26/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class LimitsDisplayData: ModelDisplay {

    var items: [LimitDisplayData]

    init(withLimits limits: LimitsBO) {

        items = [LimitDisplayData]()

        for limit in limits.limits {

            let limitDisplayData = LimitDisplayData.generateDisplayData(fromLimit: limit)
            items.append(limitDisplayData)

        }
    }
}

extension LimitsDisplayData: Equatable {

    public static func == (lhs: LimitsDisplayData, rhs: LimitsDisplayData) -> Bool {

        return lhs.items == rhs.items

    }
}

struct LimitDisplayData {

    static var allowsEditValue = true
    static var hiddenEditable = false

    var title: String
    var tagName: String
    var allowsEdit = false
    var consumedAmount: NSAttributedString
    var currentLimitAmount: NSAttributedString
    var minimumLimitAmount: NSAttributedString
    var maximumLimitAmount: NSAttributedString

    var consumedPercentage: Float
    var currentLimitPercentage: Float

    var animateConsumedLimit = false

    var consumedLimitTitle: String
    var currentLimiTitle: String
    var notEditableTitle: String

    var hideEditable = false

    var infoLimit = ""

    // MARK: - Title

    private static func titleForLimit(_ limit: LimitBO) -> String {
        var title = Localizables.cards.key_cards_limit_text + " -"

        if !limit.type.title.isEmpty {
            title = limit.type.title
        } else if let name = limit.name, !name.isEmpty {
            title = name
        } else {
            allowsEditValue = false
        }

        return title
    }
    
    // MARK: - Percentages

    private static func consumedPercentageForLimit(_ limit: LimitBO) -> Float {

        var consumedPercentage: Float = 0

        if let consumedAmountValue = limit.disposedLimits?.first??.amount, let allowedInterval = limit.allowedInterval, limit.amountLimits?.first??.amount != nil {

            if let maximumLimitAmountValue = allowedInterval.maximumAmount?.amount {

                if let minimumLimitAmountValue = allowedInterval.minimumAmount?.amount, consumedAmountValue.floatValue >= minimumLimitAmountValue.floatValue {

                    consumedPercentage = consumedAmountValue.floatValue / maximumLimitAmountValue.floatValue
                }

            } else {
                consumedPercentage = 100
            }
        } else {
            allowsEditValue = false
        }

        return consumedPercentage
    }

    private static func currentLimitPercentageForLimit(_ limit: LimitBO) -> Float {

        var currentLimitPercentage: Float = 0

        if let amount = limit.amountLimits?.first??.amount, let maximumLimitAmountValue = limit.allowedInterval?.maximumAmount?.amount {

            if let minimumLimitAmountValue = limit.allowedInterval?.minimumAmount?.amount, amount.floatValue >= minimumLimitAmountValue.floatValue {

                currentLimitPercentage = amount.floatValue / maximumLimitAmountValue.floatValue
            }

        } else {
            allowsEditValue = false
        }

        return currentLimitPercentage
    }

    // MARK: - Amount Formatter

    private static func amountFormatterForLimit(_ limit: LimitBO) -> AmountFormatter {

        var amountFormatter = AmountFormatter(codeCurrency: SessionDataManager.sessionDataInstance().mainCurrency)

        if let currentLimitCurrency = limit.amountLimits?.first??.currency, let consumedLimitCurrency = limit.disposedLimits?.first??.currency, let maximumAmountCurrency = limit.allowedInterval?.maximumAmount?.currency, let minimumAmountCurrency = limit.allowedInterval?.minimumAmount?.currency {

            if currentLimitCurrency == consumedLimitCurrency && currentLimitCurrency == maximumAmountCurrency && currentLimitCurrency == minimumAmountCurrency {

                amountFormatter = AmountFormatter(codeCurrency: currentLimitCurrency)
            }
        } else {
            allowsEditValue = false
        }

        return amountFormatter
    }

    // MARK: - Generate Amounts with Amount Formatter

    private static func currentLimitAmountFormattedForLimit(_ limit: LimitBO, amountFormatter: AmountFormatter) -> NSAttributedString {

        let currency = Currencies.currency(forCode: amountFormatter.codeCurrency)
        var currentLimitAmountFormatted = NSAttributedString(string: currency.position == .left ? currency.symbol + " -" :  "- " + currency.symbol)

        if let currentLimitAmount = limit.amountLimits?.first??.amount {

            currentLimitAmountFormatted = amountFormatter.attributtedText(forAmount: currentLimitAmount, bigFontSize: 16, smallFontSize: 12)

        } else {
            allowsEditValue = false
        }

        return currentLimitAmountFormatted
    }

    private static func consumedLimitAmountFormattedForLimit(_ limit: LimitBO, amountFormatter: AmountFormatter) -> NSAttributedString {

        let currency = Currencies.currency(forCode: amountFormatter.codeCurrency)
        var consumedAmountFormatted = NSAttributedString(string: currency.position == .left ? currency.symbol + " -" :  "- " + currency.symbol)

        if limit.amountLimits?.first??.amount != nil, let consumedLimitAmount = limit.disposedLimits?.first??.amount {

            consumedAmountFormatted = amountFormatter.attributtedText(forAmount: consumedLimitAmount, bigFontSize: 16, smallFontSize: 12)

        } else {
            allowsEditValue = false
        }

        return consumedAmountFormatted
    }

    private static func maximumLimitAmountFormattedForLimit(_ limit: LimitBO, amountFormatter: AmountFormatter, forEdit: Bool) -> NSAttributedString {

        let currency = Currencies.currency(forCode: amountFormatter.codeCurrency)
        var maximumLimitAmountFormatted = NSAttributedString(string: currency.position == .left ? currency.symbol + " -" :  "- " + currency.symbol)

        if limit.amountLimits?.first??.amount != nil, let amount = limit.allowedInterval?.maximumAmount?.amount {

            if forEdit {
                maximumLimitAmountFormatted = amountFormatter.attributtedText(forAmount: amount, bigFontSize: 14, smallFontSize: 10)
            } else {
                maximumLimitAmountFormatted = amountFormatter.attributtedText(forAmount: amount, bigFontSize: 12, smallFontSize: 10)
            }

        } else {
            allowsEditValue = false
        }

        return maximumLimitAmountFormatted
    }

    private static func minimumLimitAmountFormattedForLimit(_ limit: LimitBO, amountFormatter: AmountFormatter, forEdit: Bool) -> NSAttributedString {

        let currency = Currencies.currency(forCode: amountFormatter.codeCurrency)
        var minimumLimitAmountFormatted = NSAttributedString(string: currency.position == .left ? currency.symbol + " -" :  "- " + currency.symbol)

        if limit.amountLimits?.first??.amount != nil, let amount = limit.allowedInterval?.minimumAmount?.amount {

            if forEdit {
                minimumLimitAmountFormatted = amountFormatter.attributtedText(forAmount: amount, bigFontSize: 42, smallFontSize: 10)
            } else {
                minimumLimitAmountFormatted = amountFormatter.attributtedText(forAmount: amount, bigFontSize: 12, smallFontSize: 10)
            }

        } else {
            allowsEditValue = false
        }

        return minimumLimitAmountFormatted
    }

    // MARK: - Check if limit is editable

    private static func limitIsEditable(_ limit: LimitBO) {

        if !limit.isEditable {
            hiddenEditable = true

        } else {

            hiddenEditable = false

            if let consumedAmountValue = limit.disposedLimits?.first??.amount, let maximumLimitAmountValue = limit.allowedInterval?.maximumAmount?.amount {

                if consumedAmountValue.floatValue < maximumLimitAmountValue.floatValue {
                    allowsEditValue = true
                } else if consumedAmountValue.floatValue >= maximumLimitAmountValue.floatValue {
                    allowsEditValue = false
                }
            }
        }
    }

    // MARK: - Generate Display Data

    static func generateDisplayData(fromLimit limit: LimitBO) -> LimitDisplayData {

        allowsEditValue = true

        limitIsEditable(limit)

        let title = titleForLimit(limit)
        
        let tagName = limit.type.tagName

        let amountFormatter = amountFormatterForLimit(limit)

        let consumedPercentage = consumedPercentageForLimit(limit)
        let currentLimitPercentage = currentLimitPercentageForLimit(limit)

        let consumedAmountFormatted = consumedLimitAmountFormattedForLimit(limit, amountFormatter: amountFormatter)
        let currentLimitAmountFormatted = currentLimitAmountFormattedForLimit(limit, amountFormatter: amountFormatter)
        let minimumLimitAmountFormatted = minimumLimitAmountFormattedForLimit(limit, amountFormatter: amountFormatter, forEdit: false)
        let maximumLimitAmountFormatted = maximumLimitAmountFormattedForLimit(limit, amountFormatter: amountFormatter, forEdit: false)

        return LimitDisplayData(title: title, tagName: tagName, allowsEdit: LimitDisplayData.allowsEditValue, consumedAmount: consumedAmountFormatted, currentLimitAmount: currentLimitAmountFormatted, minimumLimitAmount: minimumLimitAmountFormatted, maximumLimitAmount: maximumLimitAmountFormatted, consumedPercentage: consumedPercentage, currentLimitPercentage: currentLimitPercentage, animateConsumedLimit: false, consumedLimitTitle: limit.type.disposedLimitTitle, currentLimiTitle: limit.type.amountsLimitTitle, notEditableTitle: limit.type.notEditableTitle, hideEditable: hiddenEditable, infoLimit: limit.infoMessage())
    }

    static func generateDisplayDataForEdit(fromLimit limit: LimitBO) -> LimitDisplayData {

        allowsEditValue = true

        limitIsEditable(limit)

        let title = titleForLimit(limit)
        
        let tagName = limit.type.tagName

        let amountFormatter = amountFormatterForLimit(limit)

        let consumedOverMaximumPercentage = consumedPercentageForLimit(limit)
        let currentLimitOverMaximumPercentage = currentLimitPercentageForLimit(limit)

        let consumedAmountFormatted = consumedLimitAmountFormattedForLimit(limit, amountFormatter: amountFormatter)
        let currentLimitAmountFormatted = currentLimitAmountFormattedForLimit(limit, amountFormatter: amountFormatter)
        let minimumLimitAmountFormatted = minimumLimitAmountFormattedForLimit(limit, amountFormatter: amountFormatter, forEdit: true)
        let maximumLimitAmountFormatted = maximumLimitAmountFormattedForLimit(limit, amountFormatter: amountFormatter, forEdit: true)

        return LimitDisplayData(title: title, tagName: tagName, allowsEdit: LimitDisplayData.allowsEditValue, consumedAmount: consumedAmountFormatted, currentLimitAmount: currentLimitAmountFormatted, minimumLimitAmount: minimumLimitAmountFormatted, maximumLimitAmount: maximumLimitAmountFormatted, consumedPercentage: consumedOverMaximumPercentage, currentLimitPercentage: currentLimitOverMaximumPercentage, animateConsumedLimit: false, consumedLimitTitle: limit.type.disposedLimitTitle, currentLimiTitle: limit.type.amountsLimitTitle, notEditableTitle: limit.type.notEditableTitle, hideEditable: hiddenEditable, infoLimit: limit.infoMessage())
    }

}

extension LimitDisplayData: Equatable {

    public static func == (lhs: LimitDisplayData, rhs: LimitDisplayData) -> Bool {

        return lhs.consumedAmount == rhs.consumedAmount
            && lhs.currentLimitAmount == rhs.currentLimitAmount
            && lhs.minimumLimitAmount == rhs.minimumLimitAmount
            && lhs.maximumLimitAmount == rhs.maximumLimitAmount
            && lhs.consumedPercentage == rhs.consumedPercentage
            && lhs.title == rhs.title
            && lhs.tagName == rhs.tagName
            && lhs.allowsEdit == rhs.allowsEdit
            && lhs.animateConsumedLimit == rhs.animateConsumedLimit
    }

}
