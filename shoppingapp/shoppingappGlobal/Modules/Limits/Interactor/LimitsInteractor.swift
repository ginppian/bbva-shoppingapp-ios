//
//  LimitsInteractor.swift
//  shoppingapp
//
//  Created by jesus.martinez on 24/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import RxSwift

class LimitsInteractor {
    let disposeBag = DisposeBag()

    deinit {
        DLog(message: "LimitsInteractor deinit")
    }

}

extension LimitsInteractor: LimitsInteractorProtocol {

    func provideLimits(withCardId cardId: String) -> Observable <ModelBO> {

        return Observable.create { observer in

            LimitsDataManager.sharedInstance.provideLimits(forCardId: cardId).subscribe(
                onNext: { result in

                    if let limitsEntity = result as? LimitsEntity {
                        observer.onNext(LimitsBO(limitsEntity: limitsEntity))
                        observer.onCompleted()
                    } else {
                        DLog(message: "Incorrect LimitsEntity")
                    }

                },
                onError: { error in

                    let evaluate: ServiceError = (error as? ServiceError)!

                    switch evaluate {

                    case .GenericErrorEntity(let errorEntity):
                        let errorBVABO = ErrorBO(error: errorEntity)
                        observer.onError(ServiceError.GenericErrorBO(error: errorBVABO))
                    default:
                        observer.onError(evaluate)
                    }
                }, onCompleted: {
                }).addDisposableTo(self.disposeBag)
            return Disposables.create()
        }

    }

}
