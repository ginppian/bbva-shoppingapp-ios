//
//  LimitsContract.swift
//  shoppingapp
//
//  Created by jesus.martinez on 24/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import RxSwift

protocol LimitsViewProtocol: ViewProtocol {

    func sendScreenOmniture(withTitleId titleId: String)
    func showLimits(withDisplayData displayData: LimitsDisplayData)
    func showSkeleton()
    func hideSkeleton()
    func showErrorView()
    func showNoContentView()
    func sendUpdateLimitSuccessfulOmniture(withLimitName limitName: String)
    func showSuccessfulToast(withText text: String)
}

protocol LimitsPresenterProtocol: PresenterProtocol {
    func viewWillAppear()
    func viewDidAppear()
    func retryButtonPressed()
    func editLimitButtonPressed(atIndex index: Int, withLimitDisplayData limitDisplayData: LimitDisplayData)
}

protocol LimitsInteractorProtocol {
    func provideLimits(withCardId cardId: String) -> Observable <ModelBO>
}
