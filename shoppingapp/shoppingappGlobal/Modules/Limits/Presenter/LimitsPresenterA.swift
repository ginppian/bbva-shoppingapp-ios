//
//  LimitsPresenterA.swift
//  shoppingappMX
//
//  Created by Armando Vazquez on 11/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

final class LimitsPresenterA<T: LimitsViewProtocol>: LimitsPresenter<T> {
    
    override func createLimitTransport(limit: LimitBO, cardId: String) -> LimitTransport {
        
        guard let card = cardBO else {
            return LimitTransport(withLimit: limit, andCardId: cardId)
        }
        return LimitTransport(withLimit: limit, andCardId: cardId, cardType: card.cardType)
    }
    
    override func loadLimits(cardBO: CardBO) {
        
        view?.showSkeleton()
        
        interactor?.provideLimits(withCardId: cardBO.cardId).subscribe(
            onNext: { [weak self] result in
                guard let `self` = self else {
                    return
                }
                
                if let resultBO = result as? LimitsBO {
                    
                    self.view?.hideSkeleton()
                    
                    resultBO.limits = resultBO.limits.filter { $0.type == LimitType.pos_daily }
                    
                    self.limitsBO = resultBO
                    
                    let isLimitEmpty = self.limitsBO?.limits.isEmpty ?? false
                    
                    if self.limitsBO?.status == ConstantsHTTPCodes.NO_CONTENT || isLimitEmpty {
                        
                        self.view?.showNoContentView()
                        
                    } else {
                        self.view?.showLimits(withDisplayData: LimitsDisplayData(withLimits: self.limitsBO!))
                    }
                }
                
            },
            onError: { [weak self] error in
                guard let `self` = self else {
                    return
                }
                
                self.view?.hideSkeleton()
                
                self.view?.showErrorView()
                
                let evaluate: ServiceError = (error as? ServiceError)!
                
                switch evaluate {
                    
                case .GenericErrorBO(let errorBO):
                    self.errorBO = errorBO
                    self.checkError()
                default:
                    break
                }
                
        }).addDisposableTo(disposeBag)
    }
    
    override func createSuccessfulToast(limitsDisplayData: LimitsDisplayData, amountFormatted: String) {
        
        let defaultMessage = String("\(Localizables.cards.key_card_limit_you_text) \( Localizables.cards.key_card_limit_daily_shopping_2) \(Localizables.cards.key_card_limit_is_text) \(amountFormatted).")
        
        guard let limitType = limitEdited?.type else {
            view?.showSuccessfulToast(withText: defaultMessage)
            return
        }
        
        var messageSuccess = defaultMessage
        
        switch limitType {
            
        case .atm_daily:
            messageSuccess = String(format: Localizables.cards.key_card_limit_atm_daily_message_success, amountFormatted)
        case .pos_daily:
            messageSuccess = String(format: Localizables.cards.key_card_limit_pos_daily_message_success, amountFormatted)
        case .office_daily:
            messageSuccess = String(format: Localizables.cards.key_card_limit_office_daily_message_success, amountFormatted)
        case .transaction_daily:
            messageSuccess = String(format: Localizables.cards.key_card_limit_transaction_daily_message_success, amountFormatted)
        default:
            messageSuccess = defaultMessage
        }
        
        view?.showSuccessfulToast(withText: messageSuccess)
    }
}
