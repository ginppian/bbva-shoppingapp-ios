//
//  LimitsPresenter.swift
//  shoppingapp
//
//  Created by jesus.martinez on 24/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import RxSwift
import CellsNativeComponents

class LimitsPresenter<T: LimitsViewProtocol>: BasePresenter<T> {

    var interactor: LimitsInteractorProtocol?

    let disposeBag = DisposeBag()

    var cardBO: CardBO?
    var limitsBO: LimitsBO?
    var limitEdited: LimitBO?
    var limitEditedIndex = 0

    override init(busManager: BusManager) {
        super.init(busManager: busManager)
        routerTag = LimitsPageReaction.ROUTER_TAG
    }

    required init() {
        super.init(tag: LimitsPageReaction.ROUTER_TAG)
    }

    override func setModel(model: Model) {
        if let cardBO = model as? CardBO {
            self.cardBO = cardBO
        } else if let limitTransport = model as? LimitTransport {
            limitEdited = limitTransport.limit
            limitsBO?.replaceLimit(limitTransport.limit)
        }
    }

    func loadLimits(cardBO: CardBO) {

        view?.showSkeleton()

        interactor?.provideLimits(withCardId: cardBO.cardId).subscribe(
            onNext: { [weak self] result in
                guard let `self` = self else {
                    return
                }

                if let resultBO = result as? LimitsBO {

                    self.view?.hideSkeleton()

                    self.limitsBO = resultBO

                    if self.limitsBO?.status == ConstantsHTTPCodes.NO_CONTENT {

                        self.view?.showNoContentView()

                    } else {
                        self.view?.showLimits(withDisplayData: LimitsDisplayData(withLimits: self.limitsBO!))
                    }
                }

            },
            onError: { [weak self] error in
                guard let `self` = self else {
                    return
                }

                self.view?.hideSkeleton()

                self.view?.showErrorView()

                let evaluate: ServiceError = (error as? ServiceError)!

                switch evaluate {

                case .GenericErrorBO(let errorBO):
                    self.errorBO = errorBO
                    self.checkError()
                default:
                    break
                }

            }).addDisposableTo(disposeBag)

    }

    func navigateToEditLimitScreen(withTransport transport: LimitTransport) {

        busManager.navigateScreen(tag: routerTag, LimitsPageReaction.EVENT_NAV_TO_EDIT_LIMITS, transport)

    }

    deinit {
        DLog(message: "LimitsPresenter deinit")
    }

    func viewDidAppear() {

        if let amountCurrencyEdited = limitEdited?.amountLimits?.first as? AmountCurrencyBO {

            let display = LimitsDisplayData(withLimits: self.limitsBO!)
            display.items[limitEditedIndex].animateConsumedLimit = true

            view?.showLimits(withDisplayData: display)

            let amountFormatter = AmountFormatter(codeCurrency: amountCurrencyEdited.currency)
            let amountFormatted = amountFormatter.format(amount: amountCurrencyEdited.amount)

            createSuccessfulToast(limitsDisplayData: display, amountFormatted: amountFormatted)

        }
        limitEdited = nil
    }

    func createLimitTransport(limit: LimitBO, cardId: String) -> LimitTransport {
        return LimitTransport(withLimit: limit, andCardId: cardId)
    }
    
    func createSuccessfulToast(limitsDisplayData: LimitsDisplayData, amountFormatted: String) {
        let textSuccessful = String("\(Localizables.cards.key_card_limit_you_text) \(limitsDisplayData.items[limitEditedIndex].title.lowercased()) \(Localizables.cards.key_card_limit_is_text) \(amountFormatted).")
        view?.showSuccessfulToast(withText: textSuccessful)
    }
    
}

extension LimitsPresenter: LimitsPresenterProtocol {
    
    func viewWillAppear() {
        
        guard let cardBO = cardBO else {
            return
        }
        
        if let title = cardBO.title?.id {
            view?.sendScreenOmniture(withTitleId: title)
        }
        
        if !viewDidLoad {
            
            loadLimits(cardBO: cardBO)
            
        }
        
        if let limitEdited = limitEdited {
            
            view?.sendUpdateLimitSuccessfulOmniture(withLimitName: limitEdited.type.tagName)
        }
        
        viewDidLoad = true
        
    }
    
    func retryButtonPressed() {

        guard let cardBO = cardBO else {
            return
        }

        loadLimits(cardBO: cardBO)

    }

    func editLimitButtonPressed(atIndex index: Int, withLimitDisplayData limitDisplayData: LimitDisplayData) {

        guard let limitsBO = self.limitsBO, let cardId = cardBO?.cardId else {
            return
        }

        if !limitDisplayData.allowsEdit {

            let message = "\(limitDisplayData.notEditableTitle) \(limitDisplayData.maximumLimitAmount.string)"

            let attributtedText = CustomAttributedText.attributedBoldText(forFullText: message, withBoldText: "", withFont: Tools.setFontBook(size: 14), withBoldFont: Tools.setFontBold(size: 14), withAlignment: .center, andForegroundColor: .BBVA600)

            errorBO = ErrorBO(messageAttributed: attributtedText, errorType: .info, allowCancel: false, okTitle: Localizables.common.key_common_accept_text)
            view?.showError(error: errorBO!)

        } else {

            limitEditedIndex = index

            let limitTransport = createLimitTransport(limit: limitsBO.limits[index], cardId: cardId)

            navigateToEditLimitScreen(withTransport: limitTransport)
        }
    }

}
