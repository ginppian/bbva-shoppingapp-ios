//
//  ShoppingListPageReaction.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 26/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

class ShoppingListPageReaction: PageReaction {

    static let ROUTER_TAG: String = "shopping-list-tag"

    override func configure() {

        navigateToDetailScreen()
    }

    func navigateToDetailScreen() {

        _ = when(id: ShoppingListPageReaction.ROUTER_TAG, type: Model.self)
            .doAction(actionSpec: ShoppingPageReaction.EVENT_NAV_TO_DETAIL_SCREEN)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, model in

                router.navigateToShoppingDetail(withModel: model!)

            }
    }

}
