//
//  ShoppingListPageReactionA.swift
//  shoppingappMX
//
//  Created by daniel.petrovic on 26/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

class ShoppingListPageReactionA: ShoppingListPageReaction {

    static let EVENT_NAV_TO_NEXT_SCREEN: ActionSpec<Model> = ActionSpec<Model>(id: "EVENT_NAV_TO_NEXT_SCREEN")
    static let EVENT_NAV_TO_LOCATION_SETTING_HELP: ActionSpec<Void> = ActionSpec<Void>(id: "EVENT_NAV_TO_LOCATION_SETTING_HELP")

    override func configure() {

        navigateFirstScreenFromShoppingListA()

        navigateToDetailScreen()

        navigateToLocationSettingHelp()
    }

    func navigateFirstScreenFromShoppingListA() {

        _ = when(id: ShoppingListPageReactionA.ROUTER_TAG, type: Model.self)
            .doAction(actionSpec: ShoppingPageReaction.EVENT_NAV_TO_NEXT_SCREEN)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, model in

                router.navigateFirstScreenFromShoppingListA(withModel: model!)

            }
    }

    func navigateToLocationSettingHelp() {

        _ = when(id: ShoppingListPageReactionA.ROUTER_TAG, type: Void.self)
            .doAction(actionSpec: ShoppingListPageReactionA.EVENT_NAV_TO_LOCATION_SETTING_HELP)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, _ in

                router.navigateToLocationSettingHelp()
            }
    }

}
