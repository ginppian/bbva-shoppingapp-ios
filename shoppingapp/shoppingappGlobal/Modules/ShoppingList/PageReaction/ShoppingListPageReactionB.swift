//
//  ShoppingListPageReactionB.swift
//  shoppingappMX
//
//  Created by jesus.martinez on 15/6/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

class ShoppingListPageReactionB: ShoppingListPageReactionA {

    override func configure() {
        super.configure()

        reactCards()
    }

    func reactCards() {

        _ = reactTo(channel: PageReactionConstants.CHANNEL_CARDS)?
            .then()
            .inside(componentID: ShoppingListPageReaction.ROUTER_TAG, component: ShoppingListPresenterB<ShoppingListViewControllerB>.self)
            .doReaction { component, param in

                if let param = param {
                    component.receive(model: param)
                }
            }
    }

}
