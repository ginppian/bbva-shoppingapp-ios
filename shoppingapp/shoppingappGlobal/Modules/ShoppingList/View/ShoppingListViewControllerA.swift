//
//  ShoppingListViewControllerA.swift
//  shoppingapp
//
//  Created by jesus.martinez on 10/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit

class ShoppingListViewControllerA: ShoppingListViewController {

    static let heightToastView: CGFloat = 100.0
    static let heightBigToastView: CGFloat = 120.0

    override func configureContentView() {

        tableView.backgroundColor = .BBVAWHITE
        tableView.register(UINib(nibName: SkeletonCarouselPromotionCell.identifier, bundle: nil), forCellReuseIdentifier: SkeletonCarouselPromotionCell.identifier)
        tableView.register(UINib(nibName: SkeletonBigPromotionCell.identifier, bundle: nil), forCellReuseIdentifier: SkeletonBigPromotionCell.identifier)
        tableView.register(UINib(nibName: PromotionHighTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: PromotionHighTableViewCell.identifier)
        tableView.register(UINib(nibName: PromotionSmallTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: PromotionSmallTableViewCell.identifier)

        let headerView = PromotionsAccessMapView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: PromotionsAccessMapView.height + ShoppingListViewController.tableViewHeaderHeight))
        headerView.delegate = self

        tableView.tableHeaderView = headerView

        tableView.separatorStyle = .none
    }

    override func showSkeleton() {
        (tableView.tableHeaderView as? PromotionsAccessMapView)?.presenter.startLoading()

        super.showSkeleton()
    }

    override func hideSkeleton() {
        super.hideSkeleton()

        (tableView.tableHeaderView as? PromotionsAccessMapView)?.presenter.finishLoading()
    }
}

extension ShoppingListViewControllerA: PromotionsAccessMapViewDelegate {

    func promotionsAccessMapViewSelected(_ promotionsAccessMapView: PromotionsAccessMapView) {
        (presenter as? ShoppingListPresenterProtocolA)?.accessMapSelected()
    }

}

extension ShoppingListViewControllerA: ShoppingListViewProtocolA {

    func showInfoToastNotGPS() {

        let toastText = NSMutableAttributedString()
            .book(Localizables.promotions.key_promotions_for_best_results_active_gps_text, size: 14)
            .bold("\n" + Localizables.promotions.key_promotions_more_info_text, size: 14)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 4
        paragraphStyle.alignment = .center
        toastText.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: toastText.length))

        ToastManager.shared().showToast(withAttributedText: toastText, backgroundColor: .DARKMEDIUMBLUE, height: ShoppingListViewControllerA.heightBigToastView, toastDelegate: self)
    }

    func showInfoToastNotPermissions() {

        let toastText = NSMutableAttributedString()
            .book(Localizables.promotions.key_enable_gps_advise_text, size: 14)
            .bold("\n" + Localizables.configuration.key_configuration, size: 14)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 4
        paragraphStyle.alignment = .center
        toastText.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: toastText.length))

        ToastManager.shared().showToast(withAttributedText: toastText, backgroundColor: .DARKMEDIUMBLUE, height: ShoppingListViewControllerA.heightBigToastView, toastDelegate: self)
    }

    func showToastUpdatedPromotions() {

        let toastText = NSMutableAttributedString().book(Localizables.promotions.key_promotions_updated_promos_for_position_text, size: 14)
        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 4
        paragraphStyle.alignment = .center
        toastText.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: toastText.length))

        ToastManager.shared().showToast(withAttributedText: toastText, backgroundColor: .BBVADARKGREEN, height: ShoppingListViewControllerA.heightToastView, toastDelegate: self)

    }

    func openAppSettings() {

        if let url = URL(string: UIApplicationOpenSettingsURLString) {

            URLOpener().openURL(url)
        }
    }
}

// MARK: - ToastDelegate
extension ShoppingListViewControllerA: ToastDelegate {

    func toastViewPressed() {

        (presenter as? ShoppingListPresenterProtocolA)?.toastPressed()
    }
}
