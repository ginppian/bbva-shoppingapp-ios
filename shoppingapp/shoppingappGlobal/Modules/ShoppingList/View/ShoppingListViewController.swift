//
//  ShoppingViewController.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 8/3/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit
import Lottie
import CellsNativeCore
import CellsNativeComponents

class ShoppingListViewController: BaseViewController {

    // MARK: Constants
    static let ID: String = String(describing: ShoppingListViewController.self)

    var presenter: ShoppingListPresenterProtocol!

    // MARK: outlets
    @IBOutlet weak var tableView: UITableView!

    @IBOutlet weak var errorScreenView: ErrorScreenView! {
        didSet {
            errorScreenView.delegate = self
        }
    }

    // Next page loading
    var footerView: UIView!
    var lottieLoader: LOTAnimationView!

    var displayItems: DisplayItemsPromotions?

    static let tableViewHeaderHeight: CGFloat = 20.0
    static let showSkeletonCompleteSecondCellHeight: CGFloat = 700.0
    static let highCellHeight: CGFloat = 335.0
    static let smallCellHeight: CGFloat = 159.0

    static let skeletonCarouselPromotionCellIndex = 0
    static let skeletonBigPromotionCellIndex = 1

    var needShowSkeleton = false
    var moreContent = true

    // MARK: life cycle

    override func viewDidLoad() {

        self.configure(appLifecycle: (UIApplication.shared.delegate as! AppDelegate).appLifeCycle!, id: ShoppingListViewController.ID)
        super.viewDidLoad()

        edgesForExtendedLayout = UIRectEdge()

        presenter.viewLoaded()

        configureContentView()
    }

    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)

        setNavigationBarDefault ()
        setNavigationBackButtonDefault()
        setAccessibilities()

        BusManager.sessionDataInstance.disableSwipeLateralMenu()
    }
    
    override func viewWillAppearAfterDismiss() {
        
        presenter.viewWillAppearAfterDismiss()
    }

    override func viewDidAppear(_ animated: Bool) {

        super.viewDidAppear(animated)

        presenter.viewDidAppear()

        configureFooterLoader()
    }

    override func viewWillDisappear(_ animated: Bool) {
        
        super.viewWillDisappear(animated)
    }

    override func initModel(withModel model: Model) {
        
        presenter.setModel(model: model)
    }

    deinit {
        
        ComponentManager.remove(id: presenter.routerTag, component: self)
        DLog(message: "Deinitialized ShoppingListViewController")
    }

    // MARK: custom methods

    func configureContentView() {

        tableView.backgroundColor = .BBVAWHITE
        tableView.register(UINib(nibName: SkeletonCarouselPromotionCell.identifier, bundle: nil), forCellReuseIdentifier: SkeletonCarouselPromotionCell.identifier)
        tableView.register(UINib(nibName: SkeletonBigPromotionCell.identifier, bundle: nil), forCellReuseIdentifier: SkeletonBigPromotionCell.identifier)
        tableView.register(UINib(nibName: PromotionHighTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: PromotionHighTableViewCell.identifier)
        tableView.register(UINib(nibName: PromotionSmallTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: PromotionSmallTableViewCell.identifier)

        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: ShoppingListViewController.tableViewHeaderHeight))
        headerView.backgroundColor = .BBVAWHITE

        tableView.tableHeaderView = headerView

        tableView.separatorStyle = .none
    }

    func configureFooterLoader() {

        footerView = UIView(frame: CGRect(x: 0, y: 0, width: tableView.frame.size.width, height: 130))
        footerView.backgroundColor = .clear

        lottieLoader = LOTAnimationView(name: ConstantsLottieAnimations.loader)
        lottieLoader.frame = CGRect(x: (UIScreen.main.bounds.width / 2) - 33.8 / 2, y: 50, width: 33.8, height: 30)
        lottieLoader.contentMode = .scaleAspectFit
        lottieLoader.loopAnimation = true

        footerView.addSubview(lottieLoader)
    }
}

extension ShoppingListViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        presenter.itemSelected(withDisplayItem: displayItems?.items[indexPath.row])
    }
}

extension ShoppingListViewController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var rows = 0
        if  needShowSkeleton {
            rows = 1
        } else {
            rows = displayItems?.items?.count ?? 0
        }

        return rows
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        var height: CGFloat = 0

        if  needShowSkeleton {
            height = ShoppingViewController.showSkeletonCompleteSecondCellHeight
        } else {

            let displayItem: DisplayItemPromotion! = displayItems?.items![indexPath.row]

            switch displayItem.typeCell {
            case CellPromotionType.high?:
                height = PromotionHighTableViewCell.height
            case CellPromotionType.small?:
                height = PromotionSmallTableViewCell.height

            default:
                height = 0
            }

        }

        return height
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if needShowSkeleton {
                if let cell = tableView.dequeueReusableCell(withIdentifier: SkeletonBigPromotionCell.identifier, for: indexPath) as? SkeletonBigPromotionCell {
                    return cell
                }
        } else {

            let displayItem: DisplayItemPromotion! = displayItems?.items![indexPath.row]

            if displayItem.typeCell == CellPromotionType.high {
                if let cell = tableView.dequeueReusableCell(withIdentifier: PromotionHighTableViewCell.identifier, for: indexPath) as? PromotionHighTableViewCell {

                    cell.configureCellWithDisplayPromotionData(withDisplayitemPromotion: displayItem)

                    return cell
                }
            } else if displayItem.typeCell == CellPromotionType.small {
                if let cell = tableView.dequeueReusableCell(withIdentifier: PromotionSmallTableViewCell.identifier, for: indexPath) as? PromotionSmallTableViewCell {

                    cell.configureCellWithDisplayPromotionData(withDisplayitemPromotion: displayItem)

                    return cell
                }
            }

        }

        return UITableViewCell()
    }
}

extension ShoppingListViewController: UIScrollViewDelegate {

    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {

        let offset = scrollView.contentOffset
        let bounds = scrollView.bounds
        let size = scrollView.contentSize
        let inset = scrollView.contentInset
        let y = offset.y + bounds.size.height - inset.bottom
        let h = size.height

        let reload_distance: CGFloat = -300.0
        if y > h + reload_distance && moreContent {
            presenter.loadDataNextPage()
        }
    }
}

extension ShoppingListViewController: CloseModalDelegate {

    func errorViewControllerAcceptPressed(_ errorViewController: ErrorViewController) {
        
        presenter.checkError()
    }

    func errorViewControllerCancelPressed(_ errorViewController: ErrorViewController) {
    }
}

extension ShoppingListViewController: ShoppingListViewProtocol {

    func showPromotions(withDisplayItemsPromotions displayItemPromotions: DisplayItemsPromotions) {
        
        displayItems = displayItemPromotions
        tableView.reloadData()
        tableView.isScrollEnabled = true
    }
    
    func showError(error: ModelBO) {
        
        presenter.showModal()
    }
    
    func changeColorEditTexts() {
    }

    func showNavTitle(withTitle title: String) {
        setNavigationTitle(withText: title)
    }
    
    func showErrorEmptyTsec() {

        tableView.isHidden = true

        let errorMessage = Localizables.common.key_common_error_nodata_text
        let displayData = ErrorScreenDisplayData(title: Localizables.common.key_error_ups_text, message: errorMessage, hasRetryButton: true)
        errorScreenView.configureError(displayData: displayData)
    }
    
    func showNoContentView() {

        tableView.isHidden = true

        let errorMessage = Localizables.promotions.key_promotions_no_promotions_text + " " + Localizables.common.key_try_later_text
        let displayData = ErrorScreenDisplayData(title: Localizables.common.key_error_ups_text, message: errorMessage, hasRetryButton: false)
        errorScreenView.configureNoContent(displayData: displayData)
    }
    
    @objc func showSkeleton() {

        needShowSkeleton = true

        tableView.isHidden = false

        UIView.animate(withDuration: 0.1, animations: {
            self.tableView.contentOffset = .zero
        }, completion: { _  in
            self.tableView.reloadData()
        })

    }

    @objc func hideSkeleton() {
        
        needShowSkeleton = false
    }
    
    func updateForNoMoreContent() {

        moreContent = false
    }
    
    func updateForMoreContent() {

        moreContent = true
    }

    func showNextPageAnimation() {

        footerView.isHidden = false

        tableView.tableFooterView = footerView

        lottieLoader.play()

        tableView.reloadData()
    }

    func hideNextPageAnimation() {

        footerView.isHidden = true

        tableView.tableFooterView = nil

        lottieLoader.pause()

        tableView.reloadData()
    }

    func sendScreenOmniture(withCategoryName categoryName: String) {

        TrackerHelper.sharedInstance().trackPromotionsScreen(forCategoryName: categoryName, andCustomerID: SessionDataManager.sessionDataInstance().customerID)
    }
    
    func sendScreenOmnitureForTrending() {
        
        TrackerHelper.sharedInstance().trackPromotionsTrendingShowAll(withCustomerID: SessionDataManager.sessionDataInstance().customerID)
    }
}

// MARK: - ErrorScreenViewDelegate

extension ShoppingListViewController: ErrorScreenViewDelegate {

    func errorScreenViewRetryButtonPressed(_ errorScreenView: ErrorScreenView) {

        presenter.retryButtonPressed()
    }
}

// MARK: - APPIUM

private extension ShoppingListViewController {

    func setAccessibilities() {
        
        #if APPIUM
            tableView.accessibilityIdentifier = ConstantsAccessibilities.PROMOTIONS_LIST
            errorScreenView.accessibilityIdentifier = ConstantsAccessibilities.PROMOTIONS_ERROR_VIEW
            Tools.setAccessibility(view: errorScreenView.titleLabel, identifier: ConstantsAccessibilities.PROMOTIONS_TITLE_ERROR)
            Tools.setAccessibility(view: errorScreenView.messageLabel, identifier: ConstantsAccessibilities.PROMOTIONS_DESCRIPTION_ERROR)
        #endif
    }
}
