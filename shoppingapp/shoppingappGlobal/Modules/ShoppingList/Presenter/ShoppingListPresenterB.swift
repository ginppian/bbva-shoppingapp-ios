//
//  ShoppingListPresenterB.swift
//  shoppingappMX
//
//  Created by jesus.martinez on 15/6/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class ShoppingListPresenterB<T: ShoppingListViewProtocolB>: ShoppingListPresenterA<T> {

    var cardTitleIds: [String]?

    func receive(model: Model) {

        if let cards = model as? CardsBO, cardTitleIds != cards.titleIds() {
            cardTitleIds = cards.titleIds()
        }
    }

    override func shoppingParamsTransport(withCategory category: CategoryBO) -> ShoppingParamsTransport {

        guard let cardTitleIds = cardTitleIds else {
            return super.shoppingParamsTransport(withCategory: category)
        }

        let location = userLocation ?? defaultLocation

        let locationParam = ShoppingParamLocation(geolocation: location, distanceParam: ShoppingDistanceParam(distance: defaultDistance, lengthType: lengthType))

        return ShoppingParamsTransport(categoryId: category.id.rawValue, location: locationParam, titleIds: cardTitleIds)
    }

}

extension ShoppingListPresenterB: ShoppingListPresenterProtocolB {

}
