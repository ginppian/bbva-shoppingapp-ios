//
//  ShoppingListPresenterA.swift
//  shoppingapp
//
//  Created by jesus.martinez on 10/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class ShoppingListPresenterA<T: ShoppingListViewProtocolA>: ShoppingListPresenter<T> {

    var defaultLocation = Settings.Global.default_location
    var userLocation: GeoLocationBO?
    var defaultDistance = Settings.PromotionsMap.default_distance
    var lengthType = Settings.PromotionsMap.default_length_type
    var locationManager: LocationManagerProtocol?
    var lastLocationErrorType: LocationErrorType?

    override func setModel(model: Model) {

        super.setModel(model: model)

        if let promotionsTransport = model as? PromotionsTransportA {
            userLocation = promotionsTransport.userLocation
        }
    }

    override func shoppingParamsTransport(withCategory category: CategoryBO) -> ShoppingParamsTransport {

        let location = userLocation ?? defaultLocation

        let locationParam = ShoppingParamLocation(geolocation: location, distanceParam: ShoppingDistanceParam(distance: defaultDistance, lengthType: lengthType))

        return ShoppingParamsTransport(categoryId: category.id.rawValue, location: locationParam)
    }

    override func viewLoaded() {

        view?.showNavTitle(withTitle: titleScreen)

        if let promotions = promotionsBO {

            showPromotions(withPromotions: promotions)

            managePagination()

        } else {

            view?.showSkeleton()
        }

        if trendingType == nil {
            locationManager = locationManager ?? LocationManager()

            locationManager?.getLocation(onSuccess: { [weak self] location -> Void in

                guard let `self` = self else {
                    return
                }

                if self.userLocation == nil && self.lastLocationErrorType != nil && self.promotionsBO != nil {
                    self.view?.showToastUpdatedPromotions()
                }

                if self.userLocation == nil {

                    self.userLocation = GeoLocationBO(withLatitude: location.coordinate.latitude, withLongitude: location.coordinate.longitude)

                    if let category = self.categoryBO, (self.promotionsBO == nil || self.lastLocationErrorType != nil) {
                        let params = self.shoppingParamsTransport(withCategory: category)

                        self.retrievePromotions(withParams: params)
                    }

                }

                self.lastLocationErrorType = nil

                }, onFail: { [weak self] locationErrorType -> Void in

                    self?.lastLocationErrorType = locationErrorType
                    self?.obtainLocationFail()

                    switch locationErrorType {

                    case .permissionDenied:

                        self?.view?.showInfoToastNotPermissions()

                    case .gpsOff:
                        self?.view?.showInfoToastNotGPS()

                    case .gpsFail:

                        DLog(message: "gpsFail")
                    }

            })
        }
    }

    private func obtainLocationFail() {

        if let category = categoryBO, promotionsBO == nil {

            let params = shoppingParamsTransport(withCategory: category)

            retrievePromotions(withParams: params)
        }
    }

    override func viewDidAppear() {

        sendOmnitureForScreen()

        if trendingType == nil, let locationM = locationManager {
            locationM.startLocationObserverAndCheckStatus()
        }
    }

    override func itemSelected(withDisplayItem displayItem: DisplayItemPromotion?) {

        if let item = displayItem, let promotionBO = item.promotionBO {

            let promotionDetailTransport = PromotionDetailTransport()
            promotionDetailTransport.promotionBO = promotionBO
            promotionDetailTransport.category = categoryBO
            promotionDetailTransport.userLocation = userLocation

            if trendingType == nil {
                if let locationM = locationManager {
                    locationM.stopLocationObserver()
                }

                promotionDetailTransport.completion = { [weak self] () -> Void in
                    if let locationM = self?.locationManager {
                        locationM.startLocationObserverAndCheckStatus()
                    }
                }
            }

            navigateToDetailScreen(withTransport: promotionDetailTransport)
        }

    }

    override func createDisplayitemWithPromotion(withPromotion promotion: PromotionBO, withNumber number: Int, withCategoryType categoryType: CategoryType) -> DisplayItemPromotion {

        var displayItemPromotion: DisplayItemPromotion

        if number == 0 {
            displayItemPromotion = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotion, withTypeCell: CellPromotionType.high, withCategoryType: categoryType, userLocation: userLocation)
        } else {
            displayItemPromotion = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotion, withTypeCell: CellPromotionType.small, withCategoryType: categoryType, userLocation: userLocation)
        }

        return displayItemPromotion
    }

}

extension ShoppingListPresenterA: ShoppingListPresenterProtocolA {

    func accessMapSelected() {

        let promotionsTransport = PromotionsTransport()
        promotionsTransport.trendingType = trendingType
        promotionsTransport.category = categoryBO

        busManager.navigateScreen(tag: routerTag, ShoppingListPageReactionA.EVENT_NAV_TO_NEXT_SCREEN, promotionsTransport)
    }

    func viewDidDisappear() {

        if trendingType == nil, let locationM = locationManager {
            locationM.stopLocationObserver()
        }
    }

    func toastPressed() {

        switch lastLocationErrorType {
        case .some(.permissionDenied):
            view?.openAppSettings()
        case .some(.gpsOff):
            busManager.navigateScreen(tag: routerTag, ShoppingListPageReactionA.EVENT_NAV_TO_LOCATION_SETTING_HELP, Void())
        default:
            break
        }
    }
}
