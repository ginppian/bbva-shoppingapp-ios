//
//  ShoppingPresenter.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 7/3/17.
//  Copyright © 2017 daniel.petrovic. All rights reserved.
//

import Foundation
import RxSwift
import CellsNativeComponents

class ShoppingListPresenter<T: ShoppingListViewProtocol>: BasePresenter<T> {

    let disposeBag = DisposeBag()
    var interactor: ShoppingListInteractorProtocol!

    var promotionsBO: PromotionsBO?
    var categoryBO: CategoryBO?
    var trendingType: TrendingType?

    var displayItems = DisplayItemsPromotions()

    var isLoadingNextPage = false

    var titleScreen = ""

    required init() {
        
        super.init(tag: ShoppingListPageReaction.ROUTER_TAG)
    }

    override init(busManager: BusManager) {
        
        super.init(busManager: busManager)
        routerTag = ShoppingListPageReaction.ROUTER_TAG
    }

    func navigateToDetailScreen(withTransport transport: PromotionDetailTransport) {

        busManager.navigateScreen(tag: routerTag, ShoppingPageReaction.EVENT_NAV_TO_DETAIL_SCREEN, transport)
    }

    func createDisplayitemWithPromotion(withPromotion promotion: PromotionBO, withNumber number: Int, withCategoryType categoryType: CategoryType) -> DisplayItemPromotion {

        var displayItemPromotion: DisplayItemPromotion

        if number == 0 {
            displayItemPromotion = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotion, withTypeCell: CellPromotionType.high, withCategoryType: categoryType)
        } else {
            displayItemPromotion = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotion, withTypeCell: CellPromotionType.small, withCategoryType: categoryType)
        }

        return displayItemPromotion
    }

    func showPromotions(withPromotions promotions: PromotionsBO) {

        displayItems = DisplayItemsPromotions()

        for i in 0 ..< promotions.promotions.count {

            let displayItemPromotion = createDisplayitemWithPromotion(withPromotion: promotions.promotions[i], withNumber: i, withCategoryType: .unknown)

            displayItems.items?.append(displayItemPromotion)
        }

        view?.showPromotions(withDisplayItemsPromotions: displayItems)
    }

    func updatePromotions(withPromotions promotions: PromotionsBO) {

        let previousPromotions = displayItems.items.count

        for i in 0 ..< promotions.promotions.count {

            let displayItemPromotion = createDisplayitemWithPromotion(withPromotion: promotions.promotions[i], withNumber: previousPromotions + i, withCategoryType: .unknown)

            displayItems.items?.append(displayItemPromotion)
        }

        view?.showPromotions(withDisplayItemsPromotions: displayItems)

    }

    func sendOmnitureForScreen() {

        if let category = categoryBO {
            let categoryName = category.name
            guard !categoryName.isEmpty else {
                return
            }
            view?.sendScreenOmniture(withCategoryName: categoryName.diacriticInsensitiveAndLowerCase())

        } else if trendingType != nil {

            view?.sendScreenOmnitureForTrending()

        }
    }

    override func setModel(model: Model) {

        if let promotionsTransport = model as? PromotionsTransport {
            if let transportPromotionsBO = promotionsTransport.promotionsBO {
                promotionsBO = PromotionsBO()
                promotionsBO?.status = transportPromotionsBO.status
                promotionsBO?.promotions = transportPromotionsBO.promotions
                promotionsBO?.pagination = promotionsTransport.promotionsBO?.pagination
            }

            titleScreen = (promotionsTransport.promotionName)!
            categoryBO = promotionsTransport.category
            trendingType = promotionsTransport.trendingType
        }
    }

    func managePagination() {

        if let nextPage = self.promotionsBO?.pagination?.links?.next {
            if nextPage.isEmpty {
                self.view?.updateForNoMoreContent()
            } else {
                self.view?.updateForMoreContent()
            }
        } else {
            self.view?.updateForNoMoreContent()
        }
    }

    func shoppingParamsTransport(withCategory category: CategoryBO) -> ShoppingParamsTransport {
        
        return ShoppingParamsTransport(categoryId: category.id.rawValue)
    }

    func viewLoaded() {

        view?.showNavTitle(withTitle: titleScreen)

        managePromotions()
    }
    
    func viewDidAppear() {

        sendOmnitureForScreen()
    }
    
    func itemSelected(withDisplayItem displayItem: DisplayItemPromotion?) {

        if let item = displayItem, let promotionBO = item.promotionBO {

            let promotionDetailTransport = PromotionDetailTransport()
            promotionDetailTransport.promotionBO = promotionBO
            promotionDetailTransport.category = categoryBO

            navigateToDetailScreen(withTransport: promotionDetailTransport)
        }
    }
}

// MARK: - Private methods

extension ShoppingListPresenter {

    func managePromotions() {

        if let promotions = promotionsBO {

            showPromotions(withPromotions: promotions)

            managePagination()

        } else if let category = categoryBO {

            let params = shoppingParamsTransport(withCategory: category)

            retrievePromotions(withParams: params)
        }
    }

    func retrievePromotions(withParams params: ShoppingParamsTransport) {

        view?.showSkeleton()

        interactor.providePromotions(byParams: params).subscribe(
            onNext: { [weak self] result in
                guard let `self` = self else {
                    return
                }
                if let promotionsBO = result as? PromotionsBO {
                    self.promotionsBO = promotionsBO

                    self.managePagination()

                    if self.promotionsBO?.status == ConstantsHTTPCodes.NO_CONTENT {
                        self.view?.showNoContentView()
                    } else {

                        self.view?.hideSkeleton()
                        self.showPromotions(withPromotions: promotionsBO)

                    }
                }

            },
            onError: { [weak self] error in
                DLog(message: "\(error)")
                guard let `self` = self else {
                    return
                }

                guard let serviceError = error as? ServiceError else {
                    return
                }

                switch serviceError {

                case .GenericErrorBO(let errorBO):

                    self.errorBO = errorBO

                    self.checkError()

                    if !errorBO.sessionExpired() {
                        self.view?.showErrorEmptyTsec()
                    }

                default:
                    break
                }
            }).addDisposableTo(disposeBag)

    }
}

// MARK: - ShoppingListPresenterProtocol

extension ShoppingListPresenter: ShoppingListPresenterProtocol {

    func loadDataNextPage() {

        if isLoadingNextPage == false, let promotions = promotionsBO, let next = promotions.pagination?.links?.next, !next.isEmpty {

            view?.showNextPageAnimation()

            isLoadingNextPage = true

            let nextURL = promotions.pagination?.links?.generateNextURL(isFinancial: false)

            interactor.providePromotions(withNextPage: nextURL!).subscribe(
                onNext: { [weak self] result in
                    guard let `self` = self else {
                        return
                    }
                    self.isLoadingNextPage = false
                    self.view?.hideNextPageAnimation()

                    if let promotionsResult = result as? PromotionsBO {

                        self.promotionsBO?.pagination = promotionsResult.pagination

                        if promotionsResult.status == ConstantsHTTPCodes.STATUS_OK {

                            self.managePagination()

                            self.updatePromotions(withPromotions: promotionsResult)

                        } else {
                            self.view?.updateForNoMoreContent()
                        }

                    }
                },
                onError: { [weak self] error in
                    guard let `self` = self else {
                        return
                    }
                    self.isLoadingNextPage = false

                    self.view?.updateForNoMoreContent()
                    self.view?.hideNextPageAnimation()

                    let evaluate: ServiceError = (error as? ServiceError)!

                    switch evaluate {

                    case .GenericErrorBO(let errorBO):
                        self.errorBO = errorBO
                        self.checkError()
                    default:
                        break
                    }
                }).addDisposableTo(self.disposeBag)
        }
    }

    func retryButtonPressed() {

        if let category = categoryBO {

            let params = shoppingParamsTransport(withCategory: category)

            retrievePromotions(withParams: params)
        }
    }
    
    func viewWillAppearAfterDismiss() {
        
        sendOmnitureForScreen()
    }

}
