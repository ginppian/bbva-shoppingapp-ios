//
//  ShoppingContract.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 15/3/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit
import RxSwift

protocol ShoppingListViewProtocol: ViewProtocol {
    func showNavTitle(withTitle title: String)
    func showPromotions(withDisplayItemsPromotions displayItemPromotions: DisplayItemsPromotions)

    func showErrorEmptyTsec()
    func showNoContentView()
    func showSkeleton()
    func hideSkeleton()

    func updateForNoMoreContent()
    func updateForMoreContent()
    func showNextPageAnimation()
    func hideNextPageAnimation()

    func sendScreenOmniture(withCategoryName categoryName: String)
    func sendScreenOmnitureForTrending()
}

protocol ShoppingListPresenterProtocol: PresenterProtocol {

    func viewLoaded()
    func viewDidAppear()
    func viewWillAppearAfterDismiss()
    func loadDataNextPage()
    func itemSelected(withDisplayItem displayItem: DisplayItemPromotion?)

    func retryButtonPressed()
}

protocol ShoppingListInteractorProtocol {

    func providePromotions(byParams params: ShoppingParamsTransport) -> Observable <ModelBO>
    func providePromotions(withNextPage nextPage: String) -> Observable <ModelBO>

}
