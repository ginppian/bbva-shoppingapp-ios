//
//  ShoppingListContractA.swift
//  shoppingapp
//
//  Created by jesus.martinez on 10/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol ShoppingListViewProtocolA: ShoppingListViewProtocol {

    func showInfoToastNotGPS()
    func showInfoToastNotPermissions()
    func showToastUpdatedPromotions()

    func openAppSettings()

}

protocol ShoppingListPresenterProtocolA: ShoppingListPresenterProtocol {

    func viewDidDisappear()
    func toastPressed()

    func accessMapSelected()

}

protocol ShoppingListInteractorProtocolA: ShoppingListInteractorProtocol {
}
