//
//  BVATransactionsListInteractor.swift
//  shoppingapp
//
//  Created by jesus.martinez on 7/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import RxSwift

class ShoppingListInteractor: ShoppingListInteractorProtocol {

    let disposeBag = DisposeBag()

    func providePromotions(byParams params: ShoppingParamsTransport) -> Observable <ModelBO> {

        return Observable.create { observer in

            ShoppingDataManager.sharedInstance.providePromotions(byParams: params).subscribe(
                onNext: { result in

                    if let promotionsEntity = result as? PromotionsEntity {
                        observer.onNext(PromotionsBO(promotionsEntity: promotionsEntity))
                        observer.onCompleted()
                    } else {
                        DLog(message: "Incorrect Promotionsentity")
                    }
                }, onError: { error in

                let evaluate: ServiceError = (error as? ServiceError)!

                switch evaluate {
                case .GenericErrorEntity(let errorEntity):
                    let errorBVABO = ErrorBO(error: errorEntity)
                    observer.onError(ServiceError.GenericErrorBO(error: errorBVABO))
                default:
                    observer.onError(evaluate)
                }

                }, onCompleted: {
                }).addDisposableTo(self.disposeBag)
            return Disposables.create()
        }
    }

    func providePromotions(withNextPage nextPage: String) -> Observable <ModelBO> {

        return Observable.create { observer in

            ShoppingDataManager.sharedInstance.providePromotions(withURL: nextPage).subscribe(
                onNext: { result in

                    if let promotionsEntity = result as? PromotionsEntity {
                        observer.onNext(PromotionsBO(promotionsEntity: promotionsEntity))
                        observer.onCompleted()
                    } else {
                        DLog(message: "Incorrect Promotionsentity")
                    }
                }, onError: { error in

                let evaluate: ServiceError = (error as? ServiceError)!

                switch evaluate {
                case .GenericErrorEntity(let errorEntity):
                    let errorBVABO = ErrorBO(error: errorEntity)
                    observer.onError(ServiceError.GenericErrorBO(error: errorBVABO))
                default:
                    observer.onError(evaluate)
                }

                }, onCompleted: {
                }).addDisposableTo(self.disposeBag)
            return Disposables.create()
        }

    }

}
