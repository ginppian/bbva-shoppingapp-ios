//
//  ShoppingDetailInteractor.swift
//  shoppingapp
//
//  Created by Luis Monroy on 10/01/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import RxSwift

class ShoppingDetailInteractor: ShoppingDetailInteractorProtocol {

    let disposeBag = DisposeBag()
    
    func providePromotion(byParams params: ShoppingDetailParamsTransport) -> Observable<ModelBO> {
        
        return Observable.create { observer in
            ShoppingDataManager.sharedInstance.providePromotion(byParams: params).subscribe(
                onNext: { result in

                    if let promotionDetailEntity = result as? PromotionDetailEntity {
                        if let promotionEntity = promotionDetailEntity.data {
                            observer.onNext(PromotionBO(promotionEntity: promotionEntity))
                            observer.onCompleted()
                        } else {
                            DLog(message: "Incorrect Promotion Entity")
                        }
                    } else {
                        DLog(message: "Incorrect Promotion Detail Entity")
                    }
                }, onError: { error in

                let evaluate: ServiceError = (error as? ServiceError)!

                switch evaluate {
                case .GenericErrorEntity(let errorEntity):
                    let errorBVABO = ErrorBO(error: errorEntity)
                    observer.onError(ServiceError.GenericErrorBO(error: errorBVABO))
                default:
                    observer.onError(evaluate)
                }

                }, onCompleted: {

                }).addDisposableTo(self.disposeBag)

            return Disposables.create()
        }
    }
}
