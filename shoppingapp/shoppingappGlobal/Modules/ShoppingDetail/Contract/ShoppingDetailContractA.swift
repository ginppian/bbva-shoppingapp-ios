//
//  ShoppingDetailContractA.swift
//  shoppingapp
//
//  Created by Luis Monroy on 22/01/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CoreLocation

protocol ShoppingDetailViewProtocolA: ShoppingDetailViewProtocol {

    func showPromoStore(withAssociateCommerceDisplay displayAssociateCommerce: AssociateCommerceDisplayData)
    func hidePromoStore()
    func showAssociatedTradeMap(withDisplayData displayData: AssociatedTradeMapDisplayData)
    func hideAssociatedTradeMap()
    func callToPhone(withPhone phone: String)
    func sendEmail(withEmail email: String)
    func launchMap(withDestinationCoordinate destinationCoordinate: CLLocationCoordinate2D, andOriginCoordinate originCoordinate: CLLocationCoordinate2D?)

}

protocol ShoppingDetailPresenterProtocolA: ShoppingDetailPresenterProtocol {

    func showStoreWebPage()
    func showStoreList()
    func pressedWebsiteOfAssociatedTrade()
    func pressedCallToPhoneOfAssociatedTrade()
    func pressedSendEmailOfAssociatedTrade()
    func pressedLaunchMapOfAssociatedTrade()

}
