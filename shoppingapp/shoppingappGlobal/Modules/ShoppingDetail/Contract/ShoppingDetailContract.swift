//
//  ShoppingDetailContract.swift
//  shoppingapp
//
//  Created by Luis Monroy on 08/01/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

protocol ShoppingDetailViewProtocol: ViewProtocol {

    func showSkeleton()
    func hideSkeleton()
    func showErrorView()
    func hideErrorView()

    func showPromo(withDisplayItemPromotion displayItemPromotion: DisplayItemPromotion)
    func showPromotionType(withPromotionTypeDisplay promotionTypeDisplay: PromotionTypeDisplayData)
    func showPromoDetail(withDisplayItemPromotionDetail displayItemPromotionDetail: DisplayItemPromotionDetail)
    func showPromoDetailImage(withPromoDetailImage promoDetailImage: UIImage?)
    func showPromoContact(withPromoContactDisplay promotionContactDisplay: ContactUsDisplayData)
    func updatePromoDetail(withDisplayItemPromotionDetail displayItemPromotionDetail: DisplayItemPromotionDetail)
    func setDistanceAndDateToShow(withString distanceAndDateToShow: String)
    func updatePromo(withDisplayItemPromotion displayItemPromotion: DisplayItemPromotion)

    func showWantThisOffer()
    func hideWantThisOffer()
    
    func showHelp(withDisplayData displayData: HelpDisplayData)

}

protocol ShoppingDetailPresenterProtocol: PresenterProtocol {

    func viewDidLoad()
    func showMoreInformation()
    func showTermsAndConditions()
    func reloadInfo()
    func dismissed()
    func showPromotionUrl()
    func getHelp()
    func helpButtonPressed(withHelpId helpId: String)

    func showGuarantee()
    func showContactDetails()
}

protocol ShoppingDetailInteractorProtocol {

    func providePromotion(byParams params: ShoppingDetailParamsTransport) -> Observable<ModelBO>
}
