//
//  ShoppingDetailContractB.swift
//  shoppingapp
//
//  Created by Rubén Jacobo on 17/09/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol ShoppingDetailViewProtocolB: ShoppingDetailViewProtocolA {
    
    func showCardsViewWith(displayDataList: [AssociatedCardsDisplayData])
    func showCreditCardRequestView()
}

protocol ShoppingDetailPresenterProtocolB: ShoppingDetailPresenterProtocolA {
    
    func showCreditCardRequestUrl()

}
