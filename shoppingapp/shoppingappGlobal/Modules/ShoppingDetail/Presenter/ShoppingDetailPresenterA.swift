//
//  ShoppingDetailPresenterA.swift
//  shoppingapp
//
//  Created by Luis Monroy on 27/05/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import RxSwift
import CellsNativeComponents
import CoreLocation

class ShoppingDetailPresenterA<T: ShoppingDetailViewProtocolA>: ShoppingDetailPresenter<T> {

    var associateCommerceDisplay = AssociateCommerceDisplayData()
    var associatedTradeMapDisplayData = AssociatedTradeMapDisplayData()
    var defaultLocation = Settings.Global.default_location
    var defaultDistance = Settings.PromotionsMap.default_distance
    var lengthType = Settings.PromotionsMap.default_length_type
    var locationManager: LocationManagerProtocol?
    var userLocationA: GeoLocationBO?

    override func getPromotionDetail() {
        
        if let locationManager = locationManager {
            locationManager.forceStartUpdatingLocation()
        } else {
            locationManager = LocationManager()
        }
        
        locationManager?.getLocation(onSuccess: { [weak self] location -> Void in
            if let `self` = self {
                
                if self.userLocationA == nil {
                    self.userLocationA = GeoLocationBO(withLatitude: location.coordinate.latitude, withLongitude: location.coordinate.longitude)
                    self.providePromotionDetail()
                }
            }
            }, onFail: { [weak self] locationErrorType -> Void in
                if let `self` = self {
                    switch locationErrorType {
                    default:
                        
                        self.providePromotionDetail()
                    }
                }
        })
    }
    
    func providePromotionDetail() {
        
        guard let promotionDetailTransport = generatePromotionDetailTransport() else {
            return
        }
        
        interactor.providePromotion(byParams: promotionDetailTransport).subscribe(
            onNext: { [weak self] result in
                guard let `self` = self else {
                    return
                }
                
                self.view?.hideSkeleton()

                if let promotion = result as? PromotionBO {
                    
                    if let promotionBO = self.promotionBO, let stores = promotionBO.stores, !stores.isEmpty {
                        
                        let displayItemPromo = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionBO, userLocation: self.userLocationA)
                        
                        self.view?.updatePromo(withDisplayItemPromotion: displayItemPromo)
                    }
                    
                    self.promotionBO = promotion
                    self.callSuperToShowWantThisOfferPromotion(promotion: promotion)
                    self.displayItemPromoDetail = DisplayItemPromotionDetail.generateDisplayItemPromotionDetail(fromPromotion: promotion)
                    self.associateCommerceDisplay = AssociateCommerceDisplayData.generateAssociateCommerceDisplay(fromPromotion: promotion)
                    self.contactUsDisplayData = ContactUsDisplayData.generateContactUsDisplay(fromPromotion: promotion)
                    
                    self.view?.showPromoDetail(withDisplayItemPromotionDetail: self.displayItemPromoDetail)
                    self.view?.showPromoContact(withPromoContactDisplay: self.contactUsDisplayData)
                    
                    let commerceDescription = self.associateCommerceDisplay.commerceDescription
                    let commerceWebPage = self.associateCommerceDisplay.commerceWebPage

                    if commerceDescription == nil && commerceWebPage == nil && !self.associateCommerceDisplay.showStores {

                        self.view?.hidePromoStore()
                    } else {

                        self.view?.showPromoStore(withAssociateCommerceDisplay: self.associateCommerceDisplay)
                    }

                    if let stores = self.promotionBO?.stores, !stores.isEmpty {

                        self.associatedTradeMapDisplayData = AssociatedTradeMapDisplayData(promotion: promotion, userLocation: self.userLocationA, shownOnMap: self.shownOnMap)
                        self.view?.showAssociatedTradeMap(withDisplayData: self.associatedTradeMapDisplayData)

                    } else {
                        self.view?.hideAssociatedTradeMap()
                    }
                }
            },
            onError: { [weak self] error in
                
                DLog(message: "\(error)")
                
                guard let `self` = self else {
                    return
                }
                
                self.userLocationA = nil
                
                guard let serviceError = error as? ServiceError else {
                    self.view?.hideSkeleton()
                    return
                }

                switch serviceError {

                case .GenericErrorBO(let errorBO):

                    self.errorBO = errorBO

                    if !errorBO.sessionExpired() {
                        self.view?.showErrorView()
                    } else {
                        self.checkError()
                    }

                default:
                    break
                }
                
                self.view?.hideSkeleton()
                
            }).addDisposableTo(disposeBag)
    }

    func callSuperToShowWantThisOfferPromotion(promotion: PromotionBO) {
        super.showWantThisOfferPromotion(promotion: promotion)
    }
    
    func showStoresList(withTransport transport: PromotionDetailTransport) {
        
        busManager.navigateScreen(tag: routerTag, ShoppingDetailPageReactionA.EVENT_NAV_TO_STORE_LIST_SCREEN, transport)
    }
    
    override func generatePromotionDetailTransport() -> ShoppingDetailParamsTransport? {
        
        guard let idPromo = promotionBO?.id else {
            return nil
        }
        
        let location = userLocationA ?? defaultLocation
        
        let locationParam = ShoppingParamLocation(geolocation: location, distanceParam: ShoppingDistanceParam(distance: defaultDistance, lengthType: lengthType))
        let expands = ShoppingExpands(stores: true, cards: true, contactDetails: true)
                
        let shoppingParams = ShoppingDetailParamsTransport(idPromo: idPromo, location: locationParam, expands: expands)
        
        return shoppingParams
    }
}

extension ShoppingDetailPresenterA: ShoppingDetailPresenterProtocolA {
    
    func showStoreList() {
        let promotionDetailTransport = PromotionDetailTransport()
        promotionDetailTransport.promotionBO = promotionBO
        promotionDetailTransport.userLocation = userLocationA
        showStoresList(withTransport: promotionDetailTransport)
    }
    
    func showStoreWebPage() {

        let webTransport = WebTransport(webPage: promotionBO?.commerceInformation.web, webTitle: promotionBO?.commerceInformation.name)
        navigateToWebPageScreen(withTransport: webTransport)
    }

    func pressedWebsiteOfAssociatedTrade() {

        let webTransport = WebTransport(webPage: associatedTradeMapDisplayData.web, webTitle: promotionBO?.commerceInformation.name)
        navigateToWebPageScreen(withTransport: webTransport)
    }

    func pressedCallToPhoneOfAssociatedTrade() {

        view?.callToPhone(withPhone: associatedTradeMapDisplayData.getPhoneContact())
    }

    func pressedSendEmailOfAssociatedTrade() {

        view?.sendEmail(withEmail: associatedTradeMapDisplayData.email!)
    }

    func pressedLaunchMapOfAssociatedTrade() {

        var userLocationCoordinate = kCLLocationCoordinate2DInvalid

        if let userLocation = userLocation {
            userLocationCoordinate = CLLocationCoordinate2D(latitude: userLocation.latitude, longitude: userLocation.longitude)
        }

        let destination = CLLocationCoordinate2D(latitude: associatedTradeMapDisplayData.latitude!, longitude: associatedTradeMapDisplayData.longitude!)
        var originCoordinate = kCLLocationCoordinate2DInvalid

        if userLocation == nil || !CLLocationCoordinate2DIsValid(userLocationCoordinate) || (userLocationCoordinate.latitude == 0 && userLocationCoordinate.longitude == 0) {
            originCoordinate = CLLocationCoordinate2D(latitude: defaultLocation.latitude, longitude: defaultLocation.longitude)
        }

        view?.launchMap(withDestinationCoordinate: destination, andOriginCoordinate: originCoordinate)
    }

}
