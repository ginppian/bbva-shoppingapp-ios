//
//  ShoppingDetailPresenterB.swift
//  shoppingapp
//
//  Created by Rubén Jacobo on 24/09/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

class ShoppingDetailPresenterB<T: ShoppingDetailViewProtocolB>: ShoppingDetailPresenterA<T> {

    var cards: CardsBO?
    let defaultTitleIds = ["VH", "VA", "VX", "DD", "VV", "V4", "UM", "TP", "TE", "V3", "V5", "VJ", "PN", "AR", "DO", "V1"]
    lazy var isUserLogged = SessionDataManager.sessionDataInstance().isUserLogged
        
    override func providePromotionDetail() {
        
        guard let promotionDetailTransport = generatePromotionDetailTransport() else {
            return
        }
        
        interactor.providePromotion(byParams: promotionDetailTransport).subscribe(
            onNext: { [weak self] result in
                guard let `self` = self else {
                    return
                }
                
                self.view?.hideSkeleton()
                
                if let promotion = result as? PromotionBO {
                    
                    if let promotionBO = self.promotionBO, let stores = promotionBO.stores, !stores.isEmpty {
                        
                        let displayItemPromo = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionBO, userLocation: self.userLocationA)
                        self.view?.updatePromo(withDisplayItemPromotion: displayItemPromo)
                    }
                    
                    self.promotionBO = promotion
                    self.callSuperToShowWantThisOfferPromotion(promotion: promotion)
                    self.displayItemPromoDetail = DisplayItemPromotionDetail.generateDisplayItemPromotionDetail(fromPromotion: promotion)
                    self.associateCommerceDisplay = AssociateCommerceDisplayData.generateAssociateCommerceDisplay(fromPromotion: promotion)
                    self.contactUsDisplayData = ContactUsDisplayData.generateContactUsDisplay(fromPromotion: promotion)

                    self.view?.showPromoDetail(withDisplayItemPromotionDetail: self.displayItemPromoDetail)
                    self.view?.showPromoContact(withPromoContactDisplay: self.contactUsDisplayData)

                    let commerceDescription = self.associateCommerceDisplay.commerceDescription
                    let commerceWebPage = self.associateCommerceDisplay.commerceWebPage
                    
                    if commerceDescription == nil && commerceWebPage == nil && !self.associateCommerceDisplay.showStores {
                        
                        self.view?.hidePromoStore()
                        
                    } else {
                        
                        self.view?.showPromoStore(withAssociateCommerceDisplay: self.associateCommerceDisplay)
                    }
                    
                    if let stores = self.promotionBO?.stores, !stores.isEmpty {
                        
                        self.associatedTradeMapDisplayData = AssociatedTradeMapDisplayData(promotion: promotion, userLocation: self.userLocationA, shownOnMap: self.shownOnMap)
                        self.view?.showAssociatedTradeMap(withDisplayData: self.associatedTradeMapDisplayData)
                        
                    } else {
                        self.view?.hideAssociatedTradeMap()
                    }
                    
                    self.showCardsFor(promotion: promotion)
                    
                    //TODO: Task (PSS2-3014). This task does not apply in the first upload to the AppStore.
//                    if !self.isUserLogged {
//                        self.view?.showCreditCardRequestView()
//                    }
                }
            },
            onError: { [weak self] error in
                
                DLog(message: "\(error)")
                
                guard let `self` = self else {
                    return
                }
                
                self.userLocationA = nil
                
                guard let serviceError = error as? ServiceError else {
                    self.view?.hideSkeleton()
                    return
                }
                
                switch serviceError {
                    
                case .GenericErrorBO(let errorBO):
                    
                    self.errorBO = errorBO
                    
                    if !errorBO.sessionExpired() {
                        self.view?.showErrorView()
                    } else {
                        self.checkError()
                    }
                    
                default:
                    break
                }
                
                self.view?.hideSkeleton()
                
        }).addDisposableTo(disposeBag)
    }
    
    override func generatePromotionDetailTransport() -> ShoppingDetailParamsTransport? {
        
        guard let idPromo = promotionBO?.id else {
            return nil
        }
        
        let location = userLocationA ?? defaultLocation
        
        let locationParam = ShoppingParamLocation(geolocation: location, distanceParam: ShoppingDistanceParam(distance: defaultDistance, lengthType: lengthType))
        let expands = ShoppingExpands(stores: true, cards: true, contactDetails: true)
        
        let titleIds = cards?.titleIds() ?? defaultTitleIds
        let shoppingParams = ShoppingDetailParamsTransport(idPromo: idPromo, location: locationParam, expands: expands, titleIds: titleIds )
        
        return shoppingParams
    }
    
    private func showCardsFor(promotion: PromotionBO) {
        var cardsList = [AssociatedCardsDisplayData]()
        
        var receivedCards = self.cards?.cards ?? [CardBO]()
        receivedCards = receivedCards.uniques()
        
        var promotionCards = promotion.cards ?? [PromotionCardBO]()
        promotionCards = promotionCards.uniques()
        
        if receivedCards.isEmpty && !promotionCards.isEmpty {
            for card in promotionCards {
                if let image = showCardFor(promotion: card) {
                    
                    let associatedCardsDisplayData = AssociatedCardsDisplayData(cardTitleName: card.cardTitle?.name ?? "", imageUrl: nil, imageNamed: image)
                    
                    cardsList.append(associatedCardsDisplayData)
                }
            }
        } else {
            for card in promotionCards {
                for anotherCard in receivedCards where card.cardTitle?.id == anotherCard.title?.id {
                    let associatedCardsDisplayData = AssociatedCardsDisplayData(cardTitleName: card.cardTitle?.name ?? "", imageUrl: anotherCard.imageFront ?? "", imageNamed: nil)
                    
                    cardsList.append(associatedCardsDisplayData)
                }
            }
        }
        
        if !cardsList.isEmpty {
            view?.showCardsViewWith(displayDataList: cardsList)
        }
        
    }
    
    private func showCardFor(promotion: PromotionCardBO) -> UIImage? {
        
        guard let imageName = promotion.cardTitle?.id else {
            return nil
        }
        
        if let image = UIImage(named: "Card\(imageName)") {
            return image
        } else {
            return UIImage(named: ConstantsImages.Card.blank_card)
        }

    }
}

extension ShoppingDetailPresenterB: ShoppingDetailPresenterProtocolB {
    
    func showCreditCardRequestUrl() {
        
        let urlString = Settings.Promotions.creditCardRequestWeb
        let urlDomain = URL(string: urlString)?.domain ?? ""

        let webTransport = WebTransport(webPage: urlString, webTitle: urlDomain)
        navigateToWebPageScreen(withTransport: webTransport)
    }
}

// MARK: - reactions
extension ShoppingDetailPresenterB {
    
    func receive(model: Model) {
        
        if let cardsReceived = model as? CardsBO {
            
            cards = cardsReceived
            
        }
    }
}
