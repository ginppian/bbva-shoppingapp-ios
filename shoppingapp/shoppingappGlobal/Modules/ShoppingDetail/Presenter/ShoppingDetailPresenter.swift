//
//  ShoppingDetailPresenter.swift
//  shoppingapp
//
//  Created by Luis Monroy on 08/01/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import RxSwift
import CellsNativeComponents

class ShoppingDetailPresenter<T: ShoppingDetailViewProtocol>: BasePresenter<T> {

    let disposeBag = DisposeBag()
    var interactor: ShoppingDetailInteractorProtocol!

    var promotionBO: PromotionBO?
    var categoryBO: CategoryBO?
    var trendingType: TrendingType?
    var userLocation: GeoLocationBO?
    var shownOnMap: Bool?

    var displayItemPromo = DisplayItemPromotion()
    var displayItemPromoDetail = DisplayItemPromotionDetail()
    var contactUsDisplayData = ContactUsDisplayData()
    var dismissCompletion: (() -> Void)?

    var helpDisplayItems = HelpDisplayData()

    let ROUTER_TAG: String = "shopping-detail-tag"

    required init() {
        super.init(tag: ROUTER_TAG)
    }

    override init(busManager: BusManager) {
        super.init(busManager: busManager)
        routerTag = "shopping-detail-tag"
    }

    func navigateToWebPageScreen(withTransport transport: WebTransport) {

        busManager.navigateScreen(tag: routerTag, ShoppingDetailPageReaction.EVENT_NAV_TO_WEBPAGE_SCREEN, transport)

    }
    
    func navigateToGuaranteeScreen(withTransport transport: PromotionDetailTransport) {
        
        busManager.navigateScreen(tag: routerTag, ShoppingDetailPageReaction.EVENT_NAV_TO_GUARANTEE_SCREEN, transport)
        
    }
    
    func navigateToContactsScreen(withTransport transport: PromotionDetailTransport) {
        
        busManager.navigateScreen(tag: routerTag, ShoppingDetailPageReaction.EVENT_NAV_TO_CONTACT_SCREEN, transport)
        
    }
    
    func showPromotion(withPromotion promotion: PromotionBO) {

        if let promotionUrl = promotionBO?.promotionUrl, !promotionUrl.isEmpty {
            view?.showWantThisOffer()
        } else {
            view?.hideWantThisOffer()
        }
        
        if displayItemPromoDetail.summaryDescription != nil {
            view?.showPromoDetail(withDisplayItemPromotionDetail: displayItemPromoDetail)
            view?.showPromoContact(withPromoContactDisplay: contactUsDisplayData)
            view?.hideSkeleton()
        } else {
            getPromotionDetail()
        }

        displayItemPromo = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotion, userLocation: userLocation)

        configureImage(withDisplayitemPromotion: displayItemPromo)

        view?.showPromo(withDisplayItemPromotion: displayItemPromo)

        let promotionTypeData = PromotionTypeDisplayData(fromPromotion: promotion)

        view?.showPromotionType(withPromotionTypeDisplay: promotionTypeData)
    }
    
    func showWantThisOfferPromotion(promotion: PromotionBO) {
        
        if let url = promotion.promotionUrl, !url.isEmpty {
            view?.showWantThisOffer()
        } else {
            view?.hideWantThisOffer()
        }
    }

    func getPromotionDetail() {

        guard let promotionDetailTransport = generatePromotionDetailTransport() else {
            return
        }
        
        interactor.providePromotion(byParams: promotionDetailTransport).subscribe(
            onNext: { [weak self] result in
                guard let `self` = self else {
                    return
                }
                
                self.view?.hideSkeleton()

                if let promotion = result as? PromotionBO {
                    
                    self.promotionBO = promotion

                    self.displayItemPromoDetail = DisplayItemPromotionDetail.generateDisplayItemPromotionDetail(fromPromotion: promotion)
                    
                    self.contactUsDisplayData = ContactUsDisplayData.generateContactUsDisplay(fromPromotion: promotion)
                    
                    self.view?.showPromoDetail(withDisplayItemPromotionDetail: self.displayItemPromoDetail)

                    self.view?.hideSkeleton()
                    self.showWantThisOfferPromotion(promotion: promotion)
                    self.view?.showPromoContact(withPromoContactDisplay: self.contactUsDisplayData)
                }
            },
            onError: { [weak self] error in
                DLog(message: "\(error)")

                guard let `self` = self else {
                    return
                }

                guard let serviceError = error as? ServiceError else {
                    self.view?.hideSkeleton()
                    return
                }

                switch serviceError {

                case .GenericErrorBO(let errorBO):

                    self.errorBO = errorBO

                    if !errorBO.sessionExpired() {
                        self.view?.showErrorView()
                    } else {
                        self.checkError()
                    }

                default:
                    break
                }
                
                self.view?.hideSkeleton()
                
            }).addDisposableTo(disposeBag)
    }
    
    func generatePromotionDetailTransport() -> ShoppingDetailParamsTransport? {
        
        guard let idPromo = promotionBO?.id else {
            return nil
        }
        
        let shoppingParams = ShoppingDetailParamsTransport(idPromo: idPromo)
        
        return shoppingParams
    }
    
    func configureImage(withDisplayitemPromotion displayItemPromotion: DisplayItemPromotion) {

        if let imageSaved = displayItemPromotion.promotionBO?.imageDetailSaved {
            view?.showPromoDetailImage(withPromoDetailImage: imageSaved)
        } else {
            let url = URL(string: displayItemPromotion.imageDetail?.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) ?? "")
            UIImage.load(fromUrl: url, completion: { [weak self] loadedImage in
                guard let image = loadedImage else {
                    DispatchQueue.main.async {
                        self?.view?.showPromoDetailImage(withPromoDetailImage: UIImage(named: ConstantsImages.Promotions.default_promotion_image))
                    }
                    return
                }
                displayItemPromotion.promotionBO?.imageDetailSaved = image
                DispatchQueue.main.async {
                    self?.view?.showPromoDetailImage(withPromoDetailImage: image)
                }
            })
        }
    }

    override func setModel(model: Model) {

        if let promotionDetailTransport = model as? PromotionDetailTransport {

            if let transportPromotionBO = promotionDetailTransport.promotionBO {
                promotionBO = transportPromotionBO
                categoryBO = promotionDetailTransport.category
                trendingType = promotionDetailTransport.trendingType
                dismissCompletion = promotionDetailTransport.completion
                userLocation = promotionDetailTransport.userLocation
                shownOnMap = promotionDetailTransport.shownOnMap
            }

            if let distanceToShow = promotionDetailTransport.distanceAndDateToShow {
                view?.setDistanceAndDateToShow(withString: distanceToShow)
            }
        }
    }
    
    func createHelpDisplayData() -> HelpDisplayData {
        
        helpDisplayItems = HelpDisplayData.generateHelpDisplayData(withConfigBO: ConfigPublicManager.sharedInstance().getConfigBO(forSection: Constants.SECTION_TO_SHOW_PROMOTION_DETAIL, withKey: PreferencesManagerKeys.kIndexToShowPromotionDetail)!, isFromSection: true)
        
        return helpDisplayItems
    }

}

extension ShoppingDetailPresenter: ShoppingDetailPresenterProtocol {

    func viewDidLoad() {

        view?.showSkeleton()

        showPromotionInfo()
        
        busManager.publishData(tag: EventTracker.id, PageReactionConstants.NEW_TRACK_EVENT, Event(type: .promotionDetail))
    }

    func showPromotionInfo() {

        if let promotion = promotionBO {
            showPromotion(withPromotion: promotion)
        }
    }

    func showMoreInformation() {

        view?.updatePromoDetail(withDisplayItemPromotionDetail: displayItemPromoDetail)
    }

    func showTermsAndConditions() {

        let webTransport = WebTransport(webPage: promotionBO?.legalConditions?.url, webTitle: Localizables.promotions.key_promotions_terms_and_conditions_text)
        navigateToWebPageScreen(withTransport: webTransport)
    }
    
    func showPromotionUrl() {

        let urlDomain = URL(string: promotionBO?.promotionUrl ?? "")?.domain ?? ""

        let webTransport = WebTransport(webPage: promotionBO?.promotionUrl, webTitle: urlDomain)
        navigateToWebPageScreen(withTransport: webTransport)
    }
    
    func reloadInfo() {

        view?.hideErrorView()
        view?.showSkeleton()
        showPromotionInfo()
    }

    func dismissed() {
        dismissCompletion?()
    }
    
    func getHelp() {
        if !ConfigPublicManager.sharedInstance().getConfigBOBySection(forKey: Constants.SECTION_TO_SHOW_PROMOTION_DETAIL).isEmpty {
            view?.showHelp(withDisplayData: createHelpDisplayData())
        }
    }
    
    func helpButtonPressed(withHelpId helpId: String) {
        busManager.navigateScreen(tag: ShoppingDetailPageReaction.ROUTER_TAG, ShoppingDetailPageReaction.EVENT_NAV_TO_DETAIL_HELP, HelpDetailTransport(helpId))
    }
    
    func showGuarantee() {
        
        let promotionDetailTransport = PromotionDetailTransport()
        promotionDetailTransport.promotionBO = promotionBO
        
        navigateToGuaranteeScreen(withTransport: promotionDetailTransport)
    }
    
    func showContactDetails() {
        busManager.publishData(tag: EventTracker.id, PageReactionConstants.NEW_TRACK_EVENT, Event(type: .noPromotionContact))

        let promotionDetailTransport = PromotionDetailTransport()
        promotionDetailTransport.promotionBO = promotionBO
        
        navigateToContactsScreen(withTransport: promotionDetailTransport)
    }
}
