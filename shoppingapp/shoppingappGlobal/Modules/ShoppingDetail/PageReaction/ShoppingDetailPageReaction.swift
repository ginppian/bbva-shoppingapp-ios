//
//  ShoppingDetailPageReaction.swift
//  shoppingapp
//
//  Created by Luis Monroy on 05/02/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

class ShoppingDetailPageReaction: PageReaction {

    static let ROUTER_TAG: String = "shopping-detail-tag"

    static let EVENT_NAV_TO_WEBPAGE_SCREEN: ActionSpec<Model> = ActionSpec<Model>(id: "EVENT_NAV_TO_WEBPAGE_SCREEN")

    static let EVENT_NAV_TO_DETAIL_HELP: ActionSpec<Model> = ActionSpec<Model>(id: "EVENT_NAV_TO_DETAIL_HELP")
    
    static let EVENT_NAV_TO_GUARANTEE_SCREEN: ActionSpec<Model> = ActionSpec<Model>(id: "EVENT_NAV_TO_GUARANTEE_SCREEN")
    
    static let EVENT_NAV_TO_CONTACT_SCREEN: ActionSpec<Model> = ActionSpec<Model>(id: "EVENT_NAV_TO_CONTACT_SCREEN")

    override func configure() {

        navigateToWebPageScreen()
        registerForNavigateToHelpDetail()
        navigateToGuaranteeScreen()
        navigateToContactScreen()
    }

    func navigateToWebPageScreen() {

        _ = when(id: ShoppingDetailPageReaction.ROUTER_TAG, type: Model.self)
            .doAction(actionSpec: ShoppingDetailPageReaction.EVENT_NAV_TO_WEBPAGE_SCREEN)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, model in

                router.navigateToShoppingWebPage(withModel: model!)
            }
    }
    
    func registerForNavigateToHelpDetail() {
        
        _ = when(id: ShoppingDetailPageReaction.ROUTER_TAG, type: Model.self)
            .doAction(actionSpec: ShoppingDetailPageReaction.EVENT_NAV_TO_DETAIL_HELP)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, model in
                
                router.navigateToDetailHelp(withModel: model!)
        }
    }
    func navigateToGuaranteeScreen() {
        
        _ = when(id: ShoppingDetailPageReaction.ROUTER_TAG, type: Model.self)
            .doAction(actionSpec: ShoppingDetailPageReaction.EVENT_NAV_TO_GUARANTEE_SCREEN)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, model in
                
                router.navigateToShoppingGuarantee(withModel: model!)
                
        }
    }
    
    func navigateToContactScreen() {
        
        _ = when(id: ShoppingDetailPageReaction.ROUTER_TAG, type: Model.self)
            .doAction(actionSpec: ShoppingDetailPageReaction.EVENT_NAV_TO_CONTACT_SCREEN)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, model in
                
                router.navigateToShoppingContact(withModel: model!)
                
        }
    }
}
