//
//  ShoppingDetailPageReactionA.swift
//  shoppingapp
//
//  Created by Developer on 25/07/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

class ShoppingDetailPageReactionA: ShoppingDetailPageReaction {
    
    static let EVENT_NAV_TO_STORE_LIST_SCREEN: ActionSpec<Model> = ActionSpec<Model>(id: "EVENT_NAV_TO_STORE_LIST_SCREEN")
    
    override func configure() {
        super.configure()
        
        navigateToStoreListScreen()
    }
    
    func navigateToStoreListScreen() {
        
        _ = when(id: ShoppingDetailPageReaction.ROUTER_TAG, type: Model.self)
            .doAction(actionSpec: ShoppingDetailPageReactionA.EVENT_NAV_TO_STORE_LIST_SCREEN)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, model in
                
                router.navigateToShoppingStoreList(withModel: model!)
                
        }
    }
}
