//
//  ShoppingDetailPageReactionB.swift
//  shoppingapp
//
//  Created by Rubén Jacobo on 24/09/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

class ShoppingDetailPageReactionB: ShoppingDetailPageReactionA {
    
    override func configure() {
        super.configure()
        
        reactCards()
    }
    
    func reactCards() {
        
        _ = reactTo(channel: PageReactionConstants.CHANNEL_CARDS)?
            .then()
            .inside(componentID: ShoppingDetailPageReactionB.ROUTER_TAG, component: ShoppingDetailPresenterB<ShoppingDetailViewControllerB>.self)
            .doReaction { component, param in
                
                if let param = param {
                    component.receive(model: param)
                }
        }
    }
}
