//
//  PromoDetailSkeletonView.swift
//  shoppingapp
//
//  Created by Luis Monroy on 15/03/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import UIKit
import Shimmer

class PromoDetailSkeletonView: UIView {

    @IBOutlet var shimmeringView1: FBShimmeringView!
    @IBOutlet var shimmeringView2: FBShimmeringView!
    @IBOutlet var shimmeringView3: FBShimmeringView!
    @IBOutlet var shimmeringView4: FBShimmeringView!
    @IBOutlet var shimmeringView5: FBShimmeringView!
    @IBOutlet var shimmeringView6: FBShimmeringView!
    @IBOutlet var shimmeringView7: FBShimmeringView!
    @IBOutlet var shimmeringView8: FBShimmeringView!

    func shimmeSkeleton() {

        var skeletonImage = SVGCache.image(forSVGNamed: ConstantsImages.Skeleton.lines, color: .BBVAPromotionShimmeringView1Color)
        skeletonImage = skeletonImage.resizableImage(withCapInsets: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 20))

        shimmeringView1.contentView = UIImageView(image: skeletonImage)
        shimmeringView2.contentView = UIImageView(image: skeletonImage)
        shimmeringView3.contentView = UIImageView(image: skeletonImage)
        shimmeringView4.contentView = UIImageView(image: skeletonImage)
        shimmeringView5.contentView = UIImageView(image: skeletonImage)
        shimmeringView6.contentView = UIImageView(image: skeletonImage)
        shimmeringView7.contentView = UIImageView(image: skeletonImage)
        shimmeringView8.contentView = UIImageView(image: skeletonImage)

        shimmeringView1.isShimmering = true
        shimmeringView2.isShimmering = true
        shimmeringView3.isShimmering = true
        shimmeringView4.isShimmering = true
        shimmeringView5.isShimmering = true
        shimmeringView6.isShimmering = true
        shimmeringView7.isShimmering = true
        shimmeringView8.isShimmering = true
    }

    static func getSkeletonView(frame: CGRect) -> PromoDetailSkeletonView? {
        let className = String(describing: self)
        let skeleton: PromoDetailSkeletonView!  = Bundle.main.loadNibNamed(className, owner: nil, options: nil)?.first as? PromoDetailSkeletonView

        if skeleton != nil {
            skeleton.frame.size.width = frame.size.width
            skeleton.frame.size.height = frame.size.height
            skeleton.shimmeSkeleton()
        }

        return skeleton
    }
}
