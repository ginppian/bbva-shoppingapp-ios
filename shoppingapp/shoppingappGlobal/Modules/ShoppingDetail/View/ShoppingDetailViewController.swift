//
//  ShoppingDetailViewController.swift
//  shoppingapp
//
//  Created by Luis Monroy on 08/01/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit
import CellsNativeCore
import CellsNativeComponents

class ShoppingDetailViewController: BaseViewController {

    // MARK: Constants
    static let ID: String = String(describing: ShoppingDetailViewController.self)

    var presenter: ShoppingDetailPresenterProtocol!
    var skeletonView: PromoDetailSkeletonView!
    var errorView: PromotionErrorView!
    var promotionID: String?
    
    var distanceAndDateToShow: String?

    // MARK: outlets
    @IBOutlet weak var detailImage: UIImageView!
    @IBOutlet weak var promoOnlineImage: UIImageView!
    @IBOutlet weak var promoPresencialImage: UIImageView!

    @IBOutlet weak var commerceNameLabel: UILabel!
    @IBOutlet weak var commerceDescriptionLabel: UILabel!
    @IBOutlet weak var benefitLabel: UILabel!

    @IBOutlet weak var validDateLabel: UILabel!
    @IBOutlet weak var getPromotionLabel: UILabel!
    @IBOutlet weak var onlinePresencialLabel: UILabel!

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var lineView: UIView!
    @IBOutlet weak var cornerView: UIView!
    @IBOutlet weak var promotionTypeView: PromotionTypeView!
    @IBOutlet weak var basicDetailView: BasicDetailView!
    @IBOutlet weak var wantThisOfferView: WantThisOfferView!
    @IBOutlet weak var wantThisOfferHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var contactUsView: ContactUsView!
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var helpView: HelpView!

    @IBOutlet weak var validDateLabelHeight: NSLayoutConstraint!
    @IBOutlet weak var promoOnlineImageWidth: NSLayoutConstraint!
    @IBOutlet weak var promoOnlineImageSpace: NSLayoutConstraint!
    @IBOutlet weak var promoPresencialImageWidth: NSLayoutConstraint!
    @IBOutlet weak var promoPresencialImageSpace: NSLayoutConstraint!
    @IBOutlet weak var promotionTypeViewWidth: NSLayoutConstraint!
    
    @IBOutlet weak var scrollBottom: NSLayoutConstraint!
    static let spaceSize: CGFloat = 20.0
    static let cornerSize: CGFloat = 87.0
    
    static let wantThisOfferDefaultHeight: CGFloat = 130

    // MARK: life cycle
    override func viewDidLoad() {

        self.configure(appLifecycle: (UIApplication.shared.delegate as! AppDelegate).appLifeCycle!, id: ShoppingDetailViewController.ID)

        super.viewDidLoad()

        basicDetailView.delegate = self
        
        wantThisOfferView.delegate = self
        
        contactUsView.delegate = self

        configureContentView()

        configureCornerView()

        presenter.viewDidLoad()

        presenter.getHelp()

        if #available(iOS 11.0, *) {
            scrollView.contentInsetAdjustmentBehavior = .never
        } else {
            self.automaticallyAdjustsScrollViewInsets = false
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {

        super.viewWillAppear(animated)
        self.tabBarController?.tabBar.isHidden = true
        configureNavigationBar()
        setAccessibilities()

        trackScreen()

        BusManager.sessionDataInstance.disableSwipeLateralMenu()
        
    }
    
    override func viewWillAppearAfterDismiss() {
        
        trackScreen()
    }
    
    deinit {

        DLog(message: "Deinitialized ShoppingDetailViewController")
    }

    override func initModel(withModel model: Model) {
        presenter.setModel(model: model)
    }

    // MARK: custom methods
    
    fileprivate func trackScreen() {
        TrackerHelper.sharedInstance().trackPromotionsDetailScreen(withCustomerID: SessionDataManager.sessionDataInstance().customerID, andPromotionID: promotionID)
    }

    func configureNavigationBar() {

        setNavigationBarTransparent()
        let image = SVGCache.image(forSVGNamed: ConstantsImages.Common.close_icon, color: .BBVAWHITE)

        setNavigationLeftButton(withImage: image, action: #selector(dismissAction))
        setNavigationTitle(withText: "")
    }

    func configureContentView() {

        detailImage.backgroundColor = .BBVA200

        commerceNameLabel.font = Tools.setFontBook(size: 12)
        commerceNameLabel.textColor = .BBVA600

        commerceDescriptionLabel.font = Tools.setFontMedium(size: 18)
        commerceDescriptionLabel.textColor = .BBVA600

        benefitLabel.font = Tools.setFontMedium(size: 14)
        benefitLabel.textColor = .MEDIUMBLUE

        validDateLabel.font = Tools.setFontBookItalic(size: 14)
        validDateLabel.textColor = .BBVA500

        getPromotionLabel.font = Tools.setFontMedium(size: 14)
        getPromotionLabel.textColor = .BBVA600
        getPromotionLabel.text = Localizables.promotions.key_promotions_how_get_promotion_text

        onlinePresencialLabel.font = Tools.setFontBookItalic(size: 14)
        onlinePresencialLabel.textColor = .BBVA500

        lineView.backgroundColor = .BBVA300
        lineView.alpha = 0.5

        scrollView.backgroundColor = .BBVAWHITE
        view.backgroundColor = .BBVAWHITE

        promoOnlineImage.image = ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Promotions.ecommerce, color: .BBVA300), width: Float(promoOnlineImage.frame.size.width), height: Float(promoOnlineImage.frame.size.height))

        promoPresencialImage.image = ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Promotions.shop_icon, color: .BBVA300), width: Float(promoPresencialImage.frame.size.width), height: Float(promoPresencialImage.frame.size.height))

    }

    func configureCornerView() {

        let newSize = ShoppingDetailViewController.cornerSize + UIApplication.shared.statusBarFrame.height
        let rect = CGRect(x: cornerView.frame.origin.x, y: cornerView.frame.origin.y, width: newSize, height: newSize)
        cornerView.frame = rect

        let path = UIBezierPath()
        path.move(to: CGPoint(x: 0.0, y: rect.size.height))
        path.addLine(to: CGPoint(x: 0.0, y: 0.0))
        path.addLine(to: CGPoint(x: rect.size.width, y: 0.0))
        path.addLine(to: CGPoint(x: 0.0, y: rect.size.height))
        path.close()

        let layer = CAShapeLayer()
        layer.path = path.cgPath
        layer.fillColor = UIColor.NAVY.cgColor
        layer.strokeColor = nil

        cornerView.layer.addSublayer(layer)
    }

    override func dismissAction() {
        BusManager.sessionDataInstance.dismissViewController(animated: true, completion: { [weak self] () -> Void in
            self?.presenter.dismissed()
        })
    }
}

extension ShoppingDetailViewController: ShoppingDetailViewProtocol {
    func showWantThisOffer() {
        wantThisOfferView.isHidden = false
        wantThisOfferHeightConstraint.constant = ShoppingDetailViewController.wantThisOfferDefaultHeight
    }
    
    func hideWantThisOffer() {
        wantThisOfferView.isHidden = true
        wantThisOfferHeightConstraint.constant = 0
    }
    
    func showSkeleton() {

        if skeletonView == nil {
            skeletonView = PromoDetailSkeletonView.getSkeletonView(frame: basicDetailView.frame)
            basicDetailView.addSubview(skeletonView)
        }
    }

    func hideSkeleton() {

        if skeletonView != nil {
            skeletonView.removeFromSuperview()
            skeletonView = nil
        }
    }

    func showErrorView() {

        if errorView == nil {
            errorView = PromotionErrorView.getErrorView(frame: basicDetailView.frame)
            errorView.delegate = self
            basicDetailView.addSubview(errorView)
        }
    }

    func hideErrorView() {

        if errorView != nil {
            errorView.removeFromSuperview()
            errorView = nil
        }
    }

    func showPromo(withDisplayItemPromotion displayItemPromotion: DisplayItemPromotion) {

        promotionID = displayItemPromotion.promotionBO?.id
        commerceNameLabel.text = displayItemPromotion.commerceName
        commerceDescriptionLabel.text = displayItemPromotion.descriptionPromotion
        commerceDescriptionLabel.sizeToFit()
        benefitLabel.text = displayItemPromotion.benefit

        if let forcedDateLabel = self.distanceAndDateToShow {
            validDateLabel.text = forcedDateLabel
        } else {
            validDateLabel.text = displayItemPromotion.dateDiffPlusDistance ?? displayItemPromotion.dateDiff
        }

        if let text = validDateLabel.text, text.isEmpty {
            validDateLabelHeight.constant = 0.0
        } else if validDateLabel.text == nil {
            validDateLabelHeight.constant = 0.0
        }

        if let usageTypeDescription = displayItemPromotion.usageTypeDescription {

            onlinePresencialLabel.text = usageTypeDescription

            if !displayItemPromotion.usageTypeOnline {
                promoOnlineImageWidth.constant = 0.0
                promoOnlineImageSpace.constant = 0.0
            }

            if !displayItemPromotion.usageTypePhysical {
                promoPresencialImageWidth.constant = 0.0
                promoPresencialImageSpace.constant = 0.0
            }
        }
    }
    
    func updatePromo(withDisplayItemPromotion displayItemPromotion: DisplayItemPromotion) {
        showPromo(withDisplayItemPromotion: displayItemPromotion)
    }

    func showPromotionType(withPromotionTypeDisplay promotionTypeDisplay: PromotionTypeDisplayData) {

        promotionTypeViewWidth.constant = promotionTypeDisplay.promotionTypeLabelWidth
            + ShoppingDetailViewController.spaceSize
        promotionTypeView.configurePromotionType(withPromotionTypeDisplayData: promotionTypeDisplay)
    }

    func showPromoDetail(withDisplayItemPromotionDetail displayItemPromotionDetail: DisplayItemPromotionDetail) {

        basicDetailView.showDisplayItemPromotionDetail(withDisplayItemPromotionDetail: displayItemPromotionDetail)
        basicDetailView.sizeToFit()
    }
    
    func showPromoContact(withPromoContactDisplay promotionContactDisplay: ContactUsDisplayData) {
        
        contactUsView.isHidden = false
        contactUsView.showContactUsDisplay(withContactUsDisplay: promotionContactDisplay)
        contactUsView.sizeToFit()
    }
    
    func showPromoDetailImage(withPromoDetailImage promoDetailImage: UIImage?) {

        if promoDetailImage != nil {

            detailImage.image = promoDetailImage
            detailImage.backgroundColor = .white
        } else {

            detailImage.backgroundColor = .BBVA200
        }

    }

    func updatePromoDetail(withDisplayItemPromotionDetail displayItemPromotionDetail: DisplayItemPromotionDetail) {

        basicDetailView.updateDisplayItemPromotionDetail(withDisplayItemPromotionDetail: displayItemPromotionDetail)
        basicDetailView.sizeToFit()
    }

    func setDistanceAndDateToShow(withString distanceAndDateToShow: String) {

        self.distanceAndDateToShow = distanceAndDateToShow
    }

    func showError(error: ModelBO) {

        presenter.showModal()
    }

    func changeColorEditTexts() {

    }
    
    func showHelp(withDisplayData displayData: HelpDisplayData) {
        helpView.delegate = self
        helpView.isHidden = false
        helpView.setHelpDisplayData(forHelpDisplayData: displayData)
    }
    
}

extension ShoppingDetailViewController: CloseModalDelegate {

    func errorViewControllerAcceptPressed(_ errorViewController: ErrorViewController) {

        presenter.checkError()
    }

    func errorViewControllerCancelPressed(_ errorViewController: ErrorViewController) {

    }
}

extension ShoppingDetailViewController: PromotionErrorViewDelegate {

    func promotionErrorViewReload(_ promotionErrorView: PromotionErrorView) {

        presenter.reloadInfo()
    }

}

extension ShoppingDetailViewController: BasicDetailViewDelegate {

    func basicDetailShowMoreInformation(_ basicDetail: BasicDetailView) {

        presenter.showMoreInformation()
    }

    func basicDetailShowTermsAndConditions(_ basicDetail: BasicDetailView) {

        presenter.showTermsAndConditions()
    }
}

extension ShoppingDetailViewController: WantThisOfferViewDelegate {
    func wantThisOfferShowStoreWebPage(_ WantThisOffer: WantThisOfferView) {
        presenter.showPromotionUrl()
    }
}

extension ShoppingDetailViewController: HelpViewDelegate {
    
    func helpButtonPressed(_ helpView: HelpView, andHelpId helpId: String) {
         presenter.helpButtonPressed(withHelpId: helpId)
    }
}

extension ShoppingDetailViewController: ContactUsViewDelegate {
    
    func contactUsShowContactDetailsPressed(_ contactUs: ContactUsView) {
    
        presenter.showContactDetails()
    }
    
    func contactUsShowGuaranteePressed(_ contactUs: ContactUsView) {
        
        presenter.showGuarantee()
    }
}

// MARK: - APPIUM

private extension ShoppingDetailViewController {

    func setAccessibilities() {

        #if APPIUM

        #endif

    }
}
