//
//  ShoppingDetailViewControllerB.swift
//  shoppingapp
//
//  Created by Rubén Jacobo on 17/09/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class ShoppingDetailViewControllerB: ShoppingDetailViewControllerA {

    @IBOutlet weak var associatedCards: AssociatedCardsView!
    @IBOutlet weak var creditCardRequestView: CreditCardRequestView!
}

extension ShoppingDetailViewControllerB: ShoppingDetailViewProtocolB {
    
    func showCardsViewWith(displayDataList: [AssociatedCardsDisplayData]) {
        
        associatedCards.cards = displayDataList
        associatedCards.configureView()
        associatedCards.isHidden = false
    }
    
    func showCreditCardRequestView() {
        
        creditCardRequestView.configureView()
        creditCardRequestView.isHidden = false
        creditCardRequestView.delegate = self
    }
}

extension ShoppingDetailViewControllerB: CreditCardRequestViewDelegate {
    
    func showCreditCardRequestLinkOnWebPage(_ creditCardRequest: CreditCardRequestView) {
        
        (presenter as! ShoppingDetailPresenterProtocolB).showCreditCardRequestUrl()
    }
}
