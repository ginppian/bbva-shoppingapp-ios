//
//  DisplayItemPromotionDetail.swift
//  shoppingappCL
//
//  Created by Luis Monroy on 08/01/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

struct DisplayItemPromotionDetail: Equatable {

    var summaryDescription: String?
    var extendedDescription: String?
    var legalConditions: String?
    var legalConditionsURL: String?
    var commerceName: String?
    var commerceDescription: String?
    var commerceWebPage: String?
    
    static func generateDisplayItemPromotionDetail(fromPromotion promotion: PromotionBO) -> DisplayItemPromotionDetail {

        var displayItemPromotionDetail = DisplayItemPromotionDetail()

        if let descriptionsBO = promotion.descriptions {

            for descriptionBO in descriptionsBO {

                if descriptionBO.id == .summary {
                    displayItemPromotionDetail.summaryDescription = descriptionBO.value
                } else if descriptionBO.id == .extended {
                    displayItemPromotionDetail.extendedDescription = descriptionBO.value
                }
            }
        }

        displayItemPromotionDetail.legalConditions = promotion.legalConditions?.text

        displayItemPromotionDetail.legalConditionsURL = promotion.legalConditions?.url

        if let commerceInfo = promotion.commerceInformation {

            displayItemPromotionDetail.commerceName = commerceInfo.name
            displayItemPromotionDetail.commerceDescription = commerceInfo.description
            displayItemPromotionDetail.commerceWebPage = commerceInfo.web
        }
        
        return displayItemPromotionDetail
    }
}
