//
//  ShoppingDetailViewControllerA.swift
//  shoppingapp
//
//  Created by Luis Monroy on 25/05/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import MessageUI
import CoreLocation

class ShoppingDetailViewControllerA: ShoppingDetailViewController {

    @IBOutlet weak var associateCommerce: AssociateCommerceView!
    @IBOutlet weak var associatedTradeMapView: AssociatedTradeMapView!

}

extension ShoppingDetailViewControllerA: ShoppingDetailViewProtocolA {

    func showPromoStore(withAssociateCommerceDisplay displayAssociateCommerce: AssociateCommerceDisplayData) {

        associateCommerce.isHidden = false
        associateCommerce.showAssociateCommerceDisplay(withAssociateCommerceDisplay: displayAssociateCommerce)
        associateCommerce.delegate = self
        associateCommerce.sizeToFit()
    }

    func hidePromoStore() {

        associateCommerce.isHidden = true
    }

    func showAssociatedTradeMap(withDisplayData displayData: AssociatedTradeMapDisplayData) {

        associatedTradeMapView.isHidden = false
        associatedTradeMapView.setupAssociatedTradeMapView(withDisplayData: displayData)
        associatedTradeMapView.delegate = self
    }

    func hideAssociatedTradeMap() {

        associatedTradeMapView.isHidden = true
    }

    func callToPhone(withPhone phone: String) {

        let tel = "\(Constants.tel_scheme):\(phone)"
        _ = URLOpener().openURL(tel)
    }

    func sendEmail(withEmail email: String) {

        _ = Tools.sendEmail(withEmail: email, mailComposeDelegate: self)
    }

    func launchMap(withDestinationCoordinate destinationCoordinate: CLLocationCoordinate2D, andOriginCoordinate originCoordinate: CLLocationCoordinate2D?) {

        Tools.launchGoogleMapInDirectionsModeWalking(withDestinationCoordinate: destinationCoordinate, andOriginCoordinate: originCoordinate)
    }
}

extension ShoppingDetailViewControllerA: AssociateCommerceViewDelegate {
    
    func associateCommerceShowStoreList(_ associateCommerce: AssociateCommerceView) {
        
        (presenter as? ShoppingDetailPresenterProtocolA)?.showStoreList()
    }
    
    func associateCommerceShowStoreWebPage(_ associateCommerce: AssociateCommerceView) {

        (presenter as? ShoppingDetailPresenterProtocolA)?.showStoreWebPage()
    }
}

extension ShoppingDetailViewControllerA: AssociatedTradeMapViewDelegate {

    func associatedTradeMapViewPressedWebsite(_ associatedTradeMapView: AssociatedTradeMapView) {

        (presenter as? ShoppingDetailPresenterProtocolA)?.pressedWebsiteOfAssociatedTrade()
    }

    func associatedTradeMapViewPressedCallToPhone(_ associatedTradeMapView: AssociatedTradeMapView) {

        (presenter as? ShoppingDetailPresenterProtocolA)?.pressedCallToPhoneOfAssociatedTrade()
    }

    func associatedTradeMapViewPressedSendEmail(_ associatedTradeMapView: AssociatedTradeMapView) {

        (presenter as? ShoppingDetailPresenterProtocolA)?.pressedSendEmailOfAssociatedTrade()
    }

    func associatedTradeMapViewPressedLaunchMap(_ associatedTradeMapView: AssociatedTradeMapView) {

        (presenter as? ShoppingDetailPresenterProtocolA)?.pressedLaunchMapOfAssociatedTrade()
    }
}

extension ShoppingDetailViewControllerA: MFMailComposeViewControllerDelegate {

    public func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}
