//
//  PromotionErrorView.swift
//  shoppingapp
//
//  Created by Luis Monroy on 18/03/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol PromotionErrorViewDelegate: class {
    func promotionErrorViewReload(_ promotionErrorView: PromotionErrorView)
}

class PromotionErrorView: UIView {

    @IBOutlet weak var promoErrorImage: UIImageView!
    @IBOutlet weak var promoErrorLabel: UILabel!
    @IBOutlet weak var promoErrorButton: UIButton!

    weak var delegate: PromotionErrorViewDelegate?

    @IBAction func reload(_ sender: Any) {

        delegate?.promotionErrorViewReload(self)
    }

    static func getErrorView(frame: CGRect) -> PromotionErrorView? {
        let className = String(describing: self)
        let errorView: PromotionErrorView!  = Bundle.main.loadNibNamed(className, owner: nil, options: nil)?.first as? PromotionErrorView

        if errorView != nil {
            errorView.frame.size.width = frame.size.width
            errorView.frame.size.height = frame.size.height

            errorView.promoErrorLabel.font = Tools.setFontBook(size: 14)
            errorView.promoErrorLabel.textColor = .BBVA600
            errorView.promoErrorLabel.text = Localizables.common.key_error_ocurred_try_again_text

            errorView.promoErrorButton.titleLabel?.font = Tools.setFontBold(size: 14)
            errorView.promoErrorButton.setTitleColor(.MEDIUMBLUE, for: .normal)
            errorView.promoErrorButton.setTitleColor(.COREBLUE, for: .highlighted)
            errorView.promoErrorButton.setTitleColor(.COREBLUE, for: .selected)
            errorView.promoErrorButton.setTitle(Localizables.transactions.key_transactions_reload_text, for: .normal)

            errorView.promoErrorImage.image = ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Common.alert_icon, color: .BBVALIGHTRED), width: Float(errorView.promoErrorImage.frame.size.width), height: Float(errorView.promoErrorImage.frame.size.height))
        }

        return errorView
    }
}
