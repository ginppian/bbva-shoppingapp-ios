//
//  TransactionDetailViewController.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 15/11/2017.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit
import CellsNativeCore
import CellsNativeComponents
import AudioToolbox

class TicketDetailViewController: BaseViewController {

    static let ID: String = String(describing: TicketDetailViewController.self)

    var presenter: TicketDetailPresenterProtocol!

    @IBOutlet weak var disclaimerView: DisclaimerView!
    @IBOutlet weak var ticketDetailView: TicketDetailView!
    @IBOutlet weak var optionsBarView: ButtonsBarView!
    @IBOutlet weak var deniedConceptView: UIView!
    @IBOutlet weak var deniedConceptLabel: UILabel!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var scrollView: UIScrollView!

    override func viewDidLoad() {

        configure(appLifecycle: (UIApplication.shared.delegate as! AppDelegate).appLifeCycle!, id: TicketDetailViewController.ID)

        super.viewDidLoad()

        presenter.viewDidLoad()

        optionsBarView.delegate = self
        disclaimerView.delegate = self

        separatorView.backgroundColor = .BBVA300
        deniedConceptLabel.font = Tools.setFontBook(size: 14)
        deniedConceptLabel.textColor = .BBVA500

        if #available(iOS 11.0, *) {
            scrollView.contentInsetAdjustmentBehavior = .never
        }
    }

    override func viewWillAppear(_ animated: Bool) {

        super.viewWillAppear(animated)

        configureNavigationBar()

        presenter.viewWillAppear()

        setAccessibilities()

        BusManager.sessionDataInstance.disableSwipeLateralMenu()
    }

    func configureNavigationBar() {

        setNavigationBarDefault()

        let image = SVGCache.image(forSVGNamed: ConstantsImages.Common.close_icon, color: .BBVAWHITE)

        setNavigationLeftButton(withImage: image, action: #selector(backAction))

        setNavigationTitle(withText: Localizables.notifications.key_ticket_text)

    }

    override func backAction() {
        // Dismiss the current Notification ViewController.
        dismissAction()
    }
    
    override func initModel(withModel model: Model) {
        presenter.setModel(model: model)
    }

    deinit {
        ComponentManager.remove(id: presenter.routerTag)
        DLog(message: "Deinitialized TicketDetailViewController")
    }
}

extension TicketDetailViewController: TicketDetailViewProtocol {

    func showTicketData(withDisplayData displayData: TicketDisplayData) {
        ticketDetailView.getTicketDisplayData(withTicketDisplayData: displayData)

        if let deniedTextSave = displayData.deniedText {
            deniedConceptView.isHidden = false
            deniedConceptLabel.text = deniedTextSave
        } else {
            deniedConceptView.isHidden = true
        }

    }
    
    func makeDeviceVibrate() {
        
        AudioServicesPlayAlertSound(SystemSoundID(kSystemSoundID_Vibrate))
    }

    func showError(error: ModelBO) {
    }

    func changeColorEditTexts() {
    }

    func showOperativeBarInfo(withDisplayData displayData: ButtonsBarDisplayData) {
        optionsBarView.show(displayButtonsBarDisplayData: displayData)
    }

    func showCallBBVA(tel: String) {

        let scheme = "tel:\(tel)"
        let urlOpener = URLOpener()

        if let url = URL(string: scheme) {
            urlOpener.openURL(url)
        }
    }

    func showDisclaimerData(withDisplayData displayData: DisclaimerDisplayData) {
        disclaimerView.setDisclaimerInformation(withDisclaimerData: displayData)
    }

    func sendOmnitureAcceptedPurchase() {
        TrackerHelper.sharedInstance().trackAcceptedPurchaseTicketScreen(withCustomerID: SessionDataManager.sessionDataInstance().customerID)

    }

    func sendOmnitureDeniedPurchase() {
        TrackerHelper.sharedInstance().trackDeniedPurchaseTicketScreen(withCustomerID: SessionDataManager.sessionDataInstance().customerID)
    }

}

extension TicketDetailViewController: ButtonsBarViewDelegate {

    func buttonBarPressed(_ buttonsBarView: ButtonsBarView, withDisplayItem displayItem: ButtonBarDisplayData) {
        presenter.buttonBarOptionPressed(withDisplayItem: displayItem)
    }
}

extension TicketDetailViewController: DisclaimerViewDelegate {

    func disclaimerViewClickTextDisclaimer(_ disclaimerView: DisclaimerView) {
        presenter.setNotRecognizedOperations()
    }

}

// MARK: - APPIUM

private extension TicketDetailViewController {

    func setAccessibilities() {
        #if APPIUM
            Tools.setAccessibility(view: disclaimerView.informationButton, identifier: ConstantsAccessibilities.TRANSACTION_DETAIL_DID_OPERATION_TITLE)
        #endif
    }

}
