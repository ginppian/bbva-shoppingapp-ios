//
//  TransactionDisplayItem.swift
//  shoppingapp
//
//  Created by jesus.martinez on 7/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit

struct TicketDisplayData {

    var image: UIImage?
    var coverUrl: String = ""
    var pan: String = ""
    var date: String = ""
    var concept: String = ""
    var amount: NSAttributedString?
    var type: String = ""
    var typeColor: UIColor?
    var deniedText: String?

    static func generateTicketDisplayData(withTicketModel ticketModel: TicketModel) -> TicketDisplayData {

        var ticketDisplayData = TicketDisplayData()

        ticketDisplayData.pan = "•" + ticketModel.pan5
        ticketDisplayData.concept = ticketModel.concept

        let ticketDate = Date.date(fromServerString: ticketModel.receiptDate, withFormat: Date.DATE_FORMAT_PUSH)

        let stringDate = Date.string(fromDate: ticketDate!, withFormat: Date.DATE_FORMAT_PUSH_FORMATTED)

        ticketDisplayData.date = stringDate

        let amountFormatter = AmountFormatter(codeCurrency: ticketModel.transactionCurrency)

        let amountDecimal = NSDecimalNumber(string: ticketModel.transactionAmount)

        ticketDisplayData.amount = amountFormatter.attributtedText(forAmount: amountDecimal, bigFontSize: 48, smallFontSize: 23)

        switch ticketModel.type {

        case .purchaseAccepted:
            ticketDisplayData.type = Localizables.common.key_gcm_notification_accepted_purchase_title
            ticketDisplayData.typeColor = .BBVAGREEN56

        case .purchaseDenied:
            ticketDisplayData.type = Localizables.common.key_gcm_notification_denied_purchase_title
            ticketDisplayData.typeColor = .BBVAPINK212

        default:
            ticketDisplayData.typeColor = .clear

        }

        let stringKey = "imageUrl" + "-" + ticketModel.pan5 + ticketModel.bin

        let imageUrl = PreferencesManager.sharedInstance().getValue(forKey: stringKey)

        if let imageUrlSave = imageUrl as? String {

            ticketDisplayData.coverUrl = imageUrlSave

            let imageData = CardImageStorage.getCardImageData(withUrl: imageUrlSave)

            if let imageData = imageData?.image {
                ticketDisplayData.image = UIImage(data: imageData)
            } else {
                ticketDisplayData.image = UIImage(named: ConstantsImages.Card.blank_card)
            }

        } else {
            ticketDisplayData.image = UIImage(named: ConstantsImages.Card.blank_card)
        }

        if ticketModel.type == .purchaseDenied {

            switch ticketModel.deniedText {
            case "107":
                ticketDisplayData.deniedText = Localizables.notifications.key_unrealized_transaction_retry_text
            case "151":
                ticketDisplayData.deniedText = Localizables.notifications.key_not_balance_text
            case "175":
                ticketDisplayData.deniedText = Localizables.notifications.key_exceeded_credit_text
            case "188":
                ticketDisplayData.deniedText = Localizables.notifications.key_amount_purchase_exceeded_credit_text
            case "229":
                ticketDisplayData.deniedText = Localizables.notifications.key_card_off_text
            case "244":
                ticketDisplayData.deniedText = Localizables.notifications.key_card_reported_for_lost_text
            case "245":
                ticketDisplayData.deniedText = Localizables.notifications.key_card_reported_for_theft_text
            case "252":
                ticketDisplayData.deniedText = Localizables.notifications.key_wrong_expiration_date_text
            case "795", "796", "797", "937", "938":
                ticketDisplayData.deniedText = Localizables.notifications.key_enter_the_application_for_purchase_text
            case "935", "939":
                ticketDisplayData.deniedText = Localizables.notifications.key_wrong_security_code_text
            case "936", "940":
                ticketDisplayData.deniedText = Localizables.notifications.key_exceeded_number_attemps_security_code_text
            default:
                break
            }
        }

        return ticketDisplayData
    }

}
