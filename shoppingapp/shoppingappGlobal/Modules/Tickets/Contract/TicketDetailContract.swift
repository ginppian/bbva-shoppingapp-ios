//
//  TicketDetailViewContract.swift
//  shoppingapp
//
//  Created by Ruben Fernandez on 07/02/2018.
//  Copyright © 2017 BBVA. All rights reserved.
//

import RxSwift

protocol TicketDetailViewProtocol: ViewProtocol {
    func showCallBBVA(tel: String)
    func showOperativeBarInfo(withDisplayData displayData: ButtonsBarDisplayData)
    func showDisclaimerData(withDisplayData displayData: DisclaimerDisplayData)
    func showTicketData(withDisplayData displayData: TicketDisplayData)
    func makeDeviceVibrate()
    func sendOmnitureAcceptedPurchase()
    func sendOmnitureDeniedPurchase()
}

protocol TicketDetailPresenterProtocol: PresenterProtocol {
    func viewDidLoad()
    func viewWillAppear()
    func buttonBarOptionPressed(withDisplayItem displayItem: ButtonBarDisplayData)
    func setNotRecognizedOperations()
}
