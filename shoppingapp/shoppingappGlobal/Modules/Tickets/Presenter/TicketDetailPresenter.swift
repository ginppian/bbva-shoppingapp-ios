//
//  TransactionDetailPresenter.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 15/11/2017.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation
import RxSwift
import CellsNativeComponents

class TicketDetailPresenter<T: TicketDetailViewProtocol>: BasePresenter<T> {

    var ticketDisplayData: TicketDisplayData?
    var ticketModel: TicketModel?

    required init() {
        super.init(tag: TicketDetailPageReaction.ROUTER_TAG)
    }

    override init(busManager: BusManager) {
        super.init(busManager: busManager)
        routerTag = TicketDetailPageReaction.ROUTER_TAG
    }

    override func setModel(model: Model) {
        ticketModel = model as? TicketModel
    }
}

extension TicketDetailPresenter: TicketDetailPresenterProtocol {

    func viewDidLoad() {
        guard let ticketModel = ticketModel else {
            return
        }

        if ticketModel.type == .purchaseAccepted {
            view?.sendOmnitureAcceptedPurchase()
        } else if ticketModel.type == .purchaseDenied {
            view?.sendOmnitureDeniedPurchase()
        }

        let vibrationEnabled = PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kConfigVibrationEnabled)
        let isVibrationEnabled = (vibrationEnabled as? Bool) ?? true
        
        if isVibrationEnabled {
            view?.makeDeviceVibrate()
        }
    }
    
    func viewWillAppear() {
        
        view?.showOperativeBarInfo(withDisplayData: ButtonsBarDisplayData.generateButtonsBarOnTransactionDisplayData())
        view?.showDisclaimerData(withDisplayData: DisclaimerDisplayData())
        if let ticketModel = ticketModel {
            view?.showTicketData(withDisplayData: TicketDisplayData.generateTicketDisplayData(withTicketModel: ticketModel))
        }
    }

    func buttonBarOptionPressed(withDisplayItem displayItem: ButtonBarDisplayData) {

        switch displayItem.optionKey {
        case .callBBVA:
            view?.showCallBBVA(tel: Settings.Global.default_bbva_phone_number)
        default:
            break
        }
    }

    func setNotRecognizedOperations() {

        let notRecognizedOperationUtils = NotRecognizedOperationUtils()
        notRecognizedOperationUtils.startFindNotRecognizedOperationViewController(close: false)
        
        if !notRecognizedOperationUtils.foundCellsViewControllerPresented
            && !notRecognizedOperationUtils.foundCellsViewControllerPushed {
            
            let notRecognizedOperationTransport = NotRecognizedOptionsTransport(isBlockHidden: true, isPowerOffHidden: true, isCallHidden: false, isLoginHidden: false)
            
            if let notRecognizedOperationTransportJSON = notRecognizedOperationTransport.toJSON() {
                
                busManager.publishData(tag: TicketDetailPageReaction.ROUTER_TAG, TransactionDetailPageReaction.PARAMS_NOT_RECOGNIZED_OPTIONS, notRecognizedOperationTransportJSON)
            }
        }
        
        busManager.navigateScreen(tag: TicketDetailViewController.ID, TransactionDetailPageReaction.ACTION_NAVIGATE_TO_NOT_OPERATION, EmptyModel())
    }
}
