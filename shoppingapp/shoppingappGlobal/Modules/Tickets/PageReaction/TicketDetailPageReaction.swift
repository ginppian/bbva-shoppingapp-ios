//
//  TransactionDetailPageReaction.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 26/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

class TicketDetailPageReaction: PageReaction {

    static let ROUTER_TAG: String = "ticket-detail-tag"

    static let EVENT_NAV_TO_NEXT_SCREEN: ActionSpec<Void> = ActionSpec<Void>(id: "EVENT_NAV_TO_NEXT_SCREEN")

    override func configure() {

        registerReactionToWriteInChannelParamsToShowNotRecognizedOptions()
        
//        navigateToNotRecognizedOperation()
    }

    func registerReactionToWriteInChannelParamsToShowNotRecognizedOptions() {

        let channel: JsonChannel = JsonChannel(TransactionDetailPageReaction.SHOW_NOT_RECOGNIZED_OPTIONS_CHANNEL)

        _ = writeOn(channel: channel)
            .when(componentID: TicketDetailPageReaction.ROUTER_TAG)
            .doAction(actionSpec: TransactionDetailPageReaction.PARAMS_NOT_RECOGNIZED_OPTIONS)
    }

//    func navigateToNotRecognizedOperation() {
//
//        _ = when(id: TicketDetailViewController.ID, type: Model.self)
//            .doAction(actionSpec: TransactionDetailPageReaction.ACTION_NAVIGATE_TO_NOT_OPERATION)
//            .then()
//            .inside(componentID: WebController.ID, component: WebController.self)
//            .doReactionAndClearChannel { webController, _ in
//
//                if let webController: WebController = ComponentManager.get(id: WebController.ID) {
//
//                    webController.navigateTo(route: TransactionDetailPageReaction.CELLS_PAGE_NOT_RECOGNIZED_OPERATION, payload: nil)
//                }
//        }
//
//        _ = reactTo(channel: WebController.ON_PAGE_LOADED_CHANNEL)?
//            .then()
//            .inside(componentID: WebController.ID, component: WebController.self)
//            .doReactionAndClearChannel { _, page in
//
//                if page == TransactionDetailPageReaction.CELLS_PAGE_NOT_RECOGNIZED_OPERATION,
//                    let viewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: NotRecognizedOperationViewController.ID) {
//                        viewController.modalPresentationStyle = .overCurrentContext
//                        let navigationController = UINavigationController(rootViewController: viewController)
//                        BusManager.sessionDataInstance.present(viewController: navigationController, animated: true, completion: nil)
//                }
//        }
//    }
    
}
