//
//  ShoppingListFilteredPresenter.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 8/3/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeComponents
import RxSwift

class ShoppingListFilteredPresenter<T: ShoppingListFilteredViewProtocol>: BasePresenter<T> {

    var interactor: ShoppingListFilteredInteractorProtocol!

    let disposeBag = DisposeBag()

    var filters: ShoppingFilter?
    var defaultLocation = Settings.Global.default_location
    var defaultDistance = Settings.PromotionsMap.default_distance
    var lengthType = Settings.PromotionsMap.default_length_type
    var pageSize: Int?
    var userLocation: GeoLocationBO?
    var isLoadingNextPage = false

    var promotionsBO: PromotionsBO?
    var categoriesBO: CategoriesBO?

    var displayItems = DisplayItemsPromotions()

    var locationManager: LocationManagerProtocol?

    required init() {
        super.init(tag: ShoppingListFilteredPageReaction.ROUTER_TAG)
    }

    override init(busManager: BusManager) {
        super.init(busManager: busManager)
        routerTag = ShoppingListFilteredPageReaction.ROUTER_TAG
    }

    override func setModel(model: Model) {

        if let shoppingFilterTransport = model as? ShoppingFilterTransport {
            filters = shoppingFilterTransport.filters
            categoriesBO = shoppingFilterTransport.categoriesBO
        }
    }

    private func sendScreenOmniture(filters: ShoppingFilter?, promotions: PromotionsBO?) {

        var promotionTypes: [String]
        if let filters = filters, let promotionTypesFilter = filters.promotionTypeFilter {
             promotionTypes = promotionTypesFilter.map { $0.literal }
        } else {
            promotionTypes = [String]()
        }

        var promotionUsageType: [String]
        if let filters = filters, let promotionUsageTypeFilter = filters.promotionUsageTypeFilter {
            promotionUsageType = promotionUsageTypeFilter.map { $0 == .physical ? "offline" : "online" }
        } else {
            promotionUsageType = [String]()
        }

        var textInFilter = ""
        if let filters = filters, let textFilter = filters.textFilter {
            textInFilter = textFilter
        }

        var totalElement = String(promotions?.promotions.count ?? 0)
        if let totalElementsFiltered = promotions?.pagination?.totalElements {
            totalElement = String(totalElementsFiltered)
        }

        let noResults = promotions != nil ? promotions!.promotions.isEmpty : true

        TrackerHelper.sharedInstance().trackPromotionsFilteredScreen(withCustomerID: SessionDataManager.sessionDataInstance().customerID, andSearchText: textInFilter.diacriticInsensitiveAndLowerCase(), promotionType: promotionTypes.joined(separator: ",").diacriticInsensitiveAndLowerCase(), online: promotionUsageType.joined(separator: ","), totalElement: totalElement, noResults: noResults)

    }

    func shoppingParams(forLocation location: GeoLocationBO) -> ShoppingParamsTransport {

        let locationTranport = ShoppingParamLocation(geolocation: location, distanceParam: ShoppingDistanceParam(distance: defaultDistance, lengthType: lengthType))
        return ShoppingParamsTransport(location: locationTranport, pageSize: pageSize)

    }

    func retrievePromotions(withLocation location: GeoLocationBO) {

        view?.showSkeleton()

        let transport = shoppingParams(forLocation: location)

        interactor?.providePromotions(byParams: transport, withFilters: filters).subscribe(

            onNext: { [weak self] result in
                guard let `self` = self else {
                    return
                }

                if let resultPromotionsBO = result as? PromotionsBO {

                    self.sendScreenOmniture(filters: self.filters, promotions: resultPromotionsBO)

                    self.promotionsBO = resultPromotionsBO

                    if let nextPage = self.promotionsBO?.pagination?.links?.next, !nextPage.isEmpty {

                        self.view?.updateForMoreContent()
                    } else {

                        self.view?.updateForNoMoreContent()
                    }

                    if self.promotionsBO?.status == ConstantsHTTPCodes.NO_CONTENT {

                        self.view?.showNoContentView()
                    } else {

                        self.view?.hideSkeleton()
                        self.updatePromotions(withPromotions: resultPromotionsBO)
                    }
                } else {

                    self.sendScreenOmniture(filters: self.filters, promotions: nil)

                }
            },
            onError: { [weak self] error in
                guard let `self` = self else {
                    return
                }
                self.view?.hideSkeleton()
                self.view?.showError()

                let evaluate: ServiceError = (error as? ServiceError)!

                switch evaluate {

                case .GenericErrorBO(let errorBO):
                    self.errorBO = errorBO
                    self.checkError()
                default:
                    break
                }

            }).addDisposableTo(disposeBag)
    }

    func showPromotions(withPromotions promotions: PromotionsBO) {

        for i in 0 ..< promotions.promotions.count {

            let displayItemPromotion = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotions.promotions[i], withTypeCell: CellPromotionType.small, userLocation: userLocation)

            displayItems.items?.append(displayItemPromotion)
        }

        view?.showPromotions(withDisplayItemsPromotions: displayItems)
    }

    func updatePromotions(withPromotions promotions: PromotionsBO) {

        displayItems = DisplayItemsPromotions()

        showPromotions(withPromotions: promotions)
    }

    func navigateToDetailScreen(withTransport transport: PromotionDetailTransport) {

        busManager.navigateScreen(tag: routerTag, ShoppingListFilteredPageReaction.EVENT_NAV_TO_DETAIL_SCREEN, transport)
    }

    func getLocation() {
        locationManager = locationManager ?? LocationManager()
        locationManager!.startLocationObserverAndCheckStatus()
        locationManager?.getLocation(onSuccess: { [weak self] location -> Void in
            if let `self` = self {
                if self.userLocation == nil {
                    self.userLocation = GeoLocationBO(withLatitude: location.coordinate.latitude, withLongitude: location.coordinate.longitude)
                    self.retrievePromotions(withLocation: self.userLocation!)
                }
            }
            }, onFail: { [weak self] _ -> Void in
                if let `self` = self {
                    self.retrievePromotions(withLocation: self.defaultLocation)
                }
        })

    }
}

// MARK: - Private methods

private extension ShoppingListFilteredPresenter {

    func loadFilters() {

        if let filters = filters {

            if let categories = categoriesBO {

                var displayFilters = [FilterScrollDisplayData]()

                for titleKey in filters.retrieveLiterals(withCategories: categories) {

                    displayFilters.append(FilterScrollDisplayData(title: titleKey.title, filterType: titleKey.key))
                }

                if !filters.isEmpty() {
                    view?.showFilters(withDisplayData: displayFilters)
                } else {
                    view?.hideFilters()
                }
            } else {

                view?.hideFilters()
            }
        }
    }
}

// MARK: - ShoppingListFilteredPresenterProtocol

extension ShoppingListFilteredPresenter: ShoppingListFilteredPresenterProtocol {

    func viewWillAppear() {

        view?.showSkeleton()
        getLocation()
        loadFilters()
    }

    func loadDataNextPage() {

        if isLoadingNextPage == false, let promotions = promotionsBO, let paginationLinks = promotions.pagination?.links, let next = paginationLinks.next, !next.isEmpty {

            view?.showNextPageAnimation()

            isLoadingNextPage = true

            let nextURL = paginationLinks.generateNextURL(isFinancial: false)

            interactor.providePromotionsWithFilters(withNextPage: nextURL).subscribe(
                onNext: { [weak self] result in
                    guard let `self` = self else {
                        return
                    }
                    self.isLoadingNextPage = false
                    self.view?.hideNextPageAnimation()

                    if let promotionsResult = result as? PromotionsBO {

                        self.promotionsBO?.pagination = promotionsResult.pagination

                        if promotionsResult.status == ConstantsHTTPCodes.STATUS_OK {

                            if let nextPage = self.promotionsBO?.pagination?.links?.next {
                                if nextPage.isEmpty {
                                    self.view?.updateForNoMoreContent()
                                } else {
                                    self.view?.updateForMoreContent()
                                }

                            } else {
                                self.view?.updateForNoMoreContent()
                            }

                            //Updates promotions
                            self.showPromotions(withPromotions: promotionsResult)

                        } else {
                            self.view?.updateForNoMoreContent()
                        }

                    }
                },
                onError: { [weak self] error in
                    guard let `self` = self else {
                        return
                    }
                    self.isLoadingNextPage = false

                    self.view?.updateForNoMoreContent()
                    self.view?.hideNextPageAnimation()

                    let evaluate: ServiceError = (error as? ServiceError)!

                    switch evaluate {

                    case .GenericErrorBO(let errorBO):
                        self.errorBO = errorBO
                        self.checkError()
                    default:
                        break
                    }
                }).addDisposableTo(self.disposeBag)

        }
    }

    func removedFilter(withDisplayData displayData: FilterScrollDisplayData) {

        guard let filters = filters else {
            view?.hideFilters()
            return
        }

        filters.remove(byFilterType: displayData.filterType)

        if filters.isEmpty() {
            view?.hideFilters()
            self.filters = nil
        }

        var location = defaultLocation

        if let userLocation = userLocation {
            location = userLocation
        }

        retrievePromotions(withLocation: location)
    }

    func removedAllFilters() {

        filters = nil
        view?.hideFilters()

        var location = defaultLocation

        if let userLocation = userLocation {
            location = userLocation
        }

        retrievePromotions(withLocation: location)
    }

    func goBackAction() {

        let shoppingFilterTransport = ShoppingFilterTransport()
        shoppingFilterTransport.shouldAllowSelectUsageType = true
        shoppingFilterTransport.filters = filters
        shoppingFilterTransport.categoriesBO = categoriesBO

        busManager.navigateScreen(tag: routerTag, ShoppingListFilteredPageReaction.EVENT_NAV_TO_SHOPPING_FILTER, shoppingFilterTransport)
    }

    func retryButtonPressed() {

        var location = defaultLocation

        if let userLocation = userLocation {
            location = userLocation
        }

        retrievePromotions(withLocation: location)
    }

    func itemSelected(withDisplayItem displayItem: DisplayItemPromotion) {

        if let promotionBO = displayItem.promotionBO {

            let promotionDetailTransport = PromotionDetailTransport()
            promotionDetailTransport.promotionBO = promotionBO
            promotionDetailTransport.userLocation = userLocation

            navigateToDetailScreen(withTransport: promotionDetailTransport)
        }

    }
}
