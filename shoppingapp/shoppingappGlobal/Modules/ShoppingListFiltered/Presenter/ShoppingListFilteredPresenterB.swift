//
//  ShoppingListFilteredPresenterB.swift
//  shoppingappMX
//
//  Created by jesus.martinez on 15/6/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class ShoppingListFilteredPresenterB<T: ShoppingListFilteredViewProtocolB>: ShoppingListFilteredPresenter<T> {

    var cardTitleIds: [String]?

    func receive(model: Model) {

        if let cards = model as? CardsBO, cardTitleIds != cards.titleIds() {
            cardTitleIds = cards.titleIds()
        }
    }

    override func shoppingParams(forLocation location: GeoLocationBO) -> ShoppingParamsTransport {

        guard let cardTitleIds = cardTitleIds else {
            return super.shoppingParams(forLocation: location)
        }

        let locationTranport = ShoppingParamLocation(geolocation: location, distanceParam: ShoppingDistanceParam(distance: defaultDistance, lengthType: lengthType))
        return ShoppingParamsTransport(location: locationTranport, titleIds: cardTitleIds, pageSize: pageSize)

    }

}

extension ShoppingListFilteredPresenterB: ShoppingListFilteredPresenterProtocolB {

}
