//
//  ShoppingListFilteredViewController.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 8/3/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents
import CoreLocation
import Lottie

class ShoppingListFilteredViewController: BaseViewController {

    // MARK: Constants
    static let ID: String = String(describing: ShoppingListFilteredViewController.self)

    static let tableViewHeaderHeight: CGFloat = 20.0

    // MARK: Vars

    var presenter: ShoppingListFilteredPresenterProtocol!

    var needShowSkeleton = false

    var displayItems: DisplayItemsPromotions?

    var moreContent = true
    var footerView: UIView!
    var lottieLoader: LOTAnimationView!

    // MARK: Outlets

    @IBOutlet weak var filtersScrollView: FiltersScrollView!
    @IBOutlet weak var filtersScrollViewHeightConstraint: NSLayoutConstraint!

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var listFilteredTableView: UITableView!

    @IBOutlet weak var errorScreenView: ErrorScreenView! {
        didSet {
            errorScreenView.delegate = self
        }
    }

    let locationManager = CLLocationManager()

    override func viewDidLoad() {

        configure(appLifecycle: (UIApplication.shared.delegate as! AppDelegate).appLifeCycle!, id: ShoppingListFilteredViewController.ID)

        super.viewDidLoad()

        if #available(iOS 11.0, *) {
            listFilteredTableView.contentInsetAdjustmentBehavior = .never
        } else {
            automaticallyAdjustsScrollViewInsets = false
        }

        configureView()
    }

    override func viewWillAppear(_ animated: Bool) {

        super.viewWillAppear(animated)

        presenter?.viewWillAppear()

        setNavigationBarDefault ()
        setNavigationBackButtonDefault()
        setNavigationTitle(withText: Localizables.promotions.key_promotions_title_text)
        setAccessibilities()
        BusManager.sessionDataInstance.disableSwipeLateralMenu()

    }

    override func viewDidAppear(_ animated: Bool) {

        super.viewDidAppear(animated)

        configureFooterLoader()
    }

    override func backAction() {

        presenter.goBackAction()
    }

    func configureView() {

        filtersScrollView.delegate = self
        configureContentView()
        errorScreenView.isHidden = true
    }

    func configureContentView() {

        contentView.isHidden = false

        listFilteredTableView.dataSource = self
        listFilteredTableView.delegate = self

        listFilteredTableView.backgroundColor = .BBVAWHITE

        listFilteredTableView.register(UINib(nibName: PromotionSmallTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: PromotionSmallTableViewCell.identifier)
        listFilteredTableView.register(UINib(nibName: SkeletonSmallPromotionCell.identifier, bundle: nil), forCellReuseIdentifier: SkeletonSmallPromotionCell.identifier)

        listFilteredTableView.rowHeight = UITableViewAutomaticDimension
        listFilteredTableView.estimatedRowHeight = PromotionSmallTableViewCell.height

        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: ShoppingListFilteredViewController.tableViewHeaderHeight))
        headerView.backgroundColor = .BBVAWHITE
        listFilteredTableView.tableHeaderView = headerView

        listFilteredTableView.separatorStyle = .none
    }

    func configureFooterLoader() {

        footerView = UIView(frame: CGRect(x: 0, y: 0, width: listFilteredTableView.frame.size.width, height: 130))
        footerView.backgroundColor = .clear

        lottieLoader = LOTAnimationView(name: ConstantsLottieAnimations.loader)
        lottieLoader.frame = CGRect(x: (UIScreen.main.bounds.width / 2) - 33.8 / 2, y: 50, width: 33.8, height: 30)
        lottieLoader.contentMode = .scaleAspectFit
        lottieLoader.loopAnimation = true

        footerView.addSubview(lottieLoader)

    }

    deinit {
        DLog(message: "Deinitialized ShoppingListFilteredViewController")
    }

    override func initModel(withModel model: Model) {
        presenter.setModel(model: model)
    }
}

extension ShoppingListFilteredViewController: ShoppingListFilteredViewProtocol {

    func showError(error: ModelBO) {
        presenter.showModal()
    }

    func changeColorEditTexts() {
    }

    func showSkeleton() {

        contentView.isHidden = false
        errorScreenView.isHidden = true
        listFilteredTableView.isScrollEnabled = false
        needShowSkeleton = true
        listFilteredTableView.reloadData()
    }

    func hideSkeleton() {

        contentView.isHidden = false
        listFilteredTableView.isScrollEnabled = true
        needShowSkeleton = false
        listFilteredTableView.reloadData()
    }

    func showPromotions(withDisplayItemsPromotions displayItemPromotions: DisplayItemsPromotions) {

        displayItems = displayItemPromotions
        contentView.isHidden = false
        errorScreenView.isHidden = true
        listFilteredTableView.isScrollEnabled = true
        listFilteredTableView.reloadData()
    }

    func updateForNoMoreContent() {

        moreContent = false
    }

    func updateForMoreContent() {

        moreContent = true
    }

    func showNextPageAnimation() {

        footerView.isHidden = false

        listFilteredTableView.tableFooterView = footerView

        lottieLoader.play()

        listFilteredTableView.reloadData()

    }

    func hideNextPageAnimation() {

        footerView.isHidden = true

        listFilteredTableView.tableFooterView = nil

        lottieLoader.pause()

        listFilteredTableView.reloadData()
    }

    func showError() {

        contentView.isHidden = true
        errorScreenView.isHidden = false

        let errorMessage = Localizables.common.key_common_error_nodata_text
        let displayData = ErrorScreenDisplayData(title: Localizables.common.key_error_ups_text, message: errorMessage, hasRetryButton: true)
        errorScreenView.configureError(displayData: displayData)
    }

    func showNoContentView() {

        contentView.isHidden = true
        errorScreenView.isHidden = false

        let errorMessage = Localizables.promotions.key_promotions_no_promotions_text + " " + Localizables.common.key_try_later_text
        let displayData = ErrorScreenDisplayData(title: Localizables.common.key_error_ups_text, message: errorMessage, hasRetryButton: false)
        errorScreenView.configureNoContent(displayData: displayData)
    }

    func showFilters(withDisplayData displayData: [FilterScrollDisplayData]) {

        filtersScrollView.loadFilters(filters: displayData)
        filtersScrollViewHeightConstraint.constant = FiltersScrollView.default_height
    }

    func hideFilters() {

        filtersScrollView.loadFilters(filters: nil)
        self.filtersScrollViewHeightConstraint.constant = 0
    }
}

extension ShoppingListFilteredViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        if let item = displayItems?.items[indexPath.row] {

            presenter.itemSelected(withDisplayItem: item)

        }

    }

}

extension ShoppingListFilteredViewController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {

        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {

        if needShowSkeleton {

            let screen = UIScreen.main.bounds
            let screenHeight = screen.size.height
            let numberCells = (screenHeight / SkeletonSmallPromotionCell.height)

            return Int(numberCells)
        }

        return displayItems?.items?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if needShowSkeleton {
            if let cell = tableView.dequeueReusableCell(withIdentifier: SkeletonSmallPromotionCell.identifier, for: indexPath) as? SkeletonSmallPromotionCell {
                return cell
            }
        } else {

            if let displayItem = displayItems?.items[indexPath.row] {

                if let cell = tableView.dequeueReusableCell(withIdentifier: PromotionSmallTableViewCell.identifier, for: indexPath) as? PromotionSmallTableViewCell {

                    cell.configureCellWithDisplayPromotionData(withDisplayitemPromotion: displayItem)

                    if indexPath.row == 0 {
                        cell.mAddSeparator.isHidden = true
                    } else {
                        cell.mAddSeparator.isHidden = false
                    }

                    return cell
                }
            }

        }

        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        var height: CGFloat = PromotionSmallTableViewCell.height

        if  needShowSkeleton {
            height = SkeletonSmallPromotionCell.height
        }
        return height
    }
}

extension ShoppingListFilteredViewController: UIScrollViewDelegate {

    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {

        let offset = scrollView.contentOffset
        let bounds = scrollView.bounds
        let size = scrollView.contentSize
        let inset = scrollView.contentInset
        let y = offset.y + bounds.size.height - inset.bottom
        let h = size.height

        let reload_distance: CGFloat = -300.0
        if y > h + reload_distance && moreContent {
            presenter.loadDataNextPage()
        }
    }
}

extension ShoppingListFilteredViewController: FiltersScrollViewDelegate {

    func removeFilterPushed(filter: FilterScrollDisplayData, inFiltersScrollView filtersScrollView: FiltersScrollView) {

        presenter.removedFilter(withDisplayData: filter)
    }

    func removeFilterLonglyPushed(inFiltersScrollView filtersScrollView: FiltersScrollView) {

    }

    func removeAllFiltersPushed(inFiltersScrollView filtersScrollView: FiltersScrollView) {

        presenter.removedAllFilters()
    }
}

extension ShoppingListFilteredViewController: CloseModalDelegate {

    func errorViewControllerAcceptPressed(_ errorViewController: ErrorViewController) {
        presenter.checkError()
    }

    func errorViewControllerCancelPressed(_ errorViewController: ErrorViewController) {
    }

}

// MARK: - ErrorScreenViewDelegate

extension ShoppingListFilteredViewController: ErrorScreenViewDelegate {

    func errorScreenViewRetryButtonPressed(_ errorScreenView: ErrorScreenView) {

        presenter.retryButtonPressed()
    }
}

// MARK: - APPIUM

private extension ShoppingListFilteredViewController {

    func setAccessibilities() {

        #if APPIUM

        #endif

    }

}
