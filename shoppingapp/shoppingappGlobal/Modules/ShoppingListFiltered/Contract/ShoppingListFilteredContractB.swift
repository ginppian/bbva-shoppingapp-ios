//
//  ShoppingListFilteredContract.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 8/3/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import RxSwift

protocol ShoppingListFilteredViewProtocolB: ShoppingListFilteredViewProtocol {

}

protocol ShoppingListFilteredPresenterProtocolB: ShoppingListFilteredPresenterProtocol {

}
