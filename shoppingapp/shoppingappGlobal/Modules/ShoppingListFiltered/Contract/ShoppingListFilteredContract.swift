//
//  ShoppingListFilteredContract.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 8/3/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import RxSwift

protocol ShoppingListFilteredViewProtocol: ViewProtocol {

    func showSkeleton()
    func hideSkeleton()

    func showPromotions(withDisplayItemsPromotions displayItemPromotions: DisplayItemsPromotions)

    func updateForNoMoreContent()
    func updateForMoreContent()
    func showNextPageAnimation()
    func hideNextPageAnimation()

    func showError()
    func showNoContentView()

    func showFilters(withDisplayData displayData: [FilterScrollDisplayData])
    func hideFilters()
}

protocol ShoppingListFilteredPresenterProtocol: PresenterProtocol {

    func viewWillAppear()
    func loadDataNextPage()

    func removedFilter(withDisplayData displayData: FilterScrollDisplayData)
    func removedAllFilters()

    func goBackAction()
    func retryButtonPressed()

    func itemSelected(withDisplayItem displayItem: DisplayItemPromotion)
}

protocol ShoppingListFilteredInteractorProtocol {

    func providePromotions(byParams params: ShoppingParamsTransport, withFilters filters: ShoppingFilter?) -> Observable<ModelBO>
    func providePromotionsWithFilters(withNextPage nextPage: String) -> Observable <ModelBO>
}
