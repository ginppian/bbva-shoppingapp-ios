//
//  ShoppingListFilteredPageReaction.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 8/3/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

class ShoppingListFilteredPageReaction: PageReaction {

    static let ROUTER_TAG: String = "shopping-list-filtered-tag"

    static let EVENT_NAV_TO_SHOPPING_FILTER: ActionSpec<Model> = ActionSpec<Model>(id: "EVENT_NAV_TO_SHOPPING_FILTER")

    static let EVENT_NAV_TO_DETAIL_SCREEN: ActionSpec<Model> = ActionSpec<Model>(id: "EVENT_NAV_TO_DETAIL_SCREEN")

    override func configure() {

        navigateShoppingFilterFromShoppingListFiltered()

        navigateToDetailScreen()
    }

    func navigateShoppingFilterFromShoppingListFiltered() {

        _ = when(id: ShoppingListFilteredPageReaction.ROUTER_TAG, type: Model.self)
            .doAction(actionSpec: ShoppingListFilteredPageReaction.EVENT_NAV_TO_SHOPPING_FILTER)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, model in

                if let model = model {
                    router.navigateShoppingFilterFromShoppingListFilteredPop(withModel: model)
                }

            }
    }

    func navigateToDetailScreen() {

        _ = when(id: ShoppingListFilteredPageReaction.ROUTER_TAG, type: Model.self)
            .doAction(actionSpec: ShoppingListFilteredPageReaction.EVENT_NAV_TO_DETAIL_SCREEN)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, model in

                if let model = model {
                    router.navigateToShoppingDetail(withModel: model)
                }

            }
    }
}
