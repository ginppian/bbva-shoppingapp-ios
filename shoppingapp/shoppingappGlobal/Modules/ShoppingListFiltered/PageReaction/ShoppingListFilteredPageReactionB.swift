//
//  ShoppingListFilteredPageReactionB.swift
//  shoppingappMX
//
//  Created by jesus.martinez on 15/6/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class ShoppingListFilteredPageReactionB: ShoppingListFilteredPageReaction {

    override func configure() {
        super.configure()

        reactCards()
    }

    func reactCards() {

        _ = reactTo(channel: PageReactionConstants.CHANNEL_CARDS)?
            .then()
            .inside(componentID: ShoppingListFilteredPageReaction.ROUTER_TAG, component: ShoppingListFilteredPresenterB<ShoppingListFilteredViewControllerB>.self)
            .doReaction { component, param in

                if let param = param {
                    component.receive(model: param)
                }
            }
    }
}
