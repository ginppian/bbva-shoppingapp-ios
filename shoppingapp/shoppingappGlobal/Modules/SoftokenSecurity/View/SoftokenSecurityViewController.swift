//
//  SoftokenSecurityViewController.swift
//  shoppingapp
//
//  Created by ignacio.bonafonte on 14/02/2019.
//  Copyright © 2019 BBVA. All rights reserved.
//

import CellsNativeCore
import CellsNativeComponents
import UIKit

class SoftokenSecurityViewController: BaseViewController {

    static let ID: String = String(describing: SoftokenSecurityViewController.self)

    var presenter: SoftokenSecurityPresenterProtocol!

    @IBOutlet weak var imformationLabel: UILabel!
    @IBOutlet weak var confirmationLabel: UILabel!
    @IBOutlet weak var acceptButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!

    override func viewDidLoad() {

        self.configure(appLifecycle: (UIApplication.shared.delegate as! AppDelegate).appLifeCycle!, id: SoftokenSecurityViewController.ID)

        super.viewDidLoad()

        configureView()
    }

    override func viewWillAppear(_ animated: Bool) {

        super.viewWillAppear(animated)

        configureNavigationBar()

        BusManager.sessionDataInstance.disableSwipeLateralMenu()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

    deinit {
        ComponentManager.remove(id: presenter.routerTag)
        DLog(message: "Deinitialized SecurityViewController")
    }

    override func backAction() {

        navigationController?.navigationBar.isTranslucent = true
        
        closeView()
    }
    
    @IBAction func acceptButtonPressed(_ sender: UIButton) {
        presenter.acceptButtonPressed()
    }
    
    @IBAction func cancelButtonPressed(_ sender: UIButton) {
        
        closeView()
    }
}

private extension SoftokenSecurityViewController {
    
    func configureNavigationBar() {
        
        let image = SVGCache.image(forSVGNamed: ConstantsImages.Common.close_icon, color: .BBVAWHITE)
        
        setNavigationLeftButton(withImage: image, action: #selector(backAction))
        
        setNavigationTitle(withText: Localizables.common.key_security_title_text)
        
        setNavigationBarTransparent()
    }
    
    func configureView() {
        
        imformationLabel.textColor = .BBVA600
        imformationLabel.font = Tools.setFontBook(size: 15)
        imformationLabel.textAlignment = .center
        
        confirmationLabel.textColor = .BBVA600
        confirmationLabel.font = Tools.setFontBook(size: 15)
        confirmationLabel.textAlignment = .center
        
        acceptButton.setBackgroundColor(color: .MEDIUMBLUE, forUIControlState: .normal)
        acceptButton.setBackgroundColor(color: .COREBLUE, forUIControlState: .highlighted)
        acceptButton.setBackgroundColor(color: .BBVA200, forUIControlState: .disabled)
        acceptButton.setTitleColor(.BBVAWHITE, for: .normal)
        acceptButton.titleLabel?.font = Tools.setFontMedium(size: 15)
        
        cancelButton.setTitleColor(.MEDIUMBLUE, for: .normal)
        cancelButton.setTitleColor(.COREBLUE, for: .highlighted)
        cancelButton.setTitleColor(.BBVA200, for: .disabled)
        cancelButton.titleLabel?.font = Tools.setFontMedium(size: 15)
    }
    
    func closeView() {
        
        BusManager.sessionDataInstance.securityInterceptorStore?.unLockStop()
        dismissAction()
    }
}

extension SoftokenSecurityViewController: ViewProtocol {

    func showError(error: ModelBO) {

    }

    func changeColorEditTexts() {

    }

}

extension SoftokenSecurityViewController: SoftokenSecurityViewProtocol {
}
