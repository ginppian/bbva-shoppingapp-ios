//
//  SoftokenSecurityPresenter.swift
//  shoppingappMX
//
//  Created by ignacio.bonafonte on 14/02/2019.
//  Copyright © 2019 BBVA. All rights reserved.
//

import Foundation

class SoftokenSecurityPresenter<T: SoftokenSecurityViewProtocol>: BasePresenter<T> {

    let AUTHENTICATION_TYPE = "authenticationtype"
    let AUTHENTICATION_DATA = "authenticationdata"
    let softokenAuthenticationType = "33"
    let softokenAuthenticationData = "otp-token"

    lazy var publicConfiguration = PublicConfigurationManager.sharedInstance().localPublicConfiguration()
    
    lazy var otpToken = Settings.Security.softToken

    required init() {
        super.init(tag: SoftokenSecurityPageReaction.ROUTER_TAG)
    }

    override init(busManager: BusManager) {
        super.init(busManager: busManager)
        routerTag = SoftokenSecurityPageReaction.ROUTER_TAG
    }
}

extension SoftokenSecurityPresenter: SoftokenSecurityPresenterProtocol {
    
    func acceptButtonPressed() {

        showLoading()
        
        busManager.dismissViewController(animated: true) { [weak self] in

            guard let `self` = self else {
                return
            }
            
            if let otpToken = self.otpToken, otpToken.isNotEmpty {
                
                var headers = [String: String]()
                headers[self.AUTHENTICATION_TYPE] = self.softokenAuthenticationType
                headers[self.AUTHENTICATION_DATA] = "\(self.softokenAuthenticationData)=\(otpToken)"
                self.busManager.securityInterceptorStore?.unLock(headers: headers)
                
            } else {
                
                self.busManager.securityInterceptorStore?.unLockStop()
            }
        }
    }
}
