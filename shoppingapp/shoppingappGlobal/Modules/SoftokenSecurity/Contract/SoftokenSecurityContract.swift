//
//  SoftokenSecurityContract.swift
//  shoppingappMX
//
//  Created by ignacio.bonafonte on 14/02/2019.
//  Copyright © 2019 BBVA. All rights reserved.
//

import Foundation

protocol SoftokenSecurityViewProtocol: ViewProtocol {

}

protocol SoftokenSecurityPresenterProtocol: PresenterProtocol {

    func acceptButtonPressed()
}
