//
//  SelectFavoriteCategoryCollectionReusableView.swift
//  shoppingappMX
//
//  Created by jesus.martinez on 4/7/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit

class SelectFavoriteCategoryCollectionReusableView: UICollectionReusableView {

    static let identifier = "SelectFavoriteCategoryCollectionReusableView"
    static let height: CGFloat = 156.0

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()

        backgroundColor = .BBVAWHITE

        titleLabel.font = Tools.setFontBook(size: 22)
        titleLabel.textColor = .BBVA500
        titleLabel.numberOfLines = 0

        descriptionLabel.font = Tools.setFontBook(size: 14)
        descriptionLabel.textColor = .BBVA600
        descriptionLabel.numberOfLines = 0
    }

    func configure() {

        titleLabel.text = Localizables.promotions.key_promotions_configure_favourites_subtitle

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 3
        paragraphStyle.alignment = .center

        let text = Localizables.promotions.key_promotions_configure_favourites_description
        let attributedString = NSMutableAttributedString(string: text)
        attributedString.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: text.count))
        attributedString.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 14), range: NSRange(location: 0, length: text.count))

        descriptionLabel.textAlignment = .center
        descriptionLabel.attributedText = attributedString
    }
}
