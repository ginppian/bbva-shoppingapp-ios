//
//  SelectFavoriteCategoryCollectionViewCell.swift
//  shoppingappMX
//
//  Created by AZIZEBULBUL on 26/06/2018.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class SelectFavoriteCategoryCollectionViewCell: UICollectionViewCell {

    static let identifier = "SelectFavoriteCategoryCollectionViewCell"

    // MARK: outlets
    @IBOutlet weak var categoryTitleLabel: UILabel!
    @IBOutlet weak var categoryImageView: UIImageView!
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var selectedMaskView: UIView!
    @IBOutlet weak var plusImageView: UIImageView!

    // MARK: Vars
    var displayData: FavoriteCategoryDisplayData?

    // MARK: life cycle
    override func awakeFromNib() {

        super.awakeFromNib()

        categoryTitleLabel.font = Tools.setFontBook(size: 14)
        categoryTitleLabel.textColor = .MEDIUMBLUE
        categoryTitleLabel.numberOfLines = 1
        shadowView.backgroundColor = .BBVA600
        shadowView.alpha = 0.2
        self.addShadow(color: .SHADOWGREY)
    }

    func configure(withDisplayData displayData: FavoriteCategoryDisplayData) {

        self.displayData = displayData
        categoryTitleLabel.text = displayData.name
        markCellAs(selected: isSelected, animated: false)

        if !Tools.isStringNilOrEmpty(withString: displayData.urlImage) {

            configureImage(withUrlImage: displayData.urlImage!)
        } else {

            categoryImageView.image = UIImage(named: ConstantsImages.Promotions.default_promotion_list_image)
        }
    }

    private func configureImage(withUrlImage urlImage: String) {

        categoryImageView.image = nil
        categoryImageView.backgroundColor = .BBVA200

        let url = URL(string: urlImage.addingPercentEncoding(withAllowedCharacters: NSCharacterSet.urlQueryAllowed)!)
        let placeholder = UIImage(named: ConstantsImages.Promotions.default_promotion_image)

        categoryImageView.setImage(fromUrl: url, placeholderImage: placeholder, completion: { success in
            if success {
                DispatchQueue.main.async {
                    self.categoryImageView.backgroundColor = UIColor.white
                }
            }
        })
    }

    func markCellAs(selected: Bool, animated: Bool) {

        if isSelected {

            if animated {

                UIView.animate(withDuration: 0.3, animations: {

                    self.isUserInteractionEnabled = false
                    self.markCellAsSelected()
                }, completion: { (_: Bool) -> Void in

                    self.isUserInteractionEnabled = true
                })
            } else {

                self.markCellAsSelected()
            }
        } else {

            if animated {

                UIView.animate(withDuration: 0.3, animations: {

                    self.isUserInteractionEnabled = false
                    self.markCellAsUnselected()
                }, completion: { (_: Bool) -> Void in

                    self.isUserInteractionEnabled = true
                })
            } else {

                self.markCellAsUnselected()
            }
        }
    }

    private func markCellAsSelected() {

        self.selectedMaskView.backgroundColor = .AQUA
        self.plusImageView.image = SVGCache.image(forSVGNamed: ConstantsImages.Common.checkmark_success, color: .BBVAWHITE)
    }

    private func markCellAsUnselected() {

        self.selectedMaskView.backgroundColor = .clear
        self.plusImageView.image = SVGCache.image(forSVGNamed: ConstantsImages.Common.expand_icon, color: .BBVAWHITE)
    }

}
