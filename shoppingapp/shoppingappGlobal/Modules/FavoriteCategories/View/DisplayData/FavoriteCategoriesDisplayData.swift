//
//  FavoriteCategoriesDisplayData.swift
//  shoppingappMX
//
//  Created by jesus.martinez on 4/7/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

struct FavoriteCategoriesDisplayData {

    let numberOfFavoriteCategories: Int
    let items: [FavoriteCategoryDisplayData]

    init(numberOfFavoriteCategories: Int, categories: CategoriesBO) {
        self.numberOfFavoriteCategories = numberOfFavoriteCategories

        var items = [FavoriteCategoryDisplayData]()

        for category in categories.categories {
            items.append(FavoriteCategoryDisplayData(name: category.name, isSelected: category.isFavourite, urlImage: category.image?.url))
        }

        self.items = items
    }
}

class FavoriteCategoryDisplayData {

    let name: String
    var isSelected = false
    let urlImage: String?

    init(name: String, isSelected: Bool, urlImage: String? = nil) {

        self.name = name
        self.isSelected = isSelected
        self.urlImage = urlImage
    }
}
