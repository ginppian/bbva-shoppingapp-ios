//
//  SelectFavoriteCategoryViewController.swift
//  shoppingappMX
//
//  Created by AZIZEBULBUL on 26/06/2018.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class SelectFavoriteCategoryViewController: BaseViewController {

    static let ID: String = String(describing: SelectFavoriteCategoryViewController.self)
    static let spaceBetweenCells: CGFloat = 10
    static let numberOfCellsForRow: CGFloat = 2
    static let cellsProportions: CGFloat = 0.85

    // MARK: outlets
    @IBOutlet weak var confirmFavoriteCategoriesButton: UIButton!
    @IBOutlet weak var laterButton: UIButton!
    @IBOutlet weak var favoriteCategoriesCollectionView: UICollectionView!
    @IBOutlet weak var bottomContainer: UIView!

    // MARK: Vars
    var displayData: FavoriteCategoriesDisplayData?
    var presenter: SelectFavoriteCategoryPresenterProtocol!
    var categoriesStore: CategoriesStore?
    var numberOfSelectedCells = 0

    // MARK: life cycle
    override func viewDidLoad() {

        self.configure(appLifecycle: (UIApplication.shared.delegate as! AppDelegate).appLifeCycle!, id: SelectFavoriteCategoryViewController.ID)

        if let networkWorker = BusManager.sessionDataInstance.networkWorker {
            categoriesStore = CategoriesStore(worker: networkWorker, host: ServerHostURL.getPromotionsURL())
        }

        super.viewDidLoad()

        configureView()
        configureFavoriteCategoriesCollectionView()
        presenter.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {

        super.viewWillAppear(animated)

        configureNavigationBar()
    }

    func configureNavigationBar() {

        setNavigationBarDefault()

        let image = SVGCache.image(forSVGNamed: ConstantsImages.Common.close_icon, color: .BBVAWHITE)
        setNavigationLeftButton(withImage: image, action: #selector(dismissViewController))

        setNavigationTitle(withText: Localizables.promotions.key_promotions_configure_favourites_title)
    }

    func configureView() {

        confirmFavoriteCategoriesButton.setBackgroundColor(color: .BBVA200, forUIControlState: .disabled)
        confirmFavoriteCategoriesButton.setBackgroundColor(color: .MEDIUMBLUE, forUIControlState: .normal)
        confirmFavoriteCategoriesButton.setBackgroundColor(color: .DARKMEDIUMBLUE, forUIControlState: .highlighted)
        confirmFavoriteCategoriesButton.titleLabel?.font = Tools.setFontBold(size: 14)
        confirmFavoriteCategoriesButton.setTitleColor(.BBVAWHITE, for: .normal)
        confirmFavoriteCategoriesButton.setTitle(Localizables.common.key_confirm_text, for: .normal)
        confirmFavoriteCategoriesButton.isEnabled = false
        laterButton.titleLabel?.font = Tools.setFontBold(size: 14)
        laterButton.setBackgroundColor(color: .clear, forUIControlState: .normal)
        laterButton.setTitleColor(.MEDIUMBLUE, for: .normal)
        laterButton.setTitle(Localizables.promotions.key_promotions_favourites_set_up_later, for: .normal)
        
        bottomContainer.addShadow(location: .top, color: .BBVA500, opacity: 0.2, radius: 1)
    }

    func configureFavoriteCategoriesCollectionView() {

        favoriteCategoriesCollectionView.dataSource = self
        favoriteCategoriesCollectionView.delegate = self
        favoriteCategoriesCollectionView.backgroundColor = .clear
        favoriteCategoriesCollectionView.allowsSelection = true
        favoriteCategoriesCollectionView.allowsMultipleSelection = true

        favoriteCategoriesCollectionView.register(UINib(nibName: SelectFavoriteCategoryCollectionViewCell.identifier, bundle: nil), forCellWithReuseIdentifier: SelectFavoriteCategoryCollectionViewCell.identifier)
        favoriteCategoriesCollectionView.register(UINib(nibName: SelectFavoriteCategoryCollectionReusableView.identifier, bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: SelectFavoriteCategoryCollectionReusableView.identifier)

    }

    private func checkBottomContainerShadow() {

        let offset = favoriteCategoriesCollectionView.contentOffset
        let bounds = favoriteCategoriesCollectionView.bounds
        let size = favoriteCategoriesCollectionView.contentSize
        let inset = favoriteCategoriesCollectionView.contentInset
        let y = offset.y + bounds.size.height - inset.bottom
        let h = size.height

        if y >= (h - SelectFavoriteCategoryViewController.spaceBetweenCells) {
            bottomContainer.layer.shadowOpacity = 0
        } else {
            bottomContainer.layer.shadowOpacity = 0.2
        }
    }

    override func initModel(withModel model: Model) {

        presenter.setModel(model: model)
    }

    override func viewWillLayoutSubviews() {

        super.viewWillLayoutSubviews()

        checkBottomContainerShadow()
    }

    @IBAction func addFavoriteCategory(_ sender: Any) {

        presenter.confirmFavoriteCategoriesPressed()
    }
}

extension SelectFavoriteCategoryViewController: SelectFavoriteCategoryViewProtocol {

    func enableSelectFavoriteCategories() {

        confirmFavoriteCategoriesButton.isEnabled = true
    }

    func disableSelectFavoriteCategories() {

        confirmFavoriteCategoriesButton.isEnabled = false
    }

    func showFavoriteCategories(withDisplayData displayData: FavoriteCategoriesDisplayData) {

        self.displayData = displayData
        favoriteCategoriesCollectionView.reloadData()

        for i in 0..<displayData.items.count {

            if displayData.items[i].isSelected {
                favoriteCategoriesCollectionView.selectItem(at: IndexPath(row: i, section: 0), animated: false, scrollPosition: .top)
            } else {
                favoriteCategoriesCollectionView.deselectItem(at: IndexPath(row: i, section: 0), animated: false)
            }
        }
    }

    func showError(error: ModelBO) {

        presenter.showModal()
    }
    
    @IBAction func dismissViewController() {
        
        presenter.dismissViewController()
    }
    
    func showToastAboutSelectFavoriteCategoriesLater() {
        
        ToastManager.shared().showToast(withText: Localizables.promotions.key_promotions_favourites_in_other_moment, backgroundColor: .DARKMEDIUMBLUE, height: defaultHeightToastView)
    }
    
    func showToastAboutFavoriteCategoriesLimit() {
        
        ToastManager.shared().showToast(withText: Localizables.promotions.key_promotions_favourites_max_selected, backgroundColor: .DARKMEDIUMBLUE, height: defaultHeightToastView)
    }

    func changeColorEditTexts() {

    }

    func selectFavoriteCategories(withDTO dto: FavouriteCategoriesDTO) {

        categoriesStore?.selectFavouriteCategories(dto)
    }
}

// MARK: - UICollectionView DataSource

extension SelectFavoriteCategoryViewController: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return displayData?.items.count ?? 0
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        if let displayItem = displayData?.items[indexPath.row] {

            if let cell = collectionView.dequeueReusableCell(withReuseIdentifier: SelectFavoriteCategoryCollectionViewCell.identifier, for: indexPath) as? SelectFavoriteCategoryCollectionViewCell {

                cell.configure(withDisplayData: displayItem)

                return cell
            }
        }

        return UICollectionViewCell()
    }

    func collectionView(_ collectionView: UICollectionView,
                        viewForSupplementaryElementOfKind kind: String,
                        at indexPath: IndexPath) -> UICollectionReusableView {

        guard kind == UICollectionElementKindSectionHeader, let view = collectionView.dequeueReusableSupplementaryView(ofKind: kind,
                                                                                                                       withReuseIdentifier: SelectFavoriteCategoryCollectionReusableView.identifier,
                                                                                                                       for: indexPath) as? SelectFavoriteCategoryCollectionReusableView else {
            return UICollectionReusableView()
        }

        view.configure()

        return view
    }
}

extension SelectFavoriteCategoryViewController: UIScrollViewDelegate {

    func scrollViewDidScroll(_ scrollView: UIScrollView) {

        checkBottomContainerShadow()
    }
}

// MARK: - UICollectionView Delegate
extension SelectFavoriteCategoryViewController: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {

        if collectionView.indexPathsForSelectedItems?.count ?? 0 >= displayData?.numberOfFavoriteCategories ?? 0 {
            
            presenter.triedToSelectMoreFavoriteCategoriesThanLimit()
            return false
        }
        return true
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        presenter.selectedCategory(atIndex: indexPath.row)

        let cell = collectionView.cellForItem(at: indexPath) as? SelectFavoriteCategoryCollectionViewCell
        cell?.markCellAs(selected: true, animated: true)
    }

    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {

        presenter.deselectedCategory(atIndex: indexPath.row)

        let cell = collectionView.cellForItem(at: indexPath) as? SelectFavoriteCategoryCollectionViewCell
        cell?.markCellAs(selected: false, animated: true)
    }
}

extension SelectFavoriteCategoryViewController: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let width = (view.frame.size.width - (3 * SelectFavoriteCategoryViewController.spaceBetweenCells)) / SelectFavoriteCategoryViewController.numberOfCellsForRow
        let height = width * SelectFavoriteCategoryViewController.cellsProportions

        return CGSize(width: width, height: height)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {

        return UIEdgeInsets(top: 0, left: SelectFavoriteCategoryViewController.spaceBetweenCells, bottom: 10, right: SelectFavoriteCategoryViewController.spaceBetweenCells)
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {

        return SelectFavoriteCategoryViewController.spaceBetweenCells
    }

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {

        return SelectFavoriteCategoryViewController.spaceBetweenCells
    }

    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        referenceSizeForHeaderInSection section: Int) -> CGSize {

        return CGSize(width: view.frame.size.width, height: SelectFavoriteCategoryCollectionReusableView.height)
    }
}

// MARK: - Categories Store result
extension SelectFavoriteCategoryViewController {

    func successSelectFavoriteCategories(model: CellsDTO) {

        presenter.confirmFavoriteCategoriesFinished()
    }

    func errorSelectFavoriteCategories(error: ErrorBO) {

        presenter.confirmFavoriteCategoriesFinished(withError: error)
    }
}

// MARK: - CloseModalDelegate
extension SelectFavoriteCategoryViewController: CloseModalDelegate {

    func errorViewControllerAcceptPressed(_ errorViewController: ErrorViewController) {
        presenter?.checkError()
    }

    func errorViewControllerCancelPressed(_ errorViewController: ErrorViewController) {
    }
}
