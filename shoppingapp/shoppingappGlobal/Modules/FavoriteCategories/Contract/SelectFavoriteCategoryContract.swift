//
//  SelectFavoriteCategoryContract.swift
//  shoppingappMX
//
//  Created by AZIZEBULBUL on 26/06/2018.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol SelectFavoriteCategoryViewProtocol: ViewProtocol {

    func showFavoriteCategories(withDisplayData displayData: FavoriteCategoriesDisplayData)
    func enableSelectFavoriteCategories()
    func disableSelectFavoriteCategories()
    func selectFavoriteCategories(withDTO dto: FavouriteCategoriesDTO)
    func showToastAboutSelectFavoriteCategoriesLater()
    func showToastAboutFavoriteCategoriesLimit()
}

protocol SelectFavoriteCategoryPresenterProtocol: PresenterProtocol {

    func viewDidLoad()
    func selectedCategory(atIndex index: Int)
    func deselectedCategory(atIndex index: Int)
    func triedToSelectMoreFavoriteCategoriesThanLimit()
    func confirmFavoriteCategoriesPressed()
    func confirmFavoriteCategoriesFinished()
    func confirmFavoriteCategoriesFinished(withError error: ErrorBO)
    func dismissViewController()
}
