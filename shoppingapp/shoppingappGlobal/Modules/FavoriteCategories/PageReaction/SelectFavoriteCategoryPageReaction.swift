//
//  SelectFavoriteCategoryPageReaction.swift
//  shoppingappMX
//
//  Created by AZIZEBULBUL on 26/06/2018.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

class SelectFavoriteCategoryPageReaction: PageReaction {

   static let ROUTER_TAG: String = "select-favorite-category-tag"

   static let INFO_CONFIRMATION_CHANNEL: Channel<Void> = Channel(name: "bcs_info_categories_confirmation_channel")
   static let INFO_CATEGORIES_NOT_CHANGE_CHANNEL: Channel<Void> = Channel(name: "bcs_info_categories_not_change_channel")

   static let SELECTION_FAVORITE_CATEGORIES_SUCCESS: ActionSpec<Void> = ActionSpec<Void>(id: "SELECTION_FAVORITE_CATEGORIES_SUCCESS")
   static let SELECTION_FAVORITE_CATEGORIES_DO_NOT_CHANGE: ActionSpec<Void> = ActionSpec<Void>(id: "SELECTION_FAVORITE_CATEGORIES_DO_NOT_CHANGE")

    override func configure() {

        successSelectFavoriteCategories()
        errorSelectFavoriteCategories()
        selectionOfFavoriteCategoriesFinishedSuccessful()
        selectionOfFavoriteCategoriesDoNotChange()
    }

    func successSelectFavoriteCategories() {

        _ = when(id: CategoriesStore.ID, type: FavouriteCategoriesDTO.self)
            .doAction(actionSpec: CategoriesStore.ON_SELECT_FAVOURITE_CATEGORIES_SUCCESS)
            .then()
            .inside(componentID: SelectFavoriteCategoryViewController.ID, component: SelectFavoriteCategoryViewController.self)
            .doReaction { controller, param in
                if let param = param {
                    controller.successSelectFavoriteCategories(model: param)
                }
            }
    }

    func errorSelectFavoriteCategories() {

        _ = when(id: CategoriesStore.ID, type: ErrorBO.self)
            .doAction(actionSpec: CategoriesStore.ON_SELECT_FAVOURITE_CATEGORIES_ERROR)
            .then()
            .inside(componentID: SelectFavoriteCategoryViewController.ID, component: SelectFavoriteCategoryViewController.self)
            .doReaction { controller, param in
                if let param = param {
                    controller.errorSelectFavoriteCategories(error: param)
                }
            }
    }

    func selectionOfFavoriteCategoriesFinishedSuccessful() {

        _ = writeOn(channel: SelectFavoriteCategoryPageReaction.INFO_CONFIRMATION_CHANNEL)
            .when(componentID: SelectFavoriteCategoryPageReaction.ROUTER_TAG)
            .doAction(actionSpec: SelectFavoriteCategoryPageReaction.SELECTION_FAVORITE_CATEGORIES_SUCCESS)

    }

    func selectionOfFavoriteCategoriesDoNotChange() {

        _ = writeOn(channel: SelectFavoriteCategoryPageReaction.INFO_CATEGORIES_NOT_CHANGE_CHANNEL)
            .when(componentID: SelectFavoriteCategoryPageReaction.ROUTER_TAG)
            .doAction(actionSpec: SelectFavoriteCategoryPageReaction.SELECTION_FAVORITE_CATEGORIES_DO_NOT_CHANGE)

    }
}
