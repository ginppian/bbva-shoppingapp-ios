//
//  SelectFavoriteCategoryPresenter.swift
//  shoppingappMX
//
//  Created by AZIZEBULBUL on 26/06/2018.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class SelectFavoriteCategoryPresenter<T: SelectFavoriteCategoryViewProtocol>: BasePresenter<T> {

    var categories: CategoriesBO?
    var selectedCategories = [CategoryType]()
    var numberOfFavoriteCategories = Settings.FavoriteCategory.number_of_favorite_categories

    override init(busManager: BusManager) {

        super.init(busManager: busManager)
        routerTag = SelectFavoriteCategoryPageReaction.ROUTER_TAG
    }

    required init() {

        super.init(tag: SelectFavoriteCategoryPageReaction.ROUTER_TAG)
    }

    override func setModel(model: Model) {

        if let categoriesModelTransport = model as? FavoriteCategoriesTransport {
            categories = categoriesModelTransport.categories
            categories?.sortAlphabeticallyByName()
            selectedCategories = categoriesModelTransport.categories.favoriteCategoryTypes()
        }
    }
}

extension SelectFavoriteCategoryPresenter: SelectFavoriteCategoryPresenterProtocol {

    func viewDidLoad() {

        guard let categories = categories else {
            return
        }

        let displayData = FavoriteCategoriesDisplayData(numberOfFavoriteCategories: numberOfFavoriteCategories, categories: categories)

        view?.showFavoriteCategories(withDisplayData: displayData)

        if selectedCategories.count == numberOfFavoriteCategories {
            view?.enableSelectFavoriteCategories()
        }
    }

    func selectedCategory(atIndex index: Int) {

        if let categoryTypeSelected = categories?.categories[safeElement: index]?.id, !selectedCategories.contains(categoryTypeSelected) {
            selectedCategories.append(categoryTypeSelected)

            if selectedCategories.count == numberOfFavoriteCategories {
                view?.enableSelectFavoriteCategories()
            }
        }
    }

    func deselectedCategory(atIndex index: Int) {

        if let categoryTypeSelected = categories?.categories[safeElement: index]?.id, let index = selectedCategories.index(of: categoryTypeSelected) {
            selectedCategories.remove(at: index)

            if selectedCategories.count < numberOfFavoriteCategories {
                view?.disableSelectFavoriteCategories()
            }
        }
    }
    
    func triedToSelectMoreFavoriteCategoriesThanLimit() {
        
        view?.showToastAboutFavoriteCategoriesLimit()
    }

    func confirmFavoriteCategoriesPressed() {

        guard let categories = categories else {
            return
        }
        
        busManager.publishData(tag: EventTracker.id, PageReactionConstants.NEW_TRACK_EVENT, Event(type: .favoriteCategories))
        
        PreferencesManager.sharedInstance().saveValue(forValue: true as AnyObject, withKey: PreferencesManagerKeys.kDontSuggestFavoriteSelection.rawValue)
        if Set(categories.favoriteCategoryTypes()) == Set(selectedCategories) {

            busManager.publishData(tag: SelectFavoriteCategoryPageReaction.ROUTER_TAG, SelectFavoriteCategoryPageReaction.SELECTION_FAVORITE_CATEGORIES_DO_NOT_CHANGE, Void())

            busManager.dismissViewController(animated: true, completion: nil)
        } else {

            showLoading()

            var favoriteCategories = [FavouriteCategoryDTO]()

            for selectedCategory in selectedCategories {
                favoriteCategories.append(FavouriteCategoryDTO(selectedCategory.rawValue, isFavourite: true))
            }

            view?.selectFavoriteCategories(withDTO: FavouriteCategoriesDTO(favouriteCategories: favoriteCategories))
        }
    }

    func confirmFavoriteCategoriesFinished() {

        busManager.publishData(tag: SelectFavoriteCategoryPageReaction.ROUTER_TAG, SelectFavoriteCategoryPageReaction.SELECTION_FAVORITE_CATEGORIES_SUCCESS, Void())

        hideLoading()

        busManager.dismissViewController(animated: true, completion: nil)
    }

    func confirmFavoriteCategoriesFinished(withError error: ErrorBO) {

        errorBO = error

        hideLoading(completion: { [weak self] in
            self?.checkError()
        })
    }
    
    func dismissViewController() {
        
        BusManager.sessionDataInstance.dismissViewController(animated: true)
        
        guard !PreferencesManager.sharedInstance().getDontSuggestFavoriteSelection() else {
            return
        }
        
        let accessCounter = PreferencesManager.sharedInstance().getPromotionsSectionAccessCounter()
        let accessCounterToWait = PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kWaitToAccessToSuggestFavoriteSelection) as? NSNumber
        
        view?.showToastAboutSelectFavoriteCategoriesLater()

        if accessCounterToWait?.intValue == accessCounter {
            
            PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: true as AnyObject, withKey: PreferencesManagerKeys.kDontSuggestFavoriteSelection)
        }
    }
}
