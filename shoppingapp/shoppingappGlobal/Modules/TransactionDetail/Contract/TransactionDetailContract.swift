//
//  TransactionDetailContract.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 15/11/2017.
//  Copyright © 2017 BBVA. All rights reserved.
//

import RxSwift

protocol TransactionDetailViewProtocol: ViewProtocol {

    func showTransactionDetail(withTransactionDisplayItems displayItems: DisplayTransactionDetail)
    func sendOmnitureScreen()
    func showOperativeBarInfo(withDisplayData displayData: ButtonsBarDisplayData)
    func showCallBBVA(tel: String)

}

protocol TransactionDetailPresenterProtocol: PresenterProtocol {

    func viewWillAppearAfterDismiss()
    func showNotOperationPressed()
    func goBackAction()
    func getTransactionInformation()
    func showOperativeBarInfo()
    func buttonBarOptionPressed(withDisplayItem displayItem: ButtonBarDisplayData)
}
