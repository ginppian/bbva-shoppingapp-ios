//
//  TransactionDetailViewController.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 15/11/2017.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit
import CellsNativeCore
import CellsNativeComponents

class TransactionDetailViewController: BaseViewController {

    static let ID: String = String(describing: TransactionDetailViewController.self)

    var presenter: TransactionDetailPresenterProtocol!

    @IBOutlet weak var disclaimerView: DisclaimerView!
    @IBOutlet weak var transactionDetailView: TransactionDetailView!
    @IBOutlet weak var optionsBarView: ButtonsBarView!
    @IBOutlet weak var scrollView: UIScrollView!

    @IBOutlet weak var transactionDetailComponentViewHeight: NSLayoutConstraint!

    var cardBO: CardBO!

    override func viewDidLoad() {

        self.configure(appLifecycle: (UIApplication.shared.delegate as! AppDelegate).appLifeCycle!, id: TransactionDetailViewController.ID)

        super.viewDidLoad()

        transactionDetailView.delegate = self

        optionsBarView.delegate = self

        disclaimerView.delegate = self

        if #available(iOS 11.0, *) {
            scrollView.contentInsetAdjustmentBehavior = .never
        }
    }

    override func viewWillAppear(_ animated: Bool) {

        super.viewWillAppear(animated)

        configureNavigationBar()

        presenter.getTransactionInformation()

        presenter.showOperativeBarInfo()

        setAccessibilities()

        BusManager.sessionDataInstance.disableSwipeLateralMenu()
    }
    
    override func viewWillAppearAfterDismiss() {
        
        presenter.viewWillAppearAfterDismiss()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

    func configureNavigationBar() {
        setNavigationBackButtonDefault()
        setNavigationTitle(withText: Localizables.transactions.key_card_movement_details_title_text)
    }

    override func backAction() {
        presenter.goBackAction()
    }

    override func initModel(withModel model: Model) {

        presenter.setModel(model: model)
    }

    deinit {
        ComponentManager.remove(id: presenter.routerTag)
        DLog(message: "Deinitialized TransactionDetailViewController")
    }
}
extension TransactionDetailViewController: TransactionDetailViewProtocol {

    func showError(error: ModelBO) {
    }

    func changeColorEditTexts() {
    }

    func showTransactionDetail(withTransactionDisplayItems displayItems: DisplayTransactionDetail) {

        disclaimerView.setDisclaimerInformation(withDisclaimerData: displayItems.displayDisclaimerData)
        transactionDetailView.setTransactionDetailData(withDisplayData: displayItems.transactionDetailData)
    }

    func sendOmnitureScreen() {
        TrackerHelper.sharedInstance().trackTransactionsDetailScreen(withCustomerID: SessionDataManager.sessionDataInstance().customerID)
    }

    func showOperativeBarInfo(withDisplayData displayData: ButtonsBarDisplayData) {
        optionsBarView.show(displayButtonsBarDisplayData: displayData)
    }

    func showCallBBVA(tel: String) {

        let scheme = "tel:\(tel)"
        let urlOpener = URLOpener()
        
        if let url = URL(string: scheme) {
            urlOpener.openURL(url)
        }
    }
}
extension TransactionDetailViewController: TransactionDetailViewDelegate {

    func setTransactionDetailHeight(withHeight height: CGFloat) {
        transactionDetailComponentViewHeight.constant = height
    }

}

extension TransactionDetailViewController: ButtonsBarViewDelegate {

    func buttonBarPressed(_ buttonsBarView: ButtonsBarView, withDisplayItem displayItem: ButtonBarDisplayData) {
        presenter.buttonBarOptionPressed(withDisplayItem: displayItem)
    }
}

extension TransactionDetailViewController: DisclaimerViewDelegate {

    func disclaimerViewClickTextDisclaimer(_ disclaimerView: DisclaimerView) {
        presenter.showNotOperationPressed()
    }

}

// MARK: - APPIUM

private extension TransactionDetailViewController {

    func setAccessibilities() {
        #if APPIUM
            Tools.setAccessibility(view: disclaimerView.informationButton, identifier: ConstantsAccessibilities.TRANSACTION_DETAIL_DID_OPERATION_TITLE)
        #endif
    }

}
