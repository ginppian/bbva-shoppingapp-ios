//
//  DisplayTransactionDetail.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 17/11/2017.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation

struct DisplayTransactionDetail {

    var displayDisclaimerData: DisclaimerDisplayData
    var transactionDetailData: TransactionDetailDisplayData

}
