//
//  TransactionDetailPageReaction.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 26/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

class TransactionDetailPageReaction: PageReaction {

    static let ROUTER_TAG: String = "transactions-detail-tag"

    // MARK: ActionSpec

    static let EVENT_NAV_TO_NEXT_SCREEN: ActionSpec<Void> = ActionSpec<Void>(id: "EVENT_NAV_TO_NEXT_SCREEN")

    static let ACTION_NAVIGATE_TO_NOT_OPERATION: ActionSpec<Model> = ActionSpec<Model>(id: "ACTION_NAVIGATE_TO_NOT_OPERATION")

    static let PARAMS_BLOCK_CARD: ActionSpec<Any> = ActionSpec<Any>(id: "PARAMS_BLOCK_CARD")

    static let PARAMS_ON_OFF_CARD: ActionSpec<Any> = ActionSpec<Any>(id: "PARAMS_ON_OFF_CARD")

    static let PARAMS_NOT_RECOGNIZED_OPTIONS: ActionSpec<Any> = ActionSpec<Any>(id: "PARAMS_NOT_RECOGNIZED_OPTIONS")

    // MARK: Channels

    static let BLOCK_CARD_INFORMATION_CHANNEL = "cbi_card_information_channel"

    static let BLOCK_TYPE_INFORMATION_CHANNEL = "cbt_card_information_channel"

    static let ON_OFF_CARD_INFORMATION_CHANNEL = "poc_information_channel"

    static let PARAMS_CURRENT_CARDBO: ActionSpec<Any> = ActionSpec<Any>(id: "PARAMS_CURRENT_CARDBO")

    static let CURRENT_CARD_INFORMATION_CHANNEL = "current_card_information_channel"

    static let SHOW_NOT_RECOGNIZED_OPTIONS_CHANNEL = "nro_information_channel"

    // CELLS PAGES

    static let CELLS_PAGE_NOT_RECOGNIZED_OPERATION = "cardUnknowTransaction"

    override func configure() {

//        navigateNotOperation()

        paramsBlockCard()

        registerReactionToWriteInChannelParamsOnOff()

        registerReactionToWriteInChannelParamsCurrentCard()

        registerReactionToWriteInChannelParamsToShowNotRecognizedOptions()
    }

    func paramsBlockCard() {

        let channelBlockInfo: JsonChannel = JsonChannel(TransactionDetailPageReaction.BLOCK_CARD_INFORMATION_CHANNEL)

        _ = writeOn(channel: channelBlockInfo)
            .when(componentID: TransactionDetailPageReaction.ROUTER_TAG)
            .doAction(actionSpec: TransactionDetailPageReaction.PARAMS_BLOCK_CARD)

        let channelBlockType: JsonChannel = JsonChannel(TransactionDetailPageReaction.BLOCK_TYPE_INFORMATION_CHANNEL)

        _ = writeOn(channel: channelBlockType)
            .when(componentID: TransactionDetailPageReaction.ROUTER_TAG)
            .doAction(actionSpec: TransactionDetailPageReaction.PARAMS_BLOCK_CARD)
    }
    func registerReactionToWriteInChannelParamsOnOff() {

        let channel: JsonChannel = JsonChannel(TransactionDetailPageReaction.ON_OFF_CARD_INFORMATION_CHANNEL)

        _ = writeOn(channel: channel)
            .when(componentID: TransactionDetailPageReaction.ROUTER_TAG)
            .doAction(actionSpec: TransactionDetailPageReaction.PARAMS_ON_OFF_CARD)
    }

    func registerReactionToWriteInChannelParamsCurrentCard() {

        let channel: Channel = Channel<Any>(name: TransactionDetailPageReaction.CURRENT_CARD_INFORMATION_CHANNEL)

        _ = writeOn(channel: channel)
            .when(componentID: TransactionDetailPageReaction.ROUTER_TAG)
            .doAction(actionSpec: TransactionDetailPageReaction.PARAMS_CURRENT_CARDBO)
    }

    func registerReactionToWriteInChannelParamsToShowNotRecognizedOptions() {

        let channel: JsonChannel = JsonChannel(TransactionDetailPageReaction.SHOW_NOT_RECOGNIZED_OPTIONS_CHANNEL)

        _ = writeOn(channel: channel)
            .when(componentID: TransactionDetailPageReaction.ROUTER_TAG)
            .doAction(actionSpec: TransactionDetailPageReaction.PARAMS_NOT_RECOGNIZED_OPTIONS)
    }

//    func navigateNotOperation() {
//
//        _ = when(id: TransactionDetailPageReaction.ROUTER_TAG, type: Model.self)
//            .doAction(actionSpec: TransactionDetailPageReaction.ACTION_NAVIGATE_TO_NOT_OPERATION)
//            .then()
//            .doWebNavigation(webRoute: TransactionDetailPageReaction.CELLS_PAGE_NOT_RECOGNIZED_OPERATION, nativeRoute: Route(viewController: { () -> UIViewController in
//                return (BusManager.sessionDataInstance.routerFactory?.getViewController(route: NotRecognizedOperationViewController.ID))!
//            }, animationToNext: { _, destinationViewController in
//
//                destinationViewController?.modalPresentationStyle = .overCurrentContext
//
//                if let destinationViewController = destinationViewController {
//                    let navigationController = UINavigationController(rootViewController: destinationViewController)
//                    BusManager.sessionDataInstance.present(viewController: navigationController, animated: true, completion: nil)
//                }
//
//            }, animationToPrevious: { _, _ in
//
//            }
//            ))
//    }
}
