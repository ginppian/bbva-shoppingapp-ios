//
//  TransactionDetailPresenter.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 15/11/2017.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation
import RxSwift
import CellsNativeComponents

class TransactionDetailPresenter<T: TransactionDetailViewProtocol>: BasePresenter<T> {

    var transactionDisplayItem: TransactionDisplayItem?
    var cardBO: CardBO?

    required init() {
        super.init(tag: TransactionDetailPageReaction.ROUTER_TAG)
    }

    override init(busManager: BusManager) {
        super.init(busManager: busManager)
        routerTag = TransactionDetailPageReaction.ROUTER_TAG
    }

    override func setModel(model: Model) {

        if let transactionsTranport = model as? TransactionsTransport {
            let newTransactionDisplayItem = transactionsTranport.transactionDisplayItem
            let newCardBO = transactionsTranport.cardBO

            if let tempTransactionDisplayItem = newTransactionDisplayItem {
                transactionDisplayItem = tempTransactionDisplayItem
            }

            if let tempCardBO = newCardBO {
                cardBO = tempCardBO
            }
        }
    }

    private func blockCard(withCardBO cardBO: CardBO) {

        var blockCardTranport = BlockCardTransport(id: cardBO.cardId, cardNumber: String(cardBO.number.suffix(4)), alias: cardBO.cardAlias())

        if let url = cardBO.imageFront {
            blockCardTranport.imageList = [CardImage(url: url)]
        }

        if let blockCardJSON = blockCardTranport.toJSON() {
            busManager.publishData(tag: TransactionDetailPageReaction.ROUTER_TAG, TransactionDetailPageReaction.PARAMS_BLOCK_CARD, blockCardJSON)
        }
    }
    private func changeCardStatus(withCardBO cardBO: CardBO) {

        var onOffCardTransport = OnOffCardTransport(id: cardBO.cardId, cardNumber: String(cardBO.number.suffix(4)), alias: cardBO.cardAlias(), enabled: cardBO.isCardOn())

        if let url = cardBO.imageFront {
            onOffCardTransport.imageList = [OnOffCardImage(url: url)]
        }

        if let onOffCardJSON = onOffCardTransport.toJSON() {
            busManager.publishData(tag: TransactionDetailPageReaction.ROUTER_TAG, TransactionDetailPageReaction.PARAMS_ON_OFF_CARD, onOffCardJSON)
        }

        busManager.publishData(tag: TransactionDetailPageReaction.ROUTER_TAG, TransactionDetailPageReaction.PARAMS_CURRENT_CARDBO, cardBO)

    }
    
    private func setNotRecognizedOperations(withCardBO cardBO: CardBO) {

        let notRecognizedOperationUtils = NotRecognizedOperationUtils()
        notRecognizedOperationUtils.startFindNotRecognizedOperationViewController(close: false)
        
        if !notRecognizedOperationUtils.foundCellsViewControllerPresented
            && !notRecognizedOperationUtils.foundCellsViewControllerPushed {
            
            let isBlockable = cardBO.isCardOperative() && cardBO.isBlockable()
            
            let isPowerOffHidden = (!cardBO.isCardOperative() || !cardBO.isCardActivationsOnOff()) ? true : !cardBO.allowCustomizeActivations()
            
            let notRecognizedOperationTransport = NotRecognizedOptionsTransport(isBlockHidden: !isBlockable, isPowerOffHidden: isPowerOffHidden, isCallHidden: false, isLoginHidden: true)
            
            if let notRecognizedOperationTransportJSON = notRecognizedOperationTransport.toJSON() {
                busManager.publishData(tag: TransactionDetailPageReaction.ROUTER_TAG, TransactionDetailPageReaction.PARAMS_NOT_RECOGNIZED_OPTIONS, notRecognizedOperationTransportJSON)
            }
        }
    }
}

extension TransactionDetailPresenter: TransactionDetailPresenterProtocol {

    func viewWillAppearAfterDismiss() {
        
        view?.sendOmnitureScreen()
    }
    
    func showNotOperationPressed() {
        busManager.navigateScreen(tag: routerTag, TransactionDetailPageReaction.ACTION_NAVIGATE_TO_NOT_OPERATION, EmptyModel())
        blockCard(withCardBO: cardBO!)
        changeCardStatus(withCardBO: cardBO!)
        setNotRecognizedOperations(withCardBO: cardBO!)
    }

    func getTransactionInformation() {
        view?.sendOmnitureScreen()
        view?.showTransactionDetail(withTransactionDisplayItems: createDisplayItems(withTransactionDisplayItem: transactionDisplayItem!))
    }

    func createDisplayItems(withTransactionDisplayItem transactionDisplayItem: TransactionDisplayItem) -> DisplayTransactionDetail {

        let displayTransactionDetail = DisplayTransactionDetail(displayDisclaimerData: DisclaimerDisplayData(), transactionDetailData: TransactionDetailDisplayData(withTransactionDetailDisplayData: transactionDisplayItem))

        return displayTransactionDetail
    }

    func goBackAction() {
        busManager.back()
    }

    func showOperativeBarInfo() {

        view?.showOperativeBarInfo(withDisplayData: ButtonsBarDisplayData.generateButtonsBarOnTransactionDisplayData())
    }

    func buttonBarOptionPressed(withDisplayItem displayItem: ButtonBarDisplayData) {

        switch displayItem.optionKey {
        case .callBBVA:
            view?.showCallBBVA(tel: Settings.Global.default_bbva_phone_number)
        default:
            break
        }
    }
}
