//
//  ShoppingFilterPageReaction.swift
//  shoppingapp
//
//  Created by jesus.martinez on 14/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

class ShoppingFilterPageReaction: PageReaction {

    static let SHOPPING_FILTERS: Channel<Model> = Channel(name: "channel-shopping-filters")
    
    static let ROUTER_TAG: String = "shopping-filter-tag"

    static let EVENT_NAV_TO_SHOPPING_LIST_FILTERED: ActionSpec<Model> = ActionSpec<Model>(id: "EVENT_NAV_TO_SHOPPING_LIST_FILTERED")
    static let EVENT_PUBLISH_FILTERS: ActionSpec<Model> = ActionSpec<Model>(id: "EVENT_PUBLISH_FILTERS")
    static let EVENT_FILTERVIEW_DISMISSED: ActionSpec<Void> = ActionSpec<Void>(id: "EVENT_FILTERVIEW_DISMISSED")

    override func configure() {

        navigateShoppingListFilteredFromShoppingFilter()
        publishFilters()
    }

    func navigateShoppingListFilteredFromShoppingFilter() {

        _ = when(id: ShoppingFilterPageReaction.ROUTER_TAG, type: Model.self)
            .doAction(actionSpec: ShoppingFilterPageReaction.EVENT_NAV_TO_SHOPPING_LIST_FILTERED)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, model in

                router.navigateShoppingListFilteredFromShoppingFilter(withModel: model!)

            }
    }
    
    func publishFilters() {
        
        _ = writeOn(channel: ShoppingFilterPageReaction.SHOPPING_FILTERS)
            .when(componentID: ShoppingFilterPageReaction.ROUTER_TAG)
            .doAction(actionSpec: ShoppingFilterPageReaction.EVENT_PUBLISH_FILTERS)
    }
}
