//
//  ShoppingFilterViewController.swift
//  shoppingapp
//
//  Created by jesus.martinez on 14/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit
import CellsNativeCore
import CellsNativeComponents

class ShoppingFilterViewController: BaseViewController {

    // MARK: Constants
    static let ID: String = String(describing: ShoppingFilterViewController.self)

    var presenter: ShoppingFilterPresenterProtocol!

    @IBOutlet weak var scrollView: UIScrollView!

    // Text filter
    @IBOutlet weak var textFilterContainerView: UIView!
    @IBOutlet weak var textFilterView: TextFilterView!

    // Promotion type filter
    @IBOutlet weak var promotionTypeFilterView: OptionsSelectionableView!

    // Commerce type filter
    @IBOutlet weak var commerceTypeFilterView: OptionsSelectionableView!
    @IBOutlet weak var separatorLineView: UIView!

    // Categories filter
    @IBOutlet weak var categoriesFilterView: MultiSelectorListView!
    @IBOutlet weak var categoriesHeightContraint: NSLayoutConstraint!

    // Bottom container
    @IBOutlet weak var bottomContainerView: UIView!
    @IBOutlet weak var restoreButton: UIButton!
    @IBOutlet weak var filterButton: UIButton!

    override func viewDidLoad() {

        self.configure(appLifecycle: (UIApplication.shared.delegate as! AppDelegate).appLifeCycle!, id: ShoppingFilterViewController.ID)

        super.viewDidLoad()

        configureView()
        configureBottomViews()

        if #available(iOS 11.0, *) {
            scrollView.contentInsetAdjustmentBehavior = .never
        }

        TrackerHelper.sharedInstance().trackPromotionsFilterScreen(withCustomerID: SessionDataManager.sessionDataInstance().customerID)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        presenter.viewWillAppear()

        setNavigationBarDefault()
        setNavigationBackButtonDefault()
        setNavigationTitle(withText: Localizables.promotions.key_search_text)

        let image = SVGCache.image(forSVGNamed: ConstantsImages.Common.close_icon, color: .BBVAWHITE)
        setNavigationLeftButton(withImage: image, action: #selector(dismissAction))

        setAccessibilities()

        BusManager.sessionDataInstance.disableSwipeLateralMenu()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        presenter.viewWasDismissed()
    }

    func configureView() {

        textFilterContainerView.backgroundColor = .NAVY

        let twoColorView = UIView(frame: view.frame)
        let navyView = UIView(frame: CGRect(x: view.frame.origin.x, y: view.frame.origin.y, width: view.frame.size.width, height: view.frame.size.height / 2))
        let whiteView = UIView(frame: CGRect(x: view.frame.origin.x, y: view.frame.size.height / 2, width: view.frame.size.width, height: view.frame.size.height / 2))
        navyView.backgroundColor = .NAVY
        whiteView.backgroundColor = .BBVAWHITE
        twoColorView.addSubview(navyView)
        twoColorView.addSubview(whiteView)

        view.insertSubview(twoColorView, belowSubview: scrollView)

        scrollView.backgroundColor = .clear

        textFilterView.delegate = self

        promotionTypeFilterView.backgroundColor = .BBVA200
        promotionTypeFilterView.delegate = self

        commerceTypeFilterView.backgroundColor = .BBVA200
        commerceTypeFilterView.delegate = self
        separatorLineView.backgroundColor = .BBVA300

        categoriesFilterView.backgroundColor = .BBVAWHITE
        categoriesFilterView.delegate = self

    }

    func configureBottomViews() {

        bottomContainerView.backgroundColor = .BBVAWHITE
        bottomContainerView.layer.shadowColor = UIColor.BBVA300.cgColor
        bottomContainerView.layer.shadowOpacity = 1
        bottomContainerView.layer.shadowOffset = CGSize.zero
        bottomContainerView.layer.shadowRadius = 2

        restoreButton.setTitle(Localizables.transactions.key_transactions_restore_button, for: .normal)
        restoreButton.setTitleColor(.BBVAWHITE, for: .normal)
        restoreButton.titleLabel?.font = Tools.setFontBold(size: 14)
        restoreButton.setBackgroundColor(color: .BBVA200, forUIControlState: .disabled)
        restoreButton.setBackgroundColor(color: .MEDIUMBLUE, forUIControlState: .normal)
        restoreButton.setBackgroundColor(color: .DARKMEDIUMBLUE, forUIControlState: .highlighted)
        restoreButton.isEnabled = false

        filterButton.setTitle(Localizables.transactions.key_transactions_results_button, for: .normal)
        filterButton.setTitleColor(.BBVAWHITE, for: .normal)
        filterButton.titleLabel?.font = Tools.setFontBold(size: 14)
        filterButton.setBackgroundColor(color: .BBVA200, forUIControlState: .disabled)
        filterButton.setBackgroundColor(color: .COREBLUE, forUIControlState: .normal)
        filterButton.setBackgroundColor(color: .DARKCOREBLUE, forUIControlState: .highlighted)
        filterButton.isEnabled = false

    }

    deinit {
        ComponentManager.remove(id: presenter.routerTag)
        DLog(message: "Deinitialized ShoppingFilterViewController")
    }

    @IBAction func restoreAction(_ sender: Any) {
        presenter.restorePressed()
    }

    @IBAction func filterAction(_ sender: Any) {
        presenter.filterPressed()
    }

    override func initModel(withModel model: Model) {
        presenter.setModel(model: model)
    }
}

extension ShoppingFilterViewController: ShoppingFilterViewProtocol {

    func showError(error: ModelBO) {

    }

    func changeColorEditTexts() {

    }

    func showTextFilter(withDisplayData displayData: TextFilterDisplayData) {
        textFilterView.presenter.displayData = displayData
    }

    func restoreTextFilter() {
        textFilterView.presenter.removeAction()
    }

    func enableRestore() {
        restoreButton.isEnabled = true
    }

    func disableRestore() {
        restoreButton.isEnabled = false
    }

    func enableFilter() {
        filterButton.isEnabled = true
    }
    
    func disableFilter() {
        filterButton.isEnabled = false
    }

    func showTypePromotionFilter(withDisplayData displayData: OptionsSelectionableDisplayData) {

        promotionTypeFilterView.presenter.displayData = displayData

    }

    func restoreTypePromotionFilter() {
        promotionTypeFilterView.deselectAll()
    }

    func showTypeCommerceFilter(withDisplayData displayData: OptionsSelectionableDisplayData) {

        commerceTypeFilterView.isHidden = false
        commerceTypeFilterView.presenter.displayData = displayData

    }

    func restoreTypeCommerceFilter() {
        commerceTypeFilterView.deselectAll()
    }

    func hideTypeCommerceFilter() {

        commerceTypeFilterView.isHidden = true

    }

    func showCategoriesFilter(withDisplayData displayData: MultiSelectorListDisplayData) {

        categoriesFilterView.isHidden = false
        categoriesFilterView.presenter.displayData = displayData

    }

    func hideCategoriesFilter() {

        categoriesFilterView.isHidden = true

    }

    func restoreCategoriesFilter() {
        categoriesFilterView.clearSelected()
    }

}

extension ShoppingFilterViewController: TextFilterViewDelegate {

    func textFilterUpdate(textFilterView: TextFilterView) {

        if let text = textFilterView.textField.text {
            presenter.updateTextFilter(withText: text)
        }

    }

}

extension ShoppingFilterViewController: OptionsSelectionableViewDelegate {

    func optionsSelectionableView(_ optionsSelectionableView: OptionsSelectionableView, didSelectOptionAtIndex index: Int) {

        if optionsSelectionableView == promotionTypeFilterView {
            presenter.selectedPromotionType(atIndex: index)
        } else {
            presenter.selectedPromotionUsageType(atIndex: index)
        }

    }

    func optionsSelectionableView(_ optionsSelectionableView: OptionsSelectionableView, didDeselectOptionAtIndex index: Int) {

        if optionsSelectionableView == promotionTypeFilterView {
            presenter.deselectedPromotionType(atIndex: index)
        } else {
            presenter.deselectedPromotionUsageType(atIndex: index)
        }

    }

}

extension ShoppingFilterViewController: MultiSelectorListViewDelegate {

    func multiSelectorListViewShowMoreInfo(_ multiSelectorListView: MultiSelectorListView, requiresHeight height: CGFloat) {

        categoriesHeightContraint.constant = height

    }

    func multiSelectorListView(_ multiSelectorListView: MultiSelectorListView, selectedAtIndex index: Int) {
        presenter.selectCategoryType(atIndex: index)
    }

    func multiSelectorListView(_ multiSelectorListView: MultiSelectorListView, deSelectedAtIndex index: Int) {
        presenter.deselectCategoryType(atIndex: index)
    }

}

// MARK: - APPIUM

private extension ShoppingFilterViewController {

    func setAccessibilities() {

        #if APPIUM

        #endif

    }

}
