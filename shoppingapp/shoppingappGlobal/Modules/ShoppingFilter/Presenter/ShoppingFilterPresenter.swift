//
//  ShoppingFilterPresenter.swift
//  shoppingapp
//
//  Created by jesus.martinez on 14/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeComponents

class ShoppingFilterPresenter<T: ShoppingFilterViewProtocol>: BasePresenter<T> {

    var maximuLengthTextFilter = Settings.PromotionsFilter.maximum_length_text_filter
    var validCharactersInTextFiler = Settings.PromotionsFilter.valid_characters_text_filter

    var promotionTypes = Settings.PromotionsFilter.promotion_types
    var promotionUsageTypes = Settings.PromotionsFilter.promotion_usage_types

    var filters = ShoppingFilter()
    var showCommerceFilter = true
    var shouldFilterInPreviousScreen = false
    var categoriesBO: CategoriesBO?

    required init() {
        super.init(tag: ShoppingFilterPageReaction.ROUTER_TAG)
    }

    override init(busManager: BusManager) {
        super.init(busManager: busManager)
        routerTag = ShoppingFilterPageReaction.ROUTER_TAG
    }

    override func setModel(model: Model) {

        if let shoppingFilterTransport = model as? ShoppingFilterTransport {
            showCommerceFilter = shoppingFilterTransport.shouldAllowSelectUsageType
            categoriesBO = shoppingFilterTransport.categoriesBO
            categoriesBO?.sortAlphabeticallyByName()
            shouldFilterInPreviousScreen = shoppingFilterTransport.shouldFilterInPreviousScreen

            filters.textFilter = shoppingFilterTransport.filters?.textFilter
            filters.promotionTypeFilter = shoppingFilterTransport.filters?.promotionTypeFilter
            filters.promotionUsageTypeFilter = shoppingFilterTransport.filters?.promotionUsageTypeFilter
            filters.categoriesFilter = shoppingFilterTransport.filters?.categoriesFilter

        }

    }

    func manageRestoreAndFilterButtonsDependingOnFilters() {

        if filters.isEmpty() {
            view?.disableRestore()
            view?.disableFilter()
        } else {
            view?.enableRestore()
            view?.enableFilter()
        }

    }

}

extension ShoppingFilterPresenter: ShoppingFilterPresenterProtocol {

    func viewWillAppear() {

        view?.showTextFilter(withDisplayData: TextFilterDisplayData(placeHolderTitle: Localizables.promotions.key_search_commerce_name_text, maximumLength: maximuLengthTextFilter, validCharacterSet: validCharactersInTextFiler, defaultValue: filters.textFilter))

        view?.showTypePromotionFilter(withDisplayData: OptionsSelectionableDisplayData(title: Localizables.promotions.key_promotion_type_text.uppercased(), promotionTypes: promotionTypes, promotionTypesSelected: filters.promotionTypeFilter))

        if showCommerceFilter && !promotionUsageTypes.isEmpty {
            view?.showTypeCommerceFilter(withDisplayData: OptionsSelectionableDisplayData(title: Localizables.promotions.key_commerce_type_text.uppercased(), promotionUsageTypes: promotionUsageTypes, promotionUsageTypesSelected: filters.promotionUsageTypeFilter))
        } else {
            view?.hideTypeCommerceFilter()
        }

        if let categories = categoriesBO, !categories.categories.isEmpty {
            view?.showCategoriesFilter(withDisplayData: MultiSelectorListDisplayData(withTitle: Localizables.promotions.key_categories_text, andCategoriesBO: categories, andCategoriesSelected: filters.categoriesFilter))
        } else {
            view?.hideCategoriesFilter()
        }

        if !filters.isEmpty() {
            view?.enableFilter()
            view?.enableRestore()
        } else {
            view?.disableRestore()
            view?.disableFilter()
        }

    }

    func updateTextFilter(withText text: String) {

        filters.textFilter = text

        if !text.isEmpty {
            view?.enableFilter()
        }

        manageRestoreAndFilterButtonsDependingOnFilters()

    }

    func selectedPromotionType(atIndex index: Int) {

        if index < promotionTypes.count && index >= 0 {
            filters.add(promotionType: promotionTypes[index])

            view?.enableFilter()
            view?.enableRestore()
        }

    }

    func deselectedPromotionType(atIndex index: Int) {

        if index < promotionTypes.count && index >= 0 {
            filters.remove(promotionType: promotionTypes[index])
        }

        manageRestoreAndFilterButtonsDependingOnFilters()

    }

    func selectedPromotionUsageType(atIndex index: Int) {

        if index < promotionUsageTypes.count && index >= 0 {
            filters.add(promotionUsageType: promotionUsageTypes[index])

            view?.enableFilter()
            view?.enableRestore()
        }

    }

    func deselectedPromotionUsageType(atIndex index: Int) {

        if index < promotionUsageTypes.count && index >= 0 {
            filters.remove(promotionUsageType: promotionUsageTypes[index])
        }

        manageRestoreAndFilterButtonsDependingOnFilters()

    }

    func selectCategoryType(atIndex index: Int) {

        if let categories = categoriesBO?.categories, index < categories.count && index >= 0 {
            filters.add(categoryType: categories[index].id)

            view?.enableFilter()
            view?.enableRestore()
        }

    }

    func deselectCategoryType(atIndex index: Int) {

        if let categories = categoriesBO?.categories, index < categories.count && index >= 0 {
            filters.remove(categoryType: categories[index].id)
        }

        manageRestoreAndFilterButtonsDependingOnFilters()

    }

    func restorePressed() {

        filters.reset()

        view?.restoreTextFilter()
        view?.restoreTypePromotionFilter()
        view?.restoreTypeCommerceFilter()
        view?.restoreCategoriesFilter()
        view?.disableRestore()
        view?.disableFilter()

    }

    func filterPressed() {

        busManager.publishData(tag: EventTracker.id, PageReactionConstants.NEW_TRACK_EVENT, Event(type: .seeShoppingFilterResults))
        
        let shoppingFilterTransport = ShoppingFilterTransport()
        shoppingFilterTransport.filters = filters
        shoppingFilterTransport.categoriesBO = categoriesBO

        if shouldFilterInPreviousScreen {
            
            busManager.publishData(tag: routerTag, ShoppingFilterPageReaction.EVENT_PUBLISH_FILTERS, shoppingFilterTransport)
            busManager.dismissViewController(animated: true, completion: nil)
        } else {
            busManager.navigateScreen(tag: routerTag, ShoppingFilterPageReaction.EVENT_NAV_TO_SHOPPING_LIST_FILTERED, shoppingFilterTransport)
        }

    }

    func viewWasDismissed() {
        
            busManager.notify(tag: ShoppingFilterPageReaction.ROUTER_TAG, ShoppingFilterPageReaction.EVENT_FILTERVIEW_DISMISSED)
    }
}
