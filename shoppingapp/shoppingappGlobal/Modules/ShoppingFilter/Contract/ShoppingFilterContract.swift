//
//  ShoppingFilterContract.swift
//  shoppingapp
//
//  Created by jesus.martinez on 14/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol ShoppingFilterViewProtocol: ViewProtocol {

    func showTextFilter(withDisplayData displayData: TextFilterDisplayData)
    func restoreTextFilter()

    func showTypePromotionFilter(withDisplayData displayData: OptionsSelectionableDisplayData)
    func restoreTypePromotionFilter()

    func showTypeCommerceFilter(withDisplayData displayData: OptionsSelectionableDisplayData)
    func hideTypeCommerceFilter()
    func restoreTypeCommerceFilter()

    func showCategoriesFilter(withDisplayData displayData: MultiSelectorListDisplayData)
    func hideCategoriesFilter()
    func restoreCategoriesFilter()

    func enableRestore()
    func disableRestore()
    func enableFilter()
    func disableFilter()
}

protocol ShoppingFilterPresenterProtocol: PresenterProtocol {

    func viewWillAppear()

    func updateTextFilter(withText text: String)

    func selectedPromotionType(atIndex index: Int)
    func deselectedPromotionType(atIndex index: Int)

    func selectedPromotionUsageType(atIndex index: Int)
    func deselectedPromotionUsageType(atIndex index: Int)

    func selectCategoryType(atIndex index: Int)
    func deselectCategoryType(atIndex index: Int)

    func restorePressed()
    func filterPressed()

    func viewWasDismissed()

}
