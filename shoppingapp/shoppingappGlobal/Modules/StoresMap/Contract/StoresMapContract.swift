//
//  StoresMapContract.swift
//  shoppingapp
//
//  Created by Developer on 18/07/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import RxSwift
import CoreLocation

protocol StoresMapViewProtocol: ViewProtocol {
    func configureTitle(selectedStoreName: String)
    func showStores(withDisplayData displayData: MapDisplayData)
    func showSelectedStore(withDisplayData displayData: StoreMapDetailDisplayData)
    func showUserLocation()
    func hideUserLocation()
    func hideStore()
    func callToPhone(withPhone phone: String)
    func sendEmail(withEmail email: String)
    func launchMap(withDestinationCoordinate destinationCoordinate: CLLocationCoordinate2D, andOriginCoordinate originCoordinate: CLLocationCoordinate2D?)
}

protocol StoresMapPresenterProtocol: PresenterProtocol {
    
    func viewDidLoad()
    func selectStore(withDisplayData displayData: MapDisplayDataItem)
    func didTapInMap()
    func pressedWebsiteOfStore()
    func pressedCallToPhoneOfStore()
    func pressedSendEmailOfStore()
    func pressedLaunchMapOfStore()
}
