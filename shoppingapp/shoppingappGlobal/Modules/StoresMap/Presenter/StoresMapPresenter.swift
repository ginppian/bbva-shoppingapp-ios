//
//  StoresMapPresenter.swift
//  shoppingapp
//
//  Created by Developer on 18/07/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CoreLocation

class StoresMapPresenter <T: StoresMapViewProtocol>: BasePresenter<T> {

    var promotionBO: PromotionBO?
    var categoryBO: CategoryBO?
    var userLocation: GeoLocationBO?
    var stores = [StoreBO]()
    var selectedStore: StoreBO?
    var dataPois = [DataPoi]()
    var displayData = MapDisplayData()
    var currentStoreMapDetailDisplayData: StoreMapDetailDisplayData?
    var defaultLocation = Settings.Global.default_location

    let ROUTER_TAG: String = "shopping-store-map-tag"
    
    required init() {
        super.init(tag: ROUTER_TAG)
    }
    
    override init(busManager: BusManager) {
        super.init(busManager: busManager)
        routerTag = "shopping-store-map-tag"
    }
    
    override func setModel(model: Model) {
        
        if let promotionDetailTransport = model as? StoresMapTransport {
            
            if let transportPromotionBO = promotionDetailTransport.promotionBO {
                promotionBO = transportPromotionBO
                userLocation = promotionDetailTransport.userLocation
                selectedStore = promotionDetailTransport.selectedStore
                categoryBO = promotionDetailTransport.category
                
                if let promotionStores = transportPromotionBO.stores {
                    stores = promotionStores
                }
            }
        }
    }
    
    private func showStores(withResult resultPromotionBO: PromotionBO?) {
        
        if let promotion = resultPromotionBO {
            
            dataPois = promotion.groupByLatitudeLongitudeStore()
            
            displayData = MapDisplayData(withDataPois: dataPois, forCategory: categoryBO)
            
            if let displayDataItem = displayData.items.first(where: { $0.idStores.first == selectedStore?.id }) {
                displayDataItem.selectedItem = true
                showSelectedStoreData(withDisplayData: displayDataItem)
            }
            
            view?.showStores(withDisplayData: displayData)
        }
    }
    
    private func showSelectedStoreData(withDisplayData displayData: MapDisplayDataItem) {
        
        if let store = stores.first(where: { $0.id == displayData.idStores.first }) {
            selectedStore = store
            let storeMapDetailDisplayData = StoreMapDetailDisplayData(storeBO: store, userLocation: userLocation, promotionType: promotionBO?.promotionType)
            currentStoreMapDetailDisplayData = storeMapDetailDisplayData
            view?.showSelectedStore(withDisplayData: storeMapDetailDisplayData)
            view?.hideUserLocation()
        }
    }
    
    private func navigateToWebPageScreen(withTransport transport: WebTransport) {
        
        busManager.navigateScreen(tag: routerTag, StoresMapPageReaction.EVENT_NAV_WEB_PAGE_FROM_MAP, transport)
        
    }
}

extension StoresMapPresenter: StoresMapPresenterProtocol {
    func viewDidLoad() {
        view?.configureTitle(selectedStoreName: selectedStore?.name ?? "")
        if let promotion = promotionBO {
            showStores(withResult: promotion)
            if userLocation != nil {
                view?.showUserLocation()
            } else {
                view?.hideUserLocation()
            }
        }
    }

    func selectStore(withDisplayData displayData: MapDisplayDataItem) {
        showSelectedStoreData(withDisplayData: displayData)
    }
    
    func didTapInMap() {
        selectedStore = nil
        
        if userLocation != nil {
            view?.showUserLocation()
        }
        
        view?.hideStore()
    }
    
    func pressedWebsiteOfStore() {
        
        if let webPage = currentStoreMapDetailDisplayData?.web {
            
            let webTransport = WebTransport(webPage: webPage, webTitle: promotionBO?.commerceInformation.name)
            navigateToWebPageScreen(withTransport: webTransport)
        }
    }

    func pressedCallToPhoneOfStore() {
        if let phoneNumer = currentStoreMapDetailDisplayData?.getPhoneContact() {
            view?.callToPhone(withPhone: phoneNumer)
        }
    }

    func pressedSendEmailOfStore() {
        if let email = currentStoreMapDetailDisplayData?.email {
            view?.sendEmail(withEmail: email)
        }
    }

    func pressedLaunchMapOfStore() {
        var originCoordinate = kCLLocationCoordinate2DInvalid

        if let userLocation = userLocation {
            originCoordinate = CLLocationCoordinate2D(latitude: userLocation.latitude, longitude: userLocation.longitude)
        }

        let destination = CLLocationCoordinate2D(latitude: currentStoreMapDetailDisplayData?.latitude ?? defaultLocation.latitude, longitude: currentStoreMapDetailDisplayData?.longitude ?? defaultLocation.longitude)

        if userLocation == nil || !CLLocationCoordinate2DIsValid(originCoordinate) || (originCoordinate.latitude == 0 && originCoordinate.longitude == 0) {
            originCoordinate = CLLocationCoordinate2D(latitude: defaultLocation.latitude, longitude: defaultLocation.longitude)
        }

        view?.launchMap(withDestinationCoordinate: destination, andOriginCoordinate: originCoordinate)
    }
}
