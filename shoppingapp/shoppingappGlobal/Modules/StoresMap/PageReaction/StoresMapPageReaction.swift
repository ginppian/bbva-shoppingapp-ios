//
//  StoresMapPageReaction.swift
//  shoppingapp
//
//  Created by developer on 08/10/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

class StoresMapPageReaction: PageReaction {
    
    static let ROUTER_TAG: String = "shopping-store-map-tag"
    
    static let EVENT_NAV_WEB_PAGE_FROM_MAP: ActionSpec<Model> = ActionSpec<Model>(id: "EVENT_NAV_WEB_PAGE_FROM_MAP")
    
    override func configure() {
        navigateToStoreMapScreen()
    }
    
    func navigateToStoreMapScreen() {
        _ = when(id: StoresMapPageReaction.ROUTER_TAG, type: Model.self)
            .doAction(actionSpec: StoresMapPageReaction.EVENT_NAV_WEB_PAGE_FROM_MAP)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, model in
                guard let modelForNavigation = model else {
                    return
                }
                
                router.navigateToShoppingWebPage(withModel: modelForNavigation)
                
        }
    }
}
