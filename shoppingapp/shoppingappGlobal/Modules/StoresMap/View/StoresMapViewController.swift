//
//  StoresMapViewController.swift
//  shoppingapp
//
//  Created by Developer on 18/07/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit
import GoogleMaps
import MessageUI

class StoresMapViewController: BaseViewController {

    static let ID: String = String(describing: StoresMapViewController.self)
    
    var presenter: StoresMapPresenterProtocol!
    let defaultZoom = Settings.Promotions.default_zoom_of_associated_trade_map

    var selectedMarker: GMSMarker?
    var hiddingPromotions = false
    let trackerCustomerID = SessionDataManager.sessionDataInstance().customerID

    // MARK: outlets
    @IBOutlet weak var mapView: GMSMapView!
    
    @IBOutlet weak var contentStackView: UIStackView!
    @IBOutlet weak var infoPromotionView: UIView!
    @IBOutlet weak var containerStoreDetail: UIView!

    @IBOutlet weak var distanceLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var additionalInfoLabel: UILabel!
    
    @IBOutlet weak var infoLineView: UIView!
    
    @IBOutlet weak var routeButton: UIButton!
    @IBOutlet weak var webButton: UIButton!
    @IBOutlet weak var emailButton: UIButton!
    @IBOutlet weak var phoneButton: UIButton!
    
    @IBOutlet weak var contentStackViewCenterConstraint: NSLayoutConstraint!
    @IBOutlet weak var contentStackViewLeadingConstraint: NSLayoutConstraint!
    @IBOutlet weak var contentStackViewTranilingEqualsToLeadingMap: NSLayoutConstraint!
    
    override func viewDidLoad() {
        self.configure(appLifecycle: (UIApplication.shared.delegate as! AppDelegate).appLifeCycle!, id: StoresMapViewController.ID)
        
        super.viewDidLoad()
        configurationInfoPromotionView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        setNavigationBarDefault()
        
        setNavigationBackButtonDefault()
        
        BusManager.sessionDataInstance.disableSwipeLateralMenu()
        
        presenter.viewDidLoad()
        
    }
    
    override func initModel(withModel model: Model) {
        presenter.setModel(model: model)
    }
    
    func configurationInfoPromotionView() {
        configurationMapView()
        
        distanceLabel.font = Tools.setFontMedium(size: 14)
        distanceLabel.textColor = .BBVA600
        
        addressLabel.font = Tools.setFontBook(size: 14)
        addressLabel.textColor = .BBVA600
        
        additionalInfoLabel.font = Tools.setFontBook(size: 14)
        additionalInfoLabel.textColor = .BBVA500
        
        infoLineView.backgroundColor = .BBVA300
        
        routeButton.setTitle(Localizables.promotions.key_how_to_get_text, for: .normal)
        routeButton.titleLabel?.font = Tools.setFontMedium(size: 14)
        routeButton.setTitleColor(.MEDIUMBLUE, for: .normal)
        routeButton.setTitleColor(.COREBLUE, for: .highlighted)
        routeButton.setTitleColor(.COREBLUE, for: .selected)
        
        webButton.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Promotions.ecommerce, color: .MEDIUMBLUE), width: Float(webButton.frame.size.width), height: Float(webButton.frame.size.height)), for: .normal)
        webButton.tintColor = .MEDIUMBLUE
        
        emailButton.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Common.email, color: .MEDIUMBLUE), width: Float(emailButton.frame.size.width), height: Float(emailButton.frame.size.height)), for: .normal)
        emailButton.tintColor = .MEDIUMBLUE
        
        phoneButton.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Common.telephone_bbva_line, color: .MEDIUMBLUE), width: Float(phoneButton.frame.size.width), height: Float(phoneButton.frame.size.height)), for: .normal)
        phoneButton.tintColor = .MEDIUMBLUE
    }
    
    func configurationMapView() {
        
        mapView.settings.myLocationButton = true
        mapView.delegate = self
    }
    
    func setupDetailStoreView(withDisplayData displayData: StoreMapDetailDisplayData) {
                
        emailButton.isHidden = true
        phoneButton.isHidden = true
        webButton.isHidden = true
        
        setupLabel(label: distanceLabel, withText: displayData.distance)
        setupLabel(label: addressLabel, withText: displayData.address)
        setupLabel(label: additionalInfoLabel, withText: displayData.additionalInfo)
        
        setupButton(button: emailButton, withContact: displayData.email)
        setupButton(button: phoneButton, withContact: displayData.getPhoneContact())
        setupButton(button: webButton, withContact: displayData.web)
    }
    
    func setupButton(button: UIButton, withContact contact: String?) {
        
        if let contact = contact, !contact.isEmpty {
            button.isHidden = false
        } else {
            button.isHidden = true
        }
    }
    
    func setupLabel(label: UILabel, withText text: String?) {
        
        if let text = text, !text.isEmpty {
            
            label.isHidden = false
            label.text = text
            
        } else {
            label.isHidden = true
        }
    }
    
    func createIcon(mapDisplayDataItem: MapDisplayDataItem) -> UIView {
        
        let view = UIView(frame: CGRect(x: 0, y: 0, width: ShoppingMapViewController.sizeMapIcon, height: ShoppingMapViewController.sizeMapIcon))
        view.layer.cornerRadius = min(view.frame.size.height, view.frame.size.width) / 2.0
        view.clipsToBounds = true
        view.backgroundColor = mapDisplayDataItem.backgroundColor
        
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: ShoppingMapViewController.sizeMapImage, height: ShoppingMapViewController.sizeMapImage))
        
        imageView.image = SVGCache.image(forSVGNamed: ConstantsImages.Promotions.tab_promotions, color: .BBVAWHITE)
        imageView.backgroundColor = mapDisplayDataItem.backgroundColor
        
        view.addSubview(imageView)
        imageView.center = view.center

        let wrapperView = UIView(frame: CGRect(x: 0, y: 0, width: ShoppingMapViewController.sizeMapIcon, height: ShoppingMapViewController.sizeMapIcon))

        wrapperView.layer.cornerRadius = ShoppingMapViewController.sizeMapIcon / 2
        wrapperView.addSubview(view)

        return wrapperView
        
    }
    
    func deselectMarker() {

        if let markerBeforeSelected = selectedMarker, let markerView = markerBeforeSelected.iconView {

            markerBeforeSelected.tracksViewChanges = true
            let contentView = markerView.subviews[0]

            UIView.animate(withDuration: ShoppingMapViewController.animationTapPromotionDuration, animations: {

                contentView.transform = .identity
            }, completion: { _ in

                markerBeforeSelected.tracksViewChanges = false

                markerView.frame = CGRect(center: markerView.center, size: CGSize(width: ShoppingMapViewController.sizeMapIcon, height: ShoppingMapViewController.sizeMapIcon))
                contentView.center = CGPoint(x: ShoppingMapViewController.sizeMapIcon / 2, y: ShoppingMapViewController.sizeMapIcon / 2)
                markerView.layer.cornerRadius = ShoppingMapViewController.sizeMapIcon / 2
            })

        }

    }
    
    func addMarker(withDisplayData displayDataItem: MapDisplayDataItem) {
        let deadlineTime = DispatchTime.now() + .milliseconds(100)
        DispatchQueue.main.asyncAfter(deadline: deadlineTime) { [weak self] in
            guard let `self` = self else {
                return
            }
            let location = CLLocationCoordinate2D(latitude: displayDataItem.latitude, longitude: displayDataItem.longitude)
            
            let marker = GMSMarker(position: location)
            marker.iconView = self.createIcon(mapDisplayDataItem: displayDataItem)
            marker.map = self.mapView
            marker.userData = displayDataItem
            marker.tracksViewChanges = false
            marker.groundAnchor = CGPoint(x: 0.5, y: 0.5)

            if displayDataItem.selectedItem {
                self.animateMarker(marker: marker)
                
                let target = CLLocationCoordinate2D(latitude: displayDataItem.latitude, longitude: displayDataItem.longitude)
                if CLLocationCoordinate2DIsValid(target), displayDataItem.latitude != 0, displayDataItem.longitude != 0 {
                    
                    self.mapView.camera = GMSCameraPosition.camera(withTarget: target, zoom: Settings.Promotions.default_zoom_of_associated_trade_map)
                }
            }
        }
    }
    
    func animateMarker(marker: GMSMarker) {

        guard let contentView = marker.iconView?.subviews[0] else {
            return
        }

        marker.tracksViewChanges = true

        marker.iconView?.frame = CGRect(center: marker.iconView?.center ?? .zero, size: CGSize(width: ShoppingMapViewController.sizeMapIconSelected, height: ShoppingMapViewController.sizeMapIconSelected))
        contentView.center = CGPoint(x: ShoppingMapViewController.sizeMapIconSelected / 2, y: ShoppingMapViewController.sizeMapIconSelected / 2)
        marker.iconView?.layer.cornerRadius = ShoppingMapViewController.sizeMapIconSelected / 2
        
        view.isUserInteractionEnabled = false
        
        UIView.animate(withDuration: ShoppingMapViewController.animationTapPromotionDuration, animations: {

            contentView.transform = CGAffineTransform(scaleX: ShoppingMapViewController.sizeMapIconSelected / ShoppingMapViewController.sizeMapIcon, y: ShoppingMapViewController.sizeMapIconSelected / ShoppingMapViewController.sizeMapIcon)
        }, completion: { _ in

            marker.tracksViewChanges = false
            self.view.isUserInteractionEnabled = true
        })
        
        selectedMarker = marker
        selectedMarker?.zIndex = ShoppingMapViewController.maximumZIndex
    }
    
    // MARK: Actions Methods
    
    @IBAction func routeButtonPressed(_ sender: Any) {
        
        TrackerHelper.sharedInstance().trackPromotionsDetailScreenButtonHowToGet(withCustomerID: trackerCustomerID)
        
        presenter.pressedLaunchMapOfStore()
    }
    
    @IBAction func webButtonPressed(_ sender: Any) {
        
        TrackerHelper.sharedInstance().trackPromotionsDetailScreenButtonWebSite(withCustomerID: trackerCustomerID)
        
        presenter.pressedWebsiteOfStore()
    }
    
    @IBAction func emailButtonPressed(_ sender: Any) {
        
        TrackerHelper.sharedInstance().trackPromotionsDetailScreenButtonEmail(withCustomerID: trackerCustomerID)
        
        presenter.pressedSendEmailOfStore()
    }
    
    @IBAction func phoneButtonPressed(_ sender: Any) {
        
        TrackerHelper.sharedInstance().trackPromotionsDetailScreenButtonPhone(withCustomerID: trackerCustomerID)
        
        presenter.pressedCallToPhoneOfStore()
    }
}

extension StoresMapViewController: StoresMapViewProtocol {
    
    func configureTitle(selectedStoreName: String) {
        animateShowPromotions()
        setNavigationTitle(withText: selectedStoreName)
    }
    
    func showStores(withDisplayData displayData: MapDisplayData) {
        mapView.clear()
        for displayDataItem in displayData.items {
            addMarker(withDisplayData: displayDataItem)
        }
    }
    
    func showUserLocation() {
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
    }
    
    func showSelectedStore(withDisplayData displayData: StoreMapDetailDisplayData) {
        setupDetailStoreView(withDisplayData: displayData)
        showStore()
    }
    
    func hideUserLocation() {
        mapView.settings.myLocationButton = false
    }
    
    func animateShowPromotions() {
        
        NSLayoutConstraint.deactivate([contentStackViewLeadingConstraint])
        NSLayoutConstraint.deactivate([contentStackViewTranilingEqualsToLeadingMap])
        NSLayoutConstraint.activate([contentStackViewCenterConstraint])
        
        UIView.animate(withDuration: ShoppingMapViewController.animationTapPromotionDuration / 2, animations: {
            self.containerStoreDetail.layoutIfNeeded()
        })
        
    }
    
    func showStore() {
        
        self.infoPromotionView.isUserInteractionEnabled = true
        
        if contentStackViewCenterConstraint.isActive {
            NSLayoutConstraint.deactivate([contentStackViewLeadingConstraint])
            NSLayoutConstraint.deactivate([contentStackViewCenterConstraint])
            NSLayoutConstraint.activate([contentStackViewTranilingEqualsToLeadingMap])
            
            UIView.animate(withDuration: ShoppingMapViewController.animationTapPromotionDuration / 2, animations: {
                self.containerStoreDetail.layoutIfNeeded()
            }, completion: { _ in
                
                NSLayoutConstraint.deactivate([self.contentStackViewTranilingEqualsToLeadingMap])
                NSLayoutConstraint.activate([self.contentStackViewLeadingConstraint])
                self.containerStoreDetail.layoutIfNeeded()
                
                self.animateShowPromotions()
            })
            
        } else {
            self.animateShowPromotions()
        }
    }
    
    func hideStore() {
        
        if !hiddingPromotions {
            hiddingPromotions = true
            
            deselectMarker()
            
            NSLayoutConstraint.deactivate([contentStackViewLeadingConstraint])
            NSLayoutConstraint.deactivate([contentStackViewCenterConstraint])
            NSLayoutConstraint.activate([contentStackViewTranilingEqualsToLeadingMap])
            
            UIView.animate(withDuration: ShoppingMapViewController.animationTapPromotionDuration / 2, animations: {
                self.containerStoreDetail.layoutIfNeeded()
            }, completion: { _ in
                
                NSLayoutConstraint.deactivate([self.contentStackViewTranilingEqualsToLeadingMap])
                NSLayoutConstraint.activate([self.contentStackViewLeadingConstraint])
                
                self.hiddingPromotions = false
            })
        }
        
    }
    
    func callToPhone(withPhone phone: String) {
        
        let tel = "\(Constants.tel_scheme):\(phone)"
        _ = URLOpener().openURL(tel)
    }
    
    func sendEmail(withEmail email: String) {
        
        _ = Tools.sendEmail(withEmail: email, mailComposeDelegate: self)
    }
    
    func launchMap(withDestinationCoordinate destinationCoordinate: CLLocationCoordinate2D, andOriginCoordinate originCoordinate: CLLocationCoordinate2D?) {
        
        Tools.launchGoogleMapInDirectionsModeWalking(withDestinationCoordinate: destinationCoordinate, andOriginCoordinate: originCoordinate)
    }
    
    func showError(error: ModelBO) { }
    
    func changeColorEditTexts() { }
}

extension StoresMapViewController: GMSMapViewDelegate {
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        
        if selectedMarker != marker {

            deselectMarker()
            animateMarker(marker: marker)
            
            if let displayData = selectedMarker?.userData as? MapDisplayDataItem {
                presenter.selectStore(withDisplayData: displayData)
            }
        }
        
        return false
    }
    
    func mapView(_ mapView: GMSMapView, didTapAt coordinate: CLLocationCoordinate2D) {
        presenter?.didTapInMap()
    }
}

extension StoresMapViewController: MFMailComposeViewControllerDelegate {
    
    public func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}
