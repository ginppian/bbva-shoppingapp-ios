//
//  ShoppingMapListPresenter.swift
//  shoppingapp
//
//  Created by Marcos on 27/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class ShoppingMapListPresenter<T: ShoppingMapListViewProtocol>: BasePresenter<T> {

    var promotions: [PromotionBO]?
    var userLocation: GeoLocationBO?
    var storeLocation: GeoLocationBO?
    var displayItems = [DisplayItemPromotion]()

    override init(busManager: BusManager) {
        super.init(busManager: busManager)
        routerTag = ShoppingMapListPageReaction.routerTag
    }

    required init() {
        super.init(tag: ShoppingMapListPageReaction.routerTag)
    }

    override func setModel(model: Model) {
        if let shoppingMapListTransport = model as? ShoppingMapListTransport {
            promotions = shoppingMapListTransport.promotions
            userLocation = shoppingMapListTransport.userLocation
            storeLocation = shoppingMapListTransport.storeLocation
        }
    }
}

extension ShoppingMapListPresenter: ShoppingMapListPresenterProtocol {

    func viewLoaded() {
        if let promotions = promotions {
            for promotion in promotions {
                let displayItemPromotion = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotion, withTypeCell: CellPromotionType.small)
                displayItems.append(displayItemPromotion)
            }
            view?.showPromotions(withDisplayItemsPromotions: displayItems)
        }
        if let location = userLocation, let storeLocation = storeLocation {
            let distance = DisplayItemPromotion.calculateDistanteBetween(storeLatitue: storeLocation.latitude, andStoreLongitude: storeLocation.longitude, withUserLatiude: location.latitude, andUserLongitude: location.longitude)
            if var distanceString = distance {
                distanceString = Localizables.promotions.key_from_distance_text + " " + distanceString
                view?.showDistance(distanceString)
            }
        }
    }
    
    func didTapInPromotionCell(atIndex index: Int) {
        
        if let promotion = promotions?[safeElement: index] {
            
            let promotionDetailTransport = PromotionDetailTransport()
            promotionDetailTransport.promotionBO = promotion
            
            promotionDetailTransport.userLocation = userLocation
            busManager.navigateScreen(tag: ShoppingMapListPageReaction.routerTag, ShoppingMapListPageReaction.eventNavToPromotionDetailScreen, promotionDetailTransport)
        }
    }
}
