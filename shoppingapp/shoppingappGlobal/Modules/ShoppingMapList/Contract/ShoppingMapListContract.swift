//
//  ShoppingMapListContract.swift
//  shoppingapp
//
//  Created by Marcos on 27/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol ShoppingMapListViewProtocol: ViewProtocol {
    func showPromotions(withDisplayItemsPromotions displayItemPromotions: [DisplayItemPromotion])
    func showDistance(_ distance: String)
}

protocol ShoppingMapListPresenterProtocol: PresenterProtocol {
    func viewLoaded()
    func didTapInPromotionCell(atIndex index: Int)
}
