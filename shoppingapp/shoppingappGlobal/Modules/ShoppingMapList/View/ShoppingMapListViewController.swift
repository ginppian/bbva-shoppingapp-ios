//
//  ShoppingMapListViewController.swift
//  shoppingapp
//
//  Created by Marcos on 27/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit
import CellsNativeCore
import CellsNativeComponents

class ShoppingMapListViewController: BaseViewController {

    // MARK: Constants

    static let ID: String = String(describing: ShoppingMapListViewController.self)

    fileprivate static let tableViewHeaderWithDistanceHeight: CGFloat = 60.0
    fileprivate static let tableViewHeaderHeight: CGFloat = 10.0
    fileprivate static let paddingTitleHeaderHeight: CGFloat = 20.0
    fileprivate static let heightTitleHeaderHeight: CGFloat = 20.0

    // MARK: Vars

    var presenter: ShoppingMapListPresenterProtocol!

    var displayItemPromotions: [DisplayItemPromotion]?

    @IBOutlet weak var promotionsTableView: UITableView!

    // MARK: - Life cycle

    override func viewDidLoad() {
        
        self.configure(appLifecycle: (UIApplication.shared.delegate as! AppDelegate).appLifeCycle!, id: ShoppingMapListViewController.ID)
        
        super.viewDidLoad()
        
        configureContentView()

        presenter.viewLoaded()

    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        setNavigationBarDefault()
        setNavigationTitle(withText: Localizables.promotions.key_view_all_title_text)
        setNavigationBackButtonDefault()
        
        trackScreen()

        BusManager.sessionDataInstance.disableSwipeLateralMenu()

    }
    
    override func viewWillAppearAfterDismiss() {
        
        trackScreen()
    }
    
    fileprivate func trackScreen() {
        
        TrackerHelper.sharedInstance().trackPromotionsMapSeeAll(withCustomerID: SessionDataManager.sessionDataInstance().customerID)
    }

    override func initModel(withModel model: Model) {
        presenter.setModel(model: model)
    }

    deinit {
        ComponentManager.remove(id: presenter.routerTag)
        DLog(message: "Deinitialized ShoppingMapListViewController")
    }

    // MARK: custom methods

    fileprivate func configureContentView() {

        promotionsTableView.backgroundColor = .BBVAWHITE
        promotionsTableView.register(UINib(nibName: PromotionSmallTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: PromotionSmallTableViewCell.identifier)

        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: ShoppingMapListViewController.tableViewHeaderHeight))
        headerView.backgroundColor = .BBVAWHITE
        promotionsTableView.tableHeaderView = headerView

        promotionsTableView.separatorStyle = .none

    }
}

extension ShoppingMapListViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        return PromotionSmallTableViewCell.height
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        presenter.didTapInPromotionCell(atIndex: indexPath.row)
    }
}

extension ShoppingMapListViewController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return displayItemPromotions?.count ?? 0
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if let displayItem = displayItemPromotions?[indexPath.row], let cell = tableView.dequeueReusableCell(withIdentifier: PromotionSmallTableViewCell.identifier, for: indexPath) as? PromotionSmallTableViewCell {

            cell.configureCellWithDisplayPromotionData(withDisplayitemPromotion: displayItem)

            cell.mAddSeparator.isHidden = (indexPath.row == 0)

            return cell
        }

        return UITableViewCell()
    }
}

extension ShoppingMapListViewController: ShoppingMapListViewProtocol {

    func showError(error: ModelBO) {
    }

    func changeColorEditTexts() {
    }

    func showDistance(_ distance: String) {

        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: ShoppingMapListViewController.tableViewHeaderWithDistanceHeight))
        headerView.backgroundColor = .BBVAWHITE

        let distanceTitleLabel = UILabel(frame: CGRect(x: ShoppingMapListViewController.paddingTitleHeaderHeight, y: (headerView.frame.size.height - ShoppingMapListViewController.heightTitleHeaderHeight) / 2, width: view.frame.size.width - (ShoppingMapListViewController.paddingTitleHeaderHeight * 2), height: ShoppingMapListViewController.heightTitleHeaderHeight))
        distanceTitleLabel.font = Tools.setFontBook(size: 16)
        distanceTitleLabel.textColor = .MEDIUMBLUE
        distanceTitleLabel.text = distance

        let headerSeparatorView = UIView(frame: CGRect(x: 0, y: ShoppingMapListViewController.tableViewHeaderWithDistanceHeight - 1, width: view.frame.size.width, height: 1))
        headerSeparatorView.backgroundColor = .BBVA200

        headerView.addSubview(distanceTitleLabel)
        headerView.addSubview(headerSeparatorView)

        promotionsTableView.tableHeaderView = headerView

    }

    func showPromotions(withDisplayItemsPromotions displayItemPromotions: [DisplayItemPromotion]) {

        self.displayItemPromotions = displayItemPromotions
        promotionsTableView.reloadData()

    }

}
