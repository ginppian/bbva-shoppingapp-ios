//
//  ShoppingMapListPageReaction.swift
//  shoppingapp
//
//  Created by Marcos on 27/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

class ShoppingMapListPageReaction: PageReaction {

    static let routerTag: String = "shopping-map-list-tag"
    static let eventNavToPromotionDetailScreen: ActionSpec<Model> = ActionSpec<Model>(id: "EVENT_NAV_TO_PROMOTION_DETAIL_SCREEN")

    override func configure() {
        
        navigateToPromotionDetailScreen()
    }
    
    func navigateToPromotionDetailScreen() {
        
        _ = when(id: ShoppingMapListPageReaction.routerTag, type: Model.self)
            .doAction(actionSpec: ShoppingMapListPageReaction.eventNavToPromotionDetailScreen)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, model in
                if let model = model {
                    router.navigateToShoppingDetail(withModel: model)
                }
        }
    }
}
