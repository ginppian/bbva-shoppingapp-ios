//
//  EditLimitPageReaction.swift
//  shoppingapp
//
//  Created by Marcos on 22/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

class EditLimitPageReaction: PageReaction {

    static let ROUTER_TAG: String = "edit-limits-tag"

    static let EVENT_NAV_TO_LIMITS: ActionSpec<Model> = ActionSpec<Model>(id: "EVENT_NAV_TO_LIMITS")

    override func configure() {

        navigateLimitsFromEditLimits()
    }

    func navigateLimitsFromEditLimits() {

        _ = when(id: EditLimitPageReaction.ROUTER_TAG, type: Model.self)
            .doAction(actionSpec: EditLimitPageReaction.EVENT_NAV_TO_LIMITS)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, model in

                router.navigateLimitsFromEditLimitsPop(withModel: model!)

            }
    }

}
