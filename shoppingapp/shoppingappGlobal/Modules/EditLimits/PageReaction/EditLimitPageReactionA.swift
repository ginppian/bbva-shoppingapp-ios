//
//  EditLimitPageReactionA.swift
//  shoppingappMX
//
//  Created by Magdali Grajales on 13/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

class EditLimitPageReactionA: EditLimitPageReaction {
    
    static let eventNavToEditHelpLimits: ActionSpec<Model> = ActionSpec<Model>(id: "EVENT_NAV_TO_EDIT_HELP_LIMITS")

    override func configure() {
        super.configure()
        
        navigateToEditLimitsHelp()
    }
    
    func navigateToEditLimitsHelp() {
        
        _ = when(id: EditLimitPageReactionA.ROUTER_TAG, type: Model.self)
            .doAction(actionSpec: EditLimitPageReactionA.eventNavToEditHelpLimits)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, model in
                if let model = model {
                    router.navigateToEditLimitsHelp(with: model)
                }
        }
    }
}
