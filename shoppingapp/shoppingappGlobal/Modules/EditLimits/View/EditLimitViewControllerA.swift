//
//  EditLimitViewControllerA.swift
//  shoppingappMX
//
//  Created by Magdali Grajales on 15/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class EditLimitViewControllerA: EditLimitViewController {
    
}

extension EditLimitViewControllerA: EditLimitViewProtocolA {
    
    func doEnterAnimationLayout(showKeyboard show: Bool) {
        
        BusManager.sessionDataInstance.hideWebview(false)
        
        let animationTimeSteep: TimeInterval = 0.5
        headerViewTopConstraint.constant = 0
        
        UIView.animate(withDuration: animationTimeSteep, animations: {
            self.headerView.alpha = 1
            
            self.view.layoutIfNeeded()
            
        }, completion: { _ in
            UIView.animate(withDuration: animationTimeSteep, animations: {
                
                self.amountTextField.alpha = 1
                self.infoView.alpha = 1
                self.confirmButton.alpha = 1
                
                self.amountViewTopConstraint.constant = EditLimitViewController.amountViewTopConstraintConstant
                self.amountViewLeftConstraint.constant = EditLimitViewController.amountViewLeftConstraintConstant
                self.amountViewRightConstraint.constant = EditLimitViewController.amountViewRightConstraintConstant
                self.confirmButtonWidthConstraint.constant = EditLimitViewController.confirmButtonWidthConstraintConstant
                self.view.layoutIfNeeded()
                
            }, completion: { _ in
                 if show {
                    self.amountTextField.textfield.becomeFirstResponder()
                 }
            })
        })
    }

}
