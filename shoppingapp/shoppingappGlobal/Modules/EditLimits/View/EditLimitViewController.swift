//
//  EditLimitViewController.swift
//  shoppingapp
//
//  Created by Marcos on 22/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit
import CellsNativeCore
import CellsNativeComponents

class EditLimitViewController: BaseViewController {

    // MARK: Constants

    static let ID: String = String(describing: EditLimitViewController.self)

    // MARK: Vars

    var presenter: EditLimitsPresenterProtocol!

    // MARK: IBOutlet
    @IBOutlet weak var headerViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var amountViewTopConstraint: NSLayoutConstraint!
    @IBOutlet weak var amountViewLeftConstraint: NSLayoutConstraint!
    @IBOutlet weak var amountViewRightConstraint: NSLayoutConstraint!
    @IBOutlet weak var confirmButtonWidthConstraint: NSLayoutConstraint!

    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var consumedAmountLabel: UILabel!
    @IBOutlet weak var consumedTitleLabel: UILabel!

    @IBOutlet weak var currentLimitAmountLabel: UILabel!
    @IBOutlet weak var currentLimiTitleLabel: UILabel!

    @IBOutlet weak var amountTextField: AmountTextfield!

    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var infoImageView: UIImageView!
    @IBOutlet weak var infoLabel: UILabel!

    @IBOutlet weak var confirmButton: UIButton!

    // MARK: - Constants

    static let amountViewTopConstraintConstantBeforeAnimation: CGFloat = 100
    static let amountViewLeftConstraintConstantBeforeAnimation: CGFloat = 100
    static let amountViewRightConstraintConstantBeforeAnimation: CGFloat = 100
    static let confirmButtonWidthConstraintConstantBeforeAnimation: CGFloat = 50

    static let amountViewTopConstraintConstant: CGFloat = 30
    static let amountViewLeftConstraintConstant: CGFloat = 30
    static let amountViewRightConstraintConstant: CGFloat = 30
    static let confirmButtonWidthConstraintConstant: CGFloat = 190

    // MARK: - Life cycle

    override func viewDidLoad() {
        self.configure(appLifecycle: (UIApplication.shared.delegate as! AppDelegate).appLifeCycle!, id: EditLimitViewController.ID)
        super.viewDidLoad()

        configureContentView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        configureNavigationBar()

        presenter.viewWillAppear()

        BusManager.sessionDataInstance.disableSwipeLateralMenu()
        BusManager.sessionDataInstance.hideWebview(true)

    }
    
    override func viewWillAppearAfterDismiss() {
        
        presenter.viewWillAppearAfterDismiss()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        presenter.viewDidAppear()
    }

    override func initModel(withModel model: Model) {
        presenter.setModel(model: model)
    }

    deinit {
        ComponentManager.remove(id: presenter.routerTag)
        DLog(message: "Deinitialized EditLimitViewController")
    }

    // MARK: - Setup views

    func configureContentView() {

        view.backgroundColor = .BBVA100

        consumedAmountLabel.textColor = .DARKAQUA
        consumedTitleLabel.textColor = .DARKAQUA
        currentLimitAmountLabel.textColor = .BBVA600
        currentLimiTitleLabel.textColor = .BBVA600

        currentLimiTitleLabel.font = Tools.setFontBook(size: 14)
        consumedTitleLabel.font = Tools.setFontBook(size: 14)

        amountTextField.delegate = self
        amountTextField.textfield.addTarget(self, action: #selector(textFieldDidChange(_:)), for: .editingChanged)

        infoImageView.image = SVGCache.image(forSVGNamed: ConstantsImages.Common.info_icon, color: .BBVALIGHTBLUE)
        infoLabel.textColor = .BBVA500
        infoLabel.font = Tools.setFontBook(size: 14)

        confirmButton.setTitle(Localizables.common.key_confirm_text, for: .normal)

        confirmButton.isEnabled = true
        confirmButton.setBackgroundColor(color: .MEDIUMBLUE, forUIControlState: .normal)
        confirmButton.setBackgroundColor(color: .COREBLUE, forUIControlState: .highlighted)
        confirmButton.setBackgroundColor(color: .BBVA200, forUIControlState: .disabled)
        confirmButton.setTitleColor(.BBVAWHITE, for: .normal)
        confirmButton.titleLabel?.font = Tools.setFontBold(size: 14)

    }

    func configureNavigationBar() {

        setNavigationBarDefault()

        let image = SVGCache.image(forSVGNamed: ConstantsImages.Common.close_icon, color: .BBVAWHITE)
        setNavigationLeftButton(withImage: image, action: #selector(backAction))

    }

    // MARK: - User Actions
    @IBAction func confirmButtonAction(_ sender: Any) {
        amountTextField.textfield.resignFirstResponder()
        presenter.confirmButtonPressed()
    }

    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationControllerOperation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {

        switch operation {
            
        case .pop:
            
            if fromVC as? EditLimitViewController != nil {
                return PopAnimatorModal()
            } else {
                return nil
            }
            
        default:
            return nil
        }
    }

}

// MARK: UITextFieldDelegate

extension EditLimitViewController: UITextFieldDelegate {

    @objc func textFieldDidChange(_ textField: UITextField) {

        let text = textField.text ?? ""
        presenter.textFieldDidChange(withText: text)

    }

}

extension EditLimitViewController: EditLimitsViewProtocol {

    func showError(error: ModelBO) {
        presenter.showModal()
    }

    func changeColorEditTexts() {

    }

    func sendScreenOmniture(withLimitName limitName: String) {
        TrackerHelper.sharedInstance().trackEditLimitScreen(forLimitName: limitName, andCustomerID: SessionDataManager.sessionDataInstance().customerID)
    }

    func showLimit(withDisplayData displayData: LimitDisplayData) {

        consumedAmountLabel.attributedText = displayData.consumedAmount
        currentLimitAmountLabel.attributedText = displayData.currentLimitAmount

        consumedTitleLabel.text = displayData.consumedLimitTitle
        currentLimiTitleLabel.text = displayData.currentLimiTitle

        infoLabel.text = displayData.infoLimit
    }

    func showEditLimit(withDisplayData displayData: AmountTextfieldDisplayData) {
        amountTextField.presenter.displayData = displayData
    }

    func showNavigation(withTitle title: String) {
        setNavigationTitle(withText: title)
    }

    func prepareForAnimation() {
        BusManager.sessionDataInstance.hideWebview(true)

        headerView.alpha = 0
        amountTextField.alpha = 0
        infoView.alpha = 0
        confirmButton.alpha = 0
        headerViewTopConstraint.constant = self.view.frame.height / 2
        amountViewTopConstraint.constant = EditLimitViewController.amountViewTopConstraintConstantBeforeAnimation
        amountViewLeftConstraint.constant = EditLimitViewController.amountViewLeftConstraintConstantBeforeAnimation
        amountViewRightConstraint.constant = EditLimitViewController.amountViewRightConstraintConstantBeforeAnimation
        confirmButtonWidthConstraint.constant = EditLimitViewController.confirmButtonWidthConstraintConstantBeforeAnimation

    }

    func doEnterAnimationLayout() {

        BusManager.sessionDataInstance.hideWebview(false)

        let animationTimeSteep: TimeInterval = 0.5
        headerViewTopConstraint.constant = 0

        UIView.animate(withDuration: animationTimeSteep, animations: {
            self.headerView.alpha = 1

            self.view.layoutIfNeeded()

        }, completion: { _ in
            UIView.animate(withDuration: animationTimeSteep, animations: {

                self.amountTextField.alpha = 1
                self.infoView.alpha = 1
                self.confirmButton.alpha = 1

                self.amountViewTopConstraint.constant = EditLimitViewController.amountViewTopConstraintConstant
                self.amountViewLeftConstraint.constant = EditLimitViewController.amountViewLeftConstraintConstant
                self.amountViewRightConstraint.constant = EditLimitViewController.amountViewRightConstraintConstant
                self.confirmButtonWidthConstraint.constant = EditLimitViewController.confirmButtonWidthConstraintConstant
                self.view.layoutIfNeeded()

            }, completion: { _ in
                self.amountTextField.textfield.becomeFirstResponder()

            })
        })
    }

    func showErrorMessageAlreadyConsumed(withMessage message: String) {

        consumedAmountLabel.textColor = .BBVADARKRED
        consumedTitleLabel.textColor = .BBVADARKRED

        amountTextField.showError()

        infoImageView.image = SVGCache.image(forSVGNamed: ConstantsImages.Common.alert_icon, color: .BBVADARKRED)
        infoLabel.text = message
    }

    func showErrorMessage(withMessage message: String) {

        consumedAmountLabel.textColor = .DARKAQUA
        consumedTitleLabel.textColor = .DARKAQUA

        amountTextField.showError()

        infoImageView.image = SVGCache.image(forSVGNamed: ConstantsImages.Common.alert_icon, color: .BBVADARKRED)
        infoLabel.text = message
    }

    func showSuccessMessage(withMessage message: String) {

        consumedAmountLabel.textColor = .DARKAQUA
        consumedTitleLabel.textColor = .DARKAQUA

        amountTextField.hideError()

        infoImageView.image = SVGCache.image(forSVGNamed: ConstantsImages.Common.info_icon, color: .BBVALIGHTBLUE)
        infoLabel.text = message
    }

}

extension EditLimitViewController: AmountTextfieldDelegate {

    func amountTextfield(_ amountTextfield: AmountTextfield, didEndEditing amount: NSDecimalNumber) {
        presenter.updateLimitText(withAmount: amount)
    }

}

extension EditLimitViewController: CloseModalDelegate {

    func errorViewControllerAcceptPressed(_ errorViewController: ErrorViewController) {
        presenter.checkError()
    }

    func errorViewControllerCancelPressed(_ errorViewController: ErrorViewController) {
    }
}
