//
//  EditLimitPresenterA.swift
//  shoppingappMX
//
//  Created by Magdali Grajales on 13/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class EditLimitPresenterA<T: EditLimitViewProtocolA>: EditLimitsPresenter<T> {
    
    var cardType: CardTypeBO?
    var preferences = PreferencesManager.sharedInstance()

    override func setModel(model: Model) {
        if let limitTransport = model as? LimitTransport {
            limitBO = limitTransport.limit
            cardId = limitTransport.cardId
            cardType = limitTransport.cardType
        }
    }
   
    override func viewWillAppear() {

        guard let limitBO = limitBO, let amountCurrencyLimit = limitBO.amountLimits?.first as? AmountCurrencyBO else {
            return
        }
        displayLimit = LimitDisplayData.generateDisplayDataForEdit(fromLimit: limitBO)
        
        let infoLimit = setInfoMessage(with: limitBO)
        displayLimit?.infoLimit = infoLimit
        
        guard let displayLimit = displayLimit else {
            return
        }
        
        let title = displayLimit.title
       
        view?.showNavigation(withTitle: title)
        view?.sendScreenOmniture(withLimitName: displayLimit.tagName)
        view?.showLimit(withDisplayData: displayLimit)
        view?.showEditLimit(withDisplayData: AmountTextfieldDisplayData(placeHolder: Localizables.cards.key_card_daily_limit_multiples, initialAmount: amountCurrencyLimit.amount.stringValue, currency: Currencies.currency(forCode: amountCurrencyLimit.currency), shouldHideErrorAutomatically: false))
        view?.prepareForAnimation()
    }
    
    override func viewDidAppear() {
        
        guard let cardType = cardType else {
            return
        }
        
        if (cardType.id == .credit_card && preferences.isFirstTimeCreditShowLimitsHelp()) ||
            (cardType.id == .debit_card && preferences.isFirstTimeDebitShowLimitsHelp()) {
            
            view?.doEnterAnimationLayout(showKeyboard: false)
            busManager.navigateScreen(tag: routerTag, EditLimitPageReactionA.eventNavToEditHelpLimits, cardType)
            
        } else {
            super.viewDidAppear()
        }
    }
    
    override func viewWillAppearAfterDismiss() {
        super.viewWillAppearAfterDismiss()
        
        if !isShowLoading {
            view?.doEnterAnimationLayout(showKeyboard: true)
        }
    }
    
    override func setInfoMessage(with limitBO: LimitBO) -> String {
        
        let withMessage = limitBO.infoMessageMultiplesAndMaximum()
        return withMessage
    }
}
