//
//  EditLimitPresenter.swift
//  shoppingapp
//
//  Created by Marcos on 22/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import RxSwift
import CellsNativeComponents

class EditLimitsPresenter<T: EditLimitsViewProtocol>: BasePresenter<T> {

    let disposeBag = DisposeBag()

    var interactor: EditLimitsInteractorProtocol?

    var cardId: String?
    var limitBO: LimitBO?
    var displayLimit: LimitDisplayData?

    var limitAmount: NSDecimalNumber?

    var multipleLimit = Settings.Limits.multiples_limit_amount

    var limitIsEditable = true

    required init() {
        super.init(tag: EditLimitPageReaction.ROUTER_TAG)
    }

    override init(busManager: BusManager) {
        super.init(busManager: busManager)
        routerTag = EditLimitPageReaction.ROUTER_TAG
    }

    override func setModel(model: Model) {
        if let limitTransport = model as? LimitTransport {
            limitBO = limitTransport.limit
            cardId = limitTransport.cardId
        }
    }

    func navigateToLimitScreen(withTransport transport: LimitTransport) {
        busManager.navigateScreen(tag: routerTag, EditLimitPageReaction.EVENT_NAV_TO_LIMITS, transport)
    }

    deinit {
        DLog(message: "EditLimitsPresenter deinit")
    }
    
    func viewDidAppear() {
        view?.doEnterAnimationLayout()
    }
    
    func viewWillAppear() {

        guard let limitBO = limitBO, let amountCurrencyLimit = limitBO.amountLimits?.first as? AmountCurrencyBO else {
            return
        }

        displayLimit = LimitDisplayData.generateDisplayDataForEdit(fromLimit: limitBO)

        guard let displayLimit = displayLimit else {
            return
        }

        view?.showNavigation(withTitle: displayLimit.title)
        view?.sendScreenOmniture(withLimitName: displayLimit.tagName)
        view?.showLimit(withDisplayData: displayLimit)
        view?.showEditLimit(withDisplayData: AmountTextfieldDisplayData(placeHolder: limitBO.type.inputAmountLimitTitle, initialAmount: amountCurrencyLimit.amount.stringValue, currency: Currencies.currency(forCode: amountCurrencyLimit.currency), shouldHideErrorAutomatically: false))
        view?.prepareForAnimation()
    }
    
    func viewWillAppearAfterDismiss() {
        
        if let displayLimit = displayLimit {
            view?.sendScreenOmniture(withLimitName: displayLimit.tagName)
        }
    }
    
    func setInfoMessage(with limitBO: LimitBO) -> String {
        let withMessage = limitBO.infoMessage()
        return withMessage
    }
    
}

extension EditLimitsPresenter: EditLimitsPresenterProtocol {
   
    func confirmButtonPressed() {

        guard let limitBO = limitBO, let cardId = cardId, let limitAmount = limitAmount, limitIsEditable else {
            return
        }

        if limitAmount != .notANumber, let amountCurrency = limitBO.amountLimits?.first as? AmountCurrencyBO {

            showLoading()
            
            guard let newLimit = AmountCurrencyBO(amountCurrency: AmountCurrencyEntity(amount: limitAmount.doubleValue, currency: amountCurrency.currency)) else {
                return
            }

            self.interactor?.udapteLimits(forCardId: cardId, forLimitId: limitBO.type.rawValue, withNewLimit: newLimit).subscribe(
                onNext: { [weak self] _ in
                    
                    amountCurrency.amount = limitAmount
                    let limitTransport = LimitTransport(withLimit: limitBO, andCardId: cardId)
                    self?.navigateToLimitScreen(withTransport: limitTransport)
                    self?.hideLoading()
                }, onError: {  [weak self] error in
                    if let evaluate = error as? ServiceError {
                        switch evaluate {
                        case .GenericErrorBO(let errorBO):
                            self?.errorBO = errorBO
                        default:
                            break
                        }
                    }

                    self?.hideLoading(completion: {
                        self?.checkError()
                    })

                }).addDisposableTo(self.disposeBag)
        }
    }

    func updateLimitText(withAmount amount: NSDecimalNumber) {

        if amount != .notANumber {
            limitAmount = amount
        }
    }

    func textFieldDidChange(withText text: String) {

        guard let limitBO = limitBO else {
            return
        }

        let amountFormatter = limitBO.amountFormatterForLimit()

        let inputText = NSDecimalNumber(string: text)
        let minimumLimit = limitBO.minimumLimitDecimalNumber()
        let maximumLimit = limitBO.maximumLimitDecimalNumber()
        let maximumLimitFormatter = amountFormatter.format(amount: maximumLimit)

        let consumedAmount = limitBO.consumedLimitAmount()
        let minimumAmount = limitBO.minimumLimitAmount()

        var message = setInfoMessage(with: limitBO)

        if text.isEmpty {

            limitIsEditable = true
            view?.showSuccessMessage(withMessage: message)
            return
        }

        //If inputText is less than minimumLimit
        if inputText.compare(minimumLimit) == .orderedAscending {
            //Min
            if minimumLimit.compare(consumedAmount) == .orderedSame {
                //Already consumed
                limitIsEditable = false
                message = limitBO.messageAlreadyConsumed()
                view?.showErrorMessageAlreadyConsumed(withMessage: message)
                return

            } else if minimumLimit.compare(minimumAmount) == .orderedSame {

                limitIsEditable = false
                message = limitBO.messageMinimum()
                view?.showErrorMessage(withMessage: message)
                return
            }

        } else if inputText.compare(maximumLimit) == .orderedDescending {
            //Max
            limitIsEditable = false
            message = Localizables.cards.key_card_limit_max_text + " \(maximumLimitFormatter)"
            view?.showErrorMessage(withMessage: message)
            return
        }

        let multiple = NSDecimalNumber(string: "\(multipleLimit)")
        let multipleFormatter = amountFormatter.format(amount: multiple)

        if inputText.doubleValue > multiple.doubleValue, inputText.doubleValue.truncatingRemainder(dividingBy: multiple.doubleValue) != 0 {

            limitIsEditable = false
            message = Localizables.cards.key_card_only_multiples + " \(multipleFormatter)"
            view?.showErrorMessage(withMessage: message)
            return

        } else if inputText.doubleValue < multiple.doubleValue, multiple.doubleValue.truncatingRemainder(dividingBy: inputText.doubleValue) != 0 {

            limitIsEditable = false
            message = Localizables.cards.key_card_only_multiples + " \(multipleFormatter)"
            view?.showErrorMessage(withMessage: message)
            return
        }

        limitIsEditable = true
        view?.showSuccessMessage(withMessage: message)
        return
    }
}
