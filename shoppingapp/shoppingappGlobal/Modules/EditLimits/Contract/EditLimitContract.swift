//
//  LimitsDetailContract.swift
//  shoppingapp
//
//  Created by Marcos on 22/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import RxSwift

protocol EditLimitsViewProtocol: ViewProtocol {

    func sendScreenOmniture(withLimitName limitName: String)
    func showNavigation(withTitle title: String)
    func prepareForAnimation()
    func doEnterAnimationLayout()
    func showLimit(withDisplayData displayData: LimitDisplayData)
    func showEditLimit(withDisplayData displayData: AmountTextfieldDisplayData)
    func showErrorMessageAlreadyConsumed(withMessage message: String)
    func showErrorMessage(withMessage message: String)
    func showSuccessMessage(withMessage message: String)

}

protocol EditLimitsPresenterProtocol: PresenterProtocol {
    func viewWillAppear()
    func viewWillAppearAfterDismiss()
    func viewDidAppear()
    func updateLimitText(withAmount amount: NSDecimalNumber)
    func confirmButtonPressed()
    func textFieldDidChange(withText text: String)
}

protocol EditLimitsInteractorProtocol {
    func udapteLimits(forCardId cardId: String, forLimitId limitId: String, withNewLimit newLimit: AmountCurrencyBO) -> Observable <ModelBO>
}
