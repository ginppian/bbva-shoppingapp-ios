//
//  EditLimitContractA.swift
//  shoppingappMX
//
//  Created by Magdali Grajales on 15/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol EditLimitViewProtocolA: EditLimitsViewProtocol {
    
    func doEnterAnimationLayout(showKeyboard show: Bool)
    
}
