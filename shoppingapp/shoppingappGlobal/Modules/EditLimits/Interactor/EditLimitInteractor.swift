//
//  EditLimitInteractor.swift
//  shoppingapp
//
//  Created by Marcos on 22/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import RxSwift

class EditLimitInteractor {
    let disposeBag = DisposeBag()

    deinit {
        DLog(message: "EditLimitInteractor deinit")
    }

}

extension EditLimitInteractor: EditLimitsInteractorProtocol {

    func udapteLimits(forCardId cardId: String, forLimitId limitId: String, withNewLimit newLimit: AmountCurrencyBO) -> Observable <ModelBO> {

        return Observable.create { observer in

            LimitsDataManager.sharedInstance.updateLimit(forCardId: cardId, withLimitId: limitId, withAmountLimit: newLimit).subscribe(
                onNext: { result in

                    if let operationResultEntity = result as? OperationResultEntity {
                        observer.onNext(OperationResultBO(entity: operationResultEntity))
                        observer.onCompleted()
                    } else {
                        DLog(message: "Incorrect OperationResultEntity")
                    }

                },
                onError: { error in

                    let evaluate: ServiceError = (error as? ServiceError)!

                    switch evaluate {

                    case .GenericErrorEntity(let errorEntity):
                        let errorBO = ErrorBO(error: errorEntity)
                        observer.onError(ServiceError.GenericErrorBO(error: errorBO))
                    default:
                        observer.onError(evaluate)
                    }
                }, onCompleted: {
                }).addDisposableTo(self.disposeBag)
            return Disposables.create()
        }

    }

}
