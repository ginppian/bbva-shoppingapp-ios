//
//  LimitsHelpViewController.swift
//  shoppingappMX
//
//  Created by Magdali Grajales on 22/10/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit

class EditLimitHelpViewController: BaseViewController {

    static let id: String = String(describing: EditLimitHelpViewController.self)
    
    let bulletLabelInfoViewRect = CGRect(x: 0, y: 0, width: 200.0, height: 20.0)
    let lightBlueInfoViewRect = CGRect(x: 0, y: 0, width: 200.0, height: 20.0)

    var presenter: EditLimitHelpPresenterProtocol!

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var contentView: UIView!
    
    @IBOutlet weak var headerImageView: UIImageView!
    @IBOutlet weak var titleOneLabel: UILabel!
    @IBOutlet weak var stackOne: UIStackView!
    @IBOutlet weak var stackTwo: UIStackView!
    
    @IBOutlet weak var acceptButton: UIButton!
    
    // MARK: - View life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        configureView()
        
        presenter?.viewLoaded()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        configureNavigationBar()
        
        BusManager.sessionDataInstance.disableSwipeLateralMenu()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        scrollView.flashScrollIndicators()
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        updateShadowView()
    }
    
    override func initModel(withModel model: Model) {
        presenter.setModel(model: model)
    }

    // MARK: - Custom methods
    
    func configureNavigationBar() {
        
        setNavigationBarDefault()
        
        let image = SVGCache.image(forSVGNamed: ConstantsImages.Common.close_icon, color: .BBVAWHITE)
        setNavigationLeftButton(withImage: image, action: #selector(dismissAction))
        
        setNavigationTitle(withText: Localizables.cards.key_cards_limits_text)
    }
    
    func configureView() {
        
        titleOneLabel.font = Tools.setFontMedium(size: 14)
        titleOneLabel.textColor = .BBVA600
        
        acceptButton.backgroundColor = .MEDIUMBLUE
        acceptButton.titleLabel?.font = Tools.setFontBold(size: 14)
        acceptButton.setTitleColor(.BBVAWHITE, for: .normal)
    }
    
    func updateShadowView() {
        if contentView.bounds.height > scrollView.bounds.height {
            
            acceptButton.superview?.layer.shadowColor = UIColor.black.cgColor
            acceptButton.superview?.layer.shadowOpacity = 0.5
            acceptButton.superview?.layer.shadowOffset = CGSize.zero
            acceptButton.superview?.layer.shadowRadius = 2
        } else {
            
            acceptButton.superview?.layer.shadowOpacity = 0
        }
    }
    
    // MARK: - IBAction
    
    @IBAction func acceptButtonPressed(_ sender: Any) {
        
        BusManager.sessionDataInstance.dismissViewController(animated: true)
    }
}

extension EditLimitHelpViewController: EditLimitHelpViewProtocol {
    
    func showError(error: ModelBO) {
    }
    
    func changeColorEditTexts() {
    }
    
    func showHelpInformation(displayData: EditLimitHelpDisplayData) {
        
        titleOneLabel.text = displayData.titleOne
        
        for text in displayData.textsOne {
            
            let view = BulletLabelInfoView(frame: bulletLabelInfoViewRect)
            view.translatesAutoresizingMaskIntoConstraints = false
            view.configureWithTitle(text, bulletImageName: displayData.bulletImageName)
            stackOne.addArrangedSubview (view)
        }
        
        if let disclaimerText = displayData.disclaimerText, !disclaimerText.isEmpty {
            
            let view = LightBlueInfoView(frame: lightBlueInfoViewRect)
            view.translatesAutoresizingMaskIntoConstraints = false
            view.configure(text: disclaimerText)
            stackTwo.addArrangedSubview(view)
        }
        
        acceptButton.setTitle(displayData.acceptButtonTitle, for: .normal)
        
        headerImageView.image = UIImage(named: displayData.headerImageName)
        
        updateShadowView()
    }
}
