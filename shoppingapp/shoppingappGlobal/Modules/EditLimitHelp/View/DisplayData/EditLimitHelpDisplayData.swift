//
//  EditLimitHelpDisplayData.swift
//  shoppingappMX
//
//  Created by Magdali Grajales on 24/10/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

struct EditLimitHelpDisplayData: Equatable {
    
    let headerImageName: String
    let bulletImageName: String
    var titleOne: String
    var textsOne: [String]
    let acceptButtonTitle: String
    var disclaimerText: String?
    var titleOneKey: String
    
    init(cardType: CardType) {
        
        let keyRoot = "KEY_CARD_HELP_LIMITS_"
        textsOne = Tools.localizedStringsForKeyAndStartIndex(key: keyRoot + "TEXT_", startIndex: 1)
        headerImageName = ConstantsImages.Card.ilu_limits_help
        bulletImageName = ConstantsImages.Common.bullet_icon
        acceptButtonTitle = Localizables.login.key_login_disconnect_understood_text
        
        if cardType == CardType.credit_card {
            
            titleOneKey = keyRoot + "CREDIT_CARD_TITLE_1"
            titleOne = NSLocalizedString(titleOneKey, comment: "")
            disclaimerText = ""
            
        } else {
            
            titleOneKey = keyRoot + "TITLE_1"
            titleOne = NSLocalizedString(titleOneKey, comment: "")
            disclaimerText = Localizables.cards.key_card_disclaimer_help_limits
        }
    }
}
