//
//  LimitsHelpContract.swift
//  shoppingappMX
//
//  Created by Magdali Grajales on 24/10/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol EditLimitHelpViewProtocol: ViewProtocol {
    func showHelpInformation(displayData: EditLimitHelpDisplayData)
    
}

protocol EditLimitHelpPresenterProtocol: PresenterProtocol {
    func viewLoaded()
}
