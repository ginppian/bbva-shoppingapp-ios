//
//  LimitsHelpPresenter.swift
//  shoppingappMX
//
//  Created by Magdali Grajales on 08/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit

class EditLimitHelpPresenter<T: EditLimitHelpViewProtocol>: BasePresenter<T> {
    
    let routerTagValue = "edit-limit-help-tag"
    var cardType: CardType? = CardType.unknown
    var displayData: EditLimitHelpDisplayData?
    
    required init() {
        super.init(tag: routerTagValue)
    }
    
    override init(busManager: BusManager) {
        super.init(busManager: busManager)
        routerTag = routerTagValue
    }
    
    override func setModel(model: Model) {
        if let cardTypeBO = model as? CardTypeBO {
            cardType = cardTypeBO.id
        }
    }
}

extension EditLimitHelpPresenter: EditLimitHelpPresenterProtocol {
    
    func viewLoaded() {
        
        if let cardType = cardType {

            displayData = EditLimitHelpDisplayData(cardType: cardType)
            view?.showHelpInformation(displayData: displayData!)
        }
    }
}
