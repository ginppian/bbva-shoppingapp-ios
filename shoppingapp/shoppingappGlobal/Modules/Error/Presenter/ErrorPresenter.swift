//
//  ErrorPresenter.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 6/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation

class ErrorPresenter<T: ErrorViewProtocol>: BasePresenter<T>, ErrorPresenterProtocol {

    let EVENT_ERROR_CONTENT_BO = "error-content-bind"
    let ROUTER_TAG: String = "error"

    required init() {
        super.init(tag: ROUTER_TAG)
    }

    override init(busManager: BusManager) {
        super.init(busManager: busManager)
        routerTag = "error"
    }

    func didLoad() {
        
        view?.configureView()
        view?.showMessageError(error: ErrorDisplay(error: errorBO))
    }
}
