//
//  ErrorViewController.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 30/6/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit
import CellsNativeCore
import CellsNativeComponents

protocol CloseModalDelegate: class {
    func errorViewControllerAcceptPressed(_ errorViewController: ErrorViewController)
    func errorViewControllerCancelPressed(_ errorViewController: ErrorViewController)
}

class ErrorViewController: BaseViewController {

    var presenter: ErrorPresenter<ErrorViewController>!

    weak var delegate: CloseModalDelegate?
    
    private var errorDisplay: ErrorDisplay?
    private var layoutFinished = false

    @IBOutlet var errorViewContainer: UIView!

    @IBOutlet var acceptButton: UIButton!
    @IBOutlet weak var cancelButton: UIButton!

    @IBOutlet var errorLabel: UILabel!
    @IBOutlet var errorCodeLabel: UILabel!

    @IBOutlet var warningImageView: UIImageView!
    @IBOutlet weak var errorLabelToButtonsVerticalConstraint: NSLayoutConstraint!
    @IBOutlet var errorCodeLabelToButtonsVerticalConstraint: NSLayoutConstraint!

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        BusManager.sessionDataInstance.disableSwipeLateralMenu()
    }

    override func viewDidLoad() {

        super.viewDidLoad()
        presenter.didLoad()
    }
    
    override func viewDidLayoutSubviews() {
        
        super.viewDidLayoutSubviews()
        
        if errorDisplay != nil && !layoutFinished {
            updateErrorCode()
        }
        layoutFinished = true
    }

    func setUpView() {
        
        setNavigationBarTransparent()
        navigationItem.setHidesBackButton(true, animated: false)
        view.backgroundColor = UIColor.NAVY.withAlphaComponent(0.9)
        errorViewContainer.backgroundColor = .BBVAWHITE
        errorLabel.font = Tools.setFontBook(size: 14)
        errorLabel.textColor = .BBVA600

        errorCodeLabel.font = Tools.setFontBook(size: 14)
        errorCodeLabel.textColor = .BBVA500
        errorCodeLabel.adjustsFontSizeToFitWidth = false

        acceptButton.backgroundColor = .MEDIUMBLUE
        acceptButton.titleLabel?.font = Tools.setFontBold(size: 14)

        acceptButton.setBackgroundColor(color: .MEDIUMBLUE, forUIControlState: .normal)
        acceptButton.setBackgroundColor(color: .DARKMEDIUMBLUE, forUIControlState: .highlighted)

        cancelButton.setTitle(Localizables.common.key_common_cancel_text, for: .normal)
        cancelButton.titleLabel?.font = Tools.setFontMedium(size: 14)
        cancelButton.setTitleColor(.MEDIUMBLUE, for: .normal)
        cancelButton.setTitleColor(.COREBLUE, for: .highlighted)
        cancelButton.setTitleColor(.COREBLUE, for: .selected)

        warningImageView.image = ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Common.alert_icon, color: .BBVALIGHTRED), width: 22, height: 22)
    }
    
    func updateView() {
        
        guard let error = errorDisplay else {
            return
        }
        
        if let message = error.message {
            errorLabel.text = message
        } else {
            errorLabel.attributedText = error.messageAttributed
        }
        
        if layoutFinished {
            updateErrorCode()
        }
        
        cancelButton.isHidden = !error.showCancel
        
        warningImageView.image = ImageUtilities.image(with: SVGCache.image(forSVGNamed: error.iconNamed, color: error.iconColor), width: 22, height: 22)
        acceptButton.setTitle(error.okTitle, for: .normal)
    }
    
    func updateErrorCode() {
        
        if let errorCodeMessage = errorDisplay?.codeMessage {
            
            errorCodeLabelToButtonsVerticalConstraint.constant = 30
            NSLayoutConstraint.deactivate([errorLabelToButtonsVerticalConstraint])
            errorCodeLabel.isHidden = false
            
            let errorCodeTextMessage = Localizables.common.key_generic_error_code + " " + errorCodeMessage
            let errorCodeTextMessageHeight = errorCodeTextMessage.height(withConstrainedWidth: errorCodeLabel.bounds.width, font: errorCodeLabel.font)
            
            if errorCodeTextMessageHeight > errorCodeLabel.bounds.height {
                
                errorCodeLabel.text = Localizables.common.key_generic_error_code + "\n" + errorCodeMessage
            } else {
                
                errorCodeLabel.text = errorCodeTextMessage
            }
        } else {
            
            NSLayoutConstraint.deactivate([errorCodeLabelToButtonsVerticalConstraint])
            errorCodeLabel.isHidden = true
            
            errorCodeLabel.text = ""
        }
    }

    @IBAction func accceptAction(_ sender: Any) {

        dismissAction()
        delegate?.errorViewControllerAcceptPressed(self)

    }

    @IBAction func cancelAction(_ sender: Any) {

        dismissAction()
        delegate?.errorViewControllerCancelPressed(self)

    }

    deinit {
        ComponentManager.remove(id: presenter.routerTag)
        DLog(message: "ErrorViewController deinitialized")
    }

}

extension ErrorViewController: ErrorViewProtocol {

    func configureView() {
        setUpView()
        setAccessibilities()
    }

    func showMessageError(error: ErrorDisplay) {
        
        errorDisplay = error
        updateView()
    }

    func showError(error: ModelBO) {

    }

    func changeColorEditTexts() {

    }

}

// MARK: - APPIUM

private extension ErrorViewController {

    func setAccessibilities() {
        #if APPIUM
        errorViewContainer.accessibilityIdentifier = ConstantsAccessibilities.ERROR_MODAL
        Tools.setAccessibility(view: acceptButton, identifier: ConstantsAccessibilities.ERROR_ACCEPT_BUTTON)
        Tools.setAccessibility(view: errorLabel, identifier: ConstantsAccessibilities.ERROR_DESCRIPTION_LABEL)
        Tools.setAccessibility(view: warningImageView, identifier: ConstantsAccessibilities.ERROR_WARNING_IMAGE)
        Tools.setAccessibility(view: cancelButton, identifier: ConstantsAccessibilities.ERROR_CANCEL_BUTTON)
        #endif

    }

}
