//
//  ErrorContract.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 6/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation

protocol ErrorViewProtocol: ViewProtocol {

    func configureView()

    func showMessageError(error: ErrorDisplay)
}

protocol ErrorPresenterProtocol: class {

    var errorBO: ErrorBO? { get set }

    func didLoad()
}
