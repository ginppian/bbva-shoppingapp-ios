//
//  UpgradeNotificationViewController.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 6/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit
import CellsNativeCore
import CellsNativeComponents

final class UpgradeNotificationViewController: BaseViewController {
    
    // MARK: Static Constants
    
    static let ID: String = String(describing: UpgradeNotificationViewController.self)
    let paddingBottomView: CGFloat = 20.0
    let statusHeight = UIApplication.shared.statusBarFrame.height
    let navigationBarViewDefaultHeight: CGFloat = 44.0
    let shadowDefaultOpacity: Float = 0.2
    
    // MARK: IBOutlets
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var navigationBarView: UIView!
    @IBOutlet weak var navigationBarViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subTitleLabel: UILabel!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var bottomViewHeightConstraint: NSLayoutConstraint!
    @IBOutlet weak var bottomStackView: UIStackView!
    @IBOutlet weak var updateButton: UIButton!
    @IBOutlet weak var updateNotButton: UIButton!
    
    // MARK: Vars
    
    var presenter: UpgradeNotificationPresenterProtocol!
    
    private var animationTopView: LOTAnimationView?
    private var urlMarket: String?
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        configureView()
        
        presenter.viewLoaded()
    }
    
    override func viewWillLayoutSubviews() {
        
        super.viewWillLayoutSubviews()
        
        checkBottomViewShadow()
    }
    
    override func initModel(withModel model: Model) {
        presenter.setModel(model: model)
    }
    
    deinit {
        ComponentManager.remove(id: presenter.routerTag)
        DLog(message: "Deinitialized UpgradeNotificationViewController")
    }
    
    // MARK: - Actions -
    
    @IBAction func pressedUpdateButton(_ sender: Any) {
        
        if let urlMarket = urlMarket {
            _ = URLOpener().openURL(urlMarket)
        }
    }
    
    @IBAction func pressedUpdateNotButton(_ sender: Any) {
        
        closeUpgradeNotificationView()
    }
}

// MARK: - Private methods -
private extension UpgradeNotificationViewController {
    
    func configureView() {
        
        setNavigationBarTransparent()
        
        navigationBarView.backgroundColor = .clear
        navigationBarViewHeightConstraint.constant = navigationBarViewDefaultHeight + statusHeight
        
        if #available(iOS 11.0, *) {
            scrollView.contentInsetAdjustmentBehavior = .never
        } else {
            automaticallyAdjustsScrollViewInsets = false
        }
        
        scrollView.delegate = self
        scrollView.bounces = false
        
        loadAnimationTopView(name: "space-bg")
        
        titleLabel.textColor = .BBVA600
        titleLabel.font = Tools.setFontMedium(size: 20)
        titleLabel.text = Localizables.others.key_update_title_text
        
        subTitleLabel.textColor = .BBVA600
        subTitleLabel.font = Tools.setFontBook(size: 14)
        subTitleLabel.text = ""
        
        bottomView.addShadow(location: .top, color: .BBVA500, opacity: shadowDefaultOpacity, radius: 1)
        
        updateButton.setBackgroundColor(color: .MEDIUMBLUE, forUIControlState: .normal)
        updateButton.setBackgroundColor(color: .DARKMEDIUMBLUE, forUIControlState: .highlighted)
        updateButton.titleLabel?.font = Tools.setFontBold(size: 14)
        updateButton.setTitleColor(.BBVAWHITE, for: .normal)
        updateButton.setTitle(Localizables.others.key_update_button_text, for: .normal)
        
        updateNotButton.setBackgroundColor(color: .clear, forUIControlState: .normal)
        updateNotButton.titleLabel?.font = Tools.setFontMedium(size: 14)
        updateNotButton.setTitleColor(.MEDIUMBLUE, for: .normal)
        updateNotButton.setTitle(Localizables.others.key_recommended_update_later_text, for: .normal)
    }
    
    func loadAnimationTopView(name: String) {
        
        let lottieAnimationView = LOTAnimationView(name: name)
        lottieAnimationView.frame = topView.bounds
        lottieAnimationView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        lottieAnimationView.contentMode = .scaleAspectFill
        lottieAnimationView.loopAnimation = true
        topView.addSubview(lottieAnimationView)
        animationTopView = lottieAnimationView
        
        animationTopView?.play()
    }
    
    func checkBottomViewShadow() {
        
        let offset = scrollView.contentOffset
        let bounds = scrollView.bounds
        let size = scrollView.contentSize
        let inset = scrollView.contentInset
        let y = offset.y + bounds.size.height - inset.bottom
        let h = size.height
        
        if y >= h {
            bottomView.layer.shadowOpacity = 0
        } else {
            bottomView.layer.shadowOpacity = shadowDefaultOpacity
        }
    }
    
    @objc func closeUpgradeNotificationView() {
        
        UpgradeManager.sharedInstance().hide()
    }
}

// MARK: - UpgradeNotificationViewProtocol -
extension UpgradeNotificationViewController: UpgradeNotificationViewProtocol {
    
    func showError(error: ModelBO) {
        presenter.showModal()
    }
    
    func changeColorEditTexts() {
    }
    
    func showMandatoryUpdate(withMessage message: String, andUrlStore urlStore: String) {
        
        urlMarket = urlStore
        
        setNavigationTitle(withText: Localizables.others.key_mandatory_update_header_text)
        
        navigationItem.leftBarButtonItem = nil
        
        subTitleLabel.text = message
        
        updateNotButton.isHidden = true
        
        let size = bottomStackView.systemLayoutSizeFitting(UILayoutFittingCompressedSize)
        bottomViewHeightConstraint.constant = size.height + (paddingBottomView * 2)
    }
    
    func showRecommendedUpdate(withMessage message: String, andUrlStore urlStore: String) {
        
        urlMarket = urlStore
        
        setNavigationTitle(withText: Localizables.others.key_recommended_update_header_text)
        
        let image = SVGCache.image(forSVGNamed: ConstantsImages.Common.close_icon, color: .BBVAWHITE)
        setNavigationLeftButton(withImage: image, action: #selector(closeUpgradeNotificationView))
        
        subTitleLabel.text = message
    }
    
    func changeNavigationBarColor(withColor color: UIColor) {
        navigationBarView.backgroundColor = color
    }
}

// MARK: - UIScrollViewDelegate -
extension UpgradeNotificationViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        
        checkBottomViewShadow()
        
        presenter.showNavigationBarAnimation(byScrollViewHeight: scrollView.bounds.height, withContentSize: scrollView.contentSize, andContentOffset: scrollView.contentOffset)
    }
}
