//
//  UpgradeNotificationPresenter.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 6/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

final class UpgradeNotificationPresenter <T: UpgradeNotificationViewProtocol>: BasePresenter<T> {
    
    var isMandatory: Bool?
    var url: String?
    var message: String?
    
    required init() {
        super.init(tag: UpgradeNotificationPageReaction.routerTag)
    }
    
    override init(busManager: BusManager) {
        super.init(busManager: busManager)
        routerTag = UpgradeNotificationPageReaction.routerTag
    }
    
    override func setModel(model: Model) {
        
        if let upgradeNotificationTransport = model as? UpgradeNotificationTransport {
            isMandatory = upgradeNotificationTransport.isMandatory
            url = upgradeNotificationTransport.url
            message = upgradeNotificationTransport.message
        }
    }
}

extension UpgradeNotificationPresenter: UpgradeNotificationPresenterProtocol {
    
    func viewLoaded() {
        
        if let message = message, let url = url {
            
            if let isMandatory = isMandatory, isMandatory {
                view?.showMandatoryUpdate(withMessage: message, andUrlStore: url)
            } else {
                view?.showRecommendedUpdate(withMessage: message, andUrlStore: url)
            }
        }
    }
    
    func showNavigationBarAnimation(byScrollViewHeight scrollViewHeight: CGFloat, withContentSize contentSize: CGSize, andContentOffset contentOffset: CGPoint) {
        
        let offsetAnimation: CGFloat = 20.0
        var offset = contentOffset.y / offsetAnimation
        
        if contentSize.height > scrollViewHeight + offsetAnimation {
            
            if offset > 1 {
                offset = 1
                let color = UIColor.NAVY.withAlphaComponent(offset)
                view?.changeNavigationBarColor(withColor: color)
                
            } else {
                
                let color = UIColor.NAVY.withAlphaComponent(offset)
                view?.changeNavigationBarColor(withColor: color)
            }
        }
    }
}
