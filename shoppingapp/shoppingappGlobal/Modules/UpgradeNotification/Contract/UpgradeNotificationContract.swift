//
//  UpgradeNotificationContract.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 6/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol UpgradeNotificationViewProtocol: ViewProtocol {
    
    func showMandatoryUpdate(withMessage message: String, andUrlStore urlStore: String)
    func showRecommendedUpdate(withMessage message: String, andUrlStore urlStore: String)
    func changeNavigationBarColor(withColor color: UIColor)
}

protocol UpgradeNotificationPresenterProtocol: PresenterProtocol {
    
    func viewLoaded()
    func showNavigationBarAnimation(byScrollViewHeight scrollViewHeight: CGFloat, withContentSize contentSize: CGSize, andContentOffset contentOffset: CGPoint)
}
