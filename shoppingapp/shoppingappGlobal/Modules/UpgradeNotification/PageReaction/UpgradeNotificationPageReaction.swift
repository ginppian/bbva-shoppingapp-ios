//
//  UpgradeNotificationPageReaction.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 6/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

class UpgradeNotificationPageReaction: PageReaction {
    
    static let routerTag: String = "upgrade-notifications-tag"
    
    override func configure() {
    }
}
