//
//  TransactionsListPresenter.swift
//  shoppingapp
//
//  Created by jesus.martinez on 7/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit
import RxSwift
import CellsNativeComponents

class TransactionsFilterPresenter<T: TransactionsFilterViewProtocol>: BasePresenter<T> {

    var cardBO: CardBO?
    var shouldNavigateToFilter = false

    var shouldShowConceptFilter = Settings.TransactionsFilter.should_show_concept_filter

    required init() {
        super.init(tag: TransactionsFilterPageReaction.ROUTER_TAG)
    }

    override init(busManager: BusManager) {

        super.init(busManager: busManager)
        routerTag = TransactionsFilterPageReaction.ROUTER_TAG
    }

    var transactionFilterTransport = TransactionFilterTransport()

    func validateTransportModel() {

        if amountFromIsModified() && amountToIsModified() && !twoAmountsAreValid() {
            view?.showCheckFilterButton(withState: false)
        } else if needConceptToFilter() || needUseDateToFilter() || needUseAmoutToFilter() || typeToIsModified() {
            view?.showCheckFilterButton(withState: true)
        } else {
            view?.showCheckFilterButton(withState: false)
        }

        if amountFromIsModified() && amountToIsModified() && !twoAmountsAreValid() {
            view?.showCheckRestoreButton(withState: true)
        } else if needConceptToFilter() || needUseDateToFilter() || needUseAmoutToFilter() || typeToIsModified() {
            view?.showCheckRestoreButton(withState: true)
        } else {
            view?.showCheckRestoreButton(withState: false)
        }

    }

    func needConceptToFilter() -> Bool {
        return !Tools.isStringNilOrEmpty(withString: transactionFilterTransport.conceptText)
    }

    func dateFromIsModified() -> Bool {
        if !(Tools.isStringNilOrEmpty(withString: transactionFilterTransport.fromDateText)) {
            return true
        }

        return false
    }

    func dateToIsModified() -> Bool {
        if !Tools.isStringNilOrEmpty(withString: transactionFilterTransport.toDateText) && (Date().string(format: Date.DATE_FORMAT_SERVER) != transactionFilterTransport.toDateText) {
            return true
        }

        return false
    }

    func needUseDateToFilter() -> Bool {

        if dateToIsModified() || (dateFromIsModified() && dateFromIsModified()) {
            return true
        }
        return false
    }

    func amountFromIsModified() -> Bool {
        if !Tools.isStringNilOrEmpty(withString: transactionFilterTransport.fromAmountText) {
            return true
        }
        return false
    }

    func amountToIsModified() -> Bool {
        if !Tools.isStringNilOrEmpty(withString: transactionFilterTransport.toAmountText) {
            return true
        }
        return false
    }

    func twoAmountsAreValid() -> Bool {
        if amountToIsModified() && amountFromIsModified() {

            if transactionFilterTransport.toAmountText?.compare(transactionFilterTransport.fromAmountText!, options: .numeric) == .orderedAscending {
                return false
            }
            return true

        } else {
            return false
        }
    }

    func needUseAmoutToFilter() -> Bool {

        if amountToIsModified() || amountFromIsModified() {
            return true
        }
        return false
    }

    func typeToIsModified() -> Bool {
        if transactionFilterTransport.filtertype != TransactionFilterType.all {
            return true
        }
        return false
    }

    func restoreTranspor() {
        transactionFilterTransport = TransactionFilterTransport()
    }

    override func setModel(model: Model) {

        if let cardsTransport = model as? CardsTransport {
            self.cardBO = cardsTransport.cardBO
            self.shouldNavigateToFilter = (cardsTransport.flagNavigation)!
        } else if let cardBO = model as? CardBO {
            self.cardBO = cardBO
        }

    }

}

extension TransactionsFilterPresenter: TransactionsFilterPresenterProtocol {

    func configureData() {

        view?.configureDateFilter(withDate: Date())
        view?.configureAmountFilter()

        view?.showCheckFilterButton(withState: false)
        view?.showCheckRestoreButton(withState: false)

        view?.sendOmnitureScreen()

        if !shouldShowConceptFilter {
            view?.hideConceptFilter()
        } else {
            view?.configureConceptFilter(withPlaceHolderTitle: Localizables.transactions.key_transactions_contains_text)
        }
    }

    func modifiedAmount(withAmountFrom amountFromText: String?, withAmountTo amountToText: String?) {
        transactionFilterTransport.fromAmountText = amountFromText
        transactionFilterTransport.toAmountText = amountToText
        validateTransportModel()
    }

    func modifiedConcept(withText text: String?) {
        transactionFilterTransport.conceptText = text
        validateTransportModel()
    }

    func modifiedDate(withDateFrom dateFromText: String?, withDateTo dateToText: String?) {
        transactionFilterTransport.fromDateText = dateFromText

        if Date().string(format: Date.DATE_FORMAT_SERVER) != dateToText {
            transactionFilterTransport.toDateText = dateToText
        } else {
            transactionFilterTransport.toDateText = nil
        }

        validateTransportModel()
    }

    func modifiedType(withFilterType filterType: TransactionFilterType) {
        transactionFilterTransport.filtertype = filterType
        validateTransportModel()
    }

    func restoreAllValues() {

        restoreTranspor()
        view?.removeConcept()
        view?.configureDateFilter(withDate: Date())
        view?.restoreAmountValues()
        view?.restoreTypeFilterValues()
        validateTransportModel()

    }

    func filterMovementens() {

        transactionFilterTransport.cardBO = cardBO

        if shouldNavigateToFilter {
            busManager.navigateScreen(tag: routerTag, TransactionsFilterPageReaction.EVENT_NAV_TO_NEXT_SCREEN, transactionFilterTransport)
        } else {

            busManager.publishData(tag: routerTag, TransactionsFilterPageReaction.EVENT_RELOAD_TRANSACTION_LIST, transactionFilterTransport)
            busManager.dismissViewController(animated: true)
        }

    }

}
