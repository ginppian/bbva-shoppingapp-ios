//
//  TransactionListContract.swift
//  shoppingapp
//
//  Created by jesus.martinez on 3/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import RxSwift

protocol TransactionsFilterViewProtocol: ViewProtocol {

    func configureConceptFilter(withPlaceHolderTitle placeHolderTitle: String)
    func configureDateFilter(withDate date: Date)
    func configureAmountFilter()
    func removeConcept()
    func restoreTypeFilterValues()
    func restoreAmountValues ()
    func showCheckFilterButton(withState state: Bool)
    func showCheckRestoreButton(withState state: Bool)
    func sendOmnitureScreen()
    func hideConceptFilter()
}

protocol TransactionsFilterPresenterProtocol: PresenterProtocol {

    func configureData()
    func modifiedConcept(withText text: String?)
    func modifiedDate(withDateFrom dateFromText: String?, withDateTo dateToText: String?)
    func modifiedType(withFilterType filterType: TransactionFilterType)
    func modifiedAmount(withAmountFrom amountFromText: String?, withAmountTo amountToText: String?)
    func restoreAllValues ()
    func filterMovementens ()

}
