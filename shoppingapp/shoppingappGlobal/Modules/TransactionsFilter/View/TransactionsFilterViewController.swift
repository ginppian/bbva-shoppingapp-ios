//
//  TransactionsFilterViewController.swift
//  shoppingapp
//
//  Created by jesus.martinez on 25/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit
import CellsNativeCore
import CellsNativeComponents
import IQKeyboardManagerSwift

class TransactionsFilterViewController: BaseViewController {

    // MARK: Constants

    static let ID: String = String(describing: TransactionsFilterViewController.self)

    // MARK: IBOutlets

    @IBOutlet weak var conceptFilterViewContainer: UIView!
    @IBOutlet weak var conceptFilterView: TextFilterView!

    @IBOutlet weak var fromToDateFilterContainerView: UIView!
    @IBOutlet weak var fromToDateFilterView: FromToFilterView!

    @IBOutlet weak var fromToAmountFilterContainerView: UIView!
    @IBOutlet weak var fromToAmountFilterView: FromToAmountFilterView!

    @IBOutlet weak var transactionTypeFilterView: TransactionTypeFilterView!
    @IBOutlet weak var tranactionTypeFilterHeightConstraint: NSLayoutConstraint!

    @IBOutlet weak var buttonsContainerView: UIView!
    @IBOutlet weak var separatorView: UIView!
    @IBOutlet weak var restoreButton: UIButton!
    @IBOutlet weak var filterButton: UIButton!
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var bottomView: UIView!

    var presenter: TransactionsFilterPresenterProtocol!

    override func viewDidLoad() {

        self.configure(appLifecycle: (UIApplication.shared.delegate as! AppDelegate).appLifeCycle!, id: TransactionsFilterViewController.ID)

        super.viewDidLoad()

        configureView()
        setAccessibilities()

    }

    override func viewWillAppear(_ animated: Bool) {

        super.viewWillAppear(animated)

        configureNavigationBar()

        presenter.configureData()

        BusManager.sessionDataInstance.disableSwipeLateralMenu()
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
    }

    deinit {
        ComponentManager.remove(id: presenter.routerTag)
        DLog(message: "Deinitialized TransactionsFilterViewController")
    }

    func configureView() {

        conceptFilterViewContainer.backgroundColor = .NAVY
        fromToDateFilterContainerView.backgroundColor = .BBVAWHITE
        fromToAmountFilterContainerView.backgroundColor = .BBVA100

        scrollView.backgroundColor = .clear
        topView.backgroundColor = .NAVY
        bottomView.backgroundColor = .white

        transactionTypeFilterView.delegate = self
        conceptFilterView.delegate = self
        fromToDateFilterView.delegate = self
        fromToAmountFilterView.delegate = self

        configureBottomViews()

    }

    func configureBottomViews() {

        buttonsContainerView.backgroundColor = .BBVAWHITE
        buttonsContainerView.layer.shadowColor = UIColor(white: 0.0, alpha: 0.2).cgColor
        buttonsContainerView.layer.shadowOpacity = 0.4
        buttonsContainerView.layer.shadowOffset = CGSize.zero
        buttonsContainerView.layer.shadowRadius = 2

        separatorView.backgroundColor = .BBVAWHITE

        restoreButton.setTitle(Localizables.transactions.key_transactions_restore_button, for: .normal)
        restoreButton.setTitleColor(.BBVAWHITE, for: .normal)
        restoreButton.titleLabel?.font = Tools.setFontBold(size: 14)
        restoreButton.setBackgroundColor(color: .BBVA200, forUIControlState: .disabled)
        restoreButton.setBackgroundColor(color: .MEDIUMBLUE, forUIControlState: .normal)
        restoreButton.setBackgroundColor(color: .DARKMEDIUMBLUE, forUIControlState: .highlighted)

        filterButton.setTitle(Localizables.transactions.key_transactions_results_button, for: .normal)
        filterButton.setTitleColor(.BBVAWHITE, for: .normal)
        filterButton.titleLabel?.font = Tools.setFontBold(size: 14)
        filterButton.setBackgroundColor(color: .BBVA200, forUIControlState: .disabled)
        filterButton.setBackgroundColor(color: .COREBLUE, forUIControlState: .normal)
        filterButton.setBackgroundColor(color: .DARKCOREBLUE, forUIControlState: .highlighted)

    }

    func configureNavigationBar() {

        setNavigationBarDefault()

        let image = SVGCache.image(forSVGNamed: ConstantsImages.Common.close_icon, color: .BBVAWHITE)
        setNavigationLeftButton(withImage: image, action: #selector(dismissAction))

        setNavigationTitle(withText: Localizables.transactions.key_transactions_search_text)
    }

    // MARK: Actions

    @IBAction func restoreAction(_ sender: AnyObject) {
        presenter.restoreAllValues()
    }

    @IBAction func filterAction(_ sender: AnyObject) {
        presenter.filterMovementens()
    }

    override func initModel(withModel model: Model) {
        presenter.setModel(model: model)
    }

}

extension TransactionsFilterViewController: TransactionTypeFilterViewDelegate {
    func modifiedType(_ transactionTypeFilterView: TransactionTypeFilterView, typeSelected type: TransactionFilterType) {
        presenter.modifiedType(withFilterType: type)
    }

    func showMoreFilters(transactionTypeFilterView: TransactionTypeFilterView, withHeight height: CGFloat) {
        tranactionTypeFilterHeightConstraint.constant = height
        IQKeyboardManager.shared.resignFirstResponder()

    }
}

extension TransactionsFilterViewController: TextFilterViewDelegate {
    func textFilterUpdate(textFilterView: TextFilterView) {
        presenter.modifiedConcept(withText: textFilterView.textField.text!)
    }
}

extension TransactionsFilterViewController: FromToFilterViewDelegate {
    func dateFilterView(_ fromToFilterView: FromToFilterView, selectedFromDate fromDate: String?, selectedToDate toDate: String?) {
         presenter.modifiedDate(withDateFrom: fromDate, withDateTo: toDate)
    }
}

extension TransactionsFilterViewController: FromToAmountFilterViewDelegate {
    func amounteFilterView(_ fromToAmountFilterView: FromToAmountFilterView, selectedFromAmount fromAmountText: String?, selectedToAmount toAmountText: String?) {
        presenter.modifiedAmount(withAmountFrom: fromAmountText, withAmountTo: toAmountText)
    }
}

extension TransactionsFilterViewController: TransactionsFilterViewProtocol {

    func showError(error: ModelBO) {

    }

    func changeColorEditTexts() {

    }

    func configureDateFilter(withDate date: Date) {

        let fromDisplayData = DateFilterDisplayData(minDate: nil, maxDate: date, title: Localizables.transactions.key_transactions_from_text, dateSelectedTitle: Localizables.transactions.key_transactions_pickdate_text, dateSelected: nil)

        let toDisplayData = DateFilterDisplayData(minDate: nil, maxDate: date, title: Localizables.transactions.key_transactions_to_text, dateSelectedTitle: Localizables.transactions.key_transactions_pickdate_text, dateSelected: date.string(format: Date.DATE_FORMAT_DAY_MONTH_YEAR))

        fromToDateFilterView.presenter.displayData = FromToFilterDisplayData(fromDisplayData: fromDisplayData, toDisplayData: toDisplayData)

    }

    func configureConceptFilter(withPlaceHolderTitle placeHolderTitle: String) {

        conceptFilterView.presenter.displayData = TextFilterDisplayData(placeHolderTitle: placeHolderTitle, maximumLength: nil, validCharacterSet: nil)

    }

    func configureAmountFilter() {
        fromToAmountFilterView.presenter.displayData = FromToAmountFilterDisplayData(error: Localizables.transactions.key_transactions_amount_range_error_text)
    }

    func showCheckFilterButton(withState state: Bool) {
            filterButton.isEnabled = state
    }

    func showCheckRestoreButton(withState state: Bool) {
            restoreButton.isEnabled = state
    }

    func removeConcept() {
        conceptFilterView.presenter.removeAction()
    }

    func restoreTypeFilterValues() {
        transactionTypeFilterView.presenter.setToDefaultValue()
    }

    func restoreAmountValues() {
        fromToAmountFilterView.presenter.removeAmountValues()
    }

    func sendOmnitureScreen() {

        TrackerHelper.sharedInstance().trackTransactionsFilterScreen(withCustomerID: SessionDataManager.sessionDataInstance().customerID)

    }

    func hideConceptFilter() {

        conceptFilterViewContainer.isHidden = true
        topView.backgroundColor = .BBVAWHITE

    }
}

// MARK: - APPIUM

private extension TransactionsFilterViewController {

    func setAccessibilities() {

        #if APPIUM

            conceptFilterView.isAccessibilityElement = false
            conceptFilterViewContainer.isAccessibilityElement = false

            Tools.setAccessibility(view: conceptFilterView.textField, identifier: ConstantsAccessibilities.TRANSACTIONS_FILTER_CONCEPT_ID)

            Tools.setAccessibility(view: conceptFilterView.removeButton, identifier: ConstantsAccessibilities.TRANSACTIONS_FILTER_REMOVECONCEPT_ID)

            Tools.setAccessibility(view: (navigationController?.navigationBar)!, identifier: ConstantsAccessibilities.TRANSACTIONS_FILTER_VIEW)

            Tools.setAccessibility(view: restoreButton, identifier: ConstantsAccessibilities.TRANSACTIONS_FILTER_RESTORE_BUTTON_ID)
            Tools.setAccessibility(view: filterButton, identifier: ConstantsAccessibilities.TRANSACTIONS_FILTER_FILTER_BUTTON_ID)

            fromToDateFilterContainerView.isAccessibilityElement = false
            fromToDateFilterView.isAccessibilityElement = false
            fromToDateFilterView.toDateFilterView.isAccessibilityElement = false
            fromToDateFilterView.fromDateFilterView.isAccessibilityElement = false

            Tools.setAccessibility(view: fromToDateFilterView.toDateFilterView.dateLabel, identifier: ConstantsAccessibilities.TRANSACTIONS_FILTER_DATETO_LABEL_ID)
            Tools.setAccessibility(view: fromToDateFilterView.fromDateFilterView.dateLabel, identifier: ConstantsAccessibilities.TRANSACTIONS_FILTER_DATEFROM_LABEL_ID)

            fromToDateFilterView.fromDateFilterView.datePicker.isAccessibilityElement = false
            fromToDateFilterView.toDateFilterView.datePicker.isAccessibilityElement = false

            Tools.setAccessibility(view: fromToDateFilterView.toDateFilterView.datePicker, identifier: ConstantsAccessibilities.TRANSACTIONS_FILTER_DATETO_PICKER_ID)
            Tools.setAccessibility(view: fromToDateFilterView.fromDateFilterView.datePicker, identifier: ConstantsAccessibilities.TRANSACTIONS_FILTER_DATEFROM_PICKER_ID)

            fromToAmountFilterContainerView.isAccessibilityElement = false
            fromToAmountFilterView.isAccessibilityElement = false
            fromToAmountFilterView.toAmountTextfieldView.isAccessibilityElement = false
            fromToAmountFilterView.fromAmountTextfieldView.isAccessibilityElement = false
            fromToAmountFilterView.toAmountTextfieldView.textfield.isAccessibilityElement = false
            fromToAmountFilterView.fromAmountTextfieldView.textfield.isAccessibilityElement = false

            Tools.setAccessibility(view: fromToAmountFilterView.toAmountTextfieldView.textfield, identifier: ConstantsAccessibilities.TRANSACTIONS_FILTER_AMOUNTTO_INPUT_ID)
             Tools.setAccessibility(view: fromToAmountFilterView.toAmountTextfieldView.textfield.placeholderLabel, identifier: ConstantsAccessibilities.TRANSACTIONS_FILTER_AMOUNTTO_PLACEHOLDER_INPUT_ID)

            Tools.setAccessibility(view: fromToAmountFilterView.fromAmountTextfieldView.textfield, identifier: ConstantsAccessibilities.TRANSACTIONS_FILTER_AMOUNTFROM_INPUT_ID)
            Tools.setAccessibility(view: fromToAmountFilterView.fromAmountTextfieldView.textfield.placeholderLabel, identifier: ConstantsAccessibilities.TRANSACTIONS_FILTER_AMOUNTFROM_PLACEHOLDER_INPUT_ID)

            transactionTypeFilterView.isAccessibilityElement = false
            Tools.setAccessibility(view: transactionTypeFilterView.moreFiltersButton, identifier: ConstantsAccessibilities.TRANSACTIONS_FILTER_DISPLAYTYPES_BUTTON_ID)

            transactionTypeFilterView.transactionFiltersTableView.isAccessibilityElement = false
            transactionTypeFilterView.transactionFiltersTableView.accessibilityIdentifier = ConstantsAccessibilities.TRANSACTIONS_FILTER_TYPE_TABLEVIEW_ID

        #endif

    }

}
