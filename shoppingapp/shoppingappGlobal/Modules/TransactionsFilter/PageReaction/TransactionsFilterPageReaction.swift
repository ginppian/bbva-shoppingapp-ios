//
//  TransactionsFilterPageReaction.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 26/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

class TransactionsFilterPageReaction: PageReaction {

    static let ROUTER_TAG: String = "transactions-filter-tag"

    static let EVENT_NAV_TO_NEXT_SCREEN: ActionSpec<Model> = ActionSpec<Model>(id: "EVENT_NAV_TO_NEXT_SCREEN")

    static let EVENT_RELOAD_TRANSACTION_LIST: ActionSpec<Model> = ActionSpec<Model>(id: "EVENT_RELOAD_TRANSACTION_LIST")

    static let CHANNEL_TRANSACTIONS_FILTER: Channel<Model> = Channel(name: "channel-transactions-filter")

    override func configure() {

        navigateFirstScreenFromTransactionFilter()
        reloadTransactionListFromTransactionFilterDismiss()
    }

    func navigateFirstScreenFromTransactionFilter() {

        _ = when(id: TransactionsFilterPageReaction.ROUTER_TAG, type: Model.self)
            .doAction(actionSpec: TransactionsFilterPageReaction.EVENT_NAV_TO_NEXT_SCREEN)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, model in

                router.navigateFirstScreenFromTransactionFilter(withModel: model!)

            }
    }

    func reloadTransactionListFromTransactionFilterDismiss() {

        _ = writeOn(channel: TransactionsFilterPageReaction.CHANNEL_TRANSACTIONS_FILTER)
            .when(componentID: TransactionsFilterPageReaction.ROUTER_TAG)
            .doAction(actionSpec: TransactionsFilterPageReaction.EVENT_RELOAD_TRANSACTION_LIST)
    }
}
