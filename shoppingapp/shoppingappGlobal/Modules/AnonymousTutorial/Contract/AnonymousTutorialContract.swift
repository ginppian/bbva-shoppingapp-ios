//
//  AnonymousTutorialContract.swift
//  shoppingapp
//
//  Created by Javier Pino on 30/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol AnonymousTutorialViewProtocol: ViewProtocol {

    func configure(displayData: AnonymousTutorialDisplayData)
    func startAnimation()
    func stopAnimation()
    func removeAnimation()
    func reloadAnimation()
    func hideNotClientButton(_ hide: Bool)
}

protocol AnonymousTutorialPresenterProtocol: PresenterProtocol {

    func viewLoaded()
    func viewAppeared()
    func viewDisappeared()
    func viewEnteredBackground()
    func viewEnteredForeground()
    func animationFinished()
    func loginButtonPressed()
    func notClientButtonPressed()
}
