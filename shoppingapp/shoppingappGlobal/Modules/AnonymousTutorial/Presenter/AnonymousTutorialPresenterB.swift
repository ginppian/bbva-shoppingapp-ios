//
//  AnonymousTutorialPresenter.swift
//  shoppingapp
//
//  Created by Javier Pino on 30/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

final class AnonymousTutorialPresenterB <T: AnonymousTutorialViewProtocol>: AnonymousTutorialPresenter<T> {

    override func notClientButtonPressed() {

        SessionDataManager.sessionDataInstance().closeSession()
        busManager.notify(tag: AnonymousTutorialPageReaction.ROUTER_TAG, AnonymousTutorialPageReactionB.EVENT_CONFIGURE_LOGIN_TO_REGISTER_IN_GLOMO)
        busManager.setRootWhenNoSession()
    }
}
