//
//  AnonymousTutorialPresenter.swift
//  shoppingapp
//
//  Created by Javier Pino on 30/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class AnonymousTutorialPresenter <T: AnonymousTutorialViewProtocol>: BasePresenter<T> {

    private var displayData: AnonymousTutorialDisplayData
    var animating = false
    var animated = false

    required init(displayData: AnonymousTutorialDisplayData) {
        self.displayData = displayData
        super.init(tag: "")
    }

    func notClientButtonPressed() {

    }
}

extension AnonymousTutorialPresenter: AnonymousTutorialPresenterProtocol {

    func viewLoaded() {

        view?.configure(displayData: displayData)
    }

    func viewAppeared() {

        if !animating {

            animating = true
            animated = false
            view?.startAnimation()
        }
        view?.hideNotClientButton(KeychainManager.shared.isUserSaved())
    }

    func viewDisappeared() {

        if animating {

            animating = false
            animated = true
        }

        view?.stopAnimation()
    }

    func viewEnteredBackground() {

        view?.removeAnimation()
    }

    func viewEnteredForeground() {

        animating = true
        animated = false
        view?.reloadAnimation()
    }

    func animationFinished() {

        animating = false
        animated = true
    }

    func loginButtonPressed() {
        
        busManager.setRootWhenNoSession()
        SessionDataManager.sessionDataInstance().closeSession()
    }

}
