//
//  AnonymousTutorialPageReaction.swift
//  shoppingapp
//
//  Created by Marcos on 4/2/19.
//  Copyright © 2019 BBVA. All rights reserved.
//

import Foundation

import CellsNativeCore
import CellsNativeComponents

class AnonymousTutorialPageReaction: PageReaction {

    static let ROUTER_TAG: String = "anonymous-tutorial-tag"
    
}
