//
//  AnonymousTutorialPageReaction.swift
//  shoppingapp
//
//  Created by Marcos on 4/2/19.
//  Copyright © 2019 BBVA. All rights reserved.
//

import Foundation

import CellsNativeCore
import CellsNativeComponents

final class AnonymousTutorialPageReactionB: AnonymousTutorialPageReaction {

    static let CONFIGURE_LOGIN_TO_REGISTER_IN_GLOMO_CHANNEL: Channel<Void> = Channel(name: "CONFIGURE_LOGIN_TO_REGISTER_IN_GLOMO_CHANNEL")

    static let EVENT_CONFIGURE_LOGIN_TO_REGISTER_IN_GLOMO: ActionSpec<Void> = ActionSpec<Void>(id: "EVENT_CONFIGURE_LOGIN_TO_REGISTER_IN_GLOMO")

    override func configure() {

        writeChannelConfigureLoginWithoutAnimations()
    }

    func writeChannelConfigureLoginWithoutAnimations() {

        _ = writeOn(channel: AnonymousTutorialPageReactionB.CONFIGURE_LOGIN_TO_REGISTER_IN_GLOMO_CHANNEL)
            .when(componentID: AnonymousTutorialPageReaction.ROUTER_TAG)
            .doAction(actionSpec: AnonymousTutorialPageReactionB.EVENT_CONFIGURE_LOGIN_TO_REGISTER_IN_GLOMO)
    }
}
