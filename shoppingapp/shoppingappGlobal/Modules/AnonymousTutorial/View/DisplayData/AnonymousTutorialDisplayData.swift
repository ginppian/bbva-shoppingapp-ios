//
//  AnonymousTutorialDisplayData.swift
//  shoppingapp
//
//  Created by Javier Pino on 30/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

struct AnonymousTutorialDisplayData {

    let navigationBarTitle: String
    let animationName: String
    let title: String
    let subtitle: String
}

extension AnonymousTutorialDisplayData: Equatable {

    public static func == (lhs: AnonymousTutorialDisplayData, rhs: AnonymousTutorialDisplayData) -> Bool {
        return lhs.navigationBarTitle == rhs.navigationBarTitle &&
            lhs.animationName == rhs.animationName &&
            lhs.title == rhs.title &&
            lhs.subtitle == rhs.subtitle
    }
}
