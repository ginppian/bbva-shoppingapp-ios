//
//  AnonymousTutorialViewController.swift
//  shoppingapp
//
//  Created by Javier Pino on 30/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit
import Lottie

final class AnonymousTutorialViewController: BaseViewController {

    static let ID: String = String(describing: AnonymousTutorialViewController.self)
    static let HomeID = String(describing: AnonymousTutorialViewController.self) + "Home"
    static let CardsID = String(describing: AnonymousTutorialViewController.self) + "Cards"
    static let RewardsID = String(describing: AnonymousTutorialViewController.self) + "Rewards"
    static let NotificationsID = String(describing: AnonymousTutorialViewController.self) + "Notifications"

    var presenter: AnonymousTutorialPresenterProtocol!
    var displayData: AnonymousTutorialDisplayData?

    private var animationView: LOTAnimationView?

    // MARK: IBOutlet
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var topView: UIView!
    @IBOutlet weak var middleView: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var subtitleLabel: UILabel!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var notClientButton: UIButton!

    // MARK: Life cycle
    override func viewDidLoad() {
        self.configure(appLifecycle: (UIApplication.shared.delegate as! AppDelegate).appLifeCycle!, id: AnonymousTutorialViewController.ID)

        super.viewDidLoad()

        configureView()
        presenter.viewLoaded()
    }

    override func viewWillAppear(_ animated: Bool) {

        super.viewWillAppear(animated)

        NotificationCenter.default.addObserver(self, selector: #selector(applicationWillEnterForeground), name: .UIApplicationWillEnterForeground, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(applicationDidEnterBackground), name: .UIApplicationDidEnterBackground, object: nil)

        setNavigationBarDefault()
        addNavigationBarMenuButton()
    }

    override func viewDidAppear(_ animated: Bool) {

        super.viewDidAppear(animated)

        presenter.viewAppeared()
    }

    override func viewDidDisappear(_ animated: Bool) {

        super.viewDidDisappear(animated)

        NotificationCenter.default.removeObserver(self)

        presenter.viewDisappeared()
    }

    // MARK: IBAction
    @IBAction func loginButtonPressed(_ sender: Any) {

        presenter.loginButtonPressed()
    }

    @IBAction func notClientButtonPressed(_ sender: Any) {

        presenter.notClientButtonPressed()
    }
}

// MARK: Private methods
private extension AnonymousTutorialViewController {

    func configureView() {

        backgroundView.backgroundColor = .clear
        backgroundImageView.backgroundColor = .clear
        topView.backgroundColor = .clear

        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [UIColor.COREBLUE.cgColor, UIColor.MEDIUMBLUE.cgColor]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = Display.bounds
        backgroundView.layer.insertSublayer(gradientLayer, at: 0)

        addDefaultBackgroundImage()

        titleLabel.backgroundColor = .clear
        titleLabel.font = Tools.setFontBold(size: 16)
        titleLabel.textColor = .BBVAWHITE

        subtitleLabel.backgroundColor = .clear
        subtitleLabel.font = Tools.setFontBook(size: 14)
        subtitleLabel.textColor = .BBVAWHITE

        loginButton.setBackgroundColor(color: .COREBLUE, forUIControlState: .normal)
        loginButton.setBackgroundColor(color: .DARKCOREBLUE, forUIControlState: .highlighted)
        loginButton.titleLabel?.font = Tools.setFontBold(size: 14)
        loginButton.setTitleColor(.BBVAWHITE, for: .normal)
        loginButton.setTitle(Localizables.login.key_dashboard_login, for: .normal)

        notClientButton.backgroundColor = .clear
        notClientButton.titleLabel?.font = Tools.setFontBold(size: 14)
        notClientButton.setTitleColor(.BBVAWHITE, for: .normal)

        let attributtedText = CustomAttributedText.attributedBoldText(forFullText: Localizables.login.key_login_use_bbva_wallet_text + " " + Localizables.login.key_login_register_text, withBoldText: Localizables.login.key_login_register_text, withFont: Tools.setFontBook(size: 14), withBoldFont: Tools.setFontBold(size: 14), andForegroundColor: .BBVAWHITE)
        let attributtedTextHighlighted = CustomAttributedText.attributedBoldText(forFullText: Localizables.login.key_login_use_bbva_wallet_text + " " + Localizables.login.key_login_register_text, withBoldText: Localizables.login.key_login_register_text, withFont: Tools.setFontBook(size: 14), withBoldFont: Tools.setFontBold(size: 14), andForegroundColor: .BBVAWHITE30)

        notClientButton.setAttributedTitle(attributtedText, for: .normal)
        notClientButton.setAttributedTitle(attributtedTextHighlighted, for: .highlighted)

    }

    func setTitleToNavigationBar(_ title: String) {

        setNavigationTitle(withText: title)
    }

    func addDefaultBackgroundImage() {

        backgroundImageView.contentMode = .scaleAspectFill
        var backgroundImage: UIImage?
        switch Display.typeIsLike {
        case .iphone5:
            backgroundImage = UIImage(named: ConstantsImages.AnonymousTutorial.imageBackgroundIphone45)
        case .iphone6plus:
            backgroundImage = UIImage(named: ConstantsImages.AnonymousTutorial.imageBackgroundIphonePlus)
        case .iphoneX:
            backgroundImage = UIImage(named: ConstantsImages.AnonymousTutorial.imageBackgroundIphoneX)
        case .iphoneXsMax:
            backgroundImage = UIImage(named: ConstantsImages.AnonymousTutorial.imageBackgroundIphoneXR)
        default:
            backgroundImage = UIImage(named: ConstantsImages.AnonymousTutorial.imageBackgroundIphone6)
        }

        backgroundImageView.image = backgroundImage
    }

    func loadAnimationViewInTopView(name: String, autoPlay: Bool) {

        let lottieAnimationView = LOTAnimationView(name: name)
        lottieAnimationView.frame = topView.bounds
        lottieAnimationView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        lottieAnimationView.contentMode = .scaleAspectFit
        topView.addSubview(lottieAnimationView)
        animationView = lottieAnimationView

        if autoPlay {
            playAnimation()
        }
    }

    func removeAnimationViewFromTopView() {

        if let animationView = animationView {

            animationView.removeFromSuperview()
        }
    }

    func playAnimation() {

        animationView?.play(completion: { [weak self] animationFinished in

            if animationFinished {
                self?.presenter.animationFinished()
            }
        })
    }

    @objc func applicationDidEnterBackground() {

        presenter.viewEnteredBackground()
    }

    @objc func applicationWillEnterForeground() {

        presenter.viewEnteredForeground()
    }
}

// MARK: AnonymousTutorialViewProtocol
extension AnonymousTutorialViewController: AnonymousTutorialViewProtocol {

    func showError(error: ModelBO) {

    }

    func changeColorEditTexts() {

    }

    func configure(displayData: AnonymousTutorialDisplayData) {

        self.displayData = displayData

        setTitleToNavigationBar(displayData.navigationBarTitle)

        removeAnimationViewFromTopView()
        loadAnimationViewInTopView(name: displayData.animationName, autoPlay: false)

        titleLabel.text = displayData.title
        subtitleLabel.text = displayData.subtitle
    }

    func startAnimation() {

        playAnimation()
    }

    func stopAnimation() {

        animationView?.stop()
    }

    func removeAnimation() {

        removeAnimationViewFromTopView()
    }

    func reloadAnimation() {

        if let animationName = displayData?.animationName {

            loadAnimationViewInTopView(name: animationName, autoPlay: true)
        }
    }

    func hideNotClientButton(_ hide: Bool) {
        notClientButton.isHidden = hide
    }
}

extension AnonymousTutorialViewController: TabBarOutTransitionAnimating {

    var movingOutView: UIView? {

        return topView
    }

    var fadingOutView: UIView? {

        return middleView
    }
}

extension AnonymousTutorialViewController: TabBarInTransitionAnimating {

    var movingInView: UIView? {

        return topView
    }

    var fadingInView: UIView? {

        return middleView
    }
}
