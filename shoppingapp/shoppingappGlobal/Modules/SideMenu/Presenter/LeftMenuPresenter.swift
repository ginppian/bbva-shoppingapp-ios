//
//  LeftMenuPresenter.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 18/10/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation
import RxSwift

class LeftMenuPresenter<T: LeftMenuViewProtocol>: BasePresenter<T> {

    let disposeBag = DisposeBag()

    var interactor: LeftMenuInteractorProtocol!
    var notificationManager: NotificationManager?

    override init(busManager: BusManager) {
        super.init(busManager: busManager)
        routerTag = LeftMenuPageReaction.ROUTER_TAG
    }

    required init() {
        super.init(tag: LeftMenuPageReaction.ROUTER_TAG)
    }
    
    func leftMenuOptionPressed(withDisplayItem displayItem: DisplayItemLeftMenu) {
        
        switch displayItem.option {
            
        case .about:
            createAndShowAbout()
            view?.closeMenu()
            
        case .settings:
            createAndShowSettings()
            view?.closeMenu()
            
        case .call:
            view?.showCallBBVA(withTel: Settings.Global.default_bbva_phone_number)
            
        case .help:
            busManager.navigateScreen(tag: routerTag, LeftMenuPageReaction.EVENT_NAV_TO_HELP, EmptyModel())
            view?.closeMenu()
            
        case .web:
            view?.showBankingMobile()
            
        default:
            
            break
        }
    }
}

// MARK: - private methods
extension LeftMenuPresenter {
    
    private func checkIfDeviceTokenIsRegister() -> Bool {
        
        let apnsToken = PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kApnsToken) as? String
        let registerDeviceToken = PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kRegisterDeviceToken) as? String
        
        if let apnsToken = apnsToken, apnsToken == registerDeviceToken {
            return true
        }
        
        return false
    }
    
    private func createConfigurationData(_ settings: @escaping (SettingsTransport) -> Void) {
        
        var areNotificationsEnabled = false
        
        notificationManager = notificationManager ?? NotificationManagerType()
        if let notificationManager = notificationManager {
            
            notificationManager.isAuthorizedStatus { [weak self] isAuthorized in
                
                areNotificationsEnabled = isAuthorized
                
                let isRegisterDeviceToken = self?.checkIfDeviceTokenIsRegister() ?? false
                
                let balancesEnabled = PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kConfigBalancesAvailable)
                let isBalancesAvailable = (balancesEnabled as? Bool) ?? false
                
                let vibrationEnabled = PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kConfigVibrationEnabled)
                let isVibrationEnabled = (vibrationEnabled as? Bool) ?? true
                
                let ticketPromotionsEnabled = PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kConfigTicketPromotionsEnabled)
                let isTicketPromotionsEnabled = (ticketPromotionsEnabled as? Bool) ?? false
                
                let settingsTransport = SettingsTransport(isNotificationsAvailable: (isRegisterDeviceToken && areNotificationsEnabled), isBalancesAvailable: isBalancesAvailable, isVibrationEnabled: isVibrationEnabled, isTicketPromotionsEnabled: isTicketPromotionsEnabled)
                
                settings(settingsTransport)
            }
        }
    }
    
    private func createAboutData() -> AboutTransport {
        
        guard let version = DeviceManager.sharedInstance().getValueBundleKey(forKey: DevicesManagerKeys.CFBundleShortVersionString) as? String else {
            return AboutTransport()
        }
        
        return AboutTransport(appVersion: version, operatingSystem: UIDevice.current.systemName, operatingSystemVersion: UIDevice.current.systemVersion, deviceModel: DeviceManager.sharedInstance().getModelName())
    }
    
    private func createAboutOptionsData() -> AboutOptionsTransport {
        
        return AboutOptionsTransport()
    }
}

extension LeftMenuPresenter: LeftMenuPresenterProtocol {

    func createDisplayData() {

        if let interactor = interactor {

            interactor.provideOptionsLeftMenu().subscribe(
                onNext: { [weak self] result in

                    if let optionsBO = result as? [LeftMenuOptionBO] {

                        var displayDataItems = [DisplayItemLeftMenu]()

                        for displayDataItem in optionsBO {
                            displayDataItems.append(DisplayItemLeftMenu(image: displayDataItem.image, title: displayDataItem.title, option: displayDataItem.option))
                        }

                        self?.view?.setDisplayData(displayData: displayDataItems)
                    }

                }
            ).addDisposableTo(disposeBag)

        }
    }

    func presentLoginAndCloseSession() {
        busManager.setRootWhenNoSession()
        SessionDataManager.sessionDataInstance().requestLogout()
        SessionDataManager.sessionDataInstance().closeSession()
        updateOptionsMenu()
    }

    func updateOptionsMenu() {

        view?.updateOptionsMenu(isUserLogged: SessionDataManager.sessionDataInstance().isUserLogged)
    }

    func checkSession() {

        if SessionDataManager.sessionDataInstance().isUserAnonymous || SessionDataManager.sessionDataInstance().isUserLogged {

            TrackerHelper.sharedInstance().trackLogoutSuccessEvent(SessionDataManager.sessionDataInstance().tabBarSelected)
            SessionDataManager.sessionDataInstance().clearTabBarSelected()

            view?.presentLoginAndCloseSession()

        } else {
            view?.closeMenu()
        }

    }
    
    func createAndShowSettings() {

        createConfigurationData { [weak self] settingsTransport in
            
            if let settingsTransportJson = settingsTransport.toJSON() {
                
                DispatchQueue.main.async {
                    
                    self?.busManager.publishData(tag: LeftMenuPageReaction.ROUTER_TAG, LeftMenuPageReaction.EVENT_PARAMS_CONFIGURATION, settingsTransportJson)
                    
                    self?.busManager.navigateScreen(tag: LeftMenuPageReaction.ROUTER_TAG, LeftMenuPageReaction.EVENT_NAV_TO_CONFIGURATION, Void())
                }
            }
        }
    }

    func createAndShowAbout() {

        if let aboutTransportJson = createAboutData().toJSON(), let aboutOptionsTransportJson = createAboutOptionsData().toJSON() {

            busManager.publishData(tag: LeftMenuPageReaction.ROUTER_TAG, LeftMenuPageReaction.EVENT_PARAMS_OPTIONS_ABOUT, aboutOptionsTransportJson)

            busManager.publishData(tag: LeftMenuPageReaction.ROUTER_TAG, LeftMenuPageReaction.EVENT_PARAMS_ABOUT, aboutTransportJson)

            busManager.navigateScreen(tag: LeftMenuPageReaction.ROUTER_TAG, LeftMenuPageReaction.EVENT_NAV_TO_ABOUT, Void())
        }
    }
}
