//
//  LeftMenuPresenterA.swift
//  shoppingappMX
//
//  Created by jesus.martinez on 26/12/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class LeftMenuPresenterA<T: LeftMenuViewProtocolA>: LeftMenuPresenter<T> {
    
    lazy var publicConfiguration = PublicConfigurationManager.sharedInstance().localPublicConfiguration()
    
    override func leftMenuOptionPressed(withDisplayItem displayItem: DisplayItemLeftMenu) {
        switch displayItem.option {
        case .mobileToken:
            navigateToMobileToken()
            view?.closeMenu()

        default:
            
            super.leftMenuOptionPressed(withDisplayItem: displayItem)
        }
    }
    
    fileprivate func navigateToMobileToken() {
        
        if view?.softTokenSerial != nil {
            
            view?.openMobileTokenFlow()
            
        } else {
            
            guard let appInstalledTransportJSON = appNameForMobileToken().toJSON() else {
                return
            }
            busManager.publishData(tag: LeftMenuPageReactionA.ROUTER_TAG, LeftMenuPageReactionA.eventParamsMobileToken, appInstalledTransportJSON)
            busManager.navigateScreen(tag: LeftMenuPageReactionA.ROUTER_TAG, LeftMenuPageReactionA.eventNavMobileToken, Void())
        }
    }
    
    fileprivate func appNameForMobileToken() -> AppInstalledTransport {
        
        if publicConfiguration.isGlomoInstalled() {
            return AppInstalledTransport(appName: AppInstalledTransport.glomo)
        } else if publicConfiguration.isBancomerInstalled() {
            return AppInstalledTransport(appName: AppInstalledTransport.bancomer)
        } else {
            return AppInstalledTransport(appName: AppInstalledTransport.store)
        }
    }
}

extension LeftMenuPresenterA: LeftMenuPresenterProtocolA {
    
}
