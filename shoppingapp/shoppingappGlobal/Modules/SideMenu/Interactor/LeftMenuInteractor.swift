//
//  LeftMenuInteractor.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 18/10/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation
import RxSwift

class LeftMenuInteractor: LeftMenuInteractorProtocol {

    let disposeBag = DisposeBag()

    func provideOptionsLeftMenu() -> Observable<[ModelBO]> {

        return Observable.create { observer in

            ConfigurationDataManager.sharedInstance.provideLeftMenuOptions().subscribe(

                onNext: { result in

                    if let optionsEntity = result as? [LeftMenuOptionEntity] {

                        var optionsBO = [LeftMenuOptionBO]()

                        for optionEntity in optionsEntity {
                            optionsBO.append(LeftMenuOptionBO(image: optionEntity.image, title: optionEntity.title, option: optionEntity.option))
                        }

                        observer.onNext(optionsBO)
                        observer.onCompleted()
                    } else {
                        DLog(message: "Incorrect LeftMenuOptionEntity")
                    }
                }

            ).addDisposableTo(self.disposeBag)
            return Disposables.create()
        }
    }

}
