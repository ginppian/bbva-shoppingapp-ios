//
//  LeftMenuViewControllerA.swift
//  shoppingappMX
//
//  Created by Jesús Ángel Sánchez Sánchez on 14/2/19.
//  Copyright © 2019 BBVA. All rights reserved.
//

import Foundation
import BGMConnectSDK

class LeftMenuViewControllerA: LeftMenuViewController {
    
}

extension LeftMenuViewControllerA: LeftMenuViewProtocolA {
    
    func openMobileTokenFlow() {
        
        GCSController.getCore().openMobileTokenFlow(currentController: self)
    }
    
    var softTokenSerial: String? {
        return GCSController.getCore().retrieveSoftTokenSerial()
    }
}
