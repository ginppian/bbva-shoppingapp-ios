//
//  LeftMenuViewController.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 13/10/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit
import CellsNativeCore
import CellsNativeComponents

class LeftMenuViewController: BaseViewController {

    // MARK: Static Constants

    static let ID: String = String(describing: LeftMenuViewController.self)

    // MARK: Outlets

    @IBOutlet weak var logoImageView: UIImageView!
    @IBOutlet weak var sessionButton: UIButton!
    @IBOutlet weak var optionsTable: UITableView!

    // MARK: Vars

    var presenter: LeftMenuPresenterProtocol!
    var displayData: [DisplayItemLeftMenu]!
    var imageView: UIImageView!

    // MARK: life cycle

    override func viewDidLoad() {
        configure(appLifecycle: (UIApplication.shared.delegate as! AppDelegate).appLifeCycle!, id: LeftMenuViewController.ID)

        super.viewDidLoad()

        presenter.createDisplayData()

        setBackgroundImage()

        configureTableOptions()

        configureSessionButton()

        setAccessibilities()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        // TODO: Force disregister reactions to avoid multiples reactions
        // Using ShoppingGlobalReaction instead of LeftMenuPageReaction allows remove this issue. Do it in PSS2-2400. Also we can create a GlobalPageReaction just for LeftMenu.
        super.viewDidDisappear(animated)

    }

    override var prefersStatusBarHidden: Bool {
        return true
    }

    deinit {
        ComponentManager.remove(id: presenter.routerTag)
        DLog(message: "Deinitialized LeftMenuViewController")
    }

    func setBackgroundImage() {
        imageView = UIImageView(frame: UIScreen.main.bounds)
        imageView.contentMode = UIViewContentMode.scaleAspectFill
        self.view.insertSubview(imageView, at: 0)
    }

    func configureSessionButton() {
        sessionButton.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.SideMenu.get_out, color: .BBVAWHITE), width: 20, height: 20), for: .normal)
        sessionButton.contentMode = .scaleAspectFit
        sessionButton.imageView?.contentMode = .scaleAspectFit
        sessionButton.clipsToBounds = true
        let spacing = 30; // the amount of spacing to appear between image and title
        sessionButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: CGFloat(spacing))
        sessionButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: CGFloat(spacing), bottom: 0, right: 0)

        sessionButton.titleLabel?.font = Tools.setFontMedium(size: 13)
        sessionButton.titleLabel?.textColor = .white
    }

    func configureTableOptions() {

        optionsTable.backgroundColor = .clear

        optionsTable.register(UINib(nibName: MenuOptionTableViewCell.identifier, bundle: nil), forCellReuseIdentifier: MenuOptionTableViewCell.identifier)

        optionsTable.showsVerticalScrollIndicator = false
        optionsTable.separatorStyle = .none

        optionsTable.dataSource = self
        optionsTable.delegate = self
    }

    func updateOptionsMenu() {
        presenter.updateOptionsMenu()
    }

    func menuOptionPressed(byIndexPath indexPath: IndexPath) {

        if let displayItem = displayData?[indexPath.row] {

            presenter.leftMenuOptionPressed(withDisplayItem: displayItem)
        }
    }

    // MARK: actions

    @IBAction func actionSession(_ sender: Any) {
       presenter.checkSession()
    }

}

extension LeftMenuViewController: LeftMenuViewProtocol {

    func showError(error: ModelBO) {
    }

    func changeColorEditTexts() {
    }

    func updateOptionsMenu(isUserLogged: Bool) {
        if !isUserLogged {
            sessionButton.setTitle(Localizables.common.key_login_session_menu_text, for: .normal)

            sessionButton.imageView!.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) / 180.0)

            imageView.image = UIImage(named: ConstantsImages.SideMenu.background_left_menu_no_session)
        } else {
            sessionButton.setTitle(Localizables.common.key_close_session_menu_text, for: .normal)

            sessionButton.imageView!.transform = CGAffineTransform(rotationAngle: (180.0 * CGFloat(Double.pi)) * 180.0)

            imageView.image = UIImage(named: ConstantsImages.SideMenu.background_left_menu_logged)
        }

        setAccessibilityForSessionButton(ForUserLogged: !isUserLogged)
    }

    func closeMenu() {
        self.sideMenuController?.hideLeftViewAnimated(Any?.self)
    }

    func presentLoginAndCloseSession() {
        self.sideMenuController?.hideLeftView(animated: true, delay: 0, completionHandler: {
            self.presenter.presentLoginAndCloseSession()
        })
    }

    func setDisplayData(displayData: [DisplayItemLeftMenu]) {
        self.displayData = displayData
        optionsTable.reloadData()
    }

    func showCallBBVA(withTel tel: String) {

        let scheme = "tel:\(tel)"
        if let url = URL(string: scheme) {
            URLOpener().openURL(url)
        }
    }

    func showBankingMobile() {

        let urlSchema = Configuration.provideUrlSchemaFromConfiguration()
        let urlBBVAMovilMarket = Configuration.provideUrlBBVAMovilMarketFromConfiguration()
        
        let urlOpener = URLOpener()

        if !urlOpener.openURL(urlSchema) {
            _ = urlOpener.openURL(urlBBVAMovilMarket)
        }
    }
}

// MARK: tableview datasource

extension LeftMenuViewController: UITableViewDataSource {

    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if let displayData = displayData {
            return displayData.count
        } else {
            return 0
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return MenuOptionTableViewCell.cellHeight
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        if let displayItem = displayData?[indexPath.row] {

            if let cell = tableView.dequeueReusableCell(withIdentifier: MenuOptionTableViewCell.identifier, for: indexPath) as? MenuOptionTableViewCell {
                cell.configure(withDisplayItem: displayItem)
                cell.delegate = self

                return cell
            }
        }

        return UITableViewCell()

    }

}

// MARK: tableview delegate

extension LeftMenuViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        menuOptionPressed(byIndexPath: indexPath)
    }
}

extension LeftMenuViewController: MenuOptionTableViewCellDelegate {

    func menuOptionButtonPressed(_ menuOptionTableViewCell: MenuOptionTableViewCell, sender: UIButton) {
        let cell = sender.superview?.superview as! UITableViewCell
        let indexPath = optionsTable.indexPath(for: cell)

        menuOptionPressed(byIndexPath: indexPath!)
    }
}

// MARK: - APPIUM

private extension LeftMenuViewController {

    func setAccessibilities() {

        #if APPIUM

            optionsTable.accessibilityIdentifier = ConstantsAccessibilities.LEFT_MENU_COMPONENT

            Tools.setAccessibility(view: sessionButton, identifier: ConstantsAccessibilities.LEFT_LOGIN_IN_MENU_OPTION)

        #endif
    }

    func setAccessibilityForSessionButton(ForUserLogged isUserLogged: Bool) {

        #if APPIUM

            Tools.setAccessibility(view: sessionButton, identifier: isUserLogged ? ConstantsAccessibilities.LEFT_LOGIN_OUT_MENU_OPTION : ConstantsAccessibilities.LEFT_LOGIN_IN_MENU_OPTION)

        #endif

    }

}
