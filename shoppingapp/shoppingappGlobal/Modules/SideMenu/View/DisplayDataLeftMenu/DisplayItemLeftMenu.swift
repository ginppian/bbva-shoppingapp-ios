//
//  DisplayItemLeftMenu.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 18/10/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation

class DisplayItemLeftMenu: ModelBO {

    let image: String
    let title: String
    let option: LeftMenuOption

    init(image: String, title: String, option: LeftMenuOption) {

        self.image = image
        self.title = title
        self.option = option
    }
}
