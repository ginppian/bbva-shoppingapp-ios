//
//  MenuOptionTableViewCell.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 18/10/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit

protocol MenuOptionTableViewCellDelegate: class {
    func menuOptionButtonPressed(_ menuOptionTableViewCell: MenuOptionTableViewCell, sender: UIButton)

}

class MenuOptionTableViewCell: UITableViewCell {

    // MARK: Constants

    static let identifier = "MenuOptionTableViewCell"
    static let cellHeight: CGFloat = 50
    weak var delegate: MenuOptionTableViewCellDelegate?

    // MARK: Outlets

    @IBOutlet weak var tittleButton: UIButton!

    // MARK: Life Cycle

    override func awakeFromNib() {
        super.awakeFromNib()

        backgroundColor = .clear
        selectionStyle = .none

        setAccessibilities()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

    func configure(withDisplayItem displayItem: DisplayItemLeftMenu) {
        tittleButton.setImage(ImageUtilities.image(with: SVGCache.image(forSVGNamed: displayItem.image, color: .BBVAWHITE), width: 20, height: 20), for: .normal)
        tittleButton.contentMode = .scaleAspectFit
        tittleButton.imageView?.contentMode = .scaleAspectFit
        tittleButton.clipsToBounds = true
        let spacing = 30; // the amount of spacing to appear between image and title
        tittleButton.imageEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: CGFloat(spacing))
        tittleButton.titleEdgeInsets = UIEdgeInsets(top: 0, left: CGFloat(spacing), bottom: 0, right: 0)
        tittleButton.setTitle(displayItem.title, for: .normal)

        tittleButton.titleLabel?.font = Tools.setFontMedium(size: 13)
        tittleButton.titleLabel?.textColor = .white
    }

    @IBAction func menuButtonPressed(_ sender: Any) {

        if let delegate = delegate {
            delegate.menuOptionButtonPressed(self, sender: sender as! UIButton)
        }

    }
}

// MARK: - APPIUM

private extension MenuOptionTableViewCell {

    func setAccessibilities() {

        #if APPIUM

            Tools.setAccessibility(view: self, identifier: ConstantsAccessibilities.LEFT_MENU_OPTION_LAYOUT)

        #endif

    }

}
