//
//  LeftMenuPageReactionA.swift
//  shoppingappMX
//
//  Created by jesus.martinez on 26/12/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

class LeftMenuPageReactionA: LeftMenuPageReaction {
    
    static let eventNavMobileToken: ActionSpec<Void> = ActionSpec<Void>(id: "EVENT_NAV_TO_MOBILE_TOKEN")
    
    static let eventParamsMobileToken: ActionSpec<Any> = ActionSpec<Any>(id: "EVENT_PARAMS_MOBILE_TOKEN")
    
    static let cellMobilePage = "userMobileToken"

    override func configure() {
        super.configure()
        
        navigateToMobileToken()
        registerReactionForWriteInChannelParamsMobileToken()
    }
    
    func navigateToMobileToken() {
        
        _ = when(id: LeftMenuPageReaction.ROUTER_TAG, type: Void.self)
            .doAction(actionSpec: LeftMenuPageReactionA.eventNavMobileToken)
            .then()
            .doWebNavigation(webRoute: LeftMenuPageReactionA.cellMobilePage, nativeRoute: Route(viewController: { () -> UIViewController in
                return (BusManager.sessionDataInstance.routerFactory?.getViewController(route: MobileTokenViewController.id))!
            }, animationToNext: { _, destinationViewController in
                
                if let destinationViewController = destinationViewController {
                    BusManager.sessionDataInstance.navigationController?.pushViewController(destinationViewController, animated: false)
                }
                
            }, animationToPrevious: { _, _ in
                
            }
            ))
    }
    
    func registerReactionForWriteInChannelParamsMobileToken() {
        
        let channel: JsonChannel = JsonChannel(MobileTokenPageReaction.menuMobileTokenChannel)
        
        _ = writeOn(channel: channel)
            .when(componentID: LeftMenuPageReactionA.ROUTER_TAG)
            .doAction(actionSpec: LeftMenuPageReactionA.eventParamsMobileToken)
    }
}
