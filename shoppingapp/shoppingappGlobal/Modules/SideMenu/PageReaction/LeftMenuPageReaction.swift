//
//  LeftMenuPageReaction.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 9/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import CellsNativeCore
import CellsNativeComponents

class LeftMenuPageReaction: PageReaction {

    static let ROUTER_TAG: String = "left-menu-tag"

    static let EVENT_NAV_TO_HELP: ActionSpec<Model> = ActionSpec<Model>(id: "EVENT_NAV_TO_HELP")

    static let EVENT_PARAMS_CONFIGURATION: ActionSpec<Any> = ActionSpec<Any>(id: "EVENT_PARAMS_CONFIGURATION")

    static let EVENT_NAV_TO_CONFIGURATION: ActionSpec<Void> = ActionSpec<Void>(id: "EVENT_NAV_TO_CONFIGURATION")

    static let EVENT_NAV_TO_ABOUT: ActionSpec<Void> = ActionSpec<Void>(id: "EVENT_NAV_TO_ABOUT")

    static let EVENT_PARAMS_ABOUT: ActionSpec<Any> = ActionSpec<Any>(id: "EVENT_PARAMS_ABOUT")

    static let EVENT_PARAMS_OPTIONS_ABOUT: ActionSpec<Any> = ActionSpec<Any>(id: "EVENT_PARAMS_OPTIONS_ABOUT")

    // Cells pages

    static let CELLS_PAGE_CONFIG_MENU = "settings"

    static let CELLS_PAGE_ABOUT = "about"

    override func configure() {

        navigateToHelpFromLeftMenu()
        registerReactionForWriteInChannelParamsConfiguration()
        registerReactionForNavigateToConfiguration()
        registerReactionForNavigateToAbout()
        registerReactionForWriteInChannelParamsAbout()
        registerReactionForWriteInChannelParamsOptionsAbout()
    }

    func navigateToHelpFromLeftMenu() {

        _ = when(id: LeftMenuPageReaction.ROUTER_TAG, type: Model.self)
            .doAction(actionSpec: LeftMenuPageReaction.EVENT_NAV_TO_HELP)
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReaction { router, model in

                router.navigateToHelpFromLeftMenu(withModel: model!)

            }
    }

    func registerReactionForWriteInChannelParamsConfiguration() {

        let channel: JsonChannel = JsonChannel(ConfigurationWalletCellPageReaction.MENU_INFORMATION_CHANNEL)

        _ = writeOn(channel: channel)
            .when(componentID: LeftMenuPageReaction.ROUTER_TAG)
            .doAction(actionSpec: LeftMenuPageReaction.EVENT_PARAMS_CONFIGURATION)
    }

    func registerReactionForNavigateToConfiguration() {

        _ = when(id: LeftMenuPageReaction.ROUTER_TAG, type: Void.self)
            .doAction(actionSpec: LeftMenuPageReaction.EVENT_NAV_TO_CONFIGURATION)
            .then()
            .doWebNavigation(webRoute: LeftMenuPageReaction.CELLS_PAGE_CONFIG_MENU, nativeRoute: Route(viewController: { () -> UIViewController in
                return (BusManager.sessionDataInstance.routerFactory?.getViewController(route: ConfigurationWalletCellViewController.ID))!
            }, animationToNext: { _, destinationViewController in

                if let destinationViewController = destinationViewController {
                    BusManager.sessionDataInstance.navigationController?.pushViewController(destinationViewController, animated: false)
                }

            }, animationToPrevious: { _, _ in

            }
            ))
        
//        _ = when(id: LeftMenuPageReaction.ROUTER_TAG, type: Void.self)
//            .doAction(actionSpec: LeftMenuPageReaction.EVENT_NAV_TO_CONFIGURATION)
//            .then()
//            .inside(componentID: WebController.ID, component: WebController.self)
//            .doReactionAndClearChannel { webController, _ in
//
//                webController.navigateTo(route: LeftMenuPageReaction.CELLS_PAGE_CONFIG_MENU, payload: nil)
//        }
//
//        _ = reactTo(channel: WebController.ON_PAGE_LOADED_CHANNEL)?
//            .then()
//            .inside(componentID: WebController.ID, component: WebController.self)
//            .doReactionAndClearChannel { _, page in
//
//                if page == LeftMenuPageReaction.CELLS_PAGE_CONFIG_MENU,
//                    let viewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: ConfigurationWalletCellViewController.ID) {
//                    viewController.modalPresentationStyle = .overCurrentContext
//                    let navigationController = UINavigationController(rootViewController: viewController)
//                    BusManager.sessionDataInstance.present(viewController: navigationController, animated: true, completion: nil)
//                }
//        }
        
    }

    func registerReactionForWriteInChannelParamsAbout() {

        let channel: JsonChannel = JsonChannel(AboutPageReaction.menuAboutChannel)

        _ = writeOn(channel: channel)
            .when(componentID: LeftMenuPageReaction.ROUTER_TAG)
            .doAction(actionSpec: LeftMenuPageReaction.EVENT_PARAMS_ABOUT)
    }

    func registerReactionForWriteInChannelParamsOptionsAbout() {

        let channel: JsonChannel = JsonChannel(AboutPageReaction.menuAboutOptionsChannel)

        _ = writeOn(channel: channel)
            .when(componentID: LeftMenuPageReaction.ROUTER_TAG)
            .doAction(actionSpec: LeftMenuPageReaction.EVENT_PARAMS_OPTIONS_ABOUT)
    }

    func registerReactionForNavigateToAbout() {

        _ = when(id: LeftMenuPageReaction.ROUTER_TAG, type: Void.self)
            .doAction(actionSpec: LeftMenuPageReaction.EVENT_NAV_TO_ABOUT)
            .then()
            .doWebNavigation(webRoute: LeftMenuPageReaction.CELLS_PAGE_ABOUT, nativeRoute: Route(viewController: { () -> UIViewController in
                return (BusManager.sessionDataInstance.routerFactory?.getViewController(route: AboutViewController.ID))!
            }, animationToNext: { _, destinationViewController in

                if let destinationViewController = destinationViewController {
                    BusManager.sessionDataInstance.navigationController?.pushViewController(destinationViewController, animated: false)
                }

            }, animationToPrevious: { _, _ in

            }
            ))
        
//        _ = when(id: LeftMenuPageReaction.ROUTER_TAG, type: Void.self)
//            .doAction(actionSpec: LeftMenuPageReaction.EVENT_NAV_TO_ABOUT)
//            .then()
//            .inside(componentID: WebController.ID, component: WebController.self)
//            .doReactionAndClearChannel { webController, _ in
//
//                webController.navigateTo(route: LeftMenuPageReaction.CELLS_PAGE_ABOUT, payload: nil)
//        }
//
//        _ = reactTo(channel: WebController.ON_PAGE_LOADED_CHANNEL)?
//            .then()
//            .inside(componentID: WebController.ID, component: WebController.self)
//            .doReactionAndClearChannel { _, page in
//
//                if page == LeftMenuPageReaction.CELLS_PAGE_ABOUT,
//                    let viewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: AboutViewController.ID) {
//                    viewController.modalPresentationStyle = .overCurrentContext
//                    let navigationController = UINavigationController(rootViewController: viewController)
//                    BusManager.sessionDataInstance.present(viewController: navigationController, animated: true, completion: nil)
//                }
//        }
        
    }

}
