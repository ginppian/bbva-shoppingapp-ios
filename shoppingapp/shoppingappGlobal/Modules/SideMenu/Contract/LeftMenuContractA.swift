//
//  LeftMenuContractA.swift
//  shoppingappMX
//
//  Created by Jesús Ángel Sánchez Sánchez on 14/2/19.
//  Copyright © 2019 BBVA. All rights reserved.
//

import Foundation

protocol LeftMenuViewProtocolA: LeftMenuViewProtocol {
    
    func openMobileTokenFlow()
    
    var softTokenSerial: String? { get }
}

protocol LeftMenuPresenterProtocolA: LeftMenuPresenterProtocol {
    
}
