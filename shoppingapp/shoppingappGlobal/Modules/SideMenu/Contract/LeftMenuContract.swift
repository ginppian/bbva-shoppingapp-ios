//
//  LeftMenuContract.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 18/10/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit
import RxSwift

protocol LeftMenuViewProtocol: ViewProtocol {
    func setDisplayData(displayData: [DisplayItemLeftMenu])
    func updateOptionsMenu(isUserLogged: Bool)
    func closeMenu()
    func presentLoginAndCloseSession()
    func showCallBBVA(withTel tel: String)
    func showBankingMobile()

}

protocol LeftMenuPresenterProtocol: PresenterProtocol {
    func createDisplayData()
    func presentLoginAndCloseSession()
    func updateOptionsMenu()
    func checkSession()
    func leftMenuOptionPressed(withDisplayItem displayItem: DisplayItemLeftMenu)

}

protocol LeftMenuInteractorProtocol {
    func provideOptionsLeftMenu() -> Observable<[ModelBO]>
}
