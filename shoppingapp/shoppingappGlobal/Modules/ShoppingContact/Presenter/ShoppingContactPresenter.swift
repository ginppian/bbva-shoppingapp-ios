//
//  ShoppingContactPresenter.swift
//  shoppingapp
//
//  Created by Jose Luis Mendoza Espinosa on 30/07/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import RxSwift
import CellsNativeComponents

class ShoppingContactPresenter<T: ShoppingContactViewProtocol>: BasePresenter<T> {
    
    var promotionBO: PromotionBO?
    var contactDisplayData = [ContactDisplayData]()
    
    let ROUTER_TAG: String = "shopping-contact-tag"
    
    required init() {
        super.init(tag: ROUTER_TAG)
    }
    
    override init(busManager: BusManager) {
        super.init(busManager: busManager)
        routerTag = "shopping-contact-tag"
    }
    
    override func setModel(model: Model) {
        
        if let promotionDetailTransport = model as? PromotionDetailTransport {
            
            promotionBO = promotionDetailTransport.promotionBO
        }
    }
}

extension ShoppingContactPresenter: ShoppingContactPresenterProtocol {
    
    func viewLoaded() {
        
        if let promotion = promotionBO {
            
            contactDisplayData = ContactDisplayData.generateContactDisplayData(fromPromotion: promotion)
            view?.showContactDetails(details: contactDisplayData)
        }
    }
    
    func pressedCallToPhoneContact(contactPhone phone: String) {
        
        view?.callToPhone(withPhone: phone)
    }
    
    func pressedSendEmailContact(contactemail email: String) {
        
        view?.sendEmail(withEmail: email)
    }
}
