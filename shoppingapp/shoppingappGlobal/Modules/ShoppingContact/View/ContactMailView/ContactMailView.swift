//
//  ContactMailView.swift
//  shoppingapp
//
//  Created by Jose Luis Mendoza Espinosa on 30/07/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol ContactMailViewDelegate: class {
    func contactMailViewSelected(_ contactMailView: ContactMailView, withMail mail: String)
}

class ContactMailView: UIView {
    
    @IBOutlet weak var contactMailImage: UIImageView!
    @IBOutlet weak var contactMailLabel: UILabel!
    
    weak var delegate: ContactMailViewDelegate?
    
    var email: String = ""
    
    @IBAction func contactSelected(_ sender: Any) {
        
        delegate?.contactMailViewSelected(self, withMail: email)
    }
    
    static func getContactMailView(withMail mail: String, posY: CGFloat) -> ContactMailView {
        
        let className = String(describing: self)
        let mailView: ContactMailView!  = Bundle.main.loadNibNamed(className, owner: nil, options: nil)?.first as? ContactMailView
        
        if mailView != nil {
            
            mailView.email = mail
            
            mailView.contactMailLabel.text = Localizables.promotions.key_send_email_text
            mailView.contactMailLabel.font = Tools.setFontMedium(size: 14)
            mailView.contactMailLabel.textColor = .MEDIUMBLUE
            mailView.contactMailLabel.textAlignment = .center
            
            mailView.contactMailImage.image = ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Common.email, color: .MEDIUMBLUE), width: Float(mailView.contactMailImage.frame.size.width), height: Float(mailView.contactMailImage.frame.size.height))
            
            mailView.frame = CGRect(x: 0.0, y: posY, width: mailView.frame.size.width, height: mailView.frame.size.height)
        }
        
        return mailView
    }
}
