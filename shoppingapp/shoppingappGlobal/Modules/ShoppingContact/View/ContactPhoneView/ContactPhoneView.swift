//
//  ContactPhoneView.swift
//  shoppingapp
//
//  Created by Jose Luis Mendoza Espinosa on 30/07/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol ContactPhoneViewDelegate: class {
    func contactPhoneViewSelected(_ contactPhoneView: ContactPhoneView, withPhone phone: String)
}

class ContactPhoneView: UIView {
    
    @IBOutlet weak var contactPhoneImage: UIImageView!
    @IBOutlet weak var contactBancomerLineLabel: UILabel!
    @IBOutlet weak var contactPhoneLabel: UILabel!
    
    weak var delegate: ContactPhoneViewDelegate?
    
    var phone: String = ""
    
    @IBAction func contactSelected(_ sender: Any) {
        
        delegate?.contactPhoneViewSelected(self, withPhone: phone)
    }
    
    static func getContactPhoneView(withPhone phone: String, posY: CGFloat) -> ContactPhoneView {
        
        let className = String(describing: self)
        let phoneView: ContactPhoneView!  = Bundle.main.loadNibNamed(className, owner: nil, options: nil)?.first as? ContactPhoneView
        
        if phoneView != nil {
            
            phoneView.phone = phone
            
            phoneView.contactBancomerLineLabel.text = Localizables.promotions.key_call_line_bbva_text
            phoneView.contactBancomerLineLabel.font = Tools.setFontMedium(size: 14)
            phoneView.contactBancomerLineLabel.textColor = .MEDIUMBLUE
            phoneView.contactBancomerLineLabel.textAlignment = .center
            
            phoneView.contactPhoneLabel.text = phone
            phoneView.contactPhoneLabel.font = Tools.setFontBook(size: 14)
            phoneView.contactPhoneLabel.textColor = .BBVA500
            phoneView.contactPhoneLabel.textAlignment = .center
            
            phoneView.contactPhoneImage.image = ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Common.telephone_bbva_line, color: .MEDIUMBLUE), width: Float(phoneView.contactPhoneImage.frame.size.width), height: Float(phoneView.contactPhoneImage.frame.size.height))
            
            phoneView.frame = CGRect(x: 0.0, y: posY, width: phoneView.frame.size.width, height: phoneView.frame.size.height)
        }
        
        return phoneView
    }
}
