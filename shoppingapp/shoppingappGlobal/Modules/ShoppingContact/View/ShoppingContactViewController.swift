//
//  ShoppingContactView.swift
//  shoppingapp
//
//  Created by Jose Luis Mendoza Espinosa on 30/07/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit
import MessageUI
import Lottie
import CellsNativeCore
import CellsNativeComponents

class ShoppingContactViewController: BaseViewController {
    
    // MARK: Constants
    static let ID: String = String(describing: ShoppingContactViewController.self)
    
    var presenter: ShoppingContactPresenterProtocol!
    
    var lottiAnimation: LOTAnimationView!
    
    // MARK: outlets
    @IBOutlet weak var stackView: UIStackView!
    @IBOutlet weak var animationView: UIView!
    @IBOutlet weak var contactLabel: UILabel!
    
    @IBOutlet weak var stackViewHeight: NSLayoutConstraint!
    
    static let spaceSize: CGFloat = 15.0
    static let separatorWidth: CGFloat = 14.0
    static let separatorHeight: CGFloat = 1.0
    
    // MARK: life cycle
    override func viewDidLoad() {
        
        self.configure(appLifecycle: (UIApplication.shared.delegate as! AppDelegate).appLifeCycle!, id: ShoppingContactViewController.ID)
        
        super.viewDidLoad()
        
        configureView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
        setNavigationBarDefault()
        
        setNavigationBackButtonDefault()
        
        BusManager.sessionDataInstance.disableSwipeLateralMenu()
        
        presenter.viewLoaded()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        super.viewDidAppear(animated)
        
        lottiAnimation.play()
    }
    
    deinit {
        if presenter != nil {
            ComponentManager.remove(id: presenter.routerTag)
        }
        DLog(message: "Deinitialized ShoppingContactViewController")
    }
    
    override func initModel(withModel model: Model) {
        presenter.setModel(model: model)
    }
    
    // Custom methods
    
    func configureView() {
        
        title = Localizables.promotions.key_contact_text
        
        contactLabel.text = Localizables.promotions.key_warning_commerce_dont_apply_text
        contactLabel.font = Tools.setFontBook(size: 14)
        contactLabel.textColor = .BBVA600
        contactLabel.textAlignment = .center
        
        lottiAnimation = LOTAnimationView(name: ConstantsLottieAnimations.contactDetails)
        lottiAnimation?.frame = CGRect(x: 0, y: 0, width: animationView.frame.size.width, height: animationView.frame.size.height)
        lottiAnimation?.contentMode = .scaleAspectFit
        animationView.addSubview(lottiAnimation)
        
        edgesForExtendedLayout = UIRectEdge()
    }
}

extension ShoppingContactViewController: ShoppingContactViewProtocol {
    
    func showContactDetails(details: [ContactDisplayData]) {
        
        var posY: CGFloat = 0
        let posX: CGFloat = (stackView.frame.size.width - ShoppingContactViewController.separatorWidth) / 2.0
        
        for contact in details {
            
            if posY > 0 {
                
                let viewLine = UIView()
                viewLine.backgroundColor = .BBVA200
                
                viewLine.frame = CGRect(x: posX, y: posY, width: ShoppingContactViewController.separatorWidth, height: ShoppingContactViewController.separatorHeight)
                stackView.addSubview(viewLine)
                posY += ShoppingContactViewController.spaceSize
            }
            
            if contact.id == .phone {
                let phoneView = ContactPhoneView.getContactPhoneView(withPhone: contact.info, posY: posY)
                phoneView.delegate = self
                stackView.addSubview(phoneView)
                
                posY += phoneView.frame.size.height + ShoppingContactViewController.spaceSize
            }
            
            if contact.id == .email {
                
                let mailView = ContactMailView.getContactMailView(withMail: contact.info, posY: posY)
                mailView.delegate = self
                stackView.addSubview(mailView)
                
                posY += mailView.frame.size.height + ShoppingContactViewController.spaceSize
            }
        }
        
        stackViewHeight.constant = posY
        stackView.sizeToFit()
        contactLabel.sizeToFit()
    }
    
    func callToPhone(withPhone phone: String) {
        
        let tel = "\(Constants.tel_scheme):\(phone)"
        _ = URLOpener().openURL(tel)
    }
    
    func sendEmail(withEmail email: String) {
        
        _ = Tools.sendEmail(withEmail: email, mailComposeDelegate: self)
    }
    
    func showError(error: ModelBO) {
        
    }
    
    func changeColorEditTexts() {
        
    }
}

extension ShoppingContactViewController: ContactPhoneViewDelegate {
    
    func contactPhoneViewSelected(_ contactPhoneView: ContactPhoneView, withPhone phone: String) {
        
        presenter.pressedCallToPhoneContact(contactPhone: phone)
    }
}

extension ShoppingContactViewController: ContactMailViewDelegate {
    
    func contactMailViewSelected(_ contactMailView: ContactMailView, withMail mail: String) {
        
        presenter.pressedSendEmailContact(contactemail: mail)
    }
}

extension ShoppingContactViewController: MFMailComposeViewControllerDelegate {
    
    public func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
}

// MARK: - APPIUM

private extension ShoppingContactViewController {
    
    func setAccessibilities() {
        
        #if APPIUM
        
        #endif
        
    }
        
}
