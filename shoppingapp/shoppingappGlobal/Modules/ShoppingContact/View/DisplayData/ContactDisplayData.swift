//
//  ContactDisplayData.swift
//  shoppingapp
//
//  Created by Jose Luis Mendoza Espinosa on 30/07/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

enum DisplayContact {
    case phone
    case email
}

struct ContactDisplayData {
    
    var id: DisplayContact
    var info: String
    
    static func generateContactDisplayData(fromPromotion promotion: PromotionBO) -> [ContactDisplayData] {
        
        var contactsData = [ContactDisplayData]()
        
        if let contactDetails = promotion.contactDetails {
            
            for contactDetail in contactDetails {
                
                if let id = contactDetail.contactType?.id, let contactInfo = contactDetail.contact,
                    id == .phone || id == .mobile || id == .email {
                    
                    let displayContact = id == .email ? DisplayContact.email : DisplayContact.phone
                    
                    contactsData.append(ContactDisplayData(id: displayContact, info: contactInfo))
                }
            }
        }
        
        if contactsData.isEmpty {
            
            contactsData.append(ContactDisplayData(id: .phone, info: Settings.Global.default_bbva_phone_number))
        }
        
        return contactsData
    }
}

extension ContactDisplayData: Equatable {
    public static func == (lhs: ContactDisplayData, rhs: ContactDisplayData) -> Bool {
        
        return lhs.id == rhs.id && lhs.info == rhs.info
        
    }
}
