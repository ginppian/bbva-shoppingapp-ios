//
//  ShoppingContactContract.swift
//  shoppingapp
//
//  Created by Jose Luis Mendoza Espinosa on 30/07/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import UIKit
import RxSwift

protocol ShoppingContactViewProtocol: ViewProtocol {
    
    func showContactDetails(details: [ContactDisplayData])
    func callToPhone(withPhone phone: String)
    func sendEmail(withEmail email: String)
}

protocol ShoppingContactPresenterProtocol: PresenterProtocol {
    
    func viewLoaded()
    func pressedCallToPhoneContact(contactPhone phone: String)
    func pressedSendEmailContact(contactemail email: String)
}

protocol ShoppingContactInteractorProtocol {
    
}
