//
//  CVVContract.swift
//  shoppingappMX
//
//  Created by Daniel Garcia on 15/01/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit
import RxSwift

protocol CVVInteractorProtocol: InteractorProtocol {
    func provideCVVForCard(withID cardId: String) -> Observable<ModelBO>
}

protocol CVVPresenterProtocol: PresenterProtocol {
    func getCvv()
    func getNewCvv()
    func updateTimer()
    func closeView()
    func acceptErrorModal()
}

protocol CVVViewProtocol: ViewProtocol {
    func updateCVV(withValue cvv: String)
    func startTimer()
    func updateTimer(withText text: String)
    func finishTimer()
    func reloadView()
    func updateView()
}
