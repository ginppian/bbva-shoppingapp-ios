//
//  CardCVVContractA.swift
//  shoppingappMX
//
//  Created by Daniel Garcia on 29/05/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol CVVPresenterProtocolA: CVVPresenterProtocol {
    func getCVVTime()
}

protocol CVVViewProtocolA: CVVViewProtocol {
    func addLoadingDots()
    func removeLoadingDots()
}
