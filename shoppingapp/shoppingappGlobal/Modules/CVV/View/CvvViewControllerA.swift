//
//  CvvViewControllerA.swift
//  shoppingappMX
//
//  Created by Daniel Garcia on 29/05/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import Lottie

final class CvvViewControllerA: CvvViewController {

    var cvvCircleView = UIView()
    var cvvValue = UILabel()
    let cvvLabel = UILabel()
    var finalCircleFrame = CGRect()
    var finalPosition = CGPoint()
    var lottiDots: LOTAnimationView!
    lazy var cardImageView = UIImageView()
    
    //constants
    let innerCircleSize = CGSize(width: 166, height: 166)
    let cvvPlaceHolderPosition = CGPoint(x: 163, y: 63)
    override var sizeForValueLabel: CGFloat {
        return CGFloat(52.0)
    }

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var layoutBottom: NSLayoutConstraint!
    @IBOutlet weak var layoutVertical: NSLayoutConstraint!
    
    @IBAction func closeButtonAction(_ sender: Any) {
        dismissAction()
    }
    
    deinit {
        DLog(message: "Deinitialized CVVViewControllerA")
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        cvvCircleView.layer.shouldRasterize = false
        cvvValue.layer.shouldRasterize = false
        setScrollView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        displayCVVView(cardReferecnce: cardImageView.frame)
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        cvvCircleView.removeFromSuperview()
        finishTimer()
    }

    override func initModel(withModel model: Model) {
        presenter.setModel(model: model)
    }

    override func configureNavigationBar() {
        super.configureNavigationBar()
        let image = SVGCache.image(forSVGNamed: ConstantsImages.Common.close_icon, color: .BBVAWHITE)
        setNavigationLeftButton(withImage: image, action: #selector(dismissAction))
        setNavigationTitle(withText: Localizables.cards.key_card_see_cvv_text)
    }
    
    private func setScrollView() {
        
        scrollView.delegate = self
        layoutBottom.constant = -4
        layoutVertical.constant = 236
    }
    
    // MARK: CVV Overrides
    
    @objc override func seeCVV() {
        cvvValue.text = nil
        expandCVVCircle()
        super.seeCVV()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        let touched = touches.first!.view
        if touched == timerView {
            timerLabel.textColor = .BBVAWHITELIGHTBLUE
            timerImage.image = SVGCache.image(forSVGNamed: ConstantsImages.Common.visualize_icon_image, color: .BBVAWHITELIGHTBLUE)
        }
    }
    
    override func setUpVariations() {
        
        (presenter as! CVVPresenterProtocolA).getCVVTime()
        finalCircleFrame = CGRect(center: cvvPlaceHolderPosition, size: innerCircleSize)
        
        valueLabel.text = ""
        valueLabel.isHidden = true
        titleLabel.isHidden = true
        view.backgroundColor = .MEDIUMBLUE
        timerLabel.textColor = .BBVAWHITE
        messageLabel.textColor = .BBVAWHITE
        closeButton.makeColor(.DARKCOREBLUE).animate()
        closeButton.setBackgroundColor(color: .BBVALIGHTBLUE, forUIControlState: .highlighted)
        timerImage.image = SVGCache.image(forSVGNamed: ConstantsImages.Common.clock_icon, color: .BBVAWHITE)
        
        cvvValue.frame = CGRect(x: 0, y: 10, width: finalCircleFrame.width, height: finalCircleFrame.height)
        cvvValue.layer.zPosition = 400
        cvvValue.textAlignment = .center
        cvvValue.font = Tools.setFontLight(size: sizeForValueLabel)
        cvvValue.textColor = .black
        
        cvvCircleView.frame = finalCircleFrame
        cvvCircleView.backgroundColor = .BBVAWHITE
        cvvCircleView.layer.cornerRadius = innerCircleSize.width / 2
        finalCircleFrame = CGRect(center: cvvPlaceHolderPosition, size: innerCircleSize)
        
        cvvLabel.frame = CGRect(x: 0, y: (cvvValue.frame.minY - 50.0), width: finalCircleFrame.width, height: finalCircleFrame.height)
        cvvLabel.layer.zPosition = 400
        cvvLabel.text = Localizables.cards.key_card_your_cvv_text
        cvvLabel.textAlignment = .center
        cvvLabel.font = Tools.setFontBold(size: sizeFormessage)
        cvvLabel.textColor = .black
        
        let translucentCircle = UIView(frame: CGRect(center: cvvCircleView.bounds.center, size: CGSize(width: cvvCircleView.bounds.width + 40, height: cvvCircleView.bounds.height + 40)))
        translucentCircle.alpha = 0.5
        translucentCircle.backgroundColor = .BBVAWHITE
        translucentCircle.layer.cornerRadius = translucentCircle.bounds.width / 2
        cvvCircleView.insertSubview(translucentCircle, belowSubview: cvvCircleView)
        cvvCircleView.addSubview(cvvValue)
        cvvCircleView.addSubview(cvvLabel)
    }

    // MARK: Animation
    
    private func displayCVVView(cardReferecnce: CGRect) {
        
        finalPosition = CGPoint(x: cardReferecnce.bounds.midX, y: cardReferecnce.bounds.maxY - (cardReferecnce.bounds.maxY) / 8)
        cvvCircleView.transform = CGAffineTransform.identity.scaledBy(x: 0, y: 0)
        scrollView.addSubview(cardImageView)
        cardImageView.addSubview(cvvCircleView)
        cardImageView.moveY(-40).easing(.easeOut).animate()
        expandCVVCircle()

    }
    
    private func expandCVVCircle() {
        cvvCircleView.transform = CGAffineTransform.identity.scaledBy(x: 0, y: 0)
        UIView.animate(withDuration: 0.5) { [weak self] in
            guard let `self` = self else {
                return
            }
            self.cvvCircleView.transform = CGAffineTransform.identity
            self.cvvCircleView.frame.origin.x = (self.finalPosition.x - self.cvvCircleView.bounds.width / 2)
            self.cvvCircleView.frame.origin.y = (self.finalPosition.y - self.cvvCircleView.bounds.width / 2)
        }
    }
    
    // MARK: CvvView Protocol
    
    override func updateCVV(withValue cvv: String) {
        lottiDots.removeFromSuperview()
        cvvValue.text = cvv
    }
    
    override func finishTimer() {
        alarmView.isHidden = false

        cardImageView.moveY(40).easing(.easeOut).animate()
        
        UIView.animate(withDuration: 0.5, animations: {
            self.cvvCircleView.scaleXY(0, 0).duration(0.5).animate()
            self.cvvCircleView.moveTo(self.cvvPlaceHolderPosition).animate()
        })

        super.finishTimer()
    }

    override func updateView() {

        UIView.animate(withDuration: visibilityTransitionTime, delay: 0, options: .curveEaseIn, animations: {
            self.view.backgroundColor = UIColor.COREBLUE
            self.closeButton.makeColor(.MEDIUMBLUE).animate()
            self.closeButton.setBackgroundColor(color: .BBVALIGHTBLUE, forUIControlState: .highlighted)

            let lottiCGRect = CGRect(
                x: (self.timerView.frame.width / 2) - 15, //'-15', so asset looks centered
                y: (self.timerView.frame.height / 2) - 2,
                width: 0, height: 0)

            self.timerImage
                .makeSize(CGSize(width: 0, height: 0))
                .makeAlpha(0)
                .easing(.easeIn)
                .duration(self.visibilityTransitionTime)
                .animate()

            self.lottiAlarm.makeFrame(lottiCGRect).makeAlpha(self.alphaToOpaque).then()
                .duration(0.2)
                .makeSize(CGSize(width: (1.2 * self.alarmView.bounds.size.width), height: (1.2 * self.alarmView.bounds.size.height))) //'1.2' proportion, so asset looks well
                .easing(.linear)
                .animate()
            self.lottiAlarm.play()
            self.view.layoutIfNeeded()
        })
    }

    override func reloadView() {
        cardImageView.moveY(-40).easing(.easeOut).animate()
        UIView.animate(withDuration: 0.5) { [weak self] in
            guard let `self` = self else {
                return
            }
            self.timerLabel.textColor = .BBVAWHITE
            self.timerImage.image = SVGCache.image(forSVGNamed: ConstantsImages.Common.clock_icon, color: .BBVAWHITE)

            self.view.backgroundColor = .MEDIUMBLUE

            self.closeButton.backgroundColor = .DARKCOREBLUE
            self.cvvCircleView.transform = CGAffineTransform.identity
            let circleWidth = self.cvvCircleView.bounds.width
            self.cvvCircleView.frame.origin.x = ((self.finalPosition.x) - circleWidth / 2)
            self.cvvCircleView.frame.origin.y = ((self.finalPosition.y) - circleWidth / 2)
        }
    }
    
}

// MARK: - FlipCardAnimatorFinisherProtocol

extension CvvViewControllerA: FlipCardAnimatorFinisherProtocol {
    
    final func prepareFinishCardImageView(withFrame frame: CGRect, from imageURL: String) {
        
        let navigationBarHeight = navigationController?.completeHeight() ?? 0.0
        cardImageView.frame = CGRect(x: frame.origin.x, y: frame.origin.y - navigationBarHeight, width: frame.width, height: frame.height)
        cardImageView.image = CardImageUtils.placeholderBackImage
    }
}

extension CvvViewControllerA: CVVViewProtocolA {
    
    final func addLoadingDots() {
        lottiDots = LOTAnimationView(name: ConstantsLottieAnimations.cvvLoadingDots)
        let centerDots = CGPoint(x: cvvValue.frame.center.x + 10, y: cvvValue.frame.center.y)
        lottiDots?.makeFrame(CGRect(center: centerDots, size: CGSize(width: cvvValue.bounds.width, height: cvvValue.bounds.height))).animate()
        lottiDots?.contentMode = .scaleAspectFit
        lottiDots?.loopAnimation = true
        lottiDots?.play()
        cvvCircleView.addSubview(lottiDots!)
    }
    
    final func removeLoadingDots() {
        lottiDots?.removeFromSuperview()
    }

}
extension CvvViewControllerA: UIScrollViewDelegate {
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
    }
}
