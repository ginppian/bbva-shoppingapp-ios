//
//  CvvViewController.swift
//  shoppingappMX
//
//  Created by Daniel Garcia on 09/01/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Lottie
import CellsNativeComponents

class CvvViewController: BaseViewController, CVVViewProtocol {

    //Animation Constants
    let locationsForAquaGradient: [NSNumber] = [0.0, 0.75]
    var sizeForValueLabel: CGFloat {
            return CGFloat(72.0)
    }
    let sizeFormessage: CGFloat = 14.0
    let sizeFortimerLabel: CGFloat = 20.0
    let birdsFrameLocation = CGPoint(x: 0, y: -100)
    let timerLabelwidth: CGFloat = 84
    let timerRotationDegrees: CGFloat = CGFloat(Double.pi / 2)
    let alphaToZero: CGFloat = 0
    let alphaToOpaque: CGFloat = 1
    let visibilityTransitionTime: Double = 0.5

    static let dots = "•••"

    //Gradients
    fileprivate var BBVACvvGradientAquaColor: [CGColor] {
        return [UIColor(red: 208 / 255.0, green: 239 / 255.0, blue: 240 / 255.0, alpha: 1.0).cgColor,
                UIColor(red: 254 / 255.0, green: 245 / 255.0, blue: 220 / 255.0, alpha: 1.0).cgColor]
    }
    fileprivate var BBVACvvGradientLightColor: [CGColor] {
        return [UIColor(red: 70 / 255.0, green: 157 / 255.0, blue: 216 / 255.0, alpha: 1.0).cgColor,
                UIColor(red: 135 / 255.0, green: 205 / 255.0, blue: 255 / 255.0, alpha: 1.0).cgColor]
    }
    fileprivate var BBVACvvGradientNightColor: [CGColor] {
        return [UIColor(red: 0 / 255.0, green: 68 / 255.0, blue: 129 / 255.0, alpha: 1.0).cgColor,
                UIColor(red: 42 / 255.0, green: 134 / 255.0, blue: 202 / 255.0, alpha: 1.0).cgColor]
    }

    //IBOutlets
    @IBOutlet weak var timerImage: UIImageView!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var valueLabel: UILabel!
    @IBOutlet weak var timerLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var principalView: UIView!
    @IBOutlet weak var timerView: UIView!
    @IBOutlet weak var alarmView: UIView!
    @IBOutlet weak var timeStackView: UIStackView!

    static let ID = String(describing: CvvViewController.self)
    weak var presenter: CVVPresenterProtocol!
    private var gradientLayer = CAGradientLayer()
    var timer = Timer()

    //LOTTIE
    var lottiAlarm: LOTAnimationView!
    private var aquaBirds: LOTAnimationView!
    private var blueBirds: LOTAnimationView!

    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        configureNavigationBar()
        BusManager.sessionDataInstance.disableSwipeLateralMenu()

        TrackerHelper.sharedInstance().trackCardCVV(withCustomerID: SessionDataManager.sessionDataInstance().customerID)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        removeBirdsAndGradient()
    }

    private func removeBirdsAndGradient() {
        guard let `aquaBirds` = aquaBirds, let `blueBirds` = blueBirds else {
            return
        }
        
        if aquaBirds.isAnimationPlaying {
            aquaBirds.removeFromSuperview()
        }
        if blueBirds.isAnimationPlaying {
            blueBirds.removeFromSuperview()
        }
        view.layer.sublayers?.remove(at: 0)
        timer.invalidate()
    }

    func configureView() {
        setUpCommonElements()
        setUpVariations()
        presenter.getCvv()
    }

    override func initModel(withModel model: Model) {
        presenter.setModel(model: model)
    }

    deinit {
        if let presenter = presenter {
            ComponentManager.remove(id: presenter.routerTag)
        }
        DLog(message: "Deinitialized CVVViewController")
    }

    private func generateGradientLayer(colors arrayColors: [CGColor], frame frameCGRect: CGRect,
                                       locations arraylocation: [NSNumber]) -> CAGradientLayer {
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = frameCGRect
        gradientLayer.colors = arrayColors
        gradientLayer.locations = arraylocation

        return gradientLayer
    }

    @objc func timerShouldChange() {
        presenter.updateTimer()
    }

    @IBAction func CloseButtonAction(_ sender: Any) {
        dismissAction()
    }

    func updateCVV(withValue cvv: String) {
        valueLabel.text = cvv
    }

    func showError(error: ModelBO) {
        presenter.showModal()
    }

    func changeColorEditTexts() { }

    func startTimer() {
        timer.invalidate()
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,
                     selector: (#selector(CvvViewController.timerShouldChange)), userInfo: nil, repeats: true)
    }

    func updateTimer(withText text: String) {
        timerLabel.text = text
    }

    func finishTimer() {
        timer.invalidate()

        valueLabel.rotateX(timerRotationDegrees).completion({
            self.valueLabel.text = CvvViewController.dots
        }).then().rotateX(-timerRotationDegrees).animate()

        timerLabel.bounds.size.width = timerLabelwidth
        timerLabel.makeAlpha(alphaToZero).duration(visibilityTransitionTime).animate()
        timerLabel.text = Localizables.cards.key_card_see_cvv_text

        timerLabel.textColor = .BBVALIGHTBLUE
        timerLabel.textAlignment = .left
        timerLabel.makeAlpha(alphaToOpaque).duration(visibilityTransitionTime).animate()

        timerImage.image = SVGCache.image(forSVGNamed: ConstantsImages.Common.visualize_icon_image, color: .BBVALIGHTBLUE)
        timerImage.rotateX(timerRotationDegrees).animate()
        timerImage.contentMode = .scaleAspectFit
        lottiAlarm.makeAlpha(alphaToZero).duration(visibilityTransitionTime).animate()
        timerImage.makeAlpha(alphaToOpaque).then().rotateX(-timerRotationDegrees).animate()

        let gesture = UITapGestureRecognizer(target: self, action: #selector(seeCVV))

        timerView.isUserInteractionEnabled = true
        timerView.addGestureRecognizer(gesture)
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        
        if let touched = touches.first?.view, touched == timerView {
            timerLabel.textColor = .COREBLUE
            timerImage.image = SVGCache.image(forSVGNamed: ConstantsImages.Common.visualize_icon_image, color: .COREBLUE)
        }
    }

    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesCancelled(touches, with: event)
        let touched = touches.first!.view
        if touched == timerView {
            timerLabel.textColor = .BBVALIGHTBLUE
            timerImage.image = SVGCache.image(forSVGNamed: ConstantsImages.Common.visualize_icon_image, color: .BBVALIGHTBLUE)
        }
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        let touched = touches.first!.view
        if touched == timerView {
            timerLabel.textColor = .BBVALIGHTBLUE
            timerImage.image = SVGCache.image(forSVGNamed: ConstantsImages.Common.visualize_icon_image, color: .BBVALIGHTBLUE)
        }

    }

    @objc func seeCVV() {
        let gesture = UITapGestureRecognizer(target: self, action: #selector(seeCVV))
        timerView.isUserInteractionEnabled = false
        timerView.removeGestureRecognizer(gesture)

        presenter.getNewCvv()
    }

    func reloadView() {
        alarmView.isHidden = true
        removeBirdsAndGradient()
        setUpCommonElements()
        setUpVariations()
    }

    func updateView() {

        alarmView.isHidden = false
        gradientLayer = view.layer.sublayers?[0] as! CAGradientLayer
        let toLightTransitionLayer = CABasicAnimation(keyPath: "colors")
        toLightTransitionLayer.duration = visibilityTransitionTime
        toLightTransitionLayer.fromValue = BBVACvvGradientAquaColor
        toLightTransitionLayer.toValue = BBVACvvGradientLightColor
        gradientLayer.add(toLightTransitionLayer, forKey: nil)

        UIView.animate(withDuration: visibilityTransitionTime, delay: 0, options: .curveEaseIn, animations: { [unowned self] in
            self.initalUpdateAnimation()
        },
        completion: { _ in
            self.completionUpdateAnimation()
        })

    }

    private func initalUpdateAnimation() {
        aquaBirds.removeFromSuperview()
        valueLabel.makeAlpha(0.2).animate()
        valueLabel.makeAlpha(0.2).animate()
        messageLabel.makeAlpha(0.1).animate()
        timerLabel.makeAlpha(0.1).animate()
        titleLabel.makeAlpha(0.1).animate()

        let lottiCGRect = CGRect(
            x: (timerView.frame.width / 2) - 15, //'-15', so asset looks centered
            y: (timerView.frame.height / 2) - 2,
            width: 0, height: 0)

        lottiAlarm.makeFrame(lottiCGRect).makeAlpha(alphaToOpaque).then()
            .duration(0.2)
            .makeSize(CGSize(width: (1.2 * alarmView.bounds.size.width), height: (1.2 * alarmView.bounds.size.height))) //'1.2' proportion, so asset looks well
            .easing(.linear)
            .animate()
        lottiAlarm.play()

        view.layoutIfNeeded()
    }

    private func completionUpdateAnimation() {
        timerImage
            .makeSize(CGSize(width: 0, height: 0))
            .makeAlpha(0)
            .easing(.easeIn)
            .duration(visibilityTransitionTime)
            .animate()

        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + 0.4) {
            UIView.animate(withDuration: self.visibilityTransitionTime, delay: 0, options: .curveEaseOut, animations: {
                self.valueLabel.textColor = .BBVAWHITE
                self.gradientLayer.colors = self.BBVACvvGradientNightColor
                self.timerLabel.textColor = .BBVAWHITE
                self.timerLabel.textAlignment = .left
                self.messageLabel.textColor = .BBVAWHITE
                self.titleLabel.textColor = .BBVALIGHTBLUE

                self.closeButton.makeColor(.COREBLUE).animate()
                self.closeButton.setBackgroundColor(color: .BBVALIGHTBLUE, forUIControlState: .highlighted)
                self.valueLabel.makeAlpha(self.alphaToOpaque).animate()
                self.valueLabel.makeAlpha(self.alphaToOpaque).animate()
                self.messageLabel.makeAlpha(self.alphaToOpaque).animate()
                self.timerLabel.makeAlpha(self.alphaToOpaque).animate()
                self.titleLabel.makeAlpha(self.alphaToOpaque).animate()

                self.valueLabel.makeAlpha(self.alphaToOpaque).animate()
                self.blueBirds.play()
                super.self().view.layoutIfNeeded()
            })
        }
    }

    // MARK: View Setup

    func configureNavigationBar() {
        setNavigationBarDefault()

        let image = SVGCache.image(forSVGNamed: ConstantsImages.Common.close_icon, color: .BBVAWHITE)
        setNavigationLeftButton(withImage: image, action: #selector(dismissAction))
        setNavigationTitle(withText: Localizables.cards.key_card_see_cvv_text)
    }

    func setUpVariations() {
        
        var aquaGradient = generateGradientLayer(colors: BBVACvvGradientAquaColor,
                                              frame: UIScreen.main.bounds,
                                              locations: locationsForAquaGradient)
        view.layer.insertSublayer(aquaGradient, below: view.layer.sublayers?[0])
        if let sublayer = view.layer.sublayers?[0], let caSublayer = sublayer as? CAGradientLayer {
            aquaGradient = caSublayer
        }
        
        aquaGradient.colors = BBVACvvGradientAquaColor

        aquaBirds = LOTAnimationView(name: ConstantsLottieAnimations.aquaBirds)
        aquaBirds.makeFrame(CGRect(x: birdsFrameLocation.x,
                                   y: birdsFrameLocation.y,
                                   width: UIScreen.main.bounds.width,
                                   height: UIScreen.main.bounds.height)).animate()
        aquaBirds.contentMode = .scaleAspectFit
        aquaBirds.loopAnimation = true

        blueBirds = LOTAnimationView(name: ConstantsLottieAnimations.blueBirds)
        blueBirds.makeFrame(CGRect(x: birdsFrameLocation.x,
                                   y: birdsFrameLocation.y,
                                   width: UIScreen.main.bounds.width,
                                   height: UIScreen.main.bounds.height)).animate()
        blueBirds.contentMode = .scaleAspectFit
        blueBirds.loopAnimation = true

        view.insertSubview(aquaBirds, belowSubview: principalView)
        view.insertSubview(blueBirds, belowSubview: principalView)

        aquaBirds.play()

    }

    func setUpCommonElements() {
        lottiAlarm = LOTAnimationView(name: ConstantsLottieAnimations.bell)
        lottiAlarm.makeFrame(CGRect(x: alarmView.frame.width / 2,
                                    y: alarmView.frame.height / 2,
                                    width: 0,
                                    height: 0)).animate()
        lottiAlarm.contentMode = .scaleAspectFit
        lottiAlarm.loopAnimation = true

        titleLabel.textAlignment = .center
        titleLabel.textColor = .DARKMEDIUMBLUE
        titleLabel.font = Tools.setFontBold(size: sizeFormessage)
        titleLabel.text = Localizables.cards.key_card_your_cvv_text

        valueLabel.textColor = .NAVY
        valueLabel.textAlignment = .center
        valueLabel.text = CvvViewController.dots
        valueLabel.font = Tools.setFontLight(size: sizeForValueLabel)

        timerLabel.textAlignment = .center
        timerLabel.textColor = .DARKMEDIUMBLUE
        timerLabel.font = Tools.setFontBold(size: sizeFortimerLabel)

        messageLabel.textColor = .NAVY
        messageLabel.textAlignment = .center
        messageLabel.font = Tools.setFontBook(size: sizeFormessage)
        messageLabel.text = Localizables.cards.key_card_cvv_validation_text

        timerImage.transform = CGAffineTransform(rotationAngle: 0)
        timerImage.image = SVGCache.image(forSVGNamed: ConstantsImages.Common.clock_icon, color: .DARKMEDIUMBLUE)

        closeButton.makeColor(.MEDIUMBLUE).animate()
        closeButton.titleLabel?.textAlignment = .center
        closeButton.setTitleColor(.BBVAWHITE, for: .normal)
        closeButton.titleLabel?.font = Tools.setFontBold(size: sizeFormessage)
        closeButton.setTitle(Localizables.common.key_close_text, for: .normal)
        closeButton.setBackgroundColor(color: .DARKCOREBLUE, forUIControlState: .highlighted)

        alarmView.addSubview(lottiAlarm)
        timerView.isUserInteractionEnabled = false
        timeStackView.bringSubview(toFront: alarmView)
    }

}

extension CvvViewController: CloseModalDelegate {

    func errorViewControllerAcceptPressed(_ errorViewController: ErrorViewController) {
        presenter.acceptErrorModal()
    }

    func errorViewControllerCancelPressed(_ errorViewController: ErrorViewController) {
    }

}
