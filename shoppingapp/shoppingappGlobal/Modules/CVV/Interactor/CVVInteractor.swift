//
//  CVVInteractor.swift
//  shoppingapp
//
//  Created by Daniel Garcia on 29/01/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import RxSwift

final class CVVInteractor {

    var cvvDataManager = CVVDataManager.sharedInstance
    var cryptoDataManager = LibCryptoDataManager.sharedInstance

    let disposeBag = DisposeBag()

    deinit {
        DLog(message: "CVV CVVInteractor")
    }
}

extension CVVInteractor: CVVInteractorProtocol {

    final func provideCVVForCard(withID cardId: String) -> Observable<ModelBO> {

        let cvvGeneration = cryptoDataManager.generateKeyPair(with: nil).flatMap { [weak self] pubKey -> Observable<CVVEntity> in
            guard let `self` = self else {
                return Observable.empty()
            }
            return self.cvvDataManager.provideCvv(forCardId: cardId, publicKey: pubKey.value)
        }

        return Observable.create { observer in
            cvvGeneration.subscribe(
                onNext: { [weak self] cvvEntity in
                    guard let `self` = self else {
                        return
                    }
                    guard var cardCvv = cvvEntity.cardCVV, !cardCvv.code.isEmpty else {
                        let errorBVABO = ErrorBO(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text))
                        observer.onError(ServiceError.GenericErrorBO(error: errorBVABO))
                        return
                    }

                    self.cryptoDataManager.decryptFromHost(hostData: cardCvv.code).subscribe(
                        onNext: { cvv in
                            let cvvValue = String(cvv.data.prefix(5).dropFirst(2))

                            cardCvv.code = cvvValue
                            cvvEntity.cardCVV = cardCvv

                            let cvvBO = CVVBO(cvvEntity: cvvEntity)
                            observer.onNext(cvvBO)
                            observer.onCompleted()
                        },
                        onError: { error in
                            observer.onError(Tools.evaluateError(with: error))
                        }).addDisposableTo(self.disposeBag)

                },
                onError: { error in
                    observer.onError(Tools.evaluateError(with: error))
                },
                onCompleted: {
                    observer.onCompleted()
                }).addDisposableTo(self.disposeBag)

            return Disposables.create()
        }
    }
}
