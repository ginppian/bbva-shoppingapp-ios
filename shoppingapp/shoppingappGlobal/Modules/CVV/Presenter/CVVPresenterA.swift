//
//  CardCVVPresenterA.swift
//  shoppingappMX
//
//  Created by Daniel Garcia on 29/05/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

final class CVVPresenterA<T: CVVViewProtocolA>: CVVPresenter<T> {
    
    deinit {
        DLog(message: "Deinited CVVPresenterA")
    }
    
    override func closeView() {
        if isUpdatingCVV == false {
            busManager.dismissViewController(animated: true)
        }
    }
    
    override func showLoading(completion: (() -> Void)?) {
        view?.addLoadingDots()
    }
    
    override func hideLoading(completion: (() -> Void)?) {
        completion?()
        if completion == nil {
            view?.removeLoadingDots()
        }
    }
    
    override func acceptErrorModal() {
        guard let errorBO = errorBO else {
            return
        }
        if errorBO.sessionExpired() {
            busManager.setRootWhenNoSession()
            busManager.dismissViewController(animated: false)
            SessionDataManager.sessionDataInstance().closeSession()
        } else {
            closeView()
        }
    }

}

extension CVVPresenterA: CVVPresenterProtocolA {
    func getCVVTime() {
        let formattedTime = calculateMinSecond(forSeconds: Int(timeForTimerInSeconds))
        view?.updateTimer(withText: formattedTime)
    }

}
