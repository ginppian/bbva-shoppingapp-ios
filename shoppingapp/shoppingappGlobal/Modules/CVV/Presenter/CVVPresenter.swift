//
//  CVVPresenter.swift
//  shoppingappMX
//
//  Created by Daniel Garcia on 10/01/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import RxSwift
import CellsNativeComponents
import CellsNativeCore

class CVVPresenter<T: CVVViewProtocol>: BasePresenter<T> {

    let EVENT_CARD_BO: String = "card-bo-bind"
    let ROUTER_TAG: String = "cvv-tag"

    let disposeBag = DisposeBag()
    var interactor: CVVInteractorProtocol!
    
    var cardID: String?
    var remainingTime = Double(0)
    var alertTime = Settings.CardCvv.time_alert_for_show_cvv
    var timeForTimerInSeconds = Settings.CardCvv.time_duration_for_show_cvv
    var isUpdatingCVV = false

    required init() {
        super.init(tag: ROUTER_TAG)
    }
    
    override init(busManager: BusManager) {
        super.init(busManager: busManager)
    }

    override func setModel(model: Model) {
        guard let CvvTransport = model as? CVVTransport else {
            checkError()
            return
        }
        cardID = CvvTransport.cardId
    }

    func calculateMinSecond(forSeconds seconds: Int) -> String {
        let minutes = seconds / 60 % 60
        let second = seconds % 60
        return String(format: "%02i:%02i", minutes, second)
    }

    func createAndProcessGenericError(message: String, errorType: ErrorType, dismissModal: Bool) {
        errorBO = ErrorBO( message: message, errorType: errorType, allowCancel: false)

        if dismissModal == true {
            hideLoading(completion: {
                self.checkError()
            })
        } else {
            checkError()
        }
    }

    func updateTimer() {
        if remainingTime >= 1 {
            remainingTime -= 1
        }
        if remainingTime == alertTime {
            view?.updateView()
        }
        if remainingTime == 0 {
            view?.finishTimer()
        } else {
            view?.updateTimer(withText: String(describing: calculateMinSecond(forSeconds: Int(remainingTime))))
        }
    }
    
    deinit {
        DLog(message: "CVV Presenter")
    }

    final func getCvv() {
        
        showLoading(completion: nil)

        guard let cardId = cardID else {
            DLog(message: "Can't get card ID")
            createAndProcessGenericError(message: Localizables.common.key_common_generic_error_text, errorType: .warning, dismissModal: false)
            return
        }

        interactor.provideCVVForCard(withID: cardId).subscribe(
            onNext: { [weak self] result in
                guard let `self` = self else {
                    return
                }
                if let cvvBO = result as? CVVBO {

                    guard let cardCVV = cvvBO.cardCVV else {
                        self.createAndProcessGenericError(message: Localizables.common.key_common_generic_error_text, errorType: .warning, dismissModal: true)
                        return
                    }

                    self.remainingTime = self.timeForTimerInSeconds
                    self.view?.updateTimer(withText: (self.calculateMinSecond(forSeconds: Int(self.remainingTime))))
                    self.view?.updateCVV(withValue: cardCVV.code)
                    self.view?.startTimer()
                    self.hideLoading(completion: nil)

                    if self.isUpdatingCVV == true {
                        self.view?.reloadView()
                    }

                }
            },
            onError: { [weak self] error in

                guard let `self` = self else {
                    return
                }
                
                if let evaluate = error as? ServiceError {
                    switch evaluate {
                    case .GenericErrorBO(let errorBO):
                        self.errorBO = errorBO
                        self.hideLoading(completion: {
                            self.checkError()
                       })
                    default:
                        break
                    }
                }
            }
        ).addDisposableTo(disposeBag)
    }

    final func getNewCvv() {
        isUpdatingCVV = true
        getCvv()
    }

    func closeView() {
        if isUpdatingCVV == false {
            busManager.back()
        }
    }
    
    func acceptErrorModal() {
        guard let errorBO = errorBO else {
            return
        }
        if errorBO.sessionExpired() == true {
            busManager.setRootWhenNoSession()
            SessionDataManager.sessionDataInstance().closeSession()
            busManager.back()
        } else {
            closeView()
        }
    }
}

extension CVVPresenter: CVVPresenterProtocol {
}
