//
//  WelcomeViewController.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 23/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit
import CellsNativeCore
import CellsNativeComponents
import Component_UI_Welcome
import CellsNativeWidgets

class WelcomeViewController: BaseViewController {
    
    // MARK: Constants

    static let ID = String(describing: WelcomeViewController.self)
    static let FIRST_INIT_COMPONENT = "firstInit"
    static let UPDATE_COMPONENT = "update"

    var presenter: WelcomePresenterProtocol!

    @IBOutlet weak var welcomeView: UIView!
    var welcomeComponent: WelcomeView?
    
    // MARK: life cycle

    override func viewDidLoad() {
        
        presenter.viewDidLoad()
        
        configure(appLifecycle: (UIApplication.shared.delegate as! AppDelegate).appLifeCycle!, id: WelcomeViewController.ID)

        super.viewDidLoad()

        setNavigationBarTransparent()
    }
    
    fileprivate func configureWelcomeComponent(forTagName tagName: String) {
        
        welcomeComponent = UIView.fromNib(superView: welcomeView, tagName: tagName) as WelcomeView
        welcomeComponent?.configureView(viewController: self, theme: ShoppingTheme())
    }
    
    deinit {
        ComponentManager.remove(id: presenter.routerTag)
        DLog(message: "Deinitialized WelcomeViewController")
    }
}

// MARK: - WelcomeViewProtocol
extension WelcomeViewController: WelcomeViewProtocol {
    
    func showError(error: ModelBO) {
    }

    func changeColorEditTexts() {
    }
    
    func showFirstInit() {
        
        configureWelcomeComponent(forTagName: WelcomeViewController.FIRST_INIT_COMPONENT)
    }
    
    func showUpdate() {
        
        configureWelcomeComponent(forTagName: WelcomeViewController.UPDATE_COMPONENT)
    }
}

// MARK: - WelcomePageReaction
extension WelcomeViewController {
    
    func acceptPressed() {
        
        presenter.acceptPressed()
    }
}
