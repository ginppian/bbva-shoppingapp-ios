//
//  WelcomeContract.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 23/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit

protocol WelcomeViewProtocol: ViewProtocol {

    func showFirstInit()
    func showUpdate()
}

protocol WelcomePresenterProtocol: PresenterProtocol {

    func viewDidLoad()
    func acceptPressed()
}
