//
//  WelcomePageReaction.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 23/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import CellsNativeCore
import Component_UI_Welcome

class WelcomePageReaction: PageReaction {

    static let ROUTER_TAG: String = "welcome-tag"

    override func configure() {

        finishFirstInit()
        finishUpdate()
    }

    func finishFirstInit() {
        
        _ = when(id: WelcomeViewController.FIRST_INIT_COMPONENT, type: Void.self)
            .doAction(actionSpec: WelcomeViewModel.actionButtonClick)
            .then()
            .inside(componentID: WelcomeViewController.ID, component: WelcomeViewController.self)
            .doReaction(reactionExecution: { component, _ in
                component.acceptPressed()
            })
    }
    
    func finishUpdate() {
        
        _ = when(id: WelcomeViewController.UPDATE_COMPONENT, type: Void.self)
            .doAction(actionSpec: WelcomeViewModel.actionButtonClick)
            .then()
            .inside(componentID: WelcomeViewController.ID, component: WelcomeViewController.self)
            .doReaction(reactionExecution: { component, _ in
                component.acceptPressed()
            })
    }
}
