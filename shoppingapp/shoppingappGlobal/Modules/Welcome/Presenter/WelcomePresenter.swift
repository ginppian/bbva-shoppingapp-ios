//
//  WelcomePresenter.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 23/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import RxSwift
import CellsNativeComponents

class WelcomePresenter<T: WelcomeViewProtocol>: BasePresenter<T> {

    let migrationKey = Settings.Migration.versionKey

    required init() {
        super.init(tag: WelcomePageReaction.ROUTER_TAG)
    }

    override init(busManager: BusManager) {

        super.init(busManager: busManager)
        routerTag = WelcomePageReaction.ROUTER_TAG
    }
}

// MARK: - WelcomePresenterProtocol
extension WelcomePresenter: WelcomePresenterProtocol {

    func viewDidLoad() {
        
        if PreferencesManager.sharedInstance().getValue(forKey: migrationKey) == nil {
            view?.showFirstInit()
        } else {
            view?.showUpdate()
        }
    }
    
    func acceptPressed() {
                
        busManager.setRootWhenNoSession()
    }
}
