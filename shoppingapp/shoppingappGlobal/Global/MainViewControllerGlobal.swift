//
//  MainViewController.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 13/10/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

class MainViewControllerGlobal: LGSideMenuController {

    static let sharedInstance = MainViewController()

    private var type: UInt?

    var leftMenuVC: LeftMenuViewController!
    
    func configureLeftMenu() -> UIViewController {
        
        let viewLeftMenu = LeftMenuViewController(nibName: "LeftMenuViewController", bundle: nil)
        let presenterLeftMenu = LeftMenuPresenter<LeftMenuViewController>()
        viewLeftMenu.presenter = presenterLeftMenu
        presenterLeftMenu.view = viewLeftMenu
        presenterLeftMenu.interactor = LeftMenuInteractor()
        
        return viewLeftMenu
    }

    func setup(type: UInt) {
        self.type = type

        // -----

        if let storyboard = storyboard {
            leftMenuVC = storyboard.instantiateViewController(withIdentifier: "LeftMenuViewController") as? LeftMenuViewController

            // Sizes and styles is set in storybord
            // You can also find there all other parameters
            // LGSideMenuController fully customizable from storyboard
        } else {
            
            leftMenuVC = configureLeftMenu() as? LeftMenuViewController

            let screenWidth = UIScreen.main.bounds.width
            leftViewWidth = screenWidth - 60
            rootViewCoverColorForLeftView = UIColor(red: 0.0, green: 0.0, blue: 1.0, alpha: 0.05)
        }

        // -----

        let greenCoverColor = UIColor(red: 0.0, green: 0.1, blue: 0.0, alpha: 0.3)
        let regularStyle: UIBlurEffectStyle
        regularStyle = .regular
        
        // -----

        switch type {
            case 0:
                leftViewPresentationStyle = .scaleFromBig
                rootViewStatusBarStyle = .lightContent
            case 1:
                leftViewPresentationStyle = .slideAbove
                rootViewCoverColorForLeftView = greenCoverColor
            case 2:
                leftViewPresentationStyle = .slideBelow
            case 3:
                leftViewPresentationStyle = .scaleFromLittle
            case 4:
                leftViewPresentationStyle = .scaleFromBig
                rootViewCoverBlurEffectForLeftView = UIBlurEffect(style: regularStyle)
                rootViewCoverAlphaForLeftView = 0.8
            case 5:
                leftViewPresentationStyle = .scaleFromBig
                leftViewCoverBlurEffect = UIBlurEffect(style: .dark)
                leftViewCoverColor = nil
            case 6:
                leftViewPresentationStyle = .slideAbove
                leftViewBackgroundBlurEffect = UIBlurEffect(style: regularStyle)
                leftViewBackgroundColor = UIColor(red: 0.0, green: 1.0, blue: 0.0, alpha: 0.05)
                rootViewCoverColorForLeftView = greenCoverColor
            case 7:
                leftViewPresentationStyle = .slideAbove
                rootViewCoverColorForLeftView = greenCoverColor
            case 8:
                leftViewPresentationStyle = .scaleFromBig
                leftViewStatusBarStyle = .lightContent
            case 9:
                swipeGestureArea = .full
                leftViewPresentationStyle = .scaleFromBig
            case 10:
                leftViewPresentationStyle = .scaleFromBig
            case 11:
                rootViewLayerBorderWidth = 5.0
                rootViewLayerBorderColor = .white
                rootViewLayerShadowRadius = 10.0

                leftViewSwipeGestureRange = LGSideMenuSwipeGestureRangeMake(0.0, 88.0)
                leftViewPresentationStyle = .scaleFromBig
                leftViewAnimationSpeed = 1.0
                leftViewBackgroundColor = UIColor(red: 0.5, green: 0.75, blue: 0.5, alpha: 1.0)
                leftViewBackgroundImageInitialScale = 1.5
                leftViewInititialOffsetX = -200.0
                leftViewInititialScale = 1.5
                leftViewCoverBlurEffect = UIBlurEffect(style: .dark)
                leftViewBackgroundImage = nil

                rootViewScaleForLeftView = 0.6
                rootViewCoverColorForLeftView = UIColor(red: 1.0, green: 1.0, blue: 0.0, alpha: 0.3)
                rootViewCoverBlurEffectForLeftView = UIBlurEffect(style: regularStyle)
                rootViewCoverAlphaForLeftView = 0.9
            default:
            break
        }

        // -----

        leftMenuViewController = leftMenuVC
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    override func leftViewWillLayoutSubviews(with size: CGSize) {
        super.leftViewWillLayoutSubviews(with: size)

        if !isLeftViewStatusBarHidden {
            leftView?.frame = CGRect(x: 0.0, y: 20.0, width: size.width, height: size.height - 20.0)
        }
    }

    override var isLeftViewStatusBarHidden: Bool {
        get {
            if type == 8 {
                return UIInterfaceOrientationIsLandscape(UIApplication.shared.statusBarOrientation) && UI_USER_INTERFACE_IDIOM() == .phone
            }

            return super.isLeftViewStatusBarHidden
        }

        set {
            super.isLeftViewStatusBarHidden = newValue
        }
    }

    deinit {
        DLog(message: "MainViewController deinitialized")
    }

}
