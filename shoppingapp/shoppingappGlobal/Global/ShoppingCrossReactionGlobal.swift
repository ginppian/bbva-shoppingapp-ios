//
//  ShoppingCrossReactionGlobal.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 11/7/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import CellsNativeCore
import CellsNativeComponents

class ShoppingCrossReactionGlobal: GlobalReaction {

    static let TAG: String = String(describing: ShoppingCrossReactionGlobal.self)

    static let LANG: String = "lang"

    static let LOCALE_ES: String = "es"
    
    static let LOCALE_DEFAULT: String = "es-MX"

    static let DELIMITIER: String = "-"

    static let PRIVATE_LOCALE_BRIDGE: String = "__native_bridge_evt_locales"
    
    let notRecognizedOperationUtils = NotRecognizedOperationUtils()

    override func configure() {
        
        hideViewNative()
        sendLocaleCountry()
        pageLoaded()
        
        publishCards()

        security()
        
        registerforWriteApnsTokenChanged()
        registerforWriteApnsTokenFailed()
    
        registerForTicketReceived()
        
        publicConfigurationSuccessLoaded()
        publicConfigurationErrorLoaded()
        
        grantingTicketSuccess()
        grantingTicketError()

        navigateToNotRecognizedOperation()
    }
    
    // MARK: - Global

    func hideViewNative() {

        _ = writeOn(channel: PageReactionConstants.CHANNEL_HIDDEN_VIEW_NATIVE)
            .when(componentID: ShoppingCrossReaction.TAG)
            .doAction(actionSpec: PageReactionConstants.HIDDEN_VIEW_NATIVE)
    }
    
    func sendLocaleCountry() {
        
        _ = reactTo(channel: WebController.ON_WEB_READY_CHANNEL)?
            .then()
            .inside(componentID: WebController.ID, component: WebController.self)
            .doReactionAndClearChannel { controller, _ in
                
                if let local = Locale.current.languageCode {
                    var locale: [String: AnyObject] = [String: AnyObject]()
                    if local == ShoppingCrossReaction.LOCALE_ES {
                        locale[ShoppingCrossReaction.LANG] = local + ShoppingCrossReaction.DELIMITIER + SessionDataManager.sessionDataInstance().codeCountry as AnyObject
                    } else {
                        locale[ShoppingCrossReaction.LANG] = ShoppingCrossReaction.LOCALE_DEFAULT as AnyObject
                    }
                    controller.sendMessageToWeb(channel: ShoppingCrossReaction.PRIVATE_LOCALE_BRIDGE, payload: locale)
                }
        }
    }
    
    func pageLoaded() {
        
        _ = reactTo(channel: WebController.ON_PAGE_LOADED_CHANNEL)?
            .then()
            .inside(componentID: WebController.ID, component: WebController.self)
            .doReactionAndClearChannel { _, page in
                
                if let page = page {
                    
                    if page != "cells-native-route::init"{
                        BusManager.sessionDataInstance.publishData(tag: ShoppingCrossReaction.TAG, PageReactionConstants.HIDDEN_VIEW_NATIVE, page)
                        
                        BusManager.sessionDataInstance.currentCellsPage = page
                    }
                }
        }
    }
    
    // MARK: - Cards

    func publishCards() {

        _ = writeOn(channel: PageReactionConstants.CHANNEL_CARDS)
            .when(componentID: ShoppingCrossReaction.TAG)
            .doAction(actionSpec: PageReactionConstants.PARAMS_CARDS)
    }
    
    // MARK: - Security

    func security() {

        _ = writeOn(channel: PageReactionConstants.CHANNEL_SECURITY)
            .when(componentID: SecurityInterceptorStore.ID)
            .doAction(actionSpec: SecurityInterceptorStore.SECURITY_TOKEN)

        _ = reactTo(channel: PageReactionConstants.CHANNEL_SECURITY)?
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReactionAndClearChannel(reactionExecution: { router, param in

                if let header = param as? [String: String] {
                    router.navigateOtp(withHeader: header)
                }
            })
    }
    
    // MARK: - Notifications

    func registerforWriteApnsTokenChanged() {

        _ = writeOn(channel: PageReactionConstants.CHANNEL_TOKEN_CHANGED)
            .when(componentID: ShoppingCrossReaction.TAG)
            .doAction(actionSpec: PageReactionConstants.TOKEN_CHANGED)
    }

    func registerforWriteApnsTokenFailed() {

        _ = writeOn(channel: PageReactionConstants.CHANNEL_TOKEN_FAILED)
            .when(componentID: ShoppingCrossReaction.TAG)
            .doAction(actionSpec: PageReactionConstants.TOKEN_FAILED)
    }
    
    func registerForTicketReceived() {
        
        _ = writeOn(channel: PageReactionConstants.CHANNEL_TICKET_RECEIVED_IN_BACKGROUND)
            .when(componentID: ShoppingCrossReaction.TAG)
            .doAction(actionSpec: PageReactionConstants.TICKET_RECEIVED_IN_BACKGROUND)
    }
    
    // MARK: - Public configuration

    func publicConfigurationSuccessLoaded() {

        _ = when(id: PublicConfigurationStore.identifier, type: PublicConfigurationDTO.self)
            .doAction(actionSpec: PublicConfigurationStore.onGetPublicConfigurationSuccess)
            .then()
            .inside(componentID: PublicConfigurationManager.id, component: PublicConfigurationManager.self)
            .doReaction(reactionExecution: { publicConfigurationManager, responseDTO in

                if let responseDTO = responseDTO {
                    publicConfigurationManager.publicConfigurationSuccessLoadedFromStore(publicConfigurationDTO: responseDTO)
                }
            })
    }

    func publicConfigurationErrorLoaded() {

        _ = when(id: PublicConfigurationStore.identifier, type: ErrorBO.self)
            .doAction(actionSpec: PublicConfigurationStore.onGetPublicConfigurationError)
            .then()
            .inside(componentID: PublicConfigurationManager.id, component: PublicConfigurationManager.self)
            .doReaction(reactionExecution: { publicConfigurationManager, error in

                if let error = error {
                    publicConfigurationManager.publicConfigurationErrorLoadedFromStore(error: error)
                }
            })
    }
    
    // MARK: - GrantingTicket
    
    func grantingTicketSuccess() {
        
        _ = when(id: GrantingTicketStore.identifier, type: GrantingTicketResponseDTO.self)
            .doAction(actionSpec: GrantingTicketStore.onGrantingSuccess)
            .then()
            .inside(componentID: PublicConfigurationManager.id, component: PublicConfigurationManager.self)
            .doReaction(reactionExecution: { publicConfigurationManager, grantingTicketResponseDTO in
                
                if let grantingTicketResponseDTO = grantingTicketResponseDTO {
                    publicConfigurationManager.grantingTicketSuccess(grantingTicketResponseDTO: grantingTicketResponseDTO)
                }
            })
    }
    
    func grantingTicketError() {
        
        _ = when(id: GrantingTicketStore.identifier, type: ErrorBO.self)
            .doAction(actionSpec: GrantingTicketStore.onGrantingError)
            .then()
            .inside(componentID: PublicConfigurationManager.id, component: PublicConfigurationManager.self)
            .doReaction(reactionExecution: { publicConfigurationManager, error in
                
                if let error = error {
                    publicConfigurationManager.grantingTicketError(error: error)
                }
            })
    }
    
    // MARK: - NotRecognizedOperation
    
    func navigateToNotRecognizedOperation() {
        
        _ = when(id: TransactionDetailPageReaction.ROUTER_TAG, type: Model.self)
            .doAction(actionSpec: TransactionDetailPageReaction.ACTION_NAVIGATE_TO_NOT_OPERATION)
            .then()
            .inside(componentID: WebController.ID, component: WebController.self)
            .doReactionAndClearChannel { webController, _ in
                
                self.notRecognizedOperationUtils.checkNavigateToNotRecognizedOperation(webController: webController)
        }

        _ = when(id: TicketDetailViewController.ID, type: Model.self)
            .doAction(actionSpec: TransactionDetailPageReaction.ACTION_NAVIGATE_TO_NOT_OPERATION)
            .then()
            .inside(componentID: WebController.ID, component: WebController.self)
            .doReactionAndClearChannel { webController, _ in
                
                self.notRecognizedOperationUtils.checkNavigateToNotRecognizedOperation(webController: webController)
        }

        _ = reactTo(channel: WebController.ON_PAGE_LOADED_CHANNEL)?
            .then()
            .inside(componentID: WebController.ID, component: WebController.self)
            .doReactionAndClearChannel { _, page in

                if page == TransactionDetailPageReaction.CELLS_PAGE_NOT_RECOGNIZED_OPERATION, self.notRecognizedOperationUtils.needNavigateToNotRecognizedOperation,
                    let viewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: NotRecognizedOperationViewController.ID) {
                    
                    self.notRecognizedOperationUtils.needNavigateToNotRecognizedOperation = false
                    viewController.modalPresentationStyle = .overCurrentContext
                    let navigationController = UINavigationController(rootViewController: viewController)
                    BusManager.sessionDataInstance.present(viewController: navigationController, animated: true, completion: nil)
                }
        }
    }
}
