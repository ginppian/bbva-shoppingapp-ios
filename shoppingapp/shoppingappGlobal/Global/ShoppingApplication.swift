//
//  ShoppingApplication.swift
//  shoppingapp
//
//  Created by AZIZEBULBUL on 01/02/2018.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeComponents
import CellsNativeCore

class ShoppingApplication: UIApplication {

    lazy var timeoutInSeconds = Settings.Timeout.timeout_for_expired_session
    
    private let expiredSessionQueue = DispatchQueue(label: "com.bbva.shoppingapp.expiredSessionQueue", qos: .userInteractive)
    
    private var dispatchWorkItem: DispatchWorkItem?
    
    func resetTimer() {
        
        dispatchWorkItem?.cancel()
        
        dispatchWorkItem = DispatchWorkItem {
            
            DispatchQueue.main.async {
                
                if SessionDataManager.sessionDataInstance().isUserLogged {
                    BusManager.sessionDataInstance.expiredSessionWithWarning()
                    cleanStackWebView()
                }
            }
        }
        
        guard let dispatchWorkItem = dispatchWorkItem else {
            return
        }
        
        expiredSessionQueue.asyncAfter(wallDeadline: .now() + timeoutInSeconds, execute: dispatchWorkItem)
    }

    override func sendEvent(_ event: UIEvent) {

        super.sendEvent(event)
        
        if let touches = event.allTouches {
            for touch in touches where touch.phase == UITouchPhase.began {
                resetTimer()
            }
        }
    }
}

public extension UIApplication {
    public var topViewController: UIViewController? {
        guard var topViewController = (UIApplication.shared.delegate as? AppDelegate)?.window?.rootViewController else {
            return nil
        }

        while let presentedViewController = topViewController.presentedViewController {
            topViewController = presentedViewController
        }
        return topViewController
    }

    public var topNavigationController: UINavigationController? {
        return topViewController as? UINavigationController
    }
}

private func cleanStackWebView() {
    if let webController: WebController = ComponentManager.get(id: WebController.ID) {
        webController.logoutWebView()
    }
}
