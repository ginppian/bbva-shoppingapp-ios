//
//  CommonPageReaction.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 11/12/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

class CommonPageReaction: PageReaction {
    
    // MARK: Constants
    
    static let tag = String(describing: CommonPageReaction.self)
    
    static let channelGlobalBbvaWebsite = "global_open_bbva_website"
    
    static let channelGlobalShare = "global_share_receipt"
    
    static let channelGlobalMoreInfo = "global_more_info"
    
    override func configure() {
        
        reactToChannelGlobalBbvaWebsite()
        reactToChannelGlobalShare()
        reactToChannelGlobalMoreInfo()
    }
    
    func reactToChannelGlobalBbvaWebsite() {
        
        let channel: JsonChannel = JsonChannel(CommonPageReaction.channelGlobalBbvaWebsite)
        
        _ = reactTo(channel: channel)?
            .then()
            .inside(componentID: CommonCellsGlobal.id, component: CommonCellsGlobal.self)
            .doReactionAndClearChannel { commonCellsGlobal, _ in
                
                commonCellsGlobal.openWeb(web: Settings.Global.default_web_bbva)
        }
    }
    
    func reactToChannelGlobalShare() {
        
        let channel: JsonChannel = JsonChannel(CommonPageReaction.channelGlobalShare)
        
        _ = reactTo(channel: channel)?
            .then()
            .inside(componentID: CommonCellsGlobal.id, component: CommonCellsGlobal.self)
            .doReactionAndClearChannel { commonCellsGlobal, _ in
                
                commonCellsGlobal.sharePdf()
        }
    }
    
    func reactToChannelGlobalMoreInfo() {
    
        let channel: JsonChannel = JsonChannel(CommonPageReaction.channelGlobalMoreInfo)
        
        _ = reactTo(channel: channel)?
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReactionAndClearChannel { router, param in
                
                if let paramDict = param as? [String: String], let idHelp = paramDict["id"] {

                    router.navigateToDetailHelp(withModel: HelpDetailTransport(idHelp))
                }
        }
    }
}
