//
//  PresentingViewsManager.swift
//  shoppingapp
//
//  Created by Marcos on 9/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol NotDismissable {}

extension TicketDetailViewController: NotDismissable {}

final class PresentingViewsManager {
    
    static let viewControllerDismissedNotification = Notification.Name(rawValue: "viewControllerDismissedNotification")

    private var notDismissablePresenters = [UIViewController]()
    private let semaphore = DispatchSemaphore(value: 1)
    private let modalQueue = DispatchQueue(label: "com.bbva.shoppingapp.presentingViewsManager.modalqueue", qos: .userInteractive)

    func present(viewController: UIViewController, animated: Bool, completion: (() -> Void)?, modalPresentationStyle: UIModalPresentationStyle = .overCurrentContext ) {
        modalQueue.async {
            self.semaphore.wait()

            let completionWithSemaphore: (() -> Void) = {
                self.semaphore.signal()
                if let completion = completion {
                    completion()
                }
            }

            viewController.modalPresentationStyle = modalPresentationStyle

            DispatchQueue.main.async {

                UIApplication.shared.topViewController?.present(viewController, animated: true, completion: completionWithSemaphore)
            }
        }
    }

    func dismissViewController(animated: Bool, completion: ((Bool) -> Void)? ) {
        modalQueue.async {
            self.semaphore.wait()

            let completionWithSemaphore: ((Bool) -> Void) = { dismissDone in
                self.semaphore.signal()
                if let completion = completion {
                    completion(dismissDone)
                    NotificationCenter.default.post(name: PresentingViewsManager.viewControllerDismissedNotification, object: nil)
                }
            }

            DispatchQueue.main.async {
                if let topViewController = UIApplication.shared.topViewController, let topPresentingViewController = topViewController.presentingViewController {

                    if let index = self.notDismissablePresenters.index(of: topPresentingViewController) {
                        self.notDismissablePresenters.remove(at: index)
                        self.dismissBackwards(fromViewController: topViewController, completion: completionWithSemaphore)
                    } else {
                        UIApplication.shared.topViewController?.dismiss(animated: animated, completion: {
                            completionWithSemaphore(true)
                        })
                    }
                } else {
                    completionWithSemaphore(true)
                }
            }
        }
    }

    func forceDismissAllPresentedViewControllers(completion: ((Bool) -> Void)? ) {

        modalQueue.async {

            self.semaphore.wait()

            let completionWithSemaphore: ((Bool) -> Void) = { dismissDone in
                self.semaphore.signal()
                if let completion = completion {
                    completion(dismissDone)
                }
            }

            DispatchQueue.main.async {

                if let topViewController = UIApplication.shared.topViewController {

                    self.notDismissablePresenters.removeAll()
                    self.dismissBackwards(fromViewController: topViewController, completion: completionWithSemaphore)
                } else {

                    completionWithSemaphore(true)
                }
            }
        }
    }

    func dismissPresentedViewControllers(completion: ((Bool) -> Void)? ) {
        guard let rootViewController = (UIApplication.shared.delegate as? AppDelegate)?.window?.rootViewController else {
            return
        }
        modalQueue.async {
            self.semaphore.wait()

            let completionWithSemaphore: ((Bool) -> Void) = { dismissDone in
                self.semaphore.signal()
                if let completion = completion {
                    completion(dismissDone)
                }
            }

            DispatchQueue.main.async {

                guard let topPresented = UIApplication.shared.topViewController else {
                    DispatchQueue.main.async {
                        completionWithSemaphore(false)
                    }
                    return
                }

                self.notDismissablePresenters = [UIViewController]()
                var dismissablesPresenters = [UIViewController]()

                self.checkForDismissables(fromViewController: rootViewController, notDismissablesPresenters: &self.notDismissablePresenters, dismissablesPresenters: &dismissablesPresenters)

                var topPresentedNotDissmisable = false
                if let topPresentedNavigationController = topPresented as? UINavigationController {
                    for childViewController  in topPresentedNavigationController.viewControllers where childViewController is NotDismissable {
                        topPresentedNotDissmisable = true
                    }
                } else {
                    topPresentedNotDissmisable = topPresented is NotDismissable
                }

                if topPresentedNotDissmisable {
                    DispatchQueue.main.async {
                        completionWithSemaphore(false)
                    }
                } else if self.notDismissablePresenters.isEmpty {

                    if let first = dismissablesPresenters.first {
                        first.dismiss(animated: false, completion: {
                            completionWithSemaphore(true)
                        })
                    } else {
                        completionWithSemaphore(false)
                    }

                } else {
                    self.dismissBackwards(fromViewController: topPresented, completion: completionWithSemaphore)
                }
            }
        }
    }

    private func checkForDismissables(fromViewController viewController: UIViewController, notDismissablesPresenters: inout [UIViewController], dismissablesPresenters: inout [UIViewController]) {

        if let presentedViewController = viewController.presentedViewController {
            if let presentedNavigationController = presentedViewController as? UINavigationController {
                var foundNotDissmisable = false
                for childViewController  in presentedNavigationController.viewControllers where childViewController is NotDismissable {
                    foundNotDissmisable = true
                    break
                }
                if foundNotDissmisable {
                    notDismissablesPresenters.append(viewController)
                } else {
                    dismissablesPresenters.append(viewController)
                }
                checkForDismissables(fromViewController: presentedNavigationController, notDismissablesPresenters: &notDismissablesPresenters, dismissablesPresenters: &dismissablesPresenters)
            } else {
                if presentedViewController is NotDismissable {
                    notDismissablesPresenters.append(viewController)
                } else {
                    dismissablesPresenters.append(viewController)
                }
                checkForDismissables(fromViewController: presentedViewController, notDismissablesPresenters: &notDismissablesPresenters, dismissablesPresenters: &dismissablesPresenters)
            }
        } else if let presentedNavigationController = viewController as? UINavigationController {
            for childViewController  in presentedNavigationController.viewControllers {
                checkForDismissables(fromViewController: childViewController, notDismissablesPresenters: &notDismissablesPresenters, dismissablesPresenters: &dismissablesPresenters)
            }
        } else if let presentedTabBarController = viewController as? UITabBarController {
            if let childViewControllers = presentedTabBarController.viewControllers {
                for childViewController  in childViewControllers {
                    checkForDismissables(fromViewController: childViewController, notDismissablesPresenters: &notDismissablesPresenters, dismissablesPresenters: &dismissablesPresenters)
                }
            }
        }
    }

    private func dismissBackwards(fromViewController viewController: UIViewController, completion: ((Bool) -> Void)?) {

        var presentingViewController = viewController

        var viewControllers = [UIViewController]()

        while let presenting = presentingViewController.presentingViewController, !notDismissablePresenters.contains(presenting) {
            viewControllers.append(presentingViewController)
            presentingViewController = presenting
        }

        viewControllers.append(presentingViewController)

        dismissModalViewControllersAnimated(viewControllers, completion: completion)

    }

    private func dismissModalViewControllersAnimated(_ viewControllers: [UIViewController], completion: ((Bool) -> Void)?) {

        guard let firstViewController = viewControllers.first else {
            if let completion = completion {
                completion(false)
            }
            return
        }

        if viewControllers.count > 2 {
            for index in 1..<viewControllers.count - 1 {
                viewControllers[index].view.isHidden = true
            }
        }

        firstViewController.dismiss(animated: true, completion: {

            if viewControllers.count > 2 {
                viewControllers.last?.dismiss(animated: false, completion: {
                    if let completion = completion {
                        completion(true)
                    }
                })
            } else {
                if let completion = completion {
                    completion(true)
                }
            }
        })
    }
}
