//
//  RouterInitFactory.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 7/9/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit
import CellsNativeCore

class RouterInitFactoryGlobal: RouterFactory {

    override init() {

        super.init()
        initViewControllers()

    }

    func initViewControllers() {

        addSplashRoute()

        addWelcomeRoute()

        addLoginRoute()

        addLoginLoaderRoute()

        addNSecurityRoute()

        addHomeRoute()

        addCardsRoute()

        addCardActivationRoute()

        addCardGenerationRoute()

        addCardCancelationRoute()

        addCardBlockRoute()

        addOnOffHelpRoute()

        addTransactionListRoute()

        addTransactionsFilterRoute()

        addTransactionDetailRoute()

        addLimitsRoute()

        addShoppingRoute()

        addShoppingDetailRoute()

        addShoppingWebRoute()
        
        addShoppingGuaranteeRoute()
        
        addShoppingContactRoute()
        
        addShoppingFilterRoute()

        addShoppingListRoute()

        addNotificationsRoute()

        addCardCVVRoute()

        addTicketDetailRoute()

        addEditLimitsRoute()

        addShoppingListFilteredRoute()

        addHelpDetailRoute()

        addShoppingMapListRoute()

        addHelpRoute()

        addLeftMenuRoute()
        addSideMenuSettingsRoute()
        addSideMenuAboutRoute()

        addAnonymousHomeRoute()
        addAnonymousCardsRoute()
        addAnonymousRewardsRoute()
        addAnonymousNotificationsRoute()

        addNotRecognizedOperationRoute()
        addActivateDevicePermissionsRoute()
        
        addUpgradeNotificationRoute()
    }

    func addSplashRoute() {
        self.addRoute(route: SplashViewController.ID, routeObject: Route(viewController: { () -> UIViewController in

            let viewSplash = SplashViewController(nibName: "SplashViewController", bundle: nil)
            let presenterSplash = SplashPresenter<SplashViewController>()
            viewSplash.presenter = presenterSplash
            presenterSplash.view = viewSplash
            return viewSplash

        }, animationToNext: { _, destinationViewController  in

            self.animation(viewController: destinationViewController!)

        }, animationToPrevious: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }))
    }

    func addWelcomeRoute() {
        self.addRoute(route: WelcomeViewController.ID, routeObject: Route(viewController: { () -> UIViewController in

            let viewWelcome = WelcomeViewController(nibName: "WelcomeViewController", bundle: nil)
            let presenterWelcome = WelcomePresenter<WelcomeViewController>()
            viewWelcome.presenter = presenterWelcome
            presenterWelcome.view = viewWelcome
            return viewWelcome

        }, animationToNext: { _, destinationViewController  in

            self.animation(viewController: destinationViewController!)

        }, animationToPrevious: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }))
    }

    func addLoginRoute() {
        self.addRoute(route: LoginViewController.ID, routeObject: Route(viewController: { () -> UIViewController in

            let viewLogin = LoginViewController(nibName: "LoginViewController", bundle: nil)
            let presenterLogin = LoginPresenter<LoginViewController>()
            viewLogin.presenter = presenterLogin
            presenterLogin.view = viewLogin
            return viewLogin

        }, animationToNext: { _, destinationViewController  in

            self.animation(viewController: destinationViewController!)

        }, animationToPrevious: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }))
    }

    func addLoginLoaderRoute() {
        self.addRoute(route: LoginLoaderViewController.ID, routeObject: Route(viewController: { () -> UIViewController in

            let viewLoginLoader = LoginLoaderViewController(nibName: "LoginLoaderViewController", bundle: nil)
            let presenterLoginLoader = LoginLoaderPresenter<LoginLoaderViewController>()
            viewLoginLoader.presenter = presenterLoginLoader
            presenterLoginLoader.view = viewLoginLoader
            presenterLoginLoader.interactor = LoginLoaderInteractor()
            return viewLoginLoader

        }, animationToNext: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }, animationToPrevious: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }))
    }

    func addHomeRoute() {
        self.addRoute(route: HomeViewController.ID, routeObject: Route(viewController: { () -> UIViewController in

            let homeViewController = HomeViewController(nibName: "HomeViewController", bundle: nil)
            let presenterHome = HomePresenter<HomeViewController>()
            presenterHome.notificationManager = NotificationManagerType()

            presenterHome.interactor = HomeInteractor()
            homeViewController.presenter = presenterHome
            presenterHome.view = homeViewController
            homeViewController.tabBarItem = UITabBarItem(title: Localizables.common.key_tabbar_start_button, image: ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Home.wallet_icon, color: .GRAYTABBAR), width: 18, height: 18), tag: 1)
            homeViewController.tabBarItem.setTitleTextAttributes([NSAttributedStringKey.font: Tools.setFontMedium(size: 10)], for: .normal)
            homeViewController.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -3)
            return homeViewController

        }, animationToNext: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }, animationToPrevious: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }))
    }

    func addCardsRoute() {
        self.addRoute(route: CardsViewController.ID, routeObject: Route(viewController: { () -> UIViewController in

            let viewCards = CardsViewController(nibName: "CardsViewController", bundle: nil)
            let presenterCards = CardsPresenter<CardsViewController>()
            viewCards.presenter = presenterCards
            presenterCards.view = viewCards
            presenterCards.interactor = CardsInteractor ()
            viewCards.tabBarItem = UITabBarItem(title: Localizables.common.key_tabbar_cards_button, image: ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Card.creditcard_icon, color: .GRAYTABBAR), width: 18, height: 18), tag: 2)
            viewCards.tabBarItem.setTitleTextAttributes([NSAttributedStringKey.font: Tools.setFontMedium(size: 10)], for: .normal)
            viewCards.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -3)
            return viewCards

        }, animationToNext: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }, animationToPrevious: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }))
    }

    func addCardActivationRoute() {
        self.addRoute(route: CardActivationViewController.ID, routeObject: Route(viewController: { () -> UIViewController in

            let viewCardActivation = CardActivationViewController(nibName: "WalletCellsViewController", bundle: nil)
            let presenterCardActivation = CardActivationPresenter<CardActivationViewController>()
            viewCardActivation.presenter = presenterCardActivation
            presenterCardActivation.view = viewCardActivation
            viewCardActivation.hidesBottomBarWhenPushed = true
            return viewCardActivation

        }, animationToNext: { originViewController, destinationViewController  in

            self.animation(viewController: destinationViewController!)

        }, animationToPrevious: { originViewController, _ in

            self.animation(viewController: originViewController)

        }))
    }

    func addCardCancelationRoute() {
        self.addRoute(route: CardCancelationViewController.ID, routeObject: Route(viewController: { () -> UIViewController in

            let viewCardCancelation = CardCancelationViewController(nibName: "WalletCellsViewController", bundle: nil)
            let presenterCardCancelation = CardCancelationPresenter<CardCancelationViewController>()
            viewCardCancelation.presenter = presenterCardCancelation
            presenterCardCancelation.view = viewCardCancelation
            viewCardCancelation.hidesBottomBarWhenPushed = true
            return viewCardCancelation

        }, animationToNext: { originViewController, destinationViewController  in

            self.animation(viewController: destinationViewController!)

        }, animationToPrevious: { originViewController, _ in

            self.animation(viewController: originViewController)

        }))
    }

    func addCardGenerationRoute() {
        self.addRoute(route: CardGenerationViewController.ID, routeObject: Route(viewController: { () -> UIViewController in

            let view = CardGenerationViewController(nibName: "WalletCellsViewController", bundle: nil)
            let presenter = CardGenerationPresenter<CardGenerationViewController>()
            view.presenter = presenter
            presenter.view = view
            view.hidesBottomBarWhenPushed = true
            return view

        }, animationToNext: { originViewController, destinationViewController  in

            self.animation(viewController: destinationViewController!)

        }, animationToPrevious: { originViewController, _ in

            self.animation(viewController: originViewController)

        }))
    }

    func addCardBlockRoute() {

        self.addRoute(route: CardBlockViewController.ID, routeObject: Route(viewController: { () -> UIViewController in

            let viewCardBlock = CardBlockViewController(nibName: "WalletCellsViewController", bundle: nil)
            let presenterCardBlock = CardBlockPresenter<CardBlockViewController>()
            viewCardBlock.presenter = presenterCardBlock
            presenterCardBlock.view = viewCardBlock
            viewCardBlock.hidesBottomBarWhenPushed = true
            return viewCardBlock

        }, animationToNext: { originViewController, destinationViewController  in

            self.animation(viewController: destinationViewController!)

        }, animationToPrevious: { originViewController, _ in

            self.animation(viewController: originViewController)

        }))
    }

    func addNotRecognizedOperationRoute() {

        self.addRoute(route: NotRecognizedOperationViewController.ID, routeObject: Route(viewController: { () -> UIViewController in

            let viewNotRecognizedOperationView = NotRecognizedOperationViewController(nibName: "WalletCellsViewController", bundle: nil)
            let presenterNotRecognizedOperation = NotRecognizedOperationPresenter<NotRecognizedOperationViewController>()
            viewNotRecognizedOperationView.presenter = presenterNotRecognizedOperation
            presenterNotRecognizedOperation.view = viewNotRecognizedOperationView
            viewNotRecognizedOperationView.hidesBottomBarWhenPushed = true
            return viewNotRecognizedOperationView

        }, animationToNext: { originViewController, destinationViewController  in

            self.animation(viewController: destinationViewController!)

        }, animationToPrevious: { originViewController, _ in

            self.animation(viewController: originViewController)

        }))
    }

    func addActivateDevicePermissionsRoute() {
        self.addRoute(route: ActivateDevicePermissionsViewController.ID, routeObject: Route(viewController: { () -> UIViewController in

            let view = ActivateDevicePermissionsViewController(nibName: "WalletCellsViewController", bundle: nil)
            let presenter = ActivateDevicePermissionsPresenter<ActivateDevicePermissionsViewController>()
            view.presenter = presenter
            presenter.view = view
            view.hidesBottomBarWhenPushed = true
            return view

        }, animationToNext: { originViewController, destinationViewController  in

            self.animation(viewController: destinationViewController!)

        }, animationToPrevious: { originViewController, _ in

            self.animation(viewController: originViewController)

        }))
    }

    func addOnOffHelpRoute() {

        self.addRoute(route: OnOffHelpViewController.ID, routeObject: Route(viewController: { () -> UIViewController in

            let onOffHelpView = OnOffHelpViewController(nibName: "OnOffHelpViewController", bundle: nil)

            let onOffHelpPresenter = OnOffHelpPresenter<OnOffHelpViewController>()

            onOffHelpView.presenter = onOffHelpPresenter

            onOffHelpPresenter.view = onOffHelpView

            return onOffHelpView

        }, animationToNext: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }, animationToPrevious: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }))
    }

    func addTransactionListRoute() {
        self.addRoute(route: TransactionListViewController.ID, routeObject: Route(viewController: { () -> UIViewController in

            let viewTransactionList = TransactionListViewController(nibName: "TransactionListViewController", bundle: nil)
            let presenterTransactionList = TransactionsListPresenter<TransactionListViewController>()
            viewTransactionList.hidesBottomBarWhenPushed = true
            viewTransactionList.presenter = presenterTransactionList
            presenterTransactionList.view = viewTransactionList
            presenterTransactionList.interactor = TransactionsListInteractor()
            return viewTransactionList

        }, animationToNext: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }, animationToPrevious: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }))
    }

    func addTransactionsFilterRoute() {
        self.addRoute(route: TransactionsFilterViewController.ID, routeObject: Route(viewController: { () -> UIViewController in

            let viewTransactionsFilter = TransactionsFilterViewController(nibName: "TransactionsFilterViewController", bundle: nil)
            let presenterTransactionsFilter = TransactionsFilterPresenter<TransactionsFilterViewController>()
            viewTransactionsFilter.hidesBottomBarWhenPushed = true
            viewTransactionsFilter.presenter = presenterTransactionsFilter
            presenterTransactionsFilter.view = viewTransactionsFilter
            return viewTransactionsFilter

        }, animationToNext: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }, animationToPrevious: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }))
    }

    func addTransactionDetailRoute() {
        self.addRoute(route: TransactionDetailViewController.ID, routeObject: Route(viewController: { () -> UIViewController in

            let viewTransactionDetail = TransactionDetailViewController(nibName: "TransactionDetailViewController", bundle: nil)
            let presenterTransactionDetail = TransactionDetailPresenter<TransactionDetailViewController>()
            viewTransactionDetail.hidesBottomBarWhenPushed = true
            viewTransactionDetail.presenter = presenterTransactionDetail
            presenterTransactionDetail.view = viewTransactionDetail
            return viewTransactionDetail

        }, animationToNext: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }, animationToPrevious: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }))
    }

    func addShoppingRoute() {
        self.addRoute(route: ShoppingViewController.ID, routeObject: Route(viewController: { () -> UIViewController in

            let viewShopping = ShoppingViewController(nibName: "ShoppingViewController", bundle: nil)
            let presenterShopping = ShoppingPresenter<ShoppingViewController>()
            viewShopping.presenter = presenterShopping
            presenterShopping.view = viewShopping
            presenterShopping.interactor = ShoppingInteractor ()
            viewShopping.tabBarItem = UITabBarItem(title: Localizables.common.key_tabbar_offers_button, image: ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Promotions.tab_promotions, color: .GRAYTABBAR), width: 18, height: 18), tag: 3)
            viewShopping.tabBarItem.setTitleTextAttributes([NSAttributedStringKey.font: Tools.setFontMedium(size: 10)], for: .normal)
            viewShopping.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -3)
            return viewShopping

        }, animationToNext: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }, animationToPrevious: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }))
    }

    func addShoppingDetailRoute() {
        self.addRoute(route: ShoppingDetailViewController.ID, routeObject: Route(viewController: { () -> UIViewController in

            let viewShoppingDetail = ShoppingDetailViewController(nibName: "ShoppingDetailViewController", bundle: nil)

            let presenterShoppingDetail = ShoppingDetailPresenter<ShoppingDetailViewController>()

            viewShoppingDetail.presenter = presenterShoppingDetail

            presenterShoppingDetail.view = viewShoppingDetail

            presenterShoppingDetail.interactor = ShoppingDetailInteractor()

            return viewShoppingDetail

        }, animationToNext: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }, animationToPrevious: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }))
    }

    func addShoppingWebRoute() {

        addRoute(route: ShoppingWebViewController.ID, routeObject: Route(viewController: { () -> UIViewController in

            let viewShoppingWeb = ShoppingWebViewController(nibName: "ShoppingWebViewController", bundle: nil)

            let presenterShoppingWeb = ShoppingWebPresenter<ShoppingWebViewController>()

            viewShoppingWeb.hidesBottomBarWhenPushed = true

            viewShoppingWeb.presenter = presenterShoppingWeb

            presenterShoppingWeb.view = viewShoppingWeb

            return viewShoppingWeb

        }, animationToNext: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }, animationToPrevious: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }))

    }
    
    func addShoppingGuaranteeRoute() {
        
        addRoute(route: ShoppingGuaranteeViewController.ID, routeObject: Route(viewController: { () -> UIViewController in
            
            let viewShoppingGuarantee = ShoppingGuaranteeViewController(nibName: "ShoppingGuaranteeViewController", bundle: nil)
            
            let presenterShoppingGuarantee = ShoppingGuaranteePresenter<ShoppingGuaranteeViewController>()
            
            viewShoppingGuarantee.hidesBottomBarWhenPushed = true
            
            viewShoppingGuarantee.presenter = presenterShoppingGuarantee
            
            presenterShoppingGuarantee.view = viewShoppingGuarantee
            
            return viewShoppingGuarantee
            
        }, animationToNext: { _, destinationViewController in
            
            self.animation(viewController: destinationViewController!)
            
        }, animationToPrevious: { _, destinationViewController in
            
            self.animation(viewController: destinationViewController!)
            
        }))
        
    }
    
    func addShoppingContactRoute() {
        
        addRoute(route: ShoppingContactViewController.ID, routeObject: Route(viewController: { () -> UIViewController in
            
            let viewShoppingContact = ShoppingContactViewController(nibName: "ShoppingContactViewController", bundle: nil)
            
            let presenterShoppingContact = ShoppingContactPresenter<ShoppingContactViewController>()
            
            viewShoppingContact.hidesBottomBarWhenPushed = true
            
            viewShoppingContact.presenter = presenterShoppingContact
            
            presenterShoppingContact.view = viewShoppingContact
            
            return viewShoppingContact
            
        }, animationToNext: { _, destinationViewController in
            
            self.animation(viewController: destinationViewController!)
            
        }, animationToPrevious: { _, destinationViewController in
            
            self.animation(viewController: destinationViewController!)
            
        }))
        
    }

    func addShoppingFilterRoute() {
        self.addRoute(route: ShoppingFilterViewController.ID, routeObject: Route(viewController: { () -> UIViewController in

            let viewShoppingFilter = ShoppingFilterViewController(nibName: "ShoppingFilterViewController", bundle: nil)

            let presenterShoppingFilter = ShoppingFilterPresenter<ShoppingFilterViewController>()

            viewShoppingFilter.presenter = presenterShoppingFilter

            presenterShoppingFilter.view = viewShoppingFilter

            return viewShoppingFilter

        }, animationToNext: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }, animationToPrevious: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }))
    }

    func addShoppingListRoute() {
        self.addRoute(route: ShoppingListViewController.ID, routeObject: Route(viewController: { () -> UIViewController in

            let viewShoppingList = ShoppingListViewController(nibName: "ShoppingListViewController", bundle: nil)

            let presenterShoppingList = ShoppingListPresenter<ShoppingListViewController>()

            viewShoppingList.hidesBottomBarWhenPushed = true

            viewShoppingList.presenter = presenterShoppingList

            presenterShoppingList.view = viewShoppingList

            presenterShoppingList.interactor = ShoppingListInteractor()

            return viewShoppingList

        }, animationToNext: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }, animationToPrevious: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }))
    }

    func addNotificationsRoute() {
        self.addRoute(route: NotificationsViewController.ID, routeObject: Route(viewController: { () -> UIViewController in

            let viewNotifications = NotificationsViewController(nibName: "NotificationsViewController", bundle: nil)
            let presenterotifications = NotificationsPresenter<NotificationsViewController>()
            viewNotifications.presenter = presenterotifications
            presenterotifications.view = viewNotifications
            viewNotifications.tabBarItem = UITabBarItem(title: Localizables.common.key_tabbar_notice_button, image: ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Notifications.alarm_icon, color: .GRAYTABBAR), width: 18, height: 18), tag: 5)
            viewNotifications.tabBarItem.setTitleTextAttributes([NSAttributedStringKey.font: Tools.setFontMedium(size: 10)], for: .normal)
            viewNotifications.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -3)
            return viewNotifications

        }, animationToNext: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }, animationToPrevious: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }))
    }

    func addNSecurityRoute() {
        self.addRoute(route: SecurityViewController.ID, routeObject: Route(viewController: { () -> UIViewController in

            let viewSecurity = SecurityViewController(nibName: "SecurityViewController", bundle: nil)
            let presenterSecurity = SecurityPresenter<SecurityViewController>()
            viewSecurity.presenter = presenterSecurity
            presenterSecurity.view = viewSecurity
            return viewSecurity

        }, animationToNext: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }, animationToPrevious: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }))
    }

    func addLimitsRoute() {

        self.addRoute(route: LimitsViewController.ID, routeObject: Route(viewController: { () -> UIViewController in

            let viewLimits = LimitsViewController(nibName: "LimitsViewController", bundle: nil)
            let presenterLimits = LimitsPresenter<LimitsViewController>()
            viewLimits.hidesBottomBarWhenPushed = true
            viewLimits.presenter = presenterLimits
            presenterLimits.interactor = LimitsInteractor()
            presenterLimits.view = viewLimits
            return viewLimits

        }, animationToNext: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }, animationToPrevious: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }))

    }

    func addTicketDetailRoute() {
        self.addRoute(route: TicketDetailViewController.ID, routeObject: Route(viewController: { () -> UIViewController in

            let viewTicketDetail = TicketDetailViewController(nibName: "TicketDetailViewController", bundle: nil)
            let presenterTicketDetail = TicketDetailPresenter<TicketDetailViewController>()
            viewTicketDetail.hidesBottomBarWhenPushed = true
            viewTicketDetail.presenter = presenterTicketDetail
            presenterTicketDetail.view = viewTicketDetail
            return viewTicketDetail

        }, animationToNext: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }, animationToPrevious: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }))
    }

    func addCardCVVRoute() {

        self.addRoute(route: CvvViewController.ID, routeObject: Route(viewController: { () -> UIViewController in

            let viewCardCVV = CvvViewController(nibName: "CvvViewController", bundle: nil)

            let presenterCardCVV = CVVPresenter<CvvViewController>()

            viewCardCVV.presenter = presenterCardCVV
            presenterCardCVV.interactor = CVVInteractor()
            presenterCardCVV.view = viewCardCVV

            return viewCardCVV

        }, animationToNext: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }, animationToPrevious: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }))

    }

    func addEditLimitsRoute() {

        self.addRoute(route: EditLimitViewController.ID, routeObject: Route(viewController: { () -> UIViewController in

            let viewEditLimits = EditLimitViewController(nibName: "EditLimitViewController", bundle: nil)
            let presenterEditLimits = EditLimitsPresenter<EditLimitViewController>()
            viewEditLimits.hidesBottomBarWhenPushed = true
            viewEditLimits.presenter = presenterEditLimits
            presenterEditLimits.interactor = EditLimitInteractor()
            presenterEditLimits.view = viewEditLimits
            return viewEditLimits

        }, animationToNext: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }, animationToPrevious: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }))

    }

    func addShoppingListFilteredRoute() {

        self.addRoute(route: ShoppingListFilteredViewController.ID, routeObject: Route(viewController: { () -> UIViewController in

            let viewShoppingListFiltered = ShoppingListFilteredViewController(nibName: "ShoppingListFilteredViewController", bundle: nil)
            let presenterShoppingListFiltered = ShoppingListFilteredPresenter<ShoppingListFilteredViewController>()
//            viewShoppingListFiltered.hidesBottomBarWhenPushed = true
            viewShoppingListFiltered.presenter = presenterShoppingListFiltered
            presenterShoppingListFiltered.interactor = ShoppingListFilteredInteractor()
            presenterShoppingListFiltered.view = viewShoppingListFiltered
            return viewShoppingListFiltered

        }, animationToNext: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }, animationToPrevious: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }))

    }

    func addShoppingMapListRoute() {

        self.addRoute(route: ShoppingMapListViewController.ID, routeObject: Route(viewController: { () -> UIViewController in

            let viewShoppingMapListVC = UIStoryboard(name: "ShoppingMapListStoryboard", bundle: nil).instantiateInitialViewController()
            guard let viewShoppingMapList = viewShoppingMapListVC as? ShoppingMapListViewController else {
                return UIViewController()
            }
            let presenterShoppingMapList = ShoppingMapListPresenter<ShoppingMapListViewController>()

            viewShoppingMapList.presenter = presenterShoppingMapList
            presenterShoppingMapList.view = viewShoppingMapList

            return viewShoppingMapList

        }, animationToNext: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }, animationToPrevious: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }))

    }

    func addHelpDetailRoute() {

        self.addRoute(route: HelpDetailViewController.ID, routeObject: Route(viewController: { () -> UIViewController in

            let viewControllerHelpDetail = HelpDetailViewController(nibName: "HelpDetailViewController", bundle: nil)
            let presenterHelpDetail = HelpDetailPresenter<HelpDetailViewController>()
            viewControllerHelpDetail.presenter = presenterHelpDetail
            presenterHelpDetail.view = viewControllerHelpDetail
            return viewControllerHelpDetail

        }, animationToNext: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }, animationToPrevious: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }))

    }

    func addHelpRoute() {
        self.addRoute(route: HelpViewController.ID, routeObject: Route(viewController: { () -> UIViewController in

            let viewHelp = HelpViewController(nibName: "HelpViewController", bundle: nil)
            let presenterHelp = HelpPresenter<HelpViewController>()
            viewHelp.hidesBottomBarWhenPushed = true
            viewHelp.presenter = presenterHelp
            presenterHelp.view = viewHelp
            return viewHelp

        }, animationToNext: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }, animationToPrevious: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }))
    }

    func addLeftMenuRoute() {
        self.addRoute(route: LeftMenuViewController.ID, routeObject: Route(viewController: { () -> UIViewController in

            let viewLeftMenu = LeftMenuViewController(nibName: "LeftMenuViewController", bundle: nil)
            let presenterLeftMenu = LeftMenuPresenter<LeftMenuViewController>()
            viewLeftMenu.presenter = presenterLeftMenu
            presenterLeftMenu.view = viewLeftMenu
            return viewLeftMenu

        }, animationToNext: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }, animationToPrevious: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }))
    }
    func addSideMenuSettingsRoute() {
        self.addRoute(route: ConfigurationWalletCellViewController.ID, routeObject: Route(viewController: { () -> UIViewController in

            let viewSideMenuCell = ConfigurationWalletCellViewController(nibName: "WalletCellsViewController", bundle: nil)
            let presenterSideMenu = ConfigurationWalletCellPresenter<ConfigurationWalletCellViewController>()
            viewSideMenuCell.presenter = presenterSideMenu
            presenterSideMenu.view = viewSideMenuCell
            viewSideMenuCell.hidesBottomBarWhenPushed = true
            return viewSideMenuCell

        }, animationToNext: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }, animationToPrevious: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }))
    }
    func addSideMenuAboutRoute() {
        self.addRoute(route: AboutViewController.ID, routeObject: Route(viewController: { () -> UIViewController in

            let viewSideMenuCell = AboutViewController(nibName: "WalletCellsViewController", bundle: nil)
            let presenterSideMenu = AboutPresenter<AboutViewController>()
            viewSideMenuCell.presenter = presenterSideMenu
            presenterSideMenu.view = viewSideMenuCell
            viewSideMenuCell.hidesBottomBarWhenPushed = true
            return viewSideMenuCell

        }, animationToNext: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }, animationToPrevious: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }))
    }
    func addAnonymousHomeRoute() {

        self.addRoute(route: AnonymousTutorialViewController.HomeID, routeObject: Route(viewController: { () -> UIViewController in

            let storyboardInitialViewController = UIStoryboard(name: "AnonymousTutorialViewController", bundle: nil).instantiateInitialViewController()
            guard let anonymousTutorialViewController = storyboardInitialViewController as? AnonymousTutorialViewController else {
                return UIViewController()
            }

            let displayData = AnonymousTutorialDisplayData(navigationBarTitle: Localizables.common.key_tabbar_start_button,
                                                           animationName: ConstantsLottieAnimations.anonymousHome,
                                                           title: Localizables.login.key_dashboard_welcome,
                                                           subtitle: Localizables.login.key_dashboard_description)

            let anonymousTutorialPresenter = AnonymousTutorialPresenter<AnonymousTutorialViewController>(displayData: displayData)

            anonymousTutorialViewController.presenter = anonymousTutorialPresenter
            anonymousTutorialPresenter.view = anonymousTutorialViewController

            anonymousTutorialViewController.tabBarItem = UITabBarItem(title: Localizables.common.key_tabbar_start_button, image: ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Home.wallet_icon, color: .GRAYTABBAR), width: 18, height: 18), tag: 1)
            anonymousTutorialViewController.tabBarItem.setTitleTextAttributes([NSAttributedStringKey.font: Tools.setFontMedium(size: 10)], for: .normal)
            anonymousTutorialViewController.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -3)

            return anonymousTutorialViewController

        }, animationToNext: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)
        }, animationToPrevious: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)
        }))
    }

    func addAnonymousCardsRoute() {

        self.addRoute(route: AnonymousTutorialViewController.CardsID, routeObject: Route(viewController: { () -> UIViewController in

            let storyboardInitialViewController = UIStoryboard(name: "AnonymousTutorialViewController", bundle: nil).instantiateInitialViewController()
            guard let anonymousTutorialViewController = storyboardInitialViewController as? AnonymousTutorialViewController else {
                return UIViewController()
            }

            let displayData = AnonymousTutorialDisplayData(navigationBarTitle: Localizables.common.key_tabbar_cards_button,
                                                           animationName: ConstantsLottieAnimations.anonymousCards,
                                                           title: Localizables.login.key_dashboard_pay_with_wallet_title,
                                                           subtitle: Localizables.login.key_dashboard_pay_with_wallet_description)

            let anonymousTutorialPresenter = AnonymousTutorialPresenter<AnonymousTutorialViewController>(displayData: displayData)

            anonymousTutorialViewController.presenter = anonymousTutorialPresenter
            anonymousTutorialPresenter.view = anonymousTutorialViewController

            anonymousTutorialViewController.tabBarItem = UITabBarItem(title: Localizables.common.key_tabbar_cards_button, image: ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Card.creditcard_icon, color: .GRAYTABBAR), width: 18, height: 18), tag: 2)
            anonymousTutorialViewController.tabBarItem.setTitleTextAttributes([NSAttributedStringKey.font: Tools.setFontMedium(size: 10)], for: .normal)
            anonymousTutorialViewController.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -3)

            return anonymousTutorialViewController

        }, animationToNext: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)
        }, animationToPrevious: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)
        }))
    }

    func addAnonymousRewardsRoute() {

        self.addRoute(route: AnonymousTutorialViewController.RewardsID, routeObject: Route(viewController: { () -> UIViewController in

            let storyboardInitialViewController = UIStoryboard(name: "AnonymousTutorialViewController", bundle: nil).instantiateInitialViewController()
            guard let anonymousTutorialViewController = storyboardInitialViewController as? AnonymousTutorialViewController else {
                return UIViewController()
            }

            let displayData = AnonymousTutorialDisplayData(navigationBarTitle: Localizables.points.key_points_text,
                                                           animationName: ConstantsLottieAnimations.anonymousPoints,
                                                           title: Localizables.login.key_dashboard_get_points_title,
                                                           subtitle: Localizables.login.key_dashboard_get_points_description)
            let anonymousTutorialPresenter = AnonymousTutorialPresenter<AnonymousTutorialViewController>(displayData: displayData)

            anonymousTutorialViewController.presenter = anonymousTutorialPresenter
            anonymousTutorialPresenter.view = anonymousTutorialViewController

            anonymousTutorialViewController.tabBarItem = UITabBarItem(title: Localizables.common.key_tabbar_points_button, image: ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Promotions.menu_promotion_icon, color: .GRAYTABBAR), width: 18, height: 18), tag: 4)
            anonymousTutorialViewController.tabBarItem.setTitleTextAttributes([NSAttributedStringKey.font: Tools.setFontMedium(size: 10)], for: .normal)
            anonymousTutorialViewController.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -3)

            return anonymousTutorialViewController

        }, animationToNext: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)
        }, animationToPrevious: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)
        }))
    }

    func addAnonymousNotificationsRoute() {

        self.addRoute(route: AnonymousTutorialViewController.NotificationsID, routeObject: Route(viewController: { () -> UIViewController in

            let storyboardInitialViewController = UIStoryboard(name: "AnonymousTutorialViewController", bundle: nil).instantiateInitialViewController()
            guard let anonymousTutorialViewController = storyboardInitialViewController as? AnonymousTutorialViewController else {
                return UIViewController()
            }

            let displayData = AnonymousTutorialDisplayData(navigationBarTitle: Localizables.common.key_tabbar_notice_button,
                                                           animationName: ConstantsLottieAnimations.anonymousNotifications,
                                                           title: Localizables.login.key_dashboard_stay_informed_title,
                                                           subtitle: Localizables.login.key_dashboard_stay_informed_description)

            let anonymousTutorialPresenter = AnonymousTutorialPresenter<AnonymousTutorialViewController>(displayData: displayData)

            anonymousTutorialViewController.presenter = anonymousTutorialPresenter
            anonymousTutorialPresenter.view = anonymousTutorialViewController

            anonymousTutorialViewController.tabBarItem = UITabBarItem(title: Localizables.common.key_tabbar_notice_button, image: ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Notifications.alarm_icon, color: .GRAYTABBAR), width: 18, height: 18), tag: 5)
            anonymousTutorialViewController.tabBarItem.setTitleTextAttributes([NSAttributedStringKey.font: Tools.setFontMedium(size: 10)], for: .normal)
            anonymousTutorialViewController.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -3)

            return anonymousTutorialViewController

        }, animationToNext: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)
        }, animationToPrevious: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)
        }))
    }
    
    func addUpgradeNotificationRoute() {
        
        self.addRoute(route: UpgradeNotificationViewController.ID, routeObject: Route(viewController: { () -> UIViewController in
            
            let storyboardInitialViewController = UIStoryboard(name: "UpgradeNotificationViewController", bundle: nil).instantiateInitialViewController()
            guard let viewUpgradeNotification = storyboardInitialViewController as? UpgradeNotificationViewController else {
                return UIViewController()
            }
            
            let presenterUpgradeNotification = UpgradeNotificationPresenter<UpgradeNotificationViewController>()
            
            viewUpgradeNotification.presenter = presenterUpgradeNotification
            presenterUpgradeNotification.view = viewUpgradeNotification
            
            return viewUpgradeNotification
            
        }, animationToNext: { _, destinationViewController in
            
            self.animation(viewController: destinationViewController!)
            
        }, animationToPrevious: { _, destinationViewController in
            
            self.animation(viewController: destinationViewController!)
            
        }))
    }

    func animation(viewController: UIViewController) {
        UIView.animate(withDuration: 1, animations: {
            viewController.view.backgroundColor = .brown
            viewController.view.frame.size.width += 10
            viewController.view.frame.size.height += 10
        }, completion: { _  in
            UIView.animate(withDuration: 1, delay: 0.25, options: [.autoreverse, .repeat], animations: {
                viewController.view.frame.origin.y -= 20
            })
        })
    }

}
