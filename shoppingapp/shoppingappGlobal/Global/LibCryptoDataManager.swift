//
//  LibCryptoDataManager.swift
//  shoppingappMX
//
//  Created by Armando Vazquez on 5/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import RxSwift

class LibCryptoDataManager: BaseDataManager {
    
    static var sharedInstance = LibCryptoDataManager()
    let libCrypt = LibMCrypt()
    
    /// Returns a public key
    /// - Parameter _info_: LibMCrypt as of 1.4 takes this argument as nil
    func generateKeyPair(with info: String?) -> Observable<PublicKeyEntity> {
        
        do {
            let publicKey = try self.libCrypt.generateKpub(info, error: ())
            let cryptoEntity = PublicKeyEntity(keyData: publicKey)
            return .just(cryptoEntity)
        } catch let thrownError {
            return makeObservableCryptError(from: thrownError)
        }
    }
    
    func decryptFromHost(hostData: String) -> Observable<HostDataEntity> {
        
        do {
            let decryptedData = try self.libCrypt.decryptHostData(hostData, error: ())
            let cryptoEntity = HostDataEntity(data: decryptedData, check: nil)
            return .just(cryptoEntity)
        } catch let thrownError {
            return makeObservableCryptError(from: thrownError)
        }
        
    }
    
    func encryptDataForHost(data: String) -> Observable<HostDataEntity> {
        
        do {
            let encryptedData = try self.libCrypt.encryptHostData(data, infoToDataCheck: nil, error: ())
            let cryptoEntity = HostDataEntity(hostData: encryptedData)
            return .just(cryptoEntity)
        } catch let thrownError {
            return makeObservableCryptError(from: thrownError)
        }
    }
    
    func encryptDataForHost(data: String, keyTag: String) -> Observable<HostDataEntity> {
        
        do {
            let encryptedData = try self.libCrypt.encryptHostData(data, infoToDataCheck: nil, withKeyTag: keyTag)
            let cryptoEntity = HostDataEntity(hostData: encryptedData)
            return .just(cryptoEntity)
        } catch let thrownError {
            return makeObservableCryptError(from: thrownError)
        }
    }

    private func makeObservableCryptError<T>(from error: Error) -> Observable<T> {
        
        let cryptoErrorEntity = ErrorEntity(error.localizedDescription, code: error._code)
        let cryptoServiceError = ServiceError.GenericErrorEntity(error: cryptoErrorEntity)
        return Observable.error(cryptoServiceError)
    }
    
}
