//
//  Router.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 25/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

class RouterGlobal {

    static let ID: String = String(describing: RouterGlobal.self)

    // swiftlint:disable weak_delegate
    var transitionDelegate: UIViewControllerTransitioningDelegate?
    // swiftlint:enable weak_delegate

    // MARK: - OTP -

    func navigateOtp(withHeader header: [String: String]) {
        BusManager.sessionDataInstance.hideLoading(completion: {

            if let securityVC = BusManager.sessionDataInstance.routerFactory?.getViewController(route: SecurityViewController.ID) as? SecurityViewController {
                let navigationController = UINavigationController(rootViewController: securityVC)
                securityVC.responseHeader = header
                BusManager.sessionDataInstance.present(viewController: navigationController, animated: true, completion: nil)
            }
        })
    }
    
    // MARK: - Welcome -
    
    func navigateToWelcome() {
        
        let welcomeVC = BusManager.sessionDataInstance.routerFactory?.getViewController(route: WelcomeViewController.ID)
        (UIApplication.shared.delegate as? AppDelegate)?.window?.rootViewController = welcomeVC
    }

    // MARK: - Login -

    func navigateFirstScreenFromLogin(withModel model: Model) {

        let viewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: LoginLoaderViewController.ID)! as! LoginLoaderViewController
        viewController.initModel(withModel: model)
        BusManager.sessionDataInstance.navigationController?.pushViewController(viewController, animated: true)
    }

    func navigateLogin() {
        
        BusManager.sessionDataInstance.setRootWhenNoSession()
        
    }

    func navigateErrorFromLoginLoader(withModel model: Model) {

        if let viewController = BusManager.sessionDataInstance.getViewControllerFromNavigation(route: LoginViewController.self) as? LoginViewController {
            
            viewController.initModel(withModel: model)
            BusManager.sessionDataInstance.navigationController?.popToViewController(viewController, animated: true)
        }
    }

    // MARK: - TabBar -

    func controllersForTabBar() -> [UIViewController] {

        let navigationFirstTab: UINavigationController = UINavigationController(rootViewController: (BusManager.sessionDataInstance.routerFactory?.getViewController(route: HomeViewController.ID))!)

        let cardsViewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: CardsViewController.ID)! as! CardsViewController
        let navigationSecondTab: UINavigationController = UINavigationController(rootViewController: cardsViewController)

        let navigationThirdTab: UINavigationController = UINavigationController(rootViewController: (BusManager.sessionDataInstance.routerFactory?.getViewController(route: ShoppingViewController.ID))!)

        let navigationFourthTab: UINavigationController = UINavigationController(rootViewController: (BusManager.sessionDataInstance.routerFactory?.getViewController(route: NotificationsViewController.ID))!)

        return [navigationFirstTab, navigationSecondTab, navigationThirdTab, navigationFourthTab]
    }

    func controllersForAnonymousTabBar() -> [UIViewController] {

        var tabBarViewControllers = [UIViewController]()

        let anonymousHomeViewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: AnonymousTutorialViewController.HomeID)
        if let homeViewController = anonymousHomeViewController {
            let homeTabNavigationController = UINavigationController(rootViewController: homeViewController)
            tabBarViewControllers.append(homeTabNavigationController)
        }

        let anonymousCardsViewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: AnonymousTutorialViewController.CardsID)
        if let cardsViewController = anonymousCardsViewController {
            let cardsTabNavigationController = UINavigationController(rootViewController: cardsViewController)
            tabBarViewControllers.append(cardsTabNavigationController)
        }

        let anonymousShoppingViewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: ShoppingViewController.ID)
        if let shoppingViewController = anonymousShoppingViewController {
            let shoppingTabNavigationController = UINavigationController(rootViewController: shoppingViewController)
            tabBarViewControllers.append(shoppingTabNavigationController)
        }

        let anonymousRewardsViewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: AnonymousTutorialViewController.RewardsID)
        if let rewardsViewController = anonymousRewardsViewController {
            let rewardsTabNavigationController = UINavigationController(rootViewController: rewardsViewController)
            tabBarViewControllers.append(rewardsTabNavigationController)
        }

        let anonymousNotificationViewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: AnonymousTutorialViewController.NotificationsID)
        if let notificationViewController = anonymousNotificationViewController {
            let notificationsTabNavigationController = UINavigationController(rootViewController: notificationViewController)
            tabBarViewControllers.append(notificationsTabNavigationController)
        }

        return tabBarViewControllers
    }

    func navigateWhenSessionFromLoginLoader() {

        let controllers = controllersForTabBar()

        BusManager.sessionDataInstance.configureTabBar(withController: controllers, selectIndex: 0)
    }

    func navigateWhenAnonymousSessionFromLoginLoader() {

        let controllers = controllersForAnonymousTabBar()

        BusManager.sessionDataInstance.configureTabBar(withController: controllers, selectIndex: 2)
    }

    func navigateToRootCards() {

        let navigationViewController = changeTabToViewController(ofType: CardsViewController.self, withModel: nil, atRoot: true) as? UINavigationController

        let cardsViewController = navigationViewController?.topViewController as? CardsViewController
        if let cardsViewController = cardsViewController {
            if let scrollView = cardsViewController.scrollView {
                scrollView.setContentOffset(.zero, animated: false)
            }
        }

        BusManager.sessionDataInstance.forceDismissAllPresentedViewControllers(completion: nil)
        if let webController: WebController = ComponentManager.get(id: WebController.ID) {
            webController.logoutWebView()
        }
    }

    func changeTabToCards(withModel model: Model) {

        changeTabToViewController(ofType: CardsViewController.self, withModel: model)
    }

    func changeTabToPromotions() {

        changeTabToViewController(ofType: ShoppingViewController.self, withModel: nil)
    }

    func changeTabToPoints() {

        changeTabToViewController(ofType: viewControllerClassForPoints(), withModel: nil)
    }

    func viewControllerClassForPoints() -> AnyClass {

        fatalError("viewControllerClassForPoints() must be overridden")
    }

    @discardableResult
    private func changeTabToViewController(ofType type: AnyClass, withModel model: Model?, atRoot: Bool = false) -> UIViewController? {

        if  let tabBarController = MainViewController.sharedInstance.rootViewController as? UITabBarController,
            let targetViewController = BusManager.sessionDataInstance.getViewControllerFromTabBar(route: type),
            let targetNavigationController = targetViewController.navigationController,
            let targetIndex = tabBarController.viewControllers?.index(of: targetNavigationController) {

            if let model = model {
                targetViewController.initModel(withModel: model)
            }

            BusManager.sessionDataInstance.setSelectedNavigationController(targetNavigationController)
            BusManager.sessionDataInstance.omniture(forTabIndex: targetIndex)
            tabBarController.selectedIndex = targetIndex

            if atRoot {
                targetNavigationController.popToRootViewController(animated: false)
            }

            return targetNavigationController
        }

        return nil
    }

    // MARK: - Transactions -

    func navigateFirstScreenFromTransactionList(withModel model: Model) {

        let viewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: TransactionsFilterViewController.ID)! as! TransactionsFilterViewController
        viewController.initModel(withModel: model)
        let navigationController = UINavigationController(rootViewController: viewController)
        BusManager.sessionDataInstance.present(viewController: navigationController, animated: true, completion: nil)
    }

    func navigateSecondScreenFromTransactionList(withModel model: Model) {

        let viewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: TransactionDetailViewController.ID)! as! TransactionDetailViewController
        viewController.initModel(withModel: model)
        BusManager.sessionDataInstance.navigationController?.pushViewController(viewController, animated: true)
    }

    func navigateFirstScreenFromCards(withModel model: Model) {

        let viewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: TransactionListViewController.ID)! as! TransactionListViewController
        viewController.initModel(withModel: model)
        BusManager.sessionDataInstance.navigationController?.pushViewController(viewController, animated: true)
    }

    func navigateSecondScreenFromCards(withModel model: Model) {

        let viewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: TransactionsFilterViewController.ID)! as! TransactionsFilterViewController
        viewController.initModel(withModel: model)

        let navigationController = UINavigationController(rootViewController: viewController)
        transitionDelegate = DismissPopAnimatorDelegate()
        navigationController.transitioningDelegate = transitionDelegate
        navigationController.modalPresentationStyle = .custom
        navigationController.modalPresentationCapturesStatusBarAppearance = true

        BusManager.sessionDataInstance.present(viewController: navigationController, animated: true, completion: nil)
    }

    func navigateThirdScreenFromCards(withModel model: Model) {

        let viewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: TransactionDetailViewController.ID)! as! TransactionDetailViewController
        viewController.initModel(withModel: model)
        BusManager.sessionDataInstance.navigationController?.pushViewController(viewController, animated: true)
    }

    func navigateFirstScreenFromTransactionFilter(withModel model: Model) {

        let viewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: TransactionListViewController.ID)! as! TransactionListViewController
        viewController.initModel(withModel: model)
        BusManager.sessionDataInstance.navigationController?.pushViewController(viewController, animated: true)
    }

    // MARK: - Promotions -

    func navigateFirstScreenFromShopping(withModel model: Model) {

        let viewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: ShoppingListViewController.ID)! as! ShoppingListViewController
        viewController.initModel(withModel: model)
        BusManager.sessionDataInstance.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToShoppingContact(withModel model: Model) {
        
        let viewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: ShoppingContactViewController.ID)! as! ShoppingContactViewController
        viewController.initModel(withModel: model)
        BusManager.sessionDataInstance.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToShoppingPromotionsCategory(withModel model: Model) {
        
        let viewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: ShoppingListViewController.ID)! as! ShoppingListViewController
        viewController.initModel(withModel: model)
        BusManager.sessionDataInstance.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToShoppingDetail(withModel model: Model) {

        let viewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: ShoppingDetailViewController.ID)! as! ShoppingDetailViewController
        viewController.initModel(withModel: model)
        let navigationController = UINavigationController(rootViewController: viewController)
        BusManager.sessionDataInstance.present(viewController: navigationController, animated: true, completion: nil)
    }

    func navigateToShoppingWebPage(withModel model: Model) {

        let viewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: ShoppingWebViewController.ID)! as! ShoppingWebViewController
        viewController.initModel(withModel: model)
        BusManager.sessionDataInstance.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToShoppingGuarantee(withModel model: Model) {
        
        let viewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: ShoppingGuaranteeViewController.ID)! as! ShoppingGuaranteeViewController
        viewController.initModel(withModel: model)
        BusManager.sessionDataInstance.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToShoppingFilter(withModel model: Model) {

        let viewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: ShoppingFilterViewController.ID)! as! ShoppingFilterViewController
        viewController.initModel(withModel: model)

        let navigationController = UINavigationController(rootViewController: viewController)
        BusManager.sessionDataInstance.present(viewController: navigationController, animated: true, completion: nil)
    }

    func navigateShoppingFilterFromShoppingListFilteredPop(withModel model: Model) {

        let viewController = BusManager.sessionDataInstance.getViewControllerFromNavigation(route: ShoppingFilterViewController.self) as! ShoppingFilterViewController
        viewController.initModel(withModel: model)
        BusManager.sessionDataInstance.navigationController?.popToViewController(viewController, animated: true)
    }

    func navigateShoppingListFilteredFromShoppingFilter(withModel model: Model) {

        let viewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: ShoppingListFilteredViewController.ID)! as! ShoppingListFilteredViewController
        viewController.initModel(withModel: model)
        BusManager.sessionDataInstance.navigationController?.pushViewController(viewController, animated: true)
    }

    func navigateShoppingMapListFromShoppingMap(withModel model: Model) {

        let viewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: ShoppingMapListViewController.ID)! as! ShoppingMapListViewController
        viewController.initModel(withModel: model)
        BusManager.sessionDataInstance.navigationController?.pushViewController(viewController, animated: true)
    }

    // MARK: - Limits -

    func navigateFourScreenFromCards(withModel model: Model) {

        let viewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: LimitsViewController.ID)! as! LimitsViewController
        viewController.initModel(withModel: model)
        BusManager.sessionDataInstance.navigationController?.pushViewController(viewController, animated: true)
    }

    func navigateToOnOffHelp(model: Model) {

        let viewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: OnOffHelpViewController.ID)! as! OnOffHelpViewController
        viewController.initModel(withModel: model)
        let navigationController = UINavigationController(rootViewController: viewController)
        BusManager.sessionDataInstance.present(viewController: navigationController, animated: true, completion: nil)
    }

    func navigateEditLimitsFromLimits(withModel model: Model) {

        let viewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: EditLimitViewController.ID)! as! EditLimitViewController
        viewController.initModel(withModel: model)
        BusManager.sessionDataInstance.navigationController?.pushViewController(viewController, animated: true)
    }

    func navigateLimitsFromEditLimitsPop(withModel model: Model) {

        let viewController = BusManager.sessionDataInstance.getViewControllerFromNavigation(route: LimitsViewController.self) as! LimitsViewController
        viewController.initModel(withModel: model)
        BusManager.sessionDataInstance.navigationController?.popToViewController(viewController, animated: false)
    }

    // MARK: - Others -

    func navigateToDetailHelp(withModel model: Model) {

        let viewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: HelpDetailViewController.ID)! as! HelpDetailViewController
        viewController.initModel(withModel: model)
        let navigationController = UINavigationController(rootViewController: viewController)
        BusManager.sessionDataInstance.present(viewController: navigationController, animated: true, completion: nil)
    }

    func navigateCardCVVScreenFromCards(model: Model) {

        let viewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: CvvViewController.ID)! as! CvvViewController
        viewController.initModel(withModel: model)
        let navigationController = UINavigationController(rootViewController: viewController)
        BusManager.sessionDataInstance.present(viewController: navigationController, animated: true, completion: nil)
    }

    func navigateToHelpFromLeftMenu(withModel model: Model) {

        let viewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: HelpViewController.ID)! as! HelpViewController
        viewController.initModel(withModel: model)
        BusManager.sessionDataInstance.navigationController?.pushViewController(viewController, animated: false)
    }

    func navigateToHelp(withModel model: Model) {

        let viewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: HelpViewController.ID)! as! HelpViewController
        viewController.initModel(withModel: model)
        BusManager.sessionDataInstance.navigationController?.pushViewController(viewController, animated: true)
    }
}
