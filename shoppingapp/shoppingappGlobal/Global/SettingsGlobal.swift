//
//  Settings.swift
//  shoppingapp
//
//  Created by jesus.martinez on 23/10/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

class SettingsGlobal {

    class GlobalSettings {

        class var default_bbva_phone_number: String {
            return ""
        }

        class var default_language: String {
            return "mx"
        }
        
        class var default_web_bbva: String {
            return ""
        }

        class var default_location: GeoLocationBO {
            return GeoLocationBO(withLatitude: 0.0, withLongitude: 0.0)
        }

        class var default_location_Timeout: TimeInterval {
            return 3.0
        }
        
        class var number_logins_to_show_rate_app: Int {
            return 5
        }
        
        class var omnitureConfigPath: String {
            return "ADBMobileConfig"
        }
    }
    
    class SecurityGlobal {
        
        class var softToken: String? {
            return nil
        }
        
        class var securityInterceptorEnabled: Bool {
            return false
        }
    }
    
    class LoginGlobal {
        class var user_maximum_length: Int? {
            return nil
        }
        class var password_maximum_length: Int? {
            return nil
        }
        class var user_keyboard_type: UIKeyboardType {
            return .default
        }
        class var password_keyboard_type: UIKeyboardType {
            return .default
        }
        class var user_valid_character_set: CharacterSet {
            return .alphanumerics
        }
        class var password_valid_character_set: CharacterSet {
            return .alphanumerics
        }
        class var showRememberPassword: Bool {
            return true
        }
    }

    class CardsLandingGlobal {
        class var show_direct_access_to_transactions: Bool {
            return false
        }

        class var show_credit_card_limit_info: Bool {
            return true
        }
    }

    class SecurityControllerGlobal {
        class var security_code_keyboard_type: UIKeyboardType {
            return .numberPad
        }
        class var security_code_maximum_length: Int {
            return 6
        }
        class var security_code_valid_character_set: CharacterSet {
            return .decimalDigits
        }
    }

    class PromotionsFilterGlobal {

        class var maximum_length_text_filter: Int {
            return 50
        }
        class var valid_characters_text_filter: CharacterSet {
            return CharacterSet(charactersIn: "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLKMNOPQRSTUVWXYZÁÃÄáãäÉËéëÍÏíïÑñÓÕÖóõöÚÜúüÝýÿ /-'%¡!")
        }

        class var promotion_types: [PromotionType] {
            return [.discount, .point]
        }

        class var promotion_usage_types: [PromotionUsageType] {
            return [.physical, .online]
        }

    }

    class PromotionsMapGlobal {

        class var max_promotions_in_carrousel: Int {
            return 4
        }
        class var default_distance: Int {
            return 100
        }
        class var default_distance_in_map: Int {
            return 2
        }
        class var default_length_type: String {
            return "KILOMETERS"
        }
        class var page_size: Int {
            return 50
        }

    }

    class PromotionsGlobal {

        class func effectiveDateText(forPromotionBO promotionBO: PromotionBO, compactSize: Bool) -> DisplayItemPromotion.EffectiveDateTextInfo? {

            let today = Calendar.current.startOfDay(for: Date())
            let startDate = Calendar.current.startOfDay(for: promotionBO.startDate)
            let endDate = Calendar.current.startOfDay(for: promotionBO.endDate)

            guard let startDiffDays = Calendar.current.dateComponents([.day], from: today, to: startDate).day  else {
                return nil
            }
            guard let endDiffDays = Calendar.current.dateComponents([.day], from: today, to: endDate).day  else {
                return nil
            }

            var effectiveDateText: String
            var isAInterval: Bool

            if endDiffDays == 0 {
                effectiveDateText = Localizables.promotions.key_finish_today
                isAInterval = false
            } else if endDiffDays <= 7 && endDiffDays > 1 && startDiffDays <= 0 {
                effectiveDateText = Localizables.promotions.key_promotions_left_text + " \(endDiffDays) " + Localizables.promotions.key_promotions_days_text
                isAInterval = true
            } else if startDiffDays <= 0 {

                isAInterval = false

                let formattedDate = promotionBO.endDate.string(format: Date.DATE_FORMAT_DAY_MONTH_YEAR_COMPACT)

                if compactSize {
                    effectiveDateText = Localizables.promotions.key_finish_text + " " + formattedDate
                } else {
                    effectiveDateText = Localizables.promotions.key_finish_in_text + " " + formattedDate
                }
            } else if startDiffDays > 0 && startDiffDays <= 5 && endDiffDays >= 1 {

                isAInterval = false

                let formattedDate = promotionBO.startDate.string(format: Date.DATE_FORMAT_DAY_MONTH_YEAR_COMPACT)

                if compactSize {
                    effectiveDateText = Localizables.promotions.key_starts_text + " " + formattedDate
                } else {
                    effectiveDateText = Localizables.promotions.key_starts_in_text + " " + formattedDate
                }
            } else {
                return nil
            }

            return (effectiveDateText, isAInterval)
        }

        class func dateDiffPlusDistance(distance: String, effectiveDateText: String, isIntervalDate: Bool) -> String {
            return effectiveDateText + (isIntervalDate ? " / " : " (") + distance + (isIntervalDate ? "" : ")")
        }

        class var should_show_configure_favorite_categories: Bool {
            return false
        }

        class var maximum_number_of_categories_to_show: Int {
            return 4
        }

        class var default_zoom_of_associated_trade_map: Float {
            return 16
        }
    }

    class TrendingPromotionComponentGlobal {
        class var should_filter_by_card_title_id: Bool {
            return false
        }
    }

    class TransactionsFilterGlobal {
        class var should_show_concept_filter: Bool {
            return true
        }
    }

    class TransactionsListGlobal {
        class var hideFilteringOfTransactions: Bool {
            return true
        }
    }

    class TimeoutGlobal {
        class var timeout_for_expired_session: TimeInterval {
            return 120.0
        }
    }

    class CardCvvGlobal {
        class var time_duration_for_show_cvv: TimeInterval {
            return 30.0
        }

        class var time_alert_for_show_cvv: TimeInterval {
            return 10.0
        }

        class var cvvId: String {
            return "CVV"
        }
        
        class func hexadecimalEncodingForCVV(cvv: String) -> String {
            
            let encryptionPaddingLength = 14
            
            // ISO 9564 Format 2
            let padding = "".padding(toLength: encryptionPaddingLength - cvv.count, withPad: "F", startingAt: 0)
            let hexCVVString = String(format: "2%d%@%@", cvv.count, cvv, padding)
            
            return hexCVVString
        }
    }

    class DidacticAreaGlobal {

        class var times_of_day_morning: Range<Int> {
            return 6..<12
        }

        class var times_of_day_afternoon: Range<Int> {
            return 12..<18
        }

        class var times_of_day_night: Range<Int> {
            return 18..<6
        }
    }

    class HomeGlobal {

        class var cardTypesToShowInHome: [PhysicalSupportType] {
            return [.normalPlastic]
        }

        class var showPointsInHome: Bool {
            return false
        }

        class var viewControllerForCustomerPoints: UIViewController? {

            return nil
        }
    }

    class LimitsGlobal {

        class var multiples_limit_amount: Double {
            return 50
        }

    }

    class CardHelpOnOffGlobal {

        class func localizationKeyForHelpOnOffAndCard(card: CardBO) -> String {
            return "KEY_CARD_HELP_ON_OFF_"
        }
    }

    class FavoriteCategoryGlobal {
        class var number_of_favorite_categories: Int {
            return 3
        }
    }
    
    class SSLPinningGlobal {
        
        class var certificates: [String] {
            
            return ["*.s3.amazonaws.com.cer",
                    "openapi.bbva.com.cer"]
        }
    }
    
    class MigrationGlobal {
        class var versionKey: String {
            return "kVersionSave"
        }
    }
}
