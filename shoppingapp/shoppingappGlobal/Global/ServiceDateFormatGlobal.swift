//
//  ServiceDateFormatGlobal.swift
//  shoppingapp
//
//  Created by AZIZEBULBUL on 25/07/2018.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class ServiceDateFormatGlobal {

    static var dateFormatter = DateFormatter()

    class func dateFormat(forService: Services) -> String {

        return ""
    }

    class func serviceDateFormatter(service: Services) -> DateFormatter {

        dateFormatter.dateFormat = dateFormat(forService: service)
        return dateFormatter

    }
}

enum Services {
    case customers

}
