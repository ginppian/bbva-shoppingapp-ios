//
//  BVAGlobalProtocol.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 12/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit

protocol CellsDTO: class {

}

protocol CellsPresenterProtocol: PresenterProtocol {

    func showPageSuccess(modelBO: ModelBO?)
    func registerOmniture(page: String)
    
}

extension CellsPresenterProtocol {

    func showPageSuccess(modelBO: ModelBO?) {

    }
    func registerOmniture(page: String) {

    }
}

protocol PresenterProtocol: class {

    var errorBO: ErrorBO? { get set }
    var routerTag: String { get set }
    var viewDidLoad: Bool { get }

    func checkError()
    func showModal()

    func showLoading(completion: (() -> Void)?)
    func hideLoading(completion: (() -> Void)?)
    func setModel(model: Model)
}

protocol ViewProtocol: class {

    func showError(error: ModelBO)
    func changeColorEditTexts()
}

protocol InteractorProtocol: class {

}

protocol ViewPickerProtocol: class {

//    func showPicker(options: PickerOptions)
}
