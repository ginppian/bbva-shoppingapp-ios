//
//  main.swift
//  shoppingapp
//
//  Created by AZIZEBULBUL on 01/02/2018.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit

_ = UIApplicationMain(
    CommandLine.argc,
    UnsafeMutableRawPointer(CommandLine.unsafeArgv)
        .bindMemory(
            to: UnsafeMutablePointer<Int8>.self,
            capacity: Int(CommandLine.argc)),
    NSStringFromClass(ShoppingApplication.self),
    NSStringFromClass(AppDelegate.self)
)
