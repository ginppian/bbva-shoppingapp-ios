//
//  KeychainManagerGlobal.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 11/01/2018.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import SwiftKeychainWrapper

class KeychainManagerGlobal {

    //Dont forget to clean them in the reset method if create a new element
    private static let firstNameKey = "firstName"
    private static let userIdKey = "userId"
    private static let uniqueIdentifierKey = "uniqueIdentifier"

    var keyChainWrapper: KeychainWrapper {
        return KeychainWrapper.standard
    }

    func getUniqueIdentifier() -> String? {
        return keyChainWrapper.string(forKey: KeychainManagerGlobal.uniqueIdentifierKey)
    }
    
    func saveUniqueIdentifier(forUniqueIdentifier ident: String) {
        keyChainWrapper.set(ident, forKey: KeychainManagerGlobal.uniqueIdentifierKey)
    }
    
    func isUserSaved() -> Bool {
        
        if !Tools.isStringNilOrEmpty(withString: getFirstname()) && !Tools.isStringNilOrEmpty(withString: getUserId()) {
            return true
        }
        return false
    }
    
    func getUserId() -> String {
        return keyChainWrapper.string(forKey: KeychainManagerGlobal.userIdKey) ?? ""
    }
    
    func saveUserId(forUserId userId: String) {
        keyChainWrapper.set(userId, forKey: KeychainManagerGlobal.userIdKey)
    }
    
    func getFirstname() -> String {
        return keyChainWrapper.string(forKey: KeychainManagerGlobal.firstNameKey) ?? ""
    }
    
    func saveFirstname(forFirstname firstname: String) {
        keyChainWrapper.set(firstname, forKey: KeychainManagerGlobal.firstNameKey)
    }
    
    func resetKeyChainItems() {
        keyChainWrapper.removeObject(forKey: KeychainManagerGlobal.firstNameKey)
        keyChainWrapper.removeObject(forKey: KeychainManagerGlobal.userIdKey)
        keyChainWrapper.removeObject(forKey: KeychainManagerGlobal.uniqueIdentifierKey)
    }
}
