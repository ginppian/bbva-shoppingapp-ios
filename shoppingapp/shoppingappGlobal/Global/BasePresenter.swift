//
//  BVAPresenter.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 12/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation
import RxSwift
import CellsNativeComponents
import CellsNativeCore

class BasePresenter<T: ViewProtocol>: PresenterProtocol {

    var busManager = BusManager.sessionDataInstance

    weak var view: T?

    var errorBO: ErrorBO?

    var routerTag: String = ""

    var viewDidLoad = false
    
    var isShowLoading = false

    init(tag: String) {
        routerTag = tag
        ComponentManager.add(id: tag, component: self)
    }

    init(busManager: BusManager) {
        self.busManager = busManager
    }

    // MARK: Este será el chequeo general de errores. Si alguien varia lo tendrá que sobreescribir
    func checkError() {

        if let errorBO = errorBO {
            if !errorBO.isErrorShown {
                
                if Settings.Security.securityInterceptorEnabled && errorBO.status == ConstantsHTTPCodes.unauthorized && errorBO.code == String(format: "\(ConstantsHTTPCodes.otpErrorCode)") {
                    //Security - Not show error
                } else {
                    view?.showError(error: errorBO)
                }
                
            } else if errorBO.sessionExpired() {
                expiredSession()
            }
        }

    }

    // MARK: Cada presenter podrá sobreescribir este método
    func showModal() {
        busManager.showModalError(error: errorBO!, delegate: view as? CloseModalDelegate)
        errorBO?.isErrorShown = true
    }

    func showLoading(completion: (() -> Void)? = nil) {
        isShowLoading = true
        busManager.showLoading(completion: completion)
    }

    func hideLoading(completion: (() -> Void)? = nil) {
        busManager.hideLoading(completion: completion)
        isShowLoading = false
    }

    func setModel(model: Model) {

    }

    private func expiredSession() {
        busManager.expiredSession()
        SessionDataManager.sessionDataInstance().closeSession()
    }
}
