//
//  BusManager.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 7/9/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation
import CellsNativeComponents
import CellsNativeCore
import WebKit
import Component_DM_Header_Interceptor
import BBVA_Network
import Component_IC_Logger
import os

let networkLog = OSLog(subsystem: "com.bbva.app.shoppingapp", category: "NetworkActivity")
let networkActivityLoggingLevel = 1
let networkErrorLoggingLevel = 2

class BusManagerGlobal: NSObject {

    // MARK: Static vars
    static var sessionDataInstance = BusManager()

    // MARK: Public vars
    var routerFactory: RouterInitFactoryGlobal?

    var networkWorker: NetworkWorker?
    var sessionConfiguration: URLSessionConfiguration?
    var mediaDownloadSession: URLSession?
    
    var networkLogger: NetworkLogger?

    var headerInterceptorStore: HeaderInterceptorStore?

    var securityInterceptorStore: SecurityInterceptorStore?

    var pageReactionManager: PageReactionGlobal = PageReactionGlobal()

    var navigationController: UINavigationController? {
        get {
            return selectedNavigationController?.topNavigationController()
        }
        set {
            selectedNavigationController = newValue
        }
    }

    var errorView: ErrorViewController?
    var errorBO: ErrorBO?
    
    var currentCellsPage = ""

    // MARK: Private vars

    fileprivate var selectedNavigationController: UINavigationController?

    fileprivate var presentingViewManager = PresentingViewsManager()

    // MARK: Reactive api

    func navigateScreen<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {

        fire(tag: tag, event: event, values: values)

    }

    func publishData<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {

        fire(tag: tag, event: event, values: values)

    }

    func notify<T>(tag: String, _ event: ActionSpec<T>) {

        fire(tag: tag, event: event)

    }

    func clearData<T>(tag: String, _ event: ActionSpec<T>) {

        clear(tag: tag, event: event)
    }

    private func fire<T>(tag: String, event: ActionSpec<T>, values: T) {
        Reactor.inside(componentId: tag)
            .doAction(actionSpec: event)
            .with(value: values)
            .fire()
    }

    private func fire<T>(tag: String, event: ActionSpec<T>) {
        Reactor.inside(componentId: tag)
            .doAction(actionSpec: event)
            .fire()
    }

    private func clear<T>(tag: String, event: ActionSpec<T>) {
        Reactor.inside(componentId: tag)
            .doAction(actionSpec: event)
            .clean()
    }

    // MARK: View controllers logic and navigation

    func getViewControllerFromNavigation(route: AnyClass) -> BaseViewController {

        for controller in navigationController!.viewControllers as Array {
            if controller.isKind(of: route) {
                return controller as! BaseViewController
            }

        }
        return BaseViewController()
    }

    func getViewControllerFromTabBar(route: AnyClass) -> BaseViewController? {

        let tabBarController = MainViewController.sharedInstance.rootViewController as? UITabBarController

        guard let viewControllersInTabBar = tabBarController?.viewControllers else {
            return nil
        }

        for index in 0..<viewControllersInTabBar.count {

            if let nvc = viewControllersInTabBar[index] as? UINavigationController {

                for controller in nvc.viewControllers {
                    if controller.isKind(of: route) {
                        return controller as? BaseViewController
                    }
                }

            } else {
                if viewControllersInTabBar[index].isKind(of: route) {
                    return viewControllersInTabBar[index] as? BaseViewController
                }
            }
        }

        return nil
    }

    func setSelectedNavigationController(_ navigationController: UINavigationController) {
        selectedNavigationController = navigationController
    }

    func enableSwipeLateralMenu() {
        MainViewController.sharedInstance.isLeftViewSwipeGestureEnabled = true
    }

    func disableSwipeLateralMenu() {
        MainViewController.sharedInstance.isLeftViewSwipeGestureEnabled = false
    }

    func showModalError(error: ModelBO, delegate: CloseModalDelegate? = nil) {

        errorView = ErrorViewController(nibName: "ErrorViewController", bundle: nil)

        errorView?.delegate = delegate

        let presenter = ErrorPresenter<ErrorViewController>()

        errorView?.presenter = presenter

        presenter.view = errorView

        presenter.errorBO = error as? ErrorBO

        errorView?.modalPresentationStyle = .overCurrentContext

        if let errorView = errorView {
            presentingViewManager.present(viewController: errorView, animated: true, completion: nil)
        }
    }

    func showTicketDetail(withModel model: Model) {

        let ticketModel = model as! TicketModel

        if ticketModel.type != TicketType.unknown {

            let viewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: TicketDetailViewController.ID)! as! TicketDetailViewController
            viewController.initModel(withModel: model)

            let aObjNavi = UINavigationController(rootViewController: viewController)

            presentingViewManager.present(viewController: aObjNavi, animated: true, completion: nil, modalPresentationStyle: .overCurrentContext)

        }
    }

    func back() {
        navigationController?.popViewController(animated: true)
    }

    func present(viewController: UIViewController, animated: Bool, completion: (() -> Void)?, modalPresentationStyle: UIModalPresentationStyle = .overCurrentContext  ) {
        
        presentingViewManager.present(viewController: viewController, animated: animated, completion: completion, modalPresentationStyle: modalPresentationStyle)
    }

    func dismissViewController(animated: Bool, completion: (() -> Void)? = nil) {
        presentingViewManager.dismissViewController(animated: animated, completion: { _ in
            if let completion = completion {
                completion()
            }
        })
    }

    func forceDismissAllPresentedViewControllers(completion: ((Bool) -> Void)? = nil) {

        presentingViewManager.forceDismissAllPresentedViewControllers(completion: completion)
    }

    func dismissPresentedViewControllers(completion: ((Bool) -> Void)? = nil) {
        presentingViewManager.dismissPresentedViewControllers(completion: completion)
    }

    func hideWebview(_ hide: Bool) {
        if let webController: WebController = ComponentManager.get(id: WebController.ID) {
            webController.webView.isHidden = hide
        }
    }

    func hideCellsControllerIfNeeded() {

        (BusManager.sessionDataInstance.navigationController?.viewControllers.last as? CellsBaseViewController)?.hideCellsViewIfNeededForWebViewZones()

    }

    func loadInitFactory(sessionConfiguration: URLSessionConfiguration? = nil) -> AppLifecycle {

        self.sessionConfiguration = sessionConfiguration
        
        loadRouter()
        loadPageReactions()
        createNetworkWorker()
        createMediaDownloadSession()

        let appLifeCycle: AppLifecycle = AppLifecycle.Builder { builder in
            builder.app = UIApplication.shared
            builder.routerFactory = routerFactory
            builder.pageReactionManager = pageReactionManager
        }.create()

        return appLifeCycle

    }

    func loadRouter() {
        routerFactory = RouterInitFactoryGlobal()
    }

    func loadPageReactions() {
        pageReactionManager = PageReactionGlobal()
    }

    func createNetworkWorker() {
        networkWorker = NetworkWorker()
        
        if let configuration = sessionConfiguration {
            networkWorker?.setCurrentEndPoint(endPoint: BBVAEndPointDefault(configuration: configuration))
        }
        
        configureLoggerInterceptor()
        
        headerInterceptorStore = HeaderInterceptorStore(worker: networkWorker!)
        securityInterceptorStore = SecurityInterceptorStore(worker: networkWorker!)

    }

    func createMediaDownloadSession() {

        if let configuration = sessionConfiguration {
            mediaDownloadSession = URLSession(configuration: configuration)
        } else {
            mediaDownloadSession = URLSession.shared
        }
    }

    func configureLoggerInterceptor() {
        
        let setting = LoggingSettings(logNetworkActivityLevel: networkActivityLoggingLevel, logNetworkErrorsLevel: networkErrorLoggingLevel)
        
            networkLogger = NetworkLogger(config: setting, networkLog: networkLog)
        
        if let logger = networkLogger {
            networkWorker?.addInterceptor(interceptor: logger)
        }
    }
    
    func loadSplash() {
        
        let viewController = routerFactory?.getViewController(route: SplashViewController.ID)!
        
        (UIApplication.shared.delegate as? AppDelegate)?.window?.rootViewController = viewController
        
    }
    
    func setRootWhenNoSession() {
        let navigation: UINavigationController = UINavigationController()
        navigation.isNavigationBarHidden = false
        navigation.view.backgroundColor = .clear
        navigation.navigationBar.shadowImage = UIImage()
        navigation.navigationBar.backgroundColor = .clear
        navigation.navigationBar.isTranslucent = false
        navigation.navigationBar.barTintColor = .NAVY
        navigation.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
        navigation.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: Tools.setFontBook(size: 18), NSAttributedStringKey.foregroundColor: UIColor.BBVAWHITE]

        if let viewController = routerFactory?.getViewController(route: LoginViewController.ID)! {

            navigation.pushViewController(viewController, animated: false)

            selectedNavigationController = navigation

            // side menu
            let mainViewController = MainViewController.sharedInstance
            mainViewController.rootViewController = navigation
            mainViewController.leftMenuVC.updateOptionsMenu()
            enableSwipeLateralMenu()
            
            (UIApplication.shared.delegate as? AppDelegate)?.window?.rootViewController = mainViewController
            
        }
    }

    func expiredSession() {

        if SessionDataManager.sessionDataInstance().isUserAnonymous || SessionDataManager.sessionDataInstance().isUserLogged {

            dismissPresentedViewControllers { [weak self] _ in

                guard let `self` = self else {
                    return
                }
                
                self.setRootWhenNoSession()
                
            }

        }

    }

    func clearCache() {
        URLCache.shared.removeAllCachedResponses()
        URLCache.shared.diskCapacity = 0
        URLCache.shared.memoryCapacity = 0
    }

    func clearCookies() {
        let cookieStorage = HTTPCookieStorage.shared

        guard let cookies = cookieStorage.cookies else {
            return
        }

        for cookie in cookies {
            cookieStorage.deleteCookie(cookie)
        }
    }

    // MARK: Loading

    func showLoading(completion: (() -> Void)?) {

        LoadingManager.shared().show(completion: completion)
    }

    func hideLoading(completion: (() -> Void)? ) {

        LoadingManager.shared().hide(completion: completion)
    }
}

// MARK: Initialization and configuration

extension BusManagerGlobal {

    func configureTabBar(withController controllers: [UIViewController], selectIndex: Int) {

        let tabBarController = UITabBarController()
        tabBarController.tabBar.barTintColor = UIColor.white
        tabBarController.tabBar.tintColor = UIColor.COREBLUE
        tabBarController.tabBar.backgroundColor = .white
        tabBarController.viewControllers = controllers
        tabBarController.delegate = self
        tabBarController.selectedIndex = selectIndex
        tabBarController.tabBar.isTranslucent = false

        omniture(forTabIndex: selectIndex)

        // side menu
        let mainViewController = MainViewController.sharedInstance
        mainViewController.rootViewController = tabBarController
        mainViewController.leftMenuVC.updateOptionsMenu()
        enableSwipeLateralMenu()

        selectedNavigationController = controllers[selectIndex] as? UINavigationController

        (UIApplication.shared.delegate as? AppDelegate)?.window?.rootViewController = mainViewController

    }

    func disableTabBar() {

        BusManager.sessionDataInstance.navigationController?.tabBarController?.tabBar.items?.forEach { $0.isEnabled = false }

    }
}

// MARK: Session

extension BusManagerGlobal {

    func expiredSessionWithWarning() {

        hideCellsControllerIfNeeded()

        if (UIApplication.shared.delegate as? AppDelegate)?.window?.rootViewController?.presentedViewController == nil {

            launchErrorSesionViewAndCloseSession()

        } else {

            dismissPresentedViewControllers(completion: { _ in
                self.launchErrorSesionViewAndCloseSession()
            })
        }
    }

    fileprivate func launchErrorSesionViewAndCloseSession() {
        let errorBO = ErrorBO(message: Localizables.login.key_expired_session, errorType: .info, allowCancel: false)
        showModalError(error: errorBO)
        
        setRootWhenNoSession()
        
        SessionDataManager.sessionDataInstance().requestLogout()

        SessionDataManager.sessionDataInstance().closeSession()

        MainViewController.sharedInstance.leftMenuVC.updateOptionsMenu()
    }

}

// MARK: Omniture

extension BusManagerGlobal {
    func omniture(forTabIndex tabIndex: Int) {

        switch tabIndex {
        case 0:

            SessionDataManager.sessionDataInstance().tabBarSelected = "app:privada:personas:inicio"
            publishData(tag: EventTracker.id, PageReactionConstants.NEW_TRACK_EVENT, Event(type: .startOption))

        case 1:

            SessionDataManager.sessionDataInstance().tabBarSelected = "app:privada:personas:tarjetas"
            publishData(tag: EventTracker.id, PageReactionConstants.NEW_TRACK_EVENT, Event(type: .cardsOption))

        case 2:

            SessionDataManager.sessionDataInstance().tabBarSelected = "app:privada:personas:promociones"
            publishData(tag: EventTracker.id, PageReactionConstants.NEW_TRACK_EVENT, Event(type: .promotionsOption))

        case 3:

            SessionDataManager.sessionDataInstance().tabBarSelected = "app:privada:personas:puntos"
            publishData(tag: EventTracker.id, PageReactionConstants.NEW_TRACK_EVENT, Event(type: .pointsOption))

        case 4:

            SessionDataManager.sessionDataInstance().tabBarSelected = "app:privada:personas:avisos"
            publishData(tag: EventTracker.id, PageReactionConstants.NEW_TRACK_EVENT, Event(type: .notificationsOption))

        default:

            SessionDataManager.sessionDataInstance().tabBarSelected = "app:privada:personas:inicio"
            publishData(tag: EventTracker.id, PageReactionConstants.NEW_TRACK_EVENT, Event(type: .startOption))

        }
    }
}

// MARK: TabBar

extension BusManagerGlobal: UITabBarControllerDelegate {

    func tabBarController(_ tabBarController: UITabBarController,
                          animationControllerForTransitionFrom fromVC: UIViewController,
                          to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
                
        if (toVC as? UINavigationController)?.viewControllers.first as? CardsViewController != nil, (fromVC as? UINavigationController)?.viewControllers.first as? HomeViewController != nil {

            if (toVC as? UINavigationController)?.viewControllers.first as? TransitionerInAnimationProtocol != nil,
                let fromTransitionVC = (fromVC as? UINavigationController)?.viewControllers.first as? TransitionerOutAnimationProtocol, let transitionImageView = fromTransitionVC.transitionImageView, let fromTransitionFrame = fromTransitionVC.transitionFrame() {

                return FaceOutInWithTransitionViewAnimationController(transitionImageView: transitionImageView, fromTransitionFrame: fromTransitionFrame)
            } else {
                return nil
            }

        } else if (toVC as? UINavigationController)?.viewControllers.first as? AnonymousTutorialViewController != nil, (fromVC as? UINavigationController)?.viewControllers.first as? AnonymousTutorialViewController != nil {

            let fromVCIndex = tabBarController.viewControllers?.index(of: fromVC) ?? 0
            let toVCIndex = tabBarController.viewControllers?.index(of: toVC) ?? 1

            if fromVCIndex > toVCIndex {
                return TabBarAnimatedTransitioning(movingDirection: .leftToRight)
            } else {
                return TabBarAnimatedTransitioning(movingDirection: .rightToLeft)
            }

        } else {
            return nil
        }
    }

    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {

        omniture(forTabIndex: tabBarController.selectedIndex)

        selectedNavigationController = viewController as? UINavigationController
    }
}
