//
//  CommonCellsGlobal.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 11/12/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import WebKit

class CommonCellsGlobal: NSObject {
    
    static let id: String = String(describing: CommonCellsGlobal.self)
    
    fileprivate var webViewForPDF: WKWebView!
    fileprivate let paperA4Rect = CGRect(x: 0, y: 0, width: 595.2, height: 841.8)
    
    func openWeb(web: String) {
        if let url = URL(string: web) {
            URLOpener().openURL(url)
        }
    }
    
    func sharePdf() {
        
        guard let cellsWebView = getCellsWebView(), let webView = cellsWebView.webView else {
            return
        }
        
        BusManager.sessionDataInstance.showLoading(completion: nil)
        
        getHtml(forWebView: webView) { html in
            
            guard let html = html else {
                return
            }
            
            let webConfiguration = WKWebViewConfiguration()
            self.webViewForPDF = WKWebView(frame: CGRect(origin: .zero, size: self.webContentSize()), configuration: webConfiguration)
            self.webViewForPDF.navigationDelegate = self
            
            self.webViewForPDF.loadHTMLString(html, baseURL: nil)
        }
    }
}

private extension CommonCellsGlobal {
    
    func webContentSize() -> CGSize {
        
        let screenSize = UIScreen.main.bounds.size
        let verticalPadding: CGFloat = 80.0
        
        return CGSize(width: screenSize.width, height: max(screenSize.height, paperA4Rect.size.height - verticalPadding))
    }
    
    func getCellsWebView() -> CellsWebView? {
        
        if let topNavigation = UIApplication.shared.topNavigationController, let lastViewController = topNavigation.viewControllers.last {
            
            return searchCellsWebView(onViewController: lastViewController)
            
        } else if let topViewController = UIApplication.shared.topViewController {
            
            return searchCellsWebView(onViewController: topViewController)
        }
        
        return nil
    }
    
    func searchCellsWebView(onViewController: UIViewController) -> CellsWebView? {
        
        for subview in onViewController.view.subviews where subview is CellsWebView {
            
            return subview as? CellsWebView
        }
        
        return nil
    }
    
    func generatePdf(using printFormatter: UIPrintFormatter) -> Data {
        
        let renderer = UIPrintPageRenderer()
        renderer.addPrintFormatter(printFormatter, startingAtPageAt: 0)
        
        let printableRect = CGRect(x: (paperA4Rect.size.width - webContentSize().width) / 2, y: 0, width: webContentSize().width, height: webContentSize().height)
        
        renderer.setValue(paperA4Rect, forKey: "paperRect")
        renderer.setValue(printableRect, forKey: "printableRect")
        
        let pdfData = NSMutableData()
        UIGraphicsBeginPDFContextToData(pdfData, .zero, nil)
        
        for i in 0..<renderer.numberOfPages {
            
            UIGraphicsBeginPDFPage()
            renderer.drawPage(at: i, in: UIGraphicsGetPDFContextBounds())
        }
        
        UIGraphicsEndPDFContext()
        
        return pdfData as Data
    }
    
    func createTmpFile(data: Data, fileName: String, ext: String) throws -> URL {
        
        let url = URL(fileURLWithPath: NSTemporaryDirectory())
            .appendingPathComponent(fileName)
            .appendingPathExtension(ext)
        
        try data.write(to: url)
        
        return url
    }
    
    func getHtml(forWebView webView: WKWebView, completion: @escaping (_ html: String?) -> Void) {
        
        webView.evaluateJavaScript("document.documentElement.outerHTML.toString()", completionHandler: { html, _ in
            
            completion(html as? String)
        })
    }
    
    func presentSharePDF() {
        
        let viewPrintFormatter = webViewForPDF.viewPrintFormatter()
        
        let pdfData = generatePdf(using: viewPrintFormatter)
        
        let dateNow = Date.string(fromDate: Date(), withFormat: "YYYYMMDDHHmm")
        
        let fileName = String("\(Localizables.others.key_share_file_name_text)-\(dateNow)")
        
        let urlFile = try? createTmpFile(data: pdfData, fileName: fileName, ext: "pdf")
        
        guard let fileURL = urlFile else {
            return
        }
        
        BusManager.sessionDataInstance.hideLoading {
            
            let viewShareController = ShareContentManager.sharedInstance().shareFile(fileURL: fileURL)
            BusManager.sessionDataInstance.present(viewController: viewShareController, animated: true, completion: nil)
        }
    }
}

// MARK: - WKNavigationDelegate
extension CommonCellsGlobal: WKNavigationDelegate {
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        
        presentSharePDF()
    }
    
    func webView(_ webView: WKWebView,
                 didFailProvisionalNavigation navigation: WKNavigation!,
                 withError error: Error) {
        
        BusManager.sessionDataInstance.hideLoading(completion: nil)
    }
}
