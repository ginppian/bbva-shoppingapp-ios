//
//  ServerHostURLProtocolGlobal.swift
//  shoppingapp
//
//  Created by Arturo Marzo de la Fuente on 19/7/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol ServerHostURLProtocolGlobal {

    static func defaultDirectory() -> String
    static func defaultDirectoryFinancial() -> String
    static func getDevicesURL() -> String
    static func getGrantingTicketURL() -> String
    static func getCustomerURL() -> String
    static func getCardsURL() -> String
    static func getPromotionsURL() -> String
    static func getContractsURL() -> String
    static func getLoyaltyURL() -> String
    static func getAppSettingsURL() -> String
}
