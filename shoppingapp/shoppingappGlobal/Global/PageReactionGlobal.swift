//
//  PageReactionGlobal.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 25/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

import CellsNativeComponents
import CellsNativeCore

class PageReactionGlobal: PageReactionManager {

    override init() {
        super.init()
        initPagesReaction()
    }

    func initPagesReaction() {

        initGlobalReaction()
        initPageSplash()
        initPageWelcome()
        initPageLogin()
        initPageLoginLoader()
        initPageHome()
        initPageCards()
        initPageCardActivation()
        initPageCardGeneration()
        initPageCardCancelation()
        initPageCardBlock()
        initPageTransactionList()
        initPageTransactionsFilter()
        initPageTransactionDetail()
        initPageShopping()
        initPageShoppingContact()
        initPageShoppingDetail()
        initPageShoppingFilter()
        initPageShoppingGuarantee()
        initPageShoppingList()
        initPageNotifications()
        initPageSecurity()
        initPageLimits()
        initPageTicketDetail()
        initPageEditLimits()
        initPageShoppingListFiltered()
        initPagetShoppingMapList()
        initPageHelp()
        initPageLeftMenu()
        initSideMenuContainerSettings()
        initSideMenuContainerAbout()
        initPageNotRecognizedOperation()
        initPageHelpDetail()
        initPageActivateDevicePermissions()
        initPageUpgradeNotification()
    }

    func initGlobalReaction() {
        addGlobal(id: ShoppingCrossReaction.TAG) { () -> GlobalReaction in
            ShoppingCrossReaction()
        }
    }

    // MARK: - Init/Login -

    func initPageSplash() {
        add(id: SplashViewController.ID) { () -> PageReaction in
            SplashPageReaction()
        }
    }

    func initPageWelcome() {
        add(id: WelcomeViewController.ID) { () -> PageReaction in
            WelcomePageReaction()
        }
    }

    func initPageLogin() {
        add(id: LoginViewController.ID) { () -> PageReaction in
            LoginPageReaction()
        }
    }

    func initPageLoginLoader() {
        add(id: LoginLoaderViewController.ID) { () -> PageReaction in
            LoginLoaderPageReaction()
        }
    }

    // MARK: - Home -

    func initPageHome() {
        add(id: HomeViewController.ID) { () -> PageReaction in
            HomePageReaction()
        }
    }

    // MARK: - Cards -

    func initPageCards() {
        add(id: CardsViewController.ID) { () -> PageReaction in
            CardsPageReaction()
        }
    }

    func initPageCardActivation() {
        add(id: CardActivationViewController.ID) { () -> PageReaction in
            CardActivationPageReaction()
        }
    }

    func initPageCardGeneration() {
        add(id: CardGenerationViewController.ID) { () -> PageReaction in
            CardGenerationPageReaction()
        }
    }

    func initPageCardCancelation() {
        add(id: CardCancelationViewController.ID) { () -> PageReaction in
            CardCancelationPageReaction()
        }
    }

    func initPageCardBlock() {
        add(id: CardBlockViewController.ID) { () -> PageReaction in
            CardBlockPageReaction()
        }
    }

    func initPageNotRecognizedOperation() {
        add(id: NotRecognizedOperationViewController.ID) { () -> PageReaction in
            NotRecognizedOperationPageReaction()
        }
    }

    func initPageActivateDevicePermissions() {
        add(id: ActivateDevicePermissionsViewController.ID) { () -> PageReaction in
            ActivateDevicePermissionsPageReaction()
        }
    }

    func initPageTransactionList() {
        add(id: TransactionListViewController.ID) { () -> PageReaction in
            TransactionListPageReaction()
        }
    }

    func initPageTransactionsFilter() {
        add(id: TransactionsFilterViewController.ID) { () -> PageReaction in
            TransactionsFilterPageReaction()
        }
    }

    func initPageTransactionDetail() {
        add(id: TransactionDetailViewController.ID) { () -> PageReaction in
            TransactionDetailPageReaction()
        }
    }

    func initPageLimits() {
        add(id: LimitsViewController.ID) { () -> PageReaction in
            LimitsPageReaction()
        }
    }

    func initPageEditLimits() {
        add(id: EditLimitViewController.ID) { () -> PageReaction in
            EditLimitPageReaction()
        }
    }

    // MARK: - Promotions -

    func initPageShopping() {
        add(id: ShoppingViewController.ID) { () -> PageReaction in
            ShoppingPageReaction()
        }
    }

    func initPageShoppingContact() {
        add(id: ShoppingContactViewController.ID) { () -> PageReaction in
            ShoppingContactPageReaction()
        }
    }
    
    func initPageShoppingDetail() {
        add(id: ShoppingDetailViewController.ID) { () -> PageReaction in
            ShoppingDetailPageReaction()
        }
    }

    func initPageShoppingFilter() {
        add(id: ShoppingFilterViewController.ID) { () -> PageReaction in
            ShoppingFilterPageReaction()
        }
    }
    
    func initPageShoppingGuarantee() {
        add(id: ShoppingGuaranteeViewController.ID) { () -> PageReaction in
            ShoppingGuaranteePageReaction()
        }
    }
    
    func initPageShoppingList() {
        add(id: ShoppingListViewController.ID) { () -> PageReaction in
            ShoppingListPageReaction()
        }
    }

    func initPageShoppingListFiltered() {
        add(id: ShoppingListFilteredViewController.ID) { () -> PageReaction in
            ShoppingListFilteredPageReaction()
        }
    }

    func initPagetShoppingMapList() {
        add(id: ShoppingMapListViewController.ID) { () -> PageReaction in
            ShoppingMapListPageReaction()
        }
    }

    // MARK: - Notifications -

    func initPageNotifications() {
        add(id: NotificationsViewController.ID) { () -> PageReaction in
            NotificationsPageReaction()
        }
    }

    func initPageTicketDetail() {
        add(id: TicketDetailViewController.ID) { () -> PageReaction in
            TicketDetailPageReaction()
        }
    }
    
    func initPageUpgradeNotification() {
        add(id: UpgradeNotificationViewController.ID) { () -> PageReaction in
            UpgradeNotificationPageReaction()
        }
    }

    // MARK: - Others -

    func initPageSecurity() {
        add(id: SecurityViewController.ID) { () -> PageReaction in
            SecurityPageReaction()
        }
    }

    func initPageHelp() {
        add(id: HelpViewController.ID) { () -> PageReaction in
            HelpPageReaction()
        }
    }

    func initPageLeftMenu() {
        add(id: LeftMenuViewController.ID) { () -> PageReaction in
            LeftMenuPageReaction()
        }
    }

    func initSideMenuContainerSettings() {
        add(id: ConfigurationWalletCellViewController.ID) { () -> PageReaction in
            ConfigurationWalletCellPageReaction()
        }
    }

    func initSideMenuContainerAbout() {
        add(id: AboutViewController.ID) { () -> PageReaction in
            AboutPageReaction()
        }
    }
    
    func initPageHelpDetail() {
        add(id: HelpDetailViewController.ID) { () -> PageReaction in
            HelpDetailPageReaction()
        }
    }
}
