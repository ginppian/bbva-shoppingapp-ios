//
//  ErrorTrackData.swift
//  shoppingapp
//
//  Created by jesus.martinez on 28/12/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class ErrorTrackData {
    
    fileprivate var errors = [String]()
    
    func append(errorCode: Int?, errorName: String) {
        errors.append("\(errorCode ?? ConstantsHTTPCodes.GENERIC_ERROR)|\(errorName)")
    }
    
    func isEmpty() -> Bool {
        return errors.isEmpty
    }
    
    func reset() {
        errors.removeAll()
    }
    
    func format() -> String {
        return errors.joined(separator: ";")
    }
}

struct Promotions {
    
    static var promotionByCategory: String {
        return "categoria"
    }
    
    static var trending: String {
        return "destacadas"
    }
    
    static var categories: String {
        return "categorias"
    }
}
