//
//  OmnitureTrackData.h
//  shoppingapp
//
//  Created by jesus.martinez on 17/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString * const kPageSegment;
extern NSString * const kApp;
extern NSString * const kLogged;
extern NSString * const kNoLogged;
extern NSString * const kAppEnvironment;
extern NSString * const kVisitsNumber;
extern NSString * const kSiteAppName;
extern NSString * const kPublicArea;
extern NSString * const kPrivateArea;
extern NSString * const kHomeType;
extern NSString * const kInformationType;
extern NSString * const kPageSegmentGeneral;
extern NSString * const kPageSegmentPeople;



@interface OmnitureTrackData : NSObject

@property (nonatomic) BOOL isLogged;
@property (nonatomic, copy) NSString *pageName;
@property (nonatomic, copy) NSString *area;
@property (nonatomic, copy) NSString *georegion;
@property (nonatomic, copy) NSString *pageType;
@property (nonatomic, copy) NSString *userProfile;
@property (nonatomic, copy) NSString *customerID;
@property (nonatomic, copy) NSString *errorPage;
@property (nonatomic, copy) NSString *customLink;
@property (nonatomic, copy) NSString *businessUnit;
@property (nonatomic, copy) NSString *language;
@property (nonatomic, copy) NSString *appVersion;
@property (nonatomic, copy) NSString *environment;
@property (nonatomic, copy) NSString *pageSegment;
@property (nonatomic, copy) NSString *error;


- (id) initWithPageName:(NSString *) pageName;

- (NSString *) logged;


@end
