//
//  TrackerHelper.m
//  shoppingapp
//
//  Created by jesus.martinez on 18/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

#import "TrackerHelper.h"
#import "TrackingHelper.h"
#import "OmnitureTrackData.h"
#import "ADBMobile.h"
#import "shoppingapp-Swift.h"

#import <CoreLocation/CoreLocation.h>

BOOL * const DEBUG_LOGGING = false;


@interface TrackerHelper() <CLLocationManagerDelegate>

@property (nonatomic, strong) TrackingHelper *trackingHelper;
@property (nonatomic, copy) NSString *georegion;
@property (nonatomic, copy) NSString *businessUnit;
@property (nonatomic, copy) NSString *language;
@property (nonatomic, copy) NSString *appVersion;
@property (nonatomic, strong) CLLocationManager *locationManager;
@property (nonatomic, copy) NSString *environment;
@property (nonatomic, copy) NSString *userProfile;


@end


@implementation TrackerHelper

NSString * const TRACKER_HELPER_PAGE_NAME_GENERATE_CARD = @"tarjetas:generacion tarjeta digital:1 informacion";
NSString * const TRACKER_HELPER_PAGE_NAME_GENERATE_CARD_TERMS = @"tarjetas:generacion tarjeta digital:2 terminos y condiciones";
NSString * const TRACKER_HELPER_PAGE_NAME_GENERATE_CARD_SUCCESS = @"tarjetas:generacion tarjeta digital:pagina exitosa";

NSString * const TRACKER_HELPER_PAGE_NAME_CARD_BLOCK_INFO = @"tarjetas:bloqueo tarjeta:1 informacion";
NSString * const TRACKER_HELPER_PAGE_NAME_CARD_BLOCK_TYPE = @"tarjetas:bloqueo tarjeta:2 motivo";
NSString * const TRACKER_HELPER_PAGE_NAME_CARD_BLOCK_SUCCESS = @"tarjetas:bloqueo tarjeta:pagina exitosa";

NSString * const TRACKER_HELPER_PAGE_NAME_CARD_CANCELATION_INFO = @"tarjetas:cancelar tarjeta:1 informacion";
NSString * const TRACKER_HELPER_PAGE_NAME_CARD_CANCELATION_SUCCESS = @"tarjetas:cancelar tarjeta:pagina exitosa";

NSString * const TRACKER_HELPER_PAGE_NAME_NOT_RECOGNIZED_OPERATION = @"tarjetas:operacion no reconocida";

NSString * const TRACKER_HELPER_HOME_BANNER_POSITION = @"superior centro";
NSString * const TRACKER_HELPER_HOME_BANNER_CAMPAIGN_FORMAT = @"banner home";

+ (TrackerHelper *)sharedInstance {
    
    static TrackerHelper *sharedInstance = nil;
    static dispatch_once_t onceToken;
    
    dispatch_once(&onceToken, ^{
        sharedInstance = [[self alloc] initPrivate];
    });
    
    return sharedInstance;
}

- (id)initPrivate {
    
    if (self = [super init]) {
        
        self.trackingHelper = [[TrackingHelper alloc] init];
        self.businessUnit = [self loadbusinessUnit];
        self.language = [self loadLanguage];
        self.appVersion = [self loadAppVersion];
        self.environment = [self loadEnvironment];
        self.userProfile = [self loadUserProfile];

        self.locationManager = [[CLLocationManager alloc] init];
        
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse) {
            
            [self startFindCountry];
        }
        
    }
    
    return self;
}

+ (NSDictionary *)configFile {
    
    NSString *pathConfigFile = [NSBundle.mainBundle pathForResource:@"Configuration" ofType:@"plist"];
    
    return [NSDictionary dictionaryWithContentsOfFile:pathConfigFile];
    
}

- (NSString *)loadbusinessUnit {
    
    NSDictionary *configFile = [TrackerHelper configFile];
    
    NSDictionary *omnitureConfiguration = [configFile objectForKey:@"OMNITURE"];
    
    return [omnitureConfiguration objectForKey:@"BUSINESS_UNIT"];
}

- (NSString *)loadUserProfile {
    
    NSDictionary *configFile = [TrackerHelper configFile];
    
    NSDictionary *omnitureConfiguration = [configFile objectForKey:@"OMNITURE"];
    
    return [omnitureConfiguration objectForKey:@"USER_PROFILE"];
}

- (NSString *)loadLanguage {

    return [[[[NSBundle mainBundle] preferredLocalizations] objectAtIndex:0] uppercaseString];

}

- (NSString *)loadAppVersion {
    
    return [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    
}

- (NSString *)loadEnvironment {
    
    NSDictionary *configFile = [TrackerHelper configFile];
    
    NSDictionary *configServer = configFile[@"SERVER_CONFIG"];
    
    NSString *environment = [configServer objectForKey:@"SelectedEnvironment"];
    
    return [NSString stringWithFormat:kAppEnvironment, [environment isEqualToString:@"production"] ? @"pro" : environment];
    
}

- (void)startFindCountry {
    
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers;
    [self.locationManager startUpdatingLocation];
    
}


- (void)stopFindCountry {
    
    [self.locationManager stopUpdatingLocation];
    self.locationManager.delegate = nil;
    self.locationManager = nil;
    
}

- (void)configure {

    [ADBMobile setDebugLogging: DEBUG_LOGGING];
    [ADBMobile collectLifecycleData];
    
}

#pragma mark - Help Methods

- (NSString *)lowerCamelCaseForString:(NSString *) string {
    
    if (string == nil || string.length == 0) {
        return @"";
    }
    
    NSArray<NSString *> *words = [string componentsSeparatedByString: @" "];
    
    NSMutableString *stringCamelCased = [[NSMutableString alloc] initWithString:words[0].lowercaseString];
    
    for (int i=1; i<words.count; i++) {
        [stringCamelCased appendString:words[i].capitalizedString];
    }
    
    return [stringCamelCased copy];
}

#pragma mark - CLLocationManagerDelegate

- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray<CLLocation *> *)locations
{
    __weak typeof(self) weakSelf = self;
    
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:locations.firstObject
                   completionHandler:^(NSArray<CLPlacemark *> * _Nullable placemarks, NSError * _Nullable error) {
                
                       if ((error == nil) && ([placemarks lastObject] != nil)) {
                           
                           CLPlacemark *placeMark = [placemarks lastObject];
                           
                           weakSelf.georegion = placeMark.ISOcountryCode;
                           
                           [weakSelf stopFindCountry];
                           
                       }
                       
    }];

}

#pragma mark - Default Config

- (OmnitureTrackData *)setDefaultConfigToTrackData:(OmnitureTrackData *)trackData {
    
    trackData.georegion = self.georegion;
    trackData.businessUnit = self.businessUnit;
    trackData.language = self.language;
    trackData.appVersion = self.appVersion;
    trackData.environment = self.environment;
    
    return trackData;
}

#pragma mark - Tracking Screens -

#pragma mark Login

- (void)trackLoginScreen:(BOOL)recognizedUser {
    
    OmnitureTrackData *trackData;

    if(recognizedUser) {
        trackData = [[OmnitureTrackData alloc] initWithPageName: @"login reconocido"];
    } else {
        trackData = [[OmnitureTrackData alloc] initWithPageName: @"login"];
    }
    trackData = [self setDefaultConfigToTrackData:trackData];
    
    trackData.area = kPublicArea;
    if(recognizedUser) {
        trackData.pageType = kInformationType;
    } else {
        trackData.pageType = kHomeType;
    }
    trackData.pageSegment = kPageSegmentGeneral;
    trackData.isLogged = [[SessionDataManager sessionDataInstance] isUserLogged];

    [self.trackingHelper trackScreen:trackData];
    
}

- (void)trackLoginSuccessEvent:(BOOL)recognizedUser {
    
    OmnitureTrackData *trackData = [[OmnitureTrackData alloc] initWithPageName: [NSString stringWithFormat:@"app:publica:general:%@", [self.trackingHelper getPrevPage]]];
    trackData = [self setDefaultConfigToTrackData:trackData];
    
    trackData.area = kPublicArea;
    if(recognizedUser) {
        trackData.pageType = kInformationType;
    } else {
        trackData.pageType = kHomeType;
    }
    trackData.pageSegment = kPageSegmentGeneral;
    trackData.customLink = @"login";


    self.trackingHelper.datos_envio[@"evento_login"] = @"event12";
    
    [self.trackingHelper trackEvent:trackData];
    
}

- (void)trackLogoutSuccessEvent:(NSString*)pageName {
    
    OmnitureTrackData *trackData = [[OmnitureTrackData alloc] initWithPageName: pageName];
    trackData = [self setDefaultConfigToTrackData:trackData];
    
    trackData.area = kPrivateArea;
    trackData.pageType = kInformationType;
    trackData.pageSegment = kPageSegmentPeople;
    trackData.customLink = @"logout";
    trackData.isLogged = [[SessionDataManager sessionDataInstance] isUserLogged];

    self.trackingHelper.datos_envio[@"evento_logout"] = @"event8";
    
    [self.trackingHelper trackEvent:trackData];
    
}

- (void)trackShowHomeBannerWithCustomerID:(NSString*)customerID campaignName:(NSString *)campaignName {
    
    OmnitureTrackData *trackData = [[OmnitureTrackData alloc] initWithPageName: @"inicio"];
    trackData = [self setDefaultConfigToTrackData:trackData];
    
    trackData.area = kPrivateArea;
    trackData.pageType = kInformationType;
    trackData.pageSegment = kPageSegmentPeople;
    
    trackData.isLogged = [[SessionDataManager sessionDataInstance] isUserLogged];
    trackData.userProfile = self.userProfile;
    trackData.customerID = customerID;

    NSString *location= TRACKER_HELPER_HOME_BANNER_POSITION;
    NSString *campaignFormat= TRACKER_HELPER_HOME_BANNER_CAMPAIGN_FORMAT;
    NSString *collectiveCode= @"";
    NSString *product= @"";
    NSString *productCode= @"";
    NSString *quantity= @"";
    
    NSString *list2 = [NSString stringWithFormat:@"%@:%@:%@:%@:%@:%@:%@", location, campaignFormat, collectiveCode, campaignName, product, productCode, quantity];

    self.trackingHelper.datos_envio[@"evento_internalCampaignImpressions_1"] = @"event34";
    self.trackingHelper.datos_envio[@"evento_internalCampaignImpressions_2"] = @"event35";
    self.trackingHelper.datos_envio[@"list2"] = list2;
    
    [self.trackingHelper trackScreen:trackData];
}

- (void)trackHomeBannerClickWithCustomerID:(NSString*)customerID campaignName:(NSString *)campaignName {
    
    OmnitureTrackData *trackData = [[OmnitureTrackData alloc] initWithPageName: @"app:privada:personas:inicio"];
    trackData = [self setDefaultConfigToTrackData:trackData];
    
    trackData.area = kPrivateArea;
    trackData.pageType = kInformationType;
    trackData.pageSegment = kPageSegmentPeople;
    
    trackData.isLogged = [[SessionDataManager sessionDataInstance] isUserLogged];
    trackData.userProfile = self.userProfile;
    trackData.customerID = customerID;

    NSString *location= TRACKER_HELPER_HOME_BANNER_POSITION;
    NSString *campaignFormat= TRACKER_HELPER_HOME_BANNER_CAMPAIGN_FORMAT;
    NSString *collectiveCode= @"";
    NSString *product= @"";
    NSString *productCode= @"";
    NSString *quantity= @"";
    
    NSString *list2 = [NSString stringWithFormat:@"%@:%@:%@:%@:%@:%@:%@", location, campaignFormat, collectiveCode, campaignName, product, productCode, quantity];

    trackData.customLink = list2;
    self.trackingHelper.datos_envio[@"evento_internalCampaignClickthrough"] = @"event36";
    self.trackingHelper.datos_envio[@"eVar35"] = list2;
    self.trackingHelper.datos_envio[@"list2"] = list2;


    [self.trackingHelper trackEvent:trackData];
}

#pragma mark Cards

- (void)trackCardsScreenWithCustomerID:(NSString *)customerID {
    
    OmnitureTrackData *trackData = [[OmnitureTrackData alloc] initWithPageName: @"tarjetas"];
    trackData = [self setDefaultConfigToTrackData:trackData];
    
    trackData.area = kPrivateArea;
    trackData.pageType = kInformationType;
    trackData.pageSegment = kPageSegmentPeople;

    trackData.isLogged = [[SessionDataManager sessionDataInstance] isUserLogged];
    trackData.userProfile = self.userProfile;
    trackData.customerID = customerID;

    [self.trackingHelper trackScreen:trackData];
    
}

- (void)trackTransactionsScreenWithCustomerID:(NSString *)customerID {
    
    OmnitureTrackData *trackData = [[OmnitureTrackData alloc] initWithPageName: @"tarjetas:movimientos"];
    trackData = [self setDefaultConfigToTrackData:trackData];
    
    trackData.area = kPrivateArea;
    trackData.pageType = kInformationType;
    trackData.pageSegment = kPageSegmentPeople;

    trackData.isLogged = [[SessionDataManager sessionDataInstance] isUserLogged];
    trackData.userProfile = self.userProfile;
    trackData.customerID = customerID;
    
    [self.trackingHelper trackScreen:trackData];
    
}

- (void)trackTransactionsFilterScreenWithCustomerID:(NSString *)customerID {
    
    OmnitureTrackData *trackData = [[OmnitureTrackData alloc] initWithPageName: @"tarjetas:movimientos:filtros"];
    trackData = [self setDefaultConfigToTrackData:trackData];
    
    trackData.area = kPrivateArea;
    trackData.pageType = kInformationType;
    trackData.pageSegment = kPageSegmentPeople;

    trackData.isLogged = [[SessionDataManager sessionDataInstance] isUserLogged];
    trackData.userProfile = self.userProfile;
    trackData.customerID = customerID;
    
    [self.trackingHelper trackScreen:trackData];
    
}

- (void)trackCardsScreenForTitleId:(NSString *)titleId andCustomerID:(NSString *)customerID {
    
    NSString *cardTitleIdLowercase = [titleId lowercaseString];
    cardTitleIdLowercase = [cardTitleIdLowercase stringByReplacingOccurrencesOfString:@"_" withString:@" "];
    
    NSString *pageName = [NSString stringWithFormat:@"tarjetas:%@", cardTitleIdLowercase];
    
    OmnitureTrackData *trackData = [[OmnitureTrackData alloc] initWithPageName: pageName];
    trackData = [self setDefaultConfigToTrackData:trackData];
    
    trackData.area = kPrivateArea;
    trackData.pageType = kInformationType;
    trackData.pageSegment = kPageSegmentPeople;

    trackData.isLogged = [[SessionDataManager sessionDataInstance] isUserLogged];
    trackData.userProfile = self.userProfile;
    trackData.customerID = customerID;
    
    [self.trackingHelper trackScreen:trackData];
    
}

- (void)trackTransactionsDetailScreenWithCustomerID:(NSString *)customerID {
    
    OmnitureTrackData *trackData = [[OmnitureTrackData alloc] initWithPageName: @"tarjetas:movimientos:detalle"];
    trackData = [self setDefaultConfigToTrackData:trackData];
    
    trackData.area = kPrivateArea;
    trackData.pageType = kInformationType;
    trackData.pageSegment = kPageSegmentPeople;
    
    trackData.isLogged = [[SessionDataManager sessionDataInstance] isUserLogged];
    trackData.userProfile = self.userProfile;
    trackData.customerID = customerID;
    
    [self.trackingHelper trackScreen:trackData];
    
}

- (void)trackCardOffSuccessWithCustomerID:(NSString *)customerID {
    
    OmnitureTrackData *trackData = [[OmnitureTrackData alloc] initWithPageName: @"tarjetas:apagar tarjeta"];
    trackData = [self setDefaultConfigToTrackData:trackData];
    
    trackData.area = kPrivateArea;
    trackData.pageType = kInformationType;
    trackData.pageSegment = kPageSegmentPeople;
    
    trackData.isLogged = [[SessionDataManager sessionDataInstance] isUserLogged];
    trackData.userProfile = self.userProfile;
    trackData.customerID = customerID;
    
    [self.trackingHelper trackScreen:trackData];
    
}

- (void)trackCardCVVWithCustomerID:(NSString *)customerID {

    OmnitureTrackData *trackData = [[OmnitureTrackData alloc] initWithPageName: @"tarjetas:cvv"];
    trackData = [self setDefaultConfigToTrackData:trackData];

    trackData.area = kPrivateArea;
    trackData.pageType = kInformationType;
    trackData.pageSegment = kPageSegmentPeople;

    trackData.isLogged = [[SessionDataManager sessionDataInstance] isUserLogged];
    trackData.userProfile = self.userProfile;
    trackData.customerID = customerID;

    [self.trackingHelper trackScreen:trackData];

}
    
#pragma mark Promotions

- (void)trackPromotionsScreenWithCustomerID:(NSString *)customerID {
        
    OmnitureTrackData *trackData = [[OmnitureTrackData alloc] initWithPageName: @"promociones"];
    trackData = [self setDefaultConfigToTrackData:trackData];
    
    trackData.area = kPrivateArea;
    trackData.pageType = kInformationType;
    trackData.pageSegment = kPageSegmentPeople;

    trackData.isLogged = [[SessionDataManager sessionDataInstance] isUserLogged];
    trackData.userProfile = self.userProfile;
    trackData.customerID = customerID;
    
    [self.trackingHelper trackScreen:trackData];
    
}

- (void)trackPromotionsScreenError:(NSString *)error andWithCustomerID:(NSString *)customerID {
    
    NSString *pageName = [NSString stringWithFormat:@"promociones:%@", error];
    OmnitureTrackData *trackData = [[OmnitureTrackData alloc] initWithPageName: pageName];
    trackData = [self setDefaultConfigToTrackData:trackData];
    
    trackData.area = kPrivateArea;
    trackData.pageType = kInformationType;
    trackData.pageSegment = kPageSegmentPeople;
    
    trackData.isLogged = [[SessionDataManager sessionDataInstance] isUserLogged];
    trackData.userProfile = self.userProfile;
    trackData.customerID = customerID;
    
    trackData.error = error;
    
    [self.trackingHelper trackErrorScreen:trackData];
    
}

- (void)trackPromotionsScreenForCategoryName:(NSString *)categoryName andCustomerID:(NSString *)customerID {
        
    NSString *pageName = [NSString stringWithFormat:@"promociones:%@", categoryName];
    
    OmnitureTrackData *trackData = [[OmnitureTrackData alloc] initWithPageName: pageName];
    trackData = [self setDefaultConfigToTrackData:trackData];
    
    trackData.area = kPrivateArea;
    trackData.pageType = kInformationType;
    trackData.pageSegment = kPageSegmentPeople;

    trackData.isLogged = [[SessionDataManager sessionDataInstance] isUserLogged];
    trackData.userProfile = self.userProfile;
    trackData.customerID = customerID;
    
    [self.trackingHelper trackScreen:trackData];
    
}

- (void)trackPromotionsMapScreenWithCustomerID:(NSString *)customerID {
    
    NSString *pageName = [NSString stringWithFormat:@"promociones:cerca de ti"];
    
    OmnitureTrackData *trackData = [[OmnitureTrackData alloc] initWithPageName: pageName];
    trackData = [self setDefaultConfigToTrackData:trackData];
    
    trackData.area = kPrivateArea;
    trackData.pageType = kInformationType;
    trackData.pageSegment = kPageSegmentPeople;
    
    trackData.isLogged = [[SessionDataManager sessionDataInstance] isUserLogged];
    trackData.userProfile = self.userProfile;
    trackData.customerID = customerID;
    
    [self.trackingHelper trackScreen:trackData];
    
}

- (void)trackPromotionsDetailScreenWithCustomerID:(NSString *)customerID andPromotionID:(NSString *)promotionID {
    
    NSString *pageName = [NSString stringWithFormat:@"promociones:detalle"];
    
    OmnitureTrackData *trackData = [[OmnitureTrackData alloc] initWithPageName: pageName];
    trackData = [self setDefaultConfigToTrackData:trackData];
    
    trackData.area = kPrivateArea;
    trackData.pageType = kInformationType;
    trackData.pageSegment = kPageSegmentPeople;
    
    trackData.isLogged = [[SessionDataManager sessionDataInstance] isUserLogged];
    trackData.userProfile = self.userProfile;
    trackData.customerID = customerID;

    self.trackingHelper.datos_envio[@"prop47"] =  promotionID ? promotionID : @"";
    self.trackingHelper.datos_envio[@"prop49"] = @"descuento";

    [self.trackingHelper trackScreen:trackData];
    
}

- (void)trackPromotionsFilterScreenWithCustomerID:(NSString *)customerID {
    
    NSString *pageName = [NSString stringWithFormat:@"promociones:filtros"];
    
    OmnitureTrackData *trackData = [[OmnitureTrackData alloc] initWithPageName: pageName];
    trackData = [self setDefaultConfigToTrackData:trackData];
    
    trackData.area = kPrivateArea;
    trackData.pageType = kInformationType;
    trackData.pageSegment = kPageSegmentPeople;
    
    trackData.isLogged = [[SessionDataManager sessionDataInstance] isUserLogged];
    trackData.userProfile = self.userProfile;
    trackData.customerID = customerID;
    
    [self.trackingHelper trackScreen:trackData];
    
}

- (void)trackPromotionsTrendingShowAllWithCustomerID:(NSString *)customerID {
    
    NSString *pageName = [NSString stringWithFormat:@"promociones:destacadas"];
    
    OmnitureTrackData *trackData = [[OmnitureTrackData alloc] initWithPageName: pageName];
    trackData = [self setDefaultConfigToTrackData:trackData];
    
    trackData.area = kPrivateArea;
    trackData.pageType = kInformationType;
    trackData.pageSegment = kPageSegmentPeople;
    
    trackData.isLogged = [[SessionDataManager sessionDataInstance] isUserLogged];
    trackData.userProfile = self.userProfile;
    trackData.customerID = customerID;
    
    [self.trackingHelper trackScreen:trackData];
    
}

- (void)trackPromotionsMapSeeAllWithCustomerID:(NSString *)customerID {
    
    NSString *pageName = [NSString stringWithFormat:@"promociones:cerca de ti:listado poi"];
    
    OmnitureTrackData *trackData = [[OmnitureTrackData alloc] initWithPageName: pageName];
    trackData = [self setDefaultConfigToTrackData:trackData];
    
    trackData.area = kPrivateArea;
    trackData.pageType = kInformationType;
    trackData.pageSegment = kPageSegmentPeople;
    
    trackData.isLogged = [[SessionDataManager sessionDataInstance] isUserLogged];
    trackData.userProfile = self.userProfile;
    trackData.customerID = customerID;
    
    [self.trackingHelper trackScreen:trackData];
    
}

- (void)trackPromotionsSeeAllCategoriesWithCustomerID:(NSString *)customerID {
    
    NSString *pageName = [NSString stringWithFormat:@"promociones"];
    
    OmnitureTrackData *trackData = [[OmnitureTrackData alloc] initWithPageName: pageName];
    trackData = [self setDefaultConfigToTrackData:trackData];
    
    trackData.area = kPrivateArea;
    trackData.pageType = kInformationType;
    trackData.pageSegment = kPageSegmentPeople;
    
    trackData.isLogged = [[SessionDataManager sessionDataInstance] isUserLogged];
    trackData.userProfile = self.userProfile;
    trackData.customerID = customerID;

    trackData.customLink = @"interno;ver todas categorias;app:privada:personas:promociones";
    self.trackingHelper.datos_envio[@"evento_linkclick"] = @"event6";
    self.trackingHelper.datos_envio[@"eVar6"] = @"interno;ver todas categorias;app:privada:personas:promociones";

    [self.trackingHelper trackAction:trackData];
    
}

- (void)trackPromotionsFilteredScreenWithCustomerID:(NSString *)customerID andSearchText:(NSString *)searchText promotionType:(NSString *)promotionType online:(NSString *)online totalElement:(NSString *)totalElement noResults:(BOOL)noResults {

    NSString *pageName = [NSString stringWithFormat:@"promociones:filtros:resultados"];

    OmnitureTrackData *trackData = [[OmnitureTrackData alloc] initWithPageName: pageName];
    trackData = [self setDefaultConfigToTrackData:trackData];

    trackData.area = kPrivateArea;
    trackData.pageType = kInformationType;
    trackData.pageSegment = kPageSegmentPeople;

    trackData.isLogged = [[SessionDataManager sessionDataInstance] isUserLogged];
    trackData.userProfile = self.userProfile;
    trackData.customerID = customerID;

    NSString *pageNamePrevious = [NSString stringWithFormat:@"%@:%@:%@:%@", kApp, kPrivateArea, kPageSegmentPeople,[self.trackingHelper getPrevPage]];
    [self.trackingHelper setPrevPage:pageName];
    NSString *noResultsEvent = noResults ? @"event10" : @"event11";
    NSString *noResultsEventName = noResults ? @"evento_internalSearchNule" : @"evento_internalSearch";

    self.trackingHelper.datos_envio[@"evento_internalSearchResultsCount"] = totalElement;
    self.trackingHelper.datos_envio[noResultsEventName] = noResultsEvent;
    self.trackingHelper.datos_envio[@"eVar11"] = searchText;
    self.trackingHelper.datos_envio[@"prop11"] = searchText;
    self.trackingHelper.datos_envio[@"eVar10"] = @"+1";
    self.trackingHelper.datos_envio[@"prop50"] = online;
    self.trackingHelper.datos_envio[@"prop49"] = promotionType;
    self.trackingHelper.datos_envio[@"prop65"] = totalElement;
    self.trackingHelper.datos_envio[@"eVar30"] = pageNamePrevious;
    self.trackingHelper.datos_envio[@"prop66"] = pageNamePrevious;
    
    [self.trackingHelper trackScreen:trackData];
}

- (void)trackPromotionsDetailScreenButtonHowToGetWithCustomerID:(NSString *)customerID {
    
    NSString *pageName = [NSString stringWithFormat:@"promociones:detalle"];
    
    OmnitureTrackData *trackData = [[OmnitureTrackData alloc] initWithPageName: pageName];
    trackData = [self setDefaultConfigToTrackData:trackData];
    
    trackData.area = kPrivateArea;
    trackData.pageType = kInformationType;
    trackData.pageSegment = kPageSegmentPeople;
    
    trackData.isLogged = [[SessionDataManager sessionDataInstance] isUserLogged];
    trackData.userProfile = self.userProfile;
    trackData.customerID = customerID;

    trackData.customLink = @"externo;boton como llegar;app:privada:personas:promociones:detalle";
    self.trackingHelper.datos_envio[@"evento_linkclick"] = @"event6";
    self.trackingHelper.datos_envio[@"eVar6"] = @"externo;boton como llegar;app:privada:personas:promociones:detalle";

    [self.trackingHelper trackAction:trackData];
    
}

- (void)trackPromotionsDetailScreenButtonWebSiteWithCustomerID:(NSString *)customerID {
    
    NSString *pageName = [NSString stringWithFormat:@"promociones:detalle"];
    
    OmnitureTrackData *trackData = [[OmnitureTrackData alloc] initWithPageName: pageName];
    trackData = [self setDefaultConfigToTrackData:trackData];
    
    trackData.area = kPrivateArea;
    trackData.pageType = kInformationType;
    trackData.pageSegment = kPageSegmentPeople;
    
    trackData.isLogged = [[SessionDataManager sessionDataInstance] isUserLogged];
    trackData.userProfile = self.userProfile;
    trackData.customerID = customerID;

    trackData.customLink = @"externo;boton website;app:privada:personas:promociones:detalle";
    self.trackingHelper.datos_envio[@"evento_linkclick"] = @"event6";
    self.trackingHelper.datos_envio[@"eVar6"] = @"externo;boton website;app:privada:personas:promociones:detalle";

    [self.trackingHelper trackAction:trackData];
    
}

- (void)trackPromotionsDetailScreenButtonEmailWithCustomerID:(NSString *)customerID {
    
    NSString *pageName = [NSString stringWithFormat:@"promociones:detalle"];
    
    OmnitureTrackData *trackData = [[OmnitureTrackData alloc] initWithPageName: pageName];
    trackData = [self setDefaultConfigToTrackData:trackData];
    
    trackData.area = kPrivateArea;
    trackData.pageType = kInformationType;
    trackData.pageSegment = kPageSegmentPeople;
    
    trackData.isLogged = [[SessionDataManager sessionDataInstance] isUserLogged];
    trackData.userProfile = self.userProfile;
    trackData.customerID = customerID;

    trackData.customLink = @"externo;boton email;app:privada:personas:promociones:detalle";
    self.trackingHelper.datos_envio[@"evento_linkclick"] = @"event6";
    self.trackingHelper.datos_envio[@"eVar6"] = @"externo;boton email;app:privada:personas:promociones:detalle";
    
    [self.trackingHelper trackAction:trackData];
    
}

- (void)trackPromotionsDetailScreenButtonPhoneWithCustomerID:(NSString *)customerID {
    
    NSString *pageName = [NSString stringWithFormat:@"promociones:detalle"];
    
    OmnitureTrackData *trackData = [[OmnitureTrackData alloc] initWithPageName: pageName];
    trackData = [self setDefaultConfigToTrackData:trackData];
    
    trackData.area = kPrivateArea;
    trackData.pageType = kInformationType;
    trackData.pageSegment = kPageSegmentPeople;
    
    trackData.isLogged = [[SessionDataManager sessionDataInstance] isUserLogged];
    trackData.userProfile = self.userProfile;
    trackData.customerID = customerID;

    trackData.customLink = @"externo;boton celular;app:privada:personas:promociones:detalle";
    self.trackingHelper.datos_envio[@"evento_linkclick"] = @"event6";
    self.trackingHelper.datos_envio[@"eVar6"] = @"externo;boton celular;app:privada:personas:promociones:detalle";

    [self.trackingHelper trackAction:trackData];
    
}

#pragma mark Limits

- (void)trackLimitsScreenWithCustomerID:(NSString *)customerID {
    
    NSString *pageName = [NSString stringWithFormat:@"tarjetas:ajustar limites"];
    
    OmnitureTrackData *trackData = [[OmnitureTrackData alloc] initWithPageName: pageName];
    trackData = [self setDefaultConfigToTrackData:trackData];
    
    trackData.area = kPrivateArea;
    trackData.pageType = kInformationType;
    trackData.pageSegment = kPageSegmentPeople;
    
    trackData.isLogged = [[SessionDataManager sessionDataInstance] isUserLogged];
    trackData.userProfile = self.userProfile;
    trackData.customerID = customerID;
    
    [self.trackingHelper trackScreen:trackData];
    
}

- (void)trackEditLimitScreenForLimitName:(NSString *)limitName andCustomerID:(NSString*)customerID {
    
    NSString *pageName = [NSString stringWithFormat:@"tarjetas:ajustar limites:%@:1 monto", [limitName lowercaseString]];
    
    OmnitureTrackData *trackData = [[OmnitureTrackData alloc] initWithPageName: pageName];
    trackData = [self setDefaultConfigToTrackData:trackData];
    
    trackData.area = kPrivateArea;
    trackData.pageType = kInformationType;
    trackData.pageSegment = kPageSegmentPeople;
    
    trackData.isLogged = [[SessionDataManager sessionDataInstance] isUserLogged];
    trackData.userProfile = self.userProfile;
    trackData.customerID = customerID;
    
    [self.trackingHelper trackScreen:trackData];
    
}

- (void)trackUpdateLimitSucessfulForLimitName:(NSString *)limitName andCustomerID:(NSString*)customerID {
    
    NSString *pageName = [NSString stringWithFormat:@"tarjetas:ajustar limites:%@:pagina exitosa", [limitName lowercaseString]];
    
    OmnitureTrackData *trackData = [[OmnitureTrackData alloc] initWithPageName: pageName];
    trackData = [self setDefaultConfigToTrackData:trackData];
    
    trackData.area = kPrivateArea;
    trackData.pageType = kInformationType;
    trackData.pageSegment = kPageSegmentPeople;
    
    trackData.isLogged = [[SessionDataManager sessionDataInstance] isUserLogged];
    trackData.userProfile = self.userProfile;
    trackData.customerID = customerID;
    
    [self.trackingHelper trackScreen:trackData];
    
}

#pragma mark Rewards

- (void)trackRewardsScreenWithCustomerID:(NSString*)customerID {
    
    NSString *pageName = [NSString stringWithFormat:@"puntos"];

    OmnitureTrackData *trackData = [[OmnitureTrackData alloc] initWithPageName: pageName];
    trackData = [self setDefaultConfigToTrackData:trackData];
    
    trackData.area = kPrivateArea;
    trackData.pageType = kInformationType;
    trackData.pageSegment = kPageSegmentPeople;
    
    trackData.isLogged = [[SessionDataManager sessionDataInstance] isUserLogged];
    trackData.userProfile = self.userProfile;
    trackData.customerID = customerID;
    
    [self.trackingHelper trackScreen:trackData];
    
}

#pragma mark Ticket Push

- (void)trackAcceptedPurchaseTicketScreenWithCustomerID:(NSString*)customerID {
    [self trackTicketScreenWithCustomerID:customerID andPushType:@"compra aprobada"];
}

- (void)trackDeniedPurchaseTicketScreenWithCustomerID:(NSString*)customerID {
    [self trackTicketScreenWithCustomerID:customerID andPushType:@"compra rechazada"];
}

- (void)trackTicketScreenWithCustomerID:(NSString*)customerID andPushType:(NSString*)pushType {
    
    NSString *pageName = [NSString stringWithFormat:@"tarjetas:ticket:%@", pushType];
    
    OmnitureTrackData *trackData = [[OmnitureTrackData alloc] initWithPageName: pageName];
    trackData = [self setDefaultConfigToTrackData:trackData];
    
    trackData.area = kPublicArea;
    trackData.pageType = kInformationType;
    trackData.pageSegment = kPageSegmentGeneral;
    
    trackData.isLogged = [[SessionDataManager sessionDataInstance] isUserLogged];
    trackData.userProfile = self.userProfile;
    trackData.customerID = customerID;
    
    [self.trackingHelper trackScreen:trackData];
}

#pragma mark Cells Page

- (void)trackCellsPageWithCustomerID:(NSString *)customerID pageName:(NSString *)pageName {
    
    OmnitureTrackData *trackData = [[OmnitureTrackData alloc] initWithPageName: pageName];
    trackData = [self setDefaultConfigToTrackData:trackData];
    
    trackData.area = kPrivateArea;
    trackData.pageType = kInformationType;
    trackData.pageSegment = kPageSegmentPeople;
    
    trackData.isLogged = [[SessionDataManager sessionDataInstance] isUserLogged];
    trackData.userProfile = self.userProfile;
    trackData.customerID = customerID;
    
    [self.trackingHelper trackScreen:trackData];
}

- (void)trackCellsPageActivateCardWithCustomerID:(NSString *)customerID pageName:(NSString *)pageName {
    
    OmnitureTrackData *trackData = [[OmnitureTrackData alloc] initWithPageName: pageName];
    trackData = [self setDefaultConfigToTrackData:trackData];
    
    trackData.area = kPrivateArea;
    trackData.pageType = kInformationType;
    trackData.pageSegment = kPageSegmentPeople;
    
    trackData.isLogged = [[SessionDataManager sessionDataInstance] isUserLogged];
    trackData.userProfile = self.userProfile;
    trackData.customerID = customerID;
    
    self.trackingHelper.datos_envio[@"eVar45"] = @"activacion tarjeta:app page visit:1 activar";
    self.trackingHelper.datos_envio[@"prop45"] = @"activacion tarjeta:app page visit:1 activar";
    self.trackingHelper.datos_envio[@"eVar46"] = @"operativa:activacion tarjeta";
    self.trackingHelper.datos_envio[@"prop46"] = @"operativa:activacion tarjeta";
    self.trackingHelper.datos_envio[@"eVar65"] = @"card activation";
    
    [self.trackingHelper trackScreen:trackData];
}

- (void)trackCellsPageSuccessActivationWithCustomerID:(NSString *)customerID pageName:(NSString *)pageName {
    
    OmnitureTrackData *trackData = [[OmnitureTrackData alloc] initWithPageName: pageName];
    trackData = [self setDefaultConfigToTrackData:trackData];
    
    trackData.area = kPrivateArea;
    trackData.pageType = kInformationType;
    trackData.pageSegment = kPageSegmentPeople;
    
    trackData.isLogged = [[SessionDataManager sessionDataInstance] isUserLogged];
    trackData.userProfile = self.userProfile;
    trackData.customerID = customerID;
    
    self.trackingHelper.datos_envio[@"eVar45"] = @"activacion tarjeta:app completed:pagina exitosa";
    self.trackingHelper.datos_envio[@"prop45"] = @"activacion tarjeta:app completed:pagina exitosa";
    self.trackingHelper.datos_envio[@"eVar46"] = @"operativa:activacion tarjeta";
    self.trackingHelper.datos_envio[@"prop46"] = @"operativa:activacion tarjeta";
    self.trackingHelper.datos_envio[@"eVar54"] = @"card activation";
    self.trackingHelper.datos_envio[@"eVar65"] = @"card activation";
    
    [self.trackingHelper trackScreen:trackData];
}


@end
