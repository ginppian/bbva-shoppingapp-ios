//
//  TrackerHelper.h
//  shoppingapp
//
//  Created by jesus.martinez on 18/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface TrackerHelper : NSObject

+ (TrackerHelper *) sharedInstance;

- (void) startFindCountry;
- (void) configure;

#pragma mark Pages name Cells

extern NSString * const TRACKER_HELPER_PAGE_NAME_GENERATE_CARD;
extern NSString * const TRACKER_HELPER_PAGE_NAME_GENERATE_CARD_TERMS;
extern NSString * const TRACKER_HELPER_PAGE_NAME_GENERATE_CARD_SUCCESS;

extern NSString * const TRACKER_HELPER_PAGE_NAME_CARD_BLOCK_INFO;
extern NSString * const TRACKER_HELPER_PAGE_NAME_CARD_BLOCK_TYPE;
extern NSString * const TRACKER_HELPER_PAGE_NAME_CARD_BLOCK_SUCCESS;

extern NSString * const TRACKER_HELPER_PAGE_NAME_CARD_CANCELATION_INFO;
extern NSString * const TRACKER_HELPER_PAGE_NAME_CARD_CANCELATION_SUCCESS;

extern NSString * const TRACKER_HELPER_PAGE_NAME_NOT_RECOGNIZED_OPERATION;

extern NSString * const TRACKER_HELPER_HOME_BANNER_POSITION;
extern NSString * const TRACKER_HELPER_HOME_BANNER_CAMPAIGN_FORMAT;

#pragma mark - Tracking Screens -

#pragma mark Login

- (void)trackLoginScreen:(BOOL)recognizedUser;
- (void)trackLoginSuccessEvent:(BOOL)recognizedUser;
- (void)trackLogoutSuccessEvent:(NSString*)pageName;
- (void)trackShowHomeBannerWithCustomerID:(NSString*)customerID campaignName:(NSString *)campaignName;
- (void)trackHomeBannerClickWithCustomerID:(NSString*)customerID campaignName:(NSString *)campaignName;

#pragma mark Cards

- (void)trackCardsScreenWithCustomerID:(NSString *)customerID;
- (void)trackTransactionsScreenWithCustomerID:(NSString *)customerID;
- (void)trackTransactionsFilterScreenWithCustomerID:(NSString *)customerID;
- (void)trackCardsScreenForTitleId:(NSString *) titleId andCustomerID:(NSString *)customerID;
- (void)trackTransactionsDetailScreenWithCustomerID:(NSString *)customerID;
- (void)trackCardOffSuccessWithCustomerID:(NSString *)customerID;
- (void)trackCardCVVWithCustomerID:(NSString *)customerID;

#pragma mark Promotions

- (void)trackPromotionsScreenWithCustomerID:(NSString *)customerID;
- (void)trackPromotionsScreenError:(NSString *)error andWithCustomerID:(NSString *)customerID;
- (void)trackPromotionsScreenForCategoryName:(NSString *)categoryName andCustomerID:(NSString *)customerID;
- (void)trackPromotionsMapScreenWithCustomerID:(NSString *)customerID;
- (void)trackPromotionsDetailScreenWithCustomerID:(NSString *)customerID andPromotionID:(NSString *)promotionID;
- (void)trackPromotionsFilterScreenWithCustomerID:(NSString *)customerID;
- (void)trackPromotionsTrendingShowAllWithCustomerID:(NSString *)customerID;
- (void)trackPromotionsMapSeeAllWithCustomerID:(NSString *)customerID;
- (void)trackPromotionsSeeAllCategoriesWithCustomerID:(NSString *)customerID;
- (void)trackPromotionsFilteredScreenWithCustomerID:(NSString *)customerID andSearchText:(NSString *)searchText promotionType:(NSString *)promotionType online:(NSString *)online totalElement:(NSString *)totalElement noResults:(BOOL)noResults;
- (void)trackPromotionsDetailScreenButtonHowToGetWithCustomerID:(NSString *)customerID;
- (void)trackPromotionsDetailScreenButtonWebSiteWithCustomerID:(NSString *)customerID;
- (void)trackPromotionsDetailScreenButtonEmailWithCustomerID:(NSString *)customerID;
- (void)trackPromotionsDetailScreenButtonPhoneWithCustomerID:(NSString *)customerID;

#pragma mark Limits

- (void)trackLimitsScreenWithCustomerID:(NSString *)customerID;
- (void)trackEditLimitScreenForLimitName:(NSString *)limitName andCustomerID:(NSString*)customerID;
- (void)trackUpdateLimitSucessfulForLimitName:(NSString *)limitName andCustomerID:(NSString*)customerID;

#pragma mark Rewards

- (void)trackRewardsScreenWithCustomerID:(NSString*)customerID;

#pragma mark Ticket Push

- (void)trackAcceptedPurchaseTicketScreenWithCustomerID:(NSString*)customerID;
- (void)trackDeniedPurchaseTicketScreenWithCustomerID:(NSString*)customerID;

#pragma mark Cells Page

- (void)trackCellsPageWithCustomerID:(NSString *)customerID pageName:(NSString *)pageName;
- (void)trackCellsPageActivateCardWithCustomerID:(NSString *)customerID pageName:(NSString *)pageName;
- (void)trackCellsPageSuccessActivationWithCustomerID:(NSString *)customerID pageName:(NSString *)pageName;
    
@end
