//
//  OmnitureTrackData.m
//  shoppingapp
//
//  Created by jesus.martinez on 17/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

#import "OmnitureTrackData.h"

NSString * const kApp = @"app";
NSString * const kLogged = @"logado";
NSString * const kNoLogged = @"no logado";
NSString * const kAppEnvironment = @"online:app:%@:ios"; //{environment}
NSString * const kVisitsNumber = @"+1";
NSString * const kSiteAppName = @"App BBVA Wallet";
NSString * const kPublicArea = @"publica";
NSString * const kPrivateArea = @"privada";

NSString * const kHomeType = @"home";
NSString * const kInformationType = @"informacion";
NSString * const kPageSegmentGeneral = @"general";
NSString * const kPageSegmentPeople = @"personas";



@implementation OmnitureTrackData

- (id) initWithPageName:(NSString *) pageName {
    
    if (self = [super init]) {
        
        self.pageName = pageName;
        
    }
    
    return self;

}


- (NSString *) logged {
    return self.isLogged ? kLogged : kNoLogged;
}


@end
