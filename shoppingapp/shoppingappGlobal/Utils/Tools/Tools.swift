//
//  Tools.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 30/3/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation
import MessageUI
import CoreLocation
import os

class Tools {
    
    ///Takes an Error, casts it as a service error, returns ServiceError.GenericErrorBO if possible,
    ///If it's not a GenericError, just returns a ServiceEror
    class func evaluateError(with error: Error) -> ServiceError {
        let evaluate: ServiceError = (error as? ServiceError)!
        switch evaluate {
        case .GenericErrorEntity(let errorEntity):
            let errorBO = ErrorBO(error: errorEntity)
            return ServiceError.GenericErrorBO(error: errorBO)
        default:
            return evaluate
        }
    }
    
    class func setAccessibility(view: UIView, identifier: String) {
        
        #if APPIUM
        
        view.isAccessibilityElement = true
        view.accessibilityIdentifier = identifier
        
        #endif
        
    }
    
    class func setFontBold(size: CGFloat) -> UIFont {
        return UIFont(name: "BentonSansBBVA-Bold", size: size)!
    }
    
    class func setFontBook(size: CGFloat) -> UIFont {
        return UIFont(name: "BentonSansBBVA-Book", size: size)!
    }
    
    class func setFontBookItalic(size: CGFloat) -> UIFont {
        return UIFont(name: "BentonSansBBVA-BookItalic", size: size)!
    }
    
    class func setFontLight(size: CGFloat) -> UIFont {
        return UIFont(name: "BentonSansBBVA-Light", size: size)!
    }
    
    class func setFontLightItalic(size: CGFloat) -> UIFont {
        return UIFont(name: "BentonSansBBVA-LightItalic", size: size)!
    }
    
    class func setFontMedium(size: CGFloat) -> UIFont {
        return UIFont(name: "BentonSansBBVA-Medium", size: size)!
    }
    
    class func setFontMediumItalic(size: CGFloat) -> UIFont {
        return UIFont(name: "BentonSansBBVA-MediumItalic", size: size)!
    }
    
    class func isStringNilOrEmpty (withString text: String?) -> Bool {
        if text != nil && !(text?.isEmpty)! {
            return false
        }
        return true
    }
    
    class func sendEmail(withEmail email: String, theSubject subject: String? = nil, andMessage message: String? = nil, mailComposeDelegate: MFMailComposeViewControllerDelegate) -> Bool {
        
        if !MFMailComposeViewController.canSendMail() {
            
            var mailUrlscheme: String = "mailto:\(email)"
            
            if let subject = subject, !subject.isEmpty {
                
                mailUrlscheme += "&" + "subject=\(subject)"
            }
            
            if let message = message, !message.isEmpty {
                mailUrlscheme += "&" + "body=\(message)"
            }
            
            if let mailUrlschemeEncoding = mailUrlscheme.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
                mailUrlscheme = mailUrlschemeEncoding
            }
            
            let urlOpener = URLOpener()
            
            if urlOpener.openURL(mailUrlscheme) {
                return true
            }
            
            return false
        }
        
        let mail = MFMailComposeViewController()
        mail.mailComposeDelegate = mailComposeDelegate
        mail.setToRecipients([email])
        
        if let subject = subject, !subject.isEmpty {
            mail.setSubject(subject)
        }
        
        if let message = message, !message.isEmpty {
            mail.setMessageBody(message, isHTML: true)
        }
        
        UIApplication.shared.topViewController?.present(mail, animated: true, completion: nil)
        
        return true
    }
    
    class func launchGoogleMapInDirectionsModeWalking(withDestinationCoordinate destinationCoordinate: CLLocationCoordinate2D, andOriginCoordinate originCoordinate: CLLocationCoordinate2D? = nil) {
        
        guard CLLocationCoordinate2DIsValid(destinationCoordinate), destinationCoordinate.latitude != 0, destinationCoordinate.longitude != 0 else {
            return
        }
        
        var iosUrlscheme: String = "comgooglemaps://?daddr=\(destinationCoordinate.latitude),\(destinationCoordinate.longitude)"
        var webUrlscheme: String = "https://www.google.com/maps/dir/?api=1&destination=\(destinationCoordinate.latitude),\(destinationCoordinate.longitude)"
        
        if let originCoordinate = originCoordinate, CLLocationCoordinate2DIsValid(originCoordinate), originCoordinate.latitude != 0, originCoordinate.longitude != 0 {
            
            iosUrlscheme += "&" + "saddr=\(originCoordinate.latitude),\(originCoordinate.longitude)"
            webUrlscheme += "&" + "origin=\(originCoordinate.latitude),\(originCoordinate.longitude)"
        }
        
        iosUrlscheme += "&" + "directionsmode=walking"
        webUrlscheme += "&" + "travelmode=walking"
        
        if let iosUrlschemeEncoding = iosUrlscheme.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
            iosUrlscheme = iosUrlschemeEncoding
        }
        
        if let webUrlschemeEncoding = webUrlscheme.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) {
            webUrlscheme = webUrlschemeEncoding
        }
        
        let urlOpener = URLOpener()
        
        if !urlOpener.openURL(iosUrlscheme) {
            _ = urlOpener.openURL(webUrlscheme)
        }
    }
    
    class func localizedStringsForKeyAndStartIndex( key: String, startIndex: Int) -> [String] {
        
        var array = [String]()
        var index = startIndex
        var finished = false
        
        while finished != true {
            
            let keyTextTofind = key + "\(index)"
            let textToAdd = NSLocalizedString(keyTextTofind, comment: "")
            
            if textToAdd != keyTextTofind && !textToAdd.isEmpty {
                
                array.append(textToAdd)
                index += 1
            } else {
                
                finished = true
            }
        }
        
        return array
        
    }
}

extension UIImage {
    static let sharedImageCache = NSCache<NSURL, UIImage>()
    
    static func load(fromUrl url: URL?, completion: @escaping (_ image: UIImage?) -> Void) {
        guard let url = url else {
            completion(nil)
            return
        }
        
        let nsurl = url as NSURL
        
        if let cachedImage = UIImage.sharedImageCache.object(forKey: nsurl) {
            completion(cachedImage)
        } else {
            BusManager.sessionDataInstance.mediaDownloadSession?.dataTask(with: url) {  data, response, error in
                guard
                    let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                    let data = data, error == nil,
                    let image = UIImage(data: data)
                    else {
                        completion(nil)
                        return
                }
                UIImage.sharedImageCache.setObject( image, forKey: nsurl )
                completion(image)
            }.resume()
        }
    }
    
    static func imageFromColor(color: UIColor) -> UIImage {
        let rect = CGRect(x: 0, y: 0, width: 1, height: 1)
        
        // create a 1 by 1 pixel context
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0)
        color.setFill()
        UIRectFill(rect)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image!
        
    }
}

extension UINavigationBar {
    func setAlpha(_ alpha: CGFloat, color: UIColor = .NAVY) -> UIImage {
        let color = color.withAlphaComponent(alpha)
        let navigationBarImage = UIImage.imageFromColor(color: color)
        setBackgroundImage(navigationBarImage, for: UIBarMetrics.default)
        barStyle = .default
        shadowImage = UIImage()
        return navigationBarImage
    }
}

extension UIImageView {
    
    func setImage(fromUrl url: URL?, placeholderImage: UIImage? = nil, completion: @escaping (_ successful: Bool) -> Void) {
        
        UIImage.load(fromUrl: url, completion: { loadedImage in
            guard let image = loadedImage else {
                DispatchQueue.main.async {
                    self.image = placeholderImage
                    completion(false)
                }
                return
            }
            DispatchQueue.main.async {
                self.image = image
                completion(true)
                
            }
        })
    }
}

extension UIViewController {
    
    /**
     *  Height of status bar + navigation bar (if navigation bar exist)
     */
    
    var topbarHeight: CGFloat {
        return UIApplication.shared.statusBarFrame.size.height +
            (self.navigationController?.navigationBar.frame.height ?? 0.0)
    }
    
    public func isPresentedIntheTop() -> Bool {
        
        guard let topPresented = UIApplication.shared.topViewController else {
            return false
        }
        
        var viewController = topPresented
        
        if let navigationViewController = viewController as? UINavigationController {
            
            viewController = navigationViewController.topViewController!
        }
        
        return viewController === self
    }
}

extension FileManager {
    
    func clearTmpDirectory() {
        do {
            let tmpDirectory = try contentsOfDirectory(atPath: NSTemporaryDirectory())
            try tmpDirectory.forEach {[unowned self] file in
                let path = String(format: "%@%@", NSTemporaryDirectory(), file)
                try self.removeItem(atPath: path)
            }
        } catch {
            DLog(message: "Error: \(error)")
        }
    }
}

/// Logs a string in debug mode.
func DLog( message: @autoclosure () -> String, filename: String = #file, function: String = #function, line: Int = #line) {
    
    let lastPathComponent = (filename as NSString).lastPathComponent
    
    #if DEBUG
        os_log("%{public}@ %{public}@", type: .debug, "\(lastPathComponent):\(line): \(function):", message())
    #else
        os_log("%{private}@ %{private}@", type: .debug, "\(lastPathComponent):\(line): \(function):", message())
    #endif
}

/// Logs an error in error mode.
func ELog( error: @autoclosure () -> Error, filename: String = #file, function: String = #function, line: Int = #line) {
    
    let lastPathComponent = (filename as NSString).lastPathComponent
    os_log("%@ %@", type: .error, "\(lastPathComponent):\(line): \(function):\n", "\(error())")
}
