//
//  PopAnimatorModal.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 6/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation

@objc class PopAnimatorModal: NSObject, UIViewControllerAnimatedTransitioning {

    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return 0.5
    }

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {

        let fromViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)!
        let toViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)!
        let containerView = transitionContext.containerView
        let bounds = UIScreen.main.bounds
        containerView.insertSubview(toViewController.view, belowSubview: fromViewController.view)

        UIView.animate(withDuration: transitionDuration(using: transitionContext), animations: {
            fromViewController.view.frame = CGRect(x: 0, y: bounds.height, width: bounds.width, height: bounds.height)
        }, completion: {  _ in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        })
    }
}
