//
//  ConfigPublicManager.swift
//  shoppingapp
//
//  Created by AZIZEBULBUL on 25/01/2018.
//  Copyright © 2018 BBVA. All rights reserved.
//
import RxSwift
import Foundation
import RNCryptor

class ConfigPublicManager {

    // MARK: Singleton
    static var instance = ConfigPublicManager()

    var configs: ConfigFilesBO?
    let disposeBag = DisposeBag()
    var didacticArea: DidacticAreasBO?

    lazy var uniqueIdentifier: String = {
        let uniqueIdentifier: String

        if let identifier = KeychainManager.shared.getUniqueIdentifier() {
            uniqueIdentifier = identifier
        } else {
            uniqueIdentifier = UUID().uuidString.sha256()!
            KeychainManager.shared.saveUniqueIdentifier(forUniqueIdentifier: uniqueIdentifier)
        }
        return uniqueIdentifier
    }()

    fileprivate let defaultIndex = -1

    class func sharedInstance() -> ConfigPublicManager {
        return instance
    }

    init() {
        DLog(message: "Init ConfigPublicManager")
        getConfigFilesBO()
        getDidacticAreaBO()
    }

    private func provideConfigFile() -> Observable <ModelBO> {

        return Observable.create { observer in

            ConfigPublicDataManager.sharedInstance.provideHelpConfigFile().subscribe(
                onNext: { result in

                    if let configFilesEntity = result as? ConfigFilesEntity {
                        observer.onNext(ConfigFilesBO(configFilesEntity: configFilesEntity))
                        observer.onCompleted()
                    } else {
                        DLog(message: "Incorrect configFilesEntity")
                        observer.onError(ServiceError.CouldNotDecodeJSON)
                    }
                },
                onError: { error in
                    DLog(message: "\(error)")

                }, onCompleted: {
                }).addDisposableTo(self.disposeBag)
            return Disposables.create()
        }
    }

    func getConfigFile() {

        provideConfigFile().subscribe(
            onNext: { [weak self] result in
                guard let `self` = self else {
                    return
                }

                if let configFilesBO = result as? ConfigFilesBO {
                    self.configs = configFilesBO

                    let version = self.configs?.version
                    let kSavedHelpVersion = PreferencesManagerKeys.kSavedHelpVersion

                    if PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: kSavedHelpVersion) as? String != version {
                        self.resetCounters()
                    }

                    PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: version as AnyObject, withKey: kSavedHelpVersion)
                }
            },
            onError: { error in
                DLog(message: "\(error)")

            }).addDisposableTo(disposeBag)

    }

    private func getConfigFilesBO() {

        let filename = Constants.help_config_file
        if isFileExist(forPath: dataFilePath(forFilename: filename)) {

            var dataString: String
            dataString = readFromSavedFile(forFilename: filename)
            if let configFilesEntity = ConfigFilesEntity.deserialize(from: dataString) {
                configs = ConfigFilesBO(configFilesEntity: configFilesEntity)
            }

            let configsFromResource = getConfigFilesBOFromResource()

            if let version = configs?.version, let versionFromResource = configsFromResource?.version {

                if let versionNumber = Double(version), let versionFromResourceNumber = Double(versionFromResource), versionNumber < versionFromResourceNumber {
                    configs = configsFromResource
                }
            } else {
                configs = getConfigFilesBOFromResource()
            }

        } else {

            configs = getConfigFilesBOFromResource()
        }
    }

    private func getConfigFilesBOFromResource() -> ConfigFilesBO? {

        let resource = Constants.help_config_file
        let fileProvider = FileProvider()
        let contentsOfFile = fileProvider.readResourceFromBundleAsString(forResource: resource, ofType: "json")

        if !contentsOfFile.isEmpty {

            if let configFilesEntity = ConfigFilesEntity.deserialize(from: contentsOfFile) {
                return ConfigFilesBO(configFilesEntity: configFilesEntity)
            }
        }
        return configs
    }

    func getConfigBOBySection(forKey key: String) -> [ConfigFileBO] {

        var configFilesBO = [ConfigFileBO]()

        if let configFiles = configs?.items {
            for configFileBO in configFiles {
                if let sectionsBO = configFileBO.sections, configFileBO.getSelectedLanguage(forKey: ConstantsLanguage.DEFAULT_LANGUAGE) != nil {
                    for sectionBO in sectionsBO {
                        if let sectionId = sectionBO.sectionId, !sectionId.isEmpty {
                            if sectionId == key {
                                configFilesBO.append(configFileBO)
                            }
                        }
                    }
                }
            }
        }

        return configFilesBO

    }

    func getConfigBO(forSection section: String, withKey key: PreferencesManagerKeys) -> ConfigFileBO? {

        let sectionsArray = getConfigBOBySection(forKey: section)

        if !sectionsArray.isEmpty {

            let indexToShow = getIndexToShowed(forKey: key, withSectionsArray: sectionsArray)

            return sectionsArray[indexToShow]

        } else {
            
            return nil
        }
    }

    func getIndexToShowed(forKey key: PreferencesManagerKeys, withSectionsArray sectionArray: [ConfigFileBO]) -> Int {

        var indexToShow: Int = 0

        if PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: key) as? Int == nil {
            indexToShow = 0
            PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: indexToShow as AnyObject, withKey: key)

        } else {

            indexToShow = (PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: key) as? Int) ?? defaultIndex
            indexToShow += 1

            if indexToShow > (sectionArray.count) - 1 {
                indexToShow = 0
            }
            PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: indexToShow as AnyObject, withKey: key)

        }

        return indexToShow
    }

    func stringToDictionary(forJsonText jsonText: String) -> NSDictionary {

        var dictonary: NSDictionary?

        if let data = jsonText.data(using: String.Encoding.utf8) {
            do {
                dictonary = try JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject] as NSDictionary?

            } catch let error as NSError {
                DLog(message: "Error StringConvertToDictionary: \(error)")
            }
        }
        return dictonary!
    }

    // MARK: Files data

    func writeJson(withData data: String, andFilename filename: String) {

        let  fileURL = getJsonFileURL(withFilename: filename)

        do {
            try data.write(to: fileURL, atomically: true, encoding: String.Encoding.utf8)
        } catch let error as NSError {
            DLog(message: "Error! write file: \(error)")
        }
    }

    func readJson(withFilename filename: String) -> String {

        var contents = ""

        let  fileURL = getJsonFileURL(withFilename: filename)

        do {
            contents = try String(contentsOf: fileURL, encoding: .utf8)
        } catch let error as NSError {
            DLog(message: "Error! read file: \(error)")
        }

        return contents
    }

    func getJsonFileURL(withFilename filename: String) -> URL {

        let documentsURL = FileManager.default.urls(for: .documentDirectory,
                                                    in: .userDomainMask)[0]

        let  fileURL = documentsURL.appendingPathComponent("\(filename).json")

        return fileURL
    }

    private func getJsonFilePath(forFilename fileName: String) -> String {
        return documentsDirectory().appendingFormat( "/\(fileName).json")
    }

    // MARK: Local data

    func writeToPlist(forItems items: String, withFilename filename: String) {

        let data = items.data(using: .utf8)
        let encriptedData = RNCryptor.encrypt(data: data!, withPassword: getPassword())
        do {
            try encriptedData.write(to: setFileURL(withfileName: filename))
        } catch let error as NSError {
            DLog(message: "Error Save Data: \(error)")
        }

    }

    func readFromSavedFile(forFilename filename: String) -> String {

        var dataString = ""
        let data = readFromSavedFileAsData(forFilename: filename)

        do {
            let originalData = try RNCryptor.decrypt(data: data as Data, withPassword: getPassword())

            dataString = NSString(data: originalData, encoding: String.Encoding.utf8.rawValue)! as String

        } catch let error as NSError {
            DLog(message: "Error ReadFile From RynCription: \(error)")

        }

        return dataString

    }

    private func readFromSavedFileAsData(forFilename filename: String) -> NSData {

        let path = dataFilePath(forFilename: filename)
        var data = NSData()

        if isFileExist(forPath: path) {
            do {
                try data = NSData(contentsOfFile: path)

            } catch  let error as NSError {
                DLog(message: "Error ReadFile: \(error)")
            }

        }

        return data

    }

    private func isFileExist(forPath path: String) -> Bool {

        let checkValidation = FileManager.default

        if checkValidation.fileExists(atPath: path) {
            return true

        }
        return false

    }

    private func dataFilePath(forFilename fileName: String) -> String {
        return documentsDirectory().appendingFormat( "/\(fileName).plist")
    }

    private func documentsDirectory() -> String {
        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory,
                                                        .userDomainMask, true)
        return paths.first!
    }

    private func setFileURL(withfileName filename: String) -> URL {
        let documentsURL = FileManager.default.urls(for: .documentDirectory,
                                                    in: .userDomainMask)[0]

        let  fileURL = documentsURL.appendingPathComponent("\(filename).plist")

        return fileURL

    }
    private func getPassword() -> String {
        
        return uniqueIdentifier
    }

    func removeExistingFile(withFileName fileName: String) {

        let existingPath = dataFilePath(forFilename: fileName)

        do {
            try FileManager.default.removeItem(atPath: existingPath)

        } catch let error as NSError {
            DLog(message: "Error RemoveFile: \(error)")
        }
    }

    func resetCounters() {
        
        PreferencesManager.sharedInstance().deleteValueWithPreferencesKey(forKey: PreferencesManagerKeys.kIndexToShowCards)
    }

    // MARK: Didactic Area

    private func provideDidacticArea() -> Observable <ModelBO> {

        return Observable.create { observer in

            DidacticAreaDataManager.sharedInstance.serviceDidacticArea().subscribe(
                onNext: { result in

                    if let didacticAreasEntity = result as? DidacticAreasEntity {
                        observer.onNext(DidacticAreasBO(didacticAreasEntity: didacticAreasEntity))
                        observer.onCompleted()
                    } else {
                        DLog(message: "Incorrect DidacticAreasEntity")
                        observer.onError(ServiceError.CouldNotDecodeJSON)
                    }
                },

                onError: { error in

                    let evaluate: ServiceError = (error as? ServiceError)!

                    switch evaluate {

                    case .GenericErrorEntity(let errorEntity):
                        let errorBVABO = ErrorBO(error: errorEntity)
                        observer.onError(ServiceError.GenericErrorBO(error: errorBVABO))
                    default:
                        observer.onError(evaluate)
                    }

                },
                onCompleted: {
                }).addDisposableTo(self.disposeBag)

            return Disposables.create()
        }
    }

    func getDidacticArea() {

        provideDidacticArea().subscribe(
            onNext: { [weak self] result in

                if let didacticAreasBO = result as? DidacticAreasBO {
                    self?.didacticArea = didacticAreasBO
                    self?.checkDidacticAreaVersion(withVersion: didacticAreasBO.version)
                }
            },
            onError: { error in
                DLog(message: "\(error)")

            }).addDisposableTo(disposeBag)

    }

    func checkDidacticAreaVersion(withVersion version: String) {

        let versionSaved = PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kSavedDidacticAreaVersion)
        if version != versionSaved as? String {
            DLog(message: "Didactic Area version changed")
            resetDidacticArea(forKey: PreferencesManagerKeys.kIndexDidacticArea)
            saveIndexsOfCategories()
        }
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: version as AnyObject, withKey: PreferencesManagerKeys.kSavedDidacticAreaVersion)
    }

    private func getDidacticAreaBO() {

        let filename = Constants.didactic_area_file
        if isFileExist(forPath: getJsonFilePath(forFilename: filename)) {

            var dataJson: String
            dataJson = readJson(withFilename: filename)
            if let didacticAreasEntity = DidacticAreasEntity.deserialize(from: dataJson) {
                didacticArea = DidacticAreasBO(didacticAreasEntity: didacticAreasEntity)
            }
        }
    }

    // MARK: Logic Didactic Area

    func getIndexDidacticAreaToShow(onItems items: [AnyObject], andKey key: PreferencesManagerKeys) -> Int {
        
        if PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: key) as? Int == nil {
            resetDidacticArea(forKey: key)
        }
        
        var indexSaved = (PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: key) as? Int) ?? defaultIndex
        if indexSaved > (items.count) - 1 {
            resetDidacticArea(forKey: key)
        }
        
        var indexToShow: Int = -1
        var index: Int = 0
        
        for _ in items {
            
            indexSaved = (PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: key) as? Int) ?? defaultIndex
            if indexSaved != index && indexSaved < index {
                indexToShow = index
            }
            
            if indexToShow != -1 {
                break
            }
            index += 1
        }
        
        if indexToShow == -1 {
            resetDidacticArea(forKey: key)
            indexToShow = 0
        }
        
        return indexToShow
    }

    private func resetDidacticArea(forKey key: PreferencesManagerKeys) {
        
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: defaultIndex as AnyObject, withKey: key)
    }

    private func saveValueDidacticArea(forValue value: AnyObject, withKey key: PreferencesManagerKeys) {
        
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: value, withKey: key)
    }

    func getDidacticAreaToShow() -> (didacticAreaBO: DidacticAreaBO?, didacticCategoryBO: DidacticCategoryBO?) {

        //First check helpId == id-special
        if let itemsIdSpecial = didacticArea?.itemsIdSpecial {

            let dateNow = Date()

            for item in itemsIdSpecial {

                if (item.startDate != nil) && (item.endDate != nil) {

                    if (dateNow.compare(item.startDate!) == .orderedDescending || dateNow.compare(item.startDate!) == .orderedSame) && (dateNow.compare(item.endDate!) == .orderedAscending || dateNow.compare(item.endDate!) == .orderedSame) {

                        return (didacticAreaBO: item, didacticCategoryBO: nil)
                    }
                }
            }
        }

        //Second check others Didactic Areas
        if let itemsDidacticArea = didacticArea?.itemsDidacticArea {

            var indexToShow: Int
            var didacticAreaBO: DidacticAreaBO

            indexToShow = getIndexDidacticAreaToShow(onItems: itemsDidacticArea, andKey: PreferencesManagerKeys.kIndexDidacticArea)
            didacticAreaBO = itemsDidacticArea[indexToShow]
            saveValueDidacticArea(forValue: indexToShow as AnyObject, withKey: PreferencesManagerKeys.kIndexDidacticArea)

            if didacticAreaBO.type == .offer {

                if didacticArea?.categories != nil {

                    if let didacticCategory = getDidacticCategoryToShow() {
                        return (didacticAreaBO: didacticAreaBO, didacticCategoryBO: didacticCategory)
                    }
                }
            }

            return (didacticAreaBO: didacticAreaBO, didacticCategoryBO: nil)
        }

        return (didacticAreaBO: nil, didacticCategoryBO: nil)
    }

    private func getWeekDayString() -> String {

        let daysOfWeek = [DaysOfWeek.sunday.rawValue, DaysOfWeek.monday.rawValue, DaysOfWeek.tuesday.rawValue, DaysOfWeek.wednesday.rawValue, DaysOfWeek.thursday.rawValue, DaysOfWeek.friday.rawValue, DaysOfWeek.saturday.rawValue]
        let date = Date()
        let calendar = Calendar.current
        let weekday = calendar.component(.weekday, from: date)

        //Weekday units are the numbers 1 through n, where n is the number of days in the week. For example, in the Gregorian calendar, n is 7 and Sunday is represented by 1.
        let weekdayString = daysOfWeek[weekday - 1]

        return weekdayString
    }

    private func getTimeByHours() -> String {

        let hour = Calendar.current.component(.hour, from: Date())

        let morning = Settings.DidacticArea.times_of_day_morning
        let afternoon = Settings.DidacticArea.times_of_day_afternoon

        switch hour {
        case morning:
            return TimesOfDay.morning.rawValue
        case afternoon:
            return TimesOfDay.afternoon.rawValue
        default:
            return TimesOfDay.night.rawValue
        }
    }

    private func getDefaultCategory() -> DidacticCategoryBO? {

        if let defaultCategory = didacticArea?.categories?.defaultCategory {

            let indexToShow = getIndexDidacticAreaToShow(onItems: defaultCategory, andKey: PreferencesManagerKeys.kIndexDefaultCategoryDidacticArea)
            _ = saveValueDidacticArea(forValue: indexToShow as AnyObject, withKey: PreferencesManagerKeys.kIndexDefaultCategoryDidacticArea)

            return defaultCategory[indexToShow]
        }

        return nil
    }

    private func saveIndexsOfCategories() {

        let daysOfWeek = [DaysOfWeek.sunday.rawValue, DaysOfWeek.monday.rawValue, DaysOfWeek.tuesday.rawValue, DaysOfWeek.wednesday.rawValue, DaysOfWeek.thursday.rawValue, DaysOfWeek.friday.rawValue, DaysOfWeek.saturday.rawValue]
        let times = [TimesOfDay.morning.rawValue, TimesOfDay.afternoon.rawValue, TimesOfDay.night.rawValue]

        var itemsCategories: [[String: Any]] = []

        for day in daysOfWeek {

            if let weekday = didacticArea?.categories?.getTimeBO(byWeekday: day) {

                for time in times {

                    if let timeBO = weekday.getDidacticCategoryBO(byTime: time) {

                        itemsCategories.append([Constants.DidacticArea.day_of_category: "\(day)", Constants.DidacticArea.time_of_category: "\(time)", Constants.DidacticArea.index_of_category: 0, Constants.DidacticArea.count_of_category: timeBO.count])
                    }
                }
            }
        }

        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: itemsCategories as AnyObject, withKey: PreferencesManagerKeys.kSaveIndexsOfCategories)
    }

    private func getIndexCategoryToShow(withDay day: String, andTime time: String) -> Int {

        var indexToShow: Int = 0
        var indexToSave: Int = 0
        var index: Int = 0

        let categories = PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kSaveIndexsOfCategories)

        if let items = categories as? [[String: Any]] {

            for item in items {

                if item[Constants.DidacticArea.day_of_category] as! String == day && item[Constants.DidacticArea.time_of_category] as! String == time {

                    indexToShow = item[Constants.DidacticArea.index_of_category] as! Int

                    indexToSave = indexToShow
                    indexToSave += 1
                    let count = item[Constants.DidacticArea.count_of_category] as! Int

                    if indexToSave <= (count) - 1 {
                        saveNewIndexOfCategory(byDay: day, forTime: time, byIndexToSave: indexToSave, andIndexOfCategory: index)
                    } else {
                        saveNewIndexOfCategory(byDay: day, forTime: time, byIndexToSave: 0, andIndexOfCategory: index)
                    }

                    break
                }

                index += 1
            }
        }

        return indexToShow
    }

    private func saveNewIndexOfCategory(byDay day: String, forTime time: String, byIndexToSave indexToSave: Int, andIndexOfCategory indexOfCategory: Int) {

        let categories = PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kSaveIndexsOfCategories)
        var itemsCategories: [[String: Any]] = []

        if let items = categories as? [[String: Any]] {

            itemsCategories = items

            var category = itemsCategories[indexOfCategory]
            category.updateValue(indexToSave, forKey: Constants.DidacticArea.index_of_category)

            itemsCategories[indexOfCategory] = category
        }

        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: itemsCategories as AnyObject, withKey: PreferencesManagerKeys.kSaveIndexsOfCategories)
    }

    func getDidacticCategoryToShow() -> DidacticCategoryBO? {

        let weekdayString = getWeekDayString()
        let timeString = getTimeByHours()

        if let weekday = didacticArea?.categories?.getTimeBO(byWeekday: weekdayString) {

            if let time = weekday.getDidacticCategoryBO(byTime: timeString) {

                let indexToShow = getIndexCategoryToShow(withDay: weekdayString, andTime: timeString)

                return time[indexToShow]
            }
        }

        return getDefaultCategory()
    }
}
