//
//  VersionControlManager.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 23/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeComponents

public class VersionControlManager {
    
    static let id = "VersionControlManager"
    
    // MARK: Singleton
    
    static var instance: VersionControlManager?
    
    class func sharedInstance() -> VersionControlManager {
        
        guard instance != nil else {
            
            instance = VersionControlManager()
            return instance!
        }
        
        return instance!
    }

    func checkVersion(withPublicConfigurationDTO publicConfigurationDTO: PublicConfigurationDTO, shouldShowNoMandatoryUpdate: Bool) {
        
        guard let publicConfig = publicConfigurationDTO.publicConfig, let update = publicConfig.update, let iOS = update.iOS, iOS.bundleVersionIsSmaller() else {
            
            return
        }
        
        if let isMandatory = iOS.isMandatory, let url = iOS.URL, let message = iOS.message {
            
            if isMandatory || shouldShowNoMandatoryUpdate {
                
                UpgradeManager.sharedInstance().show(withModel: UpgradeNotificationTransport(isMandatory, url, message))
            }
        }
    }
    
    func checkAppUpgrade() {
        
        let currentVersion = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String
        let versionOfLastRun = PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kVersionOfLastRun) as? String
        
        if let currentVersion = currentVersion, let versionOfLastRun = versionOfLastRun, (currentVersion.compare(versionOfLastRun, options: .numeric) == .orderedAscending || currentVersion.compare(versionOfLastRun, options: .numeric) == .orderedDescending) {
            
            // App was updated since last run
            PreferencesManager.sharedInstance().deleteValueWithPreferencesKey(forKey: PreferencesManagerKeys.kNumberLoginsRateApp)
            PreferencesManager.sharedInstance().deleteValueWithPreferencesKey(forKey: PreferencesManagerKeys.kUserSkipRateApp)
            PreferencesManager.sharedInstance().deleteValueWithPreferencesKey(forKey: PreferencesManagerKeys.kAppRated)
        }
        
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: currentVersion as AnyObject, withKey: PreferencesManagerKeys.kVersionOfLastRun)
    }
}
