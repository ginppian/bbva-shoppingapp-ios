//
//  ExtraTouchAreaButton.swift
//  shoppingappGlobal
//
//  Created by ignacio.bonafonte on 12/07/2018.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit

class ExtraTouchAreaButton: UIButton {
    @IBInspectable var extraTouchMargin: CGFloat = 5

    override func point(inside point: CGPoint, with event: UIEvent?) -> Bool {
        let marginToAddH = min( extraTouchMargin, self.frame.size.width - 1)
        let marginToAddV = min( extraTouchMargin, self.frame.size.height - 1)
        let area = self.bounds.insetBy(dx: -marginToAddH, dy: -marginToAddV)
        return area.contains(point)
    }

}
