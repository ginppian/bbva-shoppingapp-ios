//
//  PublicConfigurationManager.swift
//  shoppingapp
//
//  Created by Marcos on 7/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import RxSwift
import CellsNativeCore
import CellsNativeComponents

public class PublicConfigurationManager {

    static let id = "PublicConfigurationManager"
    static let bundleDefaultConfigurationFileName = "defaultPublicConfiguration"
    
    static let minutesTimestampToUpdatePublicConfiguration = 10
    
    let downloadPublicConfigurationPublisher = PublishSubject<PublicConfigurationDTO>()
    
    var fileProvider = FileProvider()
    var publicConfigurationStore: PublicConfigurationStore?
    var grantingTicketStore: GrantingTicketStore?

    // MARK: Singleton

    static var instance: PublicConfigurationManager?

    class func sharedInstance() -> PublicConfigurationManager {

        if instance != nil {

            return instance!
        } else {

            instance = PublicConfigurationManager()
            return instance!
        }
    }

    init() {

        if let networkWorker = BusManager.sessionDataInstance.networkWorker {

            ComponentManager.add(id: PublicConfigurationManager.id, component: self)
            
            publicConfigurationStore = PublicConfigurationStore(worker: networkWorker, url: ServerHostURL.getAppSettingsURL())
            
            grantingTicketStore = GrantingTicketStore(worker: networkWorker, host: ServerHostURL.getGrantingTicketURL())
        }
    }

    // MARK: - Public
    
    var isNecessaryToUpdate: Bool {
        
        guard let date = PreferencesManager.sharedInstance().getValue(forKey: PreferencesManagerKeys.kLastTimeUpdatePublicConfiguration.rawValue) as? Date else {
            return true
        }
        
        guard let differenceFromStartToEnd = Calendar.current.dateComponents([.minute], from: date, to: Date()).minute, differenceFromStartToEnd >= PublicConfigurationManager.minutesTimestampToUpdatePublicConfiguration else {
            return false
        }
        
        return true
    }
    
    func getPublicConfiguration() -> Observable<PublicConfigurationDTO> {
        
        if SessionDataManager.sessionDataInstance().isUserAnonymous || SessionDataManager.sessionDataInstance().isUserLogged {

            publicConfigurationStore?.getPublicConfiguration()
        } else {
            
            let anonymousInformationBO = Configuration.anonymousInformation()

            let grantingTicket = GrantingTicketDTO(consumerID: anonymousInformationBO.consumerId,
                                                   userID: anonymousInformationBO.userId,
                                                   authenticationType: "61",
                                                   password: nil,
                                                   idAuthenticationData: nil,
                                                   backendUserRequestUserID: "",
                                                   accessCode: anonymousInformationBO.userId,
                                                   dialogId: "")
            grantingTicketStore?.grantingTicketAnonymous(grantingTicket: grantingTicket)
        }

        return downloadPublicConfigurationPublisher.asObservable()
    }
    
    func saveDefaultPublicConfiguration() {
        
        savePublicConfiguration(loadDefaultPublicConfigurationInformation())
    }
    
    func savePublicConfiguration(_ data: PublicConfigurationDTO) {
        
        guard let jsonString = data.toJSONString() else {
            
            return
        }
        
        fileProvider.writeString(jsonString, fileName: PublicConfigurationManager.bundleDefaultConfigurationFileName + ".json")
        
        PreferencesManager.sharedInstance().saveValue(forValue: Date() as AnyObject, withKey: PreferencesManagerKeys.kLastTimeUpdatePublicConfiguration.rawValue)
    }
    
    func loadDefaultPublicConfigurationInformation() -> PublicConfigurationDTO {
        
        return DefaultPublicConfigurationDTO()
    }
    
    func localPublicConfiguration() -> PublicConfigurationDTO {
        
        let dataJSON = fileProvider.readFileNameAsString(forFileName: PublicConfigurationManager.bundleDefaultConfigurationFileName + ".json")
        
        if dataJSON.isEmpty {
            
            return loadDefaultPublicConfigurationInformation()
        } else {
            
            if let publicConfiguration = PublicConfigurationDTO.deserialize(from: dataJSON) {
                
                return publicConfiguration
            } else {
                
                return loadDefaultPublicConfigurationInformation()
            }
        }
    }
}

// MARK: - PublicConfiguration Store
extension PublicConfigurationManager {
    
    func publicConfigurationSuccessLoadedFromStore(publicConfigurationDTO: PublicConfigurationDTO) {
        
        savePublicConfiguration(publicConfigurationDTO)
        
        downloadPublicConfigurationPublisher.onNext(publicConfigurationDTO)
    }
    
    func publicConfigurationErrorLoadedFromStore(error: ErrorBO) {
        
        downloadPublicConfigurationPublisher.onNext(localPublicConfiguration())
    }
}

// MARK: - GrantingTicket Store
extension PublicConfigurationManager {
    
    func grantingTicketSuccess(grantingTicketResponseDTO: GrantingTicketResponseDTO) {
        
        publicConfigurationStore?.getPublicConfiguration()
    }
    
    func grantingTicketError(error: ErrorBO) {
        
        downloadPublicConfigurationPublisher.onNext(localPublicConfiguration())
    }
}

// MARK: - Default Configuration
extension PublicConfigurationManager {
    
    class DefaultPublicConfigurationDTO: PublicConfigurationDTO {
        
        static let glomoScheme = "BBVA-Global://"
        static let glomoMarketURL = "https://itunes.apple.com/mx/app/bbva-m%C3%A9xico/id1153847325?mt=8"

        static let bancomerScheme = "Bancomer://"
        static let bancomerMarketURL = "https://itunes.apple.com/es/app/bancomer-movil/id374824226?mt=8"

        required init() {
            
            super.init()
            publicConfig = PublicConfigData()
            
            publicConfig?.update = UpdateData()
            publicConfig?.update?.Android = UpdateOSData(isMandatory: false, URL: "https://play.google.com/store/apps/details?id=com.bbva.bbvawalletmx&hl=es", message: "Actualiza la app para seguir disfrutando de la mejor experiencia.", version: "0.1")
            publicConfig?.update?.iOS = UpdateOSData(isMandatory: false, URL: "https://itunes.apple.com/us/app/bbva-wallet-mexico/id970582311?mt=8", message: "Actualiza la app para seguir disfrutando de la mejor experiencia.", version: "0.1")
            
            publicConfig?.maintenance = MaintenanceData(Android: MaintenanceOSData(), iOS: MaintenanceOSData())
            publicConfig?.maintenance?.Android?.maintenancePeriod = MaintenancePeriodData(initTime: "2018-01-20T00:55:22.000+0100", endTime: "2018-01-20T05:55:22.000+0100")
            publicConfig?.maintenance?.Android?.message = "Estamos trabajando para que BBVA Wallet funcione cada día mejor, es por eso que el servicio no está disponible en este momento. "
            publicConfig?.maintenance?.iOS?.maintenancePeriod = MaintenancePeriodData(initTime: "2018-01-20T00:55:22.000+0100", endTime: "2018-01-20T05:55:22.000+0100")
            publicConfig?.maintenance?.iOS?.message = "Estamos trabajando para que BBVA Wallet funcione cada día mejor, es por eso que el servicio no está disponible en este momento. "
            
            publicConfig?.traceability = TraceabilityData(level: 0)
            
            publicConfig?.characterLimits = CharacterLimitsData()
            publicConfig?.characterLimits?.movementsConcept = 30
            publicConfig?.characterLimits?.movementsAmount = 10
            publicConfig?.characterLimits?.limitsAmount = 10
            publicConfig?.characterLimits?.searchPromotionsName = 30
            publicConfig?.characterLimits?.searchPromotionsLocation = 30
            
            publicConfig?.help = HelpData()
            publicConfig?.help?.contextHelp = "https://bbva-files.s3.amazonaws.com/wallet/shopping_app/context_help/MX/pro/v0/context_help.json"
            publicConfig?.help?.faq = FAQData()
            publicConfig?.help?.faq?.helpAbout = "https://bbva-files.s3.amazonaws.com/wallet/shopping_app/faq/MX/pro/v0/helpAbout.json"
            publicConfig?.help?.faq?.helpDigitalCard = "https://bbva-files.s3.amazonaws.com/wallet/shopping_app/faq/MX/pro/v0/helpDigitalCard.json"
            publicConfig?.help?.faq?.helpHce = "https://bbva-files.s3.amazonaws.com/wallet/shopping_app/faq/MX/pro/v0/helpHce.json"
            publicConfig?.help?.faq?.helpOnOff = "https://bbva-files.s3.amazonaws.com/wallet/shopping_app/faq/MX/pro/v0/helpOnOff.json"
            publicConfig?.help?.faq?.helpPoints = "https://bbva-files.s3.amazonaws.com/wallet/shopping_app/faq/MX/pro/v0/helpPoints.json"
            publicConfig?.help?.faq?.helpPromotionsDetail = "https://bbva-files.s3.amazonaws.com/wallet/shopping_app/faq/MX/pro/v0/helpPromotionsDetail.json"
            publicConfig?.help?.faq?.helpSticker = "https://bbva-files.s3.amazonaws.com/wallet/shopping_app/faq/MX/pro/v0/helpSticker.json"
            publicConfig?.help?.didacticArea = "https://bbva-files.s3.amazonaws.com/wallet/shopping_app/didactic/MX/pro/v0/didactic.json"
            
            publicConfig?.terms = TermsData(cud: "https://portal.bancomer.com/fbin/repositorio/CONTRATO_UNICO_DIGITAL.pdf", tyc: "", privacy: "https://bbva-files.s3.amazonaws.com/wallet/shopping_app/terms/AvisoPrivacidadBBVABancomer_v2julio2017.pdf")
            
            publicConfig?.mobileBancomer = MobileAppsData()
            publicConfig?.mobileBancomer?.Android = MobileAppOSData(schema: "com.bancomer.mbanking", url: "https://play.google.com/store/apps/details?id=com.bancomer.mbanking&hl=es")
            publicConfig?.mobileBancomer?.iOS = MobileAppOSData(schema: DefaultPublicConfigurationDTO.bancomerScheme, url: DefaultPublicConfigurationDTO.bancomerMarketURL)
            
            publicConfig?.mobileGlomo = MobileAppsData()
            publicConfig?.mobileGlomo?.Android = MobileAppOSData(schema: "com.bbva.glomo.mx", url: "https://play.google.com/store/apps/details?id=com.bbva.glomo.mx")
            publicConfig?.mobileGlomo?.iOS = MobileAppOSData(schema: DefaultPublicConfigurationDTO.glomoScheme, url: DefaultPublicConfigurationDTO.glomoMarketURL)
            
            publicConfig?.hasRateApp = true
            publicConfig?.callCenterPhone = "(+52) 5552262663"
            publicConfig?.promotionDetailContactPhone = "(+52) 5552262663"
            publicConfig?.patternSMS = "abcd_012%pattern%abcd_012"
            lastUpdate = NSNumber(value: (201810301712 as Int64))
        }
    }
}

extension PublicConfigurationDTO {
    
    @objc func isGlomoInstalled() -> Bool {
        
        let scheme = publicConfig?.mobileGlomo?.iOS?.schema ?? PublicConfigurationManager.DefaultPublicConfigurationDTO.glomoScheme

        return URLOpener().canOpen(scheme)
    }
    
    @objc func isBancomerInstalled() -> Bool {
        
        let scheme = publicConfig?.mobileBancomer?.iOS?.schema ?? PublicConfigurationManager.DefaultPublicConfigurationDTO.bancomerScheme
        
        return URLOpener().canOpen(scheme)
    }
}
