//
//  FaceOutFaceInAnimator.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 6/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation

@objc class FaceOutFaceInAnimator: NSObject, UIViewControllerAnimatedTransitioning {

    static let defaultFadeOutTimeFromViewController: TimeInterval = 0.3
    static let defaultSlideUpInTimeToViewController: TimeInterval = 0.5

    var belowNavigationBar = false
    var fadeOutTimeFromViewController: TimeInterval = 0 // Set greater than 0 to enable animation
    var slideUpInTimeToViewController: TimeInterval = FaceOutFaceInAnimator.defaultSlideUpInTimeToViewController // Set to 0 to disable animation

    init(belowNavigationBar: Bool, fadeOutTimeFromViewController: TimeInterval, slideUpInTimeToViewController: TimeInterval = defaultSlideUpInTimeToViewController) {
        self.belowNavigationBar = belowNavigationBar
        self.fadeOutTimeFromViewController = fadeOutTimeFromViewController
        self.slideUpInTimeToViewController = slideUpInTimeToViewController
    }

    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return slideUpInTimeToViewController
    }

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {

        let bounds = UIScreen.main.bounds
        let containerView = transitionContext.containerView
        let fromViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)!
        let toViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)!

        toViewController.view.frame = CGRect(x: 0, y: bounds.height, width: bounds.width, height: bounds.height)

        containerView.addSubview(toViewController.view)

        if fadeOutTimeFromViewController > 0 {

            UIView.animate(withDuration: 0.5, animations: {
                fromViewController.view.alpha = 0
            }, completion: { (_: Bool) in

                self.slideUpToViewController(transitionContext: transitionContext, fromViewController: fromViewController, toViewController: toViewController, bounds: bounds)

            })
        } else {

            self.slideUpToViewController(transitionContext: transitionContext, fromViewController: fromViewController, toViewController: toViewController, bounds: bounds)

        }
    }

    private func slideUpToViewController(transitionContext: UIViewControllerContextTransitioning, fromViewController: UIViewController, toViewController: UIViewController, bounds: CGRect) {

        var navigationBarHeight: CGFloat = 0.0

        if let navigationVC = toViewController.navigationController, !belowNavigationBar {
            navigationBarHeight = navigationVC.completeHeight()
        }

        if slideUpInTimeToViewController > 0 {

            UIView.animate(withDuration: self.transitionDuration(using: transitionContext), animations: {
                toViewController.view.frame = CGRect(x: 0, y: navigationBarHeight, width: bounds.width, height: bounds.height - navigationBarHeight)
            }, completion: { (finished: Bool) in

                fromViewController.view.alpha = 1
                transitionContext.completeTransition(finished)
            })

        } else {
            toViewController.view.frame = CGRect(x: 0, y: navigationBarHeight, width: bounds.width, height: bounds.height - navigationBarHeight)
            fromViewController.view.alpha = 1
            transitionContext.completeTransition(true)
        }

    }
}
