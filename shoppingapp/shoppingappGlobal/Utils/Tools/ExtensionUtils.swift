//
//  NetworkUtils.swift
//  shoppingapp
//
//  Created by ruben.fernandez on 25/8/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation

extension String {

    /// Percent escapes values to be added to a URL query as specified in RFC 3986
    ///
    /// This percent-escapes all characters besides the alphanumeric character set and "-", ".", "_", and "~".
    ///
    /// http://www.ietf.org/rfc/rfc3986.txt
    ///
    /// :returns: Returns percent-escaped string.

    func addingPercentEncodingForURLQueryValue() -> String? {
        let allowedCharacters = CharacterSet(charactersIn: "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._~/=&")

        return self.addingPercentEncoding(withAllowedCharacters: allowedCharacters)
    }

    func height(withConstrainedWidth width: CGFloat, font: UIFont) -> CGFloat {

        let constraintRect = CGSize(width: width, height: .greatestFiniteMagnitude)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)

        return ceil(boundingBox.height)
    }

    func width(withConstraintedHeight height: CGFloat, font: UIFont) -> CGFloat {

        let constraintRect = CGSize(width: .greatestFiniteMagnitude, height: height)
        let boundingBox = self.boundingRect(with: constraintRect, options: .usesLineFragmentOrigin, attributes: [NSAttributedStringKey.font: font], context: nil)

        return ceil(boundingBox.width)
    }

    func diacriticInsensitiveAndLowerCase() -> String {

        let result = self.folding(options: .diacriticInsensitive, locale: .current).lowercased()

        return result

    }

    func sha256() -> String? {

        guard let stringData = self.data(using: String.Encoding.utf8) else {
            
            return nil
        }

        var hashData = Data(count: Int(CC_SHA256_DIGEST_LENGTH))
        _ = hashData.withUnsafeMutableBytes { digestBytes in

            stringData.withUnsafeBytes { messageBytes in
                
                CC_SHA256(messageBytes, CC_LONG(stringData.count), digestBytes)
            }
        }
        return stringData.base64EncodedString(options: [])
    }

    var removeSpecialCharsAndAccentFromString: String {
        
        let characters = """
        \\abcdefghijklmnopqrstuvwxyz\" ABCDEFGHIJKLKMNOPQRSTUVWXYZ1234567890?!#'()*/:;<=>@[]{|}~
        """
        let result = Set(characters)
        return self.folding(options: .diacriticInsensitive, locale: .current).lowercased().filter { result.contains($0) }
    }

    func substring(from: Int) -> String {
        
        let fromIndex = index(self.startIndex, offsetBy: from)
        let substring = self[fromIndex...]
        return String(substring)
    }
    
    func substring(to: Int) -> String {
        
        let toIndex = index(self.startIndex, offsetBy: to)
        let substring = self[...toIndex]
        return String(substring)
    }
    
    func substring(from: Int, to: Int) -> String {
        
        let fromIndex = index(self.startIndex, offsetBy: from)
        let toIndex = index(self.startIndex, offsetBy: to)
        let substring = self[fromIndex..<toIndex]
        return String(substring)
    }
}

extension NSMutableAttributedString {

    @discardableResult func bold(_ text: String, size: CGFloat) -> NSMutableAttributedString {

        let attributes: [NSAttributedStringKey: Any] = [.font: Tools.setFontBold(size: size)]
        let attributedString = NSMutableAttributedString(string: text, attributes: attributes)
        append(attributedString)

        return self
    }

    @discardableResult func book(_ text: String, size: CGFloat) -> NSMutableAttributedString {

        let attributes: [NSAttributedStringKey: Any] = [.font: Tools.setFontBook(size: size)]
        let attributedString = NSMutableAttributedString(string: text, attributes: attributes)
        append(attributedString)

        return self
    }

    @discardableResult func bookItalic(_ text: String, size: CGFloat) -> NSMutableAttributedString {

        let attributes: [NSAttributedStringKey: Any] = [.font: Tools.setFontBookItalic(size: size)]
        let attributedString = NSMutableAttributedString(string: text, attributes: attributes)
        append(attributedString)

        return self
    }

    @discardableResult func light(_ text: String, size: CGFloat) -> NSMutableAttributedString {

        let attributes: [NSAttributedStringKey: Any] = [.font: Tools.setFontLight(size: size)]
        let attributedString = NSMutableAttributedString(string: text, attributes: attributes)
        append(attributedString)

        return self
    }

    @discardableResult func lightItalic(_ text: String, size: CGFloat) -> NSMutableAttributedString {

        let attributes: [NSAttributedStringKey: Any] = [.font: Tools.setFontLightItalic(size: size)]
        let attributedString = NSMutableAttributedString(string: text, attributes: attributes)
        append(attributedString)

        return self
    }

    @discardableResult func medium(_ text: String, size: CGFloat) -> NSMutableAttributedString {

        let attributes: [NSAttributedStringKey: Any] = [.font: Tools.setFontMedium(size: size)]
        let attributedString = NSMutableAttributedString(string: text, attributes: attributes)
        append(attributedString)

        return self
    }

    @discardableResult func mediumItalic(_ text: String, size: CGFloat) -> NSMutableAttributedString {

        let attributes: [NSAttributedStringKey: Any] = [.font: Tools.setFontMediumItalic(size: size)]
        let attributedString = NSMutableAttributedString(string: text, attributes: attributes)
        append(attributedString)

        return self
    }
}
extension Dictionary {

    /// Build string representation of HTTP parameter dictionary of keys and objects
    ///
    /// This percent escapes in compliance with RFC 3986
    ///
    /// http://www.ietf.org/rfc/rfc3986.txt
    ///
    /// :returns: String representation in the form of key1=value1&key2=value2 where the keys and values are percent escaped

    func stringFromHttpParameters(withEncoding needEncoding: Bool) -> String {
        let parameterArray = self.map { key, value -> String in
            let percentEscapedKey = needEncoding ? (key as! String).addingPercentEncodingForURLQueryValue()! : key as! String
            let percentEscapedValue = needEncoding ? (value as! String).addingPercentEncodingForURLQueryValue()! : value as! String
            return "\(percentEscapedKey)=\(percentEscapedValue)"
        }

        return parameterArray.joined(separator: "&")
    }

}

extension Encodable {
    func toJSON() -> [Any]? {
        guard let data = try? JSONEncoder().encode(self) else {
            return nil
        }
        return (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap { $0 as? [Any] }
    }
}

extension BaseViewController {
    
    var defaultHeightToastView: CGFloat {
        
        return 100
    }
}

extension URL {

    var domain: String {

        guard let host = self.host else {

            return ""
        }
        
        var domain = host
        if host.hasPrefix("www.") {
            domain = host.substring(from: 4)
        }
        
        return domain
    }
}
