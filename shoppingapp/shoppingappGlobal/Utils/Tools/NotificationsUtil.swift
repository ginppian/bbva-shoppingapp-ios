//
//  NotificationsUtil.swift
//  shoppingapp
//
//  Created by Marcos on 24/9/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import UserNotifications

protocol NotificationManager {
    func checkAuthorizedStatus(_ isDetermined: @escaping (Bool) -> Void)
    func isAuthorizedStatus(_ isAuthorized: @escaping (Bool) -> Void)
    func showRequestNotificationAutorization(_ isGranted: @escaping (Bool) -> Void)
}

extension NotificationManager {

    func showRequestNotificationAutorization(_ isGranted: @escaping (Bool) -> Void) {
        
        DispatchQueue.main.async {
            
            let center = UNUserNotificationCenter.current()
            center.requestAuthorization(options: [.badge, .alert, .sound]) { granted, _ in
                isGranted(granted)
            }
            
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
    
    func checkAuthorizedStatus(_ isDetermined: @escaping (Bool) -> Void) {
        
        let center = UNUserNotificationCenter.current()
        center.getNotificationSettings { settings in
            isDetermined(settings.authorizationStatus != UNAuthorizationStatus.notDetermined)
        }
    }

    func isAuthorizedStatus(_ isAuthorized: @escaping (Bool) -> Void) {
        
            let center = UNUserNotificationCenter.current()
            center.getNotificationSettings { settings in
                isAuthorized(settings.authorizationStatus == UNAuthorizationStatus.authorized)
            }
    }
}

struct NotificationManagerType: NotificationManager {}
