//
//  TabBarAnimatedTransition.swift
//  shoppingapp
//
//  Created by Javier Pino on 7/6/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol TabBarOutTransitionAnimating {

    var movingOutView: UIView? { get }
    var fadingOutView: UIView? { get }
}

protocol TabBarInTransitionAnimating {

    var movingInView: UIView? { get }
    var fadingInView: UIView? { get }
}

enum MovingDirection {
    case leftToRight
    case rightToLeft
}

class TabBarAnimatedTransitioning: NSObject, UIViewControllerAnimatedTransitioning {

    let transitionAnimationDuration = 0.6
    var movingDirection = MovingDirection.rightToLeft

    init(movingDirection: MovingDirection) {
        self.movingDirection = movingDirection
    }

    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return transitionAnimationDuration
    }

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {

        let contextOutViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)
        let contextInViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)

        guard let contextInViewControllerView = contextInViewController?.view else {
            return
        }

        contextInViewControllerView.isHidden = true
        transitionContext.containerView.addSubview(contextInViewControllerView)

        var outViewController = contextOutViewController
        var inViewController = contextInViewController

        if let outNavigationController = outViewController as? UINavigationController {
            outViewController = outNavigationController.viewControllers.first
        }
        if let inNavigationController = inViewController as? UINavigationController {
            inViewController = inNavigationController.viewControllers.first
        }

        let outTransitionAnimating = outViewController as? TabBarOutTransitionAnimating
        let inTransitionAnimating = inViewController as? TabBarInTransitionAnimating

        let movingOutView = outTransitionAnimating?.movingOutView
        let fadingOutView = outTransitionAnimating?.fadingOutView
        let movingOutSnapshotView = movingOutView?.snapshotView(afterScreenUpdates: true)
        let fadingOutSnapshotView = fadingOutView?.snapshotView(afterScreenUpdates: true)
        let movingInView = inTransitionAnimating?.movingInView
        let fadingInView = inTransitionAnimating?.fadingInView
        let movingInSnapshotView = movingInView?.snapshotView(afterScreenUpdates: true)
        let fadingInSnapshotView = fadingInView?.snapshotView(afterScreenUpdates: true)

        let positionToMove = Display.width

        if let movingOutSnapshotView = movingOutSnapshotView, let movingOutView = movingOutView, let movingOutViewSuperview = movingOutView.superview {
            movingOutSnapshotView.frame = movingOutViewSuperview.convert(movingOutView.frame, to: nil)
            transitionContext.containerView.addSubview(movingOutSnapshotView)
        }
        movingOutView?.isHidden = true
        if let movingInSnapshotView = movingInSnapshotView, let movingInView = movingInView, let movingInViewSuperview = movingInView.superview {
            var movingInFrame = movingInViewSuperview.convert(movingInView.frame, to: nil)
            if self.movingDirection == .leftToRight {
                movingInFrame.origin.x = -positionToMove
            } else {
                movingInFrame.origin.x = positionToMove
            }
            movingInSnapshotView.frame = movingInFrame
            transitionContext.containerView.addSubview(movingInSnapshotView)
        }
        movingInView?.isHidden = true
        if let fadingOutSnapshotView = fadingOutSnapshotView, let fadingOutView = fadingOutView, let fadingOutViewSuperview = fadingOutView.superview {
            fadingOutSnapshotView.frame = fadingOutViewSuperview.convert(fadingOutView.frame, to: nil)
            transitionContext.containerView.addSubview(fadingOutSnapshotView)
        }
        fadingOutView?.isHidden = true
        if let fadingInSnapshotView = fadingInSnapshotView, let fadingInView = fadingInView, let fadingInViewSuperview = fadingInView.superview {
            fadingInSnapshotView.alpha = 0.0
            let fadingInFrame = fadingInViewSuperview.convert(fadingInView.frame, to: nil)
            fadingInSnapshotView.frame = fadingInFrame
            transitionContext.containerView.addSubview(fadingInSnapshotView)
        }

        UIView.animateKeyframes(withDuration: transitionDuration(using: transitionContext), delay: 0.0, options: .calculationModeLinear, animations: {

            if self.movingDirection == .leftToRight {
                movingOutSnapshotView?.frame.origin.x = positionToMove
            } else {
                movingOutSnapshotView?.frame.origin.x = -positionToMove
            }
            movingInSnapshotView?.frame.origin.x = 0.0

            UIView.addKeyframe(withRelativeStartTime: 0.0, relativeDuration: 0.3, animations: {

                fadingOutSnapshotView?.alpha = 0.0
            })

            UIView.addKeyframe(withRelativeStartTime: 0.3, relativeDuration: 0.7, animations: {

                fadingInSnapshotView?.alpha = 1.0
            })

        }, completion: { _  in

            fadingInSnapshotView?.removeFromSuperview()

            contextInViewControllerView.isHidden = false
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
            movingOutSnapshotView?.removeFromSuperview()
            movingInSnapshotView?.removeFromSuperview()
            fadingOutSnapshotView?.removeFromSuperview()
            movingOutView?.isHidden = false
            movingInView?.isHidden = false
            fadingOutView?.isHidden = false
        })
    }
}
