//
//  AmountFormatter.swift
//  shoppingapp
//
//  Created by jesus.martinez on 11/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation

class AmountFormatter {

    static let MAX_DECIMALS = 2

    let codeCurrency: String
    var locale: Locale

    init (codeCurrency: String, locale: Locale) {
        self.codeCurrency = codeCurrency
        self.locale = locale
    }

    convenience init(codeCurrency: String) {
        self.init(codeCurrency: codeCurrency, locale: Locale.current)
    }

    static func decimalFormatter(locale: Locale) -> NumberFormatter {

        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.locale = locale

        return formatter
    }

    func format(amount: NSDecimalNumber, withSpace: Bool) -> String {

        return format(amount: amount, isNegative: false, withSpace: withSpace)

    }

    func format(amount: NSDecimalNumber) -> String {

        let numberZero: NSDecimalNumber = NSDecimalNumber.zero
        return format(amount: amount, isNegative: amount.compare(numberZero) == ComparisonResult.orderedAscending, withSpace: false)
    }

    func format(amount: NSDecimalNumber, isNegative: Bool) -> String {
        return format(amount: amount, isNegative: isNegative, withSpace: false)
    }

    func format(amount: NSDecimalNumber, isNegative: Bool, withSpace: Bool) -> String {

        let currency = Currencies.currency(forCode: self.codeCurrency)

        var amountFormatted = self.formatNonNegative(amount: amount, needDecimals: currency.needDecimals)

        let space = withSpace ? " " : ""

        if currency.position == .left {

            amountFormatted = currency.symbol + space + amountFormatted

        } else {

            amountFormatted += space + currency.symbol

        }

        if isNegative == true {
            amountFormatted = "-" + amountFormatted
        }

        return amountFormatted

    }

    func formatWithOutCurrency(amount: NSDecimalNumber, isNegative: Bool) -> String {

        let currency = Currencies.currency(forCode: self.codeCurrency)

        var amountFormatted = self.formatNonNegative(amount: amount, needDecimals: currency.needDecimals)

        if isNegative == true {
            amountFormatted = "-" + amountFormatted
        }

        return amountFormatted

    }

    func attributtedText(forAmount amount: NSDecimalNumber, isNegative: Bool, bigFontSize bigSize: Int, smallFontSize smallSize: Int) -> NSAttributedString {

        let currency = Currencies.currency(forCode: self.codeCurrency)

        let formattedAmount = self.format(amount: amount, isNegative: isNegative).replacingOccurrences(of: AmountFormatter.decimalFormatter(locale: self.locale).decimalSeparator, with: "")

        let numberOfDecimals = self.numberOfDecimals(ofAmount: amount, needDecimals: currency.needDecimals)

        let attributtedText = currency.position == .left ? leftAttributtedText(forFormattedAmount: formattedAmount, withNumberOfDecimals: numberOfDecimals, symbol: currency.symbol, isNegative: isNegative, bigFontSize: bigSize, smallFontSize: smallSize) : rightAttributtedText(forFormattedAmount: formattedAmount, withNumberOfDecimals: numberOfDecimals, symbol: currency.symbol, bigFontSize: bigSize, smallFontSize: smallSize)

        return attributtedText

    }

    func attributtedText(forAmount amount: NSDecimalNumber, bigFontSize bigSize: Int, smallFontSize smallSize: Int) -> NSAttributedString {

        let numberZero: NSDecimalNumber = NSDecimalNumber.zero
        let isNegative: Bool = amount.compare(numberZero) == ComparisonResult.orderedAscending
        let currency = Currencies.currency(forCode: self.codeCurrency)

        let formattedAmount = self.format(amount: amount).replacingOccurrences(of: AmountFormatter.decimalFormatter(locale: self.locale).decimalSeparator, with: "")

        let numberOfDecimals = self.numberOfDecimals(ofAmount: amount, needDecimals: currency.needDecimals)

        let attributtedText = currency.position == .left ? leftAttributtedText(forFormattedAmount: formattedAmount, withNumberOfDecimals: numberOfDecimals, symbol: currency.symbol, isNegative: isNegative, bigFontSize: bigSize, smallFontSize: smallSize) : rightAttributtedText(forFormattedAmount: formattedAmount, withNumberOfDecimals: numberOfDecimals, symbol: currency.symbol, bigFontSize: bigSize, smallFontSize: smallSize)

        return attributtedText

    }

    private func leftAttributtedText(forFormattedAmount formattedAmount: String, withNumberOfDecimals numberOfDecimals: Int, symbol: String, isNegative: Bool, bigFontSize bigSize: Int, smallFontSize smallSize: Int) -> NSAttributedString {

        var attributtedString = NSMutableAttributedString(string: formattedAmount, attributes: [NSAttributedStringKey.font: Tools.setFontBook(size: CGFloat(bigSize))])

        attributtedString = addSmallAttribute(forAttributtedString: attributtedString, location: isNegative ? 1 :0, length: symbol.count, bigFont: Tools.setFontBook(size: CGFloat(bigSize)), smallFont: Tools.setFontBook(size: CGFloat(smallSize)))

        if numberOfDecimals > 0 {

            attributtedString = addSmallAttribute(forAttributtedString: attributtedString, location: formattedAmount.count - numberOfDecimals, length: numberOfDecimals, bigFont: Tools.setFontBook(size: CGFloat(bigSize)), smallFont: Tools.setFontBook(size: CGFloat(smallSize)))

        }

        return attributtedString
    }

    private func rightAttributtedText(forFormattedAmount formattedAmount: String, withNumberOfDecimals numberOfDecimals: Int, symbol: String, bigFontSize bigSize: Int, smallFontSize smallSize: Int) -> NSAttributedString {

        let lengthSmallAmount = numberOfDecimals + symbol.count

        var attributtedString = NSMutableAttributedString(string: formattedAmount, attributes: [NSAttributedStringKey.font: Tools.setFontBook(size: CGFloat(bigSize))])

        if lengthSmallAmount > 0 {

            attributtedString = addSmallAttribute(forAttributtedString: attributtedString, location: formattedAmount.count - lengthSmallAmount, length: lengthSmallAmount, bigFont: Tools.setFontBook(size: CGFloat(bigSize)), smallFont: Tools.setFontBook(size: CGFloat(smallSize)))

        }

        return attributtedString
    }

    private func addSmallAttribute(forAttributtedString attributtedString: NSMutableAttributedString, location: Int, length: Int, bigFont: UIFont, smallFont: UIFont) -> NSMutableAttributedString {

        let offsetAmount = bigFont.capHeight - smallFont.capHeight

        attributtedString.addAttribute(NSAttributedStringKey.font, value: smallFont, range: NSRange(location: location, length: length))
        attributtedString.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: location, length: length))

        return attributtedString
    }

    private func format(amount: NSDecimalNumber, needDecimals: Bool) -> String {

        let formatter = AmountFormatter.decimalFormatter(locale: self.locale)
        let numberOfDecimals = self.numberOfDecimals(ofAmount: amount, needDecimals: needDecimals)

        formatter.minimumFractionDigits = numberOfDecimals
        formatter.maximumFractionDigits = numberOfDecimals

        if !needDecimals {
            formatter.roundingMode = .down
        }

        let result = formatter.string(from: amount as NSNumber)
        return result!
    }

    private func formatNonNegative(amount: NSDecimalNumber, needDecimals: Bool) -> String {

        return format(amount: amount.abs(), needDecimals: needDecimals)

    }

    private func numberOfDecimals(ofAmount amount: NSDecimalNumber, needDecimals: Bool) -> Int {

        guard needDecimals else {
            return 0
        }

        return amount.decimalValue.significantFractionalDecimalDigits > 0 ? AmountFormatter.MAX_DECIMALS : 0
    }

}

struct Currencies {

    static let currencies = ["EUR": CurrencyBO(code: "EUR", position: .right, needDecimals: true),
                             "CLP": CurrencyBO(code: "CLP", position: .left, needDecimals: false),
                             "USD": CurrencyBO(code: "USD", position: .left, needDecimals: true),
                             "PEN": CurrencyBO(code: "PEN", position: .left, needDecimals: true),
                             "MXN": CurrencyBO(code: "MXN", position: .left, needDecimals: true),
                             "COP": CurrencyBO(code: "COP", position: .left, needDecimals: true)
                                ]

    static func currency(forCode code: String) -> CurrencyBO {
        return self.currencies[code] ?? CurrencyBO(code: code, position: .right, needDecimals: true)
    }

}

extension NSDecimalNumber {

    func abs() -> NSDecimalNumber {

        if self.compare(NSDecimalNumber.zero) == .orderedAscending {
            let negativeOne = NSDecimalNumber(mantissa: 1, exponent: 0, isNegative: true)
            return self.multiplying(by: negativeOne)
        } else {
            return self
        }

    }

}

extension Decimal {
    var significantFractionalDecimalDigits: Int {
        return max(-exponent, 0)
    }
}
