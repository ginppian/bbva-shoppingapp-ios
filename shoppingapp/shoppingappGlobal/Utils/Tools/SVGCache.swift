//
//  SVGCache.swift
//  shoppingapp
//
//  Created by jesus.martinez on 11/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class SVGCache {

    private static var cache = NSCache<NSString, UIImage>()

    class func image(forSVGNamed svgNamed: String, color: UIColor) -> UIImage {

        let cacheKey: NSString = (svgNamed + "_" + color.toHex()) as NSString

        let image: UIImage

        if let cachedImage = cache.object(forKey: cacheKey) {

            image = cachedImage

        } else {

            image = ImageUtilities.image(SVGKImage(named: svgNamed).uiImage, with: color)

            cache.setObject(image, forKey: cacheKey)

        }

        return image

    }

    class func image(forSVGNamed svgNamed: String) -> UIImage {

        let cacheKey = svgNamed as NSString

        let image: UIImage

        if let cachedImage = cache.object(forKey: cacheKey) {

            image = cachedImage
        } else {

            image = SVGKImage(named: svgNamed).uiImage
            cache.setObject(image, forKey: cacheKey)
        }

        return image
    }
}
