//
//  URLOpener.swift
//  shoppingapp
//
//  Created by jesus.martinez on 27/12/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol URLOpenerProtocol {
    func canOpen(_ urlString: String) -> Bool
    func openURL(_ urlString: String) -> Bool
    func openURL(_ url: URL)
}

struct URLOpener: URLOpenerProtocol {
    
    func canOpen(_ urlString: String) -> Bool {
        
        guard let url = URL(string: urlString) else {
            return false
        }
        
        return UIApplication.shared.canOpenURL(url)
    }
    
    @discardableResult
    func openURL(_ urlString: String) -> Bool {
        
        let canOpen = self.canOpen(urlString)
        
        if canOpen, let url = URL(string: urlString) {
            self.openURL(url)
        }
        
        return canOpen
    }
    
    func openURL(_ url: URL) {
        
        UIApplication.shared.open(url, options: [:], completionHandler: nil)
    }
}
