//
//  DistanceTools.swift
//  shoppingapp
//
//  Created by jesus.martinez on 12/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CoreLocation

class DistanceTools {

    static func calculateDistanceInMetersBetween(latitue: Double, longitude: Double, andLatiudeToCompare latitudeToCompare: Double, andLongitudeToCompare longitudeToCompare: Double) -> Double {

        let location = CLLocation(latitude: latitue, longitude: longitude)
        let locationToCompare = CLLocation(latitude: latitudeToCompare, longitude: longitudeToCompare)
        let distanceInMeters = location.distance(from: locationToCompare)

        return distanceInMeters

    }

    static func formattedDistance(withDistance distance: Double) -> String? {

        let distanceFormatted: String?

        let numberFormatter = NumberFormatter()
        numberFormatter.minimumFractionDigits = 0

        if distance >= Double(DistanceConstants.meters_to_kilometers) {

            numberFormatter.maximumFractionDigits = 1

            let kilometers = numberFormatter.string(from: NSDecimalNumber(value: distance / Double(DistanceConstants.meters_to_kilometers)))

            if let kilometers = kilometers {
                distanceFormatted = String(format: "%@ %@", kilometers, Localizables.promotions.key_distance_kilometers)
                return distanceFormatted
            }

        } else {

            numberFormatter.maximumFractionDigits = 0

            let meters = numberFormatter.string(from: NSDecimalNumber(value: distance))

            if let meters = meters {
                distanceFormatted = String(format: "%@ %@", meters, Localizables.promotions.key_distance_meters)
                return distanceFormatted
            }
        }

        return String(format: "%.0f", distance)
    }
}
