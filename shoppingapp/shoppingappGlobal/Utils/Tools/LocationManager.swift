//
//  LocationManager.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 13/6/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CoreLocation

enum LocationErrorType {
    case permissionDenied
    case gpsOff
    case gpsFail
}

enum LocationAuthorizationStatus {
    case statusDenied
    case statusNotDetermined
    case statusAuthorized
}

protocol LocationManagerProtocol {
    func startLocationObserver()
    func startLocationObserverAndCheckStatus()
    func stopLocationObserver()
    func getLocation(onSuccess success: @escaping LocationManager.Success, onFail fail: @escaping LocationManager.Failure)
    func forceStartUpdatingLocation()
}

public class LocationManager: NSObject, CLLocationManagerDelegate, LocationManagerProtocol {

    typealias Success = ((CLLocation) -> Void)
    typealias Failure = ((_ errorType: LocationErrorType) -> Void)

    fileprivate var completionHandlerSuccess: Success?
    fileprivate var completionHandlerFailure: Failure?

    private var manager: CLLocationManager
    private var _currentLocation: CLLocation?

    private var locationErrorType: LocationErrorType?
    private var locationServicesEnabled: Bool?
    private var locationAuthorizationStatus: LocationAuthorizationStatus?

    private var observerWillEnterForeground: Any?

    private var timeoutTimer: Timer?
    private let location_timeout = Settings.Global.default_location_Timeout

    override public init() {

        manager = CLLocationManager()
        super.init()
        manager.desiredAccuracy = kCLLocationAccuracyBest

        startLocationObserver()
    }

    deinit {
        DLog(message: "- Deinit LocationManager -")
        stopLocationObserver()
    }

    // MARK: - Private Methods -

    private func requestLocationPermission() {

        manager.requestWhenInUseAuthorization()
    }

    private func startUpdatingLocation() {

        manager.delegate = self
        manager.startUpdatingLocation()

        DispatchQueue.main.async {
            self.timeoutTimer = Timer.scheduledTimer(timeInterval: self.location_timeout, target: self, selector: #selector(self.timeoutHappened), userInfo: nil, repeats: false)
        }
    }

    @objc private func timeoutHappened() {

        completionFailure(withErrorType: .gpsFail)

        stopUpdatingLocation()
    }

    private func stopUpdatingLocation() {
        DispatchQueue.main.async {
            self.timeoutTimer?.invalidate()
        }
        manager.stopUpdatingLocation()
    }

    private func checkLocationStatusWhenDidNotChange() {

        let currentGpsEnabled = CLLocationManager.locationServicesEnabled()
        let currentStatus = getLocationAuthorizationStatus()

        if let locationEnabled = locationServicesEnabled, let locationStatus = locationAuthorizationStatus {

            if locationEnabled && currentGpsEnabled == false {

                completionFailure(withErrorType: .gpsOff)

            } else if locationEnabled == false && currentGpsEnabled {

                if locationStatus == .statusDenied && currentStatus == .statusDenied {
                    completionFailure(withErrorType: .permissionDenied)
                }
            }
        }

        locationServicesEnabled = currentGpsEnabled
        locationAuthorizationStatus = currentStatus
    }

    private func checkPreviousLocationStatusHasChanged() {

        let currentGpsEnabled = CLLocationManager.locationServicesEnabled()
        let currentStatus = getLocationAuthorizationStatus()

        if let locationEnabled = locationServicesEnabled, let locationStatus = locationAuthorizationStatus {

            if locationEnabled && currentGpsEnabled == false {

                completionFailure(withErrorType: .gpsOff)

            } else if locationEnabled && currentGpsEnabled {

                if locationStatus == .statusAuthorized && currentStatus == .statusDenied {
                    completionFailure(withErrorType: .permissionDenied)
                } else if locationStatus == .statusDenied && currentStatus == .statusAuthorized {
                    startUpdatingLocation()
                }

            } else if locationEnabled == false && currentGpsEnabled {

                if locationStatus == .statusDenied && currentStatus == .statusDenied {
                    completionFailure(withErrorType: .permissionDenied)
                } else if locationStatus == .statusDenied && currentStatus == .statusAuthorized {
                    startUpdatingLocation()
                }

            }
        }

        locationServicesEnabled = currentGpsEnabled
        locationAuthorizationStatus = currentStatus
    }

    private func completionFailure(withErrorType errorType: LocationErrorType) {

        guard locationErrorType != errorType else {
            return
        }

        locationErrorType = errorType

        if let completionFailure = completionHandlerFailure {

            completionFailure(errorType)
        }
    }

    private func getLocationAuthorizationStatus() -> LocationAuthorizationStatus {

        switch CLLocationManager.authorizationStatus() {

        case .notDetermined:
            return .statusNotDetermined

        case .restricted, .denied:
            return .statusDenied

        case .authorizedWhenInUse, .authorizedAlways:
            return .statusAuthorized
        }
    }

    // MARK: - Public Methods -

    public func forceStartUpdatingLocation() {
        
        locationErrorType = nil
        startUpdatingLocation()
    }
    
    public var currentLocation: CLLocation? {

        guard let location = _currentLocation else {
            return nil
        }

        if (!CLLocationCoordinate2DIsValid(location.coordinate)) ||
            (location.coordinate.latitude == 0.0 || location.coordinate.longitude == 0.0) {
            return nil
        }
        return location
    }

    public func startLocationObserver() {

        if observerWillEnterForeground == nil {

            observerWillEnterForeground = NotificationCenter.default.addObserver(forName: NSNotification.Name.UIApplicationWillEnterForeground, object: nil, queue: nil) { [weak self] _ in
                self?.checkLocationStatusWhenDidNotChange()
            }
        }

        manager.delegate = self
    }

    public func stopLocationObserver() {

        if let observer = observerWillEnterForeground {
            NotificationCenter.default.removeObserver(observer)
            observerWillEnterForeground = nil
        }

        manager.delegate = nil
    }

    public func startLocationObserverAndCheckStatus() {

        startLocationObserver()
        checkPreviousLocationStatusHasChanged()
    }

    func getLocation(onSuccess success: @escaping LocationManager.Success, onFail fail: @escaping LocationManager.Failure) {

        completionHandlerSuccess = success
        completionHandlerFailure = fail

    }

    // MARK: - CLLocationManager Delegates -

    public func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

        locationErrorType = nil

        if let location = locations.first {
            _currentLocation = location

            if let completionSuccess = completionHandlerSuccess {
                completionSuccess(location)
            }
        }

        stopUpdatingLocation()
    }

    public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {

        completionFailure(withErrorType: .gpsFail)

        stopUpdatingLocation()
    }

    public func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {

        locationServicesEnabled = CLLocationManager.locationServicesEnabled()
        locationAuthorizationStatus = getLocationAuthorizationStatus()

        guard locationServicesEnabled! else {

            completionFailure(withErrorType: .gpsOff)
            return
        }

        switch status {

        case .notDetermined:

            DLog(message: "notDetermined")
            requestLocationPermission()

        case .restricted, .denied:

            DLog(message: "restricted or denied")
            completionFailure(withErrorType: .permissionDenied)

        case .authorizedWhenInUse, .authorizedAlways:

            DLog(message: "authorized")
            startUpdatingLocation()
        }
    }
}
