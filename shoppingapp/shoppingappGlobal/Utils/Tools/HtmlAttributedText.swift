//
//  HtmlAttributedText.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 8/8/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

extension String {
    
    var htmlAttributed: NSAttributedString? {
        
        do {
            guard let data = data(using: String.Encoding.utf8) else {
                return nil
            }
            
            return try NSAttributedString(data: data,
                                          options: [.documentType: NSAttributedString.DocumentType.html,
                                                    .characterEncoding: String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
        } catch {
            
            print("error: ", error)
            return nil
        }
    }
    
    func htmlAttributed(withFont font: UIFont, andColor color: UIColor) -> NSAttributedString? {
        
        do {
            let htmlCSSString = "<style>" +
                "a { text-decoration: none }" +
                "body" +
                "{" +
                "font-size: \(font.pointSize)px;" +
                "color: #\(color.toHex());" +
                "font-family: \(font.fontName);" +
                "}" +
                "</style>" +
            "<body>\(self)</body>"
            
            guard let data = htmlCSSString.data(using: String.Encoding.utf8) else {
                return nil
            }
            
            return try NSAttributedString(data: data,
                                          options: [.documentType: NSAttributedString.DocumentType.html,
                                                    .characterEncoding: String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
        } catch {
            print("error: ", error)
            return nil
        }
    }
    
    func htmlAttributed(font: UIFont, textColor: UIColor, linkColor: UIColor, linkVisitedColor: UIColor) -> NSAttributedString? {
        
        do {
            let htmlCSSString = "<style>" +
                "a { text-decoration: none }" +
                "body" +
                "{" +
                "font-size: \(font.pointSize)px;" +
                "color: #\(textColor.toHex());" +
                "font-family: \(font.fontName);" +
                "}" +
                "a:link" +
                "{" +
                "color: #\(linkColor.toHex());" +
                "}" +
                "a:a:visited" +
                "{" +
                "color: #\(linkVisitedColor.toHex());" +
                "}" +
                "</style>" +
                "<body>\(self)</body>"
            
            guard let data = htmlCSSString.data(using: String.Encoding.utf8) else {
                return nil
            }
            
            return try NSAttributedString(data: data,
                                          options: [.documentType: NSAttributedString.DocumentType.html,
                                                    .characterEncoding: String.Encoding.utf8.rawValue],
                                          documentAttributes: nil)
        } catch {
            print("error: ", error)
            return nil
        }
    }
}
