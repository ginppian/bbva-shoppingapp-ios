//
//  CustomTextField.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 26/6/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation
import Material

class CustomTextField: TextField {

    var verticalOffset: CGFloat = 10.0
    var paddingLeft: CGFloat = 0
    var paddingRight: CGFloat = 0

    override func textRect(forBounds bounds: CGRect) -> CGRect {
        return CGRect(x: bounds.origin.x + paddingLeft, y: bounds.origin.y, width: bounds.size.width - paddingLeft - paddingRight, height: bounds.size.height)
    }

    override func editingRect(forBounds bounds: CGRect) -> CGRect {
        return textRect(forBounds: bounds)
    }

    func configure() {
        self.placeholderNormalColor = .white
        self.placeholderActiveColor = UIColor.white.withAlphaComponent(0.6)
        self.placeholderVerticalOffset = self.frame.size.height - verticalOffset
        self.contentVerticalAlignment = UIControlContentVerticalAlignment.bottom
        self.isDividerHidden = true
        self.font = Tools.setFontBook(size: 16)
    }

    func didEndEditing() {
        if self.text == "" {
            self.placeholderNormalColor = .white
        } else {
            self.placeholderNormalColor = UIColor.white.withAlphaComponent(0.6)
        }
    }

    @discardableResult override func becomeFirstResponder() -> Bool {
        return super.becomeFirstResponder()
    }

}
