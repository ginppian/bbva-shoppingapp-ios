//
//  Colors.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 14/3/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit

class Colors: NSObject {

    static func hexStringToUIColor (hex: String, alpha: Float) -> UIColor {
        var cString: String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()

        if cString.hasPrefix("#") {
            cString.remove(at: cString.startIndex)
        }

        if (cString.count) != 6 {
            return UIColor.gray
        }

        var rgbValue: UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)

        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(alpha)
        )
    }

}

extension UIColor {

    //hex: 1973b8
    class var MEDIUMBLUE: UIColor {
        return UIColor(red: 25.0 / 255.0, green: 115.0 / 255.0, blue: 184.0 / 255.0, alpha: 1.0)
    }

    //hex: 2DCCCD
    class var AQUA: UIColor {
        return UIColor(red: 45.0 / 255.0, green: 204.0 / 255.0, blue: 205.0 / 255.0, alpha: 1.0)
    }

    //hex: 004481
    class var COREBLUE: UIColor {
        return UIColor(red: 0.0, green: 68.0 / 255.0, blue: 129.0 / 255.0, alpha: 1.0)
    }

    //hex: 072146
    class var NAVY: UIColor {
        return UIColor(red: 7.0 / 255.0, green: 33.0 / 255.0, blue: 70.0 / 255.0, alpha: 1.0)
    }

    //hex: 121212
    class var BBVA600: UIColor {
        return UIColor(white: 18.0 / 255.0, alpha: 1.0)
    }

    //hex: 043263
    class var DARKCOREBLUE: UIColor {
        return UIColor(red: 4.0 / 255.0, green: 50.0 / 255.0, blue: 99.0 / 255.0, alpha: 1.0)
    }

    //hex: 1464A5
    class var DARKMEDIUMBLUE: UIColor {
        return UIColor(red: 20.0 / 255.0, green: 100.0 / 255.0, blue: 165.0 / 255.0, alpha: 1.0)
    }

    //hex: 49A5E6
    class var BBVADARKLIGHTBLUE: UIColor {
        return UIColor(red: 73.0 / 255.0, green: 165.0 / 255.0, blue: 230.0 / 255.0, alpha: 1.0)
    }

    //hex: e77d8e
    class var BBVALIGHTRED: UIColor {
        return UIColor(red: 231.0 / 255.0, green: 125.0 / 255.0, blue: 142.0 / 255.0, alpha: 1.0)
    }

    //hex: 5bbeff
    class var BBVALIGHTBLUE: UIColor {
        return UIColor(red: 91.0 / 255.0, green: 190.0 / 255.0, blue: 255.0 / 255.0, alpha: 1.0)
    }

    //hex: fcdfdf
    class var BBVAWHITECORAL50: UIColor {
        return UIColor(red: 252.0 / 255.0, green: 223.0 / 255.0, blue: 223.0 / 255.0, alpha: 0.5)
    }

    //hex: b92a45
    class var BBVADARKRED: UIColor {
        return UIColor(red: 185.0 / 255.0, green: 42.0 / 255.0, blue: 69.0 / 255.0, alpha: 1.0)
    }

    //hex: f4c3ca
    class var BBVAWHITERED: UIColor {
        return UIColor(red: 244.0 / 255.0, green: 195.0 / 255.0, blue: 202.0 / 255.0, alpha: 1.0)
    }

    class var BBVAWHITERED30: UIColor {
        return UIColor(red: 244.0 / 255.0, green: 195.0 / 255.0, blue: 202.0 / 255.0, alpha: 0.3)
    }

    //hex: f59799
    class var BBVALIGHTCORAL: UIColor {
        return UIColor(red: 245.0 / 255.0, green: 151.0 / 255.0, blue: 153.0 / 255.0, alpha: 1.0)
    }

    //hex: f59799
    class var BBVACORAL: UIColor {
        return UIColor(red: 243.0 / 255.0, green: 94.0 / 255.0, blue: 97.0 / 255.0, alpha: 1.0)
    }

    //hex: BDBDBD
    class var GRAYTABBAR: UIColor {
        return UIColor(red: 189.0 / 255.0, green: 189.0 / 255.0, blue: 189.0 / 255.0, alpha: 1.0)
    }

    //hex: D4EDFC
    class var WHITELIGHTBLUE: UIColor {
        return UIColor(red: 212.0 / 255.0, green: 237.0 / 255.0, blue: 252.0 / 255.0, alpha: 1.0)
    }

    //hex: 007878
    class var GREEN120: UIColor {
        return UIColor(red: 0.0 / 255.0, green: 120.0 / 255.0, blue: 120.0 / 255.0, alpha: 1.0)
    }

    class var GREEN132: UIColor {
        return UIColor(red: 2.0 / 255.0, green: 132.0 / 255.0, blue: 132.0 / 255.0, alpha: 1.0)
    }

    //hex: FFFFFF
    class var BBVAWHITE: UIColor {
        return UIColor(white: 1.0, alpha: 1.0)
    }

    class var BBVAWHITE30: UIColor {
        return UIColor(white: 1.0, alpha: 0.3)
    }

    //hex: f4f4f4
    class var BBVA100: UIColor {
        return UIColor(white: 244.0 / 255.0, alpha: 1.0)
    }

    //hex: d3d3d3
    class var BBVA300: UIColor {
        return UIColor(white: 211.0 / 255.0, alpha: 1.0)
    }

    //hex: bdbdbd
    class var BBVA400: UIColor {
        return UIColor(white: 189.0 / 255.0, alpha: 1.0)
    }

    //hex: 666666
    class var BBVA500: UIColor {
        return UIColor(white: 102.0 / 255.0, alpha: 1.0)
    }

    //hex: 8E7022
    class var DARKGOLD: UIColor {
        return UIColor(red: 142.0 / 255.0, green: 112.0 / 255.0, blue: 34.0 / 255.0, alpha: 1.0)
    }

    //hex: E9E9E9
    class var BBVA200: UIColor {
        return UIColor(white: 233.0 / 255.0, alpha: 1.0)
    }

    //hex: 028484
    class var DARKAQUA: UIColor {
        return UIColor(red: 2.0 / 255.0, green: 132.0 / 255.0, blue: 132.0 / 255.0, alpha: 1.0)
    }
    //hex: 277A3E
    class var BBVADARKGREEN: UIColor {
        return UIColor(red: 39.0 / 255.0, green: 122.0 / 255.0, blue: 62.0 / 255.0, alpha: 1.0)
    }
    //hex D4EDFC
    class var BBVAWHITELIGHTBLUE: UIColor {
        return UIColor(red: 212.0 / 255.0, green: 237.0 / 255.0, blue: 252.0 / 255.0, alpha: 1.0)
    }

    class var BBVAPromotionShimmeringViewColor: UIColor {
        return UIColor(white: 230.0 / 255.0, alpha: 1.0)
    }
    class var BBVAPromotionShimmeringView1Color: UIColor {
        return UIColor(white: 211.0 / 255.0, alpha: 1.0)
    }
    class var BBVACarouselShimmeringViewColor: UIColor {
        return UIColor(white: 233.0 / 255.0, alpha: 1.0)
    }

    class var BBVADARKPINK: UIColor {
        return UIColor(red: 173.0 / 255.0, green: 83.0 / 255.0, blue: 161.0 / 255.0, alpha: 1.0)
    }

    class var BBVAMEDIUMYELLOW: UIColor {
        return UIColor(red: 196.0 / 255.0, green: 151.0 / 255.0, blue: 53.0 / 255.0, alpha: 1.0)
    }

    class var BBVAGREEN56: UIColor {
        return UIColor(red: 56.0 / 255.0, green: 141.0 / 255.0, blue: 79.0 / 255.0, alpha: 1.0)
    }

    class var BBVAPINK212: UIColor {
        return UIColor(red: 212.0 / 255.0, green: 75.0 / 255.0, blue: 80.0 / 255.0, alpha: 1.0)
    }

    class var BROWNISHGREY: UIColor {
        return UIColor(red: 102.0 / 255.0, green: 102.0 / 255.0, blue: 102.0 / 255.0, alpha: 1.0)
    }

    class var BBVAMEDIUMAQUA: UIColor {
        return UIColor(red: 2.0 / 255.0, green: 165.0 / 255.0, blue: 165.0 / 255.0, alpha: 1.0)
    }

    class var SHADOWGREY: UIColor {
        return UIColor(red: 18.0 / 255.0, green: 18.0 / 255.0, blue: 18.0 / 255.0, alpha: 0.1)
    }

    func toHex() -> String {

        let components = cgColor.components

        let r = Float(components[0])
        let g = Float(components[1])
        let b = Float(components[2])
        let a = components.count >= 4 ? Float(components[3]) : Float(1.0)

        return String(format: "%02lX%02lX%02lX%02lX", lroundf(r * 255), lroundf(g * 255), lroundf(b * 255), lroundf(a * 255))

    }
}
