//
//  SSLPinningManager.swift
//  shoppingapp
//
//  Created by Javier Pino on 24/9/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import BBVA_Network

struct SSLPinningManager {
    
    static func configureSSLPinning() {
        
        let isSSLPinningEnabled = Configuration.isSSLPinningEnabled()
        let allCertificatesAccepted = Configuration.areAllCertificatesAcceptedForSSLPinning()
        let certificates = Settings.SSLPinning.certificates
        
        let sslPinning: Certificate = allCertificatesAccepted ? CertificateAcceptAll() : CertificateSSLPinning()
        sslPinning.setCertificates(["ssl_pinning_enabled": isSSLPinningEnabled,
                                    "cert_array_name": certificates])
        sslPinning.configureCertificate()
        CertificateManager.sharedClient().currentCertificate = sslPinning
    }
    
}
