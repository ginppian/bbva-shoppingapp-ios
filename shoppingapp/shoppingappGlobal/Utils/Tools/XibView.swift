//
//  XibView.swift
//  shoppingapp
//
//  Created by jesus.martinez on 28/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit

class XibView: UIView {

    // MARK: - Initializers

    override init(frame: CGRect) {

        super.init(frame: frame)
        setupView()
    }

    required init?(coder aDecoder: NSCoder) {

        super.init(coder: aDecoder)
        setupView()
    }

    private func setupView() {

        let view = viewFromNibForClass()
        view.frame = bounds
        view.backgroundColor = .clear

        configureView()

        addSubview(view)
    }

    func viewFromNibForClass() -> UIView {

        let bundle = Bundle(for: type(of: self))
        let nib = UINib(nibName: String(describing: type(of: self)), bundle: bundle)
        let view = nib.instantiate(withOwner: self, options: nil).first as! UIView

        return view
    }

    func configureView() {

    }

}
