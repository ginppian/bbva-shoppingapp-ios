//
//  PreferencesManager.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 15/01/2018.
//  Copyright © 2018 BBVA. All rights reserved.
//

class PreferencesManager: NSObject {

    // MARK: Singleton

    static var instance: PreferencesManager?

    class func sharedInstance() -> PreferencesManager {
        if instance != nil {
            return instance!
        } else {
            instance = PreferencesManager()
            return instance!
        }
    }

    @discardableResult
    func saveValueWithPreferencesKey(forValue value: AnyObject, withKey key: PreferencesManagerKeys) -> Bool {
        
        standard().set(value, forKey: key.rawValue)
        return sync()
    }

    func getValueWithPreferencesKey(forKey key: PreferencesManagerKeys) -> AnyObject? {
        
        return standard().object(forKey: key.rawValue) as AnyObject?
    }

    @discardableResult
    func deleteValueWithPreferencesKey(forKey key: PreferencesManagerKeys) -> Bool {
        
        standard().set(nil, forKey: key.rawValue)
        return sync()
    }

    @discardableResult
    func saveValue(forValue value: AnyObject, withKey key: String) -> Bool {
        
        standard().set(value, forKey: key)
        return sync()
    }

    func getValue(forKey key: String) -> AnyObject? {
        
        return standard().object(forKey: key) as AnyObject?
    }

    func deleteValue(forKey key: String) {
        
        standard().set(nil, forKey: key)
    }

    // MARK: Helpers

    func standard() -> UserDefaults {
        
        return UserDefaults.standard
    }

    func sync() -> Bool {
        
        return standard().synchronize()
    }

    // MARK: ResetData
    func resetPreferencesData() {
        
        deleteValueWithPreferencesKey(forKey: PreferencesManagerKeys.kClearCopyPan)
        deleteValueWithPreferencesKey(forKey: PreferencesManagerKeys.kLoginNumber)
        deleteValueWithPreferencesKey(forKey: PreferencesManagerKeys.kSwitchCardOn)
        deleteValueWithPreferencesKey(forKey: PreferencesManagerKeys.kSwitchCardOff)
        deleteValueWithPreferencesKey(forKey: PreferencesManagerKeys.kDontSuggestFavoriteSelection)
        deleteValueWithPreferencesKey(forKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        deleteValueWithPreferencesKey(forKey: PreferencesManagerKeys.kWaitToAccessToSuggestFavoriteSelection)
        deleteValueWithPreferencesKey(forKey: PreferencesManagerKeys.kRegisterDeviceToken)
        deleteValueWithPreferencesKey(forKey: PreferencesManagerKeys.kLimitsHelpDebitCardShowed)
        deleteValueWithPreferencesKey(forKey: PreferencesManagerKeys.kLimitsHelpCreditCardShowed)

        deleteValueWithPreferencesKey(forKey: PreferencesManagerKeys.kAppRated)
        deleteValueWithPreferencesKey(forKey: PreferencesManagerKeys.kUserSkipRateApp)
        deleteValueWithPreferencesKey(forKey: PreferencesManagerKeys.kNumberLoginsRateApp)
    }
    
    func isFirstRun() -> Bool {

        if PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kFirstRun) as? Bool == nil {
            PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: false as AnyObject, withKey: PreferencesManagerKeys.kFirstRun)
            return true
        } else {
            return false
        }
    }
    
    func isFirstLogin() -> Bool {
        
        let loginNumberValue = PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kLoginNumber) as? NSNumber
        guard let loginNumber = loginNumberValue else {
            return true
        }
        return loginNumber == 1
    }
    
    func isFirstTimeDebitShowLimitsHelp() -> Bool {
        
        if PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kLimitsHelpDebitCardShowed) as? Bool == nil {
            PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: true as AnyObject, withKey: PreferencesManagerKeys.kLimitsHelpDebitCardShowed)
            return true
        } else {
            return false
        }
    }
    
    func isFirstTimeCreditShowLimitsHelp() -> Bool {
        
        if PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kLimitsHelpCreditCardShowed) as? Bool == nil {
            PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: true as AnyObject, withKey: PreferencesManagerKeys.kLimitsHelpCreditCardShowed)
            return true
        } else {
            return false
        }
    }
    func getDontSuggestFavoriteSelection() -> Bool {
        
        let value = PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kDontSuggestFavoriteSelection) as? Bool
        
        return value ?? false
    }
    
    func getPromotionsSectionAccessCounter() -> Int {
        
        let accessCounter = PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter) as? NSNumber
        
        return accessCounter?.intValue ?? 0
    }
    
    func incrementPromotionsSectionAccessCounter() {
        
        let accessCounter = PreferencesManager.sharedInstance().getPromotionsSectionAccessCounter()
        let incrementedValue = NSNumber(value: accessCounter + 1)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: incrementedValue, withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
    }
    
    func isFirstTimeTuringCardOn() -> Bool {
        
        if PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kSwitchCardOn) as? Bool == nil {
            return true
        } else {
            return false
        }
    }
    
    func isFirstTimeTurningCardOff() -> Bool {
        
        if PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kSwitchCardOff) as? Bool == nil {
            return true
        } else {
            return false
        }
    }

}

enum PreferencesManagerKeys: String {
    
    case kUser = "kUser"
    case kClearCopyPan = "clearClipboardTimeout"
    case kIndexToShowCards = "indexToShowCards"
    case kIndexToShowRewards = "indexToShowRewards"
    case kIndexToShowPromotionDetail = "indexToShowPromotionDetail"
    case kSavedHelpVersion = "savedHelpVersion"
    case kFirstRun = "firstRun"
    case kLoginNumber = "loginNumber"
    case kSavedDidacticAreaVersion = "savedDidacticAreaVersion"
    case kIndexDidacticArea = "indexDidacticArea"
    case kIndexDefaultCategoryDidacticArea = "indexDefaultCategoryDidacticArea"
    case kSaveIndexsOfCategories = "saveIndexsOfCategories"
    case kShowedTrendingPromotionId = "showedTrendingPromotionId"
    case kDontSuggestFavoriteSelection = "kDontSuggestFavoriteSelection"
    case kPromotionsSectionAccessCounter = "kPromotionsSectionAccessCounter"
    case kUserCloseNotificationsAdvise = "kUserCloseNotificationsAdvise"
    case kApnsToken = "kApnsToken"
    case kRegisterDeviceToken = "kRegisterDeviceToken"
    case kSwitchCardOn = "kSwitchCardOn"
    case kSwitchCardOff = "kSwitchCardOff"
    case kUserNotificationsSet = "kUserNotificationsSet"
    case kAppRated = "appRated"
    case kUserSkipRateApp = "kUserSkipRateApp"
    case kVersionOfLastRun = "kVersionOfLastRun"
    case kNumberLoginsRateApp = "kNumberLoginsRateApp"

    //Configuration
    case kLastTimeUpdatePublicConfiguration = "kLastTimeUpdatePublicConfiguration"
    case kConfigBalancesAvailable = "configBalancesAvailable"
    case kConfigNotificationsAvailable = "configNotificationsAvailable"
    case kConfigVibrationEnabled = "configVibrationEnabled"
    case kConfigTicketPromotionsEnabled = "configTicketPromotionsEnabled"
    case kWaitToAccessToSuggestFavoriteSelection = "kWaitToAccessToSuggestFavoriteSelection"
    
    // Welcome
    case kWelcomeShowed = "welcome-showed"
    
    // Limits-help
    case kLimitsHelpDebitCardShowed = "limitsHelpDebitCard-showed"
    case kLimitsHelpCreditCardShowed = "limitsHelpCreditCard-showed"

}
