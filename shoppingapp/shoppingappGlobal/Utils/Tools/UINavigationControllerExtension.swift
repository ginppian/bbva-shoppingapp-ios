//
//  UINavigationControllerExtension.swift
//  shoppingapp
//
//  Created by jesus.martinez on 28/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

extension UINavigationController {

    func completeHeight() -> CGFloat {

        return navigationBar.frame.size.height + UIApplication.shared.statusBarFrame.height

    }

    func topNavigationController() -> UINavigationController {

        guard self.presentedViewController != nil else {
            return self
        }

        if let navigation = self.presentedViewController as? UINavigationController {
            return navigation.topNavigationController()
        } else if let tab = self.presentedViewController as? UITabBarController {

            if let selectedTab = tab.selectedViewController {

                if let navigation = selectedTab as? UINavigationController {
                    return navigation.topNavigationController()
                }
            }

        }
        return self

    }

}
