//
//  CustomAttributedText.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 11/01/2018.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class CustomAttributedText {

    static func attributedBoldText(forFullText fullText: String, withBoldText boldText: String, withFont font: UIFont, withBoldFont boldFont: UIFont, withAlignment alignment: NSTextAlignment, andForegroundColor foregroundColor: UIColor) -> NSMutableAttributedString {

        let attributedString = NSMutableAttributedString(string: fullText, attributes: [
            NSAttributedStringKey.font: font,
            NSAttributedStringKey.foregroundColor: foregroundColor
            ])

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 4
        paragraphStyle.alignment = alignment

        attributedString.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: attributedString.length))

        if let boldRange = fullText.range(of: boldText) {

            let nsRange = fullText.converToNSRange(fromRange: boldRange)

            attributedString.addAttribute(NSAttributedStringKey.font, value: boldFont, range: nsRange)

        }

        return attributedString
    }

    static func attributedBoldText(forFullText fullText: String, withBoldText boldText: String, withFont font: UIFont, withBoldFont boldFont: UIFont, andForegroundColor foregroundColor: UIColor) -> NSMutableAttributedString {

        let attributedString = NSMutableAttributedString(string: fullText, attributes: [
            NSAttributedStringKey.font: font,
            NSAttributedStringKey.foregroundColor: foregroundColor
            ])

        if let boldRange = fullText.range(of: boldText) {

            let nsRange = fullText.converToNSRange(fromRange: boldRange)

            attributedString.addAttribute(NSAttributedStringKey.font, value: boldFont, range: nsRange)

        }

        return attributedString
    }
}

extension String {
    func converToNSRange(fromRange range: Range<String.Index>) -> NSRange {
        let startPos = self.distance(from: self.startIndex, to: range.lowerBound)
        let endPos = self.distance(from: self.startIndex, to: range.upperBound)
        return NSRange(location: startPos, length: endPos - startPos)
    }
}
