//
//  FaceOutInWithTransitionViewAnimationController.swift
//  shoppingapp
//
//  Created by jesus.martinez on 24/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol TransitionerAnimationProtocol: class {
    func transitionFrame() -> CGRect?
    func finishTransition()
}

protocol TransitionerOutAnimationProtocol: TransitionerAnimationProtocol {

    var transitionImageView: UIImageView? { get set }

}

protocol TransitionerInAnimationProtocol: TransitionerAnimationProtocol {
    var durationToTransition: Double { get }

    func doAnimationWhileTransition(withDuration duration: Double)
}

extension TransitionerOutAnimationProtocol {
    func finishTransition() {
        transitionImageView = nil
    }
}

class FaceOutInWithTransitionViewAnimationController: NSObject, UIViewControllerAnimatedTransitioning {

    var duration = 0.2
    var defaultTransitionViewDuration = 0.7
    var fromTransitionImageView: UIImageView
    var fromTransitionFrame: CGRect

    init(transitionImageView: UIImageView, fromTransitionFrame: CGRect) {
        self.fromTransitionImageView = transitionImageView
        self.fromTransitionFrame = fromTransitionFrame
    }

    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return duration
    }

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {

        let fromViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.from)
        let toViewController = transitionContext.viewController(forKey: UITransitionContextViewControllerKey.to)!
        transitionContext.containerView.addSubview(toViewController.view)

        let outTransitioner = ((fromViewController as? UINavigationController)?.viewControllers.first ?? fromViewController) as? TransitionerOutAnimationProtocol
        let inTransitioner = ((toViewController as? UINavigationController)?.viewControllers.first ?? toViewController) as? TransitionerInAnimationProtocol

        let transitionImageView = UIImageView(frame: fromTransitionFrame)
        transitionImageView.image = fromTransitionImageView.image
        transitionImageView.clipsToBounds = true

        var alphaLayer: CALayer?

        if fromTransitionImageView.alpha < 1 {
            let layer = CALayer()
            layer.frame = CGRect(x: 0, y: 0, width: fromTransitionFrame.size.width, height: fromTransitionFrame.size.height)
            layer.backgroundColor = UIColor.white.cgColor
            layer.opacity = Float(1 - fromTransitionImageView.alpha)
            layer.masksToBounds = true
            transitionImageView.layer.insertSublayer(layer, at: 0)
            alphaLayer = layer
        }

        transitionContext.containerView.addSubview(transitionImageView)

        let viewToFaceIn = (toViewController as? UINavigationController)?.viewControllers.first?.view ?? toViewController.view
        viewToFaceIn?.alpha = 0.0

        UIView.animate(withDuration: duration, delay: 0, options: .curveLinear, animations: {

            viewToFaceIn?.alpha = 1.0
            fromViewController?.view.alpha = 0.0

        }, completion: {  _ in

            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)

        })

        let transitionDuration = inTransitioner?.durationToTransition ?? defaultTransitionViewDuration

        UIApplication.shared.beginIgnoringInteractionEvents()

        inTransitioner?.doAnimationWhileTransition(withDuration: transitionDuration)

        UIView.animate(withDuration: transitionDuration, delay: 0, options: .curveEaseIn, animations: {

            if let inTransitionFrame = inTransitioner?.transitionFrame() {

                transitionImageView.frame = inTransitionFrame
                alphaLayer?.frame = CGRect(x: 0, y: 0, width: inTransitionFrame.size.width, height: inTransitionFrame.size.height)

            }

        }, completion: { _  in

            inTransitioner?.finishTransition()
            outTransitioner?.finishTransition()
            transitionImageView.removeFromSuperview()
            fromViewController?.view.alpha = 1.0
            UIApplication.shared.endIgnoringInteractionEvents()

        })

    }

}
