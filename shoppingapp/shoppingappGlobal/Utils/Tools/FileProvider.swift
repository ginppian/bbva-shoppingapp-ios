//
//  FileProvider.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 17/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class FileProvider {

    // MARK: Paths

    private func workDirectory() -> String {

        let paths = NSSearchPathForDirectoriesInDomains(.applicationSupportDirectory,
                                                        .userDomainMask, true)
        return paths.first!
    }

    private func filePath(forFileName fileName: String) -> String {
        return workDirectory().appendingFormat( "/\(fileName)")
    }

    private func resourcePath(forResource resource: String, ofType type: String) -> String? {

        return Bundle.main.path(forResource: resource, ofType: type)
    }

    // MARK: URL's

    private func workURL() -> URL {

        let workURL = FileManager.default.urls(for: .applicationSupportDirectory,
                                 in: .userDomainMask)
        return workURL.first!
    }

    private func fileURL(forFileName fileName: String) -> URL {
        return workURL().appendingPathComponent("\(fileName)")
    }

    // MARK: File exists

    private func fileExists(atPath path: String) -> Bool {

        let fileManager = FileManager.default
        return fileManager.fileExists(atPath: path)
    }

    // MARK: Read files

    func readFileAsData(forFilename filename: String) -> NSData? {

        let path = filePath(forFileName: filename)
        var data = NSData()

        if fileExists(atPath: path) {
            do {
                try data = NSData(contentsOfFile: path)

            } catch  let error as NSError {
                
                DLog(message: "ERROR <readFileAsData>: \(error)")
                
                return nil
            }
        } else {
            
            return nil
        }

        return data
    }
    
    func readResourceFromBundleAsString(forResource resource: String, ofType type: String) -> String {

        var contentsOfFile = ""

        if let filePath = resourcePath(forResource: resource, ofType: type) {

            do {
                contentsOfFile = try String(contentsOfFile: filePath)

            } catch let error as NSError {
                DLog(message: "ERROR <readResourceFromBundleAsString>: \(error)")
            }
        }

        return contentsOfFile
    }
    
    func readFileNameAsString(forFileName fileName: String) -> String {
        
        var contentsOfFile = ""
        let path = filePath(forFileName: fileName)
        
        if fileExists(atPath: path) {
            
            do {
                
                contentsOfFile = try String(contentsOfFile: path, encoding: String.Encoding.utf8 )
            } catch let error as NSError {
                DLog(message: "ERROR <readResourceFromBundleAsString>: \(error)")
            }
        }
        
        return contentsOfFile
    }
    
    // MARK: Write files

    func writeString(_ data: String, fileName: String) {

        let fileURL = self.fileURL(forFileName: fileName)

        do {

            try data.write(to: fileURL, atomically: true, encoding: String.Encoding.utf8)
        } catch let error as NSError {

            DLog(message: "Error! write file: \(error)")
        }
    }

}
