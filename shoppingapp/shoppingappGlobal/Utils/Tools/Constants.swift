//
//  Constants.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 28/3/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation

struct ConstantsHTTPCodes {

    static let GENERIC_ERROR = 999
    static let INCORRECT_USER_OR_PASSWORD_409 = 409   // 409
    static let STATUS_403 = 403
    static let STATUS_OK = 200
    static let NO_CONTENT = 204
    static let MORE_PAGES = 206
    static let I_AM_A_TEAPOT = 418
    static let unauthorized = 401
    static let otpErrorCode = 70
}

struct ConstantsURLs {

    static let HELP_CONFIG_FILE_URL = "https://bbva-files.s3.amazonaws.com/wallet/help/shopping_app/context_help/context_help.json"
    static let DIDACTIC_AREA_FILE_URL = "https://bbva-files.s3.amazonaws.com/wallet/help/shopping_app/didactic/didactic.json"

}
struct ConstantsLanguage {

    static let DEFAULT_LANGUAGE = Bundle.main.preferredLocalizations.first!

}

struct ImageScaleSizes {

    static let ONE_X_INDICATOR = "@1x"
    static let TWO_X_INDICATOR = "@2x"
    static let THREE_X_INDICATOR = "@3x"

}
public enum ImagesScales {

    static var imageScale: String {
        if UIScreen.main.scale == 2 {
           return ImageScaleSizes.TWO_X_INDICATOR
        } else if UIScreen.main.scale == 3 {
            return ImageScaleSizes.THREE_X_INDICATOR
        } else {
            return ImageScaleSizes.ONE_X_INDICATOR
        }
    }
}

struct ConstantsAccessibilities {

    // Navigation Bar

    static let NAVIGATION_BAR_TITLE = "navBarOutputText"
    static let LEFT_BAR_BUTTON = "leftButton"
    static let RIGHT_BAR_BUTTON = "rightBarButton"

    // Left Menu

    static let LEFT_MENU_OPTION_LAYOUT = "itemMenuLayout"
    static let LEFT_MENU_COMPONENT = "menuComponent"
    static let LEFT_LOGIN_IN_MENU_OPTION = "loginItemButton"
    static let LEFT_LOGIN_OUT_MENU_OPTION = "logoutItemButton"

    // Login

    static let LOGIN_HELLO_LABEL = "helloOutputText"
    static let LOGIN_DESCRIPTION_LABEL = "descriptionOutputText"
    static let LOGIN_ACCESS_BUTTON = "accessButton"
    static let LOGIN_SHOW_PROMOTIONS_BUTTON = "showPromotionsButton"
    static let LOGIN_ENTER_BUTTON = "enterButton"
    static let LOGIN_REMEMBER_PASSWORD = "rememberPasswordButton"
    static let LOGIN_REMOVE_TEXT_USER = "removeTextUserButton"
    static let LOGIN_REMOVE_TEXT_PASS = "removeTextPasswordButton"
    static let LOGIN_SHOW_SECURE_TEXT = "showSecureTextButton"
    static let LOGIN_USER_PLACEHOLDER = "userPlaceHolder"
    static let LOGIN_PASS_PLACEHOLDER = "passPlaceHolder"
    static let LOGIN_DOCUMENT_INPUT = "documentTypeInputPicker"
    static let LOGIN_DOCUMENT_PLACEHOLDER = "documentTypePlaceHolder"
    static let LOGIN_DOCUMENT_BUTTON = "documentTypeButton"
    static let LOGIN_USER_TEXTFIELD = "userInputText"
    static let LOGIN_PASS_TEXTFIELD = "passInputText"
    static let LOGIN_LOADER_PASS_TEXTFIELD = "descriptionOutputText"
    static let LOGIN_MENU_BUTTON = "lateralMenuButton"
    static let LOGIN_VIEW = "loginView"
    static let LOGIN_USER_NAME_LABEL = "usernameOutputText"
    static let LOGIN_CHANGE_USER_BUTTON = "changeUserButton"

    // home
    static let HOME_VIEW = "homeView"

    // Error

    static let ERROR_MODAL = "modalError"
    static let ERROR_ACCEPT_BUTTON = "okButton"
    static let ERROR_DESCRIPTION_LABEL = "descriptionErrorText"
    static let ERROR_WARNING_IMAGE = "warningErrorImage"
    static let ERROR_CANCEL_BUTTON = "cancelButton"

    // Picker

    static let PICKER_TABLEVIEW_ID = "pickerTableView"
    static let PICKER_TABLEVIEW_CELL_ID = "pickerTableViewCell"
    static let PICKER_TABLEVIEW_CELL_TITLE_ID = "pickerTableViewCellTitle"

    // Transactions
    static let TRANSACTIONS_LIST_ID = "transactionsList"
    static let NO_CONTENT_TITTLE_TRANSACTION_LIST_ID = "noContentitleOutputText"
    static let NO_CONTENT_DESC_TRANSACTION_LIST_ID = "noContentDescriptionOutputText"
    static let TRANSACTIONS_LIST_TABLE_HEADER_DATE_ID = "dateTableHeaderOutputText"
    static let TRANSACTIONS_LIST_CELL_AND_SECTION_ID = "cellAndSection"
    static let TRANSACTIONS_LIST_TABLE_HEADER_AMOUNT_ID = "amountTableHeaderOutputText"
    static let TRANSACTIONS_LIST_TABLE_HEADER_SECTION_DATE_ID = "dateTableHeaderSectionOutputText"
    static let TRANSACTIONS_LIST_CONCEPT_TEXT_ID = "conceptOutputText"
    static let TRANSACTIONS_LIST_AMOUNT_TEXT_ID = "amountOutputText"
    static let TRANSACTIONS_LIST_PENDING_TEXT_ID = "pendingOutputText"
    static let TRANSACTIONS_LIST_FINANCIABLE_IMAGE_ID = "financiableOutputImage"
    static let TRANSACTIONS_LIST_FINANCIABLE_TEXT_ID = "financiableOutputText"
    static let TRANSACTIONS_LIST_CONCEPT_FILTER_DETAIL_LABEL_ID = "conceptFilterDetailOutputText"
    static let TRANSACTIONS_LIST_DATE_FILTER_DETAIL_LABEL_ID = "dateFilterDetailOutputText"
    static let TRANSACTIONS_LIST_AMOUNT_FILTER_DETAIL_LABEL_ID = "amountFilterDetailOutputText"
    static let TRANSACTIONS_LIST_TRANSACTION_TYPE_FILTER_DETAIL_LABEL_ID = "transactionTypeFilterDetailOutputText"
    static let TRANSACTIONS_LIST_REMOVE_ALL_FILTERS_DETAIL_LABEL_ID = "removeAllFiltersDetailOutputText"
    static let TRANSACTIONS_LIST_CONCEPT_FILTER_DETAIL_CLOSE_BUTTON_ID = "conceptFilterDetailCloseButton"
    static let TRANSACTIONS_LIST_DATE_FILTER_DETAIL_CLOSE_BUTTON_ID = "dateFilterDetailCloseButton"
    static let TRANSACTIONS_LIST_AMOUNT_FILTER_DETAIL_CLOSE_BUTTON_ID = "amountFilterDetailCloseButton"
    static let TRANSACTIONS_LIST_TRANSACTION_TYPE_FILTER_DETAIL_CLOSE_BUTTON_ID = "transactionFilterDetailCloseButton"
    static let TRANSACTIONS_LIST_REMOVE_ALL_FILTERS_DETAIL_CLOSE_BUTTON_ID = "removeAllFiltersDetailCloseButton"

    // Transactions Filter
    static let TRANSACTIONS_FILTER_CONCEPT_ID = "conceptFilterInputText"
    static let TRANSACTIONS_FILTER_REMOVECONCEPT_ID = "conceptFilterRemoveButton"
    static let TRANSACTIONS_FILTER_RESTORE_BUTTON_ID = "restoreButton"
    static let TRANSACTIONS_FILTER_FILTER_BUTTON_ID = "filterButton"
    static let TRANSACTIONS_FILTER_DATETO_LABEL_ID = "dateToInputText"
    static let TRANSACTIONS_FILTER_DATEFROM_LABEL_ID = "dateFromInputText"
    static let TRANSACTIONS_FILTER_AMOUNTTO_INPUT_ID = "amountToInputText"
    static let TRANSACTIONS_FILTER_AMOUNTTO_PLACEHOLDER_INPUT_ID = "amountToPlaceholderOutputText"
    static let TRANSACTIONS_FILTER_AMOUNTFROM_INPUT_ID = "amountFromInputText"
    static let TRANSACTIONS_FILTER_AMOUNTFROM_PLACEHOLDER_INPUT_ID = "amountFromPlaceholderOutputText"
    static let TRANSACTIONS_FILTER_DISPLAYTYPES_BUTTON_ID = "displayTypesButton"

    static let TRANSACTIONS_FILTER_TITLE_CELL_TYPE_TEXT_ID = "typeCellFilterOutputText"
    static let TRANSACTIONS_FILTER_CELL_ID = "cellFilter"
    static let TRANSACTIONS_FILTER_VIEW = "filterView"

    static let TRANSACTIONS_FILTER_DATETO_PICKER_ID = "pickerDateTo"
    static let TRANSACTIONS_FILTER_DATEFROM_PICKER_ID = "pickerDateFrom"
    static let TRANSACTIONS_FILTER_TYPE_TABLEVIEW_ID = "typeTableViewFilter"

    // Transaction detail
    static let TRANSACTION_DETAIL_DID_OPERATION_TITLE = "didOperationTitle"

    static let TRANSACTION_DETAIL_TRANSACTION_NAME = "transactionName"
    static let TRANSACTION_DETAIL_TRANSACTION_AMOUNT = "transactionAmount"
    static let TRANSACTION_DETAIL_OPERATION_DATE = "operationDate"
    static let TRANSACTION_DETAIL_PENDING_TITLE = "pendingTitle"
    static let TRANSACTION_DETAIL_PENDING_DESCRIPTION = "pendingDescription"

    static let TRANSACTION_DETAIL_CARD_TITLE = "cardTitle"
    static let TRANSACTION_DETAIL_CONTRACT_NUMBER = "contractNumber"
    static let TRANSACTION_DETAIL_OPERATION_DATE_TITLE = "operationDateTitle"
    static let TRANSACTION_DETAIL_OPERATION_DATE_VALUE = "operationDateValue"
    static let TRANSACTION_DETAIL_VALUE_DATE_TITLE = "valueDateTitle"
    static let TRANSACTION_DETAIL_VALUE_DATE_VALUE = "valueDateValue"
    static let TRANSACTION_DETAIL_FOLIO_TITLE = "folioTitle"
    static let TRANSACTION_DETAIL_FOLIO_VALUE = "folioValue"

    // Cards
    static let CARDS_STATUS_IMAGE = "statusImage"
    static let CARDS_CARD_IMAGE = "cardImage"
    static let CARD_CELL = "cardGCard"
    static let CARD_TABLE = "cardsTable"
    static let CARD_AVAILABLE_OUTPUTTEXT = "availableOutputText"
    static let CARD_LINK_IMAGE = "linkImage"
    static let CARD_BULLET_IMAGE = "bulletImage"
    static let CARD_PRODUCT_TYPE_OUTPUTTEXT = "productTypeOutputText"
    static let CARD_LAST_DIGITS_OUTPUTTEXT = "productLastDigitOutputText"
    static let CARD_AMOUNT_TEXT_ID = "amountCardOutputText"
    static let CARD_SHOW_TRANSACTIONS_BUTTON = "showMovementsButton"
    static let CARD_TRANSACTION_VIEW_LAST_MOVEMENTS = "lastMovementTitleOutputText"
    static let CARD_TRANSACTION_VIEW_SEARCH_MOVEMENTS = "searchButton"
    static let CARD_LAST_MOVEMENT_EMPTY_TEXT = "lastMovementEmptyOutputText"
    static let CARD_NO_MOVEMENT_IMAGE = "infoImage"
    static let CARD_PAN_OUTPUT_TEXT = "cardPanOutputText"
    static let CARD_INFO_LAST_MOVEMENTS_OUTPUT_TEXT = "infoLastMovementOutputText"
    static let CARD_EXPIRATION_DATE_OUTPUT_TEXT = "expirationDateOutputText"
    static let CARD_HOLDER_NAME_OUTPUT_TEXT = "holderNameOutputText"
    static let CARD_BOTTOM_ACTION_BUTTON = "bottomActionButton"
    static let CARD_COPY_PAN_ACTION_BUTTON = "copyPanOutputText"
    static let CARD_GENERATE_CARD_BUTTON = "generateCardOutputText"
    static let CARDS_CARD_MULTI_TAB = "multiTab"
    static let CARDS_CARD_MULTI_TAB_BUTTON = "multiTabButton"
    static let CARDS_CARD_MULTI_TAB_DIGITAL_LINK = "multiTabDigitalLinkOutputText"
    static let CARDS_CARD_MULTI_TAB_DIGITAL_DESCRIPTION = "multiTabDigitalDescriptionOutputText"
    static let CARDS_CARD_MULTI_TAB_DIGITAL_SMALL_COVER = "multiTabDigitalSmallCoverImage"
    static let CARD_BOTTOM_ACTION_BUTTON_VER_CVV = "bottomActionButtonVerCVV"
    static let CARD_DIRECT_ACCESS_BUTTON = "seeAndSearchButton"

    static let CARD_OPERATIVES_BAR = "actionBarComponent"
    static let CARD_BUTTON_BAR = "barButton"

    static let CARD_CREDIT_CARD_GRANTED_OUTPUTTEXT = "grantedOutputText"
    static let CARD_CREDIT_CARD_DISPOSED_OUTPUTTEXT = "disposedOutputText"

    static let CARD_CREDIT_CARD_GRANTED_COMPONENT = "grantedAmountComponent"
    static let CARD_CREDIT_CARD_DISPOSED_COMPONENT = "disposedAmountComponent"

    static let CARD_CREDIT_CARD_BAR = "barComponent"

    static let CARD_SWITCH_BUTTON = "switchButton"
    static let CARD_ON_OFF_OUTPUT_TEXT = "cardOnOffOutputText"
    static let CARD_ON_OFF_COMPONENT = "cardOnOffComponent"
    static let CARD_ON_OFF_BUTTON = "cardOnOffButton"

    static let TOAST_COMPONENT = "toastComponent"
    static let TOAST_OUTPUT_TEXT = "toastSuccessOutputText"

    static let POINTS_COMPONENT = "pointsComponent"
    static let POINTS_OUTPUTTEXT = "pointsOutputText"
    static let POINTS_DESCRIPTIONTEXT = "descriptionPointsOutputText"

    // Promotions
    static let PROMOTIONS_LIST = "promotionsList"
    static let PROMOTION_HEADER_CELL = "promotionHeader"
    static let PROMOTION_HEADER_TITLE = "categoryOutputText"
    static let PROMOTION_MAIN_PROMOTION_CELL = "mainPromotion"
    static let PROMOTION_SECONDARY_PROMOTION_CELL = "secondaryPromotion"
    static let PROMOTION_PROMOTION_COMMERCE = "commerceOutputText"
    static let PROMOTION_PROMOTION_DESCRIPTION = "promotionOutputText"

    static let OTHER_CATEGORIES_LIST = "otherCategoriesList"
    static let OTHER_CATEGORY_HEADER_TITLE = "otherCategoriesHeaderOutputText"
    static let OTHER_CATEGORY_CELL = "otherCategoryItem"
    static let OTHER_CATEGORY_TITLE = "otherCategoryOutputText"
    static let SHOW_ALL_CATEGORIES_BUTTON = "showAllCategoriesButton"

    static let PROMOTIONS_ERROR_VIEW = "promotionsErrorScreen"
    static let PROMOTIONS_TITLE_ERROR = "promotionsTitleError"
    static let PROMOTIONS_DESCRIPTION_ERROR = "promotionsDescriptionError"

    //Promotions Hot
    static let PROMOTION_HOT_CARUSEL_TABLE = "promotionHotCaruselTable"
    static let PROMOTION_HOT_CELL = "promotionHotCell"
    static let PROMOTION_HOT_COMPONENT = "promotionHotComponent"
    static let PROMOTION_HOT_HEADER_TITLE = "promotionHotHeaderOutputText"
    static let SHOW_ALL_PROMOTIONS_HOT = "showAllPromotionsHotButton"
    static let PROMOTION_PROMOTIONS_HOT_BENEFIT = "promotionHotBenefitOutputText"
    static let PROMOTION_PROMOTIONS_HOT_NAME = "promotionHotOutputText"
    static let PROMOTION_PROMOTIONS_HOT_COMMERCE = "promotionHotCommerceOutputText"

    // Limits
    static let LIMITS_TABLE = "limitsList"
    static let LIMIT_CELL = "limitCell"
    static let LIMIT_TITLE = "limitTitle"
    static let EDIT_LIMIT_BUTTON = "editLimitButton"
    static let LIMIT_CONSUMED_AMOUNT = "consumedAmount"
    static let LIMIT_CONSUMED_TITLE = "consumedTitle"
    static let LIMIT_CURRENT_LIMIT_AMOUNT = "currentLimitAmount"
    static let LIMIT_CURRENT_LIMIT_TITLE = "currentLimitTitle"
    static let LIMIT_PROGRESS_LIMIT_BAR = "progressLimitBar"
    static let LIMIT_MINIMUM_LIMIT_AMOUNT = "minimumLimitAmount"
    static let LIMIT_MAXIMUM_LIMIT_AMOUNT = "maximumLimitAmount"

    //Points/Cards
    static let REWARDS_TABLE = "rewardsTable"
    static let REWARDS_CELL = "rewardsCell"
    static let REWARDS_CONTAINER_CELL = "rewardsContainer"
    static let REWARDS_IMAGE_CELL = "cardRewardImage"
    static let REWARDS_ALIAS_CELL = "aliasOutputText"
    static let REWARDS_PAN_FOUR_CELL = "panFourOutputText"
    static let REWARDS_EMPTY_POINTS_CELL = "emptyPointsOutputText"
    static let REWARDS_POINTS_CELL = "pointsOutputText"
    static let REWARDS_BACKGROUND_IMAGE_CELL = "rewardBackgroundImage"
    static let REWARDS_MORE_INFO_BUTTON = "moreInfoButton"

}

struct ConstantsImages {

    struct Login {
        static let background = "Background"
        static let user_saved_image = "UserSave"
        static let welcome_image = "welcomeImage"
        static let hide_icon = "2_hide_icon"
    }

    struct Home {
        static let wallet_icon = "1_wallet_icon"
    }

    struct SideMenu {
        static let get_out = "2_get_out"
        static let background_left_menu_logged = "BgLeftMenu"
        static let background_left_menu_no_session = "BgLeftMenuNoSession"
    }

    struct Common {
        static let help_icon_on_off = "4_help_icon"
        static let telephone_bbva_line = "4_telephone_icon"
        static let menu_icon_image = "2_009_menu_icon"
        static let visualize_icon_image = "2_visualize_icon"
        static let checkmark_success = "2_024_checkmark_icon"
        static let unfold_icon = "2_unfold_icon"
        static let search_icon = "3_search_icon"
        static let clock_icon = "4_clock_icon"
        static let bullet = "bullet"
        static let forward_icon = "2_forward_icon"
        static let add_icon = "2_040_add_icon"
        static let close_icon = "2_close_icon"
        static let return_icon = "2_return_icon"
        static let alert_icon = "4_alert_icon"
        static let info_icon = "4_info_icon"
        static let technology = "13_technology_icon"
        static let dollar_icon = "1_dollar_icon"
        static let calendar_icon = "3_calendar_icon"
        static let city_promo = "ilu_city_promo"
        static let deser_ovni = "ilu_desert_ovni"
        static let desert_rocket = "ilu_desert_rocket"
        static let arrow_down = "asset_arrow_down"
        static let arrow_up = "asset_arrow_up"
        static let edit_icon = "3_015_edit_icon"
        static let help_icon = "ilu_help_mobile"
        static let bullet_icon = "0_bullet_icon"
        static let play_icon = "play_icon"
        static let email = "4_email_icon"
        static let expand_icon = "2_021_expand_icon"
        static let shopping_icon = "5_010_shopping_icon"
        static let substract_icon = "2_041_substract_icon"
    }

    struct Skeleton {
        static let lines = "skeleton_lines"
        static let lines_reverse = "skeleton_lines_reves"
    }

    struct Card {
        static let activation_card_icon = "1_activation_card_icon"
        static let link_card_icon = "2_link_icon"
        static let creditcard_icon = "1_creditcard_icon"
        static let transfer_icon = "transfer_icon"

        static let card_activate = "card_activate"
        static let card_blocked = "card_blocked"
        static let card_delivery = "card_delivery"
        static let card_off = "card_off"
        static let blank_card =  "Card_blank_fake"
        static let blank_sticker =  "Card_sticker_fake"
        static let defaultBackCard = "defaultBackCard"
        
        static let on_off_switch_off = "switch_off"
        static let on_off_switch_on = "switch_on"
        static let backgroundPoints_image = "ilu_points_card_landing"
        static let limits = "1_035_limits_icon"
        static let blockcard = "1_008_blockcard_icon"

        static let card_bloqued_state = "card_state_blocked"
        static let card_delivery_state = "card_state_delivery"
        static let card_inoperative_state = "card_state_activate"
        static let card_off_state = "card_state_off"
        static let card_state_info = "card_state_info"

        static let card_bloqued_state_small = "card_state_blocked_small"
        static let card_delivery_state_small = "card_state_delivery_small"
        static let card_inoperative_state_small = "card_state_activate_small"
        static let card_off_state_small = "card_state_off_small"
        static let card_state_info_small = "card_state_info_small"

        static let ilu_turn_on_off = "ilu_turn_on_off"
        static let ilu_limits_help = "ilu_limits_help"
        
        static let creditCardRequestIcon = "creditCardRequest"

    }

    struct Promotions {
        static let default_promotion_image = "img_default_promo"
        static let default_promotion_list_image = "img_default_list_promo"

        static let menu_promotion_icon = "menu_promotion_icon"
        static let place_icon = "2_028_place_icon"
        static let tab_promotions = "5_010_shopping_icon"
        static let shop_icon = "5_shop_icon"

        static let activities = "1_activities_icon"
        static let auto = "2_auto_icon"
        static let beautyAndFragances = "3_beautyAndFragances_icon"
        static let diet = "4_diet_icon"
        static let foodAndGourmet = "5_foodAndGourmet_icon"
        static let hobbies = "6_hobbies_icon"
        static let home = "7_home_icon"
        static let learning = "8_learning_icon"
        static let rent = "9_rent_icon"
        static let restaurant = "10_restaurants_icon"
        static let shopping = "11_shopping_icon"
        static let shows = "12_shows_icon"
        static let technology = "13_technology_icon"
        static let travel = "14_travel_icon"
        static let bar = "15_bar_icon"
        static let bookStore = "16_bookstore_icon"
        static let boutique = "17_boutique_icon"
        static let doctor = "18_doctor_icon"
        static let flowerShop = "19_flowerShop_icon"
        static let fuelStation = "20_fuelStation_icon"
        static let health = "21_health_icon"
        static let jewelryStore = "22_jewlryStore_icon"
        static let pets = "23_pets_icon"
        static let pharmacy = "24_pharmacy_icon"
        static let services = "25_services_icon"
        static let stationery = "26_stationary_icon"
        static let supermarket = "27_supermarket_icon"
        static let toyStore = "28_toysStore_icon"
        static let ecommerce = "29_ecommerce_icon"
        static let other = "30_other_icon"

        static let promotion = "5_promotion_icon"
        static let discount_icon = "5_012_discount_icon"

    }

    struct Notifications {
        static let alarm_icon = "4_alarm_icon"
    }

    struct GpsHelp {
        static let lock_icon = "2_007_lock_icon"
        static let correct_location_icon = "2_030_correctlocation_icon"
        static let nearme_icon = "2_036_nearme_icon"
        static let settings = "1_settings_manage"
    }

    struct Rewards {
        static let rewardsPointsBackground = "ilu_puntos_bg"
        static let rewardsNoPointsBackground = "ilu_no _puntos"

    }

    struct DidacticArea {
        static let imageThumbDefault = "ilu_wallet_default_didactic_area"
        static let imageBackgroundDefault = "bg_fractal_dashboard_didactic_area"
        static let imageBackgroundOfferDefault = "bg_dashboard_default_promo"

    }

    struct Help {
        static let imageThumbDefault = "ilu_info"
        static let contract_icon = "contract_icon"

    }

    struct AnonymousTutorial {

        static let imageBackgroundIphone45 = "bg_public_access_Iphone_4_5"
        static let imageBackgroundIphone6 = "bg_public_access_Iphone_6"
        static let imageBackgroundIphonePlus = "bg_public_access_Iphone_plus"
        static let imageBackgroundIphoneX = "bg_public_access_Iphone_X"
        static let imageBackgroundIphoneXR = "bg_public_access_Iphone_XR"
    }
}

struct Constants {

    static let CLEAR_COPY_PAN = "clearClipboardTimeout"
    static let TIMEOUT_COPY_PAN = 60.0
    static let PLATFORM = "ios"
    static let SECTION_TO_SHOW_CARDS = "cards"
    static let SECTION_TO_SHOW_REWARDS = "points"
    static let SECTION_TO_SHOW_PROMOTION_DETAIL = "promotions_detail"
    static let help_config_file = "helpConfigFile"
    static let appTimeout = Notification.Name("appTimeout")
    static let didactic_area_file = "didacticAreaFile"
    static let default_toast_time_in_seconds: CGFloat = 6.0
    static let tel_scheme = "tel"
    static let UNKNOWN = "unknown"

    struct DidacticArea {
        static let day_of_category = "day"
        static let time_of_category = "time"
        static let index_of_category = "index"
        static let count_of_category = "count"

    }
    
    struct HelpId {
        static let helpAbout = "helpAbout"
        static let helpDigitalCard = "helpDigitalCard"
        static let helpOnOff = "helpOnOff"
        static let helpPoints = "helpPoints"
        static let helpPromotionsDetail = "helpPromotionsDetail"
        static let helpSticker = "helpSticker"
    }
}

struct DistanceConstants {
    static let meters_to_kilometers = 1000
    static let positionPrecisionForGroupByLatitudeLongitude = 5
}

struct ConstantsLottieAnimations {
    static let cvvLoadingDots = "loading-cvv-dots"
    static let loader = "loader"
    static let lock = "candado"
    static let bell = "campana"
    static let aquaBirds = "Pajaros Aqua loop"
    static let blueBirds = "Pajaros Blue loop"
    static let play = "play220"
    static let anonymousHome = "Inicio V03"
    static let anonymousCards = "Tarjetas V03"
    static let anonymousPoints = "Puntos V03"
    static let anonymousNotifications = "Notificaciones V03"
    static let contactDetails = "Promotions Detail Contact"
    static let walletSplashLogo = "Wallet Splash Logo"
}
