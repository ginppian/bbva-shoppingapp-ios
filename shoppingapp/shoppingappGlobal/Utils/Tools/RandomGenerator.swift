//
//  RandomGenerator.swift
//  shoppingapp
//
//  Created by Javier Pino on 25/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class RandomGenerator {

    func randomInt(fromZeroTo toInt: Int) -> Int {

        return randomInt(from: 0, to: toInt)
    }

    func randomInt(from fromInt: Int, to toInt: Int) -> Int {

        return fromInt + Int(arc4random_uniform(UInt32(toInt + 1 - fromInt)))
    }
}
