//
//  TicketUtils.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 18/02/2019.
//  Copyright © 2019 BBVA. All rights reserved.
//

import Foundation

class TicketUtils {
    
    static func openDummyTicket() {
        let stringTicket = "{\"aps\":{\"alert\":{\"loc-args\":[\"0007\",\"481516\",\"CELLS VS NATIVO\",\"666.66\",\"MXP\",\"160722084808\",\"NIKE STORE\",\"245\"],\"loc-key\":\"00050\"},\"sound\":\"default\"}}"
        
        let jsonData = stringTicket.data(using: .utf8)
        let dictionaryTicket = try? JSONSerialization.jsonObject(with: jsonData!, options: .mutableLeaves)
        
        let ticketModel = TicketModel(withPayload: dictionaryTicket as! [AnyHashable: Any])
        
        DispatchQueue.main.asyncAfter(deadline: DispatchTime.now() + .seconds(5)) {
            BusManager.sessionDataInstance.showTicketDetail(withModel: ticketModel)
        }
    }

}
