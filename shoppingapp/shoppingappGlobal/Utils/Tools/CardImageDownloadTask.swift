//
//  CardImageDownloadTask.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 29/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import RxSwift

struct CardImageUtils {

    static let placeholderImage = UIImage(named: ConstantsImages.Card.blank_card)
    static let placeholderStickerImage = UIImage(named: ConstantsImages.Card.blank_sticker)
    static let placeholderBackImage = UIImage(named: ConstantsImages.Card.defaultBackCard)
    static let disposeBag = DisposeBag()
    static let etagKey = "Etag-"

}

struct CardImageData: Codable {

    let url: String
    let etag: String?
    let image: Data?

}

class CardsImagesManager {

    static var sharedInstance = CardsImagesManager()

    var cardsImagesTask = [String: CardImageDownloadTask]()

    init() {
        DLog(message: "Init ImageCardManager Singleton")
    }

    func setupImageTasks(withCards cards: [CardBO]) {

        for card in cards {

            if let url = card.imageFront, !url.isEmpty {
                cardsImagesTask[url] = CardImageDownloadTask(url: url, isSticker: card.isSticker())
            }

            if let url = card.imageBack, !url.isEmpty {
                cardsImagesTask[url] = CardImageDownloadTask(url: url, isSticker: card.isSticker())
            }
        }
    }
}

extension UIImageView {

    typealias CardImageCompletionHandlerFinished = (_ image: UIImage?, _ newImage: Bool) -> Void

    private func setCardImage(withUrl url: String) {

        DispatchQueue.main.async {
            
            var imageDefault = CardImageUtils.placeholderImage
            
            if let isSticker = CardsImagesManager.sharedInstance.cardsImagesTask[url]?.isSticker, isSticker {
                imageDefault = CardImageUtils.placeholderStickerImage
            }
            
            self.image = CardsImagesManager.sharedInstance.cardsImagesTask[url]?.image ?? imageDefault
        }
    }

    func getCardImage(withUrl url: String, andCompletionHandler completion: CardImageCompletionHandlerFinished? = nil) {

        var task = CardsImagesManager.sharedInstance.cardsImagesTask[url]

        if task == nil {
            task = CardImageDownloadTask(url: url)
        }

        task?.getCardImage(withCompletion: { image, _ in

            DispatchQueue.main.async {
                let isNewImage = (self.image != image)
                completion?(image, isNewImage)
            }
        })

        setCardImage(withUrl: url)
    }
}

class CardImageDownloadTask {

    let url: String
    var image: UIImage?
    let isSticker: Bool?

    private var isFinishedDownloading = false
    private var isFinishedCheckEtag = false

    typealias DownloadCardImageCompletionHandler = (_ image: UIImage?, _ url: String?) -> Void

    init(url: String, isSticker: Bool? = false) {

        self.url = url
        self.isSticker = isSticker
    }

    func getCardImage(withCompletion completion: DownloadCardImageCompletionHandler? = nil) {

        if let cardImageData = CardImageStorage.getCardImageData(withUrl: url), let etag = cardImageData.etag, !etag.isEmpty {

            if let imageData = cardImageData.image, let cardImage = UIImage(data: imageData) {

                DispatchQueue.main.async {
                    if self.image == nil {
                        self.image = cardImage
                    }
                    completion?(self.image, self.url)
                }

            }

            if !isFinishedCheckEtag {

                checkImageEtag(withUrl: url, withEtag: etag) { image, url in
                    completion?(image ?? self.image, url)
                }
            }

        } else {

            if !isFinishedDownloading {

                getImageFromUrl(fromUrl: url, completion: completion)
            }
        }

    }

    // MARK: - Private methods -

    private func getImageFromUrl(fromUrl url: String, completion: DownloadCardImageCompletionHandler? = nil) {

        EtagDataManager.sharedInstance.getImage(withUrl: url).subscribe(
            onNext: { [weak self] result in

                if let imageInfo = result as EtagEntity?, let imageData = imageInfo.data as Data?, let imageDownloaded = UIImage(data: imageData), let imageEtag = imageInfo.etag as String? {

                    let cardImageData = CardImageData(url: url, etag: imageEtag, image: imageData)
                    CardImageStorage.saveCardImageData(cardImageData: cardImageData)

                    DispatchQueue.main.async {
                        self?.image = imageDownloaded
                    }

                    self?.isFinishedDownloading = true
                    DLog(message: "Download card image url: \(url)")

                    completion?(imageDownloaded, url)
                }
            },
            onError: { [weak self] _ in
                self?.isFinishedDownloading = true
                DLog(message: "Error Download card image url: \(url)")
            },
            onCompleted: {
            }).addDisposableTo(CardImageUtils.disposeBag)

    }

    private func checkImageEtag(withUrl url: String, withEtag etag: String, completion: DownloadCardImageCompletionHandler? = nil) {

        EtagDataManager.sharedInstance.checkEtagImage(withUrl: url, withEtag: etag).subscribe(
            onNext: { [weak self] result in

                if let imageInfo = result as EtagEntity?, let imageData = imageInfo.data as Data?, let imageDownloaded = UIImage(data: imageData), let imageEtag = imageInfo.etag as String? {

                    let cardImageData = CardImageData(url: url, etag: imageEtag, image: imageData)
                    CardImageStorage.saveCardImageData(cardImageData: cardImageData)

                    DispatchQueue.main.async {
                        self?.image = imageDownloaded
                    }

                    self?.isFinishedCheckEtag = true
                    DLog(message: "Etag - Download card image url: \(url)")

                    completion?(imageDownloaded, url)
                }
            },
            onError: { [weak self] _ in
                self?.isFinishedCheckEtag = true
                DLog(message: "Etag - Error Download card image url: \(url)")
                completion?(nil, url)
            },
            onCompleted: {
            }).addDisposableTo(CardImageUtils.disposeBag)

    }

}

class CardImageStorage {

    static func saveCardImageData(cardImageData: CardImageData) {

        let key = CardImageUtils.etagKey + cardImageData.url

        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(cardImageData) {

            PreferencesManager.sharedInstance().saveValue(forValue: encoded as AnyObject, withKey: key)
        }
    }

    static func getCardImageData(withUrl url: String) -> CardImageData? {

        let key = CardImageUtils.etagKey + url

        if let savedCardImageData = PreferencesManager.sharedInstance().getValue(forKey: key) as? Data {

            let decoder = JSONDecoder()
            if let cardImageData = try? decoder.decode(CardImageData.self, from: savedCardImageData) {
                return cardImageData
            }
        }

        return nil
    }
}
