//
//  PageReactionConstants.swift
//  shoppingapp
//
//  Created by jesus.martinez on 14/6/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

struct PageReactionConstants {

    static let CHANNEL_CARDS: Channel<Model> = Channel(name: "channel-cards")

    static let CHANNEL_HIDDEN_VIEW_NATIVE: Channel<String> = Channel(name: "channel-hidden-view-native")

    static let CHANNEL_SECURITY: Channel<Any> = Channel(name: "channel-security")

    static let CHANNEL_SOFTOKEN: Channel<Any> = Channel(name: "channel-softoken")

    static let CHANNEL_TOKEN_CHANGED: Channel<String> = Channel(name: "channel-token-changed")

    static let CHANNEL_TOKEN_FAILED: Channel<String> = Channel(name: "channel-token-failed")

    static let PARAMS_CARDS: ActionSpec<Model> = ActionSpec<Model>(id: "PARAMS_CARDS")

    static let HIDDEN_VIEW_NATIVE: ActionSpec<String> = ActionSpec<String>(id: "HIDDEN_VIEW_NATIVE")

    static let TOKEN_CHANGED: ActionSpec<String> = ActionSpec<String>(id: "TOKEN_CHANGED")

    static let TOKEN_FAILED: ActionSpec<String> = ActionSpec<String>(id: "TOKEN_FAILED")
        
    static let CHANNEL_NEW_TRACK_EVENT: Channel<Model> = Channel(name: "channel-new-track-event")
    
    static let NEW_TRACK_EVENT: ActionSpec<Model> = ActionSpec<Model>(id: "new-track-event")

    static let TICKET_RECEIVED_IN_BACKGROUND: ActionSpec<Model> = ActionSpec<Model>(id: "TICKET_RECEIVED_IN_BACKGROUND")

    static let CHANNEL_TICKET_RECEIVED_IN_BACKGROUND: Channel<Model> = Channel(name: "CHANNEL_TICKET_RECEIVED_IN_BACKGROUND")
}
