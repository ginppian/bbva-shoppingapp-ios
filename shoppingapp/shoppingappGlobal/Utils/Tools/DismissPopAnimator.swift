//
//  DismissPopAnimator.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 4/7/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit

class DismissPopAnimator: NSObject, UIViewControllerAnimatedTransitioning {

    let transitionAnimationDuration = 0.35

    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {

        return transitionAnimationDuration
    }

    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {

        let fromViewController = transitionContext.viewController(forKey: .from)!
        let containerView = transitionContext.containerView

        let animationDuration = transitionDuration(using: transitionContext)

        UIView.animate(withDuration: animationDuration, delay: 0, options: [.curveEaseInOut], animations: {
            fromViewController.view.transform = CGAffineTransform(translationX: containerView.bounds.width, y: 0)

        }, completion: { _ in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)

        })
    }
}

class DismissPopAnimatorDelegate: NSObject, UIViewControllerTransitioningDelegate {

    func animationController(forDismissed dismissed: UIViewController) -> UIViewControllerAnimatedTransitioning? {

        if let navigation = dismissed as? UINavigationController {

            if navigation.viewControllers.first != navigation.topViewController {
                return DismissPopAnimator()
            }
        }

        return nil
    }

}
