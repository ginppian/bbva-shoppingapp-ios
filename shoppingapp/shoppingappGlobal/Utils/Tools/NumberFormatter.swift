//
//  NumberFormatter.swift
//  shoppingapp
//
//  Created by jesus.martinez on 19/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

extension NumberFormatter {

    static func stringDecimalToStringWithSeparator(withString string: String, withLocale locale: Locale) -> String {

        let number = NSDecimalNumber(string: string)

        let formatter = NumberFormatter()
        formatter.numberStyle = .decimal
        formatter.locale = locale
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 0

        return formatter.string(from: number as NSNumber)!
    }

}
