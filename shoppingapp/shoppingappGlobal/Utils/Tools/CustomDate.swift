//
//  CustomDate.swift
//  shoppingapp
//
//  Created by jesus.martinez on 10/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation

extension Date {

    static let DATE_DD_MM_YYYY = "dd-MM-yyyy"
    static let DATE_FORMAT_SERVER = "yyyy-MM-dd"
    static let DATE_FORMAT_LONG_SERVER = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZZ"
    static let DATE_FORMAT_LONG_SERVER_ALT = "yyyy-MM-dd'T'HH:mm:ssZZZZ"
    static let DATE_FORMAT_LONG_SERVER_OTHER = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'"
    static let DATE_FORMAT_MONTH_SHORT_YEAR = "MM/yy"
    static let DATE_FORMAT_MONTH_NAME_YEAR_HOUR = "d MMMM yyyy HH:mm"
    static let DATE_FORMAT_PUSH = "yyMMddHHmmss"
    static let DATE_FORMAT_PUSH_FORMATTED = "yyyy-MM-dd HH:mm:ss"
    static let DATE_FORMAT_CELLS = "dd MMMM',' yyyy HH:mm'h'"

    static let DATE_FORMAT_DAY_MONTH = Localizables.common.key_date_mask_day_month
    static let DATE_FORMAT_DAY_MONTH_SHORT = Localizables.common.key_date_mask_day_month_short
    static let DATE_FORMAT_DAY_MONTH_YEAR = Localizables.common.key_date_mask_day_month_year
    static let DATE_FORMAT_DAY_MONTH_YEAR_SHORT = Localizables.common.key_date_mask_day_month_year_short
    static let DATE_FORMAT_DAY_MONTH_YEAR_COMPACT = Localizables.common.key_date_mask_day_month_year_compact
    static let DATE_FORMAT_DAY_MONTH_COMPLETE_YEAR_COMPACT = Localizables.common.key_date_mask_day_month_complete_year_compact

    static func locale() -> Locale {
        return Locale.current
    }

    static func dateFormatterLocalized(withLocale locale: Locale) -> DateFormatter {

        let dateFormatter = DateFormatter()
        dateFormatter.locale = locale

        return dateFormatter

    }
    
    static func utcDateFormatter() -> DateFormatter {
        
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        
        return dateFormatter
        
    }

    static func date(fromServerString dateString: String, withFormat format: String) -> Date? {

        let dateFormatter = Date.utcDateFormatter()
        dateFormatter.dateFormat = format

        let date = dateFormatter.date(from: dateString)

        return date

    }
    
    static func date(fromLocalTimeZoneString dateString: String, withFormat format: String) -> Date? {
        
        let dateFormatter = Date.dateFormatterLocalized(withLocale: Date.locale())
        dateFormatter.dateFormat = format
        
        let date = dateFormatter.date(from: dateString)
        
        return date
        
    }

    func string(format: String) -> String {

        let formatter = Date.dateFormatterLocalized(withLocale: Date.locale())
        formatter.dateFormat = format

        return formatter.string(from: self)

    }

    static func string(fromDate date: Date, withFormat format: String) -> String {

        let dateFormatter = Date.dateFormatterLocalized(withLocale: self.locale())
        dateFormatter.dateFormat = format

        let date = dateFormatter.string(from: date)

        return date

    }

    static func currentYear() -> String? {

        let date = Date()
        let calendar = Calendar.current
        let components = calendar.dateComponents([.year], from: date)

        guard let year = components.year else {
            return nil
        }

        return String(year)
    }

    static func getDateFormatForCellsModule(withDate date: String) -> String {

        let formattedDate = Date.date(fromLocalTimeZoneString: date, withFormat: Date.DATE_FORMAT_LONG_SERVER)

        guard let date = formattedDate else {
            return ""
        }

        let dateFormatter = Date.dateFormatterLocalized(withLocale: Date.locale())
        dateFormatter.dateFormat = Date.DATE_FORMAT_CELLS
        dateFormatter.monthSymbols = dateFormatter.monthSymbols.map { $0.localizedCapitalized }

        return dateFormatter.string(from: date)
    }
}
