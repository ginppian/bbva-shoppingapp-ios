//
//  ShareContentManager.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 27/9/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit

class ShareContentManager: NSObject {
    
    // MARK: Singleton
    
    static var instance = ShareContentManager()
    
    class func sharedInstance () -> ShareContentManager {
        return instance
    }
    
    func shareContent(values: [String]) -> UIActivityViewController {
        
        let activityController = UIActivityViewController(activityItems: values, applicationActivities: nil)
        activityController.excludedActivityTypes = [.addToReadingList,
                                                    .postToFlickr,
                                                    .postToVimeo,
                                                    .postToTencentWeibo,
                                                    .airDrop,
                                                    .postToWeibo,
                                                    .print,
                                                    .assignToContact,
                                                    .saveToCameraRoll]
        return activityController

    }
    
    func shareFile(fileURL: URL) -> UIActivityViewController {
        
        let activityController = UIActivityViewController(activityItems: [fileURL], applicationActivities: nil)
        
        activityController.excludedActivityTypes = [.addToReadingList,
                                                    .postToFlickr,
                                                    .postToVimeo,
                                                    .postToTencentWeibo,
                                                    .postToWeibo,
                                                    .assignToContact,
                                                    .saveToCameraRoll]
        
        return activityController
    }
}
