//
//  NotRecognizedOperationUtils.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 01/03/2019.
//  Copyright © 2019 BBVA. All rights reserved.
//

import Foundation
import CellsNativeCore
import CellsNativeComponents

class NotRecognizedOperationUtils {
    
    var foundCellsViewControllerPresented = false
    var foundCellsViewControllerPushed = false
    var needNavigateToNotRecognizedOperation = false
    
    func startFindNotRecognizedOperationViewController(close: Bool) {
        foundCellsViewControllerPresented = false
        foundCellsViewControllerPushed = false
        
        if let rootViewController = (UIApplication.shared.delegate as? AppDelegate)?.window?.rootViewController {
            findNotRecognizedOperationViewController(rootViewController: rootViewController, presented: false, close: close)
        }
    }
    
    func findNotRecognizedOperationViewController(rootViewController: UIViewController, presented: Bool, close: Bool) {
        
        if close {
            checkNeedCloseCellsViewController(viewController: rootViewController)
        } else {
            checkNotRecognizedOperationViewControllerAlreadyPresented(viewController: rootViewController, presented: presented)
        }
        
        if let childViewControllers = rootViewController.presentedViewController?.childViewControllers {
            
            for viewController in childViewControllers {
                findNotRecognizedOperationViewController(rootViewController: viewController, presented: true, close: close)
            }
        }
        
        for viewController in rootViewController.childViewControllers {
            findNotRecognizedOperationViewController(rootViewController: viewController, presented: false, close: close)
        }
    }
    
    func checkNotRecognizedOperationViewControllerAlreadyPresented(viewController: UIViewController, presented: Bool) {
        
        if let viewController = viewController as? NotRecognizedOperationViewController, !viewController.isPresentedIntheTop() {
            
            if presented {
                foundCellsViewControllerPresented = true
            } else {
                foundCellsViewControllerPushed = true
            }
        }
    }
    
    func checkNavigateToNotRecognizedOperation(webController: WebController) {
        
        startFindNotRecognizedOperationViewController(close: false)
        
        if foundCellsViewControllerPresented || foundCellsViewControllerPushed {
            
            let errorBO = ErrorBO(message: Localizables.common.key_duplicate_operation, errorType: ErrorType.warning, allowCancel: false)
            BusManager.sessionDataInstance.showModalError(error: errorBO, delegate: nil)
            
        } else {
            
            webController.navigateTo(route: TransactionDetailPageReaction.CELLS_PAGE_NOT_RECOGNIZED_OPERATION, payload: nil)
            needNavigateToNotRecognizedOperation = true
        }
    }

    func checkNeedCloseCellsViewController(viewController: UIViewController) {
        
        if let viewController = viewController as? WalletCellsViewController,
            viewController is NotRecognizedOperationViewController == false,
            !viewController.isPresentedIntheTop() {
            
            if isModal(viewController: viewController) && !foundCellsViewControllerPresented {
                
                foundCellsViewControllerPresented = true
                BusManager.sessionDataInstance.dismissPresentedViewControllers()
                
            } else if let navigationController = viewController.parent as? UINavigationController {
                
                foundCellsViewControllerPushed = true
                navigationController.popViewController(animated: false)
                navigationController.navigationBar.isHidden = false
                
            }
            
            if let webController: WebController = ComponentManager.get(id: WebController.ID) {
                webController.logoutWebView()
            }
        }
    }
    
    func isModal(viewController: UIViewController) -> Bool {
        
        return viewController.presentingViewController?.presentedViewController == viewController
            || (viewController.navigationController != nil && viewController.navigationController?.presentingViewController?.presentedViewController == viewController.navigationController)
            || viewController.tabBarController?.presentingViewController is UITabBarController
    }
}
