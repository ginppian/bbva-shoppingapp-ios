//
//  CollectionExtensions.swift
//  shoppingapp
//
//  Created by jesus.martinez on 5/6/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

extension Array where Element: Equatable {

    func uniques() -> [Element] {
        var result = [Element]()
        for value in self {
            if !result.contains(value) {
                result.append(value)
            }
        }
        return result
    }
}

extension Collection {

    /// Returns the element at the specified index iff it is within bounds, otherwise nil.
    subscript (safeElement index: Index) -> Element? {
        return indices.contains(index) ? self[index] : nil
    }
}
