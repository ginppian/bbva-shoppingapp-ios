//
//  DeviceConstants.swift
//  shoppingapp
//
//  Created by Javier Pino on 17/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

public enum DisplayType {
    
    case unknown
    case iphone5
    case iphone6
    case iphone6plus

    static let iphone7 = iphone6
    static let iphone7plus = iphone6plus

    case iphoneX
    case iphoneXsMax
}

public final class Display {
    class var width: CGFloat { return UIScreen.main.bounds.size.width }
    class var height: CGFloat { return UIScreen.main.bounds.size.height }
    class var size: CGSize { return CGSize(width: width, height: height) }
    class var bounds: CGRect { return CGRect(origin: CGPoint.zero, size: size) }
    class var maxLength: CGFloat { return max(width, height) }
    class var minLength: CGFloat { return min(width, height) }
    class var zoomed: Bool { return UIScreen.main.nativeScale >= UIScreen.main.scale }
    class var retina: Bool { return UIScreen.main.scale >= 2.0 }
    class var phone: Bool { return UIDevice.current.userInterfaceIdiom == .phone }
    class var pad: Bool { return UIDevice.current.userInterfaceIdiom == .pad }

    class var typeIsLike: DisplayType {
        
        if phone && maxLength == 568 {
            return .iphone5
        } else if phone && maxLength == 667 {
            return .iphone6
        } else if phone && maxLength == 736 {
            return .iphone6plus
        } else if phone && maxLength == 812 {
            return .iphoneX
        } else if phone && maxLength == 896 {
            return .iphoneXsMax
        }
        return .unknown
    }
}
