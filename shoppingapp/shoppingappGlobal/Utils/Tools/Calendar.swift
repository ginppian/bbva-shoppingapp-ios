//
//  Calendar.swift
//  shoppingapp
//
//  Created by jesus.martinez on 10/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation

extension Calendar {

    static func gregorianCalendar() -> Calendar {
        return Calendar(identifier: Calendar.Identifier.gregorian)
    }

    func isDate(_ date: Date, duringSameYearAsDate: Date) -> Bool {
        let dateC = dateComponents([.year], from: date)
        let duringSameWeekComponents = dateComponents([.year], from: duringSameYearAsDate)
        let result = dateC.year == duringSameWeekComponents.year
        return result
    }

}
