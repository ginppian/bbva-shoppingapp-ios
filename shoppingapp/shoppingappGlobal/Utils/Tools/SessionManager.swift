//
//  SessionManager.swift
//  shoppingapp
//
//  Created by ruben.fernandez on 12/6/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation
import RxSwift
import CellsNativeComponents
import CellsNativeCore

@objc class SessionDataManager: NSObject {

    @objc static var instance: SessionDataManager?
    
    @objc class func sessionDataInstance() -> SessionDataManager {
        
        guard instance != nil else {
            
            instance = SessionDataManager()
            return instance!
        }
        
        return instance!
    }
    
    private let concurrentQueue = DispatchQueue(label: "concurrentQueue", qos: .default, attributes: .concurrent)

    private var _isUserLogged = false
    
    @objc var isUserLogged: Bool {
        get {
            return concurrentQueue.sync { _isUserLogged }
        }
        set (newValue) {
            concurrentQueue.async(flags: .barrier) { self._isUserLogged = newValue }
        }
    }
    
    var mainCurrency: String
    var codeCountry: String
    var isUserAnonymous = false
    
    var serverHost: String?
    var serverCode: String?
    var customerID: String?

    var isShowTrendingAnimation = false
    var isShowCardAnimation = false
    var isShowRewardsAnimation = false
    let disposeBag = DisposeBag()

    var tabBarSelected: String = ""
    
    enum ServerConfigEnvironment: String {
        
        case artichoke = "mock"
        case development = "development"
        case test = "test"
        case dev = "dev"
        case hiddenLeague = "hiddenleague"
        case production = "production"
        
        var keyServerUrl: String {
            
            switch self {
            case .artichoke:
                return "SERVER_URL_MOCK"
            case .development:
                return "SERVER_URL_DEVELOPMENT"
            case .test:
                return "SERVER_URL_TEST"
            case .dev:
                return "SERVER_URL_DEV"
            case .hiddenLeague:
                return "SERVER_URL_HIDDEN_LEAGUE"
            case .production:
                return "SERVER_URL"
            }
        }
    }

    override init() {

        mainCurrency = SessionDataManager.getMainCurrency()
        codeCountry = SessionDataManager.getCodeCountry()
        super.init()
        getHost()
        getServerCode()

    }

    func closeSession() {

        isUserAnonymous = false
        isUserLogged = false
        customerID = ""
        BusManager.sessionDataInstance.createNetworkWorker()
        BusManager.sessionDataInstance.clearData(tag: ShoppingCrossReaction.TAG, PageReactionConstants.PARAMS_CARDS)
        isShowTrendingAnimation = false
        isShowCardAnimation = false
        isShowRewardsAnimation = false
    }

    func clearTabBarSelected() {

       tabBarSelected = ""
    }

    func getHost() {

        let apiServicesDictionary = SessionDataManager.getApiServicesDictionary()
        let selectedEnvironment = apiServicesDictionary["SelectedEnvironment"] as! String
        getHostBySelectedEnvironment(environment: selectedEnvironment)
    }
    
    func getHostBySelectedEnvironment(environment: String) {
        
        let apiServicesDictionary = SessionDataManager.getApiServicesDictionary()
        
        switch environment {
        case ServerConfigEnvironment.artichoke.rawValue:
            serverHost = (apiServicesDictionary[ServerConfigEnvironment.artichoke.keyServerUrl] as! String)
        case ServerConfigEnvironment.development.rawValue:
            serverHost = (apiServicesDictionary[ServerConfigEnvironment.development.keyServerUrl] as! String)
        case ServerConfigEnvironment.test.rawValue:
            serverHost = (apiServicesDictionary[ServerConfigEnvironment.test.keyServerUrl] as! String)
        case ServerConfigEnvironment.dev.rawValue:
            serverHost = (apiServicesDictionary[ServerConfigEnvironment.dev.keyServerUrl] as! String)
        case ServerConfigEnvironment.hiddenLeague.rawValue:
            serverHost = (apiServicesDictionary[ServerConfigEnvironment.hiddenLeague.keyServerUrl] as! String)
        case ServerConfigEnvironment.production.rawValue:
            serverHost = (apiServicesDictionary[ServerConfigEnvironment.production.keyServerUrl] as! String)
        default:
            serverHost = (apiServicesDictionary[ServerConfigEnvironment.production.keyServerUrl] as! String)
        }
    }

    func getServerCode() {

        let apiServicesDictionary = SessionDataManager.getApiServicesDictionary()
        self.serverCode = (apiServicesDictionary["SERVER_CODE"] as! String)
    }

    private func provideLogoutData() -> Observable<ModelBO> {

        return Observable.create { observer in

            LogoutDataManager.sharedInstance.serviceGrantingTicket().subscribe(

                onNext: { _ in
                    observer.onNext(EmptyModelBO())
                    observer.onCompleted()
                },

                onError: { error in

                    let evaluate : ServiceError = (error as? ServiceError)!

                    switch evaluate {

                    case .GenericErrorEntity(let errorEntity):
                        let errorBVABO = ErrorBO(error: errorEntity)
                        observer.onError(ServiceError.GenericErrorBO(error: errorBVABO))
                    default:
                        observer.onError(evaluate)
                    }
                },

                onCompleted: {
                    DLog(message: "nothing to do")
                }

                ).addDisposableTo(self.disposeBag)

            return Disposables.create()
        }
    }

    func requestLogout() {

        provideLogoutData().subscribe(
            onNext: { _ in
                DLog(message: "Logout success")
            },
            onError: { error in
                DLog(message: "\(error)")
                let evaluate: ServiceError = (error as? ServiceError)!

                switch evaluate {
                case .GenericErrorBO(let errorBO):
                    DLog(message: "Logout error: " + (errorBO.errorMessage() ?? ""))
                default:
                    break
                }
            }
            ).addDisposableTo(disposeBag)
    }

    private static func getConfigFile() -> [String: AnyObject] {

        let path = Bundle.main.path(forResource: "Configuration", ofType: "plist")!
        return NSDictionary(contentsOfFile: path) as! [String: AnyObject]
    }

    private static func getApiServicesDictionary() -> [String: AnyObject] {

        let configDictionary = self.getConfigFile()
        return configDictionary["SERVER_CONFIG"] as! [String: AnyObject]
    }

    private static func getMainCurrency() -> String {

         let configDictionary = self.getConfigFile()
        return configDictionary["MainCurrency"] as! String
    }

    private static func getCodeCountry() -> String {

        let configDictionary = self.getConfigFile()
        return configDictionary["CODE_COUNTRY"] as! String
    }
    
}
