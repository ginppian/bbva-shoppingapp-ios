//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

import UIKit
import RxSwift
import Lottie
import Stellar
import IQKeyboardManagerSwift
import SVGKit
import Material
import CellsNativeCore
import CellsNativeComponents

class ___VARIABLE_ModuleName___ViewController: BaseViewController {

    // MARK: Constants

    static let ROUTE: String = "___VARIABLE_ModuleName___"

    // MARK: Properties

    var presenter: ___VARIABLE_ModuleName___PresenterProtocol!

    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        ((self.navigationController as! AppNavigator).webViewContainer as! CellsPageViewController).registerComponent(tag: presenter.routerTag, component: self.presenter! as! CellsComponent)

    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        ((self.navigationController as! AppNavigator).webViewContainer as! CellsPageViewController).unregisterComponent(tag: presenter.routerTag)
    }
}

extension ___VARIABLE_ModuleName___ViewController {

}

// MARK: LoginViewProtocol implementation

extension ___VARIABLE_ModuleName___ViewController: ___VARIABLE_ModuleName___ViewProtocol {

    func showError(error: ModelBO) {

    }

    func changeColorEditTexts() {

    }
}

// MARK: - APPIUM

private extension ___VARIABLE_ModuleName___ViewController {

    func setAccessibilities() {

        #if APPIUM

        #endif

    }
}
