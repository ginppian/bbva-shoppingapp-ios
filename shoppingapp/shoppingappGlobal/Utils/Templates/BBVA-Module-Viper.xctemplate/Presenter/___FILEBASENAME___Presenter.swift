//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

import Foundation
import RxSwift
import CellsNativeComponents
import CellsNativeCore

class ___VARIABLE_ModuleName___Presenter<T: ___VARIABLE_ModuleName___ViewProtocol>: BasePresenter<T> {

    // MARK: Properties Bus

    let disposeBag = DisposeBag()

    override init(busManager: BusManager) {

        super.init(busManager: busManager)
        routerTag = "___VARIABLE_ModuleName___-tag"
    }

    override init() {
        super.init()
        routerTag = "___VARIABLE_ModuleName___-tag"
    }
}

extension ___VARIABLE_ModuleName___Presenter: ___VARIABLE_ModuleName___PresenterProtocol {
}

extension ___VARIABLE_ModuleName___Presenter: CellsComponent {

    func onMessage(property: String, params: AnyObject?) {

    }

    func onError(property: String, error: NSError) {

    }

}
