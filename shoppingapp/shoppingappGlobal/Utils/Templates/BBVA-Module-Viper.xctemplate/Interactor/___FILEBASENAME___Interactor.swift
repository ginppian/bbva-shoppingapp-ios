//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

import Foundation
import RxSwift

class ___VARIABLE_ModuleName___Interactor {

    // MARK: Properties

    let disposeBag = DisposeBag()
}

extension ___VARIABLE_ModuleName___Interactor: ___VARIABLE_ModuleName___InteractorProtocol {
}
