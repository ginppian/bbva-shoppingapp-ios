//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

import UIKit

protocol ___VARIABLE_ModuleName___ViewProtocol: ViewProtocol {
}

protocol ___VARIABLE_ModuleName___PresenterProtocol: PresenterProtocol {
}

protocol ___VARIABLE_ModuleName___InteractorProtocol: InteractorProtocol {
}
