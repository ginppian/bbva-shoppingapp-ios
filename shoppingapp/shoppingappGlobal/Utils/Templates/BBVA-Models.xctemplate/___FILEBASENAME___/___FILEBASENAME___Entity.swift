//
//  ___FILENAME___
//  ___PROJECTNAME___
//
//  Created by ___FULLUSERNAME___ on ___DATE___.
//___COPYRIGHT___
//

import Foundation
import HandyJSON

class ___VARIABLE_ModuleName___Entity: ModelEntity, HandyJSON {

    override required init() {}

}
