//
//  CardsDataManager.swift
//  shoppingapp
//
//  Created by ruben.fernandez on 12/9/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation
import RxSwift
import HandyJSON
import BBVA_Network

class ShoppingDataManager: BaseDataManager {

    static let sharedInstance = ShoppingDataManager()

    var urlTask: URLSessionDataTask?

    // MARK: - public methods

    func provideCategories() -> Observable<ModelEntity> {

        let url = ServerHostURL.getPromotionsURL() + "/categories"

        return self.provideCategories(withURL: url)

    }

    func providePromotions(byParams params: ShoppingParamsTransport, cancellable: Bool = false) -> Observable<ModelEntity> {

        var url = ServerHostURL.getPromotionsURL() + "/promotions"

        if let queryParams = params.queryParams() {
            url += "?" + queryParams
        }

        return providePromotions(withRequestCancellable: cancellable, url: url)
    }

    func providePromotionsPhysical(byParams params: ShoppingParamsTransport, cancellable: Bool = false) -> Observable<ModelEntity> {

        var url = ServerHostURL.getPromotionsURL() + "/promotions"

        if let queryParams = params.queryParams() {
            url += "?" + queryParams + "&usageTypes.id=in=(PHYSICAL)"
        } else {
            url += "?" + "usageTypes.id=in=(PHYSICAL)"
        }

        return providePromotions(withRequestCancellable: cancellable, url: url)

    }

    func providePromotions(byParams params: ShoppingParamsTransport, withFilters filters: ShoppingFilter?, cancellable: Bool = false) -> Observable<ModelEntity> {

        var url = ServerHostURL.getPromotionsURL() + "/promotions"

        var parameters = String()

        if let queryParams = params.queryParams() {
            parameters = queryParams
        }

        if let filters = filters {
            parameters += filters.retrieveServiceParameters()
        }

        parameters = parameters.addingPercentEncodingForURLQueryValue()!

        url += "?" + parameters

        return providePromotions(withRequestCancellable: cancellable, url: url)
    }

    func providePromotionsWithFilters(withNextPage nextPage: String) -> Observable<ModelEntity> {

        return self.providePromotions(withURL: nextPage)
    }

    func providePromotions(withURL url: String) -> Observable<ModelEntity> {

        DLog(message: "URL: [ \(url) ]")

        return Observable.create { observer in

            let request: NetworkRequest = self.createRequest(url, self.headers, nil, .useProtocolCachePolicy, 30, NetworkMethod.GET)

            _ = BusManager.sessionDataInstance.networkWorker?.execute(request: request) { (responseObject: Response) in

                if responseObject.error == nil {

                    let dataJson: String = responseObject.body!

//                    DLog(message: dataJson)

                    let status = Int(responseObject.code)

                    if status!>=200 && status!<=299 {

                        if status == 204 {
                            let promotionsEntity = PromotionsEntity()
                            promotionsEntity.status = 204
                            observer.onNext(promotionsEntity)
                            observer.onCompleted()
                        } else {
                            if let promotionsEntity = PromotionsEntity.deserialize(from: dataJson) {
                                promotionsEntity.status = status
                                observer.onNext(promotionsEntity)
                                observer.onCompleted()
                            } else {
                                observer.onError(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text, httpStatus: status ?? ConstantsHTTPCodes.GENERIC_ERROR)))
                            }
                        }
                    } else {
                        observer.onError(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text, httpStatus: status ?? ConstantsHTTPCodes.GENERIC_ERROR)))
                    }
                } else {

                    if let body = responseObject.body, let objectDeserialize = ErrorEntity.deserialize(from: body) {
                        observer.onError(ServiceError.GenericErrorEntity(error: objectDeserialize))
                    } else {
                        DLog(message: "Error parsing")
                        var status = ConstantsHTTPCodes.GENERIC_ERROR
                        if let errorCode = responseObject.code {
                            status = Int(errorCode) ?? ConstantsHTTPCodes.GENERIC_ERROR
                        }
                        observer.onError(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text, httpStatus: status)))                    }
                }
            }
            return Disposables.create {
            }
        }

    }

    // MARK: - private methods

    private func providePromotions(withRequestCancellable cancellable: Bool, url: String) -> Observable<ModelEntity> {

        if cancellable {
            return self.provideForPromotionsCancellingPrevious(withURL: url)
        } else {
            return self.providePromotions(withURL: url)
        }

    }

    private func provideCategories(withURL url: String) -> Observable<ModelEntity> {

        DLog(message: "URL: [ \(url) ]")

        return Observable.create { observer in

            let request: NetworkRequest = self.createRequest(url, self.headers, nil, .useProtocolCachePolicy, 30, NetworkMethod.GET)

            _ = BusManager.sessionDataInstance.networkWorker?.execute(request: request) { (responseObject: Response) in

                if responseObject.error == nil {

                    let dataJson: String = responseObject.body!

//                    DLog(message: dataJson)

                    let status = Int(responseObject.code) ?? ConstantsHTTPCodes.I_AM_A_TEAPOT

                    if status >= 200 && status <= 299 {

                        if status == 204 {

                            let categoriesEntity = CategoriesEntity()
                            categoriesEntity.status = 204
                            observer.onNext(categoriesEntity)
                            observer.onCompleted()

                        } else {
                            if let categoriesEntity = try? JSONDecoder().decode(CategoriesEntity.self, from: dataJson.data(using: .utf8)!) {
                                categoriesEntity.status = status
                                observer.onNext(categoriesEntity)
                                observer.onCompleted()
                            } else {
                                observer.onError(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text, httpStatus: ConstantsHTTPCodes.GENERIC_ERROR)))
                            }
                        }
                    } else {
                        observer.onError(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text, httpStatus: ConstantsHTTPCodes.GENERIC_ERROR)))
                    }
                } else {

                    if let objectDeserialize = ErrorEntity.deserialize(from: responseObject.body!) {
                        observer.onError(ServiceError.GenericErrorEntity(error: objectDeserialize))
                    } else {
                        DLog(message: "Error parsing")
                        var status = ConstantsHTTPCodes.GENERIC_ERROR
                        if let errorCode = responseObject.code {
                            status = Int(errorCode) ?? ConstantsHTTPCodes.GENERIC_ERROR
                        }
                        observer.onError(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text, httpStatus: status)))
                    }

                }
            }
            return Disposables.create {
            }
        }

    }

   private func provideForPromotionsCancellingPrevious(withURL url: String) -> Observable<ModelEntity> {

        DLog(message: "URL: [ \(url) ]")

        return Observable.create { [weak self] observer in

            self?.urlTask?.cancel()

            let request: NetworkRequest = (self?.createRequest(url, (self?.headers)!, nil, .useProtocolCachePolicy, 30, NetworkMethod.GET))!

            self?.urlTask = BusManager.sessionDataInstance.networkWorker?.execute(request: request) { (responseObject: Response) in

                if self?.urlTask?.currentRequest?.url?.absoluteString == url {
                    self?.urlTask = nil
                }

                if responseObject.error == nil {

                    let dataJson: String = responseObject.body!

//                    DLog(message: dataJson)

                    let status = Int(responseObject.code)

                    if status! >= 200 && status! <= 299 {

                        if status == 204 {
                            let promotionsEntity = PromotionsEntity()
                            promotionsEntity.status = 204
                            observer.onNext(promotionsEntity)
                            observer.onCompleted()
                        } else {
                            if let promotionsEntity = PromotionsEntity.deserialize(from: dataJson) {
                                promotionsEntity.status = status
                                observer.onNext(promotionsEntity)
                                observer.onCompleted()
                            } else {
                                observer.onError(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text, httpStatus: ConstantsHTTPCodes.GENERIC_ERROR)))
                            }
                        }

                    } else {
                        observer.onError(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text, httpStatus: ConstantsHTTPCodes.GENERIC_ERROR)))
                    }
                } else {

                    if let body = responseObject.body, let objectDeserialize = ErrorEntity.deserialize(from: body) {
                        observer.onError(ServiceError.GenericErrorEntity(error: objectDeserialize))
                    } else {
                        DLog(message: "Error parsing")

                        if (responseObject.error as NSError?)?.code == NSURLErrorCancelled && (self?.urlTask?.currentRequest?.url?.absoluteString != url) {
                            DLog(message: "-- Manual cancelling do not throw error -- URL (\(url))")
                            return
                        } else {
                            var status = ConstantsHTTPCodes.GENERIC_ERROR
                            if let errorCode = responseObject.code {
                                status = Int(errorCode) ?? ConstantsHTTPCodes.GENERIC_ERROR
                            }
                            observer.onError(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text, httpStatus: status)))
                        }

                    }
                }
            }
            return Disposables.create {
            }
        }

    }

    // MARK: - Detail

    func providePromotion(byParams params: ShoppingDetailParamsTransport) -> Observable<ModelEntity> {
        
        var url = ServerHostURL.getPromotionsURL() + "/promotions/" + params.idPromo
        
        if let queryParams = params.queryParams() {
            url += "?" + queryParams
        }
        
        return self.providePromotionForId(withURL: url)
    }
    
    func providePromotionForId(withURL url: String) -> Observable<ModelEntity> {

        DLog(message: "URL: [ \(url) ]")

        return Observable.create { observer in

            let request: NetworkRequest = self.createRequest(url, self.headers, nil, .useProtocolCachePolicy, 30, NetworkMethod.GET)

            _ = BusManager.sessionDataInstance.networkWorker?.execute(request: request) { (responseObject: Response) in

                if responseObject.error == nil {

                    let dataJson: String = responseObject.body!

//                    DLog(message: dataJson)

                    let status = Int(responseObject.code)

                    if status! >= 200 && status! <= 299 {

                        if status == 204 {
                            let promotionEntity = PromotionDetailEntity()
                            observer.onNext(promotionEntity)
                            observer.onCompleted()
                        } else {
                            if let promotionEntity = PromotionDetailEntity.deserialize(from: dataJson) {
                                observer.onNext(promotionEntity)
                                observer.onCompleted()
                            } else {
                                observer.onError(ServiceError.CouldNotDecodeJSON)
                            }
                        }
                    } else {
                        observer.onError(ServiceError.BadStatus(status: status!))
                    }
                } else {
                    if let objectDeserialize = ErrorEntity.deserialize(from: responseObject.body!) {
                        observer.onError(ServiceError.GenericErrorEntity(error: objectDeserialize))
                    } else {
                        DLog(message: "Error parsing")
                        var status = ConstantsHTTPCodes.GENERIC_ERROR
                        if let errorCode = responseObject.code {
                            status = Int(errorCode) ?? ConstantsHTTPCodes.GENERIC_ERROR
                        }
                        observer.onError(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text, httpStatus: status)))                    }
                }
            }
            return Disposables.create {
            }
        }

    }

}
