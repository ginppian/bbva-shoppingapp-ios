//
//  CardsDataManager.swift
//  shoppingapp
//
//  Created by ruben.fernandez on 12/9/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation
import RxSwift
import HandyJSON
import BBVA_Network

class CardsDataManager: BaseDataManager {

    static let sharedInstance = CardsDataManager()

    static let selectedEnvironment = Configuration.selectedEnvironment()

    func provideCards() -> Observable<ModelEntity> {

        let url = ServerHostURL.getCardsURL() + "/cards?expand=indicators,activations,related-contracts,rewards&status.id=OPERATIVE&status.id=INOPERATIVE&status.id=BLOCKED"

        return self.proviceCards(withURL: url)

    }

    private func proviceCards(withURL url: String) -> Observable<ModelEntity> {

        return Observable.create { observer in

            let request: NetworkRequest = self.createRequest(url, self.headers, nil, .useProtocolCachePolicy, 30, NetworkMethod.GET)

           _ = BusManager.sessionDataInstance.networkWorker?.execute(request: request) { (responseObject: Response) in

                if responseObject.error == nil {

                    let dataJson: String = responseObject.body!
                    let status = Int(responseObject.code)

                    DLog(message: dataJson)

                    if status! >= 200 && status! <= 299 {

                        if status == 204 {

                            let cardsEntity = CardsEntity()
                            cardsEntity.status = 204
                            observer.onNext(cardsEntity)
                            observer.onCompleted()

                        } else {

                            if let cardsEntity = CardsEntity.deserialize(from: dataJson) {
                                cardsEntity.status = status
                                observer.onNext(cardsEntity)
                                observer.onCompleted()
                            } else {
                                observer.onError(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
                            }

                        }
                    } else {
                        observer.onError(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
                    }
                } else {

                    if let body = responseObject.body, let objectDeserialize = ErrorEntity.deserialize(from: body) {
                        observer.onError(ServiceError.GenericErrorEntity(error: objectDeserialize))
                    } else {
                        DLog(message: "Error parsing")
                        observer.onError(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
                    }
                }
           }
            return Disposables.create {
            }
        }
    }

    func retrieveFinancialOverview(headers: [String: String]? = nil) -> Observable<ModelEntity> {

        return Observable.create { observer in

            let url = ServerHostURL.getContractsURL() + "/financial-overview"
            
            if let newHeaders = headers {
                for (key, value) in newHeaders {
                    self.headers[key] = value
                }
            }

            let request: NetworkRequest = self.createRequest(url, self.headers, nil, .useProtocolCachePolicy, 30, NetworkMethod.GET)

            _ = BusManager.sessionDataInstance.networkWorker?.execute(request: request) { (responseObject: Response) in

                if responseObject.error == nil {

                    let status = Int(responseObject.code)

                    if status! >= ConstantsHTTPCodes.STATUS_OK && status! <= 299 {

                        if status == ConstantsHTTPCodes.NO_CONTENT {

                            let financialOverviewEntity = FinancialOverviewEntity()
                            financialOverviewEntity.status = ConstantsHTTPCodes.NO_CONTENT
                            observer.onNext(financialOverviewEntity)
                            observer.onCompleted()

                        } else {

                            if let dataJson = responseObject.body.data(using: .utf8) {

                                do {
                                    let financialOverviewEntity = try JSONDecoder().decode(FinancialOverviewEntity.self, from: dataJson)
                                    financialOverviewEntity.status = status ?? ConstantsHTTPCodes.STATUS_OK
                                    observer.onNext(financialOverviewEntity)
                                    observer.onCompleted()

                                } catch {
                                    DLog(message: "Error parsing")
                                    observer.onError(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
                                }

                            } else {
                                DLog(message: "Error parsing")
                                observer.onError(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
                            }

                        }
                    } else {
                        observer.onError(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
                    }
                } else {

                    if let body = responseObject.body, let objectDeserialize = ErrorEntity.deserialize(from: body) {
                        DLog(message: body)
                        observer.onError(ServiceError.GenericErrorEntity(error: objectDeserialize))
                    } else {
                        DLog(message: "Error parsing")
                        observer.onError(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
                    }
                }
            }
            return Disposables.create {
            }
        }
    }
}
