//
//  RewardsDataManager.swift
//  shoppingapp
//
//  Created by jesus.martinez on 18/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import RxSwift
import HandyJSON
import BBVA_Network

class RewardsDataManager: BaseDataManager {

    static let sharedInstance = RewardsDataManager()

    func retrieveBalanceRewards(cardId: String) -> Observable<ModelEntity> {

        let url = ServerHostURL.getCardsURL() + "/cards/\(cardId)/rewards"

        return Observable.create { observer in

            let request: NetworkRequest = self.createRequest(url, self.headers, nil, .useProtocolCachePolicy, 30, NetworkMethod.GET)

            _ = BusManager.sessionDataInstance.networkWorker?.execute(request: request) { (responseObject: Response) in

                if responseObject.error == nil {

                    let dataJson: String = responseObject.body!
                    let status = Int(responseObject.code)

//                    DLog(message: dataJson)

                    if status! >= 200 && status! <= 299 {

                        if status == 204 {

                            let balancesRewardsEntity = BalanceRewardsEntity()
                            balancesRewardsEntity.status = 204
                            observer.onNext(balancesRewardsEntity)
                            observer.onCompleted()

                        } else {

                            if let balancesRewardsEntity = BalanceRewardsEntity.deserialize(from: dataJson) {
                                balancesRewardsEntity.status = status
                                observer.onNext(balancesRewardsEntity)
                                observer.onCompleted()
                                
                            } else {
                                observer.onError(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
                            }

                        }
                    } else {
                        observer.onError(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
                    }
                } else {

                    if let body = responseObject.body, let objectDeserialize = ErrorEntity.deserialize(from: body) {
                        observer.onError(ServiceError.GenericErrorEntity(error: objectDeserialize))
                    } else {
                        DLog(message: "Error parsing")
                        observer.onError(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
                    }
                }
            }
            return Disposables.create {
            }
        }

    }

}
