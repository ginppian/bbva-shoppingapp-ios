//
//  CVVDataManager.swift
//  shoppingapp
//
//  Created by Daniel Garcia on 29/01/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import RxSwift
import HandyJSON
import BBVA_Network

class CVVDataManager: BaseDataManager {

    static let sharedInstance = CVVDataManager()

    func provideCvv(forCardId cardId: String, publicKey: String) -> Observable<CVVEntity> {

        let url = ServerHostURL.getCardsURL() + "/cards/\(cardId)/security-data/?publicKey=\(publicKey)"

        return Observable.create { observer in

            let request: NetworkRequest = self.createRequest(url, self.headerContentType, [], .useProtocolCachePolicy, 30, NetworkMethod.GET)

            _ = BusManager.sessionDataInstance.networkWorker?.execute(request: request) { (responseObject: Response) in

                guard responseObject.error != nil  else {

                    let dataJson: String = responseObject.body!
                    let httpStatus: Int! = Int(responseObject.code)

                    guard let cvvEntity = CVVEntity.deserialize(from: dataJson) else {
                        observer.onError(ServiceError.CouldNotDecodeJSON)
                        return
                    }

                    DLog(message: dataJson)

                    if let httpStatus = httpStatus {
                        switch httpStatus {
                        case ConstantsHTTPCodes.NO_CONTENT:
                            let cvvEntity = CVVEntity()
                            cvvEntity.status = httpStatus
                            observer.onNext(cvvEntity)
                            observer.onCompleted()
                        case 200 ... 299:
                            observer.onNext(cvvEntity)
                            observer.onCompleted()
                        default:
                            observer.onError(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
                        }
                    }

                    return
                }

                guard let errorDeserialized = ErrorEntity.deserialize(from: responseObject.body) else {
                    DLog(message: "Error parsing")
                    let errorEntity = ErrorEntity(message: Localizables.common.key_common_generic_error_text)
                    observer.onError(ServiceError.GenericErrorEntity(error: errorEntity))
                    return
                }

                observer.onError(ServiceError.GenericErrorEntity(error: errorDeserialized))

            }
            return Disposables.create()
        }
    }

}
