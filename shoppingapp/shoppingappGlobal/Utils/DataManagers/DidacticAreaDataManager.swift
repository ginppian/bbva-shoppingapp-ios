//
//  DidacticAreaDataManager.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 8/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import RxSwift
import HandyJSON
import BBVA_Network

class DidacticAreaDataManager: BaseDataManager {

    static var sharedInstance = DidacticAreaDataManager()

    func serviceDidacticArea() -> Observable<ModelEntity> {

        return Observable.create { observer in

            let url = ConstantsURLs.DIDACTIC_AREA_FILE_URL

            let request: NetworkRequest = self.createRequest(url, self.headers, nil, .reloadIgnoringCacheData, 30, NetworkMethod.GET)

            _ = BusManager.sessionDataInstance.networkWorker?.execute(request: request) { (responseObject: Response) in

                if responseObject.error == nil {

                    let dataJson: String = responseObject.body!

//                    DLog(message: dataJson)

                    let status = Int(responseObject.code)

                    if status! == 200 {

                        if let didacticAreasEntity = DidacticAreasEntity.deserialize(from: dataJson) {

                            ConfigPublicManager.sharedInstance().writeJson(withData: dataJson, andFilename: Constants.didactic_area_file)

                            observer.onNext(didacticAreasEntity)
                            observer.onCompleted()
                        } else {
                            observer.onError(ServiceError.CouldNotDecodeJSON)
                        }
                    } else {
                        observer.onError(ServiceError.BadStatus(status: status!))
                    }

                } else {

                    observer.onError(ServiceError.Other(responseObject.error! as NSError))
                }
            }
            return Disposables.create {
            }
        }
    }
}
