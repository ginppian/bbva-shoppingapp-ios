//
//  LogoutDataManager.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 18/10/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation
import RxSwift
import HandyJSON
import BBVA_Network

class LogoutDataManager: BaseDataManager {

    static let sharedInstance = LogoutDataManager()

    func serviceGrantingTicket() -> Observable<ModelEntity> {

        return Observable.create { observer in

            let url = ServerHostURL.getGrantingTicketURL()

            let request: NetworkRequest = self.createRequest(url, self.headerContentType, nil, .useProtocolCachePolicy, 30, NetworkMethod.DELETE)

            _ = BusManager.sessionDataInstance.networkWorker?.execute(request: request) { (responseObject: Response) in

                if responseObject.error == nil {

                    let status = Int(responseObject.code)

                    if status! >= 200 && status! <= 299 {
                        observer.onNext(EmptyModelEntity())
                        observer.onCompleted()
                        
                    } else {
                        observer.onError(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
                    }
                } else {

                    if let body = responseObject.body, let objectDeserialize = ErrorEntity.deserialize(from: body) {
                        observer.onError(ServiceError.GenericErrorEntity(error: objectDeserialize))
                    } else {
                        DLog(message: "Error parsing")
                        observer.onError(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
                    }
                }
            }
            return Disposables.create {
            }
        }
    }

}
