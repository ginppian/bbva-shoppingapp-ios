//
//  LoginDataManager.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 7/3/17.
//  Copyright © 2017 daniel.petrovic. All rights reserved.
//

import Foundation
import RxSwift
import HandyJSON
import BBVA_Network

class LoginLoaderDataManager: BaseDataManager {

    static let sharedInstance = LoginLoaderDataManager()

    func serviceGrantingTicket(loginTransport: ModelTransport, consumerId: String) -> Observable<ModelEntity> {
        return Observable.create { observer in
            
            let granTingTicketEntity = GrandTingTicketEntity.deserialize(from: responseObject.body!)!
            observer.onNext(granTingTicketEntity)
            observer.onCompleted()
                
            
        }
//        return Observable.create { observer in
//
//            let transport = loginTransport as! LoginTransport
//
//            let body = ["authentication": ["consumerID": consumerId, "userID": transport.userId, "authenticationType": "02", "authenticationData": [["authenticationData": [transport.password], "idAuthenticationData": "password"]]], "backendUserRequest": ["userId": "", "accessCode": transport.userId, "dialogId": ""]]
//
//            let url = ServerHostURL.getGrantingTicketURL()
//
//            let request: NetworkRequest = self.createRequest(url, self.headerContentType, body, .useProtocolCachePolicy, 30, NetworkMethod.POST)
//
//            _ = BusManager.sessionDataInstance.networkWorker?.execute(request: request) { (responseObject: Response) in
//
//                if responseObject.error == nil {
//
//                    let status = Int(responseObject.code)
//
//                    if status! >= 200 && status! <= 299 {
//
//                        if let granTingTicketEntity = GrandTingTicketEntity.deserialize(from: responseObject.body!) {
//                            observer.onNext(granTingTicketEntity)
//                            observer.onCompleted()
//
//                        } else {
//                            observer.onError(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
//                        }
//                    } else {
//                        observer.onError(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
//                    }
//                } else {
//
//                    if let body = responseObject.body, let objectDeserialize = ErrorEntity.deserialize(from: body) {
//                        observer.onError(ServiceError.GenericErrorEntity(error: objectDeserialize))
//                    } else {
//                        DLog(message: "Error parsing")
//                        observer.onError(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
//                    }
//                }
//            }
//            return Disposables.create {
//            }
//        }
    }

    func serviceCustomer() -> Observable<ModelEntity> {

        return Observable.create { observer in

            let url = ServerHostURL.getCustomerURL() + "/customers"

            let request: NetworkRequest = self.createRequest(url, self.headers, nil, .useProtocolCachePolicy, 30, NetworkMethod.GET)

            _ = BusManager.sessionDataInstance.networkWorker?.execute(request: request) { (responseObject: Response) in

                if responseObject.error == nil {

//                    DLog(message: responseObject.body!)

                    let status = Int(responseObject.code)

                    if status! >= 200 && status! <= 299 {
                        if let userEntity = UserEntity.deserialize(from: responseObject.body!) {
                            observer.onNext(userEntity)
                            observer.onCompleted()

                        } else {
                            observer.onError(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
                        }
                    } else {
                        observer.onError(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
                    }

                } else {

                    if let body = responseObject.body, let objectDeserialize = ErrorEntity.deserialize(from: body) {
                        observer.onError(ServiceError.GenericErrorEntity(error: objectDeserialize))
                    } else {
                        DLog(message: "Error parsing")
                        observer.onError(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
                    }
                }
            }
            return Disposables.create {
            }
        }
    }

    func requestGrantingTicket(withAnonymousInformation anonymousInformation: AnonymousInformationBO) -> Observable<ModelEntity> {

        return Observable.create { observer in

            let body = ["authentication": ["consumerID": anonymousInformation.consumerId, "userID": anonymousInformation.userId, "authenticationType": "61", "authenticationData": []], "backendUserRequest": ["userId": "", "accessCode": anonymousInformation.userId, "dialogId": ""]]

            let url = ServerHostURL.getGrantingTicketURL()

            let request: NetworkRequest = self.createRequest(url, self.headerContentType, body, .useProtocolCachePolicy, 30, NetworkMethod.POST)

            _ = BusManager.sessionDataInstance.networkWorker?.execute(request: request) { (responseObject: Response) in

                if responseObject.error == nil {

                    let status = Int(responseObject.code)

                    if status! >= 200 && status! <= 299 {

                        if let granTingTicketEntity = GrandTingTicketEntity.deserialize(from: responseObject.body!) {
                            observer.onNext(granTingTicketEntity)
                            observer.onCompleted()
                            
                        } else {
                            observer.onError(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
                        }
                    } else {
                        observer.onError(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
                    }
                } else {

                    if let body = responseObject.body, let objectDeserialize = ErrorEntity.deserialize(from: body) {
                        observer.onError(ServiceError.GenericErrorEntity(error: objectDeserialize))
                    } else {
                        DLog(message: "Error parsing")
                        observer.onError(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
                    }
                }
            }
            return Disposables.create {
            }
        }
    }

}
