//
//  BaseDataManager.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 12/9/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation
import BBVA_Network
import HandyJSON

class BaseDataManager {

    static let currentEndPoint: BBVAEndPoint = EndPointManager.sharedClient().currentEndPoint
    
    var headers: [String: String] = [:]

    var headerContentType = ["Content-Type": "application/json", "Charset": "UTF-8"]

    func createRequest(_ url: String, _ headers: [String: String], _ body: Any?, _ cachePolicy: NSURLRequest.CachePolicy, _ timeout: Int32, _ method: NetworkMethod) -> NetworkRequest {

        return NetworkRequest(url: url, headers: headers, parameters: body, cachePolicy: cachePolicy, timeout: timeout, method: method)

    }

}
