//
//  LimitsDataManager.swift
//  shoppingapp
//
//  Created by jesus.martinez on 24/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import RxSwift
import HandyJSON
import BBVA_Network

class LimitsDataManager: BaseDataManager {

    static let sharedInstance = LimitsDataManager()

    func provideLimits(forCardId cardId: String) -> Observable<ModelEntity> {

        let url = ServerHostURL.getCardsURL() + "/cards/\(cardId)/limits"

        return Observable.create { observer in

            let request: NetworkRequest = self.createRequest(url, self.headers, nil, .useProtocolCachePolicy, 30, NetworkMethod.GET)

            _ = BusManager.sessionDataInstance.networkWorker?.execute(request: request) { (responseObject: Response) in

                if responseObject.error == nil {

                    let dataJson: String = responseObject.body!
                    let status = Int(responseObject.code)

                    DLog(message: dataJson)

                    if status! >= 200 && status! <= 299 {

                        if status == 204 {

                            let limitsEntity = LimitsEntity()
                            limitsEntity.status = 204
                            observer.onNext(limitsEntity)
                            observer.onCompleted()

                        } else {

                            if let limitsEntity = LimitsEntity.deserialize(from: dataJson) {
                                limitsEntity.status = status
                                observer.onNext(limitsEntity)
                                observer.onCompleted()
                            } else {
                                observer.onError(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
                            }

                        }
                    } else {
                        observer.onError(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
                    }
                } else {

                    if let body = responseObject.body, let objectDeserialize = ErrorEntity.deserialize(from: body) {
                        observer.onError(ServiceError.GenericErrorEntity(error: objectDeserialize))
                    } else {
                        DLog(message: "Error parsing")
                        observer.onError(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
                    }
                }
            }
            return Disposables.create {
            }
        }

    }

    func updateLimit(forCardId cardId: String, withLimitId limitId: String, withAmountLimit amountLimit: AmountCurrencyBO) -> Observable<ModelEntity> {

        let url = ServerHostURL.getCardsURL() + "/cards/\(cardId)/limits/\(limitId)"

        return Observable.create { observer in

            let amountLimits = [["amount": amountLimit.amount, "currency": amountLimit.currency]]
            let body = ["amountLimits": amountLimits]

            let request: NetworkRequest = self.createRequest(url, self.headerContentType, body, .useProtocolCachePolicy, 30, NetworkMethod.PUT)

            _ = BusManager.sessionDataInstance.networkWorker?.execute(request: request) { (responseObject: Response) in

                if responseObject.error == nil {

                    let dataJson: String = responseObject.body!
                    let status = Int(responseObject.code)

                    DLog(message: dataJson)

                    if status! >= 200 && status! <= 299 {

                        if let limitsEntity = OperationResultEntity.deserialize(from: dataJson) {
                            limitsEntity.status = status
                            observer.onNext(limitsEntity)
                            observer.onCompleted()
                        } else {
                            observer.onError(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
                        }

                    } else {
                        observer.onError(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
                    }

                } else {

                    if let body = responseObject.body, let objectDeserialize = ErrorEntity.deserialize(from: body) {
                        observer.onError(ServiceError.GenericErrorEntity(error: objectDeserialize))
                    } else {
                        DLog(message: "Error parsing")
                        observer.onError(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
                    }
                }
            }
            return Disposables.create {
            }
        }

    }

}
