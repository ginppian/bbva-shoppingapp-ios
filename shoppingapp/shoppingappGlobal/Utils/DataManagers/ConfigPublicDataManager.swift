//
//  ConfigPublicDataManager.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 23/01/2018.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import RxSwift
import HandyJSON
import BBVA_Network

class ConfigPublicDataManager: BaseDataManager {

    static var sharedInstance = ConfigPublicDataManager()

    func provideHelpConfigFile() -> Observable<ModelEntity> {

        let url = ConstantsURLs.HELP_CONFIG_FILE_URL

        return self.provideHelpConfigData(withURL: url)

    }

    private func provideHelpConfigData(withURL url: String) -> Observable<ModelEntity> {

        return Observable.create { observer in

            let request: NetworkRequest = self.createRequest(url, self.headers, nil, .reloadIgnoringCacheData, 30, NetworkMethod.GET)

            _ = BusManager.sessionDataInstance.networkWorker?.execute(request: request) { (responseObject: Response) in

                if responseObject.error == nil {

                    let dataJson: String = responseObject.body!

//                    DLog(message: dataJson)

                    let status = Int(responseObject.code)

                    if status == 200 {

                        if let configFilesEntity = ConfigFilesEntity.deserialize(from: dataJson) {

                            ConfigPublicManager.sharedInstance().writeToPlist(forItems: dataJson, withFilename: Constants.help_config_file)

                            configFilesEntity.status = status
                            observer.onNext(configFilesEntity)
                            observer.onCompleted()
                        } else {
                            observer.onError(ServiceError.CouldNotDecodeJSON)
                        }

                    } else {
                        observer.onError(ServiceError.BadStatus(status: status!))
                    }

                } else {
                    observer.onError(ServiceError.Other(responseObject.error! as NSError))
                }
            }
            return Disposables.create {
            }
        }
    }

}
