//
//  TransactionsDataManager.swift
//  shoppingapp
//
//  Created by jesus.martinez on 5/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation
import RxSwift
import HandyJSON
import BBVA_Network

class TransactionsDataManager: BaseDataManager {

    static let sharedInstance = TransactionsDataManager()

    func provideTransactions(forCardId cardId: String) -> Observable<ModelEntity> {

        let url = ServerHostURL.getCardsURL() + "/cards/\(cardId)/transactions/"

        return self.provideTransactions(withURL: url)

    }
    
    func provideAccountTransactions(filteredBy transactionTransport: TransactionFilterTransport?, for accountID: String) -> Observable<ModelEntity> {
        let url = ServerHostURL.getAccountsURL() + "/accounts/\(accountID)/transactions"
        return callProvideTransactions(withTransactionFilterTransport: transactionTransport, and: url)
    }

    func provideTransactions(withTransactionFilterTransport transactionFilterTransport: TransactionFilterTransport?, forCardId cardId: String) -> Observable<ModelEntity> {
        let url = ServerHostURL.getCardsURL() + "/cards/\(cardId)/transactions"
        return callProvideTransactions(withTransactionFilterTransport: transactionFilterTransport, and: url)
    }

    func provideTransactions(forNextPage nextPage: String) -> Observable<ModelEntity> {
        let url = nextPage
        return self.provideTransactions(withURL: url)
    }
    
    // MARK: Private Functions
    
    fileprivate func callProvideTransactions(withTransactionFilterTransport transactionFilterTransport: TransactionFilterTransport?, and url: String) -> Observable<ModelEntity> {
        
        var parametersDic = [String: AnyObject]()
        
        if let transactionFilterTransport = transactionFilterTransport {
            
            if !Tools.isStringNilOrEmpty(withString: transactionFilterTransport.conceptText) {
                parametersDic["concept"] = transactionFilterTransport.conceptText as AnyObject
            }
            
            if !Tools.isStringNilOrEmpty(withString: transactionFilterTransport.fromDateText) {
                parametersDic["fromOperationDate"] = transactionFilterTransport.fromDateText as AnyObject
            }
            
            if !Tools.isStringNilOrEmpty(withString: transactionFilterTransport.toDateText) {
                parametersDic["toOperationDate"] = transactionFilterTransport.toDateText as AnyObject
            }
            
            if !Tools.isStringNilOrEmpty(withString: transactionFilterTransport.fromAmountText) {
                parametersDic["fromLocalAmount.amount"] = transactionFilterTransport.fromAmountText as AnyObject
            }
            
            if !Tools.isStringNilOrEmpty(withString: transactionFilterTransport.toAmountText) {
                parametersDic["toLocalAmount.amount"] = transactionFilterTransport.toAmountText as AnyObject
            }
            
            if transactionFilterTransport.filtertype == TransactionFilterType.income {
                parametersDic["transactionType.id"] = "REFUND&transactionType.id=CASH_INCOME" as AnyObject
            } else if transactionFilterTransport.filtertype == TransactionFilterType.expense {
                parametersDic["transactionType.id"] = "PURCHASE&transactionType.id=CASH_WITHDRAWAL" as AnyObject
                
            }
            
            if let pageSize = transactionFilterTransport.pageSize {
                parametersDic["pageSize"] = "\(pageSize)" as AnyObject
            }
            
        }
        
        let parametersString = parametersDic.stringFromHttpParameters(withEncoding: true) as String
        
        let newurl = url + (parametersString.isEmpty ? "" : "?" + parametersString)
        
        return self.provideTransactions(withURL: newurl)
        
    }

    private func provideTransactions(withURL url: String) -> Observable<ModelEntity> {

        return Observable.create { observer in

            let request: NetworkRequest = self.createRequest(url, self.headers, nil, .useProtocolCachePolicy, 30, NetworkMethod.GET)

            _ = BusManager.sessionDataInstance.networkWorker?.execute(request: request) { (responseObject: Response) in

                if responseObject.error == nil {

                    let dataJson: String = responseObject.body!

                    DLog(message: dataJson)

                    let status = Int(responseObject.code)

                    if status! >= 200 && status! <= 299 {

                        if status == 204 {

                            let transactionEntity = TransactionsEntity()
                            transactionEntity.status = 204
                            observer.onNext(transactionEntity)
                            observer.onCompleted()

                        } else {

                            if let transactionsEntity = TransactionsEntity.deserialize(from: dataJson) {
                                transactionsEntity.status = status
                                observer.onNext(transactionsEntity)
                                observer.onCompleted()
                            } else {
                                observer.onError(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
                            }
                        }
                    } else {
                        observer.onError(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
                    }
                } else {

                    if let body = responseObject.body, let objectDeserialize = ErrorEntity.deserialize(from: body) {
                        observer.onError(ServiceError.GenericErrorEntity(error: objectDeserialize))
                    } else {
                        DLog(message: "Error parsing")
                        observer.onError(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
                    }
                }
            }
            return Disposables.create {
            }
        }
    }

}
