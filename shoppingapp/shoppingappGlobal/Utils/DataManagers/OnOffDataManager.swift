//
//  OnOffDataManager.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 02/11/2017.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation
import RxSwift
import HandyJSON
import BBVA_Network

class OnOffDataManager: BaseDataManager {

    static let sharedInstance = OnOffDataManager()
    /// Invokes to change card status
    ///
    /// - Parameters:
    ///   - cardId: the cardId to change card status
    ///   - status: the switch status
    /// - Returns: gets updated card

    static let selectedEnvironment = Configuration.selectedEnvironment()

    func provideUpdatedCardStatus(forCardId cardId: String, andStatus status: Bool) -> Observable<ModelEntity> {

        let url = ServerHostURL.getCardsURL() + "/cards/\(cardId)/activations/ON_OFF"

        return self.provideUpdatedCardStatus(withURL: url, andStatus: status)

    }

    private func provideUpdatedCardStatus(withURL url: String, andStatus status: Bool) -> Observable<ModelEntity> {

        return Observable.create { observer in

            let body = NSDictionary(dictionary: ["isActive": status])

            let request: NetworkRequest = self.createRequest(url, self.headerContentType, body, .useProtocolCachePolicy, 30, NetworkMethod.PATCH)

            _ = BusManager.sessionDataInstance.networkWorker?.execute(request: request) { (responseObject: Response) in

                if responseObject.error == nil {

                    let dataJson: String = responseObject.body!

                    DLog(message: dataJson)

                    let status = Int(responseObject.code)

                    if status! >= 200 && status! <= 299 {

                        if status == 204 {

                            let activationsEntity = ActivationsEntity()
                            activationsEntity.status = 204
                            observer.onNext(activationsEntity)
                            observer.onCompleted()

                        } else {

                            if let activationsEntity = ActivationsEntity.deserialize(from: dataJson) {
                                activationsEntity.status = status
                                observer.onNext(activationsEntity)
                                observer.onCompleted()
                            } else {
                                observer.onError(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
                            }
                        }
                    } else {
                        observer.onError(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
                    }

                } else {

                    if let body = responseObject.body, let objectDeserialize = ErrorEntity.deserialize(from: body) {
                        observer.onError(ServiceError.GenericErrorEntity(error: objectDeserialize))
                    } else {
                        DLog(message: "Error parsing")
                        observer.onError(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
                    }
                }
            }
            return Disposables.create {
            }
        }
    }

    func convertToDictionary(from text: String) -> [String: String]? {
        guard let data = text.data(using: .utf8) else {
            return nil
        }
        let anyResult = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: AnyObject]
        return anyResult as? [String: String]
    }

}
