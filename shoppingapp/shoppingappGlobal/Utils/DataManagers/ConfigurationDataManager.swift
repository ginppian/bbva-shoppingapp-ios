//
//  ConfigurationDataManager.swift
//  shoppingapp
//
//  Created by jesus.martinez on 3/8/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation
import RxSwift

class ConfigurationDataManager {

    static var sharedInstance = ConfigurationDataManager()

    func provideMainCurrency() -> Observable<ModelEntity> {

        return Observable.create { observer in

            observer.onNext(Configuration.provideCurrencyFromConfiguration())
            observer.onCompleted()

            return Disposables.create()
        }
    }

    func provideTransactionFilterTypes() -> Observable<[ModelEntity]> {

        return Observable.create { observer in

            observer.onNext(Configuration.provideTransactionTypeFilters())
            observer.onCompleted()

            return Disposables.create()
        }
    }

    func provideIdentificationDocuments() -> Observable<[ModelEntity]> {

        return Observable.create { observer in

            observer.onNext(Configuration.identificationDocuments())
            observer.onCompleted()

            return Disposables.create()
        }
    }

    func provideLeftMenuOptions() -> Observable<[ModelEntity]> {

        return Observable.create { observer in

            observer.onNext(Configuration.leftMenuOptions())
            observer.onCompleted()

            return Disposables.create()
        }
    }

    func provideConsumerId() -> Observable<String> {

        return Observable.create { observer in

            observer.onNext(Configuration.consumerId())
            observer.onCompleted()

            return Disposables.create()
        }
    }

    func provideAnonymousInformation() -> Observable<ModelEntity> {

        return Observable.create { observer in

            observer.onNext(Configuration.anonymousInformation())
            observer.onCompleted()

            return Disposables.create()
        }
    }
}
