//
//  CardsDataManager.swift
//  shoppingapp
//
//  Created by ruben.fernandez on 12/9/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation
import RxSwift

struct EtagDataManager {

    static let sharedInstance = EtagDataManager()

    func getImage(withUrl url: String) -> Observable<EtagEntity> {

        return Observable.create { observer in

            let headers: [String: String] = [:]

            BaseDataManager.currentEndPoint.get(url, headers: headers, parameters: nil, timeout: 30) { responseObject in

                guard responseObject.error == nil else {
                    observer.onError(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
                    return
                }

                do {

                    guard let headers = responseObject.header, let data = headers.data(using: .utf8), let headersDict = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any], let status = responseObject.code, Int(status) == 200, let responseData = responseObject.bodyObject as? Data, let headerEtag = headersDict["Etag"] as? String  else {
                        observer.onError(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
                        return
                    }

                    let infoImage = EtagEntity(withData: responseData, withEtag: headerEtag)
                    observer.onNext(infoImage)
                    observer.onCompleted()
                } catch {
                    observer.onError(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))

                    DLog(message: "Error de cabeceras")
                }

            }
            return Disposables.create {
            }
        }
    }

    func checkEtagImage(withUrl url: String, withEtag etag: String ) -> Observable<EtagEntity> {

        return Observable.create { observer in

            let headers: [String: String] = ["If-None-Match": etag]

            BaseDataManager.currentEndPoint.get(url, cachePolicy: .reloadIgnoringCacheData, headers: headers, parameters: nil, timeout: 30) {  responseObject in

                guard responseObject.error == nil else {
                    observer.onError(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
                    return
                }

                do {

                    guard let headers = responseObject.header, let data = headers.data(using: .utf8), let headersDict = try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any], let status = responseObject.code, Int(status) == 200, let responseData = responseObject.bodyObject as? Data, let headerEtag = headersDict["Etag"] as? String  else {
                        observer.onError(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))
                        return
                    }

                    let infoImage = EtagEntity(withData: responseData, withEtag: headerEtag)
                    observer.onNext(infoImage)
                    observer.onCompleted()

                } catch {
                    observer.onError(ServiceError.GenericErrorEntity(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text)))

                    DLog(message: "Error de cabeceras")
                }

            }
            return Disposables.create {
            }
        }
    }

}
