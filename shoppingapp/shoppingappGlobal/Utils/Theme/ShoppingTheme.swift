//
//  ShoppingTheme.swift
//  shoppingapp
//
//  Created by jesus.martinez on 24/9/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CellsNativeWidgets

open class ShoppingTheme: Theme {
    
    public var primaryButtonStyle: ButtonStyleProtocol?
    
    public var secondaryButtonStyle: ButtonStyleProtocol?
    
    public var primaryDarkBackgroundButtonStyle: ButtonStyleProtocol?
    
    public var secondaryDarkBackgroundButtonStyle: ButtonStyleProtocol? = SecondaryButtonDarkBackgroundStyleShopping()
    
    public var titleLabelStyle: LabelStyleProtocol?
    
    public var subtitleLabelStyle: LabelStyleProtocol?
    
    public var normalLabelStyle: LabelStyleProtocol?
    
    public var descriptionLabelStyle: LabelStyleProtocol?
    
    public var headlineLabelStyle: LabelStyleProtocol?
    
    public var usernameLabelStyle: LabelStyleProtocol?
    
    public var loginLabelErrorStyle: LabelStyleProtocol?
    
    public var coreBlueFractalTextFieldStyle: TextFieldStyleProtocol?
    
    public var errorTextFieldStyle: TextFieldStyleProtocol?
    
    public var welcomePageControlStyle: PageControlStyleProtocol? = WelcomePageControlStyleShopping()
    
    public static let ID: String = String(describing: ShoppingTheme.self)
    
    public init() {
        
    }
    
}

class SecondaryButtonDarkBackgroundStyleShopping: ButtonStyleProtocol {
    
    var textColorEnabled: UIColor? = .BBVAWHITE
    
    var textColorDisabled: UIColor? = UIColor.BBVAWHITE.withAlphaComponent(0.3)
    
    var textColorSelected: UIColor? = .BBVAWHITE
    
    var font: UIFont?
    
    var cornerRadius: CGFloat? = 1.0
    
    var backgroundColorEnabled: UIColor? = .MEDIUMBLUE
    
    var backgroundColorDisabled: UIColor? = UIColor.BBVA300.withAlphaComponent(0.3)
    
    var backgroundColorSelected: UIColor? = .DARKMEDIUMBLUE
    
    var horizontalTextAligment: UIControlContentHorizontalAlignment? = .center
    
    init() {
        font = Tools.setFontBold(size: 14)
    }
}

class WelcomePageControlStyleShopping: PageControlStyleProtocol {
    var currentPageIndicatorTintColor: UIColor? = .BBVAWHITE
    
    var pageIndicatorTintColor: UIColor? = UIColor.BBVAWHITE.withAlphaComponent(0.3)
    
    init() {}
}
