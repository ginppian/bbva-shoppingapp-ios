//
//  CircularButton.swift
//  CustomButton
//
//  Created by jesus.martinez on 11/10/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation
import UIKit

@IBDesignable class CircularButton: UIButton {

    @IBInspectable var image: UIImage = UIImage() {
        didSet {
            iconImageView?.image = image
        }
    }

    @IBInspectable var imageHighlighted: UIImage?

    @IBInspectable var imageWidth: CGFloat = 0.0
    @IBInspectable var imageHeight: CGFloat = 0.0

    @IBInspectable var normalColor: UIColor = .black {
        didSet {
            bottomTitleLabel?.textColor = normalColor

        }
    }

    @IBInspectable var highlightedColor: UIColor = .gray

    @IBInspectable var backgroundNormalColor: UIColor = .gray {
        didSet {
            circularContainerView?.backgroundColor = backgroundNormalColor

        }
    }

    @IBInspectable var backgroundHighlightedColor: UIColor = .gray

    @IBInspectable var numberOfLines: Int = 1 {
        didSet {
            bottomTitleLabel?.numberOfLines = numberOfLines

        }
    }

    @IBInspectable var showBottomTitle: Bool = true

    @IBInspectable var showCircularContainer: Bool = false

    @IBInspectable var imageSpaceTop: CGFloat = 20.0

    @IBInspectable var titleSpaceTop: CGFloat = 12.0

    @IBInspectable var bottomTitleText: String = "title" {
        didSet {
            bottomTitleLabel?.text = bottomTitleText
        }
    }

    @IBInspectable var fontNamed: String = "System" {
        didSet {
            bottomTitleLabel?.font = UIFont(name: fontNamed, size: fontSize)
        }
    }

    @IBInspectable var fontSize: CGFloat = 15.0 {
        didSet {
            bottomTitleLabel?.font = UIFont(name: fontNamed, size: fontSize)
        }
    }

    var circularContainerView: UIView?
    var iconImageView: UIImageView?
    var bottomTitleLabel: UILabel?

    override var isHighlighted: Bool {
        willSet {
            circularContainerView?.backgroundColor = newValue ? backgroundHighlightedColor : backgroundNormalColor
            iconImageView?.image = newValue ? imageHighlighted : image
            bottomTitleLabel?.textColor = newValue ? highlightedColor : normalColor
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()

        setupView()
        configureView()
    }

    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()

        setupView()
        configureView()
    }

    private func setupView() {

        let minDimension = min(bounds.width, bounds.height)

        if showCircularContainer {

            circularContainerView = UIView()
            circularContainerView!.layer.cornerRadius = minDimension / 2
            circularContainerView!.translatesAutoresizingMaskIntoConstraints = false

            addSubview(circularContainerView!)

            NSLayoutConstraint(item: circularContainerView!, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1.0, constant: 0).isActive = true
            NSLayoutConstraint(item: circularContainerView!, attribute: .topMargin, relatedBy: .equal, toItem: self, attribute: .topMargin, multiplier: 1.0, constant: 0.0).isActive = true
            NSLayoutConstraint(item: circularContainerView!, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: minDimension).isActive = true
            NSLayoutConstraint(item: circularContainerView!, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: minDimension).isActive = true

            iconImageView = UIImageView(image: image)
            iconImageView!.translatesAutoresizingMaskIntoConstraints = false
            iconImageView!.contentMode = .scaleAspectFit

            circularContainerView!.addSubview(iconImageView!)

            NSLayoutConstraint(item: iconImageView!, attribute: .centerX, relatedBy: .equal, toItem: circularContainerView!, attribute: .centerX, multiplier: 1.0, constant: 0).isActive = true
            NSLayoutConstraint(item: iconImageView!, attribute: .centerY, relatedBy: .equal, toItem: circularContainerView!, attribute: .centerY, multiplier: 1.0, constant: 0).isActive = true

            NSLayoutConstraint(item: iconImageView!, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: imageHeight).isActive = true
            NSLayoutConstraint(item: iconImageView!, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: imageWidth).isActive = true
        } else {

            iconImageView = UIImageView(image: image)
            iconImageView!.translatesAutoresizingMaskIntoConstraints = false
            iconImageView!.contentMode = .scaleAspectFit

            addSubview(iconImageView!)

            NSLayoutConstraint(item: iconImageView!, attribute: .centerX, relatedBy: .equal, toItem: self, attribute: .centerX, multiplier: 1.0, constant: 0).isActive = true
            NSLayoutConstraint(item: iconImageView!, attribute: .topMargin, relatedBy: .equal, toItem: self, attribute: .topMargin, multiplier: 1.0, constant: imageSpaceTop).isActive = true
            NSLayoutConstraint(item: iconImageView!, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: imageHeight).isActive = true
            NSLayoutConstraint(item: iconImageView!, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: imageWidth).isActive = true
        }

        if showBottomTitle {

            bottomTitleLabel = UILabel()
            bottomTitleLabel!.translatesAutoresizingMaskIntoConstraints = false

            addSubview(bottomTitleLabel!)

            if showCircularContainer {

                NSLayoutConstraint(item: bottomTitleLabel!, attribute: .topMargin, relatedBy: .equal, toItem: circularContainerView!, attribute: .bottom, multiplier: 1.0, constant: titleSpaceTop).isActive = true
                NSLayoutConstraint(item: bottomTitleLabel!, attribute: .leadingMargin, relatedBy: .equal, toItem: self, attribute: .leadingMargin, multiplier: 1.0, constant: 0.0).isActive = true
                NSLayoutConstraint(item: bottomTitleLabel!, attribute: .trailingMargin, relatedBy: .equal, toItem: self, attribute: .trailingMargin, multiplier: 1.0, constant: 0.0).isActive = true
            } else {

                NSLayoutConstraint(item: bottomTitleLabel!, attribute: .topMargin, relatedBy: .equal, toItem: iconImageView!, attribute: .bottom, multiplier: 1.0, constant: titleSpaceTop).isActive = true
                NSLayoutConstraint(item: bottomTitleLabel!, attribute: .leadingMargin, relatedBy: .equal, toItem: self, attribute: .leadingMargin, multiplier: 1.0, constant: 0.0).isActive = true
                NSLayoutConstraint(item: bottomTitleLabel!, attribute: .trailingMargin, relatedBy: .equal, toItem: self, attribute: .trailingMargin, multiplier: 1.0, constant: 0.0).isActive = true
            }
        }

    }

    private func configureView() {

        setTitle("", for: .normal)
        setTitle("", for: .highlighted)
        setTitle("", for: .disabled)

        imageView?.image = nil
        backgroundColor = .clear

        circularContainerView?.backgroundColor = normalColor
        circularContainerView?.isUserInteractionEnabled = false

        iconImageView?.isUserInteractionEnabled = false

        bottomTitleLabel?.textColor = normalColor
        bottomTitleLabel?.numberOfLines = numberOfLines
        bottomTitleLabel?.text = bottomTitleText
        bottomTitleLabel?.font = UIFont(name: fontNamed, size: fontSize)
        bottomTitleLabel?.textAlignment = .center
        bottomTitleLabel?.isUserInteractionEnabled = false

    }

}
