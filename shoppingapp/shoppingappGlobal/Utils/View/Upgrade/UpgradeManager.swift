//
//  UpgradeManager.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 4/12/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

public class UpgradeManager {
    
    // MARK: - Constants
    
    private static let initialFrame = CGRect(x: 0, y: UIScreen.main.bounds.size.height, width: UIScreen.main.bounds.size.width, height: UIScreen.main.bounds.size.height)
    private static let finalFrame = UIScreen.main.bounds
    private static let transitionAnimationDuration = 0.5
    
    // MARK: - Properties
    
    static var instance: UpgradeManager?
    
    class func sharedInstance() -> UpgradeManager {
        
        guard instance != nil else {
            
            let viewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: UpgradeNotificationViewController.ID)! as! UpgradeNotificationViewController
            let navigationController = UINavigationController(rootViewController: viewController)
            
            let window = UIWindow(frame: initialFrame)
            window.windowLevel = UIWindowLevelStatusBar
            window.rootViewController = navigationController
            
            instance = UpgradeManager(window: window, viewController: viewController)
            
            return instance!
        }
        
        return instance!
    }
    
    fileprivate let window: UIWindow
    fileprivate var viewController: UpgradeNotificationViewController?
    
    // MARK: - Init
    
    public init() {
        
        window = UIWindow()
        viewController = UpgradeNotificationViewController()
    }
    
    private init(window: UIWindow, viewController: UpgradeNotificationViewController) {
        
        self.window = window
        self.viewController = viewController
    }
    
    func show(withModel model: Model, completion: (() -> Void)? = nil) {
        
        window.isHidden = false
        
        if viewController == nil {
            viewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: UpgradeNotificationViewController.ID)! as? UpgradeNotificationViewController
            let navigationController = UINavigationController(rootViewController: viewController!)
            window.rootViewController = navigationController
        }
        
        viewController?.initModel(withModel: model)
        
        UIView.animate(withDuration: UpgradeManager.transitionAnimationDuration, delay: 0, options: [.curveEaseInOut], animations: {
            
            self.window.frame = UpgradeManager.finalFrame
            
        }, completion: { _ in
        })
    }
    
    func hide(completion: (() -> Void)? = nil) {
        
        UIView.animate(withDuration: UpgradeManager.transitionAnimationDuration, delay: 0, options: [.curveEaseInOut], animations: {
            
            self.window.frame = UpgradeManager.initialFrame
            
        }, completion: { _ in
            
            self.window.isHidden = true
            self.window.rootViewController = nil
            self.viewController = nil
        })
    }
}
