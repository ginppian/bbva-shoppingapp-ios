//
//  LoadingManager.swift
//  shoppingapp
//
//  Created by jesus.martinez on 26/7/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

final class LoadingManager {

    // MARK: - Properties

    private static let loadingManager: LoadingManager = {

        let frameScreen = UIScreen.main.bounds

        let viewController = LoadingViewController(nibName: "LoadingViewController", bundle: nil)

        let window = UIWindow(frame: frameScreen)
        window.windowLevel = UIWindowLevelStatusBar
        window.rootViewController = viewController

        let loadingManager = LoadingManager(window: window, loadingViewController: viewController)

        return loadingManager
    }()

    let window: UIWindow
    let loadingViewController: LoadingViewController

    // MARK: - Init

    private init(window: UIWindow, loadingViewController: LoadingViewController) {

        self.window = window
        self.loadingViewController = loadingViewController
    }

    // MARK: - Accessors

    class func shared() -> LoadingManager {
        return loadingManager
    }

    func show(completion: (() -> Void)? = nil) {

        window.isHidden = false
        loadingViewController.show(completion: completion)
    }

    func hide(completion: (() -> Void)? = nil) {

        window.isHidden = true
        loadingViewController.hide(completion: completion)
    }
}
