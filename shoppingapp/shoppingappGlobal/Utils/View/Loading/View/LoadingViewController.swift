//
//  LoadingViewController.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 07/11/2017.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation
import Lottie

class LoadingViewController: BaseViewController {

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        BusManager.sessionDataInstance.disableSwipeLateralMenu()
    }

    override func viewDidLoad() {

        super.viewDidLoad()

        configureConstraints()
    }

     var lottieLoader: LOTAnimationView = {

        let lottieLoader = LOTAnimationView(name: ConstantsLottieAnimations.loader)
        lottieLoader.translatesAutoresizingMaskIntoConstraints = false
        lottieLoader.loopAnimation = true
        return lottieLoader
    }()

    func configureConstraints() {

        view.addSubview(lottieLoader)

        let widthConstraint = NSLayoutConstraint(item: lottieLoader, attribute: .width, relatedBy: .equal,
                                                 toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 48)

        let heightConstraint = NSLayoutConstraint(item: lottieLoader, attribute: .height, relatedBy: .equal,
                                                  toItem: nil, attribute: .notAnAttribute, multiplier: 1.0, constant: 48)

        let xConstraint = NSLayoutConstraint(item: lottieLoader, attribute: .centerX, relatedBy: .equal, toItem: view, attribute: .centerX, multiplier: 1, constant: 0)

        let yConstraint = NSLayoutConstraint(item: lottieLoader, attribute: .centerY, relatedBy: .equal, toItem: view, attribute: .centerY, multiplier: 1, constant: 0)

        NSLayoutConstraint.activate([widthConstraint, heightConstraint, xConstraint, yConstraint])
    }

    func show(completion: (() -> Void)?) {

        lottieLoader.play()

        completion?()
    }

    func hide(completion: (() -> Void)?) {

        lottieLoader.pause()

        completion?()
    }

    deinit {
        DLog(message: "LoadingViewController deinitialized")
    }
}
