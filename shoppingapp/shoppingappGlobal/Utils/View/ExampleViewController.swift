//
//  ExampleViewController.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 18/9/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit
import RxSwift
import Lottie
import Stellar
import IQKeyboardManagerSwift
import SVGKit
import Material
import CellsNativeCore
import CellsNativeComponents

class ExampleViewController: BaseViewController {

    // MARK: Constants

    static let ROUTE: String = "Example"

    // MARK: Properties

    var presenter: ExamplePresenterProtocol!

    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        ((self.navigationController as! AppNavigator).webViewContainer as! CellsPageViewController).registerComponent(tag: presenter.routerTag, component: self.presenter! as! CellsComponent)

    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        ((self.navigationController as! AppNavigator).webViewContainer as! CellsPageViewController).unregisterComponent(tag: presenter.routerTag)
    }
}

extension ExampleViewController {

}

// MARK: LoginViewProtocol implementation

extension ExampleViewController: ExampleViewProtocol {

    func showError(error: ModelBO) {

    }

    func changeColorEditTexts() {

    }
}

// MARK: - APPIUM

private extension ExampleViewController {

    func setAccessibilities() {

        #if APPIUM

        #endif

    }
}
