//
//  BVAViewController.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 6/3/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit
import CellsNativeCore
import IQKeyboardManagerSwift

class BaseViewController: CellsBaseViewController, UINavigationControllerDelegate {

    // MARK: vars
    var containerIndicator: UIView!
    var menuButton: UIButton!
    static let paddingTitleNavigationBar: CGFloat = 65.0
    let barButtonDefaultSize = CGSize(width: 40.0, height: 40.0)
    let barButtonDefaultImageSize = CGSize(width: 20.0, height: 20.0)
    var barButtonDefaultEdgeInsetsAlignLeft = UIEdgeInsets.zero
    var barButtonDefaultEdgeInsetsAlignRight = UIEdgeInsets.zero

    fileprivate var observerDismissPresentedModal: Any?
    
    // MARK: life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        barButtonDefaultEdgeInsetsAlignLeft = UIEdgeInsets(top: (barButtonDefaultSize.height - barButtonDefaultImageSize.height) / 2, left: 0, bottom: (barButtonDefaultSize.height - barButtonDefaultImageSize.height) / 2, right: (barButtonDefaultSize.width - barButtonDefaultImageSize.width))
        
        barButtonDefaultEdgeInsetsAlignRight = UIEdgeInsets(top: (barButtonDefaultSize.height - barButtonDefaultImageSize.height) / 2, left: (barButtonDefaultSize.width - barButtonDefaultImageSize.width), bottom: (barButtonDefaultSize.height - barButtonDefaultImageSize.height) / 2, right: 0)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        observerDismissPresentedModal = NotificationCenter.default.addObserver(forName: PresentingViewsManager.viewControllerDismissedNotification, object: nil, queue: nil) { [weak self] _ in
            guard let `self` = self else {
                return
            }
            
            if self.isVisible() {
                self.viewWillAppearAfterDismiss()
            }
        }

        BusManager.sessionDataInstance.enableSwipeLateralMenu()
        BusManager.sessionDataInstance.hideWebview(false)

        navigationController?.delegate = self
    }
    
    // This method is called after the presented modal was dismiss and the current VC is visible
    func viewWillAppearAfterDismiss() {
        // Override only for tracking analytics purposes
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if let observer = observerDismissPresentedModal {
            NotificationCenter.default.removeObserver(observer)
        }
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }

    func setNavigationBarTransparent() {

        navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.isTranslucent = true
        navigationController?.navigationBar.isOpaque = false
        navigationController?.view.backgroundColor = UIColor.NAVY.withAlphaComponent(0.95)
        navigationController?.navigationBar.backgroundColor = .clear

        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        navigationController?.navigationBar.shadowImage = UIImage()
    }

    func setNavigationBarDefault() {
        
        navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = UIColor.NAVY
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationController?.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: Tools.setFontBook(size: 18),
                                                                        NSAttributedStringKey.foregroundColor: UIColor.BBVAWHITE]

        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
    }

    func setNavigationBarWithColor(withColor color: UIColor) {
        
        navigationController?.isNavigationBarHidden = false
        navigationController?.navigationBar.isTranslucent = false
        navigationController?.navigationBar.barTintColor = color
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationController?.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: Tools.setFontBook(size: 18),
                                                                        NSAttributedStringKey.foregroundColor: UIColor.BBVAWHITE]

        navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        navigationController?.navigationBar.shadowImage = UIImage()
    }

    func setNavigationBackButtonDefault() {

        setNavigationLeftButton(withImage: SVGCache.image(forSVGNamed: ConstantsImages.Common.return_icon, color: .BBVAWHITE), action: #selector(backAction))
    }

    func setNavigationLeftButton(withImage image: UIImage, action: Selector) {

        let leftButton = UIButton(frame: CGRect(x: 0, y: 0, width: barButtonDefaultSize.width, height: barButtonDefaultSize.height))
        leftButton.contentMode = .scaleAspectFit
        leftButton.setImage(image, for: .normal)
        leftButton.contentEdgeInsets = barButtonDefaultEdgeInsetsAlignLeft
        leftButton.addTarget(self, action: action, for: UIControlEvents.touchUpInside)
        
        let leftBarItem = UIBarButtonItem(customView: leftButton)

        leftBarItem.customView?.widthAnchor.constraint(equalToConstant: barButtonDefaultSize.width).isActive = true
        leftBarItem.customView?.heightAnchor.constraint(equalToConstant: barButtonDefaultSize.height).isActive = true

        navigationItem.leftBarButtonItem = leftBarItem

        setAccesibility(forView: leftButton, withId: ConstantsAccessibilities.LEFT_BAR_BUTTON)
    }

    func setNavigationRightButton(withImage image: UIImage, action: Selector) {

        let rightButton = UIButton(frame: CGRect(x: 0, y: 0, width: barButtonDefaultSize.width, height: barButtonDefaultSize.height))
        rightButton.contentMode = .scaleAspectFit
        rightButton.setImage(image, for: .normal)
        rightButton.contentEdgeInsets = barButtonDefaultEdgeInsetsAlignRight
        rightButton.addTarget(self, action: action, for: UIControlEvents.touchUpInside)
        
        let rightBarItem = UIBarButtonItem(customView: rightButton)
        
        rightBarItem.customView?.widthAnchor.constraint(equalToConstant: barButtonDefaultSize.width).isActive = true
        rightBarItem.customView?.heightAnchor.constraint(equalToConstant: barButtonDefaultSize.height).isActive = true

        navigationItem.rightBarButtonItem = rightBarItem

        setAccesibility(forView: rightButton, withId: ConstantsAccessibilities.RIGHT_BAR_BUTTON)
    }

    func addNavigationBarLogo() {

        let label = UILabel(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 20))
        label.contentMode = .center
        label.textAlignment = .center
        label.textColor = .white
        label.font = Tools.setFontBook(size: 21)
        label.text = Localizables.common.key_common_header_text
        label.sizeToFit()

        navigationItem.titleView = label
    }

    func setNavigationTitle(withText text: String) {

        let navigationTitleLabel = UILabel(frame: CGRect.zero)
        navigationTitleLabel.textAlignment = .center
        navigationTitleLabel.font = Tools.setFontBook(size: 18)
        navigationTitleLabel.textColor = .white
        navigationTitleLabel.text = text
        navigationTitleLabel.sizeToFit()
        
        navigationItem.titleView = navigationTitleLabel

        setAccesibility(forView: navigationTitleLabel, withId: ConstantsAccessibilities.NAVIGATION_BAR_TITLE)
    }

    func setNavigationTitle(withText text: String, color: UIColor) {

        self.setNavigationTitle(withText: text)
        let navigationTitleLabel = navigationItem.titleView as! UILabel
        navigationTitleLabel.textColor = color
    }

    @objc func backAction() {
        IQKeyboardManager.shared.resignFirstResponder()

        BusManager.sessionDataInstance.back()
    }

    @objc func dismissAction() {
        BusManager.sessionDataInstance.dismissViewController(animated: true)
    }

    func addNavigationBarMenuButton() {

        menuButton = UIButton(type: .custom)
        menuButton.frame = CGRect(x: 0, y: 0, width: barButtonDefaultSize.width, height: barButtonDefaultSize.height)
        menuButton.contentMode = .scaleAspectFit
        menuButton.contentEdgeInsets = barButtonDefaultEdgeInsetsAlignLeft

        menuButton.setImage(SVGCache.image(forSVGNamed: ConstantsImages.Common.menu_icon_image, color: .BBVAWHITE), for: .normal)
        menuButton.addTarget(self, action: #selector(showMenu), for: .touchUpInside)

        let menuBarItem = UIBarButtonItem(customView: menuButton)

        menuBarItem.customView?.widthAnchor.constraint(equalToConstant: barButtonDefaultSize.width).isActive = true
        menuBarItem.customView?.heightAnchor.constraint(equalToConstant: barButtonDefaultSize.height).isActive = true

        navigationItem.leftBarButtonItem = menuBarItem

        setAccesibility(forView: menuButton, withId: ConstantsAccessibilities.LOGIN_MENU_BUTTON)
    }
    
    @objc func showMenu() {
        sideMenuController?.showLeftView(animated: true, completionHandler: nil)
    }

    func initModel(withModel model: Model) {

    }
    
    func isVisible() -> Bool {
        
        let notModalAbove = self.presentedViewController == nil
        return self.viewIfLoaded?.window != nil && notModalAbove
    }

}

// MARK: - APPIUM

private extension BaseViewController {

    func setAccesibility(forView view: UIView, withId id: String) {
        #if APPIUM
        Tools.setAccessibility(view: view, identifier: id)
        #endif
    }

}
