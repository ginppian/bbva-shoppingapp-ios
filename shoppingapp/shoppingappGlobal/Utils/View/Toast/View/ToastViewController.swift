//
//  ToastViewController.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 26/10/2017.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit

class ToastViewController: BaseViewController, UIGestureRecognizerDelegate, UIHigherWindowDelegate {
    
    var toasts = [CustomToastView]()
    
    override func viewDidLoad() {

        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {

        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func createAndShowToastView(withHeight height: CGFloat, andText text: String?, orAttributedText attributedText: NSAttributedString?, andbackgroundColor backgroundColor: UIColor, toastDelegate: ToastDelegate?) {

        let toastView = createToastView(withHeight: height, andText: text, orAttributedText: attributedText, andbackgroundColor: backgroundColor, toastDelegate: toastDelegate)
        
        toastView.addGestureRecognizer(createTapGestureRecognizer())

        showToast(toastView, duration: TimeInterval(Constants.default_toast_time_in_seconds), topShow: view.frame.size.height - height)
    }

    private func createToastView(withHeight height: CGFloat, andText text: String?, orAttributedText attributedText: NSAttributedString?, andbackgroundColor backgroundColor: UIColor, toastDelegate: ToastDelegate?) -> CustomToastView {

        let toastView = CustomToastView(frame: CGRect(x: 0.0, y: view.bounds.size.height, width: view.bounds.size.width, height: height))
        toastView.delegate = toastDelegate
        
        toastView.autoresizingMask = [.flexibleLeftMargin, .flexibleRightMargin, .flexibleTopMargin, .flexibleBottomMargin]

        if let attributedText = attributedText {
            toastView.toastTextLabel.attributedText = attributedText
        } else {
            toastView.toastTextLabel.text = text ?? ""
        }
        toastView.toastViewBackgroundColor = backgroundColor

        return toastView
    }

    private func showToast(_ toastView: CustomToastView, duration: TimeInterval, topShow: CGFloat) {
        
        toasts.append(toastView)
        view.addSubview(toastView)
        toastView.isHidding = false
        
        UIView.animate(withDuration: 0.5, animations: {
            toastView.frame.origin.y = topShow
        }, completion: { _  in
            let timer = Timer(timeInterval: duration, target: self, selector: #selector(self.toastTimerDidFinish(_:)), userInfo: toastView, repeats: false)
            RunLoop.main.add(timer, forMode: RunLoopMode.commonModes)
        })
    }

    private func hideToast(_ toastView: UIView) {

        guard let topHide = self.view.topMostController()?.view.frame.size.height, let toastView = toastView as? CustomToastView else {
            return
        }

        if !toastView.isHidding {
            
            toastView.isHidding = true
            UIView.animate(withDuration: 0.5, animations: {
                
                toastView.frame.origin.y = topHide
            }, completion: { _  in
                
                toastView.removeFromSuperview()
                self.toasts = self.toasts.filter { $0 !== toastView }
                if self.toasts.isEmpty { // All toastViews removed, hide the viewcontroller
                    self.view.window?.isHidden = true
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
                        toastView.delegate?.toastViewDidDissapear()
                    }
                }
            })
        }
    }
    
    func isThereAToastVisible() -> Bool {
        
        return !toasts.isEmpty
    }
    
    @objc func toastTimerDidFinish(_ timer: Timer) {
        
        guard let toastView = timer.userInfo as? CustomToastView else {
            return
        }
        
        hideToast(toastView)
    }
    
    func higherWindowTouched() {
        
        for customToastView in toasts {
            
            hideToast(customToastView)
        }
    }
    
    private func createTapGestureRecognizer() -> UIGestureRecognizer {

        let hideToastGestureTap = UITapGestureRecognizer(target: self, action: #selector(self.handleTap))
        hideToastGestureTap.delegate = self
        hideToastGestureTap.numberOfTapsRequired = 1
        hideToastGestureTap.cancelsTouchesInView = false
        
        return hideToastGestureTap
    }

    @objc func handleTap(_ sender: UITapGestureRecognizer?) {
        
        for customToastView in toasts {
            
            if sender?.view == customToastView {
                customToastView.delegate?.toastViewPressed()
            }
            hideToast(customToastView)
        }
    }
}
