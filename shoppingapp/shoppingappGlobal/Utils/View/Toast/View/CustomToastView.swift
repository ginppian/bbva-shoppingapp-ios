//
//  CustomToastView.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 26/10/2017.
//  Copyright © 2017 BBVA. All rights reserved.
//

class CustomToastView: UIView {
    
    weak var delegate: ToastDelegate?
    var isHidding = false
    var toastTextLabel: UILabel = UILabel()

    var toastViewBackgroundColor: UIColor = UIColor.clear {
        willSet {
            backgroundColor = newValue
        }
    }

    override init(frame: CGRect) {
        super.init(frame: frame)

        configureView()
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!

        configureView()
    }

    func configureView() {

        toastTextLabel.translatesAutoresizingMaskIntoConstraints = false
        toastTextLabel.layer.masksToBounds = false
        toastTextLabel.textColor = .BBVAWHITE
        toastTextLabel.numberOfLines = 0
        toastTextLabel.textAlignment = .center
        toastTextLabel.font = Tools.setFontBook(size: 14)
        addSubview(toastTextLabel)

        configureConstraints()
    }

    func configureConstraints() {

        let toastViews = ["toastTextLabel": toastTextLabel]

        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "H:|-10-[toastTextLabel]-(10)-|",
                                                         options: NSLayoutFormatOptions(rawValue: 0),
                                                         metrics: nil,
                                                         views: toastViews))
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: "V:|-10-[toastTextLabel]-(10)-|",
                                                      options: NSLayoutFormatOptions(rawValue: 0),
                                                      metrics: nil,
                                                      views: toastViews))
    }
}
