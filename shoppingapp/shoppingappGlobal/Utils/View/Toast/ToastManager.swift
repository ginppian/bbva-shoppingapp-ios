//
//  ToastManager.swift
//  shoppingapp
//
//  Created by ignacio.bonafonte on 24/08/2018.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol ToastDelegate: class {

    func toastViewPressed()
    func toastViewDidDissapear()
}

extension ToastDelegate {
    
    func toastViewDidDissapear() {}
}

protocol UIHigherWindowDelegate: class {
    
    func higherWindowTouched()
}

final class UIHigherWindow: UIWindow {
    
    weak var delegate: UIHigherWindowDelegate?
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        
        let hitView = super.hitTest(point, with: event)
        
        if let hitView = hitView, hitView.isKind(of: CustomToastView.self) {
            
            return hitView
        }
        
        delegate?.higherWindowTouched()
        
        return nil
    }
}

final class ToastManager {

    // MARK: - Properties
    
    private static let toastManager: ToastManager = {

        let frameScreen = UIScreen.main.bounds

        let toastViewController = ToastViewController(nibName: "ToastViewController", bundle: nil)

        let window = UIHigherWindow(frame: frameScreen)
        window.windowLevel = UIWindowLevelNormal
        window.rootViewController = toastViewController
        window.delegate = toastViewController

        let toastManager = ToastManager(window: window, toastViewController: toastViewController)

        return toastManager
    }()

    private let window: UIHigherWindow
    var toastViewController: ToastViewController

    // MARK: - Init

    private init(window: UIHigherWindow, toastViewController: ToastViewController) {

        self.window = window
        self.toastViewController = toastViewController
    }

    // MARK: - Accessors

    class func shared() -> ToastManager {

        return toastManager
    }

    func showToast(withAttributedText text: NSAttributedString, backgroundColor: UIColor, height: CGFloat, toastDelegate: ToastDelegate? = nil) {

        window.isHidden = false
        toastViewController.createAndShowToastView(withHeight: height, andText: nil, orAttributedText: text, andbackgroundColor: backgroundColor, toastDelegate: toastDelegate)
    }

    func showToast(withText text: String, backgroundColor: UIColor, height: CGFloat, toastDelegate: ToastDelegate? = nil) {

        window.isHidden = false
        toastViewController.createAndShowToastView(withHeight: height, andText: text, orAttributedText: nil, andbackgroundColor: backgroundColor, toastDelegate: toastDelegate)
    }
    
    func isThereAToastVisible() -> Bool {
        
        return toastViewController.isThereAToastVisible()
    }
    
    private func setAccesibilitiesToast(toastView: CustomToastView) {
        
        #if APPIUM
        toastView.accessibilityIdentifier = ConstantsAccessibilities.TOAST_COMPONENT
        toastView.toastTextLabel.accessibilityIdentifier = ConstantsAccessibilities.TOAST_OUTPUT_TEXT
        #endif
    }
}
