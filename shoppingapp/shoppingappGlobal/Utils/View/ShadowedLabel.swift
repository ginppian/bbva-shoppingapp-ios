//
//  ShadowLabel.swift
//  shoppingapp
//
//  Created by jesus.martinez on 19/10/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

class ShadowedLabel: UILabel {

    var colorShadow = UIColor(white: 18.0 / 255.0, alpha: 0.8).cgColor

    override func drawText(in rect: CGRect) {

        clipsToBounds = false

        let context: CGContext? = UIGraphicsGetCurrentContext()
        context?.saveGState()
        let shadowOffset = CGSize(width: 1, height: 1)
        context?.setShadow(offset: shadowOffset, blur: 1, color: colorShadow)
        super.drawText(in: rect)
        context?.restoreGState()

    }

    func addShadowAttributeToString(substring: String, font: UIFont, shadowColor: UIColor) {

        self.text = substring
        self.font = font
        let range: NSRange = (self.text! as NSString).range(of: substring)

        let shadow = NSShadow()
        shadow.shadowOffset = CGSize(width: 1, height: 1)
        shadow.shadowBlurRadius = 1
        shadow.shadowColor = shadowColor

        let attributedString = NSMutableAttributedString(string: self.text!)
        attributedString.addAttribute(NSAttributedStringKey.shadow, value: shadow, range: range)
        self.attributedText = attributedString
    }
}
