//
//  PruebaViewController.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 18/9/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit
import RxSwift
import Lottie
import Stellar
import IQKeyboardManagerSwift
import SVGKit
import Material
import CellsNativeCore
import CellsNativeComponents

class PruebaViewController: BaseViewController {

    // MARK: Constants

    static let ROUTE: String = "Prueba"

    // MARK: Properties

    var presenter: PruebaPresenterProtocol!

    // MARK: Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        ((self.navigationController as! AppNavigator).webViewContainer as! CellsPageViewController).registerComponent(tag: presenter.routerTag, component: self.presenter! as! CellsComponent)

    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        ((self.navigationController as! AppNavigator).webViewContainer as! CellsPageViewController).unregisterComponent(tag: presenter.routerTag)
    }
}

extension PruebaViewController {

}

// MARK: LoginViewProtocol implementation

extension PruebaViewController: PruebaViewProtocol {

    func showError(error: ModelBO) {

    }

    func changeColorEditTexts() {

    }
}

// MARK: - APPIUM

private extension PruebaViewController {

    func setAccessibilities() {

        #if APPIUM

        #endif

    }
}
