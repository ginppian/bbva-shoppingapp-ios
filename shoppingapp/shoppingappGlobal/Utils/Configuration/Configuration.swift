//
//  Configuration.swift
//  shoppingapp
//
//  Created by jesus.martinez on 3/8/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation

class Configuration {

    private static func getConfigFile() -> [String: AnyObject] {

        let path = Bundle.main.path(forResource: "Configuration", ofType: "plist")!

        return NSDictionary(contentsOfFile: path) as! [String: AnyObject]

    }

    static func provideCurrencyFromConfiguration() -> CurrencyEntity {

        let configDictionary = self.getConfigFile()

        let mainCurrency = configDictionary["MainCurrency"] as! String

        return CurrencyEntity(code: mainCurrency)
    }

    static func provideTransactionTypeFilters() -> [TransactionsTypeFilterEntity] {

        let configDictionary = self.getConfigFile()

        let plistDocuments = configDictionary["TransactionsTypeFilters"] as! [[String: String]]

        var transactionsTypeFilters: [TransactionsTypeFilterEntity] = []

        for plistDocument in plistDocuments {
            let transactionFilter = TransactionsTypeFilterEntity(type: plistDocument["type"]!, titleKey: plistDocument["title_key"]!)
            transactionsTypeFilters.append(transactionFilter)
        }

        return transactionsTypeFilters
    }
    
    static func identificationDocuments() -> [IdentificationDocumentEntity] {

        let configDictionary = self.getConfigFile()

        let plistDocuments = configDictionary["IdentificationDocuments"] as! [[String: String]]

        var identificationDocuments: [IdentificationDocumentEntity] = []

        for plistDocument in plistDocuments {

            let identificationDocument = IdentificationDocumentEntity(code: plistDocument["code"]!, title: NSLocalizedString(plistDocument["title"]!, comment: ""))
            identificationDocuments.append(identificationDocument)
        }

        return identificationDocuments
    }

    static func leftMenuOptions() -> [LeftMenuOptionEntity] {

        let configDictionary = self.getConfigFile()

        let plistDocuments = configDictionary["OptionsLeftMenu"] as! [[String: String]]

        var options: [LeftMenuOptionEntity] = []

        for plistDocument in plistDocuments {
            let option = LeftMenuOptionEntity(image: plistDocument["image"]!, title: NSLocalizedString(plistDocument["title"]!, comment: ""), option: plistDocument["option"]!)
            options.append(option)
        }

        return options
    }

    static func consumerId() -> String {

        let configDictionary = self.getConfigFile()

        let serverConfig = configDictionary["SERVER_CONFIG"] as! [String: AnyObject]
        var consumerId = serverConfig["ConsumerId"] as! String

        if (serverConfig["SelectedEnvironment"] as! String) == "mock" {
            consumerId = serverConfig["ConsumerId_Mock"] as! String
        }

        return consumerId
    }

    static func anonymousInformation() -> AnonymousInformationEntity {

        let configDictionary = self.getConfigFile()

        let serverConfig = configDictionary["SERVER_CONFIG"] as! [String: AnyObject]
        let consumerId = serverConfig["ConsumerIdAnonymous"] as! String
        let userId = serverConfig["UserIdAnonymous"] as! String

        return AnonymousInformationEntity(consumerId: consumerId, userId: userId)
    }
    
    static func selectedEnvironment() -> String {

        let configDictionary = self.getConfigFile()

        let serverConfig = configDictionary["SERVER_CONFIG"] as! [String: AnyObject]
        let selectedEnvironment = serverConfig["SelectedEnvironment"] as! String

        return selectedEnvironment
    }
    
    static func googleMapsApiKey() -> String {

        let configDictionary = self.getConfigFile()

        let googleMapsKeys = configDictionary["GoogleMaps"] as! [String: AnyObject]

        guard let bundleID = Bundle.main.bundleIdentifier else {
            return googleMapsKeys["AppStore"] as! String
        }

        if bundleID.lowercased().contains("enterprise") {
            return googleMapsKeys["Enterprise"] as! String
        }

        return googleMapsKeys["AppStore"] as! String
    }

    static func provideUrlSchemaFromConfiguration() -> String {

        let configDictionary = self.getConfigFile()

        let urlSchema = configDictionary["UrlSchema"] as! String

        return urlSchema
    }

    static func provideUrlBBVAMovilMarketFromConfiguration() -> String {

        let configDictionary = self.getConfigFile()

        let urlBBVAMovilMarket = configDictionary["UrlBBVAMovilMarket"] as! String

        return urlBBVAMovilMarket
    }

    static func provideUrlMarketFromConfiguration() -> String {

        let configDictionary = self.getConfigFile()

        let urlBBVAMovilMarket = configDictionary["UrlMarket"] as! String

        return urlBBVAMovilMarket
    }
    
    static func isSSLPinningEnabled() -> Bool {
        
        let configDictionary = self.getConfigFile()
        
        let serverConfig = configDictionary["SSLPinning"] as! [String: AnyObject]
        let isSSLPinningEnabled = serverConfig["isEnabled"] as! Bool
        
        return isSSLPinningEnabled
    }
    
    static func areAllCertificatesAcceptedForSSLPinning() -> Bool {
        
        let configDictionary = self.getConfigFile()
        
        let serverConfig = configDictionary["SSLPinning"] as! [String: AnyObject]
        let isSSLPinningEnabled = serverConfig["AcceptAllCertificates"] as! Bool
        
        return isSSLPinningEnabled
    }
}
