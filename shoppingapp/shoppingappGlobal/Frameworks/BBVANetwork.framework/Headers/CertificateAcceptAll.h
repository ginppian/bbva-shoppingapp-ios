//
//  CertificateAcceptAll.h
//  http
//
//  Created by Iñaki Vitoria on 29/9/16.
//
//

#import <Foundation/Foundation.h>
#import "Certificate.h"

@interface CertificateAcceptAll : NSObject<Certificate>

@end
