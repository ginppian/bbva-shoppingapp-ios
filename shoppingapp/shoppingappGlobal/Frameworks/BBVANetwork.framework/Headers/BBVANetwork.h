//
//  BBVANetwork.h
//  BBVANetwork
//
//  Created by Gabriel Cuesta Arza on 10/5/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for BBVANetwork.
FOUNDATION_EXPORT double BBVANetworkVersionNumber;

//! Project version string for BBVANetwork.
FOUNDATION_EXPORT const unsigned char BBVANetworkVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <BBVANetwork/PublicHeader.h>
#import "BBVAEndPointDefault.h"
#import "EndPointManager.h"

#import "Certificate.h"
#import "CertificateManager.h"
#import "CertificateDefault.h"
#import "CertificateAcceptAll.h"
#import "CertificateSSLPinning.h"
