//
//  EndPointManager.h
//  http
//
//  Created by Iñaki Vitoria on 30/9/16.
//
//

#import <Foundation/Foundation.h>
#import "BBVAEndPoint.h"

@interface EndPointManager : NSObject

@property (nonatomic) id<BBVAEndPoint> currentEndPoint;

+ (instancetype)sharedClient;

@end
