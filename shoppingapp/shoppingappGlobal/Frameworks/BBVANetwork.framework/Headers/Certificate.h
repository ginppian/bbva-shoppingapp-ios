//
//  Certificate.h
//  http
//
//  Created by Iñaki Vitoria on 29/9/16.
//
//


@protocol Certificate <NSObject>

-(void) configureCertificate;

-(void)setCertificates:(NSDictionary *) certificates;

@end
