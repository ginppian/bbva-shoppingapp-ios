//
//  CertificateManager.h
//  http
//
//  Created by Iñaki Vitoria on 29/9/16.
//
//

#import <Foundation/Foundation.h>
#import "Certificate.h"

@interface CertificateManager : NSObject

@property (nonatomic, strong) id<Certificate> currentCertificate;

+ (instancetype)sharedClient;

@end
