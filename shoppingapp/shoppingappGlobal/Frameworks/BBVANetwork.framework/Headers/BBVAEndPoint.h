
@protocol BBVAEndPoint <NSObject>

//- (void)configureCertificate;

- (nullable NSURLSessionDataTask *)get:(NSString *_Nonnull)url headers:(NSDictionary *_Nonnull)headers parameters:(id _Nonnull )parameters timeout:(int)timeout withCompletion: (void(^_Nonnull)(NSDictionary * _Nonnull dictResult))completionBlock;

- (nullable NSURLSessionDataTask *)post:(NSString *_Nonnull)url headers:(NSDictionary *_Nonnull)headers parameters:(id _Nonnull )parameters timeout:(int)timeout withCompletion: (void(^_Nonnull)(NSDictionary * _Nonnull dictResult))completionBlock;

- (nullable NSURLSessionDataTask *)put:(NSString *_Nonnull)url headers:(NSDictionary *_Nonnull)headers parameters:(id _Nonnull )parameters timeout:(int)timeout withCompletion: (void(^_Nonnull)(NSDictionary * _Nonnull dictResult))completionBlock;

- (nullable NSURLSessionDataTask *)delete:(NSString *_Nonnull)url headers:(NSDictionary *_Nonnull)headers parameters:(id _Nonnull )parameters timeout:(int)timeout withCompletion: (void(^_Nonnull)(NSDictionary * _Nonnull dictResult))completionBlock;

- (nullable NSURLSessionDataTask *)head:(NSString *_Nonnull)url headers:(NSDictionary *_Nonnull)headers parameters:(id _Nonnull )parameters timeout:(int)timeout withCompletion: (void(^_Nonnull)(NSDictionary * _Nonnull dictResult))completionBlock;

- (nullable NSURLSessionDataTask *)patch:(NSString *_Nonnull)url headers:(NSDictionary *_Nonnull)headers parameters:(id _Nonnull )parameters timeout:(int)timeout withCompletion: (void(^_Nonnull)(NSDictionary * _Nonnull dictResult))completionBlock;

- (void)cleanAllCookiesWithCompletion:(void(^_Nonnull)(NSDictionary * _Nonnull dictResult))completionBlock;

@end
