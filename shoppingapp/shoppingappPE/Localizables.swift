struct Localizables {
    struct common {
        static var key_common_title_app_text: String {
            return NSLocalizedString("KEY_COMMON_TITLE_APP_TEXT", comment: "")
        }
        static var key_common_header_text: String {
            return NSLocalizedString("KEY_COMMON_HEADER_TEXT", comment: "")
        }
        static var key_common_accept_text: String {
            return NSLocalizedString("KEY_COMMON_ACCEPT_TEXT", comment: "")
        }
        static var key_common_accept: String {
            return NSLocalizedString("KEY_COMMON_ACCEPT", comment: "")
        }
        static var key_common_cancel_text: String {
            return NSLocalizedString("KEY_COMMON_CANCEL_TEXT", comment: "")
        }
        static var key_tabbar_start_button: String {
            return NSLocalizedString("KEY_TABBAR_START_BUTTON", comment: "")
        }
        static var key_tabbar_cards_button: String {
            return NSLocalizedString("KEY_TABBAR_CARDS_BUTTON", comment: "")
        }
        static var key_tabbar_offers_button: String {
            return NSLocalizedString("KEY_TABBAR_OFFERS_BUTTON", comment: "")
        }
        static var key_tabbar_points_button: String {
            return NSLocalizedString("KEY_TABBAR_POINTS_BUTTON", comment: "")
        }
        static var key_tabbar_notice_button: String {
            return NSLocalizedString("KEY_TABBAR_NOTICE_BUTTON", comment: "")
        }
        static var key_common_generic_error_text: String {
            return NSLocalizedString("KEY_COMMON_GENERIC_ERROR_TEXT", comment: "")
        }
        static var key_date_mask_day_month: String {
            return NSLocalizedString("KEY_DATE_MASK_DAY_MONTH", comment: "")
        }
        static var key_date_mask_day_month_short: String {
            return NSLocalizedString("KEY_DATE_MASK_DAY_MONTH_SHORT", comment: "")
        }
        static var key_date_mask_day_month_year: String {
            return NSLocalizedString("KEY_DATE_MASK_DAY_MONTH_YEAR", comment: "")
        }
        static var key_date_mask_day_month_year_short: String {
            return NSLocalizedString("KEY_DATE_MASK_DAY_MONTH_YEAR_SHORT", comment: "")
        }
        static var key_date_mask_day_month_year_compact: String {
            return NSLocalizedString("KEY_DATE_MASK_DAY_MONTH_YEAR_COMPACT", comment: "")
        }
        static var key_date_mask_day_month_complete_year_compact: String {
            return NSLocalizedString("KEY_DATE_MASK_DAY_MONTH_COMPLETE_YEAR_COMPACT", comment: "")
        }
        static var key_configuration_menu_text: String {
            return NSLocalizedString("KEY_CONFIGURATION_MENU_TEXT", comment: "")
        }
        static var key_help_menu_text: String {
            return NSLocalizedString("KEY_HELP_MENU_TEXT", comment: "")
        }
        static var key_go_to_bbva_menu_text: String {
            return NSLocalizedString("KEY_GO_TO_BBVA_MENU_TEXT", comment: "")
        }
        static var key_call_bbva_menu_text: String {
            return NSLocalizedString("KEY_CALL_BBVA_MENU_TEXT", comment: "")
        }
        static var key_login_session_menu_text: String {
            return NSLocalizedString("KEY_LOGIN_SESSION_MENU_TEXT", comment: "")
        }
        static var key_close_session_menu_text: String {
            return NSLocalizedString("KEY_CLOSE_SESSION_MENU_TEXT", comment: "")
        }
        static var key_not_you_done_operation_text: String {
            return NSLocalizedString("KEY_NOT_YOU_DONE_OPERATION_TEXT", comment: "")
        }
        static var key_error_ups_text: String {
            return NSLocalizedString("KEY_ERROR_UPS_TEXT", comment: "")
        }
        static var key_conexion_error_text: String {
            return NSLocalizedString("KEY_CONEXION_ERROR_TEXT", comment: "")
        }
        static var key_try_later_text: String {
            return NSLocalizedString("KEY_TRY_LATER_TEXT", comment: "")
        }
        static var key_security_title_text: String {
            return NSLocalizedString("KEY_SECURITY_TITLE_TEXT", comment: "")
        }
        static var key_introduce_key_from_sms: String {
            return NSLocalizedString("KEY_INTRODUCE_KEY_FROM_SMS", comment: "")
        }
        static var key_request_new_sms_description: String {
            return NSLocalizedString("KEY_REQUEST_NEW_SMS_DESCRIPTION", comment: "")
        }
        static var key_request_new_sms: String {
            return NSLocalizedString("KEY_REQUEST_NEW_SMS", comment: "")
        }
        static var key_confirm_text: String {
            return NSLocalizedString("KEY_CONFIRM_TEXT", comment: "")
        }
        static var key_placeholder_text: String {
            return NSLocalizedString("KEY_PLACEHOLDER_TEXT", comment: "")
        }
        static var key_common_connection_error_text: String {
            return NSLocalizedString("KEY_COMMON_CONNECTION_ERROR_TEXT", comment: "")
        }
        static var key_not_network_error_text: String {
            return NSLocalizedString("KEY_NOT_NETWORK_ERROR_TEXT", comment: "")
        }
        static var key_minute_text: String {
            return NSLocalizedString("KEY_MINUTE_TEXT", comment: "")
        }
        static var key_bbva_line_text: String {
            return NSLocalizedString("KEY_BBVA_LINE_TEXT", comment: "")
        }
        static var key_error_ocurred_try_again_text: String {
            return NSLocalizedString("KEY_ERROR_OCURRED_TRY_AGAIN_TEXT", comment: "")
        }
        static var key_sms_text: String {
            return NSLocalizedString("KEY_SMS_TEXT", comment: "")
        }
        static var key_recharge_text: String {
            return NSLocalizedString("KEY_RECHARGE_TEXT", comment: "")
        }
        static var key_mobile_payment_text: String {
            return NSLocalizedString("KEY_MOBILE_PAYMENT_TEXT", comment: "")
        }
        static var key_mobile_payment_more_info_text: String {
            return NSLocalizedString("KEY_MOBILE_PAYMENT_MORE_INFO_TEXT", comment: "")
        }
        static var key_gcm_notification_accepted_purchase_title: String {
            return NSLocalizedString("KEY_GCM_NOTIFICATION_ACCEPTED_PURCHASE_TITLE", comment: "")
        }
        static var key_gcm_notification_denied_purchase_title: String {
            return NSLocalizedString("KEY_GCM_NOTIFICATION_DENIED_PURCHASE_TITLE", comment: "")
        }
        static var key_gcm_notification_accepted_purchase_text: String {
            return NSLocalizedString("KEY_GCM_NOTIFICATION_ACCEPTED_PURCHASE_TEXT", comment: "")
        }
        static var key_gcm_notification_denied_purchase_text: String {
            return NSLocalizedString("KEY_GCM_NOTIFICATION_DENIED_PURCHASE_TEXT", comment: "")
        }
        static var key_gcm_notification_with_card_text: String {
            return NSLocalizedString("KEY_GCM_NOTIFICATION_WITH_CARD_TEXT", comment: "")
        }
        static var key_gcm_notification_asterisk_text: String {
            return NSLocalizedString("KEY_GCM_NOTIFICATION_ASTERISK_TEXT", comment: "")
        }
        static var key_common_error_text: String {
            return NSLocalizedString("KEY_COMMON_ERROR_TEXT", comment: "")
        }
        static var key_common_error_nodata_text: String {
            return NSLocalizedString("KEY_COMMON_ERROR_NODATA_TEXT", comment: "")
        }
        static var key_retry_text: String {
            return NSLocalizedString("KEY_RETRY_TEXT", comment: "")
        }
        static var key_delete_all_filters_text: String {
            return NSLocalizedString("KEY_DELETE_ALL_FILTERS_TEXT", comment: "")
        }
        static var key_from_text: String {
            return NSLocalizedString("KEY_FROM_TEXT", comment: "")
        }
        static var key_to_text: String {
            return NSLocalizedString("KEY_TO_TEXT", comment: "")
        }
        static var key_close_text: String {
            return NSLocalizedString("KEY_CLOSE_TEXT", comment: "")
        }
        static var key_key_and_text: String {
            return NSLocalizedString("KEY_KEY_AND_TEXT", comment: "")
        }
        static var key_config_mobile_payment_text: String {
            return NSLocalizedString("KEY_CONFIG_MOBILE_PAYMENT_TEXT", comment: "")
        }
        static var key_active_mobile_payment_text: String {
            return NSLocalizedString("KEY_ACTIVE_MOBILE_PAYMENT_TEXT", comment: "")
        }
        static var key_mobile_payment_not_predetermined_text: String {
            return NSLocalizedString("KEY_MOBILE_PAYMENT_NOT_PREDETERMINED_TEXT", comment: "")
        }
        static var key_mobile_payment_choose_wallet: String {
            return NSLocalizedString("KEY_MOBILE_PAYMENT_CHOOSE_WALLET", comment: "")
        }
        static var key_mobile_payment_not_available_text: String {
            return NSLocalizedString("KEY_MOBILE_PAYMENT_NOT_AVAILABLE_TEXT", comment: "")
        }
        static var key_mobile_payment_activate_nfc: String {
            return NSLocalizedString("KEY_MOBILE_PAYMENT_ACTIVATE_NFC", comment: "")
        }
        static var key_mobile_payment_is_active_text: String {
            return NSLocalizedString("KEY_MOBILE_PAYMENT_IS_ACTIVE_TEXT", comment: "")
        }
        static var key_mobile_payment_card_active_text: String {
            return NSLocalizedString("KEY_MOBILE_PAYMENT_CARD_ACTIVE_TEXT", comment: "")
        }
        static var key_mobile_payment_for_text: String {
            return NSLocalizedString("KEY_MOBILE_PAYMENT_FOR_TEXT", comment: "")
        }
        static var key_mobile_payment_purchases_loaded_in_this_card_text: String {
            return NSLocalizedString("KEY_MOBILE_PAYMENT_PURCHASES_LOADED_IN_THIS_CARD_TEXT", comment: "")
        }
        static var key_mobile_payment_deactivated_card_text: String {
            return NSLocalizedString("KEY_MOBILE_PAYMENT_DEACTIVATED_CARD_TEXT", comment: "")
        }
        static var key_mobile_payment_active_other_card_text: String {
            return NSLocalizedString("KEY_MOBILE_PAYMENT_ACTIVE_OTHER_CARD_TEXT", comment: "")
        }
        static var key_mobile_payment_the_card_text: String {
            return NSLocalizedString("KEY_MOBILE_PAYMENT_THE_CARD_TEXT", comment: "")
        }
        static var key_mobile_payment_is_off_text: String {
            return NSLocalizedString("KEY_MOBILE_PAYMENT_IS_OFF_TEXT", comment: "")
        }
        static var key_mobile_payment_is_necessary_active_text: String {
            return NSLocalizedString("KEY_MOBILE_PAYMENT_IS_NECESSARY_ACTIVE_TEXT", comment: "")
        }
        static var key_mobile_payment_active_text: String {
            return NSLocalizedString("KEY_MOBILE_PAYMENT_ACTIVE_TEXT", comment: "")
        }
        static var key_not_available_text: String {
            return NSLocalizedString("KEY_NOT_AVAILABLE_TEXT", comment: "")
        }
        static var key_mobile_payment_deactivate_text: String {
            return NSLocalizedString("KEY_MOBILE_PAYMENT_DEACTIVATE_TEXT", comment: "")
        }
        static var key_mobile_payment_on_card_text: String {
            return NSLocalizedString("KEY_MOBILE_PAYMENT_ON_CARD_TEXT", comment: "")
        }
        static var key_mobile_payment: String {
            return NSLocalizedString("KEY_MOBILE_PAYMENT", comment: "")
        }
        static var key_error_cards: String {
            return NSLocalizedString("KEY_ERROR_CARDS", comment: "")
        }
        static var key_mobile_payment_error_pin: String {
            return NSLocalizedString("KEY_MOBILE_PAYMENT_ERROR_PIN", comment: "")
        }
        static var key_mobile_payment_error_incompatible: String {
            return NSLocalizedString("KEY_MOBILE_PAYMENT_ERROR_INCOMPATIBLE", comment: "")
        }
        static var key_mobile_payment_error_no_connection: String {
            return NSLocalizedString("KEY_MOBILE_PAYMENT_ERROR_NO_CONNECTION", comment: "")
        }
        static var key_mobile_payment_error_deactivated: String {
            return NSLocalizedString("KEY_MOBILE_PAYMENT_ERROR_DEACTIVATED", comment: "")
        }
        static var key_mobile_payment_error_not_default_card: String {
            return NSLocalizedString("KEY_MOBILE_PAYMENT_ERROR_NOT_DEFAULT_CARD", comment: "")
        }
        static var key_mobile_payment_error_generic: String {
            return NSLocalizedString("KEY_MOBILE_PAYMENT_ERROR_GENERIC", comment: "")
        }
        static var key_without_connectivity_error_header: String {
            return NSLocalizedString("KEY_WITHOUT_CONNECTIVITY_ERROR_HEADER", comment: "")
        }
        static var key_without_connectivity_error_title: String {
            return NSLocalizedString("KEY_WITHOUT_CONNECTIVITY_ERROR_TITLE", comment: "")
        }
        static var key_without_connectivity_airplane_text: String {
            return NSLocalizedString("KEY_WITHOUT_CONNECTIVITY_AIRPLANE_TEXT", comment: "")
        }
        static var key_without_connectivity_wifi_text: String {
            return NSLocalizedString("KEY_WITHOUT_CONNECTIVITY_WIFI_TEXT", comment: "")
        }
        static var key_without_connectivity_data_text: String {
            return NSLocalizedString("KEY_WITHOUT_CONNECTIVITY_DATA_TEXT", comment: "")
        }
        static var key_without_connectivity_signal_text: String {
            return NSLocalizedString("KEY_WITHOUT_CONNECTIVITY_SIGNAL_TEXT", comment: "")
        }
        static var key_without_connectivity_accept_button: String {
            return NSLocalizedString("KEY_WITHOUT_CONNECTIVITY_ACCEPT_BUTTON", comment: "")
        }
        static var key_generic_error_code: String {
            return NSLocalizedString("KEY_GENERIC_ERROR_CODE", comment: "")
        }
        static var key_softoken_security_title: String {
            return NSLocalizedString("KEY_SOFTOKEN_SECURITY_TITLE", comment: "")
        }
        static var key_softoken_security_continue: String {
            return NSLocalizedString("KEY_SOFTOKEN_SECURITY_CONTINUE", comment: "")
        }
        static var key_load_error: String {
            return NSLocalizedString("KEY_LOAD_ERROR", comment: "")
        }
        static var key_call_bbva_line: String {
            return NSLocalizedString("KEY_CALL_BBVA_LINE", comment: "")
        }
        static var key_duplicate_operation: String {
            return NSLocalizedString("KEY_DUPLICATE_OPERATION", comment: "")
        }
    }
    struct promotions {
        static var key_promotions_title_text: String {
            return NSLocalizedString("KEY_PROMOTIONS_TITLE_TEXT", comment: "")
        }
        static var key_promotions_no_promotions_text: String {
            return NSLocalizedString("KEY_PROMOTIONS_NO_PROMOTIONS_TEXT", comment: "")
        }
        static var key_promotions_featured_text: String {
            return NSLocalizedString("KEY_PROMOTIONS_FEATURED_TEXT", comment: "")
        }
        static var key_promotions_see_all_text: String {
            return NSLocalizedString("KEY_PROMOTIONS_SEE_ALL_TEXT", comment: "")
        }
        static var key_promotions_others_promotions_text: String {
            return NSLocalizedString("KEY_PROMOTIONS_OTHERS_PROMOTIONS_TEXT", comment: "")
        }
        static var key_promotions_see_all_fem_text: String {
            return NSLocalizedString("KEY_PROMOTIONS_SEE_ALL_FEM_TEXT", comment: "")
        }
        static var key_promotions_left_text: String {
            return NSLocalizedString("KEY_PROMOTIONS_LEFT_TEXT", comment: "")
        }
        static var key_promotions_days_text: String {
            return NSLocalizedString("KEY_PROMOTIONS_DAYS_TEXT", comment: "")
        }
        static var key_promotions_promotion_start_text: String {
            return NSLocalizedString("KEY_PROMOTIONS_PROMOTION_START_TEXT", comment: "")
        }
        static var key_promotions_how_get_promotion_text: String {
            return NSLocalizedString("KEY_PROMOTIONS_HOW_GET_PROMOTION_TEXT", comment: "")
        }
        static var key_promotions_online_text: String {
            return NSLocalizedString("KEY_PROMOTIONS_ONLINE_TEXT", comment: "")
        }
        static var key_promotions_facetoface_text: String {
            return NSLocalizedString("KEY_PROMOTIONS_FACETOFACE_TEXT", comment: "")
        }
        static var key_promotions_online_and_facetoface_text: String {
            return NSLocalizedString("KEY_PROMOTIONS_ONLINE_AND_FACETOFACE_TEXT", comment: "")
        }
        static var key_promotions_more_info_text: String {
            return NSLocalizedString("KEY_PROMOTIONS_MORE_INFO_TEXT", comment: "")
        }
        static var key_promotions_about_text: String {
            return NSLocalizedString("KEY_PROMOTIONS_ABOUT_TEXT", comment: "")
        }
        static var key_promotions_see_establishment_text: String {
            return NSLocalizedString("KEY_PROMOTIONS_SEE_ESTABLISHMENT_TEXT", comment: "")
        }
        static var key_promotions_consider_text: String {
            return NSLocalizedString("KEY_PROMOTIONS_CONSIDER_TEXT", comment: "")
        }
        static var key_promotions_terms_and_conditions_text: String {
            return NSLocalizedString("KEY_PROMOTIONS_TERMS_AND_CONDITIONS_TEXT", comment: "")
        }
        static var key_promotions_consider_this_info_text: String {
            return NSLocalizedString("KEY_PROMOTIONS_CONSIDER_THIS_INFO_TEXT", comment: "")
        }
        static var key_promotions_remains_text: String {
            return NSLocalizedString("KEY_PROMOTIONS_REMAINS_TEXT", comment: "")
        }
        static var key_promotions_day_text: String {
            return NSLocalizedString("KEY_PROMOTIONS_DAY_TEXT", comment: "")
        }
        static var key_promotions_near_me_text: String {
            return NSLocalizedString("KEY_PROMOTIONS_NEAR_ME_TEXT", comment: "")
        }
        static var key_enable_gps_advise_text: String {
            return NSLocalizedString("KEY_ENABLE_GPS_ADVISE_TEXT", comment: "")
        }
        static var key_promotions_see_promotions_near_me_text: String {
            return NSLocalizedString("KEY_PROMOTIONS_SEE_PROMOTIONS_NEAR_ME_TEXT", comment: "")
        }
        static var key_activate_gps_advise_text: String {
            return NSLocalizedString("KEY_ACTIVATE_GPS_ADVISE_TEXT", comment: "")
        }
        static var key_activate_location_text: String {
            return NSLocalizedString("KEY_ACTIVATE_LOCATION_TEXT", comment: "")
        }
        static var key_promotions_how_to_validate_promotions_text: String {
            return NSLocalizedString("KEY_PROMOTIONS_HOW_TO_VALIDATE_PROMOTIONS_TEXT", comment: "")
        }
        static var key_promotions_valid_card_for_the_promotion_text: String {
            return NSLocalizedString("KEY_PROMOTIONS_VALID_CARD_FOR_THE_PROMOTION_TEXT", comment: "")
        }
        static var key_how_to_get_text: String {
            return NSLocalizedString("KEY_HOW_TO_GET_TEXT", comment: "")
        }
        static var key_any_questions_need_a_quick_response_text: String {
            return NSLocalizedString("KEY_ANY_QUESTIONS_NEED_A_QUICK_RESPONSE_TEXT", comment: "")
        }
        static var key_didnt_give_promotion_we_guarantee_it_text: String {
            return NSLocalizedString("KEY_DIDNT_GIVE_PROMOTION_WE_GUARANTEE_IT_TEXT", comment: "")
        }
        static var key_more_information_text: String {
            return NSLocalizedString("KEY_MORE_INFORMATION_TEXT", comment: "")
        }
        static var key_contact_us_text: String {
            return NSLocalizedString("KEY_CONTACT_US_TEXT", comment: "")
        }
        static var key_promotions_stores_zero_result_text: String {
            return NSLocalizedString("KEY_PROMOTIONS_STORES_ZERO_RESULT_TEXT", comment: "")
        }
        static var key_gps_disabled_info_text: String {
            return NSLocalizedString("KEY_GPS_DISABLED_INFO_TEXT", comment: "")
        }
        static var key_gps_disabled_info_option_settings_text: String {
            return NSLocalizedString("KEY_GPS_DISABLED_INFO_OPTION_SETTINGS_TEXT", comment: "")
        }
        static var key_gps_disabled_info_option_privacity_text: String {
            return NSLocalizedString("KEY_GPS_DISABLED_INFO_OPTION_PRIVACITY_TEXT", comment: "")
        }
        static var key_gps_disabled_info_option_location_text: String {
            return NSLocalizedString("KEY_GPS_DISABLED_INFO_OPTION_LOCATION_TEXT", comment: "")
        }
        static var key_gps_disabled_info_option_activate_text: String {
            return NSLocalizedString("KEY_GPS_DISABLED_INFO_OPTION_ACTIVATE_TEXT", comment: "")
        }
        static var key_enable_gps_text: String {
            return NSLocalizedString("KEY_ENABLE_GPS_TEXT", comment: "")
        }
        static var key_understood_text: String {
            return NSLocalizedString("KEY_UNDERSTOOD_TEXT", comment: "")
        }
        static var key_call_line_bbva_text: String {
            return NSLocalizedString("KEY_CALL_LINE_BBVA_TEXT", comment: "")
        }
        static var key_send_email_text: String {
            return NSLocalizedString("KEY_SEND_EMAIL_TEXT", comment: "")
        }
        static var key_gps_text: String {
            return NSLocalizedString("KEY_GPS_TEXT", comment: "")
        }
        static var key_settings_text: String {
            return NSLocalizedString("KEY_SETTINGS_TEXT", comment: "")
        }
        static var key_privacy_text: String {
            return NSLocalizedString("KEY_PRIVACY_TEXT", comment: "")
        }
        static var key_location_services_text: String {
            return NSLocalizedString("KEY_LOCATION_SERVICES_TEXT", comment: "")
        }
        static var key_promotion_type_text: String {
            return NSLocalizedString("KEY_PROMOTION_TYPE_TEXT", comment: "")
        }
        static var key_commerce_type_text: String {
            return NSLocalizedString("KEY_COMMERCE_TYPE_TEXT", comment: "")
        }
        static var key_search_text: String {
            return NSLocalizedString("KEY_SEARCH_TEXT", comment: "")
        }
        static var key_reset_text: String {
            return NSLocalizedString("KEY_RESET_TEXT", comment: "")
        }
        static var key_see_results_text: String {
            return NSLocalizedString("KEY_SEE_RESULTS_TEXT", comment: "")
        }
        static var key_categories_text: String {
            return NSLocalizedString("KEY_CATEGORIES_TEXT", comment: "")
        }
        static var key_physical_commerce_text: String {
            return NSLocalizedString("KEY_PHYSICAL_COMMERCE_TEXT", comment: "")
        }
        static var key_online_commerce_text: String {
            return NSLocalizedString("KEY_ONLINE_COMMERCE_TEXT", comment: "")
        }
        static var key_discount_text: String {
            return NSLocalizedString("KEY_DISCOUNT_TEXT", comment: "")
        }
        static var key_months_without_interest_text: String {
            return NSLocalizedString("KEY_MONTHS_WITHOUT_INTEREST_TEXT", comment: "")
        }
        static var key_search_commerce_name_text: String {
            return NSLocalizedString("KEY_SEARCH_COMMERCE_NAME_TEXT", comment: "")
        }
        static var key_warning_commerce_dont_apply_text: String {
            return NSLocalizedString("KEY_WARNING_COMMERCE_DONT_APPLY_TEXT", comment: "")
        }
        static var key_contact_text: String {
            return NSLocalizedString("KEY_CONTACT_TEXT", comment: "")
        }
        static var key_satisfaction_guarantee_text: String {
            return NSLocalizedString("KEY_SATISFACTION_GUARANTEE_TEXT", comment: "")
        }
        static var key_promotions_points_text: String {
            return NSLocalizedString("KEY_PROMOTIONS_POINTS_TEXT", comment: "")
        }
        static var key_distance_meters: String {
            return NSLocalizedString("KEY_DISTANCE_METERS", comment: "")
        }
        static var key_distance_kilometers: String {
            return NSLocalizedString("KEY_DISTANCE_KILOMETERS", comment: "")
        }
        static var key_starts_text: String {
            return NSLocalizedString("KEY_STARTS_TEXT", comment: "")
        }
        static var key_starts_in_text: String {
            return NSLocalizedString("KEY_STARTS_IN_TEXT", comment: "")
        }
        static var key_finish_text: String {
            return NSLocalizedString("KEY_FINISH_TEXT", comment: "")
        }
        static var key_finish_in_text: String {
            return NSLocalizedString("KEY_FINISH_IN_TEXT", comment: "")
        }
        static var key_finish_today: String {
            return NSLocalizedString("KEY_FINISH_TODAY", comment: "")
        }
        static var key_search_no_promotions_text: String {
            return NSLocalizedString("KEY_SEARCH_NO_PROMOTIONS_TEXT", comment: "")
        }
        static var key_want_this_promotion_text: String {
            return NSLocalizedString("KEY_WANT_THIS_PROMOTION_TEXT", comment: "")
        }
        static var key_use_promotion_link_text: String {
            return NSLocalizedString("KEY_USE_PROMOTION_LINK_TEXT", comment: "")
        }
        static var key_see_promotions: String {
            return NSLocalizedString("KEY_SEE_PROMOTIONS", comment: "")
        }
        static var key_view_all_title_text: String {
            return NSLocalizedString("KEY_VIEW_ALL_TITLE_TEXT", comment: "")
        }
        static var key_from_distance_text: String {
            return NSLocalizedString("KEY_FROM_DISTANCE_TEXT", comment: "")
        }
        static var key_promotions_near_you_text: String {
            return NSLocalizedString("KEY_PROMOTIONS_NEAR_YOU_TEXT", comment: "")
        }
        static var key_promotions_stores_near_center_text: String {
            return NSLocalizedString("KEY_PROMOTIONS_STORES_NEAR_CENTER_TEXT", comment: "")
        }
        static var key_promotions_stores_with_promotion_in_5km_text: String {
            return NSLocalizedString("KEY_PROMOTIONS_STORES_WITH_PROMOTION_IN_5KM_TEXT", comment: "")
        }
        static var key_promotions_no_stores_in_5km_look_for_others_stores_text: String {
            return NSLocalizedString("KEY_PROMOTIONS_NO_STORES_IN_5KM_LOOK_FOR_OTHERS_STORES_TEXT", comment: "")
        }
        static var key_promotions_no_stores_with_text: String {
            return NSLocalizedString("KEY_PROMOTIONS_NO_STORES_WITH_TEXT", comment: "")
        }
        static var key_promotons_of_you_text: String {
            return NSLocalizedString("KEY_PROMOTONS_OF_YOU_TEXT", comment: "")
        }
        static var key_promotions_no_stores_in_5km_question_others_stores_text: String {
            return NSLocalizedString("KEY_PROMOTIONS_NO_STORES_IN_5KM_QUESTION_OTHERS_STORES_TEXT", comment: "")
        }
        static var key_promotions_location_permission_text: String {
            return NSLocalizedString("KEY_PROMOTIONS_LOCATION_PERMISSION_TEXT", comment: "")
        }
        static var key_promotions_loading_at_2km_radius: String {
            return NSLocalizedString("KEY_PROMOTIONS_LOADING_AT_2KM_RADIUS", comment: "")
        }
        static var key_promotions_updated_promos_for_position_text: String {
            return NSLocalizedString("KEY_PROMOTIONS_UPDATED_PROMOS_FOR_POSITION_TEXT", comment: "")
        }
        static var key_promotions_for_best_results_active_gps_text: String {
            return NSLocalizedString("KEY_PROMOTIONS_FOR_BEST_RESULTS_ACTIVE_GPS_TEXT", comment: "")
        }
        static var key_promotions_configure_favourites_title: String {
            return NSLocalizedString("KEY_PROMOTIONS_CONFIGURE_FAVOURITES_TITLE", comment: "")
        }
        static var key_promotions_configure_favourites_subtitle: String {
            return NSLocalizedString("KEY_PROMOTIONS_CONFIGURE_FAVOURITES_SUBTITLE", comment: "")
        }
        static var key_promotions_configure_favourites_description: String {
            return NSLocalizedString("KEY_PROMOTIONS_CONFIGURE_FAVOURITES_DESCRIPTION", comment: "")
        }
        static var key_promotions_init_date: String {
            return NSLocalizedString("KEY_PROMOTIONS_INIT_DATE", comment: "")
        }
        static var key_promotions_finish_date: String {
            return NSLocalizedString("KEY_PROMOTIONS_FINISH_DATE", comment: "")
        }
        static var key_promotions_favourites_max_selected: String {
            return NSLocalizedString("KEY_PROMOTIONS_FAVOURITES_MAX_SELECTED", comment: "")
        }
        static var key_promotions_favourites_in_other_moment: String {
            return NSLocalizedString("KEY_PROMOTIONS_FAVOURITES_IN_OTHER_MOMENT", comment: "")
        }
        static var key_promotions_favourites_set_up_later: String {
            return NSLocalizedString("KEY_PROMOTIONS_FAVOURITES_SET_UP_LATER", comment: "")
        }
        static var key_promotions_see_less_text: String {
            return NSLocalizedString("KEY_PROMOTIONS_SEE_LESS_TEXT", comment: "")
        }
        static var key_promotions_suggested_text: String {
            return NSLocalizedString("KEY_PROMOTIONS_SUGGESTED_TEXT", comment: "")
        }
        static var key_promotions_link_credit_card: String {
            return NSLocalizedString("KEY_PROMOTIONS_LINK_CREDIT_CARD", comment: "")
        }
        static var key_promotions_link_credit_card_message: String {
            return NSLocalizedString("KEY_PROMOTIONS_LINK_CREDIT_CARD_MESSAGE", comment: "")
        }
        static var key_promotions_online_promotions: String {
            return NSLocalizedString("KEY_PROMOTIONS_ONLINE_PROMOTIONS", comment: "")
        }
        static var key_promotions_exclusive_promotions: String {
            return NSLocalizedString("KEY_PROMOTIONS_EXCLUSIVE_PROMOTIONS", comment: "")
        }
        static var key_promotions_see_promotions: String {
            return NSLocalizedString("KEY_PROMOTIONS_SEE_PROMOTIONS", comment: "")
        }
    }
    struct login {
        static var key_login_splash_text: String {
            return NSLocalizedString("KEY_LOGIN_SPLASH_TEXT", comment: "")
        }
        static var key_login_hello_text: String {
            return NSLocalizedString("KEY_LOGIN_HELLO_TEXT", comment: "")
        }
        static var key_login_usewallet_text: String {
            return NSLocalizedString("KEY_LOGIN_USEWALLET_TEXT", comment: "")
        }
        static var key_login_access_button: String {
            return NSLocalizedString("KEY_LOGIN_ACCESS_BUTTON", comment: "")
        }
        static var key_login_promotions_text: String {
            return NSLocalizedString("KEY_LOGIN_PROMOTIONS_TEXT", comment: "")
        }
        static var key_login_selector_text: String {
            return NSLocalizedString("KEY_LOGIN_SELECTOR_TEXT", comment: "")
        }
        static var key_login_user_text: String {
            return NSLocalizedString("KEY_LOGIN_USER_TEXT", comment: "")
        }
        static var key_login_password_text: String {
            return NSLocalizedString("KEY_LOGIN_PASSWORD_TEXT", comment: "")
        }
        static var key_login_recover_text: String {
            return NSLocalizedString("KEY_LOGIN_RECOVER_TEXT", comment: "")
        }
        static var key_login_loading_text: String {
            return NSLocalizedString("KEY_LOGIN_LOADING_TEXT", comment: "")
        }
        static var key_login_dni_text: String {
            return NSLocalizedString("KEY_LOGIN_DNI_TEXT", comment: "")
        }
        static var key_login_ruc_text: String {
            return NSLocalizedString("KEY_LOGIN_RUC_TEXT", comment: "")
        }
        static var key_login_passport_text: String {
            return NSLocalizedString("KEY_LOGIN_PASSPORT_TEXT", comment: "")
        }
        static var key_login_foreigner_id_text: String {
            return NSLocalizedString("KEY_LOGIN_FOREIGNER_ID_TEXT", comment: "")
        }
        static var key_login_military_id_text: String {
            return NSLocalizedString("KEY_LOGIN_MILITARY_ID_TEXT", comment: "")
        }
        static var key_login_diplomat_id_text: String {
            return NSLocalizedString("KEY_LOGIN_DIPLOMAT_ID_TEXT", comment: "")
        }
        static var key_login_citizenship_id_text: String {
            return NSLocalizedString("KEY_LOGIN_CITIZENSHIP_ID_TEXT", comment: "")
        }
        static var key_login_identity_card_text: String {
            return NSLocalizedString("KEY_LOGIN_IDENTITY_CARD_TEXT", comment: "")
        }
        static var key_login_personal_identity_card_text: String {
            return NSLocalizedString("KEY_LOGIN_PERSONAL_IDENTITY_CARD_TEXT", comment: "")
        }
        static var key_login_disconnect_change_user_text: String {
            return NSLocalizedString("KEY_LOGIN_DISCONNECT_CHANGE_USER_TEXT", comment: "")
        }
        static var key_login_disconnect_confirmation_text: String {
            return NSLocalizedString("KEY_LOGIN_DISCONNECT_CONFIRMATION_TEXT", comment: "")
        }
        static var key_login_disconnect_message_info1_text: String {
            return NSLocalizedString("KEY_LOGIN_DISCONNECT_MESSAGE_INFO1_TEXT", comment: "")
        }
        static var key_login_disconnect_message_info2_text: String {
            return NSLocalizedString("KEY_LOGIN_DISCONNECT_MESSAGE_INFO2_TEXT", comment: "")
        }
        static var key_login_disconnect_understood_text: String {
            return NSLocalizedString("KEY_LOGIN_DISCONNECT_UNDERSTOOD_TEXT", comment: "")
        }
        static var key_expired_session: String {
            return NSLocalizedString("KEY_EXPIRED_SESSION", comment: "")
        }
        static var key_loading_promotions: String {
            return NSLocalizedString("KEY_LOADING_PROMOTIONS", comment: "")
        }
        static var key_dashboard_welcome: String {
            return NSLocalizedString("KEY_DASHBOARD_WELCOME", comment: "")
        }
        static var key_dashboard_description: String {
            return NSLocalizedString("KEY_DASHBOARD_DESCRIPTION", comment: "")
        }
        static var key_dashboard_pay_with_wallet_title: String {
            return NSLocalizedString("KEY_DASHBOARD_PAY_WITH_WALLET_TITLE", comment: "")
        }
        static var key_dashboard_pay_with_wallet_description: String {
            return NSLocalizedString("KEY_DASHBOARD_PAY_WITH_WALLET_DESCRIPTION", comment: "")
        }
        static var key_dashboard_get_points_title: String {
            return NSLocalizedString("KEY_DASHBOARD_GET_POINTS_TITLE", comment: "")
        }
        static var key_dashboard_get_points_description: String {
            return NSLocalizedString("KEY_DASHBOARD_GET_POINTS_DESCRIPTION", comment: "")
        }
        static var key_dashboard_stay_informed_title: String {
            return NSLocalizedString("KEY_DASHBOARD_STAY_INFORMED_TITLE", comment: "")
        }
        static var key_dashboard_stay_informed_description: String {
            return NSLocalizedString("KEY_DASHBOARD_STAY_INFORMED_DESCRIPTION", comment: "")
        }
        static var key_dashboard_login: String {
            return NSLocalizedString("KEY_DASHBOARD_LOGIN", comment: "")
        }
        static var key_dashboard_points: String {
            return NSLocalizedString("KEY_DASHBOARD_POINTS", comment: "")
        }
        static var key_dashboard_start: String {
            return NSLocalizedString("KEY_DASHBOARD_START", comment: "")
        }
        static var key_dashboard_cards: String {
            return NSLocalizedString("KEY_DASHBOARD_CARDS", comment: "")
        }
        static var key_dashboard_notifications: String {
            return NSLocalizedString("KEY_DASHBOARD_NOTIFICATIONS", comment: "")
        }
        static var key_dashboard_promotions: String {
            return NSLocalizedString("KEY_DASHBOARD_PROMOTIONS", comment: "")
        }
        static var key_expired_tsec: String {
            return NSLocalizedString("KEY_EXPIRED_TSEC", comment: "")
        }
        static var key_register_user: String {
            return NSLocalizedString("KEY_REGISTER_USER", comment: "")
        }
        static var key_login_enter_text: String {
            return NSLocalizedString("KEY_LOGIN_ENTER_TEXT", comment: "")
        }
        static var key_login_use_bbva_wallet_text: String {
            return NSLocalizedString("KEY_LOGIN_USE_BBVA_WALLET_TEXT", comment: "")
        }
        static var key_login_register_text: String {
            return NSLocalizedString("KEY_LOGIN_REGISTER_TEXT", comment: "")
        }
        static var key_login_dash_text: String {
            return NSLocalizedString("KEY_LOGIN_DASH_TEXT", comment: "")
        }
        static var key_login_user_pass_error_message: String {
            return NSLocalizedString("KEY_LOGIN_USER_PASS_ERROR_MESSAGE", comment: "")
        }
        static var key_login_user_pass_block_message: String {
            return NSLocalizedString("KEY_LOGIN_USER_PASS_BLOCK_MESSAGE", comment: "")
        }
    }
    struct didactica {
        static var key_didactic_hello_text: String {
            return NSLocalizedString("KEY_DIDACTIC_HELLO_TEXT", comment: "")
        }
        static var key_didactic_exclamation_mark_text: String {
            return NSLocalizedString("KEY_DIDACTIC_EXCLAMATION_MARK_TEXT", comment: "")
        }
        static var key_didactic_explore_text: String {
            return NSLocalizedString("KEY_DIDACTIC_EXPLORE_TEXT", comment: "")
        }
        static var key_didactic_promotions_text: String {
            return NSLocalizedString("KEY_DIDACTIC_PROMOTIONS_TEXT", comment: "")
        }
        static var key_didactic_discover_text: String {
            return NSLocalizedString("KEY_DIDACTIC_DISCOVER_TEXT", comment: "")
        }
    }
    struct points {
        static var key_points_error_occured_text: String {
            return NSLocalizedString("KEY_POINTS_ERROR_OCCURED_TEXT", comment: "")
        }
        static var key_points_use_your_card_accumulate_points_text: String {
            return NSLocalizedString("KEY_POINTS_USE_YOUR_CARD_ACCUMULATE_POINTS_TEXT", comment: "")
        }
        static var key_points_text: String {
            return NSLocalizedString("KEY_POINTS_TEXT", comment: "")
        }
        static var key_points_expire_text: String {
            return NSLocalizedString("KEY_POINTS_EXPIRE_TEXT", comment: "")
        }
        static var key_points_how_to_get_points_text: String {
            return NSLocalizedString("KEY_POINTS_HOW_TO_GET_POINTS_TEXT", comment: "")
        }
        static var key_points_how_to_get_more_points_text: String {
            return NSLocalizedString("KEY_POINTS_HOW_TO_GET_MORE_POINTS_TEXT", comment: "")
        }
        static var key_points_balance_text: String {
            return NSLocalizedString("KEY_POINTS_BALANCE_TEXT", comment: "")
        }
        static var key_points_have_redmeemed_text: String {
            return NSLocalizedString("KEY_POINTS_HAVE_REDMEEMED_TEXT", comment: "")
        }
        static var key_points_card_without_points_text: String {
            return NSLocalizedString("KEY_POINTS_CARD_WITHOUT_POINTS_TEXT", comment: "")
        }
        static var key_points_from_to_date_text: String {
            return NSLocalizedString("KEY_POINTS_FROM_TO_DATE_TEXT", comment: "")
        }
        static var key_points_previous_balance_text: String {
            return NSLocalizedString("KEY_POINTS_PREVIOUS_BALANCE_TEXT", comment: "")
        }
        static var key_points_generated_text: String {
            return NSLocalizedString("KEY_POINTS_GENERATED_TEXT", comment: "")
        }
        static var key_points_used_text: String {
            return NSLocalizedString("KEY_POINTS_USED_TEXT", comment: "")
        }
        static var key_points_expired_text: String {
            return NSLocalizedString("KEY_POINTS_EXPIRED_TEXT", comment: "")
        }
        static var key_points_error_loading_try_again_text: String {
            return NSLocalizedString("KEY_POINTS_ERROR_LOADING_TRY_AGAIN_TEXT", comment: "")
        }
        static var key_points_reload_text: String {
            return NSLocalizedString("KEY_POINTS_RELOAD_TEXT", comment: "")
        }
        static var key_points_get_points_bbva: String {
            return NSLocalizedString("KEY_POINTS_GET_POINTS_BBVA", comment: "")
        }
        static var key_points_win_points: String {
            return NSLocalizedString("KEY_POINTS_WIN_POINTS", comment: "")
        }
        static var key_points_see_offers_to_redeem_points: String {
            return NSLocalizedString("KEY_POINTS_SEE_OFFERS_TO_REDEEM_POINTS", comment: "")
        }
        static var key_points_redeem_points: String {
            return NSLocalizedString("KEY_POINTS_REDEEM_POINTS", comment: "")
        }
        static var key_points_redeem_points_info_text: String {
            return NSLocalizedString("KEY_POINTS_REDEEM_POINTS_INFO_TEXT", comment: "")
        }
        static var key_points_no_purchase_to_redeem_text: String {
            return NSLocalizedString("KEY_POINTS_NO_PURCHASE_TO_REDEEM_TEXT", comment: "")
        }
        static var key_points_no_points_to_redeem_text: String {
            return NSLocalizedString("KEY_POINTS_NO_POINTS_TO_REDEEM_TEXT", comment: "")
        }
        static var key_points_equivalent_to_text: String {
            return NSLocalizedString("KEY_POINTS_EQUIVALENT_TO_TEXT", comment: "")
        }
        static var key_points_dashboard_text: String {
            return NSLocalizedString("KEY_POINTS_DASHBOARD_TEXT", comment: "")
        }
        static var key_points_dashboard_no_points_text: String {
            return NSLocalizedString("KEY_POINTS_DASHBOARD_NO_POINTS_TEXT", comment: "")
        }
        static var key_points_dashboard_learn_more_text: String {
            return NSLocalizedString("KEY_POINTS_DASHBOARD_LEARN_MORE_TEXT", comment: "")
        }
        static var key_points_error_loading_text: String {
            return NSLocalizedString("KEY_POINTS_ERROR_LOADING_TEXT", comment: "")
        }
        static var key_points_info_title_error_text: String {
            return NSLocalizedString("KEY_POINTS_INFO_TITLE_ERROR_TEXT", comment: "")
        }
        static var key_points_info_title_text: String {
            return NSLocalizedString("KEY_POINTS_INFO_TITLE_TEXT", comment: "")
        }
        static var key_points_from_date_to_date_text: String {
            return NSLocalizedString("KEY_POINTS_FROM_DATE_TO_DATE_TEXT", comment: "")
        }
        static var key_points_pattern_date_start: String {
            return NSLocalizedString("KEY_POINTS_PATTERN_DATE_START", comment: "")
        }
        static var key_points_pattern_date_end: String {
            return NSLocalizedString("KEY_POINTS_PATTERN_DATE_END", comment: "")
        }
        static var key_points_pattern_date_server: String {
            return NSLocalizedString("KEY_POINTS_PATTERN_DATE_SERVER", comment: "")
        }
        static var key_points_info_updated_yesterday: String {
            return NSLocalizedString("KEY_POINTS_INFO_UPDATED_YESTERDAY", comment: "")
        }
        static var key_points_card_list_text: String {
            return NSLocalizedString("KEY_POINTS_CARD_LIST_TEXT", comment: "")
        }
    }
    struct transactions {
        static var key_transactions_last_text: String {
            return NSLocalizedString("KEY_TRANSACTIONS_LAST_TEXT", comment: "")
        }
        static var key_transactions_seeall_text: String {
            return NSLocalizedString("KEY_TRANSACTIONS_SEEALL_TEXT", comment: "")
        }
        static var key_transactions_transactions_text: String {
            return NSLocalizedString("KEY_TRANSACTIONS_TRANSACTIONS_TEXT", comment: "")
        }
        static var key_transactions_search_text: String {
            return NSLocalizedString("KEY_TRANSACTIONS_SEARCH_TEXT", comment: "")
        }
        static var key_transactions_date_text: String {
            return NSLocalizedString("KEY_TRANSACTIONS_DATE_TEXT", comment: "")
        }
        static var key_transactions_amount_text: String {
            return NSLocalizedString("KEY_TRANSACTIONS_AMOUNT_TEXT", comment: "")
        }
        static var key_transactions_pending_text: String {
            return NSLocalizedString("KEY_TRANSACTIONS_PENDING_TEXT", comment: "")
        }
        static var key_transactions_financiable_text: String {
            return NSLocalizedString("KEY_TRANSACTIONS_FINANCIABLE_TEXT", comment: "")
        }
        static var key_transactions_financed_text: String {
            return NSLocalizedString("KEY_TRANSACTIONS_FINANCED_TEXT", comment: "")
        }
        static var key_transactions_contains_text: String {
            return NSLocalizedString("KEY_TRANSACTIONS_CONTAINS_TEXT", comment: "")
        }
        static var key_transactions_from_text: String {
            return NSLocalizedString("KEY_TRANSACTIONS_FROM_TEXT", comment: "")
        }
        static var key_transactions_to_text: String {
            return NSLocalizedString("KEY_TRANSACTIONS_TO_TEXT", comment: "")
        }
        static var key_transactions_pickdate_text: String {
            return NSLocalizedString("KEY_TRANSACTIONS_PICKDATE_TEXT", comment: "")
        }
        static var key_transactions_filters_text: String {
            return NSLocalizedString("KEY_TRANSACTIONS_FILTERS_TEXT", comment: "")
        }
        static var key_transactions_type_text: String {
            return NSLocalizedString("KEY_TRANSACTIONS_TYPE_TEXT", comment: "")
        }
        static var key_transactions_all_text: String {
            return NSLocalizedString("KEY_TRANSACTIONS_ALL_TEXT", comment: "")
        }
        static var key_transactions_income_text: String {
            return NSLocalizedString("KEY_TRANSACTIONS_INCOME_TEXT", comment: "")
        }
        static var key_transactions_expenses_text: String {
            return NSLocalizedString("KEY_TRANSACTIONS_EXPENSES_TEXT", comment: "")
        }
        static var key_transactions_restore_button: String {
            return NSLocalizedString("KEY_TRANSACTIONS_RESTORE_BUTTON", comment: "")
        }
        static var key_transactions_results_button: String {
            return NSLocalizedString("KEY_TRANSACTIONS_RESULTS_BUTTON", comment: "")
        }
        static var key_transactions_oups_text: String {
            return NSLocalizedString("KEY_TRANSACTIONS_OUPS_TEXT", comment: "")
        }
        static var key_transactions_noresults_text: String {
            return NSLocalizedString("KEY_TRANSACTIONS_NORESULTS_TEXT", comment: "")
        }
        static var key_transactions_deleteall_text: String {
            return NSLocalizedString("KEY_TRANSACTIONS_DELETEALL_TEXT", comment: "")
        }
        static var key_transactions_searchtransactions_text: String {
            return NSLocalizedString("KEY_TRANSACTIONS_SEARCHTRANSACTIONS_TEXT", comment: "")
        }
        static var key_transactions_error_text: String {
            return NSLocalizedString("KEY_TRANSACTIONS_ERROR_TEXT", comment: "")
        }
        static var key_transactions_reload_text: String {
            return NSLocalizedString("KEY_TRANSACTIONS_RELOAD_TEXT", comment: "")
        }
        static var key_transactions_amount_range_error_text: String {
            return NSLocalizedString("KEY_TRANSACTIONS_AMOUNT_RANGE_ERROR_TEXT", comment: "")
        }
        static var key_card_movement_details_title_text: String {
            return NSLocalizedString("KEY_CARD_MOVEMENT_DETAILS_TITLE_TEXT", comment: "")
        }
        static var key_card_movement_details_explanation_text: String {
            return NSLocalizedString("KEY_CARD_MOVEMENT_DETAILS_EXPLANATION_TEXT", comment: "")
        }
        static var key_card_movement_details_date_operation_text: String {
            return NSLocalizedString("KEY_CARD_MOVEMENT_DETAILS_DATE_OPERATION_TEXT", comment: "")
        }
        static var key_card_movement_details_date_application_text: String {
            return NSLocalizedString("KEY_CARD_MOVEMENT_DETAILS_DATE_APPLICATION_TEXT", comment: "")
        }
        static var key_card_movement_details_folio_text: String {
            return NSLocalizedString("KEY_CARD_MOVEMENT_DETAILS_FOLIO_TEXT", comment: "")
        }
        static var key_card_movement_details_card_text: String {
            return NSLocalizedString("KEY_CARD_MOVEMENT_DETAILS_CARD_TEXT", comment: "")
        }
        static var key_card_movement_today_text: String {
            return NSLocalizedString("KEY_CARD_MOVEMENT_TODAY_TEXT", comment: "")
        }
        static var key_card_movement_yesterday_text: String {
            return NSLocalizedString("KEY_CARD_MOVEMENT_YESTERDAY_TEXT", comment: "")
        }
        static var key_card_movement_till_yesterday_text: String {
            return NSLocalizedString("KEY_CARD_MOVEMENT_TILL_YESTERDAY_TEXT", comment: "")
        }
        static var key_card_movement_last_three_movements_text: String {
            return NSLocalizedString("KEY_CARD_MOVEMENT_LAST_THREE_MOVEMENTS_TEXT", comment: "")
        }
        static var key_card_movement_see_and_search_text: String {
            return NSLocalizedString("KEY_CARD_MOVEMENT_SEE_AND_SEARCH_TEXT", comment: "")
        }
        static var key_not_movements_text: String {
            return NSLocalizedString("KEY_NOT_MOVEMENTS_TEXT", comment: "")
        }
    }
    struct notifications {
        static var key_ticket_text: String {
            return NSLocalizedString("KEY_TICKET_TEXT", comment: "")
        }
        static var key_approved_purchase_text: String {
            return NSLocalizedString("KEY_APPROVED_PURCHASE_TEXT", comment: "")
        }
        static var key_go_to_wallet_text: String {
            return NSLocalizedString("KEY_GO_TO_WALLET_TEXT", comment: "")
        }
        static var key_account_title_text: String {
            return NSLocalizedString("KEY_ACCOUNT_TITLE_TEXT", comment: "")
        }
        static var key_credit_title_text: String {
            return NSLocalizedString("KEY_CREDIT_TITLE_TEXT", comment: "")
        }
        static var key_denied_purchase_text: String {
            return NSLocalizedString("KEY_DENIED_PURCHASE_TEXT", comment: "")
        }
        static var key_unrealized_transaction_retry_text: String {
            return NSLocalizedString("KEY_UNREALIZED_TRANSACTION_RETRY_TEXT", comment: "")
        }
        static var key_not_balance_text: String {
            return NSLocalizedString("KEY_NOT_BALANCE_TEXT", comment: "")
        }
        static var key_exceeded_credit_text: String {
            return NSLocalizedString("KEY_EXCEEDED_CREDIT_TEXT", comment: "")
        }
        static var key_amount_purchase_exceeded_credit_text: String {
            return NSLocalizedString("KEY_AMOUNT_PURCHASE_EXCEEDED_CREDIT_TEXT", comment: "")
        }
        static var key_card_off_text: String {
            return NSLocalizedString("KEY_CARD_OFF_TEXT", comment: "")
        }
        static var key_card_reported_for_lost_text: String {
            return NSLocalizedString("KEY_CARD_REPORTED_FOR_LOST_TEXT", comment: "")
        }
        static var key_card_reported_for_theft_text: String {
            return NSLocalizedString("KEY_CARD_REPORTED_FOR_THEFT_TEXT", comment: "")
        }
        static var key_wrong_expiration_date_text: String {
            return NSLocalizedString("KEY_WRONG_EXPIRATION_DATE_TEXT", comment: "")
        }
        static var key_enter_the_application_for_purchase_text: String {
            return NSLocalizedString("KEY_ENTER_THE_APPLICATION_FOR_PURCHASE_TEXT", comment: "")
        }
        static var key_wrong_security_code_text: String {
            return NSLocalizedString("KEY_WRONG_SECURITY_CODE_TEXT", comment: "")
        }
        static var key_exceeded_number_attemps_security_code_text: String {
            return NSLocalizedString("KEY_EXCEEDED_NUMBER_ATTEMPS_SECURITY_CODE_TEXT", comment: "")
        }
        static var key_ios_permission_title_alert: String {
            return NSLocalizedString("KEY_IOS_PERMISSION_TITLE_ALERT", comment: "")
        }
        static var key_ios_permission_description_alert: String {
            return NSLocalizedString("KEY_IOS_PERMISSION_DESCRIPTION_ALERT", comment: "")
        }
        static var key_ios_permission_deny_button_alert: String {
            return NSLocalizedString("KEY_IOS_PERMISSION_DENY_BUTTON_ALERT", comment: "")
        }
        static var key_ios_permission_accept_button_alert: String {
            return NSLocalizedString("KEY_IOS_PERMISSION_ACCEPT_BUTTON_ALERT", comment: "")
        }
        static var key_ios_permission_denied_text: String {
            return NSLocalizedString("KEY_IOS_PERMISSION_DENIED_TEXT", comment: "")
        }
        static var key_ios_permission_accepted_text: String {
            return NSLocalizedString("KEY_IOS_PERMISSION_ACCEPTED_TEXT", comment: "")
        }
        static var key_mobile_payment_security_off_text: String {
            return NSLocalizedString("KEY_MOBILE_PAYMENT_SECURITY_OFF_TEXT", comment: "")
        }
        static var key_mobile_payment_login_reactivate_text: String {
            return NSLocalizedString("KEY_MOBILE_PAYMENT_LOGIN_REACTIVATE_TEXT", comment: "")
        }
        static var key_mobile_payment_login: String {
            return NSLocalizedString("KEY_MOBILE_PAYMENT_LOGIN", comment: "")
        }
        static var key_mobile_payment_off_activate_text: String {
            return NSLocalizedString("KEY_MOBILE_PAYMENT_OFF_ACTIVATE_TEXT", comment: "")
        }
        static var key_mobile_payment_configuration_text: String {
            return NSLocalizedString("KEY_MOBILE_PAYMENT_CONFIGURATION_TEXT", comment: "")
        }
        static var key_mobile_payment_security_time_off_text: String {
            return NSLocalizedString("KEY_MOBILE_PAYMENT_SECURITY_TIME_OFF_TEXT", comment: "")
        }
        static var key_mobile_payment_login_reactivation_text: String {
            return NSLocalizedString("KEY_MOBILE_PAYMENT_LOGIN_REACTIVATION_TEXT", comment: "")
        }
        static var key_mobile_payment_login_text: String {
            return NSLocalizedString("KEY_MOBILE_PAYMENT_LOGIN_TEXT", comment: "")
        }
        static var key_mobile_payment_limit_text: String {
            return NSLocalizedString("KEY_MOBILE_PAYMENT_LIMIT_TEXT", comment: "")
        }
        static var key_mobile_payment_lock_screen_text: String {
            return NSLocalizedString("KEY_MOBILE_PAYMENT_LOCK_SCREEN_TEXT", comment: "")
        }
        static var key_mobile_payment_limit_lock_screen_text: String {
            return NSLocalizedString("KEY_MOBILE_PAYMENT_LIMIT_LOCK_SCREEN_TEXT", comment: "")
        }
        static var key_mobile_payment_expired_notification_text: String {
            return NSLocalizedString("KEY_MOBILE_PAYMENT_EXPIRED_NOTIFICATION_TEXT", comment: "")
        }
        static var key_mobile_payment_notification_channel_description: String {
            return NSLocalizedString("KEY_MOBILE_PAYMENT_NOTIFICATION_CHANNEL_DESCRIPTION", comment: "")
        }
        static var key_mobile_payment_hce_notification_processing_payment_text: String {
            return NSLocalizedString("KEY_MOBILE_PAYMENT_HCE_NOTIFICATION_PROCESSING_PAYMENT_TEXT", comment: "")
        }
        static var key_mobile_payment_hce_notification_wait_confirmation_text: String {
            return NSLocalizedString("KEY_MOBILE_PAYMENT_HCE_NOTIFICATION_WAIT_CONFIRMATION_TEXT", comment: "")
        }
    }
    struct menu_lateral {
        static var key_menu_lateral_configuration: String {
            return NSLocalizedString("KEY_MENU_LATERAL_CONFIGURATION", comment: "")
        }
        static var key_menu_lateral_notifications: String {
            return NSLocalizedString("KEY_MENU_LATERAL_NOTIFICATIONS", comment: "")
        }
        static var key_menu_lateral_change: String {
            return NSLocalizedString("KEY_MENU_LATERAL_CHANGE", comment: "")
        }
        static var key_menu_lateral_deactivated: String {
            return NSLocalizedString("KEY_MENU_LATERAL_DEACTIVATED", comment: "")
        }
        static var key_menu_lateral_activated: String {
            return NSLocalizedString("KEY_MENU_LATERAL_ACTIVATED", comment: "")
        }
        static var key_menu_lateral_init_section: String {
            return NSLocalizedString("KEY_MENU_LATERAL_INIT_SECTION", comment: "")
        }
        static var key_menu_lateral_visible_cards_and_balances: String {
            return NSLocalizedString("KEY_MENU_LATERAL_VISIBLE_CARDS_AND_BALANCES", comment: "")
        }
        static var key_menu_lateral_invisible_cards_and_balances: String {
            return NSLocalizedString("KEY_MENU_LATERAL_INVISIBLE_CARDS_AND_BALANCES", comment: "")
        }
        static var key_menu_lateral_legal: String {
            return NSLocalizedString("KEY_MENU_LATERAL_LEGAL", comment: "")
        }
        static var key_menu_lateral_read: String {
            return NSLocalizedString("KEY_MENU_LATERAL_READ", comment: "")
        }
        static var key_menu_lateral_version: String {
            return NSLocalizedString("KEY_MENU_LATERAL_VERSION", comment: "")
        }
        static var key_menu_lateral_configuration_show_data_text: String {
            return NSLocalizedString("KEY_MENU_LATERAL_CONFIGURATION_SHOW_DATA_TEXT", comment: "")
        }
        static var key_menu_lateral_configuration_hide_data_text: String {
            return NSLocalizedString("KEY_MENU_LATERAL_CONFIGURATION_HIDE_DATA_TEXT", comment: "")
        }
        static var key_menu_lateral_configuration_info_show_text: String {
            return NSLocalizedString("KEY_MENU_LATERAL_CONFIGURATION_INFO_SHOW_TEXT", comment: "")
        }
        static var key_menu_lateral_configuration_info_hide_text: String {
            return NSLocalizedString("KEY_MENU_LATERAL_CONFIGURATION_INFO_HIDE_TEXT", comment: "")
        }
        static var key_menu_lateral_configuration_more_info_text: String {
            return NSLocalizedString("KEY_MENU_LATERAL_CONFIGURATION_MORE_INFO_TEXT", comment: "")
        }
        static var key_menu_lateral_about: String {
            return NSLocalizedString("KEY_MENU_LATERAL_ABOUT", comment: "")
        }
        static var key_menu_lateral_share_title: String {
            return NSLocalizedString("KEY_MENU_LATERAL_SHARE_TITLE", comment: "")
        }
        static var key_menu_lateral_share_text: String {
            return NSLocalizedString("KEY_MENU_LATERAL_SHARE_TEXT", comment: "")
        }
        static var key_menu_lateral_header_text: String {
            return NSLocalizedString("KEY_MENU_LATERAL_HEADER_TEXT", comment: "")
        }
        static var key_menu_lateral_about_legal_header: String {
            return NSLocalizedString("KEY_MENU_LATERAL_ABOUT_LEGAL_HEADER", comment: "")
        }
        static var key_menu_about_legal_text: String {
            return NSLocalizedString("KEY_MENU_ABOUT_LEGAL_TEXT", comment: "")
        }
        static var key_menu_about_tyc_text: String {
            return NSLocalizedString("KEY_MENU_ABOUT_TYC_TEXT", comment: "")
        }
        static var key_menu_about_cud_text: String {
            return NSLocalizedString("KEY_MENU_ABOUT_CUD_TEXT", comment: "")
        }
        static var key_menu_about_privacy_text: String {
            return NSLocalizedString("KEY_MENU_ABOUT_PRIVACY_TEXT", comment: "")
        }
        static var key_menu_lateral_token: String {
            return NSLocalizedString("KEY_MENU_LATERAL_TOKEN", comment: "")
        }
    }
    struct cards {
        static var key_cards_availablebalance_text: String {
            return NSLocalizedString("KEY_CARDS_AVAILABLEBALANCE_TEXT", comment: "")
        }
        static var key_cards_availablecredit_text: String {
            return NSLocalizedString("KEY_CARDS_AVAILABLECREDIT_TEXT", comment: "")
        }
        static var key_cards_prepaidbalance_text: String {
            return NSLocalizedString("KEY_CARDS_PREPAIDBALANCE_TEXT", comment: "")
        }
        static var key_cards_linkedbalance_text: String {
            return NSLocalizedString("KEY_CARDS_LINKEDBALANCE_TEXT", comment: "")
        }
        static var key_cards_card_text: String {
            return NSLocalizedString("KEY_CARDS_CARD_TEXT", comment: "")
        }
        static var key_cards_account_text: String {
            return NSLocalizedString("KEY_CARDS_ACCOUNT_TEXT", comment: "")
        }
        static var key_cards_consumed_text: String {
            return NSLocalizedString("KEY_CARDS_CONSUMED_TEXT", comment: "")
        }
        static var key_cards_limit_text: String {
            return NSLocalizedString("KEY_CARDS_LIMIT_TEXT", comment: "")
        }
        static var key_cards_activate_card_text: String {
            return NSLocalizedString("KEY_CARDS_ACTIVATE_CARD_TEXT", comment: "")
        }
        static var key_cards_show_edit_text: String {
            return NSLocalizedString("KEY_CARDS_SHOW_EDIT_TEXT", comment: "")
        }
        static var key_cards_set_up_limits_text: String {
            return NSLocalizedString("KEY_CARDS_SET_UP_LIMITS_TEXT", comment: "")
        }
        static var key_cards_online_settings_text: String {
            return NSLocalizedString("KEY_CARDS_ONLINE_SETTINGS_TEXT", comment: "")
        }
        static var key_cards_three_more_text: String {
            return NSLocalizedString("KEY_CARDS_THREE_MORE_TEXT", comment: "")
        }
        static var key_cards_reload_download_text: String {
            return NSLocalizedString("KEY_CARDS_RELOAD_DOWNLOAD_TEXT", comment: "")
        }
        static var key_cards_pay_mode_text: String {
            return NSLocalizedString("KEY_CARDS_PAY_MODE_TEXT", comment: "")
        }
        static var key_cards_mobile_payment_text: String {
            return NSLocalizedString("KEY_CARDS_MOBILE_PAYMENT_TEXT", comment: "")
        }
        static var key_cards_on_off_text: String {
            return NSLocalizedString("KEY_CARDS_ON_OFF_TEXT", comment: "")
        }
        static var key_cards_off_text: String {
            return NSLocalizedString("KEY_CARDS_OFF_TEXT", comment: "")
        }
        static var key_cards_on_text: String {
            return NSLocalizedString("KEY_CARDS_ON_TEXT", comment: "")
        }
        static var key_cards_activate_description_text: String {
            return NSLocalizedString("KEY_CARDS_ACTIVATE_DESCRIPTION_TEXT", comment: "")
        }
        static var key_cards_copy_text: String {
            return NSLocalizedString("KEY_CARDS_COPY_TEXT", comment: "")
        }
        static var key_cards_copy_success_text: String {
            return NSLocalizedString("KEY_CARDS_COPY_SUCCESS_TEXT", comment: "")
        }
        static var key_cards_copy_fail_text: String {
            return NSLocalizedString("KEY_CARDS_COPY_FAIL_TEXT", comment: "")
        }
        static var key_cards_on_successful_text: String {
            return NSLocalizedString("KEY_CARDS_ON_SUCCESSFUL_TEXT", comment: "")
        }
        static var key_cards_off_successful_text: String {
            return NSLocalizedString("KEY_CARDS_OFF_SUCCESSFUL_TEXT", comment: "")
        }
        static var key_cards_physics_type_text: String {
            return NSLocalizedString("KEY_CARDS_PHYSICS_TYPE_TEXT", comment: "")
        }
        static var key_cards_digital_type_text: String {
            return NSLocalizedString("KEY_CARDS_DIGITAL_TYPE_TEXT", comment: "")
        }
        static var key_cards_sticker_type_text: String {
            return NSLocalizedString("KEY_CARDS_STICKER_TYPE_TEXT", comment: "")
        }
        static var key_cards_how_unlock_card_text: String {
            return NSLocalizedString("KEY_CARDS_HOW_UNLOCK_CARD_TEXT", comment: "")
        }
        static var key_cards_get_digital_card_text: String {
            return NSLocalizedString("KEY_CARDS_GET_DIGITAL_CARD_TEXT", comment: "")
        }
        static var key_card_generate_digital_card_text: String {
            return NSLocalizedString("KEY_CARD_GENERATE_DIGITAL_CARD_TEXT", comment: "")
        }
        static var key_card_generate_digital_title_card_text: String {
            return NSLocalizedString("KEY_CARD_GENERATE_DIGITAL_TITLE_CARD_TEXT", comment: "")
        }
        static var key_cards_create_digital_card_text: String {
            return NSLocalizedString("KEY_CARDS_CREATE_DIGITAL_CARD_TEXT", comment: "")
        }
        static var key_cards_digitalize_disable_text: String {
            return NSLocalizedString("KEY_CARDS_DIGITALIZE_DISABLE_TEXT", comment: "")
        }
        static var key_cards_digitalize_disable_for_this_card_text: String {
            return NSLocalizedString("KEY_CARDS_DIGITALIZE_DISABLE_FOR_THIS_CARD_TEXT", comment: "")
        }
        static var key_cards_nfc_enabled_text: String {
            return NSLocalizedString("KEY_CARDS_NFC_ENABLED_TEXT", comment: "")
        }
        static var key_card_digital_text: String {
            return NSLocalizedString("KEY_CARD_DIGITAL_TEXT", comment: "")
        }
        static var key_card_see_cvv_text: String {
            return NSLocalizedString("KEY_CARD_SEE_CVV_TEXT", comment: "")
        }
        static var key_card_your_cvv_text: String {
            return NSLocalizedString("KEY_CARD_YOUR_CVV_TEXT", comment: "")
        }
        static var key_card_cvv_validation_text: String {
            return NSLocalizedString("KEY_CARD_CVV_VALIDATION_TEXT", comment: "")
        }
        static var key_cards_how_activate_card_text: String {
            return NSLocalizedString("KEY_CARDS_HOW_ACTIVATE_CARD_TEXT", comment: "")
        }
        static var key_cards_accumulate_points_text: String {
            return NSLocalizedString("KEY_CARDS_ACCUMULATE_POINTS_TEXT", comment: "")
        }
        static var key_cards_limits_text: String {
            return NSLocalizedString("KEY_CARDS_LIMITS_TEXT", comment: "")
        }
        static var key_cards_withdrawn_text: String {
            return NSLocalizedString("KEY_CARDS_WITHDRAWN_TEXT", comment: "")
        }
        static var key_cards_current_limit_text: String {
            return NSLocalizedString("KEY_CARDS_CURRENT_LIMIT_TEXT", comment: "")
        }
        static var key_card_limit_min_text: String {
            return NSLocalizedString("KEY_CARD_LIMIT_MIN_TEXT", comment: "")
        }
        static var key_card_limit_and_max_text: String {
            return NSLocalizedString("KEY_CARD_LIMIT_AND_MAX_TEXT", comment: "")
        }
        static var key_card_limit_max_text: String {
            return NSLocalizedString("KEY_CARD_LIMIT_MAX_TEXT", comment: "")
        }
        static var key_card_limit_today_consumed_text: String {
            return NSLocalizedString("KEY_CARD_LIMIT_TODAY_CONSUMED_TEXT", comment: "")
        }
        static var key_card_limit_you_text: String {
            return NSLocalizedString("KEY_CARD_LIMIT_YOU_TEXT", comment: "")
        }
        static var key_card_limit_is_text: String {
            return NSLocalizedString("KEY_CARD_LIMIT_IS_TEXT", comment: "")
        }
        static var key_card_block_card_text: String {
            return NSLocalizedString("KEY_CARD_BLOCK_CARD_TEXT", comment: "")
        }
        static var key_card_status_blocked_text: String {
            return NSLocalizedString("KEY_CARD_STATUS_BLOCKED_TEXT", comment: "")
        }
        static var key_card_status_pending_activation_text: String {
            return NSLocalizedString("KEY_CARD_STATUS_PENDING_ACTIVATION_TEXT", comment: "")
        }
        static var key_card_status_off_text: String {
            return NSLocalizedString("KEY_CARD_STATUS_OFF_TEXT", comment: "")
        }
        static var key_card_status_partial_off_text: String {
            return NSLocalizedString("KEY_CARD_STATUS_PARTIAL_OFF_TEXT", comment: "")
        }
        static var key_card_status_delivery_text: String {
            return NSLocalizedString("KEY_CARD_STATUS_DELIVERY_TEXT", comment: "")
        }
        static var key_card_limit_daily_cash_withdrawal: String {
            return NSLocalizedString("KEY_CARD_LIMIT_DAILY_CASH_WITHDRAWAL", comment: "")
        }
        static var key_card_limit_monthly_cash_withdrawal: String {
            return NSLocalizedString("KEY_CARD_LIMIT_MONTHLY_CASH_WITHDRAWAL", comment: "")
        }
        static var key_card_limit_daily_shopping: String {
            return NSLocalizedString("KEY_CARD_LIMIT_DAILY_SHOPPING", comment: "")
        }
        static var key_card_limit_monthly_shopping: String {
            return NSLocalizedString("KEY_CARD_LIMIT_MONTHLY_SHOPPING", comment: "")
        }
        static var key_card_limit_daily_withdrawal_in_office: String {
            return NSLocalizedString("KEY_CARD_LIMIT_DAILY_WITHDRAWAL_IN_OFFICE", comment: "")
        }
        static var key_card_limit_monthly_withdrawal_in_office: String {
            return NSLocalizedString("KEY_CARD_LIMIT_MONTHLY_WITHDRAWAL_IN_OFFICE", comment: "")
        }
        static var key_card_limit_daily_operations: String {
            return NSLocalizedString("KEY_CARD_LIMIT_DAILY_OPERATIONS", comment: "")
        }
        static var key_card_limit_monthly_operations: String {
            return NSLocalizedString("KEY_CARD_LIMIT_MONTHLY_OPERATIONS", comment: "")
        }
        static var key_card_limit_monthly_card_recharge: String {
            return NSLocalizedString("KEY_CARD_LIMIT_MONTHLY_CARD_RECHARGE", comment: "")
        }
        static var key_card_monthly_credit_card: String {
            return NSLocalizedString("KEY_CARD_MONTHLY_CREDIT_CARD", comment: "")
        }
        static var key_card_limit_maximum_consumption: String {
            return NSLocalizedString("KEY_CARD_LIMIT_MAXIMUM_CONSUMPTION", comment: "")
        }
        static var key_card_retired_today: String {
            return NSLocalizedString("KEY_CARD_RETIRED_TODAY", comment: "")
        }
        static var key_card_retired_this_month: String {
            return NSLocalizedString("KEY_CARD_RETIRED_THIS_MONTH", comment: "")
        }
        static var key_card_bought_today: String {
            return NSLocalizedString("KEY_CARD_BOUGHT_TODAY", comment: "")
        }
        static var key_card_bought_this_month: String {
            return NSLocalizedString("KEY_CARD_BOUGHT_THIS_MONTH", comment: "")
        }
        static var key_card_consumed_today: String {
            return NSLocalizedString("KEY_CARD_CONSUMED_TODAY", comment: "")
        }
        static var key_card_consumed_this_month: String {
            return NSLocalizedString("KEY_CARD_CONSUMED_THIS_MONTH", comment: "")
        }
        static var key_card_daily_limit: String {
            return NSLocalizedString("KEY_CARD_DAILY_LIMIT", comment: "")
        }
        static var key_card_monthly_limit: String {
            return NSLocalizedString("KEY_CARD_MONTHLY_LIMIT", comment: "")
        }
        static var key_card_retired_maximum_allowed_daily: String {
            return NSLocalizedString("KEY_CARD_RETIRED_MAXIMUM_ALLOWED_DAILY", comment: "")
        }
        static var key_card_retired_maximum_allowed_monthly: String {
            return NSLocalizedString("KEY_CARD_RETIRED_MAXIMUM_ALLOWED_MONTHLY", comment: "")
        }
        static var key_card_bought_maximum_allowed_daily: String {
            return NSLocalizedString("KEY_CARD_BOUGHT_MAXIMUM_ALLOWED_DAILY", comment: "")
        }
        static var key_card_bought_maximum_allowed_monthly: String {
            return NSLocalizedString("KEY_CARD_BOUGHT_MAXIMUM_ALLOWED_MONTHLY", comment: "")
        }
        static var key_card_consumed_maximum_allowed_daily: String {
            return NSLocalizedString("KEY_CARD_CONSUMED_MAXIMUM_ALLOWED_DAILY", comment: "")
        }
        static var key_card_consumed_maximum_allowed_monthly: String {
            return NSLocalizedString("KEY_CARD_CONSUMED_MAXIMUM_ALLOWED_MONTHLY", comment: "")
        }
        static var key_card_consumed_maximum_allowed: String {
            return NSLocalizedString("KEY_CARD_CONSUMED_MAXIMUM_ALLOWED", comment: "")
        }
        static var key_card_daily_limit_multiples: String {
            return NSLocalizedString("KEY_CARD_DAILY_LIMIT_MULTIPLES", comment: "")
        }
        static var key_card_monthly_limit_multiples: String {
            return NSLocalizedString("KEY_CARD_MONTHLY_LIMIT_MULTIPLES", comment: "")
        }
        static var key_card_actual_limit_multiples: String {
            return NSLocalizedString("KEY_CARD_ACTUAL_LIMIT_MULTIPLES", comment: "")
        }
        static var key_card_minimum_monthly: String {
            return NSLocalizedString("KEY_CARD_MINIMUM_MONTHLY", comment: "")
        }
        static var key_card_minimum: String {
            return NSLocalizedString("KEY_CARD_MINIMUM", comment: "")
        }
        static var key_card_minimum_this_month: String {
            return NSLocalizedString("KEY_CARD_MINIMUM_THIS_MONTH", comment: "")
        }
        static var key_card_today_you_have_retired: String {
            return NSLocalizedString("KEY_CARD_TODAY_YOU_HAVE_RETIRED", comment: "")
        }
        static var key_card_this_month_you_have_retired: String {
            return NSLocalizedString("KEY_CARD_THIS_MONTH_YOU_HAVE_RETIRED", comment: "")
        }
        static var key_card_today_already_bought: String {
            return NSLocalizedString("KEY_CARD_TODAY_ALREADY_BOUGHT", comment: "")
        }
        static var key_card_this_month_bought: String {
            return NSLocalizedString("KEY_CARD_THIS_MONTH_BOUGHT", comment: "")
        }
        static var key_card_this_month_consumed: String {
            return NSLocalizedString("KEY_CARD_THIS_MONTH_CONSUMED", comment: "")
        }
        static var key_card_already_consumed: String {
            return NSLocalizedString("KEY_CARD_ALREADY_CONSUMED", comment: "")
        }
        static var key_card_only_multiples: String {
            return NSLocalizedString("KEY_CARD_ONLY_MULTIPLES", comment: "")
        }
        static var key_card_cancel: String {
            return NSLocalizedString("KEY_CARD_CANCEL", comment: "")
        }
        static var key_card_turn_on_off: String {
            return NSLocalizedString("KEY_CARD_TURN_ON_OFF", comment: "")
        }
        static var key_card_help_on_off_debit_title_1: String {
            return NSLocalizedString("KEY_CARD_HELP_ON_OFF_DEBIT_TITLE_1", comment: "")
        }
        static var key_card_help_on_off_debit_text_1: String {
            return NSLocalizedString("KEY_CARD_HELP_ON_OFF_DEBIT_TEXT_1", comment: "")
        }
        static var key_card_help_on_off_debit_text_2: String {
            return NSLocalizedString("KEY_CARD_HELP_ON_OFF_DEBIT_TEXT_2", comment: "")
        }
        static var key_card_help_on_off_debit_text_3: String {
            return NSLocalizedString("KEY_CARD_HELP_ON_OFF_DEBIT_TEXT_3", comment: "")
        }
        static var key_card_help_on_off_debit_text_4: String {
            return NSLocalizedString("KEY_CARD_HELP_ON_OFF_DEBIT_TEXT_4", comment: "")
        }
        static var key_card_help_on_off_debit_title_2: String {
            return NSLocalizedString("KEY_CARD_HELP_ON_OFF_DEBIT_TITLE_2", comment: "")
        }
        static var key_card_help_on_off_debit_text_two_1: String {
            return NSLocalizedString("KEY_CARD_HELP_ON_OFF_DEBIT_TEXT_TWO_1", comment: "")
        }
        static var key_card_help_on_off_debit_text_two_2: String {
            return NSLocalizedString("KEY_CARD_HELP_ON_OFF_DEBIT_TEXT_TWO_2", comment: "")
        }
        static var key_card_help_on_off_debit_text_two_3: String {
            return NSLocalizedString("KEY_CARD_HELP_ON_OFF_DEBIT_TEXT_TWO_3", comment: "")
        }
        static var key_card_help_on_off_debit_text_two_4: String {
            return NSLocalizedString("KEY_CARD_HELP_ON_OFF_DEBIT_TEXT_TWO_4", comment: "")
        }
        static var key_card_on_off_card_text: String {
            return NSLocalizedString("KEY_CARD_ON_OFF_CARD_TEXT", comment: "")
        }
        static var key_card_info_on_text: String {
            return NSLocalizedString("KEY_CARD_INFO_ON_TEXT", comment: "")
        }
        static var key_card_info_off_text: String {
            return NSLocalizedString("KEY_CARD_INFO_OFF_TEXT", comment: "")
        }
        static var key_card_help_on_off_credit_title_1: String {
            return NSLocalizedString("KEY_CARD_HELP_ON_OFF_CREDIT_TITLE_1", comment: "")
        }
        static var key_card_help_on_off_credit_text_1: String {
            return NSLocalizedString("KEY_CARD_HELP_ON_OFF_CREDIT_TEXT_1", comment: "")
        }
        static var key_card_help_on_off_credit_text_2: String {
            return NSLocalizedString("KEY_CARD_HELP_ON_OFF_CREDIT_TEXT_2", comment: "")
        }
        static var key_card_help_on_off_credit_text_3: String {
            return NSLocalizedString("KEY_CARD_HELP_ON_OFF_CREDIT_TEXT_3", comment: "")
        }
        static var key_card_help_on_off_credit_text_4: String {
            return NSLocalizedString("KEY_CARD_HELP_ON_OFF_CREDIT_TEXT_4", comment: "")
        }
        static var key_card_help_on_off_credit_title_2: String {
            return NSLocalizedString("KEY_CARD_HELP_ON_OFF_CREDIT_TITLE_2", comment: "")
        }
        static var key_card_help_on_off_credit_text_two_1: String {
            return NSLocalizedString("KEY_CARD_HELP_ON_OFF_CREDIT_TEXT_TWO_1", comment: "")
        }
        static var key_card_help_on_off_credit_text_two_2: String {
            return NSLocalizedString("KEY_CARD_HELP_ON_OFF_CREDIT_TEXT_TWO_2", comment: "")
        }
        static var key_card_help_on_off_credit_text_two_3: String {
            return NSLocalizedString("KEY_CARD_HELP_ON_OFF_CREDIT_TEXT_TWO_3", comment: "")
        }
        static var key_card_help_on_off_credit_text_two_4: String {
            return NSLocalizedString("KEY_CARD_HELP_ON_OFF_CREDIT_TEXT_TWO_4", comment: "")
        }
        static var key_card_disclaimer_on_off: String {
            return NSLocalizedString("KEY_CARD_DISCLAIMER_ON_OFF", comment: "")
        }
        static var key_card_help_on_off_title_1: String {
            return NSLocalizedString("KEY_CARD_HELP_ON_OFF_TITLE_1", comment: "")
        }
        static var key_card_help_on_off_text_1: String {
            return NSLocalizedString("KEY_CARD_HELP_ON_OFF_TEXT_1", comment: "")
        }
        static var key_card_help_on_off_text_2: String {
            return NSLocalizedString("KEY_CARD_HELP_ON_OFF_TEXT_2", comment: "")
        }
        static var key_card_help_on_off_text_3: String {
            return NSLocalizedString("KEY_CARD_HELP_ON_OFF_TEXT_3", comment: "")
        }
        static var key_card_help_on_off_text_4: String {
            return NSLocalizedString("KEY_CARD_HELP_ON_OFF_TEXT_4", comment: "")
        }
        static var key_card_help_on_off_title_2: String {
            return NSLocalizedString("KEY_CARD_HELP_ON_OFF_TITLE_2", comment: "")
        }
        static var key_card_help_on_off_text_two_1: String {
            return NSLocalizedString("KEY_CARD_HELP_ON_OFF_TEXT_TWO_1", comment: "")
        }
        static var key_card_help_on_off_text_two_2: String {
            return NSLocalizedString("KEY_CARD_HELP_ON_OFF_TEXT_TWO_2", comment: "")
        }
        static var key_card_help_on_off_text_two_3: String {
            return NSLocalizedString("KEY_CARD_HELP_ON_OFF_TEXT_TWO_3", comment: "")
        }
        static var key_card_block_share_title: String {
            return NSLocalizedString("KEY_CARD_BLOCK_SHARE_TITLE", comment: "")
        }
        static var key_card_block_share_description: String {
            return NSLocalizedString("KEY_CARD_BLOCK_SHARE_DESCRIPTION", comment: "")
        }
        static var key_card_block_share_folder: String {
            return NSLocalizedString("KEY_CARD_BLOCK_SHARE_FOLDER", comment: "")
        }
        static var key_card_block_chooser_share_title: String {
            return NSLocalizedString("KEY_CARD_BLOCK_CHOOSER_SHARE_TITLE", comment: "")
        }
        static var key_not_recognized_operation_text: String {
            return NSLocalizedString("KEY_NOT_RECOGNIZED_OPERATION_TEXT", comment: "")
        }
        static var key_nfc_desactivated: String {
            return NSLocalizedString("KEY_NFC_DESACTIVATED", comment: "")
        }
        static var key_activate_card_cvv_error: String {
            return NSLocalizedString("KEY_ACTIVATE_CARD_CVV_ERROR", comment: "")
        }
        static var key_card_limits_multiples_and_maximum: String {
            return NSLocalizedString("KEY_CARD_LIMITS_MULTIPLES_AND_MAXIMUM", comment: "")
        }
        static var key_card_help_limits_title_1: String {
            return NSLocalizedString("KEY_CARD_HELP_LIMITS_TITLE_1", comment: "")
        }
        static var key_card_help_limits_text_1: String {
            return NSLocalizedString("KEY_CARD_HELP_LIMITS_TEXT_1", comment: "")
        }
        static var key_card_help_limits_text_2: String {
            return NSLocalizedString("KEY_CARD_HELP_LIMITS_TEXT_2", comment: "")
        }
        static var key_card_help_limits_text_3: String {
            return NSLocalizedString("KEY_CARD_HELP_LIMITS_TEXT_3", comment: "")
        }
        static var key_card_disclaimer_help_limits: String {
            return NSLocalizedString("KEY_CARD_DISCLAIMER_HELP_LIMITS", comment: "")
        }
        static var key_card_create_digital_card_information: String {
            return NSLocalizedString("KEY_CARD_CREATE_DIGITAL_CARD_INFORMATION", comment: "")
        }
        static var key_cards_generate_see_cvv: String {
            return NSLocalizedString("KEY_CARDS_GENERATE_SEE_CVV", comment: "")
        }
        static var key_card_help_limits: String {
            return NSLocalizedString("KEY_CARD_HELP_LIMITS", comment: "")
        }
        static var key_card_help_on_text_1: String {
            return NSLocalizedString("KEY_CARD_HELP_ON_TEXT_1", comment: "")
        }
        static var key_card_help_on_text_2: String {
            return NSLocalizedString("KEY_CARD_HELP_ON_TEXT_2", comment: "")
        }
        static var key_card_help_on_title_1: String {
            return NSLocalizedString("KEY_CARD_HELP_ON_TITLE_1", comment: "")
        }
        static var key_card_help_on_title_2: String {
            return NSLocalizedString("KEY_CARD_HELP_ON_TITLE_2", comment: "")
        }
        static var key_card_help_on_text_two_1: String {
            return NSLocalizedString("KEY_CARD_HELP_ON_TEXT_TWO_1", comment: "")
        }
        static var key_card_help_on_accept: String {
            return NSLocalizedString("KEY_CARD_HELP_ON_ACCEPT", comment: "")
        }
        static var key_card_turn_on: String {
            return NSLocalizedString("KEY_CARD_TURN_ON", comment: "")
        }
        static var key_no_cards_question: String {
            return NSLocalizedString("KEY_NO_CARDS_QUESTION", comment: "")
        }
        static var key_how_contract_cards_information: String {
            return NSLocalizedString("KEY_HOW_CONTRACT_CARDS_INFORMATION", comment: "")
        }
        static var key_call_bbva: String {
            return NSLocalizedString("KEY_CALL_BBVA", comment: "")
        }
        static var key_contract_cards_more_information: String {
            return NSLocalizedString("KEY_CONTRACT_CARDS_MORE_INFORMATION", comment: "")
        }
        static var key_card_help_on_off_text_5: String {
            return NSLocalizedString("KEY_CARD_HELP_ON_OFF_TEXT_5", comment: "")
        }
        static var key_card_help_limits_credit_card_title_1: String {
            return NSLocalizedString("KEY_CARD_HELP_LIMITS_CREDIT_CARD_TITLE_1", comment: "")
        }
        static var key_card_help_on_title: String {
            return NSLocalizedString("KEY_CARD_HELP_ON_TITLE", comment: "")
        }
        static var key_card_help_on_title_3: String {
            return NSLocalizedString("KEY_CARD_HELP_ON_TITLE_3", comment: "")
        }
        static var key_card_help_off_title: String {
            return NSLocalizedString("KEY_CARD_HELP_OFF_TITLE", comment: "")
        }
        static var key_card_help_off_title_1: String {
            return NSLocalizedString("KEY_CARD_HELP_OFF_TITLE_1", comment: "")
        }
        static var key_card_help_off_text_1: String {
            return NSLocalizedString("KEY_CARD_HELP_OFF_TEXT_1", comment: "")
        }
        static var key_card_help_off_text_2: String {
            return NSLocalizedString("KEY_CARD_HELP_OFF_TEXT_2", comment: "")
        }
        static var key_card_help_off_title_2: String {
            return NSLocalizedString("KEY_CARD_HELP_OFF_TITLE_2", comment: "")
        }
        static var key_card_help_off_text_two_1: String {
            return NSLocalizedString("KEY_CARD_HELP_OFF_TEXT_TWO_1", comment: "")
        }
        static var key_card_help_off_title_3: String {
            return NSLocalizedString("KEY_CARD_HELP_OFF_TITLE_3", comment: "")
        }
        static var key_card_help_off_text_three_1: String {
            return NSLocalizedString("KEY_CARD_HELP_OFF_TEXT_THREE_1", comment: "")
        }
        static var key_card_help_off_text_three_2: String {
            return NSLocalizedString("KEY_CARD_HELP_OFF_TEXT_THREE_2", comment: "")
        }
        static var key_card_help_off_text_three_3: String {
            return NSLocalizedString("KEY_CARD_HELP_OFF_TEXT_THREE_3", comment: "")
        }
        static var key_card_help_off_disclaimer: String {
            return NSLocalizedString("KEY_CARD_HELP_OFF_DISCLAIMER", comment: "")
        }
        static var key_card_help_off_accept: String {
            return NSLocalizedString("KEY_CARD_HELP_OFF_ACCEPT", comment: "")
        }
        static var key_card_help_on_off_accept: String {
            return NSLocalizedString("KEY_CARD_HELP_ON_OFF_ACCEPT", comment: "")
        }
        static var key_card_help_on_off_disclaimer: String {
            return NSLocalizedString("KEY_CARD_HELP_ON_OFF_DISCLAIMER", comment: "")
        }
        static var key_card_help_on_disclaimer: String {
            return NSLocalizedString("KEY_CARD_HELP_ON_DISCLAIMER", comment: "")
        }
        static var key_card_limit_daily_shopping_2: String {
            return NSLocalizedString("KEY_CARD_LIMIT_DAILY_SHOPPING_2", comment: "")
        }
        static var key_card_your_cards: String {
            return NSLocalizedString("KEY_CARD_YOUR_CARDS", comment: "")
        }
        static var key_card_payment_with_phone_not_available: String {
            return NSLocalizedString("KEY_CARD_PAYMENT_WITH_PHONE_NOT_AVAILABLE", comment: "")
        }
        static var key_card_have_another_default_app_for_payments: String {
            return NSLocalizedString("KEY_CARD_HAVE_ANOTHER_DEFAULT_APP_FOR_PAYMENTS", comment: "")
        }
        static var key_card_select_bbva_wallet: String {
            return NSLocalizedString("KEY_CARD_SELECT_BBVA_WALLET", comment: "")
        }
        static var key_card_you_need_activate_nfc: String {
            return NSLocalizedString("KEY_CARD_YOU_NEED_ACTIVATE_NFC", comment: "")
        }
        static var key_card_limit_atm_daily_multiples_and_maximum: String {
            return NSLocalizedString("KEY_CARD_LIMIT_ATM_DAILY_MULTIPLES_AND_MAXIMUM", comment: "")
        }
        static var key_card_limit_atm_daily_message_success: String {
            return NSLocalizedString("KEY_CARD_LIMIT_ATM_DAILY_MESSAGE_SUCCESS", comment: "")
        }
        static var key_card_limit_pos_daily_multiples_and_maximum: String {
            return NSLocalizedString("KEY_CARD_LIMIT_POS_DAILY_MULTIPLES_AND_MAXIMUM", comment: "")
        }
        static var key_card_limit_pos_daily_message_success: String {
            return NSLocalizedString("KEY_CARD_LIMIT_POS_DAILY_MESSAGE_SUCCESS", comment: "")
        }
        static var key_card_limit_office_daily_multiples_and_maximum: String {
            return NSLocalizedString("KEY_CARD_LIMIT_OFFICE_DAILY_MULTIPLES_AND_MAXIMUM", comment: "")
        }
        static var key_card_limit_office_daily_message_success: String {
            return NSLocalizedString("KEY_CARD_LIMIT_OFFICE_DAILY_MESSAGE_SUCCESS", comment: "")
        }
        static var key_card_limit_transaction_daily_multiples_and_maximum: String {
            return NSLocalizedString("KEY_CARD_LIMIT_TRANSACTION_DAILY_MULTIPLES_AND_MAXIMUM", comment: "")
        }
        static var key_card_limit_transaction_daily_message_success: String {
            return NSLocalizedString("KEY_CARD_LIMIT_TRANSACTION_DAILY_MESSAGE_SUCCESS", comment: "")
        }
    }
    struct onboarding {
        static var key_onboarding_points_title: String {
            return NSLocalizedString("KEY_ONBOARDING_POINTS_TITLE", comment: "")
        }
        static var key_onboarding_points_description: String {
            return NSLocalizedString("KEY_ONBOARDING_POINTS_DESCRIPTION", comment: "")
        }
        static var key_onboarding_points_update_wallet_title: String {
            return NSLocalizedString("KEY_ONBOARDING_POINTS_UPDATE_WALLET_TITLE", comment: "")
        }
        static var key_onboarding_points_update_wallet_description: String {
            return NSLocalizedString("KEY_ONBOARDING_POINTS_UPDATE_WALLET_DESCRIPTION", comment: "")
        }
        static var key_onboarding_pay_with_app_title: String {
            return NSLocalizedString("KEY_ONBOARDING_PAY_WITH_APP_TITLE", comment: "")
        }
        static var key_onboarding_pay_with_app_description: String {
            return NSLocalizedString("KEY_ONBOARDING_PAY_WITH_APP_DESCRIPTION", comment: "")
        }
        static var key_onboarding_pay_with_sticker_title: String {
            return NSLocalizedString("KEY_ONBOARDING_PAY_WITH_STICKER_TITLE", comment: "")
        }
        static var key_onboarding_pay_with_sticker_description: String {
            return NSLocalizedString("KEY_ONBOARDING_PAY_WITH_STICKER_DESCRIPTION", comment: "")
        }
        static var key_onboarding_on_off_title: String {
            return NSLocalizedString("KEY_ONBOARDING_ON_OFF_TITLE", comment: "")
        }
        static var key_onboarding_on_off_description: String {
            return NSLocalizedString("KEY_ONBOARDING_ON_OFF_DESCRIPTION", comment: "")
        }
        static var key_onboarding_digital_card_title: String {
            return NSLocalizedString("KEY_ONBOARDING_DIGITAL_CARD_TITLE", comment: "")
        }
        static var key_onboarding_digital_card_description: String {
            return NSLocalizedString("KEY_ONBOARDING_DIGITAL_CARD_DESCRIPTION", comment: "")
        }
        static var key_onboarding_new_wallet_title: String {
            return NSLocalizedString("KEY_ONBOARDING_NEW_WALLET_TITLE", comment: "")
        }
        static var key_onboarding_new_wallet_description: String {
            return NSLocalizedString("KEY_ONBOARDING_NEW_WALLET_DESCRIPTION", comment: "")
        }
        static var key_onboarding_update_wallet_title: String {
            return NSLocalizedString("KEY_ONBOARDING_UPDATE_WALLET_TITLE", comment: "")
        }
        static var key_onboarding_update_wallet_description: String {
            return NSLocalizedString("KEY_ONBOARDING_UPDATE_WALLET_DESCRIPTION", comment: "")
        }
        static var key_onboarding_bancomer_points_title: String {
            return NSLocalizedString("KEY_ONBOARDING_BANCOMER_POINTS_TITLE", comment: "")
        }
        static var key_onboarding_bancomer_points_description: String {
            return NSLocalizedString("KEY_ONBOARDING_BANCOMER_POINTS_DESCRIPTION", comment: "")
        }
        static var key_permissions_request_header_title: String {
            return NSLocalizedString("KEY_PERMISSIONS_REQUEST_HEADER_TITLE", comment: "")
        }
        static var key_permissions_request_title_info: String {
            return NSLocalizedString("KEY_PERMISSIONS_REQUEST_TITLE_INFO", comment: "")
        }
        static var key_permissions_request_title_ko: String {
            return NSLocalizedString("KEY_PERMISSIONS_REQUEST_TITLE_KO", comment: "")
        }
        static var key_permissions_request_description_info: String {
            return NSLocalizedString("KEY_PERMISSIONS_REQUEST_DESCRIPTION_INFO", comment: "")
        }
        static var key_permissions_request_description_ko: String {
            return NSLocalizedString("KEY_PERMISSIONS_REQUEST_DESCRIPTION_KO", comment: "")
        }
        static var key_permissions_request_more_info_button: String {
            return NSLocalizedString("KEY_PERMISSIONS_REQUEST_MORE_INFO_BUTTON", comment: "")
        }
        static var key_permissions_request_button_info: String {
            return NSLocalizedString("KEY_PERMISSIONS_REQUEST_BUTTON_INFO", comment: "")
        }
        static var key_permissions_request_button_ko: String {
            return NSLocalizedString("KEY_PERMISSIONS_REQUEST_BUTTON_KO", comment: "")
        }
        static var key_permissions_request_close_button: String {
            return NSLocalizedString("KEY_PERMISSIONS_REQUEST_CLOSE_BUTTON", comment: "")
        }
        static var key_start_button: String {
            return NSLocalizedString("KEY_START_BUTTON", comment: "")
        }
    }
    struct help {
    }
    struct others {
        static var key_rate_this_app_description_text: String {
            return NSLocalizedString("KEY_RATE_THIS_APP_DESCRIPTION_TEXT", comment: "")
        }
        static var key_rate_this_app_description_ios_text: String {
            return NSLocalizedString("KEY_RATE_THIS_APP_DESCRIPTION_IOS_TEXT", comment: "")
        }
        static var key_rate_this_app_remind_me_text: String {
            return NSLocalizedString("KEY_RATE_THIS_APP_REMIND_ME_TEXT", comment: "")
        }
        static var key_rate_this_app_rate_text: String {
            return NSLocalizedString("KEY_RATE_THIS_APP_RATE_TEXT", comment: "")
        }
        static var key_rate_this_app_dismiss_text: String {
            return NSLocalizedString("KEY_RATE_THIS_APP_DISMISS_TEXT", comment: "")
        }
        static var key_rate_this_app_header_text: String {
            return NSLocalizedString("KEY_RATE_THIS_APP_HEADER_TEXT", comment: "")
        }
        static var key_mandatory_update_header_text: String {
            return NSLocalizedString("KEY_MANDATORY_UPDATE_HEADER_TEXT", comment: "")
        }
        static var key_update_title_text: String {
            return NSLocalizedString("KEY_UPDATE_TITLE_TEXT", comment: "")
        }
        static var key_update_button_text: String {
            return NSLocalizedString("KEY_UPDATE_BUTTON_TEXT", comment: "")
        }
        static var key_recommended_update_header_text: String {
            return NSLocalizedString("KEY_RECOMMENDED_UPDATE_HEADER_TEXT", comment: "")
        }
        static var key_recommended_update_later_text: String {
            return NSLocalizedString("KEY_RECOMMENDED_UPDATE_LATER_TEXT", comment: "")
        }
        static var key_maintenance_header_text: String {
            return NSLocalizedString("KEY_MAINTENANCE_HEADER_TEXT", comment: "")
        }
        static var key_maintenance_period_text: String {
            return NSLocalizedString("KEY_MAINTENANCE_PERIOD_TEXT", comment: "")
        }
        static var key_maintenance_phone_text: String {
            return NSLocalizedString("KEY_MAINTENANCE_PHONE_TEXT", comment: "")
        }
        static var key_maintenance_button_text: String {
            return NSLocalizedString("KEY_MAINTENANCE_BUTTON_TEXT", comment: "")
        }
        static var key_root_or_jailbreak_mode_text: String {
            return NSLocalizedString("KEY_ROOT_OR_JAILBREAK_MODE_TEXT", comment: "")
        }
        static var key_root_mode_with_mobile_payment_text: String {
            return NSLocalizedString("KEY_ROOT_MODE_WITH_MOBILE_PAYMENT_TEXT", comment: "")
        }
        static var key_security_header_text: String {
            return NSLocalizedString("KEY_SECURITY_HEADER_TEXT", comment: "")
        }
        static var key_share_file_name_text: String {
            return NSLocalizedString("KEY_SHARE_FILE_NAME_TEXT", comment: "")
        }
    }
    struct glomo_connect {
        static var key_digital_activation_undefined_error_title: String {
            return NSLocalizedString("DIGITAL_ACTIVATION_UNDEFINED_ERROR_TITLE", comment: "")
        }
        static var key_digital_activation_undefined_error_info: String {
            return NSLocalizedString("DIGITAL_ACTIVATION_UNDEFINED_ERROR_INFO", comment: "")
        }
        static var key_card_activation_failed: String {
            return NSLocalizedString("CARD_ACTIVATION_FAILED", comment: "")
        }
        static var key_card_activation_failed_description: String {
            return NSLocalizedString("CARD_ACTIVATION_FAILED_DESCRIPTION", comment: "")
        }
        static var key_device_activation_error: String {
            return NSLocalizedString("DEVICE_ACTIVATION_ERROR", comment: "")
        }
        static var key_bank_phone_number: String {
            return NSLocalizedString("BANK_PHONE_NUMBER", comment: "")
        }
    }
    struct configuration {
        static var key_configuration: String {
            return NSLocalizedString("KEY_CONFIGURATION", comment: "")
        }
        static var key_configuration_permission_enabled_text: String {
            return NSLocalizedString("KEY_CONFIGURATION_PERMISSION_ENABLED_TEXT", comment: "")
        }
    }
}
