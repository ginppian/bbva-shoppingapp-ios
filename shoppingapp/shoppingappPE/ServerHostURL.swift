//
//  ServerHostURL.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 4/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class ServerHostURL: ServerHostURLProtocol {

    private enum Environment: String {
        case artichoke = "mock"
    }

    private static let selectedEnvironment: Environment = Environment(rawValue: Configuration.selectedEnvironment()) ?? Environment.artichoke
    private static let serverHost = SessionDataManager.sessionDataInstance().serverHost!

    private static let v0Version = "v0"
    private static let V02Version = "V02"

    private static func defaultVersion(forEnvironment environment: Environment) -> String {

        switch environment {
        case .artichoke:
            return v0Version
        }
    }
    
    static func defaultDirectory() -> String {
        return ""
    }
    
    static func defaultDirectoryFinancial() -> String {
        return ""
    }

    static func getGrantingTicketURL() -> String {

        let path = "TechArchitecture/\(SessionDataManager.sessionDataInstance().serverCode!)/grantingTicket/"
        let version = ServerHostURL.V02Version

        switch selectedEnvironment {
        case .artichoke:
            break
        }

        return serverHost + path + version
    }

    static func getCustomerURL() -> String {

        return getCommonURL(path: "customers/")
    }
    
    static func getAccountsURL() -> String {
        
        return getCommonURL(path: "accounts/")
    }

    static func getCardsURL() -> String {

        return getCommonURL(path: "cards/")
        }

    static func getPromotionsURL() -> String {

        return getCommonURL(path: "promotions/")
    }

    static func getContractsURL() -> String {

        return getCommonURL(path: "contracts/")
        }

    static func getLoyaltyURL() -> String {

        return getCommonURL(path: "loyalty/")
    }
    
    static func getDevicesURL() -> String {
        return getCommonURL(path: "devices/")
    }

    // MARK: - AppSettingsURL

    static func getAppSettingsURL() -> String {
        
        return "https://bbva-files.s3.amazonaws.com/wallet/shopping_app/config/MX/1.0/publicConfiguration.json"
    }

    private static func getCommonURL(path: String, version: String = ServerHostURL.defaultVersion(forEnvironment: selectedEnvironment)) -> String {

        switch selectedEnvironment {
        case .artichoke:
            break
        }

        return serverHost + path + version
    }
    
    static func getKeyTagForEncryption() -> String {
        
        switch selectedEnvironment {
        case .artichoke:
            return "01"
        }
    }
}
