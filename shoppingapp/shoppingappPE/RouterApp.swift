//
//  Router.swift
//  shoppingappMX
//
//  Created by daniel.petrovic on 25/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class RouterApp: RouterGlobal {

    override func controllersForTabBar() -> [UIViewController] {

        let navigationFirstTab: UINavigationController = UINavigationController(rootViewController: (BusManager.sessionDataInstance.routerFactory?.getViewController(route: HomeViewController.ID))!)

        let cardsViewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: CardsViewController.ID)! as! CardsViewController
        let navigationSecondTab: UINavigationController = UINavigationController(rootViewController: cardsViewController)

        let navigationThirdTab: UINavigationController = UINavigationController(rootViewController: (BusManager.sessionDataInstance.routerFactory?.getViewController(route: ShoppingViewController.ID))!)

        let customerRewardsViewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: CustomerRewardsViewController.ID)! as! CustomerRewardsViewController
        let navigationFourthTab: UINavigationController = UINavigationController(rootViewController: customerRewardsViewController)

        let navigationFifthTab: UINavigationController = UINavigationController(rootViewController: (BusManager.sessionDataInstance.routerFactory?.getViewController(route: NotificationsViewController.ID))!)

        return [navigationFirstTab, navigationSecondTab, navigationThirdTab, navigationFourthTab, navigationFifthTab]
    }

    override func navigateErrorFromLoginLoader(withModel model: Model) {

        let viewController = BusManager.sessionDataInstance.getViewControllerFromNavigation(route: LoginViewControllerA.self) as! LoginViewControllerA
        viewController.initModel(withModel: model)
        BusManager.sessionDataInstance.navigationController?.popToViewController(viewController, animated: true)
    }

    override func viewControllerClassForPoints() -> AnyClass {

        return CustomerRewardsViewController.self
    }
}
