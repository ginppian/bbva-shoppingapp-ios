//
//  KeyChainManager.swift
//  shoppingappMX
//
//  Created by Javier Dominguez on 06/03/2019.
//  Copyright © 2019 BBVA. All rights reserved.
//

import Foundation

class KeychainManager: KeychainManagerGlobal {
    
    static var internalInstance: KeychainManager?
    
    static var shared: KeychainManager {
        if let instance = internalInstance {
            return instance
        } else {
            internalInstance = KeychainManager()
            return internalInstance!
        }
    }
    
}
