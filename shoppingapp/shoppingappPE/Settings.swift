//
//  Settings.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 22/3/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

class Settings: SettingsGlobal {

    class Global: GlobalSettings {
        override class var default_bbva_phone_number: String {
            return "015950000"
        }

        override class var default_location: GeoLocationBO {
            return GeoLocationBO(withLatitude: -12.0431800, withLongitude: -77.0282400)
        }
    }
    
    class Security: SecurityGlobal {
    }

    class Login: LoginGlobal {
        override class var user_maximum_length: Int? {
            return 15
        }
        override class var password_maximum_length: Int? {
            return 9
        }
    }

    class CardsLanding: CardsLandingGlobal {
    }

    class PromotionsMap: PromotionsMapGlobal {
    }

    class Promotions: PromotionsGlobal {
    }

    class TrendingPromotionComponent: TrendingPromotionComponentGlobal {
    }

    class CardCvv: CardCvvGlobal { }

    class PromotionsFilter: PromotionsFilterGlobal {
    }

    class TransactionsFilter: TransactionsFilterGlobal {
    }

    class Timeout: TimeoutGlobal {
    }

    class DidacticArea: DidacticAreaGlobal {
    }

    class Home: HomeGlobal {

        override class var viewControllerForCustomerPoints: UIViewController? {

            let pointsWidget = PointsWidget(nibName: "PointsWidget", bundle: nil)
            pointsWidget.view.frame = CGRect(origin: CGPoint(x: 0, y: 0), size: CGSize(width: 300, height: 200))

            return pointsWidget
        }
    }

    class Limits: LimitsGlobal {
    }

    class CardHelpOnOff: CardHelpOnOffGlobal {
    }

    class FavoriteCategory: FavoriteCategoryGlobal {
    }
    
    class SSLPinning: SSLPinningGlobal {
    }
    
    class Migration: MigrationGlobal {
    }
}
