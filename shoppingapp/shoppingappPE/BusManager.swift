//
//  BusManager.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 7/9/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation
import CellsNativeComponents
import CellsNativeCore

class BusManager: BusManagerGlobal {

    override func loadRouter() {
        routerFactory = RouterInitFactory()
    }

    override func loadPageReactions() {
        pageReactionManager = PageReactionApp()
    }

    func showPicker(options: ModelDisplay, delegate: PickerPresenterDelegate) {

        let view = PickerViewController(nibName: "PickerViewController", bundle: nil)
        let presenter = PickerPresenter<PickerViewController>()

        view.presenter = presenter

        presenter.view = view
        presenter.delegate = delegate
        presenter.options = options as? PickerOptionsDisplay

        view.modalPresentationStyle = .overCurrentContext

        BusManager.sessionDataInstance.present(viewController: view, animated: true, completion: nil)

    }

}
