//
//  PageReaction.swift
//  shoppingappMX
//
//  Created by daniel.petrovic on 25/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import CellsNativeCore

class PageReactionApp: PageReactionGlobal {

    override init() {
        super.init()
        addExtraPages()
    }

    func addExtraPages() {
        add(id: CustomerRewardsViewController.ID) { () -> PageReaction in
            CustomerRewardsPageReaction()
        }
    }

    override func initPageLogin() {
        add(id: LoginViewController.ID) { () -> PageReaction in
            LoginPageReactionA()
        }
    }

    override func initPageHome() {
        add(id: HomeViewController.ID) { () -> PageReaction in
            HomePageReactionA()
        }
    }

}
