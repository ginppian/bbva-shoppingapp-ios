//
//  RouterInitFactory.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 7/9/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit
import CellsNativeCore

class RouterInitFactory: RouterInitFactoryGlobal {

    override init() {
        super.init()

        addCustomerRewardsRoute()
    }

    override func addLoginRoute() {
        addRoute(route: LoginViewController.ID, routeObject: Route(viewController: { () -> UIViewController in

            let viewLogin = LoginViewControllerA(nibName: "LoginViewControllerA", bundle: nil)
            let presenterLogin = LoginPresenterA<LoginViewControllerA>()
            viewLogin.presenter = presenterLogin
            presenterLogin.view = viewLogin
            presenterLogin.interactor = LoginInteractorA()
            return viewLogin

        }, animationToNext: { _, destinationViewController  in

            if let destinationViewController = destinationViewController {
                self.animation(viewController: destinationViewController)
            }

        }, animationToPrevious: { _, destinationViewController in

            if let destinationViewController = destinationViewController {
                self.animation(viewController: destinationViewController)
            }

        }))
    }

    func addCustomerRewardsRoute() {

        self.addRoute(route: CustomerRewardsViewController.ID, routeObject: Route(viewController: { () -> UIViewController in

            let storyboardInitialViewController = UIStoryboard(name: "CustomerRewardsViewController", bundle: nil).instantiateInitialViewController()
            guard let viewCustomerRewards = storyboardInitialViewController as? CustomerRewardsViewController else {
                return UIViewController()
            }

            let presenterCustomerRewards = CustomerRewardsPresenter<CustomerRewardsViewController>()
            viewCustomerRewards.presenter = presenterCustomerRewards
            presenterCustomerRewards.view = viewCustomerRewards

            viewCustomerRewards.tabBarItem = UITabBarItem(title: Localizables.common.key_tabbar_points_button, image: ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Promotions.menu_promotion_icon, color: .GRAYTABBAR), width: 18, height: 18), tag: 4)
            viewCustomerRewards.tabBarItem.setTitleTextAttributes([NSAttributedStringKey.font: Tools.setFontMedium(size: 10)], for: .normal)
            viewCustomerRewards.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -3)
            return viewCustomerRewards

        }, animationToNext: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }, animationToPrevious: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }))
    }
}
