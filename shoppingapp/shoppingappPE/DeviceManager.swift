//
//  DeviceManager.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 23/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit

class DeviceManager: DeviceManagerGlobal {
    
    var deviceUID: String? {
        // TODO: this must be changed for the proper uid when library usage is clear
        return  UIDevice.current.identifierForVendor?.uuidString
    }
    
    // MARK: Singleton
    
    static var instance = DeviceManager()
    
    class func sharedInstance () -> DeviceManager {
        return instance
    }
    
}
