//
//  BVAAppDelegate.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 6/3/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import CellsNativeComponents
import CellsNativeCore
import BBVA_Network
import Fabric
import Crashlytics
import AdobeMobileSDK
import RxSwift

class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var appLifeCycle: AppLifecycle?

    let disposeBag = DisposeBag()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]? = nil) -> Bool {
        
        ADBMobile.overrideConfigPath(Bundle(for: type(of: self)).path(forResource: Settings.Global.omnitureConfigPath, ofType: "json"))

        if isOnTesting() {
            
            window = UIWindow(frame: UIScreen.main.bounds)
            window?.backgroundColor = .NAVY
            window?.rootViewController = UIViewController()
            window?.makeKeyAndVisible()
            return true
        }

        window = UIWindow(frame: UIScreen.main.bounds)
        
        appLifeCycle = BusManager.sessionDataInstance.loadInitFactory()

        let mainViewController = MainViewController.sharedInstance
        mainViewController.setup(type: 0)

        BusManager.sessionDataInstance.loadSplash()
        ComponentManager.add(id: RouterApp.ID, component: RouterApp())
        ComponentManager.add(id: CommonCellsGlobal.id, component: CommonCellsGlobal())
        window?.backgroundColor = .white
        window?.makeKeyAndVisible()

        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.keyboardDistanceFromTextField = 30
        IQKeyboardManager.shared.disabledToolbarClasses = [SecurityViewController.self]

        TrackerHelper.sharedInstance().configure()

        NotificationCenter.default.addObserver(self, selector: #selector(self.handlePasteboardNotification), name: .UIPasteboardChanged, object: nil)

        if PreferencesManager.sharedInstance().isFirstRun() {
            KeychainManager.shared.resetKeyChainItems()
            PublicConfigurationManager.sharedInstance().saveDefaultPublicConfiguration()
        }

        ConfigPublicManager.sharedInstance().getConfigFile()
        ConfigPublicManager.sharedInstance().getDidacticArea()

        Fabric.with([Crashlytics.self])
        
        SSLPinningManager.configureSSLPinning()
        
        VersionControlManager.sharedInstance().checkAppUpgrade()
        
        FileManager.default.clearTmpDirectory()

        UIApplication.shared.registerForRemoteNotifications()
        
        if let remoteNotification = launchOptions?[.remoteNotification] as? [AnyHashable: Any] {

            BusManager.sessionDataInstance.publishData(tag: ShoppingCrossReaction.TAG, PageReactionConstants.TICKET_RECEIVED_IN_BACKGROUND, TicketModel(withPayload: remoteNotification))
        }

        return true
    }
    
    func isOnTesting() -> Bool {
        return NSClassFromString("XCTest") != nil
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        let shoppingApp = ShoppingApplication.shared as! ShoppingApplication
        shoppingApp.resetTimer()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {

        ConfigPublicManager.sharedInstance().getConfigFile()
        ConfigPublicManager.sharedInstance().getDidacticArea()
        
        if PublicConfigurationManager.sharedInstance().isNecessaryToUpdate {
            
            let publicConfigurationObservable = PublicConfigurationManager.sharedInstance().getPublicConfiguration()
            
            publicConfigurationObservable
                .subscribeOn(SerialDispatchQueueScheduler(qos: .default)) // Just execution in background
                .observeOn(MainScheduler.instance) // Events come in main thread
                .take(1) // Just the first event
                .subscribe( onNext: { publicConfigurationDTO in
                    
                    VersionControlManager.sharedInstance().checkVersion(withPublicConfigurationDTO: publicConfigurationDTO, shouldShowNoMandatoryUpdate: false)
                    
                }).addDisposableTo(disposeBag)
        }
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.

        if PublicConfigurationManager.sharedInstance().isNecessaryToUpdate {

            let publicConfigurationObserver = PublicConfigurationManager.sharedInstance().getPublicConfiguration()

            publicConfigurationObserver
                .observeOn(SerialDispatchQueueScheduler(qos: .default)) // Que se ejecute en background
                .subscribeOn(MainScheduler.instance) //Nos avise en Main Thread
                .subscribe( onNext: { _ in
                    // Comprobar si se debe avisar al usario por actualización o cualquier otra acción
                }).addDisposableTo(disposeBag)
        }
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }
    @objc func handlePasteboardNotification(_ notification: Notification) {
        // Comprobar si el portapapeles esta vacio
        let pasteboard = notification.object as? UIPasteboard
        if (pasteboard?.string?.count ?? 0) != 0 {
            // Guardar la version actual del portapapeles
            let pasteboardVersion: Int? = pasteboard?.changeCount
            // Obtener el tiempo de expiracion
            var clearClipboardTimeout = TimeInterval()

            if let time = PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kClearCopyPan) as? Int {
                clearClipboardTimeout = TimeInterval(time)
            }
            // Crear la tarea temporizada en segundo plano
            let application = UIApplication.shared
            var bgTask: UIBackgroundTaskIdentifier = UIBackgroundTaskIdentifier()

            bgTask = application.beginBackgroundTask(expirationHandler: {
                application.endBackgroundTask(bgTask)

            })

            // Lanzar temporizador

            DispatchQueue.global(qos: .default).async(execute: {() -> Void in
                Thread.sleep(forTimeInterval: clearClipboardTimeout)
                // Borrar el portapapeles si no ha cambiado
                if pasteboardVersion == pasteboard?.changeCount {
                    pasteboard?.string = ""
                }
                // Finalizar la tarea
                application.endBackgroundTask(bgTask)
            })

        }

    }
    // MARK: Remote Notifications

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {

        let deviceTokenString = deviceToken.map { String(format: "%02hhx", $0) }.joined()
        
        DLog(message: "☁️📲📮 DEVICE TOKEN: \(deviceTokenString) ☁️📲📮")

        //Check token
        let lastApnsToken = PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kApnsToken) as? String

        if lastApnsToken != deviceTokenString {

            //save new token
            PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: deviceTokenString as AnyObject, withKey: PreferencesManagerKeys.kApnsToken)

            BusManager.sessionDataInstance.publishData(tag: ShoppingCrossReaction.TAG, PageReactionConstants.TOKEN_CHANGED, deviceTokenString)
        }
    }

    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        DLog(message: "---Z> didFailToRegisterForRemoteNotificationsWithError")

        BusManager.sessionDataInstance.publishData(tag: ShoppingCrossReaction.TAG, PageReactionConstants.TOKEN_FAILED, error.localizedDescription)
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        DLog(message: "didReceiveRemoteNotification: \(userInfo)")

        BusManager.sessionDataInstance.showTicketDetail(withModel: TicketModel(withPayload: userInfo))
    }
}
