//
//  Settings.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 22/3/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import BGMConnectSDK

class Settings: SettingsGlobal {

    class Global: GlobalSettings {
        override class var default_bbva_phone_number: String {
            return "+525552262663"
        }

        override class var default_location: GeoLocationBO {
            return GeoLocationBO(withLatitude: 19.4326018, withLongitude: -99.1353936)
        }
        
        override class var default_web_bbva: String {
            return "https://www.bancomer.com/"
        }
        
        override class var omnitureConfigPath: String {
            
            #if DEBUG
            return "ADBMobileConfig-Dev"
            #else
            
            let selectedEnvironment = Configuration.selectedEnvironment()
            
            if selectedEnvironment == ServerHostURL.Environment.production.rawValue {
                return "ADBMobileConfig-Pro"
            } else {
                return "ADBMobileConfig-Dev"
            }
            
            #endif
        }
    }
    
    class Security: SecurityGlobal {
        
        override class var softToken: String? {
            
            let selectedEnvironment = Configuration.selectedEnvironment()
            
            if selectedEnvironment == ServerHostURL.Environment.artichoke.rawValue {
                return "123456"
            } else {
                return GCSController.getCore().getOTP()
            }
        }
        
        override class var securityInterceptorEnabled: Bool {
            return false
        }
    }

    class Login: LoginGlobal {
        override class var showRememberPassword: Bool {
            return false
        }

        override class var user_maximum_length: Int? {
            return 10
        }
        override class var password_maximum_length: Int? {
            return 10
        }

        override class var user_valid_character_set: CharacterSet {
            return .decimalDigits
        }

        override class var user_keyboard_type: UIKeyboardType {
            return .numberPad
        }
        
        override class var password_keyboard_type: UIKeyboardType {
            return .namePhonePad
        }
        
        class var userMock: String {
            return "NTUyMjMzNDQ1NVFSM1hxR1Qzcmc="
        }
    }

    class CardsLanding: CardsLandingGlobal {
        override class var show_direct_access_to_transactions: Bool {
            return true
        }

        override class var show_credit_card_limit_info: Bool {
            return false
        }
    }

    class PromotionsMap: PromotionsMapGlobal {
    }

    class Promotions: PromotionsGlobal {

        override class func effectiveDateText(forPromotionBO promotionBO: PromotionBO, compactSize: Bool) -> DisplayItemPromotion.EffectiveDateTextInfo? {
            let today = Calendar.current.startOfDay(for: Date())
            let startDate = Calendar.current.startOfDay(for: promotionBO.startDate)

            guard let startDiffDays = Calendar.current.dateComponents([.day], from: today, to: startDate).day  else {
                return nil
            }

            var effectiveDateText: String
            let isAInterval = false

            if startDiffDays <= 0 {

                let formattedDate = promotionBO.endDate.string(format: Date.DATE_FORMAT_DAY_MONTH_SHORT)

                effectiveDateText = Localizables.promotions.key_promotions_finish_date + " " + formattedDate.capitalized

            } else {

                let formattedDate = promotionBO.startDate.string(format: Date.DATE_FORMAT_DAY_MONTH_SHORT)

                effectiveDateText = Localizables.promotions.key_promotions_init_date + " " + formattedDate.capitalized

            }

            return (effectiveDateText, isAInterval)
        }

        override class func dateDiffPlusDistance(distance: String, effectiveDateText: String, isIntervalDate: Bool) -> String {
            return distance + " - " + effectiveDateText
        }

        override class var should_show_configure_favorite_categories: Bool {
            return true
        }
        
        class var creditCardRequestWeb: String {
            return "https://www.bancomer.com/personas/productos/tarjetas-de-credito/tdc-online.html"
        }
    }

    class TrendingPromotionComponent: TrendingPromotionComponentGlobal {
        override class var should_filter_by_card_title_id: Bool {
            return true
        }
    }

    class PromotionsFilter: PromotionsFilterGlobal {
        override class var promotion_types: [PromotionType] {
            return [.discount, .point, .toMonths]
        }
    }

    class TransactionsFilter: TransactionsFilterGlobal {
        override class var should_show_concept_filter: Bool {
            return false
        }
    }

    class Timeout: TimeoutGlobal {
        override class var timeout_for_expired_session: TimeInterval {
            return 360.0
        }
    }

    class CardCvv: CardCvvGlobal {
        override class var time_duration_for_show_cvv: TimeInterval {
            return 300.0
        }

        override class var time_alert_for_show_cvv: TimeInterval {
            return 30.0
        }
        override class var cvvId: String {
            return "CVV2"
        }
        
        override class func hexadecimalEncodingForCVV(cvv: String) -> String {
            
            let hexCVVString = String(format: "%@0000000000000", cvv)
            
            return hexCVVString
        }
    }

    class DidacticArea: DidacticAreaGlobal {
    }

    class Home: HomeGlobal {

        override class var cardTypesToShowInHome: [PhysicalSupportType] {
            return [.normalPlastic, .virtual, .sticker, .nfc]
        }

        override class var showPointsInHome: Bool {
            return true
        }
    }

    class Limits: LimitsGlobal {

        override class var multiples_limit_amount: Double {
            return 50
        }

    }

    class CardHelpOnOff: CardHelpOnOffGlobal {
        
        override class func localizationKeyForHelpOnOffAndCard(card: CardBO? = nil) -> String {
            if let card = card {
                return card.isCardOn() ? "KEY_CARD_HELP_OFF_" : "KEY_CARD_HELP_ON_"
            }
            return "KEY_CARD_HELP_ON_OFF_"
        }
    }

    class FavoriteCategory: FavoriteCategoryGlobal {
    }

    class SSLPinning: SSLPinningGlobal {
        
        override class var certificates: [String] {
            
            return ["*.s3.amazonaws.com.cer",
                    "openapi.bbva.com.cer",
                    "*.prev.grupobbva.com.cer",
                    "artichoke.platform.bbva.com.cer",
                    "DigiCert_Global_Root_CA.cer",
                    "DigiCert_SHA2_Secure_Server_CA.cer",
                    "www.bancomermovil.net.cer",
                    "glomo.bancomermovil.com.cer"]
        }
    }

    class Migration: MigrationGlobal {
        override class var versionKey: String {
            return "UserDefaults.appVersionNumber"
        }
    }
}
