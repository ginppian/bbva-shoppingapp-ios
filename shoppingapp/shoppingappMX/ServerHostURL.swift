//
//  ServerHostURL.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 4/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class ServerHostURL: ServerHostURLProtocol {

    enum Environment: String {
        case artichoke = "mock"
        case development = "development"
        case test = "test"
        case dev = "dev"
        case hiddenLeague = "hiddenleague"
        case production = "production"
    }

    static var selectedEnvironment: Environment = Environment(rawValue: Configuration.selectedEnvironment()) ?? Environment.artichoke
    static var serverHost = SessionDataManager.sessionDataInstance().serverHost!

    // Directories
    private static let directoryTest = "SRVS_A02"
    private static let directoryDevelopment = "bmovmehxsa_mx_web"
    private static let directoryHiddenLeagueGrantingTicket = "bmovmehxlogt_mx_web"
    private static let directoryHiddenLeague = "bmovmehxlo_mx_web"

    private static let directoryProductionGrantingTicket = "bmovmehxgt_mx_web"
    private static let directoryProductionFinancial = "bmovmehx_mx_web"
    private static let directoryProduction = "bmovmehxex_mx_web"

    // Versions
    private static let v0Version = "v0"
    private static let v1Version = "v1"
    private static let V02Version = "V02"
    
    static func getSelectedEnvironment() -> Environment {
        return selectedEnvironment
    }
    
    static func setEnvironmentAndHostForUserMock(activate: Bool) {
        
        if activate {
            
            selectedEnvironment = .artichoke
            SessionDataManager.sessionDataInstance().getHostBySelectedEnvironment(environment: selectedEnvironment.rawValue)
            
        } else {
            
            selectedEnvironment = Environment(rawValue: Configuration.selectedEnvironment()) ?? Environment.artichoke
            SessionDataManager.sessionDataInstance().getHost()
        }
        
        serverHost = SessionDataManager.sessionDataInstance().serverHost!
    }
    
    static func defaultDirectory() -> String {
        
        switch selectedEnvironment {
        case .artichoke:
            return ""
        case .development:
            return directoryDevelopment
        case .test, .dev:
            return directoryTest
        case .hiddenLeague:
            return directoryHiddenLeague
        case .production:
            return directoryProduction
        }
    }
    
    static func defaultDirectoryFinancial() -> String {
        
        switch selectedEnvironment {
        case .production:
            return directoryProductionFinancial
        default:
            return ServerHostURL.defaultDirectory()
        }
    }
    
    // MARK: - GrantingTicket

    static func getGrantingTicketURL() -> String {

        var path = "TechArchitecture/\(SessionDataManager.sessionDataInstance().serverCode!)/grantingTicket/"
        let version = ServerHostURL.V02Version

        switch selectedEnvironment {
        case .artichoke:
            break
        case .development:
            path = directoryDevelopment + "/" + path
        case .test, .dev:
            path = directoryTest + "/" + path
        case .hiddenLeague:
            path = directoryHiddenLeagueGrantingTicket + "/" + path
        case .production:
            path = directoryProductionGrantingTicket + "/" + path
        }

        return serverHost + path + version
    }

    // MARK: - FinancialURLS
    
    static func getCustomerURL() -> String {

        let serviceVersion = (selectedEnvironment == .artichoke) ? v0Version : v1Version
        
        return getFinancialURL(path: "customers/", version: serviceVersion)
    }

    static func getCardsURL() -> String {

        return getFinancialURL(path: "cards/")
    }
    
    static func getAccountsURL() -> String {
        
        let serviceVersion = (selectedEnvironment == .artichoke) ? v0Version : v1Version
        
        return getFinancialURL(path: "accounts/", version: serviceVersion)
    }

    static func getContractsURL() -> String {

        return getFinancialURL(path: "contracts/")
    }

    static func getDevicesURL() -> String {
        
        return getFinancialURL(path: "devices/")
    }

    // MARK: - Loyalty URLS

    static func getLoyaltyURL() -> String {

        return getCommonURL(path: "loyalty/")
    }

    static func getPromotionsURL() -> String {

        return getCommonURL(path: "promotions/")
    }

    // MARK: - AppSettingsURL

    static func getAppSettingsURL() -> String {
        
        var url: String
        var publicSettingsId: String
        let version = "1.0"
        
        switch selectedEnvironment {
        case .artichoke:
            
            return "https://bbva-files.s3.amazonaws.com/wallet/shopping_app/config/MX/1.0/publicConfiguration.json"
        case .development:
            
            publicSettingsId = "SHOPPINGAPP-MX-\(version)-DES"
        case .test, .dev:
            
            publicSettingsId = "SHOPPINGAPP-MX-\(version)-TEST"
        case .hiddenLeague:
            
            publicSettingsId = "SHOPPINGAPP-MX-\(version)-LO"
        case .production:
            
            publicSettingsId = "SHOPPINGAPP-MX-\(version)-PRO"
        }
        
        url = getCommonURL(path: "app-settings/") + "/public-settings/\(publicSettingsId)?key=app_settings"
        
        return url
    }
    
    // MARK: - Security
    
    static func getDowngradeURL() -> String {
        
        return getFinancialURL(path: "security/", version: v1Version)
    }
    
    static func getKeyTagForEncryption() -> String {
        
        switch selectedEnvironment {
        case .artichoke, .development, .test, .dev:
            return "01"
        case .hiddenLeague:
            return "hiddenLeague"
        case .production:
            return "production"
        }
    }
    
    // MARK: - Extra Security
    
    static func environmentNeedsExtraSecurity() -> Bool {
        
        switch selectedEnvironment {
        case .hiddenLeague, .production, .test:
            return true
        default:
            return false
        }
    }
    
    // MARK: - Glomo Connect
    
    static func environmentGlomoConnect() -> String {
        
        switch selectedEnvironment {
            
        case .test:
            return "TEST"
        case .artichoke:
            return "ARTICHOKE_PROD"
        case .production:
            return "PROD"
        case .hiddenLeague:
            return "PRE_PROD"
        case .dev:
            return "DEV"
        default:
            return "PROD"
        }
    }
}

// MARK: - Private Methods
private extension ServerHostURL {
    
    private static func defaultVersion(forEnvironment environment: Environment) -> String {
        
        switch environment {
        case .artichoke, .development, .test, .dev, .hiddenLeague, .production:
            return v0Version
        }
    }
    
    private static func getCommonURL(path: String, version: String = ServerHostURL.defaultVersion(forEnvironment: selectedEnvironment)) -> String {
        
        var path = path
        switch selectedEnvironment {
        case .artichoke:
            break
        case .development:
            path = directoryDevelopment + "/" + path
        case .test, .dev:
            path = directoryTest + "/" + path
        case .hiddenLeague:
            path = directoryHiddenLeague + "/" + path
        case .production:
            path = directoryProduction + "/" + path
        }
        
        return serverHost + path + version
    }
    
    private static func getFinancialURL(path: String, version: String = ServerHostURL.defaultVersion(forEnvironment: selectedEnvironment)) -> String {
        
        var path = path
        switch selectedEnvironment {
        case .production:
            path = directoryProductionFinancial + "/" + path
        default:
            return getCommonURL(path: path, version: version)
        }
        
        return serverHost + path + version
    }
}
