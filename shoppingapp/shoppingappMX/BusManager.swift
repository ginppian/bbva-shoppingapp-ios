//
//  BusManager.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 8/9/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import Foundation
import CellsNativeComponents
import CellsNativeCore
import Component_DM_Header_Interceptor
import BBVA_Network
import Component_IC_Logger
import os

class BusManager: BusManagerGlobal {

    override func loadRouter() {
        routerFactory = RouterInitFactory()
    }

    override func loadPageReactions() {
        pageReactionManager = PageReactionApp()
    }
    
    override func tabBarController(_ tabBarController: UITabBarController,
                                   animationControllerForTransitionFrom fromVC: UIViewController,
                                   to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        if (toVC as? UINavigationController)?.viewControllers.first as? ShoppingViewControllerB != nil {
            let toViewController = (toVC as? UINavigationController)?.viewControllers.first as? ShoppingViewControllerB
            (toViewController?.presenter as? ShoppingPresenterB<ShoppingViewControllerB>)?.tabPressed()
        }
        
        return super.tabBarController(tabBarController, animationControllerForTransitionFrom: fromVC, to: toVC)
    }
    
    override func createNetworkWorker() {
        networkWorker = NetworkWorker()
        
        if let configuration = sessionConfiguration {
            networkWorker?.setCurrentEndPoint(endPoint: BBVAEndPointDefault(configuration: configuration))
        }
        
        let headersInterceptor = AppendHeadersRequestInterceptor()
        networkWorker?.addInterceptor(interceptor: headersInterceptor)
        
        configureLoggerInterceptor()
        
        if let networkWorker = networkWorker {
            headerInterceptorStore = HeaderInterceptorStore(worker: networkWorker)
            
            if Settings.Security.securityInterceptorEnabled {
                securityInterceptorStore = SecurityInterceptorStore(worker: networkWorker)
            }
        }
    }
}
