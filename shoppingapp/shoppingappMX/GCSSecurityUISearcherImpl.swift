//
//  GCSSecurityUISearcherImpl.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 31/7/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import BGMConnectManager
import BGMCoreUI
import BGMUICatalog

class GCSSecurityUISearcherImpl: GCSSecurityUISearcher {
    
    private func getSecurityDelegate() -> GCSSecurityUIDelegate? {
        
        let navigation = MainViewController.sharedInstance.rootViewController as? UINavigationController
        return navigation?.viewControllers.first as? LoginViewControllerBGM
    }
    
    override func getFirstSecurityUIDelegate() -> GCSSecurityUIDelegate? {
        
        return getSecurityDelegate()
    }
    
    override func getSecurityUIDelegate() -> GCSSecurityUIDelegate? {
        
        return getSecurityDelegate()
    }
}
