//
//  KeyChainManager.swift
//  shoppingappMX
//
//  Created by Javier Dominguez on 06/03/2019.
//  Copyright © 2019 BBVA. All rights reserved.
//

import Foundation
import BGMConnectSDK
//import BGMCoreUI

class KeychainManager: KeychainManagerGlobal {
    
//    public static let usernameKey : String = "USERNAME"
//    public static let userfullnameKey : String = "USERFULLNAME"
    private static let lastLoggedUserIdKey = "lastLoggedUserId"
    
    static var internalInstance: KeychainManager?
    
    static var shared: KeychainManager {
        if let instance = internalInstance {
            return instance
        } else {
            internalInstance = KeychainManager()
            return internalInstance!
        }
    }
    
    func isUserIdEqualToLastLoggedUserId() -> Bool {
        
        if getUserId() == getLastLoggedUserId() {
            return true
        }
        return false
    }
    
    func getLastLoggedUserId() -> String {
        return keyChainWrapper.string(forKey: KeychainManager.lastLoggedUserIdKey) ?? ""
    }
    
    func saveLastLoggedUserId(forUserId userId: String) {
        keyChainWrapper.set(userId, forKey: KeychainManager.lastLoggedUserIdKey)
    }
    
    override func isUserSaved() -> Bool {
        
        if !Tools.isStringNilOrEmpty(withString: getUserId()) {
            return true
        }
        return false
    }
    
    override func getUserId() -> String {
        return GCSController.getCore().getkeyChainManager()?.getUsername() ?? ""
    }
    
    override func saveUserId(forUserId userId: String) {
        GCSController.getCore().getkeyChainManager()?.saveUsername(username: userId)
    }
    
//    override func getFirstname() -> String {
//        return GCSController.getCore().getkeyChainManager()?.getCustomerName() ?? ""
//        return BGMUser.getSavedUserInformation().0 ?? ""
//    }
    
//    override func saveFirstname(forFirstname firstname: String) {
//        GCSController.getCore().getkeyChainManager()?.saveCustomerName(customerName: firstname)
//        BGMUser.saveUserInformation(name: firstName ?? nil, userId: userId ?? "")
//    }
    
    override func resetKeyChainItems() {
        super.resetKeyChainItems()
        keyChainWrapper.removeObject(forKey: KeychainManager.lastLoggedUserIdKey)
        GCSController.getCore().getkeyChainManager()?.cleanAppData()
        GCSController.getCore().deleteAllTokens()
        GCSController.getCore().cleanAppData()
    }
}
