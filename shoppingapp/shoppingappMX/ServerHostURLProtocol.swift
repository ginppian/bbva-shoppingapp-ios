//
//  ServerHostURLProtocol.swift
//  shoppingappMX
//
//  Created by Jesús Ángel Sánchez Sánchez on 15/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

protocol ServerHostURLProtocol: ServerHostURLProtocolGlobal {
    
    static func getDowngradeURL() -> String
}
