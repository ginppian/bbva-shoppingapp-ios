//
//  Router.swift
//  shoppingappMX
//
//  Created by daniel.petrovic on 25/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class RouterApp: RouterGlobal {

    override func navigateEditLimitsFromLimits(withModel model: Model) {
        
        let viewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: EditLimitViewControllerA.ID)! as! EditLimitViewControllerA
        viewController.initModel(withModel: model)
        
        BusManager.sessionDataInstance.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToEditLimitsHelp(with model: Model) {
        
        let viewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: EditLimitHelpViewController.id)! as! EditLimitHelpViewController
        viewController.initModel(withModel: model)
        
        let navigationController = UINavigationController(rootViewController: viewController)
        BusManager.sessionDataInstance.present(viewController: navigationController, animated: true, completion: nil)
    }
    
    override func navigateCardCVVScreenFromCards(model: Model) {
        let viewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: CvvViewControllerA.ID)! as! CvvViewControllerA
        viewController.initModel(withModel: model)
        let navigationController = UINavigationController(rootViewController: viewController)
        let flipAnimatorDelegate = FlipAnimatorDelegate()
        transitionDelegate = flipAnimatorDelegate
        navigationController.transitioningDelegate = transitionDelegate
        BusManager.sessionDataInstance.present(viewController: navigationController, animated: true, completion: nil)
    }

    override func controllersForTabBar() -> [UIViewController] {

        let navigationFirstTab: UINavigationController = UINavigationController(rootViewController: (BusManager.sessionDataInstance.routerFactory?.getViewController(route: HomeViewController.ID))!)

        let cardsViewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: CardsViewController.ID)! as! CardsViewControllerA

        let navigationSecondTab: UINavigationController = UINavigationController(rootViewController: cardsViewController)

        let shoppingViewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: ShoppingViewController.ID)! as! ShoppingViewControllerB

        let navigationThirdTab: UINavigationController = UINavigationController(rootViewController: shoppingViewController)

        let rewardsViewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: RewardsViewController.ID)! as! RewardsViewController

        let navigationFourthTab: UINavigationController = UINavigationController(rootViewController: rewardsViewController)

        return [navigationFirstTab, navigationSecondTab, navigationThirdTab, navigationFourthTab]

    }

    override func controllersForAnonymousTabBar() -> [UIViewController] {

        var tabBarViewControllers = [UIViewController]()

        let anonymousHomeViewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: AnonymousTutorialViewController.HomeID)
        if let homeViewController = anonymousHomeViewController {
            let homeTabNavigationController = UINavigationController(rootViewController: homeViewController)
            tabBarViewControllers.append(homeTabNavigationController)
        }

        let anonymousCardsViewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: AnonymousTutorialViewController.CardsID)
        if let cardsViewController = anonymousCardsViewController {
            let cardsTabNavigationController = UINavigationController(rootViewController: cardsViewController)
            tabBarViewControllers.append(cardsTabNavigationController)
        }

        let anonymousShoppingViewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: ShoppingViewController.ID)
        if let shoppingViewController = anonymousShoppingViewController {
            let shoppingTabNavigationController = UINavigationController(rootViewController: shoppingViewController)
            tabBarViewControllers.append(shoppingTabNavigationController)
        }

        let anonymousRewardsViewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: AnonymousTutorialViewController.RewardsID)
        if let rewardsViewController = anonymousRewardsViewController {
            let rewardsTabNavigationController = UINavigationController(rootViewController: rewardsViewController)
            tabBarViewControllers.append(rewardsTabNavigationController)
        }

        return tabBarViewControllers
    }
    
    override func navigateFirstScreenFromLogin(withModel model: Model) {
        
        let viewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: LoginLoaderViewController.ID)! as! LoginLoaderViewControllerBGM
        viewController.initModel(withModel: model)
        BusManager.sessionDataInstance.navigationController?.pushViewController(viewController, animated: true)
    }
    
    override func navigateFirstScreenFromShopping(withModel model: Model) {

        let viewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: ShoppingListViewController.ID)! as! ShoppingListViewControllerB
        viewController.initModel(withModel: model)
        BusManager.sessionDataInstance.navigationController?.pushViewController(viewController, animated: true)
    }

    func navigateSecondScreenFromShopping(withModel model: Model) {

        let viewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: ShoppingMapViewController.ID)! as! ShoppingMapViewControllerB
        viewController.initModel(withModel: model)
        BusManager.sessionDataInstance.navigationController?.pushViewController(viewController, animated: true)
    }

    func navigateFavoriteCategoriesFromShopping(withModel model: Model) {

        let viewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: SelectFavoriteCategoryViewController.ID)! as! SelectFavoriteCategoryViewController
        viewController.initModel(withModel: model)

        let navigationController = UINavigationController(rootViewController: viewController)
        BusManager.sessionDataInstance.present(viewController: navigationController, animated: true, completion: nil)
    }

    func navigateToLocationSettingHelp() {

        let viewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: GPSHelpViewController.id)! as! GPSHelpViewController
        let navigationController = UINavigationController(rootViewController: viewController)
        BusManager.sessionDataInstance.present(viewController: navigationController, animated: true, completion: nil)
    }

    func navigateFirstScreenFromShoppingListA(withModel model: Model) {

        let viewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: ShoppingMapViewController.ID)! as! ShoppingMapViewControllerB
        viewController.initModel(withModel: model)
        BusManager.sessionDataInstance.navigationController?.pushViewController(viewController, animated: true)
    }

    override func navigateToShoppingDetail(withModel model: Model) {

        let viewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: ShoppingDetailViewController.ID)! as! ShoppingDetailViewControllerB
        viewController.initModel(withModel: model)
        let navigationController = UINavigationController(rootViewController: viewController)
        BusManager.sessionDataInstance.present(viewController: navigationController, animated: true, completion: nil)
    }

    override func navigateShoppingListFilteredFromShoppingFilter(withModel model: Model) {

        let viewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: ShoppingListFilteredViewController.ID)! as! ShoppingListFilteredViewController
        viewController.initModel(withModel: model)
        BusManager.sessionDataInstance.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToShoppingStoreList(withModel model: Model) {
        
        let viewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: StoreListViewController.ID)! as! StoreListViewController
        viewController.initModel(withModel: model)
        BusManager.sessionDataInstance.navigationController?.pushViewController(viewController, animated: true)
    }
    
    func navigateToShoppingStoreMap(withModel model: Model) {
        
        let viewController = BusManager.sessionDataInstance.routerFactory?.getViewController(route: StoresMapViewController.ID)! as! StoresMapViewController
        viewController.initModel(withModel: model)
        BusManager.sessionDataInstance.navigationController?.pushViewController(viewController, animated: true)
    }
    
    override func viewControllerClassForPoints() -> AnyClass {

        return RewardsViewController.self
    }
    
    // MARK: - Softoken -
    
    func navigateSoftoken(withHeader header: [String: String]) {
        
        BusManager.sessionDataInstance.hideLoading(completion: {
            
            if let softokenVC = BusManager.sessionDataInstance.routerFactory?.getViewController(route: SoftokenSecurityViewController.ID) as? SoftokenSecurityViewController {
                let navigationController = UINavigationController(rootViewController: softokenVC)
                BusManager.sessionDataInstance.present(viewController: navigationController, animated: true, completion: nil)
            }
        })
    }
}
