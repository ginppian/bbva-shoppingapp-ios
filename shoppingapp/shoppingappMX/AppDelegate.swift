//
//  BVAAppDelegate.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 6/3/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import CellsNativeComponents
import CellsNativeCore
import BBVA_Network
import GoogleMaps
import UserNotifications
import Fabric
import Crashlytics
import AppsFlyerLib
import AdobeMobileSDK
import BGMCore
import BGMConnectSDK
import BGMCoreUI
import RxSwift
import BGMConnectManager

class AppDelegate: UIResponder, UIApplicationDelegate, AppsFlyerTrackerDelegate {

    var window: BGMWindow?
    var appLifeCycle: AppLifecycle?
    static let appsFlyerDevKey = "FHr5ZXWZnRFTUMstpdACf3"
    static let appID = "970582311"

    let disposeBag = DisposeBag()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]? = nil) -> Bool {

        ADBMobile.overrideConfigPath(Bundle(for: type(of: self)).path(forResource: Settings.Global.omnitureConfigPath, ofType: "json"))
        
        setGlomoConnectTabBarAppearance()
        initializeGlomoConnect()
        setupGlomoConnect()
        
        if !KeychainManager.shared.isUserSaved() {
            KeychainManager.shared.resetKeyChainItems()
            PreferencesManager.sharedInstance().resetPreferencesData()
            ConfigPublicManager.sharedInstance().resetCounters()
        } else if !KeychainManager.shared.isUserIdEqualToLastLoggedUserId() {
            KeychainManager.shared.saveFirstname(forFirstname: "")
            PreferencesManager.sharedInstance().resetPreferencesData()
            ConfigPublicManager.sharedInstance().resetCounters()
        }
        
        if isOnTesting() {
            window = BGMWindow(frame: UIScreen.main.bounds)
            window?.backgroundColor = .NAVY
            window?.rootViewController = UIViewController()
            window?.makeKeyAndVisible()
            return true
        }
        
        window = BGMWindow(frame: UIScreen.main.bounds)
        
        let sessionConfiguration = initSmi()
        appLifeCycle = BusManager.sessionDataInstance.loadInitFactory(sessionConfiguration: sessionConfiguration)

        let mainViewController = MainViewController.sharedInstance
        mainViewController.setup(type: 0)
        
        BusManager.sessionDataInstance.loadSplash()
        ComponentManager.add(id: RouterApp.ID, component: RouterApp())
        ComponentManager.add(id: CommonCellsGlobal.id, component: CommonCellsGlobal())
        ComponentManager.add(id: AppsFlyerTrackerHelper.id, component: AppsFlyerTrackerHelper())

        window?.backgroundColor = .white
        window?.makeKeyAndVisible()
        
        initAppsFlyer()
        
        IQKeyboardManager.shared.enable = true
        IQKeyboardManager.shared.shouldShowToolbarPlaceholder = false
        IQKeyboardManager.shared.shouldResignOnTouchOutside = true
        IQKeyboardManager.shared.keyboardDistanceFromTextField = 30
        IQKeyboardManager.shared.disabledToolbarClasses = [SecurityViewController.self, WalletCellsViewController.self]

        TrackerHelper.sharedInstance().configure()

        NotificationCenter.default.addObserver(self, selector: #selector(self.handlePasteboardNotification), name: .UIPasteboardChanged, object: nil)

        googleMapsApiKeyConfiguration()

        if PreferencesManager.sharedInstance().isFirstRun() {
            PublicConfigurationManager.sharedInstance().saveDefaultPublicConfiguration()
        }
        
        UIApplication.shared.applicationIconBadgeNumber = 0

        ConfigPublicManager.sharedInstance().getConfigFile()
        ConfigPublicManager.sharedInstance().getDidacticArea()

        SSLPinningManager.configureSSLPinning()

        BusManager.sessionDataInstance.clearCache()
        BusManager.sessionDataInstance.clearCookies()
        
        VersionControlManager.sharedInstance().checkAppUpgrade()
        
        FileManager.default.clearTmpDirectory()
        
        UIApplication.shared.registerForRemoteNotifications()

        if let remoteNotification = launchOptions?[.remoteNotification] as? [AnyHashable: Any] {

            BusManager.sessionDataInstance.publishData(tag: ShoppingCrossReaction.TAG, PageReactionConstants.TICKET_RECEIVED_IN_BACKGROUND, TicketModel(withPayload: remoteNotification))
        }
        
        Fabric.with([Crashlytics.self])
                
        return true
    }
    
    func isOnTesting() -> Bool {
        return NSClassFromString("XCTest") != nil
    }
    
    func initSmi() -> URLSessionConfiguration {
        
        #if DEBUG
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(receivedStateChage),
            name: NSNotification.Name(rawValue: SDSTATE_CHANGE_NOTIF),
            object: nil)
        #endif
        
        let excludedDatamiDomains = ["https://bbva-files.s3.amazonaws.com/wallet/help/shopping_app/help/Wallet_Apagar_Encender_Tarjeta.mp4",
                                     "https://bbva-files.s3.amazonaws.com/wallet/help/shopping_app/help/Wallet_Puntos.mp4",
                                     "https://bbva-files.s3.amazonaws.com/wallet/help/shopping_app/help/Wallet_Promotions.mp4",
                                     "https://bbva-files.s3.amazonaws.com/wallet/help/shopping_app/help/Wallet_Sticker.mp4",
                                     "https://bbva-files.s3.amazonaws.com/wallet/help/shopping_app/help/Wallet_Pago_Movil.mp4",
                                     "https://bbva-files.s3.amazonaws.com/wallet/shopping_app/faq/MX/v0/Wallet_Apagar_Encender_Tarjeta.mp4",
                                     "https://bbva-files.s3.amazonaws.com/wallet/shopping_app/faq/MX/v0/Wallet_Puntos.mp4",
                                     "https://bbva-files.s3.amazonaws.com/wallet/shopping_app/faq/MX/v0/Wallet_Promotions.mp4",
                                     "https://bbva-files.s3.amazonaws.com/wallet/shopping_app/faq/MX/v0/Wallet_Sticker.mp4",
                                     "https://bbva-files.s3.amazonaws.com/wallet/shopping_app/faq/MX/v0/Wallet_Pago_Movil.mp4"]
        
        SmiSdk.initSponsoredData("352f5cefce6fa44b9b4ec3ed4abfdb2e43366520", userId: nil, showSDMessage: false, exclusionUrls: excludedDatamiDomains)
        
        let sessionConfig = URLSessionConfiguration.default
        sessionConfig.timeoutIntervalForRequest = 30.0
        sessionConfig.timeoutIntervalForResource = 30.0
        SmiSdk.registerAppConfiguration(sessionConfig)
        
        return sessionConfig
    }

    #if DEBUG
    @objc func receivedStateChage(notification: NSNotification) {
        
        guard let smiResult = notification.object as? SmiResult else {
            return
        }
        
        if smiResult.sdState == SdState.SD_AVAILABLE {
            DLog(message: "receivedStateChage, SD_AVAILABLE \(smiResult.sdReason.rawValue) \(smiResult.sdReason)")
            
        } else if smiResult.sdState == SdState.SD_NOT_AVAILABLE {
            DLog (message: "receivedStateChage, SD_NOT_AVAILABLE \(smiResult.sdReason.rawValue) \(smiResult.sdReason)")
            
        } else if smiResult.sdState == SdState.SD_WIFI {
            DLog (message: "receivedStateChage, SD_WIFI %ld \(smiResult.sdReason.rawValue) \(smiResult.sdReason)")
        }
    }
    #endif
    
    func googleMapsApiKeyConfiguration() {

        let googleMapsKey = Configuration.googleMapsApiKey()

        GMSServices.provideAPIKey(googleMapsKey)

    }
    
    func initAppsFlyer() {
        AppsFlyerTracker.shared().appsFlyerDevKey = AppDelegate.appsFlyerDevKey
        AppsFlyerTracker.shared().appleAppID = AppDelegate.appID
        AppsFlyerTracker.shared().delegate = self
        
        #if DEBUG
//        AppsFlyerTracker.shared().isDebug = true
        #endif
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        let shoppingApp = ShoppingApplication.shared as! ShoppingApplication
        shoppingApp.resetTimer()

    }

    func applicationWillEnterForeground(_ application: UIApplication) {

        ConfigPublicManager.sharedInstance().getConfigFile()
        ConfigPublicManager.sharedInstance().getDidacticArea()
        
        if PublicConfigurationManager.sharedInstance().isNecessaryToUpdate {
        
            let publicConfigurationObservable = PublicConfigurationManager.sharedInstance().getPublicConfiguration()
            
            publicConfigurationObservable
                .subscribeOn(SerialDispatchQueueScheduler(qos: .default)) // Just execution in background
                .observeOn(MainScheduler.instance) // Events come in main thread
                .take(1) // Just the first event
                .subscribe( onNext: { publicConfigurationDTO in
                    
                    VersionControlManager.sharedInstance().checkVersion(withPublicConfigurationDTO: publicConfigurationDTO, shouldShowNoMandatoryUpdate: false)
                    
                }).addDisposableTo(disposeBag)
        }
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
        // Track Installs, updates & sessions(app opens) (You must include this API to enable tracking)
        AppsFlyerTracker.shared().trackAppLaunch()
        UIApplication.shared.applicationIconBadgeNumber = 0
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }

    @objc func handlePasteboardNotification(_ notification: Notification) {
        // Comprobar si el portapapeles esta vacio
        let pasteboard = notification.object as? UIPasteboard
        if (pasteboard?.string?.count ?? 0) != 0 {
            // Guardar la version actual del portapapeles
            let pasteboardVersion: Int? = pasteboard?.changeCount
            // Obtener el tiempo de expiracion
            var clearClipboardTimeout = TimeInterval()

            if let time = PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kClearCopyPan) as? Int {
                clearClipboardTimeout = TimeInterval(time)
            }

            // Crear la tarea temporizada en segundo plano
            let application = UIApplication.shared
            var bgTask: UIBackgroundTaskIdentifier = UIBackgroundTaskIdentifier()

            bgTask = application.beginBackgroundTask(expirationHandler: {
                application.endBackgroundTask(bgTask)

            })

            // Lanzar temporizador

            DispatchQueue.global(qos: .default).async(execute: {() -> Void in
                Thread.sleep(forTimeInterval: clearClipboardTimeout)
                // Borrar el portapapeles si no ha cambiado
                if pasteboardVersion == pasteboard?.changeCount {
                    pasteboard?.string = ""
                }
                // Finalizar la tarea
                application.endBackgroundTask(bgTask)
            })

        }

    }

    // MARK: Remote Notifications

    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        let deviceTokenString = deviceToken.map { String(format: "%02hhx", $0) }.joined()
        
        DLog(message: "☁️📲📮 DEVICE TOKEN: \(deviceTokenString) ☁️📲📮")
        
        //Check token
        let lastApnsToken = PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kApnsToken) as? String

        if lastApnsToken != deviceTokenString {
            
            //save new token
            PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: deviceTokenString as AnyObject, withKey: PreferencesManagerKeys.kApnsToken)

            BusManager.sessionDataInstance.publishData(tag: ShoppingCrossReaction.TAG, PageReactionConstants.TOKEN_CHANGED, deviceTokenString)
        }
    }

    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        
        BusManager.sessionDataInstance.publishData(tag: ShoppingCrossReaction.TAG, PageReactionConstants.TOKEN_FAILED, error.localizedDescription)
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
        DLog(message: "didReceiveRemoteNotification: \(userInfo)")

        BusManager.sessionDataInstance.showTicketDetail(withModel: TicketModel(withPayload: userInfo))
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey: Any] = [:]) -> Bool {
        
        handleOpenUrl(url: url)
        
        return true
    }
    
    func handleOpenUrl(url: URL) {
        
        guard let securityManager = BGMSecurityWrapper.shared.securityManager, let keychainManager = GCSController.getCore().getkeyChainManager(), let softokenManager = securityManager.softokenManager, let query = url.query  else {
            BGMSecurityWrapper.shared.handleOpenUrl(url: url)
            return
        }
        
        let queryDict = softokenManager.parseUrlQuery(query: query)
        let operation: String = queryDict[softokenManager.FIELD_OPERATION] ?? ""
        let appId = queryDict[softokenManager.FIELD_APP_ID] ?? ""
        let registrofieldChallenge = queryDict[softokenManager.FIELD_REGISTRO_CHALLENGE_KEY]
        let urlApp: String = "\(appId)://"
        let isGlomoCenterApp = keychainManager.isGlomoCenterApp()
        
        if (operation == softokenManager.OPERATION_GENERATE_OTP_TIME || operation == softokenManager.OPERATION_GENERATE_OTP_REGISTER) && isGlomoCenterApp {
            
            if self.window?.rootViewController == nil {
                self.window?.rootViewController = getLaunchController()
            }
            
            let otp = GCSController.getCore().getOTP(withChallenge: registrofieldChallenge, isQR: false) ?? ""
            let error: String = otp.count > 0 ? (softokenManager.FIELD_STATUS_OK) : (softokenManager.FIELD_STATUS_ERROR)
            keychainManager.saveOTPToKeychain(otp: otp)
            
            DispatchQueue.main.async {
                
                if let openUrl = URL(string: "\(urlApp)?\("\(softokenManager.FIELD_APP_ID)")=\("\(softokenManager.appName)")&\("\(softokenManager.FIELD_STATUS_TOKEN)")=\(error)"), UIApplication.shared.canOpenURL(openUrl) {
                    UIApplication.shared.open(openUrl, options: [:], completionHandler: nil)
                }
            }
            
        } else if operation == softokenManager.OPERATION_SHOW_TOKEN_SCREEN && isGlomoCenterApp {
            
            DispatchQueue.main.async {
                let navCont = self.window?.rootViewController
                GCSController.getCore().openMobileTokenFlow(currentController: navCont!, delegate: nil)
            }
            
        } else {
            
            BGMSecurityWrapper.shared.handleOpenUrl(url: url)
        }
    }
    
    func getLaunchController() -> UIViewController {
        return UIViewController()
    }
}

// MARK: - Glomo connect

extension AppDelegate {
    
    func initializeGlomoConnect() {
        
        let core = BGMCore.sharedCore
        core.initializeCoreConfiguration()
        core.configManager?.loadConfigurationFile(source: BGMAppConfigManagerSource(type: .SERVER, path: BGMFilePath(name: BGMGlomoAppConfiguration.kServerConfig.rawValue, fileType: "json", type: .LOCAL, filterData: true)))
        core.configManager?.loadConfigurationFile(source: BGMAppConfigManagerSource(type: .CONNECT, path: BGMFilePath(name: BGMGlomoAppConfiguration.kConnectConfig.rawValue, fileType: "json", type: .LOCAL, filterData: true)))
        
        BGMAppSettingsManager.sharedInstance.loadConfigurations(env: ServerHostURL.environmentGlomoConnect())
        
        if let coreConfig = core.coreConfiguration {
            coreConfig.apiVersionJson = core.configManager?.fetchFeatureDict(module: ServerHostURL.environmentGlomoConnect(), feature: "apiVersion", config: .SERVER) as? [String: String]
            coreConfig.populateServerConfigManager(selectedEnvironment: ServerHostURL.environmentGlomoConnect() )
        }
        try? GCSController.create(path: "serverConfig", sslConfigPath: "config", bundle: Bundle.main, environment: ServerHostURL.environmentGlomoConnect(), allowJailbreakDevice: true)
        let path = BGMFilePath(name: "appconfig", fileType: "json", type: PathType.LOCAL)
        core.configManager?.loadConfigurationFile(source: BGMAppConfigManagerSource(type: ConfigurationType.MAINAPPLN, path: path))
        
        if let configManager = BGMCore.sharedCore.configManager, let retriesString = configManager.fetchFeatureValue(module: "appConfig", attribute: "numberOfRetriesAllowed", config: ConfigurationType.APPLN), let otpSmSTimerString = configManager.fetchFeatureValue(module: "appConfig", attribute: "otpSmsTimeout", config: ConfigurationType.APPLN),
            let retries = Int(retriesString), let otpSmSTimer = Int(otpSmSTimerString) {
            GCSController.getCore().setNumberOfRetries(retries: retries)
            GCSController.getCore().setOTPSMSTimer(timer: otpSmSTimer)
            if let signOperation = BGMCore.sharedCore.configManager?.fetchFeatureDict(module: "connectConfig", feature: "signOperation", config: ConfigurationType.CONNECT) as? [String: Any], let data = signOperation["data"] as? [String: Any] {
                GCSController.getCore().setShowSoftTokenTickImage(show: data["showSoftTokenTickImage"]as? Bool ?? false)
                GCSController.getCore().setIsCheckActivationForSigning(enabled: data["checkActivationForSigning"]as? Bool ?? false)
                GCSController.getCore().setIsShowOtpFailureAsToast(enabled: data["showOtpFailureAsToast"]as? Bool ?? false)
                GCSController.getCore().setIVRConfig(regex: data["ivr"] as? String ?? .emptyString)
            }

            let dict = BGMCore.sharedCore.configManager?.fetchFeatureDict(module: "featureConfig", feature: "sideMenu", config: ConfigurationType.APPLN) as? [String: AnyObject?]
            
            let items = dict?["items"] as? [[String: AnyObject?]]
            
            let item = items?.first(where: { temp -> Bool in
                if let itemId = temp["itemId"] as? String, itemId == "CALL_BBVA" {
                    return true
                }
                return false
            })
            
            if let number = item?["iOSAction"] as? String {
                GCSController.getCore().setPhoneNumber(number: number)
            }
            if let deviceActivationConfig = BGMCore.sharedCore.configManager?.fetchFeatureDict(module: "connectConfig", feature: "deviceActivation", config: ConfigurationType.CONNECT) as? [String: Any], let data = deviceActivationConfig["data"] as? [String: Any] {
                GCSController.getCore().setDeviceActivationConfig(dict: data)
            }
            if let onboardingConfig = BGMCore.sharedCore.configManager?.fetchFeatureDict(module: "connectConfig", feature: "onboarding", config: ConfigurationType.CONNECT) as? [String: Any], let data = onboardingConfig["data"] as? [String: Any] {
                GCSController.getCore().setOnboardingConfig(dict: data)
            }
            if let digitalActivation = BGMCore.sharedCore.configManager?.fetchFeatureDict(module: "connectConfig", feature: "digitalActivation", config: ConfigurationType.CONNECT) as? [String: Any], let data = digitalActivation["data"] as? [String: Any] {
                GCSController.getCore().setIsDocumentIdentification(enabled: data["documentIdentification"]as? Bool ?? false)
            }
            if let authentication = BGMCore.sharedCore.configManager?.fetchFeatureDict(module: "connectConfig", feature: "authentication", config: ConfigurationType.CONNECT) as? [String: Any], let data = authentication["data"] as? [String: Any] {
                BGMCore.sharedCore.coreConfiguration?.authenticationType = (data["authenticationType"] as? [String: Any])?["userPassword"] as? String
                BGMCore.sharedCore.coreConfiguration?.consumerId = (data["consumerID"] as? [String: Any])?["userPassword"] as? String
            }
            
            GCSController.getCore().setShowOtpAsPassword(show: Bool(exactly: NSNumber(value: Int((BGMCore.sharedCore.configManager?.fetchFeatureValue(module: "appConfig", attribute: "showOtpAsPassword", config: ConfigurationType.APPLN)) ?? "0") ?? 0)) ?? false)
        }
        
        GCSController.getCore().setUrlScheme(urlScheme: "bbvawalletiphonemx")
        
        DispatchQueue.global(qos: .background).async {
            GCSController.getCore().initializeAdditionalFeatures()
            GCSController.getCore().initializeSSL()
        }
    }
    
    func setupGlomoConnect() {
        
        Locale.countryID = "mx"
        GCSController.getCore().setEncoding(keyTag: ServerHostURL.getKeyTagForEncryption())
        GCSController.getCore().setOTPSMSTimer(timer: 180)
        GCSController.getCore().setPhoneNumber(number: Localizables.glomo_connect.key_bank_phone_number)
        GCSController.getCore().setIVRFlowEnabled(enabled: true)
        
    }

    func setGlomoConnectTabBarAppearance() {
        
        BGMTabBarController.tintColor = .BBVADarkCoreBlue()
        BGMTabBarController.borderColor = .BBVA200()
        BGMTabBarController.normalTextColor = .BBVA500()
        BGMTabBarController.selectedTextColor = .BBVADarkCoreBlue()
        BGMTabBarController.font = .BBVAMedium(size: 12)
        BGMNavigationController.barTintColor = .bbvaBlue()
        BGMNavigationController.titleText = .BBVABook(size: 18)
        BGMContainerController.navigationShadowColor = .BBVANavy()
    }
    
}
