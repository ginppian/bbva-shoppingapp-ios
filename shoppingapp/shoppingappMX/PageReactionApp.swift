//
//  PageReaction.swift
//  shoppingappMX
//
//  Created by daniel.petrovic on 25/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import CellsNativeCore

class PageReactionApp: PageReactionGlobal {

    override init() {
        
        super.init()
        addExtraPages()
    }
    
    func addExtraPages() {
        
        addGlobal(id: TrackerGlobalReaction.tag) { () -> GlobalReaction in
            TrackerGlobalReaction()
        }

        add(id: GPSHelpViewController.id) { () -> PageReaction in
            GPSHelpPageReaction()
        }

        add(id: ShoppingMapViewController.ID) { () -> PageReaction in
            ShoppingMapPageReactionB()
        }

        add(id: SelectFavoriteCategoryViewController.ID) { () -> PageReaction in
            SelectFavoriteCategoryPageReaction()
        }

        add(id: RewardsViewController.ID) { () -> PageReaction in
            RewardsPageReaction()
        }
        
        add(id: StoreListViewController.ID) { () -> PageReaction in
            StoreListPageReaction()
        }

        add(id: CardActivationByCVVViewController.id) { () -> PageReaction in
            CardActivationByCVVPageReaction()
        }

        add(id: OnOffHelpViewController.ID) { () -> PageReaction in
            OnOffHelpPageReactionA()
        }
        
        add(id: UserBlockedViewController.id) { () -> PageReaction in
            UserBlockedPageReaction()
        }
        
        add(id: MobileTokenViewController.id) { () -> PageReaction in
            MobileTokenPageReaction()
        }

        add(id: AnonymousTutorialViewController.ID) { () -> PageReaction in
            AnonymousTutorialPageReactionB()
        }
        
        add(id: SoftokenSecurityViewController.ID) { () -> PageReaction in
            SoftokenSecurityPageReaction()
        }
    }
    
    override func initPageLogin() {
        
        add(id: LoginViewController.ID) { () -> PageReaction in
            LoginPageReactionB()
        }
    }

    override func initPageShopping() {
        
        add(id: ShoppingViewController.ID) { () -> PageReaction in
            ShoppingPageReactionB()
        }
    }

    override func initPageShoppingList() {
        
        add(id: ShoppingListViewController.ID) { () -> PageReaction in
            ShoppingListPageReactionB()
        }
    }

    override func initPageCards() {
        
        add(id: CardsViewController.ID) { () -> PageReaction in
            CardsPageReactionA()
        }
    }

    override func initPageShoppingListFiltered() {
        
        add(id: ShoppingListFilteredViewController.ID) { () -> PageReaction in
            ShoppingListFilteredPageReactionB()
        }
    }
    
    override func initPageShoppingDetail() {
        
        add(id: ShoppingDetailViewController.ID) { () -> PageReaction in
            ShoppingDetailPageReactionB()
        }
    }

    override func initPageEditLimits() {
        add(id: EditLimitViewController.ID) { () -> PageReaction in
            EditLimitPageReactionA()
        }
    }
    
    override func initPageLeftMenu() {
        add(id: LeftMenuViewController.ID) { () -> PageReaction in
            LeftMenuPageReactionA()
        }
    }
}
