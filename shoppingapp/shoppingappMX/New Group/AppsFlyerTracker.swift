//
//  AppsFlyerTracker.swift
//  shoppingappMX
//
//  Created by jesus.martinez on 7/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import AppsFlyerLib

class AppsFlyerTrackerHelper: EventTracker {
    
    fileprivate let loginTag = "Acceso"
    fileprivate let showPromotionsTag = "Ver_promociones"
    fileprivate let startOptionTag = "Inicio"
    fileprivate let cardsOptionTag = "Tarjetas"
    fileprivate let promotionsOptionTag = "Promociones"
    fileprivate let pointsOptionTag = "Puntos"
    fileprivate let notificationsOptionTag = "Notificaciones"
    fileprivate let seeShoppingFilterResultsTag = "Ver_Resultados"
    fileprivate let favoriteCategories = "Personalizar_Promociones"
    fileprivate let promotionDetail = "Detalle_Promocion"
    fileprivate let noPromotionContactTag = "Contacto_No_Promocion"
    fileprivate let promotionPushTag = "Promoción_Push"
    fileprivate let generateDigitalCardTag = "Dig_generar_tarjeta_digital"
    fileprivate let seeCardCVVTag = "Dig_Ver_CVV"
    
    func track(event: EventProtocol) {
        
        switch event.type {
        case .login:
            login()
        case .showPromotions:
            showPromotions()
        case .startOption:
            startOption()
        case .cardsOption:
            cardsOption()
        case .promotionsOption:
            promotionsOption()
        case .pointsOption:
            pointsOption()
        case .notificationsOption:
            notificationsOption()
        case .seeShoppingFilterResults:
            seeShoppingFilterResults()
        case .noPromotionContact:
            noPromotionContact()
        case .promotionPush:
            promotionPush()
        case .generateDigitalCard:
            generateDigitalCard()
        case .seeCardCVV:
            seeCardCVV()
        case .promotionDetail:
            seePromotionDetail()
        case .favoriteCategories:
            updateFavoriteCategories()
        }
    }
    
    fileprivate func trackEvent(with name: String) {
        AppsFlyerTracker.shared().trackEvent(name, withValues: nil)
    }
    
    fileprivate func login() {
        trackEvent(with: loginTag)
    }
    
    fileprivate func showPromotions() {
        trackEvent(with: showPromotionsTag)
    }
    
    fileprivate func startOption() {
        trackEvent(with: startOptionTag)
    }
    
    fileprivate func cardsOption() {
        trackEvent(with: cardsOptionTag)
    }
    
    fileprivate func promotionsOption() {
        trackEvent(with: promotionsOptionTag)
    }
    
    fileprivate func pointsOption() {
        trackEvent(with: pointsOptionTag)
    }
    
    fileprivate func notificationsOption() {
        trackEvent(with: notificationsOptionTag)
    }
    
    fileprivate func seeShoppingFilterResults() {
        trackEvent(with: seeShoppingFilterResultsTag)
    }
    
    fileprivate func noPromotionContact() {
        trackEvent(with: noPromotionContactTag)
    }
    
    fileprivate func promotionPush() {
        trackEvent(with: promotionPushTag)
    }
    
    fileprivate func generateDigitalCard() {
        trackEvent(with: generateDigitalCardTag)
    }
    
    fileprivate func seeCardCVV() {
        trackEvent(with: seeCardCVVTag)
    }
    
    fileprivate func seePromotionDetail() {
        trackEvent(with: promotionDetail)
    }
    
    fileprivate func updateFavoriteCategories() {
        trackEvent(with: favoriteCategories)
    }
}
