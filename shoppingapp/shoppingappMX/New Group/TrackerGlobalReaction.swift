//
//  TrackerGlobalReaction.swift
//  shoppingappMX
//
//  Created by jesus.martinez on 7/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import CellsNativeCore
import CellsNativeComponents

class TrackerGlobalReaction: GlobalReaction {
    
    static let tag: String = String(describing: TrackerGlobalReaction.self)

    override func configure() {
        
        publishEvent()

        reactToEvent()
    }
    
    func publishEvent() {
        
        _ = writeOn(channel: PageReactionConstants.CHANNEL_NEW_TRACK_EVENT)
            .when(componentID: AppsFlyerTrackerHelper.id)
            .doAction(actionSpec: PageReactionConstants.NEW_TRACK_EVENT)
    }
    
    func reactToEvent() {
        
        _ = reactTo(channel: PageReactionConstants.CHANNEL_NEW_TRACK_EVENT)?
            .then()
            .inside(componentID: AppsFlyerTrackerHelper.id, component: AppsFlyerTrackerHelper.self)
            .doReactionAndClearChannel { tracker, model in
                
                if let model = model as? EventProtocol {
                    tracker.track(event: model)
                }
                
        }
    }
}
