//
//  DeviceManager.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 23/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import UIKit
import BGMConnectSDK

class DeviceManager: DeviceManagerGlobal {

    var deviceUID: String? {
        return GCSController.getCore().deviceId
    }
    
    // MARK: Singleton
    
    static var instance = DeviceManager()
    
    class func sharedInstance () -> DeviceManager {
        return instance
    }
    
}
