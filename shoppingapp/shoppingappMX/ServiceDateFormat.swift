//
//  ServiceDateFormat.swift
//  shoppingappMX
//
//  Created by AZIZEBULBUL on 25/07/2018.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class ServiceDateFormat: ServiceDateFormatGlobal {

    override class  func dateFormat(forService: Services) -> String {

        switch forService {

        case .customers:
            return "yyyy-MM-dd'T'HH:mm:ss'Z'"
        }
    }
}
