//
//  MainViewControllerA.swift
//  shoppingappMX
//
//  Created by jesus.martinez on 26/12/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

class MainViewController: MainViewControllerGlobal {
    
    override func configureLeftMenu() -> UIViewController {
        
        let viewLeftMenu = LeftMenuViewControllerA(nibName: "LeftMenuViewController", bundle: nil)
        let presenterLeftMenu = LeftMenuPresenterA<LeftMenuViewControllerA>()
        viewLeftMenu.presenter = presenterLeftMenu
        presenterLeftMenu.view = viewLeftMenu
        presenterLeftMenu.interactor = LeftMenuInteractor()
        
        return viewLeftMenu
    }
}
