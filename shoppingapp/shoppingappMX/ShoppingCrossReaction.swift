//
//  ShoppingCrossReaction.swift
//  shoppingappMX
//
//  Created by Javier Dominguez on 18/12/2018.
//  Copyright © 2018 BBVA. All rights reserved.
//

import CellsNativeCore
import CellsNativeComponents

class ShoppingCrossReaction: ShoppingCrossReactionGlobal {
    
    override func configure() {
        
        super.configure()
        softoken()
    }
    
    func softoken() {
        
        _ = writeOn(channel: PageReactionConstants.CHANNEL_SOFTOKEN)
            .when(componentID: SecurityInterceptorStore.ID)
            .doAction(actionSpec: SecurityInterceptorStore.SOFTOKEN)
        
        _ = reactTo(channel: PageReactionConstants.CHANNEL_SOFTOKEN)?
            .then()
            .inside(componentID: RouterApp.ID, component: RouterApp.self)
            .doReactionAndClearChannel(reactionExecution: { router, param in
                
                let otpGlomoConnect = Settings.Security.softToken
                if let otp = otpGlomoConnect, otp.isNotEmpty {
                    
                    if let header = param as? [String: String] {
                        router.navigateSoftoken(withHeader: header)
                    }
                    
                } else {
                    
                    BusManager.sessionDataInstance.securityInterceptorStore?.unLockStop()
                }
            })
    }
}
