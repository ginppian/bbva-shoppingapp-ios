//
//  RouterInitFactory.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 7/9/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import UIKit
import CellsNativeCore

class RouterInitFactory: RouterInitFactoryGlobal {

    override init() {
        super.init()
        
        addShoppingMapRoute()
        addGpsHelpRoute()
        addFavoriteCategoriesRoute()
        addRewardsRoute()
        addShoppingStoresListRoute()
        addShoppingStoresMapRoute()
        addActivateCardByCVVdRoute()
        addUserBloquedRoute()
        addLimitsHelpRoute()
        addMobileTokenRoute()
        addSoftokenSecurityRoute()
    }
    
    override func addLeftMenuRoute() {
        self.addRoute(route: LeftMenuViewController.ID, routeObject: Route(viewController: { () -> UIViewController in
            
            let viewLeftMenu = LeftMenuViewControllerA(nibName: "LeftMenuViewController", bundle: nil)
            let presenterLeftMenu = LeftMenuPresenterA<LeftMenuViewControllerA>()
            viewLeftMenu.presenter = presenterLeftMenu
            presenterLeftMenu.view = viewLeftMenu
            return viewLeftMenu
            
        }, animationToNext: { _, destinationViewController in
            
            self.animation(viewController: destinationViewController!)
            
        }, animationToPrevious: { _, destinationViewController in
            
            self.animation(viewController: destinationViewController!)
            
        }))
    }
    
    override func addLoginRoute() {
        
        addRoute(route: LoginViewController.ID, routeObject: Route(viewController: { () -> UIViewController in
            
            let viewLogin = LoginViewControllerBGM(nibName: "LoginViewController", bundle: nil)
            let presenterLogin = LoginPresenterBGM<LoginViewControllerBGM>()
            viewLogin.presenter = presenterLogin
            presenterLogin.view = viewLogin
            return viewLogin
            
        }, animationToNext: { _, destinationViewController  in
            
            if let destinationViewController = destinationViewController {
                self.animation(viewController: destinationViewController)
            }
            
        }, animationToPrevious: { _, destinationViewController in
            
            if let destinationViewController = destinationViewController {
                self.animation(viewController: destinationViewController)
            }
            
        }))
    }

    override func addEditLimitsRoute() {

        self.addRoute(route: EditLimitViewControllerA.ID, routeObject: Route(viewController: { () -> UIViewController in

            let viewEditLimits = EditLimitViewControllerA(nibName: "EditLimitViewControllerA", bundle: nil)
            let presenterEditLimits = EditLimitPresenterA<EditLimitViewControllerA>()
            viewEditLimits.hidesBottomBarWhenPushed = true
            viewEditLimits.presenter = presenterEditLimits
            presenterEditLimits.interactor = EditLimitInteractor()
            presenterEditLimits.view = viewEditLimits
            return viewEditLimits
            
        }, animationToNext: { _, destinationViewController in
            
            self.animation(viewController: destinationViewController!)
            
        }, animationToPrevious: { _, destinationViewController in
            
            self.animation(viewController: destinationViewController!)
            
        }))
        
    }

    override func addLimitsRoute() {
        
        self.addRoute(route: LimitsViewController.ID, routeObject: Route(viewController: { () -> UIViewController in
            let viewLimits = LimitsViewController(nibName: "LimitsViewController", bundle: nil)
            let presenterLimits = LimitsPresenterA<LimitsViewController>()
            viewLimits.hidesBottomBarWhenPushed = true
            viewLimits.presenter = presenterLimits
            presenterLimits.interactor = LimitsInteractor()
            presenterLimits.view = viewLimits
            return viewLimits
            
        }, animationToNext: { _, destinationViewController in
            
            self.animation(viewController: destinationViewController!)
            
        }, animationToPrevious: { _, destinationViewController in
            
            self.animation(viewController: destinationViewController!)
            
        }))
        
    }

    override func addLoginLoaderRoute() {
        
        addRoute(route: LoginLoaderViewController.ID, routeObject: Route(viewController: { () -> UIViewController in

            let viewLoginLoader = LoginLoaderViewControllerBGM(nibName: "LoginLoaderViewController", bundle: nil)
            let presenterLoginLoader = LoginLoaderPresenterBGM<LoginLoaderViewControllerBGM>()
            viewLoginLoader.presenter = presenterLoginLoader
            presenterLoginLoader.view = viewLoginLoader
            presenterLoginLoader.interactor = LoginLoaderInteractorA()

            return viewLoginLoader

        }, animationToNext: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }, animationToPrevious: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }))
    }
    
    override func addOnOffHelpRoute() {
        
        addRoute(route: OnOffHelpViewController.ID, routeObject: Route(viewController: { () -> UIViewController in
            
            let onOffHelpView = OnOffHelpViewController(nibName: "OnOffHelpViewController", bundle: nil)
            
            let onOffHelpPresenter = OnOffHelpPresenterA<OnOffHelpViewController>()
            
            onOffHelpView.presenter = onOffHelpPresenter
            
            onOffHelpPresenter.view = onOffHelpView
            
            return onOffHelpView
            
        }, animationToNext: { _, destinationViewController in
            
            self.animation(viewController: destinationViewController!)
            
        }, animationToPrevious: { _, destinationViewController in
            
            self.animation(viewController: destinationViewController!)
            
        }))
    }

    override func addCardsRoute() {

            addRoute(route: CardsViewController.ID, routeObject: Route(viewController: { () -> UIViewController in

            let viewCards = CardsViewControllerA(nibName: "CardsViewControllerA", bundle: nil)

            let presenterCards = CardsPresenterA<CardsViewControllerA>()

            viewCards.presenter = presenterCards

            presenterCards.view = viewCards

            presenterCards.interactor = CardsInteractorA()

            viewCards.tabBarItem = UITabBarItem(title: Localizables.common.key_tabbar_cards_button, image: ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Card.creditcard_icon, color: .GRAYTABBAR), width: 18, height: 18), tag: 2)

            viewCards.tabBarItem.setTitleTextAttributes([NSAttributedStringKey.font: Tools.setFontMedium(size: 10)], for: .normal)

            viewCards.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -3)

            return viewCards
        }))

    }

    override func addShoppingRoute() {

            addRoute(route: ShoppingViewController.ID, routeObject: Route(viewController: { () -> UIViewController in

            let viewShopping = ShoppingViewControllerB(nibName: "ShoppingViewControllerB", bundle: nil)

            let presenterShopping = ShoppingPresenterB<ShoppingViewControllerB>()

            viewShopping.presenter = presenterShopping

            presenterShopping.view = viewShopping

            presenterShopping.interactor = ShoppingInteractor()

            viewShopping.tabBarItem = UITabBarItem(title: Localizables.common.key_tabbar_offers_button, image: ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Promotions.tab_promotions, color: .GRAYTABBAR), width: 18, height: 18), tag: 3)

            viewShopping.tabBarItem.setTitleTextAttributes([NSAttributedStringKey.font: Tools.setFontMedium(size: 10)], for: .normal)

            viewShopping.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -3)

            return viewShopping

        }, animationToNext: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }, animationToPrevious: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }))

    }

    override func addShoppingListRoute() {

        addRoute(route: ShoppingListViewController.ID, routeObject: Route(viewController: { () -> UIViewController in

            let viewShoppingList = ShoppingListViewControllerB(nibName: "ShoppingListViewControllerB", bundle: nil)

            let presenterShoppingList = ShoppingListPresenterB<ShoppingListViewControllerB>()

            viewShoppingList.hidesBottomBarWhenPushed = true

            viewShoppingList.presenter = presenterShoppingList

            presenterShoppingList.view = viewShoppingList

            presenterShoppingList.interactor = ShoppingListInteractor()

            return viewShoppingList

        }, animationToNext: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }, animationToPrevious: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }))

    }

    func addLimitsHelpRoute() {
        
        addRoute(route: EditLimitHelpViewController.id, routeObject: Route(viewController: { () -> UIViewController in
            let editlimitHelpView = EditLimitHelpViewController(nibName: "EditLimitHelpViewController", bundle: nil)
            let editLimitHelpPresenter = EditLimitHelpPresenter<EditLimitHelpViewController>()
            editlimitHelpView.hidesBottomBarWhenPushed = true
            editlimitHelpView.presenter = editLimitHelpPresenter
            editLimitHelpPresenter.view = editlimitHelpView
            return editlimitHelpView
            
        }, animationToNext: { _, destinationViewController in
            
            self.animation(viewController: destinationViewController!)
            
        }, animationToPrevious: { _, destinationViewController in
            
            self.animation(viewController: destinationViewController!)
            
        }))
        
    }
    
    override func addTransactionListRoute() {
        self.addRoute(route: TransactionListViewController.ID, routeObject: Route(viewController: { () -> UIViewController in

            let viewTransactionList = TransactionListViewController(nibName: "TransactionListViewController", bundle: nil)
            let presenterTransactionListA = TransactionsListPresenterA<TransactionListViewController>()
            viewTransactionList.hidesBottomBarWhenPushed = true
            viewTransactionList.presenter = presenterTransactionListA
            presenterTransactionListA.view = viewTransactionList
            presenterTransactionListA.interactor = TransactionsListInteractorA()
            return viewTransactionList

        }, animationToNext: { _, destinationViewController in
            self.animation(viewController: destinationViewController!)
        }, animationToPrevious: { _, destinationViewController in
            self.animation(viewController: destinationViewController!)
        }))
        
    }

    override func addCardCVVRoute() {

        self.addRoute(route: CvvViewController.ID, routeObject: Route(viewController: { () -> UIViewController in

            let viewCardCVV = CvvViewControllerA(nibName: "CvvViewControllerA", bundle: nil)

            let presenterCardCVV = CVVPresenterA<CvvViewControllerA>()

            viewCardCVV.presenter = presenterCardCVV
            presenterCardCVV.interactor = CVVInteractor()
            presenterCardCVV.view = viewCardCVV

            return viewCardCVV

        }, animationToNext: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }, animationToPrevious: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }))
    }

    func addShoppingMapRoute() {

        addRoute(route: ShoppingMapViewController.ID, routeObject: Route(viewController: { () -> UIViewController in

            let viewShoppingMap = ShoppingMapViewControllerB(nibName: "ShoppingMapViewControllerB", bundle: nil)

            let presenterShoppingMap = ShoppingMapPresenterB<ShoppingMapViewControllerB>()

            viewShoppingMap.hidesBottomBarWhenPushed = true

            viewShoppingMap.presenter = presenterShoppingMap

            presenterShoppingMap.view = viewShoppingMap

            presenterShoppingMap.interactor = ShoppingMapInteractor()

            return viewShoppingMap

        }, animationToNext: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }, animationToPrevious: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }))

    }

    func addGpsHelpRoute() {

        addRoute(route: GPSHelpViewController.id, routeObject: Route(viewController: { () -> UIViewController in

            let viewGPSHelp = GPSHelpViewController(nibName: "GPSHelpViewController", bundle: nil)
            let presenterGPSHelp = GPSHelpPresenter<GPSHelpViewController>()
            viewGPSHelp.hidesBottomBarWhenPushed = true
            viewGPSHelp.presenter = presenterGPSHelp
            presenterGPSHelp.view = viewGPSHelp
            return viewGPSHelp

        }, animationToNext: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }, animationToPrevious: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }))
    }

    override func addShoppingDetailRoute() {

        self.addRoute(route: ShoppingDetailViewController.ID, routeObject: Route(viewController: { () -> UIViewController in

            let viewShoppingDetail = ShoppingDetailViewControllerB(nibName: "ShoppingDetailViewControllerB", bundle: nil)

            let presenterShoppingDetail = ShoppingDetailPresenterB<ShoppingDetailViewControllerB>()

            viewShoppingDetail.presenter = presenterShoppingDetail

            presenterShoppingDetail.view = viewShoppingDetail
            
            presenterShoppingDetail.interactor = ShoppingDetailInteractor()
            
            return viewShoppingDetail

        }, animationToNext: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }, animationToPrevious: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }))
    }

    override func addShoppingListFilteredRoute() {

        self.addRoute(route: ShoppingListFilteredViewController.ID, routeObject: Route(viewController: { () -> UIViewController in

            let viewShoppingListFiltered = ShoppingListFilteredViewControllerB(nibName: "ShoppingListFilteredViewControllerB", bundle: nil)
            let presenterShoppingListFiltered = ShoppingListFilteredPresenterB<ShoppingListFilteredViewControllerB>()
            viewShoppingListFiltered.presenter = presenterShoppingListFiltered
            presenterShoppingListFiltered.interactor = ShoppingListFilteredInteractor()
            presenterShoppingListFiltered.view = viewShoppingListFiltered
            return viewShoppingListFiltered

        }, animationToNext: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }, animationToPrevious: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }))

    }
    func addFavoriteCategoriesRoute() {

        self.addRoute(route: SelectFavoriteCategoryViewController.ID, routeObject: Route(viewController: { () -> UIViewController in

            let viewFavoriteCategoriesVC = UIStoryboard(name: "SelectFavoriteCategoryViewController", bundle: nil).instantiateInitialViewController()
            guard let viewFavoriteCategories = viewFavoriteCategoriesVC as? SelectFavoriteCategoryViewController else {
                return UIViewController()
            }
           let presenterFavoriteCategories = SelectFavoriteCategoryPresenter<SelectFavoriteCategoryViewController>()

            viewFavoriteCategories.presenter = presenterFavoriteCategories
            presenterFavoriteCategories.view = viewFavoriteCategories

            return viewFavoriteCategories

        }, animationToNext: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }, animationToPrevious: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }))

    }

    func addRewardsRoute() {

        self.addRoute(route: RewardsViewController.ID, routeObject: Route(viewController: { () -> UIViewController in

            let viewRewards = RewardsViewController(nibName: "RewardsViewController", bundle: nil)
            let presenterRewards = RewardsPresenter<RewardsViewController>()
            viewRewards.presenter = presenterRewards
            presenterRewards.view = viewRewards
            viewRewards.tabBarItem = UITabBarItem(title: Localizables.common.key_tabbar_points_button, image: ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Promotions.menu_promotion_icon, color: .GRAYTABBAR), width: 18, height: 18), tag: 4)
            viewRewards.tabBarItem.setTitleTextAttributes([NSAttributedStringKey.font: Tools.setFontMedium(size: 10)], for: .normal)
            viewRewards.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -3)
            return viewRewards

        }, animationToNext: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }, animationToPrevious: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)

        }))
    }
    
    func addShoppingStoresListRoute() {
        self.addRoute(route: StoreListViewController.ID, routeObject: Route(viewController: { () -> UIViewController in
            
            let viewStoresList = StoreListViewController(nibName: "StoreListViewController", bundle: nil)
            
            let presenterStoresList = StoreListPresenter<StoreListViewController>()
            
            viewStoresList.presenter = presenterStoresList
            
            presenterStoresList.view = viewStoresList
            
            return viewStoresList
            
        }, animationToNext: { _, destinationViewController in
            
            self.animation(viewController: destinationViewController!)
            
        }, animationToPrevious: { _, destinationViewController in
            
            self.animation(viewController: destinationViewController!)
            
        }))
    }
    
    func addShoppingStoresMapRoute() {
        self.addRoute(route: StoresMapViewController.ID, routeObject: Route(viewController: { () -> UIViewController in
            
            let storyboardInitialViewController = UIStoryboard(name: "StoresMap", bundle: nil).instantiateInitialViewController()
            guard let viewStoresMap = storyboardInitialViewController as? StoresMapViewController else {
                return UIViewController()
            }
            
            let presenterStoresList = StoresMapPresenter<StoresMapViewController>()
            
            viewStoresMap.presenter = presenterStoresList
            
            presenterStoresList.view = viewStoresMap
            
            return viewStoresMap
            
        }, animationToNext: { _, destinationViewController in
            
            self.animation(viewController: destinationViewController!)
            
        }, animationToPrevious: { _, destinationViewController in
            
            self.animation(viewController: destinationViewController!)
            
        }))
    }

    func addActivateCardByCVVdRoute() {

        self.addRoute(route: CardActivationByCVVViewController.id, routeObject: Route(viewController: { () -> UIViewController in

            let view = CardActivationByCVVViewController(nibName: "WalletCellsViewController", bundle: nil)
            let presenter = CardActivationByCVVPresenter<CardActivationByCVVViewController>()
            view.presenter = presenter
            presenter.view = view
            view.hidesBottomBarWhenPushed = true
            return view

        }, animationToNext: { originViewController, destinationViewController  in

            self.animation(viewController: destinationViewController!)

        }, animationToPrevious: { originViewController, _ in

            self.animation(viewController: originViewController)

        }))

    }
    
    func addUserBloquedRoute() {
        
        self.addRoute(route: UserBlockedViewController.id, routeObject: Route(viewController: { () -> UIViewController in
            
            let view = UserBlockedViewController(nibName: "WalletCellsViewController", bundle: nil)
            let presenter = UserBlockedPresenter<UserBlockedViewController>()
            view.presenter = presenter
            presenter.view = view
            view.hidesBottomBarWhenPushed = true
            return view
            
        }, animationToNext: { originViewController, destinationViewController  in
            
            self.animation(viewController: destinationViewController!)
            
        }, animationToPrevious: { originViewController, _ in
            
            self.animation(viewController: originViewController)
            
        }))
    }
    
    func addMobileTokenRoute() {
        self.addRoute(route: MobileTokenViewController.id, routeObject: Route(viewController: { () -> UIViewController in
            
            let view = MobileTokenViewController(nibName: "WalletCellsViewController", bundle: nil)
            let presenter = MobileTokenPresenter<MobileTokenViewController>()
            view.presenter = presenter
            presenter.view = view
            view.hidesBottomBarWhenPushed = true
            return view
            
        }, animationToNext: { _, destinationViewController in
            
            self.animation(viewController: destinationViewController!)
            
        }, animationToPrevious: { _, destinationViewController in
            
            self.animation(viewController: destinationViewController!)
            
        }))
    }
    
    func addSoftokenSecurityRoute() {
        
        self.addRoute(route: SoftokenSecurityViewController.ID, routeObject: Route(viewController: { () -> UIViewController in
            
            let storyboardInitialViewController = UIStoryboard(name: "SoftokenSecurityViewController", bundle: nil).instantiateInitialViewController()
            guard let viewSoftoken = storyboardInitialViewController as? SoftokenSecurityViewController else {
                return UIViewController()
            }
            
            let presenterSoftoken = SoftokenSecurityPresenter<SoftokenSecurityViewController>()
            viewSoftoken.presenter = presenterSoftoken
            presenterSoftoken.view = viewSoftoken
            return viewSoftoken
            
        }, animationToNext: { _, destinationViewController in
            
            self.animation(viewController: destinationViewController!)
            
        }, animationToPrevious: { _, destinationViewController in
            
            self.animation(viewController: destinationViewController!)
        }))
    }

    override func addAnonymousHomeRoute() {

        addRoute(route: AnonymousTutorialViewController.HomeID, routeObject: Route(viewController: { () -> UIViewController in

            let storyboardInitialViewController = UIStoryboard(name: "AnonymousTutorialViewController", bundle: nil).instantiateInitialViewController()
            guard let anonymousTutorialViewController = storyboardInitialViewController as? AnonymousTutorialViewController else {
                return UIViewController()
            }

            let displayData = AnonymousTutorialDisplayData(navigationBarTitle: Localizables.common.key_tabbar_start_button,
                                                           animationName: ConstantsLottieAnimations.anonymousHome,
                                                           title: Localizables.login.key_dashboard_welcome,
                                                           subtitle: Localizables.login.key_dashboard_description)

            let anonymousTutorialPresenter = AnonymousTutorialPresenterB<AnonymousTutorialViewController>(displayData: displayData)

            anonymousTutorialViewController.presenter = anonymousTutorialPresenter
            anonymousTutorialPresenter.view = anonymousTutorialViewController

            anonymousTutorialViewController.tabBarItem = UITabBarItem(title: Localizables.common.key_tabbar_start_button, image: ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Home.wallet_icon, color: .GRAYTABBAR), width: 18, height: 18), tag: 1)
            anonymousTutorialViewController.tabBarItem.setTitleTextAttributes([NSAttributedStringKey.font: Tools.setFontMedium(size: 10)], for: .normal)
            anonymousTutorialViewController.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -3)

            return anonymousTutorialViewController

        }, animationToNext: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)
        }, animationToPrevious: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)
        }))
    }

    override func addAnonymousCardsRoute() {

        addRoute(route: AnonymousTutorialViewController.CardsID, routeObject: Route(viewController: { () -> UIViewController in

            let storyboardInitialViewController = UIStoryboard(name: "AnonymousTutorialViewController", bundle: nil).instantiateInitialViewController()
            guard let anonymousTutorialViewController = storyboardInitialViewController as? AnonymousTutorialViewController else {
                return UIViewController()
            }

            let displayData = AnonymousTutorialDisplayData(navigationBarTitle: Localizables.common.key_tabbar_cards_button,
                                                           animationName: ConstantsLottieAnimations.anonymousCards,
                                                           title: Localizables.login.key_dashboard_pay_with_wallet_title,
                                                           subtitle: Localizables.login.key_dashboard_pay_with_wallet_description)

            let anonymousTutorialPresenter = AnonymousTutorialPresenterB<AnonymousTutorialViewController>(displayData: displayData)

            anonymousTutorialViewController.presenter = anonymousTutorialPresenter
            anonymousTutorialPresenter.view = anonymousTutorialViewController

            anonymousTutorialViewController.tabBarItem = UITabBarItem(title: Localizables.common.key_tabbar_cards_button, image: ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Card.creditcard_icon, color: .GRAYTABBAR), width: 18, height: 18), tag: 2)
            anonymousTutorialViewController.tabBarItem.setTitleTextAttributes([NSAttributedStringKey.font: Tools.setFontMedium(size: 10)], for: .normal)
            anonymousTutorialViewController.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -3)

            return anonymousTutorialViewController

        }, animationToNext: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)
        }, animationToPrevious: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)
        }))
    }

    override func addAnonymousRewardsRoute() {

        addRoute(route: AnonymousTutorialViewController.RewardsID, routeObject: Route(viewController: { () -> UIViewController in

            let storyboardInitialViewController = UIStoryboard(name: "AnonymousTutorialViewController", bundle: nil).instantiateInitialViewController()
            guard let anonymousTutorialViewController = storyboardInitialViewController as? AnonymousTutorialViewController else {
                return UIViewController()
            }

            let displayData = AnonymousTutorialDisplayData(navigationBarTitle: Localizables.points.key_points_text,
                                                           animationName: ConstantsLottieAnimations.anonymousPoints,
                                                           title: Localizables.login.key_dashboard_get_points_title,
                                                           subtitle: Localizables.login.key_dashboard_get_points_description)

            let anonymousTutorialPresenter = AnonymousTutorialPresenterB<AnonymousTutorialViewController>(displayData: displayData)

            anonymousTutorialViewController.presenter = anonymousTutorialPresenter
            anonymousTutorialPresenter.view = anonymousTutorialViewController

            anonymousTutorialViewController.tabBarItem = UITabBarItem(title: Localizables.common.key_tabbar_points_button, image: ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Promotions.menu_promotion_icon, color: .GRAYTABBAR), width: 18, height: 18), tag: 4)
            anonymousTutorialViewController.tabBarItem.setTitleTextAttributes([NSAttributedStringKey.font: Tools.setFontMedium(size: 10)], for: .normal)
            anonymousTutorialViewController.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -3)

            return anonymousTutorialViewController

        }, animationToNext: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)
        }, animationToPrevious: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)
        }))
    }

    override func addAnonymousNotificationsRoute() {

        addRoute(route: AnonymousTutorialViewController.NotificationsID, routeObject: Route(viewController: { () -> UIViewController in

            let storyboardInitialViewController = UIStoryboard(name: "AnonymousTutorialViewController", bundle: nil).instantiateInitialViewController()
            guard let anonymousTutorialViewController = storyboardInitialViewController as? AnonymousTutorialViewController else {
                return UIViewController()
            }

            let displayData = AnonymousTutorialDisplayData(navigationBarTitle: Localizables.common.key_tabbar_notice_button,
                                                           animationName: ConstantsLottieAnimations.anonymousNotifications,
                                                           title: Localizables.login.key_dashboard_stay_informed_title,
                                                           subtitle: Localizables.login.key_dashboard_stay_informed_description)

            let anonymousTutorialPresenter = AnonymousTutorialPresenterB<AnonymousTutorialViewController>(displayData: displayData)

            anonymousTutorialViewController.presenter = anonymousTutorialPresenter
            anonymousTutorialPresenter.view = anonymousTutorialViewController

            anonymousTutorialViewController.tabBarItem = UITabBarItem(title: Localizables.common.key_tabbar_notice_button, image: ImageUtilities.image(with: SVGCache.image(forSVGNamed: ConstantsImages.Notifications.alarm_icon, color: .GRAYTABBAR), width: 18, height: 18), tag: 5)
            anonymousTutorialViewController.tabBarItem.setTitleTextAttributes([NSAttributedStringKey.font: Tools.setFontMedium(size: 10)], for: .normal)
            anonymousTutorialViewController.tabBarItem.titlePositionAdjustment = UIOffset(horizontal: 0, vertical: -3)

            return anonymousTutorialViewController

        }, animationToNext: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)
        }, animationToPrevious: { _, destinationViewController in

            self.animation(viewController: destinationViewController!)
        }))
    }
}
