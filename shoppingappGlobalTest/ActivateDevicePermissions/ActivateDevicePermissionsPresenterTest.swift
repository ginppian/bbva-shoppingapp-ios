//
//  ActivateDevicePermissionsPresenterTest.swift
//  shoppingapp
//
//  Created by Marcos on 20/9/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import XCTest
import CellsNativeComponents

#if TESTMX
@testable import shoppingappMX
#elseif TESTPE
@testable import shoppingappPE
#endif

class ActivateDevicePermissionsPresenterTest: XCTestCase {

    private var sut: ActivateDevicePermissionsPresenter<ActivateDevicePermissionsDummyView>!
    private var dummyView = ActivateDevicePermissionsDummyView()
    private var dummyPreferencesManager: DummyPreferencesManager!
    private var dummyBusManager: DummyBusManager!
    private var dummyNotificationManager = DummyNotificationManager()

    override func setUp() {
        super.setUp()

        dummyBusManager = DummyBusManager()

        sut = ActivateDevicePermissionsPresenter<ActivateDevicePermissionsDummyView>(busManager: dummyBusManager)
        sut.view = dummyView
        dummyPreferencesManager = DummyPreferencesManager()
        PreferencesManager.instance = dummyPreferencesManager
        
        dummyNotificationManager = DummyNotificationManager()
        sut.notificationManager = dummyNotificationManager
    }

    // MARK: - showRequestNotificationAutorization
    
    func test_givenAny_whenICallShowRequestNotificationAutorization_thenICallSaveInPreferencesManager() {
        
        //given
        
        //when
        sut.showRequestNotificationAutorization()
        
        //then
        XCTAssert(dummyPreferencesManager.isCalledSaveValueWithPreferencesKey == true)
        XCTAssert(dummyPreferencesManager.userClosedNotificationAdvise != nil && dummyPreferencesManager.userClosedNotificationAdvise == false)
    }
    
    func test_givenAny_whenICallShowRequestNotificationAutorization_thenICallBusManagerDissmiss() {
        
        //given
        
        //when
        sut.showRequestNotificationAutorization()

        //then
        XCTAssert(dummyBusManager.isCalledDismissViewController == true)
    }
    
    func test_givenAny_whenICallShowRequestNotificationAutorization_thenICallBusManagerNotifyEventCloseCellStak() {
        
        //given
        
        //when
        sut.showRequestNotificationAutorization()

        //then
        XCTAssert(dummyBusManager.isCalledNotifyEventCloseCellStak)
    }
    
    func test_givenNotificationsGrantedFalse_whenICallShowRequestNotificationAutorization_thenICallViewShowToastRejectedNotifications() {
        
        //given
        dummyNotificationManager.notificationsIsGranted = false
        
        //when
        sut.showRequestNotificationAutorization()

        //then
        XCTAssert(dummyView.isCalledShowToastRejectedNotifications == true)
    }
    
    func test_givenNotificationsGrantedTrue_whenICallShowRequestNotificationAutorization_thenINotCallViewShowToastRejectedNotifications() {
        
        //given
        dummyNotificationManager.notificationsIsGranted = true
        
        //when
        sut.showRequestNotificationAutorization()

        //then
        XCTAssert(dummyView.isCalledShowToastRejectedNotifications == false)
    }
    
    func test_givenNotificationsGrantedTrue_whenICallShowRequestNotificationAutorization_thenICallViewShowToastAceptedNotifications() {
        
        //given
        dummyNotificationManager.notificationsIsGranted = true

        //when
        sut.showRequestNotificationAutorization()

        //then
        XCTAssert(dummyView.isCalledShowToastAceptedNotifications == true)
    }
    
    func test_givenNotificationsGrantedFalse_whenICallShowRequestNotificationAutorization_thenINotCallViewShowToastAceptedNotifications() {
        
        //given
        dummyNotificationManager.notificationsIsGranted = false

        //when
        sut.showRequestNotificationAutorization()

        //then
        XCTAssert(dummyView.isCalledShowToastAceptedNotifications == false)
    }

    // MARK: - userClosedNotificationAdvise
    
    func test_givenAny_whenITapInCloseNotificationAdvise_thenICallSaveInPreferencesManager() {

        //given

        //when
        sut.userClosedNotificationAdvise()

        //then
        XCTAssert(dummyPreferencesManager.isCalledSaveValueWithPreferencesKey == true)
        XCTAssert(dummyPreferencesManager.userClosedNotificationAdvise != nil && dummyPreferencesManager.userClosedNotificationAdvise == true)

    }

    func test_givenAny_whenITapInCloseNotificationAdvise_thenICallBusManagerNotifyEventCloseCellStak() {

        //given

        //when
        sut.userClosedNotificationAdvise()

        //then
        XCTAssert(dummyBusManager.isCalledNotifyEventCloseCellStak)
    }

    func test_givenAny_whenITapInCloseNotificationAdvise_thenICallBusManagerDissmiss() {

        //given

        //when
        sut.userClosedNotificationAdvise()

        //then
        XCTAssert(dummyBusManager.isCalledDismissViewController == true)
    }

    // MARK: - didPermissionsChanged
    
    func test_givenAny_whenICallDidPermissionsChanged_thenICallBusManagerDissmiss() {

        //given

        //when
        sut.didPermissionsChanged(granted: true)

        //then
        XCTAssert(dummyBusManager.isCalledDismissViewController == true)
    }

    func test_givenAny_whenICallDidPermissionsChanged_thenICallBusManagerNotifyEventCloseCellStak() {

        //given

        //when
        sut.didPermissionsChanged(granted: true)

        //then
        XCTAssert(dummyBusManager.isCalledNotifyEventCloseCellStak)
    }

    func test_givenFalse_whenICallDidPermissionsChanged_thenICallViewShowToastRejectedNotifications() {

        //given

        //when
        sut.didPermissionsChanged(granted: false)

        //then
        XCTAssert(dummyView.isCalledShowToastRejectedNotifications == true)
    }

    func test_givenTrue_whenICallDidPermissionsChanged_thenINotCallViewShowToastRejectedNotifications() {

        //given

        //when
        sut.didPermissionsChanged(granted: true)

        //then
        XCTAssert(dummyView.isCalledShowToastRejectedNotifications == false)
    }

    func test_givenTrue_whenICallDidPermissionsChanged_thenICallViewShowToastAceptedNotifications() {

        //given

        //when
        sut.didPermissionsChanged(granted: true)

        //then
        XCTAssert(dummyView.isCalledShowToastAceptedNotifications == true)
    }

    func test_givenFalse_whenICallDidPermissionsChanged_thenINotCallViewShowToastAceptedNotifications() {

        //given

        //when
        sut.didPermissionsChanged(granted: false)

        //then
        XCTAssert(dummyView.isCalledShowToastAceptedNotifications == false)
    }
    
    // MARK: - configurePermissionsAdviseClosed
    
    func test_givenAny_whenICallConfigurePermissionsAdviseClosed_thenICallNotificationManagerIsAuthorizedStatus() {
        
        //given
        
        //when
        sut.configurePermissionsAdviseClosed()
        
        //then
        XCTAssert(dummyNotificationManager.isCalledIsAuthorizedStatus == true)
    }
    
    func test_givenNotificationsAuthorizedFalse_whenICallConfigurePermissionsAdviseClosed_thenICallBusManagerDismissViewController() {
        
        //given
        dummyNotificationManager.notificationsIsAuthorized = [false]
        
        //when
        sut.configurePermissionsAdviseClosed()
        
        //then
        wait(for: [dummyBusManager.expectationDismissViewController], timeout: 10)
        XCTAssert(dummyBusManager.isCalledDismissViewController == true)
    }
    
    func test_givenNotificationsAuthorizedFalse_whenICallConfigurePermissionsAdviseClosed_thenICallBusManagerNotifyEventCloseCellStack() {
        
        //given
        dummyNotificationManager.notificationsIsAuthorized = [false]
        
        //when
        sut.configurePermissionsAdviseClosed()
        
        //then
        wait(for: [dummyBusManager.expectationNotify], timeout: 10)
        XCTAssert(dummyBusManager.isCalledNotifyEventCloseCellStak == true)
    }
    
    func test_givenNotificationsAuthorizedTrue_whenICallConfigurePermissionsAdviseClosed_thenICallBusManagerDismissViewController() {
        
        //given
        dummyNotificationManager.notificationsIsAuthorized = [true]
        
        //when
        sut.configurePermissionsAdviseClosed()
        
        //then
        wait(for: [dummyBusManager.expectationDismissViewController], timeout: 10)
        XCTAssert(dummyBusManager.isCalledDismissViewController == true)
    }
    
    func test_givenNotificationsAuthorizedTrue_whenICallConfigurePermissionsAdviseClosed_thenICallBusManagerNotifyEventCloseCellStack() {
        
        //given
        dummyNotificationManager.notificationsIsAuthorized = [true]
        
        //when
        sut.configurePermissionsAdviseClosed()
        
        //then
        wait(for: [dummyBusManager.expectationNotify], timeout: 10)
        XCTAssert(dummyBusManager.isCalledNotifyEventCloseCellStak == true)
    }
    
    func test_givenNotificationsAuthorizedTrue_whenICallConfigurePermissionsAdviseClosed_thenICallPreferencesManagerGetValueForKeyApnsToken() {
        
        //given
        dummyNotificationManager.notificationsIsAuthorized = [true]
        
        //when
        sut.configurePermissionsAdviseClosed()
        
        //then
        XCTAssert(dummyPreferencesManager.isCalledGetValueWithPreferencesKey == true)
        XCTAssert(dummyPreferencesManager.keySentGetValue == PreferencesManagerKeys.kApnsToken.rawValue)
    }
    
    func test_givenNotificationsAuthorizedTrueAndApnsTokenSaved_whenICallConfigurePermissionsAdviseClosed_thenICallViewRegisterDeviceOnNotificationsService() {
        
        //given
        dummyNotificationManager.notificationsIsAuthorized = [true]
        dummyPreferencesManager.apnsTokenSaved = "AAA"
        
        //when
        sut.configurePermissionsAdviseClosed()
        
        //then
        XCTAssert(dummyView.isCalledRegisterDeviceOnNotificationsService == true)
    }
    
    // MARK: - configurePermissionsAdviseButtonPressed
    
    func test_givenAny_whenICallConfigurePermissionsAdviseButtonPressed_thenICallNotificationManagerIsAuthorizedStatus() {
        
        //given
        
        //when
        sut.configurePermissionsAdviseButtonPressed()
        
        //then
        XCTAssert(dummyNotificationManager.isCalledIsAuthorizedStatus == true)
    }
    
    func test_givenNotificationsAuthorizedFalse_whenICallConfigurePermissionsAdviseButtonPressed_thenICallViewOpenAppSettings() {
        
        //given
        dummyNotificationManager.notificationsIsAuthorized = [false]
        
        //when
        sut.configurePermissionsAdviseButtonPressed()
        
        //then
        XCTAssert(dummyView.isCalledOpenAppSettings == true)
    }
    
    func test_givenNotificationsAuthorizedTrue_whenICallConfigurePermissionsAdviseButtonPressed_thenICallBusManagerDismissViewController() {
        
        //given
        dummyNotificationManager.notificationsIsAuthorized = [true]
        
        //when
        sut.configurePermissionsAdviseButtonPressed()
        
        //then
        wait(for: [dummyBusManager.expectationDismissViewController], timeout: 10)
        XCTAssert(dummyBusManager.isCalledDismissViewController == true)
    }
    
    func test_givenNotificationsAuthorizedTrue_whenICallConfigurePermissionsAdviseButtonPressed_thenICallBusManagerNotifyEventCloseCellStack() {
        
        //given
        dummyNotificationManager.notificationsIsAuthorized = [true]
        
        //when
        sut.configurePermissionsAdviseButtonPressed()
        
        //then
        wait(for: [dummyBusManager.expectationNotify], timeout: 10)
        XCTAssert(dummyBusManager.isCalledNotifyEventCloseCellStak == true)
    }
    
    func test_givenNotificationsAuthorizedTrue_whenICallConfigurePermissionsAdviseButtonPressed_thenICallPreferencesManagerGetValueForKeyApnsToken() {
        
        //given
        dummyNotificationManager.notificationsIsAuthorized = [true]
        
        //when
        sut.configurePermissionsAdviseButtonPressed()
        
        //then
        XCTAssert(dummyPreferencesManager.isCalledGetValueWithPreferencesKey == true)
        XCTAssert(dummyPreferencesManager.keySentGetValue == PreferencesManagerKeys.kApnsToken.rawValue)
    }
    
    func test_givenNotificationsAuthorizedTrueAndApnsTokenSaved_whenICallConfigurePermissionsAdviseButtonPressed_thenICallViewRegisterDeviceOnNotificationsService() {
        
        //given
        dummyNotificationManager.notificationsIsAuthorized = [true]
        dummyPreferencesManager.apnsTokenSaved = "AAA"
        
        //when
        sut.configurePermissionsAdviseButtonPressed()
        
        //then
        XCTAssert(dummyView.isCalledRegisterDeviceOnNotificationsService == true)
    }
    
    // MARK: - applicationDidBecomeActive
    
    func test_givenAny_whenICallApplicationDidBecomeActive_thenICallNotificationManagerIsAuthorizedStatus() {
        
        //given
        
        //when
        sut.applicationDidBecomeActive()
        
        //then
        XCTAssert(dummyNotificationManager.isCalledIsAuthorizedStatus == true)
    }
    
    func test_givenNotificationsAuthorizedFalse_whenICallApplicationDidBecomeActive_thenICallBusManagerDismissViewController() {
        
        //given
        dummyNotificationManager.notificationsIsAuthorized = [false]
        
        //when
        sut.applicationDidBecomeActive()
        
        //then
        wait(for: [dummyBusManager.expectationDismissViewController], timeout: 10)
        XCTAssert(dummyBusManager.isCalledDismissViewController == true)
    }
    
    func test_givenNotificationsAuthorizedFalse_whenICallApplicationDidBecomeActive_thenICallBusManagerNotifyEventCloseCellStack() {
        
        //given
        dummyNotificationManager.notificationsIsAuthorized = [false]
        
        //when
        sut.applicationDidBecomeActive()
        
        //then
        wait(for: [dummyBusManager.expectationNotify], timeout: 10)
        XCTAssert(dummyBusManager.isCalledNotifyEventCloseCellStak == true)
    }
    
    func test_givenNotificationsAuthorizedFalse_whenICallApplicationDidBecomeActive_thenICallViewShowToastRejectedNotifications() {
        
        //given
        dummyNotificationManager.notificationsIsAuthorized = [false]
        
        //when
        sut.applicationDidBecomeActive()
        
        //then
        XCTAssert(dummyView.isCalledShowToastRejectedNotifications == true)
    }
    
    func test_givenNotificationsAuthorizedTrue_whenICallApplicationDidBecomeActive_thenICallBusManagerDismissViewController() {
        
        //given
        dummyNotificationManager.notificationsIsAuthorized = [true]
        
        //when
        sut.applicationDidBecomeActive()
        
        //then
        wait(for: [dummyBusManager.expectationDismissViewController], timeout: 10)
        XCTAssert(dummyBusManager.isCalledDismissViewController == true)
    }
    
    func test_givenNotificationsAuthorizedTrue_whenICallApplicationDidBecomeActive_thenICallBusManagerNotifyEventCloseCellStack() {
        
        //given
        dummyNotificationManager.notificationsIsAuthorized = [true]
        
        //when
        sut.applicationDidBecomeActive()
        
        //then
        wait(for: [dummyBusManager.expectationNotify], timeout: 10)
        XCTAssert(dummyBusManager.isCalledNotifyEventCloseCellStak == true)
    }
    
    func test_givenNotificationsAuthorizedTrue_whenICallApplicationDidBecomeActive_thenICallViewShowToastAceptedNotifications() {
        
        //given
        dummyNotificationManager.notificationsIsAuthorized = [true]
        
        //when
        sut.applicationDidBecomeActive()
        
        //then
        XCTAssert(dummyView.isCalledShowToastAceptedNotifications == true)
    }
    
    func test_givenNotificationsAuthorizedTrue_whenICallApplicationDidBecomeActive_thenICallPreferencesManagerGetValueForKeyApnsToken() {
        
        //given
        dummyNotificationManager.notificationsIsAuthorized = [true]
        
        //when
        sut.applicationDidBecomeActive()
        
        //then
        XCTAssert(dummyPreferencesManager.isCalledGetValueWithPreferencesKey == true)
        XCTAssert(dummyPreferencesManager.keySentGetValue == PreferencesManagerKeys.kApnsToken.rawValue)
    }
    
    func test_givenNotificationsAuthorizedTrueAndApnsTokenSaved_whenICallApplicationDidBecomeActive_thenICallViewRegisterDeviceOnNotificationsService() {
        
        //given
        dummyNotificationManager.notificationsIsAuthorized = [true]
        dummyPreferencesManager.apnsTokenSaved = "AAA"
        
        //when
        sut.applicationDidBecomeActive()
        
        //then
        XCTAssert(dummyView.isCalledRegisterDeviceOnNotificationsService == true)
    }

    // MARK: - Dummy classes
    class ActivateDevicePermissionsDummyView: ActivateDevicePermissionsViewProtocol {
        
        var isCalledShowToastRejectedNotifications = false
        var isCalledShowToastAceptedNotifications = false
        var isCalledOpenAppSettings = false
        var isCalledRegisterDeviceOnNotificationsService = false

        func showToastRejectedNotifications() {
            isCalledShowToastRejectedNotifications = true
        }

        func showToastAceptedNotifications() {
            isCalledShowToastAceptedNotifications = true
        }
        
        func openAppSettings() {
            isCalledOpenAppSettings = true
        }
        
        func registerDeviceOnNotificationsService(withToken token: String) {
            isCalledRegisterDeviceOnNotificationsService = true
        }

        func showError(error: ModelBO) {

        }

        func changeColorEditTexts() {

        }
    }

    class DummyPreferencesManager: PreferencesManager {

        var isCalledSaveValueWithPreferencesKey = false
        var isCalledGetValueWithPreferencesKey = false
        var userClosedNotificationAdvise: Bool?
        var keySentGetValue: String?
        var apnsTokenSaved: String?

        override func saveValueWithPreferencesKey(forValue value: AnyObject, withKey key: PreferencesManagerKeys) -> Bool {

            isCalledSaveValueWithPreferencesKey = true

            switch key {
            case PreferencesManagerKeys.kUserCloseNotificationsAdvise:
                userClosedNotificationAdvise = (value as? Bool)!

            default:
                break
            }

            return true
        }
        
        override func getValueWithPreferencesKey(forKey key: PreferencesManagerKeys) -> AnyObject? {
            
            isCalledGetValueWithPreferencesKey = true
            keySentGetValue = key.rawValue
            
            switch key {
            case PreferencesManagerKeys.kApnsToken:
                
                if let apnsToken = apnsTokenSaved {
                    
                    return apnsToken as AnyObject
                }
            default:
                break
            }
            
            return nil
        }
    }

    class DummyBusManager: BusManager {

        var isCalledDismissViewController = false
        var isCalledNotifyEventCloseCellStak = false
        let expectationDismissViewController = XCTestExpectation(description: "dismissViewController")
        let expectationNotify = XCTestExpectation(description: "notify")

        override func dismissViewController(animated: Bool, completion: (() -> Void)?) {
            if let completion = completion {
                completion()
            }
            
            isCalledDismissViewController = true
            expectationDismissViewController.fulfill()
        }

        override func notify<T>(tag: String, _ event: ActionSpec<T>) {
            if tag == HomePageReaction.ROUTER_TAG && event === HomePageReaction.EVENT_CLOSE_CELL_STACK {
                isCalledNotifyEventCloseCellStak = true
            }
            expectationNotify.fulfill()
        }
    }
}
