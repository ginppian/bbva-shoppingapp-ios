//
//  PickerPresenterTest.swift
//  shoppingapp
//
//  Created by Javier Pino on 24/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import XCTest

#if TESTPE
    @testable import shoppingappPE
#endif

class PickerPresenterTest: XCTestCase {

    var sut: PickerPresenter<DummyView>!

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.

        sut = PickerPresenter<DummyView>()

    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func test_givenAnyValue_whenICallViewDidLoad_thenProvideIdentificationDocumentsIsCalled() {

        //given
        let dummyView = DummyView()
        sut.view = dummyView
        let titles = ["1", "2"]
        let pickerMock = PickerOptionsDisplay()
        pickerMock.titles = titles
        sut.options = pickerMock

        //when
        sut.viewDidLoad()

        //then
        XCTAssert(  (dummyView.isCalledConfigureView == true) &&
                    (dummyView.isCalledShowPickerOptions == true))
    }

    func test_givenAnyIndex_whenICallDidSelectRow_thenDidSelectPickerOptionIsCalled() {

        //given
        let dummyView = DummyView()
        sut.view = dummyView
        let titles = ["1", "2"]
        let pickerMock = PickerOptionsDisplay()
        pickerMock.titles = titles
        sut.options = pickerMock
        let index: Int = 1
        let delegateMock = DelegateDummy()
        sut.delegate = delegateMock

        //when
        sut.didSelectRow(atIndex: index)

        //then
        XCTAssert(delegateMock.isCalledDidSelectPickerOption == true)
    }

    func test_givenAnyIndex_whenICallDidSelectRow_thenDidSelectPickerOptionIsCalledWithSameIndex() {

        //given
        let dummyView = DummyView()
        sut.view = dummyView
        let titles = ["1", "2"]
        let pickerMock = PickerOptionsDisplay()
        pickerMock.titles = titles
        sut.options = pickerMock
        let index: Int = 1
        let delegateMock = DelegateDummy()
        sut.delegate = delegateMock

        //when
        sut.didSelectRow(atIndex: index)

        //then
        XCTAssert(delegateMock.indexSelected == index)
    }

    func test_givenAnyIndex_whenICallDidSelectRow_thenViewDismissIsCalled() {

        //given
        let dummyView = DummyView()
        sut.view = dummyView
        let titles = ["1", "2"]
        let pickerMock = PickerOptionsDisplay()
        pickerMock.titles = titles
        sut.options = pickerMock
        let index: Int = 1
        let delegateMock = DelegateDummy()
        sut.delegate = delegateMock

        //when
        sut.didSelectRow(atIndex: index)

        //then
        XCTAssert(dummyView.isCalledDismiss == true)
    }

    class DummyView: PickerViewProtocol {

        var isCalledConfigureView = false
        var isCalledShowPickerOptions = false
        var isCalledDismiss = false

        func configureView() {
            isCalledConfigureView = true
        }

        func showPickerOptions(options: PickerOptionsDisplay) {
            isCalledShowPickerOptions = true
        }

        func dismiss() {
            isCalledDismiss = true
        }

        func showError(error: ModelBO) {
        }

        func changeColorEditTexts() {
        }
    }

    class DelegateDummy: PickerPresenterDelegate {

        var isCalledDidSelectPickerOption = false

        var indexSelected: Int?

        func didSelectPickerOption(atIndex index: Int) {
            isCalledDidSelectPickerOption = true
            indexSelected = index
        }
    }
}
