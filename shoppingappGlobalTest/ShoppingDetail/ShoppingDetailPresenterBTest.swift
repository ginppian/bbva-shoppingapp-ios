//
//  ShoppingDetailPresenterBTest.swift
//  shoppingapp
//
//  Created by Rubén Jacobo on 26/09/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest
import RxSwift
import CellsNativeComponents
import CoreLocation

#if TESTMX
@testable import shoppingappMX
#endif

class ShoppingDetailPresenterBTest: XCTestCase {

    var sut: ShoppingDetailPresenterB<DummyView>!
    var dummyBusManager: DummyBusManager!
    var dummyLocationManager = DummyLocationManager()
    var dummyView: DummyView!
    var dummyInteractor: DummyInteractor!
    
    override func setUp() {
        
        super.setUp()
        DummyPreferencesManager.configureDummyPreferences()
        dummyBusManager = DummyBusManager()
        sut = ShoppingDetailPresenterB<DummyView>(busManager: dummyBusManager)
        
        dummyLocationManager = DummyLocationManager()
        sut.locationManager = dummyLocationManager
        
        dummyView = DummyView()
        sut.view = dummyView
        
        dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
    }
    
    // MARK: - receive model
    
    func test_givenCardsBO_whenICallReceive_thenCardsShouldMatchWithCardsBO() {
        
        // given
        let cardsBO = CardsGenerator.getThreeCards()
        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(sut.cards === cardsBO )
        
    }

    // MARK: - ViewDidLoad
    
    // MARK: - ViewDidLoad - isCalledGetLocation
    
    func test_givenAPromotion_whenICallViewDidLoad_thenCallLocationManagerGetLocation() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyLocationManager.isCalledGetLocation == true)
    }
    
    func test_givenAPromotion_whenICallViewDidLoadAndHaveSummaryDescription_thenNotCallLocationManagerGetLocation() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.displayItemPromoDetail = DisplayItemPromotionDetail.generateDisplayItemPromotionDetail(fromPromotion: PromotionsGenerator.getPromotionDetail())
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyLocationManager.isCalledGetLocation == false)
    }
    
    // MARK: - ViewDidLoad - isCalledShowSkeleton
    
    func test_givenAny_whenViewLoad_thenICallShowSkeleton() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        let dummyView = DummyView()
        sut.view = dummyView
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowSkeleton == true)
    }
    
    // MARK: - ViewDidLoad - isCalledShowPromo
    
    func test_givenAPromotion_whenICallViewDidLoad_thenCallViewShowPromo() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowPromo == true)
    }
    
    // MARK: - ViewDidLoad - displayItemPromo
    
    func test_givenAPromotionBO_whenICallViewDidLoad_thenCreateDisplayItem() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(sut.displayItemPromo.commerceName == sut.promotionBO?.commerceInformation.name.uppercased())
        XCTAssert(sut.displayItemPromo.descriptionPromotion == sut.promotionBO?.name)
        XCTAssert(sut.displayItemPromo.benefit == sut.promotionBO?.benefit)
        XCTAssert(sut.displayItemPromo.promotionBO == sut.promotionBO)
        XCTAssert(sut.displayItemPromo.imageDetail == sut.promotionBO?.imageDetail)
        XCTAssert(sut.displayItemPromo.imageList == sut.promotionBO?.imageList)
        XCTAssert(sut.displayItemPromo.imagePush == sut.promotionBO?.imagePush)
    }
    
    func test_givenAPromotionBO_whenICallViewDidLoad_thenDisplayItemPromoMatch() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        let location = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)
        sut.userLocation = location
        let displayItemPromo = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: sut.promotionBO!, userLocation: location)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(sut.displayItemPromo == displayItemPromo)
    }
    
    func test_givenPromotionBOWithNoImageDetailSaved_whenICallViewDidLoadAndInteractorProvideImage_thenIHaveImageDetailSavedWithInteractorImage() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(sut.displayItemPromo.promotionBO?.imageDetailSaved == dummyInteractor.imagePromo)
    }
    
    // MARK: - ViewDidLoad - isCalledShowPromoDetail
    
    func test_givenAPromotion_whenICallViewDidLoadAndDisplayItemPromoDetailIsFilled_thenICallViewShowPromoDetail() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        sut.displayItemPromoDetail = DisplayItemPromotionDetail.generateDisplayItemPromotionDetail(fromPromotion: promotionBO)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowPromoDetail == true)
    }
    
    func test_givenAPromotionBOAndUserLocationAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallViewDidLoad_thenCallViewShowPromoDetail() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowPromoDetail == true)
    }
    
    func test_givenAPromotionBOAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallViewDidLoadAndLocationManagerGPSFail_thenCallViewShowPromoDetail() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowPromoDetail == true)
    }
    
    // MARK: - ViewDidLoad - isCalledShowPromoStore
    
    func test_givenAPromotionAndUserLocation_whenICallViewDidLoadAndHaveCommerceDescriptionAndWebPage_thenCallViewShowPromoStore() {
        
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        
        sut.view = dummyView
        sut.interactor = dummyInteractor
        
        // given
        sut.promotionBO = promotionBO
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowPromoStore == true)
    }
    
    func test_givenAPromotion_whenICallViewDidLoadAndHaveCommerceDescriptionAndWebPageAndLocationManagerGPSFail_thenCallViewShowPromoStore() {
        
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        
        sut.view = dummyView
        sut.interactor = dummyInteractor
        
        // given
        sut.promotionBO = promotionBO
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowPromoStore == true)
    }
    
    func test_givenAPromotionAndUserLocation_whenICallViewDidLoadAndHaveCommerceDescriptionAndNotWebPage_thenCallViewShowPromoStore() {
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceNoWebPage = true
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowPromoStore == true)
    }
    
    func test_givenAPromotion_whenICallViewDidLoadAndHaveCommerceDescriptionAndNotWebPageAndLocationManagerGPSFail_thenCallViewShowPromoStore() {
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceNoWebPage = true
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowPromoStore == true)
    }
    
    func test_givenAPromotionAndUserLocation_whenICallViewDidLoadAndHaveNotCommerceDescriptionAndWebPage_thenCallViewShowPromoStore() {
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceNotCommerceDescription = true
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowPromoStore == true)
    }
    
    func test_givenAPromotion_whenICallViewDidLoadAndHaveNotCommerceDescriptionAndWebPageAndLocationManagerGPSFail_thenCallViewShowPromoStore() {
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceNotCommerceDescription = true
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowPromoStore == true)
    }
    
    func test_givenAPromotionAndUserLocation_whenICallViewDidLoadNoHaveCommerceDescriptionAndNoWebPageAndNoStores_thenCallViewShowPromoStore() {
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceNoDescriptionAndWeb = true
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowPromoStore == false)
    }
    
    func test_givenAPromotion_whenICallViewDidLoadNoHaveCommerceDescriptionAndNoWebPageAndNoStoresAndLocationManagerGPSFail_thenCallViewShowPromoStore() {
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceNoDescriptionAndWeb = true
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowPromoStore == false)
    }
    
    // MARK: - ViewDidLoad - isCalledHidePromoStore
    
    func test_givenAPromotionAndUserLocation_whenICallViewDidLoadAndNoHaveCommerceDescriptionAndNoWebPageAndNoStores_thenCallViewHidePromoStore() {
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceNoDescriptionAndWeb = true
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetailWithoutDescriptionAndWeb()
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledHidePromoStore == true)
    }
    
    func test_givenAPromotion_whenICallViewDidLoadAndNoHaveCommerceDescriptionAndNoWebPageAndNoStoresAndLocationManagerGPSFail_thenCallViewHidePromoStore() {
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceNoDescriptionAndWeb = true
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetailWithoutDescriptionAndWeb()
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledHidePromoStore == true)
    }
    
    func test_givenAPromotionAndUserLocation_whenICallViewDidLoadAndHaveCommerceDescriptionAndWebPage_thenINotCalledHidePromoStore() {
        
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        
        sut.view = dummyView
        sut.interactor = dummyInteractor
        
        // given
        sut.promotionBO = promotionBO
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledHidePromoStore == false)
    }
    
    func test_givenAPromotion_whenICallViewDidLoadAndHaveCommerceDescriptionAndWebPageAndLocationManagerGPSFail_thenINotCalledHidePromoStore() {
        
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        
        sut.view = dummyView
        sut.interactor = dummyInteractor
        
        // given
        sut.promotionBO = promotionBO
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledHidePromoStore == false)
    }
    
    // MARK: - ViewDidLoad - isCalledProvidePromotionByParams
    
    func test_givenAPromotion_whenICallViewDidLoadAndDisplayItemPromoDetailIsFilledAndLocationManagerGPSFail_thenICallViewShowPromoDetailAndInteractorProvidePromotionByParamsWithThePromotionIdAndDefaultLocationOnShoppingParamsTransport() {
        
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        
        sut.view = dummyView
        sut.interactor = dummyInteractor
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        dummyLocationManager.locationErrorType = .gpsFail
        
        let locationParam = ShoppingParamLocation(geolocation: GeoLocationBO(withLatitude: sut.defaultLocation.latitude, withLongitude: sut.defaultLocation.longitude), distanceParam: ShoppingDistanceParam(distance: sut.defaultDistance, lengthType: sut.lengthType))
        let expands = ShoppingExpands(stores: true, cards: true, contactDetails: true)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionByParams == true)
        XCTAssert(dummyInteractor.paramsSent == ShoppingDetailParamsTransport(idPromo: sut.promotionBO!.id, location: locationParam, expands: expands, titleIds: sut.defaultTitleIds))
    }
    
    func test_givenAPromotionAndUserLocation_whenICallViewDidLoadAndDisplayItemPromoDetailIsFilled_thenICallViewShowPromoDetailAndInteractorProvidePromotionByParamsWithThePromotionIdAndDefaultLocationOnShoppingParamsTransport() {
        
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        
        sut.view = dummyView
        sut.interactor = dummyInteractor
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        let locationParam = ShoppingParamLocation(geolocation: GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0), distanceParam: ShoppingDistanceParam(distance: sut.defaultDistance, lengthType: sut.lengthType))
        let expands = ShoppingExpands(stores: true, cards: true, contactDetails: true)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionByParams == true)
        XCTAssert(dummyInteractor.paramsSent == ShoppingDetailParamsTransport(idPromo: sut.promotionBO!.id, location: locationParam, expands: expands, titleIds: sut.defaultTitleIds))
    }
    
    func test_givenAPromotionBOAndUserLocationAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallViewDidLoad_thenICallProvidePromotionWithStoresByParams() {
        
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        let promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]
        
        sut.view = dummyView
        sut.interactor = dummyInteractor
        
        // given
        sut.promotionBO = promotionBO
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionByParams == true)
    }
    
    func test_givenAPromotionBOAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallViewDidLoadAndLocationManagerGPSFail_thenICallProvidePromotionWithStoresByParams() {
        
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        let promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]
        
        sut.view = dummyView
        sut.interactor = dummyInteractor
        
        // given
        sut.promotionBO = promotionBO
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionByParams == true)
    }
    
    func test_givenAPromotionBOAndDisplayItemPromoDetailSumamaryDescription_whenICallViewDidLoad_thenINotCallProvidePromotionWithStoresByParams() {
        
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        
        sut.view = dummyView
        sut.interactor = dummyInteractor
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        sut.displayItemPromoDetail = DisplayItemPromotionDetail.generateDisplayItemPromotionDetail(fromPromotion: promotionBO)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionByParams == false)
    }
    
    // MARK: - ViewDidLoad - isCalledHideSkeleton
    
    func test_givenAPromotionBOAndUserLocationAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallViewDidLoadAndInteractorReturnsErrorDifferentThan403_thenICallViewHideSkeleton() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        let promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]
        
        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true
        
        // given
        sut.interactor = dummyInteractor
        sut.promotionBO = promotionBO
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledHideSkeleton == true)
    }
    
    func test_givenAPromotionBOAndUserLocationAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallViewDidLoad_thenICallHideSkeleton() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledHideSkeleton == true)
    }
    
    func test_givenAPromotionBOAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallViewDidLoadAndLocationManagerGPSFail_thenICallHideSkeleton() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledHideSkeleton == true)
    }
    
    func test_givenAPromotion_whenICallViewDidLoadAndDisplayItemPromoDetailIsFilled_thenICallViewHideSkeleton() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        sut.displayItemPromoDetail = DisplayItemPromotionDetail.generateDisplayItemPromotionDetail(fromPromotion: promotionBO)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledHideSkeleton == true)
    }
    
    // MARK: - ViewDidLoad - isCalledShowError and isCalledShowErrorView
    
    func test_givenAPromotionBOAndUserLocationAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallViewDidLoadAndInteractorReturnsError403_thenICallViewShowError() {
        
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        
        sut.view = dummyView
        sut.interactor = dummyInteractor
        
        let promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]
        
        let errorBO = ErrorBO()
        errorBO.status = ConstantsHTTPCodes.STATUS_403
        errorBO.code = ErrorCode.expiredSession.rawValue
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true
        
        // given
        sut.promotionBO = promotionBO
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowError == true)
    }
    
    func test_givenAPromotionBOAndUserLocationAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallViewDidLoadAndInteractorReturnsErrorDifferentThan403_thenICallViewShowErrorView() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        let promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]
        
        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true
        
        // given
        sut.interactor = dummyInteractor
        sut.promotionBO = promotionBO
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowErrorView == true)
    }
    
    func test_givenAPromotionBOAndUserLocation_whenICallViewDidLoadAndInteractorReturnsAnErrorDifferentThan403_thenICallShowErrorView() {
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceError = true
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowErrorView == true)
    }
    
    func test_givenAPromotionBOAndUserLocationAndDisplayItemPromoDetailSumamaryDescriptionNilAndLocationManagerGPSFail_whenICallViewDidLoadAndInteractorReturnsError403_thenICallViewShowError() {
        
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        
        sut.view = dummyView
        sut.interactor = dummyInteractor
        
        let promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]
        
        let errorBO = ErrorBO()
        errorBO.status = ConstantsHTTPCodes.STATUS_403
        errorBO.code = ErrorCode.expiredSession.rawValue
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true
        
        // given
        sut.promotionBO = promotionBO
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowError == true)
    }
    
    func test_givenAPromotionBOAndUserLocationAndDisplayItemPromoDetailSumamaryDescriptionNilAndLocationManagerGPSFail_whenICallViewDidLoadAndInteractorReturnsErrorDifferentThan403_thenICallViewShowErrorView() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        let promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]
        
        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true
        
        // given
        sut.interactor = dummyInteractor
        sut.promotionBO = promotionBO
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowErrorView == true)
    }
    
    func test_givenAPromotionBOAndUserLocation_whenICallViewDidLoadAndInteractorReturnsAnErrorDifferentThan403AndLocationManagerGPSFail_thenICallShowErrorView() {
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceError = true
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowErrorView == true)
    }
    
    func test_givenAPromotionBO_whenICallViewDidLoadAndInteractorReturnsSuccessAndLocationManagerGPSFail_thenINotCallShowErrorView() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowErrorView == false)
    }
    
    func test_givenAPromotionBO_whenICallViewDidLoadAndInteractorReturnsSuccessAndLocationManagerGPSFail_thenINotCallShowError() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowError == false)
    }
    
    func test_givenAPromotionBO_whenICallViewDidLoadAndInteractorReturnsSuccessAndLocationManagerGPSSuccess_thenINotCallShowErrorView() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowErrorView == false)
    }
    
    func test_givenAPromotionBO_whenICallViewDidLoadAndInteractorReturnsSuccessAndLocationManagerGPSSuccess_thenINotCallShowError() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowError == false)
    }
    
    // MARK: - ViewDidLoad - displayItemPromoDetail
    
    func test_givenAPromotionBOAndUserLocationAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallViewDidLoad_thenCreateDisplayItemDetailIsFilled() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(sut.displayItemPromoDetail.commerceDescription == sut.promotionBO?.commerceInformation.description)
        XCTAssert(sut.displayItemPromoDetail.legalConditions == sut.promotionBO?.legalConditions?.text)
        XCTAssert(sut.displayItemPromoDetail.legalConditionsURL == sut.promotionBO?.legalConditions?.url)
        XCTAssert(sut.displayItemPromoDetail.commerceName == sut.promotionBO?.commerceInformation.name)
        XCTAssert(sut.displayItemPromoDetail.commerceWebPage == sut.promotionBO?.commerceInformation.web)
    }
    
    func test_givenAPromotionBOAndUserLocationAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallViewDidLoad_thenDisplayItemPromotionDetailMatch() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        let displayItemPromoDetail = DisplayItemPromotionDetail.generateDisplayItemPromotionDetail(fromPromotion: promotionBO)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(sut.displayItemPromoDetail == displayItemPromoDetail)
    }
    
    func test_givenAPromotionBOAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallViewDidLoadAndLocationManagerGPSFail_thenDisplayItemPromotionDetailMatch() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        dummyLocationManager.locationErrorType = .gpsFail
        let displayItemPromoDetail = DisplayItemPromotionDetail.generateDisplayItemPromotionDetail(fromPromotion: promotionBO)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(sut.displayItemPromoDetail == displayItemPromoDetail)
    }
    
    // MARK: - ViewDidLoad - AssociatedTradeMap
    
    func test_givenAPromotionBOWithStoresAndDisplayItemPromoDetailSumamaryDescriptionNilAndUserLocation_whenICallViewDidLoad_thenICallShowAssociatedTradeMap() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowAssociatedTradeMap == true)
    }
    
    func test_givenAPromotionBOWithStoresAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallViewDidLoadAndLocationManagerGPSFail_thenICallShowAssociatedTradeMap() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowAssociatedTradeMap == true)
    }
    
    func test_givenAPromotionBOWithoutStoresAndDisplayItemPromoDetailSumamaryDescriptionNilAndUserLocation_whenICallViewDidLoad_thenINotCallShowAssociatedTradeMap() {
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceNoStores = true
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowAssociatedTradeMap == false)
    }
    
    func test_givenAPromotionBOWithoutStoresAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallViewDidLoadAndLocationManagerGPSFail_thenINotCallShowAssociatedTradeMap() {
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceNoStores = true
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowAssociatedTradeMap == false)
    }
    
    func test_givenAPromotionBOWithStoresAndUserLocationAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallViewDidLoad_thenICallShowAssociatedTradeMapWithDisplayDataFilled() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.associatedTradeMapDisplayData != nil)
    }
    
    func test_givenAPromotionBOWithStoresAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallViewDidLoadAndLocationManagerGPSFail_thenICallShowAssociatedTradeMapWithDisplayDataFilled() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.associatedTradeMapDisplayData != nil)
    }
    
    func test_givenAPromotionBOWithStoresAndUserLocationDisplayItemPromoDetailSumamaryDescriptionNil_whenICallViewDidLoad_thenICallShowAssociatedTradeMapWithRightDisplayData() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        let displayData = AssociatedTradeMapDisplayData(promotion: promotionBO, userLocation: GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0))
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.associatedTradeMapDisplayData == displayData)
    }
    
    func test_givenAPromotionBOWithStoresDisplayItemPromoDetailSumamaryDescriptionNil_whenICallViewDidLoadAndLocationManagerGPSFail_thenICallShowAssociatedTradeMapWithRightDisplayData() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        
        dummyLocationManager.locationErrorType = .gpsFail
        let displayData = AssociatedTradeMapDisplayData(promotion: promotionBO, userLocation: sut.userLocationA)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.associatedTradeMapDisplayData == displayData)
    }
    
    // MARK: - ViewDidLoad - isCalledHideAssociatedTradeMap
    
    func test_givenAPromotionBOWithStoresAndDisplayItemPromoDetailSumamaryDescriptionNilAndUserLocation_whenICallViewDidLoad_thenINotCallHideAssociatedTradeMap() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledHideAssociatedTradeMap == false)
    }
    
    func test_givenAPromotionBOWithStoresAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallViewDidLoadAndLocationManagerGPSFail_thenINotCallHideAssociatedTradeMap() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledHideAssociatedTradeMap == false)
    }
    
    func test_givenAPromotionBOWithoutStoresAndDisplayItemPromoDetailSumamaryDescriptionNilAndUserLocation_whenICallViewDidLoad_thenICallHideAssociatedTradeMap() {
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceNoStores = true
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledHideAssociatedTradeMap == true)
    }
    
    func test_givenAPromotionBOWithoutStoresAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallViewDidLoadAndLocationManagerGPSFail_thenICallHideAssociatedTradeMap() {
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceNoStores = true
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledHideAssociatedTradeMap == true)
    }
    
    // MARK: - ViewDidLoad - isCalledShowPromoDetailImage
    
    func test_givenPromotionBOWithNoImageDetailSaved_whenICallViewDidLoadAndInteractorProvideImageFail_thenICallShowPromoDetailImageWithImageDefault() {
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceError = true
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let imageDefault = UIImage(named: ConstantsImages.Promotions.default_promotion_image)
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO?.imageDetailSaved = imageDefault
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowPromoDetailImage == true)
        XCTAssert(dummyView.promoImage == imageDefault)
    }
    
    func test_givenPromotionBOWithImageDetailSaved_whenICallViewDidLoad_thenICallViewShowPromoDetailImage() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO?.imageDetailSaved = UIImage()
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowPromoDetailImage == true)
        
    }
    
    func test_givenPromotionBOWithImageDetailSaved_whenICallViewDidLoad_thenICallViewShowPromoDetailImageWithTheImageSaved() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        let image = UIImage()
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO?.imageDetailSaved = image
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.promoImage == sut.promotionBO?.imageDetailSaved)
    }
    
    // MARK: - ViewDidLoad - updatePromo
    
    func test_givenAPromotionBOWithStoresAndDisplayItemPromoAndUserLocation_whenICallViewDidLoad_thenICallUpdatePromo() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledUpdatePromo == true)
    }
    
    func test_givenAPromotionBOWithStoresAndDisplayItemPromo_whenICallViewDidLoadAndLocationManagerGPSFail_thenICallUpdatePromo() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledUpdatePromo == true)
    }
    
    func test_givenAPromotionBOWithNoStoresAndUserLocation_whenICallViewDidLoad_thenINotCallUpdatePromo() {
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceNoStores = true
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO?.stores = nil
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledUpdatePromo == false)
    }
    
    func test_givenAPromotionBOWithStores_whenICallViewDidLoadAndLocationManagerGPSFail_thenINotCallUpdatePromo() {
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceNoStores = true
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO?.stores = nil
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledUpdatePromo == false)
    }
    
    func test_givenAPromotionBOWithStoresAndDisplayItemPromoAndUserLocation_whenICallViewDidLoad_thenDisplayItemUpdatePromoMatch() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        let displayItemPromo = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionBO, userLocation: GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0))
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.displayItemUpdatePromo == displayItemPromo)
    }
    
    func test_givenAPromotionBOWithStoresAndDisplayItemPromo_whenICallViewDidLoadAndLocationManagerGPSFail_thenDisplayItemUpdatePromoMatch() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        dummyLocationManager.locationErrorType = .gpsFail
        let displayItemPromo = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionBO, userLocation: nil)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.displayItemUpdatePromo == displayItemPromo)
    }
    
    // MARK: - ViewDidLoad - isCalledShowCardsView - Private
    
    func test_givenAPromotionAndCards_whenICallViewDidLoad_thenCallViewShowCardsViewAndCallAssociatedCardsDisplayDataFilled() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        sut.cards = CardsGenerator.getThreeCards()
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowCardsView == true)
        XCTAssert(dummyView.associatedCardsDisplayData != nil)
    }
    
    func test_givenAPromotionAndCards_whenICallViewDidLoad_thenCallViewShowCardsView() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        sut.cards = CardsGenerator.getThreeCards()
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowCardsView == true)
    }
    
    func test_givenCardListBO_whenICallViewDidLoad_thenCardsDisplayDataCardsShouldBeCorrect() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        
        let cards = promotionBO.cards
        
        sut.promotionBO = promotionBO
        sut.cards = CardsGenerator.getThreeCards()
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.viewDidLoad()
        
        //then
        var cardsListExpected = [AssociatedCardsDisplayData]()
        sut.cards?.cards = (sut.cards?.cards.uniques())!
        
        cardsListExpected = cardsListExpected.uniques()
        for card in cards! {
            for anotherCard in (sut.cards?.cards)! where card.cardTitle?.id == anotherCard.title?.id {
                let associatedCardsDisplayData = AssociatedCardsDisplayData(cardTitleName: card.cardTitle?.name ?? "", imageUrl: anotherCard.imageFront ?? "", imageNamed: nil)
                
                cardsListExpected.append(associatedCardsDisplayData)
            }
        }
        
        XCTAssert(dummyView.associatedCardsDisplayData == cardsListExpected)
        
    }
    
    func test_givenAPromotionAndCards_whenICallViewDidLoad_thenNotCallViewShowCardsViewAndAssociatedCardsDisplayDataNotFilled() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        promotionBO.cards = nil
        sut.promotionBO = promotionBO
        sut.cards = CardsGenerator.getThreeCards()

        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowCardsView == false)
        XCTAssert(dummyView.associatedCardsDisplayData == nil)
    }
    
    func test_givenAPromotionAndCards_whenICallViewDidLoad_thenNotCallViewShowCardsView() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        promotionBO.cards = nil
        sut.promotionBO = promotionBO
        sut.cards = CardsGenerator.getThreeCards()
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowCardsView == false)
    }
    
    // MARK: - ViewDidLoad - isCalledShowCardsView - Public
    
    func test_givenAPromotionAndCards_whenICallViewDidLoad_thenCallViewShowCardsViewAndCallAssociatedCardsDisplayDataFilledAndIsPublicAccess() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        sut.cards = nil
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowCardsView == true)
        XCTAssert(dummyView.associatedCardsDisplayData != nil)
    }
    
    func test_givenAPromotionAndCards_whenICallViewDidLoad_thenCallViewShowCardsViewAndIsPublicAccess() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        sut.cards = nil
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowCardsView == true)
    }
    
    func test_givenCardListBO_whenICallViewDidLoad_thenCardsDisplayDataCardsShouldBeCorrectAndIsPublicAccess() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        
        var cards = promotionBO.cards
        
        sut.promotionBO = promotionBO
        sut.cards = nil
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.viewDidLoad()
        
        //then
        var cardsListExpected = [AssociatedCardsDisplayData]()
        cards = cards?.uniques()
        for card in cards! {
            let associatedCardsDisplayData = AssociatedCardsDisplayData(cardTitleName: (card.cardTitle?.name)!, imageUrl: nil, imageNamed: UIImage(named: "Card\((card.cardTitle?.id)!)"))
            cardsListExpected.append(associatedCardsDisplayData)
        }
        
        XCTAssert(dummyView.associatedCardsDisplayData == cardsListExpected)
        
    }
    
    func test_givenAPromotionAndCards_whenICallViewDidLoad_thenNotCallViewShowCardsViewAndAssociatedCardsDisplayDataNotFilledAndIsPublicAccess() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        promotionBO.cards = nil
        sut.promotionBO = promotionBO
        
        sut.cards = nil
    
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowCardsView == false)
        XCTAssert(dummyView.associatedCardsDisplayData == nil)
    }
    
    func test_givenAPromotionAndCards_whenICallViewDidLoad_thenNotCallViewShowCardsViewAndIsPublicAccess() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        promotionBO.cards = nil
        sut.promotionBO = promotionBO
        sut.cards = nil
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowCardsView == false)
    }
    
    // MARK: - providePromotionDetail - ShowCreditCardRequestView
    
    //TODO: Task (PSS2-3014). This task does not apply in the first upload to the AppStore.
    
//    func test_givenAPromotionAndIsUserLoggedTrue_whenICallProvidePromotionDetail_thenICallShowCreditCardRequestView() {
//
//        // given
//        let promotionBO = PromotionsGenerator.getPromotionDetail()
//        sut.promotionBO = promotionBO
//        sut.isUserLogged = false
//
//        // when
//        sut.providePromotionDetail()
//
//        // then
//        XCTAssert(dummyView.isCalledShowCreditCardRequestView == true)
//    }
//
//    func test_givenAPromotionAndIsUserLoggedFalse_whenICallProvidePromotionDetail_thenINotCallShowCreditCardRequestView() {
//
//        // given
//        let promotionBO = PromotionsGenerator.getPromotionDetail()
//        sut.promotionBO = promotionBO
//        sut.isUserLogged = true
//
//        // when
//        sut.providePromotionDetail()
//
//        // then
//        XCTAssert(dummyView.isCalledShowCreditCardRequestView == false)
//    }
    
    // MARK: - showCreditCardRequestUrl
    
    func test_givenAny_whenICallShowCreditCardRequestUrl_thenICallBusNavigateToWebPageScreenWithTheCorrectValuesInTransport() {
        
        // given
        
        // when
        sut.showCreditCardRequestUrl()
        
        // then
        XCTAssert(dummyBusManager.isCalledNavigateToWebPageScreen == true)
        XCTAssert(dummyBusManager.dummyWebValue.webPage == Settings.Promotions.creditCardRequestWeb)
        XCTAssert(dummyBusManager.dummyWebValue.webTitle == Settings.Promotions.creditCardRequestWeb)
    }
    
    class DummyInteractor: ShoppingDetailInteractorProtocol {
        
        var forceError = false
        var forceNoDescriptionAndWeb = false
        var forceNotCommerceDescription = false
        var forceNoWebPage = false
        var forceNoStores = false
        var forceNoPromotionUrl = false
        var presenter: ShoppingDetailPresenterA<DummyView>?
        var errorBO: ErrorBO?
        var promotionBO: PromotionBO!
        
        var isCalledProvidePromotionByParams = false
        var isCalledProvideImage = false
        
        var imageUrlString: String?
        var imagePromo: UIImage?
        var paramsSent: ShoppingDetailParamsTransport?

        func providePromotion(byParams params: ShoppingDetailParamsTransport) -> Observable<ModelBO> {
            isCalledProvidePromotionByParams = true
            paramsSent = params
            
            if promotionBO == nil {
                
                if forceNoDescriptionAndWeb {
                    
                    promotionBO = PromotionsGenerator.getPromotionDetailWithoutDescriptionAndWeb()
                } else {
                    
                    promotionBO = PromotionsGenerator.getPromotionDetail()
                    
                    if forceNoWebPage {
                        promotionBO.commerceInformation.web = nil
                    }
                    
                    if forceNotCommerceDescription {
                        promotionBO.commerceInformation.description = nil
                    }
                    
                    if forceNoStores {
                        promotionBO.stores = nil
                    }
                    
                    if forceNoPromotionUrl {
                        promotionBO.promotionUrl = nil
                    }
                }
            }
            
            if forceError {
                
                if errorBO == nil {
                    errorBO = ErrorBO()
                    errorBO!.isErrorShown = false
                }
                
                return Observable.error(ServiceError.GenericErrorBO(error: errorBO!))
            }
            
            return Observable <ModelBO>.just(promotionBO!)
        }
    }
    
    class DummyView: ShoppingDetailViewProtocolB, ViewProtocol {
    
        var isCalledShowPromo = false
        var isCalledShowPromotionType = false
        var isCalledShowPromoDetail = false
        var isCalledShowPromoDetailImage = false
        var isCalledUpdatePromoDetail = false
        var isCalledShowError = false
        var isCalledShowSkeleton = false
        var isCalledHideSkeleton = false
        var isCalledShowErrorView = false
        var isCalledHideErrorView = false
        var isCalledShowPromoStore = false
        var isCalledHidePromoStore = false
        var isCalledShowAssociatedTradeMap = false
        var isCalledHideAssociatedTradeMap = false
        var isCalledCallToPhone = false
        var isCalledSendEmail = false
        var isCalledLaunchMap = false
        var isCalledSetDistanceAndDateToShow = false
        var isCalledShowWantThisOffer = false
        var isCalledHideWantThisOffer = false
        
        var displayItem: DisplayItemPromotion!
        var displayItemDetail: DisplayItemPromotionDetail!
        var promoImage: UIImage!
        var associateCommerceDisplayData: AssociateCommerceDisplayData!
        var associatedTradeMapDisplayData: AssociatedTradeMapDisplayData?
        var displayDataHelp: HelpDisplayData?
        var associatedCardsDisplayData: [AssociatedCardsDisplayData]?
        
        var phoneSent: String?
        var emailSent: String?
        var destinationCoordinateSent: CLLocationCoordinate2D?
        var originCoordinateSent: CLLocationCoordinate2D?
        var distanceAndDateToShow: String?
        var isCalledHelpView = false
        
        var isCalledUpdatePromo = false
        var displayItemUpdatePromo: DisplayItemPromotion!
        
        var isCalledShowCardsView = false
        
        var promotionContactDisplayData: ContactUsDisplayData?
        var isCalledContactUsView = false
        var isCalledShowCreditCardRequestView = false
        
        func showPromoStore(withAssociateCommerceDisplay displayAssociateCommerce: AssociateCommerceDisplayData) {
            
            isCalledShowPromoStore = true
            associateCommerceDisplayData = displayAssociateCommerce
        }
        
        func hidePromoStore() {
            
            isCalledHidePromoStore = true
        }
        
        func showAssociatedTradeMap(withDisplayData displayData: AssociatedTradeMapDisplayData) {
            
            associatedTradeMapDisplayData = displayData
            isCalledShowAssociatedTradeMap = true
        }
        
        func hideAssociatedTradeMap() {
            
            isCalledHideAssociatedTradeMap = true
        }
        
        func callToPhone(withPhone phone: String) {
            
            isCalledCallToPhone = true
            phoneSent = phone
        }
        
        func sendEmail(withEmail email: String) {
            
            isCalledSendEmail = true
            emailSent = email
        }
        
        func launchMap(withDestinationCoordinate destinationCoordinate: CLLocationCoordinate2D, andOriginCoordinate originCoordinate: CLLocationCoordinate2D?) {
            
            isCalledLaunchMap = true
            destinationCoordinateSent = destinationCoordinate
            originCoordinateSent = originCoordinate
        }
        
        func showError(error: ModelBO) {
            isCalledShowError = true
        }
        
        func changeColorEditTexts() {
        }
        
        func showPromo(withDisplayItemPromotion displayItemPromotion: DisplayItemPromotion) {
            isCalledShowPromo = true
            displayItem = displayItemPromotion
        }
        
        func updatePromo(withDisplayItemPromotion displayItemPromotion: DisplayItemPromotion) {
            
            isCalledUpdatePromo = true
            displayItemUpdatePromo = displayItemPromotion
        }
        
        func showPromotionType(withPromotionTypeDisplay promotionTypeDisplay: PromotionTypeDisplayData) {
            isCalledShowPromotionType = true
        }
        
        func showPromoDetail(withDisplayItemPromotionDetail displayItemPromotionDetail: DisplayItemPromotionDetail) {
            isCalledShowPromoDetail = true
            displayItemDetail = displayItemPromotionDetail
        }
        
        func showPromoDetailImage(withPromoDetailImage promoDetailImage: UIImage?) {
            isCalledShowPromoDetailImage = true
            promoImage = promoDetailImage
        }
        
        func updatePromoDetail(withDisplayItemPromotionDetail displayItemPromotionDetail: DisplayItemPromotionDetail) {
            isCalledUpdatePromoDetail = true
        }
        
        func setDistanceAndDateToShow(withString distanceAndDateToShow: String) {
            isCalledSetDistanceAndDateToShow = true
            self.distanceAndDateToShow = distanceAndDateToShow
        }
        
        func showSkeleton() {
            isCalledShowSkeleton = true
        }
        
        func hideSkeleton() {
            isCalledHideSkeleton = true
        }
        
        func showErrorView() {
            isCalledShowErrorView = true
        }
        
        func hideErrorView() {
            isCalledHideErrorView = true
        }
        
        func showWantThisOffer() {
            
            isCalledShowWantThisOffer = true
        }
        
        func hideWantThisOffer() {
            
            isCalledHideWantThisOffer = true
        }
        
        func showHelp(withDisplayData displayData: HelpDisplayData) {
            displayDataHelp = displayData
            isCalledHelpView = true
        }
        
        func showCardsViewWith(displayDataList: [AssociatedCardsDisplayData]) {
            
            associatedCardsDisplayData = displayDataList
            isCalledShowCardsView = true
        }
        
        func showPromoContact(withPromoContactDisplay promotionContactDisplay: ContactUsDisplayData) {
            
            promotionContactDisplayData = promotionContactDisplay
            isCalledContactUsView = true
        }
        
        func showCreditCardRequestView() {
            isCalledShowCreditCardRequestView = true
        }

    }

    class DummyBusManager: BusManager {

        var isCalledNavigateToWebPageScreen = false
        var isCalledNavigateToStoreList = false
        
        var dummyDetailValue = PromotionDetailTransport()
        var dummyWebValue = WebTransport()

        override init() {
            super.init()
            routerFactory = RouterInitFactory()
        }

        override func navigateScreen<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {
            
            if tag == ShoppingDetailPageReaction.ROUTER_TAG && event === ShoppingDetailPageReaction.EVENT_NAV_TO_WEBPAGE_SCREEN {
                isCalledNavigateToWebPageScreen = true
                dummyWebValue = values as! WebTransport
            } else if tag == ShoppingDetailPageReaction.ROUTER_TAG && event === ShoppingDetailPageReactionA.EVENT_NAV_TO_STORE_LIST_SCREEN {
                isCalledNavigateToStoreList = true
                dummyDetailValue = values as! PromotionDetailTransport
            }
        }
    }
}
