//
//  ShoppingDetailPresenterTest.swift
//  shoppingapp
//
//  Created by Luis Monroy on 10/01/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest
import RxSwift
import CellsNativeComponents

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class ShoppingDetailPresenterTest: XCTestCase {

    var sut: ShoppingDetailPresenter<DummyView>!
    var dummyBusManager: DummyBusManager!

    override func setUp() {
        super.setUp()
        dummyBusManager = DummyBusManager()
        sut = ShoppingDetailPresenter<DummyView>(busManager: dummyBusManager)
    }

    // MARK: - setModel

    func test_givenAPromotionDetailTransport_whenCallSetModel_thenIHavePromotionBO() {

        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()

        let promotionDetailTransport = PromotionDetailTransport()
        promotionDetailTransport.promotionBO = promotionBO

        // when
        sut.setModel(model: promotionDetailTransport)

        // then
        XCTAssert(sut.promotionBO?.id == promotionDetailTransport.promotionBO?.id)
        XCTAssert(sut.promotionBO?.name == promotionDetailTransport.promotionBO?.name)
    }

    func test_givenAPromotionDetailTransportWithCompletion_whenCallSetModel_thenDismissCompletionIsFilled() {

        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()

        let promotionDetailTransport = PromotionDetailTransport()
        promotionDetailTransport.promotionBO = promotionBO
        promotionDetailTransport.completion = {
            print("Completion block")
        }

        // when
        sut.setModel(model: promotionDetailTransport)

        // then
        XCTAssert(sut.dismissCompletion != nil)
    }

    func test_givenAPromotionDetailTransportWithDistanceToShow_whenCallSetModel_thenSetDistanceAndDateToShowIsCalled() {

        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()

        let promotionDetailTransport = PromotionDetailTransport()
        promotionDetailTransport.promotionBO = promotionBO
        promotionDetailTransport.distanceAndDateToShow = "300 m - Fin 30 jul"

        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.setModel(model: promotionDetailTransport)

        // then
        XCTAssert(dummyView.isCalledSetDistanceAndDateToShow == true)
        XCTAssert(dummyView.distanceAndDateToShow == promotionDetailTransport.distanceAndDateToShow)
    }

    func test_givenAPromotionDetailTransport_whenCallSetModel_thenISetUserLocation() {

        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()

        let promotionDetailTransport = PromotionDetailTransport()
        promotionDetailTransport.promotionBO = promotionBO
        promotionDetailTransport.userLocation = GeoLocationBO(withLatitude: 40.0, withLongitude: -3.3)

        // when
        sut.setModel(model: promotionDetailTransport)

        // then
        XCTAssert(sut.userLocation?.latitude == promotionDetailTransport.userLocation?.latitude)
        XCTAssert(sut.userLocation?.longitude == promotionDetailTransport.userLocation?.longitude)
    }

    func test_givenAPromotionDetailTransport_whenCallSetModel_thenISetShownOnMap() {

        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()

        let promotionDetailTransport = PromotionDetailTransport()
        promotionDetailTransport.promotionBO = promotionBO
        promotionDetailTransport.shownOnMap = true

        // when
        sut.setModel(model: promotionDetailTransport)

        // then
        XCTAssert(sut.shownOnMap == promotionDetailTransport.shownOnMap)
    }

    // MARK: - ViewDidLoad

    func test_givenAny_whenICallViewDidLoad_thenICallShowSkeleton() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.viewDidLoad()

        // then
        XCTAssert(dummyView.isCalledShowSkeleton == true)
    }

    func test_givenAPromotion_whenICallViewDidLoad_thenCallViewShowPromo() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()

        // when
        sut.viewDidLoad()

        // then
        XCTAssert(dummyView.isCalledShowPromo == true)
    }
    
    func test_givenAPromotionBOAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallViewDidLoad_thenICallProvidePromotionByParams() {
        
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        let promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]

        sut.view = dummyView
        sut.interactor = dummyInteractor

        // given
        sut.promotionBO = promotionBO

        // when
        sut.viewDidLoad()

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionByParams == true)
    }
    
    func test_givenAPromotionBOAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallViewDidLoad_thenICallProvidePromotionByParamsWithThePromotionBOId() {
        
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        let paramsSent = ShoppingDetailParamsTransport(idPromo: promotionBO.id)

        sut.view = dummyView
        sut.interactor = dummyInteractor

        // given
        sut.promotionBO = promotionBO
        dummyInteractor.paramsSent = paramsSent
        
        // when
        sut.viewDidLoad()

        // then
        XCTAssert(dummyInteractor.paramsSent?.idPromo == sut.promotionBO?.id)
    }

    func test_givenAPromotionBOAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallViewDidLoadAndInteractorReturnsError403_thenICallViewShowError() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        let promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]

        let errorBO = ErrorBO()
        errorBO.status = ConstantsHTTPCodes.STATUS_403
        errorBO.code = ErrorCode.expiredSession.rawValue
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        // given
        sut.interactor = dummyInteractor
        sut.promotionBO = promotionBO

        // when
        sut.viewDidLoad()

        // then
        XCTAssert(dummyView.isCalledShowError == true)
    }

    func test_givenAPromotionBOAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallViewDidLoadAndInteractorReturnsErrorDifferentThan403_thenICallViewShowErrorView() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        let promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]

        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        // given
        sut.interactor = dummyInteractor
        sut.promotionBO = promotionBO

        // when
        sut.viewDidLoad()

        // then
        XCTAssert(dummyView.isCalledShowErrorView == true)
    }

    func test_givenAPromotionBOAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallViewDidLoadAndInteractorReturnsErrorDifferentThan403_thenICallViewHideSkeleton() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        let promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]

        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        // given
        sut.interactor = dummyInteractor
        sut.promotionBO = promotionBO

        // when
        sut.viewDidLoad()

        // then
        XCTAssert(dummyView.isCalledHideSkeleton == true)
    }

    func test_givenAPromotionBO_whenICallViewDidLoad_thenCreateDisplayItem() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()

        // when
        sut.viewDidLoad()

        // then
        XCTAssert(sut.displayItemPromo.commerceName != nil)
    }

    func test_givenAPromotionBOAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallViewDidLoad_thenCreateDisplayItemDetailIsFilled() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()

        // when
        sut.viewDidLoad()

        // then
        XCTAssert(sut.displayItemPromoDetail.commerceDescription != nil)
    }

    func test_givenAPromotionBOAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallViewDidLoad_thenCallViewShowPromoDetail() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()

        // when
        sut.viewDidLoad()

        // then
        XCTAssert(dummyView.isCalledShowPromoDetail == true)
    }

    func test_givenAPromotionBOAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallViewDidLoad_thenICallHideSkeleton() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()

        // when
        sut.viewDidLoad()

        // then
        XCTAssert(dummyView.isCalledHideSkeleton == true)
    }

    func test_givenAPromotion_whenICallViewDidLoadAndInteractorReturnsAnErrorDifferentThan403_thenICallShowErrorView() {

        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceError = true
        sut.interactor = dummyInteractor

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()

        // when
        sut.viewDidLoad()

        // then
        XCTAssert(dummyView.isCalledShowErrorView == true)
    }

    func test_givenAPromotion_whenICallViewDidLoadAndInteractorReturnsAnErrorDifferentThan403_thenICallHideSkeleton() {

        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceError = true
        sut.interactor = dummyInteractor

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()

        // when
        sut.viewDidLoad()

        // then
        XCTAssert(dummyView.isCalledHideSkeleton == true)
    }
    
    func test_givenAPromotion_whenICallViewDidLoadAndDisplayItemPromoDetailIsFilled_thenICallViewShowPromoDetailAndInteractorProvidePromotionByParamsIsNoCalled() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        sut.displayItemPromoDetail = DisplayItemPromotionDetail.generateDisplayItemPromotionDetail(fromPromotion: promotionBO)

        // when
        sut.viewDidLoad()

        // then
        XCTAssert(dummyView.isCalledShowPromoDetail == true)
        XCTAssert(dummyInteractor.isCalledProvidePromotionByParams == false)
    }
    
    func test_givenAPromotion_whenICallViewDidLoadAndDisplayItemPromoDetailIsFilled_thenICallViewHideSkeletonAndInteractorProvidePromotionByParamsIsNoCalled() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        sut.displayItemPromoDetail = DisplayItemPromotionDetail.generateDisplayItemPromotionDetail(fromPromotion: promotionBO)

        // when
        sut.viewDidLoad()

        // then
        XCTAssert(dummyView.isCalledHideSkeleton == true)
        XCTAssert(dummyInteractor.isCalledProvidePromotionByParams == false)
    }

    func test_givenAPromotion_whenICallViewDidLoadAndDisplayItemPromoDetailIsNotFilled_thenICallInteractorProvidePromotionById() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()

        // when
        sut.viewDidLoad()

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionByParams == true)
    }

    func test_givenPromotionBOWithImageDetailSaved_whenICallViewDidLoad_thenICallViewShowPromoDetailImage() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO?.imageDetailSaved = UIImage()

        // when
        sut.viewDidLoad()

        // then
        XCTAssert(dummyView.isCalledShowPromoDetailImage == true)

    }

    func test_givenPromotionBOWithImageDetailSaved_whenICallViewDidLoad_thenICallViewShowPromoDetailImageWithTheImageSaved() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let dummyView = DummyView()
        sut.view = dummyView
        let image = UIImage()

        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO?.imageDetailSaved = image

        // when
        sut.viewDidLoad()

        // then
        XCTAssert(dummyView.promoImage == image)
    }
    
    func test_givenAPromotionBO_whenICallViewDidLoad_thenDisplayItemPromoMatch() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        let location = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)
        sut.userLocation = location
        let displayItemPromo = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: sut.promotionBO!, userLocation: location)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(sut.displayItemPromo == displayItemPromo)
    }

    func test_givenPromotionBOWithNoImageDetailSaved_whenICallViewDidLoadAndInteractorProvideImage_thenIHaveImageDetailSavedWithInteractorImage() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()

        // when
        sut.viewDidLoad()

        // then
        XCTAssert(sut.displayItemPromo.promotionBO?.imageDetailSaved == dummyInteractor.imagePromo)
    }

    func test_givenPromotionBOWithNoImageDetailSaved_whenICallViewDidLoadAndInteractorProvideImageFail_thenICallShowPromoDetailImageWithImageDefault() {

        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceError = true
        sut.interactor = dummyInteractor

        let dummyView = DummyView()
        sut.view = dummyView

        let imageDefault = UIImage(named: ConstantsImages.Promotions.default_promotion_image)

        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO?.imageDetailSaved = imageDefault

        // when
        sut.viewDidLoad()

        // then
        XCTAssert(dummyView.isCalledShowPromoDetailImage == true)
        XCTAssert(dummyView.promoImage == imageDefault)
    }
    
    func test_givenAny_whenIviewDidLoad_thenICallBusPublishDataEventTrackerNewTrackEvent() {
        
        // given
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyBusManager.isCalledPublishDataEventTrackerNewTrackEvent == true)
        
    }
    
    func test_givenAny_whenIviewDidLoad_thenICallBusPublishDataEventTrackerNewTrackEventWithAppropiatedEventModel() {
        
        // given
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyBusManager.eventSent != nil)
        XCTAssert(dummyBusManager.eventSent?.type == .promotionDetail)
    }

    // MARK: - showMoreInformation

    func test_givenAPromotionDetail_whenICallShowMoreInformation_thenCallViewUpdatePromoDetail() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.showPromotionInfo()

        // when
        sut.showMoreInformation()

        // then
        XCTAssert(dummyView.isCalledUpdatePromoDetail == true)
    }

    func test_givenAPromotionDetail_whenICallShowMoreInformation_thenICallUpdatePromoDetailWithCorrectValues() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.showPromotionInfo()

        // when
        sut.showMoreInformation()

        // then
        XCTAssert(dummyView.displayItemDetail.legalConditions == sut.promotionBO?.legalConditions?.text)
        XCTAssert(dummyView.displayItemDetail.legalConditionsURL == sut.promotionBO?.legalConditions?.url)

    }

    // MARK: - showTermsAndConditions

    func test_givenAPromotionDetail_whenICallShowTermsAndConditions_thenICallBusNavigateToWebPageScreen() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.showPromotionInfo()

        // when
        sut.showTermsAndConditions()

        // then
        XCTAssert(dummyBusManager.dummyWebValue.webPage == sut.promotionBO?.legalConditions?.url)
        XCTAssert(dummyBusManager.dummyWebValue.webTitle == Localizables.promotions.key_promotions_terms_and_conditions_text)

    }

    func test_givenAPromotionDetail_whenICallShowTermsAndConditions_thenICallBusNavigateToWebPageScreenWithTheCorrectValuesInTransport() {

        // given
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let dummyView = DummyView()
        sut.view = dummyView
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.showPromotionInfo()

        // when
        sut.showTermsAndConditions()

        // then
        XCTAssert(dummyBusManager.isCalledNavigateToWebPageScreen == true)
        XCTAssert(dummyBusManager.dummyWebValue.webPage == sut.promotionBO?.legalConditions?.url)
        XCTAssert(dummyBusManager.dummyWebValue.webTitle == Localizables.promotions.key_promotions_terms_and_conditions_text)
    }

    // MARK: - reloadInfo

    func test_givenAny_whenICallReloadInfo_thenICallHideErrorView() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()

        // when
        sut.reloadInfo()

        // then
        XCTAssert(dummyView.isCalledHideErrorView == true)
    }

    func test_givenAny_whenICallReloadInfo_thenICallViewShowSkeleton() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given

        // when
        sut.reloadInfo()

        // then
        XCTAssert(dummyView.isCalledShowSkeleton == true)
    }

    func test_givenAPromotion_whenICallReloadInfoAndDisplayItemPromoDetailIsFilled_thenICallViewShowPromoDetailAndInteractorProvidePromotionByIdIsNoCalled() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        sut.displayItemPromoDetail = DisplayItemPromotionDetail.generateDisplayItemPromotionDetail(fromPromotion: promotionBO)

        // when
        sut.reloadInfo()

        // then
        XCTAssert(dummyView.isCalledShowPromoDetail == true)
        XCTAssert(dummyInteractor.isCalledProvidePromotionByParams == false)
    }

    func test_givenAPromotion_whenICallReloadInfoAndDisplayItemPromoDetailIsFilled_thenICallViewHideSkeletonAndInteractorProvidePromotionByIdIsNoCalled() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        sut.displayItemPromoDetail = DisplayItemPromotionDetail.generateDisplayItemPromotionDetail(fromPromotion: promotionBO)

        // when
        sut.reloadInfo()

        // then
        XCTAssert(dummyView.isCalledHideSkeleton == true)
        XCTAssert(dummyInteractor.isCalledProvidePromotionByParams == false)
    }

    func test_givenAPromotion_whenICallReloadInfoAndDisplayItemPromoDetailIsNotFilled_thenICallInteractorProvidePromotionById() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()

        // when
        sut.reloadInfo()

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionByParams == true)
    }

    func test_givenAPromotionBOAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallReloadInfoAndInteractorReturnsError403_thenICallViewShowError() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        let promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]

        let errorBO = ErrorBO()
        errorBO.status = ConstantsHTTPCodes.STATUS_403
        errorBO.code = ErrorCode.expiredSession.rawValue
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        // given
        sut.interactor = dummyInteractor
        sut.promotionBO = promotionBO

        // when
        sut.reloadInfo()

        // then
        XCTAssert(dummyView.isCalledShowError == true)
    }

    func test_givenAPromotionBOAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallReloadInfoAndInteractorReturnsErrorDifferentThan403_thenICallViewShowErrorView() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        let promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]

        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        // given
        sut.interactor = dummyInteractor
        sut.promotionBO = promotionBO

        // when
        sut.reloadInfo()

        // then
        XCTAssert(dummyView.isCalledShowErrorView == true)
    }

    func test_givenAPromotionBOAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallReloadInfoAndInteractorReturnsErrorDifferentThan403_thenICallViewHideSkeleton() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        let promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]

        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        // given
        sut.interactor = dummyInteractor
        sut.promotionBO = promotionBO

        // when
        sut.reloadInfo()

        // then
        XCTAssert(dummyView.isCalledHideSkeleton == true)
    }
    
    func test_givenAPromotionBOAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallReloadInfo_thenICallProvidePromotionByMaramsWithThePromotionBOId() {
        
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        let paramsSent = ShoppingDetailParamsTransport(idPromo: promotionBO.id)

        sut.view = dummyView
        sut.interactor = dummyInteractor

        // given
        sut.promotionBO = promotionBO
        dummyInteractor.paramsSent = paramsSent

        // when
        sut.reloadInfo()

        // then
        XCTAssert(dummyInteractor.paramsSent?.idPromo == sut.promotionBO?.id)
    }

    func test_givenAPromotionBOAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallReloadInfo_thenCreateDisplayItemDetailIsFilled() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()

        // when
        sut.reloadInfo()

        // then
        XCTAssert(sut.displayItemPromoDetail.commerceDescription != nil)
        XCTAssert(sut.displayItemPromoDetail.legalConditions != nil)
    }

    func test_givenAPromotionBOAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallReloadInfo_thenCallViewShowPromoDetail() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()

        // when
        sut.reloadInfo()

        // then
        XCTAssert(dummyView.isCalledShowPromoDetail == true)
    }

    func test_givenAPromotionBOAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallReloadInfo_thenICallHideSkeleton() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()

        // when
        sut.reloadInfo()

        // then
        XCTAssert(dummyView.isCalledHideSkeleton == true)
    }

    func test_givenPromotionBOWithImageDetailSaved_whenICallReloadInfo_thenICallViewShowPromoDetailImage() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO?.imageDetailSaved = UIImage()

        // when
        sut.reloadInfo()

        // then
        XCTAssert(dummyView.isCalledShowPromoDetailImage == true)

    }

    func test_givenPromotionBOWithImageDetailSaved_whenICallReloadInfo_thenICallViewShowPromoDetailImageWithTheImageSaved() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let dummyView = DummyView()
        sut.view = dummyView
        let image = UIImage()

        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO?.imageDetailSaved = image

        // when
        sut.reloadInfo()

        // then
        XCTAssert(dummyView.promoImage == image)
    }

    func test_givenPromotionBOWithNoImageDetailSaved_whenICallReloadInfoAndInteractorProvideImage_thenIHaveImageDetailSavedWithInteractorImage() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()

        // when
        sut.reloadInfo()

        // then
        XCTAssert(sut.displayItemPromo.promotionBO?.imageDetailSaved == dummyInteractor.imagePromo)
    }

    func test_givenPromotionBOWithNoImageDetailSaved_whenICallReloadInfoAndInteractorProvideImageFail_thenICallShowPromoDetailImageWithImageDefault() {

        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceError = true
        sut.interactor = dummyInteractor

        let dummyView = DummyView()
        sut.view = dummyView

        let imageDefault = UIImage(named: ConstantsImages.Promotions.default_promotion_image)

        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO?.imageDetailSaved = imageDefault

        // when
        sut.reloadInfo()

        // then
        XCTAssert(dummyView.isCalledShowPromoDetailImage == true)
        XCTAssert(dummyView.promoImage == imageDefault)
    }

    // MARK: - Dismissed

    func test_givenDismissComplention_whenICallDismissed_thenICallCompletionBlock() {

        // given
        var isCalled = false
        sut.dismissCompletion = {
            isCalled = true
        }

        // when
        sut.dismissed()

        // then
        XCTAssert(isCalled == true)
    }
    
    // MARK: - WantThisOffer
    
    func test_givenAPromotion_whenICallShowWantThisOfferPromotion_thenIHavePromotionUrl() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        sut.displayItemPromoDetail = DisplayItemPromotionDetail.generateDisplayItemPromotionDetail(fromPromotion: promotionBO)
            
        // when
        sut.viewDidLoad()

        // then
        XCTAssert(dummyView.isCalledShowWantThisOffer == true)
    }
    
    func test_givenAPromotion_whenICallViewLoadedAndCallInteractorForceNoPromotionUrl_thenICalledHideWantThisOffer() {
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceNoPromotionUrl = true
        
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledHideWantThisOffer == true)
    }
    
    func test_givenAPromotionAndDisplayItemPromoDetail_whenICallViewLoaded_thenICalledShowWantThisOffer() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        sut.displayItemPromoDetail = DisplayItemPromotionDetail.generateDisplayItemPromotionDetail(fromPromotion: promotionBO)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowWantThisOffer == true)
    }
    
    func test_givenAPromotionWithoutPromotionUrlAndDisplayItemPromoDetail_whenICallViewLoaded_thenICalledHideWantThisOffer() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetailWithoutPromotionUrl()
        sut.promotionBO = promotionBO
        sut.displayItemPromoDetail = DisplayItemPromotionDetail.generateDisplayItemPromotionDetail(fromPromotion: promotionBO)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledHideWantThisOffer == true)
    }
    
    // MARK: - ShowPromotion
    func test_givenAPromotion_whenICallShowPromotion_thenICallBusNavigateToWebPageScreen() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetailWithoutPromotionUrl()
        sut.promotionBO = promotionBO
        
        // when
        sut.showPromotionUrl()
        
        // then
        XCTAssert(dummyBusManager.dummyWebValue.webPage == sut.promotionBO?.promotionUrl)
        XCTAssert(dummyBusManager.dummyWebValue.webTitle == sut.promotionBO?.promotionUrl)
        XCTAssert(dummyBusManager.isCalledNavigateToWebPageScreen == true)
        
    }
    
    // MARK: - getHelp
    
    func test_givenConfigurationWithValuesForKeyPromotionDetail_whenICallGetHelp_thenICallShowHelpView() {
        //given
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyPublicManager = DummyPublicManager()
        ConfigPublicManager.instance = dummyPublicManager
        
        dummyPublicManager.configs = ConfigGenerator.getHelpBO()
        
        //When
        sut.getHelp()
        
        //then
        XCTAssert(dummyView.isCalledHelpView == true)
        
    }
    
    func test_givenConfigurationWithValuesForKeyPromotionDetail_whenICallGetHelp_thenHelpDisplayDataIsNotEmpty() {
        
        //given
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyPublicManager = DummyPublicManager()
        ConfigPublicManager.instance = dummyPublicManager
        
        dummyPublicManager.configs = ConfigGenerator.getHelpBO()
        
        //when
        sut.getHelp()
        
        //then
        XCTAssert(dummyView.displayDataHelp != nil)
        
    }
    func test_givenConfigurationWithValuesForKeyPromotionDetail_whenICallGetHelp_thenICallGetConfigBOBySectionAndCorrectKeyAndWithSectionValues() {
        //given
        
        let dummyPublicManager = DummyPublicManager()
        ConfigPublicManager.instance = dummyPublicManager
        
        dummyPublicManager.configs = ConfigGenerator.getHelpBO()
        
        //When
        sut.getHelp()
        
        //then
        XCTAssert(dummyPublicManager.isCalledGetConfigBOBySection == true)
        XCTAssert(dummyPublicManager.sentPromotionDetailKey == "promotions_detail")
        
    }
    
    func test_givenNilConfigurationForKeyPromotionDetail_whenICallGetHelp_thenIDontCallShowHelpView() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyPublicManager = DummyPublicManager()
        ConfigPublicManager.instance = dummyPublicManager
        
        dummyPublicManager.configs = nil
        
        //when
        sut.getHelp()
        
        //then
        XCTAssert(dummyView.isCalledHelpView == false)
        
    }
    
    func test_givenEmptyConfigurationForKeyPromotionDetail_whenICallGetHelp_thenIDontCallShowHelpView() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyPublicManager = DummyPublicManager()
        ConfigPublicManager.instance = dummyPublicManager
        
        dummyPublicManager.configs!.items = [ConfigFileBO]()
        //when
        sut.getHelp()
        
        //then
        XCTAssert(dummyView.isCalledHelpView == false)
        
    }
    // MARK: - helpButtonPressed
    
    func test_givenHelpId_whenHelpButtonPressed_thenICallBusManagerDetailHelpScreen() {
        
        //given
        let helpId = "helpId"
        
        //when
        sut.helpButtonPressed(withHelpId: helpId)
        
        //then
        XCTAssert(dummyBusManager.isCalledNavigateToDetailHelpScreen == true)
        
    }
    
    func test_givenHelpId_whenHelpButtonPressed_thenICallPublishHelpDetailTransportWithHelpId() {
        
        //given
        let helpId = "helpId"
        
        //when
        sut.helpButtonPressed(withHelpId: helpId)
        
        //then
        XCTAssert(helpId == dummyBusManager.dummyHelpDetailTransport.helpId)
        
    }
    
    // MARK: - showPromoContact
    
    func test_givenEmptyPromotion_whenICallViewLoaded_thenIDontCalledShowPromoContact() {
        
        // given
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        sut.promotionBO = nil
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledContactUsView == false)
    }
    
    func test_givenAPromotion_whenICallViewLoaded_thenICalledShowPromoContact() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledContactUsView == true)
    }
    
    func test_givenAPromotion_whenICallViewLoaded_thenICalledShowPromoContactWhitContactUsDisplayDataIsNotEmpty() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledContactUsView == true)
        XCTAssert(dummyView.promotionContactDisplayData != nil)
    }
    
    // MARK: - showGuarantee
    
    func test_givenAPromotionBO_whenICallShowGuarantee_thenICallBusManagerNavigateScreenWithAppropiatedTag() {
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        
        // when
        sut.showGuarantee()
        
        // then
        XCTAssert(dummyBusManager.isCalledNavigateToGuaranteeScreen == true)
    }
    
    func test_givenAPromotionBO_whenICallShowGuarantee_thenICallBusManagerNavigateScreenWithPromotionDetailTransportWithCorrectValues() {
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        
        // when
        sut.showGuarantee()
        
        // then
        XCTAssert(dummyBusManager.dummyDetailValue.promotionBO == sut.promotionBO)
    }
    
    // MARK: - showContactDetails
    
    func test_givenAPromotionBO_whenICallShowContactDetails_thenICallBusManagerNavigateScreenWithAppropiatedTag() {
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        
        // when
        sut.showContactDetails()
        
        // then
        XCTAssert(dummyBusManager.isCalledNavigateToContactDetailsScreen == true)
    }
    
    func test_givenAPromotionBO_whenICallShowContactDetails_thenICallBusManagerNavigateScreenWithPromotionDetailTransportWithCorrectValues() {
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        
        // when
        sut.showContactDetails()
        
        // then
        XCTAssert(dummyBusManager.dummyDetailValue.promotionBO == sut.promotionBO)
    }
    
    func test_givenAny_whenICallFilterPressed_thenICallBusPublishDataEventTrackerNewTrackEvent() {
        
        // given
        
        // when
        sut.showContactDetails()
        
        // then
        XCTAssert(dummyBusManager.isCalledPublishDataEventTrackerNewTrackEvent == true)
        
    }
    
    func test_givenAny_whenICallFilterPressed_thenICallBusPublishDataEventTrackerNewTrackEventWithAppropiatedEventModel() {
        
        // given
        
        // when
        sut.showContactDetails()
        
        // then
        XCTAssert(dummyBusManager.eventSent != nil)
        XCTAssert(dummyBusManager.eventSent?.type == .noPromotionContact)
    }
    
    // MARK: - DummyData

    class DummyInteractor: ShoppingDetailInteractorProtocol {

        var forceError = false
        var presenter: ShoppingDetailPresenter<DummyView>?
        var errorBO: ErrorBO?
        var promotionBO: PromotionBO!
        var forceNoPromotionUrl = false
        var isCalledProvidePromotionById = false
        var isCalledProvidePromotionByParams = false
        var isCalledProvideImage = false
        var imageUrlString: String?
        var imagePromo: UIImage?
        var paramsSent: ShoppingDetailParamsTransport?
        
        func providePromotion(byParams params: ShoppingDetailParamsTransport) -> Observable<ModelBO> {
            isCalledProvidePromotionByParams = true
            paramsSent = params
            
            if promotionBO == nil {
                promotionBO = PromotionsGenerator.getPromotionDetail()
                if forceNoPromotionUrl {
                    promotionBO.promotionUrl = nil
                }
            }

            if forceError {

                if errorBO == nil {
                    errorBO = ErrorBO(error: ErrorEntity(message: "Sesión expirada"))
                    errorBO!.isErrorShown = false
                }

                return Observable.error(ServiceError.GenericErrorBO(error: errorBO!))
            } else {
                return Observable <ModelBO>.just(promotionBO!)
            }
        }
    }

    class DummyView: ShoppingDetailViewProtocol, ViewProtocol {

        var isCalledShowPromo = false
        var isCalledShowPromotionType = false
        var isCalledShowPromoDetail = false
        var isCalledShowPromoDetailImage = false
        var isCalledUpdatePromoDetail = false
        var isCalledShowError = false
        var isCalledShowSkeleton = false
        var isCalledHideSkeleton = false
        var isCalledShowErrorView = false
        var isCalledHideErrorView = false
        var isCalledSetDistanceAndDateToShow = false
        var isCalledShowWantThisOffer = false
        var isCalledHideWantThisOffer = false
        var isCalledHelpView = false
        var displayItem: DisplayItemPromotion!
        var displayItemDetail: DisplayItemPromotionDetail!
        var promoImage: UIImage!
        var distanceAndDateToShow: String?
        var displayDataHelp: HelpDisplayData?

        var isCalledUpdatePromo = false
        var displayItemUpdatePromo: DisplayItemPromotion!

        var promotionContactDisplayData: ContactUsDisplayData?
        var isCalledContactUsView = false

        func showError(error: ModelBO) {
            isCalledShowError = true
        }

        func changeColorEditTexts() {
        }

        func showPromo(withDisplayItemPromotion displayItemPromotion: DisplayItemPromotion) {
            isCalledShowPromo = true
            displayItem = displayItemPromotion
        }
        
        func updatePromo(withDisplayItemPromotion displayItemPromotion: DisplayItemPromotion) {
            
            isCalledUpdatePromo = true
            displayItemUpdatePromo = displayItemPromotion
        }

        func showPromotionType(withPromotionTypeDisplay promotionTypeDisplay: PromotionTypeDisplayData) {
            isCalledShowPromotionType = true
        }

        func showPromoDetail(withDisplayItemPromotionDetail displayItemPromotionDetail: DisplayItemPromotionDetail) {
            isCalledShowPromoDetail = true
            displayItemDetail = displayItemPromotionDetail
        }

        func showPromoDetailImage(withPromoDetailImage promoDetailImage: UIImage?) {
            isCalledShowPromoDetailImage = true
            promoImage = promoDetailImage
        }

        func updatePromoDetail(withDisplayItemPromotionDetail displayItemPromotionDetail: DisplayItemPromotionDetail) {
            isCalledUpdatePromoDetail = true
        }

        func setDistanceAndDateToShow(withString distanceAndDateToShow: String) {
            isCalledSetDistanceAndDateToShow = true
            self.distanceAndDateToShow = distanceAndDateToShow
        }

        func showSkeleton() {
            isCalledShowSkeleton = true
        }

        func hideSkeleton() {
            isCalledHideSkeleton = true
        }

        func showErrorView() {
            isCalledShowErrorView = true
        }

        func hideErrorView() {
            isCalledHideErrorView = true
        }
        
        func showWantThisOffer() {
            
            isCalledShowWantThisOffer = true
        }
        
        func hideWantThisOffer() {
            
            isCalledHideWantThisOffer = true
        }
        
        func showHelp(withDisplayData displayData: HelpDisplayData) {
            
            displayDataHelp = displayData
            isCalledHelpView = true
        }

        func showPromoContact(withPromoContactDisplay promotionContactDisplay: ContactUsDisplayData) {

            promotionContactDisplayData = promotionContactDisplay
            isCalledContactUsView = true
        }
        
    }

    class DummyBusManager: BusManager {

        var isCalledNavigateToWebPageScreen = false
        var isCalledPublishDataDetailWithTagPromotionsBO = false
        var isCalledNavigateToDetailHelpScreen = false
        var isCalledNavigateToGuaranteeScreen = false
        var isCalledNavigateToContactDetailsScreen = false
        var isCalledPublishDataEventTrackerNewTrackEvent = false

        var dummyDetailValue = PromotionDetailTransport()
        var dummyHelpDetailTransport: HelpDetailTransport!
        var dummyWebValue = WebTransport()
        var eventSent: EventProtocol?

        override init() {
            super.init()
            
            routerFactory = RouterInitFactory()
        }

        override func navigateScreen<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {
            if tag == "shopping-detail-tag" && event === ShoppingDetailPageReaction.EVENT_NAV_TO_WEBPAGE_SCREEN {
                isCalledNavigateToWebPageScreen = true
                isCalledPublishDataDetailWithTagPromotionsBO = true
                dummyWebValue = values as! WebTransport
            } else if tag == ShoppingDetailPageReaction.ROUTER_TAG && event === ShoppingDetailPageReaction.EVENT_NAV_TO_DETAIL_HELP {
                isCalledNavigateToDetailHelpScreen = true
                dummyHelpDetailTransport = (values as! HelpDetailTransport)
                
            } else if tag == ShoppingDetailPageReaction.ROUTER_TAG && event === ShoppingDetailPageReaction.EVENT_NAV_TO_GUARANTEE_SCREEN {
                isCalledNavigateToGuaranteeScreen = true
                dummyDetailValue = values as! PromotionDetailTransport
            } else if tag == ShoppingDetailPageReaction.ROUTER_TAG && event === ShoppingDetailPageReaction.EVENT_NAV_TO_CONTACT_SCREEN {
                isCalledNavigateToContactDetailsScreen = true
                dummyDetailValue = values as! PromotionDetailTransport
            }
        }
        
        override func publishData<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {
            
            if tag == EventTracker.id && event === PageReactionConstants.NEW_TRACK_EVENT {
                isCalledPublishDataEventTrackerNewTrackEvent = true
                eventSent = values as? EventProtocol
            }
            
        }
    }
    
    class DummyPublicManager: ConfigPublicManager {
        
        var sentPromotionDetailKey = ""
        var isCalledGetConfigBOBySection = false
        
        override func getConfigBOBySection(forKey key: String) -> [ConfigFileBO] {
            
            isCalledGetConfigBOBySection = true
            sentPromotionDetailKey = key
            
            return super.getConfigBOBySection(forKey: key)
        }
    }

}
