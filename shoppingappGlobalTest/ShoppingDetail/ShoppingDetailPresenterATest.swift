//
//  ShoppingDetailPresenterATest.swift
//  shoppingapp
//
//  Created by Luis Monroy on 24/01/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest
import RxSwift
import CellsNativeComponents
import CoreLocation

#if TESTMX
    @testable import shoppingappMX
#endif

class ShoppingDetailPresenterATest: XCTestCase {

    var sut: ShoppingDetailPresenterA<DummyView>!
    var dummyBusManager: DummyBusManager!
    var dummyLocationManager = DummyLocationManager()

    override func setUp() {
        super.setUp()
        dummyBusManager = DummyBusManager()
        sut = ShoppingDetailPresenterA<DummyView>(busManager: dummyBusManager)
        
        dummyLocationManager = DummyLocationManager()
        sut.locationManager = dummyLocationManager
    }

    // MARK: - showStoreWebPage
    
    func test_givenAPromotion_whenICallShowStoreWebPage_thenICallBusNavigateToWebPageScreen() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.showPromotionInfo()

        // when
        sut.showStoreWebPage()

        // then
        XCTAssert(dummyBusManager.isCalledNavigateToWebPageScreen == true)
        XCTAssert(dummyBusManager.dummyWebValue.webPage == sut.promotionBO?.commerceInformation.web)
        XCTAssert(dummyBusManager.dummyWebValue.webTitle == sut.promotionBO?.commerceInformation.name)

    }

    // MARK: - pressedWebsiteOfAssociatedTrade

    func test_givenAPromotionAndDisplayData_whenICallPressedWebsiteOfAssociatedTrade_thenICallBusNavigateToWebPageScreenAndMatchWebsite() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        let displayData = AssociatedTradeMapDisplayData(promotion: promotionBO)
        sut.associatedTradeMapDisplayData = displayData

        // when
        sut.pressedWebsiteOfAssociatedTrade()

        // then
        XCTAssert(dummyBusManager.isCalledNavigateToWebPageScreen == true)
        XCTAssert(dummyBusManager.dummyWebValue.webPage == sut.associatedTradeMapDisplayData.web)
        XCTAssert(dummyBusManager.dummyWebValue.webTitle == sut.promotionBO?.commerceInformation.name)

    }

    // MARK: - pressedCallToPhoneOfAssociatedTrade

    func test_givenAPromotionAndDisplayData_whenICallPressedCallToPhoneOfAssociatedTrade_thenICallViewCallToPhone() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        let displayData = AssociatedTradeMapDisplayData(promotion: promotionBO)
        sut.associatedTradeMapDisplayData = displayData

        // when
        sut.pressedCallToPhoneOfAssociatedTrade()

        // then
        XCTAssert(dummyView.isCalledCallToPhone == true)

    }

    func test_givenAPromotionAndDisplayData_whenICallPressedCallToPhoneOfAssociatedTrade_thenICallViewCallToPhoneWithTheSamePhone() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        let displayData = AssociatedTradeMapDisplayData(promotion: promotionBO)
        sut.associatedTradeMapDisplayData = displayData

        // when
        sut.pressedCallToPhoneOfAssociatedTrade()

        // then
        XCTAssert(dummyView.phoneSent == sut.associatedTradeMapDisplayData.getPhoneContact())

    }

    // MARK: - pressedSendEmailOfAssociatedTrade

    func test_givenAPromotionAndDisplayData_whenICallPressedSendEmailOfAssociatedTrade_thenICallViewSendEmail() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        let displayData = AssociatedTradeMapDisplayData(promotion: promotionBO)
        sut.associatedTradeMapDisplayData = displayData

        // when
        sut.pressedSendEmailOfAssociatedTrade()

        // then
        XCTAssert(dummyView.isCalledSendEmail == true)

    }

    func test_givenAPromotionAndDisplayData_whenICallPressedSendEmailOfAssociatedTrade_thenICallViewSendEmailWithTheSameEmail() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        let displayData = AssociatedTradeMapDisplayData(promotion: promotionBO)
        sut.associatedTradeMapDisplayData = displayData

        // when
        sut.pressedSendEmailOfAssociatedTrade()

        // then
        XCTAssert(dummyView.emailSent == sut.associatedTradeMapDisplayData.email)

    }

    // MARK: - pressedLaunchMapOfAssociatedTrade

    func test_givenAPromotionAndDisplayDataAndDefaultLocation_whenICallPressedLaunchMapOfAssociatedTrade_thenICallViewLaunchMap() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        let displayData = AssociatedTradeMapDisplayData(promotion: promotionBO)
        sut.associatedTradeMapDisplayData = displayData

        // when
        sut.pressedLaunchMapOfAssociatedTrade()

        // then
        XCTAssert(dummyView.isCalledLaunchMap == true)

    }

    func test_givenAPromotionAndDisplayDataAndUserLocationNil_whenICallPressedLaunchMapOfAssociatedTrade_thenICallViewLaunchMapWithDefaultCoordinates() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        let displayData = AssociatedTradeMapDisplayData(promotion: promotionBO)
        sut.associatedTradeMapDisplayData = displayData

        sut.userLocation = nil

        // when
        sut.pressedLaunchMapOfAssociatedTrade()

        // then
        XCTAssert(dummyView.destinationCoordinateSent?.latitude == sut.associatedTradeMapDisplayData.latitude)
        XCTAssert(dummyView.destinationCoordinateSent?.longitude == sut.associatedTradeMapDisplayData.longitude)
        XCTAssert(dummyView.originCoordinateSent?.latitude == sut.defaultLocation.latitude)
        XCTAssert(dummyView.originCoordinateSent?.longitude == sut.defaultLocation.longitude)

    }

    func test_givenAPromotionAndDisplayDataAndUserLocation_whenICallPressedLaunchMapOfAssociatedTrade_thenICallViewLaunchMapWithCoordinatesInvalidButMethodUseUserLocationCoordinates() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        let displayData = AssociatedTradeMapDisplayData(promotion: promotionBO)
        sut.associatedTradeMapDisplayData = displayData

        sut.userLocation = GeoLocationBO(withLatitude: 40.0, withLongitude: -3.3)

        // when
        sut.pressedLaunchMapOfAssociatedTrade()

        // then
        XCTAssert(dummyView.destinationCoordinateSent?.latitude == sut.associatedTradeMapDisplayData.latitude)
        XCTAssert(dummyView.destinationCoordinateSent?.longitude == sut.associatedTradeMapDisplayData.longitude)
        XCTAssert(dummyView.originCoordinateSent?.latitude == kCLLocationCoordinate2DInvalid.latitude)
        XCTAssert(dummyView.originCoordinateSent?.longitude == kCLLocationCoordinate2DInvalid.longitude)

    }

    // MARK: - ViewDidLoad

    // MARK: - ViewDidLoad - isCalledGetLocation

    func test_givenAPromotion_whenICallViewDidLoad_thenCallLocationManagerGetLocation() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()

        // when
        sut.viewDidLoad()

        // then
        XCTAssert(dummyLocationManager.isCalledGetLocation == true)
    }
    
    func test_givenAPromotion_whenICallViewDidLoadAndHaveSummaryDescription_thenNotCallLocationManagerGetLocation() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.displayItemPromoDetail = DisplayItemPromotionDetail.generateDisplayItemPromotionDetail(fromPromotion: PromotionsGenerator.getPromotionDetail())
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyLocationManager.isCalledGetLocation == false)
    }
    
    // MARK: - ViewDidLoad - isCalledShowSkeleton
    
    func test_givenAny_whenViewLoad_thenICallShowSkeleton() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        let dummyView = DummyView()
        sut.view = dummyView
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowSkeleton == true)
    }
    
    // MARK: - ViewDidLoad - isCalledShowPromo

    func test_givenAPromotion_whenICallViewDidLoad_thenCallViewShowPromo() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowPromo == true)
    }
    
    // MARK: - ViewDidLoad - displayItemPromo
    
    func test_givenAPromotionBO_whenICallViewDidLoad_thenCreateDisplayItem() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(sut.displayItemPromo.commerceName == sut.promotionBO?.commerceInformation.name.uppercased())
        XCTAssert(sut.displayItemPromo.descriptionPromotion == sut.promotionBO?.name)
        XCTAssert(sut.displayItemPromo.benefit == sut.promotionBO?.benefit)
        XCTAssert(sut.displayItemPromo.promotionBO == sut.promotionBO)
        XCTAssert(sut.displayItemPromo.imageDetail == sut.promotionBO?.imageDetail)
        XCTAssert(sut.displayItemPromo.imageList == sut.promotionBO?.imageList)
        XCTAssert(sut.displayItemPromo.imagePush == sut.promotionBO?.imagePush)
    }
    
    func test_givenAPromotionBO_whenICallViewDidLoad_thenDisplayItemPromoMatch() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        let location = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)
        sut.userLocation = location
        let displayItemPromo = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: sut.promotionBO!, userLocation: location)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(sut.displayItemPromo == displayItemPromo)
    }
    
    func test_givenPromotionBOWithNoImageDetailSaved_whenICallViewDidLoadAndInteractorProvideImage_thenIHaveImageDetailSavedWithInteractorImage() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(sut.displayItemPromo.promotionBO?.imageDetailSaved == dummyInteractor.imagePromo)
    }
    
    // MARK: - ViewDidLoad - isCalledShowPromoDetail
    
    func test_givenAPromotion_whenICallViewDidLoadAndDisplayItemPromoDetailIsFilled_thenICallViewShowPromoDetail() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        sut.displayItemPromoDetail = DisplayItemPromotionDetail.generateDisplayItemPromotionDetail(fromPromotion: promotionBO)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowPromoDetail == true)
    }
    
    func test_givenAPromotionBOAndUserLocationAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallViewDidLoad_thenCallViewShowPromoDetail() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowPromoDetail == true)
    }
    
    func test_givenAPromotionBOAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallViewDidLoadAndLocationManagerGPSFail_thenCallViewShowPromoDetail() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowPromoDetail == true)
    }
    
    // MARK: - ViewDidLoad - isCalledShowPromoStore
    
    func test_givenAPromotionAndUserLocation_whenICallViewDidLoadAndHaveCommerceDescriptionAndWebPage_thenCallViewShowPromoStore() {
        
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        
        sut.view = dummyView
        sut.interactor = dummyInteractor
        
        // given
        sut.promotionBO = promotionBO
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowPromoStore == true)
    }
    
    func test_givenAPromotion_whenICallViewDidLoadAndHaveCommerceDescriptionAndWebPageAndLocationManagerGPSFail_thenCallViewShowPromoStore() {
        
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        
        sut.view = dummyView
        sut.interactor = dummyInteractor
        
        // given
        sut.promotionBO = promotionBO
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowPromoStore == true)
    }
    
    func test_givenAPromotionAndUserLocation_whenICallViewDidLoadAndHaveCommerceDescriptionAndNotWebPage_thenCallViewShowPromoStore() {
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceNoWebPage = true
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)

        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowPromoStore == true)
    }
    
    func test_givenAPromotion_whenICallViewDidLoadAndHaveCommerceDescriptionAndNotWebPageAndLocationManagerGPSFail_thenCallViewShowPromoStore() {
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceNoWebPage = true
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowPromoStore == true)
    }
    
    func test_givenAPromotionAndUserLocation_whenICallViewDidLoadAndHaveNotCommerceDescriptionAndWebPage_thenCallViewShowPromoStore() {
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceNotCommerceDescription = true
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)

        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowPromoStore == true)
    }
    
    func test_givenAPromotion_whenICallViewDidLoadAndHaveNotCommerceDescriptionAndWebPageAndLocationManagerGPSFail_thenCallViewShowPromoStore() {
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceNotCommerceDescription = true
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowPromoStore == true)
    }
    
    func test_givenAPromotionAndUserLocation_whenICallViewDidLoadNoHaveCommerceDescriptionAndNoWebPageAndNoStores_thenCallViewShowPromoStore() {
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceNoDescriptionAndWeb = true
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowPromoStore == false)
    }
    
    func test_givenAPromotion_whenICallViewDidLoadNoHaveCommerceDescriptionAndNoWebPageAndNoStoresAndLocationManagerGPSFail_thenCallViewShowPromoStore() {
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceNoDescriptionAndWeb = true
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowPromoStore == false)
    }
    
    // MARK: - ViewDidLoad - isCalledHidePromoStore
    
    func test_givenAPromotionAndUserLocation_whenICallViewDidLoadAndNoHaveCommerceDescriptionAndNoWebPageAndNoStores_thenCallViewHidePromoStore() {
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceNoDescriptionAndWeb = true
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetailWithoutDescriptionAndWeb()
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledHidePromoStore == true)
    }
    
    func test_givenAPromotion_whenICallViewDidLoadAndNoHaveCommerceDescriptionAndNoWebPageAndNoStoresAndLocationManagerGPSFail_thenCallViewHidePromoStore() {
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceNoDescriptionAndWeb = true
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetailWithoutDescriptionAndWeb()
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledHidePromoStore == true)
    }
    
    func test_givenAPromotionAndUserLocation_whenICallViewDidLoadAndHaveCommerceDescriptionAndWebPage_thenINotCalledHidePromoStore() {
        
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        
        sut.view = dummyView
        sut.interactor = dummyInteractor
        
        // given
        sut.promotionBO = promotionBO
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledHidePromoStore == false)
    }
    
    func test_givenAPromotion_whenICallViewDidLoadAndHaveCommerceDescriptionAndWebPageAndLocationManagerGPSFail_thenINotCalledHidePromoStore() {
        
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        
        sut.view = dummyView
        sut.interactor = dummyInteractor
        
        // given
        sut.promotionBO = promotionBO
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledHidePromoStore == false)
    }
    
    // MARK: - ViewDidLoad - isCalledProvidePromotionByParams
    
    func test_givenAPromotion_whenICallViewDidLoadAndDisplayItemPromoDetailIsFilledAndLocationManagerGPSFail_thenICallViewShowPromoDetailAndInteractorProvidePromotionByParamsWithThePromotionIdAndDefaultLocationOnShoppingParamsTransport() {
        
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        
        sut.view = dummyView
        sut.interactor = dummyInteractor
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        dummyLocationManager.locationErrorType = .gpsFail
        
        let locationParam = ShoppingParamLocation(geolocation: GeoLocationBO(withLatitude: sut.defaultLocation.latitude, withLongitude: sut.defaultLocation.longitude), distanceParam: ShoppingDistanceParam(distance: sut.defaultDistance, lengthType: sut.lengthType))
        let expands = ShoppingExpands(stores: true, cards: true, contactDetails: true)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionByParams == true)
        XCTAssert(dummyInteractor.paramsSent == ShoppingDetailParamsTransport(idPromo: sut.promotionBO!.id, location: locationParam, expands: expands))
    }
    
    func test_givenAPromotionAndUserLocation_whenICallViewDidLoadAndDisplayItemPromoDetailIsFilled_thenICallViewShowPromoDetailAndInteractorProvidePromotionByParamsWithThePromotionIdAndDefaultLocationOnShoppingParamsTransport() {
        
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        
        sut.view = dummyView
        sut.interactor = dummyInteractor
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        let locationParam = ShoppingParamLocation(geolocation: GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0), distanceParam: ShoppingDistanceParam(distance: sut.defaultDistance, lengthType: sut.lengthType))
        let expands = ShoppingExpands(stores: true, cards: true, contactDetails: true)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionByParams == true)
        XCTAssert(dummyInteractor.paramsSent == ShoppingDetailParamsTransport(idPromo: sut.promotionBO!.id, location: locationParam, expands: expands))
    }
    
    func test_givenAPromotionBOAndUserLocationAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallViewDidLoad_thenICallProvidePromotionWithStoresByParams() {
        
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        let promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]
        
        sut.view = dummyView
        sut.interactor = dummyInteractor
        
        // given
        sut.promotionBO = promotionBO
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)

        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionByParams == true)
    }
    
    func test_givenAPromotionBOAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallViewDidLoadAndLocationManagerGPSFail_thenICallProvidePromotionWithStoresByParams() {
        
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        let promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]
        
        sut.view = dummyView
        sut.interactor = dummyInteractor
        
        // given
        sut.promotionBO = promotionBO
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionByParams == true)
    }
    
    func test_givenAPromotionBOAndDisplayItemPromoDetailSumamaryDescription_whenICallViewDidLoad_thenINotCallProvidePromotionWithStoresByParams() {
        
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        
        sut.view = dummyView
        sut.interactor = dummyInteractor
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        sut.displayItemPromoDetail = DisplayItemPromotionDetail.generateDisplayItemPromotionDetail(fromPromotion: promotionBO)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionByParams == false)
    }
    
    // MARK: - ViewDidLoad - isCalledHideSkeleton

    func test_givenAPromotionBOAndUserLocationAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallViewDidLoadAndInteractorReturnsErrorDifferentThan403_thenICallViewHideSkeleton() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        let promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]
        
        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true
        
        // given
        sut.interactor = dummyInteractor
        sut.promotionBO = promotionBO
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledHideSkeleton == true)
    }
    
    func test_givenAPromotionBOAndUserLocationAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallViewDidLoad_thenICallHideSkeleton() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledHideSkeleton == true)
    }
    
    func test_givenAPromotionBOAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallViewDidLoadAndLocationManagerGPSFail_thenICallHideSkeleton() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledHideSkeleton == true)
    }
    
    func test_givenAPromotion_whenICallViewDidLoadAndDisplayItemPromoDetailIsFilled_thenICallViewHideSkeleton() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        sut.displayItemPromoDetail = DisplayItemPromotionDetail.generateDisplayItemPromotionDetail(fromPromotion: promotionBO)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledHideSkeleton == true)
    }
    
    // MARK: - ViewDidLoad - isCalledShowError and isCalledShowErrorView

    func test_givenAPromotionBOAndUserLocationAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallViewDidLoadAndInteractorReturnsError403_thenICallViewShowError() {
        
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()

        sut.view = dummyView
        sut.interactor = dummyInteractor

        let promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]
        
        let errorBO = ErrorBO()
        errorBO.status = ConstantsHTTPCodes.STATUS_403
        errorBO.code = ErrorCode.expiredSession.rawValue
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true
        
        // given
        sut.promotionBO = promotionBO
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)

        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowError == true)
    }
    
    func test_givenAPromotionBOAndUserLocationAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallViewDidLoadAndInteractorReturnsErrorDifferentThan403_thenICallViewShowErrorView() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        let promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]
        
        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true
        
        // given
        sut.interactor = dummyInteractor
        sut.promotionBO = promotionBO
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)

        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowErrorView == true)
    }
    
    func test_givenAPromotionBOAndUserLocation_whenICallViewDidLoadAndInteractorReturnsAnErrorDifferentThan403_thenICallShowErrorView() {
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceError = true
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowErrorView == true)
    }
    
    func test_givenAPromotionBOAndUserLocationAndDisplayItemPromoDetailSumamaryDescriptionNilAndLocationManagerGPSFail_whenICallViewDidLoadAndInteractorReturnsError403_thenICallViewShowError() {
        
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        
        sut.view = dummyView
        sut.interactor = dummyInteractor
        
        let promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]
        
        let errorBO = ErrorBO()
        errorBO.status = ConstantsHTTPCodes.STATUS_403
        errorBO.code = ErrorCode.expiredSession.rawValue
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true
        
        // given
        sut.promotionBO = promotionBO
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowError == true)
    }
    
    func test_givenAPromotionBOAndUserLocationAndDisplayItemPromoDetailSumamaryDescriptionNilAndLocationManagerGPSFail_whenICallViewDidLoadAndInteractorReturnsErrorDifferentThan403_thenICallViewShowErrorView() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        let promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]
        
        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true
        
        // given
        sut.interactor = dummyInteractor
        sut.promotionBO = promotionBO
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowErrorView == true)
    }
    
    func test_givenAPromotionBOAndUserLocation_whenICallViewDidLoadAndInteractorReturnsAnErrorDifferentThan403AndLocationManagerGPSFail_thenICallShowErrorView() {
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceError = true
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowErrorView == true)
    }
    
    func test_givenAPromotionBO_whenICallViewDidLoadAndInteractorReturnsSuccessAndLocationManagerGPSFail_thenINotCallShowErrorView() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowErrorView == false)
    }
    
    func test_givenAPromotionBO_whenICallViewDidLoadAndInteractorReturnsSuccessAndLocationManagerGPSFail_thenINotCallShowError() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowError == false)
    }
    
    func test_givenAPromotionBO_whenICallViewDidLoadAndInteractorReturnsSuccessAndLocationManagerGPSSuccess_thenINotCallShowErrorView() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowErrorView == false)
    }
    
    func test_givenAPromotionBO_whenICallViewDidLoadAndInteractorReturnsSuccessAndLocationManagerGPSSuccess_thenINotCallShowError() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowError == false)
    }
    
    // MARK: - ViewDidLoad - displayItemPromoDetail

    func test_givenAPromotionBOAndUserLocationAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallViewDidLoad_thenCreateDisplayItemDetailIsFilled() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)

        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(sut.displayItemPromoDetail.commerceDescription == sut.promotionBO?.commerceInformation.description)
        XCTAssert(sut.displayItemPromoDetail.legalConditions == sut.promotionBO?.legalConditions?.text)
        XCTAssert(sut.displayItemPromoDetail.legalConditionsURL == sut.promotionBO?.legalConditions?.url)
        XCTAssert(sut.displayItemPromoDetail.commerceName == sut.promotionBO?.commerceInformation.name)
        XCTAssert(sut.displayItemPromoDetail.commerceWebPage == sut.promotionBO?.commerceInformation.web)
    }
    
    func test_givenAPromotionBOAndUserLocationAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallViewDidLoad_thenDisplayItemPromotionDetailMatch() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        let displayItemPromoDetail = DisplayItemPromotionDetail.generateDisplayItemPromotionDetail(fromPromotion: promotionBO)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(sut.displayItemPromoDetail == displayItemPromoDetail)
    }
    
    func test_givenAPromotionBOAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallViewDidLoadAndLocationManagerGPSFail_thenDisplayItemPromotionDetailMatch() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        dummyLocationManager.locationErrorType = .gpsFail
        let displayItemPromoDetail = DisplayItemPromotionDetail.generateDisplayItemPromotionDetail(fromPromotion: promotionBO)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(sut.displayItemPromoDetail == displayItemPromoDetail)
    }
    
    // MARK: - ViewDidLoad - AssociatedTradeMap
    
    func test_givenAPromotionBOWithStoresAndDisplayItemPromoDetailSumamaryDescriptionNilAndUserLocation_whenICallViewDidLoad_thenICallShowAssociatedTradeMap() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowAssociatedTradeMap == true)
    }
    
    func test_givenAPromotionBOWithStoresAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallViewDidLoadAndLocationManagerGPSFail_thenICallShowAssociatedTradeMap() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowAssociatedTradeMap == true)
    }
    
    func test_givenAPromotionBOWithoutStoresAndDisplayItemPromoDetailSumamaryDescriptionNilAndUserLocation_whenICallViewDidLoad_thenINotCallShowAssociatedTradeMap() {
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceNoStores = true
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowAssociatedTradeMap == false)
    }
    
    func test_givenAPromotionBOWithoutStoresAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallViewDidLoadAndLocationManagerGPSFail_thenINotCallShowAssociatedTradeMap() {
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceNoStores = true
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowAssociatedTradeMap == false)
    }
    
    func test_givenAPromotionBOWithStoresAndUserLocationAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallViewDidLoad_thenICallShowAssociatedTradeMapWithDisplayDataFilled() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.associatedTradeMapDisplayData != nil)
    }
    
    func test_givenAPromotionBOWithStoresAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallViewDidLoadAndLocationManagerGPSFail_thenICallShowAssociatedTradeMapWithDisplayDataFilled() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.associatedTradeMapDisplayData != nil)
    }

    func test_givenAPromotionBOWithStoresAndUserLocationDisplayItemPromoDetailSumamaryDescriptionNil_whenICallViewDidLoad_thenICallShowAssociatedTradeMapWithRightDisplayData() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        let displayData = AssociatedTradeMapDisplayData(promotion: promotionBO, userLocation: GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0))
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)

        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.associatedTradeMapDisplayData == displayData)
    }
    
    func test_givenAPromotionBOWithStoresDisplayItemPromoDetailSumamaryDescriptionNil_whenICallViewDidLoadAndLocationManagerGPSFail_thenICallShowAssociatedTradeMapWithRightDisplayData() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        
        dummyLocationManager.locationErrorType = .gpsFail
        let displayData = AssociatedTradeMapDisplayData(promotion: promotionBO, userLocation: sut.userLocationA)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.associatedTradeMapDisplayData == displayData)
    }
    
    // MARK: - ViewDidLoad - isCalledHideAssociatedTradeMap
    
    func test_givenAPromotionBOWithStoresAndDisplayItemPromoDetailSumamaryDescriptionNilAndUserLocation_whenICallViewDidLoad_thenINotCallHideAssociatedTradeMap() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledHideAssociatedTradeMap == false)
    }
    
    func test_givenAPromotionBOWithStoresAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallViewDidLoadAndLocationManagerGPSFail_thenINotCallHideAssociatedTradeMap() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledHideAssociatedTradeMap == false)
    }
    
    func test_givenAPromotionBOWithoutStoresAndDisplayItemPromoDetailSumamaryDescriptionNilAndUserLocation_whenICallViewDidLoad_thenICallHideAssociatedTradeMap() {
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceNoStores = true
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledHideAssociatedTradeMap == true)
    }
    
    func test_givenAPromotionBOWithoutStoresAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallViewDidLoadAndLocationManagerGPSFail_thenICallHideAssociatedTradeMap() {
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceNoStores = true
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledHideAssociatedTradeMap == true)
    }
    
    // MARK: - ViewDidLoad - isCalledShowPromoDetailImage

    func test_givenPromotionBOWithNoImageDetailSaved_whenICallViewDidLoadAndInteractorProvideImageFail_thenICallShowPromoDetailImageWithImageDefault() {
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceError = true
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let imageDefault = UIImage(named: ConstantsImages.Promotions.default_promotion_image)
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO?.imageDetailSaved = imageDefault
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowPromoDetailImage == true)
        XCTAssert(dummyView.promoImage == imageDefault)
    }
    
    func test_givenPromotionBOWithImageDetailSaved_whenICallViewDidLoad_thenICallViewShowPromoDetailImage() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO?.imageDetailSaved = UIImage()
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowPromoDetailImage == true)
        
    }
    
    func test_givenPromotionBOWithImageDetailSaved_whenICallViewDidLoad_thenICallViewShowPromoDetailImageWithTheImageSaved() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        let image = UIImage()
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO?.imageDetailSaved = image
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.promoImage == sut.promotionBO?.imageDetailSaved)
    }
    
    // MARK: - ViewDidLoad - updatePromo
    
    func test_givenAPromotionBOWithStoresAndDisplayItemPromoAndUserLocation_whenICallViewDidLoad_thenICallUpdatePromo() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledUpdatePromo == true)
    }
    
    func test_givenAPromotionBOWithStoresAndDisplayItemPromo_whenICallViewDidLoadAndLocationManagerGPSFail_thenICallUpdatePromo() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledUpdatePromo == true)
    }
    
    func test_givenAPromotionBOWithNoStoresAndUserLocation_whenICallViewDidLoad_thenINotCallUpdatePromo() {
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceNoStores = true
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO?.stores = nil
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledUpdatePromo == false)
    }
    
    func test_givenAPromotionBOWithStores_whenICallViewDidLoadAndLocationManagerGPSFail_thenINotCallUpdatePromo() {
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceNoStores = true
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO?.stores = nil
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledUpdatePromo == false)
    }
    
    func test_givenAPromotionBOWithStoresAndDisplayItemPromoAndUserLocation_whenICallViewDidLoad_thenDisplayItemUpdatePromoMatch() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        let displayItemPromo = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionBO, userLocation: GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0))
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.displayItemUpdatePromo == displayItemPromo)
    }
    
    func test_givenAPromotionBOWithStoresAndDisplayItemPromo_whenICallViewDidLoadAndLocationManagerGPSFail_thenDisplayItemUpdatePromoMatch() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        dummyLocationManager.locationErrorType = .gpsFail
        let displayItemPromo = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionBO, userLocation: nil)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.displayItemUpdatePromo == displayItemPromo)
    }
    
    // MARK: - ReloadInfo
    
    // MARK: - ReloadInfo - isCalledGetLocation

    func test_givenAPromotion_whenICallReloadInfoAndHaveCommerceDescriptionAndWebPage_thenCallGetLocation() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(dummyLocationManager.isCalledGetLocation == true)
    }
    
    func test_givenAPromotion_whenICallReloadInfoAndHaveSummaryDescription_thenNotCallLocationManagerGetLocation() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.displayItemPromoDetail = DisplayItemPromotionDetail.generateDisplayItemPromotionDetail(fromPromotion: PromotionsGenerator.getPromotionDetail())
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(dummyLocationManager.isCalledGetLocation == false)
    }
    
    // MARK: - ReloadInfo - isCalledShowPromoStore
    
    func test_givenAPromotionAndUserLocation_whenICallReloadInfoAndHaveCommerceDescriptionAndWebPage_thenCallViewShowPromoStore() {
        
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        
        sut.view = dummyView
        sut.interactor = dummyInteractor
        
        // given
        sut.promotionBO = promotionBO
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(dummyView.isCalledShowPromoStore == true)
    }
    
    func test_givenAPromotion_whenICallReloadInfoAndHaveCommerceDescriptionAndWebPageAndLocationManagerGPSFail_thenCallViewShowPromoStore() {
        
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        
        sut.view = dummyView
        sut.interactor = dummyInteractor
        
        // given
        sut.promotionBO = promotionBO
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(dummyView.isCalledShowPromoStore == true)
    }
    
    func test_givenAPromotionAndUserLocation_whenICallReloadInfoAndHaveCommerceDescriptionAndNotWebPage_thenCallViewShowPromoStore() {
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceNoWebPage = true
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(dummyView.isCalledShowPromoStore == true)
    }
    
    func test_givenAPromotion_whenICallReloadInfoAndHaveCommerceDescriptionAndNotWebPageAndLocationManagerGPSFail_thenCallViewShowPromoStore() {
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceNoWebPage = true
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(dummyView.isCalledShowPromoStore == true)
    }
    
    func test_givenAPromotionAndUserLocation_whenICallReloadInfoAndHaveNotCommerceDescriptionAndWebPage_thenCallViewShowPromoStore() {
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceNotCommerceDescription = true
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(dummyView.isCalledShowPromoStore == true)
    }
    
    func test_givenAPromotion_whenICallReloadInfoAndHaveNotCommerceDescriptionAndWebPageAndLocationManagerGPSFail_thenCallViewShowPromoStore() {
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceNotCommerceDescription = true
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(dummyView.isCalledShowPromoStore == true)
    }
    
    func test_givenAPromotionAndUserLocation_whenICallReloadInfoNoHaveCommerceDescriptionAndNoWebPageAndNoStores_thenCallViewShowPromoStore() {
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceNoDescriptionAndWeb = true
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(dummyView.isCalledShowPromoStore == false)
    }
    
    func test_givenAPromotion_whenICallReloadInfoNoHaveCommerceDescriptionAndNoWebPageAndNoStoresAndLocationManagerGPSFail_thenCallViewShowPromoStore() {
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceNoDescriptionAndWeb = true
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(dummyView.isCalledShowPromoStore == false)
    }
    
    // MARK: - ReloadInfo - isCalledHidePromoStore

    func test_givenAPromotion_whenICallReloadInfoAndNoHaveCommerceDescriptionAndNoWebPageAndNoStores_thenCallViewHidePromoStore() {

        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceNoDescriptionAndWeb = true
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetailWithoutDescriptionAndWeb()
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.reloadInfo()

        // then
        XCTAssert(dummyView.isCalledHidePromoStore == true)
    }
    
    func test_givenAPromotion_whenICallReloadInfoAndNoHaveCommerceDescriptionAndNoWebPageAndNoStoresAndLocationManagerGPSFail_thenCallViewHidePromoStore() {
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceNoDescriptionAndWeb = true
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetailWithoutDescriptionAndWeb()
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(dummyView.isCalledHidePromoStore == true)
    }
    
    func test_givenAPromotionAndUserLocation_whenICallReloadInfoAndHaveCommerceDescriptionAndWebPage_thenINotCalledHidePromoStore() {
        
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        
        sut.view = dummyView
        sut.interactor = dummyInteractor
        
        // given
        sut.promotionBO = promotionBO
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(dummyView.isCalledHidePromoStore == false)
    }
    
    func test_givenAPromotion_whenICallReloadInfoAndHaveCommerceDescriptionAndWebPageAndLocationManagerGPSFail_thenINotCalledHidePromoStore() {
        
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        
        sut.view = dummyView
        sut.interactor = dummyInteractor
        
        // given
        sut.promotionBO = promotionBO
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(dummyView.isCalledHidePromoStore == false)
    }

    // MARK: - ReloadInfo isCalledHideErrorView

    func test_givenAnyPromotionBO_whenICallReloadInfo_thenICallHideErrorView() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()

        // when
        sut.reloadInfo()

        // then
        XCTAssert(dummyView.isCalledHideErrorView == true)
    }
    
    // MARK: - ReloadInfo isCalledShowSkeleton

    func test_givenAnyPromotionBO_whenICallReloadInfo_thenICallViewShowSkeleton() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()

        // when
        sut.reloadInfo()

        // then
        XCTAssert(dummyView.isCalledShowSkeleton == true)
    }
    
    // MARK: - ReloadInfo - isCalledHideSkeleton

    func test_givenAPromotion_whenICallReloadInfoAndDisplayItemPromoDetailIsFilled_thenICallViewHideSkeleton() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        sut.displayItemPromoDetail = DisplayItemPromotionDetail.generateDisplayItemPromotionDetail(fromPromotion: promotionBO)
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(dummyView.isCalledHideSkeleton == true)
    }
    
    func test_givenAPromotionBOAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallReloadInfoAndLocationManagerGPSFail_thenICallHideSkeleton() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(dummyView.isCalledHideSkeleton == true)
    }
    
    func test_givenAPromotionBOAndUserLocationAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallReloadInfoAndInteractorReturnsErrorDifferentThan403_thenICallViewHideSkeleton() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        let promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]
        
        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true
        
        // given
        sut.interactor = dummyInteractor
        sut.promotionBO = promotionBO
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(dummyView.isCalledHideSkeleton == true)
    }
    
    func test_givenAPromotionBOAndUserLocationAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallReloadInfo_thenICallHideSkeleton() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(dummyView.isCalledHideSkeleton == true)
    }
    
    // MARK: - ReloadInfo - isCalledShowPromoDetail
    
    func test_givenAPromotion_whenICallReloadInfoAndDisplayItemPromoDetailIsFilled_thenICallViewShowPromoDetail() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        sut.displayItemPromoDetail = DisplayItemPromotionDetail.generateDisplayItemPromotionDetail(fromPromotion: promotionBO)
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(dummyView.isCalledShowPromoDetail == true)
    }
    
    func test_givenAPromotionBOAndUserLocationAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallReloadInfo_thenCallViewShowPromoDetail() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(dummyView.isCalledShowPromoDetail == true)
    }
    
    func test_givenAPromotionBOAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallReloadInfoAndLocationManagerGPSFail_thenCallViewShowPromoDetail() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(dummyView.isCalledShowPromoDetail == true)
    }
    
    // MARK: - ReloadInfo - isCalledProvidePromotionByParams

    func test_givenAPromotion_whenICallReloadInfoAndDisplayItemPromoDetailIsFilledAndLocationManagerGPSFail_thenICallViewShowPromoDetailAndInteractorProvidePromotionByParamsWithThePromotionIdAndDefaultLocationOnShoppingParamsTransport() {
        
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        
        sut.view = dummyView
        sut.interactor = dummyInteractor
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        dummyLocationManager.locationErrorType = .gpsFail
        
        let locationParam = ShoppingParamLocation(geolocation: GeoLocationBO(withLatitude: sut.defaultLocation.latitude, withLongitude: sut.defaultLocation.longitude), distanceParam: ShoppingDistanceParam(distance: sut.defaultDistance, lengthType: sut.lengthType))
        let expands = ShoppingExpands(stores: true, cards: true, contactDetails: true)
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionByParams == true)
        XCTAssert(dummyInteractor.paramsSent == ShoppingDetailParamsTransport(idPromo: sut.promotionBO!.id, location: locationParam, expands: expands))
    }
    
    func test_givenAPromotionAndUserLocation_whenICallReloadInfoAndDisplayItemPromoDetailIsFilled_thenICallViewShowPromoDetailAndInteractorProvidePromotionByParamsWithThePromotionIdAndDefaultLocationOnShoppingParamsTransport() {
        
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        
        sut.view = dummyView
        sut.interactor = dummyInteractor
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        let locationParam = ShoppingParamLocation(geolocation: GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0), distanceParam: ShoppingDistanceParam(distance: sut.defaultDistance, lengthType: sut.lengthType))
        let expands = ShoppingExpands(stores: true, cards: true, contactDetails: true)
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionByParams == true)
        XCTAssert(dummyInteractor.paramsSent == ShoppingDetailParamsTransport(idPromo: sut.promotionBO!.id, location: locationParam, expands: expands))
    }
    
    func test_givenAPromotionBOAndUserLocationAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallReloadInfo_thenICallProvidePromotionWithStoresByParams() {
        
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        let promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]
        
        sut.view = dummyView
        sut.interactor = dummyInteractor
        
        // given
        sut.promotionBO = promotionBO
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionByParams == true)
    }
    
    func test_givenAPromotionBOAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallReloadInfoAndLocationManagerGPSFail_thenICallProvidePromotionWithStoresByParams() {
        
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        let promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]
        
        sut.view = dummyView
        sut.interactor = dummyInteractor
        
        // given
        sut.promotionBO = promotionBO
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionByParams == true)
    }
    
    func test_givenAPromotionBOAndDisplayItemPromoDetailSumamaryDescription_whenICallReloadInfo_thenINotCallProvidePromotionWithStoresByParams() {
        
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        
        sut.view = dummyView
        sut.interactor = dummyInteractor
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        sut.displayItemPromoDetail = DisplayItemPromotionDetail.generateDisplayItemPromotionDetail(fromPromotion: promotionBO)
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionByParams == false)
    }
    
    // MARK: - ReloadInfo isCalledShowPromoDetailImage
    
    func test_givenPromotionBOWithImageDetailSaved_whenICallReloadInfo_thenICallViewShowPromoDetailImage() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO?.imageDetailSaved = UIImage()
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(dummyView.isCalledShowPromoDetailImage == true)
        
    }
    
    func test_givenPromotionBOWithNoImageDetailSaved_whenICallReloadInfoAndInteractorProvideImageFail_thenICallShowPromoDetailImageWithImageDefault() {
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceError = true
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let imageDefault = UIImage(named: ConstantsImages.Promotions.default_promotion_image)
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO?.imageDetailSaved = imageDefault
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(dummyView.isCalledShowPromoDetailImage == true)
        XCTAssert(dummyView.promoImage == imageDefault)
    }
    
    func test_givenPromotionBOWithImageDetailSaved_whenICallReloadInfo_thenICallViewShowPromoDetailImageWithTheImageSaved() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        let image = UIImage()
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO?.imageDetailSaved = image
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(dummyView.promoImage == image)
    }
    
    // MARK: - ReloadInfo - displayItemPromo
    
    func test_givenAPromotionBO_whenICallReloadInfo_thenCreateDisplayItem() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(sut.displayItemPromo.commerceName == sut.promotionBO?.commerceInformation.name.uppercased())
        XCTAssert(sut.displayItemPromo.descriptionPromotion == sut.promotionBO?.name)
        XCTAssert(sut.displayItemPromo.benefit == sut.promotionBO?.benefit)
        XCTAssert(sut.displayItemPromo.promotionBO == sut.promotionBO)
        XCTAssert(sut.displayItemPromo.imageDetail == sut.promotionBO?.imageDetail)
        XCTAssert(sut.displayItemPromo.imageList == sut.promotionBO?.imageList)
        XCTAssert(sut.displayItemPromo.imagePush == sut.promotionBO?.imagePush)
    }
    
    func test_givenAPromotionBO_whenICallReloadInfo_thenDisplayItemPromotionMatch() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        let location = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)
        sut.userLocation = location
        let displayItemPromo = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: sut.promotionBO!, userLocation: location)
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(sut.displayItemPromo == displayItemPromo)
    }
    
    func test_givenPromotionBOWithNoImageDetailSaved_whenICallReloadInfoAndInteractorProvideImage_thenIHaveImageDetailSavedWithInteractorImage() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(sut.displayItemPromo.promotionBO?.imageDetailSaved == dummyInteractor.imagePromo)
    }
    
    // MARK: - ReloadInfo - displayItemPromoDetail
    
    func test_givenAPromotionBOAndUserLocationAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallReloadInfo_thenCreateDisplayItemDetailIsFilled() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(sut.displayItemPromoDetail.commerceDescription == sut.promotionBO?.commerceInformation.description)
        XCTAssert(sut.displayItemPromoDetail.legalConditions == sut.promotionBO?.legalConditions?.text)
        XCTAssert(sut.displayItemPromoDetail.legalConditionsURL == sut.promotionBO?.legalConditions?.url)
        XCTAssert(sut.displayItemPromoDetail.commerceName == sut.promotionBO?.commerceInformation.name)
        XCTAssert(sut.displayItemPromoDetail.commerceWebPage == sut.promotionBO?.commerceInformation.web)
    }
    
    func test_givenAPromotionBOAndUserLocationAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallReloadInfo_thenDisplayItemPromotionDetailMatch() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        let displayItemPromoDetail = DisplayItemPromotionDetail.generateDisplayItemPromotionDetail(fromPromotion: promotionBO)
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(sut.displayItemPromoDetail == displayItemPromoDetail)
    }
    
    // MARK: - ReloadInfo - AssociatedTradeMap
    
    func test_givenAPromotionBOWithStoresAndDisplayItemPromoDetailSumamaryDescriptionNilAndUserLocation_whenICallReloadInfo_thenICallShowAssociatedTradeMap() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(dummyView.isCalledShowAssociatedTradeMap == true)
    }
    
    func test_givenAPromotionBOWithStoresAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallReloadInfoAndLocationManagerGPSFail_thenICallShowAssociatedTradeMap() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(dummyView.isCalledShowAssociatedTradeMap == true)
    }
    
    func test_givenAPromotionBOWithoutStoresAndDisplayItemPromoDetailSumamaryDescriptionNilAndUserLocation_whenICallReloadInfo_thenINotCallShowAssociatedTradeMap() {
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceNoStores = true
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(dummyView.isCalledShowAssociatedTradeMap == false)
    }
    
    func test_givenAPromotionBOWithoutStoresAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallReloadInfoAndLocationManagerGPSFail_thenINotCallShowAssociatedTradeMap() {
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceNoStores = true
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(dummyView.isCalledShowAssociatedTradeMap == false)
    }
    
    func test_givenAPromotionBOWithStoresAndUserLocationAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallReloadInfo_thenICallShowAssociatedTradeMapWithDisplayDataFilled() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(dummyView.associatedTradeMapDisplayData != nil)
    }
    
    func test_givenAPromotionBOWithStoresAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallReloadInfoAndLocationManagerGPSFail_thenICallShowAssociatedTradeMapWithDisplayDataFilled() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(dummyView.associatedTradeMapDisplayData != nil)
    }
    
    func test_givenAPromotionBOWithStoresAndUserLocationDisplayItemPromoDetailSumamaryDescriptionNil_whenICallReloadInfo_thenICallShowAssociatedTradeMapWithRightDisplayData() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        let displayData = AssociatedTradeMapDisplayData(promotion: promotionBO, userLocation: GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0))
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(dummyView.associatedTradeMapDisplayData == displayData)
    }
    
    func test_givenAPromotionBOWithStoresDisplayItemPromoDetailSumamaryDescriptionNil_whenICallReloadInfoAndLocationManagerGPSFail_thenICallShowAssociatedTradeMapWithRightDisplayData() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        
        dummyLocationManager.locationErrorType = .gpsFail
        let displayData = AssociatedTradeMapDisplayData(promotion: promotionBO, userLocation: sut.userLocationA)
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(dummyView.associatedTradeMapDisplayData == displayData)
    }
    
    // MARK: - ReloadInfo - isCalledHideAssociatedTradeMap
    
    func test_givenAPromotionBOWithStoresAndDisplayItemPromoDetailSumamaryDescriptionNilAndUserLocation_whenICallReloadInfo_thenINotCallHideAssociatedTradeMap() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(dummyView.isCalledHideAssociatedTradeMap == false)
    }
    
    func test_givenAPromotionBOWithStoresAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallReloadInfoAndLocationManagerGPSFail_thenINotCallHideAssociatedTradeMap() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(dummyView.isCalledHideAssociatedTradeMap == false)
    }
    
    func test_givenAPromotionBOWithoutStoresAndDisplayItemPromoDetailSumamaryDescriptionNilAndUserLocation_whenICallReloadInfo_thenICallHideAssociatedTradeMap() {
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceNoStores = true
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(dummyView.isCalledHideAssociatedTradeMap == true)
    }
    
    func test_givenAPromotionBOWithoutStoresAndDisplayItemPromoDetailSumamaryDescriptionNil_whenICallReloadInfoAndLocationManagerGPSFail_thenICallHideAssociatedTradeMap() {
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceNoStores = true
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(dummyView.isCalledHideAssociatedTradeMap == true)
    }
    
    // MARK: - ReloadInfo - updatePromo
    
    func test_givenAPromotionBOWithStoresAndDisplayItemPromoAndUserLocation_whenICallReloadInfo_thenICallUpdatePromo() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(dummyView.isCalledUpdatePromo == true)
    }
    
    func test_givenAPromotionBOWithStoresAndDisplayItemPromo_whenICallReloadInfoAndLocationManagerGPSFail_thenICallUpdatePromo() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(dummyView.isCalledUpdatePromo == true)
    }
    
    func test_givenAPromotionBOWithNoStoresAndUserLocation_whenICallReloadInfo_thenINotCallUpdatePromo() {
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceNoStores = true
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO?.stores = nil
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(dummyView.isCalledUpdatePromo == false)
    }
    
    func test_givenAPromotionBOWithStores_whenICallReloadInfoAndLocationManagerGPSFail_thenINotCallUpdatePromo() {
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceNoStores = true
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO?.stores = nil
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(dummyView.isCalledUpdatePromo == false)
    }
    
    func test_givenAPromotionBOWithStoresAndDisplayItemPromoAndUserLocation_whenICallReloadInfo_thenDisplayItemUpdatePromoMatch() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        let displayItemPromo = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionBO, userLocation: GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0))
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(dummyView.displayItemUpdatePromo == displayItemPromo)
    }
    
    func test_givenAPromotionBOWithStoresAndDisplayItemPromo_whenICallReloadInfoAndLocationManagerGPSFail_thenDisplayItemUpdatePromoMatch() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        dummyLocationManager.locationErrorType = .gpsFail
        let displayItemPromo = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionBO, userLocation: nil)
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(dummyView.displayItemUpdatePromo == displayItemPromo)
    }
    
    // MARK: - showStoreList
    
    func test_givenAPromotionBOAndUserLocation_whenICallShowStoreList_thenICallBusManagerNavigateScreenWithAppropiatedTag() {
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]
        sut.userLocation = GeoLocationBO(withLatitude: 1.0, withLongitude: 2.0)
        
        // when
        sut.showStoreList()
        
        // then
        XCTAssert(dummyBusManager.isCalledNavigateToStoreList == true)
    }
    
    func test_givenAPromotionBOAndUserLocation_whenICallShowStoreList_thenICallBusManagerNavigateScreenWithPromotionDetailTransportWithCorrectValues() {
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]
        sut.userLocation = GeoLocationBO(withLatitude: 1.0, withLongitude: 2.0)
        
        // when
        sut.showStoreList()
        
        // then
        XCTAssert(dummyBusManager.dummyDetailValue.promotionBO == sut.promotionBO)
        XCTAssert(dummyBusManager.dummyDetailValue.promotionBO?.commerceInformation.name == sut.promotionBO?.commerceInformation.name)
        XCTAssert(dummyBusManager.dummyDetailValue.userLocation == sut.userLocationA)
    }
    
    // MARK: - ViewDidLoad - LocationManager

    func test_givenAUserLocation_whenICallViewDidLoadedAndLocationManagerSuccess_thenUserLocationShouldNotBeNil() {

        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        
        sut.view = dummyView
        sut.interactor = dummyInteractor
        
        // given
        sut.promotionBO = promotionBO
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)

        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(sut.userLocationA != nil)
    }
    
    func test_givenAUserLocationAndPromotionBO_whenICallViewDidLoadAndLocationManagerSuccess_thenUserLocationShouldMatchWithReturned() {
        
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        
        sut.view = dummyView
        sut.interactor = dummyInteractor
        
        // given
        sut.promotionBO = promotionBO
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.viewDidLoad()
        
        // then
        // then
        XCTAssert(sut.userLocationA?.latitude == 3.0)
        XCTAssert(sut.userLocationA?.longitude == 4.0)
    }
    
    // MARK: - ViewDidLoad - LocationManager --> GpsFail
    
    func test_givenADefaultUserLocationAndPromotionBO_whenICallViewDidLoadAndLocationManagerFail_thenUserLocationShouldBeEmpty() {
        
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        
        sut.view = dummyView
        sut.interactor = dummyInteractor
        
        // given
        sut.promotionBO = promotionBO
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(sut.userLocationA == nil)
    }
    
    // MARK: - ViewDidLoad - LocationManager --> PermissionDenied

    func test_givenADefaultUserLocationAndPromotionBO_whenICallViewDidLoadAndLocationManagerPermissionDenied_thenUserLocationShouldBeNil() {
        
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        
        sut.view = dummyView
        sut.interactor = dummyInteractor
        
        // given
        sut.promotionBO = promotionBO
        dummyLocationManager.locationErrorType = .permissionDenied
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(sut.userLocationA == nil)
    }
    
    // MARK: - ViewDidLoad - LocationManager --> GPSOff

    func test_givenADefaultUserLocationAndPromotionBO_whenICallViewDidLoadAndLocationManagerGPSOff_thenUserLocationShouldBeNil() {
        
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        
        sut.view = dummyView
        sut.interactor = dummyInteractor
        
        // given
        sut.promotionBO = promotionBO
        dummyLocationManager.locationErrorType = .gpsOff

        // when
        sut.viewDidLoad()

        // then
        XCTAssert(sut.userLocationA == nil)
    }
    
    // MARK: - ReloadInfo - LocationManager
    
    func test_givenAUserLocation_whenICallReloadInfoAndLocationManagerSuccess_thenUserLocationShouldNotBeNil() {
        
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        
        sut.view = dummyView
        sut.interactor = dummyInteractor
        
        // given
        sut.promotionBO = promotionBO
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(sut.userLocationA != nil)
    }
    
    func test_givenAUserLocationAndPromotionBO_whenICallReloadInfoAndLocationManagerSuccess_thenUserLocationShouldMatchWithReturned() {
        
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        
        sut.view = dummyView
        sut.interactor = dummyInteractor
        
        // given
        sut.promotionBO = promotionBO
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.reloadInfo()
        
        // then
        // then
        XCTAssert(sut.userLocationA?.latitude == 3.0)
        XCTAssert(sut.userLocationA?.longitude == 4.0)
    }
    
    // MARK: - ReloadInfo - LocationManager --> GpsFail
    
    func test_givenADefaultUserLocationAndPromotionBO_whenICallReloadInfoAndLocationManagerFail_thenUserLocationShouldBeNil() {
        
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        
        sut.view = dummyView
        sut.interactor = dummyInteractor
        
        // given
        sut.promotionBO = promotionBO
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(sut.userLocationA == nil)
    }
    
    // MARK: - ReloadInfo - LocationManager --> PermissionDenied
    
    func test_givenADefaultUserLocationAndPromotionBO_whenICallReloadInfoAndLocationManagerPermissionDenied_thenUserLocationShouldBeNil() {
        
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        
        sut.view = dummyView
        sut.interactor = dummyInteractor
        
        // given
        sut.promotionBO = promotionBO
        dummyLocationManager.locationErrorType = .permissionDenied
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(sut.userLocationA == nil)
    }
    
    // MARK: - ReloadInfo - LocationManager --> GPSOff
    
    func test_givenADefaultUserLocationAndPromotionBO_whenICallReloadInfoAndLocationManagerGPSOff_thenUserLocationShouldBeNil() {
        
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        
        sut.view = dummyView
        sut.interactor = dummyInteractor
        
        // given
        sut.promotionBO = promotionBO
        dummyLocationManager.locationErrorType = .gpsOff
        
        // when
        sut.reloadInfo()
        
        // then
        XCTAssert(sut.userLocationA == nil)
    }
    
    func test_givenAPromotion_whenICallShowWantThisOfferPromotion_thenIHavePromotionUrl() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        sut.displayItemPromoDetail = DisplayItemPromotionDetail.generateDisplayItemPromotionDetail(fromPromotion: promotionBO)
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.viewDidLoad()
        
        // then
        
        XCTAssert(dummyView.isCalledShowWantThisOffer == true)
    }
    
    func test_givenAPromotion_whenICallShowWantThisOfferPromotionAndLocationManagerGPSFail_thenIHavePromotionUrl() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        sut.displayItemPromoDetail = DisplayItemPromotionDetail.generateDisplayItemPromotionDetail(fromPromotion: promotionBO)
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.viewDidLoad()
        
        // then
        
        XCTAssert(dummyView.isCalledShowWantThisOffer == true)
    }
    
    func test_givenAPromotion_whenICallViewLoadedAndCallInteractorForceNoPromotionUrlAndLocationManagerSuccess_thenICalledHideWantThisOffer() {
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceNoPromotionUrl = true
        
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledHideWantThisOffer == true)
    }
    
    func test_givenAPromotion_whenICallViewLoadedAndCallInteractorForceNoPromotionUrlAndLocationManagerGPSFail_thenICalledHideWantThisOffer() {
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceNoPromotionUrl = true
        
        sut.interactor = dummyInteractor
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        sut.promotionBO = promotionBO
        dummyLocationManager.locationErrorType = .gpsFail
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledHideWantThisOffer == true)
    }

    // MARK: - DummyData

    class DummyInteractor: ShoppingDetailInteractorProtocol {

        var forceError = false
        var forceNoDescriptionAndWeb = false
        var forceNotCommerceDescription = false
        var forceNoWebPage = false
        var forceNoStores = false
        var forceNoPromotionUrl = false
        var presenter: ShoppingDetailPresenterA<DummyView>?
        var errorBO: ErrorBO?
        var promotionBO: PromotionBO!
        
        var isCalledProvidePromotionByParams = false
        var isCalledProvideImage = false

        var imageUrlString: String?
        var imagePromo: UIImage?
        var paramsSent: ShoppingDetailParamsTransport?
        
        func providePromotion(byParams params: ShoppingDetailParamsTransport) -> Observable<ModelBO> {
            isCalledProvidePromotionByParams = true
            paramsSent = params
            
            if promotionBO == nil {

                if forceNoDescriptionAndWeb {
                    
                    promotionBO = PromotionsGenerator.getPromotionDetailWithoutDescriptionAndWeb()
                } else {

                    promotionBO = PromotionsGenerator.getPromotionDetail()

                    if forceNoWebPage {
                        promotionBO.commerceInformation.web = nil
                    }

                    if forceNotCommerceDescription {
                        promotionBO.commerceInformation.description = nil
                    }

                    if forceNoStores {
                        promotionBO.stores = nil
                    }
                    
                    if forceNoPromotionUrl {
                        promotionBO.promotionUrl = nil
                    }
                }
            }

            if forceError {

                if errorBO == nil {
                    errorBO = ErrorBO()
                    errorBO!.isErrorShown = false
                }

                return Observable.error(ServiceError.GenericErrorBO(error: errorBO!))
            }

            return Observable <ModelBO>.just(promotionBO!)
        }
    }

    class DummyView: ShoppingDetailViewProtocolA, ViewProtocol {

        var isCalledShowPromo = false
        var isCalledShowPromotionType = false
        var isCalledShowPromoDetail = false
        var isCalledShowPromoDetailImage = false
        var isCalledUpdatePromoDetail = false
        var isCalledShowError = false
        var isCalledShowSkeleton = false
        var isCalledHideSkeleton = false
        var isCalledShowErrorView = false
        var isCalledHideErrorView = false
        var isCalledShowPromoStore = false
        var isCalledHidePromoStore = false
        var isCalledShowAssociatedTradeMap = false
        var isCalledHideAssociatedTradeMap = false
        var isCalledCallToPhone = false
        var isCalledSendEmail = false
        var isCalledLaunchMap = false
        var isCalledSetDistanceAndDateToShow = false
        var isCalledShowWantThisOffer = false
        var isCalledHideWantThisOffer = false

        var displayItem: DisplayItemPromotion!
        var displayItemDetail: DisplayItemPromotionDetail!
        var promoImage: UIImage!
        var associateCommerceDisplayData: AssociateCommerceDisplayData!
        var associatedTradeMapDisplayData: AssociatedTradeMapDisplayData?
        var displayDataHelp: HelpDisplayData?

        var phoneSent: String?
        var emailSent: String?
        var destinationCoordinateSent: CLLocationCoordinate2D?
        var originCoordinateSent: CLLocationCoordinate2D?
        var distanceAndDateToShow: String?
        var isCalledHelpView = false
        
        var isCalledUpdatePromo = false
        var displayItemUpdatePromo: DisplayItemPromotion!

        var promotionContactDisplayData: ContactUsDisplayData?
        var isCalledContactUsView = false
        
        func showPromoStore(withAssociateCommerceDisplay displayAssociateCommerce: AssociateCommerceDisplayData) {
            
            isCalledShowPromoStore = true
            associateCommerceDisplayData = displayAssociateCommerce
        }

        func hidePromoStore() {

            isCalledHidePromoStore = true
        }

        func showAssociatedTradeMap(withDisplayData displayData: AssociatedTradeMapDisplayData) {

            associatedTradeMapDisplayData = displayData
            isCalledShowAssociatedTradeMap = true
        }

        func hideAssociatedTradeMap() {

            isCalledHideAssociatedTradeMap = true
        }

        func callToPhone(withPhone phone: String) {

            isCalledCallToPhone = true
            phoneSent = phone
        }

        func sendEmail(withEmail email: String) {

            isCalledSendEmail = true
            emailSent = email
        }

        func launchMap(withDestinationCoordinate destinationCoordinate: CLLocationCoordinate2D, andOriginCoordinate originCoordinate: CLLocationCoordinate2D?) {

            isCalledLaunchMap = true
            destinationCoordinateSent = destinationCoordinate
            originCoordinateSent = originCoordinate
        }

        func showError(error: ModelBO) {
            isCalledShowError = true
        }

        func changeColorEditTexts() {
        }

        func showPromo(withDisplayItemPromotion displayItemPromotion: DisplayItemPromotion) {
            isCalledShowPromo = true
            displayItem = displayItemPromotion
        }
        
        func updatePromo(withDisplayItemPromotion displayItemPromotion: DisplayItemPromotion) {
            
            isCalledUpdatePromo = true
            displayItemUpdatePromo = displayItemPromotion
        }

        func showPromotionType(withPromotionTypeDisplay promotionTypeDisplay: PromotionTypeDisplayData) {
            isCalledShowPromotionType = true
        }

        func showPromoDetail(withDisplayItemPromotionDetail displayItemPromotionDetail: DisplayItemPromotionDetail) {
            isCalledShowPromoDetail = true
            displayItemDetail = displayItemPromotionDetail
        }

        func showPromoDetailImage(withPromoDetailImage promoDetailImage: UIImage?) {
            isCalledShowPromoDetailImage = true
            promoImage = promoDetailImage
        }

        func updatePromoDetail(withDisplayItemPromotionDetail displayItemPromotionDetail: DisplayItemPromotionDetail) {
            isCalledUpdatePromoDetail = true
        }

        func setDistanceAndDateToShow(withString distanceAndDateToShow: String) {
            isCalledSetDistanceAndDateToShow = true
            self.distanceAndDateToShow = distanceAndDateToShow
        }

        func showSkeleton() {
            isCalledShowSkeleton = true
        }

        func hideSkeleton() {
            isCalledHideSkeleton = true
        }

        func showErrorView() {
            isCalledShowErrorView = true
        }

        func hideErrorView() {
            isCalledHideErrorView = true
        }

        func showWantThisOffer() {
            
            isCalledShowWantThisOffer = true
        }
        
        func hideWantThisOffer() {
            
            isCalledHideWantThisOffer = true
        }

        func showHelp(withDisplayData displayData: HelpDisplayData) {
            displayDataHelp = displayData
            isCalledHelpView = true
        }
        
        func showPromoContact(withPromoContactDisplay promotionContactDisplay: ContactUsDisplayData) {
            
            promotionContactDisplayData = promotionContactDisplay
            isCalledContactUsView = true
        }

    }

    class DummyBusManager: BusManager {

        var isCalledNavigateToWebPageScreen = false
        var isCalledPublishDataDetailWithTagPromotionsBO = false
        var isCalledNavigateToStoreList = false
        
        var dummyDetailValue = PromotionDetailTransport()
        var dummyWebValue = WebTransport()

        override init() {
            super.init()
            routerFactory = RouterInitFactory()
        }

        override func navigateScreen<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {
            if tag == "shopping-detail-tag" && event === ShoppingDetailPageReaction.EVENT_NAV_TO_WEBPAGE_SCREEN {
                isCalledNavigateToWebPageScreen = true
                isCalledPublishDataDetailWithTagPromotionsBO = true
                dummyWebValue = values as! WebTransport
            } else if tag == "shopping-detail-tag" && event === ShoppingDetailPageReactionA.EVENT_NAV_TO_STORE_LIST_SCREEN {
                isCalledNavigateToStoreList = true
                dummyDetailValue = values as! PromotionDetailTransport
            }
        }
    }
}
