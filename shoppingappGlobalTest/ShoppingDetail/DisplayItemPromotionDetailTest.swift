//
//  DisplayItemPromotionDetailTest.swift
//  shoppingapp
//
//  Created by Luis Monroy on 06/04/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class DisplayItemPromotionDetailTest: XCTestCase {

    func test_givenAPromotionBO_whenICallGenerateDisplayItemPromotionDetail_thenDisplayDataPanShouldBeMatchWithPromotionBO() {

        //given
        let promotionBO = PromotionsGenerator.getPromotionDetail()

        //When
        let displayData = DisplayItemPromotionDetail.generateDisplayItemPromotionDetail(fromPromotion: promotionBO)

        // then
        XCTAssert(displayData.summaryDescription == "Con Aéropostale obtiene puntos al doble" )
        XCTAssert(displayData.extendedDescription == "En Boutiques Aéropostale obtén Puntos Bancomer al doble con tu tarjeta de Crédito Bancomer.")
        XCTAssert(displayData.legalConditions == "Promoción válida en Boutiques Aeropostale de la República Mexicana. Consulta sucursales participantes en http://aeropostalemexico.mx/boutiques/Más información en: 52262663 y lada sin costo 01(55) 52262663 opciones 1-0.Participan todas las Tarjetas de Crédito Bancomer que cuentan con el Programa Vida Bancomer Puntos.")
        XCTAssert(displayData.legalConditionsURL == "https://drive.google.com/open?id=1MX_JoYzjKz5ug2lkrR0369qcDAbouqXl")
        XCTAssert(displayData.commerceName == "Aéropostale")
        XCTAssert(displayData.commerceDescription == "Aéropostale, o simplemente como Aéro, es una empresa textil de Estados Unidos que vende ropa informal para jóvenes en más de 800 locales en todo los Estados Unidos y Canadá.")
        XCTAssert(displayData.commerceWebPage == "http://aeropostalemexico.mx/")
    }
}
