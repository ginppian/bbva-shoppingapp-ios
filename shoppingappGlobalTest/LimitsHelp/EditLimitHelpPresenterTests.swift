//
//  EditLimitHelpPresenterTests.swift
//  shoppingappMXTest
//
//  Created by Magdali Grajales on 17/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#endif

class EditLimitHelpPresenterTests: XCTestCase {

    var sut: EditLimitHelpPresenter<LimitsHelpViewDummy>!
    var dummyBusManager: DummyBusManager!
    var dummyview: LimitsHelpViewDummy!
    
    override func setUp() {
        super.setUp()
        dummyBusManager = DummyBusManager()
        sut = EditLimitHelpPresenter<LimitsHelpViewDummy>(busManager: dummyBusManager)
        
        dummyview = LimitsHelpViewDummy()
        sut.view = dummyview
    }
    
    // MARK: - setModel
    
    func test_givenCard_whenICallSetModel_thenCardTypeIdSet() {
       
        // given
        let card = CardsGenerator.getCardBOWithAlias().cards[0]
        
        // when
        sut.setModel(model: card.cardType)
        
        // then
        XCTAssert(sut.cardType != nil)
    }
    
    func test_givenCard_whenICallSetModel_thenCardTypeId() {
    
        // given
        let card = CardsGenerator.getCardBOWithAlias().cards[0]
        
        // when
        sut.setModel(model: card.cardType)
        
        // then
        XCTAssert(sut.cardType == card.cardType.id)
    }
    
    // MARK: - viewLoaded
    
    func test_givenCardTypeOnSetModel_whenICallViewLoaded_thenDisplayDataBeFill() {
        
        // given
        
        // when
        sut.viewLoaded()
        
        // then
        XCTAssert(sut.displayData != nil)
    }
    
    func test_givenCardTypeOnSetModel_whenICallViewLoaded_thenDisplayDataMatch() {
        
        // given
        let editLimitHelpDisplayData = EditLimitHelpDisplayData(cardType: sut.cardType!)
        
        // when
        sut.viewLoaded()
        
        // then
        XCTAssert(sut.displayData == editLimitHelpDisplayData)
    }
    
    func test_givenCardTypeOnSetModel_whenICallViewLoaded_thenICallShowHelpInformation() {
        
        // given
        
        // when
        sut.viewLoaded()
        
        // then
        XCTAssert(dummyview.isCalledShowHelpInformation == true)
    }
    
    func test_givenCardTypeOnSetModel_whenICallViewLoaded_thenICallShowHelpInformationAndDisplayDataMatch() {
      
        // given
        
        // when
        sut.viewLoaded()
        
        // then
       XCTAssert(dummyview.editLimitHelpDisplayDataSent == sut.displayData)
    }
    
    // MARK: - Dummy classes
    
    class LimitsHelpViewDummy: EditLimitHelpViewProtocol {
        
        var isCalledShowHelpInformation = false
        var editLimitHelpDisplayDataSent: EditLimitHelpDisplayData?
        
        func showHelpInformation(displayData: EditLimitHelpDisplayData) {
            
            isCalledShowHelpInformation = true
            editLimitHelpDisplayDataSent = displayData
        }
        
        func showError(error: ModelBO) {
        }
        
        func changeColorEditTexts() {
        }
    }
    
    class DummyBusManager: BusManager {
        
        override init() {
            super.init()
            routerFactory = RouterInitFactory()
        }
    }
}
