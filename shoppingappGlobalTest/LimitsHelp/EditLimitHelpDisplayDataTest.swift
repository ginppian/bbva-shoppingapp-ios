//
//  EditLimitHelpDisplayDataTest.swift
//  shoppingappMXTest
//
//  Created by Magdali Grajales on 16/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#endif

class EditLimitHelpDisplayDataTest: XCTestCase {

    // MARK: - EditLimitHelpDisplayData - CreditCard
    
    func test_givenCreditCard_whenICreateEditLimitsHelpDisplayData_thenFieldsAreReadWithTheProperKey() {
        
        // given
        let card = CardsGenerator.getCardBOCreditCard().cards[0]
        
        // when
        let displayData = EditLimitHelpDisplayData(cardType: card.cardType.id)
        
        // then
        XCTAssert(displayData.titleOne == NSLocalizedString("KEY_CARD_HELP_LIMITS_CREDIT_CARD_TITLE_1", comment: ""))
    }
    
    func test_givenCreditCard_whenICreateEditLimitsHelpDisplayData_thenDynamicTextsOneMatch() {
        
        // given
        let card = CardsGenerator.getCardBOCreditCard().cards[0]
        let keyRoot = "KEY_CARD_HELP_LIMITS_"
        
        // when
        let displayData = EditLimitHelpDisplayData(cardType: card.cardType.id)
        
        // then
        for index in 1...displayData.textsOne.count {
            XCTAssert(displayData.textsOne[index - 1] == NSLocalizedString(keyRoot + "TEXT_\(index)", comment: ""))
        }
    }
    
    func test_givenCreditCard_whenICreateEditLimitsHelpDisplayData_thenDisclaimerTextIsEmpty() {
        
        // given
        let card = CardsGenerator.getCardBOCreditCard().cards[0]
        
        // when
        let displayData = EditLimitHelpDisplayData(cardType: card.cardType.id)
        
        // then
        XCTAssert(displayData.disclaimerText?.isEmpty == true)
    }
    
    func test_givenCreditCard_whenICreateEditLimitsHelpDisplayData_thenImagesAndTextsMatch() {
        
        // given
        let card = CardsGenerator.getCardBOCreditCard().cards[0]
        
        // when
        let displayData = EditLimitHelpDisplayData(cardType: card.cardType.id)
        
        // then
        XCTAssert(displayData.headerImageName == ConstantsImages.Card.ilu_limits_help)
        XCTAssert(displayData.bulletImageName == ConstantsImages.Common.bullet_icon)
        XCTAssert(displayData.acceptButtonTitle == Localizables.login.key_login_disconnect_understood_text)
    }
    
    // MARK: - EditLimitHelpDisplayData - DebitCard
    
    func test_givenDebitCard_whenICreateEditLimitsHelpDisplayData_thenFieldsAreReadWithTheProperKey() {
        
        // given
        let card = CardsGenerator.getCardBOWithAlias().cards[0]
        
        // when
        let displayData = EditLimitHelpDisplayData(cardType: card.cardType.id)
        
        // then
        XCTAssert(displayData.titleOne == NSLocalizedString("KEY_CARD_HELP_LIMITS_TITLE_1", comment: ""))
    }
    
    func test_givenDebitCard_whenICreateEditLimitsHelpDisplayData_thenDisclaimerTextMatch() {
        
        // given
        let card = CardsGenerator.getCardBOWithAlias().cards[0]
        
        // when
        let displayData = EditLimitHelpDisplayData(cardType: card.cardType.id)
        
        // then
        XCTAssert(displayData.disclaimerText == Localizables.cards.key_card_disclaimer_help_limits)
    }
    
    func test_givenCardAndKeyRootLocalizable_whenICreateEditLimitsHelpDisplayData_thenDynamicTextsMatch() {
        
        // given
        let card = CardsGenerator.getCardBOWithAlias().cards[0]
        
        let keyRoot = "KEY_CARD_HELP_LIMITS_"
        
        // when
        let displayData = EditLimitHelpDisplayData(cardType: card.cardType.id)
        
        // then
        XCTAssert(!displayData.textsOne.isEmpty)
        
        //Dynamic texts
        for index in 1...displayData.textsOne.count {
            XCTAssert(displayData.textsOne[index - 1] == NSLocalizedString(keyRoot + "TEXT_\(index)", comment: ""))
        }
    }
    
    func test_givenDebitCard_whenICreateEditLimitsHelpDisplayData_thenImagesAndTextsMatch() {
        
        // given
        let card = CardsGenerator.getCardBOWithAlias().cards[0]
        
        // when
        let displayData = EditLimitHelpDisplayData(cardType: card.cardType.id)
        
        // then
        XCTAssert(displayData.headerImageName == ConstantsImages.Card.ilu_limits_help)
        XCTAssert(displayData.bulletImageName == ConstantsImages.Common.bullet_icon)
        XCTAssert(displayData.acceptButtonTitle == Localizables.login.key_login_disconnect_understood_text)
    }
}
