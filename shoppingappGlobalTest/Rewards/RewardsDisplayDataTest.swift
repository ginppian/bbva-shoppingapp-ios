//
//  RewardsDisplayDataTest.swift
//  shoppingapp
//
//  Created by ruben.fernandez on 1/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#endif

class RewardsDisplayDataTest: XCTestCase {

    // MARK: - generateRewardsDisplayData

    func test_givenCardsBO_whenICallGeneratePointsDisplayData_thenRewardDisplayDataShowExpiredSkeletonIsTrue() {
        // given
        let cardsBO = CardsGenerator.getCardsWithRewards()

        // when
        let displayData = RewardDisplayData(withCardBO: cardsBO.cards[2])

        // then
        XCTAssert(displayData?.showExpiredSkeleton == true)

    }

    func test_givenCardsBO_whenICallGeneratePointsDisplayData_thenHaveCorrectValuesAlias() {

        // given
        let cardsBO = CardsGenerator.getCardsWithRewards()

        // when
        let displayData2 = RewardDisplayData(withCardBO: cardsBO.cards[2])
        let displayData5 = RewardDisplayData(withCardBO: cardsBO.cards[5])
        let displayData6 = RewardDisplayData(withCardBO: cardsBO.cards[6])
        let displayData7 = RewardDisplayData(withCardBO: cardsBO.cards[7])

        // then
        XCTAssert(RewardDisplayData(withCardBO: cardsBO.cards[0]) == nil)
        XCTAssert(RewardDisplayData(withCardBO: cardsBO.cards[1]) == nil)

        XCTAssert(displayData2?.alias == cardsBO.cards[2].alias)
        XCTAssert(displayData2?.aliasColor == .BBVAWHITE)
        XCTAssert(displayData2?.aliasFont == Tools.setFontBook(size: 16))

        XCTAssert(RewardDisplayData(withCardBO: cardsBO.cards[3]) == nil)
        XCTAssert(RewardDisplayData(withCardBO: cardsBO.cards[4]) == nil)

        XCTAssert(displayData5?.alias == cardsBO.cards[5].alias)
        XCTAssert(displayData5?.aliasColor == .BBVAWHITE)
        XCTAssert(displayData5?.aliasFont == Tools.setFontBook(size: 16))

        XCTAssert(displayData6?.alias == cardsBO.cards[6].alias)
        XCTAssert(displayData6?.aliasColor == .BBVAWHITE)
        XCTAssert(displayData6?.aliasFont == Tools.setFontBook(size: 16))

        XCTAssert(displayData7?.alias == cardsBO.cards[7].alias)
        XCTAssert(displayData7?.aliasColor == .BBVAWHITE)
        XCTAssert(displayData7?.aliasFont == Tools.setFontBook(size: 16))

    }

    func test_givenCardsBO_whenICallGeneratePointsDisplayData_thenHaveCorrectValuesPanFour() {

        // given
        let cardsBO = CardsGenerator.getCardsWithRewards()

        // when
        let displayData2 = RewardDisplayData(withCardBO: cardsBO.cards[2])
        let displayData5 = RewardDisplayData(withCardBO: cardsBO.cards[5])
        let displayData6 = RewardDisplayData(withCardBO: cardsBO.cards[6])
        let displayData7 = RewardDisplayData(withCardBO: cardsBO.cards[7])

        // then
        XCTAssert(RewardDisplayData(withCardBO: cardsBO.cards[0]) == nil)
        XCTAssert(RewardDisplayData(withCardBO: cardsBO.cards[1]) == nil)

        var number: String = cardsBO.cards[2].number
        var last4 = number.suffix(4)
        XCTAssert(displayData2?.panFour == "•"+last4)
        XCTAssert(displayData2?.panFourColor == .BBVAWHITE)
        XCTAssert(displayData2?.panFourFont == Tools.setFontBookItalic(size: 14))

        XCTAssert(RewardDisplayData(withCardBO: cardsBO.cards[3]) == nil)
        XCTAssert(RewardDisplayData(withCardBO: cardsBO.cards[4]) == nil)

        number = cardsBO.cards[5].number
        last4 = number.suffix(4)
        XCTAssert(displayData5?.panFour == "•" + last4)
        XCTAssert(displayData5?.panFourColor == .BBVAWHITE)
        XCTAssert(displayData5?.panFourFont == Tools.setFontBookItalic(size: 14))

        number = cardsBO.cards[6].number
        last4 = number.suffix(4)
        XCTAssert(displayData6?.panFour == "•" + last4)
        XCTAssert(displayData6?.panFourColor == .BBVAWHITE)
        XCTAssert(displayData6?.panFourFont == Tools.setFontBookItalic(size: 14))

        number = cardsBO.cards[7].number
        last4 = number.suffix(4)
        XCTAssert(displayData7?.panFour == "•" + last4)
        XCTAssert(displayData7?.panFourColor == .BBVAWHITE)
        XCTAssert(displayData7?.panFourFont == Tools.setFontBookItalic(size: 14))

    }

    func test_givenCardsBO_whenICallGeneratePointsDisplayData_thenHaveCorrectImage() {

        // given
        let cardsBO = CardsGenerator.getCardsWithRewards()

        // when
        let displayData2 = RewardDisplayData(withCardBO: cardsBO.cards[2])
        let displayData5 = RewardDisplayData(withCardBO: cardsBO.cards[5])
        let displayData6 = RewardDisplayData(withCardBO: cardsBO.cards[6])
        let displayData7 = RewardDisplayData(withCardBO: cardsBO.cards[7])

        // then
        XCTAssert(RewardDisplayData(withCardBO: cardsBO.cards[0]) == nil)
        XCTAssert(RewardDisplayData(withCardBO: cardsBO.cards[1]) == nil)

        XCTAssert(displayData2?.imageCard == cardsBO.cards[2].imageFront)

        XCTAssert(RewardDisplayData(withCardBO: cardsBO.cards[3]) == nil)
        XCTAssert(RewardDisplayData(withCardBO: cardsBO.cards[4]) == nil)

        XCTAssert(displayData5?.imageCard == cardsBO.cards[5].imageFront)
        XCTAssert(displayData6?.imageCard == cardsBO.cards[6].imageFront)
        XCTAssert(displayData7?.imageCard == cardsBO.cards[7].imageFront)

    }

    func test_givenCardsBO_whenICallGeneratePointsDisplayData_thenHaveCorrectPoints() {

        // given
        let cardsBO = CardsGenerator.getCardsWithRewards()

        // when
        let displayData2 = RewardDisplayData(withCardBO: cardsBO.cards[2])
        let displayData5 = RewardDisplayData(withCardBO: cardsBO.cards[5])
        let displayData6 = RewardDisplayData(withCardBO: cardsBO.cards[6])
        let displayData7 = RewardDisplayData(withCardBO: cardsBO.cards[7])

        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal

        // then
        XCTAssert(RewardDisplayData(withCardBO: cardsBO.cards[0]) == nil)
        XCTAssert(RewardDisplayData(withCardBO: cardsBO.cards[1]) == nil)

        XCTAssert(displayData2?.points == numberFormatter.string(from: 4000000)!)
        XCTAssert(displayData2?.emptyPointsString == nil)
        XCTAssert(displayData2?.pointsFont == Tools.setFontLight(size: 48))
        XCTAssert(displayData2?.backgroundImage == ConstantsImages.Rewards.rewardsPointsBackground)

        XCTAssert(RewardDisplayData(withCardBO: cardsBO.cards[3]) == nil)
        XCTAssert(RewardDisplayData(withCardBO: cardsBO.cards[4]) == nil)

        XCTAssert(displayData5?.points == numberFormatter.string(from: 8000)!)
        XCTAssert(displayData5?.emptyPointsString == nil)
        XCTAssert(displayData5?.pointsFont == Tools.setFontLight(size: 48))
        XCTAssert(displayData5?.backgroundImage == ConstantsImages.Rewards.rewardsPointsBackground)

        XCTAssert(displayData6?.points == nil)
        XCTAssert(displayData6?.emptyPointsString == Localizables.points.key_points_card_without_points_text)
        XCTAssert(displayData6?.emptyPointsStringFont == Tools.setFontMediumItalic(size: 14))
        XCTAssert(displayData6?.emptyPointsStringColor == .BBVAWHITE)
        XCTAssert(displayData6?.backgroundImage == ConstantsImages.Rewards.rewardsNoPointsBackground)

        XCTAssert(displayData7?.points == "123")
        XCTAssert(displayData7?.emptyPointsString == nil)
        XCTAssert(displayData7?.pointsFont == Tools.setFontLight(size: 48))
        XCTAssert(displayData7?.backgroundImage == ConstantsImages.Rewards.rewardsPointsBackground)

    }

    func test_givenCardsBOWithRewardsWithExpirationInfo_whenICallGeneratePointsDisplayData_thenHaveCorrectExpiratedFields() {

        // given
        let cardsBO = CardsGenerator.getCardsWithRewardsWithExpiratedInfo()

        // when
        let displayData = RewardDisplayData(withCardBO: cardsBO.cards[13])
        let expiratedDateString = Date.string(fromDate: cardsBO.cards[13].rewards[0].nonMonetaryValueExpirationDate!, withFormat: Date.DATE_FORMAT_DAY_MONTH_COMPLETE_YEAR_COMPACT)

        let nonMonetaryValueCloseToExpire = String(format: Localizables.points.key_points_expire_text,
                                                   cardsBO.cards[13].rewards[0].nonMonetaryValueCloseToExpire!, expiratedDateString)

        // then
        XCTAssert(displayData != nil)

        XCTAssert(displayData?.nonMonetaryValueCloseToExpire! == nonMonetaryValueCloseToExpire)

    }

    func test_givenCardsBOWithRewardsWithExpirationInfo_whenICallGeneratePointsDisplayData_thenNonMonetaryValueCloseToExpireFontAndColorFilled() {

        // given
        let cardsBO = CardsGenerator.getCardsWithRewardsWithExpiratedInfo()

        // when
        let displayData = RewardDisplayData(withCardBO: cardsBO.cards[13])

        // then
        XCTAssert(displayData?.nonMonetaryValueCloseToExpireFont == Tools.setFontMediumItalic(size: 14))
        XCTAssert(displayData?.nonMonetaryValueCloseToExpireColor == .BBVAWHITE)

    }

}
