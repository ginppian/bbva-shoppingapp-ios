//
//  CustomerRewardsPresenterTests.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 2/8/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest
import RxSwift
import CellsNativeComponents

#if TESTPE
@testable import shoppingappPE
#endif

class CustomerRewardsPresenterTests: XCTestCase {

    var sut: CustomerRewardsPresenter<DummyView>!
    var dummyBusManager: DummyBusManager!
    var dummyView: DummyView!

    override func setUp() {
        super.setUp()
        dummyBusManager = DummyBusManager()
        sut = CustomerRewardsPresenter<DummyView>(busManager: dummyBusManager)

        dummyView = DummyView()
        sut.view = dummyView
    }

    // MARK: - pointsBalanceFailed

    func test_givenErrorBO_whenICallPointsBalanceFailed_thenErrorBOBeFilled() {

        // given
        let errorBO = ErrorBO(error: ErrorEntity(message: "Error"))

        // when
        sut.pointsBalanceFailed(error: errorBO)

        // then
        XCTAssert(sut.errorBO == errorBO)

    }

    func test_givenError_whenICallPointsBalanceFailed_thenICalledShowErrorView() {

        // given

        // when
        sut.pointsBalanceFailed(error: ErrorBO(error: ErrorEntity(message: "Error")))

        // then
        XCTAssert(dummyView.isCalledShowErrorView == true)
    }

    // MARK: - customerPointsShowError

    func test_givenAny_whenICallCustomerPointsShowError_thenICalledShowError() {

        // given

        // when
        sut.customerPointsShowError()

        // then
        XCTAssert(dummyView.isCalledShowError == true)
    }

    func test_givenAny_whenICallCustomerPointsShowError_thenICalledShowErrorWithCorrectError() {

        // given
        sut.errorBO = ErrorBO(message: Localizables.points.key_points_info_title_error_text, errorType: .warning, allowCancel: false)

        // when
        sut.customerPointsShowError()

        // then
        XCTAssert(dummyView.errorBO == ErrorBO(message: Localizables.points.key_points_info_title_error_text, errorType: .warning, allowCancel: false))
    }

    func test_givenAny_whenICallCustomerPointsShowError_thenErrorBOBeFilled() {

        // given

        // when
        sut.customerPointsShowError()

        // then
        XCTAssert(sut.errorBO == ErrorBO(message: Localizables.points.key_points_info_title_error_text, errorType: .warning, allowCancel: false))
    }

    class DummyView: CustomerRewardsViewProtocol, ViewProtocol {

        var isCalledShowError = false
        var isCalledShowErrorView = false

        var errorBO: ErrorBO?

        func showError(error: ModelBO) {

            isCalledShowError = true
            errorBO = error as? ErrorBO
        }

        func changeColorEditTexts() {
        }

        func showErrorView() {
            isCalledShowErrorView = true
        }
    }

    class DummyBusManager: BusManager {

        override init() {
            super.init()
            routerFactory = RouterInitFactory()
        }
    }
}
