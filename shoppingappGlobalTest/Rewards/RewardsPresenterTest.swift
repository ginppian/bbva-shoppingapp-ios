//
//  RewaradsPresenterTest.swift
//  shoppingapp
//
//  Created by jesus.martinez on 7/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import XCTest
import RxSwift
import CellsNativeComponents

#if TESTMX
    @testable import shoppingappMX
#endif

class RewardsPresenterTest: XCTestCase {

    var sut: RewardsPresenter<DummyView>!
    var dummyBusManager: DummyBusManager!
    var dummySessionManager: DummySessionManager!

    override func setUp() {
        super.setUp()
        dummyBusManager = DummyBusManager()
        sut = RewardsPresenter<DummyView>(busManager: dummyBusManager)
        
        DummySessionManager.setupMock()
        dummySessionManager = (SessionDataManager.sessionDataInstance() as! DummySessionManager)
    }

    // MARK: - receive model

    func test_givenCardsBOAndCardsNil_whenICallReceivedModel_thenCardsBOShouldBeFilled() {

        // given
        let cardsBO = CardsGenerator.getCardBOWithAlias()

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(sut.cards != nil)

    }

    func test_givenCardsBOAndCardsNil_whenICallReceivedModel_thenCardsBOShouldMatch() {

        // given
        let cardsBO = CardsGenerator.getCardBOWithAlias()

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(sut.cards === cardsBO)

    }

    func test_givenCardsBOAndCardsNotNilButDifferentTimestamp_whenICallReceivedModel_thenCardsBOShouldMatch() {

        // given
        let cardsBefore = CardsGenerator.getCardBOWithAlias()
        sut.cards = cardsBefore
        let cardsBO = CardsGenerator.getCardBOWithAlias()

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(sut.cards === cardsBO)
    }

    func test_givenCardsBOAndCardsNotNilAndSameTimestamp_whenICallReceivedModel_thenCardsBOShouldMatch() {

        // given
        let cardsBO = CardsGenerator.getCardBOWithAlias()
        sut.cards = cardsBO

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(sut.cards === cardsBO)
    }

    func test_givenPresenterWithIsShowTrendingAnimationFalseAndMoreThanOneRewards_whenICallViewDidLoad_thenICallDoAnimationRewards() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        
        dummySessionManager.isShowRewardsAnimation = false

        let cardsBO = CardsGenerator.getCardsWithRewards()

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.doAnimationIsCalled == true)

    }

    func test_givenPresenterWithIsShowTrendingAnimationFalseAndOnePromotion_whenICallViewDidLoad_thenIDoNotCallDoAnimationRewards() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        dummySessionManager.isShowRewardsAnimation = false

        let cardsBO = CardsGenerator.getCardBOWithAlias()

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.doAnimationIsCalled == false)

    }

    func test_givenPresenterWithIsShowTrendingAnimationTrueAndMoreThanOneRewards_whenICallViewDidLoad_thenIDoNotCallDoAnimationRewards() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        dummySessionManager.isShowRewardsAnimation = true

        let cardsBO = CardsGenerator.getCardBOWithAlias()

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.doAnimationIsCalled == false)

    }

    func test_givenACardsBO_whenICallViewDidLoad_thenHaveCorrectNumberOfRewardsDisplayData() {

        //given
        let dummyView = DummyView()
        sut.view = dummyView
        let cardsBO = CardsGenerator.getCardsWithRewards()

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(sut.displayDataItems.count == 4)
    }

    func test_givenACardsBO_whenICallViewDidLoad_thenICallViewShowDataWithRewards() {

        //given
        let dummyView = DummyView()
        sut.view = dummyView
        let cardsBO = CardsGenerator.getCardsWithRewards()

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.isCalledShowDataWithRewards == true)
    }

    func test_givenACardsBO_whenICallViewDidLoad_thenICallViewShowDataWithRewardsWithTheSameDisplayDataCreated() {

        //given
        let dummyView = DummyView()
        sut.view = dummyView
        let cardsBO = CardsGenerator.getCardsWithRewards()

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.rewardsSent == sut.displayDataItems)
    }

    func test_givenACardsAndCardsBOWithTheSameTimestamp_whenICallViewDidLoad_thenIDoNotCallViewShowDataWithRewards() {

        //given
        let dummyView = DummyView()
        sut.view = dummyView
        let cardsBO = CardsGenerator.getCardBOWithAlias()
        sut.cards = cardsBO

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.isCalledShowDataWithRewards == false)
    }

    func test_givenACardsBOWithoutCards_whenICallViewDidLoad_thenIDoNotCallViewShowDataWithRewards() {

        //given
        let dummyView = DummyView()
        sut.view = dummyView
        let cardsBO = CardsGenerator.getCardsEmtpy()

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.isCalledShowDataWithRewards == false)
    }

    func test_givenACardsBOWithoutRewards_whenICallViewDidLoad_thenIDoNotCallViewShowDataWithRewards() {

        //given
        let dummyView = DummyView()
        sut.view = dummyView
        let cardsBO = CardsGenerator.getCardsWithoutRewards()

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.isCalledShowDataWithRewards == false)
    }

    func test_givenACardsBOWithoutRewardsWithBancomerPoints_whenICallViewDidLoad_thenIDoNotCallViewShowDataWithRewards() {

        //given
        let dummyView = DummyView()
        sut.view = dummyView
        let cardsBO = CardsGenerator.getCardsWithRewardsOnlyMillas()

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.isCalledShowDataWithRewards == false)
    }

    func test_givenCardsBONil_whenICallViewDidLoad_thenICallViewShowEmptyView() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.cards = nil
        let model = EmptyModel()

        // when
        sut.receive(model: model)

        // then
        XCTAssert(dummyView.isCalledShowEmptyView == true)

    }

    func test_givenCardsBOEmpty_whenICallViewDidLoad_thenICallViewShowEmptyView() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let cardsBO = CardsGenerator.getCardsEmtpy()

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.isCalledShowEmptyView == true)

    }

    func test_givenACardsBOWithoutRewards_whenICallViewDidLoad_thenICallViewShowEmptyView() {

        //given
        let dummyView = DummyView()
        sut.view = dummyView
        let cardsBO = CardsGenerator.getCardsWithoutRewards()

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.isCalledShowEmptyView == true)
    }

    func test_givenACardsBOWithoutRewardsWithBancomerPoints_whenICallViewDidLoad_thenICallViewShowEmptyView() {

        //given
        let dummyView = DummyView()
        sut.view = dummyView
        let cardsBO = CardsGenerator.getCardsWithRewardsOnlyMillas()

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.isCalledShowEmptyView == true)
    }

    func test_givenACardsBONil_whenICallViewDidLoad_thenIDoNotCallViewShowHelp() {

        //given
        let dummyView = DummyView()
        sut.view = dummyView
        sut.cards = nil
        let model = EmptyModel()

        // when
        sut.receive(model: model)

        // then
        XCTAssert(dummyView.isCalledShowHelp == false)
    }

    func test_givenACardsBOWithoutCards_whenICallViewDidLoad_thenIDoNotCallViewShowHelp() {

        //given
        let dummyView = DummyView()
        sut.view = dummyView
        let cardsBO = CardsGenerator.getCardsEmtpy()

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.isCalledShowHelp == false)
    }

    func test_givenACardsBOWithoutRewards_whenICallViewDidLoad_thenIDoNotCallViewShowHelp() {

        //given
        let dummyView = DummyView()
        sut.view = dummyView
        let cardsBO = CardsGenerator.getCardsWithoutRewards()

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.isCalledShowHelp == false)
    }

    func test_givenACardsBOWithoutRewardsWithBancomerPoints_whenICallViewDidLoad_thenIDoNotCallViewShowHelp() {

        //given
        let dummyView = DummyView()
        sut.view = dummyView
        let cardsBO = CardsGenerator.getCardsWithRewardsOnlyMillas()

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.isCalledShowHelp == false)
    }

    func test_givenACardsBOWithRewardsAndConfigurationWithValuesForKeyRewards_whenICallViewDidLoad_thenICallShowHelpView() {
        //given
        let dummyView = DummyView()
        sut.view = dummyView
        let cardsBO = CardsGenerator.getCardsWithRewards()

        let dummyPublicManager = DummyPublicManager()
        ConfigPublicManager.instance = dummyPublicManager

        dummyPublicManager.configs = ConfigGenerator.getHelpBO()
        dummyPublicManager.configs?.items[0].sections[0].sectionId = Constants.SECTION_TO_SHOW_REWARDS

        //When
        sut.receive(model: cardsBO)

        //then
        XCTAssert(dummyView.isCalledShowHelp == true)

    }

    func test_givenACardsBOWithRewardsAndConfigurationWithValuesForKeyRewards_whenICallViewDidLoad_thenHelpDisplayDataIsNotEmpty() {

        //given
        let dummyView = DummyView()
        sut.view = dummyView
        let cardsBO = CardsGenerator.getCardsWithRewards()

        let dummyPublicManager = DummyPublicManager()
        ConfigPublicManager.instance = dummyPublicManager

        dummyPublicManager.configs = ConfigGenerator.getHelpBO()
        dummyPublicManager.configs?.items[0].sections[0].sectionId = Constants.SECTION_TO_SHOW_REWARDS

        //when
        sut.receive(model: cardsBO)

        //then
        XCTAssert(dummyView.helpDisplayDataSent != nil)

    }

    func test_givenACardsBOWithRewardsAndConfigurationWithValuesForKeyRewards_whenICallViewDidLoad_thenHelpDisplayDataMatchToHelpDisplayDataSent() {

        //given
        let dummyView = DummyView()
        sut.view = dummyView
        let cardsBO = CardsGenerator.getCardsWithRewards()

        let dummyPublicManager = DummyPublicManager()
        ConfigPublicManager.instance = dummyPublicManager

        dummyPublicManager.configs = ConfigGenerator.getHelpBO()
        dummyPublicManager.configs?.items[0].sections[0].sectionId = Constants.SECTION_TO_SHOW_REWARDS

        let helpDisplayItems = HelpDisplayData.generateHelpDisplayData(withConfigBO: ConfigPublicManager.sharedInstance().getConfigBO(forSection: Constants.SECTION_TO_SHOW_REWARDS, withKey: PreferencesManagerKeys.kIndexToShowRewards)!, isFromSection: true)

        //when
        sut.receive(model: cardsBO)

        //then
        XCTAssert(dummyView.helpDisplayDataSent.titleText == helpDisplayItems.titleText)
        XCTAssert(dummyView.helpDisplayDataSent.descriptionText == helpDisplayItems.descriptionText)
        XCTAssert(dummyView.helpDisplayDataSent.image == helpDisplayItems.image)
        XCTAssert(dummyView.helpDisplayDataSent.imageUrl == helpDisplayItems.imageUrl)
        XCTAssert(dummyView.helpDisplayDataSent.backgroundColor == helpDisplayItems.backgroundColor)
        XCTAssert(dummyView.helpDisplayDataSent.titleColor == helpDisplayItems.titleColor)
        XCTAssert(dummyView.helpDisplayDataSent.descriptionTextColor == helpDisplayItems.descriptionTextColor)
        XCTAssert(dummyView.helpDisplayDataSent.descriptionOnTapColor == helpDisplayItems.descriptionOnTapColor)

    }

    func test_givenACardsBOWithRewardsAndConfigurationWithValuesForKeyRewards_whenICallViewDidLoad_thenICallGetConfigBOBySectionAndCorrectKeyAndWithSectionValues() {

        //given
        let dummyView = DummyView()
        sut.view = dummyView
        let cardsBO = CardsGenerator.getCardsWithRewards()

        let dummyPublicManager = DummyPublicManager()
        ConfigPublicManager.instance = dummyPublicManager

        dummyPublicManager.configs = ConfigGenerator.getHelpBO()
        dummyPublicManager.configs?.items[0].sections[0].sectionId = Constants.SECTION_TO_SHOW_REWARDS

        //When
        sut.receive(model: cardsBO)

        //then
        XCTAssert(dummyPublicManager.isCalledGetConfigBOBySection == true)
        XCTAssert(dummyPublicManager.rewardsKeySent == Constants.SECTION_TO_SHOW_REWARDS)

    }

    func test_givenACardsBOWithRewardsAndNilConfigurationForKeyRewards_whenICallViewDidLoad_thenINotCallShowHelpView() {

        let dummyView = DummyView()
        sut.view = dummyView
        let cardsBO = CardsGenerator.getCardsWithRewards()

        let dummyPublicManager = DummyPublicManager()
        ConfigPublicManager.instance = dummyPublicManager

        dummyPublicManager.configs = nil

        //when
        sut.receive(model: cardsBO)

        //then
        XCTAssert(dummyView.isCalledShowHelp == false)

    }

    func test_givenACardsBOWithRewardsAndEmptyConfigurationForKeyCard_whenICallViewDidLoad_thenIDontCallShowHelpView() {

        let dummyView = DummyView()
        sut.view = dummyView
        let cardsBO = CardsGenerator.getCardsWithRewards()

        let dummyPublicManager = DummyPublicManager()
        ConfigPublicManager.instance = dummyPublicManager

        dummyPublicManager.configs!.items = [ConfigFileBO]()

        //when
        sut.receive(model: cardsBO)

        //then
        XCTAssert(dummyView.isCalledShowHelp == false)

    }

    // MARK: - viewWillAppear

    func test_givenAny_whenICallViewWillAppear_thenICallViewSendScreenOmniture() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledSendScreenOmniture == true)

    }
    
    // MARK: - viewWillAppearAfterDismiss
    
    func test_givenAny_whenICallViewWillAppearAfterDismiss_thenICallViewSendScreenOmniture() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        
        // when
        sut.viewWillAppearAfterDismiss()
        
        // then
        XCTAssert(dummyView.isCalledSendScreenOmniture == true)
        
    }

    // MARK: - setAnimationDone

    func test_givenPresenter_whenCallSetAnimationDone_thenSessionManagerIsShowRewardsAnimationIstrue() {

        // given

        // when
        sut.setAnimationDone()

        // then
        XCTAssert(dummySessionManager.isShowRewardsAnimation == true)

    }

    // MARK: - showedRewards atIndex

    func test_givenDisplayDataItemsEmtpy_whenICallShowedRewardsAtIndex_thenIDoNotCallViewShowRewardBreakDownWithModel() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.displayDataItems = [RewardDisplayData]()

        // when
        sut.showedRewards(atIndex: 0)

        // then
        XCTAssert(dummyView.isCalledShowRewardBreakDownWithModel == false)

    }

    func test_givenDisplayDataNotEmtpyButIndexBiggerThanItsNumberOfItems_whenICallShowedRewardsAtIndex_thenIDoNotCallViewShowRewardBreakDownWithModel() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let rewardDisplayData = RewardDisplayData(withCardBO: CardsGenerator.getCardsWithRewards().cards[2])!
        sut.displayDataItems.append(rewardDisplayData)

        // when
        sut.showedRewards(atIndex: 5)

        // then
        XCTAssert(dummyView.isCalledShowRewardBreakDownWithModel == false)

    }

    func test_givenDisplayDataNotEmtpyAndAppropiateIndexAndRewardDisplayDataHasCardBO_whenICallShowedRewardsAtIndex_thenICallViewShowRewardBreakDownWithModel() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let cardBO = CardsGenerator.getCardsWithRewards().cards[2]
        let cardBO1 = CardsGenerator.getCardsWithRewards().cards[2]
        let cardBO2 = CardsGenerator.getCardsWithRewards().cards[2]
        cardBO.cardId = "0000"
        cardBO1.cardId = "1111"
        cardBO2.cardId = "2222"
        sut.displayDataItems.append(RewardDisplayData(withCardBO: cardBO)!)
        sut.displayDataItems.append(RewardDisplayData(withCardBO: cardBO1)!)
        sut.displayDataItems.append(RewardDisplayData(withCardBO: cardBO2)!)

        // when
        sut.showedRewards(atIndex: 1)

        // then
        XCTAssert(dummyView.isCalledShowRewardBreakDownWithModel == true)

    }

    func test_givenDisplayDataNotEmtpyAndAppropiateIndexAndRewardDisplayDataHasCardBO_whenICallShowedRewardsAtIndex_thenICallViewShowRewardBreakDownWithModelWithIdOfCardAtIndex() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let cardBO = CardsGenerator.getCardsWithRewards().cards[2]
        let cardBO1 = CardsGenerator.getCardsWithRewards().cards[2]
        let cardBO2 = CardsGenerator.getCardsWithRewards().cards[2]
        cardBO.cardId = "0000"
        cardBO1.cardId = "1111"
        cardBO2.cardId = "2222"
        sut.displayDataItems.append(RewardDisplayData(withCardBO: cardBO)!)
        sut.displayDataItems.append(RewardDisplayData(withCardBO: cardBO1)!)
        sut.displayDataItems.append(RewardDisplayData(withCardBO: cardBO2)!)

        // when
        sut.showedRewards(atIndex: 1)

        // then
        XCTAssert(dummyView.rewardBreakDownModelSent?.cardId == cardBO1.cardId)

    }

    func test_givenDisplayDataNotEmtpyAndAppropiateIndexAndRewardDisplayDataHasCardBOAndBalanceRewardsByCardIdHasBalance_whenICallShowedRewardsAtIndex_thenICallViewShowRewardBreakDownWithModelWithBalanceRewardsStores() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let cardBO = CardsGenerator.getCardsWithRewards().cards[2]
        let cardBO1 = CardsGenerator.getCardsWithRewards().cards[2]
        let cardBO2 = CardsGenerator.getCardsWithRewards().cards[2]
        cardBO.cardId = "0000"
        cardBO1.cardId = "1111"
        cardBO2.cardId = "2222"
        sut.displayDataItems.append(RewardDisplayData(withCardBO: cardBO)!)
        sut.displayDataItems.append(RewardDisplayData(withCardBO: cardBO1)!)
        sut.displayDataItems.append(RewardDisplayData(withCardBO: cardBO2)!)

        sut.balanceRewardsByCardId[cardBO1.cardId] = BalanceRewardsGenerator.balanceCompleteBO()

        // when
        sut.showedRewards(atIndex: 1)

        // then
        XCTAssert(dummyView.rewardBreakDownModelSent?.balanceRewards != nil)
        XCTAssert(dummyView.rewardBreakDownModelSent?.balanceRewards == sut.balanceRewardsByCardId[cardBO1.cardId])

    }

    func test_givenDisplayDataNotEmtpyAndAppropiateIndexAndRewardDisplayDataHasCardBOAndBalanceRewardsByCardIdDoesNotHaveBalance_whenICallShowedRewardsAtIndex_thenICallViewShowRewardBreakDownWithModelWithBalanceRewardsNil() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let cardBO = CardsGenerator.getCardsWithRewards().cards[2]
        let cardBO1 = CardsGenerator.getCardsWithRewards().cards[2]
        let cardBO2 = CardsGenerator.getCardsWithRewards().cards[2]
        cardBO.cardId = "0000"
        cardBO1.cardId = "1111"
        cardBO2.cardId = "2222"
        sut.displayDataItems.append(RewardDisplayData(withCardBO: cardBO)!)
        sut.displayDataItems.append(RewardDisplayData(withCardBO: cardBO1)!)
        sut.displayDataItems.append(RewardDisplayData(withCardBO: cardBO2)!)
        
        // when
        sut.showedRewards(atIndex: 1)

        // then
        XCTAssert(dummyView.rewardBreakDownModelSent?.balanceRewards == nil)

    }
    
    func test_givenDisplayDataNotEmtpyAndAppropiateIndexAndCardIdsOfBalanceRewardsWithErrorNotCardIdOnIndexPosition_whenICallShowedRewardsAtIndex_thenICallViewShowRewardBreakDownWithModelWithShowErrorFalse() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let cardBO = CardsGenerator.getCardsWithRewards().cards[2]
        let cardBO1 = CardsGenerator.getCardsWithRewards().cards[2]
        let cardBO2 = CardsGenerator.getCardsWithRewards().cards[2]
        cardBO.cardId = "0000"
        cardBO1.cardId = "1111"
        cardBO2.cardId = "2222"
        sut.displayDataItems.append(RewardDisplayData(withCardBO: cardBO)!)
        sut.displayDataItems.append(RewardDisplayData(withCardBO: cardBO1)!)
        sut.displayDataItems.append(RewardDisplayData(withCardBO: cardBO2)!)
        
        sut.cardIdsOfBalanceRewardsWithError = [cardBO.cardId, cardBO2.cardId]
        
        // when
        sut.showedRewards(atIndex: 1)
        
        // then
        XCTAssert(dummyView.rewardBreakDownModelSent?.showError == false)
        
    }
    
    func test_givenDisplayDataNotEmtpyAndAppropiateIndexAndCardIdsOfBalanceRewardsWithErrorInCardIdOnIndexPosition_whenICallShowedRewardsAtIndex_thenICallViewShowRewardBreakDownWithModelWithShowErrorTrue() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let cardBO = CardsGenerator.getCardsWithRewards().cards[2]
        let cardBO1 = CardsGenerator.getCardsWithRewards().cards[2]
        let cardBO2 = CardsGenerator.getCardsWithRewards().cards[2]
        cardBO.cardId = "0000"
        cardBO1.cardId = "1111"
        cardBO2.cardId = "2222"
        sut.displayDataItems.append(RewardDisplayData(withCardBO: cardBO)!)
        sut.displayDataItems.append(RewardDisplayData(withCardBO: cardBO1)!)
        sut.displayDataItems.append(RewardDisplayData(withCardBO: cardBO2)!)
        
        sut.cardIdsOfBalanceRewardsWithError = [cardBO.cardId, cardBO1.cardId]
        
        // when
        sut.showedRewards(atIndex: 1)
        
        // then
        XCTAssert(dummyView.rewardBreakDownModelSent?.showError == true)
        
    }
    
    // MARK: - RewardBreakDownPresenterDelegate willRetrieveBalanceForCardId
    
    func test_givenCardIdAndCardIdsOfBalanceRewardsWithErrorWithCardId_whenICallRewardBreakDownWillRetrieveBalanceForCardId_thenCardIdsOfBalanceRewardsWithErrorShouldNotContainTheCardId() {
        
        // given
        let cardId = "1"
        let presenter = RewardBreakDownPresenter()
        sut.cardIdsOfBalanceRewardsWithError = [cardId]
        
        // when
        sut.rewardBreakDownPresenter(presenter, willRetrieveBalanceForCardId: cardId)
        
        // then
        XCTAssert(sut.cardIdsOfBalanceRewardsWithError.contains(cardId) == false)
        
    }
    
    func test_givenCardIdAndDisplayDataItemsWithCardId_whenICallRewardBreakDownWillRetrieveBalanceForCardId_thenICallViewShowExpiredSkeletonAtRightIndex() {
        
        // given
        let dummyView = DummyView()
        sut.view = dummyView
        
        let presenter = RewardBreakDownPresenter()
        let cardBO = CardsGenerator.getCardsWithRewards().cards[2]
        let cardBO1 = CardsGenerator.getCardsWithRewards().cards[2]
        let cardBO2 = CardsGenerator.getCardsWithRewards().cards[2]
        cardBO.cardId = "0000"
        cardBO1.cardId = "1111"
        cardBO2.cardId = "2222"
        sut.displayDataItems.append(RewardDisplayData(withCardBO: cardBO)!)
        sut.displayDataItems.append(RewardDisplayData(withCardBO: cardBO1)!)
        sut.displayDataItems.append(RewardDisplayData(withCardBO: cardBO2)!)
        let cardId = "1111"
        
        // when
        sut.rewardBreakDownPresenter(presenter, willRetrieveBalanceForCardId: cardId)
        
        // then
        XCTAssert(dummyView.isCalledShowExpiredSkeleton)
        XCTAssert(dummyView.indexShowExpiredSkeleton == 1)
    }

    // MARK: - RewardBreakDownPresenterDelegate retrieveError

    func test_givenErrorBO_whenICallRewardBreakDownPresenterRetrieveError_thenErrorBOShouldBeFilled() {

        // given
        let errorBO = ErrorBO()
        let presenter = RewardBreakDownPresenter()
        let cardId = "1"

        // when
        sut.rewardBreakDownPresenter(presenter, cardId: cardId, retrieveError: errorBO)

        // then
        XCTAssert(sut.errorBO != nil)

    }

    func test_givenErrorBO_whenICallRewardBreakDownPresenterRetrieveError_thenErrorBOShouldMatch() {

        // given
        let errorBO = ErrorBO()
        let presenter = RewardBreakDownPresenter()
        let cardId = "1"

        // when
        sut.rewardBreakDownPresenter(presenter, cardId: cardId, retrieveError: errorBO)
        // then
        XCTAssert(sut.errorBO! === errorBO)

    }

    func test_givenErrorBO_whenICallRewardBreakDownPresenterRetrieveError_thenICallViewShowError() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let errorBO = ErrorBO()
        let presenter = RewardBreakDownPresenter()
        let cardId = "1"

        // when
        sut.rewardBreakDownPresenter(presenter, cardId: cardId, retrieveError: errorBO)
        // then
        XCTAssert(dummyView.isCalledShowError == true)

    }

    func test_givenErrorBO_whenICallRewardBreakDownPresenterRetrieveErrorAndErrorIsExpiredSession_thenICallViewShowErrorWithErrorBO() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let errorBO = ErrorBO()
        errorBO.status = ConstantsHTTPCodes.STATUS_403
        errorBO.code = ErrorCode.expiredSession.rawValue
        let presenter = RewardBreakDownPresenter()
        let cardId = "1"

        // when
        sut.rewardBreakDownPresenter(presenter, cardId: cardId, retrieveError: errorBO)
        
        // then
        XCTAssert(dummyView.modelBOSent as? ErrorBO === errorBO)

    }

    func test_givenErrorBO_whenICallRewardBreakDownPresenterRetrieveErrorAndErrorIsNotExpiredSession_thenICallViewShowErrorWithErrorBO() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let errorBO = ErrorBO()
        errorBO.status = ConstantsHTTPCodes.INCORRECT_USER_OR_PASSWORD_409
        errorBO.code = ErrorCode.expiredSession.rawValue
        let presenter = RewardBreakDownPresenter()
        let cardId = "1"

        // when
        sut.rewardBreakDownPresenter(presenter, cardId: cardId, retrieveError: errorBO)

        // then
        XCTAssert(dummyView.modelBOSent == nil)

    }

    func test_givenErrorBO_whenICallRewardBreakDownPresenterRetrieveError_thenICallViewHideExpiredSkeletonAtRightIndex() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let errorBO = ErrorBO()
        let presenter = RewardBreakDownPresenter()
        let cardBO = CardsGenerator.getCardsWithRewards().cards[2]
        let cardBO1 = CardsGenerator.getCardsWithRewards().cards[2]
        let cardBO2 = CardsGenerator.getCardsWithRewards().cards[2]
        cardBO.cardId = "0000"
        cardBO1.cardId = "1111"
        cardBO2.cardId = "2222"
        sut.displayDataItems.append(RewardDisplayData(withCardBO: cardBO)!)
        sut.displayDataItems.append(RewardDisplayData(withCardBO: cardBO1)!)
        sut.displayDataItems.append(RewardDisplayData(withCardBO: cardBO2)!)
        let cardId = "1111"

        // when
        sut.rewardBreakDownPresenter(presenter, cardId: cardId, retrieveError: errorBO)

        // then
        XCTAssert(dummyView.isCalledHideExpiredSkeleton)
        XCTAssert(dummyView.indexHideExpiredSkeleton == 1)

    }
    
    func test_givenErrorBOAndCardId_whenICallRewardBreakDownPresenterRetrieveError_thenCardIdsOfBalanceRewardsWithErrorShouldContainTheCardId() {
        
        // given
        let errorBO = ErrorBO()
        let presenter = RewardBreakDownPresenter()
        let cardId = "1"
        
        // when
        sut.rewardBreakDownPresenter(presenter, cardId: cardId, retrieveError: errorBO)
        
        // then
        XCTAssert(sut.cardIdsOfBalanceRewardsWithError.contains(cardId) == true)
    }

    // MARK: - RewardBreakDownPresenterDelegate retrieveBalanceRewards

    func test_givenCardIdAndBalanceRewards_whenICallRewardBreakDownRetrieveBalanceRewards_thenBalanceRewardsByCardIdShouldContainTheBalanceRewardsInCardIdKey() {

        // given
        let cardId = "1"
        let balanceRewards = BalanceRewardsGenerator.balanceCompleteBO()
        let presenter = RewardBreakDownPresenter()

        // when
        sut.rewardBreakDownPresenter(presenter, cardId: cardId, retrieveBalanceRewards: balanceRewards)

        // then
        XCTAssert(sut.balanceRewardsByCardId[cardId] != nil)
        XCTAssert(sut.balanceRewardsByCardId[cardId] == balanceRewards)

    }

    func test_givenCardIdAndBalanceRewards_whenICallRewardBreakDownRetrieveBalanceRewards_thenICallViewHideExpiredSkeletonAtRightIndex() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let cardBO = CardsGenerator.getCardsWithRewards().cards[2]
        let cardBO1 = CardsGenerator.getCardsWithRewards().cards[2]
        let cardBO2 = CardsGenerator.getCardsWithRewards().cards[2]
        cardBO.cardId = "0000"
        cardBO1.cardId = "1111"
        cardBO2.cardId = "2222"
        sut.displayDataItems.append(RewardDisplayData(withCardBO: cardBO)!)
        sut.displayDataItems.append(RewardDisplayData(withCardBO: cardBO1)!)
        sut.displayDataItems.append(RewardDisplayData(withCardBO: cardBO2)!)

        let cardId = cardBO1.cardId
        let balanceRewards = BalanceRewardsGenerator.balanceCompleteBO()
        let presenter = RewardBreakDownPresenter()

        // when
        sut.rewardBreakDownPresenter(presenter, cardId: cardId, retrieveBalanceRewards: balanceRewards)

        // then
        XCTAssert(dummyView.isCalledHideExpiredSkeleton)
        XCTAssert(dummyView.indexHideExpiredSkeleton == 1)
    }

    func test_givenCardIdAndBalanceRewards_whenICallRewardBreakDownRetrieveBalanceRewards_thenIsCalledShowExpiredInfoAtRightIndexWithRightPointsAndDate() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let cardBO = CardsGenerator.getCardsWithRewards().cards[2]
        let cardBO1 = CardsGenerator.getCardsWithRewards().cards[2]
        let cardBO2 = CardsGenerator.getCardsWithRewards().cards[2]
        cardBO.cardId = "0000"
        cardBO1.cardId = "1111"
        cardBO2.cardId = "2222"
        sut.displayDataItems.append(RewardDisplayData(withCardBO: cardBO)!)
        sut.displayDataItems.append(RewardDisplayData(withCardBO: cardBO1)!)
        sut.displayDataItems.append(RewardDisplayData(withCardBO: cardBO2)!)

        let cardId = cardBO1.cardId
        let balanceRewards = BalanceRewardsGenerator.balanceCompleteBO()
        let presenter = RewardBreakDownPresenter()

        // when
        sut.rewardBreakDownPresenter(presenter, cardId: cardId, retrieveBalanceRewards: balanceRewards)

        // then
        XCTAssert(dummyView.isCalledShowExpiredInfo)
        XCTAssert(dummyView.indexShowExpiredInfo == 1)
        XCTAssert(dummyView.pointsShowExpiredInfo == balanceRewards.nonMonetaryValueCloseToExpired)
        XCTAssert(dummyView.dateShowExpiredInfo == balanceRewards.nonMonetaryValueExpirationDate)

    }
    
    func test_givenCardIdAndBalanceRewardsWithNonMonetaryValueCloseToExpiredEqualZero_whenICallRewardBreakDownRetrieveBalanceRewards_thenDontIsCalledShowExpiredInfo() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let cardBO = CardsGenerator.getCardsWithRewards().cards[2]
        let cardBO1 = CardsGenerator.getCardsWithRewards().cards[2]
        let cardBO2 = CardsGenerator.getCardsWithRewards().cards[2]
        cardBO.cardId = "0000"
        cardBO1.cardId = "1111"
        cardBO2.cardId = "2222"
        sut.displayDataItems.append(RewardDisplayData(withCardBO: cardBO)!)
        sut.displayDataItems.append(RewardDisplayData(withCardBO: cardBO1)!)
        sut.displayDataItems.append(RewardDisplayData(withCardBO: cardBO2)!)
        
        let cardId = cardBO1.cardId
        let balanceRewards = BalanceRewardsGenerator.balanceCompleteBO()
        balanceRewards.nonMonetaryValueCloseToExpired = 0
        let presenter = RewardBreakDownPresenter()
        
        // when
        sut.rewardBreakDownPresenter(presenter, cardId: cardId, retrieveBalanceRewards: balanceRewards)
        
        // then
        XCTAssert(dummyView.isCalledShowExpiredInfo == false)
    }
    
    func test_givenCardIdAndBalanceRewardsWithNonMonetaryValueCloseToExpiredNil_whenICallRewardBreakDownRetrieveBalanceRewards_thenDontIsCalledShowExpiredInfo() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let cardBO = CardsGenerator.getCardsWithRewards().cards[2]
        let cardBO1 = CardsGenerator.getCardsWithRewards().cards[2]
        let cardBO2 = CardsGenerator.getCardsWithRewards().cards[2]
        cardBO.cardId = "0000"
        cardBO1.cardId = "1111"
        cardBO2.cardId = "2222"
        sut.displayDataItems.append(RewardDisplayData(withCardBO: cardBO)!)
        sut.displayDataItems.append(RewardDisplayData(withCardBO: cardBO1)!)
        sut.displayDataItems.append(RewardDisplayData(withCardBO: cardBO2)!)
        
        let cardId = cardBO1.cardId
        let balanceRewards = BalanceRewardsGenerator.balanceCompleteBO()
        balanceRewards.nonMonetaryValueCloseToExpired = nil
        let presenter = RewardBreakDownPresenter()
        
        // when
        sut.rewardBreakDownPresenter(presenter, cardId: cardId, retrieveBalanceRewards: balanceRewards)
        
        // then
        XCTAssert(dummyView.isCalledShowExpiredInfo == false)
    }
    
    func test_givenCardIdAndBalanceRewardsAndCardIdsOfBalanceRewardsWithErrorWithCardId_whenICallRewardBreakDownRetrieveBalanceRewards_thenCardIdsOfBalanceRewardsWithErrorNotShouldContainTheCardId() {
        
        // given
        let cardId = "1"
        let balanceRewards = BalanceRewardsGenerator.balanceCompleteBO()
        let presenter = RewardBreakDownPresenter()
        sut.cardIdsOfBalanceRewardsWithError = [cardId]
        
        // when
        sut.rewardBreakDownPresenter(presenter, cardId: cardId, retrieveBalanceRewards: balanceRewards)
        
        // then
        XCTAssert(sut.cardIdsOfBalanceRewardsWithError.contains(cardId) == false)
        
    }

    // MARK: - helpButtonPressed

    func test_givenHelpId_whenHelpButtonPressed_thenICallBusManagerDetailHelpScreen() {

        //given
        let helpId = "helpId"

        //when
        sut.helpButtonPressed(withHelpId: helpId)

        //then
        XCTAssert(dummyBusManager.isCalledNavigateToDetailHelpScreen == true)

    }

    func test_givenHelpId_whenHelpButtonPressed_thenICallPublishHelpDetailTransportWithHelpId() {

        //given
        let helpId = "helpId"

        //when
        sut.helpButtonPressed(withHelpId: helpId)

        //then
        XCTAssert(helpId == dummyBusManager.dummyHelpDetailTransport.helpId)

    }

    // MARK: - moreInfoButtonPressed

    func test_givenIndex_whenMoreInfoButtonPressedAndHasEmptyNonMonetaryValue_thenICallBusManagerDetailHelpScreen() {

        //given
        let cardBO = CardsGenerator.getCardsWithRewards().cards[2]
        let cardBO1 = CardsGenerator.getCardsWithRewards().cards[2]
        let cardBO2 = CardsGenerator.getCardsWithRewards().cards[2]
        cardBO.rewards[0].nonMonetaryValue = 0
        cardBO1.rewards[0].nonMonetaryValue = 0
        cardBO2.rewards[0].nonMonetaryValue = 0
        sut.displayDataItems.append(RewardDisplayData(withCardBO: cardBO)!)
        sut.displayDataItems.append(RewardDisplayData(withCardBO: cardBO1)!)
        sut.displayDataItems.append(RewardDisplayData(withCardBO: cardBO2)!)

        //when
        sut.moreInfoButtonPressed(atIndex: 1)

        //then
        XCTAssert(dummyBusManager.isCalledNavigateToDetailHelpScreen == true)
    }

    func test_givenIndex_whenMoreInfoButtonPressedAndHasNonEmptyNonMonetaryValue_thenICallBusManagerDetailHelpScreen() {

        //given
        let cardBO = CardsGenerator.getCardsWithRewards().cards[2]
        let cardBO1 = CardsGenerator.getCardsWithRewards().cards[2]
        let cardBO2 = CardsGenerator.getCardsWithRewards().cards[2]
        cardBO.rewards[0].nonMonetaryValue = 666
        cardBO1.rewards[0].nonMonetaryValue = 666
        cardBO2.rewards[0].nonMonetaryValue = 666
        sut.displayDataItems.append(RewardDisplayData(withCardBO: cardBO)!)
        sut.displayDataItems.append(RewardDisplayData(withCardBO: cardBO1)!)
        sut.displayDataItems.append(RewardDisplayData(withCardBO: cardBO2)!)

        //when
        sut.moreInfoButtonPressed(atIndex: 1)

        //then
        XCTAssert(dummyBusManager.isCalledNavigateToPromotions == true)
    }

    // MARK: - moreInfoButtonPressed

    func test_givenAny_whenEmptyMoreInfoButtonPressedAndHasEmptyNonMonetaryValue_thenICallBusManagerDetailHelpScreen() {

        //given

        //when
        sut.emptyMoreInfoButtonPressed()

        //then
        XCTAssert(dummyBusManager.isCalledNavigateToDetailHelpScreen == true)
    }

    // MARK: DummyData

    class DummyView: RewardsViewProtocol, ViewProtocol {

        var isCalledShowError = true
        var isCalledShowDataWithRewards = false
        var doAnimationIsCalled = false
        var isCalledSendScreenOmniture = false
        var isCalledShowEmptyView = false
        var isCalledShowHelp = false
        var isCalledShowRewardBreakDownWithModel = false
        var isCalledShowExpiredSkeleton = false
        var isCalledHideExpiredSkeleton = false
        var isCalledShowExpiredInfo = false

        var rewardsSent = [RewardDisplayData]()
        var rewardBreakDownModelSent: RewardBreakDownModelProtocol?

        var helpDisplayDataSent: HelpDisplayData!
        var modelBOSent: ModelBO?
        var indexShowExpiredSkeleton: Int?
        var indexHideExpiredSkeleton: Int?
        var indexShowExpiredInfo: Int?
        var pointsShowExpiredInfo: Int?
        var dateShowExpiredInfo: Date?

        func showError(error: ModelBO) {
            isCalledShowError = true
            modelBOSent = error
        }

        func changeColorEditTexts() {
        }

        func showDataWithRewards(withRewardsDisplayItems rewardsDisplayItems: [RewardDisplayData]) {
            isCalledShowDataWithRewards = true
            rewardsSent = rewardsDisplayItems
        }

        func doAnimationRewards() {
            doAnimationIsCalled = true
        }

        func sendScreenOmniture() {
            isCalledSendScreenOmniture = true
        }

        func showEmptyView() {
            isCalledShowEmptyView = true
        }

        func showHelp(withDisplayData displayData: HelpDisplayData) {
            isCalledShowHelp = true
            helpDisplayDataSent = displayData
        }

        func showRewardBreakDown(withModel model: RewardBreakDownModelProtocol) {
            isCalledShowRewardBreakDownWithModel = true
            rewardBreakDownModelSent = model
        }

        func showExpiredSkeleton(atIndex index: Int) {
            isCalledShowExpiredSkeleton = true
            indexShowExpiredSkeleton = index
        }

        func hideExpiredSkeleton(atIndex index: Int) {
            isCalledHideExpiredSkeleton = true
            indexHideExpiredSkeleton = index
        }

        func showExpiredInfo(points: Int, date: Date, atIndex index: Int) {
            isCalledShowExpiredInfo = true
            indexShowExpiredInfo = index
            pointsShowExpiredInfo = points
            dateShowExpiredInfo = date
        }
    }

    class DummyBusManager: BusManager {

        var isCalledNavigateToDetailHelpScreen = false
        var isCalledNavigateToPromotions = false
        var dummyHelpDetailTransport: HelpDetailTransport!

        override init() {
            super.init()
            routerFactory = RouterInitFactory()
        }

        override func navigateScreen<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {

            if tag == RewardsPageReaction.ROUTER_TAG && event === RewardsPageReaction.EVENT_NAV_TO_DETAIL_HELP {
                isCalledNavigateToDetailHelpScreen = true
                dummyHelpDetailTransport = (values as! HelpDetailTransport)
            } else if tag == RewardsPageReaction.ROUTER_TAG && event === RewardsPageReaction.EVENT_CHANGE_TAB_TO_PROMOTIONS {
                isCalledNavigateToPromotions = true
            }
        }
    }
    
    class DummyPublicManager: ConfigPublicManager {

        var rewardsKeySent = ""
        var isCalledGetConfigBOBySection = false

        override func getConfigBOBySection(forKey key: String) -> [ConfigFileBO] {

            isCalledGetConfigBOBySection = true
            rewardsKeySent = key

            return super.getConfigBOBySection(forKey: key)
        }
    }
}
