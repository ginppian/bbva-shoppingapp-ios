//
//  BalancesPresenterTest.swift
//  shoppingapp
//
//  Created by ruben.fernandez on 22/9/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class BalancesPresenterTest: XCTestCase {

    let sut = BalancesPresenter()

    func test_givenAView_whenICallshowBalanceWithInfoForAmount_thenICallViewCalledShowBalanceInfo() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        var displayBalance = DisplayBalancesCard()
        displayBalance.amount = NSAttributedString()

        let width: Float = 320.0

        // when
        sut.showBalanceInfo(withDisplayBalanceCard: displayBalance, withWidth: width)

        // then
        XCTAssert(dummyView.isCalledShowBalanceInfo == true)

    }

    func test_givenACreditCardTypeAndShowCreditCardLimitInfo_whenICallShowBalanceInfo_thenICallShowMoreHeight() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        sut.showCreditCardLimitInfo = true
        let width: Float = 320.0

        var displayBalance = DisplayBalancesCard()

        var cards: CardsBO?

        cards = CardsGenerator.getCardBOCreditCard()

        displayBalance.cardBO = cards?.cards[0]

        displayBalance.cardBO?.cardType.id = .credit_card

        //when
        sut.showBalanceInfo(withDisplayBalanceCard: displayBalance, withWidth: width)

        // then
        XCTAssert(dummyView.isCalledShowMoreHeight == true)

    }
    func test_givenACreditCardTypeAndShowCreditCardLimitInfo_whenICallShowBalanceInfo_thenICallShowMoreInformation() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        sut.showCreditCardLimitInfo = true
        let width: Float = 320.0

        var displayBalance = DisplayBalancesCard()
        var cards: CardsBO?

        cards = CardsGenerator.getCardBOCreditCard()

        displayBalance.cardBO = cards?.cards[0]

        displayBalance.cardBO?.cardType.id = .credit_card
        //when
        sut.showBalanceInfo(withDisplayBalanceCard: displayBalance, withWidth: width)

        // then
        XCTAssert(dummyView.isCalledShowMoreInformation == true)

    }

    func test_givenACreditCardTypeAndShowCreditCardLimitInfo_whenICallShowBalanceInfo_thenICallShowMoreInformationWithTheSameBalanceCard() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        sut.showCreditCardLimitInfo = true

        var displayBalance = DisplayBalancesCard()
        var cards: CardsBO?

        cards = CardsGenerator.getCardBOCreditCard()

        displayBalance.cardBO = cards?.cards[0]

        displayBalance.cardBO?.cardType.id = .credit_card

        // when
        sut.showBalanceInfo(withDisplayBalanceCard: displayBalance, withWidth: 320.0)

        //then
        XCTAssert(dummyView.balanceDisplayDataDummy.cardBO === displayBalance.cardBO)

    }
    func test_givenACreditCardTypeAndShowCreditCardLimitInfo_whenICallShowBalanceInfo_thenICallShowProgressBar() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        sut.showCreditCardLimitInfo = true

        var displayBalance = DisplayBalancesCard()
        var cards: CardsBO?

        cards = CardsGenerator.getCardBOCreditCard()

        displayBalance.cardBO = cards?.cards[0]

        displayBalance.cardBO?.cardType.id = .credit_card
        //when
        sut.showBalanceInfo(withDisplayBalanceCard: displayBalance, withWidth: 320.0)

        // then
        XCTAssert(dummyView.isCalledShowProgressBar == true)

    }
   func test_givenACreditCardTypeAndShowCreditCardLimitInfo_whenICallShowBalanceInfo_thenICallShowProgressBarWithTheSameProgressPercentage() {

            // given
            let dummyView = DummyView()
            sut.view = dummyView
            sut.showCreditCardLimitInfo = true

            var displayBalance = DisplayBalancesCard()
            var cards: CardsBO?

            cards = CardsGenerator.getCardBOWithCardTypeWithOutRelatedCard(withCardType: "CREDIT_CARD")

            displayBalance.cardBO = cards?.cards[0]

            displayBalance.cardBO?.cardType.id = .credit_card

            displayBalance.balanceDisposed = 50.0

            displayBalance.balanceGranted = 40.0

            // when
            sut.showBalanceInfo(withDisplayBalanceCard: displayBalance, withWidth: 320.0)

            //then
        XCTAssert((dummyView.balanceDisplayDataDummy.balanceDisposed / dummyView.balanceDisplayDataDummy.balanceGranted) == (displayBalance.balanceDisposed / displayBalance.balanceGranted))
        }

    func test_givenACreditCardTypeAndShowCreditCardLimitInfo_whenICalculateProgressPercentage_thenIGetTheCorrectPercentage() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        sut.showCreditCardLimitInfo = true

        var displayBalance = DisplayBalancesCard()
        var cards: CardsBO?

        let width: Float = 320.0

        cards = CardsGenerator.getCardBOCreditCard()

        displayBalance.cardBO = cards?.cards[0]

        displayBalance.cardBO?.cardType.id = .credit_card

        displayBalance.balanceDisposed = 30.0

        displayBalance.balanceGranted = 20.0

        //when
        sut.showBalanceInfo(withDisplayBalanceCard: displayBalance, withWidth: width)

        // then
        XCTAssert((width) * (dummyView.balanceDisplayDataDummy.balanceDisposed / dummyView.balanceDisplayDataDummy.balanceGranted) == (width) * (displayBalance.balanceDisposed / displayBalance.balanceGranted))

    }

    func test_givenACreditCardTypeAndShowCreditCardLimitInfo_whenICallShowBalanceInfo_thenICallShowCreditCardContentView() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        sut.showCreditCardLimitInfo = true

        var displayBalance = DisplayBalancesCard()
        var cards: CardsBO?

        cards = CardsGenerator.getCardBOCreditCard()

        displayBalance.cardBO = cards?.cards[0]

        displayBalance.cardBO?.cardType.id = .credit_card

        //when
        sut.showBalanceInfo(withDisplayBalanceCard: displayBalance, withWidth: 320.0)

        // then
        XCTAssert(dummyView.isCalledShowCreditCardContentView == true)

    }

    func test_givenACardTypeDifferentThanCreditCard_whenICallShowBalanceInfo_thenICallShowLessHeight() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        var displayBalance = DisplayBalancesCard()
        var cards: CardsBO?

        cards = CardsGenerator.getCardBOCreditCard()

        displayBalance.cardBO = cards?.cards[0]

        displayBalance.cardBO?.cardType.id = .debit_card // different than creditcard

        //when
        sut.showBalanceInfo(withDisplayBalanceCard: displayBalance, withWidth: 320.0)

        // then
        XCTAssert(dummyView.isCalledShowLessHeight == true)

    }
    func test_givenACardTypeDifferentThanCreditCard_whenICallShowBalanceInfo_thenICallHideCreditCardContentView() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        var displayBalance = DisplayBalancesCard()
        var cards: CardsBO?

        cards = CardsGenerator.getCardBOCreditCard()

        displayBalance.cardBO = cards?.cards[0]

        displayBalance.cardBO?.cardType.id = .debit_card

        //when
        sut.showBalanceInfo(withDisplayBalanceCard: displayBalance, withWidth: 320)

        // then
        XCTAssert(dummyView.isCalledHideCreditCardContentView == true)

    }
    class DummyView: BalancesViewProtocol {

        var isCalledShowBalanceInfo = false
        var isCalledShowMoreHeight = false
        var isCalledShowMoreInformation = false
        var isCalledShowProgressBar = false
        var isCalledShowCreditCardContentView = false
        var isCalledShowLessHeight = false
        var isCalledHideCreditCardContentView = false

        var balanceDisplayDataDummy = DisplayBalancesCard()
        var cards: CardsBO?

        func showBalanceInfo(withDisplayBalanceCard displayBalance: DisplayBalancesCard) {
            isCalledShowBalanceInfo = true
        }

        func showMoreHeight() {
            isCalledShowMoreHeight = true
        }

        func showMoreInformation(withDisplayBalanceCard displayBalance: DisplayBalancesCard) {
            isCalledShowMoreInformation = true
            self.balanceDisplayDataDummy = displayBalance
        }

        func showProgressBar(withPercentage percentage: Float) {
            isCalledShowProgressBar = true
        }

        func showCreditCardContentView() {
            isCalledShowCreditCardContentView = true
        }

        func showLessHeight() {
            isCalledShowLessHeight = true
        }

        func hideCreditCardContentView() {
            isCalledHideCreditCardContentView = true
        }

        func receiveBalanceInfoInView(withDisplayBalanceCard displayBalance: DisplayBalancesCard) {
        }
    }

}
