//
//  TransactionDetailPresenterTest.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 21/11/2017.
//  Copyright © 2017 BBVA. All rights reserved.
//

import XCTest
import CellsNativeComponents

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class TransactionDetailPresenterTest: XCTestCase {

    var sut: TransactionDetailPresenter<DummyView>!
    var dummyBusManager: DummyBusManager!

    override func setUp() {
        super.setUp()
        dummyBusManager = DummyBusManager()
        sut = TransactionDetailPresenter<DummyView>(busManager: dummyBusManager)
    }

    override func tearDown() {
        super.tearDown()
    }
    
    // MARK: - viewWillAppearAfterDismiss
    
    func test_givenAny_whenICallViewWillAppearAfterDismiss_thenICallViewSendOmniture() {
        
        // given
        let dummyView = DummyView()
        sut.view = dummyView
        
        // when
        sut.viewWillAppearAfterDismiss()
        
        // then
        XCTAssert(dummyView.isCalledSendOmniture == true)
    }
    
    // MARK: - getTransactionInformation

    func test_givenATransactionDisplayItem_whenICallGetTransactionInformation_thenICallShowTransactionDetailAndDisplayTransactionDetailShouldBeFilled() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let transactionEntity = TransactionEntityDummyGenerator.transactionEntity()
        let transactionBO = TransactionBO(transactionEntity: transactionEntity)
        let transactionDisplayItem = TransactionDisplayItem(fromTransactionBO: transactionBO!)

        sut.transactionDisplayItem = transactionDisplayItem
        let dummyCardBO = CardsGenerator.getCardBOWithAlias().cards[0]

        sut.cardBO = dummyCardBO

        // when
        sut.getTransactionInformation()

        // then
        XCTAssert(dummyView.isCalledShowTransactionDetail == true)
        XCTAssert(dummyView.isCalledSendOmniture == true)
        XCTAssert(dummyView.displayTransactionDetail != nil)

    }
    
    // MARK: - goBackAction

    func test_givenAny_whenICallGoBackAction_thenICallBusManagerBack() {

        // given

        // when
        sut.goBackAction()

        // then
        XCTAssert(dummyBusManager.isCalledBackAction == true)

    }

    // MARK: - showOperativeBarInfo
    
    func test_givenAPresenter_whenICallShowOperativeBarInfo_thenShowOperativeBarInfo() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.showOperativeBarInfo()

        // then
        XCTAssert(dummyView.isCalledShowOperativeBarInfo == true)

    }

    func test_givenAPresenter_whenICallShowOperativeBarInfo_thenShowOperativeBarInfoWithDisplayData() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.showOperativeBarInfo()

        // then
        XCTAssert(dummyView.displayDataOperativesBarInfo != nil)
    }

    // MARK: - buttonBarOptionPressed
    
    func test_givenButtonBarDisplayData_whenICallButtonBarOptionPressedWithDisplayItem_thenICallViewShowCallBBVA() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyButtonBarDisplayData = ButtonBarDisplayData(imageNamed: "imageNamed", titleKey: "titleKey", optionKey: .callBBVA)

        // when
        sut.buttonBarOptionPressed(withDisplayItem: dummyButtonBarDisplayData)

        // then
        XCTAssert(dummyView.isCalledShowCallBBVA == true)
    }

    func test_givenButtonBarDisplayDataWithCallOption_whenICallButtonBarOptionPressedWithDisplayItem_thenICallViewShowCallBBVAWithSettingsTelephone() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyButtonBarDisplayData = ButtonBarDisplayData(imageNamed: "imageNamed", titleKey: "titleKey", optionKey: .callBBVA)

        // when
        sut.buttonBarOptionPressed(withDisplayItem: dummyButtonBarDisplayData)

        // then
        XCTAssert(dummyView.telephone == Settings.Global.default_bbva_phone_number)
    }

    // MARK: - setModel

    func test_givenATransactionBOAsParamter_whenICallOnMessage_thenTransactionDisplayItemShouldBeFilled() {

        // given
        let transactionEntity = TransactionEntityDummyGenerator.transactionEntity()
        let transactionBO = TransactionBO(transactionEntity: transactionEntity)
        let transactionDisplayItem = TransactionDisplayItem(fromTransactionBO: transactionBO!)

        let dummyCardBO = CardsGenerator.getCardBOWithAlias().cards[0]

        let transactionsTransport = TransactionsTransport()
        transactionsTransport.transactionDisplayItem = transactionDisplayItem
        transactionsTransport.cardBO = dummyCardBO

        // when
        sut.setModel(model: transactionsTransport)

        // then
        XCTAssert(sut.transactionDisplayItem != nil)

    }
    
    // MARK: - showNotOperationPressed

    func test_whenShowNotOperationPressed_thenICalledNavigateNotRecognizedOperationPage() {

        //given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]

        //when
        sut.cardBO = cardBO
        sut.showNotOperationPressed()

        //then
        XCTAssert(dummyBusManager.isCalledNavigateToNotRecognizedOperationPage == true)

    }

    func test_givenACardBOAndBlockCardTransport_whenShowNotOperationPressed_thenICalledPublishDataForBlockCardWithTheSameBlockCardTransport() {

        //given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        var blockCardTransport = BlockCardTransport(id: cardBO.cardId, cardNumber: String(cardBO.number.suffix(4)), alias: cardBO.cardAlias())

        if let url = cardBO.imageFront {
            blockCardTransport.imageList = [CardImage(url: url)]
        }

        //when
        sut.cardBO = cardBO
        sut.showNotOperationPressed()

        //then
        XCTAssert(dummyBusManager.isCalledPublishDataForBlockCard == true)
        XCTAssert(dummyBusManager.isCalledPublishDataForCardOnOff == true)
        XCTAssert(dummyBusManager.isCalledPublishDataForCurrentCardBO == true)
        XCTAssert(dummyBusManager.blockCardTransport.cardNumber == blockCardTransport.cardNumber)
        XCTAssert(dummyBusManager.blockCardTransport.id == blockCardTransport.id)
        XCTAssert(dummyBusManager.blockCardTransport.alias == blockCardTransport.alias)
        XCTAssert(dummyBusManager.blockCardTransport.imageList?.count == blockCardTransport.imageList?.count)

    }

    func test_givenACardBOAndCardOnOffTransport_whenShowNotOperationPressed_thenICalledPublishDataForCardOnOffAndTheSameCardOnOffTransport() {

        //given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        var cardOnTransport = OnOffCardTransport(id: cardBO.cardId, cardNumber: String(cardBO.number.suffix(4)), alias: cardBO.cardAlias(), enabled: cardBO.isCardOn())

        if let url = cardBO.imageFront {
            cardOnTransport.imageList = [OnOffCardImage(url: url)]
        }

        //when
        sut.cardBO = cardBO
        sut.showNotOperationPressed()

        //then
        XCTAssert(dummyBusManager.isCalledPublishDataForCardOnOff == true)
        XCTAssert(dummyBusManager.isCalledPublishDataForCurrentCardBO == true)
        XCTAssert(dummyBusManager.cardOnOffTransport.cardNumber == cardOnTransport.cardNumber)
        XCTAssert(dummyBusManager.cardOnOffTransport.enabled == cardOnTransport.enabled)
        XCTAssert(dummyBusManager.cardOnOffTransport.id == cardOnTransport.id)
        XCTAssert(dummyBusManager.cardOnOffTransport.alias == cardOnTransport.alias)
        XCTAssert(dummyBusManager.cardOnOffTransport.imageList?.count == cardOnTransport.imageList?.count)

    }

    func test_givenACardBO_whenShowNotOperationPressed_thenICalledPublishDataForCurrentCardAndTheSameCardBO() {

        //given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]

        //when
        sut.cardBO = cardBO
        sut.showNotOperationPressed()

        //then
        XCTAssert(dummyBusManager.isCalledPublishDataForCurrentCardBO == true)
        XCTAssert(dummyBusManager.currentCardBO.cardId == cardBO.cardId)

    }

    func test_givenACardBOAndNotRecognizedOptionTransport_whenShowNotOperationPressed_thenICalledPublishDataForNotRecognizedOptionsAndTheSameTransport() {

        //given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        let isPowerOffHidden = (!cardBO.isCardOperative() || !cardBO.isCardActivationsOnOff()) ? true : !cardBO.allowCustomizeActivations()
        let notRecognizedOperationTransport = NotRecognizedOptionsTransport(isBlockHidden: !(cardBO.isCardOperative() && cardBO.isBlockable()), isPowerOffHidden: isPowerOffHidden, isCallHidden: false, isLoginHidden: true)

        //when
        sut.cardBO = cardBO
        sut.showNotOperationPressed()

        //then
        XCTAssert(dummyBusManager.isCalledPublishDataForCardOnOff == true)
        XCTAssert(dummyBusManager.isCalledPublishDataForCurrentCardBO == true)
        XCTAssert(dummyBusManager.notRecognizedOptionsTransport.isCallHidden == notRecognizedOperationTransport.isCallHidden)
        XCTAssert(dummyBusManager.notRecognizedOptionsTransport.isPowerOffHidden == notRecognizedOperationTransport.isPowerOffHidden)
        XCTAssert(dummyBusManager.notRecognizedOptionsTransport.isBlockHidden == notRecognizedOperationTransport.isBlockHidden)
        XCTAssert(dummyBusManager.notRecognizedOptionsTransport.isLoginHidden == notRecognizedOperationTransport.isLoginHidden)

    }
    
    func test_givenACardBONotOperativeAndBlockableAndNotRecognizedOptionTransport_whenShowNotOperationPressed_thenICalledPublishDataForNotRecognizedOptionsAndTheSameTransport() {
        
        //given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        cardBO.status.id = .inoperative
        let isPowerOffHidden = (!cardBO.isCardOperative() || !cardBO.isCardActivationsOnOff()) ? true : !cardBO.allowCustomizeActivations()
        let notRecognizedOperationTransport = NotRecognizedOptionsTransport(isBlockHidden: !(cardBO.isCardOperative() && cardBO.isBlockable()), isPowerOffHidden: isPowerOffHidden, isCallHidden: false, isLoginHidden: true)
        
        //when
        sut.cardBO = cardBO
        sut.showNotOperationPressed()
        
        //then
        XCTAssert(dummyBusManager.isCalledPublishDataForCardOnOff == true)
        XCTAssert(dummyBusManager.isCalledPublishDataForCurrentCardBO == true)
        XCTAssert(dummyBusManager.notRecognizedOptionsTransport.isCallHidden == notRecognizedOperationTransport.isCallHidden)
        XCTAssert(dummyBusManager.notRecognizedOptionsTransport.isPowerOffHidden == notRecognizedOperationTransport.isPowerOffHidden)
        XCTAssert(dummyBusManager.notRecognizedOptionsTransport.isBlockHidden == notRecognizedOperationTransport.isBlockHidden)
        XCTAssert(dummyBusManager.notRecognizedOptionsTransport.isLoginHidden == notRecognizedOperationTransport.isLoginHidden)

    }

    class DummyView: TransactionDetailViewProtocol {

        var displayTransactionDetail: DisplayTransactionDetail?
        var isCalledShowTransactionDetail: Bool = false
        var isCalledSendOmniture: Bool = false
        var isCalledShowOperativeBarInfo: Bool = false
        var displayDataOperativesBarInfo: ButtonsBarDisplayData?
        var isCalledButtonBarOptionPressed: Bool = false
        var displayDataButtonBar: ButtonBarDisplayData?
        var isCalledShowCallBBVA: Bool = false
        var telephone = "telephone"

        func showTransactionDetail(withTransactionDisplayItems displayItems: DisplayTransactionDetail) {

            isCalledShowTransactionDetail = true
            displayTransactionDetail = displayItems

        }

        func showError(error: ModelBO) {
        }

        func changeColorEditTexts() {
        }

        func sendOmnitureScreen() {
            isCalledSendOmniture = true
        }

        func showOperativeBarInfo(withDisplayData displayData: ButtonsBarDisplayData) {
            isCalledShowOperativeBarInfo = true
            displayDataOperativesBarInfo = displayData
        }

        func buttonBarOptionPressed(withDisplayItem displayItem: ButtonBarDisplayData) {
            isCalledButtonBarOptionPressed = true
            displayDataButtonBar = displayItem
        }

        func showCallBBVA(tel: String) {
            isCalledShowCallBBVA = true
            telephone = tel
        }
    }

    class DummyBusManager: BusManager {

        var isCalledBackAction: Bool = false
        var isCalledNavigateToNotRecognizedOperationPage = false
        var dummyTag: String = ""
        var dummyEvent: ActionSpec<Any>?
        var isCalledPublishDataForBlockCard = false
        var isCalledPublishDataForCardOnOff = false
        var isCalledPublishDataForCurrentCardBO = false
        var isCalledPublishDataForNotRecognizedOptions = false

        var blockCardTransport: BlockCardTransport!
        var cardOnOffTransport: OnOffCardTransport!
        var currentCardBO: CardBO!
        var notRecognizedOptionsTransport: NotRecognizedOptionsTransport!

        override init() {

            super.init()
            routerFactory = RouterInitFactory()
        }

        override func navigateScreen<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {

            if tag == TransactionDetailPageReaction.ROUTER_TAG && event === TransactionDetailPageReaction.ACTION_NAVIGATE_TO_NOT_OPERATION {
                isCalledNavigateToNotRecognizedOperationPage = true
            }
        }

        override func publishData<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {

            if tag == TransactionDetailPageReaction.ROUTER_TAG && event === TransactionDetailPageReaction.PARAMS_BLOCK_CARD {
                isCalledPublishDataForBlockCard = true
                blockCardTransport = BlockCardTransport.deserialize(from: values as? [String: Any])

            } else if tag == TransactionDetailPageReaction.ROUTER_TAG && event === TransactionDetailPageReaction.PARAMS_ON_OFF_CARD {
                isCalledPublishDataForCardOnOff = true
                cardOnOffTransport = OnOffCardTransport.deserialize(from: values as? [String: Any])

            } else if tag == TransactionDetailPageReaction.ROUTER_TAG && event === TransactionDetailPageReaction.PARAMS_CURRENT_CARDBO {
                isCalledPublishDataForCurrentCardBO = true
                currentCardBO = (values as! CardBO)

            } else if tag == TransactionDetailPageReaction.ROUTER_TAG && event === TransactionDetailPageReaction.PARAMS_NOT_RECOGNIZED_OPTIONS {
                isCalledPublishDataForNotRecognizedOptions = true
                notRecognizedOptionsTransport = NotRecognizedOptionsTransport.deserialize(from: values as? [String: Any])

            }

            dummyTag = tag
            dummyEvent = event as? ActionSpec<Any>
        }

        override func back() {

            isCalledBackAction = true
        }

    }
}
