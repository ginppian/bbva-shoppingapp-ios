//
//  ExpandableViewDisplayDataTest.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 21/11/2017.
//  Copyright © 2017 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class ExpandableViewDisplayDataTest: XCTestCase {

    var sut: ExpandableViewDisplayData!

    override func setUp() {
        super.setUp()
        self.sut = ExpandableViewDisplayData()

    }
    func test_givenAny_whenPendingDataGeneratedWithExpandableOption_thenIShowCorrectImageAndValue() {

        // when
        let expandableDisplayData = ExpandableViewDisplayData.generateExpandableDisplayData(withExpandableOption: true, withButtonTitle: Localizables.transactions.key_transactions_pending_text )

        // then
        XCTAssert(expandableDisplayData.pendingIcon == ConstantsImages.Common.arrow_up)
        XCTAssert(expandableDisplayData.pendingDescriptionText == Localizables.transactions.key_card_movement_details_explanation_text)
        XCTAssert(expandableDisplayData.pendingTitle == Localizables.transactions.key_transactions_pending_text)

    }

    func test_givenAny_whenPendingDataGeneratedIsCollapsed_thenIShowCorrectImageAndValue() {

        // when
        let expandableDisplayData = ExpandableViewDisplayData.generateExpandableDisplayData(withExpandableOption: false, withButtonTitle: Localizables.transactions.key_transactions_pending_text)

        // then
        XCTAssert(expandableDisplayData.pendingIcon == ConstantsImages.Common.arrow_down)
        XCTAssert(expandableDisplayData.pendingTitle == Localizables.transactions.key_transactions_pending_text)

    }
}
