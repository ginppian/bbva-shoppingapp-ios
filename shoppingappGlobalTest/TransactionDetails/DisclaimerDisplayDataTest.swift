//
//  DisclaimerDisplayDataTest.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 17/11/2017.
//  Copyright © 2017 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class DisclaimerDisplayDataTest: XCTestCase {

    func test_givenAny_whenDisclaimerDisplayDataGenerated_thenIShowCorrectImageAndValue() {

        // when
        let disclaimerDisplayData = DisclaimerDisplayData()

        // then
        XCTAssert(disclaimerDisplayData.disclaimerIcon == ConstantsImages.Common.info_icon)
        XCTAssert(disclaimerDisplayData.disclaimerText == Localizables.common.key_not_you_done_operation_text)

    }
}
