//
//  EditLimitPresenterATest.swift
//  shoppingappMXTest
//
//  Created by Magdali Grajales on 17/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest
import RxSwift
import CellsNativeComponents

#if TESTMX
@testable import shoppingappMX
#endif

class EditLimitPresenterATest: XCTestCase {
    
    var sut: EditLimitPresenterA<EditLimitsViewADummy>!
    var dummyBusManager: EditLimmitsADummyBusManager!
    var dummyInteractor: DummyInteractor!
    var dummyPreferencesManager: DummyPreferencesManager!
    var dummyView: EditLimitsViewADummy!

    override func setUp() {
        
        super.setUp()
        dummyBusManager = EditLimmitsADummyBusManager()
        sut = EditLimitPresenterA<EditLimitsViewADummy>(busManager: dummyBusManager)
        sut.interactor = DummyInteractor()
        dummyView = EditLimitsViewADummy()
        sut.view = dummyView
        
        DummyPreferencesManager.configureDummyPreferences()
        dummyPreferencesManager = PreferencesManager.instance as? DummyPreferencesManager
        sut.preferences = PreferencesManager.sharedInstance()
    }
  
    // MARK: - setModel
    // MARK: - setModel - Credit
    
    func test_givenALimitTransportWithLimitBOAndCardIdCreditCard_whenICallISetModel_thenLimitBOAndCardIdShouldMatch() {

        // given
        let limitBO = LimitsGenerator.getThreeLimits().limits[2]
        let card = CardsGenerator.getCardBOCreditCard().cards[0]

        let limitTransport = LimitTransport(withLimit: limitBO, andCardId: card.cardId, cardType: card.cardType)

        // when
        sut.setModel(model: limitTransport)

        // then
        XCTAssert(sut.limitBO == limitTransport.limit)
        XCTAssert(sut.cardId! == limitTransport.cardId)
        XCTAssert(sut.cardType?.id == limitTransport.cardType?.id)
        XCTAssert(sut.cardType?.name == limitTransport.cardType?.name)
        
    }
    
    // MARK: - setModel - Dedit
    
    func test_givenALimitTransportWithLimitBOAndCardIdDebitCard_whenICallISetModel_thenLimitBOAndCardIdShouldMatch() {
        
        // given
        let limitBO = LimitsGenerator.getThreeLimits().limits[2]
        let card = CardsGenerator.getThreeCards().cards[0]
        
        let limitTransport = LimitTransport(withLimit: limitBO, andCardId: card.cardId, cardType: card.cardType)
        
        // when
        sut.setModel(model: limitTransport)
        
        // then
        XCTAssert(sut.limitBO == limitTransport.limit)
        XCTAssert(sut.cardId! == limitTransport.cardId)
        XCTAssert(sut.cardType?.id == limitTransport.cardType?.id)
        XCTAssert(sut.cardType?.name == limitTransport.cardType?.name)
    }
    
    // MARK: - ViewWillAppear
    
    func test_givenLimitBO_whenICallViewWillAppear_thenICallSendScreenOmniture() {
        
        //given
        sut.limitBO = LimitsGenerator.getThreeLimits().limits[2]
        
        //when
        sut.viewWillAppear()
        
        //then
        XCTAssert(dummyView.isCalledSendScreenOmniture == true)
    }
    
    func test_givenLimitBO_whenICallViewWillAppear_thenICallSendScreenOmnitureWithLimitTagName() {
        
        //given
        sut.limitBO = LimitsGenerator.getThreeLimits().limits[2]
        
        //when
        sut.viewWillAppear()
        
        //then
        XCTAssert(dummyView.limitNameSent == sut.displayLimit?.tagName)
    }
    
    func test_givenLimitBO_whenICallViewWillAppear_thenICallShowNavigationTitle() {
        
        //given
        sut.limitBO = LimitsGenerator.getThreeLimits().limits[2]
        
        //when
        sut.viewWillAppear()
        
        //then
        XCTAssert(dummyView.isCalledShowNavigationTitle == true)
    }
    
    func test_givenLimitBO_whenICallViewWillAppear_thenICallShowNavigationTitleAndTitleMatch() {
        
        //given
        sut.limitBO = LimitsGenerator.getThreeLimits().limits[2]
        
        //when
        sut.viewWillAppear()
        
        //then
        XCTAssert(dummyView.navigationTitleSent == sut.displayLimit?.title)
    }
    
    func test_givenLimitBO_whenICallViewWillAppear_thenICallPrepareForAnimation() {
        
        //given
        sut.limitBO = LimitsGenerator.getThreeLimits().limits[2]
        
        //when
        sut.viewWillAppear()
        
        //then
        XCTAssert(dummyView.isCalledPrepareForAnimation == true)
    }
    
    func test_givenLimitBO_whenICallViewWillAppear_thenICallShowLimit() {
        
        //given
        sut.limitBO = LimitsGenerator.getThreeLimits().limits[2]
        
        //when
        sut.viewWillAppear()
        
        //then
        XCTAssert(dummyView.isCalledShowLimit == true)
    }

    func test_givenLimitBO_whenICallViewWillAppear_thenICalleShowLimitAndDisplayDataMatch() {
        
        //given
        sut.limitBO = LimitsGenerator.getThreeLimits().limits[2]
        
        //when
        sut.viewWillAppear()
        
        //then
        XCTAssert(dummyView.limitDisplayDataSent! == sut.displayLimit!)
    }
    
    func test_givenLimitBO_whenICallViewWillAppear_thenICallShowEditLimit() {
        
        //given
        sut.limitBO = LimitsGenerator.getThreeLimits().limits[2]
        
        //when
        sut.viewWillAppear()
        
        //then
        XCTAssert(dummyView.isCalledShowEditLimit == true)
    }
    
    func test_givenLimitBO_whenICallViewWillAppear_thenICallShowEditLimitWithAmountTextfieldDisplayDataNotNil() {
        
        //given
        sut.limitBO = LimitsGenerator.getThreeLimits().limits[2]
        
        //when
        sut.viewWillAppear()
        
        //then
        XCTAssert(dummyView.amountTextfieldDisplayDataSent != nil)
    }
    
    func test_givenLimitBO_whenICallViewWillAppear_thenICallShowEditLimitAndAmountTextfieldDisplayDataMatch() {
        
        //given
        sut.limitBO = LimitsGenerator.getThreeLimits().limits[2]
        let amountCurrencyLimit = sut.limitBO?.amountLimits?.first as? AmountCurrencyBO
        let currencyBO = Currencies.currency(forCode: (amountCurrencyLimit?.currency)!)
        let amountTextfieldDisplayData = AmountTextfieldDisplayData(placeHolder: Localizables.cards.key_card_daily_limit_multiples, initialAmount: amountCurrencyLimit?.amount.stringValue, currency: currencyBO, shouldHideErrorAutomatically: false)
        
        //when
        sut.viewWillAppear()
        
        //then
        XCTAssert(dummyView.amountTextfieldDisplayDataSent == amountTextfieldDisplayData)
    }
    
    // MARK: - ViewDidAppear
    // MARK: - ViewDidAppear - Credit
    
    func test_givenCardTypeCreditCardAndPreferenceCreditCardWithoutValue_whenICallViewDidAppear_thenICallBusManagerNavToEditHelpLimit() {
        
        // given
        let card = CardsGenerator.getCardBOCreditCard().cards[0]
        sut.cardType = card.cardType

        // when
        sut.viewDidAppear()
        
        // then
        XCTAssert(dummyBusManager.navToEditHelpLimits == true)
    }
    
    func test_givenCardTypeCreditCardAndPreferenceCreditCardShowedTrue_whenICallViewDidAppear_thenINotCallBusManagerNavToEditHelpLimit() {
        
        // given
        let card = CardsGenerator.getCardBOCreditCard().cards[0]
        dummyPreferencesManager.key = PreferencesManagerKeys.kLimitsHelpCreditCardShowed.rawValue
        dummyPreferencesManager.value = true as AnyObject
        sut.cardType = card.cardType
        
        // when
        sut.viewDidAppear()
        
        // then
        XCTAssert(dummyBusManager.navToEditHelpLimits == false)
    }
    
    func test_givenCardTypeCreditCardAndPreferenceCreditCardWithoutValue_whenICallViewDidAppear_thenICallDoEnterAnimationLayout() {
        
        // given
        let card = CardsGenerator.getCardBOCreditCard().cards[0]
        sut.cardType = card.cardType
        
        // when
        sut.viewDidAppear()
        
        // then
        XCTAssert(dummyView.isCalledDoEnterAnimationLayoutShowKeyboard == true)
    }
    
    func test_givenCardTypeCreditCardAndPreferenceCreditCardWithoutValue_whenICallViewDidAppear_thenICallDoEnterAnimationLayoutWithShowKeyboardFalse() {
        
        // given
        let card = CardsGenerator.getCardBOCreditCard().cards[0]
        sut.cardType = card.cardType
        
        // when
        sut.viewDidAppear()
        
        // then
        XCTAssert(dummyView.showKeyboardSent == false)
    }
    
    // MARK: - ViewDidAppear - Debit
    
    func test_givenCardTypeDebitCardAndPreferenceDebitCardWithoutValue_whenICallViewDidAppear_thenICallBusManagerNavToEditHelpLimit() {
        
        // given
        let card = CardsGenerator.getThreeCards().cards[0]
        sut.cardType = card.cardType
        
        // when
        sut.viewDidAppear()
        
        // then
        XCTAssert(dummyBusManager.navToEditHelpLimits == true)
    }
    
    func test_givenCardTypeDebitCardAndPreferenceDebitCardShowedTrue_whenICallViewDidAppear_thenINotCallBusManagerNavToEditHelpLimit() {
        
        // given
        let card = CardsGenerator.getThreeCards().cards[0]
        dummyPreferencesManager.key = PreferencesManagerKeys.kLimitsHelpDebitCardShowed.rawValue
        dummyPreferencesManager.value = true as AnyObject
        sut.cardType = card.cardType
        
        // when
        sut.viewDidAppear()
        
        // then
        XCTAssert(dummyBusManager.navToEditHelpLimits == false)
    }
    
    func test_givenCardTypeDebitCardAndPreferenceDebitCardWithoutValue_whenICallViewDidAppear_thenICallDoEnterAnimationLayout() {
        
        // given
        let card = CardsGenerator.getThreeCards().cards[0]
        sut.cardType = card.cardType
        
        // when
        sut.viewDidAppear()
        
        // then
        XCTAssert(dummyView.isCalledDoEnterAnimationLayoutShowKeyboard == true)
    }
    
    func test_givenCardTypeDebitCardAndPreferenceDebitCardWithoutValue_whenICallViewDidAppear_thenICallDoEnterAnimationLayoutWithShowKeyboardFalse() {
        
        // given
        let card = CardsGenerator.getThreeCards().cards[0]
        sut.cardType = card.cardType
        
        // when
        sut.viewDidAppear()
        
        // then
        XCTAssert(dummyView.showKeyboardSent == false)
    }
    
    // MARK: - ViewDidAppear - super.viewDidAppear
    
    func test_givenCardTypeCreditCardAndPreferenceCreditCardShowedTrue_whenICallViewDidAppear_thenICallDoEnterAnimationLayout() {
        
        // given
        let card = CardsGenerator.getCardBOCreditCard().cards[0]
        dummyPreferencesManager.key = PreferencesManagerKeys.kLimitsHelpCreditCardShowed.rawValue
        dummyPreferencesManager.value = true as AnyObject
        sut.cardType = card.cardType
        
        // when
        sut.viewDidAppear()
        
        // then
        XCTAssert(dummyView.isCalledDoEnterAnimationLayout == true)
    }
    
    func test_givenCardTypeDebitCardAndPreferenceDebitCardShowedTrue_whenICallViewDidAppear_thenICallDoEnterAnimationLayout() {
        
        // given
        let card = CardsGenerator.getThreeCards().cards[0]
        dummyPreferencesManager.key = PreferencesManagerKeys.kLimitsHelpDebitCardShowed.rawValue
        dummyPreferencesManager.value = true as AnyObject
        sut.cardType = card.cardType
        
        // when
        sut.viewDidAppear()
        
        // then
        XCTAssert(dummyView.isCalledDoEnterAnimationLayout == true)
    }
    
    func test_givenCardTypeDiferentToCreditOrDebitCard_whenICallViewDidAppear_thenICallDoEnterAnimationLayout() {
        
        // given
        let card = CardsGenerator.getThreeCards().cards[0]
        card.cardType.id = .prepaid_card
        sut.cardType = card.cardType
        
        // when
        sut.viewDidAppear()
        
        // then
        XCTAssert(dummyView.isCalledDoEnterAnimationLayout == true)
    }
    
    func test_givenCardTypeUnknown_whenICallViewDidAppear_thenICallDoEnterAnimationLayout() {
        
        // given
        let card = CardsGenerator.getThreeCards().cards[0]
        card.cardType.id = .unknown
        sut.cardType = card.cardType
        
        // when
        sut.viewDidAppear()
        
        // then
        XCTAssert(dummyView.isCalledDoEnterAnimationLayout == true)
    }
    
    // MARK: - viewWillAppearAfterDismiss
    
    func test_givenDisplayLimit_whenICallViewWillAppearAfterDismiss_thenICallSendScreenOmniture() {
        
        //given
        let limitBO = LimitsGenerator.getThreeLimits().limits[2]
        sut.displayLimit = LimitDisplayData.generateDisplayDataForEdit(fromLimit: limitBO)
        
        //when
        sut.viewWillAppearAfterDismiss()
        
        //then
        XCTAssert(dummyView.isCalledSendScreenOmniture == true)
    }
    
    func test_givenDisplayLimit_whenICallViewWillAppearAfterDismiss_thenICallSendScreenOmnitureWithLimitTagName() {

        //given
        let limitBO = LimitsGenerator.getThreeLimits().limits[2]
        sut.displayLimit = LimitDisplayData.generateDisplayDataForEdit(fromLimit: limitBO)
        
        //when
        sut.viewWillAppearAfterDismiss()
        
        //then
        XCTAssert(dummyView.limitNameSent == sut.displayLimit?.tagName)
    }
    
    func test_givenIsShowLoadingFalse_whenICallViewWillAppearAfterDismiss_thenICallDoEnterAnimationLayout() {
        
        //given
        sut.isShowLoading = false
        
        //when
        sut.viewWillAppearAfterDismiss()
        
        //then
        XCTAssert(dummyView.isCalledDoEnterAnimationLayoutShowKeyboard == true)
    }
    
    func test_givenIsShowLoadingFalse_whenICallViewWillAppearAfterDismiss_thenICallDoEnterAnimationLayoutAndShowKeyboardTrue() {
        
        //given
        sut.isShowLoading = false
        
        //when
        sut.viewWillAppearAfterDismiss()
        
        //then
        XCTAssert(dummyView.showKeyboardSent == true)
    }
    
    func test_givenIsShowLoadingTrue_whenICallViewWillAppearAfterDismiss_thenINotCallDoEnterAnimationLayout() {
        
        //given
        sut.isShowLoading = true
        
        //when
        sut.viewWillAppearAfterDismiss()
        
        //then
        XCTAssert(dummyView.isCalledDoEnterAnimationLayoutShowKeyboard == false)
    }
    
    // MARK: - SetInfoMessage
    
    func test_givenLimitBO_whenICallSetInfoMessage_thenReturnedMessageMatch() {
        
        //given
        let limitBO = LimitsGenerator.getThreeLimits().limits[0]
        let expectedMessage = limitBO.infoMessageMultiplesAndMaximum()
        
        //when
        let returnedMessage = sut.setInfoMessage(with: limitBO)
        
        //then
        XCTAssert(expectedMessage == returnedMessage)
    }
    
    class EditLimitsViewADummy: EditLimitViewProtocolA, ViewProtocol {
        
        var isCalledShowNavigationTitle = false
        var navigationTitleSent = ""
        
        var isCalledDoEnterAnimationLayout = false
        var isCalledSendScreenOmniture = false
        var isCalledDoEnterAnimationLayoutShowKeyboard = false
        var isCalledShowLimit = false
        var isCalledPrepareForAnimation = false

        var isCalledShowSuccessfulViewPresenterA = false
        var isCalledShowEditLimit = false

        var limitNameSent = ""
        var limitDisplayDataSent: LimitDisplayData?
        var amountTextfieldDisplayDataSent: AmountTextfieldDisplayData?
        var showKeyboardSent = false

        func showError(error: ModelBO) {
        }
        
        func changeColorEditTexts() {
        }
        
        func showErrorMessageAlreadyConsumed(withMessage message: String) {
        }
        
        func showErrorMessage(withMessage message: String) {
        }
        
        func showSuccessMessage(withMessage message: String) {
        }
        
        //-------------------
        
        func doEnterAnimationLayout() {
            
            isCalledDoEnterAnimationLayout = true
        }
        
        func doEnterAnimationLayout(showKeyboard show: Bool) {
            
            isCalledDoEnterAnimationLayoutShowKeyboard = true
            showKeyboardSent = show
        }
        
        func sendScreenOmniture(withLimitName limitName: String) {
            
            isCalledSendScreenOmniture = true
            limitNameSent = limitName
        }
        
        func showNavigation(withTitle title: String) {
            
            isCalledShowNavigationTitle = true
            navigationTitleSent = title
        }
        
        func prepareForAnimation() {
            
            isCalledPrepareForAnimation = true
        }
        
        func showLimit(withDisplayData displayData: LimitDisplayData) {
            
            isCalledShowLimit = true
            limitDisplayDataSent = displayData
        }
        
        func showEditLimit(withDisplayData displayData: AmountTextfieldDisplayData) {
            
            isCalledShowEditLimit = true
            amountTextfieldDisplayDataSent = displayData
        }
    }
    
    class EditLimmitsADummyBusManager: EditLimmitsDummyBusManager {
        
        var navToEditHelpLimits = false
        
        override func navigateScreen<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {
            
            if tag == EditLimitPageReactionA.ROUTER_TAG && event === EditLimitPageReactionA.eventNavToEditHelpLimits {
                
                navToEditHelpLimits = true
            }
        }
    }
}
