//
//  EditLimitPresenterTest.swift
//  shoppingapp
//
//  Created by Marcos on 26/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest
import RxSwift
import CellsNativeComponents

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class EditLimitPresenterTest: XCTestCase {

    var sut: EditLimitsPresenter<EditLimitsViewDummy>!
    var dummyBusManager: EditLimmitsDummyBusManager!

    override func setUp() {
        super.setUp()
        dummyBusManager = EditLimmitsDummyBusManager()
        sut = EditLimitsPresenter<EditLimitsViewDummy>(busManager: dummyBusManager)

    }

    // MARK: - setModel

    func test_givenALimitTransportWithLimitBOAndCardId_whenICallISetModel_thenLimitBOAndCardIdShouldMatch() {

        // given
        let limitBO = LimitsGenerator.getThreeLimits().limits[0]
        let limitTransport = LimitTransport(withLimit: limitBO, andCardId: "1")

        // when
        sut.setModel(model: limitTransport)

        // then
        XCTAssert(sut.limitBO === limitTransport.limit)
        XCTAssert(sut.cardId! == limitTransport.cardId)

    }

    // MARK: - ViewWillAppear

    func test_givenLimitBO_whenICallViewWillAppear_thenIsCallSendScreenOmniture() {

        //given
        let dummyiew = EditLimitsViewDummy()
        sut.view = dummyiew
        sut.limitBO = LimitsGenerator.getThreeLimits().limits[0]

        //when
        sut.viewWillAppear()

        //then
        XCTAssert(dummyiew.isCalledSendScreenOmniture == true)

    }

    func test_givenLimitBO_whenICallViewWillAppear_thenIsCallSendScreenOmnitureWithLimitTagName() {

        //given
        let dummyiew = EditLimitsViewDummy()
        sut.view = dummyiew
        sut.limitBO = LimitsGenerator.getThreeLimits().limits[0]

        //when
        sut.viewWillAppear()

        //then
        XCTAssert(dummyiew.sentLimitName == sut.displayLimit?.tagName)

    }

    func test_givenLimitBO_whenICallViewWillAppear_thenIsCallShowNavigationTitle() {

        //given
        let dummyiew = EditLimitsViewDummy()
        sut.view = dummyiew
        sut.limitBO = LimitsGenerator.getThreeLimits().limits[0]

        //when
        sut.viewWillAppear()

        //then
        XCTAssert(dummyiew.isSCalledShowNavigationTitle == true)
        XCTAssert(dummyiew.sentTitleId == sut.displayLimit?.title)

    }

    func test_givenLimitBO_whenICallViewWillAppear_thenIsCallPrepareForEnterAnimation() {

        //given
        let dummyiew = EditLimitsViewDummy()
        sut.view = dummyiew
        sut.limitBO = LimitsGenerator.getThreeLimits().limits[0]

        //when
        sut.viewWillAppear()

        //then
        XCTAssert(dummyiew.isCalledPrepareForAnimation == true)

    }

    func test_givenLimitBO_whenICallViewWillAppear_thenIsCallShowLimit() {

        //given
        let dummyiew = EditLimitsViewDummy()
        sut.view = dummyiew
        sut.limitBO = LimitsGenerator.getThreeLimits().limits[0]

        //when
        sut.viewWillAppear()

        //then
        XCTAssert(dummyiew.isCalledShowLimit == true)
        XCTAssert(dummyiew.sentDisplayData! == sut.displayLimit!)

    }

    func test_givenAny_whenICallViewVillAppear_thenICallViewShowEditLimitView() {

        let dummyView = EditLimitsViewDummy()
        sut.view = dummyView
        sut.limitBO = LimitsGenerator.getThreeLimits().limits[0]

        //given

        //when
        sut.viewWillAppear()

        //then
        XCTAssert(dummyView.isCalledShowEditLimitView == true)

    }

    func test_givenAny_whenICallViewVillAppear_thenICallViewShowEditLimitViewWithDisplayDataWithPlacerholderCorrect() {

        let dummyView = EditLimitsViewDummy()
        sut.view = dummyView
        sut.limitBO = LimitsGenerator.getThreeLimits().limits[0]

        //given

        //when
        sut.viewWillAppear()

        //then
        XCTAssert(dummyView.amountTextfieldDisplayDataSent != nil)
        XCTAssert(dummyView.amountTextfieldDisplayDataSent!.placeHolder == sut.limitBO?.type.inputAmountLimitTitle)
        XCTAssert(dummyView.amountTextfieldDisplayDataSent?.shouldHideErrorAutomatically == false)

    }
    
    // MARK: - viewWillAppearAfterDismiss
    
    func test_givenDisplayLimit_whenICallViewWillAppearAfterDismiss_thenIsCallSendScreenOmniture() {
        
        //given
        let dummyiew = EditLimitsViewDummy()
        sut.view = dummyiew
        let limitBO = LimitsGenerator.getThreeLimits().limits[0]
        sut.displayLimit = LimitDisplayData.generateDisplayDataForEdit(fromLimit: limitBO)
        
        //when
        sut.viewWillAppearAfterDismiss()
        
        //then
        XCTAssert(dummyiew.isCalledSendScreenOmniture == true)
        
    }
    
    func test_givenDisplayLimit_whenICallViewWillAppearAfterDismiss_thenIsCallSendScreenOmnitureWithLimitTagName() {
        
        //given
        let dummyiew = EditLimitsViewDummy()
        sut.view = dummyiew
        let limitBO = LimitsGenerator.getThreeLimits().limits[0]
        sut.displayLimit = LimitDisplayData.generateDisplayDataForEdit(fromLimit: limitBO)
        
        //when
        sut.viewWillAppearAfterDismiss()
        
        //then
        XCTAssert(dummyiew.sentLimitName == sut.displayLimit?.tagName)
        
    }

    // MARK: - ViewDidAppear

    func test_givenLimitBO_whenICallViewDidAppear_thenIsCallDoEnterAnimation() {

        //given
        let dummyiew = EditLimitsViewDummy()
        sut.view = dummyiew
        sut.limitBO = LimitsGenerator.getThreeLimits().limits[0]

        //when
        sut.viewDidAppear()

        //then
        XCTAssert(dummyiew.isCalledDoAnimation == true)

    }

    // MARK: - SetInfoMessage
   
    func test_givenLimitBOWithAmount_whenICallInfoMessage_thenIReturnValue() {
        
        // given
        let limitBO = LimitsGenerator.getThreeLimits().limits[0]
        sut.limitBO = limitBO
        
        let minimumFormatter = limitBO.minimumLimitFormatter()
        let maximumFormatter = limitBO.maximumLimitFormatter()
        let valueExpected = Localizables.cards.key_card_limit_min_text + " \(minimumFormatter) " + Localizables.cards.key_card_limit_and_max_text + " \(maximumFormatter)"
     
        // when
        let valueInfo = sut.limitBO?.infoMessage()
        
        // then
        XCTAssert(valueInfo == valueExpected)

    }
    
    // MARK: - updateLimitText
    
    func test_givenAny_whenICallUpdateLimitTextWithText_thenIAmountShouldBeCorrect() {

        //given
        let dummyiew = EditLimitsViewDummy()
        sut.view = dummyiew
        sut.limitBO = LimitsGenerator.getThreeLimits().limits[0]

        //when
        sut.updateLimitText(withAmount: NSDecimalNumber(string: "10"))

        //then
        XCTAssert(sut.limitAmount?.stringValue == "10")

    }

    // MARK: - conmfirm button

    func test_givenLimitBOAndCardIDAndLimitAmountNotANumber_whenICallConfirmButtonPressed_thenIDoNotCallProviderUdapteLimits() {

        //given
        sut.limitBO = LimitsGenerator.getThreeLimits().limits[0]
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        sut.limitAmount = .notANumber

        //when
        sut.confirmButtonPressed()

        //then
        XCTAssert(dummyInteractor.isCalledUdapteLimits == false)

    }

    func test_givenAny_whenICallConfirmButtonAndEmptyLimitTex_thenIDoNotCallProviderUdapteLimit() {

        //given
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        let dummyiew = EditLimitsViewDummy()
        sut.view = dummyiew
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        sut.cardId = cards.cards[0].cardId
        sut.limitBO = LimitsGenerator.getThreeLimits().limits[0]

        // when
        sut.confirmButtonPressed()

        // then
        XCTAssert(dummyInteractor.isCalledUdapteLimits == false)
    }

    func test_givenLimitBOAndCardIDAndNewLimitTextAndLimitIsEditableFalse_whenICallConfirmButtonPressed_thenIDoNotCallProviderUdapteLimit() {

        //given
        sut.limitBO = LimitsGenerator.getThreeLimits().limits[0]
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        sut.cardId = cards.cards[0].cardId
        sut.limitAmount = NSDecimalNumber(string: "10")
        sut.limitIsEditable = false

        //when
        sut.confirmButtonPressed()

        //then
        XCTAssert(dummyInteractor.isCalledUdapteLimits == false)

    }

    func test_givenLimitBOAndCardIDAndNewLimitText_whenICallConfirmButtonPressed_thenICallProviderUdapteLimits() {

        //given
        sut.limitBO = LimitsGenerator.getThreeLimits().limits[0]
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        sut.cardId = cards.cards[0].cardId
        sut.limitAmount = NSDecimalNumber(string: "10")
        sut.limitIsEditable = true

        //when
        sut.confirmButtonPressed()

        //then
        XCTAssert(dummyInteractor.isCalledUdapteLimits == true)

    }

    func test_givenLimitBOAndCardIDAndNewLimitTex_whenICallConfirmButtonPressedAndReturnOk_thenICallRouterNavigateToLimitScreen() {

        //given
        sut.limitBO = LimitsGenerator.getThreeLimits().limits[0]
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        sut.cardId = cards.cards[0].cardId
        sut.limitAmount = NSDecimalNumber(string: "10")
        sut.limitIsEditable = true

        //when
        sut.confirmButtonPressed()

        //then
        XCTAssert(dummyBusManager.isCalledShowLimits == true)

    }
    
    func test_givenLimitBOAndCardIDAndNewLimitTex_whenICallConfirmButtonPressedAndReturnOk_thenLimitAmountCurrencyMatch() {
        
        //given
        sut.limitBO = LimitsGenerator.getThreeLimits().limits[0]
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        sut.cardId = cards.cards[0].cardId
        sut.limitAmount = NSDecimalNumber(string: "10")
        sut.limitIsEditable = true
        let amountCurrencyExpected = sut.limitBO?.amountLimits?.first as? AmountCurrencyBO
        
        //when
        sut.confirmButtonPressed()
        
        // then
        XCTAssert(amountCurrencyExpected?.amount == sut.limitAmount)
    }

    func test_givenLimitBOAndCardIDAndNewLimitTex_whenICallConfirmButtonPressedAndReturnOk_thenLimitTransportShouldBeCorrect() {

        //given
        sut.limitBO = LimitsGenerator.getThreeLimits().limits[0]
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        sut.cardId = cards.cards[0].cardId
        sut.limitAmount = NSDecimalNumber(string: "10")
        sut.limitIsEditable = true

        //when
        sut.confirmButtonPressed()

        // then
        XCTAssert(dummyBusManager.limitTransport?.limit.name == sut.limitBO?.name)
        XCTAssert(dummyBusManager.limitTransport?.limit.amountLimits![0]!.amount.stringValue == "10")

    }

    func test_givenAny_whenICallConfirmButtonAndNewLimitTex_thenICallViewShowLoading() {

        //given
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        let dummyiew = EditLimitsViewDummy()
        sut.view = dummyiew
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        sut.cardId = cards.cards[0].cardId
        sut.limitBO = LimitsGenerator.getThreeLimits().limits[0]
        sut.limitAmount = NSDecimalNumber(string: "10")
        sut.limitIsEditable = true

        // when
        sut.confirmButtonPressed()

        // then
        XCTAssert(dummyBusManager.isCalledShowLoading == true)
    }

    func test_givenAny_whenICallConfirmButtonAndReturnOk_thenICallDismissLoading() {

        //given
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        let dummyiew = EditLimitsViewDummy()
        sut.view = dummyiew
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        sut.cardId = cards.cards[0].cardId
        sut.limitBO = LimitsGenerator.getThreeLimits().limits[0]
        sut.limitAmount = NSDecimalNumber(string: "10")
        sut.limitIsEditable = true

        // when
        sut.confirmButtonPressed()

        // then
        XCTAssert(dummyBusManager.isCalledHideLoading == true)
    }

    func test_givenAny_whenICallConfirmButtonAndReturnError_thenICallDismissLoading() {

        //given
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        let dummyiew = EditLimitsViewDummy()
        sut.view = dummyiew
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        sut.cardId = cards.cards[0].cardId
        sut.limitBO = LimitsGenerator.getThreeLimits().limits[0]
        sut.limitAmount = NSDecimalNumber(string: "10")
        sut.limitIsEditable = true

        // when
        sut.confirmButtonPressed()

        // then
        XCTAssert(dummyBusManager.isCalledHideLoading == true)
    }

    func test_givenAny_whenICallConfirmButtonAndInteractorReturnsError_thenErrorBOShouldBeFilled() {

        //given
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        let dummyiew = EditLimitsViewDummy()
        sut.view = dummyiew
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        sut.cardId = cards.cards[0].cardId
        sut.limitBO = LimitsGenerator.getThreeLimits().limits[0]
        sut.limitAmount = NSDecimalNumber(string: "10")
        sut.limitIsEditable = true

        dummyInteractor.forceError = true

        // when
        sut.confirmButtonPressed()

        // then
        XCTAssert(sut.errorBO != nil)
    }

    func test_givenAny_whenICallConfirmButtonAndInteractorReturnsError_thenErrorBOShouldMatch() {

        //given
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        let dummyiew = EditLimitsViewDummy()
        sut.view = dummyiew
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        sut.cardId = cards.cards[0].cardId
        sut.limitBO = LimitsGenerator.getThreeLimits().limits[0]
        sut.limitIsEditable = true

        dummyInteractor.forceError = true

        // when
        sut.confirmButtonPressed()

        // then
        XCTAssert(sut.errorBO === dummyInteractor.errorBO)
    }

    // MARK: - textFieldDidChange

    func test_givenlimitBONil_whenICallTextFieldDidChange_thenNoMessageIsDisplayed() {

        // given
        let dummyiew = EditLimitsViewDummy()
        sut.view = dummyiew
        sut.limitBO = nil

        let inputText = ""

        // when
        sut.textFieldDidChange(withText: inputText)

        // then
        XCTAssert(dummyiew.isCalledShowErrorMessageAlreadyConsumed == false)
        XCTAssert(dummyiew.isCalledShowErrorMessage == false)
        XCTAssert(dummyiew.isCalledShowSuccessMessage == false)
        XCTAssert(sut.limitIsEditable == true)

    }

    func test_givenlimitBOAndInputTextEmpty_whenICallTextFieldDidChange_thenOnlyShowSuccessMessage() {

        // given
        let dummyiew = EditLimitsViewDummy()
        sut.view = dummyiew
        sut.limitBO = LimitsGenerator.getThreeLimits().limits[0]

        let inputText = ""

        // when
        sut.textFieldDidChange(withText: inputText)

        // then
        XCTAssert(dummyiew.isCalledShowErrorMessageAlreadyConsumed == false)
        XCTAssert(dummyiew.isCalledShowErrorMessage == false)
        XCTAssert(dummyiew.isCalledShowSuccessMessage == true)
        XCTAssert(dummyiew.sentMessage == sut.limitBO?.infoMessage())
        XCTAssert(sut.limitIsEditable == true)

    }

    func test_givenlimitBOAndInputTextIsLessThanMinimumLimitAndMinimumLimitEqualToConsumedAmount_whenICallTextFieldDidChange_thenOnlyShowErrorMessageAlreadyConsumed() {

        // given
        let dummyiew = EditLimitsViewDummy()
        sut.view = dummyiew
        sut.limitBO = LimitsGenerator.getThreeLimits().limits[0]
        sut.limitBO?.disposedLimits![0]!.amount = NSDecimalNumber(string: "1000")
        sut.limitBO?.allowedInterval?.minimumAmount?.amount = NSDecimalNumber(string: "50")

        let inputText = "500"

        // when
        sut.textFieldDidChange(withText: inputText)

        // then
        XCTAssert(dummyiew.isCalledShowErrorMessageAlreadyConsumed == true)
        XCTAssert(dummyiew.isCalledShowErrorMessage == false)
        XCTAssert(dummyiew.isCalledShowSuccessMessage == false)
        XCTAssert(dummyiew.sentMessage == sut.limitBO?.messageAlreadyConsumed())
        XCTAssert(sut.limitIsEditable == false)

    }

    func test_givenlimitBOAndInputTextIsLessThanMinimumLimitAndMinimumLimitEqualToMinimumAmount_whenICallTextFieldDidChange_thenOnlyShowErrorMessage() {

        // given
        let dummyiew = EditLimitsViewDummy()
        sut.view = dummyiew
        sut.limitBO = LimitsGenerator.getThreeLimits().limits[0]
        sut.limitBO?.disposedLimits![0]!.amount = NSDecimalNumber(string: "50")
        sut.limitBO?.allowedInterval?.minimumAmount?.amount = NSDecimalNumber(string: "1000")

        let inputText = "500"

        // when
        sut.textFieldDidChange(withText: inputText)

        // then
        XCTAssert(dummyiew.isCalledShowErrorMessageAlreadyConsumed == false)
        XCTAssert(dummyiew.isCalledShowErrorMessage == true)
        XCTAssert(dummyiew.isCalledShowSuccessMessage == false)
        XCTAssert(dummyiew.sentMessage == sut.limitBO?.messageMinimum())
        XCTAssert(sut.limitIsEditable == false)

    }

    func test_givenlimitBOAndInputTextIsGreaterThanMinimumLimitAndGreaterThanMaximumLimit_whenICallTextFieldDidChange_thenOnlyShowErrorMessage() {

        // given
        let dummyiew = EditLimitsViewDummy()
        sut.view = dummyiew
        sut.limitBO = LimitsGenerator.getThreeLimits().limits[0]
        sut.limitBO?.disposedLimits![0]!.amount = NSDecimalNumber(string: "50")
        sut.limitBO?.allowedInterval?.minimumAmount?.amount = NSDecimalNumber(string: "1000")
        sut.limitBO?.allowedInterval?.maximumAmount?.amount = NSDecimalNumber(string: "2000")

        let amountFormatter = (sut.limitBO?.amountFormatterForLimit())!
        let maximumLimit = sut.limitBO?.maximumLimitDecimalNumber()
        let maximumLimitFormatter = amountFormatter.format(amount: maximumLimit!)

        let inputText = "5000"

        // when
        sut.textFieldDidChange(withText: inputText)

        // then
        XCTAssert(dummyiew.isCalledShowErrorMessageAlreadyConsumed == false)
        XCTAssert(dummyiew.isCalledShowErrorMessage == true)
        XCTAssert(dummyiew.isCalledShowSuccessMessage == false)
        XCTAssert(dummyiew.sentMessage == Localizables.cards.key_card_limit_max_text + " \(maximumLimitFormatter)")
        XCTAssert(sut.limitIsEditable == false)

    }

    func test_givenlimitBOAndInputTextCorrectAndGreaterThanMultipleButNotMultiple_whenICallTextFieldDidChange_thenOnlyShowErrorMessage() {

        // given
        let dummyiew = EditLimitsViewDummy()
        sut.view = dummyiew
        sut.limitBO = LimitsGenerator.getThreeLimits().limits[0]
        sut.limitBO?.disposedLimits![0]!.amount = NSDecimalNumber(string: "1000")
        sut.limitBO?.allowedInterval?.minimumAmount?.amount = NSDecimalNumber(string: "1000")
        sut.limitBO?.allowedInterval?.maximumAmount?.amount = NSDecimalNumber(string: "5000")

        let amountFormatter = (sut.limitBO?.amountFormatterForLimit())!
        let multiple = NSDecimalNumber(string: "\(Settings.Limits.multiples_limit_amount)")
        let multipleFormatter = amountFormatter.format(amount: multiple)

        let inputText = "4004"

        // when
        sut.textFieldDidChange(withText: inputText)

        // then
        XCTAssert(dummyiew.isCalledShowErrorMessageAlreadyConsumed == false)
        XCTAssert(dummyiew.isCalledShowErrorMessage == true)
        XCTAssert(dummyiew.isCalledShowSuccessMessage == false)
        XCTAssert(dummyiew.sentMessage == Localizables.cards.key_card_only_multiples + " \(multipleFormatter)")
        XCTAssert(sut.limitIsEditable == false)

    }

    func test_givenlimitBOAndInputTextCorrectAndLessThanMultipleButNotMultiple_whenICallTextFieldDidChange_thenOnlyShowErrorMessage() {

        // given
        let dummyiew = EditLimitsViewDummy()
        sut.view = dummyiew
        sut.limitBO = LimitsGenerator.getThreeLimits().limits[0]
        sut.limitBO?.disposedLimits![0]!.amount = NSDecimalNumber(string: "1000")
        sut.limitBO?.allowedInterval?.minimumAmount?.amount = NSDecimalNumber(string: "1000")
        sut.limitBO?.allowedInterval?.maximumAmount?.amount = NSDecimalNumber(string: "5000")

        sut.multipleLimit = 2000.0

        let amountFormatter = (sut.limitBO?.amountFormatterForLimit())!
        let multiple = NSDecimalNumber(string: "\(sut.multipleLimit)")
        let multipleFormatter = amountFormatter.format(amount: multiple)

        let inputText = "1004"

        // when
        sut.textFieldDidChange(withText: inputText)

        // then
        XCTAssert(dummyiew.isCalledShowErrorMessageAlreadyConsumed == false)
        XCTAssert(dummyiew.isCalledShowErrorMessage == true)
        XCTAssert(dummyiew.isCalledShowSuccessMessage == false)
        XCTAssert(dummyiew.sentMessage == Localizables.cards.key_card_only_multiples + " \(multipleFormatter)")
        XCTAssert(sut.limitIsEditable == false)

    }

    func test_givenlimitBOAndInputTextCorrect_whenICallTextFieldDidChange_thenOnlyShowSuccessMessage() {

        // given
        let dummyiew = EditLimitsViewDummy()
        sut.view = dummyiew
        sut.limitBO = LimitsGenerator.getThreeLimits().limits[0]
        sut.limitBO?.disposedLimits![0]!.amount = NSDecimalNumber(string: "1000")
        sut.limitBO?.allowedInterval?.minimumAmount?.amount = NSDecimalNumber(string: "1000")
        sut.limitBO?.allowedInterval?.maximumAmount?.amount = NSDecimalNumber(string: "5000")

        let inputText = "4000"

        // when
        sut.textFieldDidChange(withText: inputText)

        // then
        XCTAssert(dummyiew.isCalledShowErrorMessageAlreadyConsumed == false)
        XCTAssert(dummyiew.isCalledShowErrorMessage == false)
        XCTAssert(dummyiew.isCalledShowSuccessMessage == true)
        XCTAssert(dummyiew.sentMessage == sut.limitBO?.infoMessage())
        XCTAssert(sut.limitIsEditable == true)

    }

}

class EditLimmitsDummyBusManager: BusManager {

        var isCalledShowLoading = false
        var isCalledHideLoading = false

        var isCalledShowLimits = false
        var limitTransport: LimitTransport?

        override init() {
            super.init()
            routerFactory = RouterInitFactory()
        }

        override func navigateScreen<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {
            if tag == EditLimitPageReaction.ROUTER_TAG && event === EditLimitPageReaction.EVENT_NAV_TO_LIMITS {
                isCalledShowLimits = true
                limitTransport = (values as! LimitTransport)
            }
        }

        override func showLoading(completion: (() -> Void)?) {
            completion?()
            isCalledShowLoading = true
        }

        override  func hideLoading(completion: (() -> Void)? ) {
            if let completion = completion {
                completion()
            }
            isCalledHideLoading = true
        
    }

}
    class DummyInteractor: EditLimitsInteractorProtocol {

        var isCalledUdapteLimits = false

        var errorBO: ErrorBO?
        var forceError = false

         func udapteLimits(forCardId cardId: String, forLimitId limitId: String, withNewLimit newLimit: AmountCurrencyBO) -> Observable<ModelBO> {
            isCalledUdapteLimits = true

            if !forceError {

                let limitBO = LimitsGenerator.getThreeLimits().limits[0]
                return Observable <ModelBO>.just(limitBO.amountLimits![0]!)

            } else {

                if errorBO == nil {
                    errorBO = ErrorBO(error: ErrorEntity(message: "Error"))
                    errorBO!.isErrorShown = false
                }

                return Observable.error(ServiceError.GenericErrorBO(error: errorBO!))

            }
        }
    }

    class EditLimitsViewDummy: EditLimitsViewProtocol {

        var isCalledSendScreenOmniture = false
        var isSCalledShowNavigationTitle = false
        var isCalledPrepareForAnimation = false
        var isCalledDoAnimation = false
        var isCalledShowLimit = false
        var isCalledShowError = false
        var isCalledShowEmptyTsec = false
        var isCalledShowLoading = false

        var isCalledShowSuccessfulView = false
        var isCalledShowEditLimitView = false

        var sentLimitName = ""
        var sentTitleId = ""
        var sentDisplayData: LimitDisplayData?
        var amountTextfieldDisplayDataSent: AmountTextfieldDisplayData?

        var isCalledShowErrorMessageAlreadyConsumed = false
        var isCalledShowErrorMessage = false
        var isCalledShowSuccessMessage = false
        var sentMessage = ""

        func sendScreenOmniture(withLimitName limitName: String) {

            isCalledSendScreenOmniture = true
            sentLimitName = limitName
        }

        func showNavigation(withTitle title: String) {

            isSCalledShowNavigationTitle = true
            sentTitleId = title
        }

        func prepareForAnimation() {

            isCalledPrepareForAnimation = true
        }

        func doEnterAnimationLayout() {

            isCalledDoAnimation = true
        }

        func showLimit(withDisplayData displayData: LimitDisplayData) {

            isCalledShowLimit = true
            sentDisplayData = displayData
        }

        func showErrorEmptyTsec() {

            isCalledShowEmptyTsec = true
        }

        func showEditLimitSuccessfulInfoView() {

            isCalledShowSuccessfulView = true
        }

        func showError(error: ModelBO) {

            isCalledShowError = true
        }

        func changeColorEditTexts() {

        }

        func showEditLimit(withDisplayData displayData: AmountTextfieldDisplayData) {

            isCalledShowEditLimitView = true
            amountTextfieldDisplayDataSent = displayData
        }

        func showErrorMessageAlreadyConsumed(withMessage message: String) {

            isCalledShowErrorMessageAlreadyConsumed = true
            sentMessage = message
        }

        func showErrorMessage(withMessage message: String) {

            isCalledShowErrorMessage = true
            sentMessage = message
        }

        func showSuccessMessage(withMessage message: String) {

            isCalledShowSuccessMessage = true
            sentMessage = message
        }
    
}
