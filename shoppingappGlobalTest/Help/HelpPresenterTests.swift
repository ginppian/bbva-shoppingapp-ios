//
//  HelpPresenterTests.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 25/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest
import CellsNativeComponents

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class HelpPresenterTests: XCTestCase {

    var sut: HelpPresenter<HelpViewDummy>!
    var dummyBusManager: DummyBusManager!
    var sutConfigPublicManager: ConfigPublicManager!

    override func setUp() {
        super.setUp()
        dummyBusManager = DummyBusManager()
        sut = HelpPresenter<HelpViewDummy>(busManager: dummyBusManager)

        sutConfigPublicManager = ConfigPublicManager.sharedInstance()
    }

    // MARK: - viewDidLoad

    func test_givenAny_whenICallViewDidLoad_thenICallShowHelp() {

        //given
        let dummyView = HelpViewDummy()
        sut.view = dummyView

        //When
        sut.viewDidLoad()

        //then
        XCTAssert(dummyView.isCalledShowHelp == true)

    }

    func test_givenNothing_whenICallViewDidLoad_thenDisplayDataHelpIsEmpty() {

        //given
        let dummyView = HelpViewDummy()
        sut.view = dummyView

        sutConfigPublicManager.configs = nil

        //When
        sut.viewDidLoad()

        //then
        XCTAssert(dummyView.displayDataHelp.isEmpty)

    }

    func test_givenHelpBO_whenICallViewDidLoad_thenDisplayDataHelpIsFilled() {

        //given
        let dummyView = HelpViewDummy()
        sut.view = dummyView

        sutConfigPublicManager.configs = ConfigGenerator.getHelpBO()

        //When
        sut.viewDidLoad()

        //then
        XCTAssert(!dummyView.displayDataHelp.isEmpty)

    }

    func test_givenHelpBO_whenICallViewDidLoad_thenDisplayDataHelpMatch() {

        //given
        let dummyView = HelpViewDummy()
        sut.view = dummyView

        sutConfigPublicManager.configs = ConfigGenerator.getHelpBO()

        //When
        sut.viewDidLoad()

        //then
        var index = 0

        for configBO in (sutConfigPublicManager.configs?.items)! {

            let helpDisplayItem = HelpDisplayData.generateHelpDisplayData(withConfigBO: configBO, isFromSection: false)
            XCTAssert(dummyView.displayDataHelp[index].helpId == helpDisplayItem.helpId)
            index += 1
        }
    }

    // MARK: - helpButtonPressed

    func test_givenHelpId_whenHelpButtonPressed_thenICallBusManagerDetailHelpScreen() {

        //given
        let helpId = "helpId"

        //when
        sut.helpButtonPressed(withHelpId: helpId)

        //then
        XCTAssert(dummyBusManager.isCalledNavigateToDetailHelpScreen == true)

    }

    func test_givenHelpId_whenHelpButtonPressed_thenICallPublishHelpDetailTransportWithHelpId() {

        //given
        let helpId = "helpId"

        //when
        sut.helpButtonPressed(withHelpId: helpId)

        //then
        XCTAssert(helpId == dummyBusManager.dummyHelpDetailTransport.helpId)

    }

    class HelpViewDummy: HelpViewProtocol {

        var isCalledShowHelp = false
        var displayDataHelp = [HelpDisplayData]()

        func showError(error: ModelBO) {
        }

        func changeColorEditTexts() {
        }

        func showHelp(withDisplayDataItems items: [HelpDisplayData]) {
            isCalledShowHelp = true
            displayDataHelp = items
        }

    }

    class DummyBusManager: BusManager {

        var isCalledNavigateToDetailHelpScreen = false
        var dummyHelpDetailTransport: HelpDetailTransport!

        override init() {
            super.init()
            routerFactory = RouterInitFactory()
        }

        override func navigateScreen<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {

            if tag == HelpPageReaction.ROUTER_TAG && event === HelpPageReaction.EVENT_NAV_TO_DETAIL_HELP {
                isCalledNavigateToDetailHelpScreen = true
                dummyHelpDetailTransport = (values as! HelpDetailTransport)
            }
        }
    }

}
