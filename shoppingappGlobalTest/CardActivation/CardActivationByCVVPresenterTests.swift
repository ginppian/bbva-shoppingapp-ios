//
//  CardActivationByCVVPresenterTests.swift
//  shoppingapp
//
//  Created by Marcos on 18/10/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

import CellsNativeComponents

@testable import shoppingappMX

class CardActivationByCVVPresenterTests: XCTestCase {

    var sut: CardActivationByCVVPresenter<CardActivationByCVVViewProtocolDummy>!
    var dummyBusManager: DummyBusManager!
    var dummyView: CardActivationByCVVViewProtocolDummy!
    var dummyCrypto: DummyCryptoDataManager!

    override func setUp() {
        super.setUp()
        dummyBusManager = DummyBusManager()
        dummyView = CardActivationByCVVViewProtocolDummy()
        sut = CardActivationByCVVPresenter<CardActivationByCVVViewProtocolDummy>(busManager: dummyBusManager)
        sut.view = dummyView
        
        dummyCrypto = DummyCryptoDataManager()
        sut.cryptoDataManager = dummyCrypto
    }
    
    // MARK: - setModel
    
    func test_givenErrorBO_whenICallSetModel_thenErroBOShouldMatch() {
        
        //given
        let errorBO = ErrorBO(message: "Error", errorType: .info, allowCancel: false)
        
        //when
        sut.setModel(model: errorBO)
        
        //then
        XCTAssert(sut.errorBO == errorBO)
        
    }

    // MARK: - viewWillDisappear

    func test_givenAny_whenICallViewWillDisappear_thenICalledBusManagerClearData() {

        //given

        //when
        sut.viewWillDisappear()

        //then
        XCTAssert(dummyBusManager.isCalledBusManagerClearData)
    }

    // MARK: - showPageSuccess

    func test_givenAny_whenShowPageSuccess_thenICalledNavigateToActivateSuccessPage() {

        //given

        //when
        sut.showPageSuccess()

        //then
        XCTAssert(dummyBusManager.isCalledNavigateToActivateSuccessPage == true)

    }

    func test_givenAnActivateCardSuccessObjectCells_whenShowPageSuccess_thenICalledPublishParamActivateCardSuccess() {

        //given
        var activateCardSuccessObjectCellsJson = ActivateCardSuccessObjectCells()
        activateCardSuccessObjectCellsJson.cardNumber = "1234"
        sut.activateCardSuccessObjectCells = activateCardSuccessObjectCellsJson

        //when
        sut.showPageSuccess()

        //then
        XCTAssert(dummyBusManager.isCalledPublishParamActivateCardSuccess == true)
        XCTAssert(sut.activateCardSuccessObjectCells?.cardNumber == dummyBusManager.dummyActivateCardSuccessObjectCells.cardNumber)

    }
    
    func test_givenAnActivateCardSuccessObjectCells_whenShowPageSuccess_thenSuccessShouldBeTrue() {
        
        //given
        var activateCardSuccessObjectCellsJson = ActivateCardSuccessObjectCells()
        activateCardSuccessObjectCellsJson.cardNumber = "1234"
        sut.activateCardSuccessObjectCells = activateCardSuccessObjectCellsJson
        
        //when
        sut.showPageSuccess()
        
        //then
        XCTAssert(sut.success == true)
        
    }

    func test_givenAny_whenICallShowPageSuccess_thenINotCallPublishNeededRefreshCards() {

        //given

        //when
        sut.showPageSuccess()

        //then
        XCTAssert(dummyBusManager.isCalledPublishNeededCardsRefresh == false)

    }

    func test_givenCardOperationInfo_whenICallShowPageSuccess_thenICalledPublishParamToCardCardSuccess() {

        //given

        //when
        let cardOperationInfo = CardOperationInfo(cardID: "111", fisicalCardID: nil, cardType: CardDataType.digital, cardOperation: .cancel)
        sut.cardOperationInfo = cardOperationInfo

        //when
        sut.showPageSuccess()

        //then
        XCTAssert(dummyBusManager.isCalledPublishParamActivateCardInfoSuccess == true)

    }

    func test_givenAGenerateCardSuccessObjectCell_whenICallShowPageSuccess_thenCallPublishOperationCardInfoSuccessWithRightData() {

        //given
        let cardOperationInfo = CardOperationInfo(cardID: "111", fisicalCardID: nil, cardType: CardDataType.digital, cardOperation: .cancel)
        sut.cardOperationInfo = cardOperationInfo

        // when
        sut.showPageSuccess()

        //then
        XCTAssert(dummyBusManager.cardOperationInfo?.cardID == cardOperationInfo.cardID)
        XCTAssert(dummyBusManager.cardOperationInfo?.fisicalCardID == cardOperationInfo.fisicalCardID)
        XCTAssert(dummyBusManager.cardOperationInfo?.cardType == cardOperationInfo.cardType)
        XCTAssert(dummyBusManager.cardOperationInfo?.cardOperation == cardOperationInfo.cardOperation)

    }

    // MARK: - setCVV
    func test_givenCardOperationInfo_whenICallSetCVV_thenICallCryptoManagerEncriptDataForHost() {
        
        //given
        let cardOperationInfo = CardOperationInfo(cardID: "111", fisicalCardID: nil, cardType: CardDataType.digital, cardOperation: .cancel)
        sut.cardOperationInfo = cardOperationInfo
        
        //when
        sut.setCVV(cvv: "123")
        
        //then
        XCTAssert(dummyCrypto.isEncryptDataForHost == true)
    }
    
    func test_givenCardOperationInfo_whenICallSetCVV_thenCallViewActivateCardWithCVV() {

        //given
        let cardOperationInfo = CardOperationInfo(cardID: "111", fisicalCardID: nil, cardType: CardDataType.digital, cardOperation: .cancel)
        sut.cardOperationInfo = cardOperationInfo

        //when
        sut.setCVV(cvv: "123")

        //then
        XCTAssert(dummyView.isCalledActivateCardWithCVV == true)
    }

    func test_givenCardOperationInfo_whenICallSetCVV_thenCallViewActivateCardWithCVVwithRightValues() {

        //given
        let cardOperationInfo = CardOperationInfo(cardID: "111", fisicalCardID: nil, cardType: CardDataType.digital, cardOperation: .cancel)
        sut.cardOperationInfo = cardOperationInfo
        
        //when
        sut.setCVV(cvv: "123")

        //then
        XCTAssert(dummyView.cardId == cardOperationInfo.cardID)
        XCTAssert(dummyView.cvv == dummyCrypto.hostDataEntity!.data)
        XCTAssert(dummyView.cvvId == Settings.CardCvv.cvvId)
    }

    // MARK: - viewWasDissmised

    func test_givenSuccessOk_whenICallViewWasDismiss_thenCallPublishNeededRefreshCards() {

        //given
        sut.success = true

        //when
        sut.viewWasDismissed()

        //then
        XCTAssert(dummyBusManager.isCalledPublishNeededCardsRefresh == true)

    }
    
    // MARK: Dummy data

    class CardActivationByCVVViewProtocolDummy: CardActivationByCVVViewProtocol {

        var isCalledActivateCardWithCVV = false
        var cardId: String = ""
        var cvv: String = ""
        var cvvId: String = ""

        func activateCardWithCVV(id: String, cardId: String, cvv: String) {

            cvvId = id
            isCalledActivateCardWithCVV = true
            self.cardId = cardId
            self.cvv = cvv
        }

        func viewWillDisappear() {
        }

        func showError(error: ModelBO) {
        }

        func changeColorEditTexts() {
        }

    }

    class DummyBusManager: BusManager {

        var isCalledNavigateToActivateSuccessPage = false
        var isCalledPublishParamActivateCardSuccess = false
        var isCalledPublishNeededCardsRefresh = false
        var isCalledPublishParamActivateCardInfoSuccess = false
        var isCalledBusManagerClearData = false

        var dummyActivateCardSuccessObjectCells: ActivateCardSuccessObjectCells!
        var cardOperationInfo: CardOperationInfo?

        override init() {
            super.init()
            routerFactory = RouterInitFactory()
        }

        override func navigateScreen<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {

            if tag == CardActivationByCVVPageReaction.routerTag && event === CardActivationByCVVPageReaction.eventCardActivatedSuccess {
                isCalledNavigateToActivateSuccessPage = true
            }
        }

        override func publishData<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {

            if tag == CardActivationByCVVPageReaction.routerTag && event === CardActivationByCVVPageReaction.paramsActivateCardSuccess {
                isCalledPublishParamActivateCardSuccess = true
                dummyActivateCardSuccessObjectCells = ActivateCardSuccessObjectCells.deserialize(from: values as? [String: Any])
            } else if tag == CardActivationByCVVPageReaction.routerTag && event === CardsPageReaction.PARAMS_OPERATION_CARD_INFO_SUCCESS {
                isCalledPublishParamActivateCardInfoSuccess = true
                cardOperationInfo = values as? CardOperationInfo
            }
        }

        override func notify<T>(tag: String, _ event: ActionSpec<T>) {
            if tag == CardsPageReaction.ROUTER_TAG && event === CardsPageReaction.NEEDED_CARDS_REFRESH {
                isCalledPublishNeededCardsRefresh = true
            }
        }

        override func clearData<T>(tag: String, _ event: ActionSpec<T>) {
            isCalledBusManagerClearData = true
        }
    }

}
