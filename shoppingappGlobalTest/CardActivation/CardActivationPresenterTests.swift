//
//  CardActivationPresenterTests.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 18/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest
import CellsNativeComponents

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class CardActivationPresenterTests: XCTestCase {

    var sut: CardActivationPresenter<CardActivationViewProtocolDummy>!
    var dummyBusManager: DummyBusManager!

    override func setUp() {
        super.setUp()
        dummyBusManager = DummyBusManager()
        sut = CardActivationPresenter<CardActivationViewProtocolDummy>(busManager: dummyBusManager)
    }

    func test_whenShowPageSuccess_thenICalledNavigateToActivateSuccessPage() {

        //given

        //when
        sut.showPageSuccess()

        //then
        XCTAssert(dummyBusManager.isCalledNavigateToActivateSuccessPage == true)

    }

    func test_givenAnActivateCardSuccessObjectCells_whenShowPageSuccess_thenICalledPublishParamActivateCardSuccess() {

        //given
        var activateCardSuccessObjectCellsJson = ActivateCardSuccessObjectCells()
        activateCardSuccessObjectCellsJson.cardNumber = "1234"
        sut.activateCardSuccessObjectCells = activateCardSuccessObjectCellsJson

        //when
        sut.showPageSuccess()

        //then
        XCTAssert(dummyBusManager.isCalledPublishParamActivateCardSuccess == true)
        XCTAssert(sut.activateCardSuccessObjectCells?.cardNumber == dummyBusManager.dummyActivateCardSuccessObjectCells.cardNumber)

    }

    func test_givenAny_whenICallShowPageSuccess_thenINotCallPublishNeededRefreshCards() {

        //given
        var activateCardSuccessObjectCellsJson = ActivateCardSuccessObjectCells()
        activateCardSuccessObjectCellsJson.cardNumber = "1234"
        sut.activateCardSuccessObjectCells = activateCardSuccessObjectCellsJson

        //when
        sut.showPageSuccess()

        //then
        XCTAssert(dummyBusManager.isCalledPublishNeededCardsRefresh == false)

    }

    func test_givenAny_whenICallShowPageSuccess_thenICalledPublishParamToBlockCardSuccess() {

        //given
        var activateCardSuccessObjectCellsJson = ActivateCardSuccessObjectCells()
        activateCardSuccessObjectCellsJson.cardNumber = "1234"
        sut.activateCardSuccessObjectCells = activateCardSuccessObjectCellsJson

        //when
        let cardOperationInfo = CardOperationInfo(cardID: "111", fisicalCardID: nil, cardType: CardDataType.digital, cardOperation: .cancel)
        sut.cardOperationInfo = cardOperationInfo

        //when
        sut.showPageSuccess()

        //then
        XCTAssert(dummyBusManager.isCalledPublishParamBlockCardInfoSuccess == true)

    }

    func test_givenAGenerateCardSuccessObjectCell_whenICallShowPageSuccess_thenCallPublishOperationCardInfoSuccessWithRightData() {

        //given
        var activateCardSuccessObjectCellsJson = ActivateCardSuccessObjectCells()
        activateCardSuccessObjectCellsJson.cardNumber = "1234"
        sut.activateCardSuccessObjectCells = activateCardSuccessObjectCellsJson

        let cardOperationInfo = CardOperationInfo(cardID: "111", fisicalCardID: nil, cardType: CardDataType.digital, cardOperation: .cancel)
        sut.cardOperationInfo = cardOperationInfo

        // when
        sut.showPageSuccess()

        //then
        XCTAssert(dummyBusManager.cardOperationInfo?.cardID == cardOperationInfo.cardID)
        XCTAssert(dummyBusManager.cardOperationInfo?.fisicalCardID == cardOperationInfo.fisicalCardID)
        XCTAssert(dummyBusManager.cardOperationInfo?.cardType == cardOperationInfo.cardType)
        XCTAssert(dummyBusManager.cardOperationInfo?.cardOperation == cardOperationInfo.cardOperation)

    }

    // MARK: - viewWasDissmised

    func test_givenSuccessOk_whenICallViewWasDismiss_thenCallPublishNeededRefreshCards() {

        //given
        var activateCardSuccessObjectCellsJson = ActivateCardSuccessObjectCells()
        activateCardSuccessObjectCellsJson.cardNumber = "1234"
        sut.activateCardSuccessObjectCells = activateCardSuccessObjectCellsJson

        sut.showPageSuccess()

        //when
        sut.viewWasDismissed()

        //then
        XCTAssert(dummyBusManager.isCalledPublishNeededCardsRefresh == true)

    }

    class CardActivationViewProtocolDummy: CardActivationViewProtocol {

        func showError(error: ModelBO) {
        }

        func changeColorEditTexts() {
        }

    }

    class DummyBusManager: BusManager {

        var isCalledNavigateToActivateSuccessPage = false
        var isCalledPublishParamActivateCardSuccess = false
        var isCalledPublishNeededCardsRefresh = false
        var isCalledPublishParamBlockCardInfoSuccess = false

        var dummyActivateCardSuccessObjectCells: ActivateCardSuccessObjectCells!
        var cardOperationInfo: CardOperationInfo?

        override init() {
            super.init()
            routerFactory = RouterInitFactory()
        }

        override func navigateScreen<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {

            if tag == CardActivationPageReaction.ROUTER_TAG && event === CardActivationPageReaction.EVENT_CARD_ACTIVATED_SUCCESS {
                isCalledNavigateToActivateSuccessPage = true
            }
        }

        override func publishData<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {

            if tag == CardActivationPageReaction.ROUTER_TAG && event === CardActivationPageReaction.PARAMS_ACTIVATE_CARD_SUCCESS {
                isCalledPublishParamActivateCardSuccess = true
                dummyActivateCardSuccessObjectCells = ActivateCardSuccessObjectCells.deserialize(from: values as? [String: Any])
            } else if tag == CardActivationPageReaction.ROUTER_TAG && event === CardsPageReaction.PARAMS_OPERATION_CARD_INFO_SUCCESS {
                isCalledPublishParamBlockCardInfoSuccess = true
                cardOperationInfo = values as? CardOperationInfo
            }
        }

        override func notify<T>(tag: String, _ event: ActionSpec<T>) {
            if tag == CardsPageReaction.ROUTER_TAG && event === CardsPageReaction.NEEDED_CARDS_REFRESH {
                isCalledPublishNeededCardsRefresh = true
            }
        }
    }

}
