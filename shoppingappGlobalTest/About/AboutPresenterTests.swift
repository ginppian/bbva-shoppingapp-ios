//
//  AboutPresenterTests.swift
//  shoppingapp
//
//  Created by jesus.martinez on 3/12/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest
import CellsNativeComponents

#if TESTMX
@testable import shoppingappMX
#elseif TESTPE
@testable import shoppingappPE
#endif

class AboutPresenterTests: XCTestCase {
    
    var sut: AboutPresenter<AboutViewDummy>!
    var dummyBusManager: DummyBusManager!
    
    override func setUp() {
        
        super.setUp()

        dummyBusManager = DummyBusManager()
        sut = AboutPresenter<AboutViewDummy>(busManager: dummyBusManager)
    }
    
    // MARK: - contractPressed
    
    func test_givenAPublicConfiguration_whenICallContractPressed_thenICallBusManagerNavigateToWeb() {
        
        // given
        sut.publicConfiguration = DummyPublicConfigurationDTO()
        
        // when
        sut.contractPressed()
        
        // then
        XCTAssert(dummyBusManager.isCalledNavigateToWeb == true)
    }
    
    func test_givenAPublicConfiguration_whenICallContractPressed_thenICallBusManagerNavigateToWebWithApropiatedValue() {
        
        // given
        sut.publicConfiguration = DummyPublicConfigurationDTO()
        
        // when
        sut.contractPressed()
        
        // then
        let expectedValue = WebTransport(webPage: sut.publicConfiguration.publicConfig?.terms?.cud, webTitle: Localizables.menu_lateral.key_menu_about_cud_text)
        XCTAssert(dummyBusManager.transport == expectedValue)
    }
    
    // MARK: - legalAdvicePressed
    
    func test_givenAPublicConfiguration_whenICallLegalAdvicePressed_thenICallBusManagerNavigateToWeb() {
        
        // given
        sut.publicConfiguration = DummyPublicConfigurationDTO()

        // when
        sut.legalAdvicePressed()
        
        // then
        XCTAssert(dummyBusManager.isCalledNavigateToWeb == true)
    }
    
    func test_givenAPublicConfiguration_whenICallLegalAdvicePressed_thenICallBusManagerNavigateToWebWithApropiatedValue() {
        
        // given
        sut.publicConfiguration = DummyPublicConfigurationDTO()
        
        // when
        sut.legalAdvicePressed()
        
        // then
        let expectedValue = WebTransport(webPage: sut.publicConfiguration.publicConfig?.terms?.privacy, webTitle: Localizables.menu_lateral.key_menu_about_privacy_text)
        XCTAssert(dummyBusManager.transport == expectedValue)
    }

    class AboutViewDummy: AboutViewProtocol {
        
        func shareContent() {
        }
        
        func showError(error: ModelBO) {
        }
        
        func changeColorEditTexts() {
        }
    }
    
    class DummyBusManager: BusManager {
        
        var isCalledNavigateToWeb = false
        var transport: WebTransport?
        
        override init() {
            
            super.init()
            routerFactory = RouterInitFactory()
        }
        
        override func navigateScreen<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {
            
            if tag == AboutPageReaction.routerTag && event === AboutPageReaction.eventNavigateToWeb {
                isCalledNavigateToWeb = true
                transport = (values as? WebTransport)
            }
        }
    }
    
    class DummyPublicConfigurationDTO: PublicConfigurationDTO {
        
        required init() {

            super.init()
            publicConfig?.terms = TermsData(cud: "https://portal.bancomer.com/fbin/repositorio/CONTRATO_UNICO_DIGITAL.pdf", tyc: "", privacy: "https://bbva-files.s3.amazonaws.com/wallet/shopping_app/terms/AvisoPrivacidadBBVABancomer_v2julio2017.pdf")
        }
    }
}
