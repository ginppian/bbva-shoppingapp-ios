//
//  PromotionsBOTests.swift
//  shoppingapp
//
//  Created by jesus.martinez on 13/3/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class PromotionsBOTests: XCTestCase {

    // MARK: - Promotions

    // MARK: - groupByLatitudeLongitudeStoreAndPromotions

    func test_givenPromotionsWithStores_whenICallGroupByLatitudeLongitudeStoreAndPromotions_thenDataPoisShouldMatch() {

        // given
        let sut = PromotionsGenerator.getPromotionsBOWithStores()

        // when
        let dataPois = sut.groupByLatitudeLongitudeStoreAndPromotions()

        // then
        XCTAssert(dataPois.count == 18)

        XCTAssert(dataPois[0].storeIds.count == 1)
        XCTAssert(dataPois[0].storeIds.contains("1252360"))
        XCTAssert(dataPois[0].latitude == 19.43364)
        XCTAssert(dataPois[0].longitude == -99.137450000000001)
        XCTAssert(dataPois[0].promotions.count == 2)

        XCTAssert(dataPois[2].storeIds.count == 3)
        XCTAssert(dataPois[2].storeIds.contains("6903652"))
        XCTAssert(dataPois[2].storeIds.contains("6987036"))
        XCTAssert(dataPois[2].storeIds.contains("5019740"))
        XCTAssert(dataPois[2].latitude == 19.428170000000001)
        XCTAssert(dataPois[2].longitude == -99.133960000000002)
        XCTAssert(dataPois[2].promotions.count == 2)

        XCTAssert(dataPois[8].storeIds.count == 2)
        XCTAssert(dataPois[8].storeIds.contains("6904478"))
        XCTAssert(dataPois[8].storeIds.contains("6975403"))
        XCTAssert(dataPois[8].latitude == 19.421279999999999)
        XCTAssert(dataPois[8].longitude == -99.140240000000006)
        XCTAssert(dataPois[8].promotions.count == 1)

        XCTAssert(dataPois[17].storeIds.count == 6)
        XCTAssert(dataPois[17].storeIds.contains("4021012"))
        XCTAssert(dataPois[17].storeIds.contains("4020533"))
        XCTAssert(dataPois[17].storeIds.contains("4027152"))
        XCTAssert(dataPois[17].storeIds.contains("4021007"))
        XCTAssert(dataPois[17].storeIds.contains("4020532"))
        XCTAssert(dataPois[17].storeIds.contains("4020531"))
        XCTAssert(dataPois[17].latitude == 19.43488)
        XCTAssert(dataPois[17].longitude == -99.145560000000003)
        XCTAssert(dataPois[17].promotions.count == 1)

    }

    // MARK: - PromotionType

    // MARK: - toService

    func test_givenDiscount_whenICallToService_thenShouldReturnTheValidValue() {

        // given
        let sut = PromotionType.discount

        // when
        let value = sut.toService

        // then
        XCTAssert(value == PromotionType.RATE_FOR_SERVICE)

    }

    func test_givenPoint_whenICallToService_thenShouldReturnTheValidValue() {

        // given
        let sut = PromotionType.point

        // when
        let value = sut.toService

        // then
        XCTAssert(value == PromotionType.FIXED_FOR_SERVICE)

    }

    func test_givenToMonths_whenICallToService_thenShouldReturnTheValidValue() {

        // given
        let sut = PromotionType.toMonths

        // when
        let value = sut.toService

        // then
        XCTAssert(value == PromotionType.GIFT_FOR_SERVICE)

    }

    func test_givenUnknowm_whenICallToService_thenShouldReturnTheValidValue() {

        // given
        let sut = PromotionType.unknown

        // when
        let value = sut.toService

        // then
        XCTAssert(value.isEmpty)

    }

    // MARK: - literal

    func test_givenDiscount_whenICallLiteral_thenShouldReturnTheValidValue() {

        // given
        let sut = PromotionType.discount

        // when
        let value = sut.literal

        // then
        XCTAssert(value == Localizables.promotions.key_discount_text)

    }

    func test_givenPoint_whenICallLiteral_thenShouldReturnTheValidValue() {

        // given
        let sut = PromotionType.point

        // when
        let value = sut.literal

        // then
        XCTAssert(value == Localizables.promotions.key_promotions_points_text)

    }

    func test_givenToMonths_whenICallLiteral_thenShouldReturnTheValidValue() {

        // given
        let sut = PromotionType.toMonths

        // when
        let value = sut.literal

        // then
        XCTAssert(value == Localizables.promotions.key_months_without_interest_text)

    }

    func test_givenUnknowm_whenICallLiteral_thenShouldReturnTheValidValue() {

        // given
        let sut = PromotionType.unknown

        // when
        let value = sut.literal

        // then
        XCTAssert(value.isEmpty)

    }

    // MARK: - PromotionUsageType

    // MARK: - literal

    func test_givenPhysical_whenICallLiteral_thenShouldReturnTheValidValue() {

        // given
        let sut = PromotionUsageType.physical

        // when
        let value = sut.literal

        // then
        XCTAssert(value == Localizables.promotions.key_physical_commerce_text)

    }

    func test_givenOnline_whenICallLiteral_thenShouldReturnTheValidValue() {

        // given
        let sut = PromotionUsageType.online

        // when
        let value = sut.literal

        // then
        XCTAssert(value == Localizables.promotions.key_online_commerce_text)

    }

    func test_givenUnknowmUsage_whenICallLiteral_thenShouldReturnTheValidValue() {

        // given
        let sut = PromotionUsageType.unknown

        // when
        let value = sut.literal

        // then
        XCTAssert(value.isEmpty)

    }

}
