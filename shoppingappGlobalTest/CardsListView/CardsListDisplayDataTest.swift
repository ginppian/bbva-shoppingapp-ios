//
//  CardsListDisplayDataTest.swift
//  shoppingapp
//
//  Created by Marcos on 14/3/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class CardsListDisplayDataTest: XCTestCase {

    // MARK: vars
    var dummySessionManager: DummySessionManager!
    
    // MARK: setup
    override func setUp() {
        super.setUp()
        
        DummySessionManager.setupMock()
        dummySessionManager = (SessionDataManager.sessionDataInstance() as! DummySessionManager)
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func test_givenCardListBO_whenICallInit_thenCardsDisplayDataCardsShouldBeCorrect() {

        //given
        let cards = CardsGenerator.getCardBOWithAlias()

        //when
        let displayData = CardsListDisplayData(cardsBO: cards, mainCurrencyCode: dummySessionManager.mainCurrency, cardTypesToShow: [.normalPlastic], showPoints: false)

        //then
        XCTAssert(displayData.cardsDisplayData.count == cards.cards.count)

    }

    // MARK: Title

    func test_givenCardBOWithAlias_whenICallInit_thenCardsDisplayDataCardsTitleShouldBeAlias() {

        //given
        let cards = CardsGenerator.getCardBOWithAlias()

        //when
        let displayData = CardsListDisplayData(cardsBO: cards, mainCurrencyCode: dummySessionManager.mainCurrency, cardTypesToShow: [.normalPlastic], showPoints: false)

        //then
        XCTAssert(displayData.cardsDisplayData.first?.title == cards.cards.first?.alias)

    }

    func test_givenCardWBOWithoutAlias_whenICallInit_thenCardsDisplayDataCardsTitleShouldBeTitleName() {

        //given
        let cards = CardsGenerator.getCardBOWithAlias()
        cards.cards.first?.alias = nil

        //when
        let displayData = CardsListDisplayData(cardsBO: cards, mainCurrencyCode: dummySessionManager.mainCurrency, cardTypesToShow: [.normalPlastic], showPoints: false)

        //then
        XCTAssert(displayData.cardsDisplayData.first?.title == cards.cards.first?.title?.name)

    }

    // MARK: State message and image

    func test_givenCardWBOWithUnknowState_whenICallInit_thenCardsDisplayDataCardsStateShouldBeBloqued() {

        //given
        let cards = CardsGenerator.getCardBOWithAlias()
        cards.cards.first?.status.id = .canceled //Any different operative, inoperative, and bloqued

        //when
        let displayData = CardsListDisplayData(cardsBO: cards, mainCurrencyCode: dummySessionManager.mainCurrency, cardTypesToShow: [.normalPlastic], showPoints: false)

        //then
        XCTAssert(displayData.cardsDisplayData.first?.state == Localizables.cards.key_card_status_blocked_text)
        XCTAssert(displayData.cardsDisplayData.first?.stateImage == ConstantsImages.Card.card_bloqued_state)
        XCTAssert(displayData.cardsDisplayData.first?.stateLabelColor == .BBVACORAL)

    }

    func test_givenCardWBOWithActiveState_whenICallInit_thenCardsDisplayDataCardsStateShouldBeNil() {

        //given
        let cards = CardsGenerator.getCardBOWithAlias()
        cards.cards.first?.status.id = .operative

        //when
        let displayData = CardsListDisplayData(cardsBO: cards, mainCurrencyCode: dummySessionManager.mainCurrency, cardTypesToShow: [.normalPlastic], showPoints: false)

        //then
        XCTAssert(displayData.cardsDisplayData.first?.state == nil)
        XCTAssert(displayData.cardsDisplayData.first?.stateImage == nil)

    }

    func test_givenCardWBOWithBlokedState_whenICallInit_thenCardsDisplayDataCardsStateShouldBeBloqued() {

        //given
        let cards = CardsGenerator.getCardBOWithAlias()
        cards.cards.first?.status.id = .blocked

        //when
        let displayData = CardsListDisplayData(cardsBO: cards, mainCurrencyCode: dummySessionManager.mainCurrency, cardTypesToShow: [.normalPlastic], showPoints: false)

        //then
        XCTAssert(displayData.cardsDisplayData.first?.state == Localizables.cards.key_card_status_blocked_text)
        XCTAssert(displayData.cardsDisplayData.first?.stateImage == ConstantsImages.Card.card_bloqued_state)
        XCTAssert(displayData.cardsDisplayData.first?.stateLabelColor == .BBVACORAL)

    }

    func test_givenCardWBOWithInoperativeState_whenICallInit_thenCardsDisplayDataCardsStateShouldBePending() {

        //given
        let cards = CardsGenerator.getCardBOWithAlias()
        cards.cards.first?.status.id = .inoperative

        //when
        let displayData = CardsListDisplayData(cardsBO: cards, mainCurrencyCode: dummySessionManager.mainCurrency, cardTypesToShow: [.normalPlastic], showPoints: false)

        //then
        XCTAssert(displayData.cardsDisplayData.first?.state == Localizables.cards.key_card_status_pending_activation_text)
        XCTAssert(displayData.cardsDisplayData.first?.stateImage == ConstantsImages.Card.card_inoperative_state)
        XCTAssert(displayData.cardsDisplayData.first?.stateLabelColor == .NAVY)

    }

    func test_givenCardWBOWithOperativeStateAndActivationsOnOffActiveFalse_whenICallInit_thenCardsDisplayDataCardsStateShouldBeOff() {

        //given
        let cards = CardsGenerator.getCardBOWithAlias()
        cards.cards.first?.status.id = .operative
        for activation in cards.cards.first!.activations where activation.activationId == .onoff {
            activation.isActive = false
        }

        //when
        let displayData = CardsListDisplayData(cardsBO: cards, mainCurrencyCode: dummySessionManager.mainCurrency, cardTypesToShow: [.normalPlastic], showPoints: false)

        //then
        XCTAssert(displayData.cardsDisplayData.first?.state == Localizables.cards.key_card_status_off_text)
        XCTAssert(displayData.cardsDisplayData.first?.stateImage == ConstantsImages.Card.card_off_state)
        XCTAssert(displayData.cardsDisplayData.first?.stateLabelColor == .BBVA500)

    }

    func test_givenCardWBOOperativeWithECOMMERCE_ACTIVATIONStateFalse_whenICallInit_thenCardsDisplayDataCardsStateShouldBePartialOfft() {

        //given
        let cards = CardsGenerator.getCardBOWithAlias()
        cards.cards.first?.status.id = .operative
        cards.cards.first!.activations.append(ActivationBO(activationEntity: ActivationEntity(activationId: "ECOMMERCE_ACTIVATION",
                                                                                               name: "Any",
                                                                                               isActive: false,
                                                                                               isActivationEnabledForUser: true,
                                                                                               startDate: "Any", endDate: "Any",
                                                                                               limits: nil)))
        for activation in cards.cards.first!.activations  where  activation.activationId == .ecommerceActivation {
                activation.isActive = false
        }

        //when
        let displayData = CardsListDisplayData(cardsBO: cards, mainCurrencyCode: dummySessionManager.mainCurrency, cardTypesToShow: [.normalPlastic], showPoints: false)

        //then
        XCTAssert(displayData.cardsDisplayData.first?.state == Localizables.cards.key_card_status_partial_off_text)
        XCTAssert(displayData.cardsDisplayData.first?.stateImage == ConstantsImages.Card.card_off_state)
        XCTAssert(displayData.cardsDisplayData.first?.stateLabelColor == .BBVA500)

    }

    func test_givenCardWBOOperativeWithCASHWITHDRAWAL_ACTIVATIONStateFalse_whenICallInit_thenCardsDisplayDataCardsStateShouldBePartialOfft() {

        //given
        let cards = CardsGenerator.getCardBOWithAlias()
        cards.cards.first?.status.id = .operative
        cards.cards.first!.activations.append(ActivationBO(activationEntity: ActivationEntity(activationId: "CASHWITHDRAWAL_ACTIVATION",
                                                                                               name: "Any",
                                                                                               isActive: false,
                                                                                               isActivationEnabledForUser: true,
                                                                                               startDate: "Any", endDate: "Any",
                                                                                               limits: nil)))
        for activation in cards.cards.first!.activations where  activation.activationId == .cashwithdrawalActivation {
                activation.isActive = false
        }

        //when
        let displayData = CardsListDisplayData(cardsBO: cards, mainCurrencyCode: dummySessionManager.mainCurrency, cardTypesToShow: [.normalPlastic], showPoints: false)

        //then
        XCTAssert(displayData.cardsDisplayData.first?.state == Localizables.cards.key_card_status_partial_off_text)
        XCTAssert(displayData.cardsDisplayData.first?.stateImage == ConstantsImages.Card.card_off_state)
        XCTAssert(displayData.cardsDisplayData.first?.stateLabelColor == .BBVA500)

    }

    func test_givenCardWBOOperativeWithPURCHASES_ACTIVATIONStateFalse_whenICallInit_thenCardsDisplayDataCardsStateShouldBePartialOfft() {

        //given
        let cards = CardsGenerator.getCardBOWithAlias()
        cards.cards.first?.status.id = .operative
        cards.cards.first!.activations.append(ActivationBO(activationEntity: ActivationEntity(activationId: "PURCHASES_ACTIVATION",
                                                                                               name: "Any",
                                                                                               isActive: false,
                                                                                               isActivationEnabledForUser: true,
                                                                                               startDate: "Any", endDate: "Any",
                                                                                               limits: nil)))
        for activation in cards.cards.first!.activations  where  activation.activationId == .purchasesActivation {
            activation.isActive = false
        }

        //when
        let displayData = CardsListDisplayData(cardsBO: cards, mainCurrencyCode: dummySessionManager.mainCurrency, cardTypesToShow: [.normalPlastic], showPoints: false)

        //then
        XCTAssert(displayData.cardsDisplayData.first?.state == Localizables.cards.key_card_status_partial_off_text)
        XCTAssert(displayData.cardsDisplayData.first?.stateImage == ConstantsImages.Card.card_off_state)
        XCTAssert(displayData.cardsDisplayData.first?.stateLabelColor == .BBVA500)

    }

    func test_givenCardWBOOperativeWithFOREIGN_CASHWITHDRAWAL_ACTIVATIONStateFalse_whenICallInit_thenCardsDisplayDataCardsStateShouldBePartialOfft() {

        //given
        let cards = CardsGenerator.getCardBOWithAlias()
        cards.cards.first?.status.id = .operative
        cards.cards.first!.activations.append(ActivationBO(activationEntity: ActivationEntity(activationId: "FOREIGN_CASHWITHDRAWAL_ACTIVATION",
                                                                                               name: "Any",
                                                                                               isActive: false,
                                                                                               isActivationEnabledForUser: true,
                                                                                               startDate: "Any", endDate: "Any",
                                                                                               limits: nil)))
        for activation in cards.cards.first!.activations  where  activation.activationId == .foreignCashWithDrawalActivation {
            activation.isActive = false
        }

        //when
        let displayData = CardsListDisplayData(cardsBO: cards, mainCurrencyCode: dummySessionManager.mainCurrency, cardTypesToShow: [.normalPlastic], showPoints: false)

        //then
        XCTAssert(displayData.cardsDisplayData.first?.state == Localizables.cards.key_card_status_partial_off_text)
        XCTAssert(displayData.cardsDisplayData.first?.stateImage == ConstantsImages.Card.card_off_state)
        XCTAssert(displayData.cardsDisplayData.first?.stateLabelColor == .BBVA500)

    }

    func test_givenCardWBOOperativeWithFOREIGN_PURCHASES_ACTIVATIONStateFalse_whenICallInit_thenCardsDisplayDataCardsStateShouldBePartialOfft() {

        //given
        let cards = CardsGenerator.getCardBOWithAlias()
        cards.cards.first?.status.id = .operative
        cards.cards.first!.activations.append(ActivationBO(activationEntity: ActivationEntity(activationId: "FOREIGN_PURCHASES_ACTIVATION",
                                                                                               name: "Any",
                                                                                               isActive: false,
                                                                                               isActivationEnabledForUser: true,
                                                                                               startDate: "Any", endDate: "Any",
                                                                                               limits: nil)))
        for activation in cards.cards.first!.activations  where  activation.activationId == .foreignPurchasesActivation {
            activation.isActive = false
        }

        //when
        let displayData = CardsListDisplayData(cardsBO: cards, mainCurrencyCode: dummySessionManager.mainCurrency, cardTypesToShow: [.normalPlastic], showPoints: false)

        //then
        XCTAssert(displayData.cardsDisplayData.first?.state == Localizables.cards.key_card_status_partial_off_text)
        XCTAssert(displayData.cardsDisplayData.first?.stateImage == ConstantsImages.Card.card_off_state)
        XCTAssert(displayData.cardsDisplayData.first?.stateLabelColor == .BBVA500)

    }

    // MARK: Image

    func test_givenCardWBOWithFromImageUrl_whenICallInit_thenCardsDisplayDataCardsImageShouldBeFill() {

        //given
        let cards = CardsGenerator.getCardBOWithAlias()

        //when
        let displayData = CardsListDisplayData(cardsBO: cards, mainCurrencyCode: dummySessionManager.mainCurrency, cardTypesToShow: [.normalPlastic], showPoints: false)

        //then
        XCTAssert(displayData.cardsDisplayData.first?.image != nil)

    }

    // MARK: Pan 4

    func test_givenCardWBO_whenICallInit_thenCardsDisplayDataCardsPan4ShouldBeCorrect() {

        //given
        let cards = CardsGenerator.getCardBOWithAlias()

        //when
        let displayData = CardsListDisplayData(cardsBO: cards, mainCurrencyCode: dummySessionManager.mainCurrency, cardTypesToShow: [.normalPlastic], showPoints: false)

        //then
        XCTAssert(displayData.cardsDisplayData.first?.pan4 == String(cards.cards.first!.number.suffix(4)))

    }

    // MARK: Puntos Bamcomer

    func test_givenCardWBO_whenICallInit_thenCardsDisplayDataCardsPointsShouldBeCorrect() {

        //given
        let cards = CardsGenerator.getCardBOWithAlias()
        let points = BigCardDisplayData.getPoints(cardBO: cards.cards.first!)

        //when
        let displayData = CardsListDisplayData(cardsBO: cards, mainCurrencyCode: dummySessionManager.mainCurrency, cardTypesToShow: [.normalPlastic], showPoints: false)

        //then
        XCTAssert(displayData.cardsDisplayData.first?.points == points)

    }

    // MARK: Amount

    func test_givenCreditCardWBO_whenICallInit_thenCardsDisplayDataCardsAmountShouldBeCorrect() {

        //given
        let cards = CardsGenerator.getCardBOWithAlias()
        cards.cards.first?.cardType.id = .credit_card
        let currencyBalanceToShow = cards.cards.first!.currentBalanceToShow(mainCurrencyCode: dummySessionManager.mainCurrency)
        let formatter = (cards.cards.first!.currencyMajor != nil) ? AmountFormatter(codeCurrency: cards.cards.first!.currencyMajor!) : AmountFormatter(codeCurrency: dummySessionManager.mainCurrency)
        let amount = formatter.attributtedText(forAmount: currencyBalanceToShow!.amount, isNegative: true, bigFontSize: 30, smallFontSize: 20)

        //when
        let displayData = CardsListDisplayData(cardsBO: cards, mainCurrencyCode: dummySessionManager.mainCurrency, cardTypesToShow: [.normalPlastic], showPoints: false)

        //then
        XCTAssert(displayData.cardsDisplayData.first?.amount.string == amount.string)

    }
    
    func test_givenDebitCardBO_whenICallInit_thenCardsDisplayDataCardsAmountShouldBeCorrect() {
        //given
        let cards = CardsGenerator.getCardBOWithAlias()
        let card = cards.cards.first!
        card.cardType.id = .debit_card
        let currencyBalanceToShow = card.currentBalanceToShow(mainCurrencyCode: dummySessionManager.mainCurrency)
        let formatter = (card.currencyMajor != nil) ? AmountFormatter(codeCurrency: card.currencyMajor!) : AmountFormatter(codeCurrency: dummySessionManager.mainCurrency)
        let amount = formatter.attributtedText(forAmount: currencyBalanceToShow!.amount, isNegative: false, bigFontSize: 30, smallFontSize: 20)
        
        //when
        let displayData = CardsListDisplayData(cardsBO: cards, mainCurrencyCode: dummySessionManager.mainCurrency, cardTypesToShow: [.normalPlastic], showPoints: false)
        
        //then
        XCTAssert(displayData.cardsDisplayData.first?.amount.string == amount.string)

    }
    
    // MARK: cardId

    func test_givenCardBO_whenICallInit_thenCardsDisplayHasCardIdThatMatchWithCardBO() {

        // given
        let cards = CardsGenerator.getCardBOWithAlias()

        // when
        let displayData = CardsListDisplayData(cardsBO: cards, mainCurrencyCode: dummySessionManager.mainCurrency, cardTypesToShow: [.normalPlastic], showPoints: false)

        //then
        XCTAssert(displayData.cardsDisplayData.first?.cardId == cards.cards.first?.cardId)

    }

    // MARK: - CardDisplayDataProtocol - getStatus
    
    func test_givenCardBOWithStatusBlockedAndPhysicalSupportNormalPlastic_whenICallGetStatus_thenDisplayDataMatch() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards.first!
        cardBO.status.id = .blocked
        let physicalSupportType = PhysicalSupportType.normalPlastic
        
        // when
        let cardDisplayDataProtocol = BigCardDisplayData.getStatus(cardBO: cardBO, physicalSupportType: physicalSupportType)
        
        // then
        XCTAssert(cardDisplayDataProtocol.state == Localizables.cards.key_card_status_blocked_text)
        XCTAssert(cardDisplayDataProtocol.stateImage == ConstantsImages.Card.card_bloqued_state)
        XCTAssert(cardDisplayDataProtocol.stateLabelColor == .BBVACORAL)
    }
    
    func test_givenCardBOWithStatusBlockedAndPhysicalSupportDifferentToNormalPlastic_whenICallGetStatus_thenDisplayDataMatch() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards.first!
        cardBO.status.id = .blocked
        let physicalSupportType = PhysicalSupportType.sticker
        
        // when
        let cardDisplayDataProtocol = BigCardDisplayData.getStatus(cardBO: cardBO, physicalSupportType: physicalSupportType)
        
        // then
        XCTAssert(cardDisplayDataProtocol.state == Localizables.cards.key_card_status_blocked_text)
        XCTAssert(cardDisplayDataProtocol.stateImage == ConstantsImages.Card.card_bloqued_state_small)
        XCTAssert(cardDisplayDataProtocol.stateLabelColor == .BBVACORAL)
    }
    
    func test_givenCardBOWithStatusCanceledAndPhysicalSupportNormalPlastic_whenICallGetStatus_thenDisplayDataMatch() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards.first!
        cardBO.status.id = .canceled
        let physicalSupportType = PhysicalSupportType.normalPlastic
        
        // when
        let cardDisplayDataProtocol = BigCardDisplayData.getStatus(cardBO: cardBO, physicalSupportType: physicalSupportType)
        
        // then
        XCTAssert(cardDisplayDataProtocol.state == Localizables.cards.key_card_status_blocked_text)
        XCTAssert(cardDisplayDataProtocol.stateImage == ConstantsImages.Card.card_bloqued_state)
        XCTAssert(cardDisplayDataProtocol.stateLabelColor == .BBVACORAL)
    }
    
    func test_givenCardBOWithStatusCanceledAndPhysicalSupportDifferentToNormalPlastic_whenICallGetStatus_thenDisplayDataMatch() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards.first!
        cardBO.status.id = .canceled
        let physicalSupportType = PhysicalSupportType.sticker
        
        // when
        let cardDisplayDataProtocol = BigCardDisplayData.getStatus(cardBO: cardBO, physicalSupportType: physicalSupportType)
        
        // then
        XCTAssert(cardDisplayDataProtocol.state == Localizables.cards.key_card_status_blocked_text)
        XCTAssert(cardDisplayDataProtocol.stateImage == ConstantsImages.Card.card_bloqued_state_small)
        XCTAssert(cardDisplayDataProtocol.stateLabelColor == .BBVACORAL)
    }
    
    func test_givenCardBOWithStatusUnknownAndPhysicalSupportNormalPlastic_whenICallGetStatus_thenDisplayDataMatch() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards.first!
        cardBO.status.id = .unknown
        let physicalSupportType = PhysicalSupportType.normalPlastic
        
        // when
        let cardDisplayDataProtocol = BigCardDisplayData.getStatus(cardBO: cardBO, physicalSupportType: physicalSupportType)
        
        // then
        XCTAssert(cardDisplayDataProtocol.state == Localizables.cards.key_card_status_blocked_text)
        XCTAssert(cardDisplayDataProtocol.stateImage == ConstantsImages.Card.card_bloqued_state)
        XCTAssert(cardDisplayDataProtocol.stateLabelColor == .BBVACORAL)
    }
    
    func test_givenCardBOWithStatusUnknownAndPhysicalSupportDifferentToNormalPlastic_whenICallGetStatus_thenDisplayDataMatch() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards.first!
        cardBO.status.id = .unknown
        let physicalSupportType = PhysicalSupportType.sticker
        
        // when
        let cardDisplayDataProtocol = BigCardDisplayData.getStatus(cardBO: cardBO, physicalSupportType: physicalSupportType)
        
        // then
        XCTAssert(cardDisplayDataProtocol.state == Localizables.cards.key_card_status_blocked_text)
        XCTAssert(cardDisplayDataProtocol.stateImage == ConstantsImages.Card.card_bloqued_state_small)
        XCTAssert(cardDisplayDataProtocol.stateLabelColor == .BBVACORAL)
    }
    
    //-------------
    
    func test_givenCardBOWithStatusPendingEmbossingAndPhysicalSupportNormalPlastic_whenICallGetStatus_thenDisplayDataMatch() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards.first!
        cardBO.status.id = .pendingEmbossing
        let physicalSupportType = PhysicalSupportType.normalPlastic
        
        // when
        let cardDisplayDataProtocol = BigCardDisplayData.getStatus(cardBO: cardBO, physicalSupportType: physicalSupportType)
        
        // then
        XCTAssert(cardDisplayDataProtocol.state == Localizables.cards.key_card_status_delivery_text)
        XCTAssert(cardDisplayDataProtocol.stateImage == ConstantsImages.Card.card_delivery_state)
        XCTAssert(cardDisplayDataProtocol.stateLabelColor == .DARKAQUA)
    }
    
    func test_givenCardBOWithStatusPendingEmbossingAndPhysicalSupportDifferentToNormalPlastic_whenICallGetStatus_thenDisplayDataMatch() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards.first!
        cardBO.status.id = .pendingEmbossing
        let physicalSupportType = PhysicalSupportType.sticker
        
        // when
        let cardDisplayDataProtocol = BigCardDisplayData.getStatus(cardBO: cardBO, physicalSupportType: physicalSupportType)
        
        // then
        XCTAssert(cardDisplayDataProtocol.state == Localizables.cards.key_card_status_delivery_text)
        XCTAssert(cardDisplayDataProtocol.stateImage == ConstantsImages.Card.card_delivery_state_small)
        XCTAssert(cardDisplayDataProtocol.stateLabelColor == .DARKAQUA)
    }
    
    func test_givenCardBOWithStatusPendingDeliveryAndPhysicalSupportNormalPlastic_whenICallGetStatus_thenDisplayDataMatch() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards.first!
        cardBO.status.id = .pendingDelivery
        let physicalSupportType = PhysicalSupportType.normalPlastic
        
        // when
        let cardDisplayDataProtocol = BigCardDisplayData.getStatus(cardBO: cardBO, physicalSupportType: physicalSupportType)
        
        // then
        XCTAssert(cardDisplayDataProtocol.state == Localizables.cards.key_card_status_delivery_text)
        XCTAssert(cardDisplayDataProtocol.stateImage == ConstantsImages.Card.card_delivery_state)
        XCTAssert(cardDisplayDataProtocol.stateLabelColor == .DARKAQUA)
    }
    
    func test_givenCardBOWithStatusPendingDeliveryAndPhysicalSupportDifferentToNormalPlastic_whenICallGetStatus_thenDisplayDataMatch() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards.first!
        cardBO.status.id = .pendingDelivery
        let physicalSupportType = PhysicalSupportType.sticker
        
        // when
        let cardDisplayDataProtocol = BigCardDisplayData.getStatus(cardBO: cardBO, physicalSupportType: physicalSupportType)
        
        // then
        XCTAssert(cardDisplayDataProtocol.state == Localizables.cards.key_card_status_delivery_text)
        XCTAssert(cardDisplayDataProtocol.stateImage == ConstantsImages.Card.card_delivery_state_small)
        XCTAssert(cardDisplayDataProtocol.stateLabelColor == .DARKAQUA)
    }
    
    //-------------
    
    func test_givenCardBOWithStatusInoperativeAndPhysicalSupportNormalPlastic_whenICallGetStatus_thenDisplayDataMatch() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards.first!
        cardBO.status.id = .inoperative
        let physicalSupportType = PhysicalSupportType.normalPlastic
        
        // when
        let cardDisplayDataProtocol = BigCardDisplayData.getStatus(cardBO: cardBO, physicalSupportType: physicalSupportType)
        
        // then
        XCTAssert(cardDisplayDataProtocol.state == Localizables.cards.key_card_status_pending_activation_text)
        XCTAssert(cardDisplayDataProtocol.stateImage == ConstantsImages.Card.card_inoperative_state)
        XCTAssert(cardDisplayDataProtocol.stateLabelColor == .NAVY)
    }
    
    func test_givenCardBOWithStatusInoperativeAndPhysicalSupportDifferentToNormalPlastic_whenICallGetStatus_thenDisplayDataMatch() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards.first!
        cardBO.status.id = .inoperative
        let physicalSupportType = PhysicalSupportType.sticker
        
        // when
        let cardDisplayDataProtocol = BigCardDisplayData.getStatus(cardBO: cardBO, physicalSupportType: physicalSupportType)
        
        // then
        XCTAssert(cardDisplayDataProtocol.state == Localizables.cards.key_card_status_pending_activation_text)
        XCTAssert(cardDisplayDataProtocol.stateImage == ConstantsImages.Card.card_inoperative_state_small)
        XCTAssert(cardDisplayDataProtocol.stateLabelColor == .NAVY)
    }
    
    //-------------
    
    func test_givenCardBOWithStatusOperativeAndIsCardOnFalseAndPhysicalSupportNormalPlastic_whenICallGetStatus_thenDisplayDataMatch() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards.first!
        cardBO.status.id = .operative
        cardBO.activations.first?.activationId = .onoff
        cardBO.activations.first?.isActive = false
        let physicalSupportType = PhysicalSupportType.normalPlastic
        
        // when
        let cardDisplayDataProtocol = BigCardDisplayData.getStatus(cardBO: cardBO, physicalSupportType: physicalSupportType)
        
        // then
        XCTAssert(cardDisplayDataProtocol.state == Localizables.cards.key_card_status_off_text)
        XCTAssert(cardDisplayDataProtocol.stateImage == ConstantsImages.Card.card_off_state)
        XCTAssert(cardDisplayDataProtocol.stateLabelColor == .BBVA500)
    }
    
    func test_givenCardBOWithStatusOperativeAndIsCardOnFalseAndPhysicalSupportDifferentToNormalPlastic_whenICallGetStatus_thenDisplayDataMatch() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards.first!
        cardBO.status.id = .operative
        cardBO.activations.first?.activationId = .onoff
        cardBO.activations.first?.isActive = false
        let physicalSupportType = PhysicalSupportType.sticker
        
        // when
        let cardDisplayDataProtocol = BigCardDisplayData.getStatus(cardBO: cardBO, physicalSupportType: physicalSupportType)
        
        // then
        XCTAssert(cardDisplayDataProtocol.state == Localizables.cards.key_card_status_off_text)
        XCTAssert(cardDisplayDataProtocol.stateImage == ConstantsImages.Card.card_off_state_small)
        XCTAssert(cardDisplayDataProtocol.stateLabelColor == .BBVA500)
    }
    
    //-------------
    
    func test_givenCardBOWithStatusOperativeAndOtherActivationIdFalseAndPhysicalSupportNormalPlastic_whenICallGetStatus_thenDisplayDataMatch() {
        
        // given
        let cardBO = CardsGenerator.getCardBOMultipleActivation(withStatusId: "OPERATIVE", withActivationId: "FOREIGN_PURCHASES_ACTIVATION", withIsActive: "false", withActivationId: "ON_OFF", withIsActive2: "true").cards.first!
        let physicalSupportType = PhysicalSupportType.normalPlastic
        
        // when
        let cardDisplayDataProtocol = BigCardDisplayData.getStatus(cardBO: cardBO, physicalSupportType: physicalSupportType)
        
        // then
        XCTAssert(cardDisplayDataProtocol.state == Localizables.cards.key_card_status_partial_off_text)
        XCTAssert(cardDisplayDataProtocol.stateImage == ConstantsImages.Card.card_off_state)
        XCTAssert(cardDisplayDataProtocol.stateLabelColor == .BBVA500)
    }
    
    func test_givenCardBOWithStatusOperativeAndOtherActivationIdFalseAndPhysicalSupportDifferentToNormalPlastic_whenICallGetStatus_thenDisplayDataMatch() {
        
        // given
        let cardBO = CardsGenerator.getCardBOMultipleActivation(withStatusId: "OPERATIVE", withActivationId: "FOREIGN_PURCHASES_ACTIVATION", withIsActive: "false", withActivationId: "ON_OFF", withIsActive2: "true").cards.first!
        let physicalSupportType = PhysicalSupportType.sticker
        
        // when
        let cardDisplayDataProtocol = BigCardDisplayData.getStatus(cardBO: cardBO, physicalSupportType: physicalSupportType)
        
        // then
        XCTAssert(cardDisplayDataProtocol.state == Localizables.cards.key_card_status_partial_off_text)
        XCTAssert(cardDisplayDataProtocol.stateImage == ConstantsImages.Card.card_off_state_small)
        XCTAssert(cardDisplayDataProtocol.stateLabelColor == .BBVA500)
    }
}
