//
//  CardsListPresenterTest.swift
//  shoppingapp
//
//  Created by Marcos on 14/3/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class CardsListPresenterTest: XCTestCase {

    let sut = CardsListPresenter()
    var dummySessionManager: DummySessionManager!
    
    // MARK: setup
    override func setUp() {
        super.setUp()
        
        DummySessionManager.setupMock()
        dummySessionManager = (SessionDataManager.sessionDataInstance() as! DummySessionManager)
    }
    
    override func tearDown() {
        super.tearDown()
    }

    // MARK: - setModel

    func test_givenCardsBO_whenICallSetModel_thenIGenerateCardListDisplayData() {

        // given
        let cardsBO = CardsGenerator.getCardBOWithAlias()
        let dummyView = CardListViewDummy()
        sut.view = dummyView

        // when
        _ = ConfigurationDataManager.sharedInstance.provideMainCurrency().subscribe(

            onNext: { result in

                if let currencyEntity = result as? CurrencyEntity {

                    self.sut.setModel(model: CardListModel(cardBO: cardsBO, currencyCode: currencyEntity.code, cardTypesToShow: [.normalPlastic], showPoints: false))

                    // then
                    XCTAssert(dummyView.displayDataCardList != nil)
                }
            })

    }

    func test_givenCardsBO_whenICallSetModel_thenICallShowCardListDisplayData() {

        // given
        let cardsBO = CardsGenerator.getCardBOWithAlias()
        let dummyView = CardListViewDummy()
        sut.view = dummyView

        // when
        _ = ConfigurationDataManager.sharedInstance.provideMainCurrency().subscribe(

            onNext: { result in

                if let currencyEntity = result as? CurrencyEntity {
                    self.sut.setModel(model: CardListModel(cardBO: cardsBO, currencyCode: currencyEntity.code, cardTypesToShow: [.normalPlastic], showPoints: false))
                    // then
                    XCTAssert(dummyView.isCalledShowDisplayCards == true)
                }
            })
        // then

    }

    // MARK: - cardPressed

    func test_givenADelegateAndDisplayData_whenICallCardPressed_thenICallDelegateDidSelectCardId() {

        // given
        let dummyDelegate = DummyPresenterDelegate()
        let cards = CardsGenerator.getCardBOWithAlias()
        let displayData = CardsListDisplayData(cardsBO: cards, mainCurrencyCode: dummySessionManager.mainCurrency, cardTypesToShow: [.normalPlastic], showPoints: false).cardsDisplayData.first!
        sut.delegate = dummyDelegate

        // when
        sut.cardPressed(withDisplayData: displayData)

        // then
        XCTAssert(dummyDelegate.isCalledDidSelectCardId == true)

    }

    func test_givenADelegateAndDisplayData_whenICallCardPressed_thenICallDelegateDidSelectCardIdWithTheCardIdOfDisplayData() {

        // given
        let dummyDelegate = DummyPresenterDelegate()
        let cards = CardsGenerator.getCardBOWithAlias()
        let displayData = CardsListDisplayData(cardsBO: cards, mainCurrencyCode: dummySessionManager.mainCurrency, cardTypesToShow: [.normalPlastic], showPoints: false).cardsDisplayData.first!
        sut.delegate = dummyDelegate

        // when
        sut.cardPressed(withDisplayData: displayData)

        // then
        XCTAssert(dummyDelegate.cardIdSent == displayData.cardId)

    }

    // MARK: - Dummy data

    class DummyPresenterDelegate: CardsListPresenterDelegate {

        var isCalledDidSelectCardId = false
        var cardIdSent: String?

        func cardsListPresenter(_ cardsListPresenter: CardsListPresenter, didSelectCardId cardId: String) {
            isCalledDidSelectCardId = true
            cardIdSent = cardId
        }

    }

    class CardListViewDummy: CardsListViewProtocol {

        var isCalledShowDisplayCards = false
        var displayDataCardList: CardsListDisplayData?

        func showError(error: ModelBO) {
        }

        func changeColorEditTexts() {
        }

        func showDisplayCards(displayCards: CardsListDisplayData) {
            isCalledShowDisplayCards = true
            displayDataCardList = displayCards
        }
    }
}
