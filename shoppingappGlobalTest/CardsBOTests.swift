//
//  CardsBOTests.swift
//  shoppingapp
//
//  Created by jesus.martinez on 4/6/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
@testable import shoppingappMX
#elseif TESTPE
@testable import shoppingappPE
#endif

class CardsBOTests: XCTestCase {

    var dummyCardsImagesManager: DummyCardsImagesManager!

    override func setUp() {
        super.setUp()
        dummyCardsImagesManager = DummyCardsImagesManager()
        CardsImagesManager.sharedInstance = dummyCardsImagesManager
    }

    // MARK: - init FinancialOverviewEntity
    
    func test_givenFinancialOverviewEntity_whenICallInitFinancialOverviewEntity_thenTypeParameterIsSetOnlyOnFrontNotNormalPlasticCreditCardURLs() {
        
        // given
        let financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverviewWithDigitalCards()
        
        // when
        let sut = CardsBO(financialOverviewEntity: financialOverviewEntity, user: UserBO(user: UserEntity()))
        
        // then
        let typeParameterExpected = "&type="
        
        for card in sut.cards {
            if card.isCreditCard(), !card.isNormalPlastic() {
                XCTAssert(card.imageFront!.suffix(13).contains(typeParameterExpected))
            } else {
                XCTAssertFalse(card.imageFront!.suffix(13).contains(typeParameterExpected))
            }
        }
    }
    
    func test_givenFinancialOverviewEntity_whenICallInitFinancialOverviewEntity_thenRelatedBinIsInFrontNotNormalPlasticCreditCardURLs() {
        //given
        let financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverviewWithDigitalCards()
        
        // when
        let sut = CardsBO(financialOverviewEntity: financialOverviewEntity, user: UserBO(user: UserEntity()))
        
        // then
        let creditDigitalFrontURL = sut.cards[5].imageFront
        let relatedBinForDebitExpected = sut.cards[4].number.prefix(6)
        let typeParameterExpected = "&type=\(relatedBinForDebitExpected)"
        
        XCTAssert(creditDigitalFrontURL!.contains(typeParameterExpected))
        XCTAssert(creditDigitalFrontURL!.contains(relatedBinForDebitExpected))
    }
    
    func test_givenFinancialOverviewEntity_whenICallInitFinancialOverviewEntity_thenStatusMatch() {

        // given
        let financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverview()

        // when
        let sut = CardsBO(financialOverviewEntity: financialOverviewEntity, user: UserBO(user: UserEntity()))

        // then
        XCTAssert(sut.status == financialOverviewEntity.status)
    }

    func test_givenFinancialOverviewEntity_whenICallInitFinancialOverviewEntity_thenNumberOfCardsShouldMatchWithContractsWithProductTypeCards() {

        // given
        let financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverview()
        let cardsExpected = financialOverviewEntity.data?.contracts?.filter({ contractEntity -> Bool in
            contractEntity.productType == FinancialOverviewContractEntity.cardProductType
        })

        // when
        let sut = CardsBO(financialOverviewEntity: financialOverviewEntity, user: UserBO(user: UserEntity()))

        // then
        XCTAssert(sut.cards.count == cardsExpected?.count)
    }

    func test_givenFinancialOverviewEntityWithContractsNil_whenICallInitFinancialOverviewEntity_thenCardsShouldBeEmtpy() {

        // given
        let financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverview()
        financialOverviewEntity.data?.contracts = nil

        // when
        let sut = CardsBO(financialOverviewEntity: financialOverviewEntity, user: UserBO(user: UserEntity()))

        // then
        XCTAssert(sut.cards.isEmpty == true)
    }

    func test_givenFinancialOverviewEntity_whenICallInitFinancialOverviewEntity_thenICallCardsImagesManagerSetupImageTasks() {

        // given
        let financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverview()

        // when
        _ = CardsBO(financialOverviewEntity: financialOverviewEntity, user: UserBO(user: UserEntity()))

        // then
        XCTAssert(dummyCardsImagesManager.isCalledSetupImageTasks == true)
    }

    func test_givenFinancialOverviewEntity_whenICallInitFinancialOverviewEntity_thenICallFinancialOverviewEntityApplyContractRelation() {

        // given
        let dummyFinancialOverviewEntity = DummyFinancialOverviewContractEntity()
        dummyFinancialOverviewEntity.data?.contracts = FinancialOverviewEntityGenerator.financialOverview().data?.contracts?.filter({ entity -> Bool in
            entity.id == FinancialOverviewContractEntity.cardProductType
        })
        // when
        _ = CardsBO(financialOverviewEntity: dummyFinancialOverviewEntity, user: UserBO(user: UserEntity()))

        // then
        XCTAssert(dummyFinancialOverviewEntity.isCalledApplyContractRelation == true)
    }

    func test_givenFinancialOverviewEntityWithCards_whenICallInitFinancialOverviewEntity_thenCardsShoulHaveTheirRelactionContractCorrect() {

        // given
        let financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverview()

        // when
        let sut = CardsBO(financialOverviewEntity: financialOverviewEntity, user: UserBO(user: UserEntity()))

        // then
        XCTAssert(sut.cards[0].relatedContracts.count == 2)

        XCTAssert(sut.cards[0].relatedContracts[0].relatedContractId == "1039485357")
        XCTAssert(sut.cards[0].relatedContracts[0].contractId == "8QJZ9PD6PW8M7JDSGCSER9RE9ZVCP8AQWZEZE33AATJMM620R8Z0")
        XCTAssert(sut.cards[0].relatedContracts[0].number == "4815160009396192")
        XCTAssert(sut.cards[0].relatedContracts[0].numberType?.id == .pan)
        XCTAssert(sut.cards[0].relatedContracts[0].product?.id == .cards)
        XCTAssert(sut.cards[0].relatedContracts[0].relationType?.id == .linkedWith)

        XCTAssert(sut.cards[0].relatedContracts[1].relatedContractId == "1039485357")
        XCTAssert(sut.cards[0].relatedContracts[1].contractId == "RYNKN9XY5XV5V3KN52CAJECW01Q4KGG6PMD6DDRAACP2YMK2MMD0")
        XCTAssert(sut.cards[0].relatedContracts[1].number == "4172123000009991")
        XCTAssert(sut.cards[0].relatedContracts[1].numberType?.id == .pan)
        XCTAssert(sut.cards[0].relatedContracts[1].product?.id == .cards)
        XCTAssert(sut.cards[0].relatedContracts[1].relationType?.id == .linkedWith)

        XCTAssert(sut.cards[1].relatedContracts.count == 1)

        XCTAssert(sut.cards[1].relatedContracts[0].relatedContractId == "1039485357")
        XCTAssert(sut.cards[1].relatedContracts[0].contractId == "Z8P0E6Q107GY0A4VWC0Y84CAX7TSC6GJR4SE86PSNK6P3ESEE5RG")
        XCTAssert(sut.cards[1].relatedContracts[0].number == "4152313302204560")
        XCTAssert(sut.cards[1].relatedContracts[0].numberType?.id == .pan)
        XCTAssert(sut.cards[1].relatedContracts[0].product?.id == .cards)
        XCTAssert(sut.cards[1].relatedContracts[0].relationType?.id == .linkedWith)

        XCTAssert(sut.cards[2].relatedContracts.count == 1)

        XCTAssert(sut.cards[2].relatedContracts[0].relatedContractId == "1039485357")
        XCTAssert(sut.cards[2].relatedContracts[0].contractId == "Z8P0E6Q107GY0A4VWC0Y84CAX7TSC6GJR4SE86PSNK6P3ESEE5RG")
        XCTAssert(sut.cards[2].relatedContracts[0].number == "4152313302204560")
        XCTAssert(sut.cards[2].relatedContracts[0].numberType?.id == .pan)
        XCTAssert(sut.cards[2].relatedContracts[0].product?.id == .cards)
        XCTAssert(sut.cards[2].relatedContracts[0].relationType?.id == .linkedWith)

        XCTAssert(sut.cards[3].relatedContracts.isEmpty)
    }

    func test_givenFinancialOverviewEntityWithCards_whenICallInitFinancialOverviewEntity_thenCardsShouldHaveTheirIndicatorsCorrectlySet() {
        
        // given
        let financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverview()

        let filtered = financialOverviewEntity.data!.contracts!.filter({ $0.productType == FinancialOverviewContractEntity.cardProductType })
        financialOverviewEntity.data!.contracts = filtered

        financialOverviewEntity.data!.contracts![0].userAccessControl = [UserAccessControlEntity]()
        financialOverviewEntity.data!.contracts![0].userAccessControl!.append(UserAccessControlEntity(canOperate: true))
        financialOverviewEntity.data!.contracts![0].detail!.indicators!.removeAll()
        financialOverviewEntity.data!.contracts![0].detail!.indicators!.append(IDNameActiveEntity(id: "BLOCKABLE", name: nil, isActive: false))

        financialOverviewEntity.data!.contracts![1].userAccessControl = [UserAccessControlEntity]()
        financialOverviewEntity.data!.contracts![1].userAccessControl!.append(UserAccessControlEntity(canOperate: false))
        financialOverviewEntity.data!.contracts![1].detail!.indicators!.removeAll()
        financialOverviewEntity.data!.contracts![1].detail!.indicators!.append(IDNameActiveEntity(id: "BLOCKABLE", name: nil, isActive: true))

        // when
        let sut = CardsBO(financialOverviewEntity: financialOverviewEntity, user: UserBO(user: UserEntity()))

        // then
        XCTAssert(sut.cards[0].indicators.first(where: { $0.indicatorId == .settableToOperative })!.isActive == true)
        XCTAssert(sut.cards[1].indicators.first(where: { $0.indicatorId == .settableToOperative })!.isActive == false)
        XCTAssert(sut.cards[0].indicators.first(where: { $0.indicatorId == .cancelable }) == nil)
        XCTAssert(sut.cards[1].indicators.first(where: { $0.indicatorId == .cancelable }) == nil)
        XCTAssert(sut.cards[0].indicators.first(where: { $0.indicatorId == .customizableActivations })!.isActive == true)
        XCTAssert(sut.cards[1].indicators.first(where: { $0.indicatorId == .customizableActivations })!.isActive == true)
    }
    
    func test_givenFinancialOverviewEntityWithAccounts_whenICallInitFinancialOverviewEntity_thenAccountIDAreNotSetInCredit() {
        
        //given
        let financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverview()
        
        // when
        let sut = CardsBO(financialOverviewEntity: financialOverviewEntity, user: UserBO(user: UserEntity()))
        let cards = sut.cards
        
        let accountIDs = cards.compactMap { card -> String? in
            if card.cardType.id == CardType.credit_card {
                return card.accountID
            }
            return card.accountID
        }
        //then
        XCTAssert(accountIDs.isEmpty == true)
    }
    
    func test_givenFinancialOverviewEntityWithAccounts_whenICallInitFinancialOverviewEntity_thenAccountIDAreMatchNumberOfDebitCards() {
        //given
        let financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverviewWithAccountID()
        
        // when
        let sut = CardsBO(financialOverviewEntity: financialOverviewEntity, user: UserBO(user: UserEntity()))
        let cards = sut.cards
        
        let accountIDs = cards.compactMap { card -> String? in
            if card.cardType.id == CardType.debit_card {
                return card.accountID
            }
            return card.accountID
        }
        let debitCards = cards.filter { $0.cardType.id == CardType.debit_card }
        //then
        XCTAssert(accountIDs.count == debitCards.count)
    }

    func test_givenFinancialOverviewEntityWithAccounts_whenICallInitFinancialOverviewEntity_thenAccountIDOnCardShouldMatchItsRelatedAccountContract() {
        
        //given
        let accountID = "lQHpW1zOZE0FGXFjF1KJII_EH23v29-81tJwoDM8T72nYFL0lPhXmg"
        let financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverviewWithAccountID()
        //when
        let sut = CardsBO(financialOverviewEntity: financialOverviewEntity, user: UserBO(user: UserEntity()))
        
        //then
        let debitCards = sut.cards.filter { $0.cardType.id == CardType.debit_card }
        let accountsID = debitCards.filter { $0.accountID == accountID }
        
        XCTAssert(accountsID.count == debitCards.count)
        
    }

    // MARK: - titleIds

    func test_givenCardsBOWithTitleIdsNotRepited_whenICallTitleIds_thenReturnTitleIdsNotRepited() {

        // given
        let sut = CardsGenerator.getThreeCards()

        // when
        let result = sut.titleIds()

        // then
        XCTAssert(result! == ["XA", "VA", "VD"])

    }

    func test_givenCardsBOWithTitleIdsRepited_whenICallTitleIds_thenReturnTitleIdsNotRepited() {

        // given
        let sut = CardsGenerator.getThreeCards()
        sut.cards[0].title?.id = "XA"
        sut.cards[1].title?.id = "DA"
        sut.cards[2].title?.id = "XA"

        // when
        let result = sut.titleIds()

        // then
        XCTAssert(result! == ["XA", "DA"])

    }

    func test_givenCardsBOWithoutTitleIds_whenICallTitleIds_thenReturnTitleIdsNil() {

        // given
        let sut = CardsGenerator.getThreeCards()
        sut.cards[0].title = nil
        sut.cards[1].title = nil
        sut.cards[2].title = nil

        // when
        let result = sut.titleIds()

        // then
        XCTAssert(result == nil)

    }

    func test_givenCardsBOWithoutCards_whenICallTitleIds_thenReturnTitleIdsNil() {

        // given
        let sut = CardsGenerator.getCardsEmtpy()

        // when
        let result = sut.titleIds()

        // then
        XCTAssert(result == nil)

    }
    
    // MARK: - firstNfcCard
    
    func test_givenCardsBOWithPhysicalSupportNfc_whenICallFirstNfcCard_thenReturnCardBO() {
        
        // given
        let sut = CardsGenerator.getSecondCardsBOWithPhysicalSupportNfc()
        
        // when
        let result = sut.firstNfcCard()
        
        // then
        XCTAssert(result == sut.cards[1])
        
    }
    
    func test_givenCardsBOWithoutPhysicalSupportNfc_whenICallFirstNfcCard_thenReturnCardBONil() {
        
        // given
        let sut = CardsGenerator.getThreeCards()
        
        // when
        let result = sut.firstNfcCard()
        
        // then
        XCTAssert(result == nil)
        
    }
}
