//
//  ShoppingWebPresenterTest.swift
//  shoppingapp
//
//  Created by Luis Monroy on 08/03/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest
import RxSwift
import CellsNativeComponents

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class ShoppingWebPresenterTest: XCTestCase {

    var sut: ShoppingWebPresenter<DummyView>!
    var dummyBusManager: DummyBusManager!

    override func setUp() {
        
        super.setUp()
        dummyBusManager = DummyBusManager()
        sut = ShoppingWebPresenter<DummyView>(busManager: dummyBusManager)
    }

    // MARK: - setModel

    func test_givenAPromotionDetailTransport_whenCallSetModel_thenIHaveWebInfo() {

        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()

        let webTransport = WebTransport(webPage: promotionBO.legalConditions?.url, webTitle: Localizables.promotions.key_promotions_terms_and_conditions_text)

        // when
        sut.setModel(model: webTransport)

        // then
        XCTAssert(sut.webPageUrl == webTransport.webPage)
        XCTAssert(sut.webTitle == webTransport.webTitle)
    }

    // MARK: - viewDidAppear
    
    func test_givenAWebPageAndWebTitle_whenICallViewDidAppear_thenICallBusManagerShowLoading() {
        
        // given
        let dummyView = DummyView()
        sut.view = dummyView
        
        sut.webPageUrl = "webPage"
        sut.webTitle = "webTitle"
        
        // when
        sut.viewDidAppear()
        
        // then
        XCTAssert(dummyBusManager.isCalledShowLoading == true)
    }

    func test_givenAWebPageAndWebTitle_whenICallViewDidAppear_thenICallShowWebPage() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        sut.webPageUrl = "webPage"
        sut.webTitle = "webTitle"

        // when
        sut.viewDidAppear()

        // then
        XCTAssert(dummyView.isCalledShowWebPage == true)
    }

    func test_givenAWebPageAndWebTitle_whenICallViewDidAppear_thenICallShowWebPageWithWebPageAndWebTitleCorrect() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        sut.webPageUrl = "webPage"
        sut.webTitle = "webTitle"

        // when
        sut.viewDidAppear()

        // then
        XCTAssert(dummyView.webPageSent == sut.webPageUrl)
        XCTAssert(dummyView.webTitleSent == sut.webTitle)
    }
    
    // MARK: - contentLoadedSuccess
    
    func test_givenAny_whenICallContentLoadedSuccess_thenICallBusManagerHideLoading() {
        
        // given
        
        // when
        sut.contentLoadedSuccess()
        
        // then
        XCTAssert(dummyBusManager.isCalledHideLoading == true)
    }
    
    // MARK: - contentLoadedWithError
    
    func test_givenAny_whenICallContentLoadedWithError_thenICallBusManagerHideLoading() {
        
        // given
        
        // when
        sut.contentLoadedWithError()
        
        // then
        XCTAssert(dummyBusManager.isCalledHideLoading == true)
    }

    class DummyView: ShoppingWebViewProtocol, ViewProtocol {

        var isCalledShowWebPage = false

        var webPageSent: String?
        var webTitleSent: String?

        func showWebPage(ofUrl url: String, withTitle title: String) {
            isCalledShowWebPage = true

            webPageSent = url
            webTitleSent = title
        }

        func showError(error: ModelBO) {

        }

        func changeColorEditTexts() {

        }
    }

    class DummyBusManager: BusManager {
        
        var isCalledShowLoading = false
        var isCalledHideLoading = false

        override init() {
            
            super.init()
            routerFactory = RouterInitFactory()
        }
        
        override func showLoading(completion: (() -> Void)?) {
            
            completion?()
            
            isCalledShowLoading = true
        }
        
        override func hideLoading(completion: (() -> Void)? ) {
            
            completion?()

            isCalledHideLoading = true
        }
    }
}
