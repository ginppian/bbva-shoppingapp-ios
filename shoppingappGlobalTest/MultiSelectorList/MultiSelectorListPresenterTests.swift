//
//  MultiSelectorListPresenterTests.swift
//  shoppingapp
//
//  Created by jesus.martinez on 26/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class MultiSelectorListPresenterTests: XCTestCase {

    let sut = MultiSelectorListPresenter()

    // MARK: - onConfigureView

    func test_givenDisplayData_whenICallOnConfigureView_thenICallViewSetTitle() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.displayData = MultiSelectorListDisplayData(withTitle: "title")

        // when
        sut.onConfigureView()

        // then
        XCTAssert(dummyView.isCalledConfigureTitle == true)

    }

    func test_givenDisplayData_whenICallOnConfigureView_thenICallViewConfigureTitleWithTitleOfDisplayData() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.displayData = MultiSelectorListDisplayData(withTitle: "title")

        // when
        sut.onConfigureView()

        // then
        XCTAssert(dummyView.titleSent == sut.displayData!.title)

    }

    func test_givenDisplayDataWithNoOptionSelected_whenICallOnConfigureView_thenIDoNotCallViewShowOptions() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        var options = [OptionItemDisplayData]()
        options.append(OptionItemDisplayData(imageName: "imageName", title: "title1"))
        options.append(OptionItemDisplayData(imageName: "imageName", title: "title2"))
        options.append(OptionItemDisplayData(imageName: "imageName", title: "title3"))

        sut.displayData = MultiSelectorListDisplayData(withTitle: "title")
        sut.displayData?.options = options

        // when
        sut.onConfigureView()

        // then
        XCTAssert(dummyView.isCalledShowOptions == false)

    }

    func test_givenDisplayDataWithOneOrMoreOptionsSelected_whenICallOnConfigureView_thenICallViewShowOptions() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        var options = [OptionItemDisplayData]()
        options.append(OptionItemDisplayData(imageName: "imageName", title: "title1"))
        options.append(OptionItemDisplayData(imageName: "imageName", title: "title2"))
        options.append(OptionItemDisplayData(imageName: "imageName", title: "title3", isSelected: true))

        sut.displayData = MultiSelectorListDisplayData(withTitle: "title")
        sut.displayData?.options = options

        // when
        sut.onConfigureView()

        // then
        XCTAssert(dummyView.isCalledShowOptions == true)

    }

    func test_givenDisplayDataWithOneOrMoreOptionsSelected_whenICallOnConfigureView_thenICallViewShowOptionsThanDisplayData() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        var options = [OptionItemDisplayData]()
        options.append(OptionItemDisplayData(imageName: "imageName", title: "title1"))
        options.append(OptionItemDisplayData(imageName: "imageName", title: "title2", isSelected: true))
        options.append(OptionItemDisplayData(imageName: "imageName", title: "title3"))

        sut.displayData = MultiSelectorListDisplayData(withTitle: "title")
        sut.displayData?.options = options

        // when
        sut.onConfigureView()

        // then
        XCTAssert(dummyView.optionsSent! == sut.displayData!.options)

    }

    func test_givennDisplayDataAndOptionsShowedTrue_whenICallOnConfigureView_thenICallViewReloadStatus() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        var options = [OptionItemDisplayData]()
        options.append(OptionItemDisplayData(imageName: "imageName", title: "title1"))
        options.append(OptionItemDisplayData(imageName: "imageName", title: "title2", isSelected: true))
        options.append(OptionItemDisplayData(imageName: "imageName", title: "title3"))

        sut.displayData = MultiSelectorListDisplayData(withTitle: "title")
        sut.displayData?.options = options

        sut.optionsShowed = true

        // when
        sut.onConfigureView()

        // then
        XCTAssert(dummyView.isCalledReloadStatus == true)

    }

    func test_givenDisplayDataAndOptionsShowedTrue_whenICallOnConfigureView_thenICallViewReloadStatusWithDisplayData() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        var options = [OptionItemDisplayData]()
        options.append(OptionItemDisplayData(imageName: "imageName", title: "title1"))
        options.append(OptionItemDisplayData(imageName: "imageName", title: "title2", isSelected: true))
        options.append(OptionItemDisplayData(imageName: "imageName", title: "title3"))

        sut.displayData = MultiSelectorListDisplayData(withTitle: "title")
        sut.displayData?.options = options

        sut.optionsShowed = true

        // when
        sut.onConfigureView()

        // then
        XCTAssert(dummyView.optionsSent! == sut.displayData!.options)

    }

    // MARK: - showMoreInfoPressed

    func test_givenDisplayData_whenICallShowMoreInfoPressed_thenICallViewShowOptions() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.displayData = MultiSelectorListDisplayData(withTitle: "title")

        // when
        sut.showMoreInfoPressed()

        // then
        XCTAssert(dummyView.isCalledShowOptions == true)

    }

    func test_givenDisplayData_whenICallShowMoreInfoPressed_thenICallViewShowOptionsWithTheSameOptionsThanDisplayData() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given

        var options = [OptionItemDisplayData]()
        options.append(OptionItemDisplayData(imageName: "imageName", title: "title1"))
        options.append(OptionItemDisplayData(imageName: "imageName", title: "title2"))
        options.append(OptionItemDisplayData(imageName: "imageName", title: "title3"))

        sut.displayData = MultiSelectorListDisplayData(withTitle: "title")
        sut.displayData?.options = options

        // when
        sut.showMoreInfoPressed()

        // then
        XCTAssert(dummyView.optionsSent! == sut.displayData!.options)

    }

    func test_givenAny_whenICallShowMoreInfoPressed_thenOptionsShowedShouldBeTrue() {

        // given
        sut.optionsShowed = false

        // when
        sut.showMoreInfoPressed()

        // then
        XCTAssert(sut.optionsShowed == true)

    }

    class DummyView: MultiSelectorListViewProtocol {

        var isCalledConfigureTitle = false
        var isCalledShowOptions = false
        var isCalledReloadStatus = false

        var titleSent = ""
        var optionsSent: [OptionItemDisplayData]?

        func showTitle(withText text: String) {

            isCalledConfigureTitle = true

            titleSent = text

        }

        func showOptions(withDisplayData displayData: [OptionItemDisplayData]) {

            isCalledShowOptions = true
            optionsSent = displayData

        }

        func reloadStatus(withDisplayData displayData: [OptionItemDisplayData]) {

            isCalledReloadStatus = true
            optionsSent = displayData
        }

    }

}
