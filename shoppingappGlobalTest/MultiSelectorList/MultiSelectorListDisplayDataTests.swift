//
//  MultiSelectorListDisplayDataTests.swift
//  shoppingapp
//
//  Created by jesus.martinez on 26/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class MultiSelectorListDisplayDataTests: XCTestCase {

    // MARK: - InitWithTitle

    func test_givenText_whenICallInitWithTitle_thenTitleShouldMatchAndOptionsShouldNotBeNil() {

        // given
        let text = "text"

        // when
        let sut = MultiSelectorListDisplayData(withTitle: text)

        // then
        XCTAssert(sut.title == text)

    }

    // MARK: - InitWithTitle: andCategoriesBO:

    func test_givenTitleAndCategoriesBO_whenICallInitWithTitleAndCategoriesBO_thenTitleShouldMatchAndNumberOfOptionsShouldMatch() {

        // given
        let text = "text"
        let categories = PromotionsGenerator.getCategoryBO()

        // when
        let sut = MultiSelectorListDisplayData(withTitle: text, andCategoriesBO: categories)

        // then
        XCTAssert(sut.title == text)
        XCTAssert(sut.options.count == categories.categories.count)
        XCTAssert(sut.options[0].isSelected == false)

    }

    func test_givenTitleAndCategoriesBOActivities_whenICallInitWithTitleAndCategoriesBO_thenOptionTitleAndImageShouldMatch() {

        // given
        let text = "text"
        let categories = PromotionsGenerator.getCategoryBO()
        categories.categories[0].id = .activities

        // when
        let sut = MultiSelectorListDisplayData(withTitle: text, andCategoriesBO: categories)

        // then
        XCTAssert(sut.options[0].title == categories.categories[0].name)
        XCTAssert(sut.options[0].imageName == ConstantsImages.Promotions.activities)
        XCTAssert(sut.options[0].isSelected == false)

    }

    func test_givenTitleAndCategoriesBOAuto_whenICallInitWithTitleAndCategoriesBO_thenOptionTitleAndImageShouldMatch() {

        // given
        let text = "text"
        let categories = PromotionsGenerator.getCategoryBO()
        categories.categories[0].id = .auto

        // when
        let sut = MultiSelectorListDisplayData(withTitle: text, andCategoriesBO: categories)

        // then
        XCTAssert(sut.options[0].title == categories.categories[0].name)
        XCTAssert(sut.options[0].imageName == ConstantsImages.Promotions.auto)
        XCTAssert(sut.options[0].isSelected == false)

    }

    func test_givenTitleAndCategoriesBOBeautyAndFragances_whenICallInitWithTitleAndCategoriesBO_thenOptionTitleAndImageShouldMatch() {

        // given
        let text = "text"
        let categories = PromotionsGenerator.getCategoryBO()
        categories.categories[0].id = .beautyAndGragances

        // when
        let sut = MultiSelectorListDisplayData(withTitle: text, andCategoriesBO: categories)

        // then
        XCTAssert(sut.options[0].title == categories.categories[0].name)
        XCTAssert(sut.options[0].imageName == ConstantsImages.Promotions.beautyAndFragances)
        XCTAssert(sut.options[0].isSelected == false)

    }

    func test_givenTitleAndCategoriesBOdietAndNutrition_whenICallInitWithTitleAndCategoriesBO_thenOptionTitleAndImageShouldMatch() {

        // given
        let text = "text"
        let categories = PromotionsGenerator.getCategoryBO()
        categories.categories[0].id = .dietAndNutrition

        // when
        let sut = MultiSelectorListDisplayData(withTitle: text, andCategoriesBO: categories)

        // then
        XCTAssert(sut.options[0].title == categories.categories[0].name)
        XCTAssert(sut.options[0].imageName == ConstantsImages.Promotions.diet)
        XCTAssert(sut.options[0].isSelected == false)

    }

    func test_givenTitleAndCategoriesBOfoodAndGourmet_whenICallInitWithTitleAndCategoriesBO_thenOptionTitleAndImageShouldMatch() {

        // given
        let text = "text"
        let categories = PromotionsGenerator.getCategoryBO()
        categories.categories[0].id = .foodAndGourmet

        // when
        let sut = MultiSelectorListDisplayData(withTitle: text, andCategoriesBO: categories)

        // then
        XCTAssert(sut.options[0].title == categories.categories[0].name)
        XCTAssert(sut.options[0].imageName == ConstantsImages.Promotions.foodAndGourmet)
        XCTAssert(sut.options[0].isSelected == false)

    }

    func test_givenTitleAndCategoriesBOhobbies_whenICallInitWithTitleAndCategoriesBO_thenOptionTitleAndImageShouldMatch() {

        // given
        let text = "text"
        let categories = PromotionsGenerator.getCategoryBO()
        categories.categories[0].id = .hobbies

        // when
        let sut = MultiSelectorListDisplayData(withTitle: text, andCategoriesBO: categories)

        // then
        XCTAssert(sut.options[0].title == categories.categories[0].name)
        XCTAssert(sut.options[0].imageName == ConstantsImages.Promotions.hobbies)
        XCTAssert(sut.options[0].isSelected == false)

    }

    func test_givenTitleAndCategoriesBOhome_whenICallInitWithTitleAndCategoriesBO_thenOptionTitleAndImageShouldMatch() {

        // given
        let text = "text"
        let categories = PromotionsGenerator.getCategoryBO()
        categories.categories[0].id = .home

        // when
        let sut = MultiSelectorListDisplayData(withTitle: text, andCategoriesBO: categories)

        // then
        XCTAssert(sut.options[0].title == categories.categories[0].name)
        XCTAssert(sut.options[0].imageName == ConstantsImages.Promotions.home)
        XCTAssert(sut.options[0].isSelected == false)

    }

    func test_givenTitleAndCategoriesBOlearning_whenICallInitWithTitleAndCategoriesBO_thenOptionTitleAndImageShouldMatch() {

        // given
        let text = "text"
        let categories = PromotionsGenerator.getCategoryBO()
        categories.categories[0].id = .learning

        // when
        let sut = MultiSelectorListDisplayData(withTitle: text, andCategoriesBO: categories)

        // then
        XCTAssert(sut.options[0].title == categories.categories[0].name)
        XCTAssert(sut.options[0].imageName == ConstantsImages.Promotions.learning)
        XCTAssert(sut.options[0].isSelected == false)

    }

    func test_givenTitleAndCategoriesBOother_whenICallInitWithTitleAndCategoriesBO_thenOptionTitleAndImageShouldMatch() {

        // given
        let text = "text"
        let categories = PromotionsGenerator.getCategoryBO()
        categories.categories[0].id = .other

        // when
        let sut = MultiSelectorListDisplayData(withTitle: text, andCategoriesBO: categories)

        // then
        XCTAssert(sut.options[0].title == categories.categories[0].name)
        XCTAssert(sut.options[0].imageName == ConstantsImages.Promotions.other)
        XCTAssert(sut.options[0].isSelected == false)

    }

    func test_givenTitleAndCategoriesBOrent_whenICallInitWithTitleAndCategoriesBO_thenOptionTitleAndImageShouldMatch() {

        // given
        let text = "text"
        let categories = PromotionsGenerator.getCategoryBO()
        categories.categories[0].id = .rent

        // when
        let sut = MultiSelectorListDisplayData(withTitle: text, andCategoriesBO: categories)

        // then
        XCTAssert(sut.options[0].title == categories.categories[0].name)
        XCTAssert(sut.options[0].imageName == ConstantsImages.Promotions.rent)
        XCTAssert(sut.options[0].isSelected == false)

    }

    func test_givenTitleAndCategoriesBOrestaurants_whenICallInitWithTitleAndCategoriesBO_thenOptionTitleAndImageShouldMatch() {

        // given
        let text = "text"
        let categories = PromotionsGenerator.getCategoryBO()
        categories.categories[0].id = .restaurants

        // when
        let sut = MultiSelectorListDisplayData(withTitle: text, andCategoriesBO: categories)

        // then
        XCTAssert(sut.options[0].title == categories.categories[0].name)
        XCTAssert(sut.options[0].imageName == ConstantsImages.Promotions.restaurant)
        XCTAssert(sut.options[0].isSelected == false)

    }

    func test_givenTitleAndCategoriesBOshoppings_whenICallInitWithTitleAndCategoriesBO_thenOptionTitleAndImageShouldMatch() {

        // given
        let text = "text"
        let categories = PromotionsGenerator.getCategoryBO()
        categories.categories[0].id = .shopping

        // when
        let sut = MultiSelectorListDisplayData(withTitle: text, andCategoriesBO: categories)

        // then
        XCTAssert(sut.options[0].title == categories.categories[0].name)
        XCTAssert(sut.options[0].imageName == ConstantsImages.Promotions.shopping)
        XCTAssert(sut.options[0].isSelected == false)

    }

    func test_givenTitleAndCategoriesBOshows_whenICallInitWithTitleAndCategoriesBO_thenOptionTitleAndImageShouldMatch() {

        // given
        let text = "text"
        let categories = PromotionsGenerator.getCategoryBO()
        categories.categories[0].id = .shows

        // when
        let sut = MultiSelectorListDisplayData(withTitle: text, andCategoriesBO: categories)

        // then
        XCTAssert(sut.options[0].title == categories.categories[0].name)
        XCTAssert(sut.options[0].imageName == ConstantsImages.Promotions.shows)
        XCTAssert(sut.options[0].isSelected == false)

    }

    func test_givenTitleAndCategoriesBOtechnology_whenICallInitWithTitleAndCategoriesBO_thenOptionTitleAndImageShouldMatch() {

        // given
        let text = "text"
        let categories = PromotionsGenerator.getCategoryBO()
        categories.categories[0].id = .technology

        // when
        let sut = MultiSelectorListDisplayData(withTitle: text, andCategoriesBO: categories)

        // then
        XCTAssert(sut.options[0].title == categories.categories[0].name)
        XCTAssert(sut.options[0].imageName == ConstantsImages.Promotions.technology)
        XCTAssert(sut.options[0].isSelected == false)

    }

    func test_givenTitleAndCategoriesBOtravel_whenICallInitWithTitleAndCategoriesBO_thenOptionTitleAndImageShouldMatch() {

        // given
        let text = "text"
        let categories = PromotionsGenerator.getCategoryBO()
        categories.categories[0].id = .travel

        // when
        let sut = MultiSelectorListDisplayData(withTitle: text, andCategoriesBO: categories)

        // then
        XCTAssert(sut.options[0].title == categories.categories[0].name)
        XCTAssert(sut.options[0].imageName == ConstantsImages.Promotions.travel)
        XCTAssert(sut.options[0].isSelected == false)

    }

    func test_givenTitleAndCategoriesBOunknown_whenICallInitWithTitleAndCategoriesBO_thenOptionTitleAndImageShouldMatch() {

        // given
        let text = "text"
        let categories = PromotionsGenerator.getCategoryBO()
        categories.categories[0].id = .unknown

        // when
        let sut = MultiSelectorListDisplayData(withTitle: text, andCategoriesBO: categories)

        // then
        XCTAssert(sut.options[0].title == categories.categories[0].name)
        XCTAssert(sut.options[0].imageName == ConstantsImages.Promotions.promotion)
        XCTAssert(sut.options[0].isSelected == false)

    }

    func test_givenCategoriesBOAndSomeCategoriesSelected_whenICallInitWithTitleCategoriesBO_thenDisplayDataOfCategoriesSelectedShouldBeTrue() {

        // given
        let text = "text"
        let categories = PromotionsGenerator.getCategoryBO()
        let categoriesSelected: [CategoryType] = [.auto, .home, .ecommerce]

        // when
        let sut = MultiSelectorListDisplayData(withTitle: text, andCategoriesBO: categories, andCategoriesSelected: categoriesSelected)

        // then
        XCTAssert(sut.options[1].isSelected == true)
        XCTAssert(sut.options[6].isSelected == true)
        XCTAssert(sut.options[28].isSelected == true)

    }

}
