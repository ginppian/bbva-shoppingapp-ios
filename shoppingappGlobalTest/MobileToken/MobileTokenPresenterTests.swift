//
//  MobileTokenPresenterTests.swift
//  shoppingappMXTest
//
//  Created by jesus.martinez on 27/12/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
@testable import shoppingappMX
#endif

class MobileTokenPresenterTests: XCTestCase {
    
    var sut: MobileTokenPresenter<DummyMobileTokenView>!
    var dummyBusManager = DummyBusManager()
    var dummyURLOpener = DummyURLOpener()
    
    override func setUp() {
        super.setUp()
        
        dummyURLOpener = DummyURLOpener()
        
        sut = MobileTokenPresenter(busManager: dummyBusManager)
        sut.urlOpener = dummyURLOpener
    }
    
    // MARK: - openAppPressed
    
    func test_givenAny_whenICallOpenAppPressed_thenICallURLOpenerOpenURL() {
        
        // given
        
        // when
        sut.openAppPressed()
        
        // then
        XCTAssert(dummyURLOpener.isCalledCanOpenURL == true)
    }
    
    func test_givenGlomoInstalled_whenICallOpenAppPressed_thenICallURLOpenerOpenURLWithGlomoScheme() {
        
        let dummyPublicConfigurationDTO = MockPublicConfigurationDTO()
        
        // given
        dummyPublicConfigurationDTO.mockIsGlomoInstalled = true
        sut.publicConfiguration = dummyPublicConfigurationDTO
        
        // when
        sut.openAppPressed()

        // then
        XCTAssert(dummyURLOpener.urlSent == URL(string: dummyPublicConfigurationDTO.publicConfig!.mobileGlomo!.iOS!.schema!))
    }
    
    func test_givenNotGlomoInstalledAndBancomerInstalled_whenICallOpenAppPressed_thenICallURLOpenerOpenURLWithBancomerScheme() {
        
        let dummyPublicConfigurationDTO = MockPublicConfigurationDTO()
        
        // given
        dummyPublicConfigurationDTO.mockIsGlomoInstalled = false
        dummyPublicConfigurationDTO.mockIsBancomerInstalled = true
        sut.publicConfiguration = dummyPublicConfigurationDTO
        
        // when
        sut.openAppPressed()

        // then
        XCTAssert(dummyURLOpener.urlSent == URL(string: dummyPublicConfigurationDTO.publicConfig!.mobileBancomer!.iOS!.schema!))
    }
    
    func test_givenNotGlomoInstalledAndNeitherBancomerInstalled_whenICallOpenAppPressed_thenICallURLOpenerOpenURLWithGlomoStore() {
        
        let dummyPublicConfigurationDTO = MockPublicConfigurationDTO()
        
        // given
        dummyPublicConfigurationDTO.mockIsGlomoInstalled = false
        dummyPublicConfigurationDTO.mockIsBancomerInstalled = false
        sut.publicConfiguration = dummyPublicConfigurationDTO
        
        // when
        sut.openAppPressed()

        // then
        XCTAssert(dummyURLOpener.urlSent == URL(string: dummyPublicConfigurationDTO.publicConfig!.mobileGlomo!.iOS!.url!))
    }
    
    class DummyMobileTokenView: MobileTokenViewProtocol {
        
        func showError(error: ModelBO) {
        }
        func changeColorEditTexts() {
        }
    }
}
