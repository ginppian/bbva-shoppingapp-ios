//
//  TransactionsListPresenterTest.swift
//  shoppingapp
//
//  Created by jesus.martinez on 7/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import XCTest
import RxSwift
import CellsNativeComponents

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class TransactionsFilterPresenterTest: XCTestCase {

    var sut: TransactionsFilterPresenter<DummyView>!
    var dummyBusManager: DummyBusManager!

    override func setUp() {
        super.setUp()
        dummyBusManager = DummyBusManager()
        sut = TransactionsFilterPresenter<DummyView>(busManager: dummyBusManager)
    }

    override func tearDown() {
        super.tearDown()
    }

    // MARK: ConfigureData

    func test_givenAViewAndShouldShowConceptFilter_whenICallConfigureData_thenCallConfigureConceptFilter() {

        //given
        let dummyView = DummyView()
        sut.shouldShowConceptFilter = true

        //when
        sut.view = dummyView
        sut.configureData()

        //then
        XCTAssert(dummyView.isCalledConfigureConceptFilter == true)

    }

    func test_givenAViewAndShouldShowConceptFilter_whenICallConfigureData_thenCallConfigureConceptFilterWithAppropiateText() {

        //given
        let dummyView = DummyView()
        sut.shouldShowConceptFilter = true

        //when
        sut.view = dummyView
        sut.configureData()

        //then
        XCTAssert(dummyView.conceptPlaceHolderSent == Localizables.transactions.key_transactions_contains_text)

    }

    func test_givenAView_whenICallConfigureData_thenCallConfigureDateFilter() {

        //given
        let dummyView = DummyView()

        //when
        sut.view = dummyView
        sut.configureData()

        //then
        XCTAssert(dummyView.isCalledConfigureDateFilter == true)

    }

    func test_givenAView_whenICallConfigureData_thenCallConfigureAmountFilter() {

        //given
        let dummyView = DummyView()

        //when
        sut.view = dummyView
        sut.configureData()

        //then
        XCTAssert(dummyView.isCalledConfigureAmountFilter == true)

    }

    func test_givenAView_whenICallConfigureData_thenCallConfigureDateFilterWithTodayDate() {

        //given
        let dummyView = DummyView()

        //when
        sut.view = dummyView
        sut.configureData()

        //then
        XCTAssert(dummyView.isCalledShowCheckFilterButtonState == true)

    }

    func test_givenAView_whenICallConfigureData_thenCallShowCheckRestoreButtonState() {

        //given
        let dummyView = DummyView()

        //when
        sut.view = dummyView
        sut.configureData()

        //then
        XCTAssert(dummyView.isCalledShowCheckRestoreButtonState == true)

    }

    func test_givenAny_whenICallConfigureData_thenICallViewSendScreenOmniture() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.configureData()

        // then
        XCTAssert(dummyView.isCalledSendScreenOmniture == true)

    }

    func test_givenAShowConceptFilter_whenICallConfigureData_thenIDoNotCallHideConceptFilter() {

        //given
        let dummyView = DummyView()
        sut.view = dummyView
        sut.shouldShowConceptFilter = true

        //when
        sut.configureData()

        //then
        XCTAssert(dummyView.isCalledHideConceptFilter == false)

    }

    func test_givenAHideConceptFilter_whenICallConfigureData_thenICallHideConceptFilter() {

        //given
        let dummyView = DummyView()
        sut.view = dummyView
        sut.shouldShowConceptFilter = false

        //when
        sut.configureData()

        //then
        XCTAssert(dummyView.isCalledHideConceptFilter == true)

    }

    // MARK: TransportFilter Concept

    func test_givenAEmptyConcept_whenICallModifiedConcept_thenTheTransportModelConceptIsEmpty() {

        //given
        let concept: String = ""
        //when
        sut.modifiedConcept(withText: concept)

        //then
        XCTAssert((sut.transactionFilterTransport.conceptText?.isEmpty)!)

    }

    func test_givenANilConcept_whenICallModifiedConcept_thenTheTransportModelConceptIsNil() {

        //given
        let concept: String? = nil
        //when
        sut.modifiedConcept(withText: concept)

        //then
        XCTAssert(sut.transactionFilterTransport.conceptText == nil)

    }

    func test_givenAConcept_whenICallModifiedConcept_thenTheTransportModelConceptIsEqualToReceiveConcept() {

        //given
        let concept: String = "asdf"
        //when
        sut.modifiedConcept(withText: concept)

        //then
        XCTAssert(sut.transactionFilterTransport.conceptText == "asdf")

    }

    func test_givenATransportConceptEmpty_whenICallNeedConceptToFilter_thenTheReturnValueIsFalse() {

        //given
        sut.transactionFilterTransport.conceptText = ""

        //when
        let returnValue = sut.needConceptToFilter()

        //then
        XCTAssert(returnValue == false)

    }

    func test_givenATransportConceptNil_whenICallNeedConceptToFilter_thenTheReturnValueIsFalse() {

        //given
        sut.transactionFilterTransport.conceptText = nil

        //when
        let returnValue = sut.needConceptToFilter()

        //then
        XCTAssert(returnValue == false)

    }

    func test_givenATransportConcept_whenICallNeedConceptToFilter_thenTheReturnValueIsTrue() {

        //given
        sut.transactionFilterTransport.conceptText = "asdf"

        //when
        let returnValue = sut.needConceptToFilter()

        //then
        XCTAssert(returnValue == true)

    }

    // MARK: TransportFilter Date

    func test_givenAEmptyFromDate_whenICallModifiedDate_thenTheTransportModelFromDateIsEmpty() {

        //given
        let fromDate: String = ""

        //when
        sut.modifiedDate(withDateFrom: fromDate, withDateTo: "")

        //then
        XCTAssert((sut.transactionFilterTransport.fromDateText?.isEmpty)!)

    }

    func test_givenAEmptyToDate_whenICallModifiedDate_thenTheTransportModelToDateIsEmpty() {

        //given
        let toDate: String = ""

        //when
        sut.modifiedDate(withDateFrom: "", withDateTo: toDate)

        //then
        XCTAssert((sut.transactionFilterTransport.toDateText?.isEmpty)!)

    }

    func test_givenANilFromDate_whenICallModifiedDate_thenTheTransportModelFromDateIsNil() {

        //given
        let fromDate: String? = nil

        //when
        sut.modifiedDate(withDateFrom: fromDate, withDateTo: "")

        //then
        XCTAssert(sut.transactionFilterTransport.fromDateText == nil)

    }

    func test_givenANilToDate_whenICallModifiedDate_thenTheTransportModelToDateIsNil() {

        //given
        let toDate: String? = nil

        //when
        sut.modifiedDate(withDateFrom: "", withDateTo: toDate)

        //then
        XCTAssert(sut.transactionFilterTransport.toDateText == nil)

    }

    func test_givenAFromDate_whenICallModifiedDate_thenTheTransportModelFromDateIsEqualToReceiveFromDate() {

        //given
        let date = Date.date(fromLocalTimeZoneString: "2017-01-01", withFormat: Date.DATE_FORMAT_SERVER)
        let fromDate = date?.string(format: Date.DATE_FORMAT_SERVER)

        //when
        sut.modifiedDate(withDateFrom: fromDate, withDateTo: "")

        //then
        XCTAssert(sut.transactionFilterTransport.fromDateText == fromDate)

    }

    func test_givenAToDate_whenICallModifiedDate_thenTheTransportModelToDateIsEqualToReceiveToDate() {

        //given
        let date = Date.date(fromLocalTimeZoneString: "2017-02-02", withFormat: Date.DATE_FORMAT_SERVER)
        let toDate = date?.string(format: Date.DATE_FORMAT_SERVER)

        //when
        sut.modifiedDate(withDateFrom: "", withDateTo: toDate)

        //then
        XCTAssert(sut.transactionFilterTransport.toDateText == toDate)

    }

    func test_givenATodayDateinToDate_whenIcallModifiedDate_thenTheTransportToDateIsNotModified() {

        //given
        let toDate: String = Date().string(format: Date.DATE_FORMAT_SERVER)

        //when
        sut.modifiedDate(withDateFrom: "", withDateTo: toDate)

        //then
        XCTAssert(sut.transactionFilterTransport.toDateText == nil)

    }

    func test_givenADateinToDate_whenIModifiedToDateToToday_thenTheTransportToDateIsNil() {

        //given

        let date = Date.date(fromLocalTimeZoneString: "2017-02-02", withFormat: Date.DATE_FORMAT_SERVER)
        var toDate = date?.string(format: Date.DATE_FORMAT_SERVER)

        sut.modifiedDate(withDateFrom: "", withDateTo: toDate)

        XCTAssert(sut.transactionFilterTransport.toDateText == toDate)

        //when

        toDate = Date().string(format: Date.DATE_FORMAT_SERVER)

        sut.modifiedDate(withDateFrom: "", withDateTo: toDate)

        //then
        XCTAssert(sut.transactionFilterTransport.toDateText == nil)

    }

    func test_givenAFromDateNilOrEmptyInTransportFromDate_whenICallDateFromIsModified_theReturnValueIsFalse() {

        //given
        let fromDate: String? = nil
        sut.transactionFilterTransport.fromDateText = fromDate

        //when
        let boolValue: Bool = sut.dateFromIsModified()
        //then
        XCTAssert(boolValue == false)
    }

    func test_givenAFromDateInTransportFromDate_whenICallDateFromIsModified_theReturnValueIsTrue() {

        //given
        let date = Date.date(fromLocalTimeZoneString: "2017-01-01", withFormat: Date.DATE_FORMAT_SERVER)
        let fromDate = date?.string(format: Date.DATE_FORMAT_SERVER)
        sut.transactionFilterTransport.fromDateText = fromDate

        //when
        let boolValue: Bool = sut.dateFromIsModified()
        //then
        XCTAssert(boolValue == true)
    }

    func test_givenAToDateNilOrEmptyInTransportFromDate_whenICallDateToIsModified_theReturnValueIsFalse() {

        //given
        let toDate: String? = nil
        sut.transactionFilterTransport.toDateText = toDate

        //when
        let boolValue: Bool = sut.dateToIsModified()
        //then
        XCTAssert(boolValue == false)
    }

    func test_givenAToDateInTransportToDateEqualToday_whenICallDateToIsModified_theReturnValueIsFalse() {

        //given
        let toDate: String = Date().string(format: Date.DATE_FORMAT_SERVER)
        sut.transactionFilterTransport.toDateText = toDate

        //when
        let boolValue: Bool = sut.dateToIsModified()
        //then
        XCTAssert(boolValue == false)
    }

    func test_givenAToDateInTransportToDateNotEqualToday_whenICallDateToIsModified_theReturnValueIsTrue() {

        //given
        let date = Date.date(fromLocalTimeZoneString: "2017-09-04", withFormat: Date.DATE_FORMAT_SERVER)
        sut.transactionFilterTransport.toDateText = date?.string(format: Date.DATE_FORMAT_SERVER)

        //when
        let boolValue: Bool = sut.dateToIsModified()
        //then
        XCTAssert(boolValue == true)
    }

    func test_givenADateFromNotModifiedAndToDateNotModified_whenICallNeedUseDateToFilter_thenReturnValueIsFalse() {

        //given
        let fromDate: String? = nil
        sut.transactionFilterTransport.fromDateText = fromDate
        let toDate: String? = nil
        sut.transactionFilterTransport.toDateText = toDate

        //then
        let boolNeedUse = sut.needUseDateToFilter()

        //then
        XCTAssert(boolNeedUse == false)
    }

    func test_givenADateFromNotModifiedAndToDateModified_whenICallNeedUseDateToFilter_thenReturnValueIsTrue() {

        //given
        let fromDate: String? = nil
        sut.transactionFilterTransport.fromDateText = fromDate
        let date = Date.date(fromLocalTimeZoneString: "2017-08-12", withFormat: Date.DATE_FORMAT_SERVER)
        sut.transactionFilterTransport.toDateText = date?.string(format: Date.DATE_FORMAT_SERVER)

        //then
        let boolNeedUse = sut.needUseDateToFilter()

        //then
        XCTAssert(boolNeedUse == true)
    }

    func test_givenADateFromModifiedAndToDateModified_whenICallNeedUseDateToFilter_thenReturnValueIsTrue() {

        //given
        var date = Date.date(fromLocalTimeZoneString: "2017-01-01", withFormat: Date.DATE_FORMAT_SERVER)
        let fromDate = date?.string(format: Date.DATE_FORMAT_SERVER)
        date = Date.date(fromLocalTimeZoneString: "2017-08-12", withFormat: Date.DATE_FORMAT_SERVER)
        let toDate = date?.string(format: Date.DATE_FORMAT_SERVER)

        sut.transactionFilterTransport.fromDateText = fromDate
        sut.transactionFilterTransport.toDateText = toDate

        //then
        let boolNeedUse = sut.needUseDateToFilter()

        //then
        XCTAssert(boolNeedUse == true)
    }

    func test_givenADateFromModifiedAndToDateNotModified_whenICallNeedUseDateToFilter_thenReturnValueIsTrue() {

        //given
        let date = Date.date(fromLocalTimeZoneString: "2017-01-01", withFormat: Date.DATE_FORMAT_SERVER)
        let fromDate = date?.string(format: Date.DATE_FORMAT_SERVER)
        sut.transactionFilterTransport.fromDateText = fromDate
        let toDate: String? = nil
        sut.transactionFilterTransport.toDateText = toDate

        //then
        let boolNeedUse = sut.needUseDateToFilter()

        //then
        XCTAssert(boolNeedUse == true)
    }

    // MARK: TransportFilter Amount

    func test_givenAEmptyFromAmount_whenICallModifiedAmount_thenTheTransportModelFromAmountIsEmpty() {

        //given
        let fromAmountText: String = ""

        //when
        sut.modifiedAmount(withAmountFrom: fromAmountText, withAmountTo: "")

        //then
        XCTAssert(Tools.isStringNilOrEmpty(withString: sut.transactionFilterTransport.fromAmountText) == true)

    }

    func test_givenANilFromAmount_whenICallModifiedAmount_thenTheTransportModelFromAmountIsEmpty() {

        //given
        let fromAmountText: String? = nil

        //when
        sut.modifiedAmount(withAmountFrom: fromAmountText, withAmountTo: "")

        //then
        XCTAssert(Tools.isStringNilOrEmpty(withString: sut.transactionFilterTransport.fromAmountText) == true)

    }

    func test_givenAEmptyToAmount_whenICallModifiedAmount_thenTheTransportModelToAmountIsEmpty() {

        //given
        let toAmountText: String = ""

        //when
        sut.modifiedAmount(withAmountFrom: "", withAmountTo: toAmountText)

        //then
        XCTAssert(Tools.isStringNilOrEmpty(withString: sut.transactionFilterTransport.toAmountText) == true)

    }

    func test_givenANilToAmount_whenICallModifiedAmount_thenTheTransportModelToAmountIsEmpty() {

        //given
        let toAmountText: String? = nil

        //when
        sut.modifiedAmount(withAmountFrom: "", withAmountTo: toAmountText)

        //then
        XCTAssert(Tools.isStringNilOrEmpty(withString: sut.transactionFilterTransport.toAmountText) == true)

    }

    func test_givenAFromAmount_whenICallModifiedAmount_thenTheTransportModelFromAmountIsEqualToReceiveFromAmount() {

        //given
        let fromAmount: String = "10"

        //when
        sut.modifiedAmount(withAmountFrom: fromAmount, withAmountTo: "")

        //then
        XCTAssert(sut.transactionFilterTransport.fromAmountText == "10")

    }

    func test_givenAToAmount_whenICallModifiedAmount_thenTheTransportModelToAmountIsEqualToReceiveToAmount() {

        //given
        let toAmount: String = "10"

        //when
        sut.modifiedAmount(withAmountFrom: "", withAmountTo: toAmount)

        //then
        XCTAssert(sut.transactionFilterTransport.toAmountText == "10")

    }

    func test_givenAEmptyFromAmountInTransportAmountFromText_whenICallAmountFromIsModified_thenReturnFalse() {

        //given
        let fromAmount: String = ""
        sut.transactionFilterTransport.fromAmountText = fromAmount

        //when
        let boolValue = sut.amountFromIsModified()

        //Then
        XCTAssert(boolValue == false)

    }

    func test_givenANilFromAmountInTransportAmountFromText_whenICallAmountFromIsModified_thenReturnFalse() {

        //given
        let fromAmount: String? = nil
        sut.transactionFilterTransport.fromAmountText = fromAmount

        //when
        let boolValue = sut.amountFromIsModified()

        //Then
        XCTAssert(boolValue == false)

    }

    func test_givenAFromAmountInTransportAmountFromText_whenICallAmountFromIsModified_thenReturnTrue() {

        //given
        let fromAmount: String = "10"
        sut.transactionFilterTransport.fromAmountText = fromAmount

        //when
        let boolValue = sut.amountFromIsModified()

        //Then
        XCTAssert(boolValue == true)

    }

    func test_givenAEmptyToAmountInTransportAmountToText_whenICallAmountToIsModified_thenReturnFalse() {

        //given
        let toAmount: String = ""
        sut.transactionFilterTransport.toAmountText = toAmount

        //when
        let boolValue = sut.amountToIsModified()

        //Then
        XCTAssert(boolValue == false)

    }

    func test_givenANilToAmountInTransportAmountToText_whenICallAmountToIsModified_thenReturnFalse() {

        //given
        let toAmount: String? = nil
        sut.transactionFilterTransport.toAmountText = toAmount

        //when
        let boolValue = sut.amountToIsModified()

        //Then
        XCTAssert(boolValue == false)

    }

    func test_givenAToAmountInTransportAmountToText_whenICallAmountToIsModified_thenReturnTrue() {

        //given
        let toAmount: String = "10"
        sut.transactionFilterTransport.toAmountText = toAmount

        //when
        let boolValue = sut.amountToIsModified()

        //Then
        XCTAssert(boolValue == true)

    }

    func test_givenAFromAmountNotModifiedAndToAmountNotModified_whenICallNeedUseAmoutToFilter_theReturnValueFalse() {

        //given
        let fromAmount: String? = nil
        sut.transactionFilterTransport.fromAmountText = fromAmount
        let toAmount: String? = nil
        sut.transactionFilterTransport.toAmountText = toAmount

        //when
        let returnValue = sut.needUseAmoutToFilter()

        //then
        XCTAssert(returnValue == false)

    }

    func test_givenAFromAmountModifiedAndToAmountNotModified_whenICallNeedUseAmoutToFilter_theReturnValueTrue() {

        //given
        let fromAmount: String = "10"
        sut.transactionFilterTransport.fromAmountText = fromAmount
        let toAmount: String? = nil
        sut.transactionFilterTransport.toAmountText = toAmount

        //when
        let returnValue = sut.needUseAmoutToFilter()

        //then
        XCTAssert(returnValue == true)

    }

    func test_givenAFromAmountNotModifiedAndToAmountModified_whenICallNeedUseAmoutToFilter_theReturnValueFalse() {

        //given
        let fromAmount: String = ""
        sut.transactionFilterTransport.fromAmountText = fromAmount
        let toAmount: String = ""
        sut.transactionFilterTransport.toAmountText = toAmount

        //when
        let returnValue = sut.needUseAmoutToFilter()

        //then
        XCTAssert(returnValue == false)

    }

    func test_givenAFromAmountModifiedAndToAmountModified_whenICallNeedUseAmoutToFilter_theReturnValueTrue() {

        //given
        let fromAmount: String = "10"
        sut.transactionFilterTransport.fromAmountText = fromAmount
        let toAmount: String = "10"
        sut.transactionFilterTransport.toAmountText = toAmount

        //when
        let returnValue = sut.needUseAmoutToFilter()

        //then
        XCTAssert(returnValue == true)

    }

    // MARK: TransportFilter Type

    func test_givenAType_whenICallModifiedType_thenTheTransportModelTypeTheSame() {

        //given
        let type: TransactionFilterType = TransactionFilterType.income

        //when
        sut.modifiedType(withFilterType: type)

        //then
        XCTAssert(sut.transactionFilterTransport.filtertype == TransactionFilterType.income)

    }

    func test_givenATypeIncome_whenITypeToIsModified_thenReturnTrue() {

        //given
        let type: TransactionFilterType = TransactionFilterType.income

        //when
        sut.modifiedType(withFilterType: type)
        let returnValue = sut.typeToIsModified()

        //then
        XCTAssert(returnValue == true)

    }

    func test_givenATypeExpenses_whenITypeToIsModified_thenReturnTrue() {

        //given
        let type: TransactionFilterType = TransactionFilterType.expense

        //when
        sut.modifiedType(withFilterType: type)
        let returnValue = sut.typeToIsModified()

        //then
        XCTAssert(returnValue == true)

    }

    func test_givenATypeExpenses_whenITypeToIsModified_thenReturnFalse() {

        //given
        let type: TransactionFilterType = TransactionFilterType.all

        //when
        sut.modifiedType(withFilterType: type)
        let returnValue = sut.typeToIsModified()

        //then
        XCTAssert(returnValue == false)

    }

    // MARK: Filter Button logic

    func test_givenAConcept_whenICallValidateTransportModel_thenCheckCallShowCheckFilterButton() {

        //given
        sut.transactionFilterTransport.conceptText = "asdf"
        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.isCalledShowCheckFilterButtonState == true)

    }

    func test_givenAEmptyConcept_whenICallValidateTransportModel_thenCheckCallShowCheckFilterButtonReturnFalse() {

        //given
        sut.transactionFilterTransport.conceptText = ""
        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.filterButtonState == false)

    }

    func test_givenNotModifiedConceptAndNotModifiedDate_whenICallValidateTransportModel_thenCheckCallShowCheckFilterButtonReturnFalse() {

        //given
        sut.transactionFilterTransport.conceptText = ""
        sut.transactionFilterTransport.toDateText = Date().string(format: Date.DATE_FORMAT_SERVER)

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.filterButtonState == false)

    }

    func test_givenAOConcept_whenICallValidateTransportModel_thenCheckCallShowCheckFilterButtonReturnTrue() {

        //given
        sut.transactionFilterTransport.conceptText = "asdf"
        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.filterButtonState == true)

    }

    func test_givenAConceptAndInvalidDate_whenICallValidateTransportModel_thenCheckCallShowCheckFilterButton() {

        //given
        sut.transactionFilterTransport.conceptText = "asdf"
        sut.transactionFilterTransport.toDateText = Date().string(format: Date.DATE_FORMAT_SERVER).capitalized

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.isCalledShowCheckFilterButtonState == true)

    }

    func test_givenAConceptAndNotModifiedDate_whenICallValidateTransportModel_thenCheckCallShowCheckFilterButtonReturnTrue() {

        //given
        sut.transactionFilterTransport.conceptText = "asdf"
        sut.transactionFilterTransport.toDateText = Date().string(format: Date.DATE_FORMAT_SERVER).capitalized

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.filterButtonState == true)

    }

    func test_givenAConceptAndDate_whenICallValidateTransportModel_thenCheckCallShowCheckFilterButtonReturnTrue() {

        //given
        sut.transactionFilterTransport.conceptText = "asdf"
        let date = Date.date(fromLocalTimeZoneString: "2017-08-12", withFormat: Date.DATE_FORMAT_SERVER)
        sut.transactionFilterTransport.toDateText = date?.string(format: Date.DATE_FORMAT_SERVER)

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.filterButtonState == true)

    }

    func testGivenANotModifiedConceptAndNotModifiedDateAndModifiedFromAmount_whenICallValidateTransportModel_thenCheckCallShowCheckFilterButton() {

        //given
        sut.transactionFilterTransport.conceptText = ""
        sut.transactionFilterTransport.toDateText = Date().string(format: Date.DATE_FORMAT_SERVER).capitalized
        sut.transactionFilterTransport.fromAmountText = "10"

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.isCalledShowCheckFilterButtonState == true)

    }

    func testGivenANotModifiedConceptAndNotModifiedDateAndModifiedFromAmount_whenICallValidateTransportModel_thenCheckCallShowCheckFilterButtonReturnTrue() {

        //given
        sut.transactionFilterTransport.conceptText = ""
        sut.transactionFilterTransport.toDateText = Date().string(format: Date.DATE_FORMAT_SERVER).capitalized
        sut.transactionFilterTransport.fromAmountText = "10"

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.filterButtonState == true)

    }

    func testGivenANotModifiedConceptAndNotModifiedDateAndNotModifiedFromAmount_whenICallValidateTransportModel_thenCheckCallShowCheckFilterButton() {

        //given
        sut.transactionFilterTransport.conceptText = ""
        sut.transactionFilterTransport.toDateText = Date().string(format: Date.DATE_FORMAT_SERVER).capitalized
        sut.transactionFilterTransport.fromAmountText = ""

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.isCalledShowCheckFilterButtonState == true)

    }

    func testGivenANotModifiedConceptAndNotModifiedDateAndNotModifiedFromAmount_whenICallValidateTransportModel_thenCheckCallShowCheckFilterButtonReturnFalse() {

        //given
        sut.transactionFilterTransport.conceptText = ""
        sut.transactionFilterTransport.toDateText = Date().string(format: Date.DATE_FORMAT_SERVER)
        sut.transactionFilterTransport.fromAmountText = ""

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.filterButtonState == false)

    }

    func testGivenANotModifiedConceptAndNotModifiedDateAndModifiedToAmount_whenICallValidateTransportModel_thenCheckCallShowCheckFilterButton() {

        //given
        sut.transactionFilterTransport.conceptText = ""
        sut.transactionFilterTransport.toDateText = Date().string(format: Date.DATE_FORMAT_SERVER).capitalized
        sut.transactionFilterTransport.toAmountText = "10"

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.isCalledShowCheckFilterButtonState == true)

    }

    func testGivenANotModifiedConceptAndNotModifiedDateAndModifiedToAmount_whenICallValidateTransportModel_thenCheckCallShowCheckFilterButtonReturnTrue() {

        //given
        sut.transactionFilterTransport.conceptText = ""
        sut.transactionFilterTransport.toDateText = Date().string(format: Date.DATE_FORMAT_SERVER).capitalized
        sut.transactionFilterTransport.toAmountText = "10"

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.filterButtonState == true)

    }

    func testGivenANotModifiedConceptAndNotModifiedDateAndNotModifiedToAmount_whenICallValidateTransportModel_thenCheckCallShowCheckFilterButton() {

        //given
        sut.transactionFilterTransport.conceptText = ""
        sut.transactionFilterTransport.toDateText = Date().string(format: Date.DATE_FORMAT_SERVER).capitalized
        sut.transactionFilterTransport.toAmountText = ""

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.isCalledShowCheckFilterButtonState == true)

    }

    func testGivenANotModifiedConceptAndNotModifiedDateAndNotModifiedToAmount_whenICallValidateTransportModel_thenCheckCallShowCheckFilterButtonReturnFalse() {

        //given
        sut.transactionFilterTransport.conceptText = ""
        sut.transactionFilterTransport.toDateText = Date().string(format: Date.DATE_FORMAT_SERVER)
        sut.transactionFilterTransport.toAmountText = ""

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.filterButtonState == false)

    }

    func testGivenANotModifiedConceptAndNotModifiedDateAndInvalidAmount_whenICallValidateTransportModel_thenCheckCallShowCheckFilterButtonReturnFalse() {

        //given
        sut.transactionFilterTransport.conceptText = ""
        sut.transactionFilterTransport.toDateText = Date().string(format: Date.DATE_FORMAT_SERVER)
        sut.transactionFilterTransport.fromAmountText = "23"
        sut.transactionFilterTransport.toAmountText = "22"

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.filterButtonState == false)

    }

    func testGivenANotModifiedConceptAndNotModifiedDateAndValidAmount_whenICallValidateTransportModel_thenCheckCallShowCheckFilterButtonReturnTrue() {

        //given
        sut.transactionFilterTransport.conceptText = ""
        sut.transactionFilterTransport.toDateText = Date().string(format: Date.DATE_FORMAT_SERVER).capitalized
        sut.transactionFilterTransport.fromAmountText = "23"
        sut.transactionFilterTransport.toAmountText = "25"

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.filterButtonState == true)

    }

    func testGivenAModifiedConceptAndNotModifiedDateAndValidAmount_whenICallValidateTransportModel_thenCheckCallShowCheckFilterButtonReturnTrue() {

        //given
        sut.transactionFilterTransport.conceptText = "asdf"
        sut.transactionFilterTransport.toDateText = Date().string(format: Date.DATE_FORMAT_SERVER).capitalized
        sut.transactionFilterTransport.fromAmountText = "23"
        sut.transactionFilterTransport.toAmountText = "25"

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.filterButtonState == true)

    }

    func testGivenAModifiedConceptAndModifiedDateAndValidAmount_whenICallValidateTransportModel_thenCheckCallShowCheckFilterButtonReturnTrue() {

        //given
        sut.transactionFilterTransport.conceptText = "asdf"
        let date = Date.date(fromLocalTimeZoneString: "2017-08-12", withFormat: Date.DATE_FORMAT_SERVER)
        sut.transactionFilterTransport.toDateText = date?.string(format: Date.DATE_FORMAT_SERVER)
        sut.transactionFilterTransport.fromAmountText = "23"
        sut.transactionFilterTransport.toAmountText = "25"

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.filterButtonState == true)

    }

    func testGivenAModifiedConceptAndNotModifiedDateAndInvalidAmount_whenICallValidateTransportModel_thenCheckCallShowCheckFilterButtonReturnFalse() {

        //given
        sut.transactionFilterTransport.conceptText = "asdf"
        sut.transactionFilterTransport.fromAmountText = "23"
        sut.transactionFilterTransport.toAmountText = "22"

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.filterButtonState == false)

    }

    func testGivenAModifiedConceptAndModifiedDateAndInvalidAmount_whenICallValidateTransportModel_thenCheckCallShowCheckFilterButtonReturnFalse() {

        //given
        sut.transactionFilterTransport.conceptText = "asdf"
        let date = Date.date(fromLocalTimeZoneString: "2017-08-12", withFormat: Date.DATE_FORMAT_SERVER)
        sut.transactionFilterTransport.toDateText = date?.string(format: Date.DATE_FORMAT_SERVER)
        sut.transactionFilterTransport.fromAmountText = "23"
        sut.transactionFilterTransport.toAmountText = "22"

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.filterButtonState == false)

    }

    func testGivenANotModifiedConceptAndNotModifiedDateAndNotModifiedAmountAndNotModifiedType_whenICallValidateTransportModel_thenIsCalledShowCheckFilterButtonState() {

        //given
        sut.transactionFilterTransport.filtertype = TransactionFilterType.all

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.isCalledShowCheckFilterButtonState == true)

    }

    func testGivenANotModifiedConceptAndNotModifiedDateAndNotModifiedAmountAndNotModifiedType_whenICallValidateTransportModel_thenCheckCallShowCheckFilterButtonReturnFalse() {

        //given
        sut.transactionFilterTransport.filtertype = TransactionFilterType.all

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.filterButtonState == false)

    }

    func testGivenANotModifiedConceptAndNotModifiedDateAndNotModifiedAmountAndModifiedType_whenICallValidateTransportModel_thenCheckCallShowCheckFilterButtonReturnTrue() {

        //given
        sut.transactionFilterTransport.filtertype = TransactionFilterType.income

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.filterButtonState == true)

    }

    func testGivenAModifiedConceptAndNotModifiedDateAndNotModifiedAmountAndModifiedType_whenICallValidateTransportModel_thenCheckCallShowCheckFilterButtonReturnTrue() {

        //given
        sut.transactionFilterTransport.conceptText = "asdf"
        sut.transactionFilterTransport.filtertype = TransactionFilterType.income

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.filterButtonState == true)

    }

    func testGivenAModifiedConceptAndModifiedDateAndNotModifiedAmountAndModifiedType_whenICallValidateTransportModel_thenCheckCallShowCheckFilterButtonReturnTrue() {

        //given
        sut.transactionFilterTransport.conceptText = "asdf"
        let date = Date.date(fromLocalTimeZoneString: "2017-08-12", withFormat: Date.DATE_FORMAT_SERVER)
        sut.transactionFilterTransport.toDateText = date?.string(format: Date.DATE_FORMAT_SERVER)
        sut.transactionFilterTransport.filtertype = TransactionFilterType.income

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.filterButtonState == true)

    }

    func testGivenAModifiedConceptAndModifiedDateAndModifiedAmountAndModifiedType_whenICallValidateTransportModel_thenCheckCallShowCheckFilterButtonReturnTrue() {

        //given
        sut.transactionFilterTransport.conceptText = "asdf"
        let date = Date.date(fromLocalTimeZoneString: "2017-08-12", withFormat: Date.DATE_FORMAT_SERVER)
        sut.transactionFilterTransport.toDateText = date?.string(format: Date.DATE_FORMAT_SERVER)
        sut.transactionFilterTransport.fromAmountText = "21"
        sut.transactionFilterTransport.toAmountText = "22"
        sut.transactionFilterTransport.filtertype = TransactionFilterType.income

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.filterButtonState == true)

    }

    func testGivenAModifiedConceptAndModifiedDateAndInvalidModifiedAmountAndModifiedType_whenICallValidateTransportModel_thenCheckCallShowCheckFilterButtonReturnFalse() {

        //given
        sut.transactionFilterTransport.conceptText = "asdf"
        let date = Date.date(fromLocalTimeZoneString: "2017-08-12", withFormat: Date.DATE_FORMAT_SERVER)
        sut.transactionFilterTransport.toDateText = date?.string(format: Date.DATE_FORMAT_SERVER)
        sut.transactionFilterTransport.fromAmountText = "33"
        sut.transactionFilterTransport.toAmountText = "22"
        sut.transactionFilterTransport.filtertype = TransactionFilterType.income

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.filterButtonState == false)

    }

    func testGivenAModifiedConceptAndNotModifiedModifiedDateAndModifiedAmountAndModifiedType_whenICallValidateTransportModel_thenCheckCallShowCheckFilterButtonReturnTrue() {

        //given
        sut.transactionFilterTransport.conceptText = "asdf"
        sut.transactionFilterTransport.toDateText = Date().string(format: Date.DATE_FORMAT_SERVER).capitalized
        sut.transactionFilterTransport.fromAmountText = "21"
        sut.transactionFilterTransport.toAmountText = "22"
        sut.transactionFilterTransport.filtertype = TransactionFilterType.income

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.filterButtonState == true)

    }

    // MARK: Restore Button logic

    func test_givenAConcept_whenICallValidateTransportModel_thenCheckCallShowCheckRestoreButton() {

        //given
        sut.transactionFilterTransport.conceptText = "asdf"
        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.isCalledShowCheckRestoreButtonState == true)

    }

    func test_givenAEmptyConcept_whenICallValidateTransportModel_thenCheckCallShowCheckRestoreButtonReturnFalse() {

        //given
        sut.transactionFilterTransport.conceptText = ""
        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.restoreButtonState == false)

    }

    func test_givenNotModifiedConceptAndNotModifiedDate_whenICallValidateTransportModel_thenCheckCallShowCheckRestoreButtonReturnFalse() {

        //given
        sut.transactionFilterTransport.conceptText = ""
        sut.transactionFilterTransport.toDateText = Date().string(format: Date.DATE_FORMAT_SERVER)

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.restoreButtonState == false)

    }

    func test_givenAOConcept_whenICallValidateTransportModel_thenCheckCallShowCheckRestoreButtonReturnTrue() {

        //given
        sut.transactionFilterTransport.conceptText = "asdf"
        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.restoreButtonState == true)

    }

    func test_givenAConceptAndInvalidDate_whenICallValidateTransportModel_thenCheckCallShowCheckRestoreButton() {

        //given
        sut.transactionFilterTransport.conceptText = "asdf"
        sut.transactionFilterTransport.toDateText = Date().string(format: Date.DATE_FORMAT_SERVER).capitalized

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.isCalledShowCheckRestoreButtonState == true)

    }

    func test_givenAConceptAndNotModifiedDate_whenICallValidateTransportModel_thenCheckCallShowCheckRestoreButtonReturnTrue() {

        //given
        sut.transactionFilterTransport.conceptText = "asdf"
        sut.transactionFilterTransport.toDateText = Date().string(format: Date.DATE_FORMAT_SERVER).capitalized

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.restoreButtonState == true)

    }

    func test_givenAConceptAndDate_whenICallValidateTransportModel_thenCheckCallShowCheckRestoreButtonReturnTrue() {

        //given
        sut.transactionFilterTransport.conceptText = "asdf"
        let date = Date.date(fromLocalTimeZoneString: "2017-08-12", withFormat: Date.DATE_FORMAT_SERVER)
        sut.transactionFilterTransport.toDateText = date?.string(format: Date.DATE_FORMAT_SERVER)

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.restoreButtonState == true)

    }

    func testGivenANotModifiedConceptAndNotModifiedDateAndModifiedFromAmount_whenICallValidateTransportModel_thenCheckCallShowCheckRestoreButton() {

        //given
        sut.transactionFilterTransport.conceptText = ""
        sut.transactionFilterTransport.toDateText = Date().string(format: Date.DATE_FORMAT_SERVER).capitalized
        sut.transactionFilterTransport.fromAmountText = "10"

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.isCalledShowCheckRestoreButtonState == true)

    }

    func testGivenANotModifiedConceptAndNotModifiedDateAndModifiedFromAmount_whenICallValidateTransportModel_thenCheckCallShowCheckRestoreButtonReturnTrue() {

        //given
        sut.transactionFilterTransport.conceptText = ""
        sut.transactionFilterTransport.toDateText = Date().string(format: Date.DATE_FORMAT_SERVER).capitalized
        sut.transactionFilterTransport.fromAmountText = "10"

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.restoreButtonState == true)

    }

    func testGivenANotModifiedConceptAndNotModifiedDateAndNotModifiedFromAmount_whenICallValidateTransportModel_thenCheckCallShowCheckRestoreButton() {

        //given
        sut.transactionFilterTransport.conceptText = ""
        sut.transactionFilterTransport.toDateText = Date().string(format: Date.DATE_FORMAT_SERVER).capitalized
        sut.transactionFilterTransport.fromAmountText = ""

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.isCalledShowCheckRestoreButtonState == true)

    }

    func testGivenANotModifiedConceptAndNotModifiedDateAndNotModifiedFromAmount_whenICallValidateTransportModel_thenCheckCallShowCheckRestoreButtonReturnFalse() {

        //given
        sut.transactionFilterTransport.conceptText = ""
        sut.transactionFilterTransport.toDateText = Date().string(format: Date.DATE_FORMAT_SERVER)
        sut.transactionFilterTransport.fromAmountText = ""

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.restoreButtonState == false)

    }

    func testGivenANotModifiedConceptAndNotModifiedDateAndModifiedToAmount_whenICallValidateTransportModel_thenCheckCallShowCheckRestoreButton() {

        //given
        sut.transactionFilterTransport.conceptText = ""
        sut.transactionFilterTransport.toDateText = Date().string(format: Date.DATE_FORMAT_SERVER).capitalized
        sut.transactionFilterTransport.toAmountText = "10"

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.isCalledShowCheckRestoreButtonState == true)

    }

    func testGivenANotModifiedConceptAndNotModifiedDateAndModifiedToAmount_whenICallValidateTransportModel_thenCheckCallShowCheckRestoreButtonReturnTrue() {

        //given
        sut.transactionFilterTransport.conceptText = ""
        sut.transactionFilterTransport.toDateText = Date().string(format: Date.DATE_FORMAT_SERVER).capitalized
        sut.transactionFilterTransport.toAmountText = "10"

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.restoreButtonState == true)

    }

    func testGivenANotModifiedConceptAndNotModifiedDateAndNotModifiedToAmount_whenICallValidateTransportModel_thenCheckCallShowCheckRestoreButton() {

        //given
        sut.transactionFilterTransport.conceptText = ""
        sut.transactionFilterTransport.toDateText = Date().string(format: Date.DATE_FORMAT_SERVER).capitalized
        sut.transactionFilterTransport.toAmountText = ""

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.isCalledShowCheckRestoreButtonState == true)

    }

    func testGivenANotModifiedConceptAndNotModifiedDateAndNotModifiedToAmount_whenICallValidateTransportModel_thenCheckCallShowCheckRestoreButtonReturnFalse() {

        //given
        sut.transactionFilterTransport.conceptText = ""
        sut.transactionFilterTransport.toDateText = Date().string(format: Date.DATE_FORMAT_SERVER)
        sut.transactionFilterTransport.toAmountText = ""

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.restoreButtonState == false)

    }

    func testGivenANotModifiedConceptAndNotModifiedDateAndInvalidAmount_whenICallValidateTransportModel_thenCheckCallShowCheckRestoreButtonReturnTrue() {

        //given
        sut.transactionFilterTransport.conceptText = ""
        sut.transactionFilterTransport.toDateText = Date().string(format: Date.DATE_FORMAT_SERVER).capitalized
        sut.transactionFilterTransport.fromAmountText = "23"
        sut.transactionFilterTransport.toAmountText = "22"

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.restoreButtonState == true)

    }

    func testGivenANotModifiedConceptAndNotModifiedDateAndValidAmount_whenICallValidateTransportModel_thenCheckCallShowCheckRestoreButtonReturnTrue() {

        //given
        sut.transactionFilterTransport.conceptText = ""
        sut.transactionFilterTransport.toDateText = Date().string(format: Date.DATE_FORMAT_SERVER).capitalized
        sut.transactionFilterTransport.fromAmountText = "23"
        sut.transactionFilterTransport.toAmountText = "25"

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.restoreButtonState == true)

    }

    func testGivenAModifiedConceptAndNotModifiedDateAndValidAmount_whenICallValidateTransportModel_thenCheckCallShowCheckRestoreButtonReturnTrue() {

        //given
        sut.transactionFilterTransport.conceptText = "asdf"
        sut.transactionFilterTransport.toDateText = Date().string(format: Date.DATE_FORMAT_SERVER).capitalized
        sut.transactionFilterTransport.fromAmountText = "23"
        sut.transactionFilterTransport.toAmountText = "25"

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.restoreButtonState == true)

    }

    func testGivenAModifiedConceptAndModifiedDateAndValidAmount_whenICallValidateTransportModel_thenCheckCallShowCheckRestoreButtonReturnTrue() {

        //given
        sut.transactionFilterTransport.conceptText = "asdf"
        let date = Date.date(fromLocalTimeZoneString: "2017-08-12", withFormat: Date.DATE_FORMAT_SERVER)
        sut.transactionFilterTransport.toDateText = date?.string(format: Date.DATE_FORMAT_SERVER)
        sut.transactionFilterTransport.fromAmountText = "23"
        sut.transactionFilterTransport.toAmountText = "25"

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.restoreButtonState == true)

    }

    func testGivenAModifiedConceptAndNotModifiedDateAndInvalidAmount_whenICallValidateTransportModel_thenCheckCallShowCheckRestoreButtonReturnTrue() {

        //given
        sut.transactionFilterTransport.conceptText = "asdf"
        sut.transactionFilterTransport.toDateText = Date().string(format: Date.DATE_FORMAT_SERVER).capitalized
        sut.transactionFilterTransport.fromAmountText = "23"
        sut.transactionFilterTransport.toAmountText = "22"

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.restoreButtonState == true)

    }

    func testGivenAModifiedConceptAndModifiedDateAndInvalidAmount_whenICallValidateTransportModel_thenCheckCallShowCheckRestoreButtonReturnTrue() {

        //given
        sut.transactionFilterTransport.conceptText = "asdf"
        let date = Date.date(fromLocalTimeZoneString: "2017-08-12", withFormat: Date.DATE_FORMAT_SERVER)
        sut.transactionFilterTransport.toDateText = date?.string(format: Date.DATE_FORMAT_SERVER)
        sut.transactionFilterTransport.fromAmountText = "23"
        sut.transactionFilterTransport.toAmountText = "22"

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.restoreButtonState == true)

    }

    func testGivenANotModifiedConceptAndNotModifiedDateAndNotModifiedAmountAndNotModifiedType_whenICallValidateTransportModel_thenIsCalledShowCheckRestoreButtonState() {

        //given
        sut.transactionFilterTransport.filtertype = TransactionFilterType.all

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.isCalledShowCheckRestoreButtonState == true)

    }

    func testGivenANotModifiedConceptAndNotModifiedDateAndNotModifiedAmountAndNotModifiedType_whenICallValidateTransportModel_thenCheckCallShowCheckRestoreButtonReturnFalse() {

        //given
        sut.transactionFilterTransport.filtertype = TransactionFilterType.all

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.restoreButtonState == false)

    }

    func testGivenANotModifiedConceptAndNotModifiedDateAndNotModifiedAmountAndModifiedType_whenICallValidateTransportModel_thenCheckCallShowCheckRestoreButtonReturnTrue() {

        //given
        sut.transactionFilterTransport.filtertype = TransactionFilterType.income

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.restoreButtonState == true)

    }

    func testGivenAModifiedConceptAndNotModifiedDateAndNotModifiedAmountAndModifiedType_whenICallValidateTransportModel_thenCheckCallShowCheckRestoreButtonReturnTrue() {

        //given
        sut.transactionFilterTransport.conceptText = "asdf"
        sut.transactionFilterTransport.filtertype = TransactionFilterType.income

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.restoreButtonState == true)

    }

    func testGivenAModifiedConceptAndModifiedDateAndNotModifiedAmountAndModifiedType_whenICallValidateTransportModel_thenCheckCallShowCheckRestoreButtonReturnTrue() {

        //given
        sut.transactionFilterTransport.conceptText = "asdf"
        let date = Date.date(fromLocalTimeZoneString: "2017-08-12", withFormat: Date.DATE_FORMAT_SERVER)
        sut.transactionFilterTransport.toDateText = date?.string(format: Date.DATE_FORMAT_SERVER)
        sut.transactionFilterTransport.filtertype = TransactionFilterType.income

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.restoreButtonState == true)

    }

    func testGivenAModifiedConceptAndModifiedDateAndModifiedAmountAndModifiedType_whenICallValidateTransportModel_thenCheckCallShowCheckRestoreButtonReturnTrue() {

        //given
        sut.transactionFilterTransport.conceptText = "asdf"
        let date = Date.date(fromLocalTimeZoneString: "2017-08-12", withFormat: Date.DATE_FORMAT_SERVER)
        sut.transactionFilterTransport.toDateText = date?.string(format: Date.DATE_FORMAT_SERVER)
        sut.transactionFilterTransport.fromAmountText = "21"
        sut.transactionFilterTransport.toAmountText = "22"
        sut.transactionFilterTransport.filtertype = TransactionFilterType.income

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.restoreButtonState == true)

    }

    func testGivenAModifiedConceptAndModifiedDateAndInvalidModifiedAmountAndModifiedType_whenICallValidateTransportModel_thenCheckCallShowCheckRestoreButtonReturnTrue() {

        //given
        sut.transactionFilterTransport.conceptText = "asdf"
        let date = Date.date(fromLocalTimeZoneString: "2017-08-12", withFormat: Date.DATE_FORMAT_SERVER)
        sut.transactionFilterTransport.toDateText = date?.string(format: Date.DATE_FORMAT_SERVER)
        sut.transactionFilterTransport.fromAmountText = "33"
        sut.transactionFilterTransport.toAmountText = "22"
        sut.transactionFilterTransport.filtertype = TransactionFilterType.income

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.restoreButtonState == true)

    }

    func testGivenAModifiedConceptAndNotModifiedModifiedDateAndModifiedAmountAndModifiedType_whenICallValidateTransportModel_thenCheckCallShowCheckRestoreButtonReturnTrue() {

        //given
        sut.transactionFilterTransport.conceptText = "asdf"
        sut.transactionFilterTransport.toDateText = Date().string(format: Date.DATE_FORMAT_SERVER)
        sut.transactionFilterTransport.fromAmountText = "21"
        sut.transactionFilterTransport.toAmountText = "22"
        sut.transactionFilterTransport.filtertype = TransactionFilterType.income

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.validateTransportModel()

        //then
        XCTAssert(dummyView.restoreButtonState == true)

    }

    func testGivenAView_whenICallRestoreAllValues_thenCheckCallisCalledRemoveConcet() {

        //given
        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.restoreAllValues()

        //then
        XCTAssert(dummyView.isCalledRemoveConcet == true)
    }

    func testGivenAView_whenICallRestoreAllValues_thenCheckCallisCalledConfigureDateFilter() {

        //given
        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.restoreAllValues()

        //then
        XCTAssert(dummyView.isCalledConfigureDateFilter == true)

    }

    func testGivenAView_whenICallRestoreAllValues_thenCheckCallisCalledRestoreTypeFilterValues() {

        //given
        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.restoreAllValues()

        //then
        XCTAssert(dummyView.isCalledRestoreTypeFilterValues == true)

    }

    func testGivenAView_whenICallRestoreAllValues_thenCheckCallisCalledRestoreAmountValues() {

        //given
        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.restoreAllValues()

        //then
        XCTAssert(dummyView.isCalledRestoreAmountValues == true)

    }

    func testGivenAView_whenICallRestoreAllValues_thenTranposrtConceptIsEmpty() {

        //given
        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.restoreAllValues()

        //then
        XCTAssert(Tools.isStringNilOrEmpty(withString: sut.transactionFilterTransport.conceptText))

    }

    func testGivenAView_whenICallRestoreAllValues_thenTranposrtConceptFromDateIsEmpty() {

        //given
        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.restoreAllValues()

        //then
        XCTAssert(Tools.isStringNilOrEmpty(withString: sut.transactionFilterTransport.fromDateText))

    }

    func testGivenAView_whenICallRestoreAllValues_thenTranposrtConceptToDateIsEmpty() {

        //given
        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.restoreAllValues()

        //then
        XCTAssert(Tools.isStringNilOrEmpty(withString: sut.transactionFilterTransport.toDateText))

    }

    func testGivenAView_whenICallRestoreAllValues_thenTranposrtConceptFromAmountIsEmpty() {

        //given
        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.restoreAllValues()

        //then
        XCTAssert(Tools.isStringNilOrEmpty(withString: sut.transactionFilterTransport.fromAmountText))

    }

    func testGivenAView_whenICallRestoreAllValues_thenTranposrtConceptToAmountIsEmpty() {

        //given
        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.restoreAllValues()

        //then
        XCTAssert(Tools.isStringNilOrEmpty(withString: sut.transactionFilterTransport.toAmountText))

    }

    func testGivenAView_whenICallRestoreAllValues_thenTranposrtConceptTypeIsAll() {

        //given
        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.restoreAllValues()

        //then
        XCTAssert(sut.transactionFilterTransport.filtertype == TransactionFilterType.all)

    }

    func test_givenARouterAndShouldNavigateToFilterTrue_whenICallFilterMovementens_thenCallRouterPresentTransactionsWithTransportModel() {

        // given
        let transactionFilterTransport = TransactionFilterTransport()
        sut.shouldNavigateToFilter = true

        // when
        sut.transactionFilterTransport = transactionFilterTransport
        sut.filterMovementens()

        // then
        XCTAssert(dummyBusManager.isCalledNavigateWithTagTransactionFilterAndEventTransactionFilterBo == true)
        XCTAssert(dummyBusManager.saveTransactionFilterTranspor === transactionFilterTransport)

    }

    func test_givenARouterAndShouldNavigateToFilterFalse_whenICallFilterMovementens_thenCallPublishDataWithTransportModel() {

        // given
        let transactionFilterTransport = TransactionFilterTransport()

        sut.shouldNavigateToFilter = false

        // when
        sut.transactionFilterTransport = transactionFilterTransport
        sut.filterMovementens()

        // then
        XCTAssert(dummyBusManager.isCalledPublishDataToReloadTransactionList == true)
        XCTAssert(dummyBusManager.saveTransactionFilterTranspor === transactionFilterTransport)

    }

    func test_givenARouterAndShouldNavigateToFilterFalse_whenICallFilterMovementens_thenCallRouterToDismiss() {

        // given
        let transactionFilterTransport = TransactionFilterTransport()

        sut.shouldNavigateToFilter = false

        // when
        sut.transactionFilterTransport = transactionFilterTransport
        sut.filterMovementens()

        // then
        XCTAssert(dummyBusManager.isCalledDismissViewController == true)

    }

    func test_givenACardBO_whenICallFilterMovementens_thenCallRouterPresentTransactionsWithTransportModelAndCardBOShouldMatch() {

        // given
        let dummyCardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = dummyCardBO

        // when
        sut.filterMovementens()

        // then
        XCTAssert(dummyBusManager.saveTransactionFilterTranspor.cardBO === dummyCardBO)

    }

    func test_givenARouterAndShouldNavigateToFilterFalse_whenICallFilterMovementens_thenCallPublishDataWithFilterTransport() {

        // given
        let transactionFilterTransport = TransactionFilterTransport()

        // when
        sut.transactionFilterTransport = transactionFilterTransport
        sut.filterMovementens()

        // then
        XCTAssert(dummyBusManager.isCalledPublishDataToReloadTransactionList == true)
        XCTAssert(dummyBusManager.saveTransactionFilterTranspor === transactionFilterTransport)

    }

    func test_givenARouterAndShouldNavigateToFilterFalse_whenICallFilterMovementens_thenCallRouterToDismissViewController() {

        // given
        let transactionFilterTransport = TransactionFilterTransport()

        // when
        sut.transactionFilterTransport = transactionFilterTransport
        sut.filterMovementens()

        // then
        XCTAssert(dummyBusManager.isCalledDismissViewController == true)

    }

    // MARK: - On message

    func test_givenAPropertyNavigationFlagAndNavigationFlagParamAsTrue_whenICallOnMessage_thenShouldNavigateToFilterShouldHaveTheSameValue() {

        // given
        let navigationFlag = true
        let cardBO = CardsGenerator.getCardBOCreditCard().cards[0]
        let cardsTransport = CardsTransport(cardBO: cardBO, flag: navigationFlag)

        // when
        sut.setModel(model: cardsTransport)

        // then
        XCTAssert(sut.shouldNavigateToFilter == navigationFlag)

    }

    func test_givenAPropertyNavigationFlagAndNavigationFlagParamAsFalse_whenICallOnMessage_thenShouldNavigateToFilterShouldHaveTheSameValue() {

        // given
        let navigationFlag = false
        let cardBO = CardsGenerator.getCardBOCreditCard().cards[0]
        let cardsTransport = CardsTransport(cardBO: cardBO, flag: navigationFlag)

        // when
        sut.setModel(model: cardsTransport)

        // then
        XCTAssert(sut.shouldNavigateToFilter == navigationFlag)

    }

    func test_givenACardBOPropertyAndCardBOParam_whenICallOnMessage_thenCardBOShouldMatchAnd() {

        // given
        let cardBO = CardsGenerator.getCardBOCreditCard().cards[0]
        let cardsTransport = CardsTransport(cardBO: cardBO, flag: false)
        // when
        sut.setModel(model: cardsTransport)

        // then
        XCTAssert(sut.cardBO! === cardBO)

    }

    // MARK: DummyData
    class DummyView: TransactionsFilterViewProtocol {

        var isCalledConfigureDateFilter: Bool = false
        var isCalledConfigureAmountFilter: Bool = false
        var isCalledShowCheckFilterButtonState: Bool = false
        var isCalledShowCheckRestoreButtonState: Bool = false
        var isCalledRemoveConcet: Bool = false
        var isCalledRestoreTypeFilterValues: Bool = false
        var isCalledRestoreAmountValues: Bool = false
        var isCalledHideConceptFilter: Bool = false
        var isCalledConfigureConceptFilter = false

        var isSameDate: Bool = false
        var restoreButtonState: Bool = false
        var filterButtonState: Bool = false

        var isCalledSendScreenOmniture = false
        var conceptPlaceHolderSent = ""

        func showError(error: ModelBO) {
        }

        func changeColorEditTexts() {
        }

        func configureDateFilter(withDate date: Date) {
            if date.string(format: Date.DATE_FORMAT_DAY_MONTH_YEAR).capitalized == Date().string(format: Date.DATE_FORMAT_DAY_MONTH_YEAR).capitalized {
                isSameDate = true
            }
            isCalledConfigureDateFilter = true
        }

        func configureAmountFilter() {
            isCalledConfigureAmountFilter = true
        }

        func showCheckFilterButton(withState state: Bool) {
            isCalledShowCheckFilterButtonState = true
            filterButtonState = state
        }

        func showCheckRestoreButton(withState state: Bool) {
            isCalledShowCheckRestoreButtonState = true
            restoreButtonState = state
        }

        func removeConcept() {
            isCalledRemoveConcet = true
        }

        func restoreTypeFilterValues() {
            isCalledRestoreTypeFilterValues = true
        }

        func restoreAmountValues() {
            isCalledRestoreAmountValues = true
        }

        func sendOmnitureScreen() {
            isCalledSendScreenOmniture = true
        }

        func hideConceptFilter() {
            isCalledHideConceptFilter = true
        }

        func configureConceptFilter(withPlaceHolderTitle placeHolderTitle: String) {
            isCalledConfigureConceptFilter = true
            conceptPlaceHolderSent = placeHolderTitle
        }
    }

    class DummyBusManager: BusManager {

        var saveTransactionFilterTranspor = TransactionFilterTransport()
        var isCalledNavigateWithTagTransactionFilterAndEventTransactionFilterBo: Bool = false
        var isCalledPopToViewController: Bool = false
        var isCalledPublishDataToReloadTransactionList = false
        var isCalledDismissViewController = false

        override init() {
            super.init()
            routerFactory = RouterInitFactory()
        }

        override func navigateScreen<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {
            if tag == TransactionsFilterPageReaction.ROUTER_TAG && event === TransactionsFilterPageReaction.EVENT_NAV_TO_NEXT_SCREEN {
                saveTransactionFilterTranspor = values as! TransactionFilterTransport
                isCalledNavigateWithTagTransactionFilterAndEventTransactionFilterBo = true

            }
        }

        override func publishData<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {
            if tag == TransactionsFilterPageReaction.ROUTER_TAG && event === TransactionsFilterPageReaction.EVENT_RELOAD_TRANSACTION_LIST {
                isCalledPublishDataToReloadTransactionList = true
                saveTransactionFilterTranspor = values as! TransactionFilterTransport
            }
        }

        override func dismissViewController(animated: Bool, completion: (() -> Void)?) {
            if let completion = completion {
                completion()
            }
            isCalledDismissViewController = true
        }

    }

}
