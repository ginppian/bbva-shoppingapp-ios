//
//  NotRecognizedOperationPresenterTests.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 18/7/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest
import CellsNativeComponents

#if TESTMX
@testable import shoppingappMX
#elseif TESTPE
@testable import shoppingappPE
#endif

class NotRecognizedOperationPresenterTests: XCTestCase {

    var sut: NotRecognizedOperationPresenter<NotRecognizedOperationViewProtocolDummy>!
    var dummyBusManager: DummyBusManager!
    var dummyView: NotRecognizedOperationViewProtocolDummy!
    var dummySessionManager: DummySessionManager!

    override func setUp() {
        super.setUp()
        dummyBusManager = DummyBusManager()
        sut = NotRecognizedOperationPresenter<NotRecognizedOperationViewProtocolDummy>(busManager: dummyBusManager)

        dummyView = NotRecognizedOperationViewProtocolDummy()
        sut.view = dummyView
        
        DummySessionManager.setupMock()
        dummySessionManager = (SessionDataManager.sessionDataInstance() as! DummySessionManager)
    }

    // MARK: - setModel

    func test_givenErrorBO_whenICallSetModel_thenErrorBOBeFilled() {

        // given
        let errorBO = ErrorBO(error: ErrorEntity(message: "Error"))

        // when
        sut.setModel(model: errorBO)

        // then
        XCTAssert(sut.errorBO === errorBO)

    }

    func test_givenCardBlockObjectCells_whenICallSetModel_thenBlockCardObjectCellsBeFilled() {

        // given
        let blockCardObjectCells = RequestBlockCardObjectCells(id: "id", typeBlock: "STOLEN")

        // when
        sut.setModel(model: blockCardObjectCells)

        // then
        XCTAssert(sut.blockCardObjectCells?.id == blockCardObjectCells.id)
        XCTAssert(sut.blockCardObjectCells?.typeBlock == blockCardObjectCells.typeBlock)

    }

    func test_givenCardOnOffObjectCells_whenICallSetModel_thenCardOnOffObjectCellsBeFilled() {

        // given
        let onOffObjectCells = OnOffCardObjectCells(id: "id", enabled: true)

        // when
        sut.setModel(model: onOffObjectCells)

        // then
        XCTAssert(sut.cardOnOffObjectCells?.id == onOffObjectCells.id)
        XCTAssert(sut.cardOnOffObjectCells?.enabled == onOffObjectCells.enabled)

    }

    // MARK: - showPageSuccess

    func test_givenBlockCardObjectCellsAndBlockCardDTO_whenICallShowPageSuccess_thenICalledNavigateToBlockCardSuccessPage() {

        //given
        let blockCardObjectCells = RequestBlockCardObjectCells(id: "id", typeBlock: "STOLEN")
        sut.blockCardObjectCells = blockCardObjectCells

        let blockCardDTO = BlockCardDTO()
        blockCardDTO.data = BlockCardData()
        blockCardDTO.data?.blockId = "id"
        blockCardDTO.data?.reference = "1234"
        let blockCardBO = BlockCardBO(blockCardDTO: blockCardDTO)

        let cardsBO = CardsGenerator.getThreeCards()
        sut.cardBO = cardsBO.cards[0]

        //when
        sut.showPageSuccess(modelBO: blockCardBO)

        //then
        XCTAssert(dummyBusManager.isCalledNavigateToBlockCardSuccessPage == true)

    }

    func test_givenBlockCardObjectCellsAndBlockCardDTO_whenICallShowPageSuccess_thenICalledPublishParamToBlockCardSuccessPage() {

        //given
        let blockCardObjectCells = RequestBlockCardObjectCells(id: "id", typeBlock: "STOLEN")
        sut.blockCardObjectCells = blockCardObjectCells

        let blockCardDTO = BlockCardDTO()
        blockCardDTO.data = BlockCardData()
        blockCardDTO.data?.blockId = "id"
        blockCardDTO.data?.reference = "1234"
        let blockCardBO = BlockCardBO(blockCardDTO: blockCardDTO)

        let cardsBO = CardsGenerator.getThreeCards()
        sut.cardBO = cardsBO.cards[0]

        //when
        sut.showPageSuccess(modelBO: blockCardBO)

        //then
        XCTAssert(dummyBusManager.isCalledPublishParamBlockCardSuccess == true)

    }

    func test_givenBlockCardObjectCellsAndBlockCardDTO_whenICallShowPageSuccess_thenPublishParamMatch() {

        //given
        let blockCardObjectCells = RequestBlockCardObjectCells(id: "id", typeBlock: "STOLEN")
        sut.blockCardObjectCells = blockCardObjectCells

        let blockCardDTO = BlockCardDTO()
        blockCardDTO.data = BlockCardData()
        blockCardDTO.data?.blockId = "id"
        blockCardDTO.data?.reference = "1234"
        blockCardDTO.data?.blockDate = "2018-07-18T06:52:04.695Z"
        let blockCardBO = BlockCardBO(blockCardDTO: blockCardDTO)

        let date = Date.getDateFormatForCellsModule(withDate: blockCardDTO.data?.blockDate ?? "")

        let cardsBO = CardsGenerator.getThreeCards()
        sut.cardBO = cardsBO.cards[0]

        //when
        sut.showPageSuccess(modelBO: blockCardBO)

        //then
        XCTAssert(dummyBusManager.blockCardSuccessSent.cardNumber == cardsBO.cards[0].number.suffix(4))
        XCTAssert(dummyBusManager.blockCardSuccessSent.folio == blockCardDTO.data?.reference)
        XCTAssert(dummyBusManager.blockCardSuccessSent.date == date)

    }
    func test_givenOnOffCardResponseDTOAndCardBO_whenICallShowPageSuccess_thenIPublishedCorrectParamsAndChangeCardStatus() {

        //given
        let cardsBO = CardsGenerator.getThreeCards()

        let cardOnOffResponseDTO = OnOffResponseDTO()
        cardOnOffResponseDTO.data = OnOffResponse()
        cardOnOffResponseDTO.data?.activationId = "id"
        cardOnOffResponseDTO.data?.name = "name"
        cardOnOffResponseDTO.data?.isActive = true
        let onOffResponseBO = OnOffResponseBO(onOffResponseDTO: cardOnOffResponseDTO)

        sut.cardBO = cardsBO.cards[0]

        //when
        sut.showPageSuccess(modelBO: onOffResponseBO)

        //then
        XCTAssert(dummyBusManager.isCalledPublishDataParamsOnOff == true)
        XCTAssert(dummyBusManager.cardModel.cardBO.activations[0].isActive == cardOnOffResponseDTO.data?.isActive)

    }

    func test_givenOnOffCardResponseDTOAndCardBO_whenICallShowPageSuccess_thenIShowToast() {

        //given
        let cardsBO = CardsGenerator.getThreeCards()

        let cardOnOffResponseDTO = OnOffResponseDTO()
        cardOnOffResponseDTO.data = OnOffResponse()
        cardOnOffResponseDTO.data?.activationId = "id"
        cardOnOffResponseDTO.data?.name = "name"
        cardOnOffResponseDTO.data?.isActive = true
        let onOffResponseBO = OnOffResponseBO(onOffResponseDTO: cardOnOffResponseDTO)

        sut.cardBO = cardsBO.cards[0]

        //when
        sut.showPageSuccess(modelBO: onOffResponseBO)

        //then
        XCTAssert(dummyView.isCalledShowToast)
        XCTAssert(dummyView.showToastColor == .BBVADARKGREEN)

    }

    func test_givenOnOffCardResponseDTOAndCardBOAndCardIsActive_whenICallShowPageSuccess_thenIShowToastWithCorrectMessage() {

        //given
        let cardsBO = CardsGenerator.getThreeCards()

        let cardOnOffResponseDTO = OnOffResponseDTO()
        cardOnOffResponseDTO.data = OnOffResponse()
        cardOnOffResponseDTO.data?.activationId = "id"
        cardOnOffResponseDTO.data?.name = "name"
        cardOnOffResponseDTO.data?.isActive = true
        let onOffResponseBO = OnOffResponseBO(onOffResponseDTO: cardOnOffResponseDTO)

        sut.cardBO = cardsBO.cards[0]

        //when
        sut.showPageSuccess(modelBO: onOffResponseBO)

        //then
        XCTAssert(dummyView.isCalledShowToast)
        XCTAssert(dummyView.showToastMessage == Localizables.cards.key_cards_on_successful_text)

    }

    func test_givenOnOffCardResponseDTOAndCardBOAndCardIsNotActive_whenICallShowPageSuccess_thenIShowToastWithCorrectMessage() {

        //given
        let cardsBO = CardsGenerator.getThreeCards()

        let cardOnOffResponseDTO = OnOffResponseDTO()
        cardOnOffResponseDTO.data = OnOffResponse()
        cardOnOffResponseDTO.data?.activationId = "id"
        cardOnOffResponseDTO.data?.name = "name"
        cardOnOffResponseDTO.data?.isActive = false
        let onOffResponseBO = OnOffResponseBO(onOffResponseDTO: cardOnOffResponseDTO)

        sut.cardBO = cardsBO.cards[0]

        //when
        sut.showPageSuccess(modelBO: onOffResponseBO)

        //then
        XCTAssert(dummyView.isCalledShowToast)
        XCTAssert(dummyView.showToastMessage == Localizables.cards.key_cards_off_successful_text)

    }

    func test_givenAnErrorBOAndCardBOWithErrorStatusDifferentThan401AndErrorCodeDifferentThan70_whenICallOnOffFailed_thenICallPublishDataParamsOnOffWithInitialStatus() {

        // given
        let cardsBO = CardsGenerator.getThreeCards()

        let errorBO = ErrorBO()
        errorBO.status = 500
        errorBO.code = "80"

        sut.cardBO = cardsBO.cards[0]

        // when
        sut.onOffFailed(error: errorBO)

        // then
        XCTAssert(dummyBusManager.isCalledPublishDataParamsOnOffWithInitialState == true)
        XCTAssert(dummyBusManager.onOffTransport.enabled == cardsBO.cards[0].activations[0].isActive)
        XCTAssert(dummyView.isCalledShowError == true)

    }
    func test_givenAnErrorBOAndCardBOWithErrorStatus401_whenICallOnOffFailed_thenIDontShowErrorModalAndPublishInitialStatus() {

        // given
        let cardsBO = CardsGenerator.getThreeCards()

        let errorBO = ErrorBO()
        errorBO.status = 401
        errorBO.code = "70"

        sut.cardBO = cardsBO.cards[0]

        // when
        sut.onOffFailed(error: errorBO)

        // then
        XCTAssert(dummyView.isCalledShowError == false)
        XCTAssert(dummyBusManager.isCalledPublishDataParamsOnOffWithInitialState == true)
        XCTAssert(dummyBusManager.onOffTransport.enabled == cardsBO.cards[0].activations[0].isActive)

    }
    // MARK: - registerOmniture

    func test_givenPage_whenICallRegisterOmniture_thenICallViewSendTrackCellsPageNotRecognizedOperation() {

        // given
        let page = NotRecognizedOperationPageReaction.CELLS_PAGE_NOT_RECOGNIZED_OPERATION

        // when
        sut.registerOmniture(page: page)

        // then
        XCTAssert(dummyView.isCalledSendTrackCellsPageNotRecognizedOperation == true)

    }

    func test_givenPage_whenICallRegisterOmniture_thenICallViewSendTrackCellsPageCardBlockInfo() {

        // given
        let page = CardBlockPageReaction.CELLS_PAGE_CARD_BLOCK_INFO

        // when
        sut.registerOmniture(page: page)

        // then
        XCTAssert(dummyView.isCalledSendTrackCellsPageCardBlockInfo == true)

    }

    func test_givenPage_whenICallRegisterOmniture_thenICallViewSendTrackCellsCardBlockType() {

        // given
        let page = CardBlockPageReaction.CELLS_PAGE_CARD_BLOCK_TYPE

        // when
        sut.registerOmniture(page: page)

        // then
        XCTAssert(dummyView.isCalledSendTrackCellsPageCardBlockType == true)

    }

    func test_givenPage_whenICallRegisterOmniture_thenICallViewSendTrackCellsPageCardBlockSuccess() {

        // given
        let page = CardBlockPageReaction.CELLS_PAGE_BLOCK_CARD_SUCCESS

        // when
        sut.registerOmniture(page: page)

        // then
        XCTAssert(dummyView.isCalledSendTrackCellsPageCardBlockSuccess == true)

    }

    // MARK: - CurrentCardBO

    func test_givenAny_whenICallSetCurrentCardBO_thenCardBOMustBeFilled() {

        //given
        let cardBO = CardsGenerator.getThreeCards().cards[0]

        //when
        sut.setCurrentCard(card: cardBO)

        //then
        XCTAssert(sut.cardBO?.cardId == cardBO.cardId)

    }

    func test_givenCardBO_whenICallGetCurrentCardBO_thenCardBOMustReturnedTheCard() {

        //given
        let cardBO = CardsGenerator.getThreeCards().cards[0]
        sut.cardBO = cardBO

        //when
        let returnedCardBO = sut.getCurrentCard()

        //then
        XCTAssert(sut.cardBO?.cardId == returnedCardBO?.cardId)

    }

    // MARK: - navigateToWallet

    func test_givenNotSession_whenICallNavigateToWallet_thenICallBusManagerSetRootWhenNoSession() {

        //given
        dummySessionManager.isUserLogged = false

        //when
        sut.navigateToWallet()

        //then
        XCTAssert(dummyBusManager.isCalledSetRootWhenNoSession)

    }

    func test_givenSession_whenICallNavigateToWallet_thenICallBusManagerSetRootWhenNoSession() {

        //given
        dummySessionManager.isUserLogged = true

        //when
        sut.navigateToWallet()

        //then
        XCTAssert(dummyBusManager.isCalledNavigateToRootCards == true)

    }

    class NotRecognizedOperationViewProtocolDummy: NotRecognizedOperationViewProtocol {

        var isCalledSendTrackCellsPageNotRecognizedOperation = false
        var isCalledSendTrackCellsPageCardBlockInfo = false
        var isCalledSendTrackCellsPageCardBlockType = false
        var isCalledSendTrackCellsPageCardBlockSuccess = false
        var isCalledShowError = false
        var isCalledShowToast = false
        var showToastMessage: String?
        var showToastColor: UIColor?

        func showError(error: ModelBO) {
            isCalledShowError = true
        }

        func changeColorEditTexts() {
        }

        func sendTrackCellsPageNotRecognizedOperation() {
            isCalledSendTrackCellsPageNotRecognizedOperation = true
        }

        func sendTrackCellsPageCardBlockInfo() {
            isCalledSendTrackCellsPageCardBlockInfo = true
        }

        func sendTrackCellsPageCardBlockType() {
            isCalledSendTrackCellsPageCardBlockType = true
        }

        func sendTrackCellsPageCardBlockSuccess() {
            isCalledSendTrackCellsPageCardBlockSuccess = true
        }

        func showToastSuccess(withSuccessMessage message: String, andBackgroundColor color: UIColor) {
            isCalledShowToast = true
            showToastMessage = message
            showToastColor = color
        }

    }

    class DummyBusManager: BusManager {

        var isCalledNavigateToBlockCardSuccessPage = false
        var isCalledPublishParamBlockCardSuccess = false
        var isCalledSetRootWhenNoSession = true
        var isCalledNavigateToRootCards = false

        var blockCardSuccessSent: BlockCardSuccess!

        var isCalledPublishDataParamsOnOff = false
        var cardModel: CardModel!
        var onOffTransport: OnOffCardTransport!

        var isCalledPublishDataParamsOnOffWithInitialState = false

        override init() {
            super.init()
            routerFactory = RouterInitFactory()
        }

        override func navigateScreen<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {

            if tag == NotRecognizedOperationPageReaction.ROUTER_TAG && event === NotRecognizedOperationPageReaction.EVENT_BLOCK_CARD_SUCCESS {
                isCalledNavigateToBlockCardSuccessPage = true
            }
        }

        override func publishData<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {

            if tag == NotRecognizedOperationPageReaction.ROUTER_TAG && event === NotRecognizedOperationPageReaction.PARAMS_BLOCK_CARD_SUCCESS {
                isCalledPublishParamBlockCardSuccess = true
                blockCardSuccessSent = BlockCardSuccess.deserialize(from: values as? [String: Any])
            } else if tag == NotRecognizedOperationPageReaction.ROUTER_TAG && event === CardsPageReaction.EVENT_CARD_UPDATED {
                isCalledPublishDataParamsOnOff = true
                cardModel = (values as! CardModel)
            } else if tag == TransactionDetailPageReaction.ROUTER_TAG && event === TransactionDetailPageReaction.PARAMS_ON_OFF_CARD {
                isCalledPublishDataParamsOnOffWithInitialState = true
                onOffTransport = OnOffCardTransport.deserialize(from: values as? [String: Any])
            }
        }

        override func setRootWhenNoSession() {

            isCalledSetRootWhenNoSession = true
        }

        override func notify<T>(tag: String, _ event: ActionSpec<T>) {

            if tag == NotRecognizedOperationPageReaction.ROUTER_TAG && event === NotRecognizedOperationPageReaction.EVENT_NAV_TO_ROOT_CARDS {

                isCalledNavigateToRootCards = true
            }
        }
    }

}
