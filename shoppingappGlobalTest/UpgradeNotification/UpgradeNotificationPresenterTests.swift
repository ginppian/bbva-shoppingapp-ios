//
//  UpgradeNotificationPresenterTests.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 8/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest
import CellsNativeComponents

#if TESTMX
@testable import shoppingappMX
#elseif TESTPE
@testable import shoppingappPE
#endif

class UpgradeNotificationPresenterTests: XCTestCase {
    
    var sut: UpgradeNotificationPresenter<UpgradeNotificationViewProtocolDummy>!
    var dummyBusManager: DummyBusManager!
    var dummyView: UpgradeNotificationViewProtocolDummy!
    var dummyVersionControlManager: DummyVersionControlManager!
    
    override func setUp() {
        super.setUp()
        
        dummyBusManager = DummyBusManager()
        sut = UpgradeNotificationPresenter<UpgradeNotificationViewProtocolDummy>(busManager: dummyBusManager)
        
        dummyView = UpgradeNotificationViewProtocolDummy()
        sut.view = dummyView
        
        DummyVersionControlManager.setupMock()
        dummyVersionControlManager = (VersionControlManager.sharedInstance() as! DummyVersionControlManager)
    }
    
    // MARK: - setModel
    
    func test_givenUpgradeNotificationTransport_whenICallSetModel_thenPropertiesFilled() {
        
        // given
        let transport = UpgradeNotificationTransport(false, "url", "message")
        
        // when
        sut.setModel(model: transport)
        
        // then
        XCTAssert(sut.isMandatory == transport.isMandatory)
        XCTAssert(sut.url == transport.url)
        XCTAssert(sut.message == transport.message)
    }
    
    // MARK: - viewLoaded
    
    func test_givenNothing_whenICalledViewLoaded_thenINotCallShowUpdates() {
        
        // given
        
        // when
        sut.viewLoaded()
        
        // then
        XCTAssert(dummyView.isCalledShowMandatoryUpdate == false)
        XCTAssert(dummyView.isCalledShowRecommendedUpdate == false)
    }
    
    func test_givenMessageAndUrlAndMandatoryTrue_whenICalledViewLoaded_thenICalledShowMandatoryUpdate() {
        
        // given
        sut.message = "message"
        sut.url = "url"
        sut.isMandatory = true
        
        // when
        sut.viewLoaded()
        
        // then
        XCTAssert(dummyView.isCalledShowMandatoryUpdate == true)
    }
    
    func test_givenMessageAndUrlAndMandatoryTrue_whenICalledViewLoaded_thenICalledShowMandatoryUpdateWithMessageAndUrl() {
        
        // given
        sut.message = "message"
        sut.url = "url"
        sut.isMandatory = true
        
        // when
        sut.viewLoaded()
        
        // then
        XCTAssert(dummyView.updateMessage == sut.message)
        XCTAssert(dummyView.updateUrlStore == sut.url)
    }
    
    func test_givenMessageAndUrlAndMandatoryFalse_whenICalledViewLoaded_thenICalledShowRecommendedUpdate() {
        
        // given
        sut.message = "message"
        sut.url = "url"
        sut.isMandatory = false
        
        // when
        sut.viewLoaded()
        
        // then
        XCTAssert(dummyView.isCalledShowRecommendedUpdate == true)
    }
    
    func test_givenMessageAndUrlAndMandatoryFalse_whenICalledViewLoaded_thenICalledShowRecommendedUpdateWithMessageAndUrl() {
        
        // given
        sut.message = "message"
        sut.url = "url"
        sut.isMandatory = false
        
        // when
        sut.viewLoaded()
        
        // then
        XCTAssert(dummyView.updateMessage == sut.message)
        XCTAssert(dummyView.updateUrlStore == sut.url)
    }
    
    // MARK: - showNavigationBarAnimation
    
    func test_giveContentSizeHeightGreaterThanscrollViewHeightPlusOffsetAnimation_whenICallShowNavigationBarAnimation_thenICallChangeNavigationBarColor() {
        
        //given
        dummyView.scrollViewHeight = 640
        dummyView.contentSize = CGSize(width: 320, height: 700)
        dummyView.contentOffset = CGPoint(x: 0, y: 60)
        dummyView.offsetAnimation = 30.0
        
        //When
        sut.showNavigationBarAnimation(byScrollViewHeight: dummyView.scrollViewHeight, withContentSize: dummyView.contentSize, andContentOffset: dummyView.contentOffset)
        
        //then
        XCTAssert(dummyView.isCalledChangeNavigationBarColor == true)
    }
    
    func test_giveContentSizeHeightSmallerThanscrollViewHeightPlusOffsetAnimation_whenICallShowNavigationBarAnimation_thenIDoNotCallChangeNavigationBarColor() {
        
        //given
        dummyView.scrollViewHeight = 640
        dummyView.contentSize = CGSize(width: 320, height: 640)
        dummyView.contentOffset = CGPoint(x: 0, y: 60)
        dummyView.offsetAnimation = 30.0
        
        //When
        sut.showNavigationBarAnimation(byScrollViewHeight: dummyView.scrollViewHeight, withContentSize: dummyView.contentSize, andContentOffset: dummyView.contentOffset)
        
        //then
        XCTAssert(dummyView.isCalledChangeNavigationBarColor == false)
    }
    
    func test_giveContentSizeHeightGreaterThanscrollViewHeightPlusOffsetAnimation_whenICallShowNavigationBarAnimation_thenICallChangeNavigationBarColorWithColor() {
        
        //given
        dummyView.scrollViewHeight = 640
        dummyView.contentSize = CGSize(width: 320, height: 700)
        dummyView.contentOffset = CGPoint(x: 0, y: 60)
        dummyView.offsetAnimation = 30.0
        let offset = dummyView.contentOffset.y / dummyView.offsetAnimation
        let color = UIColor.NAVY.withAlphaComponent(offset)
        
        //When
        sut.showNavigationBarAnimation(byScrollViewHeight: dummyView.scrollViewHeight, withContentSize: dummyView.contentSize, andContentOffset: dummyView.contentOffset)
        
        //then
        XCTAssert(dummyView.navigationBarColor == color)
    }
    
    class UpgradeNotificationViewProtocolDummy: UpgradeNotificationViewProtocol, ViewProtocol {
        
        var isCalledShowMandatoryUpdate = false
        var isCalledShowRecommendedUpdate = false
        var isCalledChangeNavigationBarColor = false
        
        var scrollViewHeight: CGFloat!
        var contentSize: CGSize!
        var contentOffset: CGPoint!
        var offsetAnimation: CGFloat!
        
        var updateMessage: String!
        var updateUrlStore: String!
        var navigationBarColor: UIColor!
        
        func showError(error: ModelBO) {
        }
        
        func changeColorEditTexts() {
        }
        
        func showMandatoryUpdate(withMessage message: String, andUrlStore urlStore: String) {
            
            isCalledShowMandatoryUpdate = true
            updateMessage = message
            updateUrlStore = urlStore
        }
        
        func showRecommendedUpdate(withMessage message: String, andUrlStore urlStore: String) {
            
            isCalledShowRecommendedUpdate = true
            updateMessage = message
            updateUrlStore = urlStore
        }
        
        func changeNavigationBarColor(withColor color: UIColor) {
            
            isCalledChangeNavigationBarColor = true
            navigationBarColor = color
        }
    }
}
