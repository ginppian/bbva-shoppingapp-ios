//
//  DidacticAreaDisplayDataTest.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 15/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class DidacticAreaDisplayDataTest: XCTestCase {

    // MARK: - generateDidacticAreaDisplayData - Empty data

    func test_givenAny_whenICallGenerateDidacticAreaDisplayData_thenDisplayDataMatchToDefaultDisplayData() {

        // given

        // when
        let displayData = DidacticAreaDisplayData.generateDidacticAreaDisplayData(withDidacticAreaBO: nil, andDidacticCategoryBO: nil)

        let defaultDisplayData = displayData.showDefaultDidacticAreaTypeDidactic()

        // then
        XCTAssert(displayData.helpId == defaultDisplayData.helpId)
        XCTAssert(displayData.title == defaultDisplayData.title)
        XCTAssert(displayData.description == defaultDisplayData.description)
        XCTAssert(displayData.imageBackgroundURL == defaultDisplayData.imageBackgroundURL)
        XCTAssert(displayData.imageThumbURL == defaultDisplayData.imageThumbURL)
        XCTAssert(displayData.imageBackground == defaultDisplayData.imageBackground)
        XCTAssert(displayData.imageThumb == defaultDisplayData.imageThumb)
        XCTAssert(displayData.titleColor == defaultDisplayData.titleColor)
        XCTAssert(displayData.categoryId == defaultDisplayData.categoryId)
        XCTAssert(displayData.categoryName == defaultDisplayData.categoryName)
        XCTAssert(displayData.url == defaultDisplayData.url)
        XCTAssert(displayData.descriptionColor == defaultDisplayData.descriptionColor)
        XCTAssert(displayData.descriptionOnTapColor == defaultDisplayData.descriptionOnTapColor)
        XCTAssert(displayData.descriptionOnTapColor == defaultDisplayData.descriptionOnTapColor)
    }

    func test_givenAny_whenICallGenerateDidacticAreaDisplayData_thenTypeOfferIsFalse() {

        // given

        // when
        let displayData = DidacticAreaDisplayData.generateDidacticAreaDisplayData(withDidacticAreaBO: nil, andDidacticCategoryBO: nil)

        // then
        XCTAssert(displayData.isTypeOffer == false)

    }

    func test_givenAny_whenICallGenerateDidacticAreaDisplayData_thenImageBackgroundMatchToImage() {

        // given

        // when
        let displayData = DidacticAreaDisplayData.generateDidacticAreaDisplayData(withDidacticAreaBO: nil, andDidacticCategoryBO: nil)

        // then
        XCTAssert(displayData.imageBackground == ConstantsImages.DidacticArea.imageBackgroundDefault)

    }

    func test_givenAny_whenICallGenerateDidacticAreaDisplayData_thenImageBackgroundOfferMatchToImage() {

        // given

        // when
        let displayData = DidacticAreaDisplayData.generateDidacticAreaDisplayData(withDidacticAreaBO: nil, andDidacticCategoryBO: nil)

        // then
        XCTAssert(displayData.imageBackgroundOffer == ConstantsImages.DidacticArea.imageBackgroundOfferDefault)

    }

    func test_givenAny_whenICallGenerateDidacticAreaDisplayData_thenImageThumbMatchToImage() {

        // given

        // when
        let displayData = DidacticAreaDisplayData.generateDidacticAreaDisplayData(withDidacticAreaBO: nil, andDidacticCategoryBO: nil)

        // then
        XCTAssert(displayData.imageThumb == ConstantsImages.DidacticArea.imageThumbDefault)

    }

    func test_givenAny_whenICallGenerateDidacticAreaDisplayData_thenTitleColorMatchToColor() {

        // given

        // when
        let displayData = DidacticAreaDisplayData.generateDidacticAreaDisplayData(withDidacticAreaBO: nil, andDidacticCategoryBO: nil)

        // then
        XCTAssert(displayData.titleColor == .BBVAWHITE)

    }

    func test_givenAny_whenICallGenerateDidacticAreaDisplayData_thenDescriptionColorMatchToColor() {

        // given

        // when
        let displayData = DidacticAreaDisplayData.generateDidacticAreaDisplayData(withDidacticAreaBO: nil, andDidacticCategoryBO: nil)

        // then
        XCTAssert(displayData.descriptionColor == .BBVALIGHTBLUE)

    }

    func test_givenAny_whenICallGenerateDidacticAreaDisplayData_thenDescriptionOnTapColorMatchToColor() {

        // given

        // when
        let displayData = DidacticAreaDisplayData.generateDidacticAreaDisplayData(withDidacticAreaBO: nil, andDidacticCategoryBO: nil)

        // then
        XCTAssert(displayData.descriptionOnTapColor == .MEDIUMBLUE)

    }

    // MARK: - generateDidacticAreaDisplayData - DidacticAreaBO == filled (Type == Unknowm) & DidacticCategoryBO == nil

    func test_givenDidacticAreaBOTypeUnknowm_whenICallGenerateDidacticAreaDisplayData_thenDisplayDataMatchToDefaultDisplayData() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO().items![1]
        didacticAreaBO.type = TypeOfDidacticArea.unknowm

        // when
        let displayData = DidacticAreaDisplayData.generateDidacticAreaDisplayData(withDidacticAreaBO: didacticAreaBO, andDidacticCategoryBO: nil)

        let defaultDisplayData = displayData.showDefaultDidacticAreaTypeDidactic()

        // then
        XCTAssert(displayData.helpId == defaultDisplayData.helpId)
        XCTAssert(displayData.title == defaultDisplayData.title)
        XCTAssert(displayData.description == defaultDisplayData.description)
        XCTAssert(displayData.imageBackgroundURL == defaultDisplayData.imageBackgroundURL)
        XCTAssert(displayData.imageThumbURL == defaultDisplayData.imageThumbURL)
        XCTAssert(displayData.imageBackground == defaultDisplayData.imageBackground)
        XCTAssert(displayData.imageThumb == defaultDisplayData.imageThumb)
        XCTAssert(displayData.titleColor == defaultDisplayData.titleColor)
        XCTAssert(displayData.descriptionColor == defaultDisplayData.descriptionColor)
        XCTAssert(displayData.descriptionOnTapColor == defaultDisplayData.descriptionOnTapColor)
    }

    // MARK: - generateDidacticAreaDisplayData - DidacticAreaBO == filled (Type == Didactic) & DidacticCategoryBO == nil

    func test_givenDidacticAreaBOTypeDidactic_whenICallGenerateDidacticAreaDisplayData_thenTypeOfferIsFalse() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO().items![1]
        didacticAreaBO.type = TypeOfDidacticArea.didactic

        // when
        let displayData = DidacticAreaDisplayData.generateDidacticAreaDisplayData(withDidacticAreaBO: didacticAreaBO, andDidacticCategoryBO: nil)

        // then
        XCTAssert(displayData.isTypeOffer == false)

    }

    func test_givenDidacticAreaBOTypeDidactic_whenICallGenerateDidacticAreaDisplayData_thenImageBackgroundURLIsEmpty() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO().items![1]
        didacticAreaBO.type = TypeOfDidacticArea.didactic

        // when
        let displayData = DidacticAreaDisplayData.generateDidacticAreaDisplayData(withDidacticAreaBO: didacticAreaBO, andDidacticCategoryBO: nil)

        // then
        XCTAssert(displayData.imageBackgroundURL.isEmpty == true)

    }

    func test_givenDidacticAreaBOTypeDidactic_whenICallGenerateDidacticAreaDisplayData_thenImageThumbNotEmpty() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO().items![1]
        didacticAreaBO.type = TypeOfDidacticArea.didactic

        // when
        let displayData = DidacticAreaDisplayData.generateDidacticAreaDisplayData(withDidacticAreaBO: didacticAreaBO, andDidacticCategoryBO: nil)

        // then
        XCTAssert(displayData.imageThumb.isEmpty == false)

    }

    func test_givenDidacticAreaBOTypeDidacticAndImageThumbEmpty_whenICallGenerateDidacticAreaDisplayData_thenImageThumbByDefault() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO().items![1]
        didacticAreaBO.type = TypeOfDidacticArea.didactic
        didacticAreaBO.imageThumb = ""

        // when
        let displayData = DidacticAreaDisplayData.generateDidacticAreaDisplayData(withDidacticAreaBO: didacticAreaBO, andDidacticCategoryBO: nil)

        // then
        XCTAssert(displayData.imageThumb == ConstantsImages.DidacticArea.imageThumbDefault)

    }

    func test_givenDidacticAreaBOTypeDidactic_whenICallGenerateDidacticAreaDisplayData_thenTitleNotEmpty() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO().items![1]
        didacticAreaBO.type = TypeOfDidacticArea.didactic

        // when
        let displayData = DidacticAreaDisplayData.generateDidacticAreaDisplayData(withDidacticAreaBO: didacticAreaBO, andDidacticCategoryBO: nil)

        // then
        XCTAssert(displayData.title.isEmpty == false)

    }

    func test_givenDidacticAreaBOTypeDidactic_whenICallGenerateDidacticAreaDisplayData_thenDescriptionNotEmpty() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO().items![1]
        didacticAreaBO.type = TypeOfDidacticArea.didactic

        // when
        let displayData = DidacticAreaDisplayData.generateDidacticAreaDisplayData(withDidacticAreaBO: didacticAreaBO, andDidacticCategoryBO: nil)

        // then
        XCTAssert(displayData.description.isEmpty == false)

    }

    func test_givenDidacticAreaBOTypeDidacticAndColorsEmpty_whenICallGenerateDidacticAreaDisplayData_thenColorsByDefault() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO().items![1]
        didacticAreaBO.type = TypeOfDidacticArea.didactic
        didacticAreaBO.titleColor = nil
        didacticAreaBO.descriptionColor = nil
        didacticAreaBO.descriptionOnTapColor = nil

        // when
        let displayData = DidacticAreaDisplayData.generateDidacticAreaDisplayData(withDidacticAreaBO: didacticAreaBO, andDidacticCategoryBO: nil)

        // then
        XCTAssert(displayData.titleColor == .BBVAWHITE)
        XCTAssert(displayData.descriptionColor == .BBVALIGHTBLUE)
        XCTAssert(displayData.descriptionOnTapColor == .MEDIUMBLUE)
    }

    func test_givenDidacticAreaBOTypeDidacticAndTitleColorEmpty_whenICallGenerateDidacticAreaDisplayData_thenColorsByDefault() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO().items![1]
        didacticAreaBO.type = TypeOfDidacticArea.didactic
        didacticAreaBO.titleColor = nil

        // when
        let displayData = DidacticAreaDisplayData.generateDidacticAreaDisplayData(withDidacticAreaBO: didacticAreaBO, andDidacticCategoryBO: nil)

        // then
        XCTAssert(displayData.titleColor == .BBVAWHITE)
        XCTAssert(displayData.descriptionColor == .BBVALIGHTBLUE)
        XCTAssert(displayData.descriptionOnTapColor == .MEDIUMBLUE)
    }

    func test_givenDidacticAreaBOTypeDidacticAndDescriptionColorEmpty_whenICallGenerateDidacticAreaDisplayData_thenColorsByDefault() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO().items![1]
        didacticAreaBO.type = TypeOfDidacticArea.didactic
        didacticAreaBO.descriptionColor = nil

        // when
        let displayData = DidacticAreaDisplayData.generateDidacticAreaDisplayData(withDidacticAreaBO: didacticAreaBO, andDidacticCategoryBO: nil)

        // then
        XCTAssert(displayData.titleColor == .BBVAWHITE)
        XCTAssert(displayData.descriptionColor == .BBVALIGHTBLUE)
        XCTAssert(displayData.descriptionOnTapColor == .MEDIUMBLUE)
    }

    func test_givenDidacticAreaBOTypeDidacticAndDescriptionOnTapColorEmpty_whenICallGenerateDidacticAreaDisplayData_thenColorsByDefault() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO().items![1]
        didacticAreaBO.type = TypeOfDidacticArea.didactic
        didacticAreaBO.descriptionOnTapColor = nil

        // when
        let displayData = DidacticAreaDisplayData.generateDidacticAreaDisplayData(withDidacticAreaBO: didacticAreaBO, andDidacticCategoryBO: nil)

        // then
        XCTAssert(displayData.titleColor == .BBVAWHITE)
        XCTAssert(displayData.descriptionColor == .BBVALIGHTBLUE)
        XCTAssert(displayData.descriptionOnTapColor == .MEDIUMBLUE)
    }

    func test_givenDidacticAreaBOTypeDidacticAndHelpIdAndTextsEmpty_whenICallGenerateDidacticAreaDisplayData_thenDidacticAreaByDefault() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO().items![1]
        didacticAreaBO.type = TypeOfDidacticArea.didactic
        didacticAreaBO.helpId = nil
        didacticAreaBO.texts = nil

        // when
        let displayData = DidacticAreaDisplayData.generateDidacticAreaDisplayData(withDidacticAreaBO: didacticAreaBO, andDidacticCategoryBO: nil)

        // then
        let name = KeychainManager.shared.getFirstname()
        let firstname = (!name.isEmpty) ? " \(name)" : ""
        let title = Localizables.didactica.key_didactic_hello_text + "\(firstname)" + Localizables.didactica.key_didactic_exclamation_mark_text
        let description = Localizables.didactica.key_didactic_explore_text

        XCTAssert(displayData.imageBackground == ConstantsImages.DidacticArea.imageBackgroundDefault)
        XCTAssert(displayData.imageThumb == ConstantsImages.DidacticArea.imageThumbDefault)
        XCTAssert(displayData.title == title)
        XCTAssert(displayData.description == description)
    }

    func test_givenDidacticAreaBOTypeDidacticAndHelpIdNil_whenICallGenerateDidacticAreaDisplayData_thenDidacticAreaByDefault() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO().items![1]
        didacticAreaBO.type = TypeOfDidacticArea.didactic
        didacticAreaBO.helpId = nil

        // when
        let displayData = DidacticAreaDisplayData.generateDidacticAreaDisplayData(withDidacticAreaBO: didacticAreaBO, andDidacticCategoryBO: nil)

        // then
        let name = KeychainManager.shared.getFirstname()
        let firstname = (!name.isEmpty) ? " \(name)" : ""
        let title = Localizables.didactica.key_didactic_hello_text + "\(firstname)" + Localizables.didactica.key_didactic_exclamation_mark_text
        let description = Localizables.didactica.key_didactic_explore_text

        XCTAssert(displayData.imageBackground == ConstantsImages.DidacticArea.imageBackgroundDefault)
        XCTAssert(displayData.imageThumb == ConstantsImages.DidacticArea.imageThumbDefault)
        XCTAssert(displayData.title == title)
        XCTAssert(displayData.description == description)
    }

    func test_givenDidacticAreaBOTypeDidacticAndHelpIdEmpty_whenICallGenerateDidacticAreaDisplayData_thenDidacticAreaByDefault() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO().items![1]
        didacticAreaBO.type = TypeOfDidacticArea.didactic
        didacticAreaBO.helpId = ""

        // when
        let displayData = DidacticAreaDisplayData.generateDidacticAreaDisplayData(withDidacticAreaBO: didacticAreaBO, andDidacticCategoryBO: nil)

        // then
        let name = KeychainManager.shared.getFirstname()
        let firstname = (!name.isEmpty) ? " \(name)" : ""
        let title = Localizables.didactica.key_didactic_hello_text + "\(firstname)" + Localizables.didactica.key_didactic_exclamation_mark_text
        let description = Localizables.didactica.key_didactic_explore_text

        XCTAssert(displayData.imageBackground == ConstantsImages.DidacticArea.imageBackgroundDefault)
        XCTAssert(displayData.imageThumb == ConstantsImages.DidacticArea.imageThumbDefault)
        XCTAssert(displayData.title == title)
        XCTAssert(displayData.description == description)
    }

    func test_givenDidacticAreaBOTypeDidacticAndTextsEmpty_whenICallGenerateDidacticAreaDisplayData_thenDidacticAreaByDefault() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO().items![1]
        didacticAreaBO.type = TypeOfDidacticArea.didactic
        didacticAreaBO.texts = nil

        // when
        let displayData = DidacticAreaDisplayData.generateDidacticAreaDisplayData(withDidacticAreaBO: didacticAreaBO, andDidacticCategoryBO: nil)

        // then
        let name = KeychainManager.shared.getFirstname()
        let firstname = (!name.isEmpty) ? " \(name)" : ""
        let title = Localizables.didactica.key_didactic_hello_text + "\(firstname)" + Localizables.didactica.key_didactic_exclamation_mark_text
        let description = Localizables.didactica.key_didactic_explore_text

        XCTAssert(displayData.imageBackground == ConstantsImages.DidacticArea.imageBackgroundDefault)
        XCTAssert(displayData.imageThumb == ConstantsImages.DidacticArea.imageThumbDefault)
        XCTAssert(displayData.title == title)
        XCTAssert(displayData.description == description)
    }

    func test_givenDidacticAreaBOTypeDidacticAndTitleAndDescriptionEmpty_whenICallGenerateDidacticAreaDisplayData_thenDidacticAreaByDefault() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO().items![1]
        didacticAreaBO.type = TypeOfDidacticArea.didactic
        didacticAreaBO.texts![0].title = ""
        didacticAreaBO.texts![0].description = ""

        // when
        let displayData = DidacticAreaDisplayData.generateDidacticAreaDisplayData(withDidacticAreaBO: didacticAreaBO, andDidacticCategoryBO: nil)

        // then
        let name = KeychainManager.shared.getFirstname()
        let firstname = (!name.isEmpty) ? " \(name)" : ""
        let title = Localizables.didactica.key_didactic_hello_text + "\(firstname)" + Localizables.didactica.key_didactic_exclamation_mark_text
        let description = Localizables.didactica.key_didactic_explore_text

        XCTAssert(displayData.imageBackground == ConstantsImages.DidacticArea.imageBackgroundDefault)
        XCTAssert(displayData.imageThumb == ConstantsImages.DidacticArea.imageThumbDefault)
        XCTAssert(displayData.title == title)
        XCTAssert(displayData.description == description)
    }

    // MARK: - generateDidacticAreaDisplayData - DidacticAreaBO == filled (Type == Offer) & DidacticCategoryBO == nil

    func test_givenDidacticAreaBOTypeOfferWithOutImageBackground_whenICallGenerateDidacticAreaDisplayData_thenDisplayDataMatchToDefaultDisplayData() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO().items![0]
        didacticAreaBO.type = TypeOfDidacticArea.offer
        didacticAreaBO.imageBackground = ""

        // when
        let displayData = DidacticAreaDisplayData.generateDidacticAreaDisplayData(withDidacticAreaBO: didacticAreaBO, andDidacticCategoryBO: nil)

        let defaultDisplayData = displayData.setDefaultColorsAndBackgroundDidacticAreaTypeOffer(withDidacticAreaDisplayData: displayData)

        // then
        XCTAssert(displayData.helpId == defaultDisplayData.helpId)
        XCTAssert(displayData.title == defaultDisplayData.title)
        XCTAssert(displayData.description == defaultDisplayData.description)
        XCTAssert(displayData.imageBackgroundURL == defaultDisplayData.imageBackgroundURL)
        XCTAssert(displayData.imageThumbURL == defaultDisplayData.imageThumbURL)
        XCTAssert(displayData.imageBackground == defaultDisplayData.imageBackground)
        XCTAssert(displayData.imageThumb == defaultDisplayData.imageThumb)
        XCTAssert(displayData.titleColor == defaultDisplayData.titleColor)
        XCTAssert(displayData.descriptionColor == defaultDisplayData.descriptionColor)
        XCTAssert(displayData.descriptionOnTapColor == defaultDisplayData.descriptionOnTapColor)

    }

    func test_givenDidacticAreaBOTypeOffer_whenICallGenerateDidacticAreaDisplayData_thenTypeOfferIsTrue() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO().items![0]
        didacticAreaBO.type = TypeOfDidacticArea.offer

        // when
        let displayData = DidacticAreaDisplayData.generateDidacticAreaDisplayData(withDidacticAreaBO: didacticAreaBO, andDidacticCategoryBO: nil)

        // then
        XCTAssert(displayData.isTypeOffer == true)

    }

    func test_givenDidacticAreaBOTypeOfferAndDescriptionColorEmpty_whenICallGenerateDidacticAreaDisplayData_thenDescriptionColorMatchToColor() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO().items![0]
        didacticAreaBO.type = TypeOfDidacticArea.offer
        didacticAreaBO.descriptionColor = nil

        // when
        let displayData = DidacticAreaDisplayData.generateDidacticAreaDisplayData(withDidacticAreaBO: didacticAreaBO, andDidacticCategoryBO: nil)

        let defaultDisplayData = displayData.setDefaultColorsAndBackgroundDidacticAreaTypeOffer(withDidacticAreaDisplayData: displayData)

        // then
        XCTAssert(displayData.descriptionColor == defaultDisplayData.descriptionColor)

    }

    func test_givenDidacticAreaBOTypeOfferAndDescriptionOnTapColorEmpty_whenICallGenerateDidacticAreaDisplayData_thenDescriptionOnTapColorMatchToColor() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO().items![0]
        didacticAreaBO.type = TypeOfDidacticArea.offer
        didacticAreaBO.descriptionOnTapColor = nil

        // when
        let displayData = DidacticAreaDisplayData.generateDidacticAreaDisplayData(withDidacticAreaBO: didacticAreaBO, andDidacticCategoryBO: nil)

        let defaultDisplayData = displayData.setDefaultColorsAndBackgroundDidacticAreaTypeOffer(withDidacticAreaDisplayData: displayData)

        // then
        XCTAssert(displayData.descriptionOnTapColor == defaultDisplayData.descriptionOnTapColor)

    }

    func test_givenDidacticAreaBOTypeOfferAndHexStringEmpty_whenICallGenerateDidacticAreaDisplayData_thenDescriptionColorMatchToColor() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO().items![0]
        didacticAreaBO.type = TypeOfDidacticArea.offer
        didacticAreaBO.descriptionColor?.hexString = nil

        // when
        let displayData = DidacticAreaDisplayData.generateDidacticAreaDisplayData(withDidacticAreaBO: didacticAreaBO, andDidacticCategoryBO: nil)

        let defaultDisplayData = displayData.setDefaultColorsAndBackgroundDidacticAreaTypeOffer(withDidacticAreaDisplayData: displayData)

        // then
        XCTAssert(displayData.descriptionColor == defaultDisplayData.descriptionColor)

    }

    func test_givenDidacticAreaBOTypeOfferAndHexStringEmpty_whenICallGenerateDidacticAreaDisplayData_thenDescriptionOnTapColorMatchToColor() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO().items![0]
        didacticAreaBO.type = TypeOfDidacticArea.offer
        didacticAreaBO.descriptionOnTapColor?.hexString = nil

        // when
        let displayData = DidacticAreaDisplayData.generateDidacticAreaDisplayData(withDidacticAreaBO: didacticAreaBO, andDidacticCategoryBO: nil)

        let defaultDisplayData = displayData.setDefaultColorsAndBackgroundDidacticAreaTypeOffer(withDidacticAreaDisplayData: displayData)

        // then
        XCTAssert(displayData.descriptionOnTapColor == defaultDisplayData.descriptionOnTapColor)

    }

    func test_givenDidacticAreaBOTypeOfferAndAlphaEmpty_whenICallGenerateDidacticAreaDisplayData_thenDescriptionColorMatchToColor() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO().items![0]
        didacticAreaBO.type = TypeOfDidacticArea.offer
        didacticAreaBO.descriptionColor?.alpha = nil

        // when
        let displayData = DidacticAreaDisplayData.generateDidacticAreaDisplayData(withDidacticAreaBO: didacticAreaBO, andDidacticCategoryBO: nil)

        let defaultDisplayData = displayData.setDefaultColorsAndBackgroundDidacticAreaTypeOffer(withDidacticAreaDisplayData: displayData)

        // then
        XCTAssert(displayData.descriptionColor == defaultDisplayData.descriptionColor)

    }

    func test_givenDidacticAreaBOTypeOfferAndAlphaEmpty_whenICallGenerateDidacticAreaDisplayData_thenDescriptionOnTapColorMatchToColor() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO().items![0]
        didacticAreaBO.type = TypeOfDidacticArea.offer
        didacticAreaBO.descriptionOnTapColor?.alpha = nil

        // when
        let displayData = DidacticAreaDisplayData.generateDidacticAreaDisplayData(withDidacticAreaBO: didacticAreaBO, andDidacticCategoryBO: nil)

        let defaultDisplayData = displayData.setDefaultColorsAndBackgroundDidacticAreaTypeOffer(withDidacticAreaDisplayData: displayData)

        // then
        XCTAssert(displayData.descriptionOnTapColor == defaultDisplayData.descriptionOnTapColor)

    }

    // MARK: - generateDidacticAreaDisplayData - DidacticAreaBO == filled (Type == Offer) & DidacticCategoryBO == filled

    func test_givenDidacticAreaBOTypeOfferAndDidacticCategoryBO_whenICallGenerateDidacticAreaDisplayData_thenTypeOfferIsTrue() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO().items![0]
        didacticAreaBO.type = TypeOfDidacticArea.offer
        let didacticCategoryBO = ConfigGenerator.getDidacticAreasBO().categories?.defaultCategory![0]

        // when
        let displayData = DidacticAreaDisplayData.generateDidacticAreaDisplayData(withDidacticAreaBO: didacticAreaBO, andDidacticCategoryBO: didacticCategoryBO)

        // then
        XCTAssert(displayData.isTypeOffer == true)

    }

    func test_givenDidacticAreaBOTypeOfferAndDidacticCategoryBO_whenICallGenerateDidacticAreaDisplayData_thenCategoryIdNotEmpty() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO().items![2]
        didacticAreaBO.type = TypeOfDidacticArea.offer
        let didacticCategoryBO = ConfigGenerator.getDidacticAreasBO().categories?.defaultCategory![0]

        // when
        let displayData = DidacticAreaDisplayData.generateDidacticAreaDisplayData(withDidacticAreaBO: didacticAreaBO, andDidacticCategoryBO: didacticCategoryBO)

        // then
        XCTAssert(displayData.categoryId.isEmpty == false)

    }

    func test_givenDidacticAreaBOTypeOfferAndDidacticCategoryBO_whenICallGenerateDidacticAreaDisplayData_thenImageBackgroundURLNotEmpty() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO().items![2]
        didacticAreaBO.type = TypeOfDidacticArea.offer
        let didacticCategoryBO = ConfigGenerator.getDidacticAreasBO().categories?.defaultCategory![0]

        // when
        let displayData = DidacticAreaDisplayData.generateDidacticAreaDisplayData(withDidacticAreaBO: didacticAreaBO, andDidacticCategoryBO: didacticCategoryBO)

        // then
        XCTAssert(displayData.imageBackgroundURL.isEmpty == false)

    }

    func test_givenDidacticAreaBOTypeOfferAndDidacticCategoryBO_whenICallGenerateDidacticAreaDisplayData_thenTitleNotEmpty() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO().items![2]
        didacticAreaBO.type = TypeOfDidacticArea.offer
        let didacticCategoryBO = ConfigGenerator.getDidacticAreasBO().categories?.defaultCategory![0]

        // when
        let displayData = DidacticAreaDisplayData.generateDidacticAreaDisplayData(withDidacticAreaBO: didacticAreaBO, andDidacticCategoryBO: didacticCategoryBO)

        // then
        XCTAssert(displayData.title.isEmpty == false)

    }

    func test_givenDidacticAreaBOTypeOfferAndDidacticCategoryBO_whenICallGenerateDidacticAreaDisplayData_thenDescriptionNotEmpty() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO().items![2]
        didacticAreaBO.type = TypeOfDidacticArea.offer
        let didacticCategoryBO = ConfigGenerator.getDidacticAreasBO().categories?.defaultCategory![0]

        // when
        let displayData = DidacticAreaDisplayData.generateDidacticAreaDisplayData(withDidacticAreaBO: didacticAreaBO, andDidacticCategoryBO: didacticCategoryBO)

        // then
        XCTAssert(displayData.description.isEmpty == false)

    }

    func test_givenDidacticAreaBOTypeOfferAndDidacticCategoryBOWithBackgroundAndColorsEmpty_whenICallGenerateDidacticAreaDisplayData_thenImageBackgroundAndColorsByDefault() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO().items![2]
        didacticAreaBO.type = TypeOfDidacticArea.offer
        let didacticCategoryBO = ConfigGenerator.getDidacticAreasBO().categories?.defaultCategory![0]
        didacticCategoryBO?.imageBackground = ""
        didacticCategoryBO?.titleColor = nil
        didacticCategoryBO?.descriptionColor = nil
        didacticCategoryBO?.descriptionOnTapColor = nil

        // when
        let displayData = DidacticAreaDisplayData.generateDidacticAreaDisplayData(withDidacticAreaBO: didacticAreaBO, andDidacticCategoryBO: didacticCategoryBO)

        // then
        XCTAssert(displayData.imageBackgroundOffer == ConstantsImages.DidacticArea.imageBackgroundOfferDefault)
        XCTAssert(displayData.titleColor == .BBVAWHITE)
        XCTAssert(displayData.descriptionColor == .BBVAWHITE)
        XCTAssert(displayData.descriptionOnTapColor == UIColor.BBVAWHITE.withAlphaComponent(0.3))
    }

    func test_givenDidacticAreaBOTypeOfferAndDidacticCategoryBOWithBackgroundEmpty_whenICallGenerateDidacticAreaDisplayData_thenImageBackgroundAndColorsByDefault() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO().items![2]
        didacticAreaBO.type = TypeOfDidacticArea.offer
        let didacticCategoryBO = ConfigGenerator.getDidacticAreasBO().categories?.defaultCategory![0]
        didacticCategoryBO?.imageBackground = ""

        // when
        let displayData = DidacticAreaDisplayData.generateDidacticAreaDisplayData(withDidacticAreaBO: didacticAreaBO, andDidacticCategoryBO: didacticCategoryBO)

        // then
        XCTAssert(displayData.imageBackgroundOffer == ConstantsImages.DidacticArea.imageBackgroundOfferDefault)
        XCTAssert(displayData.titleColor == .BBVAWHITE)
        XCTAssert(displayData.descriptionColor == .BBVAWHITE)
        XCTAssert(displayData.descriptionOnTapColor == UIColor.BBVAWHITE.withAlphaComponent(0.3))
    }

    func test_givenDidacticAreaBOTypeOfferAndDidacticCategoryBOWithTitleColorEmpty_whenICallGenerateDidacticAreaDisplayData_thenImageBackgroundAndColorsByDefault() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO().items![2]
        didacticAreaBO.type = TypeOfDidacticArea.offer
        let didacticCategoryBO = ConfigGenerator.getDidacticAreasBO().categories?.defaultCategory![0]
        didacticCategoryBO?.titleColor = nil

        // when
        let displayData = DidacticAreaDisplayData.generateDidacticAreaDisplayData(withDidacticAreaBO: didacticAreaBO, andDidacticCategoryBO: didacticCategoryBO)

        // then
        XCTAssert(displayData.imageBackgroundOffer == ConstantsImages.DidacticArea.imageBackgroundOfferDefault)
        XCTAssert(displayData.titleColor == .BBVAWHITE)
        XCTAssert(displayData.descriptionColor == .BBVAWHITE)
        XCTAssert(displayData.descriptionOnTapColor == UIColor.BBVAWHITE.withAlphaComponent(0.3))
    }

    func test_givenDidacticAreaBOTypeOfferAndDidacticCategoryBOWithDescriptionColorEmpty_whenICallGenerateDidacticAreaDisplayData_thenImageBackgroundAndColorsByDefault() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO().items![2]
        didacticAreaBO.type = TypeOfDidacticArea.offer
        let didacticCategoryBO = ConfigGenerator.getDidacticAreasBO().categories?.defaultCategory![0]
        didacticCategoryBO?.descriptionColor = nil

        // when
        let displayData = DidacticAreaDisplayData.generateDidacticAreaDisplayData(withDidacticAreaBO: didacticAreaBO, andDidacticCategoryBO: didacticCategoryBO)

        // then
        XCTAssert(displayData.imageBackgroundOffer == ConstantsImages.DidacticArea.imageBackgroundOfferDefault)
        XCTAssert(displayData.titleColor == .BBVAWHITE)
        XCTAssert(displayData.descriptionColor == .BBVAWHITE)
        XCTAssert(displayData.descriptionOnTapColor == UIColor.BBVAWHITE.withAlphaComponent(0.3))
    }

    func test_givenDidacticAreaBOTypeOfferAndDidacticCategoryBOWithDescriptionOnTapColorEmpty_whenICallGenerateDidacticAreaDisplayData_thenImageBackgroundAndColorsByDefault() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO().items![2]
        didacticAreaBO.type = TypeOfDidacticArea.offer
        let didacticCategoryBO = ConfigGenerator.getDidacticAreasBO().categories?.defaultCategory![0]
        didacticCategoryBO?.descriptionOnTapColor = nil

        // when
        let displayData = DidacticAreaDisplayData.generateDidacticAreaDisplayData(withDidacticAreaBO: didacticAreaBO, andDidacticCategoryBO: didacticCategoryBO)

        // then
        XCTAssert(displayData.imageBackgroundOffer == ConstantsImages.DidacticArea.imageBackgroundOfferDefault)
        XCTAssert(displayData.titleColor == .BBVAWHITE)
        XCTAssert(displayData.descriptionColor == .BBVAWHITE)
        XCTAssert(displayData.descriptionOnTapColor == UIColor.BBVAWHITE.withAlphaComponent(0.3))
    }

    func test_givenDidacticAreaBOTypeOfferAndDidacticCategoryBOWithTextsEmpty_whenICallGenerateDidacticAreaDisplayData_thenTitleAndDescriptionByDefault() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO().items![2]
        didacticAreaBO.type = TypeOfDidacticArea.offer
        let didacticCategoryBO = ConfigGenerator.getDidacticAreasBO().categories?.defaultCategory![0]
        didacticCategoryBO?.texts = nil
        // when
        let displayData = DidacticAreaDisplayData.generateDidacticAreaDisplayData(withDidacticAreaBO: didacticAreaBO, andDidacticCategoryBO: didacticCategoryBO)

        // then
        XCTAssert(displayData.title == Localizables.didactica.key_didactic_promotions_text)
        XCTAssert(displayData.description == Localizables.didactica.key_didactic_discover_text)
    }

    func test_givenDidacticAreaBOTypeOfferAndDidacticCategoryBOWithTitleAndDescriptionEmpty_whenICallGenerateDidacticAreaDisplayData_thenTitleAndDescriptionByDefault() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO().items![2]
        didacticAreaBO.type = TypeOfDidacticArea.offer
        let didacticCategoryBO = ConfigGenerator.getDidacticAreasBO().categories?.defaultCategory![0]
        didacticCategoryBO?.texts![0].title = ""
        didacticCategoryBO?.texts![0].description = ""
        // when
        let displayData = DidacticAreaDisplayData.generateDidacticAreaDisplayData(withDidacticAreaBO: didacticAreaBO, andDidacticCategoryBO: didacticCategoryBO)

        // then
        XCTAssert(displayData.title == Localizables.didactica.key_didactic_promotions_text)
        XCTAssert(displayData.description == Localizables.didactica.key_didactic_discover_text)
    }

    func test_givenDidacticAreaBOTypeOfferAndDidacticCategoryBOWithTitleEmpty_whenICallGenerateDidacticAreaDisplayData_thenTitleAndDescriptionByDefault() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO().items![2]
        didacticAreaBO.type = TypeOfDidacticArea.offer
        let didacticCategoryBO = ConfigGenerator.getDidacticAreasBO().categories?.defaultCategory![0]
        didacticCategoryBO?.texts![0].title = ""

        // when
        let displayData = DidacticAreaDisplayData.generateDidacticAreaDisplayData(withDidacticAreaBO: didacticAreaBO, andDidacticCategoryBO: didacticCategoryBO)

        // then
        XCTAssert(displayData.title == Localizables.didactica.key_didactic_promotions_text)
        XCTAssert(displayData.description == Localizables.didactica.key_didactic_discover_text)
    }

    func test_givenDidacticAreaBOTypeOfferAndDidacticCategoryBOWithCategoryIdNil_whenICallGenerateDidacticAreaDisplayData_thenDidacticAreaByDefault() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO().items![2]
        didacticAreaBO.type = TypeOfDidacticArea.offer
        let didacticCategoryBO = ConfigGenerator.getDidacticAreasBO().categories?.defaultCategory![0]
        didacticCategoryBO?.categoryId = nil

        // when
        let displayData = DidacticAreaDisplayData.generateDidacticAreaDisplayData(withDidacticAreaBO: didacticAreaBO, andDidacticCategoryBO: didacticCategoryBO)

        // then
        let name = KeychainManager.shared.getFirstname()
        let firstname = (!name.isEmpty) ? " \(name)" : ""
        let title = Localizables.didactica.key_didactic_hello_text + "\(firstname)" + Localizables.didactica.key_didactic_exclamation_mark_text
        let description = Localizables.didactica.key_didactic_explore_text

        XCTAssert(displayData.helpId.isEmpty)
        XCTAssert(displayData.title == title)
        XCTAssert(displayData.description == description)

        XCTAssert(displayData.imageBackgroundURL.isEmpty)
        XCTAssert(displayData.imageThumbURL.isEmpty)

        XCTAssert(displayData.imageBackground == ConstantsImages.DidacticArea.imageBackgroundDefault)
        XCTAssert(displayData.imageThumb == ConstantsImages.DidacticArea.imageThumbDefault)

        XCTAssert(displayData.titleColor == .BBVAWHITE)
        XCTAssert(displayData.descriptionColor == .BBVALIGHTBLUE)
        XCTAssert(displayData.descriptionOnTapColor == .MEDIUMBLUE)
    }

    func test_givenDidacticAreaBOTypeOfferAndDidacticCategoryBOWithCategoryIdEmpty_whenICallGenerateDidacticAreaDisplayData_thenDidacticAreaByDefault() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO().items![2]
        didacticAreaBO.type = TypeOfDidacticArea.offer
        let didacticCategoryBO = ConfigGenerator.getDidacticAreasBO().categories?.defaultCategory![0]
        didacticCategoryBO?.categoryId = ""

        // when
        let displayData = DidacticAreaDisplayData.generateDidacticAreaDisplayData(withDidacticAreaBO: didacticAreaBO, andDidacticCategoryBO: didacticCategoryBO)

        // then
        let name = KeychainManager.shared.getFirstname()
        let firstname = (!name.isEmpty) ? " \(name)" : ""
        let title = Localizables.didactica.key_didactic_hello_text + "\(firstname)" + Localizables.didactica.key_didactic_exclamation_mark_text
        let description = Localizables.didactica.key_didactic_explore_text

        XCTAssert(displayData.helpId.isEmpty)
        XCTAssert(displayData.title == title)
        XCTAssert(displayData.description == description)

        XCTAssert(displayData.imageBackgroundURL.isEmpty)
        XCTAssert(displayData.imageThumbURL.isEmpty)

        XCTAssert(displayData.imageBackground == ConstantsImages.DidacticArea.imageBackgroundDefault)
        XCTAssert(displayData.imageThumb == ConstantsImages.DidacticArea.imageThumbDefault)

        XCTAssert(displayData.titleColor == .BBVAWHITE)
        XCTAssert(displayData.descriptionColor == .BBVALIGHTBLUE)
        XCTAssert(displayData.descriptionOnTapColor == .MEDIUMBLUE)
    }
}
