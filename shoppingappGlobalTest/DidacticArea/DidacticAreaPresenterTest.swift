//
//  DidacticAreaPresenterTest.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 20/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest
import RxSwift

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class DidacticAreaPresenterTest: XCTestCase {

    let sut = DidacticAreaPresenter()

    // MARK: Offer

    func test_givenADisplayDataWithOffer_whenIReceiveDidacticAreaDisplayData_thenIShowDidacticAreaOffer() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        var displayData = DidacticAreaDisplayData()
        displayData.isTypeOffer = true

        // when
        sut.receiveDidacticAreaDisplayData(withDisplayData: displayData)

        // then
        XCTAssert(dummyView.isCalledShowDidacticAreaOffer == true)

    }

    func test_givenADisplayDataWithOffer_whenIReceiveDidacticAreaDisplayData_thenIShowDidacticAreaOfferWithTheDisplayDataOfParameter() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        var displayData = DidacticAreaDisplayData()
        displayData.isTypeOffer = true

        // when
        sut.receiveDidacticAreaDisplayData(withDisplayData: displayData)

        // then
        XCTAssert(dummyView.sentDisplayData! == displayData)

    }

    func test_givenADisplayDataWithDidactic_whenIReceiveDidacticAreaDisplayData_thenINotShowDidacticAreaOffer() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        var displayData = DidacticAreaDisplayData()
        displayData.isTypeOffer = false

        // when
        sut.receiveDidacticAreaDisplayData(withDisplayData: displayData)

        // then
        XCTAssert(dummyView.isCalledShowDidacticAreaOffer == false)

    }

    // MARK: Didactic

    func test_givenADisplayDataWithDidactic_whenIReceiveDidacticAreaDisplayData_thenIShowDidacticArea() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        var displayData = DidacticAreaDisplayData()
        displayData.isTypeOffer = false

        // when
        sut.receiveDidacticAreaDisplayData(withDisplayData: displayData)

        // then
        XCTAssert(dummyView.isCalledShowDidacticArea == true)

    }

    func test_givenADisplayDataWithDidactic_whenIReceiveDidacticAreaDisplayData_thenIShowDidacticAreaWithTheDisplayDataOfParameter() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        var displayData = DidacticAreaDisplayData()
        displayData.isTypeOffer = false

        // when
        sut.receiveDidacticAreaDisplayData(withDisplayData: displayData)

        // then
        XCTAssert(dummyView.sentDisplayData! == displayData)

    }

    func test_givenADisplayDataWithOffer_whenIReceiveDidacticAreaDisplayData_thenINotShowDidacticArea() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        var displayData = DidacticAreaDisplayData()
        displayData.isTypeOffer = true

        // when
        sut.receiveDidacticAreaDisplayData(withDisplayData: displayData)

        // then
        XCTAssert(dummyView.isCalledShowDidacticArea == false)

    }
    
    // MARK: Didactic Area button pressed
    
    func test_givenDelegateAndHelpIdEmpty_whenICallDidacticAreaButtonPressed_thenICallDelegateDidacticButtonPressed() {
        
        // given
        let dummyDelegate = DummyDidacticAreaPresenterDelegate()
        sut.delegate = dummyDelegate
        
        let helpId = ""
        
        // when
        sut.didacticAreaButtonPressed(withHelpId: helpId)
        
        // then
        XCTAssert(dummyDelegate.isCalledDidacticButtonPressed == true)
        
    }
    
    func test_givenDelegateAndHelpId_whenICallDidacticAreaButtonPressed_thenICallDelegateDidacticButtonPressed() {
        
        // given
        let dummyDelegate = DummyDidacticAreaPresenterDelegate()
        sut.delegate = dummyDelegate
        
        let helpId = "helpId"
        
        // when
        sut.didacticAreaButtonPressed(withHelpId: helpId)
        
        // then
        XCTAssert(dummyDelegate.isCalledDidacticButtonPressed == true)
        
    }
    
    func test_givenDelegateAndHelpIdNil_whenICallDidacticAreaButtonPressed_thenINotCallDelegateDidacticButtonPressed() {
        
        // given
        let dummyDelegate = DummyDidacticAreaPresenterDelegate()
        sut.delegate = dummyDelegate
        
        // when
        sut.didacticAreaButtonPressed(withHelpId: nil)
        
        // then
        XCTAssert(dummyDelegate.isCalledDidacticButtonPressed == false)
        
    }
    
    func test_givenDelegateAndHelpId_whenICallDidacticAreaButtonPressed_thenHelpIdSentMatchToHelpId() {
        
        // given
        let dummyDelegate = DummyDidacticAreaPresenterDelegate()
        sut.delegate = dummyDelegate
        
        let helpId = "helpId"
        
        // when
        sut.didacticAreaButtonPressed(withHelpId: helpId)
        
        // then
        XCTAssert(dummyDelegate.helpIdSent == helpId)
        
    }
    
    // MARK: Didactic Area offer button pressed

    func test_givenDelegateAndTitleAndCategoryIdEmpty_whenICallDidacticAreaOfferButtonPressed_thenICallDelegateOfferButtonPressed() {

        // given
        let dummyDelegate = DummyDidacticAreaPresenterDelegate()
        sut.delegate = dummyDelegate

        let categoryId = ""
        let title = "title1"

        // when
        sut.didacticAreaOfferButtonPressed(withCategoryId: categoryId, andTitle: title)

        // then
        XCTAssert(dummyDelegate.isCalledOfferButtonPressed == true)

    }

    func test_givenDelegateAndTitleAndCategoryId_whenICallDidacticAreaOfferButtonPressed_thenICallDelegateOfferButtonPressed() {

        // given
        let dummyDelegate = DummyDidacticAreaPresenterDelegate()
        sut.delegate = dummyDelegate

        let categoryId = "categoryId"
        let title = "title1"
        
        // when
        sut.didacticAreaOfferButtonPressed(withCategoryId: categoryId, andTitle: title)

        // then
        XCTAssert(dummyDelegate.isCalledOfferButtonPressed == true)

    }

    func test_givenDelegateAndTitleAndCategoryIdNil_whenICallDidacticAreaOfferButtonPressed_thenINotCallDelegateOfferButtonPressed() {

        // given
        let dummyDelegate = DummyDidacticAreaPresenterDelegate()
        sut.delegate = dummyDelegate
        let title = "title1"
        
        // when
        sut.didacticAreaOfferButtonPressed(withCategoryId: nil, andTitle: title)

        // then
        XCTAssert(dummyDelegate.isCalledOfferButtonPressed == false)
    }

    func test_givenDelegateAndTitleAndCategoryId_whenICallDidacticAreaOfferButtonPressed_thenCategoryIdSentAndTitleSentMatchToCategoryIdAndTitle() {

        // given
        let dummyDelegate = DummyDidacticAreaPresenterDelegate()
        sut.delegate = dummyDelegate

        let categoryId = "categoryId"
        let title = "title1"
        
        // when
        sut.didacticAreaOfferButtonPressed(withCategoryId: categoryId, andTitle: title)

        // then
        XCTAssert(dummyDelegate.categoryIdSent == categoryId)
        XCTAssert(dummyDelegate.titleSent == title)
    }

    // MARK: Didactic Area special offer button pressed
    
    func test_givenDelegateAndURLAndTitle_whenICallDidacticAreaSpecialOfferButtonPressed_thenICallDelegateOfferButtonPressed() {
        
        // given
        let dummyDelegate = DummyDidacticAreaPresenterDelegate()
        sut.delegate = dummyDelegate
        
        let url = URL(string: "www.bancomer.com")
        let title = "title1"
        
        // when
        sut.didacticAreaSpecialOfferButtonPressed(withURL: url!, andTitle: title)
        
        // then
        XCTAssert(dummyDelegate.isCalledSpecialOfferButtonPressed == true)
    }
    
    func test_givenDelegateAndURLNilAndTitle_whenICallDidacticAreaSpecialOfferButtonPressed_thenIDontCallDelegateOfferButtonPressed() {
        
        // given
        let dummyDelegate = DummyDidacticAreaPresenterDelegate()
        sut.delegate = dummyDelegate
        
        let title = "title1"
        
        // when
        sut.didacticAreaSpecialOfferButtonPressed(withURL: nil, andTitle: title)
        
        // then
        XCTAssert(dummyDelegate.isCalledSpecialOfferButtonPressed == false)
    }
    
    func test_givenDelegateAndURLAndTitleNil_whenICallDidacticAreaSpecialOfferButtonPressed_thenIDontCallDelegateOfferButtonPressed() {
        
        // given
        let dummyDelegate = DummyDidacticAreaPresenterDelegate()
        sut.delegate = dummyDelegate
        
        let url = URL(string: "www.bancomer.com")
        
        // when
        sut.didacticAreaSpecialOfferButtonPressed(withURL: url, andTitle: nil)
        
        // then
        XCTAssert(dummyDelegate.isCalledSpecialOfferButtonPressed == false)
    }
    
    func test_givenDelegateAndNilURLAndNilTitle_whenICallDidacticAreaSpecialOfferButtonPressed_thenIDontCallDelegateOfferButtonPressed() {
        
        // given
        let dummyDelegate = DummyDidacticAreaPresenterDelegate()
        sut.delegate = dummyDelegate
        
        // when
        sut.didacticAreaSpecialOfferButtonPressed(withURL: nil, andTitle: nil)
        
        // then
        XCTAssert(dummyDelegate.isCalledSpecialOfferButtonPressed == false)
    }
    
    func test_givenDelegateAndURLAndTitle_whenICallDidacticAreaSpecialOfferButtonPressed_thenSentURLAndTitleMatchToCategoryId() {
        
        // given
        let dummyDelegate = DummyDidacticAreaPresenterDelegate()
        sut.delegate = dummyDelegate
        
        let url = URL(string: "www.bancomer.com")
        let title = "title1"
        
        // when
        sut.didacticAreaSpecialOfferButtonPressed(withURL: url!, andTitle: title)
        
        // then
        XCTAssert(dummyDelegate.urlSent == url)
        XCTAssert(dummyDelegate.titleSent == title)
    }

    class DummyView: DidacticAreaViewProtocol {

        var isCalledShowDidacticArea = false
        var isCalledShowDidacticAreaOffer = false

        var sentDisplayData: DidacticAreaDisplayData?

        func showDidacticArea(withDisplayData displayData: DidacticAreaDisplayData) {
            
            isCalledShowDidacticArea = true
            sentDisplayData = displayData
        }

        func showDidacticAreaOffer(withDisplayData displayData: DidacticAreaDisplayData) {
            
            isCalledShowDidacticAreaOffer = true
            sentDisplayData = displayData
        }
    }

    class DummyDidacticAreaPresenterDelegate: DidacticAreaPresenterDelegate {

        var isCalledOfferButtonPressed = false
        var isCalledDidacticButtonPressed = false
        var isCalledSpecialOfferButtonPressed = false
        var categoryIdSent: String?
        var helpIdSent: String?
        var urlSent: URL?
        var titleSent: String?
        
        func didacticAreaPresenterDidacticButtonPressed(_ didacticAreaPresenter: DidacticAreaPresenter, andHelpId helpId: String) {
            
            isCalledDidacticButtonPressed = true
            helpIdSent = helpId
        }
        
        func didacticAreaPresenterOfferButtonPressed(_ didacticAreaPresenter: DidacticAreaPresenter, categoryId: String, andTitle title: String) {
            
            isCalledOfferButtonPressed = true
            categoryIdSent = categoryId
            titleSent = title
        }
        
        func didacticAreaPresenterSpecialOfferButtonPressed(_ didacticAreaPresenter: DidacticAreaPresenter, _ url: URL, andTitle title: String) {
            
            isCalledSpecialOfferButtonPressed = true
            urlSent = url
            titleSent = title
        }
    }

}
