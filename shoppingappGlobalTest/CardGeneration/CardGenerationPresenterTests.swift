//
//  CardGenerationPresenterTests.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 15/6/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest
import CellsNativeComponents

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class CardGenerationPresenterTests: XCTestCase {

    var sut: CardGenerationPresenter<CardGenerationViewProtocolDummy>!
    var dummyBusManager: DummyBusManager!
    var dummyView: CardGenerationViewProtocolDummy!

    override func setUp() {
        super.setUp()
        dummyBusManager = DummyBusManager()
        sut = CardGenerationPresenter<CardGenerationViewProtocolDummy>(busManager: dummyBusManager)

        dummyView = CardGenerationViewProtocolDummy()
        sut.view = dummyView
    }

    // MARK: - setModel

    func test_givenErrorBO_whenICallSetModel_thenErrorBOBeFilled() {

        // given
        let errorBO = ErrorBO(error: ErrorEntity(message: "Error"))

        // when
        sut.setModel(model: errorBO)

        // then
        XCTAssert(sut.errorBO === errorBO)

    }

    // MARK: - showPageSuccess

    func test_givenGeneratedCardBO_whenShowPageSuccess_thenGeneratedCardBONotNil() {
        
        //given
        let generatedCardDTO = GeneratedCardDTO()
        generatedCardDTO.data = GeneratedCardDataDTO()
        generatedCardDTO.data?.operationNumber = "12345678"
        let generatedCardBO = GeneratedCardBO(generatedCardDTO: generatedCardDTO)
        
        //when
        sut.showPageSuccess(modelBO: generatedCardBO)
        
        //then
        XCTAssert(sut.generatedCardBO != nil)
    }
    
    func test_givenGeneratedCardBO_whenShowPageSuccess_thenGeneratedCardBOMatch() {
        
        //given
        let generatedCardDTO = GeneratedCardDTO()
        generatedCardDTO.data = GeneratedCardDataDTO()
        generatedCardDTO.data?.operationNumber = "12345678"
        let generatedCardBO = GeneratedCardBO(generatedCardDTO: generatedCardDTO)
        
        //when
        sut.showPageSuccess(modelBO: generatedCardBO)
        
        //then
        XCTAssert(sut.generatedCardBO?.folio == generatedCardBO.folio)
    }
    
    func test_whenShowPageSuccess_thenICalledNavigateToGenerateSuccessPage() {

        //given
        let generatedCardDTO = GeneratedCardDTO()
        generatedCardDTO.data = GeneratedCardDataDTO()
        generatedCardDTO.data?.operationNumber = "12345678"
        let generatedCardBO = GeneratedCardBO(generatedCardDTO: generatedCardDTO)
        sut.generatedCardBO = generatedCardBO

        //when
        sut.showPageSuccess(modelBO: generatedCardBO)

        //then
        XCTAssert(dummyBusManager.isCalledNavigateToGenerateSuccessPage == true)

    }

    func test_givenAGenerateCardSuccessObjectCells_whenShowPageSuccess_thenICalledPublishParamGenerateCardSuccess() {

        //given
        let generatedCardDTO = GeneratedCardDTO()
        generatedCardDTO.data = GeneratedCardDataDTO()
        generatedCardDTO.data?.operationNumber = "12345678"
        let generatedCardBO = GeneratedCardBO(generatedCardDTO: generatedCardDTO)
        sut.generatedCardBO = generatedCardBO
        //when
        sut.showPageSuccess(modelBO: generatedCardBO)

        //then
        XCTAssert(dummyBusManager.isCalledPublishParamGenerateCardSuccess == true)
        XCTAssert(sut.generatedCardBO?.folio == dummyBusManager.dummyGeneratedCardBO.folio)

    }

    func test_givenAGenerateCardSuccessObjectCell_whenICallShowPageSuccess_thenICalledPublishParamToBlockCardSuccess() {

        //given
        let generatedCardDTO = GeneratedCardDTO()
        generatedCardDTO.data = GeneratedCardDataDTO()
        generatedCardDTO.data?.operationNumber = "12345678"
        let generatedCardBO = GeneratedCardBO(generatedCardDTO: generatedCardDTO)
        sut.generatedCardBO = generatedCardBO
        let cardOperationInfo = CardOperationInfo(cardID: "111", fisicalCardID: nil, cardType: CardDataType.digital, cardOperation: .cancel)
        sut.cardOperationInfo = cardOperationInfo

        //when
        sut.showPageSuccess(modelBO: generatedCardBO)

        //then
        XCTAssert(dummyBusManager.isCalledPublishParamBlockCardInfoSuccess == true)

    }

    func test_givenAGenerateCardSuccessObjectCell_whenICallShowPageSuccess_thenCallPublishOperationCardInfoSuccessWithRightData() {

        //given
        let generatedCardDTO = GeneratedCardDTO()
        generatedCardDTO.data = GeneratedCardDataDTO()
        generatedCardDTO.data?.operationNumber = "12345678"
        let generatedCardBO = GeneratedCardBO(generatedCardDTO: generatedCardDTO)
        sut.generatedCardBO = generatedCardBO
        let cardOperationInfo = CardOperationInfo(cardID: "111", fisicalCardID: nil, cardType: CardDataType.digital, cardOperation: .cancel)
        sut.cardOperationInfo = cardOperationInfo

        // when
        sut.showPageSuccess(modelBO: generatedCardBO)

        //then
        XCTAssert(dummyBusManager.cardOperationInfo?.cardID == cardOperationInfo.cardID)
        XCTAssert(dummyBusManager.cardOperationInfo?.fisicalCardID == cardOperationInfo.fisicalCardID)
        XCTAssert(dummyBusManager.cardOperationInfo?.cardType == cardOperationInfo.cardType)
        XCTAssert(dummyBusManager.cardOperationInfo?.cardOperation == cardOperationInfo.cardOperation)

    }

    func test_givenAGenerateCardSuccessObjectCells_whenICallShowPageSuccess_thenCallPublishNeededRefreshCards() {

        //given
        let generatedCardDTO = GeneratedCardDTO()
        generatedCardDTO.data = GeneratedCardDataDTO()
        generatedCardDTO.data?.operationNumber = "12345678"
        let generatedCardBO = GeneratedCardBO(generatedCardDTO: generatedCardDTO)
        sut.generatedCardBO = generatedCardBO
        //when
        sut.showPageSuccess(modelBO: generatedCardBO)

        //then
        XCTAssert(dummyBusManager.isCalledPublishNeededCardsRefresh == false)

    }

    // MARK: - registerOmniture

    func test_givenPage_whenICallRegisterOmniture_thenICallViewSendTrackCellsPageGenerateCard() {

        // given
        let page = CardGenerationPageReaction.CELLS_PAGE_GENERATE_CARD

        // when
        sut.registerOmniture(page: page)

        // then
        XCTAssert(dummyView.isCalledSendTrackCellsPageGenerateCard == true)

    }

    func test_givenPage_whenICallRegisterOmniture_thenICallViewSendTrackCellsPageTermsConditions() {

        // given
        let page = CardGenerationPageReaction.CELLS_PAGE_TERMS_CONDITIONS

        // when
        sut.registerOmniture(page: page)

        // then
        XCTAssert(dummyView.isCalledSendTrackCellsPageTermsConditions == true)

    }

    func test_givenPage_whenICallRegisterOmniture_thenICallViewSendTrackCellsPageGenerateCardSuccess() {

        // given
        let page = CardGenerationPageReaction.CELLS_PAGE_GENERATE_CARD_SUCCESS

        // when
        sut.registerOmniture(page: page)

        // then
        XCTAssert(dummyView.isCalledSendTrackCellsPageGenerateCardSuccess == true)

    }
    
    func test_givenPage_whenICallRegisterOmniture_thenICallBusPublishDataEventTrackerNewTrackEventw() {
        
        // given
        let page = CardGenerationPageReaction.CELLS_PAGE_GENERATE_CARD_SUCCESS
        
        // when
        sut.registerOmniture(page: page)
        
        // then
        XCTAssert(dummyBusManager.isCalledPublishDataEventTrackerNewTrackEvent == true)

    }
    
    func test_givenPage_whenICallRegisterOmniture_thenICallBusPublishDataEventTrackerNewTrackEventWithAppropiatedEventModel() {
        
        // given
        let page = CardGenerationPageReaction.CELLS_PAGE_GENERATE_CARD_SUCCESS
        
        // when
        sut.registerOmniture(page: page)
        
        // then
        XCTAssert(dummyBusManager.eventSent != nil)
        XCTAssert(dummyBusManager.eventSent?.type == .generateDigitalCard)

    }
    
    // MARK: - viewWasDissmised

    func test_givenSuccessOk_whenICallViewWasDismiss_thenCallPublishNeededRefreshCards() {

        //given
        let generatedCardDTO = GeneratedCardDTO()
        generatedCardDTO.data = GeneratedCardDataDTO()
        generatedCardDTO.data?.operationNumber = "12345678"
        let generatedCardBO = GeneratedCardBO(generatedCardDTO: generatedCardDTO)
        sut.generatedCardBO = generatedCardBO

        sut.showPageSuccess(modelBO: generatedCardBO)

        //when
        sut.viewWasDismissed()

        //then
        XCTAssert(dummyBusManager.isCalledPublishNeededCardsRefresh == true)

    }

    class CardGenerationViewProtocolDummy: CardGenerationViewProtocol {

        var isCalledSendTrackCellsPageGenerateCard = false
        var isCalledSendTrackCellsPageTermsConditions = false
        var isCalledSendTrackCellsPageGenerateCardSuccess = false

        func showError(error: ModelBO) {
        }

        func changeColorEditTexts() {
        }

        func sendTrackCellsPageGenerateCard() {
            isCalledSendTrackCellsPageGenerateCard = true
        }

        func sendTrackCellsPageTermsConditions() {
            isCalledSendTrackCellsPageTermsConditions = true
        }

        func sendTrackCellsPageGenerateCardSuccess() {
            isCalledSendTrackCellsPageGenerateCardSuccess = true
        }
    }

    class DummyBusManager: BusManager {

        var isCalledNavigateToGenerateSuccessPage = false
        var isCalledPublishParamGenerateCardSuccess = false
        var isCalledPublishNeededCardsRefresh = false
        var isCalledPublishParamBlockCardInfoSuccess = false
        var isCalledPublishDataEventTrackerNewTrackEvent = false

        var dummyGeneratedCardBO: GeneratedCardBO!
        var cardOperationInfo: CardOperationInfo?
        var eventSent: EventProtocol?

        override init() {
            super.init()
            routerFactory = RouterInitFactory()
        }

        override func navigateScreen<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {

            if tag == CardGenerationPageReaction.ROUTER_TAG && event === CardGenerationPageReaction.EVENT_CARD_GENERATED_SUCCESS {
                isCalledNavigateToGenerateSuccessPage = true
            }
        }

        override func publishData<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {

            if tag == CardGenerationPageReaction.ROUTER_TAG && event === CardGenerationPageReaction.PARAMS_GENERATE_CARD_SUCCESS {
                isCalledPublishParamGenerateCardSuccess = true
                
                if let jsonData = try? JSONSerialization.data(withJSONObject: values, options: []) {
                    dummyGeneratedCardBO = try? JSONDecoder().decode(GeneratedCardBO.self, from: jsonData)
                }

            } else if tag == CardGenerationPageReaction.ROUTER_TAG && event === CardsPageReaction.PARAMS_OPERATION_CARD_INFO_SUCCESS {
                isCalledPublishParamBlockCardInfoSuccess = true
                cardOperationInfo = values as? CardOperationInfo
            } else if tag == EventTracker.id && event === PageReactionConstants.NEW_TRACK_EVENT {
                isCalledPublishDataEventTrackerNewTrackEvent = true
                eventSent = values as? EventProtocol
            }

        }

        override func notify<T>(tag: String, _ event: ActionSpec<T>) {
            if tag == CardsPageReaction.ROUTER_TAG && event === CardsPageReaction.NEEDED_CARDS_REFRESH {
                isCalledPublishNeededCardsRefresh = true
            }
        }
    }

}
