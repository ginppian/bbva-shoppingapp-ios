//
//  CategoriesBOTests.swift
//  shoppingapp
//
//  Created by jesus.martinez on 13/3/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class CategoriesBOTests: XCTestCase {

    // MARK: - CategoryType

    func test_givenCategoriesBOAndCategoryTypeContained_whenICallGetCategoryName_thenReturnTheNameOfCategoryBO() {

        // given
        let sut = CategoryType.auto
        let categoriesBO = PromotionsGenerator.getCategoryBO()

        // when
        let name = sut.getCategoryName(fromCategories: categoriesBO)

        // then
        var nameExpected = ""

        for category in categoriesBO.categories where category.id == sut {
                nameExpected = category.name
        }

        XCTAssert(name == nameExpected)

    }

    func test_givenCategoriesBOAndCategoryTypeNotContained_whenICallGetCategoryName_thenReturnNil() {

        // given
        let sut = CategoryType.unknown
        let categoriesBO = PromotionsGenerator.getCategoryBO()

        // when
        let name = sut.getCategoryName(fromCategories: categoriesBO)

        // then
        XCTAssert(name == nil)

    }

    func test_givenCategoriesBOEmptyAndCategoryTypeValid_whenICallGetCategoryName_thenReturnNil() {

        // given
        let sut = CategoryType.unknown
        let categoriesBO = PromotionsGenerator.getCategoryBO()
        categoriesBO.categories = [CategoryBO]()

        // when
        let name = sut.getCategoryName(fromCategories: categoriesBO)

        // then
        XCTAssert(name == nil)

    }

    // MARK: - sortAlphabeticallyByName

    func test_givenCategoriesNotSorted_whenICallSortAlphabeticallyByName_thenCategoriesAreSortedByName() {

        // given
        let categoriesBO = PromotionsGenerator.getNotSortCategoryBO()

        // when
        categoriesBO.sortAlphabeticallyByName()

        // then
        var areSort = true
        for i in 0..<(categoriesBO.categories.count - 1) where categoriesBO.categories[i].name > categoriesBO.categories[i + 1].name {
                areSort = false
                break
        }

        XCTAssert(areSort == true)

    }

    // MARK: - favoriteCategoryTypes

    func test_givenCategories_whenICallSortAlphabeticallyByName_thenCategoriesAreSortedByName() {

        // given
        let categoriesBO = PromotionsGenerator.getCategoryBO()
        for i in 0..<categoriesBO.categories.count {
            categoriesBO.categories[i].isFavourite = false
        }
        categoriesBO.categories[3].isFavourite = true
        categoriesBO.categories[8].isFavourite = true
        categoriesBO.categories[20].isFavourite = true

        // when
        let result = categoriesBO.favoriteCategoryTypes()

        // then
        XCTAssert(result == [categoriesBO.categories[3].id, categoriesBO.categories[8].id, categoriesBO.categories[20].id])
    }

}
