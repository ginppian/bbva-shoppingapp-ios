//
//  LoginPresenterATest.swift
//  shoppingapp
//
//  Created by Javier Pino on 21/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import XCTest
import RxSwift

#if TESTPE
    @testable import shoppingappPE
#endif

class LoginPresenterATest: XCTestCase {

    var sut: LoginPresenterA<DummyViewA>!
    var dummyBusManager: DummyBusManager!

    override func setUp() {
        super.setUp()
        KeychainManagerTestHelper.configureKeychainManagerForTesting()
        dummyBusManager = DummyBusManager()
        sut = LoginPresenterA<DummyViewA>(busManager: dummyBusManager)
    }

    override func tearDown() {
        super.tearDown()
    }

    // MARK: - selectedIdentificationDocument

    func test_givenAnyValue_whenISetSelectedIndexAndIdentificationDocumentsBothToNil_thenSelectedDocumentIsNil() {

        //given

        //when
        sut.pickerOptions = nil
        sut.identificationDocuments = nil

        //then
        XCTAssert(sut.selectedIdentificationDocument == nil)
    }

    func test_givenAnyValue_whenISetSomeSelectedIndexAndIdentificationDocumentsToNil_thenSelectedDocumentIsNotNil() {

        //given

        //when
        sut.pickerOptions = LoginPresenterADummyDataGenerator.dummyPickerOptionsDisplayWithSelectedIndex()
        sut.identificationDocuments = nil

        //then
        XCTAssert(sut.selectedIdentificationDocument == nil)
    }

    func test_givenAnyValue_whenISetSelectedIndexToNilAndSomeIdentificationDocuments_thenSelectedDocumentIsNotNil() {

        //given

        //when
        sut.pickerOptions = nil
        sut.identificationDocuments = LoginPresenterADummyDataGenerator.dummyIdentificationDocuments()

        //then
        XCTAssert(sut.selectedIdentificationDocument == nil)
    }

    func test_givenAnyValue_whenISetSelectedIndexAndIdentificationDocuments_thenSelectedDocumentIsNotNil() {

        //given

        //when
        sut.pickerOptions = LoginPresenterADummyDataGenerator.dummyPickerOptionsDisplayWithSelectedIndex()
        sut.identificationDocuments = LoginPresenterADummyDataGenerator.dummyIdentificationDocuments()

        //then
        XCTAssert(sut.selectedIdentificationDocument != nil)
    }

    func test_givenAnyValue_whenISetSelectedIndexAndIdentificationDocuments_thenSelectedDocumentIsCorrectOne() {

        //given

        //when
        sut.pickerOptions = LoginPresenterADummyDataGenerator.dummyPickerOptionsDisplayWithSelectedIndex()
        sut.identificationDocuments = LoginPresenterADummyDataGenerator.dummyIdentificationDocuments()

        //then
        let selectedIdentificationDocument = sut.identificationDocuments![sut.pickerOptions!.selectedIndex!]
        XCTAssert(sut.selectedIdentificationDocument === selectedIdentificationDocument)
    }

    // MARK: - viewDidLoad

    func test_givenAnyValue_whenICallViewDidLoad_thenProvideIdentificationDocumentsIsCalled() {

        //given
        let dummyInteractorA = DummyInteractorA()
        sut.interactor = dummyInteractorA

        //when
        sut.viewDidLoad()

        //then
        XCTAssert(dummyInteractorA.isCalledProvideIdentificationDocuments == true)
    }

    func test_givenAnyValue_whenICallViewDidLoad_thenIdentificationDocumentHasSameDocumentsAsInteractorReturns() {

        //given
        let dummyInteractorA = DummyInteractorA()
        sut.interactor = dummyInteractorA

        //when
        sut.viewDidLoad()

        //then
        XCTAssert(  (sut.identificationDocuments?[0] === dummyInteractorA.identificationDocuments[0]) &&
                    (sut.identificationDocuments?[1] === dummyInteractorA.identificationDocuments[1]))
    }

    func test_givenAnyValue_whenICallViewDidLoad_thenPickerOptionsTitlesHasSameTitlesAsInteractorReturns() {

        //given
        let dummyInteractorA = DummyInteractorA()
        sut.interactor = dummyInteractorA

        //when
        sut.viewDidLoad()

        //then
        XCTAssert(  (sut.pickerOptions?.titles?[0] == dummyInteractorA.identificationDocuments[0].title) &&
                    (sut.pickerOptions?.titles?[1] == dummyInteractorA.identificationDocuments[1].title))
    }

    // MARK: - identificationDocumentPickerPressed

    func test_givenSomePickerOptions_whenICallIdentificationDocumentPickerPressed_thenPresenPickerIsCalled() {

        //given
        let dummyInteractorA = DummyInteractorA()
        sut.interactor = dummyInteractorA
        let titles = ["1", "2"]
        let pickerMock = PickerOptionsDisplay()
        pickerMock.titles = titles
        sut.pickerOptions = pickerMock

        //when
        sut.identificationDocumentPickerPressed()

        //then
        XCTAssert(dummyBusManager.isCalledShowPicker == true)
    }

    func test_givenSomePickerOptions_whenICallIdentificationDocumentPickerPressed_thenPickerDelegateIsSetWithPresenter() {

        //given
        let dummyInteractorA = DummyInteractorA()
        sut.interactor = dummyInteractorA
        let titles = ["1", "2"]
        let pickerMock = PickerOptionsDisplay()
        pickerMock.titles = titles
        sut.pickerOptions = pickerMock

        //when
        sut.identificationDocumentPickerPressed()

        //then
        XCTAssert(dummyBusManager.pickerDelegate === sut)
    }

    func test_givenSomePickerOptions_whenICallIdentificationDocumentPickerPressed_thenPickerOptionsPassedToRouterAreTheSameAsPresenter() {

        //given
        let dummyInteractorA = DummyInteractorA()
        sut.interactor = dummyInteractorA
        let titles = ["1", "2"]
        let pickerMock = PickerOptionsDisplay()
        pickerMock.titles = titles
        sut.pickerOptions = pickerMock

        //when
        sut.identificationDocumentPickerPressed()

        //then
        XCTAssert(dummyBusManager.pickerOptions === sut.pickerOptions)
    }

    // MARK: - didSelectPickerOption

    func test_givenSomeIndex_whenICallDidSelectPickerOption_thenUpdateDocumentTextIsCalled() {

        //given
        let dummyView = DummyViewA()
        sut.view = dummyView
        let titles = ["1", "2"]
        let pickerMock = PickerOptionsDisplay()
        pickerMock.titles = titles
        sut.pickerOptions = pickerMock

        //when
        sut.didSelectPickerOption(atIndex: 1)

        //then
        XCTAssert(dummyView.isCalledUpdateDocumentText == true)
    }

    func test_givenAnIndex_whenICallDidSelectPickerOption_thenSelectedIndexIsSetWithSameValue() {

        //given
        let dummyView = DummyViewA()
        sut.view = dummyView
        let titles = ["1", "2"]
        let pickerMock = PickerOptionsDisplay()
        pickerMock.titles = titles
        sut.pickerOptions = pickerMock

        let indexPressed = 1

        //when
        sut.didSelectPickerOption(atIndex: indexPressed)

        //then
        XCTAssert(sut.pickerOptions?.selectedIndex == indexPressed)
    }

    func test_givenAnIndex_whenICallDidSelectPickerOption_thenViewHasCorrectDocumentTitle() {

        //given
        let dummyView = DummyViewA()
        sut.view = dummyView
        let titles = ["1", "2"]
        let pickerMock = PickerOptionsDisplay()
        pickerMock.titles = titles
        sut.pickerOptions = pickerMock

        //when
        sut.didSelectPickerOption(atIndex: 1)

        //then
        XCTAssert(dummyView.documentTitle == sut.pickerOptions?.titles?[1])
    }

    // MARK: - checkEnterButtonState

    func test_givenAnLoginTransportWithFilledFieldsAndIdentificationDocumentsWithoutSelectedIndex_whenICallCheckEnterButtonState_thenCallShowCheckEnterButtonStateWithFalseBoolValue() {
        //given
        let loginTransport = LoginTransport(username: "user", password: "pass")
        let dummyView = DummyViewA()
        sut.view = dummyView
        sut.pickerOptions = LoginPresenterADummyDataGenerator.dummyPickerOptionsDisplayWithoutSelectedIndex()
        sut.identificationDocuments = LoginPresenterADummyDataGenerator.dummyIdentificationDocuments()

        //when
        sut.checkEnterButtonState(loginTransport: loginTransport)

        //then
        XCTAssert(dummyView.isCalledCheckEnterButtonStateWithFalseBool == true)
        XCTAssert(dummyView.isCalledCheckEnterButtonStateWithTrueBool == false)

    }

    func test_givenAnLoginTransportWithFilledFieldsAndSelectedIndexWithoutIdentificationDocuments_whenICallCheckEnterButtonState_thenCallShowCheckEnterButtonStateWithFalseBoolValue() {
        //given
        let loginTransport = LoginTransport(username: "user", password: "pass")
        let dummyView = DummyViewA()
        sut.view = dummyView
        sut.pickerOptions = LoginPresenterADummyDataGenerator.dummyPickerOptionsDisplayWithSelectedIndex()
        sut.identificationDocuments = nil

        //when
        sut.checkEnterButtonState(loginTransport: loginTransport)

        //then
        XCTAssert(dummyView.isCalledCheckEnterButtonStateWithFalseBool == true)
        XCTAssert(dummyView.isCalledCheckEnterButtonStateWithTrueBool == false)
    }

    func test_givenAnLoginTransportWithEmptyUserAndSelectedIdentificationDocument_whenICallCheckEnterButtonState_thenCallShowCheckEnterButtonStateWithFalseBoolValue() {

        //given
        let loginTransport = LoginTransport(username: "", password: "pass")
        let dummyView = DummyViewA()
        sut.view = dummyView
        sut.pickerOptions = LoginPresenterADummyDataGenerator.dummyPickerOptionsDisplayWithSelectedIndex()
        sut.identificationDocuments = LoginPresenterADummyDataGenerator.dummyIdentificationDocuments()

        //when
        sut.checkEnterButtonState(loginTransport: loginTransport)

        //then
        XCTAssert(dummyView.isCalledCheckEnterButtonStateWithFalseBool == true)
        XCTAssert(dummyView.isCalledCheckEnterButtonStateWithTrueBool == false)

    }

    func test_givenAnLoginTransportWithEmptyPasswordAndSelectedIdentificationDocument_whenICallCheckEnterButtonState_thenCallShowCheckEnterButtonStateWithFalseBoolValue() {

        //given
        let loginTransport = LoginTransport(username: "user", password: "")
        let dummyView = DummyViewA()
        sut.view = dummyView
        sut.pickerOptions = LoginPresenterADummyDataGenerator.dummyPickerOptionsDisplayWithSelectedIndex()
        sut.identificationDocuments = LoginPresenterADummyDataGenerator.dummyIdentificationDocuments()

        //when
        sut.checkEnterButtonState(loginTransport: loginTransport)

        //then
        XCTAssert(dummyView.isCalledCheckEnterButtonStateWithFalseBool == true)
        XCTAssert(dummyView.isCalledCheckEnterButtonStateWithTrueBool == false)

    }

    func test_givenAnLoginTransportWithFilledFieldsAndSelectedIdentificationDocument_whenICallCheckEnterButtonState_thenCallShowCheckEnterButtonStateWithTrueBoolValue() {

        //given
        let loginTransport = LoginTransport(username: "user", password: "password")
        let dummyView = DummyViewA()
        sut.view = dummyView
        sut.pickerOptions = LoginPresenterADummyDataGenerator.dummyPickerOptionsDisplayWithSelectedIndex()
        sut.identificationDocuments = LoginPresenterADummyDataGenerator.dummyIdentificationDocuments()

        //when
        sut.checkEnterButtonState(loginTransport: loginTransport)

        //then
        XCTAssert(dummyView.isCalledCheckEnterButtonStateWithFalseBool == false)
        XCTAssert(dummyView.isCalledCheckEnterButtonStateWithTrueBool == true)

    }

    // MARK: - Dummy Classes

    class DummyViewA: LoginViewProtocolA {
        func showLoggedAnimation() {

        }

        func showAnimationLoggedView() {
            //
        }

        func showLoggedView(withDisplayData displayData: LoginDisplayData) {
            //
        }

        var isCalledUpdateDocumentText = false
        var isCalledCheckEnterButtonStateWithTrueBool = false
        var isCalledCheckEnterButtonStateWithFalseBool = false

        var documentTitle = ""

        func updateIndentificationDocumentText(text: String) {
            isCalledUpdateDocumentText = true
            documentTitle = text
        }

        func showLogin() {
        }

        func showAnimation() {
        }

        func removeTextInputUser(value: String) {
        }

        func removeTextInputPass(value: String) {
        }

        func showCheckEnterButtonState(state: Bool) {
            if state == false {
                isCalledCheckEnterButtonStateWithFalseBool = true
            } else {
                isCalledCheckEnterButtonStateWithTrueBool = true
            }
        }

        func showSecureVisualizeIcon(state: Bool) {
        }

        func showSecureHideIcon(state: Bool) {
        }

        func askLocationPrivileges() {
        }

        func showUserViewStatusNormal() {
        }

        func showPassViewStatusNormal() {
        }

        func showError(error: ModelBO) {
        }

        func changeColorEditTexts() {
        }

        func moveFocusToPasswordTextInput() {
        }

        func dismissKeyboard() {
        }

        func showLoginAnimation() {
        }
    }

    class DummyInteractorA: LoginInteractorProtocolA {

        var isCalledProvideIdentificationDocuments = false

        var identificationDocuments: [IdentificationDocumentBO] = []

        func provideIdentificationDocuments() -> Observable<[ModelBO]> {
            isCalledProvideIdentificationDocuments = true

            let document1 = IdentificationDocumentBO(code: "1", title: "1")
            let document2 = IdentificationDocumentBO(code: "2", title: "2")
            let modelBO: [ModelBO] = [document1, document2]
            identificationDocuments = modelBO as! [IdentificationDocumentBO]
            return Observable<[ModelBO]>.just(modelBO as [ModelBO])
        }
        func provideLogoutData() -> Observable<ModelBO> {

            return Observable<ModelBO>.just(EmptyModelBO())

        }
    }

    class DummyBusManager: BusManager {

        var isCalledShowPicker = false

        var pickerOptions: PickerOptionsDisplay?
        weak var pickerDelegate: PickerPresenterDelegate?

        override init() {
            super.init()
            routerFactory = RouterInitFactory()
        }

        override func showPicker(options: ModelDisplay, delegate: PickerPresenterDelegate) {
            isCalledShowPicker = true
            pickerOptions = options as? PickerOptionsDisplay
            pickerDelegate = delegate
        }

    }
}

class LoginPresenterADummyDataGenerator {

    static func dummyIdentificationDocuments() -> [IdentificationDocumentBO] {

        let dniDocument = IdentificationDocumentBO(code: "L", title: Localizables.login.key_login_dni_text)
        let passportDocument = IdentificationDocumentBO(code: "P", title: Localizables.login.key_login_passport_text)

        return [dniDocument, passportDocument]
    }

    static func dummyPickerOptionsDisplayWithoutSelectedIndex() -> PickerOptionsDisplay {

        let pickerOptionsDisplay = PickerOptionsDisplay()
        pickerOptionsDisplay.titles = [Localizables.login.key_login_dni_text, Localizables.login.key_login_passport_text]

        return pickerOptionsDisplay
    }

    static func dummyPickerOptionsDisplayWithSelectedIndex() -> PickerOptionsDisplay {

        let pickerOptionsDisplay = PickerOptionsDisplay()
        pickerOptionsDisplay.titles = [Localizables.login.key_login_dni_text, Localizables.login.key_login_passport_text]
        pickerOptionsDisplay.selectedIndex = 1

        return pickerOptionsDisplay
    }
}
