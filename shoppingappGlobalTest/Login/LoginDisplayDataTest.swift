//
//  LoginDisplayDataTest.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 10/01/2018.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest
#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class LoginDisplayDataTest: XCTestCase {

    override func setUp() {
        super.setUp()
        
        KeychainManagerTestHelper.configureKeychainManagerForTesting()
    }
    
    func test_givenASavedUser_whenICallGenerateLoginDisplayData_thenIDisplayCorrectData() {

        //given
        KeychainManager.shared.saveFirstname(forFirstname: "firstname")
         let backgroundHeight: CGFloat = 440.0
        var expectedBackgroundHeight: CGFloat = 0.0

        //When
        let displayData = LoginDisplayData.generateLoginDisplayData()

        expectedBackgroundHeight = backgroundHeight
        
        // then
        XCTAssert(displayData.greetingText == Localizables.login.key_login_hello_text)
        XCTAssert(displayData.changeUserText == Localizables.login.key_login_disconnect_change_user_text)
        XCTAssert(displayData.userSavedImage == ConstantsImages.Login.user_saved_image)
        XCTAssert(displayData.userSavedName == KeychainManager.shared.getFirstname().capitalized)
        XCTAssert(displayData.backgroundHeight == expectedBackgroundHeight)
    }

    func test_givenASavedUserAndAttributedText_whenICallGenerateChangeUserDisplayData_thenIDisplayCorrectData() {

        //given
        KeychainManager.shared.saveFirstname(forFirstname: "firstname")

        //When
        let displayData = LoginDisplayData.generateChangeUserDisplayData()

        // then
        XCTAssert(displayData.loginDisconnectMessage != nil)
        XCTAssert(displayData.okButtonTitle == Localizables.login.key_login_disconnect_understood_text)
    }
    
    func test_givenUserIdSmallerThan5AndNotSavedUser_whenICallGenerateLoginDisplayData_thenUserSavedNameIsEmpty() {
        
        //given
        KeychainManager.shared.saveUserId(forUserId: "1")
        
        //When
        let displayData = LoginDisplayData.generateLoginDisplayData()
        
        // then
        XCTAssert(displayData.userSavedName?.isEmpty == true)
    }
    
    func test_givenUserIdMoreThan5AndNotSavedUser_whenICallGenerateLoginDisplayData_thenUserSavedNameIsUserId() {
        
        //given
        KeychainManager.shared.saveUserId(forUserId: "123456")
        
        //When
        let displayData = LoginDisplayData.generateLoginDisplayData()
        
        // then
        XCTAssert(displayData.userSavedName == "\u{25CF}" + KeychainManager.shared.getUserId().suffix(5))
    }
}
