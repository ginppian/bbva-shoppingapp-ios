//
//  LoginPresenterBGMTest.swift
//  shoppingappMXTest
//
//  Created by Jesús Ángel Sánchez Sánchez on 19/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest
import RxSwift
import CellsNativeComponents

#if TESTMX
    @testable import shoppingappMX
#endif

class LoginPresenterBGMTest: XCTestCase {
    
    var sut: LoginPresenterBGM<DummyView>!
    var dummyBusManager: DummyBusManager!
    var dummyView = DummyView()
    
    override func setUp() {
        super.setUp()
        KeychainManagerTestHelper.configureKeychainManagerForTesting()
        dummyBusManager = DummyBusManager()
        sut = LoginPresenterBGM<DummyView>(busManager: dummyBusManager)
        
        dummyView = DummyView()
        sut.view = dummyView
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    // MARK: CheckError
    
    func test_givenAnErrorBOWithError_whenICallCheckErrorIsShownIsFalse_thenCallShowErrorInView() {
        
        //given
        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        sut.errorBO = errorBO
        
        //when
        sut.checkError()
        
        //then
        XCTAssert(dummyView.isCalledShowError == true)
    }
    
    func test_givenAnErrorBO_whenICallCheckError_thenmessageIsSameInView() {
        
        //given
        let errorBO = ErrorBO(error: ErrorEntity(message: "Error en el password"))
        
        errorBO.isErrorShown = false
        sut.errorBO = errorBO
        
        //when
        sut.checkError()
        
        //then
        XCTAssertEqual(dummyView.errorBO?.errorMessage(), errorBO.errorMessage())
    }
    
    func test_givenNoErrorBO_whenICallCheckError_thenNotCallShowErrorInView() {
        
        //given
        
        //when
        sut.checkError()
        
        //then
        XCTAssert(dummyView.isCalledShowError == false)
    }
    
    func test_givenAnErrorBO_whenErrorBOIsShownIsTrueThenNotCallShowModal() {
        
        //given
        let errorBO = ErrorBO(error: ErrorEntity(message: "Error en el password"))
        
        errorBO.isErrorShown = true
        sut.errorBO = errorBO
        
        //when
        sut.checkError()
        
        //then
        XCTAssert(dummyView.isCalledShowError == false)
    }
    
    func test_givenAnErrorBOWithHttpStatusBetween400And499_whenICallCheckErrorIsErrorShown_ThenCallChagenColorEditTexts() {
        
        //given
        for i in 400...499 {
            
            let errorBO = ErrorBO(error: ErrorEntity(message: "Error en el password"))
            
            errorBO.isErrorShown = true
            errorBO.status = i
            sut.errorBO = errorBO
            
            //when
            sut.checkError()
            
            //then
            XCTAssert(dummyView.isCalledChangeColorEditTexts == true)
            
        }
        
    }
    
    func test_givenAnErrorBOWithHttpStatus403_whenICallCheckErrorIsErrorShown_ThenCallChagenColorEditTexts() {
        
        //given
        for i in 400...499 {
            
            let errorBO = ErrorBO(error: ErrorEntity(message: "Error en el password"))
            
            errorBO.isErrorShown = true
            errorBO.status = i
            sut.errorBO = errorBO
            
            //when
            sut.checkError()
            
            //then
            XCTAssert(dummyView.isCalledChangeColorEditTexts == true)
            
        }
        
    }
    
    func test_givenAnErrorBOWithError403AndErrorCode162_whenICallCheckErrorIsShownIsFalse_thenIsErrorShownSetTrue() {
        
        //given
        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        errorBO.status = 403
        errorBO.code = "162"
        sut.errorBO = errorBO
        
        //when
        sut.checkError()
        
        //then
        XCTAssert(sut.errorBO?.isErrorShown == true)
    }
    
    func test_givenAnErrorBOWithError403AndErrorCode162_whenICallCheckErrorIsShownIsFalse_thenICalledNavigateToUserBloqued() {
        
        //given
        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        errorBO.status = 403
        errorBO.code = "162"
        sut.errorBO = errorBO
        
        //when
        sut.checkError()
        
        //then
        XCTAssert(dummyBusManager.isCalledNavigateToUserBloqued == true)
    }
    
    func test_givenAnErrorBOWithErrorDifferentTo403_whenICallCheckErrorIsShownIsFalse_thenICalledShowError() {
        
        //given
        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        errorBO.status = 400
        sut.errorBO = errorBO
        
        //when
        sut.checkError()
        
        //then
        XCTAssert(dummyView.isCalledShowError == true)
    }
    
    func test_givenALoginTransport_whenICallDigitalActivationSuccess_thenIsCalledNavigateToLoginLoader() {
        
        //given
        let loginTransport = LoginTransport(username: "0021223355", password: "123456")
        
        //when
        sut.digitalActivationSuccess(loginTransport: loginTransport)
        
        //then
        XCTAssert(dummyBusManager.isCalledNavigateToLoginLoader == true)
    }
    
    func test_givenALoginTransport_whenICallDigitalActivationSuccess_thenLoginTransportInBusManagerIsTheSame() {
        
        //given
        let loginTransport = LoginTransport(username: "0021223355", password: "123456")
        
        //when
        sut.digitalActivationSuccess(loginTransport: loginTransport)
        
        //then
        XCTAssert(dummyBusManager.loginTransport === loginTransport)
    }

    // MARK: configureToRegisterInGlomo

    func test_given_whenICallConfigureToRegisterInGlomo_thenLaunchRegisterInGlomoIsTrue() {

        //given

        //when
        sut.configureToRegisterInGlomo()

        //then
        XCTAssert(sut.launchRegisterInGlomo == true)
    }

    func test_given_whenICallConfigureToRegisterInGlomo_thenICallViewConfigureToRegisterInGlomo() {

        //given

        //when
        sut.configureToRegisterInGlomo()

        //then
        XCTAssert(dummyView.isCalledConfiguredRegisterInGlomo == true)
    }

    // MARK: - viewDidAppear

    func test_givenNoErrorBOAndNoSavedUserAndAnimationFinishedFalse_whenICallViewDidAppear_thenCallShowAnimation() {
        //given
        let dummyView = DummyView()
        sut.view = dummyView
        KeychainManager.shared.resetKeyChainItems()
        sut.animationFinished = false

        //when
        sut.viewDidAppear()

        //then
        XCTAssert(dummyView.isCalledShowAnimation == true)
        XCTAssert(sut.animationFinished == true)
    }

    func testLaunchRegisterInGlomoTrue_givenNoErrorBOAndNoSavedUserAndAnimationFinishedTrue_whenICallViewDidAppear_thenINotCallShowAnimation() {
        //given
        let dummyView = DummyView()
        sut.view = dummyView
        KeychainManager.shared.resetKeyChainItems()
        sut.animationFinished = false
        sut.launchRegisterInGlomo = true

        //when
        sut.viewDidAppear()

        //then
        XCTAssert(dummyView.isCalledShowAnimation == false)
        XCTAssert(sut.animationFinished == true)
    }

    func testLaunchRegisterInGlomoTrue_givenNoErrorBOAndNoSavedUserAndAnimationFinishedTrue_whenICallViewDidAppear_thenICallViewLaunchRegisterInGlomo() {
        //given
        let dummyView = DummyView()
        sut.view = dummyView
        KeychainManager.shared.resetKeyChainItems()
        sut.animationFinished = false
        sut.launchRegisterInGlomo = true

        //when
        sut.viewDidAppear()

        //then
        XCTAssert(dummyView.isCalledLaunchRegisterInGlomo == true)
    }
    
    func testLaunchRegisterInGlomoTrue_givenNoErrorBOAndNoSavedUserAndAnimationFinishedTrue_whenICallViewDidAppear_thenLaunchRegisterInGlomoIsFalse() {
        //given
        let dummyView = DummyView()
        sut.view = dummyView
        KeychainManager.shared.resetKeyChainItems()
        sut.animationFinished = false
        sut.launchRegisterInGlomo = true
        
        //when
        sut.viewDidAppear()
        
        //then
        XCTAssert(sut.launchRegisterInGlomo == false)
    }

    func testLaunchRegisterInGlomoFalse_givenNoErrorBOAndNoSavedUserAndAnimationFinishedTrue_whenICallViewDidAppear_thenNotCallShowAnimation() {
        //given
        let dummyView = DummyView()
        sut.view = dummyView
        KeychainManager.shared.resetKeyChainItems()
        sut.animationFinished = false
        sut.launchRegisterInGlomo = false

        //when
        sut.viewDidAppear()

        //then
        XCTAssert(dummyView.isCalledShowAnimation == true)
        XCTAssert(sut.animationFinished == true)
    }

    func test_givenAnErrorBO_whenICallViewDidAppear_thenNotCallShowAnimationAndLoggedViewAnimation() {
        //given
        let dummyView = DummyView()
        sut.view = dummyView

        let errorBO = ErrorBO(error: ErrorEntity(message: "Error"))
        sut.errorBO = errorBO

        //when
        sut.viewDidAppear()

        //then
        XCTAssert(dummyView.isCalledShowAnimation == false)
        XCTAssert(dummyView.isCalledShowLogggedAnimation == false)

    }

    func test_givenNoErrorBOAndSavedUserAndAnimationFinishedFalse_whenICallViewDidAppear_thenCallShowLoggedAnimation() {
        //given
        let dummyView = DummyView()
        sut.view = dummyView
        KeychainManager.shared.saveFirstname(forFirstname: "firstname")
        KeychainManager.shared.saveUserId(forUserId: "userId")
        sut.animationFinished = false

        //when
        sut.viewDidAppear()

        //then
        XCTAssert(dummyView.isCalledShowLogggedAnimation == true)
        XCTAssert(sut.animationFinished == true)
    }

    func test_givenNoErrorBOAndSavedUserAndAnimationFinishedTrue_whenICallViewDidAppear_thenCallShowLoggedAnimation() {
        //given
        let dummyView = DummyView()
        sut.view = dummyView
        KeychainManager.shared.saveFirstname(forFirstname: "firstname")
        KeychainManager.shared.saveUserId(forUserId: "userId")
        sut.animationFinished = true

        //when
        sut.viewDidAppear()

        //then
        XCTAssert(dummyView.isCalledShowLogggedAnimation == false)
        XCTAssert(sut.animationFinished == true)
    }

    // MARK: - Dummy Classes
    
    class DummyView: LoginViewProtocolBGM {

        var isCalledShowError = false
        var isCalledChangeColorEditTexts = false
        var isCalledShowAnimation = false
        var isCalledShowLogggedAnimation = false
        var isCalledLaunchRegisterInGlomo = false
        var isCalledConfiguredRegisterInGlomo = false

        var errorBO: ErrorBO?
        
        func showLogin() {
        }
        
        func showLoggedView(withDisplayData displayData: LoginDisplayData) {
        }
        
        func showAnimation() {

            isCalledShowAnimation = true
        }
        
        func removeTextInputUser(value: String) {
        }
        
        func removeTextInputPass(value: String) {
        }
        
        func showCheckEnterButtonState(state: Bool) {
        }
        
        func showSecureVisualizeIcon(state: Bool) {
        }
        
        func showSecureHideIcon(state: Bool) {
        }
        
        func askLocationPrivileges() {
        }
        
        func showUserViewStatusNormal() {
        }
        
        func showPassViewStatusNormal() {
        }
        
        func moveFocusToPasswordTextInput() {
        }
        
        func dismissKeyboard() {
        }
        
        func showLoginAnimation() {
        }
        
        func showLoggedAnimation() {

            isCalledShowLogggedAnimation = true
        }
        
        func showError(error: ModelBO) {
            errorBO = error as? ErrorBO
            isCalledShowError = true
        }
        
        func changeColorEditTexts() {
            isCalledChangeColorEditTexts = true
        }

        func launchRegisterInGlomo() {

            isCalledLaunchRegisterInGlomo = true
        }

        func configureToRegisterInGlomo() {

            isCalledConfiguredRegisterInGlomo = true
        }
    }
    
    class DummyBusManager: BusManager {
        
        var isCalledNavigateToUserBloqued = false
        
        var isCalledNavigateToLoginLoader = false
        
        var loginTransport: LoginTransport?
        
        override init() {
            super.init()
            routerFactory = RouterInitFactory()
        }
        
        override func navigateScreen<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {
            
            if tag == LoginPageReactionB.ROUTER_TAG && event === LoginPageReactionB.eventNavToUserBlocked {
                isCalledNavigateToUserBloqued = true
            } else if tag == LoginPageReactionB.ROUTER_TAG && event === LoginPageReactionB.ACTION_NAVIGATE_TO_LOGIN_LOADER {
                isCalledNavigateToLoginLoader = true
                loginTransport = values as? LoginTransport
            }
        }
    }
}
