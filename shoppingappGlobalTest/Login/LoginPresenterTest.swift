//
//  shoppingappTest.swift
//  shoppingappTest
//
//  Created by ruben.fernandez on 8/6/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import XCTest
import RxSwift
import CellsNativeComponents

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class LoginPresenterTest: XCTestCase {

    var sut: LoginPresenter<LoginViewDummy>!
    var dummyBusManager: DummyBusManager!
    var dummySessionManager: DummySessionManager!

    override func setUp() {
        super.setUp()
        KeychainManagerTestHelper.configureKeychainManagerForTesting()
        dummyBusManager = DummyBusManager()
        sut = LoginPresenter<LoginViewDummy>(busManager: dummyBusManager)

        DummySessionManager.setupMock()
        dummySessionManager = (SessionDataManager.sessionDataInstance() as! DummySessionManager)
    }

    override func tearDown() {
        super.tearDown()
    }
    // MARK: removeTextUser

    func test_givenAnyValue_whenICallRemoveTextUser_thenReturnCallRemoveTextInputUser() {

        //given
        let dummyView = LoginViewDummy()
        sut.view = dummyView

        //when
        sut.removeTextUser()

        //then
        XCTAssert(dummyView.isCalledRemoveTextInputUser == true)

    }

    func test_givenAnyValue_whenICallRemoveTextUser_thenReturnICallViewShowUserViewStatusNormal() {

        //given
        let dummyView = LoginViewDummy()
        sut.view = dummyView

        //when
        sut.removeTextUser()

        //then
        XCTAssert(dummyView.isCalledShowUserViewStatusNormal == true)

    }
    // MARK: removeTextPass

    func test_givenAnyValue_whenICallRemoveTextPass_thenReturnCallRemoveTextInputPass() {

        //given
        let dummyView = LoginViewDummy()
        sut.view = dummyView

        //when
        sut.removeTextPass()

        //then
        XCTAssert(dummyView.isCalledRemoveTextInputPass == true)

    }

    func test_givenAnyValue_whenICallRemoveTextPass_thenReturnICallViewShowPassViewStatusNormal() {

        //given
        let dummyView = LoginViewDummy()
        sut.view = dummyView

        //when
        sut.removeTextPass()

        //then
        XCTAssert(dummyView.isCalledShowPassViewStatusNormal == true)

    }
    // MARK: checkStateSecure

    func test_givenTrueBoolValue_whenICallCheckStateSecure_thenCalledSecureHideIconWithFalseBool() {

        //given
        let dummyView = LoginViewDummy()
        dummyView.sendSecureValue = true
        sut.view = dummyView

        //when
        sut.checkStateSecure(boolValue: true)

        //
        XCTAssert(dummyView.isCalledSecureHideIconWithOppositeBool == true)
    }

    func test_givenFalseBoolValue_whenICallCheckStateSecure_thenCalledSecureVisualizeIconWithTrueBool() {

        //given
        let dummyView = LoginViewDummy()
        dummyView.sendSecureValue = false
        sut.view = dummyView

        //when
        sut.checkStateSecure(boolValue: false)

        //then
        XCTAssert(dummyView.isCalledSecureVisualizeIconWithOppositeBool == true)
    }
    // MARK: checkEnterButtonState

    func test_givenAEmptyUserAndPassHasText_whenICallCheckEnterButtonState_thenCallShowCheckEnterButtonStateWithFalseBoolValue() {

        //given
        let loginTransport = LoginTransport(username: "", password: "Pass")
        let dummyView = LoginViewDummy()
        sut.view = dummyView

        //when
        sut.checkEnterButtonState(loginTransport: loginTransport)

        //then
        XCTAssert(dummyView.isCalledCheckEnterButtonStateWithFalseBool == true)
        XCTAssert(dummyView.isCalledCheckEnterButtonStateWithTrueBool == false)

    }

    func test_givenUserHasTextAndEmptyPass_whenICallCheckEnterButtonState_thenCallShowCheckEnterButtonStateWithFalseBoolValue() {

        //given
        let loginTransport = LoginTransport(username: "User", password: "")
        let dummyView = LoginViewDummy()
        sut.view = dummyView

        //when
        sut.checkEnterButtonState(loginTransport: loginTransport)

        //then
        XCTAssert(dummyView.isCalledCheckEnterButtonStateWithFalseBool == true)
        XCTAssert(dummyView.isCalledCheckEnterButtonStateWithTrueBool == false)

    }

    func test_givenUserHasTextAndPassHasText_whenICallCheckEnterButtonState_thenCallShowCheckEnterButtonStateWithTrueBoolValue() {

        //given
        let loginTransport = LoginTransport(username: "User", password: "Pass")

        let dummyView = LoginViewDummy()
        sut.view = dummyView

        //when
        sut.checkEnterButtonState(loginTransport: loginTransport)

        //then
        XCTAssert(dummyView.isCalledCheckEnterButtonStateWithTrueBool == true)
        XCTAssert(dummyView.isCalledCheckEnterButtonStateWithFalseBool == false)

    }

    func test_givenUserEmptyTextAndEmptyPass_whenICallCheckEnterButtonState_thenCallShowCheckEnterButtonStateWithFalseBoolValue() {

        //given
        let loginTransport = LoginTransport(username: "", password: "")
        let dummyView = LoginViewDummy()
        sut.view = dummyView

        //when
        sut.checkEnterButtonState(loginTransport: loginTransport)

        //then
        XCTAssert(dummyView.isCalledCheckEnterButtonStateWithFalseBool == true)
        XCTAssert(dummyView.isCalledCheckEnterButtonStateWithTrueBool == false)

    }

    // MARK: loginAnimationFinished

    func test_givenUserAndPasswordCorrect_whenICallLoginAnimationFinished_thenTheLoginTransportDataIsSameThatLoginTransportDataInRouterNextLogin() {

        //given
        let mockTransport = LoginTransport(username: "user", password: "pass")

        //when
        sut.loginAnimationFinished(loginTransport: mockTransport)

        //then

        XCTAssert(dummyBusManager.saveLoginTransport as? LoginTransport === mockTransport)
        XCTAssert(dummyBusManager.isCalledNavigateScreenWithTagLoginAndEventNavToNextLoginBind == true)

    }
    
    func test_givenLoginTransport_whenICallLoginAnimationFinished_thenICallBusPublishDataEventTrackerNewTrackEvent() {
        
        // given
        let mockTransport = LoginTransport(username: "user", password: "pass")
        
        //when
        sut.loginAnimationFinished(loginTransport: mockTransport)

        // then
        XCTAssert(dummyBusManager.isCalledPublishDataEventTrackerNewTrackEvent == true)
        
    }
    
    func test_givenLoginTransport_whenICallLoginAnimationFinished_thenICallBusPublishDataEventTrackerNewTrackEventWithAppropiatedEventModel() {
        
        // given
        let mockTransport = LoginTransport(username: "user", password: "pass")
        
        //when
        sut.loginAnimationFinished(loginTransport: mockTransport)

        // then
        XCTAssert(dummyBusManager.eventSent != nil)
        XCTAssert(dummyBusManager.eventSent?.type == .login)
    }

    // MARK: - viewDidAppear

    func test_givenNoErrorBOAndNoSavedUserAndAnimationFinishedFalse_whenICallViewDidAppear_thenCallShowAnimation() {
        //given
        let dummyView = LoginViewDummy()
        sut.view = dummyView
        KeychainManager.shared.resetKeyChainItems()
        sut.animationFinished = false

        //when
        sut.viewDidAppear()

        //then
        XCTAssert(dummyView.isCalledShowAnimation == true)
        XCTAssert(sut.animationFinished == true)
    }

    func test_givenNoErrorBOAndNoSavedUserAndAnimationFinishedTrue_whenICallViewDidAppear_thenNotCallShowAnimation() {
        //given
        let dummyView = LoginViewDummy()
        sut.view = dummyView
        KeychainManager.shared.resetKeyChainItems()
        sut.animationFinished = true

        //when
        sut.viewDidAppear()

        //then
        XCTAssert(dummyView.isCalledShowAnimation == false)
        XCTAssert(sut.animationFinished == true)
    }

    func test_givenAnErrorBO_whenICallViewDidAppear_thenNotCallShowAnimationAndLoggedViewAnimation() {
        //given
        let dummyView = LoginViewDummy()
        sut.view = dummyView

        let errorBO = ErrorBO(error: ErrorEntity(message: "Error"))
        sut.errorBO = errorBO

        //when
        sut.viewDidAppear()

        //then
        XCTAssert(dummyView.isCalledShowAnimation == false)
        XCTAssert(dummyView.isCalledShowLogggedAnimation == false)

    }

    func test_givenNoErrorBOAndSavedUserAndAnimationFinishedFalse_whenICallViewDidAppear_thenCallShowLoggedAnimation() {
        //given
        let dummyView = LoginViewDummy()
        sut.view = dummyView
        KeychainManager.shared.saveFirstname(forFirstname: "firstname")
        KeychainManager.shared.saveUserId(forUserId: "userId")
        sut.animationFinished = false

        //when
        sut.viewDidAppear()

        //then
        XCTAssert(dummyView.isCalledShowLogggedAnimation == true)
        XCTAssert(sut.animationFinished == true)
    }

    func test_givenNoErrorBOAndSavedUserAndAnimationFinishedTrue_whenICallViewDidAppear_thenCallShowLoggedAnimation() {
        //given
        let dummyView = LoginViewDummy()
        sut.view = dummyView
        KeychainManager.shared.saveFirstname(forFirstname: "firstname")
        KeychainManager.shared.saveUserId(forUserId: "userId")
        sut.animationFinished = true

        //when
        sut.viewDidAppear()

        //then
        XCTAssert(dummyView.isCalledShowLogggedAnimation == false)
        XCTAssert(sut.animationFinished == true)
    }

    // MARK: startLogin

    func test_givenStartLoginWithNotSavedUser_whenICallStartLogin_thenCallShowLogin() {

        //given
        let dummyView = LoginViewDummy()
        sut.view = dummyView
        KeychainManager.shared.resetKeyChainItems()

        //when
        sut.startLogin()

        //then
        XCTAssert(dummyView.isCalledShowLogin == true)
    }

    func test_givenStartLoginWithSavedUser_whenICallStartLogin_thenCallShowLoginAndLoggedDataIsTheSame() {

        //given
        let dummyView = LoginViewDummy()
        sut.view = dummyView
        KeychainManager.shared.saveFirstname(forFirstname: "firstname")
        KeychainManager.shared.saveUserId(forUserId: "userid")

        //when
        sut.startLogin()

        //then
        XCTAssert(dummyView.isCalledShowLogggedView == true)
        XCTAssert(sut.loginDisplayData != nil)
        XCTAssert(sut.loginDisplayData?.userSavedName == dummyView.loggedDisplayData?.userSavedName)
        XCTAssert(sut.loginDisplayData?.changeUserText == dummyView.loggedDisplayData?.changeUserText)
        XCTAssert(sut.loginDisplayData?.userSavedImage == dummyView.loggedDisplayData?.userSavedImage)
        XCTAssert(sut.loginDisplayData?.greetingText == dummyView.loggedDisplayData?.greetingText)

    }
    // MARK: showModal

    func test_whenCallShowModalThenPresenterCallRouterErrorLogin() {

        //given
        let errorBO = ErrorBO(error: ErrorEntity(message: "Error en el password"))
        sut.errorBO = errorBO

        //when
        sut.showModal()

        //then
        XCTAssert(dummyBusManager.isCalledShowModalError == true)
    }

    func test_givenAnErrorBO_whenICallShowModal_thenErrorBOIsSameThanErrorBOInRouterErrorLogin() {
        //given
        //let dummyRouter = DummyRouter()
        //sut.router = dummyRouter

        let errorBO = ErrorBO(error: ErrorEntity(message: "Error en el password"))

        sut.errorBO = errorBO

        //when
        sut.showModal()

        //then
        XCTAssert(dummyBusManager.errorBO === sut.errorBO)
    }

    func test_givenAnErrorBO_whenICallShowModal_thenErrorBOIsShownIsTrue() {

        //given
        let errorBO = ErrorBO(error: ErrorEntity(message: "Error en el password"))

        errorBO.isErrorShown = false
        sut.errorBO = errorBO

        //when
        sut.showModal()

        //then
        XCTAssert(sut.errorBO?.isErrorShown == true)
    }

    // MARK: checkLocationPrivileges

    func test_givenAnyValue_whenICallCheckLocationPrivileges_thenReturnCallAskLocationPrivileges() {

        // given
        let dummyView = LoginViewDummy()
        sut.view = dummyView

        // when
        sut.checkLocationPrivileges()

        // then
        XCTAssert(dummyView.isCalledAskLocationPrivileges == true)

    }

    // MARK: userTextInputIntroPressed

    func test_givenAnyValue_whenICallUserInputIntroPressed_thenMoveFocusToPasswordIsCalled() {

        //given
        let dummyView = LoginViewDummy()
        sut.view = dummyView

        //when
        sut.userTextInputIntroPressed()

        //then
        XCTAssert(dummyView.isCalledMoveFocusToPasswordTextInput == true)

    }

    // MARK: passwordTextInputIntroPressed

    func test_givenALoginTransportWithoutPassword_whenICallPasswordInputIntroPressed_thenDismissKeyboardIsCalled() {

        //given
        let mockTransport = LoginTransport(username: "user", password: "")
        let dummyView = LoginViewDummy()
        sut.view = dummyView

        //when
        sut.passwordTextInputIntroPressed(loginTransport: mockTransport)

        //then
        XCTAssert(dummyView.isCalledDismissKeyboard == true)

    }

    func test_givenALoginTransportWithoutUser_whenICallPasswordInputIntroPressed_thenDismissKeyboardIsCalled() {

        //given
        let mockTransport = LoginTransport(username: "", password: "pass")
        let dummyView = LoginViewDummy()
        sut.view = dummyView

        //when
        sut.passwordTextInputIntroPressed(loginTransport: mockTransport)

        //then
        XCTAssert(dummyView.isCalledDismissKeyboard == true)

    }

    func test_givenALoginTransportWithUserAndPassword_whenICallPasswordInputIntroPressed_thenShowLoginAnimationIsCalled() {

        //given
        let mockTransport = LoginTransport(username: "user", password: "pass")
        let dummyView = LoginViewDummy()
        sut.view = dummyView

        //when
        sut.passwordTextInputIntroPressed(loginTransport: mockTransport)

        //then
        XCTAssert(dummyView.isCalledShowLoginAnimation == true)

    }

    // MARK: loginAnimationFinished

    func test_givenUserAndPassCorrect_whenICallLoginAnimationFinished_thenPresenterCallRouterNextLogin() {

        //given
        let mockTransport = LoginTransport(username: "user", password: "pass")

        //when
        sut.loginAnimationFinished(loginTransport: mockTransport)

        //then

        XCTAssert(dummyBusManager.saveLoginTransport as? LoginTransport === mockTransport)
        XCTAssert(dummyBusManager.isCalledNavigateScreenWithTagLoginAndEventNavToNextLoginBind == true)

    }

    // MARK: CheckError

    func test_givenAnErrorBOWithError_whenICallCheckErrorIsShownIsFalse_thenCallShowErrorInView() {

        //given
        let dummyView = LoginViewDummy()
        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        sut.errorBO = errorBO
        sut.view = dummyView

        //when
        sut.checkError()

        //then
        XCTAssert(dummyView.isCalledShowError == true)
    }

    func test_givenAnErrorBO_whenICallCheckError_thenmessageIsSameInView() {

        //given
        let dummyView = LoginViewDummy()
        sut.view = dummyView

        let errorBO = ErrorBO(error: ErrorEntity(message: "Error en el password"))

        errorBO.isErrorShown = false
        sut.errorBO = errorBO

        //when
        sut.checkError()

        //then
        XCTAssertEqual(dummyView.errorBO?.errorMessage(), errorBO.errorMessage())
    }

    func test_givenNoErrorBO_whenICallCheckError_thenNotCallShowErrorInView() {

        //given
        let dummyView = LoginViewDummy()
        sut.view = dummyView

        //when
        sut.checkError()

        //then
        XCTAssert(dummyView.isCalledShowError == false)
    }

    func test_givenAnErrorBO_whenErrorBOIsShownIsTrueThenNotCallShowModal() {
        //given
        let dummyView = LoginViewDummy()
        sut.view = dummyView

        let errorBO = ErrorBO(error: ErrorEntity(message: "Error en el password"))

        errorBO.isErrorShown = true
        sut.errorBO = errorBO

        //when
        sut.checkError()

        //then
        XCTAssert(dummyView.isCalledShowError == false)
    }

    func test_givenAnErrorBOWithHttpStatusBetween400And499_whenICallCheckErrorIsErrorShown_ThenCallChagenColorEditTexts() {
        //given
        let dummyView = LoginViewDummy()
        sut.view = dummyView

        for i in 400...499 {

            let errorBO = ErrorBO(error: ErrorEntity(message: "Error en el password"))

            errorBO.isErrorShown = true
            errorBO.status = i
            sut.errorBO = errorBO

            //when
            sut.checkError()

            //then
            XCTAssert(dummyView.isCalledChangeColorEditTexts == true)

        }

    }

    func test_givenAnErrorBOWithHttpStatus403_whenICallCheckErrorIsErrorShown_ThenCallChagenColorEditTexts() {
        //given
        let dummyView = LoginViewDummy()
        sut.view = dummyView

        for i in 400...499 {

            let errorBO = ErrorBO(error: ErrorEntity(message: "Error en el password"))

            errorBO.isErrorShown = true
            errorBO.status = i
            sut.errorBO = errorBO

            //when
            sut.checkError()

            //then
            XCTAssert(dummyView.isCalledChangeColorEditTexts == true)

        }

    }

    // MARK: - userTextfieldDidChanged

    func test_givenView_whenICallUserTextfieldDidChanged_thenICallViewShowUserViewStatusNormal() {

        // given
        let dummyView = LoginViewDummy()
        sut.view = dummyView

        // when
        sut.userTextfieldDidChanged()

        // then
        XCTAssert(dummyView.isCalledShowUserViewStatusNormal == true)

    }

    // MARK: - passTextfieldDidChanged

    func test_givenView_whenICallPassTextfieldDidChanged_thenICallViewShowPassViewStatusNormal() {

        // given
        let dummyView = LoginViewDummy()
        sut.view = dummyView

        // when
        sut.passTextfieldDidChanged()

        // then
        XCTAssert(dummyView.isCalledShowPassViewStatusNormal == true)

    }
    // MARK: - loginButtonPressed

    func test_givenUserAndPassCorrect_whenICallLoginButtonPressed_thenPresenterCallRouterNextLogin() {

        //given
        let mockTransport = LoginTransport(username: "user", password: "pass")

        //when
        sut.loginButtonPressed(loginTransport: mockTransport)

        //then

        XCTAssert(dummyBusManager.saveLoginTransport as? LoginTransport === mockTransport)
        XCTAssert(dummyBusManager.isCalledNavigateScreenWithTagLoginAndEventNavToNextLoginBind == true)

    }
    func test_givenUserAndPasswordCorrect_whenICallDoLogin_thenTheLoginTransportDataIsSameThatLoginTransportDataInRouterNextLogin() {

        //given
        let mockTransport = LoginTransport(username: "user", password: "pass")

        //when
        sut.loginButtonPressed(loginTransport: mockTransport)

        //then

        XCTAssert(dummyBusManager.saveLoginTransport as? LoginTransport === mockTransport)
        XCTAssert(dummyBusManager.isCalledNavigateScreenWithTagLoginAndEventNavToNextLoginBind == true)

    }
    
    // MARK: - acceptButtonPressed

    func test_givenASavedUserAndChangeUserPressed_whenICallAcceptButtonPressedWithUnderstoodTitleAndSessionIsClosed_thenIRemoveAllSavedData() {

        //given
        KeychainManager.shared.saveUserId(forUserId: "userId")
        KeychainManager.shared.saveFirstname(forFirstname: "firstname")
        let dummyPreferencesManager = DummyPreferencesManager()
        let dummyPublicManager = DummyPublicManager()
        sut.changeUserPressed = true

        //when
        PreferencesManager.instance = dummyPreferencesManager
        ConfigPublicManager.instance = dummyPublicManager

        sut.acceptButtonPressed()

        // then
        XCTAssert(KeychainManager.shared.getUserId() == "")
        XCTAssert(KeychainManager.shared.getFirstname() == "")
        XCTAssert(dummySessionManager.tabBarSelected  == "")
        XCTAssert(dummySessionManager.isCalledCloseSession == true)
        XCTAssert(dummySessionManager.isCalledRequestLogout == true)
        XCTAssert(dummyPreferencesManager.isCalledResetPreferencesData == true)
        XCTAssert(dummyPublicManager.isCalledResetCounters == true)
        XCTAssert(dummyBusManager.isCalledSetRootWhenNoSession == true)

    }
    func test_givenASavedUserAndChangeUserPressed_whenICallAcceptButtonPressedWithUnderstoodTitleAndSessionIsNotClosed_thenIRemoveAllSavedData() {

        //given
        KeychainManager.shared.saveUserId(forUserId: "userId")
        KeychainManager.shared.saveFirstname(forFirstname: "firstname")
        let dummyPreferencesManager = DummyPreferencesManager()
        let dummyPublicManager = DummyPublicManager()
        sut.changeUserPressed = true

        //when
        PreferencesManager.instance = dummyPreferencesManager
        ConfigPublicManager.instance = dummyPublicManager

        sut.acceptButtonPressed()

        // then
        XCTAssert(KeychainManager.shared.getUserId() == "")
        XCTAssert(KeychainManager.shared.getFirstname() == "")
        XCTAssert(dummySessionManager.tabBarSelected  == "")
        XCTAssert(dummySessionManager.isCalledCloseSession == true)
        XCTAssert(dummySessionManager.isCalledRequestLogout == true)
        XCTAssert(dummyPreferencesManager.isCalledResetPreferencesData == true)
        XCTAssert(dummyPublicManager.isCalledResetCounters == true)

    }
    func test_givenChangeUserPressedFalse_whenICallAcceptButtonPressedWithATitleDifferentThanUnderstood_thenICallCheckError() {

        //given
        let dummyView = LoginViewDummy()
        sut.view = dummyView
        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        sut.errorBO = errorBO
        sut.changeUserPressed = false

        //when
        sut.acceptButtonPressed()

        // then
        XCTAssert(dummyView.isCalledShowError == true)

    }

    func test_givenAny_whenICallAcceptButtonPressed_thenChangeUserPressedShouldBeFalse() {

        // given
        sut.changeUserPressed = true

        // when
        sut.acceptButtonPressed()

        // then
        XCTAssert(sut.changeUserPressed == false)

    }

    // MARK: - cancelButtonPressed

    func test_givenAny_whenICallCancelButtonPressed_thenChangeUserPressedShouldBeFalse() {

        // given
        sut.changeUserPressed = true

        // when
        sut.cancelButtonPressed()

        // then
        XCTAssert(sut.changeUserPressed == false)

    }

    // MARK: - changeUserConfirmation

    func test_givenAny_whenICallChangeUserConfirmation_thenICallViewShowError() {

        // given
        let dummyView = LoginViewDummy()
        sut.view = dummyView

        // when
        sut.changeUserConfirmation()

        // then
        XCTAssert(dummyView.isCalledShowError == true)

    }

    func test_givenAny_whenICallChangeUserConfirmation_thenErrorBOShouldHaveValue() {

        // given
        let dummyView = LoginViewDummy()
        sut.view = dummyView

        // when
        sut.changeUserConfirmation()

        // then
        XCTAssert(sut.errorBO != nil)
    }

    func test_givenAny_whenICallChangeUserConfirmation_thenShowedDataTheSameAsErrorBOValues() {

        // given
        let dummyView = LoginViewDummy()
        sut.view = dummyView

        // when
        sut.changeUserConfirmation()

        // then
        XCTAssert(sut.errorBO!.errorMessage() == nil)
        XCTAssert(sut.errorBO!.messageAttributed == dummyView.errorBO?.messageAttributed)
        XCTAssert(sut.errorBO!.errorType == dummyView.errorBO?.errorType)
        XCTAssert(sut.errorBO!.allowCancel == dummyView.errorBO?.allowCancel)
        XCTAssert(sut.errorBO!.okButtonTitle == dummyView.errorBO?.okButtonTitle)

    }

    func test_givenAny_whenICallChangeUserConfirmation_thenChangeUserPressedShouldBeTrue() {

        // given
        sut.changeUserPressed = false

        // when
        sut.changeUserConfirmation()

        // then
        XCTAssert(sut.changeUserPressed == true)

    }

    // MARK: - showPromotionsPressed

    func test_givenAny_whenICallShowPromotionsPressed_thenICallBusManagerNavigateScreenWithTagLoginAndEventNavToNextLogin() {

        // given

        // when
        sut.showPromotionsPressed()

        // then
        XCTAssert(dummyBusManager.isCalledNavigateScreenWithTagLoginAndEventNavToNextLoginBind == true)

    }

    func test_givenAny_whenICallShowPromotionsPressed_thenICallBusManagerNavigateScreenWithModelButNotLoginTransport() {

        // given

        // when
        sut.showPromotionsPressed()

        // then
        XCTAssert(dummyBusManager.saveLoginTransport != nil)
        XCTAssert((dummyBusManager.saveLoginTransport is LoginTransport) == false)

    }
    
    func test_givenAny_whenICallShowPromotionsPressed_thenICallBusPublishDataEventTrackerNewTrackEvent() {
        
        // given
        
        // when
        sut.showPromotionsPressed()
        
        // then
        XCTAssert(dummyBusManager.isCalledPublishDataEventTrackerNewTrackEvent == true)
        
    }
    
    func test_givenAny_whenICallShowPromotionsPressed_thenICallBusPublishDataEventTrackerNewTrackEventWithAppropiatedEventModel() {
        
        // given
        
        // when
        sut.showPromotionsPressed()
        
        // then
        XCTAssert(dummyBusManager.eventSent != nil)
        XCTAssert(dummyBusManager.eventSent?.type == .showPromotions)
    }

    // MARK: - ticketReceivedInLaunch
    func test_givenAny_whenICallTicketReceivedInLaunchWithTicket_thenICallBusManagerShowTicketDetail() {

        //given

        //when
        let stringTicket = "{\"aps\":{\"alert\":{\"loc-args\":[\"01424\",\"481515\",\"SUSHI TAI\",\"3.21\",\"MXP\",\"160722084808\",\"EDW PLAT\"],\"loc-key\":\"00001\"},\"sound\":\"default\"}}"

        let jsonData = stringTicket.data(using: .utf8)
        let dictionaryTicket = try? JSONSerialization.jsonObject(with: jsonData!, options: .mutableLeaves)
        let ticket = TicketModel(withPayload: dictionaryTicket as! [AnyHashable: Any])

        sut.ticketReceivedInLaunch(ticket: ticket)

        //then
        XCTAssert(dummyBusManager.isCalledShowTicketDetail == true)
    }
    
    func test_givenAny_whenICallTicketReceivedInLaunchWithTicket_thenTicketModelMatch() {
        
        //given
        
        //when
        let stringTicket = "{\"aps\":{\"alert\":{\"loc-args\":[\"01424\",\"481515\",\"SUSHI TAI\",\"3.21\",\"MXP\",\"160722084808\",\"EDW PLAT\"],\"loc-key\":\"00001\"},\"sound\":\"default\"}}"
        
        let jsonData = stringTicket.data(using: .utf8)
        let dictionaryTicket = try? JSONSerialization.jsonObject(with: jsonData!, options: .mutableLeaves)
        let ticket = TicketModel(withPayload: dictionaryTicket as! [AnyHashable: Any])
        
        sut.ticketReceivedInLaunch(ticket: ticket)
        
        //then
        XCTAssert(dummyBusManager.ticket! == ticket)
    }

    class LoginViewDummy: LoginViewProtocol {

        var isCalledShowLogin = false
        var isCalledShowAnimation = false
        var isCalledRemoveTextInputUser = false
        var isCalledRemoveTextInputPass = false
        var isCalledCheckEnterButtonStateWithTrueBool = false
        var isCalledCheckEnterButtonStateWithFalseBool = false
        var isCalledSecureVisualizeIconWithOppositeBool = false
        var isCalledSecureHideIconWithOppositeBool = false
        var sendSecureValue = false
        var isCalledAskLocationPrivileges = false
        var isCalledShowError = false
        var isCalledChangeColorEditTexts = false
        var isCalledShowUserViewStatusNormal = false
        var isCalledShowPassViewStatusNormal = false
        var isCalledMoveFocusToPasswordTextInput = false
        var isCalledDismissKeyboard = false
        var isCalledShowLoginAnimation = false
        var isCalledShowLogggedAnimation = false
        var isCalledShowLogggedView = false

        var loggedDisplayData: LoginDisplayData?

        var errorBO: ErrorBO?

        func showLogin() {
            isCalledShowLogin = true
        }

        func showAnimation() {
            isCalledShowAnimation = true
        }

        func removeTextInputUser(value: String) {
            if value == "" {
                isCalledRemoveTextInputUser = true
            }
        }

        func removeTextInputPass(value: String) {
            if value == "" {
                isCalledRemoveTextInputPass = true
            }
        }

        func showCheckEnterButtonState(state: Bool) {
            if state == false {
                isCalledCheckEnterButtonStateWithFalseBool = true
            } else {
                isCalledCheckEnterButtonStateWithTrueBool = true
            }
        }

        func showSecureVisualizeIcon(state: Bool) {
            if state != sendSecureValue {
                isCalledSecureVisualizeIconWithOppositeBool = true
            }
        }

        func showSecureHideIcon(state: Bool) {
            if state != sendSecureValue {
                isCalledSecureHideIconWithOppositeBool = true
            }
        }

        func showError(error: ModelBO) {
            errorBO = error as? ErrorBO
            isCalledShowError = true
        }

        func changeColorEditTexts() {
           isCalledChangeColorEditTexts = true
        }

        func askLocationPrivileges() {
            self.isCalledAskLocationPrivileges = true
        }

        func showUserViewStatusNormal() {
            isCalledShowUserViewStatusNormal = true
        }

        func showPassViewStatusNormal() {
            isCalledShowPassViewStatusNormal = true
        }

        func moveFocusToPasswordTextInput() {
            isCalledMoveFocusToPasswordTextInput = true
        }

        func dismissKeyboard() {
            isCalledDismissKeyboard = true
        }

        func showLoginAnimation() {
            isCalledShowLoginAnimation = true
        }

        func showLoggedAnimation() {
            isCalledShowLogggedAnimation = true
        }

        func showLoggedView(withDisplayData displayData: LoginDisplayData) {
            loggedDisplayData = displayData
            isCalledShowLogggedView = true
        }

    }

    class DummyBusManager: BusManager {

        var isCalledPublishDataWithTagLoginAndEventLoginTransport = false
        var isCalledNavigateScreenWithTagLoginAndEventNavToNextLoginBind = false
        var isCalledPublishDataEventTrackerNewTrackEvent = false
        var isCalledShowModalError = false
        var isCalledSetRootWhenNoSession = false
        var isCalledShowTicketDetail = false

        var saveLoginTransport: Model?
        var eventSent: EventProtocol?
        var ticket: TicketModel?
        
        override init() {
            super.init()
            routerFactory = RouterInitFactory()
        }

        override func navigateScreen<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {
            if tag == LoginPageReaction.ROUTER_TAG && event === LoginPageReaction.ACTION_NAVIGATE_TO_LOGIN_LOADER {
                saveLoginTransport = values as? Model
                isCalledNavigateScreenWithTagLoginAndEventNavToNextLoginBind = true
            }
        }
        
        override func publishData<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {
            
            if tag == EventTracker.id && event === PageReactionConstants.NEW_TRACK_EVENT {
                isCalledPublishDataEventTrackerNewTrackEvent = true
                eventSent = values as? EventProtocol
            }
            
        }

        override func showModalError(error: ModelBO, delegate: CloseModalDelegate?) {
            errorBO = error as? ErrorBO
            isCalledShowModalError = true
        }
        
        override func setRootWhenNoSession() {
            isCalledSetRootWhenNoSession = true
        }

        override func showTicketDetail(withModel model: Model) {

            isCalledShowTicketDetail = true
            ticket = model as? TicketModel
        }

    }

    class DummyPreferencesManager: PreferencesManager {

        var isCalledResetPreferencesData = false

        override func resetPreferencesData() {
            isCalledResetPreferencesData = true
        }

    }
    class DummyPublicManager: ConfigPublicManager {

        var isCalledResetCounters = false

        override func resetCounters() {
            isCalledResetCounters = true
        }
    }
}
