//
//  StoresDisplayDataTest.swift
//  shoppingapp
//
//  Created by developer on 31/07/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import XCTest

#if TESTMX
@testable import shoppingappMX
#endif

class StoresDisplayDataTest: XCTestCase {
    
    // MARK: - StoresDisplayData test
    
    func test_givenAEmptyStoresBOAndUserLocation_whenICallStoresDisplayData_thenItemsShouldBeEmpty() {
        
        // given
        let stores = [StoreBO]()
        let userLocation = GeoLocationBO(withLatitude: 44.0, withLongitude: 3.0)
        
        // when
        let sut = StoresDisplayData(withStores: stores, userLocation: userLocation)

        // then
        XCTAssert(sut.items.isEmpty == true)
    }
    
    func test_givenAEmptyStoresBO_whenICallStoresDisplayData_thenItemsShouldBeEmpty() {
        
        // given
        let stores = [StoreBO]()
        
        // when
       let sut = StoresDisplayData(withStores: stores, userLocation: nil)
        
        // then
        XCTAssert(sut.items.isEmpty == true)
    }
    
    func test_givenAStoresBOAndUserLocation_whenICallStoresDisplayData_thenItemsShouldNotBeEmpty() {
        
        // given
        let stores = PromotionsGenerator.getPromotionDetail().stores!
        let userLocation = GeoLocationBO(withLatitude: 44.0, withLongitude: 3.0)

        // when
        let sut = StoresDisplayData(withStores: stores, userLocation: userLocation)

        // then
        XCTAssert(sut.items.isEmpty == false)
    }
    
    func test_givenAStoresBO_whenICallStoresDisplayData_thenItemsShouldNotBeEmpty() {
        
        // given
        let stores = PromotionsGenerator.getPromotionDetail().stores!
        
        // when
        let sut = StoresDisplayData(withStores: stores, userLocation: nil)
        
        // then
        XCTAssert(sut.items.isEmpty == false)
    }
    
    func test_givenAStoresBOAndUserLocation_whenICallStoresDisplayData_thenItemsShouldMatchWithNumberOfStores() {
        
        // given
        let stores = PromotionsGenerator.getPromotionDetail().stores!
        let userLocation = GeoLocationBO(withLatitude: 44.0, withLongitude: 3.0)
        
        // when
        let sut = StoresDisplayData(withStores: stores, userLocation: userLocation)

        // then
        XCTAssert(sut.items.count == stores.count)
    }

    func test_givenAStoresBO_whenICallStoresDisplayData_thenItemsShouldMatchWithNumberOfStores() {
        
        // given
        let stores = PromotionsGenerator.getPromotionDetail().stores!
        
        // when
        let sut = StoresDisplayData(withStores: stores, userLocation: nil)
        
        // then
        XCTAssert(sut.items.count == stores.count)
    }
    
    // MARK: - StoreDisplayData test

    func test_givenAStoresBO_whenIInitStoreDisplayDataWithStores_thenItemsShouldMatchWithNumberOfStores() {
        
        // given
        let storesBO = PromotionsGenerator.getPromotionDetail().stores!
        var items = [StoreDisplayData]()

        // when
        
        for store in storesBO {
            let displayItem = StoreDisplayData(fromStore: store)
            items.append(displayItem)
        }
        
        // then
        XCTAssert(storesBO.count == items.count)
    }

    func test_givenAStoreBO_whenICallGenerateStoreDisplayData_thenDisplayDataShouldBeMatchWithStoreBO() {
        
        //given
        let storeBO = PromotionsGenerator.getPromotionDetail().stores![0]
        let location = storeBO.location!
        let address = "\(location.addressName!.capitalized), \(location.zipCode!.capitalized), \(location.city!.capitalized), \(location.state!.capitalized)"
        
        //When
        let storeDisplayData = StoreDisplayData(fromStore: storeBO)
        
        // then
        XCTAssert(storeDisplayData.address == address)
    }
    
    func test_givenAStoreBOWithoutZipCodeAndState_whenICallInit_thenAddressOnlyContainNameAndCity() {
        
        // given
        let storeBO = PromotionsGenerator.getPromotionDetail().stores![0]
        storeBO.location?.zipCode = nil
        storeBO.location?.state = nil
        let location = storeBO.location!
        let address = "\(location.addressName!.capitalized), \(location.city!.capitalized)"
        
        // when
        let storeDisplayData = StoreDisplayData(fromStore: storeBO)
        
        // then
        XCTAssert(storeDisplayData.address == address)
        
    }
    
    func test_givenStoreBOWithDistanceAndUserLocation_whenICallGenerateStoreDisplayData_thenFormattedDistance() {
        
        // given
        let storeBO = PromotionsGenerator.getPromotionDetail().stores![0]
        let formattedDistance = storeBO.formattedDistance()!
        let userLocation = GeoLocationBO(withLatitude: 44.0, withLongitude: 3.0)
        
        // when
        let storeDisplayData = StoreDisplayData(fromStore: storeBO, userLocation: userLocation)
        
        // then
        XCTAssert(storeDisplayData.formattedDistance == Localizables.promotions.key_from_distance_text + " " + formattedDistance)
    }
    
    func test_givenStoreBO_whenICallGenerateStoreDisplayData_thenFormattedDistanceBeEmpty() {
        
        // given
        let storeBO = PromotionsGenerator.getPromotionDetail().stores![0]
        
        // when
        let storeDisplayData = StoreDisplayData(fromStore: storeBO, userLocation: nil)
        
        // then
        XCTAssert(storeDisplayData.formattedDistance.isEmpty == true)
    }
    
    func test_givenStoreBOWithDistanceNil_whenICallGenerateStoreDisplayData_thenFormattedDistanceIsEmpty() {
        
        // given
        let storeBO = PromotionsGenerator.getPromotionDetail().stores![1]
        
        // when
        let storeDisplayData = StoreDisplayData(fromStore: storeBO)
        
        // then
        XCTAssert(storeDisplayData.formattedDistance.isEmpty == true)
    }
}
