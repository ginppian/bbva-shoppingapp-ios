//
//  StoreListPresenterTests.swift
//  shoppingapp
//
//  Created by Developer on 26/07/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest
import CellsNativeComponents

#if TESTMX
@testable import shoppingappMX
#endif

class StoreListPresenterTests: XCTestCase {
    
    var sut: StoreListPresenter<DummyView>!
    var dummyView: DummyView?
    var dummyBusManager: DummyBusManager!

    override func setUp() {
        
        super.setUp()
        
        dummyView = DummyView()
        
        dummyBusManager = DummyBusManager()
        sut = StoreListPresenter<DummyView>(busManager: dummyBusManager)
        
        sut.view = dummyView
    }
    
    // MARK: - setModel
    
    func test_givenPromotionDetailTransportWithTitle_whenICallSetModel_thenTitleShouldMatch() {
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]
        let promotionDetailTransport = PromotionDetailTransport()
        promotionDetailTransport.promotionBO = promotionBO
        
        // when
        sut.setModel(model: promotionDetailTransport)
        
        // then
        XCTAssert(sut.title == promotionDetailTransport.promotionBO?.commerceInformation.name)
    }
    
    func test_givenPromotionDetailTransportWithPromotionBO_whenICallSetModel_thenStoresNumberShouldMatch() {
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]
        let promotionDetailTransport = PromotionDetailTransport()
        promotionDetailTransport.promotionBO = promotionBO
        
        // when
        sut.setModel(model: promotionDetailTransport)
        
        // then
        XCTAssert(sut.stores.count == promotionDetailTransport.promotionBO?.stores?.count)
    }

    func test_givenPromotionDetailTransportWithPromotionBO_whenICallSetModel_thenStoresShouldMatch() {
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]
        let promotionDetailTransport = PromotionDetailTransport()
        promotionDetailTransport.promotionBO = promotionBO
        
        // when
        sut.setModel(model: promotionDetailTransport)
        
        // then
        XCTAssert(sut.stores[0] === promotionDetailTransport.promotionBO?.stores?[0])
    }
    
    // MARK: - viewWillAppear
    
    func test_givenAny_whenICallViewWillAppear_thenICallViewConfigureView() {
        
        // given
        
        // when
        sut.viewWillAppear()
        
        // then
        XCTAssert(dummyView?.isCalledConfigureViewWithTitle == true)
    }
    
    func test_givenATitleAndStores_whenICallViewWillAppear_thenICallViewConfigureViewWithAppropiateTitle() {
        
        // given
        sut.title = "BBVA Shopping App"
        sut.stores = PromotionsGenerator.getPromotionsBO().promotions[0].stores!
        
        // when
        sut.viewWillAppear()
        
        // then
        XCTAssert(dummyView?.sentTitle == String(format: "%@ (%d)", sut.title, sut.stores.count))
    }
    
    func test_givenAny_whenICallViewWillAppear_thenICallShowStores() {
        
        // given
        
        // when
        sut.viewWillAppear()
        
        // then
        XCTAssert(dummyView?.isCalledShowStoresWithDisplayItemsStores == true)
    }
    
    func test_givenAStoresAndUserLocation_whenICallViewWillAppear_thenICallShowStoresWithAppropiateDisplayItemStores() {
        
        // given
        sut.stores = PromotionsGenerator.getPromotionsBO().promotions[0].stores!
        let userLocation = GeoLocationBO(withLatitude: 44.0, withLongitude: 3.0)
        let displayItemsStores = StoresDisplayData(withStores: sut.stores, userLocation: userLocation)
        
        // when
        sut.viewWillAppear()
        
        // then
        XCTAssert(dummyView?.sentDisplayItems == displayItemsStores)
    }
    
    func test_givenAStores_whenICallViewWillAppear_thenICallShowStoresWithAppropiateDisplayItemStores() {
        
        // given
        sut.stores = PromotionsGenerator.getPromotionsBO().promotions[0].stores!
        let displayItemsStores = StoresDisplayData(withStores: sut.stores, userLocation: nil)
        
        // when
        sut.viewWillAppear()
        
        // then
        XCTAssert(dummyView?.sentDisplayItems == displayItemsStores)
    }
    
    // MARK: - showStoreList
    
    func test_givenAPromotionBOAndUserLocation_whenICallShowStoreList_thenICallBusManagerNavigateScreenWithAppropiatedTag() {
        
        // given
        
        sut.stores = PromotionsGenerator.getPromotionsBO().promotions[0].stores!
        sut.promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]
        sut.userLocation = GeoLocationBO(withLatitude: 1.0, withLongitude: 2.0)
        
        // when
        sut.itemSelected(atIndex: 0)
        
        // then
        XCTAssert(dummyBusManager.isCalledNavigateToStoresMap == true)
    }
    
    func test_givenAPromotionBOAndUserLocation_whenICallShowStoreList_thenICallBusManagerNavigateScreenWithPromotionDetailTransportWithCorrectValues() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        
        sut.stores = PromotionsGenerator.getPromotionsBO().promotions[0].stores!
        sut.promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]
        sut.userLocation = GeoLocationBO(withLatitude: 1.0, withLongitude: 2.0)

        // when
        sut.itemSelected(atIndex: 0)

        // then
        XCTAssert(dummyBusManager.dummyDetailValue.promotionBO == sut.promotionBO)
        XCTAssert(dummyBusManager.dummyDetailValue.selectedStore?.id == sut.stores[0].id)
        XCTAssert(dummyBusManager.dummyDetailValue.promotionBO?.commerceInformation.name == sut.promotionBO?.commerceInformation.name)
        XCTAssert(dummyBusManager.dummyDetailValue.category === sut.categoryBO)
    }

    // MARK: - Dummy data
    
    class DummyView: StoreListViewProtocol {
        
        var isCalledConfigureViewWithTitle = false
        var isCalledShowStoresWithDisplayItemsStores = false
        
        var sentTitle = ""
        var sentDisplayItems = StoresDisplayData()

        func configureView(withTitle title: String) {
            isCalledConfigureViewWithTitle = true
            sentTitle = title
        }
        
        func showStores(withStoresDisplayData storesDisplayData: StoresDisplayData) {
            isCalledShowStoresWithDisplayItemsStores = true
            sentDisplayItems = storesDisplayData
        }
        
        func showError(error: ModelBO) { }
        
        func changeColorEditTexts() { }
    }
    
    class DummyBusManager: BusManager {
        
        var isCalledNavigateToStoresMap = false
        
        var dummyDetailValue = StoresMapTransport()
        
        override init() {
            super.init()
            routerFactory = RouterInitFactory()
        }
        
        override func navigateScreen<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {
            if tag == "shopping-storeList-tag" && event === StoreListPageReaction.EVENT_NAV_TO_STORE_MAP_SCREEN {
                isCalledNavigateToStoresMap = true
                dummyDetailValue = values as! StoresMapTransport
            }
        }
    }
}
