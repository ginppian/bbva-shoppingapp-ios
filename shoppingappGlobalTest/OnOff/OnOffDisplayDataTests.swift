//
//  OnOffDisplayDataTests.swift
//  shoppingapp
//
//  Created by jesus.martinez on 24/10/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class OnOffDisplayDataTests: XCTestCase {

    // MARK: - generateOnOffDisplayData

    func test_givenOperativeCardBO_whenICallGenerateOnOffDisplayData_thenSwitchStatusShouldBeTrue() {

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "false").cards[0]

        // when
        let displayData = OnOffDisplayData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.switchStatus == true)

    }

    func test_givenOperativeCardBOAndOnOffIsActive_whenICallGenerateOnOffDisplayData_thenSwitchTextSwithImageSwitchValueAndAccesibilityValueShouldHaveCorrectValues() {

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        // when
        let displayData = OnOffDisplayData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.switchText == Localizables.cards.key_cards_off_text)
        XCTAssert(displayData.switchImage == ConstantsImages.Card.on_off_switch_on)
        XCTAssert(displayData.accesibilityValue == "1")

    }

    func test_givenOperativeCardBOAndOnOffIsNotActive_whenICallGenerateOnOffDisplayData_thenSwitchTextSwithImageSwitchValueAndAccesibilityValueShouldHaveCorrectValues() {

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "false").cards[0]

        // when
        let displayData = OnOffDisplayData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.switchText == Localizables.cards.key_cards_on_text)
        XCTAssert(displayData.switchImage == ConstantsImages.Card.on_off_switch_off)
        XCTAssert(displayData.accesibilityValue == "0")

    }

    func test_givenOperativeCardBOAndOnOffIsActive_whenICallGenerateOnOffDisplayData_thenSwitchValueShouldBeTrue() {

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        // when
        let displayData = OnOffDisplayData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.switchValue == true)

    }

    func test_givenOperativeCardBOAndOnOffIsNotActive_whenICallGenerateOnOffDisplayData_thenSwitchValueShouldBeFalse() {

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "false").cards[0]

        // when
        let displayData = OnOffDisplayData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.switchValue == false)

    }

    func test_givenInoperativeCardBO_whenICallGenerateOnOffDisplayData_thenSwitchStatusShouldBeFalse() {

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "INOPERATIVE", withActivationId: "ON_OFF", withIsActive: "false").cards[0]

        // when
        let displayData = OnOffDisplayData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.switchStatus == false)

    }

    func test_givenInoperativeCardBO_whenICallGenerateOnOffDisplayData_thenSwitchTextSwithImageSwitchValueAndAccesibilityValueShouldHaveCorrectValues() {

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "INOPERATIVE", withActivationId: "ON_OFF", withIsActive: "false").cards[0]

        // when
        let displayData = OnOffDisplayData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.switchText == Localizables.cards.key_cards_on_text)
        XCTAssert(displayData.switchImage == ConstantsImages.Card.on_off_switch_off)
        XCTAssert(displayData.switchValue == false)
        XCTAssert(displayData.accesibilityValue == "0")

    }
    
    func test_givenCardBOWithOutActivations_whenICallGenerateOnOffDisplayData_thenSwitchStatusFalseAndSwitchTextSwithImageSwitchValueAndAccesibilityValueShouldHaveCorrectValues() {
        
        // given
        let cardBO = CardsGenerator.getThreeCards().cards[0]
        cardBO.activations = []
        
        // when
        let displayData = OnOffDisplayData(fromCardBO: cardBO)
        
        // then
        XCTAssert(displayData.switchText == Localizables.cards.key_cards_on_text)
        XCTAssert(displayData.switchImage == ConstantsImages.Card.on_off_switch_off)
        XCTAssert(displayData.switchValue == false)
        XCTAssert(displayData.accesibilityValue == "0")
        XCTAssert(displayData.switchStatus == false)
        
    }

}
