//
//  OnOffPresenterTest.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 19/10/2017.
//  Copyright © 2017 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class OnOffPresenterTest: XCTestCase {

    let sut = OnOffPresenter()

    func test_givenAView_whenICallShowViewStatusWithTrueSwitchStatus_thenICallViewEnable() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        let cardBO = CardsGenerator.getCardBO(withStatusId: "INOPERATIVE", withActivationId: "ON_OFF", withIsActive: "false").cards[0]

        var displayOnOff = OnOffDisplayData(fromCardBO: cardBO)
        displayOnOff.switchStatus = true

        // when
        sut.showViewStatus(onOffDisplayData: displayOnOff)

        // then
        XCTAssert(dummyView.isCalledViewEnable == true)

    }

    func test_givenAView_whenICallShowViewStatusWithFalseSwitchStatus_thenICallViewDisable() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        let cardBO = CardsGenerator.getCardBO(withStatusId: "INOPERATIVE", withActivationId: "ON_OFF", withIsActive: "false").cards[0]

        var displayOnOff = OnOffDisplayData(fromCardBO: cardBO)
        displayOnOff.switchStatus = false

        // when
        sut.showViewStatus(onOffDisplayData: displayOnOff)

        // then
        XCTAssert(dummyView.isCalledViewDisable == true)

    }
    
    func test_givenACardBOwithIsActiveTrue_whenICallShowViewStatusWithFalseSwitchStatus_thenSwitchValueMatch() {
        
        // given
        let dummyView = DummyView()
        sut.view = dummyView
        let cardBO = CardsGenerator.getCardBO(withStatusId: "INOPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        
        var displayOnOff = OnOffDisplayData(fromCardBO: cardBO)
        displayOnOff.switchStatus = false
        
        // when
        sut.showViewStatus(onOffDisplayData: displayOnOff)
        
        // then
        XCTAssert(dummyView.sentSwitchValue == true)
        
    }

    class DummyView: OnOffViewProtocol {

        var isCalledViewDisable = false
        var isCalledViewEnable = false
        var sentSwitchValue = false

        func showSwitchValue(onOffDisplayData displayOnOffData: OnOffDisplayData) {
        }

        func viewDisable(withSwitchValue switchValue: Bool) {
            isCalledViewDisable = true
            sentSwitchValue = switchValue
        }

        func viewEnable() {
            isCalledViewEnable = true
        }
    }

}
