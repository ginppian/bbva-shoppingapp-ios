//
//  ShoppingFilterPresenterTests.swift
//  shoppingapp
//
//  Created by jesus.martinez on 21/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest
import RxSwift
import CellsNativeComponents

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class ShoppingFilterPresenterTests: XCTestCase {

    var sut: ShoppingFilterPresenter<DummyView>!
    var dummyBusManager: DummyBusManager!

    override func setUp() {
        super.setUp()
        dummyBusManager = DummyBusManager()
        sut = ShoppingFilterPresenter<DummyView>(busManager: dummyBusManager)
    }

    // MARK: - setModel

    func test_givenShoppingTransportWithShouldAllowSelectUsageType_whenICallSetModel_thenShowCommerFilterShouldMatch() {

        // given
        let model = ShoppingFilterTransport()
        model.shouldAllowSelectUsageType = false

        // when
        sut.setModel(model: model)

        // then
        XCTAssert(model.shouldAllowSelectUsageType == sut.showCommerceFilter)

    }

    func test_givenShoppingTransportWithCategoriesBO_whenICallSetModel_thenCategoriesBOShouldMatch() {

        // given
        let model = ShoppingFilterTransport()
        model.categoriesBO = PromotionsGenerator.getCategoryBO()

        // when
        sut.setModel(model: model)

        // then
        XCTAssert(model.categoriesBO! === sut.categoriesBO!)

    }

    func test_givenShoppingTransportWithCategoriesBO_whenICallSetModel_thenCategoriesBOAreSortedAlphabeticallyByName() {

        // given
        let model = ShoppingFilterTransport()
        model.categoriesBO = PromotionsGenerator.getCategoryBO()

        // when
        sut.setModel(model: model)

        // then
        var areSort = true
        for i in 0..<(sut.categoriesBO!.categories.count - 1)  where  sut.categoriesBO!.categories[i].name > sut.categoriesBO!.categories[i + 1].name {
                areSort = false
                break
        }

        XCTAssert(areSort == true)
    }

    func test_givenShoppingTransport_whenICallSetModel_thenShouldFilterInPreviousScreenWithValueOfTransport() {

        // given
        let model = ShoppingFilterTransport()
        model.shouldFilterInPreviousScreen = true

        // when
        sut.setModel(model: model)

        // then
        XCTAssert(sut.shouldFilterInPreviousScreen == model.shouldFilterInPreviousScreen)

    }

    func test_givenShoppingTransportWithFiltersNil_whenICallSetModel_thenShouldFiltersShouldBeEmpty() {

        // given
        let model = ShoppingFilterTransport()

        // when
        sut.setModel(model: model)

        // then
        XCTAssert(sut.filters.isEmpty() == true)

    }

    func test_givenShoppingTransportWithFilters_whenICallSetModel_thenShouldFiltersShouldMatch() {

        // given
        let filters = ShoppingFilter()
        filters.textFilter = "textFilter"
        filters.promotionTypeFilter = [.discount]
        filters.promotionUsageTypeFilter = [.online]
        filters.categoriesFilter = [.home, .health, .hobbies]
        let model = ShoppingFilterTransport()
        model.filters = filters

        // when
        sut.setModel(model: model)

        // then
        XCTAssert(sut.filters == model.filters)

    }

    // MARK: - viewWillAppear

    func test_givenAny_whenICallviewWillAppear_thenICallViewShowTextFilter() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowTextFilter == true)

    }

    func test_givenAny_whenICallviewWillAppear_thenICallViewShowTextFilterWithDisplayDataWithCorrectValues() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.textFilterDisplayData != nil)
        XCTAssert(dummyView.textFilterDisplayData?.placeHolderTitle == Localizables.promotions.key_search_commerce_name_text)
        XCTAssert(dummyView.textFilterDisplayData?.maximumLength == sut.maximuLengthTextFilter)
        XCTAssert(dummyView.textFilterDisplayData?.validCharacterSet == sut.validCharactersInTextFiler)
        XCTAssert(dummyView.textFilterDisplayData?.defaultValue == nil)

    }

    func test_givenShoppingFilterWithDefaultText_whenICallviewWillAppear_thenICallViewShowTextFilterWithDisplayDataWithCorrectValuesAndDefaultValueShouldMatch() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let filters = ShoppingFilter()
        filters.textFilter = "textFilter"
        sut.filters = filters

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.textFilterDisplayData != nil)
        XCTAssert(dummyView.textFilterDisplayData?.placeHolderTitle == Localizables.promotions.key_search_commerce_name_text)
        XCTAssert(dummyView.textFilterDisplayData?.maximumLength == sut.maximuLengthTextFilter)
        XCTAssert(dummyView.textFilterDisplayData?.validCharacterSet == sut.validCharactersInTextFiler)
        XCTAssert(dummyView.textFilterDisplayData?.defaultValue! == sut.filters.textFilter!)

    }

    func test_givenAny_whenICallviewWillAppear_thenICallViewShowPromotionFilter() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowTypePromotionFilter == true)

    }

    func test_givenAny_whenICallviewWillAppear_thenICallViewShowPromotionFilterWithAppropiateTitle() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.typePromotionsDisplayData?.title == Localizables.promotions.key_promotion_type_text.uppercased())

    }

    func test_givenAPromotionTypes_whenICallviewWillAppear_thenICallViewShowPromotionFilterWithTheSameNumberOfOptionsThanPromotionTypes() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.promotionTypes = [.discount, .point]

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.typePromotionsDisplayData?.options.count == sut.promotionTypes.count)

    }

    func test_givenAPromotionTypesAndFiltersWithPromotionTypes_whenICallviewWillAppear_thenICallViewShowPromotionFilterWithTheSameNumberOfOptionsThanPromotionTypesAndShouldBeSelectedInTheRightOrder() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let filters = ShoppingFilter()
        filters.promotionTypeFilter = [.point]
        sut.filters = filters
        sut.promotionTypes = [.discount, .point, .toMonths]

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.typePromotionsDisplayData?.options[0].isSelected == false)
        XCTAssert(dummyView.typePromotionsDisplayData?.options[1].isSelected == true)
        XCTAssert(dummyView.typePromotionsDisplayData?.options[2].isSelected == false)

    }

    func test_givenShowCommerceFilterTrue_whenICallviewWillAppear_thenICallViewShowCommerceFilter() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.showCommerceFilter = true

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowTypeCommerceFilter == true)

    }

    func test_givenShowCommerceFilterTrue_whenICallviewWillAppear_thenICallViewShowPromotionCommerceWithAppropiateTitle() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.showCommerceFilter = true

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.typeCommerceDisplayData?.title == Localizables.promotions.key_commerce_type_text.uppercased())

    }

    func test_givenShowCommerceFilterTrueAndPromotionUsageTypes_whenICallviewWillAppear_thenICallViewShowPromotionCommerceWithTheSameNumberOfOptionsThanPromotionUsageTypes() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.showCommerceFilter = true
        sut.promotionUsageTypes = [.physical, .online]

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.typeCommerceDisplayData?.options.count == sut.promotionUsageTypes.count)

    }

    func test_givenShowCommerceFilterTrueAndPromotionUsageTypesAndFilterOnline_whenICallviewWillAppear_thenICallViewShowPromotionCommerceWithTheSameNumberOfOptionsThanPromotionUsageTypesAndTheRightOptionShouldBeSelected() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.showCommerceFilter = true
        sut.promotionUsageTypes = [.physical, .online]
        let filters = ShoppingFilter()
        filters.promotionUsageTypeFilter = [.online]
        sut.filters = filters

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.typeCommerceDisplayData?.options[0].isSelected == false)
        XCTAssert(dummyView.typeCommerceDisplayData?.options[1].isSelected == true)

    }

    func test_givenShowCommerceFilterTrueAndPromotionUsageTypesEmpty_whenICallviewWillAppear_thenICallViewHideCommerceFilter() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.showCommerceFilter = true
        sut.promotionUsageTypes = [PromotionUsageType]()

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledHideTypeCommerceFilter == true)

    }

    func test_givenShowCommerceFilterFalse_whenICallviewWillAppear_thenICallViewHideCommerceFilter() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.showCommerceFilter = false

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledHideTypeCommerceFilter == true)

    }

    func test_givenNotCategoriesBO_whenICallviewWillAppear_thenICallViewHideCategoriesFilter() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.categoriesBO = nil

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledHideCategoriesFilter == true)

    }

    func test_givenCategoriesBOEmtpy_whenICallviewWillAppear_thenICallViewHideCategoriesFilter() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let categoriesBO = PromotionsGenerator.getCategoryBO()
        categoriesBO.categories = [CategoryBO]()
        sut.categoriesBO = categoriesBO

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledHideCategoriesFilter == true)

    }

    func test_givenCategoriesBO_whenICallviewWillAppear_thenICallViewShowCategoriesFilter() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.categoriesBO = PromotionsGenerator.getCategoryBO()

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowCategoriesFilter == true)

    }

    func test_givenCategoriesBO_whenICallviewWillAppear_thenICallViewShowCategoriesFilterWithDisplayDataWithAppropiateTitle() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.categoriesBO = PromotionsGenerator.getCategoryBO()

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.multiSelectorListDisplayDataSent!.title == Localizables.promotions.key_categories_text)

    }

    func test_givenCategoriesBO_whenICallviewWillAppear_thenICallViewShowCategoriesFilterWithDisplayDataWithTheSameNumberOfOptionsThanCategories() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.categoriesBO = PromotionsGenerator.getCategoryBO()

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.multiSelectorListDisplayDataSent!.options.count == sut.categoriesBO!.categories.count)

    }

    func test_givenCategoriesBOAndFiltersWithHomeActivitiesAndBeautifyCategoryTypeSelected_whenICallviewWillAppear_thenICallViewShowCategoriesFilterWithDisplayDataWithTheseCategoriesIsSelectedTrueAndOtherFalse() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.categoriesBO = PromotionsGenerator.getCategoryBO()
        let filters = ShoppingFilter()
        filters.categoriesFilter = [.home, .activities, .beautyAndGragances]
        sut.filters = filters

        // when
        sut.viewWillAppear()

        // then
        for i in 0..<dummyView.multiSelectorListDisplayDataSent!.options.count {

            if  i == 0 || i == 2 || i == 6 {
                XCTAssert(dummyView.multiSelectorListDisplayDataSent!.options[i].isSelected == true)
            } else {
                XCTAssert(dummyView.multiSelectorListDisplayDataSent!.options[i].isSelected == false)
            }

        }

    }

    func test_givenFiltersNotEmtpy_whenICallviewWillAppear_thenICallViewEnableFilter() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let filters = ShoppingFilter()
        filters.categoriesFilter = [.home, .activities, .beautyAndGragances]
        sut.filters = filters

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledEnableFilter == true)

    }

    func test_givenFiltersNotEmtpy_whenICallviewWillAppear_thenICallViewEnableRestore() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let filters = ShoppingFilter()
        filters.categoriesFilter = [.home, .activities, .beautyAndGragances]
        sut.filters = filters

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledEnableRestore == true)

    }

    func test_givenFiltersEmtpy_whenICallviewWillAppear_thenIDoNotCallViewEnableFilter() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let filters = ShoppingFilter()
        sut.filters = filters

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledEnableFilter == false)

    }

    func test_givenFiltersEmtpy_whenICallviewWillAppear_thenIDoNotCallViewEnableRestore() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let filters = ShoppingFilter()
        sut.filters = filters

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledEnableRestore == false)

    }

    func test_givenFiltersEmtpy_whenICallviewWillAppear_thenICallViewDisableRestore() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let filters = ShoppingFilter()
        sut.filters = filters

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledDisableRestore == true)

    }
    
    func test_givenFiltersEmtpy_whenICallviewWillAppear_thenICallViewDisableFilter() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let filters = ShoppingFilter()
        sut.filters = filters
        
        // when
        sut.viewWillAppear()
        
        // then
        XCTAssert(dummyView.isCalledDisableFilter == true)
        
    }

    // MARK: - updateTextFilter

    func test_givenTextAsParameter_whenICallUpdateTextFilter_thenTextFilterShouldMatchWithParameter() {

        // given
        let text = "value"

        // when
        sut.updateTextFilter(withText: text)

        // then
        XCTAssert(sut.filters.textFilter == text)

    }

    func test_givenTextAsParameter_whenICallUpdateTextFilter_thenICallViewEnableFilter() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let text = "value"

        // when
        sut.updateTextFilter(withText: text)

        // then
        XCTAssert(dummyView.isCalledEnableFilter == true)

    }

    func test_givenEmptyTextAsParameter_whenICallUpdateTextFilter_thenIDoNotCallViewEnableFilter() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let text = ""

        // when
        sut.updateTextFilter(withText: text)

        // then
        XCTAssert(dummyView.isCalledEnableFilter == false)

    }

    func test_givenTextAsParameter_whenICallUpdateTextFilter_thenICallViewEnableRestore() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let text = "value"

        // when
        sut.updateTextFilter(withText: text)

        // then
        XCTAssert(dummyView.isCalledEnableRestore == true)

    }

    func test_givenAny_whenICallUpdateTextFilter_thenICallFiltersIsEmpty() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyTransport = ShoppingFilterTransportDummy()
        sut.filters = dummyTransport

        // given
        let text = ""

        // when
        sut.updateTextFilter(withText: text)

        // then
        XCTAssert(dummyTransport.isCalledIsEmpty == true)

    }

    func test_givenFiltersEmtpy_whenICallUpdateTextFilter_thenICallViewDisableRestore() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyTransport = ShoppingFilterTransportDummy()
        dummyTransport.shouldReturnEmpty = true
        sut.filters = dummyTransport

        // given
        let text = ""

        // when
        sut.updateTextFilter(withText: text)

        // then
        XCTAssert(dummyView.isCalledDisableRestore == true)

    }

    func test_givenFiltersNotEmtpy_whenICallUpdateTextFilter_thenICallViewEnableFilter() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyTransport = ShoppingFilterTransportDummy()
        dummyTransport.shouldReturnEmpty = false
        sut.filters = dummyTransport

        // given
        let text = "value"

        // when
        sut.updateTextFilter(withText: text)

        // then
        XCTAssert(dummyView.isCalledEnableRestore == true)
        XCTAssert(dummyView.isCalledEnableFilter == true)

    }
    
    func test_givenFiltersEmtpy_whenICallUpdateTextFilter_thenICallViewDisableFilter() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyTransport = ShoppingFilterTransportDummy()
        dummyTransport.shouldReturnEmpty = true
        sut.filters = dummyTransport
        
        // given
        let text = ""
        
        // when
        sut.updateTextFilter(withText: text)
        
        // then
        XCTAssert(dummyView.isCalledDisableFilter == true)
        
    }

    // MARK: - restorePressed

    func test_givenAny_whenICallRestorePressed_thenICallViewDisableRestore() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given

        // when
        sut.restorePressed()

        // then
        XCTAssert(dummyView.isCalledDisableRestore == true)

    }

    func test_givenAny_whenICallRestorePressed_thenICallFiltersReset() {

        let dummyTransport = ShoppingFilterTransportDummy()
        sut.filters = dummyTransport

        // given

        // when
        sut.restorePressed()

        // then
        XCTAssert(dummyTransport.isCalledReset == true)

    }

    func test_givenTextFilterValue_whenICallRestorePressed_thenICallViewRestoreTextFiler() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given

        // when
        sut.restorePressed()

        // then
        XCTAssert(dummyView.isCalledRestoreTextFilter == true)

    }

    func test_givenAny_whenICallRestorePressed_thenICallViewRestoreTypePromotionFilter() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given

        // when
        sut.restorePressed()

        // then
        XCTAssert(dummyView.isCalledRestoreTypePromotionFilter == true)

    }

    func test_givenAny_whenICallRestorePressed_thenICallViewRestoreTypeCommerceFilter() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given

        // when
        sut.restorePressed()

        // then
        XCTAssert(dummyView.isCalledRestoreTypeCommerceFilter == true)

    }

    func test_givenAny_whenICallRestorePressed_thenICallViewRestoreCategoryFilter() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given

        // when
        sut.restorePressed()

        // then
        XCTAssert(dummyView.isCalledRestoreCategoriesFilter == true)

    }
    
    func test_givenAny_whenICallRestorePressed_thenICallViewDisableFilter() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        
        // when
        sut.restorePressed()
        
        // then
        XCTAssert(dummyView.isCalledDisableFilter == true)
        
    }

    // MARK: - selectedPromotionType

    func test_givenPromotionTypes_whenICallSelectedPromotionTypeWithAValidIndex_thenICallViewEnableFilter() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let index = 1
        sut.promotionTypes = [.discount, .toMonths]

        // when
        sut.selectedPromotionType(atIndex: index)

        // then
        XCTAssert(dummyView.isCalledEnableFilter == true)

    }

    func test_givenPromotionTypes_whenICallSelectedPromotionTypeWithAValidIndex_thenICallViewEnableRestore() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let index = 1
        sut.promotionTypes = [.discount, .toMonths]

        // when
        sut.selectedPromotionType(atIndex: index)

        // then
        XCTAssert(dummyView.isCalledEnableRestore == true)

    }

    func test_givenPromotionTypes_whenICallSelectedPromotionTypeWithAValidIndex_thenICallFiltersAddPromotionType() {

        let dummyTransport = ShoppingFilterTransportDummy()
        sut.filters = dummyTransport

        // given
        let index = 1
        sut.promotionTypes = [.discount, .toMonths]

        // when
        sut.selectedPromotionType(atIndex: index)

        // then
        XCTAssert(dummyTransport.isCalledAddPromotionType == true)

    }

    func test_givenPromotionTypes_whenICallSelectedPromotionTypeWithAValidIndex_thenICallFiltersAddPromotionTypeWithPromotionTypeAtIndex() {

        let dummyTransport = ShoppingFilterTransportDummy()
        sut.filters = dummyTransport

        // given
        let index = 1
        sut.promotionTypes = [.discount, .toMonths]

        // when
        sut.selectedPromotionType(atIndex: index)

        // then
        XCTAssert(dummyTransport.promotionTypeFilterSent! == sut.promotionTypes[index])

    }

    func test_givenPromotionTypes_whenICallSelectedPromotionTypeWithAInValidIndex_thenIDoNotCallViewEnableFilter() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let index = 2
        sut.promotionTypes = [.discount, .toMonths]

        // when
        sut.selectedPromotionType(atIndex: index)

        // then
        XCTAssert(dummyView.isCalledEnableFilter == false)

    }

    func test_givenPromotionTypes_whenICallSelectedPromotionTypeWithAInValidIndex_thenIDoNotCallViewEnableRestore() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let index = 2
        sut.promotionTypes = [.discount, .toMonths]

        // when
        sut.selectedPromotionType(atIndex: index)

        // then
        XCTAssert(dummyView.isCalledEnableRestore == false)

    }

    func test_givenPromotionTypes_whenICallSelectedPromotionTypeWithAInValidIndex_thenIDoNotCallFiltersAddPromotionType() {

        let dummyTransport = ShoppingFilterTransportDummy()
        sut.filters = dummyTransport

        // given
        let index = 2
        sut.promotionTypes = [.discount, .toMonths]

        // when
        sut.selectedPromotionType(atIndex: index)

        // then
        XCTAssert(dummyTransport.isCalledAddPromotionType == false)

    }

    func test_givenPromotionTypes_whenICallSelectedPromotionTypeWithAInValidIndexNegative_thenIDoNotCallViewEnableFilter() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let index = -2
        sut.promotionTypes = [.discount, .toMonths]

        // when
        sut.selectedPromotionType(atIndex: index)

        // then
        XCTAssert(dummyView.isCalledEnableFilter == false)

    }

    func test_givenPromotionTypes_whenICallSelectedPromotionTypeWithAInValidIndexNegative_thenIDoNotCallViewEnableRestore() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let index = -2
        sut.promotionTypes = [.discount, .toMonths]

        // when
        sut.selectedPromotionType(atIndex: index)

        // then
        XCTAssert(dummyView.isCalledEnableRestore == false)

    }

    func test_givenPromotionTypes_whenICallSelectedPromotionTypeWithAInValidIndexNegative_thenIDoNotCallFiltersAddPromotionType() {

        let dummyTransport = ShoppingFilterTransportDummy()
        sut.filters = dummyTransport

        // given
        let index = -2
        sut.promotionTypes = [.discount, .toMonths]

        // when
        sut.selectedPromotionType(atIndex: index)

        // then
        XCTAssert(dummyTransport.isCalledAddPromotionType == false)

    }

    // MARK: - deselectedPromotionType

    func test_givenAPromotionTypeAndAValidIndex_whenICallDeselectedPromotionType_thenICallFiltersRemovePromotionType() {

        let dummyTransport = ShoppingFilterTransportDummy()
        sut.filters = dummyTransport

        // given
        let index = 1
        sut.promotionTypes = [.discount, .toMonths]

        // when
        sut.deselectedPromotionType(atIndex: index)

        // then
        XCTAssert(dummyTransport.isCalledRemovePromotionType == true)

    }

    func test_givenAPromotionTypeAndAInValidIndex_whenICallDeselectedPromotionType_thenIDoNotCallFiltersRemovePromotionType() {

        let dummyTransport = ShoppingFilterTransportDummy()
        sut.filters = dummyTransport

        // given
        let index = 3
        sut.promotionTypes = [.discount, .toMonths]

        // when
        sut.deselectedPromotionType(atIndex: index)

        // then
        XCTAssert(dummyTransport.isCalledRemovePromotionType == false)

    }

    func test_givenAPromotionTypeAndANegativeIndex_whenICallDeselectedPromotionType_thenIDoNotCallFiltersRemovePromotionType() {

        let dummyTransport = ShoppingFilterTransportDummy()
        sut.filters = dummyTransport

        // given
        let index = -1
        sut.promotionTypes = [.discount, .toMonths]

        // when
        sut.deselectedPromotionType(atIndex: index)

        // then
        XCTAssert(dummyTransport.isCalledRemovePromotionType == false)

    }

    func test_givenAny_whenICallDeselectedPromotionType_thenICallFiltersIsEmpty() {

        let dummyTransport = ShoppingFilterTransportDummy()
        sut.filters = dummyTransport

        // given
        let index = 0

        // when
        sut.deselectedPromotionType(atIndex: index)

        // then
        XCTAssert(dummyTransport.isCalledIsEmpty == true)

    }

    func test_givenFiltersEmtpy_whenICallDeselectedPromotionType_thenICallViewDisableRestore() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyTransport = ShoppingFilterTransportDummy()
        dummyTransport.shouldReturnEmpty = true
        sut.filters = dummyTransport

        // given
        let index = 0

        // when
        sut.deselectedPromotionType(atIndex: index)

        // then
        XCTAssert(dummyView.isCalledDisableRestore == true)

    }

    func test_givenFiltersNotEmtpy_whenICallDeselectedPromotionType_thenICallViewEnableRestore() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyTransport = ShoppingFilterTransportDummy()
        dummyTransport.shouldReturnEmpty = false
        sut.filters = dummyTransport

        // given
        let index = 0

        // when
        sut.deselectedPromotionType(atIndex: index)

        // then
        XCTAssert(dummyView.isCalledEnableRestore == true)

    }
    
    func test_givenFiltersEmtpy_whenICallDeselectedPromotionType_thenICallViewDisableFiltere() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyTransport = ShoppingFilterTransportDummy()
        dummyTransport.shouldReturnEmpty = true
        sut.filters = dummyTransport
        
        // given
        let index = 0
        
        // when
        sut.deselectedPromotionType(atIndex: index)
        
        // then
        XCTAssert(dummyView.isCalledDisableFilter == true)
        
    }

    // MARK: - selectedPromotionUsageType

    func test_givenPromotionUsageTypes_whenICallSelectedPromotionUsageTypeWithAValidIndex_thenICallViewEnableFilter() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let index = 1
        sut.promotionUsageTypes = [.physical, .online]

        // when
        sut.selectedPromotionUsageType(atIndex: index)

        // then
        XCTAssert(dummyView.isCalledEnableFilter == true)

    }

    func test_givenPromotionUsageTypes_whenICallSelectedPromotionUsageTypeWithAValidIndex_thenICallViewEnableRestore() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let index = 1
        sut.promotionUsageTypes = [.physical, .online]

        // when
        sut.selectedPromotionUsageType(atIndex: index)

        // then
        XCTAssert(dummyView.isCalledEnableRestore == true)

    }

    func test_givenPromotionUsageTypes_whenICallSelectedPromotionUsageTypeWithAValidIndex_thenICallFiltersAddPromotionUsageType() {

        let dummyTransport = ShoppingFilterTransportDummy()
        sut.filters = dummyTransport

        // given
        let index = 1
        sut.promotionUsageTypes = [.physical, .online]

        // when
        sut.selectedPromotionUsageType(atIndex: index)

        // then
        XCTAssert(dummyTransport.isCalledAddPromotionUsageType == true)

    }

    func test_givenPromotionUsageTypes_whenICallSelectedPromotionUsageTypeWithAValidIndex_thenICallFiltersAddPromotionUsageTypeWithThePromotionUsageTypeAtIndex() {

        let dummyTransport = ShoppingFilterTransportDummy()
        sut.filters = dummyTransport

        // given
        let index = 1
        sut.promotionUsageTypes = [.physical, .online]

        // when
        sut.selectedPromotionUsageType(atIndex: index)

        // then
        XCTAssert(dummyTransport.promotionUsageTypeFilterSent! == sut.promotionUsageTypes[index])

    }

    func test_givenPromotionUsageTypes_whenICallSelectedPromotionUsageTypeWithAInValidIndex_thenIDoNotCallViewEnableFilter() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let index = 2
        sut.promotionUsageTypes = [.physical, .online]

        // when
        sut.selectedPromotionUsageType(atIndex: index)

        // then
        XCTAssert(dummyView.isCalledEnableFilter == false)

    }

    func test_givenPromotionUsageTypes_whenICallSelectedPromotionUsageTypeWithAInValidIndex_thenIDoNotCallViewEnableRestore() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let index = 2
        sut.promotionUsageTypes = [.physical, .online]

        // when
        sut.selectedPromotionUsageType(atIndex: index)

        // then
        XCTAssert(dummyView.isCalledEnableRestore == false)

    }

    func test_givenPromotionUsageTypes_whenICallSelectedPromotionUsageTypeWithAInValidIndex_thenIDoNotCallFiltersAddPromotionUsageType() {

        let dummyTransport = ShoppingFilterTransportDummy()
        sut.filters = dummyTransport

        // given
        let index = 2
        sut.promotionUsageTypes = [.physical, .online]

        // when
        sut.selectedPromotionUsageType(atIndex: index)

        // then
        XCTAssert(dummyTransport.isCalledAddPromotionUsageType == false)

    }

    func test_givenPromotionUsageTypes_whenICallSelectedPromotionUsageTypeWithAInValidIndexNegative_thenIDoNotCallViewEnableFilter() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let index = -2
        sut.promotionUsageTypes = [.physical, .online]

        // when
        sut.selectedPromotionUsageType(atIndex: index)

        // then
        XCTAssert(dummyView.isCalledEnableFilter == false)

    }

    func test_givenPromotionUsageTypes_whenICallSelectedPromotionUsageTypeWithAInValidIndexNegative_thenIDoNotCallViewEnableRestore() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let index = -2
        sut.promotionUsageTypes = [.physical, .online]

        // when
        sut.selectedPromotionUsageType(atIndex: index)

        // then
        XCTAssert(dummyView.isCalledEnableRestore == false)

    }

    func test_givenPromotionUsageTypes_whenICallSelectedPromotionUsageTypeWithAInValidIndexNegative_thenIDoNotCallFiltersAddPromotionUsageType() {

        let dummyTransport = ShoppingFilterTransportDummy()
        sut.filters = dummyTransport

        // given
        let index = -2
        sut.promotionUsageTypes = [.physical, .online]

        // when
        sut.selectedPromotionUsageType(atIndex: index)

        // then
        XCTAssert(dummyTransport.isCalledAddPromotionUsageType == false)

    }

    // MARK: - deselectedPromotionUsageType

    func test_givenAPromotionUsageTypeAndAValidIndex_whenICallDeselectedPromotionUsageType_thenICallFiltersRemovePromotionUsageType() {

        let dummyTransport = ShoppingFilterTransportDummy()
        sut.filters = dummyTransport

        // given
        let index = 1
        sut.promotionUsageTypes = [.physical, .online]

        // when
        sut.deselectedPromotionUsageType(atIndex: index)

        // then
        XCTAssert(dummyTransport.isCalledRemovePromotionUsageType == true)

    }

    func test_givenAPromotionUsageTypeAndAInValidIndex_whenICallDeselectedPromotionUsageType_thenIDoNotCallFiltersRemovePromotionUsageType() {

        let dummyTransport = ShoppingFilterTransportDummy()
        sut.filters = dummyTransport

        // given
        let index = 3
        sut.promotionUsageTypes = [.physical, .online]

        // when
        sut.deselectedPromotionUsageType(atIndex: index)

        // then
        XCTAssert(dummyTransport.isCalledRemovePromotionUsageType == false)

    }

    func test_givenAPromotionUsageTypeAndANegativeIndex_whenICallDeselectedPromotionUsageType_thenIDoNotCallFiltersRemovePromotionUsageType() {

        let dummyTransport = ShoppingFilterTransportDummy()
        sut.filters = dummyTransport

        // given
        let index = -1
        sut.promotionUsageTypes = [.physical, .online]

        // when
        sut.deselectedPromotionUsageType(atIndex: index)

        // then
        XCTAssert(dummyTransport.isCalledRemovePromotionUsageType == false)

    }

    func test_givenAny_whenICallDeselectedPromotionUsageType_thenICallFiltersIsEmpty() {

        let dummyTransport = ShoppingFilterTransportDummy()
        sut.filters = dummyTransport

        // given
        let index = 0

        // when
        sut.deselectedPromotionUsageType(atIndex: index)

        // then
        XCTAssert(dummyTransport.isCalledIsEmpty == true)

    }

    func test_givenFiltersEmtpy_whenICallDeselectedPromotionUsageType_thenICallViewDisableRestore() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyTransport = ShoppingFilterTransportDummy()
        dummyTransport.shouldReturnEmpty = true
        sut.filters = dummyTransport

        // given
        let index = 0

        // when
        sut.deselectedPromotionUsageType(atIndex: index)

        // then
        XCTAssert(dummyView.isCalledDisableRestore == true)

    }

    func test_givenFiltersNotEmtpy_whenICallDeselectedPromotionUsageType_thenICallViewEnableRestore() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyTransport = ShoppingFilterTransportDummy()
        dummyTransport.shouldReturnEmpty = false
        sut.filters = dummyTransport

        // given
        let index = 0

        // when
        sut.deselectedPromotionUsageType(atIndex: index)

        // then
        XCTAssert(dummyView.isCalledEnableRestore == true)

    }
    
    func test_givenFiltersEmtpy_whenICallDeselectedPromotionUsageType_thenICallViewDisableFilter() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyTransport = ShoppingFilterTransportDummy()
        dummyTransport.shouldReturnEmpty = true
        sut.filters = dummyTransport
        
        // given
        let index = 0
        
        // when
        sut.deselectedPromotionUsageType(atIndex: index)
        
        // then
        XCTAssert(dummyView.isCalledDisableFilter == true)
        
    }

    // MARK: - selectCategoryType

    func test_givenACategoriesBO_whenICallselectCategoryTypeWithAValidIndex_thenICallViewEnableFilter() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let index = 1
        sut.categoriesBO = PromotionsGenerator.getCategoryBO()

        // when
        sut.selectCategoryType(atIndex: index)

        // then
        XCTAssert(dummyView.isCalledEnableFilter == true)

    }

    func test_givenACategoriesBO_whenICallSelectCategoryTypeWithAValidIndex_thenICallViewEnableRestore() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let index = 1
        sut.categoriesBO = PromotionsGenerator.getCategoryBO()

        // when
        sut.selectCategoryType(atIndex: index)

        // then
        XCTAssert(dummyView.isCalledEnableRestore == true)

    }

    func test_givenACategoriesBO_whenICallSelectCategoryTypeWithAValidIndex_thenICallFiltersAddCategoryType() {

        let dummyTransport = ShoppingFilterTransportDummy()
        sut.filters = dummyTransport

        // given
        let index = 1
        sut.categoriesBO = PromotionsGenerator.getCategoryBO()

        // when
        sut.selectCategoryType(atIndex: index)

        // then
        XCTAssert(dummyTransport.isCalledAddCategoryType == true)

    }

    func test_givenACategoriesBO_whenICallSelectCategoryTypeWithAValidIndex_thenICallFiltersAddCategoryTypeWithCategoryTypeAtIndex() {

        let dummyTransport = ShoppingFilterTransportDummy()
        sut.filters = dummyTransport

        // given
        let index = 1
        sut.categoriesBO = PromotionsGenerator.getCategoryBO()

        // when
        sut.selectCategoryType(atIndex: index)

        // then
        XCTAssert(dummyTransport.categoryTypeSent! == sut.categoriesBO!.categories[index].id)

    }

    func test_givenACategoriesBO_whenICallSelectCategoryTypeWithAInValidIndex_thenIDoNotCallViewEnableFilter() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.categoriesBO = PromotionsGenerator.getCategoryBO()
        let index = sut.categoriesBO!.categories.count + 5

        // when
        sut.selectCategoryType(atIndex: index)

        // then
        XCTAssert(dummyView.isCalledEnableFilter == false)

    }

    func test_givenACategoriesBO_whenICallSelectCategoryTypeWithAInValidIndex_thenIDoNotCallViewEnableRestore() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.categoriesBO = PromotionsGenerator.getCategoryBO()
        let index = sut.categoriesBO!.categories.count + 5

        // when
        sut.selectCategoryType(atIndex: index)

        // then
        XCTAssert(dummyView.isCalledEnableRestore == false)

    }

    func test_givenACategoriesBO_whenICallSelectCategoryTypeWithAInValidIndex_thenIDoNotCallFiltersAddPromotionType() {

        let dummyTransport = ShoppingFilterTransportDummy()
        sut.filters = dummyTransport

        // given
        sut.categoriesBO = PromotionsGenerator.getCategoryBO()
        let index = sut.categoriesBO!.categories.count + 5

        // when
        sut.selectCategoryType(atIndex: index)

        // then
        XCTAssert(dummyTransport.isCalledAddCategoryType == false)

    }

    func test_givenACategoriesBO_whenICallSelectCategoryTypeWithAInValidIndexNegative_thenIDoNotCallViewEnableFilter() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let index = -2
        sut.categoriesBO = PromotionsGenerator.getCategoryBO()

        // when
        sut.selectCategoryType(atIndex: index)

        // then
        XCTAssert(dummyView.isCalledEnableFilter == false)

    }

    func test_givenACategoriesBO_whenICallSelectCategoryTypeWithAInValidIndexNegative_thenIDoNotCallViewEnableRestore() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let index = -2
        sut.categoriesBO = PromotionsGenerator.getCategoryBO()

        // when
        sut.selectCategoryType(atIndex: index)

        // then
        XCTAssert(dummyView.isCalledEnableRestore == false)

    }

    func test_givenACategoriesBO_whenICallSelectCategoryTypeWithAInValidIndexNegative_thenIDoNotCallFiltersAddPromotionType() {

        let dummyTransport = ShoppingFilterTransportDummy()
        sut.filters = dummyTransport

        // given
        let index = -2
        sut.categoriesBO = PromotionsGenerator.getCategoryBO()

        // when
        sut.selectCategoryType(atIndex: index)

        // then
        XCTAssert(dummyTransport.isCalledAddPromotionType == false)

    }

    // MARK: - deselectCategoryType

    func test_givenACategoryTypeAndAValidIndex_whenICallDeselectCategoryType_thenICallFiltersRemoveCategoryType() {

        let dummyTransport = ShoppingFilterTransportDummy()
        sut.filters = dummyTransport

        // given
        let index = 1
        sut.categoriesBO = PromotionsGenerator.getCategoryBO()

        // when
        sut.deselectCategoryType(atIndex: index)

        // then
        XCTAssert(dummyTransport.isCalledRemoveCategoryType == true)

    }

    func test_givenACategoryTypeAndAInValidIndex_whenICallDeselectCategoryType_thenIDoNotCallFiltersRemoveCategoryType() {

        let dummyTransport = ShoppingFilterTransportDummy()
        sut.filters = dummyTransport

        // given
        sut.categoriesBO = PromotionsGenerator.getCategoryBO()
        let index = sut.categoriesBO!.categories.count + 5

        // when
        sut.deselectCategoryType(atIndex: index)

        // then
        XCTAssert(dummyTransport.isCalledRemoveCategoryType == false)

    }

    func test_givenACategoryTypeAndANegativeIndex_whenICallDeselectCategoryType_thenIDoNotCallFiltersRemoveCategoryType() {

        let dummyTransport = ShoppingFilterTransportDummy()
        sut.filters = dummyTransport

        // given
        let index = -1
        sut.categoriesBO = PromotionsGenerator.getCategoryBO()

        // when
        sut.deselectCategoryType(atIndex: index)

        // then
        XCTAssert(dummyTransport.isCalledRemoveCategoryType == false)

    }

    func test_givenAny_whenICallDeselectCategoryType_thenICallFiltersIsEmpty() {

        let dummyTransport = ShoppingFilterTransportDummy()
        sut.filters = dummyTransport

        // given
        let index = 0

        // when
        sut.deselectCategoryType(atIndex: index)

        // then
        XCTAssert(dummyTransport.isCalledIsEmpty == true)

    }

    func test_givenFiltersEmtpy_whenICallDeselectCategoryType_thenICallViewDisableRestore() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyTransport = ShoppingFilterTransportDummy()
        dummyTransport.shouldReturnEmpty = true
        sut.filters = dummyTransport

        // given
        let index = 0

        // when
        sut.deselectCategoryType(atIndex: index)

        // then
        XCTAssert(dummyView.isCalledDisableRestore == true)

    }

    func test_givenFiltersNotEmtpy_whenICallDeselectCategoryType_thenICallViewEnableRestore() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyTransport = ShoppingFilterTransportDummy()
        dummyTransport.shouldReturnEmpty = false
        sut.filters = dummyTransport

        // given
        let index = 0

        // when
        sut.deselectCategoryType(atIndex: index)

        // then
        XCTAssert(dummyView.isCalledEnableRestore == true)

    }

    func test_givenFiltersEmtpy_whenICallDeselectCategoryType_thenICallViewDisableFilter() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyTransport = ShoppingFilterTransportDummy()
        dummyTransport.shouldReturnEmpty = true
        sut.filters = dummyTransport
        
        // given
        let index = 0
        
        // when
        sut.deselectCategoryType(atIndex: index)
        
        // then
        XCTAssert(dummyView.isCalledDisableFilter == true)
        
    }
    
    // MARK: - filterPressed

    func test_givenShouldNavigateToFilterFalse_whenICallFilterPressed_thenICallBusManagerNavigateScreenToShoppingListFiltered() {

        // given
        sut.shouldFilterInPreviousScreen = false

        // when
        sut.filterPressed()

        // then
        XCTAssert(dummyBusManager.isCalledNavigateToShoppingListFiltered == true)

    }

    func test_givenShouldNavigateToFilterFalse_whenICallFilterPressed_thenICallBusManagerNavigateScreenToShoppingListFilteredWithFilterTransportCorrectlyFilled() {

        // given
        sut.shouldFilterInPreviousScreen = false

        // when
        sut.filterPressed()

        // then
        XCTAssert(dummyBusManager.filterTransportSent != nil)
        XCTAssert(dummyBusManager.filterTransportSent!.filters != nil)
        XCTAssert(dummyBusManager.filterTransportSent!.categoriesBO === sut.categoriesBO)

    }

    func test_givenShouldNavigateToFilterTrue_whenICallFilterPressed_thenICallBusManagerPublishData() {

        // given
        sut.shouldFilterInPreviousScreen = true

        // when
        sut.filterPressed()

        // then
        XCTAssert(dummyBusManager.isCalledPublishData == true)
    }

    func test_givenShouldNavigateToFilterTrue_whenICallFilterPressed_thenICallBusManagerPublishDataWithFilterTransportCorrectlyFilled() {

        // given
        sut.shouldFilterInPreviousScreen = true

        // when
        sut.filterPressed()

        // then
        XCTAssert(dummyBusManager.filterTransportSent != nil)
        XCTAssert(dummyBusManager.filterTransportSent!.filters != nil)
        XCTAssert(dummyBusManager.filterTransportSent!.categoriesBO === sut.categoriesBO)
    }
    
    func test_givenShouldNavigateToFilterTrue_whenICallFilterPressed_thenICallBusManagerDismissViewController() {
        
        // given
        sut.shouldFilterInPreviousScreen = true
        
        // when
        sut.filterPressed()
        
        // then
        XCTAssert(dummyBusManager.isCalledDismissViewController == true)
    }
    
    // MARK: - ViewWasDismissed
    
    func test_givenAny_whenICallviewWasDismissed_thenICallViewShowTextFilter() {
        
        // given
        
        // when
        sut.viewWasDismissed()
        
        // then
        XCTAssert(dummyBusManager.isCalledNotifyEventFilterViewDismissed == true)
        
    }

    // MARK: - filterPressed
    
    func test_givenAny_whenICallFilterPressed_thenICallBusPublishDataEventTrackerNewTrackEvent() {
        
        // given
        
        // when
        sut.filterPressed()
        
        // then
        XCTAssert(dummyBusManager.isCalledPublishDataEventTrackerNewTrackEvent == true)
        
    }
    
    func test_givenAny_whenICallFilterPressed_thenICallBusPublishDataEventTrackerNewTrackEventWithAppropiatedEventModel() {
        
        // given
        
        // when
        sut.filterPressed()
        
        // then
        XCTAssert(dummyBusManager.eventSent != nil)
        XCTAssert(dummyBusManager.eventSent?.type == .seeShoppingFilterResults)
    }

    class DummyView: ShoppingFilterViewProtocol, ViewProtocol {

        var isCalledShowTextFilter = false
        var isCalledRestoreTextFilter = false
        var isCalledEnableRestore = false
        var isCalledDisableRestore = false
        var isCalledEnableFilter = false
        var isCalledDisableFilter = false
        var isCalledShowTypePromotionFilter = false
        var isCalledShowTypeCommerceFilter = false
        var isCalledHideTypeCommerceFilter = false
        var isCalledRestoreTypePromotionFilter = false
        var isCalledRestoreTypeCommerceFilter = false
        var isCalledShowCategoriesFilter = false
        var  isCalledHideCategoriesFilter = false
        var isCalledRestoreCategoriesFilter = false

        var textFilterDisplayData: TextFilterDisplayData?
        var typePromotionsDisplayData: OptionsSelectionableDisplayData?
        var typeCommerceDisplayData: OptionsSelectionableDisplayData?
        var multiSelectorListDisplayDataSent: MultiSelectorListDisplayData?

        func showError(error: ModelBO) {
        }

        func changeColorEditTexts() {
        }

        func showTextFilter(withDisplayData displayData: TextFilterDisplayData) {

            isCalledShowTextFilter = true
            textFilterDisplayData = displayData

        }

        func restoreTextFilter() {
            isCalledRestoreTextFilter = true
        }

        func enableRestore() {
            isCalledEnableRestore = true
        }

        func disableRestore() {
            isCalledDisableRestore = true
        }

        func enableFilter() {
            isCalledEnableFilter = true
        }
        
        func disableFilter() {
            isCalledDisableFilter = true
        }

        func showTypePromotionFilter(withDisplayData displayData: OptionsSelectionableDisplayData) {

            isCalledShowTypePromotionFilter = true
            typePromotionsDisplayData = displayData

        }

        func restoreTypePromotionFilter() {
            isCalledRestoreTypePromotionFilter = true
        }

        func showTypeCommerceFilter(withDisplayData displayData: OptionsSelectionableDisplayData) {

            isCalledShowTypeCommerceFilter = true
            typeCommerceDisplayData = displayData

        }

        func restoreTypeCommerceFilter() {
            isCalledRestoreTypeCommerceFilter = true

        }

        func hideTypeCommerceFilter() {

            isCalledHideTypeCommerceFilter = true

        }

        func showCategoriesFilter(withDisplayData displayData: MultiSelectorListDisplayData) {

            isCalledShowCategoriesFilter = true
            multiSelectorListDisplayDataSent = displayData

        }

        func hideCategoriesFilter() {
            isCalledHideCategoriesFilter = true
        }

        func restoreCategoriesFilter() {
            isCalledRestoreCategoriesFilter = true
        }

    }

    class DummyBusManager: BusManager {

        var isCalledNavigateToShoppingListFiltered = false
        var isCalledExpiredSession = false
        var filterTransportSent: ShoppingFilterTransport?
        var isCalledDismissViewController = false
        var isCalledPublishData = false
        var isCalledNotifyEventFilterViewDismissed = false
        var isCalledPublishDataEventTrackerNewTrackEvent = false

        var eventSent: EventProtocol?

        override init() {
            super.init()
            routerFactory = RouterInitFactory()
        }

        override func navigateScreen<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {

            if tag == ShoppingFilterPageReaction.ROUTER_TAG && event === ShoppingFilterPageReaction.EVENT_NAV_TO_SHOPPING_LIST_FILTERED {
                isCalledNavigateToShoppingListFiltered = true
                filterTransportSent = values as? ShoppingFilterTransport
            }
        }

        override func expiredSession() {
            isCalledExpiredSession = true
        }
        
        override func publishData<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {
            
            if tag == EventTracker.id && event === PageReactionConstants.NEW_TRACK_EVENT {
                isCalledPublishDataEventTrackerNewTrackEvent = true
                eventSent = values as? EventProtocol
            } else {
                isCalledPublishData = true
                filterTransportSent = values as? ShoppingFilterTransport
            }
        }
        
        override func dismissViewController(animated: Bool, completion: (() -> Void)?) {
            completion?()
            
            isCalledDismissViewController = true
        }

        override func notify<T>(tag: String, _ event: ActionSpec<T>) {
            if tag == ShoppingFilterPageReaction.ROUTER_TAG && event === ShoppingFilterPageReaction.EVENT_FILTERVIEW_DISMISSED {
                isCalledNotifyEventFilterViewDismissed = true
            }
        }
    }

    class ShoppingFilterTransportDummy: ShoppingFilter {

        var isCalledIsEmpty = false
        var isCalledAddPromotionType = false
        var isCalledAddPromotionUsageType = false
        var isCalledReset = false
        var isCalledRemovePromotionUsageType = false
        var isCalledRemovePromotionType = false
        var isCalledAddCategoryType = false
        var isCalledRemoveCategoryType = false

        var promotionTypeFilterSent: PromotionType?
        var promotionUsageTypeFilterSent: PromotionUsageType?
        var categoryTypeSent: CategoryType?

        var shouldReturnEmpty = false

        override func isEmpty() -> Bool {

            isCalledIsEmpty = true

            return shouldReturnEmpty

        }

        override func add(promotionType: PromotionType) {
            super.add(promotionType: promotionType)

            promotionTypeFilterSent = promotionType
            isCalledAddPromotionType = true

        }

        override func add(promotionUsageType: PromotionUsageType) {
            super.add(promotionUsageType: promotionUsageType)

            promotionUsageTypeFilterSent = promotionUsageType
            isCalledAddPromotionUsageType = true

        }

        override func remove(promotionType: PromotionType) {
            super.remove(promotionType: promotionType)

            isCalledRemovePromotionType = true
        }

       override func remove(promotionUsageType: PromotionUsageType) {
            super.remove(promotionUsageType: promotionUsageType)

            isCalledRemovePromotionUsageType = true
        }

        override func add(categoryType: CategoryType) {

            isCalledAddCategoryType = true

            categoryTypeSent = categoryType

        }

        override func remove(categoryType: CategoryType) {

            isCalledRemoveCategoryType = true

            categoryTypeSent = categoryType

        }

        override func reset() {
            super.reset()

            isCalledReset = true

        }

    }
}
