//
//  ErrorBOTests.swift
//  shoppingapp
//
//  Created by jesus.martinez on 28/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
@testable import shoppingappMX
#elseif TESTPE
@testable import shoppingappPE
#endif

class ErrorBOTests: XCTestCase {

    // MARK: - Session Expired

    func test_givenNotStatusNeitherCode_whenICallSessionExpired_thenReturnFalse() {

        let sut = ErrorBO()

        // given

        // when
        let result = sut.sessionExpired()

        // then
        XCTAssert(result == false)

    }

    func test_givenNotStatus_whenICallSessionExpired_thenReturnFalse() {

        let sut = ErrorBO()

        // given
        sut.code = ErrorCode.expiredSession.rawValue

        // when
        let result = sut.sessionExpired()

        // then
        XCTAssert(result == false)

    }

    func test_givenStatus403AndErrorCodeExpiredSession_whenICallSessionExpired_thenReturnTrue() {

        let sut = ErrorBO()

        // given
        sut.code = ErrorCode.expiredSession.rawValue
        sut.status = ConstantsHTTPCodes.STATUS_403

        // when
        let result = sut.sessionExpired()

        // then
        XCTAssert(result == true)

    }

    func test_givenDifferentThan403AndErrorCodeExpiredSession_whenICallSessionExpired_thenReturnFalse() {

        let sut = ErrorBO()

        // given
        sut.code = ErrorCode.expiredSession.rawValue
        sut.status = 402

        // when
        let result = sut.sessionExpired()

        // then
        XCTAssert(result == false)

    }

    func test_givenStatus403AndErrorCodeDifferentThanExpiredSession_whenICallSessionExpired_thenReturnFalse() {

        let sut = ErrorBO()

        // given
        sut.code = "67"
        sut.status = ConstantsHTTPCodes.STATUS_403

        // when
        let result = sut.sessionExpired()

        // then
        XCTAssert(result == false)

    }

    // MARK: - Error message

    func test_givenAMessageStatus403AndErrorCodeExpiredSession_whenICallErrorMessage_thenReturnExpiredTsecLocalizable() {

        // given
        let sut = ErrorBO(error: ErrorEntity(message: "Error"))
        sut.code = "68"
        sut.status = ConstantsHTTPCodes.STATUS_403

        // when
        let result = sut.errorMessage()

        // then
        XCTAssert(result == Localizables.login.key_expired_tsec)

    }

    func test_givenAMessageStatus403AndDifferentThanErrorCodeExpiredSession_whenICallErrorMessage_thenReturnMessage() {

        // given
        let message = "Error"
        let sut = ErrorBO(error: ErrorEntity(message: message))
        sut.code = "67"
        sut.status = ConstantsHTTPCodes.STATUS_403

        // when
        let result = sut.errorMessage()

        // then
        XCTAssert(result == message)

    }

    func test_givenAMessageDifferentThanStatus403AndErrorCodeExpiredSession_whenICallErrorMessage_thenReturnMessage() {

        // given
        let message = "Error"
        let sut = ErrorBO(error: ErrorEntity(message: message))
        sut.code = "68"
        sut.status = 404

        // when
        let result = sut.errorMessage()

        // then
        XCTAssert(result == message)

    }

    func test_givenAMessage_whenICallErrorMessage_thenReturnMessage() {

        // given
        let message = "Error"
        let sut = ErrorBO(error: ErrorEntity(message: message))
        sut.code = "68"
        sut.status = 404

        // when
        let result = sut.errorMessage()

        // then
        XCTAssert(result == message)

    }

}
