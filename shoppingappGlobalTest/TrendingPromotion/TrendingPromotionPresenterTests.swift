//
//  TrendingPromotionPresenterTests.swift
//  shoppingapp
//
//  Created by Javier Pino on 20/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest
import RxSwift

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class TrendingPromotionPresenterTests: XCTestCase {

    var sut: TrendingPromotionPresenter!
    let dummyView = DummyTrendingPromotionView()
    let dummyInteractor = DummyTrendingPromotionInteractor()
    
    override func setUp() {
        
        super.setUp()
        
        sut = TrendingPromotionPresenter()
        
        sut.view = dummyView
        sut.interactor = dummyInteractor
    }

    // MARK: - viewLoaded

    func test_givenAny_whenICallViewLoaded_thenICallViewShowSkeleton() {

        // given

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowSkeleton == true)
    }

    func test_givenFilterByCardsTitleIdsFalse_whenICallViewLoaded_thenIDoNotCallDelegateTrendingPromotionCardsForFilter() {

        let dummyDelegate = DummyTrendingPromotionPresenterDelegate()
        sut.delegate = dummyDelegate

        // given
        sut.filterByCardsTitleIds = false

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyDelegate.isCalledTrendingPromotionCardsForFilter == false)
    }

    func test_givenFilterByCardsTitleIdsTrue_whenICallViewLoaded_thenICallDelegateTrendingPromotionCardsForFilter() {

        let dummyDelegate = DummyTrendingPromotionPresenterDelegate()
        sut.delegate = dummyDelegate

        // given
        sut.filterByCardsTitleIds = true

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyDelegate.isCalledTrendingPromotionCardsForFilter == true)
    }

    func test_givenAny_whenICallViewLoaded_thenICallInteractorProvidePromotions() {

        // given

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotions == true)
    }

    func test_givenFilterByCardsTitleIdsFalse_whenICallViewLoaded_thenICallInteractorProvidePromotionsWithAppropiateParams() {
        
        // given
        sut.filterByCardsTitleIds = false

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.paramsSent == ShoppingParamsTransport(trendingId: TrendingType.hot.rawValue, pageSize: sut.pageSize, orderBy: .endDate))
    }

    func test_givenFilterByCardsTitleIdsTrue_whenICallViewLoaded_thenICallInteractorProvidePromotionsWithAppropiateParams() {
        let dummyDelegate = DummyTrendingPromotionPresenterDelegate()
        sut.delegate = dummyDelegate
        
        // given
        sut.filterByCardsTitleIds = true
        let cardsReturned = CardsGenerator.getCardBOWithAlias()
        dummyDelegate.returnedCards = cardsReturned

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.paramsSent == ShoppingParamsTransport(trendingId: TrendingType.hot.rawValue, titleIds: cardsReturned.titleIds(), pageSize: sut.pageSize, orderBy: .endDate))
    }

    func test_givenAny_whenICallViewLoadedAndInteractorReturnedAnError_thenICallViewShowConnectionErrorView() {

        dummyInteractor.forceError = true

        // given

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowConnectionErrorView == true)
    }

    func test_givenAny_whenICallViewLoadedAndInteractorReturnedSuccess_thenICallViewShowTrendingPromotionView() {

        // given

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowTrendingPromotionView == true)
    }

    func test_givenAPromotionBO_whenICallViewLoadedAndInteractorReturnedSuccess_thenICallViewShowTrendingPromotionViewWithRightDisplayData() {

        // given
        let promotionsBO = PromotionsGenerator.getPromotionsBO()
        let promotionBO = promotionsBO.promotions[0]
        dummyInteractor.promotionBO = promotionBO

        let expectedDisplayData = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionBO, withTypeCell: CellPromotionType.small)

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.promotionDisplayData! == expectedDisplayData)
    }

    func test_givenADelegate_whenICallViewLoadedAndInteractorReturnedAnErrorWithStatusCode403_thenICallDelegateDidReceiveError() {

        let dummyDelegate = DummyTrendingPromotionPresenterDelegate()
        dummyInteractor.forceError = true
        let error = ErrorBO()
        error.status = ConstantsHTTPCodes.STATUS_403
        error.code = ErrorCode.expiredSession.rawValue
        dummyInteractor.returnedError = error

        // given
        sut.delegate = dummyDelegate

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyDelegate.isCalledDidReceiveError == true)
    }

    func test_givenADelegate_whenICallViewLoadedAndInteractorReturnedAnErrorWithStatusCode403_thenICallDelegateDidReceiveErrorWithReturnedError() {

        let dummyDelegate = DummyTrendingPromotionPresenterDelegate()
        dummyInteractor.forceError = true
        let error = ErrorBO()
        error.status = ConstantsHTTPCodes.STATUS_403
        error.code = ErrorCode.expiredSession.rawValue
        dummyInteractor.returnedError = error

        // given
        sut.delegate = dummyDelegate

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyDelegate.receivedError === dummyInteractor.returnedError)
    }

    // MARK: - connectionErrorRetryButtonPressed

    func test_givenAny_whenICallConnectionErrorRetryButtonPressed_thenICallViewShowSkeleton() {

        // given

        // when
        sut.connectionErrorRetryButtonPressed()

        // then
        XCTAssert(dummyView.isCalledShowSkeleton == true)
    }

    func test_givenFilterByCardsTitleIdsFalse_whenICallConnectionErrorRetryButtonPressed_thenIDoNotCallDelegateTrendingPromotionCardsForFilter() {

        let dummyDelegate = DummyTrendingPromotionPresenterDelegate()
        sut.delegate = dummyDelegate

        // given
        sut.filterByCardsTitleIds = false

        // when
        sut.connectionErrorRetryButtonPressed()

        // then
        XCTAssert(dummyDelegate.isCalledTrendingPromotionCardsForFilter == false)
    }

    func test_givenFilterByCardsTitleIdsTrue_whenICallConnectionErrorRetryButtonPressed_thenICallDelegateTrendingPromotionCardsForFilter() {

        let dummyDelegate = DummyTrendingPromotionPresenterDelegate()
        sut.delegate = dummyDelegate
        
        // given
        sut.filterByCardsTitleIds = true

        // when
        sut.connectionErrorRetryButtonPressed()

        // then
        XCTAssert(dummyDelegate.isCalledTrendingPromotionCardsForFilter == true)
    }

    func test_givenAny_whenICallConnectionErrorRetryButtonPressed_thenICallInteractorProvidePromotions() {

        // given

        // when
        sut.connectionErrorRetryButtonPressed()

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotions == true)
    }

    func test_givengivenFilterByCardsTitleIdsFalse_whenICallConnectionErrorRetryButtonPressed_thenICallInteractorProvidePromotionsWithAppropiateParams() {

        // given
        sut.filterByCardsTitleIds = false

        // when
        sut.connectionErrorRetryButtonPressed()

        // then
        XCTAssert(dummyInteractor.paramsSent == ShoppingParamsTransport(trendingId: TrendingType.hot.rawValue, pageSize: sut.pageSize, orderBy: .endDate))
    }

    func test_givenFilterByCardsTitleIdsTrue_whenICallConnectionErrorRetryButtonPressed_thenICallInteractorProvidePromotionsWithAppropiateParams() {

        let dummyDelegate = DummyTrendingPromotionPresenterDelegate()
        sut.delegate = dummyDelegate

        // given
        sut.filterByCardsTitleIds = true
        let cardsReturned = CardsGenerator.getCardBOWithAlias()
        dummyDelegate.returnedCards = cardsReturned

        // when
        sut.connectionErrorRetryButtonPressed()

        // then
        XCTAssert(dummyInteractor.paramsSent == ShoppingParamsTransport(trendingId: TrendingType.hot.rawValue, titleIds: cardsReturned.titleIds(), pageSize: sut.pageSize, orderBy: .endDate))
    }

    func test_givenAny_whenICallConnectionErrorRetryButtonPressedAndInteractorReturnedAnError_thenICallViewShowConnectionErrorView() {
        
        dummyInteractor.forceError = true

        // given

        // when
        sut.connectionErrorRetryButtonPressed()

        // then
        XCTAssert(dummyView.isCalledShowConnectionErrorView == true)
    }

    func test_givenAny_whenICallConnectionErrorRetryButtonPressedAndInteractorReturnedSuccess_thenICallViewShowTrendingPromotionView() {

        // given

        // when
        sut.connectionErrorRetryButtonPressed()

        // then
        XCTAssert(dummyView.isCalledShowTrendingPromotionView == true)
    }

    func test_givenAPromotionBO_whenICallConnectionErrorRetryButtonPressedAndInteractorReturnedSuccess_thenICallViewShowTrendingPromotionViewWithRightDisplayData() {

        // given
        let promotionsBO = PromotionsGenerator.getPromotionsBO()
        let promotionBO = promotionsBO.promotions[0]
        dummyInteractor.promotionBO = promotionBO

        let expectedDisplayData = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionBO, withTypeCell: CellPromotionType.small)

        // when
        sut.connectionErrorRetryButtonPressed()

        // then
        XCTAssert(dummyView.promotionDisplayData! == expectedDisplayData)
    }

    func test_givenADelegate_whenICallConnectionErrorRetryButtonPressedAndInteractorReturnedAnErrorWithStatusCode403_thenICallDelegateDidReceiveError() {

        let dummyDelegate = DummyTrendingPromotionPresenterDelegate()
        dummyInteractor.forceError = true
        let error = ErrorBO()
        error.status = ConstantsHTTPCodes.STATUS_403
        error.code = ErrorCode.expiredSession.rawValue
        dummyInteractor.returnedError = error

        // given
        sut.delegate = dummyDelegate

        // when
        sut.connectionErrorRetryButtonPressed()

        // then
        XCTAssert(dummyDelegate.isCalledDidReceiveError == true)
    }

    func test_givenADelegate_whenICallConnectionErrorRetryButtonPressedAndInteractorReturnedAnErrorWithStatusCode403_thenICallDelegateDidReceiveErrorWithReturnedError() {

        let dummyDelegate = DummyTrendingPromotionPresenterDelegate()
        dummyInteractor.forceError = true
        let error = ErrorBO()
        error.status = ConstantsHTTPCodes.STATUS_403
        error.code = ErrorCode.expiredSession.rawValue
        dummyInteractor.returnedError = error

        // given
        sut.delegate = dummyDelegate

        // when
        sut.connectionErrorRetryButtonPressed()

        // then
        XCTAssert(dummyDelegate.receivedError === dummyInteractor.returnedError)
    }

    // MARK: - headerViewPressed

    func test_givenADelegate_whenICallHeaderViewPressed_thenICallDelegateHeaderPressed() {

        let dummyDelegate = DummyTrendingPromotionPresenterDelegate()

        // given
        sut.delegate = dummyDelegate

        // when
        sut.headerViewPressed()

        // then
        XCTAssert(dummyDelegate.isCalledHeaderPressed == true)
    }

    // MARK: - trendingPromotionViewPressed

    func test_givenADelegateAndNoPromotion_whenICallTrendingPromotionViewPressed_thenIDontCallDelegateViewPressed() {

        let dummyDelegate = DummyTrendingPromotionPresenterDelegate()

        // given
        sut.delegate = dummyDelegate

        // when
        sut.trendingPromotionViewPressed()

        // then
        XCTAssert(dummyDelegate.isCalledViewPressed == false)
    }

    func test_givenADelegateAndAPromotion_whenICallTrendingPromotionViewPressed_thenICallDelegateViewPressed() {

        let dummyDelegate = DummyTrendingPromotionPresenterDelegate()

        let promotionsBO = PromotionsGenerator.getPromotionsBO()
        let promotionBO = promotionsBO.promotions[0]

        // given
        sut.delegate = dummyDelegate
        sut.trendingPromotion = promotionBO

        // when
        sut.trendingPromotionViewPressed()

        // then
        XCTAssert(dummyDelegate.isCalledViewPressed == true)
    }

    func test_givenADelegateAndAPromotion_whenICallTrendingPromotionViewPressed_thenICallDelegateViewPressedWithSamePromotion() {

        let dummyDelegate = DummyTrendingPromotionPresenterDelegate()

        let promotionsBO = PromotionsGenerator.getPromotionsBO()
        let promotionBO = promotionsBO.promotions[0]

        // given
        sut.delegate = dummyDelegate
        sut.trendingPromotion = promotionBO

        // when
        sut.trendingPromotionViewPressed()

        // then
        XCTAssert(dummyDelegate.receivedPromotion?.id == promotionBO.id)
    }

    // MARK: - Dummydata

    class DummyTrendingPromotionView: TrendingPromotionViewProtocol {

        var isCalledShowSkeleton = false
        var isCalledShowConnectionErrorView = false
        var isCalledShowTrendingPromotionView = false
        var promotionDisplayData: DisplayItemPromotion?

        func showSkeletonView() {

            isCalledShowSkeleton = true
        }

        func showConnectionErrorView() {

            isCalledShowConnectionErrorView = true
        }

        func showTrendingPromotionView(displayData: DisplayItemPromotion) {

            isCalledShowTrendingPromotionView = true
            promotionDisplayData = displayData
        }
    }

    class DummyTrendingPromotionInteractor: TrendingPromotionInteractorProtocol {

        var isCalledProvidePromotions = false
        var forceError = false
        var promotionBO: PromotionBO?
        var returnedError: ErrorBO?
        var paramsSent: ShoppingParamsTransport?

        func providePromotions(byParams params: ShoppingParamsTransport) -> Observable <ModelBO> {

            isCalledProvidePromotions = true
            paramsSent = params

            if forceError {

                if returnedError == nil {
                    returnedError = ErrorBO(error: ErrorEntity(message: "Error"))
                }

                return Observable.error(ServiceError.GenericErrorBO(error: returnedError!))
            } else {

                if promotionBO == nil {
                    let promotionsBO = PromotionsGenerator.getPromotionsBO()
                    promotionBO = promotionsBO.promotions[0]
                }

                return Observable <ModelBO>.just(promotionBO! as ModelBO)
            }
        }
    }

    class DummyTrendingPromotionPresenterDelegate: TrendingPromotionPresenterDelegate {

        var isCalledDidReceiveError = false
        var isCalledHeaderPressed = false
        var isCalledViewPressed = false
        var isCalledTrendingPromotionCardsForFilter = false
        var receivedError: ErrorBO?
        var receivedPromotion: PromotionBO?
        var returnedCards: CardsBO?

        func trendingPromotionCardsForFilter(_ trendingPromotionPresenter: TrendingPromotionPresenter) -> CardsBO? {
            isCalledTrendingPromotionCardsForFilter = true
            return returnedCards
        }

        func trendingPromotionPresenter(_ trendingPromotionPresenter: TrendingPromotionPresenter, didReceiveError error: ErrorBO) {
            isCalledDidReceiveError = true
            receivedError = error
        }

        func trendingPromotionHeaderPressed(_ trendingPromotionPresenter: TrendingPromotionPresenter) {
            isCalledHeaderPressed = true
        }

        func trendingPromotionViewPressed(_ trendingPromotionPresenter: TrendingPromotionPresenter, promotion: PromotionBO) {
            isCalledViewPressed = true
            receivedPromotion = promotion
        }
    }
}
