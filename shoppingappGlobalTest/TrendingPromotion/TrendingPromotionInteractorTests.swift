//
//  TrendingPromotionInteractorTests.swift
//  shoppingapp
//
//  Created by Javier Pino on 24/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest
import RxSwift

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class TrendingPromotionInteractorTests: XCTestCase {

    let sut = TrendingPromotionInteractor()

    // MARK: - provideTrendingPromotion

    func test_givenParams_whenICallProvideTrendingPromotion_thenICallShoppingDataManagerProvidePromotions() {

        let dummyShoppingDataManager = DummyShoppingDataManager()
        sut.shoppingDataManager = dummyShoppingDataManager

        // given
        let params = ShoppingParamsTransport(trendingId: TrendingType.hot.rawValue, pageSize: 5, orderBy: .endDate)

        // when
        _ = sut.providePromotions(byParams: params).subscribe()

        // then
        XCTAssert(dummyShoppingDataManager.isCalledProvidePromotions == true)
    }

    func test_givenAny_whenICallProvideTrendingPromotionAndShoppingDataManagerReturnedAnError_thenICallOnError() {

        let dummyShoppingDataManager = DummyShoppingDataManager()
        dummyShoppingDataManager.forceError = true
        sut.shoppingDataManager = dummyShoppingDataManager

        // given

        let params = ShoppingParamsTransport(trendingId: TrendingType.hot.rawValue, pageSize: 5, orderBy: .endDate)

        // when
        _ = sut.providePromotions(byParams: params).subscribe(
            // then
            onNext: { _ in
                XCTFail()
            },
            onError: { _ in
                XCTAssert(true)
            })
    }

    func test_givenAny_whenICallProvideTrendingPromotionAndShoppingDataManagerReturnedSuccess_thenICallOnNext() {

        let dummyShoppingDataManager = DummyShoppingDataManager()
        sut.shoppingDataManager = dummyShoppingDataManager

        // given

        let params = ShoppingParamsTransport(trendingId: TrendingType.hot.rawValue, pageSize: 5, orderBy: .endDate)

        // when
        _ = sut.providePromotions(byParams: params).subscribe(
            // then
            onNext: { _ in
                XCTAssert(true)
            },
            onError: { _ in
                XCTFail()
            })
    }

    func test_givenAPromotionsEntity_whenICallProvideTrendingPromotionAndShoppingDataManagerReturnedOnePromotion_thenICallOnNextWithSamePromotion() {

        let dummyShoppingDataManager = DummyShoppingDataManager()
        sut.shoppingDataManager = dummyShoppingDataManager

        // given
        let promotionsEntity = PromotionsGenerator.getPromotionsEntityOnePromotion()
        dummyShoppingDataManager.promotionsEntity = promotionsEntity
        let promotionsBO = PromotionsBO(promotionsEntity: promotionsEntity)
        let promotionBO = promotionsBO.promotions[0]

        let params = ShoppingParamsTransport(trendingId: TrendingType.hot.rawValue, pageSize: 5, orderBy: .endDate)

        // when
        _ = sut.providePromotions(byParams: params).subscribe(
            // then
            onNext: { result in

                let promotion = result as? PromotionBO
                XCTAssert(promotion?.id == promotionBO.id)
            },
            onError: { _ in
                XCTFail()
            })
    }

    func test_givenAPromotionsEntityWithFivePromotionsAndASavedPromotionId_whenICallProvideTrendingPromotion_thenICallOnNextWithADifferentPromotion() {

        let dummyShoppingDataManager = DummyShoppingDataManager()
        sut.shoppingDataManager = dummyShoppingDataManager
        let dummyPreferencesManager = DummyPreferencesManager()
        sut.preferencesManager = dummyPreferencesManager

        // given
        let promotionsEntity = PromotionsGenerator.getPromotionsEntityFivePromotions()
        dummyShoppingDataManager.promotionsEntity = promotionsEntity
        let promotionsBO = PromotionsBO(promotionsEntity: promotionsEntity)
        let promotionBO = promotionsBO.promotions[0]
        dummyPreferencesManager.forceSavedId = promotionBO.id

        let params = ShoppingParamsTransport(trendingId: TrendingType.hot.rawValue, pageSize: 5, orderBy: .endDate)

        // when
        _ = sut.providePromotions(byParams: params).subscribe(
            // then
            onNext: { result in

                let promotion = result as? PromotionBO
                XCTAssert(promotion?.id != promotionBO.id)
            },
            onError: { _ in
                XCTFail()
            })
    }

    func test_givenAPromotionsEntityWithFivePromotionsARandomIndexAndUnsavedPromotionId_whenICallProvideTrendingPromotion_thenICallOnNextWithThePromotionAtSameIndex() {

        let dummyShoppingDataManager = DummyShoppingDataManager()
        sut.shoppingDataManager = dummyShoppingDataManager
        let dummyPreferencesManager = DummyPreferencesManager()
        sut.preferencesManager = dummyPreferencesManager
        let dummyRandomGenerator = DummyRandomGenerator()
        sut.randomGenerator = dummyRandomGenerator

        // given
        let promotionsEntity = PromotionsGenerator.getPromotionsEntityFivePromotions()
        dummyShoppingDataManager.promotionsEntity = promotionsEntity
        let randomIndex = 3
        dummyRandomGenerator.forcedInt = randomIndex
        let promotionsBO = PromotionsBO(promotionsEntity: promotionsEntity)
        let promotionBO = promotionsBO.promotions[randomIndex]
        dummyPreferencesManager.forceSavedId = nil

        let params = ShoppingParamsTransport(trendingId: TrendingType.hot.rawValue, pageSize: 5, orderBy: .endDate)

        // when
        _ = sut.providePromotions(byParams: params).subscribe(
            // then
            onNext: { result in

                let promotion = result as? PromotionBO
                XCTAssert(promotion?.id == promotionBO.id)
            },
            onError: { _ in
                XCTFail()
            })
    }

    func test_givenAPromotionsEntityWithFivePromotionsARandomIndexAndSavedPromotionId_whenICallProvideTrendingPromotion_thenICallOnNextWithThePromotionAtSameIndex() {

        let dummyShoppingDataManager = DummyShoppingDataManager()
        sut.shoppingDataManager = dummyShoppingDataManager
        let dummyPreferencesManager = DummyPreferencesManager()
        sut.preferencesManager = dummyPreferencesManager
        let dummyRandomGenerator = DummyRandomGenerator()
        sut.randomGenerator = dummyRandomGenerator

        // given
        let promotionsEntity = PromotionsGenerator.getPromotionsEntityFivePromotions()
        dummyShoppingDataManager.promotionsEntity = promotionsEntity
        let randomIndex = 3
        dummyRandomGenerator.forcedInt = randomIndex
        let promotionsBO = PromotionsBO(promotionsEntity: promotionsEntity)
        dummyPreferencesManager.forceSavedId = promotionsBO.promotions[1].id
        promotionsBO.promotions = promotionsBO.promotions.filter { $0.id != dummyPreferencesManager.forceSavedId }
        let promotionBO = promotionsBO.promotions[randomIndex]

        let params = ShoppingParamsTransport(trendingId: TrendingType.hot.rawValue, pageSize: 5, orderBy: .endDate)

        // when
        _ = sut.providePromotions(byParams: params).subscribe(
            // then
            onNext: { result in

                let promotion = result as? PromotionBO
                XCTAssert(promotion?.id == promotionBO.id)
            },
            onError: { _ in
                XCTFail()
            })
    }

    // MARK: - Dummy data
    class DummyShoppingDataManager: ShoppingDataManager {

        var isCalledProvidePromotions = false
        var forceError = false
        var promotionsEntity: PromotionsEntity?

        override func providePromotions(byParams params: ShoppingParamsTransport, cancellable: Bool = false) -> Observable<ModelEntity> {

            isCalledProvidePromotions = true

            if forceError {

                let errorEntity = ErrorEntity()
                errorEntity.message = "Error"

                return Observable.error(ServiceError.GenericErrorEntity(error: errorEntity))
            } else {

                if promotionsEntity == nil {
                    promotionsEntity = PromotionsGenerator.getPromotionsEntity()
                }

                return Observable <ModelEntity>.just(promotionsEntity! as ModelEntity)
            }
        }
    }

    class DummyPreferencesManager: PreferencesManager {

        var isCalledGetValueWithPreferencesKeyForKeyShowedTrendingPromotionId = false
        var forceSavedId: String?

        override func getValueWithPreferencesKey(forKey key: PreferencesManagerKeys) -> AnyObject? {

            if key == .kShowedTrendingPromotionId {

                isCalledGetValueWithPreferencesKeyForKeyShowedTrendingPromotionId = true

                return forceSavedId as AnyObject
            } else {

                return super.getValueWithPreferencesKey(forKey: key)
            }
        }

        override func saveValueWithPreferencesKey(forValue value: AnyObject, withKey key: PreferencesManagerKeys) -> Bool {
            return true
        }
    }

    class DummyRandomGenerator: RandomGenerator {

        var forcedInt: Int?

        override func randomInt(fromZeroTo toInt: Int) -> Int {
            if let forcedInt = forcedInt {
                return forcedInt
            }
            return super.randomInt(fromZeroTo: toInt)
        }
    }
}
