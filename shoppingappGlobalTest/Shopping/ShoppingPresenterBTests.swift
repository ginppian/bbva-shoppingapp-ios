//
//  ShoppingPresenterBTests.swift
//  shoppingappMXTest
//
//  Created by jesus.martinez on 4/6/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest
import RxSwift
import CellsNativeComponents
import CoreLocation

#if TESTMX
@testable import shoppingappMX
#endif

class ShoppingPresenterBTests: XCTestCase {
    
    var sut: ShoppingPresenterB<DummyView>!
    var dummyBusManager: DummyBusManager!
    var dummyView = DummyView()
    var dummyInteractor = DummyInteractor()
    var dummyLocationManager = DummyLocationManager()
    var dummySessionManager: DummySessionManager!

    override func setUp() {

        super.setUp()
        DummyPreferencesManager.configureDummyPreferences()
        dummyBusManager = DummyBusManager()
        sut = ShoppingPresenterB<DummyView>(busManager: dummyBusManager)

        dummyView = DummyView()
        sut.view = dummyView

        dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        dummyLocationManager = DummyLocationManager()
        sut.locationManager = dummyLocationManager
        DummyToastViewController.insertDummyToast()
        
        DummySessionManager.setupMock()
        dummySessionManager = (SessionDataManager.sessionDataInstance() as! DummySessionManager)
    }

    // MARK: - receive model

    func test_givenCardsBO_whenICallReceive_thenCardTitleIdsShouldMatchWithTitleIdsInCardsBO() {

        // given
        let cardsBO = CardsGenerator.getThreeCards()

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(sut.cardTitleIds! == cardsBO.titleIds()!)

    }

    func test_givenCardTitleIdsAndCardsBOWithCardTitleIdsDifferent_whenICallReceive_thenCardTitleIdsShouldMatchWithTitleIdsInCardsBO() {

        // given
        let cardsBO = CardsGenerator.getThreeCards()
        sut.cardTitleIds = ["XA", "XD"]

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(sut.cardTitleIds! == cardsBO.titleIds()!)

    }

    // MARK: - viewLoaded -> permissionDenied

    func test_givenLocationErrorByPermissionDenied_whenICallViewLoaded_thenISetLastLocationErrorType() {

        // given
        dummyLocationManager.locationErrorType = .permissionDenied

        // when
        sut.viewLoaded()

        // then
        XCTAssert(sut.lastLocationErrorType == .permissionDenied)
    }

    func test_givenLocationErrorByPermissionDenied_whenICallViewLoaded_thenICallViewShowToastNotPermission() {

        // given
        dummyLocationManager.locationErrorType = .permissionDenied

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowToastNotPermission == true)
    }

    func test_givenLocationErrorByPermissionDeniedAndUserLocationNil_whenICallViewLoaded_thenIResetDefaultAttributes() {

        // given
        dummyLocationManager.locationErrorType = .permissionDenied
        sut.userLocation = nil

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.wasPromotionsCalled == 0)
        XCTAssert(dummyInteractor.wasTrendingFinish == false)
        XCTAssert(dummyInteractor.wasPromotionsFinish == false)
        XCTAssert(dummyInteractor.wasErrorNoTsec == false)
        XCTAssert(dummyInteractor.wasPromotionsAll.isEmpty)
        XCTAssert(dummyInteractor.wasDisplayItems == DisplayItemsPromotions())

    }

    func test_givenLocationErrorByPermissionDeniedAndUserLocationNil_whenICallViewLoaded_thenICallShowSkeleton() {

        // given
        dummyLocationManager.locationErrorType = .permissionDenied
        sut.userLocation = nil

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowSkeleton == true)

    }

    func test_givenACardTitleIdsAndLocationErrorByPermissionDenied_whenICallViewLoaded_thenICallInteractorProvidePromotionsWithAppropiateParams() {

        // given
        dummyInteractor.categoriesToReturn = PromotionsGenerator.getCategoryBO()
        sut.cardTitleIds = ["XA", "XD"]
        dummyLocationManager.locationErrorType = .permissionDenied

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.categoriesParams[0] == ShoppingParamsTransport(categoryId: dummyInteractor.categoriesToReturn?.categories[0].id.rawValue, location: ShoppingParamLocation(geolocation: sut.defaultLocation, distanceParam: ShoppingDistanceParam(distance: sut.defaultDistance, lengthType: sut.lengthType)), titleIds: sut.cardTitleIds, pageSize: sut.pageSizeForPromotionsForCategories))
        XCTAssert(dummyInteractor.categoriesParams[1] == ShoppingParamsTransport(categoryId: dummyInteractor.categoriesToReturn?.categories[1].id.rawValue, location: ShoppingParamLocation(geolocation: sut.defaultLocation, distanceParam: ShoppingDistanceParam(distance: sut.defaultDistance, lengthType: sut.lengthType)), titleIds: sut.cardTitleIds, pageSize: sut.pageSizeForPromotionsForCategories))
        XCTAssert(dummyInteractor.categoriesParams[2] == ShoppingParamsTransport(categoryId: dummyInteractor.categoriesToReturn?.categories[2].id.rawValue, location: ShoppingParamLocation(geolocation: sut.defaultLocation, distanceParam: ShoppingDistanceParam(distance: sut.defaultDistance, lengthType: sut.lengthType)), titleIds: sut.cardTitleIds, pageSize: sut.pageSizeForPromotionsForCategories))

    }

    func test_givenACardTitleIdsAndLocationErrorByPermissionDenied_whenICallViewLoaded_thenICallInteractorProvideCategories() {

        // given
        sut.cardTitleIds = ["XA", "XD"]
        dummyLocationManager.locationErrorType = .permissionDenied

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.isCalledProvideCategories == true)
    }

    func test_givenACardTitleIdsAndLocationErrorByPermissionDenied_whenICallViewLoadedAndCategoriesReturnsSuccess_thenICallProvideTrendingPromotions() {

        // given
        sut.cardTitleIds = ["XA", "XD"]
        dummyLocationManager.locationErrorType = .permissionDenied

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.isCalledProvideTrendingPromotions == true)

    }

    func test_givenLocationErrorByPermissionDeniedAndDisplayItems_whenICallViewLoaded_thenINotCallInteractorProvideCategoriesAndTrendingPromotions() {

        // given
        dummyLocationManager.locationErrorType = .permissionDenied

        sut.displayItems.items = [DisplayItemPromotion.generateDisplayItemPromotion(withCategoryString: "ACTIVITIES", withCategoryType: .activities)]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.isCalledProvideCategories == false)
        XCTAssert(dummyInteractor.isCalledProvideTrendingPromotions == false)
    }

    func test_givenACardTitleIdsAndLocationErrorByPermissionDenied_whenICallViewLoadedAndCategoriesReturnsSuccess_thenICallProvideTrendingPromotionsWithAppropiateParams() {

        // given
        sut.cardTitleIds = ["XA", "XD"]
        dummyLocationManager.locationErrorType = .permissionDenied

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.paramsTrending == ShoppingParamsTransport(trendingId: TrendingType.hot.rawValue, titleIds: sut.cardTitleIds, orderBy: .endDate))

    }

    func test_givenACardTitleIdsAndLocationErrorByPermissionDenied_whenICallViewLoadedAndReturnsSuccess_thenThePresenterRetriveCategories() {

        // given
        sut.cardTitleIds = ["XA", "XD"]
        dummyLocationManager.locationErrorType = .permissionDenied

        // when
        sut.viewLoaded()
        // then
        XCTAssert(dummyInteractor.categoriesBO === sut.categories)

    }

    func test_givenACardTitleIdsAndLocationErrorByPermissionDenied_whenICallViewLoadedAndReturnsError403_thenICallViewShowError() {

        let errorBO = ErrorBO()
        errorBO.status = ConstantsHTTPCodes.STATUS_403
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        // given
        sut.cardTitleIds = ["XA", "XD"]
        dummyLocationManager.locationErrorType = .permissionDenied

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowError == true)

    }

    func test_givenACardTitleIdsAndLocationErrorByPermissionDenied_whenICallViewLoadedAndReturnsErrorDifferentThan403_thenICallViewShowEmptyTsec() {

        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        // given
        sut.cardTitleIds = ["XA", "XD"]
        dummyLocationManager.locationErrorType = .permissionDenied

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowEmptyTsec == true)

    }

    func test_givenACardTitleIdsAndLocationErrorByPermissionDenied_whenICallViewLoadedAndReturnsError403_thenINotCallViewShowEmptyTsec() {

        let errorBO = ErrorBO()
        errorBO.status = ConstantsHTTPCodes.STATUS_403
        errorBO.code = ErrorCode.expiredSession.rawValue
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        // given
        sut.cardTitleIds = ["XA", "XD"]
        dummyLocationManager.locationErrorType = .permissionDenied

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowEmptyTsec == false)

    }

    func test_givenACardTitleIdsAndLocationErrorByPermissionDenied_whenICallViewLoadedWithNoContent_thenINotCallViewShowNoContent() {

        dummyInteractor.forceNoContent = true

        // given
        sut.cardTitleIds = ["XA", "XD"]
        dummyLocationManager.locationErrorType = .permissionDenied

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowNoContent == true)

    }

    func test_givenACardTitleIdsAndLocationErrorByPermissionDenied_whenICallViewLoaded_thenICallShowCategories() {

        // given
        sut.cardTitleIds = ["XA", "XD"]
        dummyLocationManager.locationErrorType = .permissionDenied

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowCategories == true)

    }

    func test_givenACardTitleIdsAndLocationErrorByPermissionDenied_whenICallViewLoadedWithThreeCategories_thenINotCallShowCategories() {

        dummyInteractor.forceThreeCategories = true

        // given
        sut.cardTitleIds = ["XA", "XD"]
        dummyLocationManager.locationErrorType = .permissionDenied

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowCategories == false)

    }

    func test_givenACardTitleIdsAndLocationErrorByPermissionDenied_whenICallViewLoadedAndCategoriesReturnSuccess_thenICallShowCategoriesWithCorrectValue() {

        // given
        sut.cardTitleIds = ["XA", "XD"]
        dummyLocationManager.locationErrorType = .permissionDenied

        // when
        sut.viewLoaded()

        // then
        if let categoriesComponent = dummyView.categoriesToComponent {
            XCTAssert(categoriesComponent.categories.count == 27)

            for i in 0 ..< categoriesComponent.categories.count {

                XCTAssert(categoriesComponent.categories[i].id == sut.categories!.categories[i + 3].id)
                XCTAssert(categoriesComponent.categories[i].name == sut.categories!.categories[i + 3].name)

            }
        } else {
            XCTFail("Should not be nil")
        }

    }

    func test_givenACardTitleIdsAndLocationErrorByPermissionDenied_whenICallViewLoadedAndCategoriesReturnSuccess_thenICallProvidePromotionsWithCategory() {

        // given
        sut.cardTitleIds = ["XA", "XD"]
        dummyLocationManager.locationErrorType = .permissionDenied

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionByParams == true)

    }

    // MARK: - viewLoaded -> gpsOff

    func test_givenLocationErrorByGpsOff_whenICallViewLoaded_thenISetLastLocationErrorType() {

        // given
        dummyLocationManager.locationErrorType = .gpsOff

        // when
        sut.viewLoaded()

        // then
        XCTAssert(sut.lastLocationErrorType == .gpsOff)
    }

    func test_givenLocationErrorByGpsOff_whenICallViewLoaded_thenICallViewShowToastNotGPS() {

        // given
        dummyLocationManager.locationErrorType = .gpsOff

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowToastNotGPS == true)
    }

    func test_givenLocationErrorByGpsOffAndUserLocationNil_whenICallViewLoaded_thenIResetDefaultAttributes() {

        // given
        dummyLocationManager.locationErrorType = .gpsOff
        sut.userLocation = nil

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.wasPromotionsCalled == 0)
        XCTAssert(dummyInteractor.wasTrendingFinish == false)
        XCTAssert(dummyInteractor.wasPromotionsFinish == false)
        XCTAssert(dummyInteractor.wasErrorNoTsec == false)
        XCTAssert(dummyInteractor.wasPromotionsAll.isEmpty)
        XCTAssert(dummyInteractor.wasDisplayItems == DisplayItemsPromotions())

    }

    func test_givenLocationErrorByGpsOffAndUserLocationNil_whenICallViewLoaded_thenICallShowSkeleton() {

        // given
        dummyLocationManager.locationErrorType = .gpsOff
        sut.userLocation = nil

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowSkeleton == true)

    }

    func test_givenACardTitleIdsAndLocationErrorByGpsOff_whenICallViewLoaded_thenICallInteractorProvidePromotionsWithAppropiateParams() {

        // given
        dummyInteractor.categoriesToReturn = PromotionsGenerator.getCategoryBO()
        sut.cardTitleIds = ["XA", "XD"]

        dummyLocationManager.locationErrorType = .gpsOff

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.categoriesParams[0] == ShoppingParamsTransport(categoryId: dummyInteractor.categoriesToReturn?.categories[0].id.rawValue, location: ShoppingParamLocation(geolocation: sut.defaultLocation, distanceParam: ShoppingDistanceParam(distance: sut.defaultDistance, lengthType: sut.lengthType)), titleIds: sut.cardTitleIds, pageSize: sut.pageSizeForPromotionsForCategories))
        XCTAssert(dummyInteractor.categoriesParams[1] == ShoppingParamsTransport(categoryId: dummyInteractor.categoriesToReturn?.categories[1].id.rawValue, location: ShoppingParamLocation(geolocation: sut.defaultLocation, distanceParam: ShoppingDistanceParam(distance: sut.defaultDistance, lengthType: sut.lengthType)), titleIds: sut.cardTitleIds, pageSize: sut.pageSizeForPromotionsForCategories))
        XCTAssert(dummyInteractor.categoriesParams[2] == ShoppingParamsTransport(categoryId: dummyInteractor.categoriesToReturn?.categories[2].id.rawValue, location: ShoppingParamLocation(geolocation: sut.defaultLocation, distanceParam: ShoppingDistanceParam(distance: sut.defaultDistance, lengthType: sut.lengthType)), titleIds: sut.cardTitleIds, pageSize: sut.pageSizeForPromotionsForCategories))

    }

    func test_givenACardTitleIdsAndLocationErrorByGpsOff_whenICallViewLoaded_thenICallInteractorProvideCategories() {

        // given
        sut.cardTitleIds = ["XA", "XD"]

        dummyLocationManager.locationErrorType = .gpsOff

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.isCalledProvideCategories == true)
    }

    func test_givenACardTitleIdsAndLocationErrorByGpsOff_whenICallViewLoadedAndCategoriesReturnsSuccess_thenICallProvideTrendingPromotions() {

        // given
        sut.cardTitleIds = ["XA", "XD"]

        dummyLocationManager.locationErrorType = .gpsOff

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.isCalledProvideTrendingPromotions == true)

    }

    func test_givenLocationErrorByGpsOffAndDisplayItems_whenICallViewLoaded_thenINotCallInteractorProvideCategoriesAndTrendingPromotions() {

        // given
        dummyLocationManager.locationErrorType = .gpsOff

        sut.displayItems.items = [DisplayItemPromotion.generateDisplayItemPromotion(withCategoryString: "ACTIVITIES", withCategoryType: .activities)]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.isCalledProvideCategories == false)
        XCTAssert(dummyInteractor.isCalledProvideTrendingPromotions == false)
    }

    func test_givenACardTitleIdsAndLocationErrorByGpsOff_whenICallViewLoadedAndCategoriesReturnsSuccess_thenICallProvideTrendingPromotionsWithAppropiateParams() {

        // given
        sut.cardTitleIds = ["XA", "XD"]

        dummyLocationManager.locationErrorType = .gpsOff

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.paramsTrending == ShoppingParamsTransport(trendingId: TrendingType.hot.rawValue, titleIds: sut.cardTitleIds, orderBy: .endDate))

    }

    func test_givenACardTitleIdsAndLocationErrorByGpsOff_whenICallViewLoadedAndReturnsSuccess_thenThePresenterRetriveCategories() {

        // given
        dummyLocationManager.locationErrorType = .gpsOff

        // when
        sut.viewLoaded()
        // then
        XCTAssert(dummyInteractor.categoriesBO === sut.categories)

    }

    func test_givenACardTitleIdsAndLocationErrorByGpsOff_whenICallViewLoadedAndReturnsError403_thenICallViewShowError() {

        let errorBO = ErrorBO()
        errorBO.status = ConstantsHTTPCodes.STATUS_403
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        // given
        sut.cardTitleIds = ["XA", "XD"]

        dummyLocationManager.locationErrorType = .gpsOff

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowError == true)

    }

    func test_givenACardTitleIdsAndLocationErrorByGpsOff_whenICallViewLoadedAndReturnsErrorDifferentThan403_thenICallViewShowEmptyTsec() {

        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        // given
        sut.cardTitleIds = ["XA", "XD"]

        dummyLocationManager.locationErrorType = .gpsOff

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowEmptyTsec == true)

    }

    func test_givenACardTitleIdsAndLocationErrorByGpsOff_whenICallViewLoadedAndReturnsError403_thenINotCallViewShowEmptyTsec() {

        let errorBO = ErrorBO()
        errorBO.status = ConstantsHTTPCodes.STATUS_403
        errorBO.code = ErrorCode.expiredSession.rawValue
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        // given
        sut.cardTitleIds = ["XA", "XD"]

        dummyLocationManager.locationErrorType = .gpsOff

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowEmptyTsec == false)

    }

    func test_givenACardTitleIdsAndLocationErrorByGpsOff_whenICallViewLoadedWithNoContent_thenINotCallViewShowNoContent() {

        dummyInteractor.forceNoContent = true

        // given
        sut.cardTitleIds = ["XA", "XD"]

        dummyLocationManager.locationErrorType = .gpsOff

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowNoContent == true)

    }

    func test_givenACardTitleIdsAndLocationErrorByGpsOff_whenICallViewLoaded_thenICallShowCategories() {

        // given
        sut.cardTitleIds = ["XA", "XD"]

        dummyLocationManager.locationErrorType = .gpsOff

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowCategories == true)

    }

    func test_givenACardTitleIdsAndLocationErrorByGpsOff_whenICallViewLoadedWithThreeCategories_thenINotCallShowCategories() {

        dummyInteractor.forceThreeCategories = true

        // given
        sut.cardTitleIds = ["XA", "XD"]

        dummyLocationManager.locationErrorType = .gpsOff

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowCategories == false)

    }

    func test_givenACardTitleIdsAndLocationErrorByGpsOff_whenICallViewLoadedAndCategoriesReturnSuccess_thenICallShowCategoriesWithCorrectValue() {

        // given
        sut.cardTitleIds = ["XA", "XD"]

        dummyLocationManager.locationErrorType = .gpsOff

        // when
        sut.viewLoaded()

        // then
        if let categoriesComponent = dummyView.categoriesToComponent {
            XCTAssert(categoriesComponent.categories.count == 27)

            for i in 0 ..< categoriesComponent.categories.count {

                XCTAssert(categoriesComponent.categories[i].id == sut.categories!.categories[i + 3].id)
                XCTAssert(categoriesComponent.categories[i].name == sut.categories!.categories[i + 3].name)

            }
        } else {
            XCTFail("Should not be nil")
        }

    }

    func test_givenACardTitleIdsAndLocationErrorByGpsOff_whenICallViewLoadedAndCategoriesReturnSuccess_thenICallProvidePromotionsWithCategory() {

        // given
        sut.cardTitleIds = ["XA", "XD"]

        dummyLocationManager.locationErrorType = .gpsOff

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionByParams == true)

    }

    // MARK: - viewLoaded -> gpsFail

    func test_givenLocationErrorByGpsFail_whenICallViewLoaded_thenISetLastLocationErrorType() {

        // given
        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewLoaded()

        // then
        XCTAssert(sut.lastLocationErrorType == .gpsFail)
    }

    func test_givenLocationErrorByGpsFailAndUserLocationNil_whenICallViewLoaded_thenIResetDefaultAttributes() {

        // given
        dummyLocationManager.locationErrorType = .gpsFail
        sut.userLocation = nil

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.wasPromotionsCalled == 0)
        XCTAssert(dummyInteractor.wasTrendingFinish == false)
        XCTAssert(dummyInteractor.wasPromotionsFinish == false)
        XCTAssert(dummyInteractor.wasErrorNoTsec == false)
        XCTAssert(dummyInteractor.wasPromotionsAll.isEmpty)
        XCTAssert(dummyInteractor.wasDisplayItems == DisplayItemsPromotions())

    }

    func test_givenLocationErrorByGpsFailAndUserLocationNil_whenICallViewLoaded_thenICallShowSkeleton() {

        // given
        dummyLocationManager.locationErrorType = .gpsFail
        sut.userLocation = nil

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowSkeleton == true)

    }

    func test_givenACardTitleIdsAndLocationErrorByGpsFail_whenICallViewLoaded_thenICallInteractorProvidePromotionsWithAppropiateParams() {

        // given
        dummyInteractor.categoriesToReturn = PromotionsGenerator.getCategoryBO()
        sut.cardTitleIds = ["XA", "XD"]

        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.categoriesParams[0] == ShoppingParamsTransport(categoryId: dummyInteractor.categoriesToReturn?.categories[0].id.rawValue, location: ShoppingParamLocation(geolocation: sut.defaultLocation, distanceParam: ShoppingDistanceParam(distance: sut.defaultDistance, lengthType: sut.lengthType)), titleIds: sut.cardTitleIds, pageSize: sut.pageSizeForPromotionsForCategories))
        XCTAssert(dummyInteractor.categoriesParams[1] == ShoppingParamsTransport(categoryId: dummyInteractor.categoriesToReturn?.categories[1].id.rawValue, location: ShoppingParamLocation(geolocation: sut.defaultLocation, distanceParam: ShoppingDistanceParam(distance: sut.defaultDistance, lengthType: sut.lengthType)), titleIds: sut.cardTitleIds, pageSize: sut.pageSizeForPromotionsForCategories))
        XCTAssert(dummyInteractor.categoriesParams[2] == ShoppingParamsTransport(categoryId: dummyInteractor.categoriesToReturn?.categories[2].id.rawValue, location: ShoppingParamLocation(geolocation: sut.defaultLocation, distanceParam: ShoppingDistanceParam(distance: sut.defaultDistance, lengthType: sut.lengthType)), titleIds: sut.cardTitleIds, pageSize: sut.pageSizeForPromotionsForCategories))

    }

    func test_givenACardTitleIdsAndLocationErrorByGpsFail_whenICallViewLoaded_thenICallInteractorProvideCategories() {

        // given
        sut.cardTitleIds = ["XA", "XD"]

        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.isCalledProvideCategories == true)
    }

    func test_givenACardTitleIdsAndLocationErrorByGpsFail_whenICallViewLoadedAndCategoriesReturnsSuccess_thenICallProvideTrendingPromotions() {

        // given
        sut.cardTitleIds = ["XA", "XD"]

        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.isCalledProvideTrendingPromotions == true)

    }

    func test_givenLocationErrorByGpsFailAndDisplayItems_whenICallViewLoaded_thenINotCallInteractorProvideCategoriesAndTrendingPromotions() {

        // given
        dummyLocationManager.locationErrorType = .gpsFail

        sut.displayItems.items = [DisplayItemPromotion.generateDisplayItemPromotion(withCategoryString: "ACTIVITIES", withCategoryType: .activities)]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.isCalledProvideCategories == false)
        XCTAssert(dummyInteractor.isCalledProvideTrendingPromotions == false)
    }

    func test_givenACardTitleIdsAndLocationErrorByGpsFail_whenICallViewLoadedAndCategoriesReturnsSuccess_thenICallProvideTrendingPromotionsWithAppropiateParams() {

        // given
        sut.cardTitleIds = ["XA", "XD"]

        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.paramsTrending == ShoppingParamsTransport(trendingId: TrendingType.hot.rawValue, titleIds: sut.cardTitleIds, orderBy: .endDate))

    }

    func test_givenACardTitleIdsAndLocationErrorByGpsFail_whenICallViewLoadedAndReturnsSuccess_thenThePresenterRetriveCategories() {

        // given
        sut.cardTitleIds = ["XA", "XD"]

        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.categoriesBO === sut.categories)

    }

    func test_givenACardTitleIdsAndLocationErrorByGpsFail_whenICallViewLoadedAndReturnsError403_thenICallViewShowError() {

        let errorBO = ErrorBO()
        errorBO.status = ConstantsHTTPCodes.STATUS_403
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        // given
        sut.cardTitleIds = ["XA", "XD"]

        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowError == true)

    }

    func test_givenACardTitleIdsAndLocationErrorByGpsFail_whenICallViewLoadedAndReturnsErrorDifferentThan403_thenICallViewShowEmptyTsec() {

        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        // given
        sut.cardTitleIds = ["XA", "XD"]

        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowEmptyTsec == true)

    }

    func test_givenACardTitleIdsAndLocationErrorByGpsFail_whenICallViewLoadedAndReturnsError403_thenINotCallViewShowEmptyTsec() {

        let errorBO = ErrorBO()
        errorBO.status = ConstantsHTTPCodes.STATUS_403
        errorBO.code = ErrorCode.expiredSession.rawValue
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        // given
        sut.cardTitleIds = ["XA", "XD"]

        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowEmptyTsec == false)

    }

    func test_givenACardTitleIdsAndLocationErrorByGpsFail_whenICallViewLoadedWithNoContent_thenINotCallViewShowNoContent() {

        dummyInteractor.forceNoContent = true

        // given
        sut.cardTitleIds = ["XA", "XD"]

        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowNoContent == true)

    }

    func test_givenACardTitleIdsAndLocationErrorByGpsFail_whenICallViewLoaded_thenICallShowCategories() {

        // given
        sut.cardTitleIds = ["XA", "XD"]

        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowCategories == true)

    }

    func test_givenACardTitleIdsAndLocationErrorByGpsFail_whenICallViewLoadedWithThreeCategories_thenINotCallShowCategories() {

        dummyInteractor.forceThreeCategories = true

        // given
        sut.cardTitleIds = ["XA", "XD"]

        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowCategories == false)

    }

    func test_givenACardTitleIdsAndLocationErrorByGpsFail_whenICallViewLoadedAndCategoriesReturnSuccess_thenICallShowCategoriesWithCorrectValue() {

        // given
        sut.cardTitleIds = ["XA", "XD"]

        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewLoaded()

        // then
        if let categoriesComponent = dummyView.categoriesToComponent {
            XCTAssert(categoriesComponent.categories.count == 27)

            for i in 0 ..< categoriesComponent.categories.count {

                XCTAssert(categoriesComponent.categories[i].id == sut.categories!.categories[i + 3].id)
                XCTAssert(categoriesComponent.categories[i].name == sut.categories!.categories[i + 3].name)

            }
        } else {
            XCTFail("Should not be nil")
        }

    }

    func test_givenACardTitleIdsAndLocationErrorByGpsFail_whenICallViewLoadedAndCategoriesReturnSuccess_thenICallProvidePromotionsWithCategory() {

        // given
        sut.cardTitleIds = ["XA", "XD"]

        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionByParams == true)

    }

    // MARK: - viewLoaded -> locationSuccess

    func test_givenLastLocationErrorType_whenICallViewLoaded_thenLastLocationErrorTypeShouldbeNil() {

        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)

        // given
        sut.lastLocationErrorType = .gpsOff

        // when
        sut.viewLoaded()

        // then
        XCTAssert(sut.lastLocationErrorType == nil)
    }

    func test_givenLastLocationErrorTypeGPSOff_whenICallViewLoaded_thenICallViewShowToastUpdatedPromotions() {

        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)

        // given
        sut.lastLocationErrorType = .gpsOff

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowToastUpdatedPromotions == true)
    }

    func test_givenLastLocationErrorTypePermissionDenied_whenICallViewLoaded_thenICallViewShowToastUpdatedPromotions() {

        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)

        // given
        sut.lastLocationErrorType = .permissionDenied

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowToastUpdatedPromotions == true)
    }

    func test_givenLastLocationErrorTypeGPSFail_whenICallViewLoaded_thenICallViewShowToastUpdatedPromotions() {

        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)

        // given
        sut.lastLocationErrorType = .gpsFail

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowToastUpdatedPromotions == true)
    }

    func test_givenNotUserLocation_whenICallViewLoaded_thenUserLocationShouldBeFilled() {

        // given
        sut.cardTitleIds = ["XA", "XD"]
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)

        // when
        sut.viewLoaded()

        // then
        XCTAssert(sut.userLocation != nil)

    }

    func test_givenNotUserLocation_whenICallViewLoaded_thenUserLocationShouldMatchWithLatitudeAndLongitudeByParam() {

        // given
        sut.cardTitleIds = ["XA", "XD"]
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)

        // when
        sut.viewLoaded()

        // then
        XCTAssert(sut.userLocation?.latitude == 3.0)
        XCTAssert(sut.userLocation?.longitude == 4.0)

    }

    func test_givenUserLocation_whenICallViewLoaded_thenShouldNotChange() {

        // given
        sut.userLocation = GeoLocationBO(withLatitude: 8.0, withLongitude: 9.0)
        sut.cardTitleIds = ["XA", "XD"]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(sut.userLocation?.latitude == 8.0)
        XCTAssert(sut.userLocation?.longitude == 9.0)

        XCTAssert(sut.userLocation?.latitude != 3.0)
        XCTAssert(sut.userLocation?.longitude != 4.0)
    }

    func test_givenACardTitleIdsAndLocationSuccess_whenICallViewLoaded_thenICallInteractorProvideCategories() {

        // given
        sut.cardTitleIds = ["XA", "XD"]
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.isCalledProvideCategories == true)
    }

    func test_givenACardTitleIdsAndLocationSuccess_whenICallViewLoadedAndCategoriesReturnsSuccess_thenICallProvideTrendingPromotions() {

        // given
        sut.cardTitleIds = ["XA", "XD"]
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.isCalledProvideTrendingPromotions == true)

    }

    func test_givenLocationSuccessAndUserLocation_whenICallViewLoaded_thenINotCallInteractorProvideCategoriesAndTrendingPromotions() {

        // given
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        sut.userLocation = GeoLocationBO(withLatitude: 8.0, withLongitude: 9.0)

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.isCalledProvideCategories == false)
        XCTAssert(dummyInteractor.isCalledProvideTrendingPromotions == false)
    }

    func test_givenLocationSuccessAndUserLocationNil_whenICallViewLoaded_thenIResetDefaultAttributes() {

        // given
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        sut.userLocation = nil

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.wasPromotionsCalled == 0)
        XCTAssert(dummyInteractor.wasTrendingFinish == false)
        XCTAssert(dummyInteractor.wasPromotionsFinish == false)
        XCTAssert(dummyInteractor.wasErrorNoTsec == false)
        XCTAssert(dummyInteractor.wasPromotionsAll.isEmpty)
        XCTAssert(dummyInteractor.wasDisplayItems == DisplayItemsPromotions())

    }

    func test_givenLocationSuccessAndUserLocationNil_whenICallViewLoaded_thenICallShowSkeleton() {

        // given
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        sut.userLocation = nil

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowSkeleton == true)

    }

    func test_givenACardTitleIdsAndLocationSuccess_whenICallViewLoadedAndCategoriesReturnsSuccess_thenICallProvideTrendingPromotionsWithAppropiateParams() {

        // given
        sut.cardTitleIds = ["XA", "XD"]
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.paramsTrending == ShoppingParamsTransport(trendingId: TrendingType.hot.rawValue, titleIds: sut.cardTitleIds, orderBy: .endDate))

    }

    func test_givenACardTitleIdsAndLocationSuccess_whenICallViewLoadedAndCategoriesReturnsSuccess_thenICallInteractorProvidePromotionsWithAppropiateParams() {

        // given
        dummyInteractor.categoriesToReturn = PromotionsGenerator.getCategoryBO()
        sut.cardTitleIds = ["XA", "XD"]
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)

        // when
        sut.viewLoaded()

        // then

        let locationParamExpected = ShoppingParamLocation(geolocation: GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0), distanceParam: ShoppingDistanceParam(distance: sut.defaultDistance, lengthType: sut.lengthType))

        XCTAssert(dummyInteractor.categoriesParams[0] == ShoppingParamsTransport(categoryId: dummyInteractor.categoriesToReturn?.categories[0].id.rawValue, location: locationParamExpected, titleIds: sut.cardTitleIds, pageSize: sut.pageSizeForPromotionsForCategories))
        XCTAssert(dummyInteractor.categoriesParams[1] == ShoppingParamsTransport(categoryId: dummyInteractor.categoriesToReturn?.categories[1].id.rawValue, location: locationParamExpected, titleIds: sut.cardTitleIds, pageSize: sut.pageSizeForPromotionsForCategories))
        XCTAssert(dummyInteractor.categoriesParams[2] == ShoppingParamsTransport(categoryId: dummyInteractor.categoriesToReturn?.categories[2].id.rawValue, location: locationParamExpected, titleIds: sut.cardTitleIds, pageSize: sut.pageSizeForPromotionsForCategories))

    }

    func test_givenACardTitleIdsAndLocationSuccess_whenICallViewLoadedAndReturnsSuccess_thenThePresenterRetriveCategories() {

        // given
        sut.cardTitleIds = ["XA", "XD"]
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.categoriesBO === sut.categories)

    }

    func test_givenACardTitleIdsAndLocationSuccess_whenICallViewLoadedAndReturnsError403_thenICallViewShowError() {

        let errorBO = ErrorBO()
        errorBO.status = ConstantsHTTPCodes.STATUS_403
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        // given
        sut.cardTitleIds = ["XA", "XD"]
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowError == true)

    }

    func test_givenACardTitleIdsAndLocationSuccess_whenICallViewLoadedAndReturnsErrorDifferentThan403_thenICallViewShowEmptyTsec() {

        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        // given
        sut.cardTitleIds = ["XA", "XD"]
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowEmptyTsec == true)

    }

    func test_givenACardTitleIdsAndLocationSuccess_whenICallViewLoadedAndReturnsError403_thenINotCallViewShowEmptyTsec() {

        let errorBO = ErrorBO()
        errorBO.status = ConstantsHTTPCodes.STATUS_403
        errorBO.code = ErrorCode.expiredSession.rawValue
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        // given
        sut.cardTitleIds = ["XA", "XD"]
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowEmptyTsec == false)

    }

    func test_givenACardTitleIdsAndLocationSuccess_whenICallViewLoadedWithNoContent_thenINotCallViewShowNoContent() {

        dummyInteractor.forceNoContent = true

        // given
        sut.cardTitleIds = ["XA", "XD"]
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowNoContent == true)

    }

    func test_givenACardTitleIdsAndLocationSuccess_whenICallViewLoaded_thenICallShowCategories() {

        // given
        sut.cardTitleIds = ["XA", "XD"]
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowCategories == true)

    }

    func test_givenACardTitleIdsAndLocationSuccess_whenICallViewLoadedWithThreeCategories_thenINotCallShowCategories() {

        dummyInteractor.forceThreeCategories = true

        // given
        sut.cardTitleIds = ["XA", "XD"]
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowCategories == false)

    }

    func test_givenACardTitleIdsAndLocationSuccess_whenICallViewLoadedAndCategoriesReturnSuccess_thenICallShowCategoriesWithCorrectValue() {

        // given
        sut.cardTitleIds = ["XA", "XD"]
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)

        // when
        sut.viewLoaded()

        // then
        if let categoriesComponent = dummyView.categoriesToComponent {
            XCTAssert(categoriesComponent.categories.count == 27)

            for i in 0 ..< categoriesComponent.categories.count {

                XCTAssert(categoriesComponent.categories[i].id == sut.categories!.categories[i + 3].id)
                XCTAssert(categoriesComponent.categories[i].name == sut.categories!.categories[i + 3].name)

            }
        } else {
            XCTFail("Should not be nil")
        }

    }

    func test_givenACardTitleIdsAndLocationSuccess_whenICallViewLoadedAndCategoriesReturnSuccess_thenICallProvidePromotionsWithCategory() {

        // given
        sut.cardTitleIds = ["XA", "XD"]
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionByParams == true)

    }

    func test_givenAny_whenICallNavigateToFavoriteCategoriesSelector_thenIDontCallNavigateScreen() {

        // given

        // when
        sut.selectFavoriteCategoriesPressed()

        // then
        XCTAssert(dummyBusManager.isCalledNavigateScreen == false)
    }

    func test_givenAny_whenICallNavigateToFavoriteCategoriesSelector_thenIDontCallNavigateToSelectFavoriteCategories() {

        // given

        // when
        sut.selectFavoriteCategoriesPressed()

        // then
        XCTAssert(dummyBusManager.isCalledNavigateSelectFavoriteCategories == false)
    }

    func test_givenCategories_whenICallNavigateToFavoriteCategoriesSelector_thenICallNavigateScreen() {

        // given
        sut.categories = PromotionsGenerator.getCategoryBO()

        // when
        sut.selectFavoriteCategoriesPressed()

        // then
        XCTAssert(dummyBusManager.isCalledNavigateScreen == true)
    }

    func test_givenCategories_whenINavigateToFavoriteCategoriesSelector_thenICallNavigateToSelectFavoriteCategories() {

        // given
        sut.categories = PromotionsGenerator.getCategoryBO()

        // when
        sut.selectFavoriteCategoriesPressed()

        // then
        XCTAssert(dummyBusManager.isCalledNavigateSelectFavoriteCategories == true)
    }
    
    // MARK: - selectFavoriteCategoriesPressed
    
    func test_givenAny_whenICallSelectFavoriteCategoriesPressed_thenIRegisterToNotSuggestFavoriteSelection() {
        
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: false as AnyObject, withKey: PreferencesManagerKeys.kDontSuggestFavoriteSelection)
        
        // given
        
        // when
        sut.selectFavoriteCategoriesPressed()
        
        // then
        XCTAssert(PreferencesManager.sharedInstance().getDontSuggestFavoriteSelection() == true)
    }
    
    // MARK: - promotionsWillBeShown
    
    func test_givenCategoriesAndUserLoggedAndAccessCounterToZeroAndDontSuggestFavoriteSelectionToFalse_whenICallPromotionsWillBeShown_thenWaitToAccessToSuggestFavoriteSelectionIsSetToFour() {
        
        // given
        sut.categories = PromotionsGenerator.getCategoryBO()
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 0), withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 0), withKey: PreferencesManagerKeys.kWaitToAccessToSuggestFavoriteSelection)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: false as AnyObject, withKey: PreferencesManagerKeys.kDontSuggestFavoriteSelection)
        dummySessionManager.isUserLogged = true
        
        // when
        sut.promotionsWillBeShown()
        
        // then
        let value = PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kWaitToAccessToSuggestFavoriteSelection) as? NSNumber
        XCTAssert(value == NSNumber(value: 4))
    }
    
    func test_givenAccessCounterToZeroAndUserLoggedAndDontSuggestFavoriteSelectionToTrue_whenICallPromotionsWillBeShown_thenWaitToAccessToSuggestFavoriteSelectionIsNotChanged() {
        
        // given
        let originalValue = NSNumber(value: 0)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 0), withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: originalValue, withKey: PreferencesManagerKeys.kWaitToAccessToSuggestFavoriteSelection)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: true as AnyObject, withKey: PreferencesManagerKeys.kDontSuggestFavoriteSelection)
        dummySessionManager.isUserLogged = true
        
        // when
        sut.promotionsWillBeShown()
        
        // then
        let value = PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kWaitToAccessToSuggestFavoriteSelection) as? NSNumber
        XCTAssert(value == originalValue)
    }
    
    func test_givenCategoriesAndUserNotLogged_whenICallPromotionsWillBeShown_thenIDontIncrementAccessCounter() {
        
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 0), withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        
        // given
        sut.categories = PromotionsGenerator.getCategoryBO()
        dummySessionManager.isUserLogged = false
        
        // when
        let numberBeforeCall = PreferencesManager.sharedInstance().getPromotionsSectionAccessCounter()
        sut.promotionsWillBeShown()
        let numberAfterCall = PreferencesManager.sharedInstance().getPromotionsSectionAccessCounter()
        
        // then
        XCTAssert(numberAfterCall == numberBeforeCall)
    }
    
    func test_givenCategoriesAndUserLogged_whenICallPromotionsWillBeShown_thenIIncrementAccessCounter() {
        
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 0), withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        
        // given
        sut.categories = PromotionsGenerator.getCategoryBO()
        dummySessionManager.isUserLogged = true
        
        // when
        let numberBeforeCall = PreferencesManager.sharedInstance().getPromotionsSectionAccessCounter()
        sut.promotionsWillBeShown()
        let numberAfterCall = PreferencesManager.sharedInstance().getPromotionsSectionAccessCounter()
        
        // then
        XCTAssert(numberAfterCall == numberBeforeCall + 1)
    }
    
    func test_givenNoCategoriesAndUserLogged_whenICallPromotionsWillBeShown_thenIDontIncrementAccessCounter() {
        
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 0), withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        
        // given
        dummySessionManager.isUserLogged = true
        
        // when
        let numberBeforeCall = PreferencesManager.sharedInstance().getPromotionsSectionAccessCounter()
        sut.promotionsWillBeShown()
        let numberAfterCall = PreferencesManager.sharedInstance().getPromotionsSectionAccessCounter()
        
        // then
        XCTAssert(numberAfterCall == numberBeforeCall)
    }
    
    func test_givenCategoriesAndUserLoggedAndToastIsVisible_whenICallPromotionsWillBeShown_thenIDontIncrementAccessCounter() {
        
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 0), withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        
        // given
        sut.categories = PromotionsGenerator.getCategoryBO()
        (ToastManager.shared().toastViewController as? DummyToastViewController)?.toastVisible = true
        dummySessionManager.isUserLogged = true
        
        // when
        let numberBeforeCall = PreferencesManager.sharedInstance().getPromotionsSectionAccessCounter()
        sut.promotionsWillBeShown()
        let numberAfterCall = PreferencesManager.sharedInstance().getPromotionsSectionAccessCounter()
        
        // then
        XCTAssert(numberAfterCall == numberBeforeCall)
    }
    
    func test_givenCategoriesAndUserLoggedAndNoToastVisible_whenICallPromotionsWillBeShown_thenIIncrementAccessCounter() {
        
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 0), withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        
        // given
        sut.categories = PromotionsGenerator.getCategoryBO()
        (ToastManager.shared().toastViewController as? DummyToastViewController)?.toastVisible = false
        dummySessionManager.isUserLogged = true
        
        // when
        let numberBeforeCall = PreferencesManager.sharedInstance().getPromotionsSectionAccessCounter()
        sut.promotionsWillBeShown()
        let numberAfterCall = PreferencesManager.sharedInstance().getPromotionsSectionAccessCounter()
        
        // then
        XCTAssert(numberAfterCall == numberBeforeCall + 1)
    }
    
    func test_givenNoCategoriesAndUserLoggedAndAccessCounterToOneAndDontSuggestFavoriteSelectionToFalse_whenICallPromotionsWillBeShownForSecondTime_thenIDontNavigateToSelectFavoriteCategories() {
        
        // given
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 1), withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: false as AnyObject, withKey: PreferencesManagerKeys.kDontSuggestFavoriteSelection)
        dummySessionManager.isUserLogged = true
        
        // when
        sut.promotionsWillBeShown()
        
        // then
        XCTAssert(dummyBusManager.isCalledNavigateScreen == false)
        XCTAssert(dummyBusManager.isCalledNavigateSelectFavoriteCategories == false)
    }
    
    func test_givenCategoriesAndUserLoggedAndAccessCounterToOneAndDontSuggestFavoriteSelectionToTrue_whenICallPromotionsWillBeShownForSecondTime_thenIDontNavigateToSelectFavoriteCategories() {
        
        // given
        sut.categories = PromotionsGenerator.getCategoryBO()
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 1), withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: true as AnyObject, withKey: PreferencesManagerKeys.kDontSuggestFavoriteSelection)
        dummySessionManager.isUserLogged = true
        
        // when
        sut.promotionsWillBeShown()
        
        // then
        XCTAssert(dummyBusManager.isCalledNavigateScreen == false)
        XCTAssert(dummyBusManager.isCalledNavigateSelectFavoriteCategories == false)
    }
    
    func test_givenCategoriesAndUserLoggedAndAccessCounterToOneAndDontSuggestFavoriteSelectionToFalseAndAToastVisible_whenICallPromotionsWillBeShownForSecondTime_thenIDontNavigateToSelectFavoriteCategories() {
        
        // given
        sut.categories = PromotionsGenerator.getCategoryBO()
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 1), withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: false as AnyObject, withKey: PreferencesManagerKeys.kDontSuggestFavoriteSelection)
        (ToastManager.shared().toastViewController as? DummyToastViewController)?.toastVisible = true
        dummySessionManager.isUserLogged = true
        
        // when
        sut.promotionsWillBeShown()
        
        // then
        XCTAssert(dummyBusManager.isCalledNavigateScreen == false)
        XCTAssert(dummyBusManager.isCalledNavigateSelectFavoriteCategories == false)
    }
    
    func test_givenCategoriesAndUserLoggedAndAccessCounterToOneAndDontSuggestFavoriteSelectionToFalseAndNoToastVisiblewhenICallPromotionsWillBeShownForSecondTime_thenINavigateToSelectFavoriteCategories() {
        
        // given
        sut.categories = PromotionsGenerator.getCategoryBO()
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 1), withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: false as AnyObject, withKey: PreferencesManagerKeys.kDontSuggestFavoriteSelection)
        (ToastManager.shared().toastViewController as? DummyToastViewController)?.toastVisible = false
        dummySessionManager.isUserLogged = true
        
        // when
        sut.promotionsWillBeShown()
        
        // then
        XCTAssert(dummyBusManager.isCalledNavigateScreen == true)
        XCTAssert(dummyBusManager.isCalledNavigateSelectFavoriteCategories == true)
    }
    
    func test_givenNoCategoriesAndUserLoggedAndAccessCounterToThreeAndWaitToAccessToSuggestFavoriteSelectionToFourAndDontSuggestFavoriteSelectionToFalse_whenICallPromotionsWillBeShown_thenIDontNavigateToSelectFavoriteCategories() {
        
        // given
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 3), withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 4), withKey: PreferencesManagerKeys.kWaitToAccessToSuggestFavoriteSelection)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: false as AnyObject, withKey: PreferencesManagerKeys.kDontSuggestFavoriteSelection)
        dummySessionManager.isUserLogged = true
        
        // when
        sut.promotionsWillBeShown()
        
        // then
        XCTAssert(dummyBusManager.isCalledNavigateScreen == false)
        XCTAssert(dummyBusManager.isCalledNavigateSelectFavoriteCategories == false)
    }
    
    func test_givenCategoriesAndUserLoggedAndAccessCounterToThreeAndWaitToAccessToSuggestFavoriteSelectionToFourAndDontSuggestFavoriteSelectionToFalseAndToastVisible_whenICallPromotionsWillBeShown_thenIDontNavigateToSelectFavoriteCategories() {
        
        // given
        sut.categories = PromotionsGenerator.getCategoryBO()
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 3), withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 4), withKey: PreferencesManagerKeys.kWaitToAccessToSuggestFavoriteSelection)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: false as AnyObject, withKey: PreferencesManagerKeys.kDontSuggestFavoriteSelection)
        (ToastManager.shared().toastViewController as? DummyToastViewController)?.toastVisible = true
        dummySessionManager.isUserLogged = true
        
        // when
        sut.promotionsWillBeShown()
        
        // then
        XCTAssert(dummyBusManager.isCalledNavigateScreen == false)
        XCTAssert(dummyBusManager.isCalledNavigateSelectFavoriteCategories == false)
    }
    
    func test_givenCategoriesAndUserLoggedAndAccessCounterToThreeAndWaitToAccessToSuggestFavoriteSelectionToFourAndDontSuggestFavoriteSelectionToFalseAndNoNoToastVisible_whenICallPromotionsWillBeShown_thenINavigateToSelectFavoriteCategories() {
        
        // given
        sut.categories = PromotionsGenerator.getCategoryBO()
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 3), withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 4), withKey: PreferencesManagerKeys.kWaitToAccessToSuggestFavoriteSelection)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: false as AnyObject, withKey: PreferencesManagerKeys.kDontSuggestFavoriteSelection)
        (ToastManager.shared().toastViewController as? DummyToastViewController)?.toastVisible = false
        dummySessionManager.isUserLogged = true
        
        // when
        sut.promotionsWillBeShown()
        
        // then
        XCTAssert(dummyBusManager.isCalledNavigateScreen == true)
        XCTAssert(dummyBusManager.isCalledNavigateSelectFavoriteCategories == true)
    }
    
    func test_givenCategoriesAndUserLoggedAndAccessCounterToThreeAndWaitToAccessToSuggestFavoriteSelectionToFourAndDontSuggestFavoriteSelectionToTrue_whenICallPromotionsWillBeShown_thenIDontNavigateToSelectFavoriteCategories() {
        
        // given
        sut.categories = PromotionsGenerator.getCategoryBO()
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 3), withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 4), withKey: PreferencesManagerKeys.kWaitToAccessToSuggestFavoriteSelection)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: true as AnyObject, withKey: PreferencesManagerKeys.kDontSuggestFavoriteSelection)
        dummySessionManager.isUserLogged = true
        
        // when
        sut.promotionsWillBeShown()
        
        // then
        XCTAssert(dummyBusManager.isCalledNavigateScreen == false)
        XCTAssert(dummyBusManager.isCalledNavigateSelectFavoriteCategories == false)
    }
    
    func test_givenNoCategoriesAndUserLoggedAndAccessCounterToFourAndWaitToAccessToSuggestFavoriteSelectionToFiveAndDontSuggestFavoriteSelectionToFalse_whenICallPromotionsWillBeShown_thenIDontNavigateToSelectFavoriteCategories() {
        
        // given
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 4), withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 5), withKey: PreferencesManagerKeys.kWaitToAccessToSuggestFavoriteSelection)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: false as AnyObject, withKey: PreferencesManagerKeys.kDontSuggestFavoriteSelection)
        dummySessionManager.isUserLogged = true
        
        // when
        sut.promotionsWillBeShown()
        
        // then
        XCTAssert(dummyBusManager.isCalledNavigateScreen == false)
        XCTAssert(dummyBusManager.isCalledNavigateSelectFavoriteCategories == false)
    }
    
    func test_givenCategoriesAndUserLoggedAndAccessCounterToFourAndWaitToAccessToSuggestFavoriteSelectionToFiveAndDontSuggestFavoriteSelectionToFalseAndToastVisible_whenICallPromotionsWillBeShown_thenIDontNavigateToSelectFavoriteCategories() {
        
        // given
        sut.categories = PromotionsGenerator.getCategoryBO()
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 4), withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 5), withKey: PreferencesManagerKeys.kWaitToAccessToSuggestFavoriteSelection)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: false as AnyObject, withKey: PreferencesManagerKeys.kDontSuggestFavoriteSelection)
        (ToastManager.shared().toastViewController as? DummyToastViewController)?.toastVisible = true
        dummySessionManager.isUserLogged = true
        
        // when
        sut.promotionsWillBeShown()
        
        // then
        XCTAssert(dummyBusManager.isCalledNavigateScreen == false)
        XCTAssert(dummyBusManager.isCalledNavigateSelectFavoriteCategories == false)
    }
    
    func test_givenCategoriesAndUserLoggedAndAccessCounterToFourAndWaitToAccessToSuggestFavoriteSelectionToFiveAndDontSuggestFavoriteSelectionToFalseAndNoToastVisible_whenICallPromotionsWillBeShown_thenINavigateToSelectFavoriteCategories() {
        
        // given
        sut.categories = PromotionsGenerator.getCategoryBO()
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 4), withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 5), withKey: PreferencesManagerKeys.kWaitToAccessToSuggestFavoriteSelection)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: false as AnyObject, withKey: PreferencesManagerKeys.kDontSuggestFavoriteSelection)
        (ToastManager.shared().toastViewController as? DummyToastViewController)?.toastVisible = false
        dummySessionManager.isUserLogged = true
        
        // when
        sut.promotionsWillBeShown()
        
        // then
        XCTAssert(dummyBusManager.isCalledNavigateScreen == true)
        XCTAssert(dummyBusManager.isCalledNavigateSelectFavoriteCategories == true)
    }
    
    func test_givenCategoriesAndUserLoggedAndAccessCounterToFourAndWaitToAccessToSuggestFavoriteSelectionToFiveAndDontSuggestFavoriteSelectionToTrue_whenICallPromotionsWillBeShown_thenIDontNavigateToSelectFavoriteCategories() {
        
        // given
        sut.categories = PromotionsGenerator.getCategoryBO()
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 4), withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 5), withKey: PreferencesManagerKeys.kWaitToAccessToSuggestFavoriteSelection)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: true as AnyObject, withKey: PreferencesManagerKeys.kDontSuggestFavoriteSelection)
        dummySessionManager.isUserLogged = true
        
        // when
        sut.promotionsWillBeShown()
        
        // then
        XCTAssert(dummyBusManager.isCalledNavigateScreen == false)
        XCTAssert(dummyBusManager.isCalledNavigateSelectFavoriteCategories == false)
    }
    
    // MARK: - userScrollsToBottom
    
    func test_givenAccessCounterToThree_whenICallUserScrollsToBottom_thenWaitToAccessToSuggestFavoriteSelectionIsSetToFive() {
        
        // given
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 3), withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 4), withKey: PreferencesManagerKeys.kWaitToAccessToSuggestFavoriteSelection)
        
        // when
        sut.userScrollsToBottom()
        
        // then
        let value = PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kWaitToAccessToSuggestFavoriteSelection) as? NSNumber
        XCTAssert(value == NSNumber(value: 5))
    }
    
    func test_givenAccessCounterDifferentOfThree_whenICallUserScrollsToBottom_thenWaitToAccessToSuggestFavoriteSelectionIsNotChanged() {
        
        let originalValue = NSNumber(value: 4)
        
        // given
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 666), withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: originalValue, withKey: PreferencesManagerKeys.kWaitToAccessToSuggestFavoriteSelection)
        
        // when
        sut.userScrollsToBottom()
        
        // then
        let value = PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kWaitToAccessToSuggestFavoriteSelection) as? NSNumber
        XCTAssert(value == originalValue)
    }
    
    // MARK: - toastViewDidDissapear
    
    func test_givenCategoriesAndUserLogged_whenICallToastViewDidDissapearAndViewIsVisible_thenIIncrementAccessCounterAndRegisterToCountAccessInNextApparition() {
        
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 0), withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        
        // given
        sut.categories = PromotionsGenerator.getCategoryBO()
        dummySessionManager.isUserLogged = true
        
        // when
        let numberBeforeCall = PreferencesManager.sharedInstance().getPromotionsSectionAccessCounter()
        sut.toastViewDidDissapearAndViewIsVisible()
        let numberAfterCall = PreferencesManager.sharedInstance().getPromotionsSectionAccessCounter()
        
        // then
        XCTAssert(numberAfterCall == numberBeforeCall + 1)
        XCTAssert(sut.countAccessInNextApparition == false)
    }
    
    func test_givenCategoriesAndUserLogged_whenICallToastViewDidDissapearButViewIsNotVisible_thenIDontIncrementAccessCounterAndDontRegisterToCountAccessInNextApparition() {
        
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 0), withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        
        // given
        sut.categories = PromotionsGenerator.getCategoryBO()
        dummySessionManager.isUserLogged = true
        
        // when
        let numberBeforeCall = PreferencesManager.sharedInstance().getPromotionsSectionAccessCounter()
        sut.toastViewDidDissapearButViewIsNotVisible()
        let numberAfterCall = PreferencesManager.sharedInstance().getPromotionsSectionAccessCounter()
        
        // then
        XCTAssert(numberAfterCall == numberBeforeCall)
        XCTAssert(sut.countAccessInNextApparition == true)
    }
    
    // MARK: - checkCountAccessInNextApparition
    
    func test_givenCategoriesAndUserLoggedAndCountAccessInNextApparitionToTrue_whenICallCheckCountAccessInNextApparition_thenIIncrementAccessCounterAndSetCountAccessInNextApparitionToFalse() {
        
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 0), withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        
        // given
        sut.categories = PromotionsGenerator.getCategoryBO()
        sut.countAccessInNextApparition = true
        dummySessionManager.isUserLogged = true
        
        // when
        let numberBeforeCall = PreferencesManager.sharedInstance().getPromotionsSectionAccessCounter()
        sut.checkCountAccessInNextApparition()
        let numberAfterCall = PreferencesManager.sharedInstance().getPromotionsSectionAccessCounter()
        
        // then
        XCTAssert(numberAfterCall == numberBeforeCall + 1)
        XCTAssert(sut.countAccessInNextApparition == false)
    }
    
    func test_givenCategoriesAndUserLoggedAndCountAccessInNextApparitionToFalse_whenICallCheckCountAccessInNextApparition_thenIDontIncrementAccessCounterCountAccessInNextApparitionIsStillFalse() {
        
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 0), withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        
        // given
        sut.categories = PromotionsGenerator.getCategoryBO()
        sut.countAccessInNextApparition = false
        dummySessionManager.isUserLogged = true
        
        // when
        let numberBeforeCall = PreferencesManager.sharedInstance().getPromotionsSectionAccessCounter()
        sut.checkCountAccessInNextApparition()
        let numberAfterCall = PreferencesManager.sharedInstance().getPromotionsSectionAccessCounter()
        
        // then
        XCTAssert(numberAfterCall == numberBeforeCall)
        XCTAssert(sut.countAccessInNextApparition == false)
    }
    
    // MARK: - favoriteCategoriesChanged

    func test_givenAny_whenICallFavoriteCategoriesChanged_thenICallViewShowSkeleton() {

        let dummyView = DummyView()
        sut.view = dummyView
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given

        // when

        sut.favoriteCategoriesChanged()

        // then
        XCTAssert(dummyView.isCalledShowSkeleton == true)
    }

    func test_givenAny_whenICallFavoriteCategoriesChanged_thenICallInteractorProvideCategories() {

        let dummyView = DummyView()
        sut.view = dummyView
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given

        // when
        sut.favoriteCategoriesChanged()

        // then
        XCTAssert(dummyInteractor.isCalledProvideCategories == true)
    }

    func test_givenAny_whenICallFavoriteCategoriesChangedAndCategoriesReturnsSuccess_thenICallProvideTrendingPromotions() {

        // given

        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()

        sut.view = dummyView
        sut.interactor = dummyInteractor

        // when
        sut.favoriteCategoriesChanged()

        // then
        XCTAssert(dummyInteractor.isCalledProvideTrendingPromotions == true)

    }

    func test_givenCardTitleIds_whenICallFavoriteCategoriesChangedAndCategoriesReturnsSuccess_thenICallProvideTrendingPromotionsWithAppropiateParams() {

        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()

        sut.view = dummyView
        sut.interactor = dummyInteractor

        // given
        sut.cardTitleIds = ["XA", "XD"]

        // when
        sut.favoriteCategoriesChanged()

        // then
        XCTAssert(dummyInteractor.paramsTrending == ShoppingParamsTransport(trendingId: TrendingType.hot.rawValue, titleIds: sut.cardTitleIds, orderBy: .endDate))
    }

    func test_givenAny_whenICallFavoriteCategoriesChangedAndReturnsSuccess_thenThePresenterRetriveCategories() {

        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()

        sut.view = dummyView
        sut.interactor = dummyInteractor

        // when
        sut.favoriteCategoriesChanged()
        // then
        XCTAssert(dummyInteractor.categoriesBO === sut.categories)

    }

    func test_givenAny_whenICallFavoriteCategoriesChangedAndReturnsError403_thenICallViewShowError() {

        // given

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()

        let errorBO = ErrorBO()
        errorBO.status = ConstantsHTTPCodes.STATUS_403
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        sut.interactor = dummyInteractor

        // when
        sut.favoriteCategoriesChanged()

        // then
        XCTAssert(dummyView.isCalledShowError == true)

    }

    func test_givenAny_whenICallFavoriteCategoriesChangedAndReturnsErrorDifferentThan403_thenICallViewShowEmptyTsec() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()

        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        sut.interactor = dummyInteractor

        // when
        sut.favoriteCategoriesChanged()

        // then
        XCTAssert(dummyView.isCalledShowEmptyTsec == true)

    }

    func test_givenAny_whenICallFavoriteCategoriesChangedAndReturnsError403_thenINotCallViewShowEmptyTsec() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()

        let errorBO = ErrorBO()
        errorBO.status = ConstantsHTTPCodes.STATUS_403
        errorBO.code = ErrorCode.expiredSession.rawValue
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        sut.interactor = dummyInteractor

        // when
        sut.favoriteCategoriesChanged()

        // then
        XCTAssert(dummyView.isCalledShowEmptyTsec == false)

    }

    func test_givenAny_whenICallFavoriteCategoriesChangedWithNoContent_thenINotCallViewShowNoContent() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceNoContent = true

        sut.interactor = dummyInteractor
        // when
        sut.favoriteCategoriesChanged()

        // then
        XCTAssert(dummyView.isCalledShowNoContent == true)

    }

    func test_givenAny_whenICallFavoriteCategoriesChanged_thenICallShowCategories() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // when
        sut.favoriteCategoriesChanged()

        // then
        XCTAssert(dummyView.isCalledShowCategories == true)

    }

    func test_givenAny_whenICallFavoriteCategoriesChangedWithThreeCategories_thenINotCallShowCategories() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceThreeCategories = true

        sut.interactor = dummyInteractor

        // when
        sut.favoriteCategoriesChanged()

        // then
        XCTAssert(dummyView.isCalledShowCategories == false)

    }

    func test_givenAny_whenICallRetryButtonAndCategoriesReturnSuccess_thenICallShowCategoriesWithCorrectValue() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()

        sut.interactor = dummyInteractor

        // when
        sut.favoriteCategoriesChanged()

        // then
        XCTAssert(dummyView.categoriesToComponent?.categories.count == 27)

        for i in 0 ..< dummyView.categoriesToComponent!.categories.count {

            XCTAssert(dummyView.categoriesToComponent!.categories[i].id == sut.categories!.categories[i + 3].id)
            XCTAssert(dummyView.categoriesToComponent!.categories[i].name == sut.categories!.categories[i + 3].name)

        }

    }

    func test_givenAny_whenICallFavoriteCategoriesChangedAndCategoriesReturnSuccess_thenICallProvidePromotionsWithCategory() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // when
        sut.favoriteCategoriesChanged()

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionByParams == true)

    }

    func test_givenAny_whenICallFavoriteCategoriesChanged_thenIResetDefaultAttributes() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given

        // when
        sut.favoriteCategoriesChanged()

        // then
        XCTAssert(dummyInteractor.wasPromotionsCalled == 0)
        XCTAssert(dummyInteractor.wasTrendingFinish == false)
        XCTAssert(dummyInteractor.wasPromotionsFinish == false)
        XCTAssert(dummyInteractor.wasErrorNoTsec == false)
        XCTAssert(dummyInteractor.wasPromotionsAll.isEmpty)
        XCTAssert(dummyInteractor.wasDisplayItems == DisplayItemsPromotions())

    }

    func test_givenAny_whenICallFavoriteCategoriesChanged_thenICallShowSkeleton() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given

        // when
        sut.favoriteCategoriesChanged()

        // then
        XCTAssert(dummyView.isCalledShowSkeleton == true)

    }

    // MARK: - favoriteCategoriesNotChanged

    func test_givenAny_whenICallFavoriteCategoriesNotChanged_thenICallViewScrollToTop() {

        // given

        // when
        sut.favoriteCategoriesNotChanged()

        // then
        XCTAssert(dummyView.isCalledScrollToTop == true)
    }
    
    // MARK: - tabPressed
    
    func test_givenCategoriesAndUserLoggedAndAccessCounterToZeroAndDontSuggestFavoriteSelectionToFalse_whenICallTabPressed_thenWaitToAccessToSuggestFavoriteSelectionIsSetToFour() {
        
        // given
        sut.categories = PromotionsGenerator.getCategoryBO()
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 0), withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 0), withKey: PreferencesManagerKeys.kWaitToAccessToSuggestFavoriteSelection)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: false as AnyObject, withKey: PreferencesManagerKeys.kDontSuggestFavoriteSelection)
        dummySessionManager.isUserLogged = true
        
        // when
        sut.tabPressed()
        
        // then
        let value = PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kWaitToAccessToSuggestFavoriteSelection) as? NSNumber
        XCTAssert(value == NSNumber(value: 4))
    }
    
    func test_givenAccessCounterToZeroAndUserLoggedAndDontSuggestFavoriteSelectionToTrue_whenICallTabPressed_thenWaitToAccessToSuggestFavoriteSelectionIsNotChanged() {
        
        // given
        let originalValue = NSNumber(value: 0)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 0), withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: originalValue, withKey: PreferencesManagerKeys.kWaitToAccessToSuggestFavoriteSelection)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: true as AnyObject, withKey: PreferencesManagerKeys.kDontSuggestFavoriteSelection)
        dummySessionManager.isUserLogged = true
        
        // when
        sut.tabPressed()
        
        // then
        let value = PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kWaitToAccessToSuggestFavoriteSelection) as? NSNumber
        XCTAssert(value == originalValue)
    }
    
    func test_givenCategoriesAndUserNotLogged_whenICallTabPressed_thenIDontIncrementAccessCounter() {
        
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 0), withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        
        // given
        sut.categories = PromotionsGenerator.getCategoryBO()
        dummySessionManager.isUserLogged = false
        
        // when
        let numberBeforeCall = PreferencesManager.sharedInstance().getPromotionsSectionAccessCounter()
        sut.tabPressed()
        let numberAfterCall = PreferencesManager.sharedInstance().getPromotionsSectionAccessCounter()
        
        // then
        XCTAssert(numberAfterCall == numberBeforeCall)
    }
    
    func test_givenCategoriesAndUserLogged_whenICallTabPressed_thenIIncrementAccessCounter() {
        
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 0), withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        
        // given
        sut.categories = PromotionsGenerator.getCategoryBO()
        dummySessionManager.isUserLogged = true
        
        // when
        let numberBeforeCall = PreferencesManager.sharedInstance().getPromotionsSectionAccessCounter()
        sut.tabPressed()
        let numberAfterCall = PreferencesManager.sharedInstance().getPromotionsSectionAccessCounter()
        
        // then
        XCTAssert(numberAfterCall == numberBeforeCall + 1)
    }
    
    func test_givenNoCategoriesAndUserLogged_whenICallTabPressed_thenIDontIncrementAccessCounter() {
        
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 0), withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        
        // given
        dummySessionManager.isUserLogged = true
        
        // when
        let numberBeforeCall = PreferencesManager.sharedInstance().getPromotionsSectionAccessCounter()
        sut.tabPressed()
        let numberAfterCall = PreferencesManager.sharedInstance().getPromotionsSectionAccessCounter()
        
        // then
        XCTAssert(numberAfterCall == numberBeforeCall)
    }
    
    func test_givenCategoriesAndUserLoggedAndToastIsVisible_whenICallTabPressed_thenIDontIncrementAccessCounter() {
        
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 0), withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        
        // given
        sut.categories = PromotionsGenerator.getCategoryBO()
        (ToastManager.shared().toastViewController as? DummyToastViewController)?.toastVisible = true
        dummySessionManager.isUserLogged = true
        
        // when
        let numberBeforeCall = PreferencesManager.sharedInstance().getPromotionsSectionAccessCounter()
        sut.tabPressed()
        let numberAfterCall = PreferencesManager.sharedInstance().getPromotionsSectionAccessCounter()
        
        // then
        XCTAssert(numberAfterCall == numberBeforeCall)
    }
    
    func test_givenCategoriesAndUserLoggedAndNoToastVisible_whenICallTabPressed_thenIIncrementAccessCounter() {
        
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 0), withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        
        // given
        sut.categories = PromotionsGenerator.getCategoryBO()
        (ToastManager.shared().toastViewController as? DummyToastViewController)?.toastVisible = false
        dummySessionManager.isUserLogged = true
        
        // when
        let numberBeforeCall = PreferencesManager.sharedInstance().getPromotionsSectionAccessCounter()
        sut.tabPressed()
        let numberAfterCall = PreferencesManager.sharedInstance().getPromotionsSectionAccessCounter()
        
        // then
        XCTAssert(numberAfterCall == numberBeforeCall + 1)
    }
    
    func test_givenNoCategoriesAndUserLoggedAndAccessCounterToOneAndDontSuggestFavoriteSelectionToFalse_whenICallTabPressedForSecondTime_thenIDontNavigateToSelectFavoriteCategories() {
        
        // given
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 1), withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: false as AnyObject, withKey: PreferencesManagerKeys.kDontSuggestFavoriteSelection)
        dummySessionManager.isUserLogged = true
        
        // when
        sut.tabPressed()
        
        // then
        XCTAssert(dummyBusManager.isCalledNavigateScreen == false)
        XCTAssert(dummyBusManager.isCalledNavigateSelectFavoriteCategories == false)
    }
    
    func test_givenCategoriesAndUserLoggedAndAccessCounterToOneAndDontSuggestFavoriteSelectionToTrue_whenICallTabPressedForSecondTime_thenIDontNavigateToSelectFavoriteCategories() {
        
        // given
        sut.categories = PromotionsGenerator.getCategoryBO()
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 1), withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: true as AnyObject, withKey: PreferencesManagerKeys.kDontSuggestFavoriteSelection)
        dummySessionManager.isUserLogged = true
        
        // when
        sut.tabPressed()
        
        // then
        XCTAssert(dummyBusManager.isCalledNavigateScreen == false)
        XCTAssert(dummyBusManager.isCalledNavigateSelectFavoriteCategories == false)
    }
    
    func test_givenCategoriesAndUserLoggedAndAccessCounterToOneAndDontSuggestFavoriteSelectionToFalseAndAToastVisible_whenICallTabPressedForSecondTime_thenIDontNavigateToSelectFavoriteCategories() {
        
        // given
        sut.categories = PromotionsGenerator.getCategoryBO()
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 1), withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: false as AnyObject, withKey: PreferencesManagerKeys.kDontSuggestFavoriteSelection)
        (ToastManager.shared().toastViewController as? DummyToastViewController)?.toastVisible = true
        dummySessionManager.isUserLogged = true
        
        // when
        sut.tabPressed()
        
        // then
        XCTAssert(dummyBusManager.isCalledNavigateScreen == false)
        XCTAssert(dummyBusManager.isCalledNavigateSelectFavoriteCategories == false)
    }
    
    func test_givenCategoriesAndUserLoggedAndAccessCounterToOneAndDontSuggestFavoriteSelectionToFalseAndNoToastVisiblewhenICallTabPressedForSecondTime_thenINavigateToSelectFavoriteCategories() {
        
        // given
        sut.categories = PromotionsGenerator.getCategoryBO()
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 1), withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: false as AnyObject, withKey: PreferencesManagerKeys.kDontSuggestFavoriteSelection)
        (ToastManager.shared().toastViewController as? DummyToastViewController)?.toastVisible = false
        dummySessionManager.isUserLogged = true
        
        // when
        sut.tabPressed()
        
        // then
        XCTAssert(dummyBusManager.isCalledNavigateScreen == true)
        XCTAssert(dummyBusManager.isCalledNavigateSelectFavoriteCategories == true)
    }
    
    func test_givenNoCategoriesAndUserLoggedAndAccessCounterToThreeAndWaitToAccessToSuggestFavoriteSelectionToFourAndDontSuggestFavoriteSelectionToFalse_whenICallTabPressed_thenIDontNavigateToSelectFavoriteCategories() {
        
        // given
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 3), withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 4), withKey: PreferencesManagerKeys.kWaitToAccessToSuggestFavoriteSelection)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: false as AnyObject, withKey: PreferencesManagerKeys.kDontSuggestFavoriteSelection)
        dummySessionManager.isUserLogged = true
        
        // when
        sut.tabPressed()
        
        // then
        XCTAssert(dummyBusManager.isCalledNavigateScreen == false)
        XCTAssert(dummyBusManager.isCalledNavigateSelectFavoriteCategories == false)
    }
    
    func test_givenCategoriesAndUserLoggedAndAccessCounterToThreeAndWaitToAccessToSuggestFavoriteSelectionToFourAndDontSuggestFavoriteSelectionToFalseAndToastVisible_whenICallTabPressed_thenIDontNavigateToSelectFavoriteCategories() {
        
        // given
        sut.categories = PromotionsGenerator.getCategoryBO()
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 3), withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 4), withKey: PreferencesManagerKeys.kWaitToAccessToSuggestFavoriteSelection)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: false as AnyObject, withKey: PreferencesManagerKeys.kDontSuggestFavoriteSelection)
        (ToastManager.shared().toastViewController as? DummyToastViewController)?.toastVisible = true
        dummySessionManager.isUserLogged = true
        
        // when
        sut.tabPressed()
        
        // then
        XCTAssert(dummyBusManager.isCalledNavigateScreen == false)
        XCTAssert(dummyBusManager.isCalledNavigateSelectFavoriteCategories == false)
    }
    
    func test_givenCategoriesAndUserLoggedAndAccessCounterToThreeAndWaitToAccessToSuggestFavoriteSelectionToFourAndDontSuggestFavoriteSelectionToFalseAndNoNoToastVisible_whenICallTabPressed_thenINavigateToSelectFavoriteCategories() {
        
        // given
        sut.categories = PromotionsGenerator.getCategoryBO()
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 3), withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 4), withKey: PreferencesManagerKeys.kWaitToAccessToSuggestFavoriteSelection)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: false as AnyObject, withKey: PreferencesManagerKeys.kDontSuggestFavoriteSelection)
        (ToastManager.shared().toastViewController as? DummyToastViewController)?.toastVisible = false
        dummySessionManager.isUserLogged = true
        
        // when
        sut.tabPressed()
        
        // then
        XCTAssert(dummyBusManager.isCalledNavigateScreen == true)
        XCTAssert(dummyBusManager.isCalledNavigateSelectFavoriteCategories == true)
    }
    
    func test_givenCategoriesAndUserLoggedAndAccessCounterToThreeAndWaitToAccessToSuggestFavoriteSelectionToFourAndDontSuggestFavoriteSelectionToTrue_whenICallTabPressed_thenIDontNavigateToSelectFavoriteCategories() {
        
        // given
        sut.categories = PromotionsGenerator.getCategoryBO()
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 3), withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 4), withKey: PreferencesManagerKeys.kWaitToAccessToSuggestFavoriteSelection)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: true as AnyObject, withKey: PreferencesManagerKeys.kDontSuggestFavoriteSelection)
        dummySessionManager.isUserLogged = true
        
        // when
        sut.tabPressed()
        
        // then
        XCTAssert(dummyBusManager.isCalledNavigateScreen == false)
        XCTAssert(dummyBusManager.isCalledNavigateSelectFavoriteCategories == false)
    }
    
    func test_givenNoCategoriesAndUserLoggedAndAccessCounterToFourAndWaitToAccessToSuggestFavoriteSelectionToFiveAndDontSuggestFavoriteSelectionToFalse_whenICallTabPressed_thenIDontNavigateToSelectFavoriteCategories() {
        
        // given
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 4), withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 5), withKey: PreferencesManagerKeys.kWaitToAccessToSuggestFavoriteSelection)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: false as AnyObject, withKey: PreferencesManagerKeys.kDontSuggestFavoriteSelection)
        dummySessionManager.isUserLogged = true
        
        // when
        sut.tabPressed()
        
        // then
        XCTAssert(dummyBusManager.isCalledNavigateScreen == false)
        XCTAssert(dummyBusManager.isCalledNavigateSelectFavoriteCategories == false)
    }
    
    func test_givenCategoriesAndUserLoggedAndAccessCounterToFourAndWaitToAccessToSuggestFavoriteSelectionToFiveAndDontSuggestFavoriteSelectionToFalseAndToastVisible_whenICallTabPressed_thenIDontNavigateToSelectFavoriteCategories() {
        
        // given
        sut.categories = PromotionsGenerator.getCategoryBO()
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 4), withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 5), withKey: PreferencesManagerKeys.kWaitToAccessToSuggestFavoriteSelection)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: false as AnyObject, withKey: PreferencesManagerKeys.kDontSuggestFavoriteSelection)
        (ToastManager.shared().toastViewController as? DummyToastViewController)?.toastVisible = true
        dummySessionManager.isUserLogged = true
        
        // when
        sut.tabPressed()
        
        // then
        XCTAssert(dummyBusManager.isCalledNavigateScreen == false)
        XCTAssert(dummyBusManager.isCalledNavigateSelectFavoriteCategories == false)
    }
    
    func test_givenCategoriesAndUserLoggedAndAccessCounterToFourAndWaitToAccessToSuggestFavoriteSelectionToFiveAndDontSuggestFavoriteSelectionToFalseAndNoToastVisible_whenICallTabPressed_thenINavigateToSelectFavoriteCategories() {
        
        // given
        sut.categories = PromotionsGenerator.getCategoryBO()
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 4), withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 5), withKey: PreferencesManagerKeys.kWaitToAccessToSuggestFavoriteSelection)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: false as AnyObject, withKey: PreferencesManagerKeys.kDontSuggestFavoriteSelection)
        (ToastManager.shared().toastViewController as? DummyToastViewController)?.toastVisible = false
        dummySessionManager.isUserLogged = true
        
        // when
        sut.tabPressed()
        
        // then
        XCTAssert(dummyBusManager.isCalledNavigateScreen == true)
        XCTAssert(dummyBusManager.isCalledNavigateSelectFavoriteCategories == true)
    }
    
    func test_givenCategoriesAndUserLoggedAndAccessCounterToFourAndWaitToAccessToSuggestFavoriteSelectionToFiveAndDontSuggestFavoriteSelectionToTrue_whenICallTabPressed_thenIDontNavigateToSelectFavoriteCategories() {
        
        // given
        sut.categories = PromotionsGenerator.getCategoryBO()
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 4), withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 5), withKey: PreferencesManagerKeys.kWaitToAccessToSuggestFavoriteSelection)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: true as AnyObject, withKey: PreferencesManagerKeys.kDontSuggestFavoriteSelection)
        dummySessionManager.isUserLogged = true
        
        // when
        sut.tabPressed()
        
        // then
        XCTAssert(dummyBusManager.isCalledNavigateScreen == false)
        XCTAssert(dummyBusManager.isCalledNavigateSelectFavoriteCategories == false)
    }

    // MARK: - Dummy data

    class DummyView: BaseViewController, ShoppingViewProtocolB, ViewProtocol {

        var isCalledShowTransactionsWithDisplayData = false
        var isCalledShowError = false
        var isCalledShowSuccessToast = false
        var isCalledShowEmptyTsec = false
        var isCalledShowNoContent = false
        var isCalledShowSkeleton = false
        var isCalledHideSkeleton = false
        var isCalledShowPromotions = false
        var isCalledShowCategories = false
        var showInfoDataIsCalled = false
        var isCalledShowFilterButton = false
        var isCalledShowToastNotGPS = false
        var isCalledShowToastNotPermission = false
        var isCalledShowToastUpdatedPromotions = false
        var isCalledHideToast = false
        var isCalledOpenAppSettings = false
        var isCalledScrollToTop = true
        
        var diplayItems: DisplayItemsPromotions?
        var transactionsDisplayData: TransactionDisplayData?
        var categoriesToComponent: CategoriesBO?

        func showInfoToastNotGPS() {
            isCalledShowToastNotGPS = true
        }

        func showInfoToastNotPermissions() {
            isCalledShowToastNotPermission = true
        }

        func showToastUpdatedPromotions() {
            isCalledShowToastUpdatedPromotions = true
        }

        func showError(error: ModelBO) {
            isCalledShowError = true
        }

        func changeColorEditTexts() {
        }

        func showErrorEmptyTsec() {
            isCalledShowEmptyTsec = true
        }

        func showNoContentView() {
            isCalledShowNoContent = true
        }

        func showSkeleton() {
            isCalledShowSkeleton = true
        }

        func hideSkeleton() {
            isCalledHideSkeleton = true
        }

        func showPromotions(withDisplayItemsPromotions displayItemPromotions: DisplayItemsPromotions) {
            isCalledShowPromotions = true
            diplayItems = displayItemPromotions
        }

        func showCategories(withDisplayModel displayModel: PromotionsCategoryDisplayModel) {
            isCalledShowCategories = true
            categoriesToComponent = displayModel.categories
        }

        func showFilterButton() {
            isCalledShowFilterButton = true
        }

        func hideToast() {
            isCalledHideToast = true
        }

        func openAppSettings() {
            isCalledOpenAppSettings = true
        }

        func scrollToTop() {
            isCalledScrollToTop = true
        }
        
        func isVisibleOnTop() -> Bool {
            return true
        }
        
        func trackScreen() {
            
        }
        
        func trackErrorScreen(withError error: String) {
            
        }
    }

    class DummyBusManager: BusManager {

        var isCalledNavigateScreen = false
        var isCalledNavigateSelectFavoriteCategories = false

        var dummyCategoriesTransport: FavoriteCategoriesTransport!

        override init() {

            super.init()
            routerFactory = RouterInitFactory()
        }

        override func navigateScreen<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {

            isCalledNavigateScreen = true

            if tag == ShoppingPageReactionB.ROUTER_TAG && event === ShoppingPageReactionB.EVENT_NAV_TO_FAVORITE_CATEGORIES {

                isCalledNavigateSelectFavoriteCategories = true
                dummyCategoriesTransport = (values as! FavoriteCategoriesTransport)
            }
        }
    }

    class DummyInteractor: ShoppingInteractorProtocol {

        var forceError = false

        var forceErrorForPromotions = false

        var wasLoadingNextPage = true

        var presenter: ShoppingPresenter<DummyView>?

        var errorBO: ErrorBO?

        var isCalledProvideCategories = false

        var isCalledProvidePromotionByParams = false

        var isCalledProvideTrendingPromotions = false
        var paramsTrending: ShoppingParamsTransport?

        var forceNoContent = false

        var categoriesBO: CategoriesBO!
        var categoriesToReturn: CategoriesBO?

        var promotionsBO: PromotionsBO!

        var promotionsTrendingBO: PromotionsBO!

        var forceThreeCategories = false

        var categoriesParams = [ShoppingParamsTransport]()

        var wasPromotionsCalled = 0
        var wasTrendingFinish = false
        var wasPromotionsFinish = false
        var wasErrorNoTsec = false
        var wasPromotionsAll = [String: PromotionsBO]()
        var wasDisplayItems = DisplayItemsPromotions()

        func provideCategories() -> Observable<ModelBO> {

            isCalledProvideCategories = true

            if let presenter = presenter {

                wasPromotionsCalled = presenter.promotionsCalled
                wasTrendingFinish = presenter.trendingFinish
                wasPromotionsFinish = presenter.promotionsFinish
                wasErrorNoTsec = presenter.errorNoTsec
                wasPromotionsAll = presenter.promotionsAll
                wasDisplayItems = presenter.displayItems
            }

            if let categoriesToReturn = categoriesToReturn {
                categoriesBO = categoriesToReturn
            } else {
                if forceThreeCategories {
                    categoriesBO = PromotionsGenerator.getThreeCategoryBO()
                } else {
                    categoriesBO = PromotionsGenerator.getCategoryBO()
                }
            }

            if forceNoContent {
                categoriesBO.status = 204
            }

            if !forceError {

                return Observable <ModelBO>.just(categoriesBO)

            } else {

                if errorBO == nil {
                    errorBO = ErrorBO(error: ErrorEntity(message: "Sesión expirada"))
                    errorBO!.isErrorShown = false
                }

                return Observable.error(ServiceError.GenericErrorBO(error: errorBO!))

            }
        }

        func providePromotions(byParams params: ShoppingParamsTransport) -> Observable <ModelBO> {

            if !isCalledProvideTrendingPromotions {
                isCalledProvideTrendingPromotions = (params.trendingId == TrendingType.hot.rawValue)
                paramsTrending = params
            }

            if params.categoryId != nil {
                isCalledProvidePromotionByParams = true
                categoriesParams.append(params)
            }

            return Observable <ModelBO>.just(PromotionsGenerator.getPromotionsBO())

        }
    }

    class DummyLocationManager: LocationManager {

        var isCalledGetLocation = false

        var completionHandlerSuccess: Success?
        var completionHandlerFailure: Failure?

        var locationErrorType: LocationErrorType?
        var location: CLLocation?

        var isCalledStartLocationObserverAndCheckStatus = false
        var isCalledStopLocationObserver = false

        override func getLocation(onSuccess success: @escaping LocationManager.Success, onFail fail: @escaping LocationManager.Failure) {

            isCalledGetLocation = true

            completionHandlerSuccess = success
            completionHandlerFailure = fail

            if let completionFailure = completionHandlerFailure, let locationErrorType = locationErrorType {

                completionFailure(locationErrorType)

            } else if let completionSuccess = completionHandlerSuccess, let location = location {

                completionSuccess(location)
            }

            super.getLocation(onSuccess: success, onFail: fail)
        }

        override func startLocationObserverAndCheckStatus() {

            isCalledStartLocationObserverAndCheckStatus = true

            super.startLocationObserverAndCheckStatus()
        }

        override func stopLocationObserver() {

            isCalledStopLocationObserver = true

            super.stopLocationObserver()
        }
    }

}
