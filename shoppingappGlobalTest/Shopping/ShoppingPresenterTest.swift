//
//  BVATransactionsListPresenterTest.swift
//  shoppingapp
//
//  Created by jesus.martinez on 7/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import XCTest
import RxSwift
import CellsNativeComponents

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class ShoppingPresenterTest: XCTestCase {

    var sut: ShoppingPresenter<DummyView>!
    var dummyBusManager: DummyBusManager!
    var dummyErrorTrackData = DummyErrorTrackData()

    override func setUp() {
        super.setUp()
        dummyBusManager = DummyBusManager()
        sut = ShoppingPresenter<DummyView>(busManager: dummyBusManager)
        
        dummyErrorTrackData = DummyErrorTrackData()
        sut.errorsTrackData = dummyErrorTrackData

    }
    
    // MARK: - viewWillAppear
    
    func test_givenErrorsTrackDataEmpty_whenICallViewWillAppear_thenICallViewTrackScreen() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        dummyErrorTrackData.forceIsEmptyValue = true

        // when
        sut.viewWillAppear()
        
        // then
        XCTAssert(dummyView.isCalledTrackScreen == true)
    }
    
    func test_givenErrorsTrackDataNotEmpty_whenICallViewWillAppear_thenICallViewTrackErrorScreen() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        dummyErrorTrackData.forceIsEmptyValue = false
        
        // when
        sut.viewWillAppear()
        
        // then
        XCTAssert(dummyView.isCalledTrackErrorScreen == true)
    }
    
    func test_givenErrorsTrackDataNotEmpty_whenICallViewWillAppear_thenICallViewTrackErrorScreenWithCorrectValues() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        dummyErrorTrackData.forceIsEmptyValue = false
        dummyErrorTrackData.append(errorCode: 33, errorName: "Error")
        dummyErrorTrackData.append(errorCode: 334, errorName: "Error_2")
        
        // when
        sut.viewWillAppear()
        
        // then
        XCTAssert(dummyView.trackErrorSent == dummyErrorTrackData.format())
    }

    // MARK: - viewLoaded

    func test_givenAny_whenICallViewLoaded_thenIResetDefaultAttributes() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.wasPromotionsCalled == 0)
        XCTAssert(dummyInteractor.wasTrendingFinish == false)
        XCTAssert(dummyInteractor.wasPromotionsFinish == false)
        XCTAssert(dummyInteractor.wasErrorNoTsec == false)
        XCTAssert(dummyInteractor.wasPromotionsAll.isEmpty)
        XCTAssert(dummyInteractor.wasDisplayItems == DisplayItemsPromotions())

    }

    func test_givenAPresenter_whenViewLoad_thenICallShowSkeleton() {

        // given
        let dummyView = DummyView()

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // when
        sut.view = dummyView

        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowSkeleton == true)

    }

    func test_givenAPresenter_whenviewLoadedWithError403_thenICallHideSkeleton() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()

        let errorBO = ErrorBO()
        errorBO.status = ConstantsHTTPCodes.STATUS_403
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        sut.interactor = dummyInteractor

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledHideSkeleton == true)

    }

    func test_givenAPresenter_whenviewLoadedWithNoContent_thenICallHideSkeleton() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceNoContent = true
        sut.interactor = dummyInteractor

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledHideSkeleton == true)

    }

    func test_givenAPresenter_whenICallviewLoaded_thenICallTheInteractor() {

        // given

        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()

        sut.view = dummyView
        sut.interactor = dummyInteractor

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.isCalledProvideCategories == true)

    }

    func test_givenAPresenter_whenICallviewLoaded_thenICallProvideTrendingPromotions() {

        // given

        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()

        sut.view = dummyView
        sut.interactor = dummyInteractor

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.isCalledProvideTrendingPromotions == true)

    }

    func test_givenAPresenter_whenICallviewLoaded_thenICallProvideTrendingPromotionsWithAppropiateParams() {

        // given

        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()

        sut.view = dummyView
        sut.interactor = dummyInteractor

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.paramsTrending == ShoppingParamsTransport(trendingId: TrendingType.hot.rawValue, orderBy: .endDate))

    }

    func test_givenAPresenter_whenICallviewLoaded_thenThePresenterRetriveCategories() {

        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()

        sut.view = dummyView
        sut.interactor = dummyInteractor

        // when
        sut.viewLoaded()
        // then
        XCTAssert(dummyInteractor.categoriesBO === sut.categories)

    }

    func test_givenAnError403AndErrorNotShown_whenICallProvideCategoriesPresenterError_thenICallViewShowError() {

        // given

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()

        let errorBO = ErrorBO()
        errorBO.status = ConstantsHTTPCodes.STATUS_403
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        sut.interactor = dummyInteractor

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowError == true)

    }

    func test_givenAnNotError403AndErrorNotShown_whenICallProvideCategoriesPresenterError_thenICallViewShowEmptyTsec() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()

        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        sut.interactor = dummyInteractor

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowEmptyTsec == true)

    }

    func test_givenAnError403AndErrorNotShown_whenICallProvideCategoriesPresenterError_thenINotCallViewShowEmptyTsec() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()

        let errorBO = ErrorBO()
        errorBO.status = ConstantsHTTPCodes.STATUS_403
        errorBO.code = ErrorCode.expiredSession.rawValue
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        sut.interactor = dummyInteractor

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowEmptyTsec == false)

    }

    func test_givenCategories_whenICallProvideCategoriesWithNoContent_thenINotCallViewShowNoContent() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceNoContent = true

        sut.interactor = dummyInteractor
        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowNoContent == true)

    }

    func test_givenCategories_whenICallProvideCategories_thenICallViewShowCategories() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowCategories == true)

    }

    func test_givenCategories_whenICallProvideCategories_thenICall() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowCategories == true)

    }

    func test_givenCategories_whenICallProvideCategoriesWithThreeCategories_thenIDoNotCallViewShowCategories() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceThreeCategories = true

        sut.interactor = dummyInteractor

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowCategories == false)

    }

    func test_givenCategories_whenICallProvideCategoriesWithCategories_thenICallViewShowCategoriesWithCorrectValue() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()

        sut.interactor = dummyInteractor

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.displayModelToComponent!.categories.categories.count == 27)

        for i in 0 ..< dummyView.displayModelToComponent!.categories.categories.count {

            XCTAssert(dummyView.displayModelToComponent!.categories.categories[i].id == sut.categories!.categories[i + 3].id)
            XCTAssert(dummyView.displayModelToComponent!.categories.categories[i].name == sut.categories!.categories[i + 3].name)

        }

    }

    func test_givenCategoriesAndAllowConfigureFavoriteCategoriesFalse_whenICallProvideCategoriesWithCategories_thenICallViewShowCategoriesWithShouldShowSelectFavoriteCategoriesFalse() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.allowConfigureFavoriteCategories = false

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.displayModelToComponent!.shouldShowSelectFavoriteCategories == false)
    }

    func test_givenCategoriesAndAllowConfigureFavoriteCategoriesTrueAndIsUserLoggedFalse_whenICallProvideCategoriesWithCategories_thenICallViewShowCategoriesWithShouldShowSelectFavoriteCategoriesFalse() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.allowConfigureFavoriteCategories = true
        sut.isUserLogged = false
        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.displayModelToComponent!.shouldShowSelectFavoriteCategories == false)
    }

    func test_givenCategoriesAndAllowConfigureFavoriteCategoriesTrueAndIsUserLoggedTrue_whenICallProvideCategoriesWithCategories_thenICallViewShowCategoriesWithShouldShowSelectFavoriteCategoriesTrue() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.allowConfigureFavoriteCategories = true
        sut.isUserLogged = true

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.displayModelToComponent!.shouldShowSelectFavoriteCategories == true)
    }

    func test_givenCategories_whenICallViewLoaded_thenICallProvidePromotionsWithCategory() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionByParams == true)

    }
    
    func test_givenAny_whenICallViewLoaded_thenICallErrorsTrackDataReset() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        
        // when
        sut.viewLoaded()
        
        // then
        XCTAssert(dummyErrorTrackData.isCalledReset == true)

    }
    
    func test_givenAny_whenICallViewLoadedAndInteractorReturnsAndError_thenICallErrorsTrackDataAppend() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true
        
        // when
        sut.viewLoaded()
        
        // then
        XCTAssert(dummyErrorTrackData.isCalledAppend == true)
        
    }
    
    func test_givenAny_whenICallViewLoadedAndInteractorReturnsAndError_thenICallErrorsTrackDataAppendCorrectValues() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true
        
        // when
        sut.viewLoaded()
        
        // then
        XCTAssert(dummyErrorTrackData.errorCodeReceived == errorBO.status)
        XCTAssert(dummyErrorTrackData.errorNameReceived == Promotions.categories)
        
    }
    
    func test_givenAny_whenICallViewLoadedAndInteractorReturnsAndErrorDifferentThanSessionExpiredAndErrorsTrackDataNotEmpty_thenICallViewTrackErrorScreen() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true
        dummyErrorTrackData.forceIsEmptyValue = false
        
        // when
        sut.viewLoaded()
        
        // then
        XCTAssert(dummyView.isCalledTrackErrorScreen == true)
        
    }
    
    func test_givenAny_whenICallViewLoadedAndInteractorReturnsAndErrorDifferentThanSessionExpiredAndErrorsTrackDataNotEmpty_thenICallViewTrackErrorScreenWithCorrectValue() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true
        dummyErrorTrackData.forceIsEmptyValue = false
        
        // when
        sut.viewLoaded()
        
        // then
        XCTAssert(dummyView.trackErrorSent == dummyErrorTrackData.format())
        
    }
    
    func test_givenAnNotError403AndErrorNotShown_whenICallViewLoaded_thenICallErrorTrackDataAppend() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceErrorForPromotions = true
        
        // when
        sut.viewLoaded()
        
        // then
        XCTAssert(dummyErrorTrackData.isCalledAppend == true)

    }
    
    func test_givenAnNotError403AndErrorNotShown_whenICallViewLoaded_thenICallErrorTrackDataAppendWithCorrectValues() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceErrorForPromotions = true
        
        // when
        sut.viewLoaded()
        
        // then
        XCTAssert(dummyErrorTrackData.errorCodeReceived == errorBO.status)
        XCTAssert(dummyErrorTrackData.errorNameReceived == Promotions.promotionByCategory)
    }
    
    // MARK: DisplayItemPromotions

    func test_givenSutWithPromotions_whenICallGenerateDisplayItems_thenIHaveItems() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let promotionsBO = PromotionsGenerator.getPromotionsBO()
        sut.promotionsAll["ACTIVITIES"] = promotionsBO

        sut.categories = PromotionsGenerator.getCategoryBO()

        // when
        sut.createDisplayDataItems(withCategory: CategoryType.activities, withCategoryString: "ACTIVITIES")

        // then
        XCTAssert(sut.displayItems.items?.count == 3)

    }

    func test_givenSutWithPromotions_whenICallGenerateDisplayItems_thenTheSameValues() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let promotionsBO = PromotionsGenerator.getPromotionsBO()
        sut.promotionsAll["ACTIVITIES"] = promotionsBO

        sut.categories = PromotionsGenerator.getCategoryBO()

        // when
        sut.createDisplayDataItems(withCategory: CategoryType.activities, withCategoryString: "ACTIVITIES")

        // then
        XCTAssertTrue(sut.displayItems.items?[0].typeCell == CellPromotionType.header)
        XCTAssertTrue(sut.displayItems.items?[0].categoryString  == "ACTIVITIES")

        XCTAssert(sut.displayItems.items?[1].commerceName == "Repsol".uppercased() )
        XCTAssert(sut.displayItems.items![1].benefit == "Descuentos" )
        XCTAssert(sut.displayItems.items?[1].descriptionPromotion == "Save 5% when filling up your car by paying with your BBVA credit card in Repsol stations." )
        XCTAssertTrue(sut.displayItems.items?[1].dateDiff != nil )
        XCTAssertTrue(sut.displayItems.items?[1].typeCell == CellPromotionType.high)
        XCTAssertTrue(sut.displayItems.items?[1].categoryType == CategoryType.activities)

        XCTAssert(sut.displayItems.items?[2].commerceName == "Ikea".uppercased() )
        XCTAssert(sut.displayItems.items?[2].benefit == "" )
        XCTAssert(sut.displayItems.items?[2].descriptionPromotion == "From 20€ discount in your next purchase in Ikea." )
        XCTAssertTrue(sut.displayItems.items?[2].dateDiff != nil )
        XCTAssertTrue(sut.displayItems.items?[2].typeCell == CellPromotionType.small)
        XCTAssertTrue(sut.displayItems.items?[2].categoryType == CategoryType.activities)

    }

    func test_givenSutWithPromotionsBigger_whenICallGenerateDisplayItems_thenIHave5Items() {

        let dummyView = DummyView()
        sut.view = dummyView

        let promotionsBO = PromotionsGenerator.getPromotionsBOBigger()
        sut.promotionsAll["ACTIVITIES"] = promotionsBO

        sut.categories = PromotionsGenerator.getCategoryBO()

        // when
        sut.createDisplayDataItems(withCategory: CategoryType.activities, withCategoryString: "ACTIVITIES")

        // then
        XCTAssert(sut.displayItems.items?.count == 5)

    }

    func test_givenCategories_whenICallProvideCategories_thenICallShowPromotions() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // when
        sut.viewLoaded()

        // then
        XCTAssert(sut.view?.isCalledShowPromotions == true)

    }

    func test_givenCategories_whenICallProvideCategoriesAndGetPromotions_thenICallHideSkeleton() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // when
        sut.viewLoaded()

        // then
        XCTAssert(sut.view?.isCalledHideSkeleton == true)

    }

    func test_givenAnNotError403AndErrorNotShown_whenICallProvidePromotionsPresenterError_thenErrorNoTsecTrue() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceErrorForPromotions = true

        // when
        sut.viewLoaded()

        // then
        XCTAssert(sut.errorNoTsec == true)

    }

    // MARK: - getPromotionTrending

    func test_givenCategories_whenICallProvideTrendingCategories_thenIReceivePromotions() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // when
        sut.getPromotionTrending()

        // then
        XCTAssert(sut.promotionsTrending != nil)

    }

    func test_givenCategories_whenICallProvideTrendingCategories_thenHaveDisplayItemWithValuesCorrect() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // when
        sut.getPromotionTrending()

        let displayItemPromotion = sut.displayItems.items![0]

        // then
        XCTAssert(displayItemPromotion.typeCell == .trending)
        XCTAssert(displayItemPromotion.itemsTrending.count == 2)

    }

    func test_givenCategories_whenICallProvideTrendingCategories_thenHaveBoolValueFinish() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // when
        sut.getPromotionTrending()

        // then
        XCTAssert(sut.trendingFinish == true)

    }

    func test_givenErrorBO_whenICallGetPromotionTrendingAndInteractorReturnOtherError_thenErrorBOShouldHaveTheSameStatusErrorThanReturned() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceErrorForPromotions = true
        let errorBO = ErrorBO()
        errorBO.status = 403
        dummyInteractor.errorBO = errorBO
        sut.interactor = dummyInteractor

        let errorBOBefore = ErrorBO()
        errorBOBefore.status = 500
        sut.errorBO = errorBOBefore

        // when
        sut.getPromotionTrending()

        // then
        XCTAssert(sut.errorBO?.status == errorBO.status)

    }
    
    func test_givenErrorBO_whenICallGetPromotionTrendingAndInteractorReturnOtherError_thenICallErrorsTrackDataAppend() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceErrorForPromotions = true
        let errorBO = ErrorBO()
        errorBO.status = 403
        dummyInteractor.errorBO = errorBO
        sut.interactor = dummyInteractor
        
        // given
        let errorBOBefore = ErrorBO()
        errorBOBefore.status = 500
        sut.errorBO = errorBOBefore
        
        // when
        sut.getPromotionTrending()
        
        // then
        XCTAssert(dummyErrorTrackData.isCalledAppend == true)
        
    }
    
    func test_givenErrorBO_whenICallGetPromotionTrendingAndInteractorReturnOtherError_thenICallErrorsTrackDataAppendCorrectValues() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceErrorForPromotions = true
        let errorBO = ErrorBO()
        errorBO.status = 403
        dummyInteractor.errorBO = errorBO
        sut.interactor = dummyInteractor
        
        // given
        let errorBOBefore = ErrorBO()
        errorBOBefore.status = 500
        sut.errorBO = errorBOBefore
        
        // when
        sut.getPromotionTrending()
        
        // then
        XCTAssert(dummyErrorTrackData.errorCodeReceived == errorBO.status)
        XCTAssert(dummyErrorTrackData.errorNameReceived == Promotions.trending)
        
    }

    // MARK: - checkFinishService

    func test_givenTrendingFinishFalseAndPromotionsFinishFalse_whenICallCheckFinishService_thenIDoNotUpdateView() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.trendingFinish = false
        sut.promotionsFinish = false

        // when
        sut.checkFinishService()

        // then
        XCTAssert(dummyView.isCalledShowPromotions == false)
        XCTAssert(dummyView.isCalledShowFilterButton == false)
        XCTAssert(dummyView.isCalledHideSkeleton == false)
        XCTAssert(dummyView.isCalledShowNoContent == false)
        XCTAssert(dummyView.isCalledShowEmptyTsec == false)

    }

    func test_givenTrendingFinishTrueAndPromotionsFinishFalse_whenICallCheckFinishService_thenIDoNotUpdateView() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.trendingFinish = true
        sut.promotionsFinish = false

        // when
        sut.checkFinishService()

        // then
        XCTAssert(dummyView.isCalledShowPromotions == false)
        XCTAssert(dummyView.isCalledShowFilterButton == false)
        XCTAssert(dummyView.isCalledHideSkeleton == false)
        XCTAssert(dummyView.isCalledShowNoContent == false)
        XCTAssert(dummyView.isCalledShowEmptyTsec == false)

    }

    func test_givenTrendingFinishFalseAndPromotionsFinishTrue_whenICallCheckFinishService_thenIDoNotUpdateView() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.trendingFinish = false
        sut.promotionsFinish = true

        // when
        sut.checkFinishService()

        // then
        XCTAssert(dummyView.isCalledShowPromotions == false)
        XCTAssert(dummyView.isCalledShowFilterButton == false)
        XCTAssert(dummyView.isCalledHideSkeleton == false)
        XCTAssert(dummyView.isCalledShowNoContent == false)
        XCTAssert(dummyView.isCalledShowEmptyTsec == false)

    }

    func test_givenTrendingFinishTrueAndPromotionsFinishTrueAndErrorNoTsecFalseAndDisplayItemsEmpty_whenICallCheckFinishService_thenICallViewShowNoContent() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.promotionsFinish = true
        sut.trendingFinish = true
        sut.errorNoTsec = false
        sut.promotionsTrending = nil
        sut.promotionsAll = [String: PromotionsBO]()

        // when
        sut.checkFinishService()

        // then
        XCTAssert(dummyView.isCalledShowPromotions == false)
        XCTAssert(dummyView.isCalledShowFilterButton == false)
        XCTAssert(dummyView.isCalledHideSkeleton == false)
        XCTAssert(dummyView.isCalledShowNoContent == true)
        XCTAssert(dummyView.isCalledShowEmptyTsec == false)

    }

    func test_givenTrendingFinishTrueAndPromotionsFinishTrueAndErrorNoTsecTrueAndDisplayItemsEmpty_whenICallCheckFinishService_thenICallViewShowNoContent() {

        let dummyView = DummyView()
        sut.view = dummyView

        sut.promotionsFinish = true
        sut.trendingFinish = true
        sut.errorNoTsec = true
        sut.promotionsTrending = nil
        sut.promotionsAll = [String: PromotionsBO]()

        // when
        sut.checkFinishService()

        // then
        XCTAssert(dummyView.isCalledShowPromotions == false)
        XCTAssert(dummyView.isCalledShowFilterButton == false)
        XCTAssert(dummyView.isCalledHideSkeleton == false)
        XCTAssert(dummyView.isCalledShowNoContent == false)
        XCTAssert(dummyView.isCalledShowEmptyTsec == true)

    }

    func test_givenTrendingFinishTrueAndPromotionsFinishTrueAndDisplayItemsWithOnlyPromotionsTrending_whenICallCheckFinishService_thenICallViewShowPromotionsShowFilterButtonAndHideSkeleton() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.promotionsFinish = true
        sut.trendingFinish = true

        let promotionTrending = PromotionsGenerator.getPromotionsBO()
        let displayPromotionsTrending = DisplayItemPromotion.generateDisplayItemPromotionTrending(withTrendingPromotions: promotionTrending)
        sut.displayItems.items.append(displayPromotionsTrending)

        // when
        sut.checkFinishService()

        // then
        XCTAssert(dummyView.isCalledShowPromotions == true)
        XCTAssert(dummyView.isCalledShowFilterButton == true)
        XCTAssert(dummyView.isCalledHideSkeleton == true)
        XCTAssert(dummyView.isCalledShowNoContent == false)
        XCTAssert(dummyView.isCalledShowEmptyTsec == false)

    }

    func test_givenTrendingFinishTrueAndPromotionsFinishTrueAndDisplayItemsWithNoPromotionsTrendingByAtLeastAPromotionOfAnyCategory_whenICallCheckFinishService_thenICallViewShowPromotionsShowFilterButtonAndHideSkeleton() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.promotionsFinish = true
        sut.trendingFinish = true

        let promotions = PromotionsGenerator.getPromotionsBO()
        let displayPromotionsActivity = sut.createDisplayitemWithPromotion(withPromotion: promotions.promotions.first!, withNumber: 0, withCategoryType: .activities)
        sut.displayItems.items.append(displayPromotionsActivity)

        // when
        sut.checkFinishService()

        // then
        XCTAssert(dummyView.isCalledShowPromotions == true)
        XCTAssert(dummyView.isCalledShowFilterButton == true)
        XCTAssert(dummyView.isCalledHideSkeleton == true)
        XCTAssert(dummyView.isCalledShowNoContent == false)
        XCTAssert(dummyView.isCalledShowEmptyTsec == false)

    }

    func test_givenTrendingFinishTrueAndPromotionsFinishTrueAndDisplayItemsWithPromotionsTrendingAndFromCategories_whenICallCheckFinishService_thenICallViewShowPromotionsShowFilterButtonAndHideSkeleton() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.promotionsFinish = true
        sut.trendingFinish = true

        let promotions = PromotionsGenerator.getPromotionsBO()
        let displayPromotionsTrending = DisplayItemPromotion.generateDisplayItemPromotionTrending(withTrendingPromotions: promotions)
        sut.displayItems.items.append(displayPromotionsTrending)

        let displayPromotionsActivity = sut.createDisplayitemWithPromotion(withPromotion: promotions.promotions.first!, withNumber: 0, withCategoryType: .activities)
        let displayPromotionsActivity2 = sut.createDisplayitemWithPromotion(withPromotion: promotions.promotions[1], withNumber: 1, withCategoryType: .activities)

        sut.displayItems.items.append(displayPromotionsActivity)
        sut.displayItems.items.append(displayPromotionsActivity2)

        let displayPromotionsAuto = sut.createDisplayitemWithPromotion(withPromotion: promotions.promotions.first!, withNumber: 0, withCategoryType: .auto)
        sut.displayItems.items.append(displayPromotionsAuto)

        let displayPromotionsFlowerShop = sut.createDisplayitemWithPromotion(withPromotion: promotions.promotions.first!, withNumber: 0, withCategoryType: .flowerShop)
        sut.displayItems.items.append(displayPromotionsFlowerShop)

        // when
        sut.checkFinishService()

        // then
        XCTAssert(dummyView.isCalledShowPromotions == true)
        XCTAssert(dummyView.isCalledShowFilterButton == true)
        XCTAssert(dummyView.isCalledHideSkeleton == true)
        XCTAssert(dummyView.isCalledShowNoContent == false)
        XCTAssert(dummyView.isCalledShowEmptyTsec == false)

    }
    
    func test_givenTrendingFinishTrueAndPromotionsFinishTrueAndErrorNoTsecTrueAndDisplayItemsEmptyAndErrorsTrackDataNotEmpty_whenICallCheckFinishService_thenICallViewTrackErrorScreen() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionsFinish = true
        sut.trendingFinish = true
        sut.errorNoTsec = true
        sut.promotionsTrending = nil
        sut.promotionsAll = [String: PromotionsBO]()
        dummyErrorTrackData.forceIsEmptyValue = false
        
        // when
        sut.checkFinishService()
        
        // then
        XCTAssert(dummyView.isCalledTrackErrorScreen == true)
        
    }
    
    func test_givenTrendingFinishTrueAndPromotionsFinishTrueAndErrorNoTsecTrueAndDisplayItemsEmptyAndErrorsTrackDataNotEmpty_whenICallCheckFinishService_thenICallViewTrackErrorScreenWithCorrectValue() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionsFinish = true
        sut.trendingFinish = true
        sut.errorNoTsec = true
        sut.promotionsTrending = nil
        sut.promotionsAll = [String: PromotionsBO]()
        dummyErrorTrackData.forceIsEmptyValue = false
        
        // when
        sut.checkFinishService()
        
        // then
        XCTAssert(dummyView.trackErrorSent == dummyErrorTrackData.format())
        
    }

    // MARK: - showTrendingPromotions

    func test_givenAPresenter_whenCallShowTrendingPromotions_thenCallBusToNavigate() {
        // when
        sut.showTrendingPromotions()

        // then
        XCTAssert(dummyBusManager.isCalledNavigateToShoppingList == true)
    }

    func test_givenAPresenterWithTrendingPromotions_whenCallShowTrendingPromotions_thenCallBusToNavigateWithObject() {
        // given
        sut.promotionsTrending = PromotionsGenerator.getPromotionsBO()

        // when
        sut.showTrendingPromotions()

        // then
        XCTAssert(dummyBusManager.isCalledPublishDataWithTagPromotionsBO == true)

       let promotionsTransport = dummyBusManager.dummyValues

        XCTAssert(sut.promotionsTrending === promotionsTransport.promotionsBO)
        XCTAssert(promotionsTransport.promotionName == Localizables.promotions.key_promotions_featured_text.capitalized)
        XCTAssert(promotionsTransport.category == nil)
        XCTAssert(promotionsTransport.trendingType == .hot)

    }

    // MARK: - categorySelected

    func test_givenACategory_whenICallCategorySelected_thenICallBusNavigateToNextScreen() {

        // given
        let category = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.categorySelected(withCategory: category)

        // then
        XCTAssert(dummyBusManager.isCalledNavigateToShoppingList == true)

    }

    func test_givenACategory_whenICallCategorySelected_thenCallBusToNavigateWithObject() {

        // given
        let category = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.categorySelected(withCategory: category)

        // then
        XCTAssert(dummyBusManager.isCalledPublishDataWithTagPromotionsBO == true)

        let promotionsTransport = dummyBusManager.dummyValues

        XCTAssert(promotionsTransport.promotionsBO == nil)
        XCTAssert(promotionsTransport.promotionName == category.name)
        XCTAssert(promotionsTransport.category === category)
    }

    // MARK: - getPromotionsForNextCategory

    func test_givenNoCategoriesBO_whenICallGetPromotionsForNextCategory_thenIDoNotCallInteractorProvidePromotionsByParams() {

        // given
        sut.promotionsCalled = 0

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // when
        sut.getPromotionsForNextCategory()

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionByParams == false)

    }

    func test_givenCategoriesBOWithoutCategories_whenICallGetPromotionsForNextCategory_thenIDoNotCallInteractorProvidePromotionsByParams() {

        // given
        sut.categories = CategoriesBO()
        sut.promotionsCalled = 0

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // when
        sut.getPromotionsForNextCategory()

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionByParams == false)

    }

    func test_givenCategoriesAndPromotionsCalledEqualsThanPromotionsScreen_whenICallGetPromotionsForNextCategory_thenIDoNotCallInteractorProvidePromotionsByParams() {

        // given
        sut.categories = PromotionsGenerator.getCategoryBO()
        sut.promotionsCalled = sut.promotionsForScreen

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // when
        sut.getPromotionsForNextCategory()

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionByParams == false)

    }

    func test_givenCategoriesAndPromotionsCalledMoreThanPromotionsScreen_whenICallGetPromotionsForNextCategory_thenIDoNotCallInteractorProvidePromotionsByParams() {

        // given
        sut.categories = PromotionsGenerator.getCategoryBO()
        sut.promotionsCalled = sut.promotionsForScreen + 1

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // when
        sut.getPromotionsForNextCategory()

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionByParams == false)

    }

    func test_givenCategoriesAndPromotionsCalledLessThanPromotionsScreen_whenICallGetPromotionsForNextCategory_thenICallInteractorProvidePromotionsByParams() {

        // given
        sut.categories = PromotionsGenerator.getCategoryBO()
        sut.promotionsCalled = 0

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // when
        sut.getPromotionsForNextCategory()

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionByParams == true)

    }

    func test_givenCategoriesAndPromotionsCalledLessThanPromotionsScreen_whenICallGetPromotionsForNextCategory_thenICallInteractorProvidePromotionsByParamsWithCategoryIdOfPromotionsCalled() {

        // given
        sut.categories = PromotionsGenerator.getCategoryBO()
        sut.promotionsCalled = sut.promotionsForScreen - sut.promotionsForScreen

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // when
        sut.getPromotionsForNextCategory()

        // then
        XCTAssert(dummyInteractor.categoriesParams[0] == ShoppingParamsTransport(categoryId: sut.categories?.categories[0].id.rawValue))
        XCTAssert(dummyInteractor.categoriesParams[1] == ShoppingParamsTransport(categoryId: sut.categories?.categories[1].id.rawValue))
        XCTAssert(dummyInteractor.categoriesParams[2] == ShoppingParamsTransport(categoryId: sut.categories?.categories[2].id.rawValue))

    }

    func test_givenCategoriesLessThanPAndPromotionsCalledEqualsThanCategoriesNumber_whenICallGetPromotionsForNextCategory_thenIDoNotCallInteractorProvidePromotionsByParams() {

        // given
        let categoriesBO = PromotionsGenerator.getCategoryBO()

        var categoriesTrimmed = [CategoryBO]()

        for i in 0 ..< sut.promotionsForScreen - 1 {
            categoriesTrimmed.append(categoriesBO.categories[i])
        }

        categoriesBO.categories = categoriesTrimmed
        sut.categories = categoriesBO
        sut.promotionsCalled = sut.categories!.categories.count

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // when
        sut.getPromotionsForNextCategory()

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionByParams == false)

    }

    func test_givenCategoriesAndPromotionsCalledMoreThanCategoriesNumber_whenICallGetPromotionsForNextCategory_thenIDoNotCallInteractorProvidePromotionsByParams() {

        // given
        let categoriesBO = PromotionsGenerator.getCategoryBO()

        var categoriesTrimmed = [CategoryBO]()

        for i in 0 ..< sut.promotionsForScreen - 1 {
            categoriesTrimmed.append(categoriesBO.categories[i])
        }

        categoriesBO.categories = categoriesTrimmed
        sut.categories = categoriesBO
        sut.promotionsCalled = sut.categories!.categories.count + 1

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // when
        sut.getPromotionsForNextCategory()

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionByParams == false)

    }

    func test_givenCategoriesAndPromotionsCalledLessThanCategoriesNumber_whenICallGetPromotionsForNextCategory_thenICallInteractorProvidePromotionsByParams() {

        // given
        let categoriesBO = PromotionsGenerator.getCategoryBO()

        var categoriesTrimmed = [CategoryBO]()

        for i in 0 ..< sut.promotionsForScreen - 1 {
            categoriesTrimmed.append(categoriesBO.categories[i])
        }

        categoriesBO.categories = categoriesTrimmed
        sut.categories = categoriesBO
        sut.promotionsCalled = 0

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // when
        sut.getPromotionsForNextCategory()

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionByParams == true)

    }

    func test_givenCategoriesAndPromotionsCalledLessThanCategoriesNumberAndErrorBO_whenICallGetPromotionsForNextCategoryAndInteractorReturnAndError_thenErrorBOStatusShouldMatchWithReturned() {

        // given
        let categoriesBO = PromotionsGenerator.getCategoryBO()

        var categoriesTrimmed = [CategoryBO]()

        for i in 0 ..< sut.promotionsForScreen - 1 {
            categoriesTrimmed.append(categoriesBO.categories[i])
        }

        categoriesBO.categories = categoriesTrimmed
        sut.categories = categoriesBO
        sut.promotionsCalled = 0

        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceErrorForPromotions = true
        let errorBO = ErrorBO()
        errorBO.status = 403
        dummyInteractor.errorBO = errorBO

        let errorBOBefore = ErrorBO()
        errorBOBefore.status = 500
        sut.errorBO = errorBOBefore
        sut.interactor = dummyInteractor

        // when
        sut.getPromotionsForNextCategory()

        // then
        XCTAssert(sut.errorBO?.status == errorBO.status)

    }

    func test_givenCategoriesAndPromotionsCalledLessThanPromotionsScreen_whenICallGetPromotionsForNextCategory_thenPromotionsCalledShouldMatchWithPromotionsForScreen() {

        // given
        sut.categories = PromotionsGenerator.getCategoryBO()
        sut.promotionsCalled = 0

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // when
        sut.getPromotionsForNextCategory()

        // then
        XCTAssert(sut.promotionsCalled == sut.promotionsForScreen)

    }

    func test_givenCategoriesAndPromotionsCalledLessThanCategoriesNumber_whenICallGetPromotionsForNextCategory_thenPromotionsCalledShouldMatchWithCategoriesNumber() {

        // given
        let categoriesBO = PromotionsGenerator.getCategoryBO()

        var categoriesTrimmed = [CategoryBO]()

        for i in 0 ..< sut.promotionsForScreen - 1 {
            categoriesTrimmed.append(categoriesBO.categories[i])
        }

        categoriesBO.categories = categoriesTrimmed
        sut.categories = categoriesBO
        sut.promotionsCalled = 0

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // when
        sut.getPromotionsForNextCategory()

        // then
        XCTAssert(sut.promotionsCalled == sut.categories!.categories.count)

    }

    // MARK: filterPressed

    func test_givenAny_whenICallFilterPressed_thenICallBusManagerNavigateScreenToFilterScreen() {

        // given

        // when
        sut.filterPressed()

        // then
        XCTAssert(dummyBusManager.isCalledNavigateToFilterScreen == true)

    }

    func test_givenAny_whenICallFilterPressed_thenICallBusManagerNavigateScreenToFilterScreenWithFilterTransportCorrectlyFilled() {

        // given

        // when
        sut.filterPressed()

        // then
        XCTAssert(dummyBusManager.filterTransportSent != nil)
        XCTAssert(dummyBusManager.filterTransportSent?.shouldAllowSelectUsageType == true)
        XCTAssert(dummyBusManager.filterTransportSent!.filters == nil)
        XCTAssert(dummyBusManager.filterTransportSent!.categoriesBO === sut.categories)
        XCTAssert(dummyBusManager.filterTransportSent?.shouldFilterInPreviousScreen == false)

    }

    // MARK: - itemSelected

    func test_givenADisplayItemHeader_whenICallItemSelected_thenICallBusNavigateToNextScreen() {

        // given
        let displayItem = DisplayItemPromotion.generateDisplayItemPromotion(withCategoryString: "ACTIVITIES", withCategoryType: .activities)
        sut.categories = PromotionsGenerator.getCategoryBO()

        // when
        sut.itemSelected(withDisplayItem: displayItem)

        // then
        XCTAssert(dummyBusManager.isCalledNavigateToShoppingList == true)

    }

    func test_givenADisplayItemHeaderAndCategories_whenICallItemSelected_thenCallBusToNavigateWithObject() {

        // given
        let displayItem = DisplayItemPromotion.generateDisplayItemPromotion(withCategoryString: "ACTIVITIES", withCategoryType: .activities)
        sut.categories = PromotionsGenerator.getCategoryBO()

        // when
        sut.itemSelected(withDisplayItem: displayItem)

        // then
        XCTAssert(dummyBusManager.isCalledPublishDataWithTagPromotionsBO == true)

        let promotionsTransport = dummyBusManager.dummyValues

        XCTAssert(sut.promotionsAll[displayItem.categoryType!.rawValue] === promotionsTransport.promotionsBO)
        XCTAssert(promotionsTransport.promotionName == displayItem.categoryString)
        XCTAssert(promotionsTransport.category != nil)
        XCTAssert(promotionsTransport.category?.id == displayItem.categoryType)

    }

    func test_givenADisplayItemPromotionTypeHigh_whenICallItemSelected_thenICallBusNavigateToDetailScreen() {

        // given
        let promotionsBO = PromotionsGenerator.getPromotionsBO()
        let displayItem = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionsBO.promotions[0], withTypeCell: .high)

        // when
        sut.itemSelected(withDisplayItem: displayItem)

        // then
        XCTAssert(dummyBusManager.isCalledNavigateToDetailScreen == true)
    }

    func test_givenADisplayItemPromotionTypeHigh_whenICallItemSelected_thenCallBusToNavigateDetailWithObject() {

        // given
        let promotionsBO = PromotionsGenerator.getPromotionsBO()
        let displayItem = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionsBO.promotions[0], withTypeCell: .high, withCategoryType: .activities)
        sut.categories = PromotionsGenerator.getCategoryBO()

        // when
        sut.itemSelected(withDisplayItem: displayItem)

        // then
        XCTAssert(dummyBusManager.isCalledPublishDataDetailWithTagPromotionsBO == true)

        let promotionDetailTransport = dummyBusManager.dummyDetailValue

        XCTAssert(promotionDetailTransport.promotionBO === displayItem.promotionBO)
        XCTAssert(promotionDetailTransport.category != nil)
    }

    func test_givenADisplayItemPromotionTypeSmall_whenICallItemSelected_thenICallBusNavigateToDetailScreen() {

        // given
        let promotionsBO = PromotionsGenerator.getPromotionsBO()
        let displayItem = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionsBO.promotions[0], withTypeCell: .small)

        // when
        sut.itemSelected(withDisplayItem: displayItem)

        // then
        XCTAssert(dummyBusManager.isCalledNavigateToDetailScreen == true)
    }

    func test_givenADisplayItemPromotionTypeSmall_whenICallItemSelected_thenCallBusToNavigateDetailWithObject() {

        // given
        let promotionsBO = PromotionsGenerator.getPromotionsBO()
        let displayItem = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionsBO.promotions[0], withTypeCell: .small, withCategoryType: .activities)
        sut.categories = PromotionsGenerator.getCategoryBO()

        // when
        sut.itemSelected(withDisplayItem: displayItem)

        // then
        XCTAssert(dummyBusManager.isCalledPublishDataDetailWithTagPromotionsBO == true)

        let promotionDetailTransport = dummyBusManager.dummyDetailValue

        XCTAssert(promotionDetailTransport.promotionBO === displayItem.promotionBO)
        XCTAssert(promotionDetailTransport.category != nil)
    }

    func test_givenADisplayItemPromotionTypeTrending_whenICallItemSelected_thenICallBusNavigateToDetailScreen() {

        // given
        let promotionsBO = PromotionsGenerator.getPromotionsBO()
        let displayItem = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionsBO.promotions[0], withTypeCell: .trending)

        // when
        sut.itemSelected(withDisplayItem: displayItem)

        // then
        XCTAssert(dummyBusManager.isCalledNavigateToDetailScreen == true)
    }

    func test_givenADisplayItemPromotionTypeTrending_whenICallItemSelected_thenCallBusToNavigateDetailWithObject() {

        // given
        let promotionsBO = PromotionsGenerator.getPromotionsBO()
        let displayItem = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionsBO.promotions[0], withTypeCell: .trending, withCategoryType: .activities)
        sut.categories = PromotionsGenerator.getCategoryBO()

        // when
        sut.itemSelected(withDisplayItem: displayItem)

        // then
        XCTAssert(dummyBusManager.isCalledPublishDataDetailWithTagPromotionsBO == true)

        let promotionDetailTransport = dummyBusManager.dummyDetailValue

        XCTAssert(promotionDetailTransport.promotionBO === displayItem.promotionBO)
        XCTAssert(promotionDetailTransport.category != nil)
    }

    func test_givenADisplayItemPromotionNotHeaderAndWithoutPromotionBO_whenICallItemSelected_thenIDoNotCallBusManagerNavigateDetail() {

        // given
        let promotionsBO = PromotionsGenerator.getPromotionsBO()
        var displayItem = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionsBO.promotions[0], withTypeCell: .high, withCategoryType: .activities)
        sut.categories = PromotionsGenerator.getCategoryBO()
        displayItem.promotionBO = nil

        // when
        sut.itemSelected(withDisplayItem: displayItem)

        // then
        XCTAssert(dummyBusManager.isCalledPublishDataDetailWithTagPromotionsBO == false)
    }

    // MARK: - retryButtonPressed

    func test_givenAny_whenICallRetryButtonPressed_thenICallViewShowSkeleton() {

        let dummyView = DummyView()
        sut.view = dummyView
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given

        // when

        sut.retryButtonPressed()

        // then
        XCTAssert(dummyView.isCalledShowSkeleton == true)
    }

    func test_givenAny_whenICallRetryButtonPressed_thenICallInteractorProvideCategories() {

        let dummyView = DummyView()
        sut.view = dummyView
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(dummyInteractor.isCalledProvideCategories == true)
    }

    func test_givenAny_whenICallRetryButtonPressedAndCategoriesReturnsSuccess_thenICallProvideTrendingPromotions() {

        // given

        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()

        sut.view = dummyView
        sut.interactor = dummyInteractor

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(dummyInteractor.isCalledProvideTrendingPromotions == true)

    }

    func test_givenAny_whenICallRetryButtonPressedAndCategoriesReturnsSuccess_thenICallProvideTrendingPromotionsWithAppropiateParams() {

        // given

        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()

        sut.view = dummyView
        sut.interactor = dummyInteractor

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.paramsTrending == ShoppingParamsTransport(trendingId: TrendingType.hot.rawValue, orderBy: .endDate))

    }

    func test_givenAny_whenICallRetryButtonPressedAndReturnsSuccess_thenThePresenterRetriveCategories() {

        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()

        sut.view = dummyView
        sut.interactor = dummyInteractor

        // when
        sut.retryButtonPressed()
        // then
        XCTAssert(dummyInteractor.categoriesBO === sut.categories)

    }

    func test_givenAny_whenICallRetryButtonPressedAndReturnsError403_thenICallViewShowError() {

        // given

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()

        let errorBO = ErrorBO()
        errorBO.status = ConstantsHTTPCodes.STATUS_403
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        sut.interactor = dummyInteractor

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(dummyView.isCalledShowError == true)

    }

    func test_givenAny_whenICallRetryButtonPressedAndReturnsErrorDifferentThan403_thenICallViewShowEmptyTsec() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()

        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        sut.interactor = dummyInteractor

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(dummyView.isCalledShowEmptyTsec == true)

    }

    func test_givenAny_whenICallRetryButtonPressedAndReturnsError403_thenINotCallViewShowEmptyTsec() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()

        let errorBO = ErrorBO()
        errorBO.status = ConstantsHTTPCodes.STATUS_403
        errorBO.code = ErrorCode.expiredSession.rawValue
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        sut.interactor = dummyInteractor

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(dummyView.isCalledShowEmptyTsec == false)

    }

    func test_givenAny_whenICallRetryButtonPressedWithNoContent_thenINotCallViewShowNoContent() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceNoContent = true

        sut.interactor = dummyInteractor
        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(dummyView.isCalledShowNoContent == true)

    }

    func test_givenAny_whenICallRetryButtonPressed_thenICallViewShowCategories() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(dummyView.isCalledShowCategories == true)

    }

    func test_givenAny_whenICallRetryButtonPressedWithThreeCategories_thenIDoNotCallViewShowCategories() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceThreeCategories = true

        sut.interactor = dummyInteractor

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(dummyView.isCalledShowCategories == false)

    }

    func test_givenAny_whenICallRetryButtonAndCategoriesReturnSuccess_thenICallViewShowCategoriesWithCorrectValue() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()

        sut.interactor = dummyInteractor

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(dummyView.displayModelToComponent?.categories.categories.count == 27)

        for i in 0 ..< dummyView.displayModelToComponent!.categories.categories.count {

            XCTAssert(dummyView.displayModelToComponent!.categories.categories[i].id == sut.categories!.categories[i + 3].id)
            XCTAssert(dummyView.displayModelToComponent!.categories.categories[i].name == sut.categories!.categories[i + 3].name)

        }

    }

    func test_givenAny_whenICallRetryButtonPressedAndCategoriesReturnSuccess_thenICallProvidePromotionsWithCategory() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionByParams == true)

    }

    func test_givenAny_whenICallRetryButtonPressed_thenIResetDefaultAttributes() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(dummyInteractor.wasPromotionsCalled == 0)
        XCTAssert(dummyInteractor.wasTrendingFinish == false)
        XCTAssert(dummyInteractor.wasPromotionsFinish == false)
        XCTAssert(dummyInteractor.wasErrorNoTsec == false)
        XCTAssert(dummyInteractor.wasPromotionsAll.isEmpty)
        XCTAssert(dummyInteractor.wasDisplayItems == DisplayItemsPromotions())

    }

    func test_givenAny_whenICallRetryButtonPressed_thenICallShowSkeleton() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(dummyView.isCalledShowSkeleton == true)

    }
    
    func test_givenAny_whenICallRetryButtonPressed_thenICallErrorTrackDataReset() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        
        // when
        sut.retryButtonPressed()
        
        // then
        XCTAssert(dummyErrorTrackData.isCalledReset == true)
    }
    
    func test_givenAny_whenICallRetryButtonPressedAndInteractorReturnsAndError_thenICallErrorsTrackDataAppend() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true
        
        // when
        sut.retryButtonPressed()
        
        // then
        XCTAssert(dummyErrorTrackData.isCalledAppend == true)
        
    }
    
    func test_givenAny_whenICallRetryButtonPressedAndInteractorReturnsAndError_thenICallErrorsTrackDataAppendCorrectValues() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true
        
        // when
        sut.retryButtonPressed()
        
        // then
        XCTAssert(dummyErrorTrackData.errorCodeReceived == errorBO.status)
        XCTAssert(dummyErrorTrackData.errorNameReceived == Promotions.categories)
        
    }
    
    func test_givenAny_whenICallRetryButtonPressedAndInteractorReturnsAndErrorDifferentThanSessionExpiredAndErrorsTrackDataNotEmpty_thenICallViewTrackErrorScreen() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true
        dummyErrorTrackData.forceIsEmptyValue = false
        
        // when
        sut.retryButtonPressed()
        
        // then
        XCTAssert(dummyView.isCalledTrackErrorScreen == true)
        
    }
    
    func test_givenAny_whenICallRetryButtonPressedAndInteractorReturnsAndErrorDifferentThanSessionExpiredAndErrorsTrackDataNotEmpty_thenICallViewTrackErrorScreenWithCorrectValue() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true
        dummyErrorTrackData.forceIsEmptyValue = false
        
        // when
        sut.retryButtonPressed()
        
        // then
        XCTAssert(dummyView.trackErrorSent == dummyErrorTrackData.format())
        
    }
    
    func test_givenAnNotError403AndErrorNotShown_whenICallRetryButtonPressed_thenICallErrorTrackDataAppend() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceErrorForPromotions = true
        
        // when
        sut.retryButtonPressed()
        
        // then
        XCTAssert(dummyErrorTrackData.isCalledAppend == true)
        
    }
    
    func test_givenAnNotError403AndErrorNotShown_whenICallRetryButtonPressed_thenICallErrorTrackDataAppendWithCorrectValues() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceErrorForPromotions = true
        
        // when
        sut.retryButtonPressed()
        
        // then
        XCTAssert(dummyErrorTrackData.errorCodeReceived == errorBO.status)
        XCTAssert(dummyErrorTrackData.errorNameReceived == Promotions.promotionByCategory)
    }

    // MARK: - DummyData

    class DummyInteractor: ShoppingInteractorProtocol {

        var forceError = false

        var forceErrorForPromotions = false

        var wasLoadingNextPage = true

        var presenter: ShoppingPresenter<DummyView>?

        var errorBO: ErrorBO?

        var isCalledProvideCategories = false

        var isCalledProvidePromotionByParams = false

        var isCalledProvideTrendingPromotions = false
        var paramsTrending: ShoppingParamsTransport?

        var forceNoContent = false

        var categoriesBO: CategoriesBO!

        var promotionsBO: PromotionsBO!

        var promotionsTrendingBO: PromotionsBO!

        var forceThreeCategories = false

        var categoriesParams = [ShoppingParamsTransport]()

        var wasPromotionsCalled = 0
        var wasTrendingFinish = false
        var wasPromotionsFinish = false
        var wasErrorNoTsec = false
        var wasPromotionsAll = [String: PromotionsBO]()
        var wasDisplayItems = DisplayItemsPromotions()

        func provideCategories() -> Observable<ModelBO> {

            isCalledProvideCategories = true

            if let presenter = presenter {

                wasPromotionsCalled = presenter.promotionsCalled
                wasTrendingFinish = presenter.trendingFinish
                wasPromotionsFinish = presenter.promotionsFinish
                wasErrorNoTsec = presenter.errorNoTsec
                wasPromotionsAll = presenter.promotionsAll
                wasDisplayItems = presenter.displayItems
            }

            if forceThreeCategories {
                categoriesBO = PromotionsGenerator.getThreeCategoryBO()
            } else {
                categoriesBO = PromotionsGenerator.getCategoryBO()
            }

            if forceNoContent {
                categoriesBO.status = 204
            }

            if !forceError {

                return Observable <ModelBO>.just(categoriesBO)

            } else {

                if errorBO == nil {
                    errorBO = ErrorBO(error: ErrorEntity(message: "Sesión expirada"))
                    errorBO!.isErrorShown = false
                }

                return Observable.error(ServiceError.GenericErrorBO(error: errorBO!))

            }
        }

        func providePromotions(byParams params: ShoppingParamsTransport) -> Observable <ModelBO> {
                isCalledProvidePromotionByParams = true

                if !isCalledProvideTrendingPromotions {
                    isCalledProvideTrendingPromotions = (params.trendingId == TrendingType.hot.rawValue)
                    paramsTrending = params
                }

                if params.categoryId != nil {
                    categoriesParams.append(params)
                }

                promotionsBO = PromotionsGenerator.getPromotionsBO()

                if !forceErrorForPromotions {

                    return Observable <ModelBO>.just(promotionsBO!)

                } else {

                    if errorBO == nil {
                        errorBO = ErrorBO(error: ErrorEntity(message: "Error"))
                        errorBO!.isErrorShown = false
                    }

                    return Observable.error(ServiceError.GenericErrorBO(error: errorBO!))
                }
        }

    }

    class DummyView: ShoppingViewProtocol, ViewProtocol {

        var isCalledShowTransactionsWithDisplayData = false
        var isCalledShowError = false
        var isCalledShowSuccessToast = false
        var isCalledShowEmptyTsec = false
        var isCalledShowNoContent = false
        var isCalledShowSkeleton = false
        var isCalledHideSkeleton = false
        var isCalledShowPromotions = false
        var isCalledShowCategories = false
        var showInfoDataIsCalled = false
        var isCalledShowFilterButton = false
        var isCalledTrackScreen = false
        var isCalledTrackErrorScreen = false
        var trackErrorSent = ""

        var diplayItems: DisplayItemsPromotions?
        var transactionsDisplayData: TransactionDisplayData?
        var displayModelToComponent: PromotionsCategoryDisplayModel?

        func showError(error: ModelBO) {
            isCalledShowError = true
        }

        func changeColorEditTexts() {
        }

        func showErrorEmptyTsec() {
            isCalledShowEmptyTsec = true
        }

        func showNoContentView() {
            isCalledShowNoContent = true
        }

        func showSkeleton() {
            isCalledShowSkeleton = true
        }

        func hideSkeleton() {
            isCalledHideSkeleton = true
        }

        func showPromotions(withDisplayItemsPromotions displayItemPromotions: DisplayItemsPromotions) {
            isCalledShowPromotions = true
            diplayItems = displayItemPromotions
        }

        func showCategories(withDisplayModel displayModel: PromotionsCategoryDisplayModel) {
            isCalledShowCategories = true
            displayModelToComponent = displayModel
        }

        func showFilterButton() {
            isCalledShowFilterButton = true
        }
        
        func trackScreen() {
            isCalledTrackScreen = true
        }
        
        func trackErrorScreen(withError error: String) {
            isCalledTrackErrorScreen = true
            trackErrorSent = error
        }
    }

    class DummyBusManager: BusManager {

        var isCalledNavigateToShoppingList = false
        var isCalledNavigateToFilterScreen = false
        var isCalledPublishDataWithTagPromotionsBO = false
        var isCalledPublishDataDetailWithTagPromotionsBO = false
        var isCalledNavigateToDetailScreen = false

        var dummyValues = PromotionsTransport()
        var filterTransportSent: ShoppingFilterTransport?
        var dummyDetailValue = PromotionDetailTransport()

        override init() {
            super.init()
            routerFactory = RouterInitFactory()
        }

        override func navigateScreen<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {
            if tag == ShoppingPageReaction.ROUTER_TAG && event === ShoppingPageReaction.EVENT_NAV_TO_NEXT_SCREEN {
                isCalledNavigateToShoppingList = true
                isCalledPublishDataWithTagPromotionsBO = true
                dummyValues = values as! PromotionsTransport
            } else if tag == ShoppingPageReaction.ROUTER_TAG && event === ShoppingPageReaction.EVENT_NAV_TO_FILTER_SCREEN {
                isCalledNavigateToFilterScreen = true
                filterTransportSent = values as? ShoppingFilterTransport

            } else if tag == "shopping-tag" && event === ShoppingPageReaction.EVENT_NAV_TO_DETAIL_SCREEN {
                isCalledNavigateToDetailScreen = true
                isCalledPublishDataDetailWithTagPromotionsBO = true
                dummyDetailValue = values as! PromotionDetailTransport
            }
        }

    }
}
