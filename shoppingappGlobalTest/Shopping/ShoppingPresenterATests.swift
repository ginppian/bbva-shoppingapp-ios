//
//  ShoppingPresenterATests.swift
//  shoppingapp
//
//  Created by jesus.martinez on 10/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest
import RxSwift
import CellsNativeComponents
import CoreLocation

#if TESTMX
    @testable import shoppingappMX
#endif

class ShoppingPresenterATests: XCTestCase {

    var sut: ShoppingPresenterA<DummyView>!
    var dummyBusManager: DummyBusManager!
    var dummyView = DummyView()
    var dummyInteractor = DummyInteractor()
    var dummyLocationManager = DummyLocationManager()

    override func setUp() {
        super.setUp()
        dummyBusManager = DummyBusManager()
        sut = ShoppingPresenterA<DummyView>(busManager: dummyBusManager)

        dummyView = DummyView()
        sut.view = dummyView

        dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        dummyLocationManager = DummyLocationManager()
        sut.locationManager = dummyLocationManager
    }

    // MARK: - viewLoaded

    func test_givenAny_whenICallViewLoaded_thenICallViewShowSkeleton() {

        // given

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowSkeleton == true)

    }

    func test_givenAny_whenICallViewLoaded_thenICallLocationManagerGetLocation() {

        // given

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyLocationManager.isCalledGetLocation == true)

    }

    // MARK: - viewLoaded -> gpsFail

    func test_givenLocationErrorByGpsFail_whenICallViewLoaded_thenISetLastLocationErrorType() {

        // given
        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewLoaded()

        // then
        XCTAssert(sut.lastLocationErrorType == .gpsFail)
    }

    func test_givenLocationErrorByGpsFailAndUserLocationNil_whenICallViewLoaded_thenIResetDefaultAttributes() {

        // given
        dummyLocationManager.locationErrorType = .gpsFail
        sut.userLocation = nil

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.wasPromotionsCalled == 0)
        XCTAssert(dummyInteractor.wasTrendingFinish == false)
        XCTAssert(dummyInteractor.wasPromotionsFinish == false)
        XCTAssert(dummyInteractor.wasErrorNoTsec == false)
        XCTAssert(dummyInteractor.wasPromotionsAll.isEmpty)
        XCTAssert(dummyInteractor.wasDisplayItems == DisplayItemsPromotions())

    }

    func test_givenLocationErrorByGpsFailAndUserLocationNil_whenICallViewLoaded_thenICallShowSkeleton() {

        // given
        dummyLocationManager.locationErrorType = .gpsFail
        sut.userLocation = nil

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowSkeleton == true)

    }

    func test_givenLocationErrorByGpsFail_whenICallViewLoaded_thenICallInteractorProvidePromotionsWithAppropiateParams() {

        // given
        dummyLocationManager.locationErrorType = .gpsFail

        dummyInteractor.categoriesToReturn = PromotionsGenerator.getCategoryBO()

        // when
        sut.viewLoaded()

        // then
        let locationParamExpected = ShoppingParamLocation(geolocation: sut.defaultLocation, distanceParam: ShoppingDistanceParam(distance: sut.defaultDistance, lengthType: sut.lengthType))

        XCTAssert(dummyInteractor.categoriesParams[0] == ShoppingParamsTransport(categoryId: dummyInteractor.categoriesToReturn?.categories[0].id.rawValue, location: locationParamExpected))
        XCTAssert(dummyInteractor.categoriesParams[1] == ShoppingParamsTransport(categoryId: dummyInteractor.categoriesToReturn?.categories[1].id.rawValue, location: locationParamExpected))
        XCTAssert(dummyInteractor.categoriesParams[2] == ShoppingParamsTransport(categoryId: dummyInteractor.categoriesToReturn?.categories[2].id.rawValue, location: locationParamExpected))

    }

    func test_givenLocationErrorByGpsFail_whenICallViewLoaded_thenICallInteractorProvideCategories() {

        // given
        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.isCalledProvideCategories == true)
    }

    func test_givenLocationErrorByGpsFail_whenICallViewLoadedAndCategoriesReturnsSuccess_thenICallProvideTrendingPromotions() {

        // given
        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.isCalledProvideTrendingPromotions == true)

    }

    func test_givenLocationErrorByGpsFailAndDisplayItems_whenICallViewLoaded_thenINotCallInteractorProvideCategoriesAndTrendingPromotions() {

        // given
        dummyLocationManager.locationErrorType = .gpsFail

        sut.displayItems.items = [DisplayItemPromotion.generateDisplayItemPromotion(withCategoryString: "ACTIVITIES", withCategoryType: .activities)]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.isCalledProvideCategories == false)
        XCTAssert(dummyInteractor.isCalledProvideTrendingPromotions == false)
    }

    func test_givenLocationError_whenICallViewLoadedAndCategoriesReturnsSuccess_thenICallProvideTrendingPromotionsWithAppropiateParams() {

        // given
        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.paramsTrending == ShoppingParamsTransport(trendingId: TrendingType.hot.rawValue, orderBy: .endDate))

    }

    func test_givenLocationErrorByGpsFail_whenICallViewLoadedAndReturnsSuccess_thenThePresenterRetriveCategories() {

        // given
        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.categoriesBO === sut.categories)

    }

    func test_givenLocationErrorByGpsFail_whenICallViewLoadedAndReturnsError403_thenICallViewShowError() {

        let errorBO = ErrorBO()
        errorBO.status = ConstantsHTTPCodes.STATUS_403
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        // given
        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowError == true)

    }

    func test_givenLocationErrorByGpsFail_whenICallViewLoadedAndReturnsErrorDifferentThan403_thenICallViewShowEmptyTsec() {

        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        // given
        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowEmptyTsec == true)

    }

    func test_givenLocationErrorByGpsFail_whenICallViewLoadedAndReturnsError403_thenINotCallViewShowEmptyTsec() {

        let errorBO = ErrorBO()
        errorBO.status = ConstantsHTTPCodes.STATUS_403
        errorBO.code = ErrorCode.expiredSession.rawValue
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        // given
        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowEmptyTsec == false)

    }

    func test_givenLocationErrorByGpsFail_whenICallViewLoadedWithNoContent_thenINotCallViewShowNoContent() {

        dummyInteractor.forceNoContent = true

        // given
        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowNoContent == true)

    }

    func test_givenLocationErrorByGpsFail_whenICallViewLoaded_thenICallShowCategories() {

        // given
        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowCategories == true)

    }

    func test_givenLocationErrorByGpsFail_whenICallViewLoadedWithThreeCategories_thenINotCallShowCategories() {

        dummyInteractor.forceThreeCategories = true

        // given
        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowCategories == false)

    }

    func test_givenLocationErrorByGpsFail_whenICallViewLoadedAndCategoriesReturnSuccess_thenICallShowCategoriesWithCorrectValue() {

        // given
        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewLoaded()

        // then
        if let categoriesComponent = dummyView.categoriesToComponent {
            XCTAssert(categoriesComponent.categories.count == 27)

            for i in 0 ..< categoriesComponent.categories.count {

                XCTAssert(categoriesComponent.categories[i].id == sut.categories!.categories[i + 3].id)
                XCTAssert(categoriesComponent.categories[i].name == sut.categories!.categories[i + 3].name)

            }
        } else {
            XCTFail("Should not be nil")
        }

    }

    func test_givenLocationErrorByGpsFail_whenICallViewLoadedAndCategoriesReturnSuccess_thenICallProvidePromotionsWithCategory() {

        // given
        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionByParams == true)

    }

    // MARK: - viewLoaded -> gpsOff

    func test_givenLocationErrorByGpsOff_whenICallViewLoaded_thenISetLastLocationErrorType() {

        // given
        dummyLocationManager.locationErrorType = .gpsOff

        // when
        sut.viewLoaded()

        // then
        XCTAssert(sut.lastLocationErrorType == .gpsOff)
    }

    func test_givenLocationErrorByGpsOff_whenICallViewLoaded_thenICallViewShowToastNotGPS() {

        // given
        dummyLocationManager.locationErrorType = .gpsOff

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowToastNotGPS == true)
    }

    func test_givenLocationErrorByGpsOffAndUserLocationNil_whenICallViewLoaded_thenIResetDefaultAttributes() {

        // given
        dummyLocationManager.locationErrorType = .gpsOff
        sut.userLocation = nil

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.wasPromotionsCalled == 0)
        XCTAssert(dummyInteractor.wasTrendingFinish == false)
        XCTAssert(dummyInteractor.wasPromotionsFinish == false)
        XCTAssert(dummyInteractor.wasErrorNoTsec == false)
        XCTAssert(dummyInteractor.wasPromotionsAll.isEmpty)
        XCTAssert(dummyInteractor.wasDisplayItems == DisplayItemsPromotions())

    }

    func test_givenLocationErrorByGpsOffAndUserLocationNil_whenICallViewLoaded_thenICallShowSkeleton() {

        // given
        dummyLocationManager.locationErrorType = .gpsOff
        sut.userLocation = nil

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowSkeleton == true)

    }

    func test_givenLocationErrorByGpsOff_whenICallViewLoaded_thenICallInteractorProvidePromotionsWithAppropiateParams() {

        // given
        dummyLocationManager.locationErrorType = .gpsOff

        dummyInteractor.categoriesToReturn = PromotionsGenerator.getCategoryBO()

        // when
        sut.viewLoaded()

        // then
        let locationParamExpected = ShoppingParamLocation(geolocation: sut.defaultLocation, distanceParam: ShoppingDistanceParam(distance: sut.defaultDistance, lengthType: sut.lengthType))

        XCTAssert(dummyInteractor.categoriesParams[0] == ShoppingParamsTransport(categoryId: dummyInteractor.categoriesToReturn?.categories[0].id.rawValue, location: locationParamExpected))
        XCTAssert(dummyInteractor.categoriesParams[1] == ShoppingParamsTransport(categoryId: dummyInteractor.categoriesToReturn?.categories[1].id.rawValue, location: locationParamExpected))
        XCTAssert(dummyInteractor.categoriesParams[2] == ShoppingParamsTransport(categoryId: dummyInteractor.categoriesToReturn?.categories[2].id.rawValue, location: locationParamExpected))

    }

    func test_givenLocationErrorByGpsOff_whenICallViewLoaded_thenICallInteractorProvideCategories() {

        // given
        dummyLocationManager.locationErrorType = .gpsOff

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.isCalledProvideCategories == true)
    }

    func test_givenLocationErrorByGpsOff_whenICallViewLoadedAndCategoriesReturnsSuccess_thenICallProvideTrendingPromotions() {

        // given
        dummyLocationManager.locationErrorType = .gpsOff

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.isCalledProvideTrendingPromotions == true)

    }

    func test_givenLocationErrorByGpsOffAndDisplayItems_whenICallViewLoaded_thenINotCallInteractorProvideCategoriesAndTrendingPromotions() {

        // given
        dummyLocationManager.locationErrorType = .gpsOff

        sut.displayItems.items = [DisplayItemPromotion.generateDisplayItemPromotion(withCategoryString: "ACTIVITIES", withCategoryType: .activities)]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.isCalledProvideCategories == false)
        XCTAssert(dummyInteractor.isCalledProvideTrendingPromotions == false)
    }

    func test_givenLocationErrorByGpsOff_whenICallViewLoadedAndCategoriesReturnsSuccess_thenICallProvideTrendingPromotionsWithAppropiateParams() {

        // given
        dummyLocationManager.locationErrorType = .gpsOff

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.paramsTrending == ShoppingParamsTransport(trendingId: TrendingType.hot.rawValue, orderBy: .endDate))

    }

    func test_givenLocationErrorByGpsOff_whenICallViewLoadedAndReturnsSuccess_thenThePresenterRetriveCategories() {

        // given
        dummyLocationManager.locationErrorType = .gpsOff

        // when
        sut.viewLoaded()
        // then
        XCTAssert(dummyInteractor.categoriesBO === sut.categories)

    }

    func test_givenLocationErrorByGpsOff_whenICallViewLoadedAndReturnsError403_thenICallViewShowError() {

        let errorBO = ErrorBO()
        errorBO.status = ConstantsHTTPCodes.STATUS_403
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        // given
        dummyLocationManager.locationErrorType = .gpsOff

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowError == true)

    }

    func test_givenLocationErrorByGpsOff_whenICallViewLoadedAndReturnsErrorDifferentThan403_thenICallViewShowEmptyTsec() {

        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        // given
        dummyLocationManager.locationErrorType = .gpsOff

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowEmptyTsec == true)

    }

    func test_givenLocationErrorByGpsOff_whenICallViewLoadedAndReturnsError403_thenINotCallViewShowEmptyTsec() {

        let errorBO = ErrorBO()
        errorBO.status = ConstantsHTTPCodes.STATUS_403
        errorBO.code = ErrorCode.expiredSession.rawValue
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        // given
        dummyLocationManager.locationErrorType = .gpsOff

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowEmptyTsec == false)

    }

    func test_givenLocationErrorByGpsOff_whenICallViewLoadedWithNoContent_thenINotCallViewShowNoContent() {

        dummyInteractor.forceNoContent = true

        // given
        dummyLocationManager.locationErrorType = .gpsOff

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowNoContent == true)

    }

    func test_givenLocationErrorByGpsOff_whenICallViewLoaded_thenICallShowCategories() {

        // given
        dummyLocationManager.locationErrorType = .gpsOff

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowCategories == true)

    }

    func test_givenLocationErrorByGpsOff_whenICallViewLoadedWithThreeCategories_thenINotCallShowCategories() {

        dummyInteractor.forceThreeCategories = true

        // given
        dummyLocationManager.locationErrorType = .gpsOff

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowCategories == false)

    }

    func test_givenLocationErrorByGpsOff_whenICallViewLoadedAndCategoriesReturnSuccess_thenICallShowCategoriesWithCorrectValue() {

        // given
        dummyLocationManager.locationErrorType = .gpsOff

        // when
        sut.viewLoaded()

        // then
        if let categoriesComponent = dummyView.categoriesToComponent {
            XCTAssert(categoriesComponent.categories.count == 27)

            for i in 0 ..< categoriesComponent.categories.count {

                XCTAssert(categoriesComponent.categories[i].id == sut.categories!.categories[i + 3].id)
                XCTAssert(categoriesComponent.categories[i].name == sut.categories!.categories[i + 3].name)

            }
        } else {
            XCTFail("Should not be nil")
        }

    }

    func test_givenLocationErrorByGpsOff_whenICallViewLoadedAndCategoriesReturnSuccess_thenICallProvidePromotionsWithCategory() {

        // given
        dummyLocationManager.locationErrorType = .gpsOff

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionByParams == true)

    }

    // MARK: - viewLoaded -> permissionDenied

    func test_givenLocationErrorByPermissionDenied_whenICallViewLoaded_thenISetLastLocationErrorType() {

        // given
        dummyLocationManager.locationErrorType = .permissionDenied

        // when
        sut.viewLoaded()

        // then
        XCTAssert(sut.lastLocationErrorType == .permissionDenied)
    }

    func test_givenLocationErrorByPermissionDenied_whenICallViewLoaded_thenICallViewShowToastNotPermission() {

        // given
        dummyLocationManager.locationErrorType = .permissionDenied

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowToastNotPermission == true)
    }

    func test_givenLocationErrorByPermissionDeniedAndUserLocationNil_whenICallViewLoaded_thenIResetDefaultAttributes() {

        // given
        dummyLocationManager.locationErrorType = .permissionDenied
        sut.userLocation = nil

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.wasPromotionsCalled == 0)
        XCTAssert(dummyInteractor.wasTrendingFinish == false)
        XCTAssert(dummyInteractor.wasPromotionsFinish == false)
        XCTAssert(dummyInteractor.wasErrorNoTsec == false)
        XCTAssert(dummyInteractor.wasPromotionsAll.isEmpty)
        XCTAssert(dummyInteractor.wasDisplayItems == DisplayItemsPromotions())

    }

    func test_givenLocationErrorByPermissionDeniedAndUserLocationNil_whenICallViewLoaded_thenICallShowSkeleton() {

        // given
        dummyLocationManager.locationErrorType = .permissionDenied
        sut.userLocation = nil

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowSkeleton == true)

    }

    func test_givenLocationErrorByPermissionDenied_whenICallViewLoaded_thenICallInteractorProvidePromotionsWithAppropiateParams() {

        // given
        dummyLocationManager.locationErrorType = .permissionDenied

        dummyInteractor.categoriesToReturn = PromotionsGenerator.getCategoryBO()

        // when
        sut.viewLoaded()

        // then
        let locationParamExpected = ShoppingParamLocation(geolocation: sut.defaultLocation, distanceParam: ShoppingDistanceParam(distance: sut.defaultDistance, lengthType: sut.lengthType))

        XCTAssert(dummyInteractor.categoriesParams[0] == ShoppingParamsTransport(categoryId: dummyInteractor.categoriesToReturn?.categories[0].id.rawValue, location: locationParamExpected))
        XCTAssert(dummyInteractor.categoriesParams[1] == ShoppingParamsTransport(categoryId: dummyInteractor.categoriesToReturn?.categories[1].id.rawValue, location: locationParamExpected))
        XCTAssert(dummyInteractor.categoriesParams[2] == ShoppingParamsTransport(categoryId: dummyInteractor.categoriesToReturn?.categories[2].id.rawValue, location: locationParamExpected))

    }

    func test_givenLocationErrorByPermissionDenied_whenICallViewLoaded_thenICallInteractorProvideCategories() {

        // given
        dummyLocationManager.locationErrorType = .permissionDenied

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.isCalledProvideCategories == true)
    }

    func test_givenLocationErrorByPermissionDenied_whenICallViewLoadedAndCategoriesReturnsSuccess_thenICallProvideTrendingPromotions() {

        // given
        dummyLocationManager.locationErrorType = .permissionDenied

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.isCalledProvideTrendingPromotions == true)

    }

    func test_givenLocationErrorByPermissionDeniedAndDisplayItems_whenICallViewLoaded_thenINotCallInteractorProvideCategoriesAndTrendingPromotions() {

        // given
        dummyLocationManager.locationErrorType = .permissionDenied

        sut.displayItems.items = [DisplayItemPromotion.generateDisplayItemPromotion(withCategoryString: "ACTIVITIES", withCategoryType: .activities)]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.isCalledProvideCategories == false)
        XCTAssert(dummyInteractor.isCalledProvideTrendingPromotions == false)
    }

    func test_givenLocationErrorByPermissionDenied_whenICallViewLoadedAndCategoriesReturnsSuccess_thenICallProvideTrendingPromotionsWithAppropiateParams() {

        // given
        dummyLocationManager.locationErrorType = .permissionDenied

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.paramsTrending == ShoppingParamsTransport(trendingId: TrendingType.hot.rawValue, orderBy: .endDate))

    }

    func test_givenLocationErrorByPermissionDenied_whenICallViewLoadedAndReturnsSuccess_thenThePresenterRetriveCategories() {

        // given
        dummyLocationManager.locationErrorType = .permissionDenied

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.categoriesBO === sut.categories)

    }

    func test_givenLocationErrorByPermissionDenied_whenICallViewLoadedAndReturnsError403_thenICallViewShowError() {

        let errorBO = ErrorBO()
        errorBO.status = ConstantsHTTPCodes.STATUS_403
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        // given
        dummyLocationManager.locationErrorType = .permissionDenied

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowError == true)

    }

    func test_givenLocationErrorByPermissionDenied_whenICallViewLoadedAndReturnsErrorDifferentThan403_thenICallViewShowEmptyTsec() {

        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        // given
        dummyLocationManager.locationErrorType = .permissionDenied

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowEmptyTsec == true)

    }

    func test_givenLocationErrorByPermissionDenied_whenICallViewLoadedAndReturnsError403_thenINotCallViewShowEmptyTsec() {

        let errorBO = ErrorBO()
        errorBO.status = ConstantsHTTPCodes.STATUS_403
        errorBO.code = ErrorCode.expiredSession.rawValue
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        // given
        dummyLocationManager.locationErrorType = .permissionDenied

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowEmptyTsec == false)

    }

    func test_givenLocationErrorByPermissionDenied_whenICallViewLoadedWithNoContent_thenINotCallViewShowNoContent() {

        dummyInteractor.forceNoContent = true

        // given
        dummyLocationManager.locationErrorType = .permissionDenied

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowNoContent == true)

    }

    func test_givenLocationErrorByPermissionDenied_whenICallViewLoaded_thenICallShowCategories() {

        // given
        dummyLocationManager.locationErrorType = .permissionDenied

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowCategories == true)

    }

    func test_givenLocationErrorByPermissionDenied_whenICallViewLoadedWithThreeCategories_thenINotCallShowCategories() {

        dummyInteractor.forceThreeCategories = true

        // given
        dummyLocationManager.locationErrorType = .permissionDenied

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowCategories == false)

    }

    func test_givenLocationErrorByPermissionDenied_whenICallViewLoadedAndCategoriesReturnSuccess_thenICallShowCategoriesWithCorrectValue() {

        // given
        dummyLocationManager.locationErrorType = .permissionDenied

        // when
        sut.viewLoaded()

        // then
        if let categoriesComponent = dummyView.categoriesToComponent {
            XCTAssert(categoriesComponent.categories.count == 27)

            for i in 0 ..< categoriesComponent.categories.count {

                XCTAssert(categoriesComponent.categories[i].id == sut.categories!.categories[i + 3].id)
                XCTAssert(categoriesComponent.categories[i].name == sut.categories!.categories[i + 3].name)

            }
        } else {
            XCTFail("Should not be nil")
        }

    }

    func test_givenLocationErrorByPermissionDenied_whenICallViewLoadedAndCategoriesReturnSuccess_thenICallProvidePromotionsWithCategory() {

        // given
        dummyLocationManager.locationErrorType = .permissionDenied

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionByParams == true)

    }

    // MARK: - viewLoaded -> locationSuccess

    func test_givenLastLocationErrorType_whenICallViewLoaded_thenLastLocationErrorTypeShouldbeNil() {

        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)

        // given
        sut.lastLocationErrorType = .gpsOff

        // when
        sut.viewLoaded()

        // then
        XCTAssert(sut.lastLocationErrorType == nil)
    }

    func test_givenLastLocationErrorTypeGPSOffAndUserLocationNil_whenICallViewLoaded_thenICallViewShowToastUpdatedPromotions() {

        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)

        // given
        sut.lastLocationErrorType = .gpsOff
        sut.userLocation = nil

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowToastUpdatedPromotions == true)
    }

    func test_givenLastLocationErrorTypePermissionDeniedAndUserLocationNil_whenICallViewLoaded_thenICallViewShowToastUpdatedPromotions() {

        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)

        // given
        sut.lastLocationErrorType = .permissionDenied
        sut.userLocation = nil

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowToastUpdatedPromotions == true)
    }

    func test_givenLastLocationErrorTypeGPSFailAndUserLocationNil_whenICallViewLoaded_thenICallViewShowToastUpdatedPromotions() {

        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)

        // given
        sut.lastLocationErrorType = .gpsFail
        sut.userLocation = nil

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowToastUpdatedPromotions == true)
    }

    func test_givenLastLocationErrorTypeGPSOffAndUserLocationNotNil_whenICallViewLoaded_thenIDoNotCallViewShowToastUpdatedPromotions() {

        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)

        // given
        sut.lastLocationErrorType = .gpsOff
        sut.userLocation = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.1)

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowToastUpdatedPromotions == false)
    }

    func test_givenLastLocationErrorTypePermissionDeniedAndUserLocationNotNil_whenICallViewLoaded_thenIDoNotCallViewShowToastUpdatedPromotions() {

        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)

        // given
        sut.lastLocationErrorType = .permissionDenied
        sut.userLocation = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.1)

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowToastUpdatedPromotions == false)
    }

    func test_givenLastLocationErrorTypeGPSFailAndUserLocationNotNil_whenICallViewLoaded_thenIDoNotCallViewShowToastUpdatedPromotions() {

        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)

        // given
        sut.lastLocationErrorType = .gpsFail
        sut.userLocation = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.1)

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowToastUpdatedPromotions == false)
    }

    func test_givenNotUserLocation_whenICallViewLoaded_thenUserLocationShouldBeFilled() {

        // given
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)

        // when
        sut.viewLoaded()

        // then
        XCTAssert(sut.userLocation != nil)

    }

    func test_givenNotUserLocation_whenICallViewLoaded_thenUserLocationShouldMatchWithLatitudeAndLongitudeByParam() {

        // given
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)

        // when
        sut.viewLoaded()

        // then
        XCTAssert(sut.userLocation?.latitude == 3.0)
        XCTAssert(sut.userLocation?.longitude == 4.0)

    }

    func test_givenUserLocation_whenICallViewLoaded_thenShouldNotChange() {

        // given
        sut.userLocation = GeoLocationBO(withLatitude: 8.0, withLongitude: 9.0)

        // when
        sut.viewLoaded()

        // then
        XCTAssert(sut.userLocation?.latitude == 8.0)
        XCTAssert(sut.userLocation?.longitude == 9.0)

        XCTAssert(sut.userLocation?.latitude != 3.0)
        XCTAssert(sut.userLocation?.longitude != 4.0)
    }

    func test_givenLocationSuccess_whenICallViewLoaded_thenICallInteractorProvideCategories() {

        // given
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.isCalledProvideCategories == true)
    }

    func test_givenLocationSuccess_whenICallViewLoadedAndCategoriesReturnsSuccess_thenICallProvideTrendingPromotions() {

        // given
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.isCalledProvideTrendingPromotions == true)

    }

    func test_givenLocationSuccessAndUserLocation_whenICallViewLoaded_thenINotCallInteractorProvideCategoriesAndTrendingPromotions() {

        // given
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        sut.userLocation = GeoLocationBO(withLatitude: 8.0, withLongitude: 9.0)

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.isCalledProvideCategories == false)
        XCTAssert(dummyInteractor.isCalledProvideTrendingPromotions == false)
    }

    func test_givenLocationSuccessAndUserLocationNil_whenICallViewLoaded_thenIResetDefaultAttributes() {

        // given
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        sut.userLocation = nil

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.wasPromotionsCalled == 0)
        XCTAssert(dummyInteractor.wasTrendingFinish == false)
        XCTAssert(dummyInteractor.wasPromotionsFinish == false)
        XCTAssert(dummyInteractor.wasErrorNoTsec == false)
        XCTAssert(dummyInteractor.wasPromotionsAll.isEmpty)
        XCTAssert(dummyInteractor.wasDisplayItems == DisplayItemsPromotions())

    }

    func test_givenLocationSuccessAndUserLocationNil_whenICallViewLoaded_thenICallShowSkeleton() {

        // given
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        sut.userLocation = nil

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowSkeleton == true)

    }

    func test_givenLocationSuccess_whenICallViewLoadedAndCategoriesReturnsSuccess_thenICallProvideTrendingPromotionsWithAppropiateParams() {

        // given
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.paramsTrending == ShoppingParamsTransport(trendingId: TrendingType.hot.rawValue, orderBy: .endDate))

    }

    func test_givenLocationSuccess_whenICallViewLoadedAndCategoriesReturnsSuccess_thenICallInteractorProvidePromotionsWithAppropiateParams() {

        // given
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)

        dummyInteractor.categoriesToReturn = PromotionsGenerator.getCategoryBO()

        // when
        sut.viewLoaded()

        // then
        let locationParamExpected = ShoppingParamLocation(geolocation: GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0), distanceParam: ShoppingDistanceParam(distance: sut.defaultDistance, lengthType: sut.lengthType))

        XCTAssert(dummyInteractor.categoriesParams[0] == ShoppingParamsTransport(categoryId: dummyInteractor.categoriesToReturn?.categories[0].id.rawValue, location: locationParamExpected))
        XCTAssert(dummyInteractor.categoriesParams[1] == ShoppingParamsTransport(categoryId: dummyInteractor.categoriesToReturn?.categories[1].id.rawValue, location: locationParamExpected))
        XCTAssert(dummyInteractor.categoriesParams[2] == ShoppingParamsTransport(categoryId: dummyInteractor.categoriesToReturn?.categories[2].id.rawValue, location: locationParamExpected))

    }

    func test_givenLocationSuccess_whenICallViewLoadedAndReturnsSuccess_thenThePresenterRetriveCategories() {

        // given
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.categoriesBO === sut.categories)

    }

    func test_givenLocationSuccess_whenICallViewLoadedAndReturnsError403_thenICallViewShowError() {

        let errorBO = ErrorBO()
        errorBO.status = ConstantsHTTPCodes.STATUS_403
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        // given
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowError == true)

    }

    func test_givenLocationSuccess_whenICallViewLoadedAndReturnsErrorDifferentThan403_thenICallViewShowEmptyTsec() {

        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        // given
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowEmptyTsec == true)

    }

    func test_givenLocationSuccess_whenICallViewLoadedAndReturnsError403_thenINotCallViewShowEmptyTsec() {

        let errorBO = ErrorBO()
        errorBO.status = ConstantsHTTPCodes.STATUS_403
        errorBO.code = ErrorCode.expiredSession.rawValue
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        // given
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowEmptyTsec == false)

    }

    func test_givenLocationSuccess_whenICallViewLoadedWithNoContent_thenINotCallViewShowNoContent() {

        dummyInteractor.forceNoContent = true

        // given
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowNoContent == true)

    }

    func test_givenLocationSuccess_whenICallViewLoaded_thenICallShowCategories() {

        // given
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowCategories == true)

    }

    func test_givenLocationSuccess_whenICallViewLoadedWithThreeCategories_thenINotCallShowCategories() {

        dummyInteractor.forceThreeCategories = true

        // given
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowCategories == false)

    }

    func test_givenLocationSuccess_whenICallViewLoadedAndCategoriesReturnSuccess_thenICallShowCategoriesWithCorrectValue() {

        // given
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)

        // when
        sut.viewLoaded()

        // then
        if let categoriesComponent = dummyView.categoriesToComponent {
            XCTAssert(categoriesComponent.categories.count == 27)

            for i in 0 ..< categoriesComponent.categories.count {

                XCTAssert(categoriesComponent.categories[i].id == sut.categories!.categories[i + 3].id)
                XCTAssert(categoriesComponent.categories[i].name == sut.categories!.categories[i + 3].name)

            }
        } else {
            XCTFail("Should not be nil")
        }

    }

    func test_givenLocationSuccess_whenICallViewLoadedAndCategoriesReturnSuccess_thenICallProvidePromotionsWithCategory() {

        // given
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionByParams == true)

    }

    // MARK: - viewDidAppear

    func test_givenAny_whenICallViewDidAppear_thenICallLocationManagerStartLocationObserverAndCheckStatus() {

        // given

        // when
        sut.viewDidAppear()

        // then
        XCTAssert(dummyLocationManager.isCalledStartLocationObserverAndCheckStatus == true)

    }

    // MARK: - viewDidDisappear

    func test_givenAny_whenICallViewDidDisappear_thenICallLocationManagerStopLocationObserver() {

        // given

        // when
        sut.viewDidDisappear()

        // then
        XCTAssert(dummyLocationManager.isCalledStopLocationObserver == true)

    }

    // MARK: - accessMapSelected

    func test_givenAny_whenICallAccessMapSelected_thenICallNavigateScreenWithEventSecondScreen() {

        // given

        // when
        sut.accessMapSelected()

        // then
        XCTAssert(dummyBusManager.isCalledNavigateToSecondScreen == true)

    }

    func test_givenAny_whenICallAccessMapSelected_thenICallNavigateScreenWithEventSecondScreenWithTransportWithCorrectValues() {

        // given

        // when
        sut.accessMapSelected()

        // then
        XCTAssert(dummyBusManager.promotionsTransport != nil)
        XCTAssert(dummyBusManager.promotionsTransport!.categories === sut.categories)

    }

    // MARK: - toastPressed

    func test_givenLastLocationErrorTypeGpsOff_whenICallToastPressed_thenICallBusManagerNavigateScreenWithEventLocationSettingHelp() {

        // given
        sut.lastLocationErrorType = .gpsOff

        // when
        sut.toastPressed()

        // then
        XCTAssert(dummyBusManager.isCalledNavigateToLocationSettingHelp == true)
    }

    func test_givenLastLocationErrorTypePermissionDenied_whenICallToastPressed_thenICallViewOpenAppSettings() {

        // given
        sut.lastLocationErrorType = .permissionDenied

        // when
        sut.toastPressed()

        // then
        XCTAssert(dummyView.isCalledOpenAppSettings == true)
    }

    // MARK: - itemSelected

    func test_givenADisplayItemHeader_whenICallItemSelected_thenICallBusNavigateToNextScreen() {

        // given
        let displayItem = DisplayItemPromotion.generateDisplayItemPromotion(withCategoryString: "ACTIVITIES", withCategoryType: .activities)
        sut.categories = PromotionsGenerator.getCategoryBO()

        // when
        sut.itemSelected(withDisplayItem: displayItem)

        // then
        XCTAssert(dummyBusManager.isCalledNavigateToShoppingList == true)

    }

    func test_givenADisplayItemHeaderAndCategories_whenICallItemSelected_thenCallBusToNavigateWithObject() {

        // given
        let displayItem = DisplayItemPromotion.generateDisplayItemPromotion(withCategoryString: "ACTIVITIES", withCategoryType: .activities)
        sut.categories = PromotionsGenerator.getCategoryBO()

        // when
        sut.itemSelected(withDisplayItem: displayItem)

        // then
        XCTAssert(dummyBusManager.isCalledPublishDataWithTagPromotionsBO == true)

        let promotionsTransport = dummyBusManager.promotionsTransport!

        XCTAssert(sut.promotionsAll[displayItem.categoryType!.rawValue] === promotionsTransport.promotionsBO)
        XCTAssert(promotionsTransport.promotionName == displayItem.categoryString)
        XCTAssert(promotionsTransport.category != nil)
        XCTAssert(promotionsTransport.category?.id == displayItem.categoryType)

    }

    func test_givenADisplayItemPromotionTypeHigh_whenICallItemSelected_thenICallBusNavigateToDetailScreen() {

        // given
        let promotionsBO = PromotionsGenerator.getPromotionsBO()
        let displayItem = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionsBO.promotions[0], withTypeCell: .high)

        // when
        sut.itemSelected(withDisplayItem: displayItem)

        // then
        XCTAssert(dummyBusManager.isCalledNavigateToDetailScreen == true)
    }

    func test_givenADisplayItemPromotionTypeHigh_whenICallItemSelected_thenCallBusToNavigateDetailWithObject() {

        // given
        let promotionsBO = PromotionsGenerator.getPromotionsBO()
        let displayItem = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionsBO.promotions[0], withTypeCell: .high, withCategoryType: .activities)
        sut.categories = PromotionsGenerator.getCategoryBO()

        // when
        sut.itemSelected(withDisplayItem: displayItem)

        // then
        XCTAssert(dummyBusManager.isCalledPublishDataDetailWithTagPromotionsBO == true)

        let promotionDetailTransport = dummyBusManager.dummyDetailValue

        XCTAssert(promotionDetailTransport.promotionBO === displayItem.promotionBO)
        XCTAssert(promotionDetailTransport.category != nil)
        XCTAssert(promotionDetailTransport.userLocation == sut.userLocation)
    }

    func test_givenADisplayItemPromotionTypeSmall_whenICallItemSelected_thenICallBusNavigateToDetailScreen() {

        // given
        let promotionsBO = PromotionsGenerator.getPromotionsBO()
        let displayItem = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionsBO.promotions[0], withTypeCell: .small)

        // when
        sut.itemSelected(withDisplayItem: displayItem)

        // then
        XCTAssert(dummyBusManager.isCalledNavigateToDetailScreen == true)
    }

    func test_givenADisplayItemPromotionTypeSmall_whenICallItemSelected_thenCallBusToNavigateDetailWithObject() {

        // given
        let promotionsBO = PromotionsGenerator.getPromotionsBO()
        let displayItem = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionsBO.promotions[0], withTypeCell: .small, withCategoryType: .activities)
        sut.categories = PromotionsGenerator.getCategoryBO()

        // when
        sut.itemSelected(withDisplayItem: displayItem)

        // then
        XCTAssert(dummyBusManager.isCalledPublishDataDetailWithTagPromotionsBO == true)

        let promotionDetailTransport = dummyBusManager.dummyDetailValue

        XCTAssert(promotionDetailTransport.promotionBO === displayItem.promotionBO)
        XCTAssert(promotionDetailTransport.category != nil)
        XCTAssert(promotionDetailTransport.userLocation == sut.userLocation)
    }

    func test_givenADisplayItemPromotionTypeTrending_whenICallItemSelected_thenICallBusNavigateToDetailScreen() {

        // given
        let promotionsBO = PromotionsGenerator.getPromotionsBO()
        let displayItem = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionsBO.promotions[0], withTypeCell: .trending)

        // when
        sut.itemSelected(withDisplayItem: displayItem)

        // then
        XCTAssert(dummyBusManager.isCalledNavigateToDetailScreen == true)
    }

    func test_givenADisplayItemPromotionTypeTrending_whenICallItemSelected_thenCallBusToNavigateDetailWithObject() {

        // given
        let promotionsBO = PromotionsGenerator.getPromotionsBO()
        let displayItem = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionsBO.promotions[0], withTypeCell: .trending, withCategoryType: .activities)
        sut.categories = PromotionsGenerator.getCategoryBO()

        // when
        sut.itemSelected(withDisplayItem: displayItem)

        // then
        XCTAssert(dummyBusManager.isCalledPublishDataDetailWithTagPromotionsBO == true)

        let promotionDetailTransport = dummyBusManager.dummyDetailValue

        XCTAssert(promotionDetailTransport.promotionBO === displayItem.promotionBO)
        XCTAssert(promotionDetailTransport.category != nil)
        XCTAssert(promotionDetailTransport.userLocation == sut.userLocation)
    }

    func test_givenADisplayItemPromotionNotHeaderAndWithoutPromotionBO_whenICallItemSelected_thenIDoNotCallBusManagerNavigateDetail() {

        // given
        let promotionsBO = PromotionsGenerator.getPromotionsBO()
        var displayItem = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionsBO.promotions[0], withTypeCell: .high, withCategoryType: .activities)
        sut.categories = PromotionsGenerator.getCategoryBO()
        displayItem.promotionBO = nil

        // when
        sut.itemSelected(withDisplayItem: displayItem)

        // then
        XCTAssert(dummyBusManager.isCalledPublishDataDetailWithTagPromotionsBO == false)
    }

    func test_givenADisplayItemPromotionTypeHighAndLocationManager_whenICallItemSelected_thenILocationManagerStopLocation() {

        // given
        let promotionsBO = PromotionsGenerator.getPromotionsBO()
        let displayItem = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionsBO.promotions[0], withTypeCell: .high)

        // when
        sut.itemSelected(withDisplayItem: displayItem)

        // then
        XCTAssert(dummyLocationManager.isCalledStopLocationObserver == true)
    }

    func test_givenADisplayItemPromotionTypeSmallAndLocationManager_whenICallItemSelected_thenILocationManagerStopLocation() {

        // given
        let promotionsBO = PromotionsGenerator.getPromotionsBO()
        let displayItem = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionsBO.promotions[0], withTypeCell: .small)

        // when
        sut.itemSelected(withDisplayItem: displayItem)

        // then
        XCTAssert(dummyLocationManager.isCalledStopLocationObserver == true)
    }

    func test_givenADisplayItemPromotionTypeTrendingAndLocationManager_whenICallItemSelected_thenILocationManagerStopLocation() {

        // given
        let promotionsBO = PromotionsGenerator.getPromotionsBO()
        let displayItem = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionsBO.promotions[0], withTypeCell: .trending)

        // when
        sut.itemSelected(withDisplayItem: displayItem)

        // then
        XCTAssert(dummyLocationManager.isCalledStopLocationObserver == true)
    }

    func test_givenADisplayItemPromotionTypeHigh_whenICallItemSelected_thenICallNavigateScreenWithTransportWithCompletion() {

        // given
        let promotionsBO = PromotionsGenerator.getPromotionsBO()
        let displayItem = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionsBO.promotions[0], withTypeCell: .high)

        // when
        sut.itemSelected(withDisplayItem: displayItem)

        // then
        XCTAssert(dummyBusManager.dummyDetailValue.completion != nil)
    }

    func test_givenADisplayItemPromotionTypeSmall_whenICallItemSelected_thenICallNavigateScreenWithTransportWithCompletion() {

        // given
        let promotionsBO = PromotionsGenerator.getPromotionsBO()
        let displayItem = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionsBO.promotions[0], withTypeCell: .small)

        // when
        sut.itemSelected(withDisplayItem: displayItem)

        // then
        XCTAssert(dummyBusManager.dummyDetailValue.completion != nil)
    }

    func test_givenADisplayItemPromotionTypeTrending_whenICallItemSelected_thenICallNavigateScreenWithTransportWithCompletion() {

        // given
        let promotionsBO = PromotionsGenerator.getPromotionsBO()
        let displayItem = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionsBO.promotions[0], withTypeCell: .trending)

        // when
        sut.itemSelected(withDisplayItem: displayItem)

        // then
        XCTAssert(dummyBusManager.dummyDetailValue.completion != nil)
    }

    func test_givenADisplayItemPromotionTypeHigh_whenICallItemSelected_thenICallNavigateScreenWithTransportCompletionShouldStartLocation() {

        // given
        let promotionsBO = PromotionsGenerator.getPromotionsBO()
        let displayItem = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionsBO.promotions[0], withTypeCell: .high)

        // when
        sut.itemSelected(withDisplayItem: displayItem)

        // then
        dummyBusManager.dummyDetailValue.completion?()
        XCTAssert(dummyLocationManager.isCalledStartLocationObserverAndCheckStatus == true)
    }

    func test_givenADisplayItemPromotionTypeSmall_whenICallItemSelected_thenICallNavigateScreenWithTransportCompletionShouldStartLocation() {

        // given
        let promotionsBO = PromotionsGenerator.getPromotionsBO()
        let displayItem = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionsBO.promotions[0], withTypeCell: .small)

        // when
        sut.itemSelected(withDisplayItem: displayItem)

        // then
        dummyBusManager.dummyDetailValue.completion?()
        XCTAssert(dummyLocationManager.isCalledStartLocationObserverAndCheckStatus == true)
    }

    func test_givenADisplayItemPromotionTypeTrending_whenICallItemSelected_thenICallNavigateScreenTransportWithCompletionShouldStartLocation() {

        // given
        let promotionsBO = PromotionsGenerator.getPromotionsBO()
        let displayItem = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionsBO.promotions[0], withTypeCell: .trending)

        // when
        sut.itemSelected(withDisplayItem: displayItem)

        // then
        dummyBusManager.dummyDetailValue.completion?()
        XCTAssert(dummyLocationManager.isCalledStartLocationObserverAndCheckStatus == true)
    }

    func test_givenADisplayItemPromotionTypeHeaderAndUserLocation_whenICallItemSelected_thenICallNavigateNextScreenWithUserLocation() {

        // given
        let promotionsBO = PromotionsGenerator.getPromotionsBO()
        var displayItem = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionsBO.promotions[0], withTypeCell: .header)
        displayItem.categoryType = .activities
        sut.userLocation = GeoLocationBO(withLatitude: 2.0, withLongitude: 3.1)
        sut.categories = PromotionsGenerator.getCategoryBO()

        // when
        sut.itemSelected(withDisplayItem: displayItem)

        // then
        XCTAssert(dummyBusManager.isCalledNavigateToShoppingList == true)
        XCTAssert(dummyBusManager.promotionsTransportA?.userLocation == sut.userLocation)
    }

    class DummyView: ShoppingViewProtocolA, ViewProtocol {

        var isCalledShowTransactionsWithDisplayData = false
        var isCalledShowError = false
        var isCalledShowSuccessToast = false
        var isCalledShowEmptyTsec = false
        var isCalledShowNoContent = false
        var isCalledShowSkeleton = false
        var isCalledHideSkeleton = false
        var isCalledShowPromotions = false
        var isCalledShowCategories = false
        var showInfoDataIsCalled = false
        var isCalledShowFilterButton = false
        var isCalledShowToastNotGPS = false
        var isCalledShowToastNotPermission = false
        var isCalledShowToastUpdatedPromotions = false
        var isCalledHideToast = false
        var isCalledOpenAppSettings = false

        var diplayItems: DisplayItemsPromotions?
        var transactionsDisplayData: TransactionDisplayData?
        var categoriesToComponent: CategoriesBO?

        func showInfoToastNotGPS() {
            isCalledShowToastNotGPS = true
        }

        func showInfoToastNotPermissions() {
            isCalledShowToastNotPermission = true
        }

        func showToastUpdatedPromotions() {
            isCalledShowToastUpdatedPromotions = true
        }

        func showError(error: ModelBO) {
            isCalledShowError = true
        }

        func changeColorEditTexts() {
        }

        func showErrorEmptyTsec() {
            isCalledShowEmptyTsec = true
        }

        func showNoContentView() {
            isCalledShowNoContent = true
        }

        func showSkeleton() {
            isCalledShowSkeleton = true
        }

        func hideSkeleton() {
            isCalledHideSkeleton = true
        }

        func showPromotions(withDisplayItemsPromotions displayItemPromotions: DisplayItemsPromotions) {
            isCalledShowPromotions = true
            diplayItems = displayItemPromotions
        }

        func showCategories(withDisplayModel displayModel: PromotionsCategoryDisplayModel) {
            isCalledShowCategories = true
            categoriesToComponent = displayModel.categories
        }

        func showFilterButton() {
            isCalledShowFilterButton = true
        }

        func hideToast() {
            isCalledHideToast = true
        }

        func openAppSettings() {
            isCalledOpenAppSettings = true
        }
        
        func trackScreen() {

        }
        
        func trackErrorScreen(withError error: String) {

        }
    }

    class DummyBusManager: BusManager {

        var isCalledNavigateToShoppingList = false
        var isCalledNavigateToSecondScreen = false
        var isCalledNavigateToLocationSettingHelp = false
        var isCalledNavigateToDetailScreen = false
        var isCalledPublishDataWithTagPromotionsBO = false
        var isCalledPublishDataDetailWithTagPromotionsBO = false

        var dummyValues = [String: AnyObject]()

        var promotionsTransport: PromotionsTransport?
        var promotionsTransportA: PromotionsTransportA?
        var dummyDetailValue = PromotionDetailTransport()

        override init() {
            super.init()
            routerFactory = RouterInitFactory()
        }

        override func navigateScreen<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {

            if tag == ShoppingPageReaction.ROUTER_TAG && event === ShoppingPageReaction.EVENT_NAV_TO_NEXT_SCREEN {
                isCalledNavigateToShoppingList = true
                isCalledPublishDataWithTagPromotionsBO = true

                promotionsTransport = values as? PromotionsTransport
                promotionsTransportA = values as? PromotionsTransportA
            }

            if tag == ShoppingPageReactionA.ROUTER_TAG && event === ShoppingPageReactionA.EVENT_NAV_TO_SECOND_SCREEN {
                isCalledNavigateToSecondScreen = true
                promotionsTransport = values as? PromotionsTransport
            }

            if tag == ShoppingPageReactionA.ROUTER_TAG && event === ShoppingPageReactionA.EVENT_NAV_TO_LOCATION_SETTING_HELP {
                isCalledNavigateToLocationSettingHelp = true
            }

            if tag == "shopping-tag" && event === ShoppingPageReaction.EVENT_NAV_TO_DETAIL_SCREEN {
                isCalledNavigateToDetailScreen = true
                isCalledPublishDataDetailWithTagPromotionsBO = true
                dummyDetailValue = values as! PromotionDetailTransport
            }

        }
    }

    class DummyInteractor: ShoppingInteractorProtocol {

        var forceError = false

        var forceErrorForPromotions = false

        var wasLoadingNextPage = true

        var presenter: ShoppingPresenter<DummyView>?

        var errorBO: ErrorBO?

        var isCalledProvideCategories = false

        var isCalledProvidePromotionByParams = false

        var isCalledProvideTrendingPromotions = false
        var paramsTrending: ShoppingParamsTransport?

        var forceNoContent = false

        var categoriesBO: CategoriesBO!
        var categoriesToReturn: CategoriesBO?

        var promotionsBO: PromotionsBO!

        var promotionsTrendingBO: PromotionsBO!

        var forceThreeCategories = false

        var categoriesParams = [ShoppingParamsTransport]()

        var wasPromotionsCalled = 0
        var wasTrendingFinish = false
        var wasPromotionsFinish = false
        var wasErrorNoTsec = false
        var wasPromotionsAll = [String: PromotionsBO]()
        var wasDisplayItems = DisplayItemsPromotions()

        func provideCategories() -> Observable<ModelBO> {

            isCalledProvideCategories = true

            if let presenter = presenter {

                wasPromotionsCalled = presenter.promotionsCalled
                wasTrendingFinish = presenter.trendingFinish
                wasPromotionsFinish = presenter.promotionsFinish
                wasErrorNoTsec = presenter.errorNoTsec
                wasPromotionsAll = presenter.promotionsAll
                wasDisplayItems = presenter.displayItems
            }

            if let categoriesToReturn = categoriesToReturn {
                categoriesBO = categoriesToReturn
            } else {
                if forceThreeCategories {
                    categoriesBO = PromotionsGenerator.getThreeCategoryBO()
                } else {
                    categoriesBO = PromotionsGenerator.getCategoryBO()
                }
            }

            if forceNoContent {
                categoriesBO.status = 204
            }

            if !forceError {

                return Observable <ModelBO>.just(categoriesBO)

            } else {

                if errorBO == nil {
                    errorBO = ErrorBO(error: ErrorEntity(message: "Sesión expirada"))
                    errorBO!.isErrorShown = false
                }

                return Observable.error(ServiceError.GenericErrorBO(error: errorBO!))

            }
        }

        func providePromotions(byParams params: ShoppingParamsTransport) -> Observable <ModelBO> {
            isCalledProvidePromotionByParams = true

            if !isCalledProvideTrendingPromotions {
                isCalledProvideTrendingPromotions = (params.trendingId == TrendingType.hot.rawValue)
                paramsTrending = params
            }

            if params.categoryId != nil {
                categoriesParams.append(params)
            }

            promotionsBO = PromotionsGenerator.getPromotionsBO()

            if !forceErrorForPromotions {

                return Observable <ModelBO>.just(promotionsBO!)

            } else {

                if errorBO == nil {
                    errorBO = ErrorBO(error: ErrorEntity(message: "Error"))
                    errorBO!.isErrorShown = false
                }

                return Observable.error(ServiceError.GenericErrorBO(error: errorBO!))
            }
        }

    }

}
