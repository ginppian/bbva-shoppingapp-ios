//
//  DisplayItemPromotionTest.swift
//  shoppingapp
//
//  Created by ruben.fernandez on 10/11/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class DisplayItemPromotionTest: XCTestCase {

    func test_givenAPromotionBOWithoutCategoryType_whenICallgGenerateDisplayItemPromotion_thenDisplayDataPanShouldBeMatchWithPromotionBO() {

        //given
        let promotionBO = PromotionsGenerator.getPromotionsBO()

        //When
        let displayData = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionBO.promotions[0], withTypeCell: CellPromotionType.high)

        // then
        XCTAssert(displayData.commerceName == "Repsol".uppercased() )
        XCTAssert(displayData.benefit == "Descuentos" )
        XCTAssert(displayData.descriptionPromotion == "Save 5% when filling up your car by paying with your BBVA credit card in Repsol stations." )
        XCTAssertTrue(displayData.dateDiff != nil )
        XCTAssertTrue(displayData.typeCell == .high)
        XCTAssertTrue(displayData.categoryType == .unknown)
        XCTAssertTrue(displayData.imageList != nil)
        XCTAssertTrue(displayData.imagePush == nil)
        XCTAssertTrue(displayData.imageDetail != nil)
    }

    func test_givenAPromotionBO_whenICallgGenerateDisplayItemPromotion_thenDisplayDataPanShouldBeMatchWithPromotionBO() {

        //given
        let promotionBO = PromotionsGenerator.getPromotionsBO()

        //When
        let displayData = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionBO.promotions[0], withTypeCell: CellPromotionType.high, withCategoryType: CategoryType.activities)

        // then
        XCTAssert(displayData.commerceName == "Repsol".uppercased() )
        XCTAssert(displayData.benefit == "Descuentos" )
        XCTAssert(displayData.descriptionPromotion == "Save 5% when filling up your car by paying with your BBVA credit card in Repsol stations." )
        XCTAssertTrue(displayData.dateDiff != nil )
        XCTAssertTrue(displayData.typeCell == .high)
        XCTAssertTrue(displayData.categoryType == .activities)
        XCTAssertTrue(displayData.imageList != nil)
        XCTAssertTrue(displayData.imagePush == nil)
        XCTAssertTrue(displayData.imageDetail != nil)
    }

    func test_givenAPromotionBO_whenICallgGenerateDisplayItemPromotion_thenDisplayDataPanShouldBeRemainOneDay() {

        //given
        let promotionBO = PromotionsGenerator.getPromotionsOneDayLeftBO()

        //When
        let displayData = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionBO.promotions[0], withTypeCell: CellPromotionType.high, withCategoryType: CategoryType.activities)

        // then
        XCTAssert(displayData.commerceName == "Repsol".uppercased() )
        XCTAssert(displayData.benefit == "Descuentos" )
        XCTAssert(displayData.descriptionPromotion == "Save 5% when filling up your car by paying with your BBVA credit card in Repsol stations." )
        XCTAssertTrue(displayData.typeCell == .high)
        XCTAssertTrue(displayData.categoryType == .activities)
        XCTAssertTrue(displayData.imageList != nil)
        XCTAssertTrue(displayData.imagePush == nil)
        XCTAssertTrue(displayData.imageDetail != nil)
    }

    func test_givenAOtherPromotionBO_whenICallgGenerateDisplayItemPromotion_thenDisplayDataPanShouldBeMatchWithPromotionBO() {

        //given
        let promotionBO = PromotionsGenerator.getPromotionsBO()

        //When
        let displayData = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionBO.promotions[1], withTypeCell: CellPromotionType.small, withCategoryType: CategoryType.activities)

        // then
        XCTAssert(displayData.commerceName == "Ikea".uppercased() )
        XCTAssert(displayData.descriptionPromotion == "From 20€ discount in your next purchase in Ikea." )
        XCTAssert(displayData.benefit == "" )
        XCTAssertTrue(displayData.dateDiff != nil )
        XCTAssertTrue(displayData.typeCell == .small)
        XCTAssertTrue(displayData.categoryType == .activities)
        XCTAssertTrue(displayData.imageList != nil)
        XCTAssertTrue(displayData.imagePush == nil)
        XCTAssertTrue(displayData.imageDetail != nil)

    }

    func test_givenACategoryString_whenICallgGenerateDisplayItemPromotion_thenHeaderTypeCell() {

        //given

        //When
        let displayData = DisplayItemPromotion.generateDisplayItemPromotion(withCategoryString: "ACTIVITIES", withCategoryType: .activities)

        // then
        XCTAssert(displayData.commerceName == nil )
        XCTAssert(displayData.descriptionPromotion == nil )
        XCTAssertTrue(displayData.dateDiff == nil )
        XCTAssertTrue(displayData.imageList == nil)
        XCTAssertTrue(displayData.imagePush == nil)
        XCTAssertTrue(displayData.imageDetail == nil)
        XCTAssertTrue(displayData.typeCell == .header)
        XCTAssertTrue(displayData.categoryString == "ACTIVITIES")
        XCTAssertTrue(displayData.categoryType == .activities)

    }

    func test_givenATrendingPromotions_whenICallGenerateDisplayItemPromotionTrending_thenHaveTheCorrectValue() {

        //given
        let promotionBO = PromotionsGenerator.getPromotionsBO()

        //When
        let displayData = DisplayItemPromotion.generateDisplayItemPromotionTrending(withTrendingPromotions: promotionBO)

        // then
        XCTAssert(displayData.commerceName == nil )
        XCTAssert(displayData.descriptionPromotion == nil )
        XCTAssertTrue(displayData.dateDiff == nil )
        XCTAssertTrue(displayData.typeCell == .trending)
        XCTAssertTrue(displayData.categoryString == TrendingType.hot.rawValue)
        XCTAssertTrue(displayData.itemsTrending.count == 2)
        XCTAssert(displayData.itemsTrending[0].commerceName == "Repsol".uppercased() )
        XCTAssert(displayData.itemsTrending[0].benefit == "Descuentos" )
        XCTAssert(displayData.itemsTrending[0].descriptionPromotion == "Save 5% when filling up your car by paying with your BBVA credit card in Repsol stations." )
        XCTAssertTrue(displayData.itemsTrending[0].dateDiff != nil )
        XCTAssertTrue(displayData.itemsTrending[0].imageList != nil)
        XCTAssertTrue(displayData.itemsTrending[0].imagePush == nil)
        XCTAssertTrue(displayData.itemsTrending[0].imageDetail != nil)
        XCTAssertTrue(displayData.itemsTrending[0].typeCell == .trending )
        XCTAssertTrue(displayData.itemsTrending[1].typeCell == .trending )

    }

    func test_givenATrendingPromotionsWithStores_whenICallGenerateDisplayItemPromotionTrending_thenDontShowDistance() {
        //given
        let promotionBO = PromotionsGenerator.getPromotionsBOWithStores()

        //When
        let displayData = DisplayItemPromotion.generateDisplayItemPromotionTrending(withTrendingPromotions: promotionBO)

        // then
        XCTAssert(displayData.dateDiffPlusDistance == nil )
    }

    func test_givenAPromotionBOWithStores_whenICallGenerateDisplayItemPromotion_thenDistanceIsNil() {
        //given
        let promotionBO = PromotionsGenerator.getPromotionDetail()

        //When
        let displayData = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionBO)

        // then
        XCTAssert(displayData.dateDiffPlusDistance == nil )
    }

    func test_givenAPromotionBOWithStoresAndUserLocation_whenICallGenerateDisplayItemPromotion_thenDistanceIsShown() {
        //given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        let userLocation = GeoLocationBO(withLatitude: 40.0, withLongitude: -3.3)

        //When
        let displayData = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionBO, userLocation: userLocation)

        // then
        XCTAssert(displayData.dateDiffPlusDistance != nil )
    }

    func test_givenAPromotionBOWithStoresAndUserLocation_whenICallGenerateDisplayItemPromotion_thenDateAndDistanceMatch() {
        //given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        let userLocation = GeoLocationBO(withLatitude: 40.0, withLongitude: -3.3)

        let effectiveDateTextInfo = Settings.Promotions.effectiveDateText(forPromotionBO: promotionBO, compactSize: false)
        let dateDiff = effectiveDateTextInfo?.date
        let distance = promotionBO.stores?.first?.formattedDistance()
        let dateDiffPlusDistance = Settings.Promotions.dateDiffPlusDistance(distance: distance!, effectiveDateText: dateDiff!, isIntervalDate: false)
        let displayItemPromotionDataInfo = (dateDiff: dateDiff, dateDiffPlusDistance: dateDiffPlusDistance)

        //When
        let displayData = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionBO, userLocation: userLocation)

        // then
        XCTAssert(displayData.dateDiffPlusDistance == displayItemPromotionDataInfo.dateDiffPlusDistance)
        XCTAssert(displayData.dateDiff == displayItemPromotionDataInfo.dateDiff)
    }

    func test_givenAPromotionBOWithoutUserLatitudeAndLongitude_whenICallgGenerateDisplayItemPromotionInMap_thenDisplayDataPanShouldBeMatchWithPromotionBO() {

        //given
        let promotionBO = PromotionsGenerator.getPromotionsBO()
        let storeLatitude = 40.446311
        let storeLongitude = -3.692449
        let userLatitude: Double? = nil
        let userLongitude: Double? = nil

        //When
        let displayData = DisplayItemPromotion.generateDisplayItemPromotionInMap(withPromotion: promotionBO.promotions[0], withStoreLatitude: storeLatitude, andStoreLongitude: storeLongitude, andWithUserLatitude: userLatitude, andUserLongitude: userLongitude)

        // then
        XCTAssert(displayData.commerceName == "Repsol".uppercased() )
        XCTAssert(displayData.benefit == "Descuentos" )
        XCTAssert(displayData.descriptionPromotion == "Save 5% when filling up your car by paying with your BBVA credit card in Repsol stations." )
        XCTAssertTrue(displayData.imageList != nil)
        XCTAssertTrue(displayData.imagePush == nil)
        XCTAssertTrue(displayData.imageDetail != nil)
    }

    func test_givenAPromotionBOWithUserLatitudeAndLongitude_whenICallgGenerateDisplayItemPromotionInMap_thenDisplayDataPanShouldBeMatchWithPromotionBO() {

        //given
        let promotionBO = PromotionsGenerator.getPromotionsBO()
        let storeLatitude = 40.446311
        let storeLongitude = -3.692449
        let userLatitude = 40.446099
        let userLongitude = -3.689681

        //When
        let displayData = DisplayItemPromotion.generateDisplayItemPromotionInMap(withPromotion: promotionBO.promotions[0], withStoreLatitude: storeLatitude, andStoreLongitude: storeLongitude, andWithUserLatitude: userLatitude, andUserLongitude: userLongitude)

        // then
        XCTAssert(displayData.commerceName == "Repsol".uppercased() )
        XCTAssert(displayData.benefit == "Descuentos" )
        XCTAssert(displayData.descriptionPromotion == "Save 5% when filling up your car by paying with your BBVA credit card in Repsol stations." )
        XCTAssertTrue(displayData.imageList != nil)
        XCTAssertTrue(displayData.imagePush == nil)
        XCTAssertTrue(displayData.imageDetail != nil)
        XCTAssertTrue(displayData.dateDiffPlusDistance != nil)
    }

    func test_givenAPromotionBOWithUserLatitudeAndLongitude_whenICallgGenerateDisplayItemPromotionInMap_thenDistanceShouldBeTheCorrectInMeters() {

        //given
        let promotionBO = PromotionsGenerator.getPromotionsBO()
        let storeLatitude = 40.446311
        let storeLongitude = -3.692449
        let userLatitude = 40.446099
        let userLongitude = -3.689681
        promotionBO.promotions[0].startDate = Date()
        promotionBO.promotions[0].endDate = Calendar.current.date(byAdding: .day, value: 8, to: Date())

        //When
        let displayData = DisplayItemPromotion.generateDisplayItemPromotionInMap(withPromotion: promotionBO.promotions[0], withStoreLatitude: storeLatitude, andStoreLongitude: storeLongitude, andWithUserLatitude: userLatitude, andUserLongitude: userLongitude)

        // then
        #if TESTMX
            XCTAssert(displayData.dateDiffPlusDistance! == String(format: "%@ %@ - %@", "236", Localizables.promotions.key_distance_meters, displayData.dateDiff!))

        #else
            XCTAssert(displayData.dateDiffPlusDistance!.hasSuffix(String(format: "%@ %@)", "236", Localizables.promotions.key_distance_meters)))
        #endif

    }

    func test_givenAPromotionBOWithUserLatitudeAndLongitude_whenICallgGenerateDisplayItemPromotionInMap_thenDistanceShouldBeTheCorrectInKilometers() {

        //given
        let promotionBO = PromotionsGenerator.getPromotionsBO()
        let storeLatitude = 40.446311
        let storeLongitude = -3.692449
        let userLatitude = 40.446814
        let userLongitude = -3.667943
        promotionBO.promotions[0].startDate = Date()
        promotionBO.promotions[0].endDate = Calendar.current.date(byAdding: .day, value: 8, to: Date())

        //When
        let displayData = DisplayItemPromotion.generateDisplayItemPromotionInMap(withPromotion: promotionBO.promotions[0], withStoreLatitude: storeLatitude, andStoreLongitude: storeLongitude, andWithUserLatitude: userLatitude, andUserLongitude: userLongitude)

        // then

        let numberFormatter = NumberFormatter()
        numberFormatter.minimumFractionDigits = 0
        numberFormatter.maximumFractionDigits = 1

        let value = NSDecimalNumber(value: 2.1)

        #if TESTMX
            XCTAssert(displayData.dateDiffPlusDistance! == String(format: "%@ %@ - %@", numberFormatter.string(from: value)!, Localizables.promotions.key_distance_kilometers, displayData.dateDiff!))
        #else
            XCTAssert(displayData.dateDiffPlusDistance!.hasSuffix(String(format: "%@ %@)", numberFormatter.string(from: value)!, Localizables.promotions.key_distance_kilometers)))
        #endif

    }

    func test_givenAPromotionBOWithUserLatitudeAndLongitude_whenICallgGenerateDisplayItemPromotionInMap_thenDatePlusDistanceShouldBeTheCorrectWithInBrackets() {

        //given
        let promotionBO = PromotionsGenerator.getPromotionsBO()
        let storeLatitude = 40.446311
        let storeLongitude = -3.692449
        let userLatitude = 40.446099
        let userLongitude = -3.689681
        promotionBO.promotions[0].startDate = Date()
        promotionBO.promotions[0].endDate = Calendar.current.date(byAdding: .day, value: 8, to: Date())

        //when
        let displayData = DisplayItemPromotion.generateDisplayItemPromotionInMap(withPromotion: promotionBO.promotions[0], withStoreLatitude: storeLatitude, andStoreLongitude: storeLongitude, andWithUserLatitude: userLatitude, andUserLongitude: userLongitude)

        //then
        #if TESTMX
            XCTAssert(displayData.dateDiffPlusDistance! == String(format: "%@ %@ - %@", "236", Localizables.promotions.key_distance_meters, displayData.dateDiff!))
        #else
            XCTAssert(displayData.dateDiffPlusDistance! == String(format: "%@ (%@ %@)", displayData.dateDiff!, "236", Localizables.promotions.key_distance_meters))
        #endif

    }

    func test_givenAPromotionBOWithUserLatitudeAndLongitude_whenICallgGenerateDisplayItemPromotionInMap_thenDatePlusDistanceShouldBeTheCorrectWithSlashOrBrakets() {

        //given
        let promotionBO = PromotionsGenerator.getPromotionsBO()
        let storeLatitude = 40.446311
        let storeLongitude = -3.692449
        let userLatitude = 40.446099
        let userLongitude = -3.689681

        //when
        let displayData = DisplayItemPromotion.generateDisplayItemPromotionInMap(withPromotion: promotionBO.promotions[0], withStoreLatitude: storeLatitude, andStoreLongitude: storeLongitude, andWithUserLatitude: userLatitude, andUserLongitude: userLongitude)

        //then
        #if TESTMX
            XCTAssert(displayData.dateDiffPlusDistance! == String(format: "%@ %@ - %@", "236", Localizables.promotions.key_distance_meters, displayData.dateDiff!))
        #else
            XCTAssert(displayData.dateDiffPlusDistance! == String(format: "%@ / %@ %@", displayData.dateDiff!, "236", Localizables.promotions.key_distance_meters))
        #endif

    }

    func test_givenAPromotionBOOnlyUsageTypeOnline_whenICallGenerateDisplayItemPromotionInDetail_thenUsageTypesShouldBeMatchWithPromotionBO() {

        //given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        let usageType = UsageTypeBO(promotionUsageIdType: ID_Type(id: "ONLINE", name: nil))

        promotionBO.usageTypes = [usageType]

        //when
        let displayData = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionBO)

        //then
        XCTAssert(displayData.usageTypeDescription == Localizables.promotions.key_promotions_online_text)

    }

    func test_givenAPromotionBOOnlyUsageTypePhysical_whenICallGenerateDisplayItemPromotionInDetail_thenUsageTypesShouldBeMatchWithPromotionBO() {

        //given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        let usageType = UsageTypeBO(promotionUsageIdType: ID_Type(id: "PHYSICAL", name: nil))

        promotionBO.usageTypes = [usageType]

        //when
        let displayData = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionBO)

        //then
        XCTAssert(displayData.usageTypeDescription == Localizables.promotions.key_promotions_facetoface_text)
    }

    func test_givenAPromotionBOUsageTypePhysicalAndOnline_whenICallGenerateDisplayItemPromotionInDetail_thenUsageTypesShouldBeMatchWithPromotionBO() {

        //given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        let usageTypeOnline = UsageTypeBO(promotionUsageIdType: ID_Type(id: "ONLINE", name: nil))
        let usageType = UsageTypeBO(promotionUsageIdType: ID_Type(id: "PHYSICAL", name: nil))

        promotionBO.usageTypes = [usageTypeOnline, usageType]

        //when
        let displayData = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionBO)

        //then
        XCTAssert(displayData.usageTypeDescription == Localizables.promotions.key_promotions_online_and_facetoface_text)

    }

}
