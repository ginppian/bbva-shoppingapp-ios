//
//  LimitsPresenterATest.swift
//  shoppingappMX
//
//  Created by Magdali Grajales on 21/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest
import RxSwift
import CellsNativeComponents

#if TESTMX
    @testable import shoppingappMX
#endif

class LimitsPresenterATests: XCTestCase {
    
    var sut: LimitsPresenterA<LimitsViewADummy>!
    var dummyBusManager: LimitsDummyBusManager!
    var dummyView: LimitsViewADummy!
    var dummyInteractor: DummyLimitsInteractorA!
    
    override func setUp() {
        super.setUp()
        dummyBusManager = LimitsDummyBusManager()
        sut = LimitsPresenterA<LimitsViewADummy>(busManager: dummyBusManager)
        
        dummyView = LimitsViewADummy()
        sut.view = dummyView
        
        dummyInteractor = DummyLimitsInteractorA()
        sut.interactor = dummyInteractor
    }
    
    // MARK: - CreateLimitTransport
    
    func test_givenLimitBOAndCardId_whenICallCreateLimitTransport_thenLimitBOAndCardIdShouldMatch() {
        
        // given
        let limitsBO = LimitsGenerator.getThreeLimits()
        let limitBO = limitsBO.limits[0]
        let cardId = "1234"
       
        sut.cardBO = nil
        sut.limitsBO = limitsBO
        
        let limitTransport = LimitTransport(withLimit: limitBO, andCardId: cardId)
        
        // when
        let expectedLimitTransport = sut.createLimitTransport(limit: limitsBO.limits[0], cardId: cardId)
        
        // then
        XCTAssert(limitTransport.cardId == expectedLimitTransport.cardId)
        XCTAssert(limitTransport.limit === expectedLimitTransport.limit)
        XCTAssert(limitTransport.cardType == expectedLimitTransport.cardType)
    }
    
    func test_givenLimitBOWithCardBOAndCardType_whenICallCreateLimitTranspor_thenLimitBOWithCardIdAndCardTypeShouldMatch() {
        
        // given
        let limitsBO = LimitsGenerator.getThreeLimits()
        let limitBO = limitsBO.limits[0]
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        
        sut.cardBO = cardBO
        sut.limitsBO = limitsBO
        
        let limitTransport = LimitTransport(withLimit: limitBO, andCardId: cardBO.cardId, cardType: cardBO.cardType)
        
        // when
        let expectedLimitTransport = sut.createLimitTransport(limit: limitsBO.limits[0], cardId: cardBO.cardId)
        
        // then
        XCTAssert(limitTransport.cardId == expectedLimitTransport.cardId)
        XCTAssert(limitTransport.limit === expectedLimitTransport.limit)
        XCTAssert(limitTransport.cardType == expectedLimitTransport.cardType)
    }
    
    // MARK: - LoadLimits
    
    func test_givenCardBO_whenICallLoadLimits_thenICallShowSkeleton() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        
        // when
        sut.loadLimits(cardBO: cardBO)
        
        // then
        XCTAssert(dummyView.isCalledShowSkeleton == true)
    }
    
    func test_givenCardBO_whenICallLoadLimits_thenICallInteractorProvideLimits() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        
        // when
        sut.loadLimits(cardBO: cardBO)
        
        // then
        XCTAssert(dummyInteractor.isCalledProvideLimits == true)
    }
    
    func test_givenCardBO_whenICallLoadLimits_thenICallInteractorProvideLimitsWithCardId() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        
        // when
        sut.loadLimits(cardBO: cardBO)
        
        // then
        XCTAssert(dummyInteractor.cardIdSent == cardBO.cardId)
    }
    
    // MARK: - LoadLimits - Interactor Return Success
    
    func test_givenCardBO_whenICallLoadLimitsAndInteractorReturnSuccess_thenICallHideSkeleton() {
        
        //given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        
        //when
        sut.loadLimits(cardBO: cardBO)
        
        //then
        XCTAssert(dummyView.isCalledHideSkeleton == true)
    }
    
    func test_givenCardBO_whenICallLoadLimitsAndInteractorReturnSuccessWithThreeTypesOfLimits_thenLimitsOnlyHaveTypePosDaily() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        dummyInteractor.limitsBO = LimitsGenerator.getThreeLimits()
        
        // when
        sut.loadLimits(cardBO: cardBO)
        
        // then
        XCTAssert(sut.limitsBO?.limits.count == 1)
        XCTAssert(sut.limitsBO?.limits[0].type == .pos_daily)
    }
    
    func test_givenCardBO_whenICallLoadLimitsAndInteractorReturns204_thenICallShowNoContentView() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        dummyInteractor.responseStatus = ConstantsHTTPCodes.NO_CONTENT
        
        // when
        sut.loadLimits(cardBO: cardBO)
        
        // then
        XCTAssert(dummyView.isCalledShowNoContentView == true)
    }
    
    func test_givenCardBO_whenICallLoadLimitsAndInteractorReturns200AndLimitsEmpty_thenICallShowNoContentView() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        dummyInteractor.limitsBO = LimitsGenerator.getThreeLimits()
        dummyInteractor.limitsBO?.limits = []
        
        // when
        sut.loadLimits(cardBO: cardBO)
        
        // then
        XCTAssert(dummyView.isCalledShowNoContentView == true)
    }
    
    func test_givenCardBO_whenICallLoadLimitsAndInteractorReturns200_thenICallShowLimits() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        dummyInteractor.responseStatus = ConstantsHTTPCodes.STATUS_OK
        
        // when
        sut.loadLimits(cardBO: cardBO)
        
        // then
        XCTAssert(dummyView.isCalledShowLimits == true)
    }
    
    func test_givenCardBO_whenICallLoadLimitsAndInteractorReturns200_thenICallShowLimitsWithDisplayDataWithSameNumberOfItemsAsLimitsReceived() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        dummyInteractor.responseStatus = ConstantsHTTPCodes.STATUS_OK
        dummyInteractor.limitsBO = LimitsGenerator.getThreeLimits()
        
        // when
        sut.loadLimits(cardBO: cardBO)
        
        // then
        XCTAssert(dummyView.limitsDisplayDataSent!.items.count == dummyInteractor.limitsBO!.limits.count)
    }
    
    func test_givenCardBO_whenICallLoadLimitsAndInteractorReturns200_thenICallShowLimitsAndDisplayDataMatch() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        let limitsBO = LimitsGenerator.getThreeLimits()
        dummyInteractor.responseStatus = ConstantsHTTPCodes.STATUS_OK
        dummyInteractor.limitsBO = limitsBO
        let limitsBOexpected = limitsBO
        limitsBOexpected.limits = limitsBOexpected.limits.filter { $0.type == LimitType.pos_daily }
        let limitsDisplayData = LimitsDisplayData(withLimits: limitsBOexpected)
        
        // when
        sut.loadLimits(cardBO: cardBO)
        
        // then
        XCTAssert(dummyView.limitsDisplayDataSent == limitsDisplayData)
    }
    
    // MARK: - LoadLimits - Interactor Return Error
    
    func test_givenCardBO_whenICallLoadLimitsAndInteractorReturnsError_thenICallHideSkeleton() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        dummyInteractor.forceError = true
        
        // when
        sut.loadLimits(cardBO: cardBO)
        
        // then
        XCTAssert(dummyView.isCalledHideSkeleton == true)
    }
    
    func test_givenCardBO_whenICallLoadLimitsAndInteractorReturnsError_thenICallShowErrorView() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        dummyInteractor.forceError = true
        
        // when
        sut.loadLimits(cardBO: cardBO)
        
        // then
        XCTAssert(dummyView.isCalledShowErrorView == true)
    }
    
    func test_givenViewDidLoadFalseAndCardBO_whenICallViewWillAppearAndInteractorReturnsError_thenErrorBOShouldBeFilled() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        dummyInteractor.forceError = true
        
        // when
        sut.loadLimits(cardBO: cardBO)
        
        // then
        XCTAssert(sut.errorBO != nil)
    }
    
    // MARK: - CreateSuccesfulToast

    func test_givenLimitBO_whenICallViewDidAppear_thenICallViewShowSuccessfulToast() {
        
        let limits = LimitsGenerator.getThreeLimits()
        let limitBO = limits.limits[0]
        
        let amountFormatter = AmountFormatter(codeCurrency: limitBO.amountLimits![0]!.currency)
        let amountFormatted = amountFormatter.format(amount: limitBO.amountLimits![0]!.amount)
        
        // given
        let display = LimitsDisplayData(withLimits: limits)
        
        // when
        sut.createSuccessfulToast(limitsDisplayData: display, amountFormatted: amountFormatted)
        
        // then
        XCTAssert(dummyView.isCalledShowSuccessfulToast == true)
    }
    
    func test_givenLimitsDisplayDataAndLimitEditedNil_whenICallCreateSuccesful_thenICallShowSuccessfulToastAndTextMatch() {
        
        let limits = LimitsGenerator.getThreeLimits()
        let limitBO = limits.limits[0]
        
        let amountFormatter = AmountFormatter(codeCurrency: limitBO.amountLimits![0]!.currency)
        let amountFormatted = amountFormatter.format(amount: limitBO.amountLimits![0]!.amount)
        
        // given
        let display = LimitsDisplayData(withLimits: limits)
        sut.limitEdited = nil
        
        // when
        sut.createSuccessfulToast(limitsDisplayData: display, amountFormatted: amountFormatted)
        
        // then
        XCTAssert(dummyView.toastTextSent == Localizables.cards.key_card_limit_you_text + " " + Localizables.cards.key_card_limit_daily_shopping_2 + " " + Localizables.cards.key_card_limit_is_text + " " + amountFormatted + ".")
    }
    
    func test_givenLimitsDisplayDataAndLimitEditedWithTypeAtmDaily_whenICallCreateSuccesful_thenICallShowSuccessfulToastAndTextMatch() {
        
        let limits = LimitsGenerator.getThreeLimits()
        let limitBO = limits.limits[0]
        
        let amountFormatter = AmountFormatter(codeCurrency: limitBO.amountLimits![0]!.currency)
        let amountFormatted = amountFormatter.format(amount: limitBO.amountLimits![0]!.amount)
        
        // given
        let display = LimitsDisplayData(withLimits: limits)
        sut.limitEdited = limitBO
        sut.limitEdited?.type = .atm_daily
        
        // when
        sut.createSuccessfulToast(limitsDisplayData: display, amountFormatted: amountFormatted)
        
        // then
        XCTAssert(dummyView.toastTextSent == String(format: Localizables.cards.key_card_limit_atm_daily_message_success, amountFormatted))
    }
    
    func test_givenLimitsDisplayDataAndLimitEditedWithTypePosDaily_whenICallCreateSuccesful_thenICallShowSuccessfulToastAndTextMatch() {
        
        let limits = LimitsGenerator.getThreeLimits()
        let limitBO = limits.limits[0]
        
        let amountFormatter = AmountFormatter(codeCurrency: limitBO.amountLimits![0]!.currency)
        let amountFormatted = amountFormatter.format(amount: limitBO.amountLimits![0]!.amount)
        
        // given
        let display = LimitsDisplayData(withLimits: limits)
        sut.limitEdited = limitBO
        sut.limitEdited?.type = .pos_daily
        
        // when
        sut.createSuccessfulToast(limitsDisplayData: display, amountFormatted: amountFormatted)
        
        // then
        XCTAssert(dummyView.toastTextSent == String(format: Localizables.cards.key_card_limit_pos_daily_message_success, amountFormatted))
    }
    
    func test_givenLimitsDisplayDataAndLimitEditedWithTypeOfficeDaily_whenICallCreateSuccesful_thenICallShowSuccessfulToastAndTextMatch() {
        
        let limits = LimitsGenerator.getThreeLimits()
        let limitBO = limits.limits[0]
        
        let amountFormatter = AmountFormatter(codeCurrency: limitBO.amountLimits![0]!.currency)
        let amountFormatted = amountFormatter.format(amount: limitBO.amountLimits![0]!.amount)
        
        // given
        let display = LimitsDisplayData(withLimits: limits)
        sut.limitEdited = limitBO
        sut.limitEdited?.type = .office_daily
        
        // when
        sut.createSuccessfulToast(limitsDisplayData: display, amountFormatted: amountFormatted)
        
        // then
        XCTAssert(dummyView.toastTextSent == String(format: Localizables.cards.key_card_limit_office_daily_message_success, amountFormatted))
    }
    
    func test_givenLimitsDisplayDataAndLimitEditedWithTypeTransactionDaily_whenICallCreateSuccesful_thenICallShowSuccessfulToastAndTextMatch() {
        
        let limits = LimitsGenerator.getThreeLimits()
        let limitBO = limits.limits[0]
        
        let amountFormatter = AmountFormatter(codeCurrency: limitBO.amountLimits![0]!.currency)
        let amountFormatted = amountFormatter.format(amount: limitBO.amountLimits![0]!.amount)
        
        // given
        let display = LimitsDisplayData(withLimits: limits)
        sut.limitEdited = limitBO
        sut.limitEdited?.type = .transaction_daily
        
        // when
        sut.createSuccessfulToast(limitsDisplayData: display, amountFormatted: amountFormatted)
        
        // then
        XCTAssert(dummyView.toastTextSent == String(format: Localizables.cards.key_card_limit_transaction_daily_message_success, amountFormatted))
    }
    
    func test_givenLimitsDisplayDataAndLimitEditedWithTypeGrantedCredit_whenICallCreateSuccesful_thenICallShowSuccessfulToastAndTextMatch() {
        
        let limits = LimitsGenerator.getThreeLimits()
        let limitBO = limits.limits[0]
        
        let amountFormatter = AmountFormatter(codeCurrency: limitBO.amountLimits![0]!.currency)
        let amountFormatted = amountFormatter.format(amount: limitBO.amountLimits![0]!.amount)
        
        // given
        let display = LimitsDisplayData(withLimits: limits)
        sut.limitEdited = limitBO
        sut.limitEdited?.type = .granted_credit
        
        // when
        sut.createSuccessfulToast(limitsDisplayData: display, amountFormatted: amountFormatted)
        
        // then
        XCTAssert(dummyView.toastTextSent == Localizables.cards.key_card_limit_you_text + " " + Localizables.cards.key_card_limit_daily_shopping_2 + " " + Localizables.cards.key_card_limit_is_text + " " + amountFormatted + ".")
    }
    
    // MARK: - Setup Dummys
    class LimitsViewADummy: LimitsViewProtocol {
        
        var isCalledShowSuccessfulToast = false
        var isCalledShowSkeleton = false
        var isCalledHideSkeleton = false
        var isCalledShowLimits = false
        var limitsDisplayDataSent: LimitsDisplayData?
        var isCalledShowErrorView = false
        var isCalledShowNoContentView = false

        var toastTextSent = ""

        func showError(error: ModelBO) {
        }
        
        func changeColorEditTexts() {
        }
        
        func sendScreenOmniture(withTitleId titleId: String) {
        }
        
        func sendUpdateLimitSuccessfulOmniture(withLimitName limitName: String) {
        }
        
        //--------------------
        
        func showLimits(withDisplayData displayData: LimitsDisplayData) {
            
            isCalledShowLimits = true
            limitsDisplayDataSent = displayData
        }
        
        func showSkeleton() {
            
            isCalledShowSkeleton = true
        }
        
        func hideSkeleton() {
            
            isCalledHideSkeleton = true
        }
        
        func showErrorView() {
            
            isCalledShowErrorView = true
        }
        
        func showNoContentView() {
            
            isCalledShowNoContentView = true
        }
        
        func showSuccessfulToast(withText text: String) {
            
            isCalledShowSuccessfulToast = true
            toastTextSent = text
        }
    }
    
    class DummyLimitsInteractorA: LimitsInteractorProtocol {
        
        var isCalledProvideLimits = false
        
        var forceError = false
        var responseStatus = 200
        var cardIdSent = "sentCardId"
        
        var limitsBO: LimitsBO?
        var errorBO: ErrorBO?

        func provideLimits(withCardId cardId: String) -> Observable<ModelBO> {
            
            isCalledProvideLimits = true
            cardIdSent = cardId

            if !forceError {
                
                if limitsBO == nil {
                    limitsBO = LimitsGenerator.getThreeLimits()
                }
                
                limitsBO?.status = responseStatus
                
                return Observable <ModelBO>.just(limitsBO!)
            } else {
                
                if errorBO == nil {
                    errorBO = ErrorBO(error: ErrorEntity(message: "Error"))
                    errorBO!.isErrorShown = false
                }
                
                return Observable.error(ServiceError.GenericErrorBO(error: errorBO!))
                
            }
        }
    }
}
