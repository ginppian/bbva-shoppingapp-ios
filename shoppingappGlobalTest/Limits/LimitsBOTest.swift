//
//  LimitsBOTest.swift
//  shoppingapp
//
//  Created by Marcos on 13/3/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class LimitsBOTest: XCTestCase {

    func test_givenLimits_whenICallReplaceLimitWithLimitEdited_thenLimitsShouldBeCorrect() {

        //given
        let limits = LimitsGenerator.getThreeLimits()
        let limitBO0 = LimitsGenerator.getThreeLimits().limits[0]
        let limitBO1 = LimitsGenerator.getThreeLimits().limits[1]
        let limitBO2 = LimitsGenerator.getThreeLimits().limits[2]

        //when
        limitBO1.name = "limit edited"
        limits.replaceLimit(limitBO1)

        //then
        XCTAssert(limits.limits[0] == limitBO0)
        XCTAssert(limits.limits[1] == limitBO1)
        XCTAssert(limits.limits[2] == limitBO2)
        XCTAssert(limits.limits[1].name == "limit edited")

    }

}
