//
//  LimitsDisplayDataTests.swift
//  shoppingapp
//
//  Created by jesus.martinez on 29/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class LimitsDisplayDataTests: XCTestCase {

    // MARK: vars
    var dummySessionManager: DummySessionManager!
    
    // MARK: setup
    override func setUp() {
        super.setUp()
        
        DummySessionManager.setupMock()
        dummySessionManager = (SessionDataManager.sessionDataInstance() as! DummySessionManager)
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    // MARK: - LimitsDisplayData

    func test_givenLimitsBO_whenICallInit_thenIHaveTheSameNumberOfItemsThanLimits() {

        // given
        let limits = LimitsGenerator.getThreeLimits()

        // when
        let displayData = LimitsDisplayData(withLimits: limits)

        // then
        XCTAssert(displayData.items.count == limits.limits.count)

    }

    // MARK: - LimitDisplayData

    func test_givenLimitBOWithTypeCorrect_whenICallGenerateDisplayDataFromLimit_thenTitleMatchWithLimitName() {

        // given
        let limit = LimitsGenerator.getThreeLimits().limits[0]
        limit.type = .atm_daily

        // when
        let displayData = LimitDisplayData.generateDisplayData(fromLimit: limit)

        // then
        XCTAssert(displayData.title == limit.type.title)

    }

    func test_givenLimitBOWithTypeUnknowm_whenICallGenerateDisplayDataFromLimit_thenTitleMatchWithLimitName() {

        // given
        let limit = LimitsGenerator.getThreeLimits().limits[0]
        limit.type = .unknowm

        // when
        let displayData = LimitDisplayData.generateDisplayData(fromLimit: limit)

        // then
        XCTAssert(displayData.title == limit.name)

    }
    
    func test_givenLimitBOWithTypeCorrect_whenICallGenerateDisplayDataFromLimit_thenTagNameWithLimitTagName() {
        
        // given
        let limit = LimitsGenerator.getThreeLimits().limits[0]
        limit.type = .atm_daily
        
        // when
        let displayData = LimitDisplayData.generateDisplayData(fromLimit: limit)
        
        // then
        XCTAssert(displayData.tagName == limit.type.tagName)
        
    }

    func test_givenLimitBOWithLimitType_whenICallGenerateDisplayDataFromLimit_thenConsumedLimitTitleMatchWithDisposedLimitTitle() {

        // given
        let limit = LimitsGenerator.getThreeLimits().limits[0]
        limit.type = .atm_daily

        // when
        let displayData = LimitDisplayData.generateDisplayData(fromLimit: limit)

        // then
        XCTAssert(displayData.consumedLimitTitle == limit.type.disposedLimitTitle)

    }

    func test_givenLimitBOWithLimitType_whenICallGenerateDisplayDataFromLimit_thenCurrentLimiTitleMatchWithAmountsLimitTitle() {

        // given
        let limit = LimitsGenerator.getThreeLimits().limits[0]
        limit.type = .atm_daily

        // when
        let displayData = LimitDisplayData.generateDisplayData(fromLimit: limit)

        // then
        XCTAssert(displayData.currentLimiTitle == limit.type.amountsLimitTitle)

    }

    func test_givenLimitBOWithLimitType_whenICallGenerateDisplayDataFromLimit_thenNotEditableTitleMatchWithNotEditableTitle() {

        // given
        let limit = LimitsGenerator.getThreeLimits().limits[0]
        limit.type = .atm_daily

        // when
        let displayData = LimitDisplayData.generateDisplayData(fromLimit: limit)

        // then
        XCTAssert(displayData.notEditableTitle == limit.type.notEditableTitle)

    }

    func test_givenLimitBO_whenICallGenerateDisplayDataFromLimit_thenCurrentLimiShouldMatch() {

        // given
        let limit = LimitsGenerator.getThreeLimits().limits[0]
        let formatter = AmountFormatter(codeCurrency: limit.amountLimits![0]!.currency)

        // when
        let displayData = LimitDisplayData.generateDisplayData(fromLimit: limit)

        // then
        XCTAssert(displayData.currentLimitAmount == formatter.attributtedText(forAmount: limit.amountLimits![0]!.amount, bigFontSize: 16, smallFontSize: 12))

    }

    func test_givenLimitBO_whenICallGenerateDisplayDataFromLimit_thenConsumedOverMaximumPercentageShouldMatch() {

        // given
        let limit = LimitsGenerator.getThreeLimits().limits[0]

        // when
        let displayData = LimitDisplayData.generateDisplayData(fromLimit: limit)

        // then
        XCTAssert(displayData.consumedPercentage == limit.disposedLimits![0]!.amount.floatValue / (limit.allowedInterval?.maximumAmount?.amount.floatValue)!)

    }

    func test_givenLimitBOAndConsumedAmountValueNil_whenICallGenerateDisplayDataFromLimit_thenConsumedOverMaximumPercentageShouldBeZero() {

        // given
        let limit = LimitsGenerator.getThreeLimits().limits[0]
        limit.disposedLimits = nil

        // when
        let displayData = LimitDisplayData.generateDisplayData(fromLimit: limit)

        // then
        XCTAssert(displayData.consumedPercentage == 0)

    }

    func test_givenLimitBOAndConsumedAmountMinorThanMinimumLimit_whenICallGenerateDisplayDataFromLimit_thenConsumedOverMaximumPercentageShouldBeZero() {

        // given
        let limit = LimitsGenerator.getThreeLimits().limits[0]
        limit.disposedLimits![0]!.amount = 100
        limit.allowedInterval?.minimumAmount?.amount = 1000

        // when
        let displayData = LimitDisplayData.generateDisplayData(fromLimit: limit)

        // then
        XCTAssert(displayData.consumedPercentage == 0)

    }

    func test_givenLimitBOAndMaximumLimitAmountValueNil_whenICallGenerateDisplayDataFromLimit_thenConsumedOverMaximumPercentageShouldBeMaxValue() {

        // given
        let limit = LimitsGenerator.getThreeLimits().limits[0]
        limit.allowedInterval?.maximumAmount = nil

        // when
        let displayData = LimitDisplayData.generateDisplayData(fromLimit: limit)

        // then
        XCTAssert(displayData.consumedPercentage == 100)

    }

    func test_givenLimitBO_whenICallGenerateDisplayDataFromLimit_thenCurrentLimitOverMaximumPercentageShouldMatch() {

        // given
        let limit = LimitsGenerator.getThreeLimits().limits[0]

        // when
        let displayData = LimitDisplayData.generateDisplayData(fromLimit: limit)

        // then
        XCTAssert(displayData.currentLimitPercentage == limit.amountLimits![0]!.amount.floatValue / (limit.allowedInterval?.maximumAmount?.amount.floatValue)!)

    }

    func test_givenLimitBO_whenICallGenerateDisplayDataFromLimit_thenCurrentLimitOverMaximumPercentageShouldBeZeroAndEditDisabled() {

        // given
        let limit = LimitsGenerator.getThreeLimits().limits[0]
        limit.allowedInterval?.maximumAmount = nil

        // when
        let displayData = LimitDisplayData.generateDisplayData(fromLimit: limit)

        // then
        XCTAssert(displayData.currentLimitPercentage == 0)

    }

    func test_givenLimitBOAndActualLimitMinorThanMinimumLimit_whenICallGenerateDisplayDataFromLimit_thenCurrentLimitOverMaximumPercentageShouldBeZero() {

        // given
        let limit = LimitsGenerator.getThreeLimits().limits[0]
        limit.amountLimits![0]!.amount = 200
        limit.allowedInterval?.minimumAmount?.amount = 1000

        // when
        let displayData = LimitDisplayData.generateDisplayData(fromLimit: limit)

        // then
        XCTAssert(displayData.currentLimitPercentage == 0)

    }

    func test_givenLimitBO_whenICallGenerateDisplayDataFromLimit_thenDisplayDataMatchToFormatterValues() {

        // given
        let limit = LimitsGenerator.getThreeLimits().limits[0]
        let formatter = AmountFormatter(codeCurrency: limit.amountLimits![0]!.currency)

        // when
        let displayData = LimitDisplayData.generateDisplayData(fromLimit: limit)

        // then
        XCTAssert(displayData.consumedAmount == formatter.attributtedText(forAmount: limit.disposedLimits![0]!.amount, bigFontSize: 16, smallFontSize: 12))
        XCTAssert(displayData.minimumLimitAmount == formatter.attributtedText(forAmount: (limit.allowedInterval?.minimumAmount?.amount)!, bigFontSize: 12, smallFontSize: 10))
        XCTAssert(displayData.maximumLimitAmount == formatter.attributtedText(forAmount: (limit.allowedInterval?.maximumAmount?.amount)!, bigFontSize: 12, smallFontSize: 10))

    }

    func test_givenLimitsBOWithConsumedLimitSmallerThanMaximumAmmount_whenICallGenerateDisplayDataFromLimit_thenEditShouldBeEnabled() {

        //given
        let limit = LimitsGenerator.getThreeLimits().limits[0]
        limit.disposedLimits![0]!.amount = 2000
        limit.allowedInterval?.maximumAmount?.amount = 5000
        //when
        let displayData = LimitDisplayData.generateDisplayData(fromLimit: limit)

        //then
        XCTAssert(displayData.allowsEdit == true)

    }

    func test_givenLimitsBOWithIsEditableTrue_whenICallGenerateDisplayDataFromLimit_thenHideEditableIsFalse() {

        //given
        let limit = LimitsGenerator.getThreeLimits().limits[0]
        limit.isEditable = true
        //when
        let displayData = LimitDisplayData.generateDisplayData(fromLimit: limit)

        //then
        XCTAssert(displayData.hideEditable == false)

    }

    func test_givenLimitsBOWithIsEditableFalse_whenICallGenerateDisplayDataFromLimit_thenHideEditableIsTrue() {

        //given
        let limit = LimitsGenerator.getThreeLimits().limits[0]
        limit.isEditable = false
        //when
        let displayData = LimitDisplayData.generateDisplayData(fromLimit: limit)

        //then
        XCTAssert(displayData.hideEditable == true)

    }

    func test_givenLimitsBOWithIsEditableTrue_whenICallGenerateDisplayDataFromLimit_thenInfoLimitMatch() {

        //given
        let limit = LimitsGenerator.getThreeLimits().limits[0]
        limit.isEditable = true

        //when
        let displayData = LimitDisplayData.generateDisplayData(fromLimit: limit)

        //then
        XCTAssert(displayData.infoLimit == limit.infoMessage())

    }

    // MARK: - LimitDisplayData incorrect values

    func test_givenLimitsBOWithoutAmountLimits_whenICallGenerateDisplayDataFromLimit_thenInfoShouldBeAdaptedAndEditDisabled() {

        //given
        let limit = LimitsGenerator.getThreeLimits().limits[0]
        limit.amountLimits = nil
        let currency = Currencies.currency(forCode: dummySessionManager.mainCurrency)

        //when
        let displayData = LimitDisplayData.generateDisplayData(fromLimit: limit)

        //then
        XCTAssert(displayData.currentLimitAmount.string == (currency.position == .left ? currency.symbol + " -" :  "- " + currency.symbol) )
        XCTAssert(displayData.allowsEdit == false)

    }

    func test_givenLimitBOWithoutNameAndTypeUnknowm_whenICallGenerateDisplayDataFromLimit_thenTitleShouldBeAdaptedAndEditDisabled() {

        //given
        let limit = LimitsGenerator.getThreeLimits().limits[0]
        limit.type = .unknowm
        limit.name = nil

        //when
        let displayData = LimitDisplayData.generateDisplayData(fromLimit: limit)

        //then
        XCTAssert(displayData.title == Localizables.cards.key_cards_limit_text + " -" )
        XCTAssert(displayData.allowsEdit == false)

    }

    func test_givenLimitBOWithoutMaximun_whenICallGenerateDisplayDataFromLimit_thenMaximumShouldBeAdaptedAndEditDisabled() {

        //given
        let limit = LimitsGenerator.getThreeLimits().limits[0]
        limit.allowedInterval?.maximumAmount = nil
        let currency = Currencies.currency(forCode: dummySessionManager.mainCurrency)

        //when
        let displayData = LimitDisplayData.generateDisplayData(fromLimit: limit)

        //then
        XCTAssert(displayData.maximumLimitAmount.string == (currency.position == .left ? currency.symbol + " -" :  "- " + currency.symbol) )
        XCTAssert(displayData.allowsEdit == false)

    }

    func test_givenLimitBOWithoutMinimum_whenICallGenerateDisplayDataFromLimit_thenMinimumShouldBeAdaptedAndEditDisabled() {

        //given
        let limit = LimitsGenerator.getThreeLimits().limits[0]
        limit.allowedInterval?.minimumAmount = nil
        let currency = Currencies.currency(forCode: dummySessionManager.mainCurrency)

        //when
        let displayData = LimitDisplayData.generateDisplayData(fromLimit: limit)

        //then
        XCTAssert(displayData.minimumLimitAmount.string == (currency.position == .left ? currency.symbol + " -" :  "- " + currency.symbol) )
        XCTAssert(displayData.allowsEdit == false)

    }

    func test_givenLimitBOWithoutDisposed_whenICallGenerateDisplayDataFromLimit_thenConsumedShouldBeAdaptedAndEditDisabled() {

        //given
        let limit = LimitsGenerator.getThreeLimits().limits[0]
        limit.disposedLimits = nil
        let currency = Currencies.currency(forCode: dummySessionManager.mainCurrency)

        //when
        let displayData = LimitDisplayData.generateDisplayData(fromLimit: limit)

        //then
        XCTAssert(displayData.consumedAmount.string == (currency.position == .left ? currency.symbol + " -" :  "- " + currency.symbol) )
        XCTAssert(displayData.allowsEdit == false)

    }

    func test_givenLimitBOWithoutCurrency_whenICallGenerateDisplayDataFromLimit_thenCurrentLimitShouldBeAdaptedAndEditDisabled() {

        //given
        let limit = LimitsGenerator.getThreeLimits().limits[0]
        limit.amountLimits![0] = nil
        let currency = Currencies.currency(forCode: dummySessionManager.mainCurrency)

        //when
        let displayData = LimitDisplayData.generateDisplayData(fromLimit: limit)

        //then
        XCTAssert(displayData.currentLimitAmount.string == (currency.position == .left ? currency.symbol + " -" :  "- " + currency.symbol) )
        XCTAssert(displayData.allowsEdit == false)

    }

    func test_givenLimitsBOWithoutMinimumAmount_whenICallGenerateDisplayDataFromLimit_thenInfoLimitIsEmpty() {

        //given
        let limit = LimitsGenerator.getThreeLimits().limits[0]
        limit.allowedInterval?.minimumAmount = nil

        //when
        let displayData = LimitDisplayData.generateDisplayData(fromLimit: limit)

        //then
        XCTAssert(displayData.infoLimit.isEmpty == true)

    }

    func test_givenLimitsBOWithoutMaximumAmount_whenICallGenerateDisplayDataFromLimit_thenInfoLimitIsEmpty() {

        //given
        let limit = LimitsGenerator.getThreeLimits().limits[0]
        limit.allowedInterval?.maximumAmount = nil

        //when
        let displayData = LimitDisplayData.generateDisplayData(fromLimit: limit)

        //then
        XCTAssert(displayData.infoLimit.isEmpty == true)

    }

    func test_givenLimitsBOWithoutDisposed_whenICallGenerateDisplayDataFromLimit_thenInfoLimitIsEmpty() {

        //given
        let limit = LimitsGenerator.getThreeLimits().limits[0]
        limit.disposedLimits = nil

        //when
        let displayData = LimitDisplayData.generateDisplayData(fromLimit: limit)

        //then
        XCTAssert(displayData.infoLimit.isEmpty == true)

    }

}
