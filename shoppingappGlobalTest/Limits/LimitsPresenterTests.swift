//
//  LimitsPresenterTests.swift
//  shoppingapp
//
//  Created by jesus.martinez on 25/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest
import RxSwift
import CellsNativeComponents

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class LimitsPresenterTests: XCTestCase {

    var sut: LimitsPresenter<LimitsViewDummy>!
    var dummyBusManager: LimitsDummyBusManager!

    override func setUp() {
        super.setUp()
        dummyBusManager = LimitsDummyBusManager()
        sut = LimitsPresenter<LimitsViewDummy>(busManager: dummyBusManager)
    }

    // MARK: - setModel

    func test_givenCardBO_whenICallSetModel_thenCardBOShouldBeFilled() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]

        // when
        sut.setModel(model: cardBO)

        // then
        XCTAssert(sut.cardBO != nil)

    }

    func test_givenCardBO_whenICallSetModel_thenCardBOShouldMatch() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]

        // when
        sut.setModel(model: cardBO)

        // then
        XCTAssert(sut.cardBO === cardBO)

    }

    func test_givenLimitTransport_whenICallSetModel_thenLimitEditedShouldBeFilled() {

        // given
        let limitBO = LimitsGenerator.getThreeLimits().limits[0]
        let limitTransport = LimitTransport(withLimit: limitBO, andCardId: "1")

        // when
        sut.setModel(model: limitTransport)

        // then
        XCTAssert(sut.limitEdited != nil)

    }

    func test_givenLimitTransport_whenICallSetModel_thenLimitEditedShouldMatchWithLimitOfTransport() {

        // given
        let limitBO = LimitsGenerator.getThreeLimits().limits[0]
        let limitTransport = LimitTransport(withLimit: limitBO, andCardId: "1")

        // when
        sut.setModel(model: limitTransport)

        // then
        XCTAssert(sut.limitEdited == limitBO)

    }

    func test_givenLimitTransportAndLimitsBO_whenICallSetModel_thenShouldCallLimitsBOReplaceLimit() {

        // given
        let limitBO = LimitsGenerator.getThreeLimits().limits[0]
        let limitTransport = LimitTransport(withLimit: limitBO, andCardId: "1")

        let dummyLimistBO = DummyLimitsBO(limitsEntity: LimitsGenerator.getThreeLimitsEntity())
        sut.limitsBO = dummyLimistBO

        // when
        sut.setModel(model: limitTransport)

        // then
        XCTAssert(dummyLimistBO.isCalledReplaceLimit == true)

    }

    func test_givenLimitTransportAndLimitsBO_whenICallSetModel_thenShouldCallLimitsBOReplaceLimitWithLimitBOFromTransport() {

        // given
        let limitBO = LimitsGenerator.getThreeLimits().limits[0]
        let limitTransport = LimitTransport(withLimit: limitBO, andCardId: "1")

        let dummyLimistBO = DummyLimitsBO(limitsEntity: LimitsGenerator.getThreeLimitsEntity())
        sut.limitsBO = dummyLimistBO

        // when
        sut.setModel(model: limitTransport)

        // then
        XCTAssert(dummyLimistBO.limitToReplace == limitBO)

    }

    // MARK: - viewWillAppear

    func test_givenLimitEditedBO_whenICallViewWillAppear_thenICallViewSendOmnitureLimitUpdateSuccessful() {

        let dummyview = LimitsViewDummy()
        sut.view = dummyview

        // given
        sut.cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        let limitBO = LimitsGenerator.getThreeLimits().limits[0]
        sut.limitEdited = limitBO
        sut.limitsBO = LimitsGenerator.getThreeLimits()

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyview.isCalledSendUpdateLimitSuccessfulOmniture == true)

    }

    func test_givenLimitEditedBO_whenICallViewWillAppear_thenICallViewSendOmnitureLimitUpdateSuccessfulWithTagName() {

        let dummyview = LimitsViewDummy()
        sut.view = dummyview

        // given
        sut.cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        let limitBO = LimitsGenerator.getThreeLimits().limits[0]
        sut.limitEdited = limitBO
        sut.limitsBO = LimitsGenerator.getThreeLimits()
        let name = limitBO.type.tagName

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyview.sentLimitName == name)

    }

    func test_givenCardBO_whenICallViewWillAppear_thenICallViewSendOmniture() {

        let dummyiew = LimitsViewDummy()
        sut.view = dummyiew

        // given
        sut.cardBO = CardsGenerator.getCardBOWithAlias().cards[0]

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyiew.isSendScreenOmniture == true)

    }

    func test_givenCardBO_whenICallViewWillAppear_thenICallViewSendOmnitureWithTitleId() {

        let dummyiew = LimitsViewDummy()
        sut.view = dummyiew

        // given
        sut.cardBO = CardsGenerator.getCardBOWithAlias().cards[0]

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyiew.sentTitleId == sut.cardBO!.title?.id)

    }

    func test_givenCardBO_whenICallViewWillAppear_thenViewDidLoadShouldBeTrue() {

        // given
        sut.cardBO = CardsGenerator.getCardBOWithAlias().cards[0]

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(sut.viewDidLoad == true)

    }

    func test_givenViewDidLoadFalseAndCardBO_whenICallViewWillAppear_thenICallViewShowSkeleton() {

        let dummyInteractor = DummyLimitsInteractor()
        sut.interactor = dummyInteractor
        let dummyView = LimitsViewDummy()
        sut.view = dummyView

        //given
        sut.viewDidLoad = false
        sut.cardBO = CardsGenerator.getCardBOWithAlias().cards[0]

        //when
        sut.viewWillAppear()

        //then
        XCTAssert(dummyView.isCalledShowSkeleton == true)
    }

    func test_givenViewDidLoadFalseAndCardBO_whenICallViewWillAppear_thenICallInteractorProvideLimits() {

        let dummyInteractor = DummyLimitsInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.viewDidLoad = false
        sut.cardBO = CardsGenerator.getCardBOWithAlias().cards[0]

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyInteractor.isCalledProvideLimits == true)
    }

    func test_givenViewDidLoadTrueAndCardBO_whenICallViewWillAppear_thenIDoNotCallInteractorProvideLimits() {

        let dummyInteractor = DummyLimitsInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.viewDidLoad = true
        sut.cardBO = CardsGenerator.getCardBOWithAlias().cards[0]

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyInteractor.isCalledProvideLimits == false)
    }

    func test_givenViewDidLoadFalseAndCardBO_whenICallViewWillAppear_thenICallInteractorProvideLimitsWithCardId() {

        let dummyInteractor = DummyLimitsInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.viewDidLoad = false
        sut.cardBO = CardsGenerator.getCardBOWithAlias().cards[0]

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyInteractor.sentCardId == sut.cardBO?.cardId)
    }

    func test_givenViewDidLoadFalseAndCardBO_whenICallViewWillAppearAndInteractorReturnsError_thenICallViewShowErrorView() {

        let dummyInteractor = DummyLimitsInteractor()
        sut.interactor = dummyInteractor
        let dummyiew = LimitsViewDummy()
        sut.view = dummyiew
        dummyInteractor.forceError = true

        // given
        sut.cardBO = CardsGenerator.getCardBOWithAlias().cards[0]

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyiew.isCalledShowErrorView == true)
    }

    func test_givenViewDidLoadFalseAndCardBO_whenICallViewWillAppearAndInteractorReturnsError_thenICallViewHideSkeleton() {

        let dummyInteractor = DummyLimitsInteractor()
        sut.interactor = dummyInteractor
        let dummyiew = LimitsViewDummy()
        sut.view = dummyiew
        dummyInteractor.forceError = true

        // given
        sut.cardBO = CardsGenerator.getCardBOWithAlias().cards[0]

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyiew.isCalledHideSkeleton == true)
    }

    func test_givenViewDidLoadFalseAndCardBO_whenICallViewWillAppearAndInteractorReturnsError_thenErrorBOShouldBeFilled() {

        let dummyInteractor = DummyLimitsInteractor()
        sut.interactor = dummyInteractor
        dummyInteractor.forceError = true

        // given
        sut.viewDidLoad = false
        sut.cardBO = CardsGenerator.getCardBOWithAlias().cards[0]

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(sut.errorBO != nil)
    }

    func test_givenViewDidLoadFalseAndCardBO_whenICallViewWillAppearAndInteractorReturnsError_thenErrorBOShouldMatchWithInteractorReturned() {

        let dummyInteractor = DummyLimitsInteractor()
        sut.interactor = dummyInteractor
        dummyInteractor.forceError = true

        // given
        sut.viewDidLoad = false
        sut.cardBO = CardsGenerator.getCardBOWithAlias().cards[0]

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(sut.errorBO === dummyInteractor.errorBO)
    }

    func test_givenViewDidLoadFalseAndCardBO_whenICallViewWillAppearAndInteractorReturnSuccess_thenICallViewHideSkeleton() {

        let dummyInteractor = DummyLimitsInteractor()
        sut.interactor = dummyInteractor
        let dummyView = LimitsViewDummy()
        sut.view = dummyView

        //given
        sut.viewDidLoad = false
        sut.cardBO = CardsGenerator.getCardBOWithAlias().cards[0]

        //when
        sut.viewWillAppear()

        //then
        XCTAssert(dummyView.isCalledHideSkeleton == true)
    }

    func test_givenViewDidLoadFalseAndCardBO_whenICallViewWillAppearAndInteractorReturnsSuccess_thenLimitsBOShouldBeFilled() {

        let dummyInteractor = DummyLimitsInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.viewDidLoad = false
        sut.cardBO = CardsGenerator.getCardBOWithAlias().cards[0]

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(sut.limitsBO != nil)
    }

    func test_givenViewDidLoadFalseAndCardBO_whenICallViewWillAppearAndInteractorReturnsSuccess_thenLimitsBOShouldMatchWithInteractorReturned() {

        let dummyInteractor = DummyLimitsInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.viewDidLoad = false
        sut.cardBO = CardsGenerator.getCardBOWithAlias().cards[0]

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(sut.limitsBO! === dummyInteractor.limitsBO!)
    }

    func test_givenViewDidLoadFalseAndCardBO_whenICallViewWillAppearAndInteractorReturns200_thenICallViewShowLimits() {

        let dummyiew = LimitsViewDummy()
        sut.view = dummyiew

        let dummyInteractor = DummyLimitsInteractor()
        sut.interactor = dummyInteractor
        dummyInteractor.responseStatus = ConstantsHTTPCodes.STATUS_OK

        // given
        sut.viewDidLoad = false
        sut.cardBO = CardsGenerator.getCardBOWithAlias().cards[0]

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyiew.isCalledShowLimits == true)
    }

    func test_givenViewDidLoadFalseAndCardBO_whenICallViewWillAppearAndInteractorReturns204_thenICallViewShowNoContentView() {

        let dummyiew = LimitsViewDummy()
        sut.view = dummyiew

        let dummyInteractor = DummyLimitsInteractor()
        sut.interactor = dummyInteractor
        dummyInteractor.responseStatus = ConstantsHTTPCodes.NO_CONTENT

        // given
        sut.viewDidLoad = false
        sut.cardBO = CardsGenerator.getCardBOWithAlias().cards[0]

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyiew.isCalledShowNoContentView == true)
    }

    func test_givenViewDidLoadFalseAndCardBO_whenICallViewWillAppearAndInteractorReturns200_thenICallViewShowLimitsWithDisplayDataWithSameNumberOfItemsAsLimitsReceived() {

        let dummyiew = LimitsViewDummy()
        sut.view = dummyiew

        let dummyInteractor = DummyLimitsInteractor()
        sut.interactor = dummyInteractor
        dummyInteractor.responseStatus = ConstantsHTTPCodes.STATUS_OK
        dummyInteractor.limitsBO = LimitsGenerator.getThreeLimits()

        // given
        sut.viewDidLoad = false
        sut.cardBO = CardsGenerator.getCardBOWithAlias().cards[0]

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyiew.sentDisplayData != nil)
        XCTAssert(dummyiew.sentDisplayData!.items.count == dummyInteractor.limitsBO!.limits.count)

    }

    // MARK: - ViewDidAppear

    func test_givenLimitEditedBO_whenICallViewDidAppear_thenICallViewShowSuccessfulToast() {

        let dummyview = LimitsViewDummy()
        sut.view = dummyview

        // given
        sut.cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        let limitBO = LimitsGenerator.getThreeLimits().limits[0]
        sut.limitEdited = limitBO
        sut.limitsBO = LimitsGenerator.getThreeLimits()

        // when
        sut.viewDidAppear()

        // then
        XCTAssert(dummyview.isCalledShowSuccessfulToast == true)

    }

    func test_givenLimitEditedBO_whenICallViewDidAppear_thenICallViewShowSuccessfulToastWithCorrectText() {

        let dummyview = LimitsViewDummy()
        sut.view = dummyview

        // given
        sut.cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        let limits = LimitsGenerator.getThreeLimits()
        let limitBO = limits.limits[0]
        sut.limitsBO = limits
        sut.limitEdited = limitBO

        let amountFormatter = AmountFormatter(codeCurrency: limitBO.amountLimits![0]!.currency)
        let amountFormatted = amountFormatter.format(amount: limitBO.amountLimits![0]!.amount)

        let display = LimitsDisplayData(withLimits: limits)

        // when
        sut.viewDidAppear()

        // then
        XCTAssert(dummyview.sentToastText == Localizables.cards.key_card_limit_you_text + " " + display.items[0].title.lowercased() + " " + Localizables.cards.key_card_limit_is_text + " " + amountFormatted + ".")

    }

    func test_givenLimitEditedBO_whenICallViewDidAppear_thenICallViewShowLimits() {

        let dummyView = LimitsViewDummy()
        sut.view = dummyView

        //given
        sut.cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        let limits = LimitsGenerator.getThreeLimits()
        let limitBO = limits.limits[0]
        sut.limitsBO = limits
        sut.limitEdited = limitBO

        //when
        sut.viewDidAppear()

        //then
        XCTAssert(dummyView.isCalledShowLimits == true)
    }

    func test_givenLimitEditedBO_whenICallViewDidAppear_thenICallViewShowLimitsWithCorrectDisplay() {

        let dummyView = LimitsViewDummy()
        sut.view = dummyView

        //given
        sut.cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        let limits = LimitsGenerator.getThreeLimits()
        let limitBO = limits.limits[0]
        sut.limitsBO = limits
        sut.limitEdited = limitBO

        let display = LimitsDisplayData(withLimits: limits)
        display.items[0].animateConsumedLimit = true

        //when
        sut.viewDidAppear()

        //then
        XCTAssert(dummyView.sentDisplayData! == display)
    }

    func test_givenLimitEditedBO_whenICallViewDidAppear_thenDisplayItemAnimateConsumedLimitShouldBeTrue() {

        let dummyView = LimitsViewDummy()
        sut.view = dummyView

        //given
        sut.cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        let limits = LimitsGenerator.getThreeLimits()
        let limitBO = limits.limits[0]
        sut.limitsBO = limits
        sut.limitEdited = limitBO

        //when
        sut.viewDidAppear()

        //then
        XCTAssert(dummyView.sentDisplayData?.items[0].animateConsumedLimit == true)
    }

    func test_givenAny_whenICallViewDidAppear_thenLimitEditedShoulBeNil() {

        //given

        //when
        sut.viewDidAppear()

        //then
        XCTAssert(sut.limitEdited == nil)

    }

    // MARK: - RetryButton

    func test_givenCardBO_whenICallRetryButton_thenICallViewShowSkeleton() {

        let dummyInteractor = DummyLimitsInteractor()
        sut.interactor = dummyInteractor
        let dummyView = LimitsViewDummy()
        sut.view = dummyView

        //given
        sut.cardBO = CardsGenerator.getCardBOWithAlias().cards[0]

        //when
        sut.retryButtonPressed()

        //then
        XCTAssert(dummyView.isCalledShowSkeleton == true)
    }

    func test_givenCardBO_whenICallRetryButton_thenICallInteractorProvideLimits() {

        let dummyInteractor = DummyLimitsInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.cardBO = CardsGenerator.getCardBOWithAlias().cards[0]

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(dummyInteractor.isCalledProvideLimits == true)
    }

    func test_givenCardBO_whenICallRetryButton_thenICallInteractorProvideLimitsWithCardId() {

        let dummyInteractor = DummyLimitsInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.cardBO = CardsGenerator.getCardBOWithAlias().cards[0]

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(dummyInteractor.sentCardId == sut.cardBO?.cardId)
    }

    func test_givenCardBO_whenICallRetryButtonAndInteractorReturnsSuccess_thenLimitsBOShouldBeFilled() {

        let dummyInteractor = DummyLimitsInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.cardBO = CardsGenerator.getCardBOWithAlias().cards[0]

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(sut.limitsBO != nil)
    }

    func test_givenCardBO_whenICallRetryButtonAndInteractorReturnsSuccess_thenLimitsBOShouldMatchWithInteractorReturned() {

        let dummyInteractor = DummyLimitsInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.cardBO = CardsGenerator.getCardBOWithAlias().cards[0]

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(sut.limitsBO! === dummyInteractor.limitsBO!)
    }

    func test_givenCardBO_whenICallRetryButtonAndInteractorReturns200_thenICallViewShowLimits() {

        let dummyiew = LimitsViewDummy()
        sut.view = dummyiew

        let dummyInteractor = DummyLimitsInteractor()
        sut.interactor = dummyInteractor
        dummyInteractor.responseStatus = ConstantsHTTPCodes.STATUS_OK

        // given
        sut.cardBO = CardsGenerator.getCardBOWithAlias().cards[0]

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(dummyiew.isCalledShowLimits == true)
    }

    func test_givenCardBO_whenICallRetryButtonAndInteractorReturns200_thenICallViewShowLimitsWithDisplayDataWithSameNumberOfItemsAsLimitsReceived() {

        let dummyiew = LimitsViewDummy()
        sut.view = dummyiew

        let dummyInteractor = DummyLimitsInteractor()
        sut.interactor = dummyInteractor
        dummyInteractor.responseStatus = ConstantsHTTPCodes.STATUS_OK
        dummyInteractor.limitsBO = LimitsGenerator.getThreeLimits()

        // given
        sut.cardBO = CardsGenerator.getCardBOWithAlias().cards[0]

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(dummyiew.sentDisplayData != nil)
        XCTAssert(dummyiew.sentDisplayData!.items.count == dummyInteractor.limitsBO!.limits.count)

    }

    func test_givendCardBO_whenICallRetryButtonAndInteractorReturns204_thenICallViewShowNoContentView() {

        let dummyiew = LimitsViewDummy()
        sut.view = dummyiew

        let dummyInteractor = DummyLimitsInteractor()
        sut.interactor = dummyInteractor
        dummyInteractor.responseStatus = ConstantsHTTPCodes.NO_CONTENT

        // given
        sut.viewDidLoad = false
        sut.cardBO = CardsGenerator.getCardBOWithAlias().cards[0]

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(dummyiew.isCalledShowNoContentView == true)
    }

    func test_givenViewDidLoadFalseAndCardBO_whenICallRetryButtonAndInteractorReturnsError_thenICallViewHideSkeleton() {

        let dummyInteractor = DummyLimitsInteractor()
        sut.interactor = dummyInteractor
        let dummyiew = LimitsViewDummy()
        sut.view = dummyiew
        dummyInteractor.forceError = true

        // given
        sut.cardBO = CardsGenerator.getCardBOWithAlias().cards[0]

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(dummyiew.isCalledHideSkeleton == true)
    }

    func test_givenCardBO_whenICallRetryButtonAndInteractorReturnsError_thenICallViewShowErrorView() {

        let dummyInteractor = DummyLimitsInteractor()
        sut.interactor = dummyInteractor
        let dummyiew = LimitsViewDummy()
        sut.view = dummyiew
        dummyInteractor.forceError = true

        // given
        sut.cardBO = CardsGenerator.getCardBOWithAlias().cards[0]

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(dummyiew.isCalledShowErrorView == true)
    }

    func test_givenCardBO_whenICallRetryButtonAndInteractorReturnsError_thenErrorBOShouldBeFilled() {

        let dummyInteractor = DummyLimitsInteractor()
        sut.interactor = dummyInteractor
        dummyInteractor.forceError = true

        // given
        sut.cardBO = CardsGenerator.getCardBOWithAlias().cards[0]

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(sut.errorBO != nil)
    }

    func test_givenCardBO_whenICallRetryButtonAndInteractorReturnsError_thenErrorBOShouldMatchWithInteractorReturned() {

        let dummyInteractor = DummyLimitsInteractor()
        sut.interactor = dummyInteractor
        dummyInteractor.forceError = true

        // given
        sut.viewDidLoad = false
        sut.cardBO = CardsGenerator.getCardBOWithAlias().cards[0]

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(sut.errorBO === dummyInteractor.errorBO)
    }

    // MARK: - editLimitButtonPressed

    func test_givenLimitsBOAndCardBOAndAllowsEditTrue_whenICallEditLimitButton_thenRouterCallNavigateToEditLimitScreen() {

        // given
        let limitBO = LimitsGenerator.getThreeLimits()
        sut.limitsBO = limitBO
        sut.cardBO = CardsGenerator.getCardBOCreditCard().cards[0]

        let limitsDisplayData = LimitsDisplayData(withLimits: limitBO)
        var displayData = limitsDisplayData.items[0]
        displayData.allowsEdit = true

        // when
        sut.editLimitButtonPressed(atIndex: 0, withLimitDisplayData: displayData)

        // then
        XCTAssert(dummyBusManager.isCalledShowEditLimits == true)
    }

    func test_givenLimitsBOAndCardBOAndAllowsEditFalse_whenICallEditLimitButton_thenRouterNotCallNavigateToEditLimitScreen() {

        // given
        let limitBO = LimitsGenerator.getThreeLimits()
        sut.limitsBO = limitBO
        sut.cardBO = CardsGenerator.getCardBOCreditCard().cards[0]

        let limitsDisplayData = LimitsDisplayData(withLimits: limitBO)
        var displayData = limitsDisplayData.items[0]
        displayData.allowsEdit = false

        // when
        sut.editLimitButtonPressed(atIndex: 0, withLimitDisplayData: displayData)

        // then
        XCTAssert(dummyBusManager.isCalledShowEditLimits == false)
    }

    func test_givenLimitsBOAndCardBOAndAllowsEditTrue_whenICallEditLimitButton_thenLimitsShouldMatch() {

        // given
        let limitBO = LimitsGenerator.getThreeLimits()
        sut.limitsBO = limitBO
        sut.cardBO = CardsGenerator.getCardBOCreditCard().cards[0]

        let limitsDisplayData = LimitsDisplayData(withLimits: limitBO)
        var displayData = limitsDisplayData.items[0]
        displayData.allowsEdit = true

        // when
        sut.editLimitButtonPressed(atIndex: 0, withLimitDisplayData: displayData)

        // then
        XCTAssert(dummyBusManager.limitTransport?.limit === sut.limitsBO!.limits[0])
    }

    func test_givenCardBOAndAllowsEditTrue_whenICallEditLimitButton_thenLimitTransportShouldHaveCardIdThatMatchWithCardIdOfCardBO() {

        // given
        let limitBO = LimitsGenerator.getThreeLimits()
        sut.limitsBO = limitBO
        sut.cardBO = CardsGenerator.getCardBOCreditCard().cards[0]

        let limitsDisplayData = LimitsDisplayData(withLimits: limitBO)
        var displayData = limitsDisplayData.items[0]
        displayData.allowsEdit = true

        // when
        sut.editLimitButtonPressed(atIndex: 0, withLimitDisplayData: displayData)

        // then
        XCTAssert(dummyBusManager.limitTransport?.cardId == sut.cardBO?.cardId)
    }

    func test_givenLimitsBOAndCardBOAndAllowsEditFalse_whenICallEditLimitButton_thenErrorBOShouldBeFilled() {

        // given
        let limitBO = LimitsGenerator.getThreeLimits()
        sut.limitsBO = limitBO
        sut.cardBO = CardsGenerator.getCardBOCreditCard().cards[0]

        let limitsDisplayData = LimitsDisplayData(withLimits: limitBO)
        var displayData = limitsDisplayData.items[0]
        displayData.allowsEdit = false

        // when
        sut.editLimitButtonPressed(atIndex: 0, withLimitDisplayData: displayData)

        // then
        XCTAssert(sut.errorBO != nil)
    }

    func test_givenLimitsBOAndCardBOAndAllowsEditFalseAndErrorBO_whenICallEditLimitButton_thenErrorBOMatch() {

        // given
        let limitBO = LimitsGenerator.getThreeLimits()
        sut.limitsBO = limitBO
        sut.cardBO = CardsGenerator.getCardBOCreditCard().cards[0]

        let limitsDisplayData = LimitsDisplayData(withLimits: limitBO)
        var displayData = limitsDisplayData.items[0]
        displayData.allowsEdit = false

        let message = "\(displayData.notEditableTitle) \(displayData.maximumLimitAmount.string)"
        let attributtedText = CustomAttributedText.attributedBoldText(forFullText: message, withBoldText: "", withFont: Tools.setFontBook(size: 14), withBoldFont: Tools.setFontBold(size: 14), withAlignment: .center, andForegroundColor: .BBVA600)
        let errorBO = ErrorBO(messageAttributed: attributtedText, errorType: .info, allowCancel: false, okTitle: Localizables.common.key_common_accept_text)

        // when
        sut.editLimitButtonPressed(atIndex: 0, withLimitDisplayData: displayData)

        // then
        XCTAssert(sut.errorBO?.messageAttributed?.string == errorBO.messageAttributed?.string)
        XCTAssert(sut.errorBO?.allowCancel == errorBO.allowCancel)
        XCTAssert(sut.errorBO?.errorType == errorBO.errorType)
        XCTAssert(sut.errorBO?.okButtonTitle == errorBO.okButtonTitle)
    }

    func test_givenLimitsBOAndCardBOAndAllowsEditFalse_whenICallEditLimitButton_thenIsCalledShowError() {

        let dummyiew = LimitsViewDummy()
        sut.view = dummyiew
        // given
        let limitBO = LimitsGenerator.getThreeLimits()
        sut.limitsBO = limitBO
        sut.cardBO = CardsGenerator.getCardBOCreditCard().cards[0]

        let limitsDisplayData = LimitsDisplayData(withLimits: limitBO)
        var displayData = limitsDisplayData.items[0]
        displayData.allowsEdit = false

        // when
        sut.editLimitButtonPressed(atIndex: 0, withLimitDisplayData: displayData)

        // then
        XCTAssert(dummyiew.isCalledShowError == true)
    }

    func test_givenLimitsBOAndCardBOAndAllowsEditTrue_whenICallEditLimitButton_thenNotCalledShowError() {

        let dummyiew = LimitsViewDummy()
        sut.view = dummyiew
        // given
        let limitBO = LimitsGenerator.getThreeLimits()
        sut.limitsBO = limitBO
        sut.cardBO = CardsGenerator.getCardBOCreditCard().cards[0]

        let limitsDisplayData = LimitsDisplayData(withLimits: limitBO)
        var displayData = limitsDisplayData.items[0]
        displayData.allowsEdit = true

        // when
        sut.editLimitButtonPressed(atIndex: 0, withLimitDisplayData: displayData)

        // then
        XCTAssert(dummyiew.isCalledShowError == false)
    }
}

    class LimitsViewDummy: LimitsViewProtocol {

        var isCalledShowError = false
        var isSendScreenOmniture = false
        var isCalledShowLimits = false
        var isCalledShowSkeleton = false
        var isCalledHideSkeleton = false
        var isCalledShowNoContentView = false
        var isCalledShowErrorView = false
        var isCalledSendUpdateLimitSuccessfulOmniture = false
        var isCalledShowSuccessfulToast = false
        var isCalledAnimateCurrentLimitToActualPercent = false

        var sentLimitName = ""
        var sentTitleId = ""
        var sentToastText = ""
        var sentDisplayData: LimitsDisplayData?

        func showError(error: ModelBO) {

            isCalledShowError = true
        }

        func changeColorEditTexts() {

        }

        func sendScreenOmniture(withTitleId titleId: String) {

            isSendScreenOmniture = true
            sentTitleId = titleId
        }

        func showLimits(withDisplayData displayData: LimitsDisplayData) {

            isCalledShowLimits = true
            sentDisplayData = displayData
        }

        func showSkeleton() {

            isCalledShowSkeleton = true
        }

        func hideSkeleton() {

            isCalledHideSkeleton = true
        }

        func showNoContentView() {

            isCalledShowNoContentView = true
        }

        func showErrorView() {

            isCalledShowErrorView = true
        }

        func sendUpdateLimitSuccessfulOmniture(withLimitName limitName: String) {

            isCalledSendUpdateLimitSuccessfulOmniture = true
            sentLimitName = limitName
        }

        func showSuccessfulToast(withText text: String) {

            isCalledShowSuccessfulToast = true
            sentToastText = text
        }

        func animateToActualPercentCurrentLimit(limitId: String, fromPercent: Float) {

            isCalledAnimateCurrentLimitToActualPercent = true
        }
    }

    class LimitsDummyBusManager: BusManager {

        var isCalledShowEditLimits = false
        var limitTransport: LimitTransport?

        override init() {

            super.init()
            routerFactory = RouterInitFactory()
        }

        override func navigateScreen<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {

            if tag == LimitsPageReaction.ROUTER_TAG && event === LimitsPageReaction.EVENT_NAV_TO_EDIT_LIMITS {
                isCalledShowEditLimits = true
                limitTransport = (values as! LimitTransport)
            }
        }

    }

    class DummyLimitsInteractor: LimitsInteractorProtocol {

        var isCalledProvideLimits = false

        var forceError = false
        var responseStatus = 200
        var sentCardId = "sentCardId"

        var limitsBO: LimitsBO?
        var errorBO: ErrorBO?

        func provideLimits(withCardId cardId: String) -> Observable <ModelBO> {

            sentCardId = cardId

            isCalledProvideLimits = true

            if !forceError {

                if limitsBO == nil {
                    limitsBO = LimitsGenerator.getThreeLimits()
                }

                limitsBO?.status = responseStatus

                return Observable <ModelBO>.just(limitsBO!)
            } else {

                if errorBO == nil {
                    errorBO = ErrorBO(error: ErrorEntity(message: "Error"))
                    errorBO!.isErrorShown = false
                }

                return Observable.error(ServiceError.GenericErrorBO(error: errorBO!))

            }

        }

    }

    class DummyLimitsBO: LimitsBO {

        var isCalledReplaceLimit = false
        var limitToReplace: LimitBO?

        override func replaceLimit(_ limit: LimitBO) {

            isCalledReplaceLimit = true
            limitToReplace = limit

        }

}
