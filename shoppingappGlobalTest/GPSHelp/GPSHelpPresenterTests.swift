//
//  GPSHelpPresenterTests.swift
//  shoppingapp
//
//  Created by Javier Pino on 19/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest
import CellsNativeComponents

#if TESTMX
@testable import shoppingappMX
#endif

class GPSHelpPresenterTests: XCTestCase {
    
    var sut: GPSHelpPresenter<DummyGPSHelpView>!
    var dummyView: DummyGPSHelpView!
    var dummyBusManager: DummyBusManager!
    
    override func setUp() {
        
        super.setUp()
        
        dummyBusManager = DummyBusManager()
        sut = GPSHelpPresenter<DummyGPSHelpView>(busManager: dummyBusManager)

        dummyView = DummyGPSHelpView()
        sut.view = dummyView
    }
    
    // MARK: - acceptButtonPressed
    
    func test_givenAny_whenICallAcceptButtonPressed_thenICallViewDismiss() {
        
        //given
        
        //when
        sut.acceptButtonPressed()
        
        //then
        XCTAssert(dummyView.isCalledDismiss == true)
    }
    
    func test_givenAny_whenICallAcceptButtonPressed_thenICallBusManagerPublishDataWithRightEvent() {
        
        //given
        
        //when
        sut.acceptButtonPressed()
        
        //then
        XCTAssert(dummyBusManager.isCalledPublishDataDismissScreen == true)
    }
    
    // MARK: - cancelButtonPressed
    
    func test_givenAny_whenICallCancelButtonPressed_thenICallViewDismiss() {
        
        //given
        
        //when
        sut.cancelButtonPressed()
        
        //then
        XCTAssert(dummyView.isCalledDismiss == true)
    }
    
    func test_givenAny_whenICallCancelButtonPressed_thenICallBusManagerPublishDataWithRightEvent() {
        
        //given
        
        //when
        sut.cancelButtonPressed()
        
        //then
        XCTAssert(dummyBusManager.isCalledPublishDataDismissScreen == true)
    }
    
    // MARK: - Dummy classes
    
    class DummyGPSHelpView: GPSHelpViewProtocol {
        
        var isCalledDismiss = false
        
        func dismiss() {
            
            isCalledDismiss = true
        }
        
        func showError(error: ModelBO) {
        }
        
        func changeColorEditTexts() {
        }
    }
    
    class DummyBusManager: BusManager {
        
        var isCalledPublishDataDismissScreen = false
        let expectationPublishData = XCTestExpectation(description: "Waiting for publish data")
        
        override init() {
            super.init()
            routerFactory = RouterInitFactory()
        }
        
        override func publishData<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {
            
            if tag == GPSHelpPageReaction.routerTag && event === GPSHelpPageReaction.eventDismissScreen {
                isCalledPublishDataDismissScreen = true
            }
            
            expectationPublishData.fulfill()
        }
    }
}
