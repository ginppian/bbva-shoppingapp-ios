//
//  BVALoginLoaderTest.swift
//  shoppingapp
//
//  Created by ruben.fernandez on 15/6/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import XCTest
import RxSwift
import CellsNativeComponents

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class LoginLoaderPresenterTest: XCTestCase {

    var sut: LoginLoaderPresenter<DummyLoaderView>!
    var dummyBusManager: DummyBusManager!
    private var dummyPreferencesManager: DummyPreferencesManager!
    var dummySessionManager: DummySessionManager!

    override func setUp() {
        
        super.setUp()
        KeychainManagerTestHelper.configureKeychainManagerForTesting()
        dummyBusManager = DummyBusManager()
        sut = LoginLoaderPresenter<DummyLoaderView>(busManager: dummyBusManager)

        DummyPreferencesManager.configureDummyPreferences()
        dummyPreferencesManager = PreferencesManager.instance as? DummyPreferencesManager
        
        DummySessionManager.setupMock()
        dummySessionManager = (SessionDataManager.sessionDataInstance() as! DummySessionManager)
    }

    // MARK: - viewDidLoad

    func test_givenAny_whenICallviewDidLoad_thenICallViewShowUnlockTitle() {

        let dummyView = DummyLoaderView()
        sut.view = dummyView
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given

        // when
        sut.viewDidLoad()

        // then
        XCTAssert(dummyView.isCalledShowUnlockTitle == true)

    }

    func test_givenLoginTransport_whenICallviewDidLoad_thenICallViewShowUnlockTitleWithAppropiateTitle() {

        let dummyView = DummyLoaderView()
        sut.view = dummyView
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.loginTransport = LoginTransport(username: "user", password: "pass")

        // when
        sut.viewDidLoad()

        // then
        XCTAssert(dummyView.sentShowUnlockTitle == Localizables.login.key_login_loading_text)

    }

    func test_givenLoginTransportNil_whenICallviewDidLoad_thenICallViewShowUnlockTitleWithAppropiateTitle() {

        let dummyView = DummyLoaderView()
        sut.view = dummyView
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.loginTransport = nil

        // when
        sut.viewDidLoad()

        // then
        XCTAssert(dummyView.sentShowUnlockTitle == Localizables.login.key_loading_promotions)

    }

    func test_givenACorrectUserAndPassword_whenICallviewDidLoad_thenCallProvideLoginData() {

        //given
        let mockTransport = LoginTransport(username: "user", password: "pass")
        let dummyInteractor = DummyInteractor()
        let dummyView = DummyLoaderView()

        //when
        sut.interactor = dummyInteractor
        sut.loginTransport = mockTransport
        sut.view = dummyView
        sut.viewDidLoad()

        //then
        XCTAssert(dummyInteractor.isCalledProvideLoginData == true)

    }
    
    //------------------------
    
    func test_givenACorrectCardsAndPreferencesManagerLoginNumberNil_whenICallViewDidLoadAndTheInteractorCardsIsSuccess_thenPreferencesManagerSaveLoginNumberWith1() {
        
        //given
        let loginTransport = LoginTransport(username: "user", password: "pass")
        sut.loginTransport = loginTransport
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        dummyPreferencesManager.keysWithValues = [PreferencesManagerKeys.kLoginNumber.rawValue: NSNumber() as AnyObject]
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyPreferencesManager.saveValueSentValues[0] as? NSNumber == 1)
        XCTAssert(dummyPreferencesManager.getValueSentKeys[0] == PreferencesManagerKeys.kLoginNumber)
        XCTAssert(dummyPreferencesManager.saveValueSentKeys[0] == PreferencesManagerKeys.kLoginNumber)
    }
    
    func test_givenACorrectCardsAndPreferencesManagerLoginNumber_whenICallViewDidLoadAndTheInteractorCardsIsSuccess_thenPreferencesManagerSaveLoginNumberByAddingOne() {
        
        // given
        let loginTransport = LoginTransport(username: "user", password: "pass")
        sut.loginTransport = loginTransport
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let firstValue = 2
        dummyPreferencesManager.keysWithValues = [PreferencesManagerKeys.kLoginNumber.rawValue: NSNumber(value: firstValue) as AnyObject]
        let valueExpected = NSNumber(value: firstValue + 1)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyPreferencesManager.saveValueSentValues[0] as? NSNumber == valueExpected)
        XCTAssert(dummyPreferencesManager.getValueSentKeys[0] == PreferencesManagerKeys.kLoginNumber)
        XCTAssert(dummyPreferencesManager.saveValueSentKeys[0] == PreferencesManagerKeys.kLoginNumber)
    }
    
    func test_givenACorrectCardsAndPreferencesManagerNumberLoginsRateAppNil_whenICallViewDidLoadAndTheInteractorCardsIsSuccess_thenPreferencesManagerSaveNumberLoginsRateAppWith1() {
        
        //given
        let loginTransport = LoginTransport(username: "user", password: "pass")
        sut.loginTransport = loginTransport
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        dummyPreferencesManager.keysWithValues = [PreferencesManagerKeys.kNumberLoginsRateApp.rawValue: NSNumber() as AnyObject]
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyPreferencesManager.saveValueSentValues[1] as? NSNumber == 1)
        XCTAssert(dummyPreferencesManager.getValueSentKeys[1] == PreferencesManagerKeys.kNumberLoginsRateApp)
        XCTAssert(dummyPreferencesManager.saveValueSentKeys[1] == PreferencesManagerKeys.kNumberLoginsRateApp)
    }
    
    func test_givenACorrectCardsAndPreferencesManagerNumberLoginsRateApp_whenICallViewDidLoadAndTheInteractorCardsIsSuccess_thenPreferencesManagerSaveNumberLoginsRateAppByAddingOne() {
        
        // given
        let loginTransport = LoginTransport(username: "user", password: "pass")
        sut.loginTransport = loginTransport
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let firstValue = 2
        dummyPreferencesManager.keysWithValues = [PreferencesManagerKeys.kNumberLoginsRateApp.rawValue: NSNumber(value: firstValue) as AnyObject]
        let valueExpected = NSNumber(value: firstValue + 1)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyPreferencesManager.saveValueSentValues[1] as? NSNumber == valueExpected)
        XCTAssert(dummyPreferencesManager.getValueSentKeys[1] == PreferencesManagerKeys.kNumberLoginsRateApp)
        XCTAssert(dummyPreferencesManager.saveValueSentKeys[1] == PreferencesManagerKeys.kNumberLoginsRateApp)
    }
    
    //------------------------

    func test_givenACorrectUserAndPassword_whenICallviewDidLoad_thenTheInteractorReceiveTheSameLoginTransport() {

        let dummyInteractor = DummyInteractor()
        let dummyView = DummyLoaderView()

        //given
        let mockTransport = LoginTransport(username: "user", password: "pass")

        //when
        sut.interactor = dummyInteractor
        sut.loginTransport = mockTransport
        sut.view = dummyView
        sut.viewDidLoad()
        //then
        XCTAssert(dummyInteractor.loginTransportNew as? LoginTransport === mockTransport)

    }

    func test_givenACorrectCustomerData_whenICallviewDidLoadAndTheInteractorCustomerDataIsSuccess_thenCallProvideards() {

        let dummyInteractor = DummyInteractor()
        let dummyView = DummyLoaderView()

        //given
        let mockTransport = LoginTransport(username: "user", password: "pass")

        //when
        sut.interactor = dummyInteractor
        sut.loginTransport = mockTransport
        sut.view = dummyView
        sut.viewDidLoad()

        //then
        XCTAssert(dummyInteractor.isCalledProvideCards == true)

    }

    func test_givenACorrectCards_whenICallviewDidLoadAndTheInteractorCardsIsSuccess_thenCallViewToStartAnimation() {

        let dummyInteractor = DummyInteractor()
        let dummyView = DummyLoaderView()

        //given
        let mockTransport = LoginTransport(username: "user", password: "pass")

        //when
        sut.interactor = dummyInteractor
        sut.loginTransport = mockTransport
        sut.view = dummyView
        sut.viewDidLoad()
        //then
        XCTAssert(dummyView.isCalledStartAnimation == true)

    }

    func test_givenACorrectGrantingTicket_whenICallviewDidLoadAndTheInteractorGrandTingTickerFailure_thenCallRouterGoToLoginWithError() {

        let dummyInteractor = DummyInteractor()

        //given
        let mockTransport = LoginTransport(username: "error", password: "pass")

        //when
        sut.interactor = dummyInteractor
        sut.loginTransport = mockTransport
        sut.viewDidLoad()

        //then
        XCTAssert(dummyBusManager.isCalledPublishDataWithTagLoginLoaderAndEventErrorLogin == true)
        XCTAssert(dummyBusManager.saveErrorLogin === dummyInteractor.errorLogin)

    }
    
    func test_givenAGrantingTicketStatus403AndErrorCode160_whenICallviewDidLoadAndTheInteractorGrandTingTickerFailure_thenCallRouterGoToLoginWithError() {
        
        let dummyInteractor = DummyInteractor()
        
        //given
        let mockTransport = LoginTransport(username: "error", password: "pass")
        dummyInteractor.statusGrantingTicket = 403
        dummyInteractor.errorCodeGrantingTicket = "160"
        
        //when
        sut.interactor = dummyInteractor
        sut.loginTransport = mockTransport
        sut.viewDidLoad()
        
        //then
        XCTAssert(dummyBusManager.isCalledPublishDataWithTagLoginLoaderAndEventErrorLogin == true)
        XCTAssert(dummyBusManager.saveErrorLogin?.errorMessage() == Localizables.login.key_login_user_pass_error_message)
        XCTAssert(dummyBusManager.saveErrorLogin?.status == dummyInteractor.errorLogin?.status)
        XCTAssert(dummyInteractor.errorLogin?.errorMessage() == Localizables.login.key_login_user_pass_error_message)
        
    }
    
    func test_givenAGrantingTicketStatus403AndErrorCode161_whenICallviewDidLoadAndTheInteractorGrandTingTickerFailure_thenCallRouterGoToLoginWithError() {
        
        let dummyInteractor = DummyInteractor()
        
        //given
        let mockTransport = LoginTransport(username: "error", password: "pass")
        dummyInteractor.statusGrantingTicket = 403
        dummyInteractor.errorCodeGrantingTicket = "161"
        
        //when
        sut.interactor = dummyInteractor
        sut.loginTransport = mockTransport
        sut.viewDidLoad()
        
        //then
        XCTAssert(dummyBusManager.isCalledPublishDataWithTagLoginLoaderAndEventErrorLogin == true)
        XCTAssert(dummyBusManager.saveErrorLogin?.errorMessage() == Localizables.login.key_login_user_pass_error_message)
        XCTAssert(dummyBusManager.saveErrorLogin?.status == dummyInteractor.errorLogin?.status)
        XCTAssert(dummyInteractor.errorLogin?.errorMessage() == Localizables.login.key_login_user_pass_error_message)
        
    }

    func test_givenACorrectGrantingTicket_whenICallviewDidLoadAndTheInteractorCustomerDataisFailure_thenCallRouterGoToLoginWithError() {

        let dummyInteractor = DummyInteractor()

        //given
        let mockTransport = LoginTransport(username: "user", password: "pass")
        dummyInteractor.forceErrorCustomerData = true

        //when
        sut.interactor = dummyInteractor
        sut.loginTransport = mockTransport
        sut.viewDidLoad()

        //then
        XCTAssert(dummyBusManager.isCalledPublishDataWithTagLoginLoaderAndEventErrorLogin == true)
        XCTAssert(dummyBusManager.saveErrorLogin === dummyInteractor.errorLogin)
    }

    func test_givenACorrectGrantingTicketAndCustomerData_whenICallviewDidLoadAndTheInteractorCardsFailure_thenCallRouterGoToLoginWithError() {

        let dummyInteractor = DummyInteractor()

        //given
        let mockTransport = LoginTransport(username: "user", password: "pass")
        dummyInteractor.forceErrorCards = true

        //when
        sut.interactor = dummyInteractor
        sut.loginTransport = mockTransport
        sut.viewDidLoad()

        //then
        XCTAssert(dummyBusManager.isCalledPublishDataWithTagLoginLoaderAndEventErrorLogin == true)
        XCTAssert(dummyBusManager.saveErrorLogin === dummyInteractor.errorLogin)
    }
    func test_givenACorrectCardsAndNotSavedUser_whenICallviewDidLoadAndTheInteractorCardsIsSuccess_thenISaveUseridAndFirstnameAndSaveCustomeridInTheSessionAndUserLogged() {

        let dummyView = DummyLoaderView()
        let dummyInteractor = DummyInteractor()

        //given
        let mockTransport = LoginTransport(username: "user", password: "pass")
        dummyInteractor.userBO.firstName = "firstname"
        dummyInteractor.userBO.customerId = "customerId"

        KeychainManager.shared.resetKeyChainItems()

        //when
        sut.interactor = dummyInteractor
        sut.loginTransport = mockTransport
        sut.view = dummyView
        sut.viewDidLoad()

        //then
        XCTAssert(KeychainManager.shared.getFirstname() == "firstname")
        XCTAssert(KeychainManager.shared.getUserId() == "user")
        XCTAssert(dummySessionManager.customerID == "customerId" )

    }
    func test_givenACorrectCardsAndSavedUser_whenICallviewDidLoadAndTheInteractorCardsIsSuccess_thenIDontSaveUseridAndFirstnameAndISaveCustomeridInTheSessionAndUserLogged() {

        let dummyView = DummyLoaderView()
        let dummyInteractor = DummyInteractor()

        //given
        let mockTransport = LoginTransport(username: "user", password: "pass")
        dummyInteractor.userBO.customerId = "customerId"
        KeychainManager.shared.saveUserId(forUserId: mockTransport.userId)
        KeychainManager.shared.saveFirstname(forFirstname: "firstname")

        //when
        sut.interactor = dummyInteractor
        sut.loginTransport = mockTransport
        sut.view = dummyView
        sut.viewDidLoad()

        //then
        XCTAssert(KeychainManager.shared.getFirstname() == "firstname")
        XCTAssert(KeychainManager.shared.getUserId() == "user")
        XCTAssert(dummySessionManager.customerID == "customerId" )

    }
    func test_givenAInCorrectCardsAndSavedUser_whenICallviewDidLoadAndTheInteractorCardsWithError_thenIDontSaveUseridAndFirstnameAndCustomeridInTheSessionAndUserLogged() {

        let dummyView = DummyLoaderView()
        let dummyInteractor = DummyInteractor()

        //given
        let mockTransport = LoginTransport(username: "user", password: "pass")
        dummyInteractor.forceErrorCards = true
        KeychainManager.shared.saveUserId(forUserId: mockTransport.userId)
        KeychainManager.shared.saveFirstname(forFirstname: "firstname")

        //when
        sut.interactor = dummyInteractor
        sut.loginTransport = mockTransport
        sut.view = dummyView
        sut.viewDidLoad()

        //then
        XCTAssert(KeychainManager.shared.getFirstname() == "firstname")
        XCTAssert(KeychainManager.shared.getUserId() == "user")
        XCTAssert(dummySessionManager.customerID == nil )

    }

    func test_givenAnInCorrectCardsAndNotSavedUser_whenICallviewDidLoadAndTheInteractorCardsIsSuccess_thenIDontSaveUseridAndFirstnameAndCustomeridInTheSession() {

        //given
        let mockTransport = LoginTransport(username: "user", password: "pass")
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceErrorCards = true

        let dummyView = DummyLoaderView()
        KeychainManager.shared.resetKeyChainItems()

        //when
        sut.interactor = dummyInteractor
        sut.loginTransport = mockTransport
        sut.view = dummyView
        sut.viewDidLoad()

        //then
        XCTAssert(KeychainManager.shared.getFirstname() == "" )
        XCTAssert(KeychainManager.shared.getUserId() == "" )
        XCTAssert(dummySessionManager.customerID == nil )

    }

    func test_givenLoginTransportNil_whenICallviewDidLoad_thenICallInteractorProvideAnonymousInformation() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.loginTransport = nil

        // when
        sut.viewDidLoad()

        // then
        XCTAssert(dummyInteractor.isCalledProvideAnonymousInformation == true)

    }

    func test_givenLoginTransportNil_whenICallViewWillApearAndInteractorReturnsAnonymousInformationError_thenICallRouterGoToLoginWithError() {

        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceErrorAnonymousInformation = true
        sut.interactor = dummyInteractor

        // given
        sut.loginTransport = nil

        // when
        sut.viewDidLoad()

        // then
        XCTAssert(dummyBusManager.isCalledPublishDataWithTagLoginLoaderAndEventErrorLogin == true)
        XCTAssert(dummyBusManager.saveErrorLogin === dummyInteractor.errorLogin)
    }

    func test_givenLoginTransportNil_whenICallviewDidLoadAndInteractorReturnsAnonymousInformation_thenICallInteractorRequestGrantingTicket() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.loginTransport = nil

        // when
        sut.viewDidLoad()

        // then
        XCTAssert(dummyInteractor.isCalledRequestGrantingTicket == true)

    }

    func test_givenLoginTransportNil_whenICallviewDidLoadAndInteractorReturnsAnonymousInformation_thenICallInteractorRequestGrantingTicketWithAnonymousInformationReturned() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let entity = AnonymousInformationEntity(consumerId: "consumerId", userId: "userId")
        dummyInteractor.forceAnonymousInformation = AnonymousInformationBO(entity: entity)

        // given
        sut.loginTransport = nil

        // when
        sut.viewDidLoad()

        // then
        XCTAssert(dummyInteractor.sentAnonymousInformation != nil)
        XCTAssert(dummyInteractor.sentAnonymousInformation == dummyInteractor.forceAnonymousInformation)

    }

    func test_givenLoginTransportNil_whenICallviewDidLoadAndInteractorReturnsAnonymousInformationAndGrantingTicketReturnsError_thenICallRouterGoToLoginWithError() {

        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceErrorGrantingTicket = true
        sut.interactor = dummyInteractor

        //given
        sut.loginTransport = nil

        //when
        sut.viewDidLoad()

        //then
        XCTAssert(dummyBusManager.isCalledPublishDataWithTagLoginLoaderAndEventErrorLogin == true)
        XCTAssert(dummyBusManager.saveErrorLogin === dummyInteractor.errorLogin)

    }

    func test_givenLoginTransportNil_whenICallviewDidLoadAndInteractorReturnsAnonymousInformationAndGrantingTicketReturnsSuccess_thenICallViewStartUnlockAnimation() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let dummyView = DummyLoaderView()
        sut.view = dummyView

        // given
        sut.loginTransport = nil

        // when
        sut.viewDidLoad()

        // then
        XCTAssert(dummyView.isCalledStartAnimation == true)

    }

    // MARK: - didFinishUnlockAnimation

    func test_givenCards_whenICallDidFinishUnlockAnimation_thenRouterGoToHome() {

        let dummyView = DummyLoaderView()
        sut.view = dummyView

        //given
        sut.cards = CardsGenerator.getThreeCards()

        //when
        sut.didFinishUnlockAnimation()

        //then
        XCTAssert(dummyBusManager.isCalledLoadNextInitFactory == true)

    }

    func test_givenCards_whenICallDidFinishUnlockAnimation_thenSessionManagerIsUserLoggedShouldBeTrue() {
        
        // given
        sut.cards = CardsGenerator.getThreeCards()

        // when
        sut.didFinishUnlockAnimation()

        // then
        XCTAssert(dummySessionManager.isUserLogged == true)

    }
    
    func test_givenCardsNil_whenICallDidFinishUnlockAnimation_thenSessionManagerIsUserAnonymousShouldBeTrue() {
        
        // given
        sut.cards = nil
        
        // when
        sut.didFinishUnlockAnimation()
        
        // then
        XCTAssert(dummySessionManager.isUserAnonymous == true)
        
    }

    func test_givenCards_whenICallDidFinishUnlockAnimation_thenICallBusManagerPublishDataWithParamCards() {

        let dummyView = DummyLoaderView()
        sut.view = dummyView

        //given
        sut.cards = CardsGenerator.getThreeCards()

        //when
        sut.didFinishUnlockAnimation()

        //then
        XCTAssert(dummyBusManager.isCalledPublishChannelCards == true)

    }

    func test_givenCards_whenICallDidFinishUnlockAnimation_thenICallBusManagerPublishDataWithParamCardsWithCards() {

        let dummyView = DummyLoaderView()
        sut.view = dummyView

        //given
        sut.cards = CardsGenerator.getThreeCards()

        //when
        sut.didFinishUnlockAnimation()

        //then
        XCTAssert(dummyBusManager.saveCards === sut.cards)

    }

    func test_givenCardsNil_whenICallDidFinishUnlockAnimation_thenICallBusManagerAnonymousPromotionsSession() {

        //given
        sut.cards = nil

        //when
        sut.didFinishUnlockAnimation()

        //then
        XCTAssert(dummyBusManager.isCalledNavigateToAnonymousPromotionsSession == true)

    }
    
    func test_givenCards_whenICallDidFinishUnlockAnimation_thenICallPreferencesManagerWithAppropiateKeyAndValue() {
        
        let dummyPreferencesManager = DummyPreferencesManager()
        PreferencesManager.instance = dummyPreferencesManager
        
        //given
        sut.cards = CardsGenerator.getThreeCards()
        
        //when
        sut.didFinishUnlockAnimation()
        
        //then
        XCTAssert(dummyPreferencesManager.key! == PreferencesManagerKeys.kWelcomeShowed.rawValue)
        XCTAssert((dummyPreferencesManager.value! as! Bool) == true)
        
    }

    class DummyInteractor: LoginLoaderInteractorProtocol {

        var isCalledProvideLoginData = false
        var isCalledProvideCustomerData = false
        var isCalledProvideAnonymousInformation = false
        var isCalledRequestGrantingTicket = false
        var forceErrorCustomerData = false
        var forceErrorCards = false
        var forceErrorGrantingTicket = false
        var forceErrorAnonymousInformation = false

        var loginTransportNew: ModelTransport?
        var errorLogin: ErrorBO?

        var isCalledProvideCards = false
        var statusToReturn = 206
        var statusGrantingTicket = 200
        var errorCodeGrantingTicket = "160"
        var cards: CardsBO?
        var dummyCards: CardsBO?
        var userBO: UserBO = UserBO(user: UserEntity())
        var sentAnonymousInformation: AnonymousInformationBO?
        var forceAnonymousInformation: AnonymousInformationBO?

        func provideConsumerId() -> Observable<String> {

            return Observable<String>.just("00000" as String)
        }

        func provideLoginData(loginTransport: ModelTransport, consumerId: String) -> Observable<ModelBO> {

            loginTransportNew = loginTransport
            isCalledProvideLoginData = true

            let grantingTicket = GrandTingTicketBO(grantingTicket: nil)
            grantingTicket.authenticationState = "OK"

            if (loginTransport as? LoginTransport)?.username != "error" {
                return Observable <ModelBO>.just(grantingTicket as ModelBO)
            } else {
                
                var message = "Error"
                
                if statusGrantingTicket == 403 && (errorCodeGrantingTicket == "160" || errorCodeGrantingTicket == "161") {
                    message = Localizables.login.key_login_user_pass_error_message
                }
                
                let errorBO = ErrorBO(error: ErrorEntity(message: message))
                errorBO.status = statusGrantingTicket
                errorBO.code = errorCodeGrantingTicket
                errorLogin = errorBO
                return Observable.error(ServiceError.GenericErrorBO(error: errorBO))
            }
        }

        func provideCustomerData() -> Observable<ModelBO> {

            isCalledProvideCustomerData = true

            if !forceErrorCustomerData {
                return Observable <ModelBO>.just(userBO as ModelBO)
            } else {
                let errorBO = ErrorBO(error: ErrorEntity(message: "Error"))
                errorLogin = errorBO
                return Observable.error(ServiceError.GenericErrorBO(error: errorBO))
            }
        }

        func provideCards() -> Observable <ModelBO> {

            self.isCalledProvideCards = true

            let dummyCardsEntity = CardsEntity()
            dummyCardsEntity.status = statusToReturn

            if let dummyCards = dummyCards {
                self.cards = dummyCards

            } else {
                self.cards = CardsGenerator.getCardBOWithAlias()
            }

            if !forceErrorCards {

                return Observable <ModelBO>.just(self.cards!)

            } else {

                let errorBO = ErrorBO(error: ErrorEntity(message: "Error"))

                errorLogin = errorBO
                return Observable.error(ServiceError.GenericErrorBO(error: errorBO))

            }
        }

        func provideAnonymousInformation() -> Observable<ModelBO> {

            isCalledProvideAnonymousInformation = true

            if forceErrorAnonymousInformation {

                let errorBO = ErrorBO(error: ErrorEntity(message: "Error"))

                errorLogin = errorBO
                return Observable.error(ServiceError.GenericErrorBO(error: errorBO))

            } else if let forceAnonymousInformation = forceAnonymousInformation {

                return Observable<ModelBO>.just(forceAnonymousInformation)
            } else {

                let entity = AnonymousInformationEntity(consumerId: "consumerId", userId: "userId")
                return Observable<ModelBO>.just(AnonymousInformationBO(entity: entity))
            }

        }

        func requestGrantingTicket(withAnonymousInformation anonymousInformation: AnonymousInformationBO) -> Observable<ModelBO> {

            sentAnonymousInformation = anonymousInformation
            isCalledRequestGrantingTicket = true

            let grantingTicket = GrandTingTicketBO(grantingTicket: nil)
            grantingTicket.authenticationState = "OK"

            if forceErrorGrantingTicket {
                let errorBO = ErrorBO(error: ErrorEntity(message: "Error"))
                errorLogin = errorBO
                return Observable.error(ServiceError.GenericErrorBO(error: errorBO))
            } else {
                return Observable <ModelBO>.just(grantingTicket as ModelBO)
            }

        }
    }

    class DummyBusManager: BusManager {

        var isCalledPublishDataWithTagLoginLoaderAndEventErrorLogin = false
        var isCalledLoadNextInitFactory = false
        var isCalledNavigateToAnonymousPromotionsSession = false
        var isCalledPublishChannelCards = true
        var saveCards: CardsBO!

        var saveErrorLogin: ErrorBO?

        override init() {
            super.init()
            routerFactory = RouterInitFactory()
        }

        override func publishData<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {

             if tag == ShoppingCrossReaction.TAG && event === PageReactionConstants.CHANNEL_CARDS {
                isCalledPublishChannelCards = true
             } else if tag == ShoppingCrossReaction.TAG && event === PageReactionConstants.PARAMS_CARDS {
                saveCards = (values as! CardsBO)
            }
        }

        override func navigateScreen<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {

            if tag == LoginLoaderPageReaction.ROUTER_TAG && event === LoginLoaderPageReaction.EVENT_LOGIN_FINISHED {
                isCalledLoadNextInitFactory = true
            } else if tag == LoginLoaderPageReaction.ROUTER_TAG && event === LoginLoaderPageReaction.EVENT_ERROR_LOGIN {
                saveErrorLogin = values as? ErrorBO
                isCalledPublishDataWithTagLoginLoaderAndEventErrorLogin = true
            } else if tag == LoginLoaderPageReaction.ROUTER_TAG && event === LoginLoaderPageReaction.EVENT_ANONYMOUS_SESSION {
                isCalledNavigateToAnonymousPromotionsSession = true
            }
        }

    }

    class DummyLoaderView: LoginLoaderViewProtocol {

        var isCalledStartAnimation = false
        var isCalledShowUnlockTitle = false
        var sentShowUnlockTitle = ""

        func startUnlockAnimation() {
            isCalledStartAnimation = true
        }

        func showUnlockTitle(_ title: String) {
            isCalledShowUnlockTitle = true
            sentShowUnlockTitle = title
        }

        func showError(error: ModelBO) {
        }

        func changeColorEditTexts() {
        }

    }
}
