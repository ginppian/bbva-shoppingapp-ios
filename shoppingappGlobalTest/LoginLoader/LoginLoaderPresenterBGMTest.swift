//
//  LoginLoaderPresenterBGMTest.swift
//  shoppingappMXTest
//
//  Created by Jesús Ángel Sánchez Sánchez on 03/12/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest
import RxSwift
import CellsNativeComponents
import BGMConnectSDK
import Component_DM_Header_Interceptor
import BBVA_Network

#if TESTMX
    @testable import shoppingappMX
#endif

class LoginLoaderPresenterBGMTest: XCTestCase {
    
    var sut: LoginLoaderPresenterBGM<DummyLoaderViewBGM>!
    var dummyBusManager: DummyBusManager!
    var dummySessionManager: DummySessionManager!
    var dummyInteractor = DummyInteractor()
    var dummyView = DummyLoaderViewBGM()
    var dummyHeaderInterceptorStore: DummyHeaderInterceptorStore!
    
    override func setUp() {
        super.setUp()
        dummyBusManager = DummyBusManager()
        sut = LoginLoaderPresenterBGM<DummyLoaderViewBGM>(busManager: dummyBusManager)
        
        DummySessionManager.setupMock()
        dummySessionManager = (SessionDataManager.sessionDataInstance() as! DummySessionManager)
        
        dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        dummyView = DummyLoaderViewBGM()
        sut.view = dummyView
        
        KeychainManagerTestHelper.configureKeychainManagerForTesting()
        DummyPreferencesManager.configureDummyPreferences()
        
        dummyHeaderInterceptorStore = DummyHeaderInterceptorStore()
        BusManager.sessionDataInstance.headerInterceptorStore = dummyHeaderInterceptorStore
    }
    
    // MARK: - requestGrantingTicket - Interactor
    
    func test_givenACorrectUserAndPassword_whenICallRequestGrantingTicket_thenICallProvideLoginData() {
        
        //given
        let loginTransport = LoginTransport(username: "user", password: "pass")
        
        //when
        sut.requestGrantingTicket(loginTransport: loginTransport, consumerId: "00000")
        
        //then
        XCTAssert(dummyInteractor.isCalledProvideLoginData == true)
    }
    
    func test_givenACorrectUserAndPassword_whenICallRequestGrantingTicket_thenLoginTransportMatch() {
        
        //given
        let loginTransport = LoginTransport(username: "user", password: "pass")
        
        //when
        sut.requestGrantingTicket(loginTransport: loginTransport, consumerId: "00000")
        
        //then
        XCTAssert(dummyInteractor.loginTransportNew as? LoginTransport === loginTransport)
    }
    
    func test_givenACorrectUserAndPassword_whenICallRequestGrantingTicket_thenConsumerIdMatch() {
        
        //given
        let loginTransport = LoginTransport(username: "user", password: "pass")
        let consumerId = "00000"
        
        //when
        sut.requestGrantingTicket(loginTransport: loginTransport, consumerId: consumerId)
        
        //then
        XCTAssert(dummyInteractor.consumerIdSent == consumerId)
    }
    
    // MARK: - requestGrantingTicket - provideLoginData Error
    
    func test_givenAnyError_whenICallRequestGrantingTicketAndInteractorFailure_thenCallRouterGoToLoginWithError() {
        
        // given
        let loginTransport = LoginTransport(username: "error", password: "pass")
        
        // when
        sut.requestGrantingTicket(loginTransport: loginTransport, consumerId: "00000")
        
        // then
        XCTAssert(dummyBusManager.isCalledPublishDataWithTagLoginLoaderAndEventErrorLogin == true)
        XCTAssert(dummyBusManager.saveErrorLogin === dummyInteractor.errorLogin)
    }
    
    func test_givenErrorWithStatus403AndErrorCode160_whenICallRequestGrantingTicketAndInteractorFailure_thenCallRouterGoToLoginWithError() {
        
        // given
        let loginTransport = LoginTransport(username: "error", password: "pass")
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.statusGrantingTicket = 403
        dummyInteractor.errorCodeGrantingTicket = "160"
        sut.interactor = dummyInteractor
        
        //when
        sut.requestGrantingTicket(loginTransport: loginTransport, consumerId: "00000")
        
        //then
        XCTAssert(dummyBusManager.isCalledPublishDataWithTagLoginLoaderAndEventErrorLogin == true)
        XCTAssert(dummyBusManager.saveErrorLogin?.errorMessage() == Localizables.login.key_login_user_pass_error_message)
        XCTAssert(dummyBusManager.saveErrorLogin?.status == dummyInteractor.errorLogin?.status)
        XCTAssert(dummyInteractor.errorLogin?.errorMessage() == Localizables.login.key_login_user_pass_error_message)
    }
    
    func test_givenErrorWithStatus403AndErrorCode161_whenICallRequestGrantingTicketAndInteractorFailure_thenCallRouterGoToLoginWithError() {
        
        // given
        let loginTransport = LoginTransport(username: "error", password: "pass")
        
        let dummyInteractor = DummyInteractor()
        dummyInteractor.statusGrantingTicket = 403
        dummyInteractor.errorCodeGrantingTicket = "161"
        sut.interactor = dummyInteractor
        
        //when
        sut.requestGrantingTicket(loginTransport: loginTransport, consumerId: "00000")
        
        //then
        XCTAssert(dummyBusManager.isCalledPublishDataWithTagLoginLoaderAndEventErrorLogin == true)
        XCTAssert(dummyBusManager.saveErrorLogin?.errorMessage() == Localizables.login.key_login_user_pass_error_message)
        XCTAssert(dummyBusManager.saveErrorLogin?.status == dummyInteractor.errorLogin?.status)
        XCTAssert(dummyInteractor.errorLogin?.errorMessage() == Localizables.login.key_login_user_pass_error_message)
    }
    
    // MARK: - requestGrantingTicket - provideLoginData Success
    
    func test_givenACorrectUserAndPassword_whenICallRequestGrantingTicket_thenGrantingTicketBOFill() {
        
        // given
        let loginTransport = LoginTransport(username: "user", password: "pass")
        
        // when
        sut.requestGrantingTicket(loginTransport: loginTransport, consumerId: "00000")
        
        // then
        XCTAssert(sut.grantingTicketBO != nil)
    }
    
    func test_givenACorrectUserAndPasswordAndEnvironmentNeedsExtraSecurityTrueAndTsec_whenICallRequestGrantingTicket_thenICallCheckDeviceActivation() {
        
        // given
        let loginTransport = LoginTransport(username: "user", password: "pass")
        sut.environmentNeedsExtraSecurity = true
        dummyHeaderInterceptorStore.dummyTsec = "tsec"
        
        // when
        sut.requestGrantingTicket(loginTransport: loginTransport, consumerId: "00000")
        
        // then
        XCTAssert(dummyView.isCalledCheckDeviceActivation == true)
    }
    
    func test_givenACorrectUserAndPasswordAndEnvironmentNeedsExtraSecurityTrueAndTsec_whenICallRequestGrantingTicket_thenICallCheckDeviceActivationAndLoginTransportMatch() {
        
        // given
        let loginTransport = LoginTransport(username: "user", password: "pass")
        sut.environmentNeedsExtraSecurity = true
        dummyHeaderInterceptorStore.dummyTsec = "tsec"
        
        // when
        sut.requestGrantingTicket(loginTransport: loginTransport, consumerId: "00000")
        
        // then
        XCTAssert(dummyView.loginTransportSent === loginTransport)
    }
    
    func test_givenACorrectUserAndPasswordAndEnvironmentNeedsExtraSecurityTrueAndTsec_whenICallRequestGrantingTicket_thenICallCheckDeviceActivationAndGrantingTicketBOMatch() {
        
        // given
        let loginTransport = LoginTransport(username: "user", password: "pass")
        sut.environmentNeedsExtraSecurity = true
        dummyHeaderInterceptorStore.dummyTsec = "tsec"
        
        // when
        sut.requestGrantingTicket(loginTransport: loginTransport, consumerId: "00000")
        
        // then
        XCTAssert(dummyView.grantingTicketResponseSent === sut.grantingTicketBO)
    }
    
    func test_givenACorrectUserAndPasswordAndEnvironmentNeedsExtraSecurityTrueAndTsec_whenICallRequestGrantingTicket_thenICallCheckDeviceActivationAndTsecMatch() {
        
        // given
        let loginTransport = LoginTransport(username: "user", password: "pass")
        sut.environmentNeedsExtraSecurity = true
        dummyHeaderInterceptorStore.dummyTsec = "tsec"
        
        // when
        sut.requestGrantingTicket(loginTransport: loginTransport, consumerId: "00000")
        
        // then
        XCTAssert(dummyView.tsecSent == dummyHeaderInterceptorStore.dummyTsec)
    }
    
    func test_givenACorrectUserAndPasswordAndEnvironmentNeedsExtraSecurityFalse_whenICallRequestGrantingTicket_thenINotCallCheckDeviceActivation() {

        // given
        let loginTransport = LoginTransport(username: "user", password: "pass")
        sut.environmentNeedsExtraSecurity = false

        // when
        sut.requestGrantingTicket(loginTransport: loginTransport, consumerId: "00000")

        // then
        XCTAssert(dummyView.isCalledCheckDeviceActivation == false)
    }
    
    func test_givenACorrectUserAndPasswordAndEnvironmentNeedsExtraSecurityFalse_whenICallRequestGrantingTicket_thenICallProvideCustomerData() {
        
        // given
        let loginTransport = LoginTransport(username: "user", password: "pass")
        sut.environmentNeedsExtraSecurity = false
        
        // when
        sut.requestGrantingTicket(loginTransport: loginTransport, consumerId: "00000")
        
        // then
        XCTAssert(dummyInteractor.isCalledProvideCustomerData == true)
    }
    
    // MARK: - requestGrantingTicket - provideLoginData Success - provideCards Success
    
    func test_givenACorrectUserAndPasswordAndEnvironmentNeedsExtraSecurityFalse_whenICallRequestGrantingTicketAndTheInteractorCustomerDataIsSuccess_thenCallProvideCards() {
        
        // given
        let loginTransport = LoginTransport(username: "user", password: "pass")
        sut.environmentNeedsExtraSecurity = false
        
        // when
        sut.requestGrantingTicket(loginTransport: loginTransport, consumerId: "00000")
        
        //then
        XCTAssert(dummyInteractor.isCalledProvideCards == true)
    }
    
    func test_givenACorrectUserAndPasswordAndEnvironmentNeedsExtraSecurityFalse_whenICallRequestGrantingTicketAndTheInteractorCardsIsSuccess_thenCallViewToStartUnlockAnimation() {
        
        // given
        let loginTransport = LoginTransport(username: "user", password: "pass")
        sut.environmentNeedsExtraSecurity = false
        
        // when
        sut.requestGrantingTicket(loginTransport: loginTransport, consumerId: "00000")
        
        //then
        XCTAssert(dummyView.isCalledStartUnlockAnimation == true)
    }
    
    func test_givenACorrectUserAndPasswordAndEnvironmentNeedsExtraSecurityFalse_whenICallRequestGrantingTicketAndTheInteractorCardsIsSuccess_thenCardsMatch() {
        
        // given
        let loginTransport = LoginTransport(username: "user", password: "pass")
        sut.environmentNeedsExtraSecurity = false
        
        // when
        sut.requestGrantingTicket(loginTransport: loginTransport, consumerId: "00000")
        
        //then
        XCTAssert(sut.cards === dummyInteractor.cards)
    }
    
    func test_givenACorrectCardsAndNotSavedUserAndEnvironmentNeedsExtraSecurityFalse_whenICallRequestGrantingTicketAndTheInteractorCardsIsSuccess_thenISaveUserIdAndFirstnameAndSaveCustomerIdInTheSessionAndUserLogged() {
        
        //given
        let loginTransport = LoginTransport(username: "user", password: "pass")
        sut.loginTransport = loginTransport
        sut.environmentNeedsExtraSecurity = false
        dummyInteractor.userBO.customerId = "customerId"
        dummyInteractor.userBO.firstName = "firstName"
        
        KeychainManager.shared.resetKeyChainItems()
        
        //when
        sut.requestGrantingTicket(loginTransport: loginTransport, consumerId: "00000")
        
        //then
        XCTAssert(dummySessionManager.customerID == dummyInteractor.userBO.customerId)
        XCTAssert(KeychainManager.shared.getFirstname() == dummyInteractor.userBO.firstName)
        XCTAssert(KeychainManager.shared.getUserId() == loginTransport.userId)
    }
    
    func test_givenACorrectCardsAndSavedUserAndEnvironmentNeedsExtraSecurityFalse_whenICallRequestGrantingTicketAndTheInteractorCardsIsSuccess_thenIDontSaveUserIdAndFirstnameAndISaveCustomerIdInTheSessionAndUserLogged() {
        
        //given
        let loginTransport = LoginTransport(username: "user", password: "pass")
        sut.loginTransport = loginTransport
        sut.environmentNeedsExtraSecurity = false
        dummyInteractor.userBO.customerId = "customerId"
        let firstName = "firstName"
        dummyInteractor.userBO.firstName = firstName
        
        KeychainManager.shared.saveUserId(forUserId: loginTransport.userId)
        KeychainManager.shared.saveFirstname(forFirstname: firstName)
        
        //when
        sut.requestGrantingTicket(loginTransport: loginTransport, consumerId: "00000")
        
        //then
        XCTAssert(KeychainManager.shared.getFirstname() == dummyInteractor.userBO.firstName)
        XCTAssert(KeychainManager.shared.getUserId() == loginTransport.userId)
        XCTAssert(dummySessionManager.customerID == dummyInteractor.userBO.customerId)
    }
    
    func test_givenLoginTransportAndGrantingTicketAndUserAndCardsWithNfcAndEnvironmentNeedsExtraSecurityFalse_whenICallRequestGrantingTicketAndTheInteractorCardsIsSuccess_thenWeHaveCardsWithNfc() {
        
        // given
        let loginTransport = LoginTransport(username: "user", password: "pass")
        sut.environmentNeedsExtraSecurity = false
        
        let cardsBO = CardsGenerator.getSecondCardsBOWithPhysicalSupportNfc()
        dummyInteractor.dummyCards = cardsBO
        
        // when
        sut.requestGrantingTicket(loginTransport: loginTransport, consumerId: "00000")
        
        // then
        XCTAssert(sut.cards?.firstNfcCard() != nil)
    }
    
    func test_givenLoginTransportAndGrantingTicketAndUserAndCardsWithNfcAndEnvironmentNeedsExtraSecurityFalse_whenICallRequestGrantingTicketAndTheInteractorCardsIsSuccess_thenICalledDowngradeService() {
        
        // given
        let loginTransport = LoginTransport(username: "user", password: "pass")
        sut.environmentNeedsExtraSecurity = false
        
        let cardsBO = CardsGenerator.getSecondCardsBOWithPhysicalSupportNfc()
        dummyInteractor.dummyCards = cardsBO
        
        // when
        sut.requestGrantingTicket(loginTransport: loginTransport, consumerId: "00000")
        
        // then
        XCTAssert(dummyView.isCalledDowngradeService == true)
    }
    
    func test_givenLoginTransportAndGrantingTicketAndUserAndCardsWithoutNfcAndEnvironmentNeedsExtraSecurityFalse_whenICallRequestGrantingTicketAndTheInteractorCardsIsSuccess_thenINotCalledDowngradeService() {
        
        // given
        let loginTransport = LoginTransport(username: "user", password: "pass")
        sut.environmentNeedsExtraSecurity = false
        
        let cardsBO = CardsGenerator.getThreeCards()
        dummyInteractor.dummyCards = cardsBO
        
        // when
        sut.requestGrantingTicket(loginTransport: loginTransport, consumerId: "00000")
        
        // then
        XCTAssert(dummyView.isCalledDowngradeService == false)
    }
    
    func test_givenLoginTransportAndGrantingTicketAndUserAndCardsWithNfcAndEnvironmentNeedsExtraSecurityFalse_whenICallRequestGrantingTicketAndTheInteractorCardsIsSuccess_thenICalledDowngradeServiceAndTheParametersAreCorrect() {
        
        // given
        let loginTransport = LoginTransport(username: "user", password: "pass")
        sut.environmentNeedsExtraSecurity = false
        
        let cardsBO = CardsGenerator.getSecondCardsBOWithPhysicalSupportNfc()
        dummyInteractor.dummyCards = cardsBO
        let cardNfc = cardsBO.firstNfcCard()
        
        // when
        sut.requestGrantingTicket(loginTransport: loginTransport, consumerId: "00000")
        
        // then
        XCTAssert(dummyView.cardIdSent == cardNfc?.cardId)
        XCTAssert(dummyView.resetIndicatorSent == true)
    }
    
    // MARK: - requestGrantingTicket - provideLoginData Success - provideCards Failure
    
    func test_givenNotSavedUser_whenICallRequestGrantingTicketAndTheInteractorCardsFailureAndEnvironmentNeedsExtraSecurityFalse_thenIDontSaveUserIdAndFirstnameAndCustomerIdInTheSession() {
        
        //given
        let loginTransport = LoginTransport(username: "user", password: "pass")
        sut.environmentNeedsExtraSecurity = false
        
        dummyInteractor.forceErrorCards = true
        
        //when
        sut.requestGrantingTicket(loginTransport: loginTransport, consumerId: "00000")
        
        //then
        XCTAssert(KeychainManager.shared.getFirstname() == "")
        XCTAssert(KeychainManager.shared.getUserId() == "")
        XCTAssert(dummySessionManager.customerID == nil)
    }
    
    func test_givenACorrectCustomerDataAndEnvironmentNeedsExtraSecurityFalse_whenICallRequestGrantingTicketAndTheInteractorCardsFailure_thenCallRouterGoToLoginWithError() {
        
        //given
        let loginTransport = LoginTransport(username: "user", password: "pass")
        sut.environmentNeedsExtraSecurity = false
        
        dummyInteractor.forceErrorCards = true
        
        //when
        sut.requestGrantingTicket(loginTransport: loginTransport, consumerId: "00000")
        
        //then
        XCTAssert(dummyBusManager.isCalledPublishDataWithTagLoginLoaderAndEventErrorLogin == true)
        XCTAssert(dummyBusManager.saveErrorLogin === dummyInteractor.errorLogin)
    }
    
    // MARK: - deviceActivationSuccess
    
    func test_givenAny_whenICallDeviceActivationSuccess_thenICallProvideCustomerData() {
        
        // given
        
        // when
        sut.deviceActivationSuccess()
        
        // then
        XCTAssert(dummyInteractor.isCalledProvideCustomerData == true)
    }
    
    // MARK: - deviceActivationSuccess - provideCards Success
    
    func test_givenAny_whenICallDeviceActivationSuccessAndTheInteractorCustomerDataIsSuccess_thenCallProvideCards() {
        
        //given
        
        //when
        sut.deviceActivationSuccess()
        
        //then
        XCTAssert(dummyInteractor.isCalledProvideCards == true)
    }
    
    func test_givenAny_whenICallDeviceActivationSuccessAndTheInteractorCardsIsSuccess_thenCallViewToStartUnlockAnimation() {
        
        //given
        
        //when
        sut.deviceActivationSuccess()
        
        //then
        XCTAssert(dummyView.isCalledStartUnlockAnimation == true)
    }
    
    func test_givenAny_whenICallDeviceActivationSuccessAndTheInteractorCardsIsSuccess_thenCardsMatch() {
        
        //given
        
        //when
        sut.deviceActivationSuccess()
        
        //then
        XCTAssert(sut.cards === dummyInteractor.cards)
    }
    
    func test_givenACorrectCardsAndNotSavedUser_whenICallDeviceActivationSuccessAndTheInteractorCardsIsSuccess_thenISaveUserIdAndFirstnameAndSaveCustomerIdInTheSessionAndUserLogged() {
        
        //given
        let loginTransport = LoginTransport(username: "user", password: "pass")
        sut.loginTransport = loginTransport
        dummyInteractor.userBO.customerId = "customerId"
        dummyInteractor.userBO.firstName = "firstName"
        
        KeychainManager.shared.resetKeyChainItems()
        
        //when
        sut.deviceActivationSuccess()
        
        //then
        XCTAssert(dummySessionManager.customerID == dummyInteractor.userBO.customerId)
        XCTAssert(KeychainManager.shared.getFirstname() == dummyInteractor.userBO.firstName)
        XCTAssert(KeychainManager.shared.getUserId() == loginTransport.userId)
    }
    
    func test_givenACorrectCardsAndSavedUser_whenICallDeviceActivationSuccessAndTheInteractorCardsIsSuccess_thenIDontSaveUserIdAndFirstnameAndISaveCustomerIdInTheSessionAndUserLogged() {
        
        //given
        let loginTransport = LoginTransport(username: "user", password: "pass")
        sut.loginTransport = loginTransport
        dummyInteractor.userBO.customerId = "customerId"
        let firstName = "firstName"
        dummyInteractor.userBO.firstName = firstName
        
        KeychainManager.shared.saveUserId(forUserId: loginTransport.userId)
        KeychainManager.shared.saveFirstname(forFirstname: firstName)
        
        //when
        sut.deviceActivationSuccess()
        
        //then
        XCTAssert(KeychainManager.shared.getFirstname() == dummyInteractor.userBO.firstName)
        XCTAssert(KeychainManager.shared.getUserId() == loginTransport.userId)
        XCTAssert(dummySessionManager.customerID == dummyInteractor.userBO.customerId)
    }
    
    func test_givenLoginTransportAndGrantingTicketAndUserAndCardsWithNfc_whenICallDeviceActivationSuccessAndTheInteractorCardsIsSuccess_thenWeHaveCardsWithNfc() {
        
        // given
        let cardsBO = CardsGenerator.getSecondCardsBOWithPhysicalSupportNfc()
        dummyInteractor.dummyCards = cardsBO
        
        // when
        sut.deviceActivationSuccess()
        
        // then
        XCTAssert(sut.cards?.firstNfcCard() != nil)
    }
    
    func test_givenLoginTransportAndGrantingTicketAndUserAndCardsWithNfc_whenICallDeviceActivationSuccessAndTheInteractorCardsIsSuccess_thenICalledDowngradeService() {
        
        // given
        let cardsBO = CardsGenerator.getSecondCardsBOWithPhysicalSupportNfc()
        dummyInteractor.dummyCards = cardsBO
        
        // when
        sut.deviceActivationSuccess()
        
        // then
        XCTAssert(dummyView.isCalledDowngradeService == true)
    }
    
    func test_givenLoginTransportAndGrantingTicketAndUserAndCardsWithoutNfc_whenICallDeviceActivationSuccessAndTheInteractorCardsIsSuccess_thenINotCalledDowngradeService() {
        
        // given
        let cardsBO = CardsGenerator.getThreeCards()
        dummyInteractor.dummyCards = cardsBO
        
        // when
        sut.deviceActivationSuccess()
        
        // then
        XCTAssert(dummyView.isCalledDowngradeService == false)
    }
    
    func test_givenLoginTransportAndGrantingTicketAndUserAndCardsWithNfc_whenICallDeviceActivationSuccessAndTheInteractorCardsIsSuccess_thenICalledDowngradeServiceAndTheParametersAreCorrect() {
        
        // given
        let cardsBO = CardsGenerator.getSecondCardsBOWithPhysicalSupportNfc()
        dummyInteractor.dummyCards = cardsBO
        let cardNfc = cardsBO.firstNfcCard()
        
        // when
        sut.deviceActivationSuccess()
        
        // then
        XCTAssert(dummyView.cardIdSent == cardNfc?.cardId)
        XCTAssert(dummyView.resetIndicatorSent == true)
    }
    
    // MARK: - deviceActivationSuccess - provideCards Failure
    
    func test_givenNotSavedUser_whenICallDeviceActivationSuccessAndTheInteractorCardsFailure_thenIDontSaveUserIdAndFirstnameAndCustomerIdInTheSession() {
        
        //given
        dummyInteractor.forceErrorCards = true
        
        //when
        sut.deviceActivationSuccess()
        
        //then
        XCTAssert(KeychainManager.shared.getFirstname() == "")
        XCTAssert(KeychainManager.shared.getUserId() == "")
        XCTAssert(dummySessionManager.customerID == nil)
    }
    
    func test_givenACorrectCustomerData_whenICallDeviceActivationSuccessAndTheInteractorCardsFailure_thenCallRouterGoToLoginWithError() {
        
        //given
        dummyInteractor.forceErrorCards = true
        
        //when
        sut.deviceActivationSuccess()
        
        //then
        XCTAssert(dummyBusManager.isCalledPublishDataWithTagLoginLoaderAndEventErrorLogin == true)
        XCTAssert(dummyBusManager.saveErrorLogin === dummyInteractor.errorLogin)
    }
    
    //TODO: No testear lo del número de logins porque se va a cambiar en otra tarea PSS2-2935
    
    // MARK: - DummyView
    
    class DummyLoaderViewBGM: LoginLoaderViewProtocolBGM {
        
        var isCalledStartUnlockAnimation = false
        var isCalledShowUnlockTitle = false
        var showUnlockTitleSent = ""
        var isCalledDowngradeService = false
        var cardIdSent = ""
        var resetIndicatorSent = false
        var isCalledCheckDeviceActivation = false
        var loginTransportSent: LoginTransport?
        var grantingTicketResponseSent: GrandTingTicketBO?
        var tsecSent = ""
        
        func showError(error: ModelBO) {
        }
        
        func changeColorEditTexts() {
        }
        
        func startUnlockAnimation() {
            isCalledStartUnlockAnimation = true
        }
        
        func showUnlockTitle(_ title: String) {
            isCalledShowUnlockTitle = true
            showUnlockTitleSent = title
        }
        
        func downgradeService(withCardId cardId: String, andResetIndicator resetIndicator: Bool) {
            isCalledDowngradeService = true
            cardIdSent = cardId
            resetIndicatorSent = resetIndicator
        }
        
        func checkDeviceActivation(loginTransport: LoginTransport, grantingTicketResponse: GrandTingTicketBO, tsec: String) {
            isCalledCheckDeviceActivation = true
            loginTransportSent = loginTransport
            grantingTicketResponseSent = grantingTicketResponse
            tsecSent = tsec
        }
    }
    
    class DummyInteractor: LoginLoaderInteractorProtocolA {
        
        var isCalledProvideLoginData = false
        var isCalledProvideCustomerData = false
        var isCalledProvideAnonymousInformation = false
        var isCalledRequestGrantingTicket = false
        var isCalledProvideConsumerId = false
        var forceErrorCustomerData = false
        var forceErrorCards = false
        var forceErrorGrantingTicket = false
        var forceErrorAnonymousInformation = false
        
        var loginTransportNew: ModelTransport?
        var consumerIdSent = ""
        var errorLogin: ErrorBO?
        
        var isCalledProvideCards = false
        var statusToReturn = 206
        var statusGrantingTicket = 200
        var errorCodeGrantingTicket = "160"
        var cards: CardsBO?
        var dummyCards: CardsBO?
        var userBO: UserBO = UserBO(user: UserEntity())
        var sentAnonymousInformation: AnonymousInformationBO?
        var forceAnonymousInformation: AnonymousInformationBO?
        
        func provideConsumerId() -> Observable<String> {
            
            isCalledProvideConsumerId = true
            return Observable<String>.just("00000" as String)
        }
        
        func provideLoginData(loginTransport: ModelTransport, consumerId: String) -> Observable<ModelBO> {
            
            loginTransportNew = loginTransport
            consumerIdSent = consumerId
            isCalledProvideLoginData = true
            
            let grantingTicket = GrandTingTicketBO(grantingTicket: nil)
            grantingTicket.authenticationState = "OK"
            
            if (loginTransport as? LoginTransport)?.username != "error" {
                return Observable <ModelBO>.just(grantingTicket as ModelBO)
            } else {
                
                var message = "Error"
                
                if statusGrantingTicket == 403 && (errorCodeGrantingTicket == "160" || errorCodeGrantingTicket == "161") {
                    message = Localizables.login.key_login_user_pass_error_message
                }
                
                let errorBO = ErrorBO(error: ErrorEntity(message: message))
                errorBO.status = statusGrantingTicket
                errorBO.code = errorCodeGrantingTicket
                errorLogin = errorBO
                return Observable.error(ServiceError.GenericErrorBO(error: errorBO))
            }
        }
        
        func provideCustomerData() -> Observable<ModelBO> {
            
            isCalledProvideCustomerData = true
            
            if !forceErrorCustomerData {
                return Observable <ModelBO>.just(userBO as ModelBO)
            } else {
                if let error = errorLogin {
                    return Observable.error(ServiceError.GenericErrorBO(error: error))
                } else {
                    let errorBO = ErrorBO(error: ErrorEntity(message: "Error"))
                    errorLogin = errorBO
                    return Observable.error(ServiceError.GenericErrorBO(error: errorBO))
                }
            }
        }
        
        func provideCards() -> Observable <ModelBO> {
            
            self.isCalledProvideCards = true
            
            let dummyCardsEntity = CardsEntity()
            dummyCardsEntity.status = statusToReturn
            
            if let dummyCards = dummyCards {
                self.cards = dummyCards
                
            } else {
                self.cards = CardsGenerator.getCardBOWithAlias()
            }
            
            if !forceErrorCards {
                
                return Observable <ModelBO>.just(self.cards!)
                
            } else {
                
                let errorBO = ErrorBO(error: ErrorEntity(message: "Error"))
                
                errorLogin = errorBO
                return Observable.error(ServiceError.GenericErrorBO(error: errorBO))
                
            }
        }
        
        func provideAnonymousInformation() -> Observable<ModelBO> {
            
            isCalledProvideAnonymousInformation = true
            
            if forceErrorAnonymousInformation {
                
                let errorBO = ErrorBO(error: ErrorEntity(message: "Error"))
                
                errorLogin = errorBO
                return Observable.error(ServiceError.GenericErrorBO(error: errorBO))
                
            } else if let forceAnonymousInformation = forceAnonymousInformation {
                
                return Observable<ModelBO>.just(forceAnonymousInformation)
            } else {
                
                let entity = AnonymousInformationEntity(consumerId: "consumerId", userId: "userId")
                return Observable<ModelBO>.just(AnonymousInformationBO(entity: entity))
            }
            
        }
        
        func requestGrantingTicket(withAnonymousInformation anonymousInformation: AnonymousInformationBO) -> Observable<ModelBO> {
            
            sentAnonymousInformation = anonymousInformation
            isCalledRequestGrantingTicket = true
            
            let grantingTicket = GrandTingTicketBO(grantingTicket: nil)
            grantingTicket.authenticationState = "OK"
            
            if forceErrorGrantingTicket {
                let errorBO = ErrorBO(error: ErrorEntity(message: "Error"))
                errorLogin = errorBO
                return Observable.error(ServiceError.GenericErrorBO(error: errorBO))
            } else {
                return Observable <ModelBO>.just(grantingTicket as ModelBO)
            }
        }
    }
    
    class DummyBusManager: BusManager {
        
        var isCalledPublishDataWithTagLoginLoaderAndEventErrorLogin = false
        var isCalledLoadNextInitFactory = false
        var isCalledNavigateToAnonymousPromotionsSession = false
        var saveErrorLogin: ErrorBO?
        
        override init() {
            super.init()
            routerFactory = RouterInitFactory()
        }
        
        override func navigateScreen<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {
            
            if tag == LoginLoaderPageReaction.ROUTER_TAG && event === LoginLoaderPageReaction.EVENT_LOGIN_FINISHED {
                isCalledLoadNextInitFactory = true
            } else if tag == LoginLoaderPageReaction.ROUTER_TAG && event === LoginLoaderPageReaction.EVENT_ERROR_LOGIN {
                saveErrorLogin = values as? ErrorBO
                isCalledPublishDataWithTagLoginLoaderAndEventErrorLogin = true
            } else if tag == LoginLoaderPageReaction.ROUTER_TAG && event === LoginLoaderPageReaction.EVENT_ANONYMOUS_SESSION {
                isCalledNavigateToAnonymousPromotionsSession = true
            }
        }
    }
    
    class DummyHeaderInterceptorStore: HeaderInterceptorStore {
        
        var dummyTsec = ""
        
        required init() {
            super.init(worker: NetworkWorker())
        }
        
        override func getTsec() -> String? {
            
            return dummyTsec
        }
        
        override func setTsec(tsec: String) {
            dummyTsec = tsec
        }
    }
}
