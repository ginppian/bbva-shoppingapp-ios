//
//  LoginLoaderPresenterATest.swift
//  shoppingappMXTest
//
//  Created by Jesús Ángel Sánchez Sánchez on 14/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest
import RxSwift
import CellsNativeComponents

#if TESTMX
@testable import shoppingappMX
#endif

class LoginLoaderPresenterATest: XCTestCase {
    
    var sut: LoginLoaderPresenterA<DummyLoaderViewA>!
    var dummyLoaderViewA = DummyLoaderViewA()
    var dummyBusManager: DummyBusManager!
    var dummySessionManager: DummySessionManager!
    private var dummyPreferencesManager: DummyPreferencesManager!
    
    override func setUp() {
        super.setUp()
        dummyBusManager = DummyBusManager()
        sut = LoginLoaderPresenterA<DummyLoaderViewA>(busManager: dummyBusManager)
        sut.view = dummyLoaderViewA
        
        DummySessionManager.setupMock()
        dummySessionManager = (SessionDataManager.sessionDataInstance() as! DummySessionManager)
        
        KeychainManagerTestHelper.configureKeychainManagerForTesting()
        
        DummyPreferencesManager.configureDummyPreferences()
        dummyPreferencesManager = PreferencesManager.instance as? DummyPreferencesManager
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    // MARK: - viewDidLoad
    
    func test_givenLoginTransport_whenICallViewDidLoad_thenSelectedEnvironmentAndHostMatch() {
        
        // given
        let dummyInteractorA = DummyInteractorA()
        sut.interactor = dummyInteractorA
        let loginTransport = LoginTransport(username: Settings.Login.userMock, password: Settings.Login.userPasswordMock)
        sut.loginTransport = loginTransport
        
        // when
        sut.viewDidLoad()
        
        let expectedServerHost = SessionDataManager.sessionDataInstance().serverHost!
        
        // then
        XCTAssert(ServerHostURL.selectedEnvironment == .artichoke)
        XCTAssert(ServerHostURL.serverHost == expectedServerHost)
    }
    
    func test_givenLoginTransportAndGrantingTicketAndUserAndCardsWithNfc_whenICallViewDidLoadAndTheInteractorCardsIsSuccess_thenWeHaveCardsWithNfc() {
        
        // given
        let dummyInteractorA = DummyInteractorA()
        let cardsBO = CardsGenerator.getSecondCardsBOWithPhysicalSupportNfc()
        dummyInteractorA.dummyCards = cardsBO
        let loginTransport = LoginTransport(username: "user", password: "pass")
        sut.interactor = dummyInteractorA
        sut.loginTransport = loginTransport
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(sut.cards?.firstNfcCard() != nil)
    }
    
    func test_givenLoginTransportAndGrantingTicketAndUserAndCardsWithNfc_whenICallViewDidLoadAndTheInteractorCardsIsSuccess_thenICalledDowngradeService() {
        
        // given
        let dummyInteractorA = DummyInteractorA()
        let cardsBO = CardsGenerator.getSecondCardsBOWithPhysicalSupportNfc()
        dummyInteractorA.dummyCards = cardsBO
        let loginTransport = LoginTransport(username: "user", password: "pass")
        sut.interactor = dummyInteractorA
        sut.loginTransport = loginTransport
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyLoaderViewA.isCalledDowngradeService == true)
    }
    
    func test_givenLoginTransportAndGrantingTicketAndUserAndCardsWithoutNfc_whenICallViewDidLoadAndTheInteractorCardsIsSuccess_thenINotCalledDowngradeService() {
        
        // given
        let dummyInteractorA = DummyInteractorA()
        let cardsBO = CardsGenerator.getThreeCards()
        dummyInteractorA.dummyCards = cardsBO
        let loginTransport = LoginTransport(username: "user", password: "pass")
        sut.interactor = dummyInteractorA
        sut.loginTransport = loginTransport
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyLoaderViewA.isCalledDowngradeService == false)
    }
    
    func test_givenLoginTransportAndGrantingTicketAndUserAndCardsWithNfc_whenICallViewDidLoadAndTheInteractorCardsIsSuccess_thenICalledDowngradeServiceAndTheParametersAreCorrect() {
        
        // given
        let dummyInteractorA = DummyInteractorA()
        let cardsBO = CardsGenerator.getSecondCardsBOWithPhysicalSupportNfc()
        dummyInteractorA.dummyCards = cardsBO
        let cardNfc = cardsBO.firstNfcCard()
        let loginTransport = LoginTransport(username: "user", password: "pass")
        sut.interactor = dummyInteractorA
        sut.loginTransport = loginTransport
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyLoaderViewA.cardIdSent == cardNfc?.cardId)
        XCTAssert(dummyLoaderViewA.resetIndicatorSent == true)
    }
    
    //------------------------
    
    func test_givenACorrectCardsAndPreferencesManagerLoginNumberNil_whenICallViewDidLoadAndTheInteractorCardsIsSuccess_thenPreferencesManagerSaveLoginNumberWith1() {
        
        // given
        let loginTransport = LoginTransport(username: "user", password: "pass")
        sut.loginTransport = loginTransport
        let dummyInteractorA = DummyInteractorA()
        sut.interactor = dummyInteractorA
        
        dummyPreferencesManager.keysWithValues = [PreferencesManagerKeys.kLoginNumber.rawValue: NSNumber() as AnyObject]
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyPreferencesManager.saveValueSentValues[0] as? NSNumber == 1)
        XCTAssert(dummyPreferencesManager.getValueSentKeys[0] == PreferencesManagerKeys.kLoginNumber)
        XCTAssert(dummyPreferencesManager.saveValueSentKeys[0] == PreferencesManagerKeys.kLoginNumber)
    }
    
    func test_givenACorrectCardsAndPreferencesManagerLoginNumber_whenICallViewDidLoadAndTheInteractorCardsIsSuccess_thenPreferencesManagerSaveLoginNumberByAddingOne() {
        
        // given
        let loginTransport = LoginTransport(username: "user", password: "pass")
        sut.loginTransport = loginTransport
        let dummyInteractorA = DummyInteractorA()
        sut.interactor = dummyInteractorA
        
        let firstValue = 2
        dummyPreferencesManager.keysWithValues = [PreferencesManagerKeys.kLoginNumber.rawValue: NSNumber(value: firstValue) as AnyObject]
        let valueExpected = NSNumber(value: firstValue + 1)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyPreferencesManager.saveValueSentValues[0] as? NSNumber == valueExpected)
        XCTAssert(dummyPreferencesManager.getValueSentKeys[0] == PreferencesManagerKeys.kLoginNumber)
        XCTAssert(dummyPreferencesManager.saveValueSentKeys[0] == PreferencesManagerKeys.kLoginNumber)
    }
    
    func test_givenACorrectCardsAndPreferencesManagerNumberLoginsRateAppNil_whenICallViewDidLoadAndTheInteractorCardsIsSuccess_thenPreferencesManagerSaveNumberLoginsRateAppWith1() {
        
        // given
        let loginTransport = LoginTransport(username: "user", password: "pass")
        sut.loginTransport = loginTransport
        let dummyInteractorA = DummyInteractorA()
        sut.interactor = dummyInteractorA
        
        dummyPreferencesManager.keysWithValues = [PreferencesManagerKeys.kNumberLoginsRateApp.rawValue: NSNumber() as AnyObject]
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyPreferencesManager.saveValueSentValues[1] as? NSNumber == 1)
        XCTAssert(dummyPreferencesManager.getValueSentKeys[1] == PreferencesManagerKeys.kNumberLoginsRateApp)
        XCTAssert(dummyPreferencesManager.saveValueSentKeys[1] == PreferencesManagerKeys.kNumberLoginsRateApp)
    }
    
    func test_givenACorrectCardsAndPreferencesManagerNumberLoginsRateApp_whenICallViewDidLoadAndTheInteractorCardsIsSuccess_thenPreferencesManagerSaveNumberLoginsRateAppByAddingOne() {
        
        // given
        let loginTransport = LoginTransport(username: "user", password: "pass")
        sut.loginTransport = loginTransport
        let dummyInteractorA = DummyInteractorA()
        sut.interactor = dummyInteractorA
        
        let firstValue = 2
        dummyPreferencesManager.keysWithValues = [PreferencesManagerKeys.kNumberLoginsRateApp.rawValue: NSNumber(value: firstValue) as AnyObject]
        let valueExpected = NSNumber(value: firstValue + 1)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyPreferencesManager.saveValueSentValues[1] as? NSNumber == valueExpected)
        XCTAssert(dummyPreferencesManager.getValueSentKeys[1] == PreferencesManagerKeys.kNumberLoginsRateApp)
        XCTAssert(dummyPreferencesManager.saveValueSentKeys[1] == PreferencesManagerKeys.kNumberLoginsRateApp)
    }
    
    //------------------------
    
    func test_givenACorrectCards_whenICallViewDidLoadAndTheInteractorCardsIsSuccess_thenCallViewToStartAnimation() {
        
        //given
        let dummyInteractorA = DummyInteractorA()
        let loginTransport = LoginTransport(username: "user", password: "pass")
        sut.interactor = dummyInteractorA
        sut.loginTransport = loginTransport
        
        //when
        sut.viewDidLoad()
        
        //then
        XCTAssert(dummyLoaderViewA.isCalledStartAnimation == true)
    }
    
    //------------------------
    
    func test_givenACorrectGrantingTicketAndCustomerData_whenICallViewDidLoadAndTheInteractorCardsFailure_thenCallRouterGoToLoginWithError() {
        
        //given
        let dummyInteractorA = DummyInteractorA()
        let loginTransport = LoginTransport(username: "user", password: "pass")
        dummyInteractorA.forceErrorCards = true
        sut.interactor = dummyInteractorA
        sut.loginTransport = loginTransport
        
        //when
        sut.viewDidLoad()
        
        //then
        XCTAssert(dummyBusManager.isCalledNavigateToErrorLogin == true)
        XCTAssert(dummyBusManager.saveErrorLogin === dummyInteractorA.errorLogin)
    }
    
    //------------------------
    
    func test_givenACorrectCardsAndNotSavedUser_whenICallViewDidLoadAndTheInteractorCardsIsSuccess_thenISaveUseridAndFirstnameAndSaveCustomeridInTheSessionAndUserLogged() {
        
        //given
        let dummyInteractorA = DummyInteractorA()
        let loginTransport = LoginTransport(username: "user", password: "pass")
        let firstName = "firstname"
        let customerId = "customerId"
        
        dummyInteractorA.userBO.firstName = firstName
        dummyInteractorA.userBO.customerId = customerId
        
        sut.interactor = dummyInteractorA
        sut.loginTransport = loginTransport
        
        //when
        sut.viewDidLoad()
        
        //then
        XCTAssert(KeychainManager.shared.getFirstname() == firstName)
        XCTAssert(KeychainManager.shared.getUserId() == loginTransport.userId)
        XCTAssert(dummySessionManager.customerID == customerId)
    }
    
    func test_givenACorrectCardsAndSavedUser_whenICallViewDidLoadAndTheInteractorCardsIsSuccess_thenISaveUserIdAndFirstnameAndISaveCustomerIdInTheSessionAndUserLogged() {
        
        //given
        let dummyInteractorA = DummyInteractorA()
        let loginTransport = LoginTransport(username: "user", password: "pass")
        let firstName = "firstname"
        let customerId = "customerId"
        
        dummyInteractorA.userBO.customerId = customerId
        dummyInteractorA.userBO.firstName = firstName
        
        sut.interactor = dummyInteractorA
        sut.loginTransport = loginTransport
        
        //when
        sut.viewDidLoad()
        
        //then
        XCTAssert(KeychainManager.shared.getFirstname() == firstName)
        XCTAssert(KeychainManager.shared.getUserId() == loginTransport.userId)
        XCTAssert(KeychainManager.shared.getLastLoggedUserId() == loginTransport.userId)
        XCTAssert(dummySessionManager.customerID == customerId)
    }
    
    func test_givenAInCorrectCardsAndSavedUser_whenICallViewDidLoadAndTheInteractorCardsWithError_thenIDontSaveUserIdAndFirstnameAndCustomerIdInTheSessionAndUserLogged() {
        
        //given
        let dummyInteractorA = DummyInteractorA()
        let loginTransport = LoginTransport(username: "user", password: "pass")
        dummyInteractorA.forceErrorCards = true
        
        let firstName = "firstname"
        KeychainManager.shared.saveUserId(forUserId: loginTransport.userId)
        KeychainManager.shared.saveFirstname(forFirstname: firstName)
        
        sut.interactor = dummyInteractorA
        sut.loginTransport = loginTransport
        
        //when
        sut.viewDidLoad()
        
        //then
        XCTAssert(KeychainManager.shared.getFirstname() == firstName)
        XCTAssert(KeychainManager.shared.getUserId() == loginTransport.userId)
        XCTAssert(dummySessionManager.customerID == nil )
    }
    
    func test_givenAnInCorrectCardsAndNotSavedUser_whenICallViewDidLoadAndTheInteractorCardsIsSuccess_thenIDontSaveUserIdAndFirstnameAndCustomerIdInTheSession() {
        
        //given
        let loginTransport = LoginTransport(username: "user", password: "pass")
        let dummyInteractorA = DummyInteractorA()
        dummyInteractorA.forceErrorCards = true
        
        sut.interactor = dummyInteractorA
        sut.loginTransport = loginTransport
        
        //when
        sut.viewDidLoad()
        
        //then
        XCTAssert(KeychainManager.shared.getFirstname() == "")
        XCTAssert(KeychainManager.shared.getUserId() == "")
        XCTAssert(dummySessionManager.customerID == nil)
    }
    
    // MARK: - DummyView
    
    class DummyLoaderViewA: LoginLoaderViewProtocolA {
        
        var isCalledDowngradeService = false
        var cardIdSent = ""
        var resetIndicatorSent = false
        var isCalledStartAnimation = false
        
        func startUnlockAnimation() {
            isCalledStartAnimation = true
        }
        
        func showUnlockTitle(_ title: String) {
        }
        
        func showError(error: ModelBO) {
        }
        
        func changeColorEditTexts() {
        }
        
        func downgradeService(withCardId cardId: String, andResetIndicator resetIndicator: Bool) {
            isCalledDowngradeService = true
            cardIdSent = cardId
            resetIndicatorSent = resetIndicator
        }
    }
    
    class DummyInteractorA: LoginLoaderInteractorProtocolA {
        
        var userBO: UserBO = UserBO(user: UserEntity())
        var cards: CardsBO?
        var dummyCards: CardsBO?
        var forceErrorCards = false
        var errorLogin: ErrorBO?
        
        func provideAnonymousInformation() -> Observable<ModelBO> {
            return Observable<ModelBO>.just(EmptyModelBO())
        }
        
        func requestGrantingTicket(withAnonymousInformation anonymousInformation: AnonymousInformationBO) -> Observable<ModelBO> {
            return Observable<ModelBO>.just(EmptyModelBO())
        }
        
        //---------
        
        func provideConsumerId() -> Observable<String> {
            return Observable<String>.just("00000" as String)
        }
        
        func provideLoginData(loginTransport: ModelTransport, consumerId: String) -> Observable<ModelBO> {
            
            let grantingTicket = GrandTingTicketBO(grantingTicket: nil)
            grantingTicket.authenticationState = "OK"
            
            if (loginTransport as? LoginTransport)?.username != "error" {
                return Observable <ModelBO>.just(grantingTicket as ModelBO)
            }
            
            return Observable<ModelBO>.just(EmptyModelBO())
        }
        
        func provideCustomerData() -> Observable<ModelBO> {
            return Observable <ModelBO>.just(userBO as ModelBO)
        }
        
        func provideCards() -> Observable<ModelBO> {
            
            if let dummyCards = dummyCards {
                cards = dummyCards
            } else {
                cards = CardsGenerator.getCardBOWithAlias()
            }
            
            if !forceErrorCards {
                
                return Observable <ModelBO>.just(self.cards!)
                
            } else {
                
                let errorBO = ErrorBO(error: ErrorEntity(message: "Error"))
                
                errorLogin = errorBO
                return Observable.error(ServiceError.GenericErrorBO(error: errorBO))
            }
        }
    }
    
    class DummyBusManager: BusManager {
        
        var isCalledNavigateToErrorLogin = false
        var saveErrorLogin: ErrorBO?
        
        override init() {
            super.init()
            routerFactory = RouterInitFactory()
        }
        
        override func navigateScreen<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {
            
            if tag == LoginLoaderPageReaction.ROUTER_TAG && event === LoginLoaderPageReaction.EVENT_ERROR_LOGIN {
                saveErrorLogin = values as? ErrorBO
                isCalledNavigateToErrorLogin = true
            }
        }
    }
}
