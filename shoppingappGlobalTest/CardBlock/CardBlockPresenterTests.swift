//
//  CardBlockPresenterTests.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 17/7/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest
import CellsNativeComponents

#if TESTMX
@testable import shoppingappMX
#elseif TESTPE
@testable import shoppingappPE
#endif

class CardBlockPresenterTests: XCTestCase {

    var sut: CardBlockPresenter<CardBlockPresenterViewProtocolDummy>!
    var dummyBusManager: DummyBusManager!
    var dummyView: CardBlockPresenterViewProtocolDummy!

    override func setUp() {
        super.setUp()
        dummyBusManager = DummyBusManager()
        sut = CardBlockPresenter<CardBlockPresenterViewProtocolDummy>(busManager: dummyBusManager)

        dummyView = CardBlockPresenterViewProtocolDummy()
        sut.view = dummyView
    }

    // MARK: - setModel

    func test_givenErrorBO_whenICallSetModel_thenErrorBOBeFilled() {

        // given
        let errorBO = ErrorBO(error: ErrorEntity(message: "Error"))

        // when
        sut.setModel(model: errorBO)

        // then
        XCTAssert(sut.errorBO === errorBO)

    }

    func test_givenCardBlockObjectCells_whenICallSetModel_thenBlockCardObjectCellsBeFilled() {

        // given
        let blockCardObjectCells = RequestBlockCardObjectCells(id: "id", typeBlock: "STOLEN")

        // when
        sut.setModel(model: blockCardObjectCells)

        // then
        XCTAssert(sut.blockCardObjectCells?.id == blockCardObjectCells.id)
        XCTAssert(sut.blockCardObjectCells?.typeBlock == blockCardObjectCells.typeBlock)

    }

    // MARK: - showPageSuccess

    func test_givenBlockCardObjectCellsAndBlockCardDTO_whenICallShowPageSuccess_thenICalledNavigateToBlockCardSuccessPage() {

        //given
        let blockCardObjectCells = RequestBlockCardObjectCells(id: "id", typeBlock: "STOLEN")
        sut.blockCardObjectCells = blockCardObjectCells

        let blockCardDTO = BlockCardDTO()
        blockCardDTO.data = BlockCardData()
        blockCardDTO.data?.blockId = "id"
        blockCardDTO.data?.reference = "1234"
        let blockCardBO = BlockCardBO(blockCardDTO: blockCardDTO)

        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        //when
        sut.showPageSuccess(modelBO: blockCardBO)

        //then
        XCTAssert(dummyBusManager.isCalledNavigateToBlockCardSuccessPage == true)

    }

    func test_givenBlockCardObjectCellsAndBlockCardDTO_whenICallShowPageSuccess_thenICalledPublishParamToBlockCardSuccessPage() {

        //given
        let blockCardObjectCells = RequestBlockCardObjectCells(id: "id", typeBlock: "STOLEN")
        sut.blockCardObjectCells = blockCardObjectCells
        
        let blockCardDTO = BlockCardDTO()
        blockCardDTO.data = BlockCardData()
        blockCardDTO.data?.blockId = "id"
        blockCardDTO.data?.reference = "1234"
        let blockCardBO = BlockCardBO(blockCardDTO: blockCardDTO)

        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        //when
        sut.showPageSuccess(modelBO: blockCardBO)

        //then
        XCTAssert(dummyBusManager.isCalledPublishParamBlockCardSuccess == true)

    }

    func test_givenBlockCardObjectCellsAndBlockCardDTO_whenICallShowPageSuccess_thenPublishParamMatch() {

        //given
        let blockCardObjectCells = RequestBlockCardObjectCells(id: "id", typeBlock: "STOLEN")
        sut.blockCardObjectCells = blockCardObjectCells

        let blockCardDTO = BlockCardDTO()
        blockCardDTO.data = BlockCardData()
        blockCardDTO.data?.blockId = "id"
        blockCardDTO.data?.reference = "1234"
        blockCardDTO.data?.blockDate = "2018-07-18T06:52:04.695Z"
        let blockCardBO = BlockCardBO(blockCardDTO: blockCardDTO)

        let date = Date.getDateFormatForCellsModule(withDate: blockCardDTO.data?.blockDate ?? "")

        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        //when
        sut.showPageSuccess(modelBO: blockCardBO)

        //then
        XCTAssert(dummyBusManager.blockCardSuccessSent.cardNumber == String(sut.cardBO!.number.suffix(4)))
        XCTAssert(dummyBusManager.blockCardSuccessSent.folio == blockCardDTO.data?.reference)
        XCTAssert(dummyBusManager.blockCardSuccessSent.date == date)

    }
    
    func test_givenBlockCardObjectCellsAndBlockCardDTO_whenICallShowPageSuccess_thenSaveResultSuccessFromBlockDTO() {
        
        //given
        let blockCardObjectCells = RequestBlockCardObjectCells(id: "id", typeBlock: "STOLEN")
        sut.blockCardObjectCells = blockCardObjectCells
        
        let blockCardDTO = BlockCardDTO()
        blockCardDTO.data = BlockCardData()
        blockCardDTO.data?.blockId = "id"
        blockCardDTO.data?.reference = "1234"
        blockCardDTO.data?.blockDate = "2018-07-18T06:52:04.695Z"
        let blockCardBO = BlockCardBO(blockCardDTO: blockCardDTO)

        let date = Date.getDateFormatForCellsModule(withDate: blockCardDTO.data?.blockDate ?? "")
        let folio = blockCardDTO.data?.reference ?? ""
        
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO
        
        //when
        sut.showPageSuccess(modelBO: blockCardBO)
        
        //then
        XCTAssert(sut.successResult?.cardNumber == String(cardBO.number.suffix(4)))
        XCTAssert(sut.successResult?.date == String(date))
        XCTAssert(sut.successResult?.folio == String(folio))
        
    }

    func test_givendBlockCardDTOAndCardOperationInfo_whenICallShowPageSuccess_thenCallPublishOperationCardInfoSucess() {

        //given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        let blockCardDTO = BlockCardDTO()
        blockCardDTO.data = BlockCardData()
        blockCardDTO.data?.blockId = "id"
        blockCardDTO.data?.reference = "1234"
        blockCardDTO.data?.blockDate = "2018-07-18T06:52:04.695Z"
        let blockCardBO = BlockCardBO(blockCardDTO: blockCardDTO)

        //when
        sut.showPageSuccess(modelBO: blockCardBO)

        let cardOperationInfo = CardOperationInfo(cardID: "111", fisicalCardID: nil, cardType: CardDataType.digital, cardOperation: .block)
        sut.cardOperationInfo = cardOperationInfo

        // when
        sut.showPageSuccess(modelBO: blockCardBO)

        //then
        XCTAssert(dummyBusManager.cardOperationInfo?.cardID == cardOperationInfo.cardID)
        XCTAssert(dummyBusManager.cardOperationInfo?.fisicalCardID == cardOperationInfo.fisicalCardID)
        XCTAssert(dummyBusManager.cardOperationInfo?.cardType == cardOperationInfo.cardType)
        XCTAssert(dummyBusManager.cardOperationInfo?.cardOperation == cardOperationInfo.cardOperation)

    }

    // MARK: - registerOmniture

    func test_givenPage_whenICallRegisterOmniture_thenICallViewSendTrackCellsPageCardBlockInfo() {

        // given
        let page = CardBlockPageReaction.CELLS_PAGE_CARD_BLOCK_INFO

        // when
        sut.registerOmniture(page: page)

        // then
        XCTAssert(dummyView.isCalledSendTrackCellsPageCardBlockInfo == true)

    }

    func test_givenPage_whenICallRegisterOmniture_thenICallViewSendTrackCellsCardBlockType() {

        // given
        let page = CardBlockPageReaction.CELLS_PAGE_CARD_BLOCK_TYPE

        // when
        sut.registerOmniture(page: page)

        // then
        XCTAssert(dummyView.isCalledSendTrackCellsPageCardBlockType == true)

    }

    func test_givenPage_whenICallRegisterOmniture_thenICallViewSendTrackCellsPageCardBlockSuccess() {

        // given
        let page = CardBlockPageReaction.CELLS_PAGE_BLOCK_CARD_SUCCESS

        // when
        sut.registerOmniture(page: page)

        // then
        XCTAssert(dummyView.isCalledSendTrackCellsPageCardBlockSuccess == true)

    }

    // MARK: - viewWasDissmised

    func test_givenSuccessOk_whenICallViewWasDismiss_thenCallPublishNeededRefreshCards() {

        //given
        sut.successResult = BlockCardSuccess(cardNumber: "111", date: "111", folio: "111")

        //when
        sut.viewWasDismissed()

        //then
        XCTAssert(dummyBusManager.isCalledPublishNeededCardsRefresh == true)

    }

    class CardBlockPresenterViewProtocolDummy: CardBlockViewProtocol {
        
        var isCalledSendTrackCellsPageCardBlockInfo = false
        var isCalledSendTrackCellsPageCardBlockType = false
        var isCalledSendTrackCellsPageCardBlockSuccess = false

        func showError(error: ModelBO) {
        }

        func changeColorEditTexts() {
        }

        func sendTrackCellsPageCardBlockInfo() {
            isCalledSendTrackCellsPageCardBlockInfo = true
        }

        func sendTrackCellsPageCardBlockType() {
            isCalledSendTrackCellsPageCardBlockType = true
        }

        func sendTrackCellsPageCardBlockSuccess() {
            isCalledSendTrackCellsPageCardBlockSuccess = true
        }
    }

    class DummyBusManager: BusManager {

        var isCalledNavigateToBlockCardSuccessPage = false
        var isCalledPublishParamBlockCardSuccess = false
        var isCalledPublishNeededCardsRefresh = false
        var isCalledPublishParamBlockCardInfoSuccess = false

        var blockCardSuccessSent: BlockCardSuccess!
        var cardOperationInfo: CardOperationInfo?

        override init() {
            super.init()
            routerFactory = RouterInitFactory()
        }

        override func navigateScreen<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {

            if tag == CardBlockPageReaction.ROUTER_TAG && event === CardBlockPageReaction.EVENT_BLOCK_CARD_SUCCESS {
                isCalledNavigateToBlockCardSuccessPage = true
            }
        }

        override func publishData<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {

            if tag == CardBlockPageReaction.ROUTER_TAG && event === CardBlockPageReaction.PARAMS_BLOCK_CARD_SUCCESS {
                isCalledPublishParamBlockCardSuccess = true
                blockCardSuccessSent = BlockCardSuccess.deserialize(from: values as? [String: Any])
            } else if tag == CardBlockPageReaction.ROUTER_TAG && event === CardsPageReaction.PARAMS_OPERATION_CARD_INFO_SUCCESS {
                isCalledPublishParamBlockCardInfoSuccess = true
                cardOperationInfo = values as? CardOperationInfo
            }
        }

        override func notify<T>(tag: String, _ event: ActionSpec<T>) {
            if tag == CardsPageReaction.ROUTER_TAG && event === CardsPageReaction.NEEDED_CARDS_REFRESH {
                isCalledPublishNeededCardsRefresh = true
            }
        }
    }

}
