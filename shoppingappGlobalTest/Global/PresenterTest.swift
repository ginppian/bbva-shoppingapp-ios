//
//  PresenterTest.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 13/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class PresenterTest: XCTestCase {

    var sut: BasePresenter<LoginViewDummy>!
    var dummyBusManager: DummyBusManager!
    var dummySessionManager: DummySessionManager!

    override func setUp() {
        super.setUp()

        dummyBusManager = DummyBusManager()
        sut = BasePresenter<LoginViewDummy>(busManager: dummyBusManager)

        DummySessionManager.setupMock()
        dummySessionManager = (SessionDataManager.sessionDataInstance() as! DummySessionManager)
    }

    override func tearDown() {
        super.tearDown()
    }

    func test_givenAnErrorBOAndIsErrorShownFalse_whenICallCheckError_thenCallShowErrorInView() {

        //given
        let dummyView = LoginViewDummy()
        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        sut.errorBO = errorBO
        sut.view = dummyView

        //when
        sut.checkError()

        //then
        XCTAssert(dummyView.isCalledShowError == true)
    }

    func test_givenAnErrorBO_whenICallCheckError_thenmessageIsSameInView() {

        //given
        let dummyView = LoginViewDummy()
        sut.view = dummyView

        let entity = ErrorEntity(message: "Error en el password")
        entity.code = "80"
        entity.httpStatus = 404
        let errorBO = ErrorBO(error: entity)
        sut.errorBO = errorBO

        //when
        sut.checkError()

        //then
        XCTAssertEqual(dummyView.errorBO?.errorMessage(), errorBO.errorMessage())
    }

    func test_givenNoErrorBO_whenICallCheckError_thenNotCallShowErrorInView() {

        //given
        let dummyView = LoginViewDummy()
        sut.view = dummyView

        //when
        sut.checkError()

        //then
        XCTAssert(dummyView.isCalledShowError == false)
    }

    func test_givenAnErrorBO_whenErrorBOIsShownIsTrueThenNotCallShowModal() {
        //given
        let dummyView = LoginViewDummy()
        sut.view = dummyView

        let entity = ErrorEntity(message: "Error en el password")
        entity.code = "80"
        entity.httpStatus = 404
        let errorBO = ErrorBO(error: entity)
        errorBO.isErrorShown = true
        sut.errorBO = errorBO

        //when
        sut.checkError()

        //then
        XCTAssert(dummyView.isCalledShowError == false)
    }

    func test_givenErroBOWithHttpStatus403_whenICallChecError_thenCallCloseSessionOfSessionManager() {

        // given
        let errorBO = ErrorBO(error: ErrorEntity(message: "Sesión expirada"))

        errorBO.isErrorShown = true
        errorBO.status = ConstantsHTTPCodes.STATUS_403
        errorBO.code = ErrorCode.expiredSession.rawValue
        sut.errorBO = errorBO

        // when
        sut.checkError()

        // then
        XCTAssert(dummySessionManager.isCalledCloseSession == true)

    }

    func test_givenErroBOWithAnyHttpStatusDifferentFrom403_whenICallCheckError_thenDoNotCallCloseSessionOfSessionManager() {

        // given
        let errorBO = ErrorBO(error: ErrorEntity(message: "Sesión expirada"))
        errorBO.isErrorShown = true
        errorBO.status = 402
        sut.errorBO = errorBO

        // when
        sut.checkError()

        // then
        XCTAssert(dummySessionManager.isCalledCloseSession == false)

    }

    func test_givenErroBOWithHttpStatus403_whenICallCheckError_thenCallPresentLoginOfRouter() {

        // given
        let errorBO = ErrorBO(error: ErrorEntity(message: "Sesión expirada"))
        errorBO.isErrorShown = true
        errorBO.status = ConstantsHTTPCodes.STATUS_403
        errorBO.code = ErrorCode.expiredSession.rawValue
        sut.errorBO = errorBO

        // when
        sut.checkError()

        // then
        XCTAssert(dummyBusManager.isCalledExpiredSession == true)

    }

    func test_givenErroBOWithAnyHttpStatusDifferentFrom403And402_whenICallError_thenDoNotCallPresentLoginOfRouter() {

        // given
        let errorBO = ErrorBO(error: ErrorEntity(message: "Sesión expirada"))
        errorBO.isErrorShown = true
        errorBO.status = 402
        sut.errorBO = errorBO

        // when
        sut.checkError()

        // then
        XCTAssert(dummyBusManager.isCalledExpiredSession == false)

    }
    func test_givenAny_whenICallShowLoading_thenICallBusManagerShowLoading() {

        // when
        sut.showLoading(completion: nil)

        // then
        XCTAssert(dummyBusManager.isCalledShowLoading == true)

    }

    func test_givenAny_whenICallHideLLoading_thenICallBusManagerHideLoading() {

        // when
        sut.hideLoading(completion: nil)

        // then
        XCTAssert(dummyBusManager.isCalledHideLoading == true)

    }

    class LoginViewDummy: ViewProtocol {

        var isCalledShowError = false
        var isCalledChangeColorEditTexts = false

        var errorBO: ErrorBO?

        func showError(error: ModelBO) {
            errorBO = error as? ErrorBO
            isCalledShowError = true
        }

        func changeColorEditTexts() {
            isCalledChangeColorEditTexts = true
        }

    }

    class DummyBusManager: BusManager {

        var isCalledExpiredSession = false
        var isCalledShowLoading = false
        var isCalledHideLoading = false

        override init() {
            super.init()
            routerFactory = RouterInitFactory()
        }

        override func expiredSession() {
            isCalledExpiredSession = true
        }
        override func showLoading(completion: (() -> Void)?) {
            completion?()
            isCalledShowLoading = true
        }

        override func hideLoading(completion: (() -> Void)?) {

            isCalledHideLoading = true
        }
    }

}
