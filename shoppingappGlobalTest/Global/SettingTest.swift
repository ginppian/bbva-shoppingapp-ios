//
//  SettingTest.swift
//  shoppingapp
//
//  Created by Marcos on 14/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTPE
    @testable import shoppingappPE
#endif

class SettingTest: XCTestCase {

    struct UnexpectedNilError: Error {}

    var promotion: PromotionBO!

    override func setUp() {
        super.setUp()
        let promotionsBO = PromotionsGenerator.getPromotionsOneDayLeftBO()
        promotion = promotionsBO.promotions[0]
    }

    override func tearDown() {
        super.tearDown()
    }

    // MARK: Promotions

    func test_givenAPromotionBOThatStartIn6DaysAndEndIn8Days_whenICallEffectiveDateText_thenTextIsNull() {

        //givenpromotion.startDate
        promotion.startDate = Calendar.current.date(byAdding: .day, value: 6, to: Date())
        promotion.endDate = Calendar.current.date(byAdding: .day, value: 8, to: Date())

        //when
        let outputText = Settings.Promotions.effectiveDateText(forPromotionBO: promotion, compactSize: false)?.date

        //then
        XCTAssert(outputText == nil)

    }

    func test_givenAPromotionBOThatStartIn5DaysAndEndIn8Days_whenICallEffectiveDateText_thenTextIsStartInAndIsNotInterval() throws {

        //given
        promotion.startDate = Calendar.current.date(byAdding: .day, value: 5, to: Date())
        promotion.endDate = Calendar.current.date(byAdding: .day, value: 8, to: Date())
        let formattedDate = promotion.startDate.string(format: Date.DATE_FORMAT_DAY_MONTH_YEAR_COMPACT)

        //when
        let outputText = Settings.Promotions.effectiveDateText(forPromotionBO: promotion, compactSize: false)

        //then
         XCTAssert(outputText?.date == Localizables.promotions.key_starts_in_text +  " " + formattedDate)
        XCTAssert(outputText?.isInterval == false)

    }

    func test_givenAPromotionBOThatStartIn5DaysAndEndIn8DaysAndCompactSize_whenICallEffectiveDateText_thenTextIsStartAndIsNotInterval() throws {

        //given
        promotion.startDate = Calendar.current.date(byAdding: .day, value: 5, to: Date())
        promotion.endDate = Calendar.current.date(byAdding: .day, value: 8, to: Date())
        let formattedDate = promotion.startDate.string(format: Date.DATE_FORMAT_DAY_MONTH_YEAR_COMPACT)

        //when
        let outputText = Settings.Promotions.effectiveDateText(forPromotionBO: promotion, compactSize: true)

        //then
        XCTAssert(outputText?.date == Localizables.promotions.key_starts_text +  " " + formattedDate)
        XCTAssert(outputText?.isInterval == false)

    }

    func test_givenAPromotionBOThatStartIn5DaysAndEndIn7Days_whenICallEffectiveDateText_thenTextIsLeftAndIsInterval() {

        //given
        promotion.startDate = Date()
        promotion.endDate = Calendar.current.date(byAdding: .day, value: 7, to: Date())

        //when
        let outputText = Settings.Promotions.effectiveDateText(forPromotionBO: promotion, compactSize: false)

        //then
        XCTAssert(outputText?.date == Localizables.promotions.key_promotions_left_text + " 7 " + Localizables.promotions.key_promotions_days_text)
        XCTAssert(outputText?.isInterval == true)

    }

    func test_givenAPromotionBOThatYetStartedAndEndIn8_whenICallEffectiveDateText_thenTextIsFinishInAndIsNotInterval() throws {

        //given
        promotion.startDate = Date()
        promotion.endDate = Calendar.current.date(byAdding: .day, value: 8, to: Date())
        let formattedDate = promotion.endDate.string(format: Date.DATE_FORMAT_DAY_MONTH_YEAR_COMPACT)

        //when
        let outputText = Settings.Promotions.effectiveDateText(forPromotionBO: promotion, compactSize: false)

        //then
        XCTAssert(outputText?.date == Localizables.promotions.key_finish_in_text +  " " + formattedDate)
        XCTAssert(outputText?.isInterval == false)

    }

    func test_givenAPromotionBOThatYetStartedAndEndIn8AndCompactSize_whenICallEffectiveDateText_thenTextIsFinishAndIsNotInterval() throws {

        //given
        promotion.startDate = Date()
        promotion.endDate = Calendar.current.date(byAdding: .day, value: 8, to: Date())
        let formattedDate = promotion.endDate.string(format: Date.DATE_FORMAT_DAY_MONTH_YEAR_COMPACT)

        //when
        let outputText = Settings.Promotions.effectiveDateText(forPromotionBO: promotion, compactSize: true)

        //then
        XCTAssert(outputText?.date == Localizables.promotions.key_finish_text +  " " + formattedDate)
        XCTAssert(outputText?.isInterval == false)

    }

    func test_givenAPromotionBOThatYetStartedAndEndIn7_whenICallEffectiveDateText_thenTextIsLeftAndIsInterval() {

        //given
        promotion.startDate = Date()
        promotion.endDate = Calendar.current.date(byAdding: .day, value: 7, to: Date())

        //when
        let outputText = Settings.Promotions.effectiveDateText(forPromotionBO: promotion, compactSize: false)

        //then
        XCTAssert(outputText?.date == Localizables.promotions.key_promotions_left_text + " 7 " + Localizables.promotions.key_promotions_days_text)
        XCTAssert(outputText?.isInterval == true)

    }

    func test_givenAPromotionBOThatYetStartedAndEndIn1_whenICallEffectiveDateText_thenTextIsFinishTodayAndIsNotInterval() {

        //given
        promotion.startDate = Date()
        promotion.endDate = Calendar.current.date(byAdding: .hour, value: 1, to: Date(), wrappingComponents: true)

        //when
        let outputText = Settings.Promotions.effectiveDateText(forPromotionBO: promotion, compactSize: false)

        //then
        XCTAssert(outputText?.date == Localizables.promotions.key_finish_today)
        XCTAssert(outputText?.isInterval == false)

    }

}
