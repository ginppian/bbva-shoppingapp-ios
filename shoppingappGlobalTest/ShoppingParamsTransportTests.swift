//
//  ShoppingParamsTransportTests.swift
//  shoppingapp
//
//  Created by jesus.martinez on 30/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
@testable import shoppingappMX
#elseif TESTPE
@testable import shoppingappPE
#endif

class ShoppingParamsTransportTests: XCTestCase {

    // MARK: - ShoppingParamsTranspot queryParams (no values)

    func test_givenNil_whenICallQueryParams_thenShouldReturnNil() {

        // given
        let categoryId: String? = nil
        let sut = ShoppingParamsTransport(categoryId: categoryId)

        // when
        let result = sut.queryParams()

        // then
        XCTAssert(result == nil)

    }

    // MARK: - ShoppingParamsTransport queryParams (categoryId)

    func test_givenCategoryId_whenICallQueryParams_thenShouldReturnCorrectQueryParams() {

        // given
        let categoryId = "AUTO"
        let sut = ShoppingParamsTransport(categoryId: categoryId)

        // when
        let result = sut.queryParams()

        // then
        XCTAssert(result == "categories.id=\(categoryId)")

    }

    // MARK: - ShoppingParamsTransport queryParams (trendingId)

    func test_givenTrendingId_whenICallQueryParams_thenShouldReturnCorrectQueryParams() {

        // given
        let trendingId = "HOT"
        let sut = ShoppingParamsTransport(trendingId: trendingId)

        // when
        let result = sut.queryParams()

        // then
        XCTAssert(result == "trending.id=\(trendingId)")

    }

    func test_givenTrendingIdAndCategoryId_whenICallQueryParams_thenShouldReturnNil() {

        // given
        let trendingId = "HOT"
        let categoryId = "AUTO"
        let sut = ShoppingParamsTransport(categoryId: categoryId, trendingId: trendingId)

        // when
        let result = sut.queryParams()

        // then
        XCTAssert(result == "categories.id=\(categoryId)&trending.id=\(trendingId)")

    }

    // MARK: - ShoppingParamsTransport queryParams (ShoppingParamLocation)

    func test_givenLocationWithOnlyGeolocation_whenICallQueryParams_thenReturnACorrectQueryParams() {

        // given
        let paramLocation = ShoppingParamsGenerator.locationParamWithoutDistanceParam()
        let sut = ShoppingParamsTransport(location: paramLocation)
        let expand = ShoppingExpands(stores: true)

        // when
        let result = sut.queryParams()

        // then
        XCTAssert(result == expand.expands() + "&stores.location.geolocation.latitude=\(sut.location!.geolocation.latitude)&stores.location.geolocation.longitude=\(sut.location!.geolocation.longitude)")
    }

    func test_givenLocationCompleteWithExpands_whenICallQueryParams_thenReturnACorrectQueryParams() {

        // given
        let paramLocation = ShoppingParamsGenerator.locationParam()
        let sut = ShoppingParamsTransport(location: paramLocation)
        let expand = ShoppingExpands(stores: true)

        // when
        let result = sut.queryParams()

        // then
        XCTAssert(result == expand.expands() + "&stores.location.geolocation.latitude=\(sut.location!.geolocation.latitude)&stores.location.geolocation.longitude=\(sut.location!.geolocation.longitude)" + "&" + sut.location!.distanceParam!.queryParams()!)

    }

    func test_givenLocationOnlyGeolocationAndCategoryIdWithExpands_whenICallQueryParams_thenReturnACorrectQueryParams() {

        // given
        let categoryId = "AUTO"
        let paramLocation = ShoppingParamsGenerator.locationParamWithoutDistanceParam()
        let sut = ShoppingParamsTransport(categoryId: categoryId, location: paramLocation)
        let expand = ShoppingExpands(stores: true)

        // when
        let result = sut.queryParams()

        // then
        XCTAssert(result == expand.expands() + "&stores.location.geolocation.latitude=\(sut.location!.geolocation.latitude)&stores.location.geolocation.longitude=\(sut.location!.geolocation.longitude)" + "&categories.id=\(categoryId)")

    }

    func test_givenLocationCompleteWithExpandsAndCategoryId_whenICallQueryParams_thenReturnACorrectQueryParams() {

        // given
        let categoryId = "AUTO"
        let paramLocation = ShoppingParamsGenerator.locationParam()
        let sut = ShoppingParamsTransport(categoryId: categoryId, location: paramLocation)
        let expand = ShoppingExpands(stores: true)

        // when
        let result = sut.queryParams()

        // then
        XCTAssert(result == expand.expands() + "&stores.location.geolocation.latitude=\(sut.location!.geolocation.latitude)&stores.location.geolocation.longitude=\(sut.location!.geolocation.longitude)" + "&" + sut.location!.distanceParam!.queryParams()! + "&categories.id=\(categoryId)")
    }

    func test_givenTrendingIdAndCategoryIdAndLocationCompleteWithExpands_whenICallQueryParams_thenShouldReturnNil() {

        // given
        let trendingId = "HOT"
        let categoryId = "AUTO"
        let paramLocation = ShoppingParamsGenerator.locationParam()
        let sut = ShoppingParamsTransport(categoryId: categoryId, trendingId: trendingId, location: paramLocation)
        let expand = ShoppingExpands(stores: true)

        // when
        let result = sut.queryParams()

        // then
        XCTAssert(result == expand.expands() + "&stores.location.geolocation.latitude=\(sut.location!.geolocation.latitude)&stores.location.geolocation.longitude=\(sut.location!.geolocation.longitude)" + "&" + sut.location!.distanceParam!.queryParams()! + "&categories.id=\(categoryId)&trending.id=\(trendingId)")

    }

    // MARK: - ShoppingParamsTransport queryParams (pageSize)

    func test_givenAPageSize_whenICallQueryParams_thenShouldReturnCorrectQueryParams() {

        // given
        let categoryId: String? = nil
        let pageSize = 20
        let sut = ShoppingParamsTransport(categoryId: categoryId, trendingId: nil, location: nil, pageSize: pageSize)

        // when
        let result = sut.queryParams()

        // then
        XCTAssert(result == "pageSize=\(pageSize)")

    }

    func test_givenCategoryIdAndPageSize_whenICallQueryParams_thenShouldReturnCorrectQueryParams() {

        // given
        let categoryId = "AUTO"
        let pageSize = 20
        let sut = ShoppingParamsTransport(categoryId: categoryId, trendingId: nil, location: nil, pageSize: pageSize)

        // when
        let result = sut.queryParams()

        // then
        XCTAssert(result == "categories.id=\(categoryId)" + "&pageSize=\(pageSize)")

    }

    func test_givenTrendingIdAndPageSize_whenICallQueryParams_thenShouldReturnCorrectQueryParams() {

        // given
        let trendingId = "HOT"
        let pageSize = 20
        let sut = ShoppingParamsTransport(trendingId: trendingId, pageSize: pageSize)

        // when
        let result = sut.queryParams()

        // then
        XCTAssert(result == "trending.id=\(trendingId)" + "&pageSize=\(pageSize)")

    }

    func test_givenLocationCompleteWithExpandsAndPageSize_whenICallQueryParams_thenReturnACorrectQueryParams() {

        // given
        let paramLocation = ShoppingParamsGenerator.locationParam()
        let pageSize = 20
        let sut = ShoppingParamsTransport(location: paramLocation, pageSize: pageSize)
        let expand = ShoppingExpands(stores: true)

        // when
        let result = sut.queryParams()

        // then
        XCTAssert(result == expand.expands() + "&stores.location.geolocation.latitude=\(sut.location!.geolocation.latitude)&stores.location.geolocation.longitude=\(sut.location!.geolocation.longitude)" + "&" + sut.location!.distanceParam!.queryParams()! + "&pageSize=\(pageSize)")

    }

    func test_givenTrendingIdAndCategoryIdAndLocationCompleteAndPageSizeWithExpands_whenICallQueryParams_thenShouldReturnNil() {

        // given
        let trendingId = "HOT"
        let categoryId = "AUTO"
        let paramLocation = ShoppingParamsGenerator.locationParam()
        let pageSize = 20
        let sut = ShoppingParamsTransport(categoryId: categoryId, trendingId: trendingId, location: paramLocation, pageSize: pageSize)
        let expand = ShoppingExpands(stores: true)

        // when
        let result = sut.queryParams()

        // then
        XCTAssert(result == expand.expands() + "&stores.location.geolocation.latitude=\(sut.location!.geolocation.latitude)&stores.location.geolocation.longitude=\(sut.location!.geolocation.longitude)" + "&" + sut.location!.distanceParam!.queryParams()! + "&categories.id=\(categoryId)&trending.id=\(trendingId)" + "&pageSize=\(pageSize)")

    }

    // MARK: - ShoppingParamsTransport queryParams (orderBy)

    func test_givenOrdeBy_whenICallQueryParams_thenShouldReturnCorrectQueryParams() {

        // given
        let categoryId: String? = nil
        let pageSize: Int? = nil
        let orderBy: OrderBy = .endDate
        let sut = ShoppingParamsTransport(categoryId: categoryId, trendingId: nil, location: nil, pageSize: pageSize, orderBy: orderBy)

        // when
        let result = sut.queryParams()

        // then
        XCTAssert(result == "orderBy=\(orderBy.rawValue)")

    }

    func test_givenAPageSizeAndOrdeBy_whenICallQueryParams_thenShouldReturnCorrectQueryParams() {

        // given
        let categoryId: String? = nil
        let pageSize = 20
        let orderBy: OrderBy = .endDate
        let sut = ShoppingParamsTransport(categoryId: categoryId, trendingId: nil, location: nil, pageSize: pageSize, orderBy: orderBy)

        // when
        let result = sut.queryParams()

        // then
        XCTAssert(result ==  "pageSize=\(pageSize)" + "&orderBy=\(orderBy.rawValue)")

    }

    func test_givenCategoryIdAndPageSizeAndOrdeBy_whenICallQueryParams_thenShouldReturnCorrectQueryParams() {

        // given
        let categoryId = "AUTO"
        let pageSize = 20
        let orderBy: OrderBy = .endDate
        let sut = ShoppingParamsTransport(categoryId: categoryId, trendingId: nil, location: nil, pageSize: pageSize, orderBy: orderBy)

        // when
        let result = sut.queryParams()

        // then
        XCTAssert(result == "categories.id=\(categoryId)" + "&pageSize=\(pageSize)" + "&orderBy=\(orderBy.rawValue)")

    }

    func test_givenTrendingIdAndPageSizeAndOrdeBy_whenICallQueryParams_thenShouldReturnCorrectQueryParams() {

        // given
        let trendingId = "HOT"
        let pageSize = 20
        let orderBy: OrderBy = .endDate
        let sut = ShoppingParamsTransport(categoryId: nil, trendingId: trendingId, location: nil, pageSize: pageSize, orderBy: orderBy)

        // when
        let result = sut.queryParams()

        // then
        XCTAssert(result == "trending.id=\(trendingId)" + "&pageSize=\(pageSize)" + "&orderBy=\(orderBy.rawValue)")

    }

    func test_givenTrendingIdAndOrdeBy_whenICallQueryParams_thenShouldReturnCorrectQueryParams() {

        // given
        let trendingId = "HOT"
        let orderBy: OrderBy = .endDate
        let sut = ShoppingParamsTransport(categoryId: nil, trendingId: trendingId, location: nil, pageSize: nil, orderBy: orderBy)

        // when
        let result = sut.queryParams()

        // then
        XCTAssert(result == "trending.id=\(trendingId)" + "&orderBy=\(orderBy.rawValue)")

    }

    func test_givenLocationCompleteWithExpandsAndPageSizeAndOrdeBy_whenICallQueryParams_thenReturnACorrectQueryParams() {

        // given
        let paramLocation = ShoppingParamsGenerator.locationParam()
        let pageSize = 20
        let orderBy: OrderBy = .endDate
        let sut = ShoppingParamsTransport(location: paramLocation, pageSize: pageSize, orderBy: orderBy)
        let expand = ShoppingExpands(stores: true)

        // when
        let result = sut.queryParams()

        // then
        XCTAssert(result == expand.expands() + "&stores.location.geolocation.latitude=\(sut.location!.geolocation.latitude)&stores.location.geolocation.longitude=\(sut.location!.geolocation.longitude)" + "&" + sut.location!.distanceParam!.queryParams()! + "&pageSize=\(pageSize)" + "&orderBy=\(orderBy.rawValue)")

    }

    func test_givenTrendingIdAndCategoryIdAndLocationCompleteAndPageSizeAndOrdeByWithExpands_whenICallQueryParams_thenShouldReturnNil() {

        // given
        let trendingId = "HOT"
        let categoryId = "AUTO"
        let paramLocation = ShoppingParamsGenerator.locationParam()
        let pageSize = 20
        let orderBy: OrderBy = .endDate
        let sut = ShoppingParamsTransport(categoryId: categoryId, trendingId: trendingId, location: paramLocation, pageSize: pageSize, orderBy: orderBy)
        let expand = ShoppingExpands(stores: true)

        // when
        let result = sut.queryParams()

        // then
        XCTAssert(result == expand.expands() + "&stores.location.geolocation.latitude=\(sut.location!.geolocation.latitude)&stores.location.geolocation.longitude=\(sut.location!.geolocation.longitude)" + "&" + sut.location!.distanceParam!.queryParams()! + "&categories.id=\(categoryId)&trending.id=\(trendingId)" + "&pageSize=\(pageSize)" + "&orderBy=\(orderBy.rawValue)")

    }

    // MARK: - ShoppingParamsTransport queryParams (titleIds)

    func test_givenTitleIds_whenICallQueryParams_thenReturnACorrectQueryParams() {

        // given
        let sut = ShoppingParamsTransport(titleIds: ["XA", "DA"])

        // when
        let result = sut.queryParams()

        // then
        XCTAssert(result == "title.id=in=(XA,DA)")
    }

    func test_givenTitleIdsOnlyOne_whenICallQueryParams_thenReturnACorrectQueryParams() {

        // given
        let sut = ShoppingParamsTransport(titleIds: ["XA"])

        // when
        let result = sut.queryParams()

        // then
        XCTAssert(result == "title.id=in=(XA)")
    }

    func test_givenTitleIdsEmtpy_whenICallQueryParams_thenReturnACorrectQueryParams() {

        // given
        let sut = ShoppingParamsTransport(titleIds: [])

        // when
        let result = sut.queryParams()

        // then
        XCTAssert(result == nil)
    }

    func test_givenTitleIdsRepited_whenICallQueryParams_thenReturnACorrectQueryParams() {

        // given
        let sut = ShoppingParamsTransport(titleIds: ["XA", "DA", "DA", "DA", "XA", "DA", "XA", "XA"])

        // when
        let result = sut.queryParams()

        // then
        XCTAssert(result == "title.id=in=(XA,DA)")
    }

    func test_givenTitleIdsAndOrderBy_whenICallQueryParams_thenShouldReturnCorrectQueryParams() {

        // given
        let orderBy: OrderBy = .endDate
        let sut = ShoppingParamsTransport(titleIds: ["XA", "DA"], orderBy: orderBy)

        // when
        let result = sut.queryParams()

        // then
        XCTAssert(result == "title.id=in=(XA,DA)&orderBy=\(orderBy.rawValue)")
    }

    func test_givenTitleIdsAndPageSizeAndOrderBy_whenICallQueryParams_thenShouldReturnCorrectQueryParams() {

        // given
        let pageSize = 20
        let orderBy: OrderBy = .endDate
        let sut = ShoppingParamsTransport(titleIds: ["XA", "DA"], pageSize: pageSize, orderBy: orderBy)

        // when
        let result = sut.queryParams()

        // then
        XCTAssert(result == "title.id=in=(XA,DA)" + "&pageSize=\(pageSize)" + "&orderBy=\(orderBy.rawValue)")
    }

    func test_givenTitleIdsandCategoryIdAndPageSizeAndOrdeBy_whenICallQueryParams_thenShouldReturnCorrectQueryParams() {

        // given
        let categoryId = "AUTO"
        let pageSize = 20
        let orderBy: OrderBy = .endDate
        let sut = ShoppingParamsTransport(categoryId: categoryId, titleIds: ["XA", "DA"], pageSize: pageSize, orderBy: orderBy)

        // when
        let result = sut.queryParams()

        // then
        XCTAssert(result == "categories.id=\(categoryId)" + "&title.id=in=(XA,DA)" + "&pageSize=\(pageSize)" + "&orderBy=\(orderBy.rawValue)")

    }

    func test_givenTitleIdsAndTrendingIdAndPageSizeAndOrdeBy_whenICallQueryParams_thenShouldReturnCorrectQueryParams() {

        // given
        let trendingId = "HOT"
        let pageSize = 20
        let orderBy: OrderBy = .endDate
        let sut = ShoppingParamsTransport(trendingId: trendingId, titleIds: ["XA", "DA"], pageSize: pageSize, orderBy: orderBy)

        // when
        let result = sut.queryParams()

        // then
        XCTAssert(result == "trending.id=\(trendingId)" + "&title.id=in=(XA,DA)" + "&pageSize=\(pageSize)" + "&orderBy=\(orderBy.rawValue)")

    }

    func test_givenTitleIdsAndTrendingIdAndCategoryIdAndLocationCompleteAndPageSizeAndOrdeByWithExpands_whenICallQueryParams_thenShouldReturnNil() {

        // given
        let trendingId = "HOT"
        let categoryId = "AUTO"
        let paramLocation = ShoppingParamsGenerator.locationParam()
        let pageSize = 20
        let orderBy: OrderBy = .endDate
        let sut = ShoppingParamsTransport(categoryId: categoryId, trendingId: trendingId, location: paramLocation, titleIds: ["XA", "DA"], pageSize: pageSize, orderBy: orderBy)
        let expand = ShoppingExpands(stores: true)

        // when
        let result = sut.queryParams()

        // then
        XCTAssert(result == expand.expands() + "&stores.location.geolocation.latitude=\(sut.location!.geolocation.latitude)&stores.location.geolocation.longitude=\(sut.location!.geolocation.longitude)" + "&" + sut.location!.distanceParam!.queryParams()! + "&categories.id=\(categoryId)&trending.id=\(trendingId)" + "&title.id=in=(XA,DA)" + "&pageSize=\(pageSize)" + "&orderBy=\(orderBy.rawValue)")

    }

    // MARK: - ShoppingParamLocation queryParams

    func test_givenGeolocationBO_whenICallQueryParams_thenReturnACorrectQueryParams() {

        // given
        let sut = ShoppingParamsGenerator.locationParamWithoutDistanceParam()

        // when
        let result = sut.queryParams()

        // then
        XCTAssert(result == "stores.location.geolocation.latitude=\(sut.geolocation.latitude)&stores.location.geolocation.longitude=\(sut.geolocation.longitude)")

    }

    func test_givenGeolocationBOAndDistanceParam_whenICallQueryParams_thenReturnACorrectQueryParams() {

        // given
        let sut = ShoppingParamsGenerator.locationParam()

        // when
        let result = sut.queryParams()

        // then
        XCTAssert(result == "stores.location.geolocation.latitude=\(sut.geolocation.latitude)&stores.location.geolocation.longitude=\(sut.geolocation.longitude)" + "&" + sut.distanceParam!.queryParams()!)

    }

    // MARK: - ShoppingDistanceParam queryParams

    func test_givenDistanceAndLengthType_whenICallQueryParams_thenReturnACorrectQueryParams() {

        // given
        let sut = ShoppingParamsGenerator.distanceParam()

        // when
        let result = sut.queryParams()

        // then
        XCTAssert(result == "stores.distance.length=\(sut.distance)&stores.distance.lengthType.id=\(sut.lengthType)")

    }
    
    // MARK: - ShoppingExpands expands

    func test_givenStoresExpands_whenICallExpands_thenShouldReturnCorrectExpands() {
        
        // given
        let expand = ShoppingExpands(stores: true)
        
        // when
        let result = expand.expands()
        
        // then
        XCTAssert(result == "expand=stores")
    }
    
    func test_givenCardsExpands_whenICallExpands_thenShouldReturnCorrectExpands() {
        
        // given
        let expand = ShoppingExpands(cards: true)
        
        // when
        let result = expand.expands()
        
        // then
        XCTAssert(result == "expand=cards")
    }
    
    func test_givenContactDetailsExpands_whenICallExpands_thenShouldReturnCorrectExpands() {
        
        // given
        let expand = ShoppingExpands(contactDetails: true)
        
        // when
        let result = expand.expands()
        
        // then
        XCTAssert(result == "expand=contact-details")
    }
    
    func test_givenOnlyTwoExpands_whenICallExpands_thenShouldReturnCorrectExpands() {
        
        // given
        let expand = ShoppingExpands(stores: true, cards: false, contactDetails: true)
        
        // when
        let result = expand.expands()
        
        // then
        XCTAssert(result == "expand=stores,contact-details")
    }
    
    func test_givenAllExpands_whenICallExpands_thenShouldReturnCorrectExpands() {
        
        // given
        let expand = ShoppingExpands(stores: true, cards: true, contactDetails: true)
        
        // when
        let result = expand.expands()
        
        // then
        XCTAssert(result == "expand=stores,cards,contact-details")
    }
    
    // MARK: - ShoppingDetailParamsTransport queryParams (ShoppingParamLocation)
    
    func test_givenIdPromoAndLocationWithoutExpands_whenICallQueryParams_thenReturnNil() {
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        let paramLocation = ShoppingParamsGenerator.locationParam()
        let sut = ShoppingDetailParamsTransport(idPromo: promotionBO.id, location: paramLocation)
        
        // when
        let result = sut.queryParams()
        
        // then
        XCTAssert(result == nil)
    }
    
    func test_givenIdPromoAndLocationWithoutDistanceParamWithExpands_whenICallQueryParams_thenReturnACorrectShoppingDetailQueryParams() {
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        let paramLocation = ShoppingParamsGenerator.locationParamWithoutDistanceParam()
        let expands = ShoppingExpands(stores: true, cards: true, contactDetails: true)
        let sut = ShoppingDetailParamsTransport(idPromo: promotionBO.id, location: paramLocation, expands: expands)
        
        // when
        let result = sut.queryParams()
        
        // then
        XCTAssert(result == expands.expands() + "&stores.location.geolocation.latitude=\(sut.location!.geolocation.latitude)&stores.location.geolocation.longitude=\(sut.location!.geolocation.longitude)")
    }
    
    func test_givenLocationCompleteWithExpandsAndPromotionId_whenICallQueryParams_thenReturnACorrectShoppingDetailQueryParams() {
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        let paramLocation = ShoppingParamsGenerator.locationParam()
        let expands = ShoppingExpands(stores: true, cards: true, contactDetails: true)
        let sut = ShoppingDetailParamsTransport(idPromo: promotionBO.id, location: paramLocation, expands: expands)
        
        // when
        let result = sut.queryParams()
        
        // then
        XCTAssert(result == expands.expands() + "&stores.location.geolocation.latitude=\(sut.location!.geolocation.latitude)&stores.location.geolocation.longitude=\(sut.location!.geolocation.longitude)" + "&" + sut.location!.distanceParam!.queryParams()!)
        
    }
    
    func test_givenAllExpandsAndLocationCompleteAndTitleIds_whenICallQueryParams_thenShouldReturnCorrectQueryParams() {
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        let cards = CardsGenerator.getCardBOWithAlias()
        let expands = ShoppingExpands(stores: true, cards: true, contactDetails: true)
        let paramLocation = ShoppingParamsGenerator.locationParam()
        var titleIdsParam = String()
        if let titleIds = cards.titleIds(), !titleIds.isEmpty {
            
            let titleIdsFormatted = titleIds.uniques().joined(separator: ",")
            titleIdsParam = titleIdsFormatted
        }
        
        let sut = ShoppingDetailParamsTransport(idPromo: promotionBO.id, location: paramLocation, expands: expands, titleIds: cards.titleIds())
        let params = expands.expands() + "&stores.location.geolocation.latitude=\(sut.location!.geolocation.latitude)&stores.location.geolocation.longitude=\(sut.location!.geolocation.longitude)" + "&" + sut.location!.distanceParam!.queryParams()! + "&title.id=in=(\(titleIdsParam))"
        // when
        let result = sut.queryParams()
        
        // then
        XCTAssert(result == params)
        
    }
}
