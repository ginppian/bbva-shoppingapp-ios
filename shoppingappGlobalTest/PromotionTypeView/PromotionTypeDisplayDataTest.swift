//
//  PromotionTypeDisplayDataTest.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 09/01/2018.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class PromotionTypeDisplayDataTest: XCTestCase {

    func test_givenAPromotionBOWithPromotionTypeRate_whenICallGeneratePromotionTypeDisplayData_thenIDisplayCorrectData() {

        //given
        let promotionBO = PromotionsGenerator.getPromotionsBOWithPromotionType()

        //When
         let displayData = PromotionTypeDisplayData(fromPromotion: promotionBO.promotions[0])

        // then
        XCTAssert(displayData.promotionTypeBackgroundColor == .BBVADARKPINK )
        XCTAssert(displayData.promotionTypeText == promotionBO.promotions[0].promotionType.name.uppercased())
        XCTAssert(displayData.promotionTypeViewWidth == 98.0)
        XCTAssert(displayData.promotionTypeLabelWidth == 78.0)

    }
    func test_givenAPromotionBOWithPromotionTypeFixed_whenICallGeneratePromotionTypeDisplayData_thenIDisplayCorrectData() {

        //given
        let promotionBO = PromotionsGenerator.getPromotionsBOWithPromotionType()

        //When
        let displayData = PromotionTypeDisplayData(fromPromotion: promotionBO.promotions[3])

        // then
        XCTAssert(displayData.promotionTypeBackgroundColor == .BBVADARKPINK )
        XCTAssert(displayData.promotionTypeText == promotionBO.promotions[3].promotionType.name.uppercased())
        XCTAssert(displayData.promotionTypeViewWidth == 98.0)
        XCTAssert(displayData.promotionTypeLabelWidth == 78.0)

    }
    func test_givenAPromotionBOWithPromotionTypeGift_whenICallGeneratePromotionTypeDisplayData_thenIDisplayCorrectData() {

        //given
        let promotionBO = PromotionsGenerator.getPromotionsBOWithPromotionType()

        //When
        let displayData = PromotionTypeDisplayData(fromPromotion: promotionBO.promotions[4])

        // then
        XCTAssert(displayData.promotionTypeBackgroundColor == .BBVADARKPINK )
        XCTAssert(displayData.promotionTypeText == promotionBO.promotions[4].promotionType.name.uppercased())
        XCTAssert(displayData.promotionTypeViewWidth == 98.0)
        XCTAssert(displayData.promotionTypeLabelWidth == 78.0)

    }
    func test_givenAPromotionBOWithPromotionTypeCashback_whenICallGeneratePromotionTypeDisplayData_thenIDisplayCorrectData() {

        //given
        let promotionBO = PromotionsGenerator.getPromotionsBOWithPromotionType()

        //When
        let displayData = PromotionTypeDisplayData(fromPromotion: promotionBO.promotions[5])

        // then
        XCTAssert(displayData.promotionTypeBackgroundColor == .BBVADARKPINK )
        XCTAssert(displayData.promotionTypeText == promotionBO.promotions[5].promotionType.name.uppercased())
        XCTAssert(displayData.promotionTypeViewWidth == 98.0)
        XCTAssert(displayData.promotionTypeLabelWidth == 78.0)

    }
    func test_givenAPromotionBOWithPromotionTypePostPayment_whenICallGeneratePromotionTypeDisplayData_thenIDisplayCorrectData() {

        //given
        let promotionBO = PromotionsGenerator.getPromotionsBOWithPromotionType()

        //When
        let displayData = PromotionTypeDisplayData(fromPromotion: promotionBO.promotions[1])

        // then
        XCTAssert(displayData.promotionTypeBackgroundColor == .BBVAMEDIUMYELLOW )
        XCTAssert(displayData.promotionTypeText == promotionBO.promotions[1].promotionType.name.uppercased())
        XCTAssert(displayData.promotionTypeViewWidth == 77.0)
        XCTAssert(displayData.promotionTypeLabelWidth == 57.0)

    }
    func test_givenAPromotionBOWithPromotionTypeLoyaltyPoints_whenICallGeneratePromotionTypeDisplayData_thenIDisplayCorrectData() {

        //given
        let promotionBO = PromotionsGenerator.getPromotionsBOWithPromotionType()

        //When
        let displayData = PromotionTypeDisplayData(fromPromotion: promotionBO.promotions[2])

        // then
        XCTAssert(displayData.promotionTypeBackgroundColor == .DARKAQUA)
        XCTAssert(displayData.promotionTypeText == promotionBO.promotions[2].promotionType.name.uppercased())
        XCTAssert(displayData.promotionTypeViewWidth == 77.0)
        XCTAssert(displayData.promotionTypeLabelWidth == 57.0)

    }
    func test_givenAPromotionBOWithPromotionTypeDifferentThanFixedAndRateAndCashbackAndGiftAndPostPaymentAndLoyaltypoint_whenICallGeneratePromotionTypeDisplayData_thenIDisplayCorrectData() {

        //given
        let promotionBO = PromotionsGenerator.getPromotionsBOWithPromotionType()

        //When
        let displayData = PromotionTypeDisplayData(fromPromotion: promotionBO.promotions[7])

        // then
        XCTAssert(displayData.promotionTypeBackgroundColor == .clear )
        XCTAssert(displayData.promotionTypeText == nil)
        XCTAssert(displayData.promotionTypeViewWidth == 0.0)
        XCTAssert(displayData.promotionTypeLabelWidth == 0.0)

    }
}
