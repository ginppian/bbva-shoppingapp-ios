//
//  TransactionDetailPresenterTest.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 21/11/2017.
//  Copyright © 2017 BBVA. All rights reserved.
//

import XCTest
import CellsNativeComponents

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class TicketDetailPresenterTest: XCTestCase {

    var sut: TicketDetailPresenter<DummyView>!
    var dummyView: DummyView!
    var dummyBusManager: DummyBusManager!
    var dummyPreferencesManager: DummyPreferencesManager!

    override func setUp() {
        super.setUp()
        
        DummyPreferencesManager.configureDummyPreferences()
        dummyPreferencesManager = PreferencesManager.instance as? DummyPreferencesManager
        
        dummyBusManager = DummyBusManager()
        sut = TicketDetailPresenter<DummyView>(busManager: dummyBusManager)
        
        dummyView = DummyView()
        sut.view = dummyView
    }

    // MARK: - viewDidLoad

    func test_givenAnAcceptedPurchase_whenICallViewDidLoad_thenICallViewSendAcceptedPurchase() {
        
        // given
        sut.ticketModel = TicketsGenerator.acceptedPurchase()

        // when
        sut.viewDidLoad()

        // then
        XCTAssert(dummyView.isCalledSendOminuteAcceptedPurchase == true)
    }

    func test_givenADeniedPurchase_whenICallViewDidLoad_thenICallViewSendDeniedPurchase() {

        // given
        sut.ticketModel = TicketsGenerator.deniedPurchase()

        // when
        sut.viewDidLoad()

        // then
        XCTAssert(dummyView.isCalledSendOminuteDeniedPurchase == true)
    }

    func test_givenNothing_whenICallViewDidLoad_thenINotCallViewSendAcceptedPurchaseAndINotCallViewSendDeniedPurchase() {

        // given

        // when
        sut.viewDidLoad()

        // then
        XCTAssert(dummyView.isCalledSendOminuteAcceptedPurchase == false)
        XCTAssert(dummyView.isCalledSendOminuteDeniedPurchase == false)
    }
    
    func test_givenAnyTicketModel_whenICallViewDidLoad_thenICallPreferencesManagerGetValue() {
        
        //given
        sut.ticketModel = TicketsGenerator.acceptedPurchase()
        
        //when
        sut.viewDidLoad()
        
        //then
        XCTAssert(dummyPreferencesManager.isCalledGetValueWithPreferencesKey == true)
    }
    
    func test_givenAnyTicketModel_whenICallViewDidLoad_thenICallPreferencesManagerGetValueWithRightKey() {
        
        //given
        sut.ticketModel = TicketsGenerator.acceptedPurchase()
        
        //when
        sut.viewDidLoad()
        
        //then
        XCTAssert(dummyPreferencesManager.getValueSentKey == .kConfigVibrationEnabled)
    }
    
    func test_givenAnyTicketModelAndSavedVibrationEnabledInPreferencesManager_whenICallViewDidLoad_thenICallViewMakeDeviceVibrate() {
        
        //given
        sut.ticketModel = TicketsGenerator.acceptedPurchase()
        dummyPreferencesManager.keysWithValues = [PreferencesManagerKeys.kConfigVibrationEnabled.rawValue: true as AnyObject]
        
        //when
        sut.viewDidLoad()
        
        //then
        XCTAssert(dummyView.isCalledMakeDeviceVibrate == true)
    }

    // MARK: - viewWillAppear

    func test_givenAPresenter_whenICallViewWillAppear_thenShowOperativeBarInfo() {

        // given

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowOperativeBarInfo == true)
    }

    func test_givenAPresenter_whenICallViewWillAppear_thenShowOperativeBarInfoWithDisplayData() {

        // given
        
        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.displayDataOperativesBarInfo != nil)
    }
    
    func test_givenPresenter_whenICallViewWillAppear_thenCallView() {
        
        // given
        
        // when
        sut.viewWillAppear()
        
        // then
        XCTAssert(dummyView.isCallShowDisclaimerData == true)
    }
    
    func test_givenPresenter_whenICallViewWillAppear_thenHaveValue() {
        
        // given
        
        // when
        sut.viewWillAppear()
        
        // then
        XCTAssert(dummyView.displayDataDisclaimer != nil)
    }
    
    func test_givenPresenter_whenICallViewWillAppear_thenICallViewShowTicketData() {
        
        // given
        
        let stringTicket = "{\"aps\":{\"alert\":{\"loc-args\":[\"01424\",\"481515\",\"SUSHI TAI\",\"3.21\",\"MXP\",\"160722084808\",\"EDW PLAT\"],\"loc-key\":\"00001\"},\"sound\":\"default\"}}"
        
        let jsonData = stringTicket.data(using: .utf8)
        let dictionaryTicket = try? JSONSerialization.jsonObject(with: jsonData!, options: .mutableLeaves)
        
        sut.ticketModel = TicketModel(withPayload: dictionaryTicket as! [AnyHashable: Any])
        
        // when
        sut.viewWillAppear()
        
        // then
        XCTAssert(dummyView.isCallShowTicketData == true)
    }
    
    func test_givenPresenter_whenICallViewWillAppear_thenDisplayDataIsNotNil() {
        
        // given
        
        let stringTicket = "{\"aps\":{\"alert\":{\"loc-args\":[\"01424\",\"481515\",\"SUSHI TAI\",\"3.21\",\"MXP\",\"160722084808\",\"EDW PLAT\"],\"loc-key\":\"00001\"},\"sound\":\"default\"}}"
        
        let jsonData = stringTicket.data(using: .utf8)
        let dictionaryTicket = try? JSONSerialization.jsonObject(with: jsonData!, options: .mutableLeaves)
        
        sut.ticketModel = TicketModel(withPayload: dictionaryTicket as! [AnyHashable: Any])
        
        // when
        sut.viewWillAppear()
        
        // then
        XCTAssert(dummyView.displayDataTicket != nil)
    }
    
    // MARK: - buttonBarOptionPressed

    func test_givenButtonBarDisplayData_whenICallButtonBarOptionPressedWithDisplayItem_thenICallViewShowCallBBVA() {

        // given

        let dummyButtonBarDisplayData = ButtonBarDisplayData(imageNamed: "imageNamed", titleKey: "titleKey", optionKey: .callBBVA)

        // when
        sut.buttonBarOptionPressed(withDisplayItem: dummyButtonBarDisplayData)

        // then
        XCTAssert(dummyView.isCalledShowCallBBVA == true)
    }

    func test_givenButtonBarDisplayDataWithCallOption_whenICallButtonBarOptionPressedWithDisplayItem_thenICallViewShowCallBBVAWithSettingsTelephone() {

        // given

        let dummyButtonBarDisplayData = ButtonBarDisplayData(imageNamed: "imageNamed", titleKey: "titleKey", optionKey: .callBBVA)

        // when
        sut.buttonBarOptionPressed(withDisplayItem: dummyButtonBarDisplayData)

        // then
        XCTAssert(dummyView.telephone == Settings.Global.default_bbva_phone_number)
    }

    func test_whenShowNotOperationPressed_thenICalledNavigateNotRecognizedOperationPage() {

        //given

        //when
        sut.setNotRecognizedOperations()

        //then
        XCTAssert(dummyBusManager.isCalledNavigateToNotRecognizedOperationPage == true)
    }

    func test_givenAndNotRecognizedOptionTransport_whenSetNotRecognizedOperations_thenICalledPublishDataForNotRecognizedOptionsAndTheSameTransport() {

        //given
        let notRecognizedOperationTransport = NotRecognizedOptionsTransport(isBlockHidden: true, isPowerOffHidden: true, isCallHidden: false, isLoginHidden: false)

        //when
        sut.setNotRecognizedOperations()

        //then
        XCTAssert(dummyBusManager.notRecognizedOptionsTransport.isCallHidden == notRecognizedOperationTransport.isCallHidden)
        XCTAssert(dummyBusManager.notRecognizedOptionsTransport.isPowerOffHidden == notRecognizedOperationTransport.isPowerOffHidden)
        XCTAssert(dummyBusManager.notRecognizedOptionsTransport.isBlockHidden == notRecognizedOperationTransport.isBlockHidden)
        XCTAssert(dummyBusManager.notRecognizedOptionsTransport.isLoginHidden == notRecognizedOperationTransport.isLoginHidden)
    }

    class DummyView: TicketDetailViewProtocol {

        var isCalledShowOperativeBarInfo: Bool = false
        var displayDataOperativesBarInfo: ButtonsBarDisplayData?
        var isCalledButtonBarOptionPressed: Bool = false
        var displayDataButtonBar: ButtonBarDisplayData?
        var isCalledShowCallBBVA: Bool = false
        var isCallShowDisclaimerData: Bool = false
        var displayDataDisclaimer: DisclaimerDisplayData?
        var isCallShowTicketData: Bool = false
        var displayDataTicket: TicketDisplayData?
        var isCalledMakeDeviceVibrate = false
        var telephone = "telephone"
        var isCalledSendOminuteAcceptedPurchase = false
        var isCalledSendOminuteDeniedPurchase = false

        func showError(error: ModelBO) {
        }

        func changeColorEditTexts() {
        }

        func showOperativeBarInfo(withDisplayData displayData: ButtonsBarDisplayData) {
            isCalledShowOperativeBarInfo = true
            displayDataOperativesBarInfo = displayData
        }

        func buttonBarOptionPressed(withDisplayItem displayItem: ButtonBarDisplayData) {
            isCalledButtonBarOptionPressed = true
            displayDataButtonBar = displayItem
        }

        func showCallBBVA(tel: String) {
            isCalledShowCallBBVA = true
            telephone = tel
        }

        func showDisclaimerData(withDisplayData displayData: DisclaimerDisplayData) {
            isCallShowDisclaimerData = true
            displayDataDisclaimer = displayData
        }

        func showTicketData(withDisplayData displayData: TicketDisplayData) {
            isCallShowTicketData = true
            displayDataTicket = displayData
        }
        
        func makeDeviceVibrate() {
            isCalledMakeDeviceVibrate = true
        }

        func sendOmnitureAcceptedPurchase() {
            isCalledSendOminuteAcceptedPurchase = true
        }

        func sendOmnitureDeniedPurchase() {
            isCalledSendOminuteDeniedPurchase = true
        }
    }

    class DummyBusManager: BusManager {

        var isCalledNavigateToNotRecognizedOperationPage = false
        var isCalledPublishDataForNotRecognizedOptions = false

        var dummyTag: String = ""
        var dummyEvent: ActionSpec<Any>?

        var notRecognizedOptionsTransport: NotRecognizedOptionsTransport!

        override init() {

            super.init()
            routerFactory = RouterInitFactory()
        }

        override func navigateScreen<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {

            if tag == TicketDetailViewController.ID && event === TransactionDetailPageReaction.ACTION_NAVIGATE_TO_NOT_OPERATION {

                isCalledNavigateToNotRecognizedOperationPage = true
            }
        }

        override func publishData<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {

            if tag == TicketDetailPageReaction.ROUTER_TAG && event === TransactionDetailPageReaction.PARAMS_NOT_RECOGNIZED_OPTIONS {

                isCalledPublishDataForNotRecognizedOptions = true
                notRecognizedOptionsTransport = NotRecognizedOptionsTransport.deserialize(from: values as? [String: Any])
            }

            dummyTag = tag
            dummyEvent = event as? ActionSpec<Any>
        }
    }
}
