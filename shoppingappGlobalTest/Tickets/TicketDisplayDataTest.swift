//
//  TicketDisplayDataTest.swift
//  shoppingapp
//
//  Created by ruben.fernandez on 8/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class TicketDisplayDataTest: XCTestCase {

    override func setUp() {
        super.setUp()

        let dummyPreferencesManager = DummyPreferencesManager()
        PreferencesManager.instance = dummyPreferencesManager
    }

    func test_givenATicketModelAccepted_whenCallGenerateTicketDisplayData_thenHaveCorrectValues() {

        //given
        let stringTicket = "{\"aps\":{\"alert\":{\"loc-args\":[\"01424\",\"481515\",\"SUSHI TAI\",\"3.21\",\"MXP\",\"160722084808\",\"EDW PLAT\"],\"loc-key\":\"00001\"},\"sound\":\"default\"}}"

        let jsonData = stringTicket.data(using: .utf8)
        let dictionaryTicket = try? JSONSerialization.jsonObject(with: jsonData!, options: .mutableLeaves)

        let ticketModel = TicketModel(withPayload: dictionaryTicket as! [AnyHashable: Any])

        //when
        let ticketDisplayData = TicketDisplayData.generateTicketDisplayData(withTicketModel: ticketModel)

        let formatter = AmountFormatter(codeCurrency: ticketModel.transactionCurrency)

        let decimalNumber = NSDecimalNumber(string: ticketModel.transactionAmount)
        
        let ticketDate = Date.date(fromServerString: ticketModel.receiptDate, withFormat: Date.DATE_FORMAT_PUSH)
        let stringDate = Date.string(fromDate: ticketDate!, withFormat: Date.DATE_FORMAT_PUSH_FORMATTED)

        //then

        XCTAssert(ticketDisplayData.pan == "•" + "01424")
        XCTAssert(ticketDisplayData.concept == "SUSHI TAI")
        XCTAssert(ticketDisplayData.date == stringDate)

        XCTAssert(ticketDisplayData.amount == formatter.attributtedText(forAmount: decimalNumber, bigFontSize: 48, smallFontSize: 23))

        XCTAssert(ticketDisplayData.type == Localizables.common.key_gcm_notification_accepted_purchase_title)
        XCTAssert(ticketDisplayData.typeColor == UIColor.BBVAGREEN56)
        XCTAssert(ticketDisplayData.image != nil)

    }

    func test_givenATicketModelAcceptedAndHaveImageSave_whenCallGenerateTicketDisplayData_thenHaveCoverUrl() {

        //given
        let stringTicket = "{\"aps\":{\"alert\":{\"loc-args\":[\"01424\",\"481515\",\"SUSHI TAI\",\"3.21\",\"MXP\",\"160722084808\",\"EDW PLAT\"],\"loc-key\":\"00001\"},\"sound\":\"default\"}}"

        let jsonData = stringTicket.data(using: .utf8)
        let dictionaryTicket = try? JSONSerialization.jsonObject(with: jsonData!, options: .mutableLeaves)

        let ticketModel = TicketModel(withPayload: dictionaryTicket as! [AnyHashable: Any])

        PreferencesManager.sharedInstance().saveValue(forValue: "urlSaved" as AnyObject, withKey: "imageUrl"+"-"+ticketModel.pan5 + ticketModel.bin)

        //when
        let ticketDisplayData = TicketDisplayData.generateTicketDisplayData(withTicketModel: ticketModel)

        let formatter = AmountFormatter(codeCurrency: ticketModel.transactionCurrency)

        let decimalNumber = NSDecimalNumber(string: ticketModel.transactionAmount)
        
        let ticketDate = Date.date(fromServerString: ticketModel.receiptDate, withFormat: Date.DATE_FORMAT_PUSH)
        let stringDate = Date.string(fromDate: ticketDate!, withFormat: Date.DATE_FORMAT_PUSH_FORMATTED)

        //then

        XCTAssert(ticketDisplayData.pan == "•" + "01424")
        XCTAssert(ticketDisplayData.concept == "SUSHI TAI")
        XCTAssert(ticketDisplayData.date == stringDate)

        XCTAssert(ticketDisplayData.amount == formatter.attributtedText(forAmount: decimalNumber, bigFontSize: 48, smallFontSize: 23))

        XCTAssert(ticketDisplayData.type == Localizables.common.key_gcm_notification_accepted_purchase_title)
        XCTAssert(ticketDisplayData.typeColor == UIColor.BBVAGREEN56)
        XCTAssert(ticketDisplayData.coverUrl == "urlSaved")

    }

    func test_givenATicketModelDeniedAndHaveImageSave_whenCallGenerateTicketDisplayData_thenHaveCoverUrl() {

        //given
        let stringTicket = "{\"aps\":{\"alert\":{\"loc-args\":[\"01424\",\"481515\",\"SUSHI TAI\",\"3.21\",\"MXP\",\"160722084808\",\"EDW PLAT\"],\"loc-key\":\"00050\"},\"sound\":\"default\"}}"

        let jsonData = stringTicket.data(using: .utf8)
        let dictionaryTicket = try? JSONSerialization.jsonObject(with: jsonData!, options: .mutableLeaves)

        let ticketModel = TicketModel(withPayload: dictionaryTicket as! [AnyHashable: Any])

        PreferencesManager.sharedInstance().saveValue(forValue: "urlSaved" as AnyObject, withKey: "imageUrl"+"-"+ticketModel.pan5 + ticketModel.bin)

        //when
        let ticketDisplayData = TicketDisplayData.generateTicketDisplayData(withTicketModel: ticketModel)

        let formatter = AmountFormatter(codeCurrency: ticketModel.transactionCurrency)

        let decimalNumber = NSDecimalNumber(string: ticketModel.transactionAmount)
        
        let ticketDate = Date.date(fromServerString: ticketModel.receiptDate, withFormat: Date.DATE_FORMAT_PUSH)
        let stringDate = Date.string(fromDate: ticketDate!, withFormat: Date.DATE_FORMAT_PUSH_FORMATTED)

        //then

        XCTAssert(ticketDisplayData.pan == "•" + "01424")
        XCTAssert(ticketDisplayData.concept == "SUSHI TAI")
        XCTAssert(ticketDisplayData.date == stringDate)

        XCTAssert(ticketDisplayData.amount == formatter.attributtedText(forAmount: decimalNumber, bigFontSize: 48, smallFontSize: 23))

        XCTAssert(ticketDisplayData.type == Localizables.common.key_gcm_notification_denied_purchase_title)
        XCTAssert(ticketDisplayData.typeColor == UIColor.BBVAPINK212)
        XCTAssert(ticketDisplayData.coverUrl == "urlSaved")

    }

    func test_givenATicketModelDeniedWithDeniedText107_whenCallGenerateTicketDisplayData_thenHaveCoverUrl() {

        //given
        let stringTicket = "{\"aps\":{\"alert\":{\"loc-args\":[\"01424\",\"481515\",\"SUSHI TAI\",\"3.21\",\"MXP\",\"160722084808\",\"EDW PLAT\",\"107\"],\"loc-key\":\"00050\"},\"sound\":\"default\"}}"

        let jsonData = stringTicket.data(using: .utf8)
        let dictionaryTicket = try? JSONSerialization.jsonObject(with: jsonData!, options: .mutableLeaves)

        let ticketModel = TicketModel(withPayload: dictionaryTicket as! [AnyHashable: Any])

        PreferencesManager.sharedInstance().saveValue(forValue: "urlSaved" as AnyObject, withKey: "imageUrl"+"-"+ticketModel.pan5 + ticketModel.bin)

        //when
        let ticketDisplayData = TicketDisplayData.generateTicketDisplayData(withTicketModel: ticketModel)

        let formatter = AmountFormatter(codeCurrency: ticketModel.transactionCurrency)

        let decimalNumber = NSDecimalNumber(string: ticketModel.transactionAmount)
        
        let ticketDate = Date.date(fromServerString: ticketModel.receiptDate, withFormat: Date.DATE_FORMAT_PUSH)
        let stringDate = Date.string(fromDate: ticketDate!, withFormat: Date.DATE_FORMAT_PUSH_FORMATTED)

        //then

        XCTAssert(ticketDisplayData.pan == "•" + "01424")
        XCTAssert(ticketDisplayData.concept == "SUSHI TAI")
        XCTAssert(ticketDisplayData.date == stringDate)

        XCTAssert(ticketDisplayData.amount == formatter.attributtedText(forAmount: decimalNumber, bigFontSize: 48, smallFontSize: 23))

        XCTAssert(ticketDisplayData.type == Localizables.common.key_gcm_notification_denied_purchase_title)
        XCTAssert(ticketDisplayData.typeColor == UIColor.BBVAPINK212)
        XCTAssert(ticketDisplayData.coverUrl == "urlSaved")
        XCTAssert(ticketDisplayData.deniedText == Localizables.notifications.key_unrealized_transaction_retry_text)

    }

    func test_givenATicketModelDeniedWithDeniedText151_whenCallGenerateTicketDisplayData_thenHaveCoverUrl() {

        //given
        let stringTicket = "{\"aps\":{\"alert\":{\"loc-args\":[\"01424\",\"481515\",\"SUSHI TAI\",\"3.21\",\"MXP\",\"160722084808\",\"EDW PLAT\",\"151\"],\"loc-key\":\"00050\"},\"sound\":\"default\"}}"

        let jsonData = stringTicket.data(using: .utf8)
        let dictionaryTicket = try? JSONSerialization.jsonObject(with: jsonData!, options: .mutableLeaves)

        let ticketModel = TicketModel(withPayload: dictionaryTicket as! [AnyHashable: Any])

        PreferencesManager.sharedInstance().saveValue(forValue: "urlSaved" as AnyObject, withKey: "imageUrl"+"-"+ticketModel.pan5 + ticketModel.bin)

        //when
        let ticketDisplayData = TicketDisplayData.generateTicketDisplayData(withTicketModel: ticketModel)

        let formatter = AmountFormatter(codeCurrency: ticketModel.transactionCurrency)

        let decimalNumber = NSDecimalNumber(string: ticketModel.transactionAmount)
        
        let ticketDate = Date.date(fromServerString: ticketModel.receiptDate, withFormat: Date.DATE_FORMAT_PUSH)
        let stringDate = Date.string(fromDate: ticketDate!, withFormat: Date.DATE_FORMAT_PUSH_FORMATTED)

        //then

        XCTAssert(ticketDisplayData.pan == "•" + "01424")
        XCTAssert(ticketDisplayData.concept == "SUSHI TAI")
        XCTAssert(ticketDisplayData.date == stringDate)

        XCTAssert(ticketDisplayData.amount == formatter.attributtedText(forAmount: decimalNumber, bigFontSize: 48, smallFontSize: 23))

        XCTAssert(ticketDisplayData.type == Localizables.common.key_gcm_notification_denied_purchase_title)
        XCTAssert(ticketDisplayData.typeColor == UIColor.BBVAPINK212)
        XCTAssert(ticketDisplayData.coverUrl == "urlSaved")
        XCTAssert(ticketDisplayData.deniedText == Localizables.notifications.key_not_balance_text)

    }

    func test_givenATicketModelDeniedWithDeniedText175_whenCallGenerateTicketDisplayData_thenHaveCoverUrl() {

        //given
        let stringTicket = "{\"aps\":{\"alert\":{\"loc-args\":[\"01424\",\"481515\",\"SUSHI TAI\",\"3.21\",\"MXP\",\"160722084808\",\"EDW PLAT\",\"175\"],\"loc-key\":\"00050\"},\"sound\":\"default\"}}"

        let jsonData = stringTicket.data(using: .utf8)
        let dictionaryTicket = try? JSONSerialization.jsonObject(with: jsonData!, options: .mutableLeaves)

        let ticketModel = TicketModel(withPayload: dictionaryTicket as! [AnyHashable: Any])

        PreferencesManager.sharedInstance().saveValue(forValue: "urlSaved" as AnyObject, withKey: "imageUrl"+"-"+ticketModel.pan5 + ticketModel.bin)

        //when
        let ticketDisplayData = TicketDisplayData.generateTicketDisplayData(withTicketModel: ticketModel)

        let formatter = AmountFormatter(codeCurrency: ticketModel.transactionCurrency)

        let decimalNumber = NSDecimalNumber(string: ticketModel.transactionAmount)
        
        let ticketDate = Date.date(fromServerString: ticketModel.receiptDate, withFormat: Date.DATE_FORMAT_PUSH)
        let stringDate = Date.string(fromDate: ticketDate!, withFormat: Date.DATE_FORMAT_PUSH_FORMATTED)

        //then

        XCTAssert(ticketDisplayData.pan == "•" + "01424")
        XCTAssert(ticketDisplayData.concept == "SUSHI TAI")
        XCTAssert(ticketDisplayData.date == stringDate)

        XCTAssert(ticketDisplayData.amount == formatter.attributtedText(forAmount: decimalNumber, bigFontSize: 48, smallFontSize: 23))

        XCTAssert(ticketDisplayData.type == Localizables.common.key_gcm_notification_denied_purchase_title)
        XCTAssert(ticketDisplayData.typeColor == UIColor.BBVAPINK212)
        XCTAssert(ticketDisplayData.coverUrl == "urlSaved")
        XCTAssert(ticketDisplayData.deniedText == Localizables.notifications.key_exceeded_credit_text)

    }

    func test_givenATicketModelDeniedWithDeniedText188_whenCallGenerateTicketDisplayData_thenHaveCoverUrl() {

        //given
        let stringTicket = "{\"aps\":{\"alert\":{\"loc-args\":[\"01424\",\"481515\",\"SUSHI TAI\",\"3.21\",\"MXP\",\"160722084808\",\"EDW PLAT\",\"188\"],\"loc-key\":\"00050\"},\"sound\":\"default\"}}"

        let jsonData = stringTicket.data(using: .utf8)
        let dictionaryTicket = try? JSONSerialization.jsonObject(with: jsonData!, options: .mutableLeaves)

        let ticketModel = TicketModel(withPayload: dictionaryTicket as! [AnyHashable: Any])

        PreferencesManager.sharedInstance().saveValue(forValue: "urlSaved" as AnyObject, withKey: "imageUrl"+"-"+ticketModel.pan5 + ticketModel.bin)

        //when
        let ticketDisplayData = TicketDisplayData.generateTicketDisplayData(withTicketModel: ticketModel)

        let formatter = AmountFormatter(codeCurrency: ticketModel.transactionCurrency)

        let decimalNumber = NSDecimalNumber(string: ticketModel.transactionAmount)
        
        let ticketDate = Date.date(fromServerString: ticketModel.receiptDate, withFormat: Date.DATE_FORMAT_PUSH)
        let stringDate = Date.string(fromDate: ticketDate!, withFormat: Date.DATE_FORMAT_PUSH_FORMATTED)

        //then

        XCTAssert(ticketDisplayData.pan == "•" + "01424")
        XCTAssert(ticketDisplayData.concept == "SUSHI TAI")
        XCTAssert(ticketDisplayData.date == stringDate)

        XCTAssert(ticketDisplayData.amount == formatter.attributtedText(forAmount: decimalNumber, bigFontSize: 48, smallFontSize: 23))

        XCTAssert(ticketDisplayData.type == Localizables.common.key_gcm_notification_denied_purchase_title)
        XCTAssert(ticketDisplayData.typeColor == UIColor.BBVAPINK212)
        XCTAssert(ticketDisplayData.coverUrl == "urlSaved")
        XCTAssert(ticketDisplayData.deniedText == Localizables.notifications.key_amount_purchase_exceeded_credit_text)

    }

    func test_givenATicketModelDeniedWithDeniedText229_whenCallGenerateTicketDisplayData_thenHaveCoverUrl() {

        //given
        let stringTicket = "{\"aps\":{\"alert\":{\"loc-args\":[\"01424\",\"481515\",\"SUSHI TAI\",\"3.21\",\"MXP\",\"160722084808\",\"EDW PLAT\",\"229\"],\"loc-key\":\"00050\"},\"sound\":\"default\"}}"

        let jsonData = stringTicket.data(using: .utf8)
        let dictionaryTicket = try? JSONSerialization.jsonObject(with: jsonData!, options: .mutableLeaves)

        let ticketModel = TicketModel(withPayload: dictionaryTicket as! [AnyHashable: Any])

        PreferencesManager.sharedInstance().saveValue(forValue: "urlSaved" as AnyObject, withKey: "imageUrl"+"-"+ticketModel.pan5 + ticketModel.bin)

        //when
        let ticketDisplayData = TicketDisplayData.generateTicketDisplayData(withTicketModel: ticketModel)

        let formatter = AmountFormatter(codeCurrency: ticketModel.transactionCurrency)

        let decimalNumber = NSDecimalNumber(string: ticketModel.transactionAmount)
        
        let ticketDate = Date.date(fromServerString: ticketModel.receiptDate, withFormat: Date.DATE_FORMAT_PUSH)
        let stringDate = Date.string(fromDate: ticketDate!, withFormat: Date.DATE_FORMAT_PUSH_FORMATTED)

        //then

        XCTAssert(ticketDisplayData.pan == "•" + "01424")
        XCTAssert(ticketDisplayData.concept == "SUSHI TAI")
        XCTAssert(ticketDisplayData.date == stringDate)

        XCTAssert(ticketDisplayData.amount == formatter.attributtedText(forAmount: decimalNumber, bigFontSize: 48, smallFontSize: 23))

        XCTAssert(ticketDisplayData.type == Localizables.common.key_gcm_notification_denied_purchase_title)
        XCTAssert(ticketDisplayData.typeColor == UIColor.BBVAPINK212)
        XCTAssert(ticketDisplayData.coverUrl == "urlSaved")
        XCTAssert(ticketDisplayData.deniedText == Localizables.notifications.key_card_off_text)

    }

    func test_givenATicketModelDeniedWithDeniedText244_whenCallGenerateTicketDisplayData_thenHaveCoverUrl() {

        //given
        let stringTicket = "{\"aps\":{\"alert\":{\"loc-args\":[\"01424\",\"481515\",\"SUSHI TAI\",\"3.21\",\"MXP\",\"160722084808\",\"EDW PLAT\",\"244\"],\"loc-key\":\"00050\"},\"sound\":\"default\"}}"

        let jsonData = stringTicket.data(using: .utf8)
        let dictionaryTicket = try? JSONSerialization.jsonObject(with: jsonData!, options: .mutableLeaves)

        let ticketModel = TicketModel(withPayload: dictionaryTicket as! [AnyHashable: Any])

        PreferencesManager.sharedInstance().saveValue(forValue: "urlSaved" as AnyObject, withKey: "imageUrl"+"-"+ticketModel.pan5 + ticketModel.bin)

        //when
        let ticketDisplayData = TicketDisplayData.generateTicketDisplayData(withTicketModel: ticketModel)

        let formatter = AmountFormatter(codeCurrency: ticketModel.transactionCurrency)

        let decimalNumber = NSDecimalNumber(string: ticketModel.transactionAmount)
        
        let ticketDate = Date.date(fromServerString: ticketModel.receiptDate, withFormat: Date.DATE_FORMAT_PUSH)
        let stringDate = Date.string(fromDate: ticketDate!, withFormat: Date.DATE_FORMAT_PUSH_FORMATTED)

        //then

        XCTAssert(ticketDisplayData.pan == "•" + "01424")
        XCTAssert(ticketDisplayData.concept == "SUSHI TAI")
        XCTAssert(ticketDisplayData.date == stringDate)

        XCTAssert(ticketDisplayData.amount == formatter.attributtedText(forAmount: decimalNumber, bigFontSize: 48, smallFontSize: 23))

        XCTAssert(ticketDisplayData.type == Localizables.common.key_gcm_notification_denied_purchase_title)
        XCTAssert(ticketDisplayData.typeColor == UIColor.BBVAPINK212)
        XCTAssert(ticketDisplayData.coverUrl == "urlSaved")
        XCTAssert(ticketDisplayData.deniedText == Localizables.notifications.key_card_reported_for_lost_text)

    }

    func test_givenATicketModelDeniedWithDeniedText245_whenCallGenerateTicketDisplayData_thenHaveCoverUrl() {

        //given
        let stringTicket = "{\"aps\":{\"alert\":{\"loc-args\":[\"01424\",\"481515\",\"SUSHI TAI\",\"3.21\",\"MXP\",\"160722084808\",\"EDW PLAT\",\"245\"],\"loc-key\":\"00050\"},\"sound\":\"default\"}}"

        let jsonData = stringTicket.data(using: .utf8)
        let dictionaryTicket = try? JSONSerialization.jsonObject(with: jsonData!, options: .mutableLeaves)

        let ticketModel = TicketModel(withPayload: dictionaryTicket as! [AnyHashable: Any])

        PreferencesManager.sharedInstance().saveValue(forValue: "urlSaved" as AnyObject, withKey: "imageUrl"+"-"+ticketModel.pan5 + ticketModel.bin)

        //when
        let ticketDisplayData = TicketDisplayData.generateTicketDisplayData(withTicketModel: ticketModel)

        let formatter = AmountFormatter(codeCurrency: ticketModel.transactionCurrency)

        let decimalNumber = NSDecimalNumber(string: ticketModel.transactionAmount)
        
        let ticketDate = Date.date(fromServerString: ticketModel.receiptDate, withFormat: Date.DATE_FORMAT_PUSH)
        let stringDate = Date.string(fromDate: ticketDate!, withFormat: Date.DATE_FORMAT_PUSH_FORMATTED)

        //then

        XCTAssert(ticketDisplayData.pan == "•" + "01424")
        XCTAssert(ticketDisplayData.concept == "SUSHI TAI")
        XCTAssert(ticketDisplayData.date == stringDate)

        XCTAssert(ticketDisplayData.amount == formatter.attributtedText(forAmount: decimalNumber, bigFontSize: 48, smallFontSize: 23))

        XCTAssert(ticketDisplayData.type == Localizables.common.key_gcm_notification_denied_purchase_title)
        XCTAssert(ticketDisplayData.typeColor == UIColor.BBVAPINK212)
        XCTAssert(ticketDisplayData.coverUrl == "urlSaved")
        XCTAssert(ticketDisplayData.deniedText == Localizables.notifications.key_card_reported_for_theft_text)

    }

    func test_givenATicketModelDeniedWithDeniedText252_whenCallGenerateTicketDisplayData_thenHaveCoverUrl() {

        //given
        let stringTicket = "{\"aps\":{\"alert\":{\"loc-args\":[\"01424\",\"481515\",\"SUSHI TAI\",\"3.21\",\"MXP\",\"160722084808\",\"EDW PLAT\",\"252\"],\"loc-key\":\"00050\"},\"sound\":\"default\"}}"

        let jsonData = stringTicket.data(using: .utf8)
        let dictionaryTicket = try? JSONSerialization.jsonObject(with: jsonData!, options: .mutableLeaves)

        let ticketModel = TicketModel(withPayload: dictionaryTicket as! [AnyHashable: Any])

        PreferencesManager.sharedInstance().saveValue(forValue: "urlSaved" as AnyObject, withKey: "imageUrl"+"-"+ticketModel.pan5 + ticketModel.bin)

        //when
        let ticketDisplayData = TicketDisplayData.generateTicketDisplayData(withTicketModel: ticketModel)

        let formatter = AmountFormatter(codeCurrency: ticketModel.transactionCurrency)

        let decimalNumber = NSDecimalNumber(string: ticketModel.transactionAmount)
        
        let ticketDate = Date.date(fromServerString: ticketModel.receiptDate, withFormat: Date.DATE_FORMAT_PUSH)
        let stringDate = Date.string(fromDate: ticketDate!, withFormat: Date.DATE_FORMAT_PUSH_FORMATTED)

        //then

        XCTAssert(ticketDisplayData.pan == "•" + "01424")
        XCTAssert(ticketDisplayData.concept == "SUSHI TAI")
        XCTAssert(ticketDisplayData.date == stringDate)

        XCTAssert(ticketDisplayData.amount == formatter.attributtedText(forAmount: decimalNumber, bigFontSize: 48, smallFontSize: 23))

        XCTAssert(ticketDisplayData.type == Localizables.common.key_gcm_notification_denied_purchase_title)
        XCTAssert(ticketDisplayData.typeColor == UIColor.BBVAPINK212)
        XCTAssert(ticketDisplayData.coverUrl == "urlSaved")
        XCTAssert(ticketDisplayData.deniedText == Localizables.notifications.key_wrong_expiration_date_text)

    }

    func test_givenATicketModelDeniedWithDeniedText795_whenCallGenerateTicketDisplayData_thenHaveCoverUrl() {

        //given
        let stringTicket = "{\"aps\":{\"alert\":{\"loc-args\":[\"01424\",\"481515\",\"SUSHI TAI\",\"3.21\",\"MXP\",\"160722084808\",\"EDW PLAT\",\"795\"],\"loc-key\":\"00050\"},\"sound\":\"default\"}}"

        let jsonData = stringTicket.data(using: .utf8)
        let dictionaryTicket = try? JSONSerialization.jsonObject(with: jsonData!, options: .mutableLeaves)

        let ticketModel = TicketModel(withPayload: dictionaryTicket as! [AnyHashable: Any])

        PreferencesManager.sharedInstance().saveValue(forValue: "urlSaved" as AnyObject, withKey: "imageUrl"+"-"+ticketModel.pan5 + ticketModel.bin)

        //when
        let ticketDisplayData = TicketDisplayData.generateTicketDisplayData(withTicketModel: ticketModel)

        let formatter = AmountFormatter(codeCurrency: ticketModel.transactionCurrency)

        let decimalNumber = NSDecimalNumber(string: ticketModel.transactionAmount)
        
        let ticketDate = Date.date(fromServerString: ticketModel.receiptDate, withFormat: Date.DATE_FORMAT_PUSH)
        let stringDate = Date.string(fromDate: ticketDate!, withFormat: Date.DATE_FORMAT_PUSH_FORMATTED)

        //then

        XCTAssert(ticketDisplayData.pan == "•" + "01424")
        XCTAssert(ticketDisplayData.concept == "SUSHI TAI")
        XCTAssert(ticketDisplayData.date == stringDate)

        XCTAssert(ticketDisplayData.amount == formatter.attributtedText(forAmount: decimalNumber, bigFontSize: 48, smallFontSize: 23))

        XCTAssert(ticketDisplayData.type == Localizables.common.key_gcm_notification_denied_purchase_title)
        XCTAssert(ticketDisplayData.typeColor == UIColor.BBVAPINK212)
        XCTAssert(ticketDisplayData.coverUrl == "urlSaved")
        XCTAssert(ticketDisplayData.deniedText == Localizables.notifications.key_enter_the_application_for_purchase_text)

    }

    func test_givenATicketModelDeniedWithDeniedText796_whenCallGenerateTicketDisplayData_thenHaveCoverUrl() {

        //given
        let stringTicket = "{\"aps\":{\"alert\":{\"loc-args\":[\"01424\",\"481515\",\"SUSHI TAI\",\"3.21\",\"MXP\",\"160722084808\",\"EDW PLAT\",\"796\"],\"loc-key\":\"00050\"},\"sound\":\"default\"}}"

        let jsonData = stringTicket.data(using: .utf8)
        let dictionaryTicket = try? JSONSerialization.jsonObject(with: jsonData!, options: .mutableLeaves)

        let ticketModel = TicketModel(withPayload: dictionaryTicket as! [AnyHashable: Any])

        PreferencesManager.sharedInstance().saveValue(forValue: "urlSaved" as AnyObject, withKey: "imageUrl"+"-"+ticketModel.pan5 + ticketModel.bin)

        //when
        let ticketDisplayData = TicketDisplayData.generateTicketDisplayData(withTicketModel: ticketModel)

        let formatter = AmountFormatter(codeCurrency: ticketModel.transactionCurrency)

        let decimalNumber = NSDecimalNumber(string: ticketModel.transactionAmount)
        
        let ticketDate = Date.date(fromServerString: ticketModel.receiptDate, withFormat: Date.DATE_FORMAT_PUSH)
        let stringDate = Date.string(fromDate: ticketDate!, withFormat: Date.DATE_FORMAT_PUSH_FORMATTED)

        //then

        XCTAssert(ticketDisplayData.pan == "•" + "01424")
        XCTAssert(ticketDisplayData.concept == "SUSHI TAI")
        XCTAssert(ticketDisplayData.date == stringDate)

        XCTAssert(ticketDisplayData.amount == formatter.attributtedText(forAmount: decimalNumber, bigFontSize: 48, smallFontSize: 23))

        XCTAssert(ticketDisplayData.type == Localizables.common.key_gcm_notification_denied_purchase_title)
        XCTAssert(ticketDisplayData.typeColor == UIColor.BBVAPINK212)
        XCTAssert(ticketDisplayData.coverUrl == "urlSaved")
        XCTAssert(ticketDisplayData.deniedText == Localizables.notifications.key_enter_the_application_for_purchase_text)

    }

    func test_givenATicketModelDeniedWithDeniedText797_whenCallGenerateTicketDisplayData_thenHaveCoverUrl() {

        //given
        let stringTicket = "{\"aps\":{\"alert\":{\"loc-args\":[\"01424\",\"481515\",\"SUSHI TAI\",\"3.21\",\"MXP\",\"160722084808\",\"EDW PLAT\",\"797\"],\"loc-key\":\"00050\"},\"sound\":\"default\"}}"

        let jsonData = stringTicket.data(using: .utf8)
        let dictionaryTicket = try? JSONSerialization.jsonObject(with: jsonData!, options: .mutableLeaves)

        let ticketModel = TicketModel(withPayload: dictionaryTicket as! [AnyHashable: Any])

        PreferencesManager.sharedInstance().saveValue(forValue: "urlSaved" as AnyObject, withKey: "imageUrl"+"-"+ticketModel.pan5 + ticketModel.bin)

        //when
        let ticketDisplayData = TicketDisplayData.generateTicketDisplayData(withTicketModel: ticketModel)

        let formatter = AmountFormatter(codeCurrency: ticketModel.transactionCurrency)

        let decimalNumber = NSDecimalNumber(string: ticketModel.transactionAmount)
        
        let ticketDate = Date.date(fromServerString: ticketModel.receiptDate, withFormat: Date.DATE_FORMAT_PUSH)
        let stringDate = Date.string(fromDate: ticketDate!, withFormat: Date.DATE_FORMAT_PUSH_FORMATTED)

        //then

        XCTAssert(ticketDisplayData.pan == "•" + "01424")
        XCTAssert(ticketDisplayData.concept == "SUSHI TAI")
        XCTAssert(ticketDisplayData.date == stringDate)

        XCTAssert(ticketDisplayData.amount == formatter.attributtedText(forAmount: decimalNumber, bigFontSize: 48, smallFontSize: 23))

        XCTAssert(ticketDisplayData.type == Localizables.common.key_gcm_notification_denied_purchase_title)
        XCTAssert(ticketDisplayData.typeColor == UIColor.BBVAPINK212)
        XCTAssert(ticketDisplayData.coverUrl == "urlSaved")
        XCTAssert(ticketDisplayData.deniedText == Localizables.notifications.key_enter_the_application_for_purchase_text)

    }

    func test_givenATicketModelDeniedWithDeniedText935_whenCallGenerateTicketDisplayData_thenHaveCoverUrl() {

        //given
        let stringTicket = "{\"aps\":{\"alert\":{\"loc-args\":[\"01424\",\"481515\",\"SUSHI TAI\",\"3.21\",\"MXP\",\"160722084808\",\"EDW PLAT\",\"935\"],\"loc-key\":\"00050\"},\"sound\":\"default\"}}"

        let jsonData = stringTicket.data(using: .utf8)
        let dictionaryTicket = try? JSONSerialization.jsonObject(with: jsonData!, options: .mutableLeaves)

        let ticketModel = TicketModel(withPayload: dictionaryTicket as! [AnyHashable: Any])

        PreferencesManager.sharedInstance().saveValue(forValue: "urlSaved" as AnyObject, withKey: "imageUrl"+"-"+ticketModel.pan5 + ticketModel.bin)

        //when
        let ticketDisplayData = TicketDisplayData.generateTicketDisplayData(withTicketModel: ticketModel)

        let formatter = AmountFormatter(codeCurrency: ticketModel.transactionCurrency)

        let decimalNumber = NSDecimalNumber(string: ticketModel.transactionAmount)
        
        let ticketDate = Date.date(fromServerString: ticketModel.receiptDate, withFormat: Date.DATE_FORMAT_PUSH)
        let stringDate = Date.string(fromDate: ticketDate!, withFormat: Date.DATE_FORMAT_PUSH_FORMATTED)

        //then

        XCTAssert(ticketDisplayData.pan == "•" + "01424")
        XCTAssert(ticketDisplayData.concept == "SUSHI TAI")
        XCTAssert(ticketDisplayData.date == stringDate)

        XCTAssert(ticketDisplayData.amount == formatter.attributtedText(forAmount: decimalNumber, bigFontSize: 48, smallFontSize: 23))

        XCTAssert(ticketDisplayData.type == Localizables.common.key_gcm_notification_denied_purchase_title)
        XCTAssert(ticketDisplayData.typeColor == UIColor.BBVAPINK212)
        XCTAssert(ticketDisplayData.coverUrl == "urlSaved")
        XCTAssert(ticketDisplayData.deniedText == Localizables.notifications.key_wrong_security_code_text)

    }

    func test_givenATicketModelDeniedWithDeniedText936_whenCallGenerateTicketDisplayData_thenHaveCoverUrl() {

        //given
        let stringTicket = "{\"aps\":{\"alert\":{\"loc-args\":[\"01424\",\"481515\",\"SUSHI TAI\",\"3.21\",\"MXP\",\"160722084808\",\"EDW PLAT\",\"936\"],\"loc-key\":\"00050\"},\"sound\":\"default\"}}"

        let jsonData = stringTicket.data(using: .utf8)
        let dictionaryTicket = try? JSONSerialization.jsonObject(with: jsonData!, options: .mutableLeaves)

        let ticketModel = TicketModel(withPayload: dictionaryTicket as! [AnyHashable: Any])

        PreferencesManager.sharedInstance().saveValue(forValue: "urlSaved" as AnyObject, withKey: "imageUrl"+"-"+ticketModel.pan5 + ticketModel.bin)

        //when
        let ticketDisplayData = TicketDisplayData.generateTicketDisplayData(withTicketModel: ticketModel)

        let formatter = AmountFormatter(codeCurrency: ticketModel.transactionCurrency)

        let decimalNumber = NSDecimalNumber(string: ticketModel.transactionAmount)
        
        let ticketDate = Date.date(fromServerString: ticketModel.receiptDate, withFormat: Date.DATE_FORMAT_PUSH)
        let stringDate = Date.string(fromDate: ticketDate!, withFormat: Date.DATE_FORMAT_PUSH_FORMATTED)

        //then

        XCTAssert(ticketDisplayData.pan == "•" + "01424")
        XCTAssert(ticketDisplayData.concept == "SUSHI TAI")
        XCTAssert(ticketDisplayData.date == stringDate)

        XCTAssert(ticketDisplayData.amount == formatter.attributtedText(forAmount: decimalNumber, bigFontSize: 48, smallFontSize: 23))

        XCTAssert(ticketDisplayData.type == Localizables.common.key_gcm_notification_denied_purchase_title)
        XCTAssert(ticketDisplayData.typeColor == UIColor.BBVAPINK212)
        XCTAssert(ticketDisplayData.coverUrl == "urlSaved")
        XCTAssert(ticketDisplayData.deniedText == Localizables.notifications.key_exceeded_number_attemps_security_code_text)

    }

    func test_givenATicketModelDeniedWithDeniedText937_whenCallGenerateTicketDisplayData_thenHaveCoverUrl() {

        //given
        let stringTicket = "{\"aps\":{\"alert\":{\"loc-args\":[\"01424\",\"481515\",\"SUSHI TAI\",\"3.21\",\"MXP\",\"160722084808\",\"EDW PLAT\",\"937\"],\"loc-key\":\"00050\"},\"sound\":\"default\"}}"

        let jsonData = stringTicket.data(using: .utf8)
        let dictionaryTicket = try? JSONSerialization.jsonObject(with: jsonData!, options: .mutableLeaves)

        let ticketModel = TicketModel(withPayload: dictionaryTicket as! [AnyHashable: Any])

        PreferencesManager.sharedInstance().saveValue(forValue: "urlSaved" as AnyObject, withKey: "imageUrl"+"-"+ticketModel.pan5 + ticketModel.bin)

        //when
        let ticketDisplayData = TicketDisplayData.generateTicketDisplayData(withTicketModel: ticketModel)

        let formatter = AmountFormatter(codeCurrency: ticketModel.transactionCurrency)

        let decimalNumber = NSDecimalNumber(string: ticketModel.transactionAmount)
        
        let ticketDate = Date.date(fromServerString: ticketModel.receiptDate, withFormat: Date.DATE_FORMAT_PUSH)
        let stringDate = Date.string(fromDate: ticketDate!, withFormat: Date.DATE_FORMAT_PUSH_FORMATTED)

        //then

        XCTAssert(ticketDisplayData.pan == "•" + "01424")
        XCTAssert(ticketDisplayData.concept == "SUSHI TAI")
        XCTAssert(ticketDisplayData.date == stringDate)

        XCTAssert(ticketDisplayData.amount == formatter.attributtedText(forAmount: decimalNumber, bigFontSize: 48, smallFontSize: 23))

        XCTAssert(ticketDisplayData.type == Localizables.common.key_gcm_notification_denied_purchase_title)
        XCTAssert(ticketDisplayData.typeColor == UIColor.BBVAPINK212)
        XCTAssert(ticketDisplayData.coverUrl == "urlSaved")
        XCTAssert(ticketDisplayData.deniedText == Localizables.notifications.key_enter_the_application_for_purchase_text)

    }

    func test_givenATicketModelDeniedWithDeniedText938_whenCallGenerateTicketDisplayData_thenHaveCoverUrl() {

        //given
        let stringTicket = "{\"aps\":{\"alert\":{\"loc-args\":[\"01424\",\"481515\",\"SUSHI TAI\",\"3.21\",\"MXP\",\"160722084808\",\"EDW PLAT\",\"938\"],\"loc-key\":\"00050\"},\"sound\":\"default\"}}"

        let jsonData = stringTicket.data(using: .utf8)
        let dictionaryTicket = try? JSONSerialization.jsonObject(with: jsonData!, options: .mutableLeaves)

        let ticketModel = TicketModel(withPayload: dictionaryTicket as! [AnyHashable: Any])

        PreferencesManager.sharedInstance().saveValue(forValue: "urlSaved" as AnyObject, withKey: "imageUrl"+"-"+ticketModel.pan5 + ticketModel.bin)

        //when
        let ticketDisplayData = TicketDisplayData.generateTicketDisplayData(withTicketModel: ticketModel)

        let formatter = AmountFormatter(codeCurrency: ticketModel.transactionCurrency)

        let decimalNumber = NSDecimalNumber(string: ticketModel.transactionAmount)
        
        let ticketDate = Date.date(fromServerString: ticketModel.receiptDate, withFormat: Date.DATE_FORMAT_PUSH)
        let stringDate = Date.string(fromDate: ticketDate!, withFormat: Date.DATE_FORMAT_PUSH_FORMATTED)

        //then

        XCTAssert(ticketDisplayData.pan == "•" + "01424")
        XCTAssert(ticketDisplayData.concept == "SUSHI TAI")
        XCTAssert(ticketDisplayData.date == stringDate)

        XCTAssert(ticketDisplayData.amount == formatter.attributtedText(forAmount: decimalNumber, bigFontSize: 48, smallFontSize: 23))

        XCTAssert(ticketDisplayData.type == Localizables.common.key_gcm_notification_denied_purchase_title)
        XCTAssert(ticketDisplayData.typeColor == UIColor.BBVAPINK212)
        XCTAssert(ticketDisplayData.coverUrl == "urlSaved")
        XCTAssert(ticketDisplayData.deniedText == Localizables.notifications.key_enter_the_application_for_purchase_text)

    }

    func test_givenATicketModelDeniedWithDeniedText939_whenCallGenerateTicketDisplayData_thenHaveCoverUrl() {

        //given
        let stringTicket = "{\"aps\":{\"alert\":{\"loc-args\":[\"01424\",\"481515\",\"SUSHI TAI\",\"3.21\",\"MXP\",\"160722084808\",\"EDW PLAT\",\"939\"],\"loc-key\":\"00050\"},\"sound\":\"default\"}}"

        let jsonData = stringTicket.data(using: .utf8)
        let dictionaryTicket = try? JSONSerialization.jsonObject(with: jsonData!, options: .mutableLeaves)

        let ticketModel = TicketModel(withPayload: dictionaryTicket as! [AnyHashable: Any])

        PreferencesManager.sharedInstance().saveValue(forValue: "urlSaved" as AnyObject, withKey: "imageUrl"+"-"+ticketModel.pan5 + ticketModel.bin)

        //when
        let ticketDisplayData = TicketDisplayData.generateTicketDisplayData(withTicketModel: ticketModel)

        let formatter = AmountFormatter(codeCurrency: ticketModel.transactionCurrency)

        let decimalNumber = NSDecimalNumber(string: ticketModel.transactionAmount)
        
        let ticketDate = Date.date(fromServerString: ticketModel.receiptDate, withFormat: Date.DATE_FORMAT_PUSH)
        let stringDate = Date.string(fromDate: ticketDate!, withFormat: Date.DATE_FORMAT_PUSH_FORMATTED)

        //then

        XCTAssert(ticketDisplayData.pan == "•" + "01424")
        XCTAssert(ticketDisplayData.concept == "SUSHI TAI")
        XCTAssert(ticketDisplayData.date == stringDate)

        XCTAssert(ticketDisplayData.amount == formatter.attributtedText(forAmount: decimalNumber, bigFontSize: 48, smallFontSize: 23))

        XCTAssert(ticketDisplayData.type == Localizables.common.key_gcm_notification_denied_purchase_title)
        XCTAssert(ticketDisplayData.typeColor == UIColor.BBVAPINK212)
        XCTAssert(ticketDisplayData.coverUrl == "urlSaved")
        XCTAssert(ticketDisplayData.deniedText == Localizables.notifications.key_wrong_security_code_text)

    }

    func test_givenATicketModelDeniedWithDeniedText940_whenCallGenerateTicketDisplayData_thenHaveCoverUrl() {

        //given
        let stringTicket = "{\"aps\":{\"alert\":{\"loc-args\":[\"01424\",\"481515\",\"SUSHI TAI\",\"3.21\",\"MXP\",\"160722084808\",\"EDW PLAT\",\"940\"],\"loc-key\":\"00050\"},\"sound\":\"default\"}}"

        let jsonData = stringTicket.data(using: .utf8)
        let dictionaryTicket = try? JSONSerialization.jsonObject(with: jsonData!, options: .mutableLeaves)

        let ticketModel = TicketModel(withPayload: dictionaryTicket as! [AnyHashable: Any])

        PreferencesManager.sharedInstance().saveValue(forValue: "urlSaved" as AnyObject, withKey: "imageUrl"+"-"+ticketModel.pan5 + ticketModel.bin)

        //when
        let ticketDisplayData = TicketDisplayData.generateTicketDisplayData(withTicketModel: ticketModel)

        let formatter = AmountFormatter(codeCurrency: ticketModel.transactionCurrency)

        let decimalNumber = NSDecimalNumber(string: ticketModel.transactionAmount)
        
        let ticketDate = Date.date(fromServerString: ticketModel.receiptDate, withFormat: Date.DATE_FORMAT_PUSH)
        let stringDate = Date.string(fromDate: ticketDate!, withFormat: Date.DATE_FORMAT_PUSH_FORMATTED)

        //then

        XCTAssert(ticketDisplayData.pan == "•" + "01424")
        XCTAssert(ticketDisplayData.concept == "SUSHI TAI")
        XCTAssert(ticketDisplayData.date == stringDate)

        XCTAssert(ticketDisplayData.amount == formatter.attributtedText(forAmount: decimalNumber, bigFontSize: 48, smallFontSize: 23))

        XCTAssert(ticketDisplayData.type == Localizables.common.key_gcm_notification_denied_purchase_title)
        XCTAssert(ticketDisplayData.typeColor == UIColor.BBVAPINK212)
        XCTAssert(ticketDisplayData.coverUrl == "urlSaved")
        XCTAssert(ticketDisplayData.deniedText == Localizables.notifications.key_exceeded_number_attemps_security_code_text)

    }

    class DummyPreferencesManager: PreferencesManager {

        var valueSaved: AnyObject?

        override func getValue(forKey key: String) -> AnyObject? {

            return valueSaved
        }

        override func saveValue(forValue value: AnyObject, withKey key: String) -> Bool {

            valueSaved = value

            return true
        }

    }

}
