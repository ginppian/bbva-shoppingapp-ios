//
//  OptionsSelectionableDisplayDataTests.swift
//  shoppingapp
//
//  Created by jesus.martinez on 22/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class OptionsSelectionableDisplayDataTests: XCTestCase {

    // MARK: initWithTitle

    func test_givenATitle_whenICallInitWithTitle_thenTitleShouldMatchAndOptionsShouldBeEmpty() {

        // given
        let title = "title"

        // when
        let sut = OptionsSelectionableDisplayData(title: title)

        // then
        XCTAssert(sut.title == title)
        XCTAssert(sut.options.isEmpty == true)

    }

    // MARK: initWithTitle: promotionTypes:

    func test_givenATitleAndPromotionTypesEmpty_whenICallInitWithTitlePromotionType_thenTitleShouldMatchAndOptionsShouldBeEmpty() {

        // given
        let title = "title"
        let promotionTypes = [PromotionType]()

        // when
        let sut = OptionsSelectionableDisplayData(title: title, promotionTypes: promotionTypes)

        // then
        XCTAssert(sut.title == title)
        XCTAssert(sut.options.isEmpty == true)

    }

    func test_givenATitleAndPromotionTypesWithAllTypes_whenICallInitWithTitlePromotionType_thenTitleShouldMatchAndOptionsShouldHaveTheCorrectValuesInTheRightOrder() {

        // given
        let title = "title"
        let promotionTypes: [PromotionType] = [.discount, .toMonths, .point, .unknown]

        // when
        let sut = OptionsSelectionableDisplayData(title: title, promotionTypes: promotionTypes)

        // then
        XCTAssert(sut.title == title)
        XCTAssert(sut.options.count == (promotionTypes.count - 1))

        XCTAssert(sut.options[0].imageName == ConstantsImages.Promotions.discount_icon)
        XCTAssert(sut.options[0].title == Localizables.promotions.key_discount_text)
        XCTAssert(sut.options[0].isSelected == false)

        XCTAssert(sut.options[1].imageName == ConstantsImages.Common.calendar_icon)
        XCTAssert(sut.options[1].title == Localizables.promotions.key_months_without_interest_text)
        XCTAssert(sut.options[1].isSelected == false)

        XCTAssert(sut.options[2].imageName == ConstantsImages.Promotions.promotion)
        XCTAssert(sut.options[2].title == Localizables.promotions.key_promotions_points_text)
        XCTAssert(sut.options[2].isSelected == false)

    }

    func test_givenATitleAndPromotionTypesWithAllTypesAndPromotionTypeSelectedWithTwoOfThem_whenICallInitWithTitlePromotionType_thenTitleShouldMatchAndOptionsShouldHaveTheCorrectValuesInTheRightOrderAndTheTwoOfThemShouldBeSelected() {

        // given
        let title = "title"
        let promotionTypes: [PromotionType] = [.discount, .toMonths, .point, .unknown]
        let promotionTypeSelected: [PromotionType] = [.discount, .point]

        // when
        let sut = OptionsSelectionableDisplayData(title: title, promotionTypes: promotionTypes, promotionTypesSelected: promotionTypeSelected)

        // then
        XCTAssert(sut.title == title)
        XCTAssert(sut.options.count == (promotionTypes.count - 1))

        XCTAssert(sut.options[0].imageName == ConstantsImages.Promotions.discount_icon)
        XCTAssert(sut.options[0].title == Localizables.promotions.key_discount_text)
        XCTAssert(sut.options[0].isSelected == true)

        XCTAssert(sut.options[1].imageName == ConstantsImages.Common.calendar_icon)
        XCTAssert(sut.options[1].title == Localizables.promotions.key_months_without_interest_text)
        XCTAssert(sut.options[1].isSelected == false)

        XCTAssert(sut.options[2].imageName == ConstantsImages.Promotions.promotion)
        XCTAssert(sut.options[2].title == Localizables.promotions.key_promotions_points_text)
        XCTAssert(sut.options[2].isSelected == true)

    }

    // MARK: initWithTitle: promotionUsageTypes:

    func test_givenATitleAndPromotionUsageTypesEmpty_whenICallInitWithTitlePromotionType_thenTitleShouldMatchAndOptionsShouldBeEmpty() {

        // given
        let title = "title"
        let promotionUsageTypes = [PromotionUsageType]()

        // when
        let sut = OptionsSelectionableDisplayData(title: title, promotionUsageTypes: promotionUsageTypes)

        // then
        XCTAssert(sut.title == title)
        XCTAssert(sut.options.isEmpty == true)

    }

    func test_givenATitleAndPromotionUsageTypesWithAllTypes_whenICallInitWithTitlePromotionType_thenTitleShouldMatchAndOptionsShouldHaveTheCorrectValuesInTheRightOrder() {

        // given
        let title = "title"
        let promotionUsageTypes: [PromotionUsageType] = [.physical, .online, .unknown]

        // when
        let sut = OptionsSelectionableDisplayData(title: title, promotionUsageTypes: promotionUsageTypes)

        // then
        XCTAssert(sut.title == title)
        XCTAssert(sut.options.count == (promotionUsageTypes.count - 1))

        XCTAssert(sut.options[0].imageName == ConstantsImages.Promotions.shopping)
        XCTAssert(sut.options[0].title == Localizables.promotions.key_physical_commerce_text)
        XCTAssert(sut.options[0].isSelected == false)

        XCTAssert(sut.options[1].imageName == ConstantsImages.Promotions.ecommerce)
        XCTAssert(sut.options[1].title == Localizables.promotions.key_online_commerce_text)
        XCTAssert(sut.options[1].isSelected == false)

    }

    func test_givenATitleAndPromotionUsageTypesWithAllTypesWithOnlineSelected_whenICallInitWithTitlePromotionType_thenTitleShouldMatchAndOptionsShouldHaveTheCorrectValuesInTheRightOrderAndOnlineShouldBeSelected() {

        // given
        let title = "title"
        let promotionUsageTypes: [PromotionUsageType] = [.physical, .online, .unknown]
        let promotionUsageTypesSelected: [PromotionUsageType] = [.online]

        // when
        let sut = OptionsSelectionableDisplayData(title: title, promotionUsageTypes: promotionUsageTypes, promotionUsageTypesSelected: promotionUsageTypesSelected)

        // then
        XCTAssert(sut.title == title)
        XCTAssert(sut.options.count == (promotionUsageTypes.count - 1))

        XCTAssert(sut.options[0].imageName == ConstantsImages.Promotions.shopping)
        XCTAssert(sut.options[0].title == Localizables.promotions.key_physical_commerce_text)
        XCTAssert(sut.options[0].isSelected == false)

        XCTAssert(sut.options[1].imageName == ConstantsImages.Promotions.ecommerce)
        XCTAssert(sut.options[1].title == Localizables.promotions.key_online_commerce_text)
        XCTAssert(sut.options[1].isSelected == true)

    }

}
