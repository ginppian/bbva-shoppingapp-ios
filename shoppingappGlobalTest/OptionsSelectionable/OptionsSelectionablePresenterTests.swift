//
//  OptionsSelectionablePresenterTests.swift
//  shoppingapp
//
//  Created by jesus.martinez on 21/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class OptionsSelectionablePresenterTests: XCTestCase {

    let sut = OptionsSelectionablePresenter()

    // MARK: - onConfigureView

    func test_givenDisplayData_whenICallOnConfigureView_thenICallViewConfigureTitle() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.displayData = OptionsSelectionableDisplayData(title: "title")

        // when
        sut.onConfigureView()

        // then
        XCTAssert(dummyView.isCalledConfigureTitle == true)

    }

    func test_givenDisplayData_whenICallOnConfigureView_thenICallViewConfigureTitleWithTitleOfDisplayData() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.displayData = OptionsSelectionableDisplayData(title: "title")

        // when
        sut.onConfigureView()

        // then
        XCTAssert(dummyView.titleSent == sut.displayData!.title)

    }

    func test_givenDisplayData_whenICallOnConfigureView_thenICallViewShowOptions() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.displayData = OptionsSelectionableDisplayData(title: "title")

        // when
        sut.onConfigureView()

        // then
        XCTAssert(dummyView.isCalledShowOptions == true)

    }

    func test_givenDisplayData_whenICallOnConfigureView_thenICallViewShowOptionsWithOptionsEmptyThanDisplayData() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.displayData = OptionsSelectionableDisplayData(title: "title")

        // when
        sut.onConfigureView()

        // then
        XCTAssert(dummyView.optionsSent?.count == sut.displayData?.options.count)

    }

    func test_givenDisplayDataWithPromotionTypes_whenICallOnConfigureView_thenICallViewConfigureTitle() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.displayData = OptionsSelectionableDisplayData(title: "title", promotionTypes: [.discount, .point])

        // when
        sut.onConfigureView()

        // then
        XCTAssert(dummyView.isCalledConfigureTitle == true)

    }

    func test_givenDisplayDataWithPromotionTypes_whenICallOnConfigureView_thenICallViewConfigureTitleWithTitleOfDisplayData() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.displayData = OptionsSelectionableDisplayData(title: "title", promotionTypes: [.discount, .point])

        // when
        sut.onConfigureView()

        // then
        XCTAssert(dummyView.titleSent == sut.displayData!.title)

    }

    func test_givenDisplayDataWithPromotionTypes_whenICallOnConfigureView_thenICallViewShowOptions() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.displayData = OptionsSelectionableDisplayData(title: "title", promotionTypes: [.discount, .point])

        // when
        sut.onConfigureView()

        // then
        XCTAssert(dummyView.isCalledShowOptions == true)

    }

    func test_givenDisplayData_whenICallOnConfigureView_thenICallViewShowOptionsWithTheSameOptionsThanDisplayData() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.displayData = OptionsSelectionableDisplayData(title: "title", promotionTypes: [.discount, .point])

        // when
        sut.onConfigureView()

        // then
        XCTAssert(dummyView.optionsSent?.count == sut.displayData?.options.count)

    }

    func test_givenDisplayDataWithPromotionUsageTypes_whenICallOnConfigureView_thenICallViewConfigureTitle() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.displayData = OptionsSelectionableDisplayData(title: "title", promotionUsageTypes: [.physical])

        // when
        sut.onConfigureView()

        // then
        XCTAssert(dummyView.isCalledConfigureTitle == true)

    }

    func test_givenDisplayDataWithPromotionUsageTypes_whenICallOnConfigureView_thenICallViewConfigureTitleWithTitleOfDisplayData() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.displayData = OptionsSelectionableDisplayData(title: "title", promotionUsageTypes: [.physical])

        // when
        sut.onConfigureView()

        // then
        XCTAssert(dummyView.titleSent == sut.displayData!.title)

    }

    func test_givenDisplayDataWithPromotionUsageTypes_whenICallOnConfigureView_thenICallViewShowOptions() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.displayData = OptionsSelectionableDisplayData(title: "title", promotionUsageTypes: [.physical])

        // when
        sut.onConfigureView()

        // then
        XCTAssert(dummyView.isCalledShowOptions == true)

    }

    func test_givenDisplayDataWithPromotionUsageTypes_whenICallOnConfigureView_thenICallViewShowOptionsWithTheSameOptionsThanDisplayData() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.displayData = OptionsSelectionableDisplayData(title: "title", promotionUsageTypes: [.physical])

        // when
        sut.onConfigureView()

        // then
        XCTAssert(dummyView.optionsSent?.count == sut.displayData?.options.count)

    }

    class DummyView: OptionsSelectionableViewProtocol {

        var isCalledConfigureTitle = false
        var isCalledShowOptions = false
        var titleSent = ""
        var optionsSent: [OptionSelectionableDisplayData]?

        func configureTitle(withText text: String) {

            isCalledConfigureTitle = true

            titleSent = text

        }

        func showOptions(withDisplayData displayData: [OptionSelectionableDisplayData]) {

            isCalledShowOptions = true

            optionsSent = displayData
        }

    }

}
