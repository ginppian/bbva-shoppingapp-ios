//
//  DisplayItemPromotionTest.swift
//  shoppingapp
//
//  Created by ruben.fernandez on 10/11/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class DisplayItemCategoryTest: XCTestCase {

    func test_givenAPromotionBO_whenICallgGenerateDisplayItemPromotion_thenDisplayDataPanShouldBeMatchWithPromotionBO() {

        //given
        let categoriesBO = PromotionsGenerator.getCategoryBO()

        //When
        let displayData = DisplayItemCategory(fromCategory: categoriesBO.categories[0])
        let displayData2 = DisplayItemCategory(fromCategory: categoriesBO.categories[1])
        let displayData3 = DisplayItemCategory(fromCategory: categoriesBO.categories[2])
        let displayData4 = DisplayItemCategory(fromCategory: categoriesBO.categories[3])
        let displayData5 = DisplayItemCategory(fromCategory: categoriesBO.categories[4])
        let displayData6 = DisplayItemCategory(fromCategory: categoriesBO.categories[5])
        let displayData7 = DisplayItemCategory(fromCategory: categoriesBO.categories[6])
        let displayData8 = DisplayItemCategory(fromCategory: categoriesBO.categories[7])
        let displayData9 = DisplayItemCategory(fromCategory: categoriesBO.categories[8])
        let displayData10 = DisplayItemCategory(fromCategory: categoriesBO.categories[9])
        let displayData11 = DisplayItemCategory(fromCategory: categoriesBO.categories[10])
        let displayData12 = DisplayItemCategory(fromCategory: categoriesBO.categories[11])
        let displayData13 = DisplayItemCategory(fromCategory: categoriesBO.categories[12])
        let displayData14 = DisplayItemCategory(fromCategory: categoriesBO.categories[13])
        let displayData15 = DisplayItemCategory(fromCategory: categoriesBO.categories[14])
        let displayData16 = DisplayItemCategory(fromCategory: categoriesBO.categories[15])
        let displayData17 = DisplayItemCategory(fromCategory: categoriesBO.categories[16])
        let displayData18 = DisplayItemCategory(fromCategory: categoriesBO.categories[17])
        let displayData19 = DisplayItemCategory(fromCategory: categoriesBO.categories[18])
        let displayData20 = DisplayItemCategory(fromCategory: categoriesBO.categories[19])
        let displayData21 = DisplayItemCategory(fromCategory: categoriesBO.categories[20])
        let displayData22 = DisplayItemCategory(fromCategory: categoriesBO.categories[21])
        let displayData23 = DisplayItemCategory(fromCategory: categoriesBO.categories[22])
        let displayData24 = DisplayItemCategory(fromCategory: categoriesBO.categories[23])
        let displayData25 = DisplayItemCategory(fromCategory: categoriesBO.categories[24])
        let displayData26 = DisplayItemCategory(fromCategory: categoriesBO.categories[25])
        let displayData27 = DisplayItemCategory(fromCategory: categoriesBO.categories[26])
        let displayData28 = DisplayItemCategory(fromCategory: categoriesBO.categories[27])
        let displayData29 = DisplayItemCategory(fromCategory: categoriesBO.categories[28])
        let displayData30 = DisplayItemCategory(fromCategory: categoriesBO.categories[29])

        // then
        XCTAssert(displayData.categoryName == "Activities")
        XCTAssert(displayData.categoryImage == ConstantsImages.Promotions.activities)
        XCTAssert(displayData.categoryImageColor == .BBVA400)

        XCTAssert(displayData2.categoryName == "Auto")
        XCTAssert(displayData2.categoryImage == ConstantsImages.Promotions.auto)
        XCTAssert(displayData2.categoryImageColor == .BBVA400)

        XCTAssert(displayData3.categoryName == "Beauty and Fragances")
        XCTAssert(displayData3.categoryImage == ConstantsImages.Promotions.beautyAndFragances)
        XCTAssert(displayData3.categoryImageColor == .BBVA400 )

        XCTAssert(displayData4.categoryName == "Diet and Nutrition")
        XCTAssert(displayData4.categoryImage == ConstantsImages.Promotions.diet)
        XCTAssert(displayData4.categoryImageColor == .BBVA400 )

        XCTAssert(displayData5.categoryName == "Food and Gourmet")
        XCTAssert(displayData5.categoryImage == ConstantsImages.Promotions.foodAndGourmet)
        XCTAssert(displayData5.categoryImageColor == .BBVA400)

        XCTAssert(displayData6.categoryName == "Hobbies")
        XCTAssert(displayData6.categoryImage == ConstantsImages.Promotions.hobbies)
        XCTAssert(displayData6.categoryImageColor == .BBVA400)

        XCTAssert(displayData7.categoryName == "Home")
        XCTAssert(displayData7.categoryImage == ConstantsImages.Promotions.home)
        XCTAssert(displayData7.categoryImageColor == .BBVA400)

        XCTAssert(displayData8.categoryName == "Learning")
        XCTAssert(displayData8.categoryImage == ConstantsImages.Promotions.learning)
        XCTAssert(displayData8.categoryImageColor == .BBVA400)

        XCTAssert(displayData9.categoryName == "Rent")
        XCTAssert(displayData9.categoryImage == ConstantsImages.Promotions.rent)
        XCTAssert(displayData9.categoryImageColor == .BBVA400)

        XCTAssert(displayData10.categoryName == "Restaurant")
        XCTAssert(displayData10.categoryImage == ConstantsImages.Promotions.restaurant)
        XCTAssert(displayData10.categoryImageColor == .BBVA400)

        XCTAssert(displayData11.categoryName == "Shopping")
        XCTAssert(displayData11.categoryImage == ConstantsImages.Promotions.shopping)
        XCTAssert(displayData11.categoryImageColor == .BBVA400)

        XCTAssert(displayData12.categoryName == "Shows")
        XCTAssert(displayData12.categoryImage == ConstantsImages.Promotions.shows)
        XCTAssert(displayData12.categoryImageColor == .BBVA400)

        XCTAssert(displayData13.categoryName == "Technology")
        XCTAssert(displayData13.categoryImage == ConstantsImages.Common.technology)
        XCTAssert(displayData13.categoryImageColor == .BBVA400)

        XCTAssert(displayData14.categoryName == "Travel")
        XCTAssert(displayData14.categoryImage == ConstantsImages.Promotions.travel)
        XCTAssert(displayData14.categoryImageColor == .BBVA400)

        XCTAssert(displayData15.categoryName == "Bar")
        XCTAssert(displayData15.categoryImage == ConstantsImages.Promotions.bar)
        XCTAssert(displayData15.categoryImageColor == .BBVA400)

        XCTAssert(displayData16.categoryName == "Book Store")
        XCTAssert(displayData16.categoryImage == ConstantsImages.Promotions.bookStore)
        XCTAssert(displayData16.categoryImageColor == .BBVA400)

        XCTAssert(displayData17.categoryName == "Boutique")
        XCTAssert(displayData17.categoryImage == ConstantsImages.Promotions.boutique)
        XCTAssert(displayData17.categoryImageColor == .BBVA400)

        XCTAssert(displayData18.categoryName == "Doctor")
        XCTAssert(displayData18.categoryImage == ConstantsImages.Promotions.doctor)
        XCTAssert(displayData18.categoryImageColor == .BBVA400)

        XCTAssert(displayData19.categoryName == "Flower Shop")
        XCTAssert(displayData19.categoryImage == ConstantsImages.Promotions.flowerShop)
        XCTAssert(displayData19.categoryImageColor == .BBVA400 )

        XCTAssert(displayData20.categoryName == "Fuel Station")
        XCTAssert(displayData20.categoryImage == ConstantsImages.Promotions.fuelStation)
        XCTAssert(displayData20.categoryImageColor == .BBVA400 )

        XCTAssert(displayData21.categoryName == "Health")
        XCTAssert(displayData21.categoryImage == ConstantsImages.Promotions.health)
        XCTAssert(displayData21.categoryImageColor == .BBVA400)

        XCTAssert(displayData22.categoryName == "Jewelry Store")
        XCTAssert(displayData22.categoryImage == ConstantsImages.Promotions.jewelryStore)
        XCTAssert(displayData22.categoryImageColor == .BBVA400)

        XCTAssert(displayData23.categoryName == "Pets")
        XCTAssert(displayData23.categoryImage == ConstantsImages.Promotions.pets)
        XCTAssert(displayData23.categoryImageColor == .BBVA400)

        XCTAssert(displayData24.categoryName == "Pharmacy")
        XCTAssert(displayData24.categoryImage == ConstantsImages.Promotions.pharmacy)
        XCTAssert(displayData24.categoryImageColor == .BBVA400)

        XCTAssert(displayData25.categoryName == "Services")
        XCTAssert(displayData25.categoryImage == ConstantsImages.Promotions.services)
        XCTAssert(displayData25.categoryImageColor == .BBVA400)

        XCTAssert(displayData26.categoryName == "Stationery")
        XCTAssert(displayData26.categoryImage == ConstantsImages.Promotions.stationery)
        XCTAssert(displayData26.categoryImageColor == .BBVA400)

        XCTAssert(displayData27.categoryName == "Supermarket")
        XCTAssert(displayData27.categoryImage == ConstantsImages.Promotions.supermarket)
        XCTAssert(displayData27.categoryImageColor == .BBVA400)

        XCTAssert(displayData28.categoryName == "Toy Store")
        XCTAssert(displayData28.categoryImage == ConstantsImages.Promotions.toyStore)
        XCTAssert(displayData28.categoryImageColor == .BBVA400)

        XCTAssert(displayData29.categoryName == "Ecommerce")
        XCTAssert(displayData29.categoryImage == ConstantsImages.Promotions.ecommerce)
        XCTAssert(displayData29.categoryImageColor == .BBVA400)

        XCTAssert(displayData30.categoryName == "Other")
        XCTAssert(displayData30.categoryImage == ConstantsImages.Promotions.other)
        XCTAssert(displayData30.categoryImageColor == .BBVA400)
    }

}
