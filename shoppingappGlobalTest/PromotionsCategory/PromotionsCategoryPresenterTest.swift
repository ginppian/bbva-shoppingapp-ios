//
//  TransactionTypeFilterPresenterTest.swift
//  shoppingapp
//
//  Created by jesus.martinez on 3/8/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import XCTest
import RxSwift

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class PromotionsCategoryPresenterTest: XCTestCase {

    let sut = PromotionsCategoryPresenter()

    // MARK: - configureView

    func test_givenView_whenICallOnConfigureViewWithCategories_thenDisplayItemsNotNil() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let displayModel = PromotionsCategoryDisplayModel(categories: PromotionsGenerator.getCategoryBO(), shouldShowSelectFavoriteCategories: false)

        // when
        sut.onConfigureView(withDisplayModel: displayModel)

        // then
        XCTAssert(sut.displayItems != nil)
    }

    func test_givenView_whenICallOnConfigureViewWithCategories_thenIHaveDisplayItems() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let displayModel = PromotionsCategoryDisplayModel(categories: PromotionsGenerator.getCategoryBO(), shouldShowSelectFavoriteCategories: false)

        // when
        sut.onConfigureView(withDisplayModel: displayModel)

        // then
        XCTAssert(sut.displayItems?.items?.count == 30)
    }

    func test_givenView_whenICallOnConfigureViewWithCategories_thenShowSelectFavoriteCategoriesIsEqualInDisplayItems() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        let showSelectFavoriteCategories = true
        let displayModel = PromotionsCategoryDisplayModel(categories: PromotionsGenerator.getCategoryBO(), shouldShowSelectFavoriteCategories: showSelectFavoriteCategories)

        // when
        sut.onConfigureView(withDisplayModel: displayModel)

        // then
        XCTAssert(sut.displayItems?.showSelectFavoriteCategories == showSelectFavoriteCategories)
    }

    func test_givenView_whenICallOnConfigureViewWithCategories_thenIHaveDisplayItemsToShow() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        let displayModel = PromotionsCategoryDisplayModel(categories: PromotionsGenerator.getThreeCategoryBO(), shouldShowSelectFavoriteCategories: false)

        // when
        sut.onConfigureView(withDisplayModel: displayModel)

        // then
        XCTAssert(sut.displayItemsToShow != nil)

    }

    func test_givenView_whenICallOnConfigureViewWithCategories_thenShowSelectFavoriteCategoriesIsEqualInDisplayItemsToShow() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        let showSelectFavoriteCategories = true
        let displayModel = PromotionsCategoryDisplayModel(categories: PromotionsGenerator.getThreeCategoryBO(), shouldShowSelectFavoriteCategories: showSelectFavoriteCategories)

        // when
        sut.onConfigureView(withDisplayModel: displayModel)

        // then
        XCTAssert(sut.displayItemsToShow?.showSelectFavoriteCategories == showSelectFavoriteCategories)
    }

    func test_givenLessThanMaximumNumberOfCategoriesToShow_thenIHaveDisplayItemsShowAllCategoriesTrue() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        let displayModel = PromotionsCategoryDisplayModel(categories: PromotionsGenerator.getThreeCategoryBO(), shouldShowSelectFavoriteCategories: false)

        // when
        sut.onConfigureView(withDisplayModel: displayModel)

        // then
        XCTAssert(sut.displayItems?.showAllCategories == true)
    }

    func test_givenLessThanMaximumNumberOfCategoriesToShow_thenIHaveDisplayItemsToShowShowAllCategoriesTrue() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        let displayModel = PromotionsCategoryDisplayModel(categories: PromotionsGenerator.getThreeCategoryBO(), shouldShowSelectFavoriteCategories: false)

        // when
        sut.onConfigureView(withDisplayModel: displayModel)

        // then
        XCTAssert(sut.displayItemsToShow?.showAllCategories == true)
    }

    func test_givenLessThanMaximumNumberOfCategoriesToShowAndShowSelectFavoriteCategoriesTrue_thenICallConfigureFooter() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        let displayModel = PromotionsCategoryDisplayModel(categories: PromotionsGenerator.getThreeCategoryBO(), shouldShowSelectFavoriteCategories: true)

        // when
        sut.onConfigureView(withDisplayModel: displayModel)

        // then
        XCTAssert(dummyView.configureFooterIsCalled == true)
    }
    
    func test_givenLessThanMaximumNumberOfCategoriesToShowA_thenICallPrepareTable() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        let displayModel = PromotionsCategoryDisplayModel(categories: PromotionsGenerator.getThreeCategoryBO(), shouldShowSelectFavoriteCategories: false)

        // when
        sut.onConfigureView(withDisplayModel: displayModel)

        // then
        XCTAssert(dummyView.prepareTableIsCalled == true)
    }

    func test_givenView_whenICallOnConfigureViewWithMoreThanFourCategories_thenDisplayItemsShowAllCategoriesIsFalse() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let displayModel = PromotionsCategoryDisplayModel(categories: PromotionsGenerator.getFiveCategoryBO(), shouldShowSelectFavoriteCategories: false)

        // when
        sut.onConfigureView(withDisplayModel: displayModel)

        // then
        XCTAssert(sut.displayItems?.showAllCategories == false)

    }

    func test_givenView_whenICallOnConfigureViewWithMoreThanFourCategories_thenDisplayItemsToShowShowAllCategoriesIsFalse() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let displayModel = PromotionsCategoryDisplayModel(categories: PromotionsGenerator.getFiveCategoryBO(), shouldShowSelectFavoriteCategories: false)

        // when
        sut.onConfigureView(withDisplayModel: displayModel)

        // then
        XCTAssert(sut.displayItemsToShow?.showAllCategories == false)

    }

    func test_givenView_whenICallOnConfigureViewWithMoreThanFourCategories_thenICallConfigureFooter() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let displayModel = PromotionsCategoryDisplayModel(categories: PromotionsGenerator.getFiveCategoryBO(), shouldShowSelectFavoriteCategories: false)

        // when
        sut.onConfigureView(withDisplayModel: displayModel)

        // then
        XCTAssert(dummyView.configureFooterIsCalled == true)
    }

    func test_givenView_whenICallOnConfigureViewWithMoreThanFourCategories_thenICallPrepareTable() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let displayModel = PromotionsCategoryDisplayModel(categories: PromotionsGenerator.getFiveCategoryBO(), shouldShowSelectFavoriteCategories: false)

        // when
        sut.onConfigureView(withDisplayModel: displayModel)

        // then
        XCTAssert(dummyView.prepareTableIsCalled == true)
    }

    func test_givenView_whenICallOnConfigureViewWithFourCategories_thenIHaveFourCategories() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let displayModel = PromotionsCategoryDisplayModel(categories: PromotionsGenerator.getFourCategoryBO(), shouldShowSelectFavoriteCategories: false)

        // when
        sut.onConfigureView(withDisplayModel: displayModel)

        // then
        XCTAssert(sut.displayItemsToShow?.items?.count == 4)

        XCTAssert(sut.displayItemsToShow?.items![0].categoryName == "Activities" )
        XCTAssert(sut.displayItemsToShow?.items![0].categoryImage == ConstantsImages.Promotions.activities)
        XCTAssert(sut.displayItemsToShow?.items![0].categoryImageColor == .BBVA400 )

        XCTAssert(sut.displayItemsToShow?.items![1].categoryName == "Auto" )
        XCTAssert(sut.displayItemsToShow?.items![1].categoryImage == ConstantsImages.Promotions.auto)
        XCTAssert(sut.displayItemsToShow?.items![1].categoryImageColor == .BBVA400 )

        XCTAssert(sut.displayItemsToShow?.items![2].categoryName == "Beauty and Fragances" )
        XCTAssert(sut.displayItemsToShow?.items![2].categoryImage == ConstantsImages.Promotions.beautyAndFragances)
        XCTAssert(sut.displayItemsToShow?.items![2].categoryImageColor == .BBVA400 )

        XCTAssert(sut.displayItemsToShow?.items![3].categoryName == "Diet and Nutrition" )
        XCTAssert(sut.displayItemsToShow?.items![3].categoryImage == ConstantsImages.Promotions.diet)
        XCTAssert(sut.displayItemsToShow?.items![3].categoryImageColor == .BBVA400 )

    }

    func test_givenView_whenICallOnConfigureViewWithFiveCategories_thenIHaveFourCategories() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let displayModel = PromotionsCategoryDisplayModel(categories: PromotionsGenerator.getFiveCategoryBO(), shouldShowSelectFavoriteCategories: false)

        // when
        sut.onConfigureView(withDisplayModel: displayModel)

        // then
        XCTAssert(sut.displayItemsToShow?.items?.count == 4)

        XCTAssert(sut.displayItemsToShow?.items![0].categoryName == "Activities" )
        XCTAssert(sut.displayItemsToShow?.items![0].categoryImage == ConstantsImages.Promotions.activities)
        XCTAssert(sut.displayItemsToShow?.items![0].categoryImageColor == .BBVA400 )

        XCTAssert(sut.displayItemsToShow?.items![1].categoryName == "Auto" )
        XCTAssert(sut.displayItemsToShow?.items![1].categoryImage == ConstantsImages.Promotions.auto)
        XCTAssert(sut.displayItemsToShow?.items![1].categoryImageColor == .BBVA400 )

        XCTAssert(sut.displayItemsToShow?.items![2].categoryName == "Beauty and Fragances" )
        XCTAssert(sut.displayItemsToShow?.items![2].categoryImage == ConstantsImages.Promotions.beautyAndFragances)
        XCTAssert(sut.displayItemsToShow?.items![2].categoryImageColor == .BBVA400 )

        XCTAssert(sut.displayItemsToShow?.items![3].categoryName == "Diet and Nutrition" )
        XCTAssert(sut.displayItemsToShow?.items![3].categoryImage == ConstantsImages.Promotions.diet)
        XCTAssert(sut.displayItemsToShow?.items![3].categoryImageColor == .BBVA400 )
    }

    func test_givenCategories_whenICallOnConfigure_thenCategoriesBOAreSortAlphabetical() {

        // given
        let displayModel = PromotionsCategoryDisplayModel(categories: PromotionsGenerator.getNotSortCategoryBO(), shouldShowSelectFavoriteCategories: false)

        // when
        sut.onConfigureView(withDisplayModel: displayModel)

        // then
        var areSort = true
        for i in 0..<(sut.categoriesBO!.categories.count - 1) where sut.categoriesBO!.categories[i].name > sut.categoriesBO!.categories[i + 1].name {
                areSort = false
                break
        }

        XCTAssert(areSort == true)
    }

    func test_givenCategories_whenICallOnConfigure_thenDisplayItemsAreSortAlphabetical() {

        // given
        let displayModel = PromotionsCategoryDisplayModel(categories: PromotionsGenerator.getNotSortCategoryBO(), shouldShowSelectFavoriteCategories: false)

        // when
        sut.onConfigureView(withDisplayModel: displayModel)

        // then
        var areSort = true
        for i in 0..<(sut.categoriesBO!.categories.count - 1) where sut.displayItems!.items![i].categoryName! > sut.displayItems!.items![i + 1].categoryName! {
                areSort = false
                break
        }

        XCTAssert(areSort == true)

    }

    func test_givenCategories_whenICallOnConfigure_thenDisplayToShowItemsAreSortAlphabetical() {

        // given
        let displayModel = PromotionsCategoryDisplayModel(categories: PromotionsGenerator.getNotSortCategoryBO(), shouldShowSelectFavoriteCategories: false)

        // when
        sut.onConfigureView(withDisplayModel: displayModel)

        // then
        var areSort = true
        for i in 0..<(PromotionsCategoryPresenter.maximumNumberOfCategoriesToShow - 1) where sut.displayItemsToShow!.items![i].categoryName! > sut.displayItemsToShow!.items![i + 1].categoryName! {
                areSort = false
                break
        }

        XCTAssert(areSort == true)

    }

    func test_givenCategories_whenICallOnConfigure_thenICallViewWithDisplayItemsToShowAreSortAlphabetical() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let displayModel = PromotionsCategoryDisplayModel(categories: PromotionsGenerator.getNotSortCategoryBO(), shouldShowSelectFavoriteCategories: false)

        // when
        sut.onConfigureView(withDisplayModel: displayModel)

        // then
        var areSort = true
        for i in 0..<(PromotionsCategoryPresenter.maximumNumberOfCategoriesToShow - 1) where dummyView.sentDisplayItems!.items![i].categoryName! > dummyView.sentDisplayItems!.items![i + 1].categoryName! {
            areSort = false
            break
        }

        XCTAssert(areSort == true)

    }

    // MARK: - itemSelected

    func test_givenACategoriesAndIndex_whenICallItemSelected_thenICallViewInformCategorySelectedWithCategory() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        sut.categoriesBO = PromotionsGenerator.getFiveCategoryBO()

        // when
        sut.itemSelected(atIndex: 3)

        // then
        XCTAssert(dummyView.informCategorySelectedCalled == true)
        XCTAssert(dummyView.categorySelected === sut.categoriesBO!.categories[3])
    }
    
    // MARK: - showCategoriesByButtonPressed (showAllCategories)
    
    func test_givenDisplayItemsWithShowAllCategoriesFalse_whenShowCategoriesByButtonPressed_thenDisplayItemsToShowMatchWithDisplayItems() {
        
        // given
        sut.categoriesBO = PromotionsGenerator.getSevenCategoryBO()
        
        let displayItems = DisplayItemCategories()
        for category in (sut.categoriesBO?.categories)! {
            displayItems.items?.append(DisplayItemCategory(fromCategory: category))
        }
        displayItems.showAllCategories = false
        sut.displayItems = displayItems
        sut.displayItemsToShow = DisplayItemCategories()
        
        // when
        sut.showCategoriesByButtonPressed()
        
        // then
        XCTAssert(sut.displayItemsToShow?.items?.count == sut.displayItems?.items?.count)
        
        for index in 0...(sut.displayItemsToShow!.items!.count - 1) {
            XCTAssert(sut.displayItemsToShow?.items![index] == sut.displayItems?.items![index])
        }
    }
    
    func test_givenDisplayItemsWithShowAllCategoriesFalse_whenShowCategoriesByButtonPressed_thenDisplayItemsShowAllCategoriesIsTrue() {
        
        // given
        sut.categoriesBO = PromotionsGenerator.getSevenCategoryBO()
        
        let displayItems = DisplayItemCategories()
        for category in (sut.categoriesBO?.categories)! {
            displayItems.items?.append(DisplayItemCategory(fromCategory: category))
        }
        displayItems.showAllCategories = false
        sut.displayItems = displayItems
        sut.displayItemsToShow = DisplayItemCategories()
        
        // when
        sut.showCategoriesByButtonPressed()
        
        // then
        XCTAssert(sut.displayItems?.showAllCategories == true)
    }
    
    func test_givenDisplayItemsWithShowAllCategoriesFalse_whenShowCategoriesByButtonPressed_thenDisplayItemsToShowShowAllCategoriesIsTrue() {
        
        // given
        sut.categoriesBO = PromotionsGenerator.getSevenCategoryBO()
        
        let displayItems = DisplayItemCategories()
        for category in (sut.categoriesBO?.categories)! {
            displayItems.items?.append(DisplayItemCategory(fromCategory: category))
        }
        displayItems.showAllCategories = false
        sut.displayItems = displayItems
        sut.displayItemsToShow = DisplayItemCategories()
        
        // when
        sut.showCategoriesByButtonPressed()
        
        // then
        XCTAssert(sut.displayItemsToShow?.showAllCategories == true)
    }
    
    func test_givenDisplayItemsWithShowAllCategoriesFalse_whenShowCategoriesByButtonPressed_thenConfigureFooterIsCalled() {
        
        // given
        let dummyView = DummyView()
        sut.view = dummyView
        
        sut.categoriesBO = PromotionsGenerator.getSevenCategoryBO()
        
        let displayItems = DisplayItemCategories()
        for category in (sut.categoriesBO?.categories)! {
            displayItems.items?.append(DisplayItemCategory(fromCategory: category))
        }
        displayItems.showAllCategories = false
        sut.displayItems = displayItems
        sut.displayItemsToShow = DisplayItemCategories()
        
        // when
        sut.showCategoriesByButtonPressed()
        
        // then
        XCTAssert(dummyView.configureFooterIsCalled == true)
    }
    
    func test_givenDisplayItemsWithShowAllCategoriesFalse_whenShowCategoriesByButtonPressed_thenShowSelectFavoriteCategoriesSentMatch() {
        
        // given
        let dummyView = DummyView()
        sut.view = dummyView
        
        sut.categoriesBO = PromotionsGenerator.getSevenCategoryBO()
        
        let displayItems = DisplayItemCategories()
        for category in (sut.categoriesBO?.categories)! {
            displayItems.items?.append(DisplayItemCategory(fromCategory: category))
        }
        displayItems.showAllCategories = false
        sut.displayItems = displayItems
        sut.displayItemsToShow = DisplayItemCategories()
        
        // when
        sut.showCategoriesByButtonPressed()
        
        // then
        XCTAssert(dummyView.showSelectFavoriteCategoriesSent == sut.displayItems?.showSelectFavoriteCategories)
    }
    
    func test_givenDisplayItemsWithShowAllCategoriesFalse_whenShowCategoriesByButtonPressed_thenShowAllCategoriesSentIsTrue() {
        
        // given
        let dummyView = DummyView()
        sut.view = dummyView
        
        sut.categoriesBO = PromotionsGenerator.getSevenCategoryBO()
        
        let displayItems = DisplayItemCategories()
        for category in (sut.categoriesBO?.categories)! {
            displayItems.items?.append(DisplayItemCategory(fromCategory: category))
        }
        displayItems.showAllCategories = false
        sut.displayItems = displayItems
        sut.displayItemsToShow = DisplayItemCategories()
        
        // when
        sut.showCategoriesByButtonPressed()
        
        // then
        XCTAssert(dummyView.showAllCategoriesSent == true)
    }
    
    func test_givenDisplayItemsWithShowAllCategoriesFalse_whenShowCategoriesByButtonPressed_thenConfigureCategoriesButtonForSeeLessCategoriesIsCalled() {
        
        // given
        let dummyView = DummyView()
        sut.view = dummyView
        
        sut.categoriesBO = PromotionsGenerator.getSevenCategoryBO()
        
        let displayItems = DisplayItemCategories()
        for category in (sut.categoriesBO?.categories)! {
            displayItems.items?.append(DisplayItemCategory(fromCategory: category))
        }
        displayItems.showAllCategories = false
        sut.displayItems = displayItems
        sut.displayItemsToShow = DisplayItemCategories()
        
        // when
        sut.showCategoriesByButtonPressed()
        
        // then
        XCTAssert(dummyView.configureCategoriesButtonForSeeLessCategoriesIsCalled == true)
    }
    
    func test_givenDisplayItemsWithShowAllCategoriesFalse_whenShowCategoriesByButtonPressed_thenPrepareTableIsCalled() {
        
        // given
        let dummyView = DummyView()
        sut.view = dummyView
        
        sut.categoriesBO = PromotionsGenerator.getSevenCategoryBO()
        
        let displayItems = DisplayItemCategories()
        for category in (sut.categoriesBO?.categories)! {
            displayItems.items?.append(DisplayItemCategory(fromCategory: category))
        }
        displayItems.showAllCategories = false
        sut.displayItems = displayItems
        sut.displayItemsToShow = DisplayItemCategories()
        
        // when
        sut.showCategoriesByButtonPressed()
        
        // then
        XCTAssert(dummyView.prepareTableIsCalled == true)
    }
    
    func test_givenDisplayItemsWithShowAllCategoriesFalse_whenShowCategoriesByButtonPressed_thenSentDisplayItemsMatch() {
        
        // given
        let dummyView = DummyView()
        sut.view = dummyView
        
        sut.categoriesBO = PromotionsGenerator.getSevenCategoryBO()
        
        let displayItems = DisplayItemCategories()
        for category in (sut.categoriesBO?.categories)! {
            displayItems.items?.append(DisplayItemCategory(fromCategory: category))
        }
        displayItems.showAllCategories = false
        sut.displayItems = displayItems
        sut.displayItemsToShow = DisplayItemCategories()
        
        // when
        sut.showCategoriesByButtonPressed()
        
        // then
        XCTAssert(dummyView.sentDisplayItems?.items?.count == sut.displayItemsToShow?.items?.count)
        
        for index in 0...(sut.displayItemsToShow!.items!.count - 1) {
            XCTAssert(dummyView.sentDisplayItems?.items![index] == sut.displayItemsToShow?.items![index])
        }
    }
    
    func test_givenDisplayItemsWithShowAllCategoriesFalse_whenShowCategoriesByButtonPressed_thenTrackPromotionsSeeAllCategoriesIsCalled() {
        
        // given
        let dummyView = DummyView()
        sut.view = dummyView
        
        sut.categoriesBO = PromotionsGenerator.getSevenCategoryBO()
        
        let displayItems = DisplayItemCategories()
        for category in (sut.categoriesBO?.categories)! {
            displayItems.items?.append(DisplayItemCategory(fromCategory: category))
        }
        displayItems.showAllCategories = false
        sut.displayItems = displayItems
        sut.displayItemsToShow = DisplayItemCategories()
        
        // when
        sut.showCategoriesByButtonPressed()
        
        // then
        XCTAssert(dummyView.trackPromotionsSeeAllCategoriesIsCalled == true)
    }
    
    // MARK: - showCategoriesByButtonPressed (showLessCategories)
    
    func test_givenDisplayItemsWithShowAllCategoriesTrue_whenShowCategoriesByButtonPressed_thenDisplayItemsToShowCountEqualsToMaximumNumberOfCategoriesToShowAndDisplayItemsToShowMatchWithDisplayItems() {
        
        // given
        sut.categoriesBO = PromotionsGenerator.getSevenCategoryBO()
        
        let displayItems = DisplayItemCategories()
        for category in (sut.categoriesBO?.categories)! {
            displayItems.items?.append(DisplayItemCategory(fromCategory: category))
        }
        displayItems.showAllCategories = true
        sut.displayItems = displayItems
        sut.displayItemsToShow = DisplayItemCategories()
        
        // when
        sut.showCategoriesByButtonPressed()
        
        // then
        XCTAssert(sut.displayItemsToShow?.items?.count == PromotionsCategoryPresenter.maximumNumberOfCategoriesToShow)
        
        for index in 0...(sut.displayItemsToShow!.items!.count - 1) {
            XCTAssert(sut.displayItemsToShow?.items![index] == sut.displayItems?.items![index])
        }
    }
    
    func test_givenDisplayItemsWithShowAllCategoriesTrue_whenShowCategoriesByButtonPressed_thenDisplayItemsShowAllCategoriesIsFalse() {
        
        // given
        sut.categoriesBO = PromotionsGenerator.getSevenCategoryBO()
        
        let displayItems = DisplayItemCategories()
        for category in (sut.categoriesBO?.categories)! {
            displayItems.items?.append(DisplayItemCategory(fromCategory: category))
        }
        displayItems.showAllCategories = true
        sut.displayItems = displayItems
        sut.displayItemsToShow = DisplayItemCategories()
        
        // when
        sut.showCategoriesByButtonPressed()
        
        // then
        XCTAssert(sut.displayItems?.showAllCategories == false)
    }
    
    func test_givenDisplayItemsWithShowAllCategoriesTrue_whenShowCategoriesByButtonPressed_thenDisplayItemsToShowShowAllCategoriesIsFalse() {
        
        // given
        sut.categoriesBO = PromotionsGenerator.getSevenCategoryBO()
        
        let displayItems = DisplayItemCategories()
        for category in (sut.categoriesBO?.categories)! {
            displayItems.items?.append(DisplayItemCategory(fromCategory: category))
        }
        displayItems.showAllCategories = true
        sut.displayItems = displayItems
        sut.displayItemsToShow = DisplayItemCategories()
        
        // when
        sut.showCategoriesByButtonPressed()
        
        // then
        XCTAssert(sut.displayItemsToShow?.showAllCategories == false)
    }
    
    func test_givenDisplayItemsWithShowAllCategoriesTrue_whenShowCategoriesByButtonPressed_thenConfigureFooterIsCalled() {
        
        // given
        let dummyView = DummyView()
        sut.view = dummyView
        
        sut.categoriesBO = PromotionsGenerator.getSevenCategoryBO()
        
        let displayItems = DisplayItemCategories()
        for category in (sut.categoriesBO?.categories)! {
            displayItems.items?.append(DisplayItemCategory(fromCategory: category))
        }
        displayItems.showAllCategories = true
        sut.displayItems = displayItems
        sut.displayItemsToShow = DisplayItemCategories()
        
        // when
        sut.showCategoriesByButtonPressed()
        
        // then
        XCTAssert(dummyView.configureFooterIsCalled == true)
    }
    
    func test_givenDisplayItemsWithShowAllCategoriesTrue_whenShowCategoriesByButtonPressed_thenShowSelectFavoriteCategoriesSentMatch() {
        
        // given
        let dummyView = DummyView()
        sut.view = dummyView
        
        sut.categoriesBO = PromotionsGenerator.getSevenCategoryBO()
        
        let displayItems = DisplayItemCategories()
        for category in (sut.categoriesBO?.categories)! {
            displayItems.items?.append(DisplayItemCategory(fromCategory: category))
        }
        displayItems.showAllCategories = true
        sut.displayItems = displayItems
        sut.displayItemsToShow = DisplayItemCategories()
        
        // when
        sut.showCategoriesByButtonPressed()
        
        // then
        XCTAssert(dummyView.showSelectFavoriteCategoriesSent == sut.displayItems?.showSelectFavoriteCategories)
    }
    
    func test_givenDisplayItemsWithShowAllCategoriesTrue_whenShowCategoriesByButtonPressed_thenShowAllCategoriesSentIsFalse() {
        
        // given
        let dummyView = DummyView()
        sut.view = dummyView
        
        sut.categoriesBO = PromotionsGenerator.getSevenCategoryBO()
        
        let displayItems = DisplayItemCategories()
        for category in (sut.categoriesBO?.categories)! {
            displayItems.items?.append(DisplayItemCategory(fromCategory: category))
        }
        displayItems.showAllCategories = true
        sut.displayItems = displayItems
        sut.displayItemsToShow = DisplayItemCategories()
        
        // when
        sut.showCategoriesByButtonPressed()
        
        // then
        XCTAssert(dummyView.showAllCategoriesSent == false)
    }
    
    func test_givenDisplayItemsWithShowAllCategoriesTrue_whenShowCategoriesByButtonPressed_thenConfigureCategoriesButtonForSeeAllCategoriesIsCalled() {
        
        // given
        let dummyView = DummyView()
        sut.view = dummyView
        
        sut.categoriesBO = PromotionsGenerator.getSevenCategoryBO()
        
        let displayItems = DisplayItemCategories()
        for category in (sut.categoriesBO?.categories)! {
            displayItems.items?.append(DisplayItemCategory(fromCategory: category))
        }
        displayItems.showAllCategories = true
        sut.displayItems = displayItems
        sut.displayItemsToShow = DisplayItemCategories()
        
        // when
        sut.showCategoriesByButtonPressed()
        
        // then
        XCTAssert(dummyView.configureCategoriesButtonForSeeAllCategoriesIsCalled == true)
    }
    
    func test_givenDisplayItemsWithShowAllCategoriesTrue_whenShowCategoriesByButtonPressed_thenPrepareTableIsCalled() {
        
        // given
        let dummyView = DummyView()
        sut.view = dummyView
        
        sut.categoriesBO = PromotionsGenerator.getSevenCategoryBO()
        
        let displayItems = DisplayItemCategories()
        for category in (sut.categoriesBO?.categories)! {
            displayItems.items?.append(DisplayItemCategory(fromCategory: category))
        }
        displayItems.showAllCategories = true
        sut.displayItems = displayItems
        sut.displayItemsToShow = DisplayItemCategories()
        
        // when
        sut.showCategoriesByButtonPressed()
        
        // then
        XCTAssert(dummyView.prepareTableIsCalled == true)
    }
    
    func test_givenDisplayItemsWithShowAllCategoriesTrue_whenShowCategoriesByButtonPressed_thenSentDisplayItemsMatch() {
        
        // given
        let dummyView = DummyView()
        sut.view = dummyView
        
        sut.categoriesBO = PromotionsGenerator.getSevenCategoryBO()
        
        let displayItems = DisplayItemCategories()
        for category in (sut.categoriesBO?.categories)! {
            displayItems.items?.append(DisplayItemCategory(fromCategory: category))
        }
        displayItems.showAllCategories = true
        sut.displayItems = displayItems
        sut.displayItemsToShow = DisplayItemCategories()
        
        // when
        sut.showCategoriesByButtonPressed()
        
        // then
        XCTAssert(dummyView.sentDisplayItems?.items?.count == sut.displayItemsToShow?.items?.count)
        
        for index in 0...(sut.displayItemsToShow!.items!.count - 1) {
            XCTAssert(dummyView.sentDisplayItems?.items![index] == sut.displayItemsToShow?.items![index])
        }
    }

    // MARK: - Dummy

    class DummyView: PromotionsCategoryViewProtocol {
        
        var prepareTableIsCalled = false
        var configureFooterIsCalled = false
        var showSelectFavoriteCategoriesSent: Bool?
        var showAllCategoriesSent: Bool?
        var informCategorySelectedCalled = false
        var categorySelected: CategoryBO?
        var displayItemsCount = 0
        var sentDisplayItems: DisplayItemCategories?
        
        var configureCategoriesButtonForSeeAllCategoriesIsCalled = false
        var configureCategoriesButtonForSeeLessCategoriesIsCalled = false
        var trackPromotionsSeeAllCategoriesIsCalled = false

        func prepareTable(withDisplayItems displayItems: DisplayItemCategories) {

            prepareTableIsCalled = true
            displayItemsCount = displayItems.items!.count
            sentDisplayItems = displayItems
        }

        func configureFooter(showSelectFavoriteCategories: Bool, showAllCategories: Bool) {

            configureFooterIsCalled = true
            showSelectFavoriteCategoriesSent = showSelectFavoriteCategories
            showAllCategoriesSent = showAllCategories
        }

        func informCategorySelected(withCategory category: CategoryBO) {

            informCategorySelectedCalled = true
            categorySelected = category
        }
        
        func configureCategoriesButtonForLessCategories() {
            configureCategoriesButtonForSeeLessCategoriesIsCalled = true
        }
        
        func configureCategoriesButtonForSeeAllCategories() {
            configureCategoriesButtonForSeeAllCategoriesIsCalled = true
        }
        
        func trackPromotionsSeeAllCategories() {
            trackPromotionsSeeAllCategoriesIsCalled = true
        }
    }
}
