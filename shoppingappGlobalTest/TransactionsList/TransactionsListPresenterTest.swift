//
//  TransactionsListPresenterTest.swift
//  shoppingapp
//
//  Created by jesus.martinez on 7/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import XCTest
import RxSwift
import CellsNativeComponents

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class TransactionsListPresenterTest: XCTestCase {

    var sut: TransactionsListPresenter<TransactionsListDummyView>!
    var dummyBusManager: TransactionsListDummyBusManager!
    var dummyUiNavigationController: DummyUiNavigationController!

    override func setUp() {
        super.setUp()
        dummyBusManager = TransactionsListDummyBusManager()
        dummyUiNavigationController = DummyUiNavigationController()
        dummyBusManager.navigationController = dummyUiNavigationController
        sut = TransactionsListPresenter<TransactionsListDummyView>(busManager: dummyBusManager)
    }

    override func tearDown() {
        super.tearDown()
    }
    
    // MARK: - viewWillAppearAfterDismiss

    func test_givenAny_whenICallViewWillAppearAfterDismiss_thenICallViewSendScreenOmniture() {
        
        // given
        let dummyView = TransactionsListDummyView()
        sut.view = dummyView
        
        // when
        sut.viewWillAppearAfterDismiss()
        
        // then
        XCTAssert(dummyView.isCalledSendScreenOmniture == true)
        
    }

    // MARK: LoadData

    func test_givenTransactionsBONilAndViewDidLoadFalse_whenICallLoadData_thenICallViewShowSkeleton() {

        // given
        let dummyInteractor = TransactionListDummyInteractor()
        let dummyView = TransactionsListDummyView()

        sut.interactor = dummyInteractor
        sut.view = dummyView
        sut.transactionsBO = nil

        let dummyCardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = dummyCardBO

        // when
        sut.loadData()

        // then
        XCTAssert(dummyView.isCalledShowSkeleton == true)

    }

    func test_givenTransactionsBONilAndViewDidLoadTrue_whenICallLoadData_thenIDontCallViewShowSkeleton() {

        // given
        let dummyInteractor = TransactionListDummyInteractor()
        let dummyView = TransactionsListDummyView()

        sut.interactor = dummyInteractor
        sut.view = dummyView
        sut.transactionsBO = nil
        sut.viewDidLoad = true

        let dummyCardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = dummyCardBO

        // when
        sut.loadData()

        // then
        XCTAssert(dummyView.isCalledShowSkeleton == false)

    }

    func test_givenTransactionsBONilAndCardBOAndViewDidLoadFalse_whenICallLoadData_thenICallInteractorProvideTransactions() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        let dummyInteractor = TransactionListDummyInteractor()
        sut.interactor = dummyInteractor

        // when
        sut.loadData()

        // then
        XCTAssert(dummyInteractor.isCalledProvideTransactions == true)

    }

    func test_givenTransactionsBONilAndCardBOAndViewDidLoadFalse_whenICallLoadData_thenCallProvideTransactionsInteractorWithTheCardIdOfCard() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        let dummyInteractor = TransactionListDummyInteractor()
        sut.interactor = dummyInteractor

        // when
        sut.loadData()

        // then
        XCTAssert(dummyInteractor.cardId == cardBO.cardId)

    }

    func test_givenTransactionsBONilAndCardBOAndViewDidLoadFalse_whenICallLoadDataAndInteractorReturnSuccess_thenTransactionsBOFilled() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        let dummyInteractor = TransactionListDummyInteractor()

        sut.interactor = dummyInteractor

        // when
        sut.loadData()

        // then
        XCTAssert(sut.transactionsBO != nil)
    }

    func test_givenTransactionsBONilAndCardBOAndViewDidLoadFalseAndNoFilters_whenICallLoadDataAndInteractorReturnSuccess204_thenICallViewShowNoContent() {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 204
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        let dummyView = TransactionsListDummyView()

        sut.view = dummyView

        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.transactionsBO = transactionsBO

        sut.interactor = dummyInteractor

        // when
        sut.loadData()

        // then
        XCTAssert(dummyView.isCalledShowNoContent == true)
    }

    func test_givenTransactionsBONilAndCardBOAndViewDidLoadFalseAndSomeFilters_whenICallLoadDataAndInteractorReturnSuccess204_thenICallViewShowNoResults() {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 204
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        let dummyView = TransactionsListDummyView()
        sut.view = dummyView

        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.transactionsBO = transactionsBO
        sut.interactor = dummyInteractor

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "concept"
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.loadData()

        // then
        XCTAssert(dummyView.isCalledShowNoResults == true)
    }

    func test_givenTransactionsBONilAndCardBOAndViewDidLoadFalse_whenICallLoadDataAndInteractorReturnSuccess200_thenICallViewHideNoContent() {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 200
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        let dummyView = TransactionsListDummyView()

        sut.view = dummyView

        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.transactionsBO = transactionsBO

        sut.interactor = dummyInteractor

        // when
        sut.loadData()

        // then
        XCTAssert(dummyView.isCalledHideSkeleton == true)
    }

    func test_givenTransactionsBONilAndCardBOAndViewDidLoadFalse_whenICallLoadDataAndInteractorReturnSuccess200_thenICallViewUpdateForNoMoreContent() {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 200
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        let dummyView = TransactionsListDummyView()

        sut.view = dummyView

        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.transactionsBO = transactionsBO

        sut.interactor = dummyInteractor

        // when
        sut.loadData()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == true)
    }

    func test_givenTransactionsBONilAndCardBOAndViewDidLoadFalse_whenICallLoadDataAndInteractorReturnSuccess200WithTransactions_thenICallViewShowTransactions() {

        // given
        let transactionsBO = TransactionEntityDummyGenerator.transactionsBOWithTransactions()

        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        let dummyView = TransactionsListDummyView()

        sut.view = dummyView

        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.transactionsBO = transactionsBO

        sut.interactor = dummyInteractor

        // when
        sut.loadData()

        // then
        XCTAssert(dummyView.isCalledShowTransactions == true)
    }

    func test_givenTransactionsBONilAndCardBOAndViewDidLoadFalse_whenICallLoadDataAndInteractorReturnSuccess200WithTransactions_thenICallViewShowTransactionsWithDisplayData() {

        // given
        let transactionsBO = TransactionEntityDummyGenerator.transactionsBOWithTransactions()

        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        let dummyView = TransactionsListDummyView()

        sut.view = dummyView

        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.transactionsBO = transactionsBO

        sut.interactor = dummyInteractor

        // when
        sut.loadData()

        // then
        XCTAssert(dummyView.transactionsDisplayData != nil)
    }

    func test_givenTransactionsBONilAndCardBOAndViewDidLoadFalse_whenICallLoadDataAndInteratorReturnError_thenTransactionsBOShouldBeNil() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.forceError = true

        sut.interactor = dummyInteractor

        // when
        sut.loadData()

        // then
        XCTAssert(sut.transactionsBO == nil)
    }

    func test_givenTransactionsBONilAndCardBOAndViewDidLoadFalse_whenICallLoadDataAndInteratorReturnError_thenICallViewShowErrorContent() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        let dummyView = TransactionsListDummyView()

        sut.view = dummyView

        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.forceError = true

        sut.interactor = dummyInteractor

        // when
        sut.loadData()

        // then
        XCTAssert(dummyView.isCalledShowErrorContent == true)
    }

    func test_givenTransactionsBONilAndCardBOAndViewDidLoadFalse_whenICallLoadDataAndInteratorReturnError_thenErrorBOShouldBeFilled() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.forceError = true

        sut.interactor = dummyInteractor

        // when
        sut.loadData()

        // then
        XCTAssert(sut.errorBO != nil)
    }

    func test_givenTransactionsBOWithStatus204AndViewDidLoadFalseAndNoFilters_whenICallLoadData_thenPresenterCallViewShowNoContent() {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 204
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        let dummyView = TransactionsListDummyView()

        sut.view = dummyView
        sut.transactionsBO = transactionsBO

        let dummyCardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = dummyCardBO

        // when
        sut.loadData()

        // then
        XCTAssert(dummyView.isCalledShowNoContent == true)
        XCTAssert(dummyView.isCalledShowTransactions == false)

    }

    func test_givenTransactionsBOWithStatus204AndViewDidLoadFalseAndSomeFilters_whenICallLoadData_thenPresenterCallViewShowNoResults() {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 204
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        let dummyView = TransactionsListDummyView()

        sut.view = dummyView
        sut.transactionsBO = transactionsBO

        let dummyCardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = dummyCardBO

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "concept"
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.loadData()

        // then
        XCTAssert(dummyView.isCalledShowNoResults == true)
        XCTAssert(dummyView.isCalledShowTransactions == false)

    }

    func test_givenTransactionsBOWithStatus200AndWithTransactionsAndViewDidLoadFalse_whenICallLoadData_thenPresenterCallViewShowTransactions() {

        // given
        let transactionsBO = TransactionEntityDummyGenerator.transactionsBOWithTransactions()

        let dummyView = TransactionsListDummyView()

        sut.view = dummyView
        sut.transactionsBO = transactionsBO

        // when
        sut.loadData()

        // then
        XCTAssert(dummyView.isCalledShowTransactions == true)
        XCTAssert(dummyView.isCalledShowNoContent == false)
        XCTAssert(dummyView.isCalledShowNoResults == false)

    }

    func test_givenTransactionsBOWithStatus208AndViewDidLoadFalse_whenICallLoadData_thenNeitherViewShowTransactionsNorViewShowNoContentNorViewShowNoResultsAreCalled() {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 208
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        let dummyView = TransactionsListDummyView()

        sut.view = dummyView
        sut.transactionsBO = transactionsBO

        // when
        sut.loadData()

        // then
        XCTAssert(dummyView.isCalledShowTransactions == false)
        XCTAssert(dummyView.isCalledShowNoContent == false)
        XCTAssert(dummyView.isCalledShowNoResults == false)

    }

    func test_givenTransactionsBOWithStatus200AndWithTransactionsAndViewDidLoadFalse_whenICallLoadData_thenPresenterCallViewShowTransactionsWithATransactionsDisplayData() {

        // given
        let transactionsBO = TransactionEntityDummyGenerator.transactionsBOWithTransactions()

        let dummyView = TransactionsListDummyView()

        sut.view = dummyView
        sut.transactionsBO = transactionsBO

        // when
        sut.loadData()

        // then
        XCTAssert(dummyView.transactionsDisplayData != nil)
    }

    func test_givenTransactionsBOWithStatus200AndWithTransactionsAndViewDidLoadFalse_whenICallLoadData_thenCallAddTransactionsDisplayData() {

        // given
        let transactionsBO = TransactionEntityDummyGenerator.transactionsBOWithTransactions()

        let dummyView = TransactionsListDummyView()

        let dummyTransactionsDisplayData = DummyTransactionsDisplayData()

        sut.displayData = dummyTransactionsDisplayData
        sut.view = dummyView
        sut.transactionsBO = transactionsBO

        dummyView.transactionsBO = transactionsBO

        // when
        sut.loadData()

        // then
        XCTAssert(dummyTransactionsDisplayData.isCallAddTransactionsDisplayData == true)
    }

    func test_givenTransactionsBOWithStatus200AndWithTransactionsAndViewDidLoadFalse_whenICallLoadData_thenTransactionsDisplayDataShouldInitializatedWithGivenTransactionsOfTransactionsBO() {

        // given
        let transactionsBO = TransactionEntityDummyGenerator.transactionsBOWithTransactions()

        let dummyView = TransactionsListDummyView()

        let dummyTransactionsDisplayData = DummyTransactionsDisplayData()

        sut.displayData = dummyTransactionsDisplayData
        sut.view = dummyView
        sut.transactionsBO = transactionsBO

        dummyView.transactionsBO = transactionsBO

        // when
        sut.loadData()

        // then
        XCTAssert(dummyTransactionsDisplayData.transactions?.count == transactionsBO.transactions.count)
    }

    func test_givenTransactionsBOWithStatus200AndViewDidLoadFalse_whenICallLoadData_thenCallViewNoMoreContent() {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 200
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        let dummyView = TransactionsListDummyView()

        let dummyTransactionsDisplayData = DummyTransactionsDisplayData()

        sut.displayData = dummyTransactionsDisplayData
        sut.view = dummyView
        sut.transactionsBO = transactionsBO

        dummyView.transactionsBO = transactionsBO

        // when
        sut.loadData()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == true)
    }

    func test_givenAny_whenICallLoadData_thenICallViewSendScreenOmniture() {

        // given
        let dummyView = TransactionsListDummyView()
        sut.view = dummyView

        // when
        sut.loadData()

        // then
        XCTAssert(dummyView.isCalledSendScreenOmniture == true)

    }

    func test_givenTransactionsBOWithStatus204AndViewDidLoadTrue_whenICallLoadData_thenNeitherViewShowTransactionsNorViewShowNoContentNorViewShowNoResultsAreCalled() {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 204
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        let dummyView = TransactionsListDummyView()

        sut.viewDidLoad = true
        sut.view = dummyView
        sut.transactionsBO = transactionsBO

        // when
        sut.loadData()

        // then
        XCTAssert(dummyView.isCalledShowTransactions == false)
        XCTAssert(dummyView.isCalledShowNoContent == false)
        XCTAssert(dummyView.isCalledShowNoResults == false)

    }

    func test_givenTransactionsBOWithStatus200AndViewDidLoadTrueAndNoFilters_whenICallLoadData_thenNeitherViewShowTransactionsNorViewShowNoContentNorViewShowNoResultsAreCalled() {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 200
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        let dummyView = TransactionsListDummyView()

        sut.viewDidLoad = true
        sut.view = dummyView
        sut.transactionsBO = transactionsBO

        // when
        sut.loadData()

        // then
        XCTAssert(dummyView.isCalledShowTransactions == false)
        XCTAssert(dummyView.isCalledShowNoContent == false)
        XCTAssert(dummyView.isCalledShowNoResults == false)

    }

    func test_givenTransactionsBOWithStatus208AndViewDidLoadTrueAndNoFilters_whenICallLoadData_thenNeitherViewShowTransactionsNorViewShowNoContentNorViewShowNoResultsAreCalled() {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 208
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        let dummyView = TransactionsListDummyView()

        sut.viewDidLoad = true
        sut.view = dummyView
        sut.transactionsBO = transactionsBO

        // when
        sut.loadData()

        // then
        XCTAssert(dummyView.isCalledShowTransactions == false)
        XCTAssert(dummyView.isCalledShowNoContent == false)
        XCTAssert(dummyView.isCalledShowNoResults == false)

    }

    func test_givenTransactionsBOWithStatus200AndViewDidLoadTrue_whenICallLoadData_thenDoNotCallAddTransactionsDisplayData() {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 200
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        let dummyView = TransactionsListDummyView()

        let dummyTransactionsDisplayData = DummyTransactionsDisplayData()

        sut.viewDidLoad = true
        sut.displayData = dummyTransactionsDisplayData
        sut.view = dummyView
        sut.transactionsBO = transactionsBO

        dummyView.transactionsBO = transactionsBO

        // when
        sut.loadData()

        // then
        XCTAssert(dummyTransactionsDisplayData.isCallAddTransactionsDisplayData == false)
    }

    func test_givenTransactionsBOWithStatus200AndViewDidLoadTrue_whenICallLoadData_thenDoNotCallViewNoMoreContent() {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 200
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        let dummyView = TransactionsListDummyView()

        let dummyTransactionsDisplayData = DummyTransactionsDisplayData()

        sut.viewDidLoad = true
        sut.displayData = dummyTransactionsDisplayData
        sut.view = dummyView
        sut.transactionsBO = transactionsBO

        dummyView.transactionsBO = transactionsBO

        // when
        sut.loadData()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == false)
    }

    func test_givenAny_whenICallLoadData_thenViewDidLoadShouldBeTrue() {

        // given
        let dummyInteractor = TransactionListDummyInteractor()
        sut.interactor = dummyInteractor

        let dummyCardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = dummyCardBO

        // when
        sut.loadData()

        // then
        XCTAssert(sut.viewDidLoad == true)

    }
    
    func test_givenTransactionsBOWithStatus200AndWithTransactionsAndViewDidLoadFalse_whenICallLoadData_thenINotCallShowNoContent() {
        
        // given
        let transactionsBO = TransactionEntityDummyGenerator.transactionsBOWithTransactions()
        
        let dummyView = TransactionsListDummyView()
        
        sut.view = dummyView
        sut.transactionsBO = transactionsBO
        
        dummyView.transactionsBO = transactionsBO
        
        // when
        sut.loadData()
        
        // then
        XCTAssert(dummyView.isCalledShowNoContent == false)
    }
    
    func test_givenTransactionsBOWithStatus200AndWithoutTransactionsAndViewDidLoadFalse_whenICallLoadData_thenICallShowNoContent() {
        
        // given
        let transactionsBO = TransactionEntityDummyGenerator.transactionsBOWithoutTransactions()
        
        let dummyView = TransactionsListDummyView()
        
        sut.view = dummyView
        sut.transactionsBO = transactionsBO
        
        dummyView.transactionsBO = transactionsBO
        
        // when
        sut.loadData()
        
        // then
        XCTAssert(dummyView.isCalledShowNoContent == true)
    }
    
    func test_givenTransactionsBONilAndCardBOAndViewDidLoadFalse_whenICallLoadDataAndInteractorReturnSuccess200WithTransactions_thenINotCallShowNoContent() {
        
        // given
        let transactionsBO = TransactionEntityDummyGenerator.transactionsBOWithTransactions()
        
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO
        
        let dummyView = TransactionsListDummyView()
        
        sut.view = dummyView
        
        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.transactionsBO = transactionsBO
        
        sut.interactor = dummyInteractor
        
        // when
        sut.loadData()
        
        // then
        XCTAssert(dummyView.isCalledShowNoContent == false)
    }
    
    func test_givenTransactionsBONilAndCardBOAndViewDidLoadFalse_whenICallLoadDataAndInteractorReturnSuccess200WithoutTransactions_thenICallShowNoContent() {
        
        // given
        let transactionsBO = TransactionEntityDummyGenerator.transactionsBOWithoutTransactions()
        
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO
        
        let dummyView = TransactionsListDummyView()
        
        sut.view = dummyView
        
        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.transactionsBO = transactionsBO
        
        sut.interactor = dummyInteractor
        
        // when
        sut.loadData()
        
        // then
        XCTAssert(dummyView.isCalledShowNoContent == true)
    }

    // MARK: LoadDataNextPage

    func test_givenTransactionsBOWithStatus200AndNextPage_whenICallLoadDataNextPage_thenCallProvideTransactionsNextPage () {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 200
        transactionsEntity.pagination = GeneratorDummyPaginationEntity.completePaginationEntity()
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        let dummyInteractor = TransactionListDummyInteractor()

        let dummyTransactionsDisplayData = DummyTransactionsDisplayData()

        sut.interactor = dummyInteractor
        sut.transactionsBO = transactionsBO
        transactionsEntity.pagination = GeneratorDummyPaginationEntity.completePaginationEntity()
        sut.displayData = dummyTransactionsDisplayData

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyInteractor.isCallProvideTransactionsNextPage == true)

    }

    func test_givenTransactionsBOWithStatus200AndNoNextPage_whenICallLoadDataNextPage_thenDoNotCallProvideTransactionsNextPage () {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 200
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        let dummyInteractor = TransactionListDummyInteractor()

        sut.interactor = dummyInteractor
        sut.transactionsBO = transactionsBO

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyInteractor.isCallProvideTransactionsNextPage == false)

    }

    func test_givenTransactionsBOWithStatus200AndNextPage_whenICallLoadDataNextPageAndIsLoadingTheNextPage_thenDoNotCallProvideTransactionsNextPage () {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 200
        transactionsEntity.pagination = GeneratorDummyPaginationEntity.completePaginationEntity()
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        let dummyInteractor = TransactionListDummyInteractor()

        sut.interactor = dummyInteractor
        sut.transactionsBO = transactionsBO
        sut.isLoadingNextPage = true

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyInteractor.isCallProvideTransactionsNextPage == false)

    }

    func test_givenTransactionsBOWithStatus200AndNextPage_whenICallLoadDataNextPage_thenCallProvideTransactionsNextPageWithTransactionsBONextPage () {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 200
        transactionsEntity.pagination = GeneratorDummyPaginationEntity.completePaginationEntity()
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        let dummyInteractor = TransactionListDummyInteractor()

        let dummyTransactionsDisplayData = DummyTransactionsDisplayData()

        sut.interactor = dummyInteractor
        sut.transactionsBO = transactionsBO
        sut.displayData = dummyTransactionsDisplayData

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyInteractor.nextPage == transactionsBO.pagination?.links?.generateNextURL(isFinancial: true))

    }

    func test_givenTransactionsBOWithStatus200AndNextPage_whenICallLoadDataNextPageAndReturned200_thenPresenterCallViewShowTransactionsAndMoreContent () {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 200
        transactionsEntity.pagination = GeneratorDummyPaginationEntity.completePaginationEntity()
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.statusToReturn = 200

        let dummyView = TransactionsListDummyView()

        let dummyTransactionsDisplayData = DummyTransactionsDisplayData()

        sut.view = dummyView
        sut.interactor = dummyInteractor
        sut.transactionsBO = transactionsBO
        sut.displayData = dummyTransactionsDisplayData

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyView.isCalledShowTransactions == true)
        XCTAssert(dummyView.isCalledUpdateForMoreContent == true)

    }

    func test_givenTransactionsBOWithStatus200AndNextPage_whenICallLoadDataNextPageAndReturnedNextPageIsNil_thenPresenterCallViewNoMoreContent () {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 200
        transactionsEntity.pagination = GeneratorDummyPaginationEntity.completePaginationEntity()
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.statusToReturn = 200
        dummyInteractor.notReturnPagination = true

        let dummyView = TransactionsListDummyView()

        let dummyTransactionsDisplayData = DummyTransactionsDisplayData()

        sut.view = dummyView
        sut.interactor = dummyInteractor
        sut.transactionsBO = transactionsBO
        sut.displayData = dummyTransactionsDisplayData

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == true)

    }

    func test_givenTransactionsBOWithStatus200AndNextPage_whenICallLoadDataNextPageAndReturned204_thenPresenterCallViewUpdateForMoreContent () {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 200
        transactionsEntity.pagination = GeneratorDummyPaginationEntity.completePaginationEntity()
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.statusToReturn = 204

        let dummyView = TransactionsListDummyView()

        sut.view = dummyView
        sut.interactor = dummyInteractor
        sut.transactionsBO = transactionsBO

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyView.isCalledUpdateForMoreContent == true)
        XCTAssert(dummyView.isCalledShowTransactions == false)

    }

    func test_givenTransactionsBOWithStatus200_whenICallLoadDataNextPage_thenIsLoadingNextPageShouldBeTrue () {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 200
        transactionsEntity.pagination = GeneratorDummyPaginationEntity.completePaginationEntity()
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.presenter = sut

        sut.interactor = dummyInteractor
        sut.transactionsBO = transactionsBO

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyInteractor.wasLoadingNextPage == true)
    }

    func test_givenTransactionsBOWithStatus200AndNextPage_whenICallLoadDataNextPageAndReturnedSucess_thenIsLoadingNextPageShouldBeFalse () {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 200
        transactionsEntity.pagination = GeneratorDummyPaginationEntity.completePaginationEntity()
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.statusToReturn = 204

        let dummyView = TransactionsListDummyView()

        sut.view = dummyView
        sut.interactor = dummyInteractor
        sut.transactionsBO = transactionsBO

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(sut.isLoadingNextPage == false)

    }

    func test_givenTransactionsBOWithStatus200AndNextPage_whenICallLoadDataNextPageAndReturnedSucess_thenCallViewHideNextPageAnimation () {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 200
        transactionsEntity.pagination = GeneratorDummyPaginationEntity.completePaginationEntity()
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.statusToReturn = 204

        let dummyView = TransactionsListDummyView()

        sut.view = dummyView
        sut.interactor = dummyInteractor
        sut.transactionsBO = transactionsBO

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyView.isCalledHideNextPageAnimation == true)

    }

    func test_givenTransactionsBOWithStatus200AndNextPage_whenICallLoadDataNextPageAndReturnedError_thenIsLoadingNextPageShouldBeFalse () {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 200
        transactionsEntity.pagination = GeneratorDummyPaginationEntity.completePaginationEntity()
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.forceError = true

        let dummyView = TransactionsListDummyView()

        sut.view = dummyView
        sut.interactor = dummyInteractor
        sut.transactionsBO = transactionsBO

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(sut.isLoadingNextPage == false)

    }

    func test_givenTransactionsBOWithStatus200AndNextPage_whenICallLoadDataNextPageAndReturnedError_thenCallViewUpdateForNoMoreContent () {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 200
        transactionsEntity.pagination = GeneratorDummyPaginationEntity.completePaginationEntity()
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.forceError = true

        let dummyView = TransactionsListDummyView()

        sut.view = dummyView
        sut.interactor = dummyInteractor
        sut.transactionsBO = transactionsBO

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == true)

    }

    func test_givenTransactionsBOWithStatus200AndNextPage_whenICallLoadDataNextPageAndReturnedError_thenCallViewHideNextPageAnimation () {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 200
        transactionsEntity.pagination = GeneratorDummyPaginationEntity.completePaginationEntity()
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.forceError = true

        let dummyView = TransactionsListDummyView()

        sut.view = dummyView
        sut.interactor = dummyInteractor
        sut.transactionsBO = transactionsBO

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyView.isCalledHideNextPageAnimation == true)

    }

    func test_givenTransactionsBOWithStatus200AndNextPageAndTransactionsDisplayData_whenICallLoadDataNextPageAndReturned200_thenCallAddItemsTransactionsDisplayData () {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 200
        transactionsEntity.pagination = GeneratorDummyPaginationEntity.completePaginationEntity()
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.statusToReturn = 200

        let dummyTransactionsDisplayData = DummyTransactionsDisplayData()

        sut.interactor = dummyInteractor
        sut.transactionsBO = transactionsBO
        sut.displayData = dummyTransactionsDisplayData

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyTransactionsDisplayData.isCallAddTransactionsDisplayData == true)

    }

    func test_givenTransactionsBOWithStatus200AndNextPage_whenICallLoadDataNextPageAndReturned200_thenCallAddTransactionsDisplayData () {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 200
        transactionsEntity.pagination = GeneratorDummyPaginationEntity.completePaginationEntity()
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.statusToReturn = 200

        let dummyTransactionsDisplayData = DummyTransactionsDisplayData()

        sut.interactor = dummyInteractor
        sut.transactionsBO = transactionsBO
        sut.displayData = dummyTransactionsDisplayData

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyTransactionsDisplayData.isCallAddTransactionsDisplayData == true)

    }

    func test_givenTransactionsBOOWithStatus200AndNextPage_whenICallLoadDataNextPageAndReturned200_thenTransactionsBOPaginationShouldMatchWithPaginationReturned() {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 200
        transactionsEntity.pagination = GeneratorDummyPaginationEntity.completePaginationEntity()
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.statusToReturn = 200

        let dummyTransactionsDisplayData = DummyTransactionsDisplayData()

        sut.interactor = dummyInteractor
        sut.transactionsBO = transactionsBO
        sut.displayData = dummyTransactionsDisplayData

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(sut.transactionsBO?.pagination === dummyInteractor.pagination)

    }

    func test_givenTransactionsBOWithStatus200AndNextPage_whenICallLoadDataNextPageAndReturnedError_thenErrorBOShouldMatchWithReturned () {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 200
        transactionsEntity.pagination = GeneratorDummyPaginationEntity.completePaginationEntity()
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.forceError = true

        let dummyView = TransactionsListDummyView()

        sut.view = dummyView
        sut.interactor = dummyInteractor
        sut.transactionsBO = transactionsBO

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(sut.errorBO === dummyInteractor.errorBO)

    }

    func test_givenTransactionsBOWithStatus200AndNextPage_whenICallLoadDataNextPageAndReturnedError_thenCallViewShowError () {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 200
        transactionsEntity.pagination = GeneratorDummyPaginationEntity.completePaginationEntity()
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.forceError = true

        let dummyView = TransactionsListDummyView()

        sut.view = dummyView
        sut.interactor = dummyInteractor
        sut.transactionsBO = transactionsBO

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyView.isShowErrorCalled == true)

    }

    func test_givenTransactionsBOWithStatus200AndNextPage_whenICallLoadDataNextPageIsNotLoadingANextPage_thenCallViewShowNextPageAnimation() {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 200
        transactionsEntity.pagination = GeneratorDummyPaginationEntity.completePaginationEntity()
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        sut.transactionsBO = transactionsBO
        sut.isLoadingNextPage = false

        let dummyInteractor = TransactionListDummyInteractor()

        let dummyView = TransactionsListDummyView()
        sut.view = dummyView
        sut.interactor = dummyInteractor

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyView.isCalledShowNextPageAnimation == true)

    }

    func test_givenTransactionsBOWithStatus200AndNextPage_whenICallLoadDataNextPageIsLoadingANextPage_thenCallViewShowNextPageAnimation() {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 200
        transactionsEntity.pagination = GeneratorDummyPaginationEntity.completePaginationEntity()
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        sut.transactionsBO = transactionsBO
        sut.isLoadingNextPage = true

        let dummyInteractor = TransactionListDummyInteractor()

        let dummyView = TransactionsListDummyView()
        sut.view = dummyView
        sut.interactor = dummyInteractor

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyView.isCalledShowNextPageAnimation == false)

    }

    func test_givenTransactionsBOWithStatus200_whenICallLoadDataNextPageIsNotLoadingANextPage_thenCallViewShowNextPageAnimation() {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 200
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        sut.transactionsBO = transactionsBO
        sut.isLoadingNextPage = false

        let dummyInteractor = TransactionListDummyInteractor()

        let dummyView = TransactionsListDummyView()
        sut.view = dummyView
        sut.interactor = dummyInteractor

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyView.isCalledShowNextPageAnimation == false)

    }

    // MARK: - Present Transactions filter

    func test_givenACardBO_whenICallPresentTransactionsFilter_thenCallRouterPresentTransactionsFilter() {

        // given

        let dummyCardBO = CardsGenerator.getCardBOWithAlias().cards[0]

        sut.cardBO = dummyCardBO

        // when
        sut.presentTransactionsFilter()

        // then
        XCTAssert(dummyBusManager.isCalledNavigateNextTransactionsListScreen == true)

    }

    func test_givenACardBO_whenICallPresentTransactionsFilter_thenPublishOnBusTheSameCardBO() {

        // given

        let dummyCardBO = CardsGenerator.getCardBOWithAlias().cards[0]

        sut.cardBO = dummyCardBO

        // when
        sut.presentTransactionsFilter()

        // then
        XCTAssert(dummyBusManager.cardBO === dummyCardBO)

    }

    // MARK: - Load Data Transactions filter

    func test_givenACardBOAndTransactionsFilterTransport_whenICallCheckFilterData_thenShouldLoadFiltersShouldBeFalse() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        let dummyInteractor = TransactionListDummyInteractor()

        sut.interactor = dummyInteractor

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.checkFilterData()

        // then
        XCTAssert(sut.shouldLoadFilters == false)

    }

    func test_givenACardBOATransactionsFilterTransportAndShouldLoadFiltersTrue_whenICallCheckFilterData_thenCallProvideTransactions() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO
        sut.shouldLoadFilters = true

        let dummyInteractor = TransactionListDummyInteractor()

        sut.interactor = dummyInteractor

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.checkFilterData()

        // then
        XCTAssert(dummyInteractor.isCalledProvideTransactions == true)

    }

    func test_givenACardBOATransactionsFilterTransportAndShouldLoadFiltersFalse_whenICallCheckFilterData_thenCallDoNotProvideTransactions() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO
        sut.shouldLoadFilters = false

        let dummyInteractor = TransactionListDummyInteractor()

        sut.interactor = dummyInteractor

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.checkFilterData()

        // then
        XCTAssert(dummyInteractor.isCalledProvideTransactions == false)

    }

    func test_givenACardBOATransactionsFilterTransportAndShouldLoadFiltersTrue_whenICallCheckFilterData_thenCallViewShowSkeleton() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO
        sut.shouldLoadFilters = true

        let dummyInteractor = TransactionListDummyInteractor()

        let dummyView = TransactionsListDummyView()
        sut.view = dummyView

        sut.interactor = dummyInteractor

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.checkFilterData()

        // then
        XCTAssert(dummyView.isCalledShowSkeleton == true)

    }

    func test_givenACardBOATransactionsFilterTransportAndShouldLoadFiltersFalse_whenICallCheckFilterData_thenCallDoNotViewShowSkeleton() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO
        sut.shouldLoadFilters = false

        let dummyInteractor = TransactionListDummyInteractor()

        let dummyView = TransactionsListDummyView()
        sut.view = dummyView

        sut.interactor = dummyInteractor

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.checkFilterData()

        // then
        XCTAssert(dummyView.isCalledShowSkeleton == false)

    }

    func test_givenACardBOAndAPresenterWithOutTransactionFilterTransport_whenICallCheckFilterData_thenNotCallProvideTransactions() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        let dummyInteractor = TransactionListDummyInteractor()

        sut.interactor = dummyInteractor

        // when
        sut.checkFilterData()

        // then
        XCTAssert(dummyInteractor.isCalledProvideTransactions == false)

    }

    func test_givenACard_whenICallCheckFilterDataAndShouldLoadFilters_thenCallProvideTransactionsInteractorWithTheCardIdOfCard() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO
        sut.shouldLoadFilters = true

        let dummyInteractor = TransactionListDummyInteractor()
        sut.interactor = dummyInteractor

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.checkFilterData()

        // then
        XCTAssert(dummyInteractor.cardId == cardBO.cardId)

    }

    func test_givenACardWithCardIdAndShouldLoadFiltersTrue_whenICallCheckFilterDataAndInteractorReturnSuccess_thenTransactionsBOFilled() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO
        sut.shouldLoadFilters = true

        let dummyInteractor = TransactionListDummyInteractor()

        sut.interactor = dummyInteractor

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.checkFilterData()

        // then
        XCTAssert(sut.transactionsBO != nil)
    }

    func test_givenACardWithCardId_whenICallCheckFilterDataAndInteratorReturnError_thenTransactionsBOShouldBeNil() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.forceError = true

        let dummyTransactionsEntity = TransactionsEntity()
        dummyTransactionsEntity.status = 200

        sut.interactor = dummyInteractor

        sut.transactionsBO = TransactionsBO(transactionsEntity: dummyTransactionsEntity)

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.checkFilterData()

        // then
        XCTAssert(sut.transactionsBO == nil)
    }

    func test_givenACardWithCardId_whenICallCheckFilterDataAndInteratorReturnError_thenICallViewShowErrorContent() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        let dummyView = TransactionsListDummyView()
        sut.view = dummyView

        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.forceError = true

        let dummyTransactionsEntity = TransactionsEntity()
        dummyTransactionsEntity.status = 200

        sut.interactor = dummyInteractor

        sut.transactionsBO = TransactionsBO(transactionsEntity: dummyTransactionsEntity)

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.checkFilterData()

        // then
        XCTAssert(dummyView.isCalledShowErrorContent == true)
    }

    func test_givenACardWithCardId_whenICallCheckFilterDataAndInteratorReturnError_thenErrorBOShouldBeFilled() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        let dummyView = TransactionsListDummyView()
        sut.view = dummyView

        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.forceError = true

        let dummyTransactionsEntity = TransactionsEntity()
        dummyTransactionsEntity.status = 200

        sut.interactor = dummyInteractor

        sut.transactionsBO = TransactionsBO(transactionsEntity: dummyTransactionsEntity)

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.checkFilterData()

        // then
        XCTAssert(sut.errorBO != nil)
    }

    func test_givenACardBOAndAPresenterWithTransactionFilterTransportAndShouldLoadFiltersTrue_whenICallCheckFilterData_thenCallShowFilterScrollView() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO
        sut.shouldLoadFilters = true

        let dummyInteractor = TransactionListDummyInteractor()
        sut.interactor = dummyInteractor
        let dummyView = TransactionsListDummyView()
        sut.view = dummyView
        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.checkFilterData()

        // then
        XCTAssert(dummyView.isCalledShowFiltersScrollView == true)
        XCTAssert(dummyView.isCalledHideFiltersScrollView == false)

    }

    func test_givenACardBOAndAPresenterWithoutTransactionFilterTransportAndShouldLoadFiltersTrue_whenICallCheckFilterData_thenCallShowFilterScrollView() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO
        sut.shouldLoadFilters = true

        let dummyInteractor = TransactionListDummyInteractor()
        sut.interactor = dummyInteractor
        let dummyView = TransactionsListDummyView()
        sut.view = dummyView
        let transactionFilterTransport = TransactionFilterTransport()
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.checkFilterData()

        // then
        XCTAssert(dummyView.isCalledShowFiltersScrollView == false)
        XCTAssert(dummyView.isCalledHideFiltersScrollView == true)

    }

    func test_givenACardBOAndAPresenterWithTransactionFilterTransportAndShouldLoadFiltersFalse_whenICallCheckFilterData_thenCallDoNotShowFilterScrollView() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO
        sut.shouldLoadFilters = false

        let dummyInteractor = TransactionListDummyInteractor()
        sut.interactor = dummyInteractor
        let dummyView = TransactionsListDummyView()
        sut.view = dummyView
        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.checkFilterData()

        // then
        XCTAssert(dummyView.isCalledShowFiltersScrollView == false)
        XCTAssert(dummyView.isCalledHideFiltersScrollView == false)

    }

    func test_givenACardBOAndAPresenterWithoutTransactionFilterTransportAndShouldLoadFiltersFalse_whenICallCheckFilterData_thenCallDoNotShowFilterScrollView() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO
        sut.shouldLoadFilters = false

        let dummyInteractor = TransactionListDummyInteractor()
        sut.interactor = dummyInteractor
        let dummyView = TransactionsListDummyView()
        sut.view = dummyView
        let transactionFilterTransport = TransactionFilterTransport()
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.checkFilterData()

        // then
        XCTAssert(dummyView.isCalledShowFiltersScrollView == false)
        XCTAssert(dummyView.isCalledHideFiltersScrollView == false)

    }

    func test_givenAPresenterWithTransactionFilterTransportWithConcept_whenICallGenerateFiltersArray_thenFiltersArrayContainsConcept() {

        // given
        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        let filters = sut.generateFiltersArray(withTransactionFilterTransport: transactionFilterTransport)

        // then
        XCTAssert(!filters.isEmpty)
        XCTAssert(filters[0].title == "Concepto")

    }

    func test_givenAPresenterWithTransactionFilterTransportWithoutConcept_whenICallGenerateFiltersArray_thenFiltersListNotContainsConcept() {

        // given
        let transactionFilterTransport = TransactionFilterTransport()
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        let filters = sut.generateFiltersArray(withTransactionFilterTransport: transactionFilterTransport)

        // then
        XCTAssert(filters.isEmpty)

    }

    func test_givenAPresenterWithTransactionFilterTransportWithFromAndToDateText_whenICallGenerateFiltersArray_thenFiltersArrayContainsDateText() {

        // given
        let transactionFilterTransport = TransactionFilterTransport()
        var date = Date.date(fromLocalTimeZoneString: "2017-05-18", withFormat: Date.DATE_FORMAT_SERVER)
        transactionFilterTransport.fromDateText = date?.string(format: Date.DATE_FORMAT_SERVER)
        date = Date.date(fromLocalTimeZoneString: "2017-06-01", withFormat: Date.DATE_FORMAT_SERVER)
        transactionFilterTransport.toDateText = date?.string(format: Date.DATE_FORMAT_SERVER)
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        let filters = sut.generateFiltersArray(withTransactionFilterTransport: transactionFilterTransport)

        let format = Date.DATE_FORMAT_SERVER
        date = Date.date(fromLocalTimeZoneString: transactionFilterTransport.fromDateText!, withFormat: format)
        let fromDateText = date?.string(format: Date.DATE_FORMAT_DAY_MONTH_YEAR_SHORT).capitalized
        date = Date.date(fromLocalTimeZoneString: transactionFilterTransport.toDateText!, withFormat: format)
        let toDateText = date?.string(format: Date.DATE_FORMAT_DAY_MONTH_YEAR_SHORT).capitalized

        let dateString = fromDateText! + " - " + toDateText!

        // then
        XCTAssert(!filters.isEmpty)
        XCTAssert(filters[0].title == dateString)

    }

    func test_givenAPresenterWithTransactionFilterTransportWithToDateText_whenICallGenerateFiltersArray_thenFiltersArrayContainsDateText() {

        // given
        let transactionFilterTransport = TransactionFilterTransport()
        var date = Date.date(fromLocalTimeZoneString: "2017-06-01", withFormat: Date.DATE_FORMAT_SERVER)
        transactionFilterTransport.toDateText = date?.string(format: Date.DATE_FORMAT_SERVER)
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        let filters = sut.generateFiltersArray(withTransactionFilterTransport: transactionFilterTransport)

        let format = Date.DATE_FORMAT_SERVER
        date = Date.date(fromLocalTimeZoneString: transactionFilterTransport.toDateText!, withFormat: format)
        let toDateText = date?.string(format: Date.DATE_FORMAT_DAY_MONTH_YEAR_SHORT).capitalized

        let dateString = Localizables.transactions.key_transactions_to_text + " " + toDateText!

        // then
        XCTAssert(!filters.isEmpty)
        XCTAssert(filters[0].title == dateString)

    }

    func test_givenAPresenterWithTransactionFilterTransportWithToDateToToday_whenICallGenerateFiltersArray_thenFiltersArrayContainsDateTextToday() {

        // given
        let transactionFilterTransport = TransactionFilterTransport()

        let format = Date.DATE_FORMAT_SERVER

        let yesterday = Calendar.current.date(byAdding: .day, value: -1, to: Date())

        let dateYesterday = Date.string(fromDate: yesterday!, withFormat: format)

        let date = Date.date(fromLocalTimeZoneString: dateYesterday, withFormat: Date.DATE_FORMAT_SERVER)
        transactionFilterTransport.toDateText = date?.string(format: Date.DATE_FORMAT_SERVER)
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        let filters = sut.generateFiltersArray(withTransactionFilterTransport: transactionFilterTransport)

        let dateString = Localizables.transactions.key_transactions_to_text + " " + Localizables.transactions.key_card_movement_yesterday_text.lowercased()

        // then
        XCTAssert(!filters.isEmpty)
        XCTAssert(filters[0].title == dateString)

    }

    func test_givenAPresenterWithTransactionFilterTransportWithFromTodayAndToDateTextToday_whenICallGenerateFiltersArray_thenFiltersArrayContainsDateText() {

        // given
        let format = Date.DATE_FORMAT_SERVER

        let dateToday = Date.string(fromDate: Date(), withFormat: format)

        let transactionFilterTransport = TransactionFilterTransport()
        var date = Date.date(fromLocalTimeZoneString: dateToday, withFormat: Date.DATE_FORMAT_SERVER)
        transactionFilterTransport.fromDateText = date?.string(format: Date.DATE_FORMAT_SERVER)
        date = Date.date(fromLocalTimeZoneString: dateToday, withFormat: Date.DATE_FORMAT_SERVER)
        transactionFilterTransport.toDateText = date?.string(format: Date.DATE_FORMAT_SERVER)
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        let filters = sut.generateFiltersArray(withTransactionFilterTransport: transactionFilterTransport)

        let dateString = Localizables.transactions.key_card_movement_today_text + " - " + Localizables.transactions.key_card_movement_today_text

        // then
        XCTAssert(!filters.isEmpty)
        XCTAssert(filters[0].title == dateString)

    }

    func test_givenAPresenterWithTransactionFilterTransportWithFromYesterdayAndToDateTextToday_whenICallGenerateFiltersArray_thenFiltersArrayContainsDateText() {

        // given
        let format = Date.DATE_FORMAT_SERVER

        let yesterday = Calendar.current.date(byAdding: .day, value: -1, to: Date())

        let dateYesterday = Date.string(fromDate: yesterday!, withFormat: format)

        let dateToday = Date.string(fromDate: Date(), withFormat: format)

        let transactionFilterTransport = TransactionFilterTransport()
        var date = Date.date(fromLocalTimeZoneString: dateYesterday, withFormat: Date.DATE_FORMAT_SERVER)
        transactionFilterTransport.fromDateText = date?.string(format: Date.DATE_FORMAT_SERVER)
        date = Date.date(fromLocalTimeZoneString: dateToday, withFormat: Date.DATE_FORMAT_SERVER)
        transactionFilterTransport.toDateText = date?.string(format: Date.DATE_FORMAT_SERVER)
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        let filters = sut.generateFiltersArray(withTransactionFilterTransport: transactionFilterTransport)

        let dateString = Localizables.transactions.key_card_movement_yesterday_text + " - " + Localizables.transactions.key_card_movement_today_text

        // then
        XCTAssert(!filters.isEmpty)
        XCTAssert(filters[0].title == dateString)

    }

    func test_givenAPresenterWithTransactionFilterTransportWithFromYesterdayAndToDateTextYesterday_whenICallGenerateFiltersArray_thenFiltersArrayContainsDateText() {

        // given
        let format = Date.DATE_FORMAT_SERVER

        let yesterday = Calendar.current.date(byAdding: .day, value: -1, to: Date())

        let dateYesterday = Date.string(fromDate: yesterday!, withFormat: format)

        let transactionFilterTransport = TransactionFilterTransport()
        var date = Date.date(fromLocalTimeZoneString: dateYesterday, withFormat: Date.DATE_FORMAT_SERVER)
        transactionFilterTransport.fromDateText = date?.string(format: Date.DATE_FORMAT_SERVER)
        date = Date.date(fromLocalTimeZoneString: dateYesterday, withFormat: Date.DATE_FORMAT_SERVER)
        transactionFilterTransport.toDateText = date?.string(format: Date.DATE_FORMAT_SERVER)
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        let filters = sut.generateFiltersArray(withTransactionFilterTransport: transactionFilterTransport)

        let dateString = Localizables.transactions.key_card_movement_yesterday_text + " - " + Localizables.transactions.key_card_movement_yesterday_text

        // then
        XCTAssert(!filters.isEmpty)
        XCTAssert(filters[0].title == dateString)

    }

    func test_givenAPresenterWithTransactionFilterTransportWithFromDateAndToDateTextYesterday_whenICallGenerateFiltersArray_thenFiltersArrayContainsDateText() {

        // given
        let format = Date.DATE_FORMAT_SERVER

        let yesterday = Calendar.current.date(byAdding: .day, value: -1, to: Date())

        let dateYesterday = Date.string(fromDate: yesterday!, withFormat: format)

        let transactionFilterTransport = TransactionFilterTransport()
        var date = Date.date(fromLocalTimeZoneString: "2017-05-18", withFormat: Date.DATE_FORMAT_SERVER)
        transactionFilterTransport.fromDateText = date?.string(format: Date.DATE_FORMAT_SERVER)

        date = Date.date(fromLocalTimeZoneString: dateYesterday, withFormat: Date.DATE_FORMAT_SERVER)
        transactionFilterTransport.toDateText = date?.string(format: Date.DATE_FORMAT_SERVER)
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        let filters = sut.generateFiltersArray(withTransactionFilterTransport: transactionFilterTransport)

        date = Date.date(fromLocalTimeZoneString: transactionFilterTransport.fromDateText!, withFormat: format)
        let fromDateText = date?.string(format: Date.DATE_FORMAT_DAY_MONTH_YEAR_SHORT).capitalized

        let dateString = fromDateText! + " - " + Localizables.transactions.key_card_movement_yesterday_text

        // then
        XCTAssert(!filters.isEmpty)
        XCTAssert(filters[0].title == dateString)

    }

    func test_givenAPresenterWithTransactionFilterTransportWithFromDateAndToDateTextToday_whenICallGenerateFiltersArray_thenFiltersArrayContainsDateText() {

        // given
        let format = Date.DATE_FORMAT_SERVER

        let dateToday = Date.string(fromDate: Date(), withFormat: format)

        let transactionFilterTransport = TransactionFilterTransport()
        var date = Date.date(fromLocalTimeZoneString: "2017-05-18", withFormat: Date.DATE_FORMAT_SERVER)
        transactionFilterTransport.fromDateText = date?.string(format: Date.DATE_FORMAT_SERVER)

        date = Date.date(fromLocalTimeZoneString: dateToday, withFormat: Date.DATE_FORMAT_SERVER)
        transactionFilterTransport.toDateText = date?.string(format: Date.DATE_FORMAT_SERVER)
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        let filters = sut.generateFiltersArray(withTransactionFilterTransport: transactionFilterTransport)

        date = Date.date(fromLocalTimeZoneString: transactionFilterTransport.fromDateText!, withFormat: format)
        let fromDateText = date?.string(format: Date.DATE_FORMAT_DAY_MONTH_YEAR_SHORT).capitalized

        let dateString = fromDateText! + " - " + Localizables.transactions.key_card_movement_today_text

        // then
        XCTAssert(!filters.isEmpty)
        XCTAssert(filters[0].title == dateString)

    }

    func test_givenAPresenterWithTransactionFilterTransportWithoutFromAndToDateText_whenICallGenerateFiltersArray_thenFiltersListNotContainsDate() {

        // given
        let transactionFilterTransport = TransactionFilterTransport()
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        let filters = sut.generateFiltersArray(withTransactionFilterTransport: transactionFilterTransport)

        // then
        XCTAssert(filters.isEmpty)

    }

    func test_givenAPresenterWithTransactionFilterTransportWithFromAndToAmountText_whenICallGenerateFiltersArray_thenFiltersArrayContainsAmountText() {

        // given
        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.fromAmountText = "30"
        transactionFilterTransport.toAmountText = "70"
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        let filtersStrings = sut.generateFiltersArray(withTransactionFilterTransport: transactionFilterTransport)

        // then
        if sut.currencyBO.position == .left {

            XCTAssert(filtersStrings[0].title == (sut.currencyBO.symbol + "30 - " + sut.currencyBO.symbol + "70"))
        } else {

            XCTAssert(filtersStrings[0].title == ("30" + sut.currencyBO.symbol + " - 70" + sut.currencyBO.symbol))
        }

    }

    func test_givenAPresenterWithTransactionFilterTransportWithFromAmountText_whenICallGenerateFiltersArray_thenFiltersArrayContainsFromAmountText() {

        // given
        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.fromAmountText = "30"
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        let filters = sut.generateFiltersArray(withTransactionFilterTransport: transactionFilterTransport)

        // then
        XCTAssert(!filters.isEmpty)
        if sut.currencyBO.position == .left {

            XCTAssert(filters[0].title == (Localizables.transactions.key_transactions_from_text + " " + sut.currencyBO.symbol + "30"))
        } else {

            XCTAssert(filters[0].title == (Localizables.transactions.key_transactions_from_text + " 30" + sut.currencyBO.symbol))
        }

    }

    func test_givenAPresenterWithTransactionFilterTransportWithToAmountText_whenICallGenerateFiltersArray_thenFiltersArrayContainsToAmountText() {

        // given
        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.toAmountText = "70"
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        let filters = sut.generateFiltersArray(withTransactionFilterTransport: transactionFilterTransport)

        // then
        XCTAssert(!filters.isEmpty)
        if sut.currencyBO.position == .left {

            XCTAssert(filters[0].title == (Localizables.transactions.key_transactions_to_text + " " + sut.currencyBO.symbol + "70"))
        } else {

            XCTAssert(filters[0].title == (Localizables.transactions.key_transactions_to_text + " 70" + sut.currencyBO.symbol))
        }

    }

    func test_givenAPresenterWithTransactionFilterTransportWithoutFromAndToAmountText_whenICallGenerateFiltersArray_thenFiltersListNotContainsAmount() {

        // given
        let transactionFilterTransport = TransactionFilterTransport()
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        let filters = sut.generateFiltersArray(withTransactionFilterTransport: transactionFilterTransport)

        // then
        XCTAssert(filters.isEmpty)

    }

    func test_givenAPresenterWithTransactionFilterTransportWithFilterTypeIncome_whenICallGenerateFiltersArray_thenFiltersArrayContainsFilterTypeIncomeText() {

        // given
        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.filtertype = TransactionFilterType.income
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        let filters = sut.generateFiltersArray(withTransactionFilterTransport: transactionFilterTransport)

        // then
        XCTAssert(!filters.isEmpty)
        XCTAssert(filters[0].title == Localizables.transactions.key_transactions_income_text)

    }

    func test_givenAPresenterWithTransactionFilterTransportWithFilterTypeExpenses_whenICallGenerateFiltersArray_thenFiltersArrayContainsFilterTypeExpensesText() {

        // given
        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.filtertype = TransactionFilterType.expense
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        let filters = sut.generateFiltersArray(withTransactionFilterTransport: transactionFilterTransport)

        // then
        XCTAssert(!filters.isEmpty)
        XCTAssert(filters[0].title == Localizables.transactions.key_transactions_expenses_text)

    }

    func test_givenAPresenterWithTransactionFilterTransportWithFilterTypeAll_whenICallGenerateFiltersArray_thenFiltersArrayNotContainsFilterTypeType() {

        // given
        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.filtertype = TransactionFilterType.all
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        let filters = sut.generateFiltersArray(withTransactionFilterTransport: transactionFilterTransport)

        // then
        XCTAssert(filters.isEmpty)

    }

    func test_givenAPresenterWithoutTransactionFilterTransport_whenICallRemoveFilter_thenMethodReturnsFalse() {

        // given
        let filter = FilterScrollDisplayData(title: "Concepto",
                                             filterType: "concept",
                                             isRemoveAllFiltersButton: false,
                                             labelAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_CONCEPT_FILTER_DETAIL_LABEL_ID,
                                             buttonAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_CONCEPT_FILTER_DETAIL_CLOSE_BUTTON_ID)
        sut.transactionFilterTransport = nil

        // when
        let response = sut.removeFilterInTransactionFilterTransport(forFilter: filter)

        // then
        XCTAssert(response == false)

    }

    func test_givenAPresenterWithFiltersAndUnknownFilterType_whenICallRemoveFilter_thenMethodReturnsFalse() {

        // given
        let filter = FilterScrollDisplayData(title: "Desconocido",
                                             filterType: "unknown",
                                             isRemoveAllFiltersButton: false,
                                             labelAccessibilityId: "",
                                             buttonAccessibilityId: "")
        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = filter.title
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        let response = sut.removeFilterInTransactionFilterTransport(forFilter: filter)

        // then
        XCTAssert(response == false)

    }

    func test_givenAPresenterWithFilterTypeConcept_whenICallRemoveFilterTypeContent_thenFilterIsRemoved() {

        // given
        let filter = FilterScrollDisplayData(title: "Concepto",
                                             filterType: "concept",
                                             isRemoveAllFiltersButton: false,
                                             labelAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_CONCEPT_FILTER_DETAIL_LABEL_ID,
                                             buttonAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_CONCEPT_FILTER_DETAIL_CLOSE_BUTTON_ID)
        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = filter.title
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        let response = sut.removeFilterInTransactionFilterTransport(forFilter: filter)

        // then
        XCTAssert(response)
        XCTAssert(transactionFilterTransport.conceptText == nil)

    }

    func test_givenAPresenterWithoutFilterTypeConcept_whenICallRemoveFilterTypeContent_thenMethodReturnsFalse() {

        // given
        let filter = FilterScrollDisplayData(title: "Concepto",
                                             filterType: "concept",
                                             isRemoveAllFiltersButton: false,
                                             labelAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_CONCEPT_FILTER_DETAIL_LABEL_ID,
                                             buttonAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_CONCEPT_FILTER_DETAIL_CLOSE_BUTTON_ID)
        let transactionFilterTransport = TransactionFilterTransport()
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        let response = sut.removeFilterInTransactionFilterTransport(forFilter: filter)

        // then
        XCTAssert(response == false)

    }

    func test_givenAPresenterWithFilterTypeDate_whenICallRemoveFilterTypeDate_thenFilterIsRemoved() {

        // given
        let date = Date.date(fromLocalTimeZoneString: "2017-06-01", withFormat: Date.DATE_FORMAT_SERVER)
        let filter = FilterScrollDisplayData(title: (date?.string(format: Date.DATE_FORMAT_SERVER))!,
                                             filterType: "date",
                                             isRemoveAllFiltersButton: false,
                                             labelAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_DATE_FILTER_DETAIL_LABEL_ID,
                                             buttonAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_DATE_FILTER_DETAIL_CLOSE_BUTTON_ID)
        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.fromDateText = filter.title
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        let response = sut.removeFilterInTransactionFilterTransport(forFilter: filter)

        // then
        XCTAssert(response)
        XCTAssert(transactionFilterTransport.fromDateText == nil && transactionFilterTransport.toDateText == nil)

    }

    func test_givenAPresenterWithoutFilterTypeDate_whenICallRemoveFilterTypeDate_thenMethodReturnsFalse() {

        // given
        let date = Date.date(fromLocalTimeZoneString: "2017-06-01", withFormat: Date.DATE_FORMAT_SERVER)
        let filter = FilterScrollDisplayData(title: (date?.string(format: Date.DATE_FORMAT_SERVER))!,
                                             filterType: "date",
                                             isRemoveAllFiltersButton: false,
                                             labelAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_DATE_FILTER_DETAIL_LABEL_ID,
                                             buttonAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_DATE_FILTER_DETAIL_CLOSE_BUTTON_ID)
        let transactionFilterTransport = TransactionFilterTransport()
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        let response = sut.removeFilterInTransactionFilterTransport(forFilter: filter)

        // then
        XCTAssert(response == false)

    }

    func test_givenAPresenterWithFilterTypeAmount_whenICallRemoveFilterTypeAmount_thenFilterIsRemoved() {

        // given
        let filter = FilterScrollDisplayData(title: "30",
                                             filterType: "amount",
                                             isRemoveAllFiltersButton: false,
                                             labelAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_AMOUNT_FILTER_DETAIL_LABEL_ID,
                                             buttonAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_AMOUNT_FILTER_DETAIL_CLOSE_BUTTON_ID)
        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.fromAmountText = filter.title
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        let response = sut.removeFilterInTransactionFilterTransport(forFilter: filter)

        // then
        XCTAssert(response)
        XCTAssert(transactionFilterTransport.fromAmountText == nil && transactionFilterTransport.toAmountText == nil)

    }

    func test_givenAPresenterWithoutFilterTypeAmount_whenICallRemoveFilterTypeAmount_thenMethodReturnsFalse() {

        // given
        let filter = FilterScrollDisplayData(title: "30",
                                             filterType: "amount",
                                             isRemoveAllFiltersButton: false,
                                             labelAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_AMOUNT_FILTER_DETAIL_LABEL_ID,
                                             buttonAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_AMOUNT_FILTER_DETAIL_CLOSE_BUTTON_ID)
        let transactionFilterTransport = TransactionFilterTransport()
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        let response = sut.removeFilterInTransactionFilterTransport(forFilter: filter)

        // then
        XCTAssert(response == false)

    }

    func test_givenAPresenterWithFilterTypeType_whenICallRemoveFilterTypeType_thenFilterIsRemoved() {

        // given
        let filter = FilterScrollDisplayData(title: Localizables.transactions.key_transactions_income_text,
                                             filterType: "type",
                                             isRemoveAllFiltersButton: false,
                                             labelAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_TRANSACTION_TYPE_FILTER_DETAIL_LABEL_ID,
                                             buttonAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_TRANSACTION_TYPE_FILTER_DETAIL_CLOSE_BUTTON_ID)
        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.filtertype = TransactionFilterType.income
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        let response = sut.removeFilterInTransactionFilterTransport(forFilter: filter)

        // then
        XCTAssert(response)
        XCTAssert(transactionFilterTransport.filtertype == TransactionFilterType.all)

    }

    func test_givenAPresenterWithoutFilterTypeType_whenICallRemoveFilterTypeType_thenMethodReturnsFalse() {

        // given
        let filter = FilterScrollDisplayData(title: Localizables.transactions.key_transactions_income_text,
                                             filterType: "type",
                                             isRemoveAllFiltersButton: false,
                                             labelAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_TRANSACTION_TYPE_FILTER_DETAIL_LABEL_ID,
                                             buttonAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_TRANSACTION_TYPE_FILTER_DETAIL_CLOSE_BUTTON_ID)
        let transactionFilterTransport = TransactionFilterTransport()
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        let response = sut.removeFilterInTransactionFilterTransport(forFilter: filter)

        // then
        XCTAssert(response == false)

    }

    func test_givenAPresenter_whenICallGenerateFilterRemoveAll_thenFiltersArrayContainsFilterTypeRemoveAll() {

        // when
        let filters = sut.generateFilterRemoveAll()

        // then
        XCTAssert(!filters.isEmpty)
        XCTAssert(filters[0].filterType == "removeAll")

    }

    func test_givenACardBOAndAPresenterWithTransactionFilterTransport_whenICallcleanTransactionFilterTransportValues_thenTransactionFilterTransportIsUnitialized() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.cleanTransactionFilterTransportValues()

        // then
        XCTAssert((sut.transactionFilterTransport?.isUnitialized())!)

    }

    func test_givenCardBOAndAPresenter_whenICallRemoveFilter_thenCallProvideTransactions() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        let filter = FilterScrollDisplayData(title: "Concepto",
                                             filterType: "concept",
                                             isRemoveAllFiltersButton: false,
                                             labelAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_CONCEPT_FILTER_DETAIL_LABEL_ID,
                                             buttonAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_CONCEPT_FILTER_DETAIL_CLOSE_BUTTON_ID)
        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = filter.title
        sut.transactionFilterTransport = transactionFilterTransport

        let dummyInteractor = TransactionListDummyInteractor()
        sut.interactor = dummyInteractor

        // when
        sut.removeFilter(filter: filter, inFiltersScrollView: FiltersScrollView())

        // then
        XCTAssert(dummyInteractor.isCalledProvideTransactions == true)

    }

    func test_givenACardBOAndAPresenter_whenICallRemoveFilterAndThereAreMoreFilters_thenCallShowFilterScrollView() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        let dummyInteractor = TransactionListDummyInteractor()
        sut.interactor = dummyInteractor
        let dummyView = TransactionsListDummyView()
        sut.view = dummyView
        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        var date = Date.date(fromLocalTimeZoneString: "2017-05-01", withFormat: Date.DATE_FORMAT_SERVER)
        transactionFilterTransport.fromDateText = date?.string(format: Date.DATE_FORMAT_SERVER)
        date = Date.date(fromLocalTimeZoneString: "2017-06-01", withFormat: Date.DATE_FORMAT_SERVER)
        transactionFilterTransport.toDateText = date?.string(format: Date.DATE_FORMAT_SERVER)
        let filter = FilterScrollDisplayData(title: "Concepto",
                                             filterType: "concept",
                                             isRemoveAllFiltersButton: false,
                                             labelAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_CONCEPT_FILTER_DETAIL_LABEL_ID,
                                             buttonAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_CONCEPT_FILTER_DETAIL_CLOSE_BUTTON_ID)
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.removeFilter(filter: filter, inFiltersScrollView: FiltersScrollView())

        // then
        XCTAssert(dummyView.isCalledShowFiltersScrollView == true)
        XCTAssert(dummyView.isCalledHideFiltersScrollView == false)
    }

    func test_givenACardBOAndAPresenter_whenICallRemoveFilterAndThereArentMoreFilters_thenCallHideFilterScrollView() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        let dummyInteractor = TransactionListDummyInteractor()
        sut.interactor = dummyInteractor
        let dummyView = TransactionsListDummyView()
        sut.view = dummyView
        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        let filter = FilterScrollDisplayData(title: "Concepto",
                                             filterType: "concept",
                                             isRemoveAllFiltersButton: false,
                                             labelAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_CONCEPT_FILTER_DETAIL_LABEL_ID,
                                             buttonAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_CONCEPT_FILTER_DETAIL_CLOSE_BUTTON_ID)
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.removeFilter(filter: filter, inFiltersScrollView: FiltersScrollView())

        // then
        XCTAssert(dummyView.isCalledShowFiltersScrollView == false)
        XCTAssert(dummyView.isCalledHideFiltersScrollView == true)
    }

    func test_givenACardBOAndAPresenter_whenICallRemoveFilterLonglyPushed_thenCallShowFiltersScrollViewWithFilterTypeRemoveAllAndTransactionFilterTransportStillHasFiltersApplied() {

        //given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        let dummyInteractor = TransactionListDummyInteractor()
        sut.interactor = dummyInteractor
        let dummyView = TransactionsListDummyView()
        sut.view = dummyView
        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        var date = Date.date(fromLocalTimeZoneString: "2017-05-01", withFormat: Date.DATE_FORMAT_SERVER)
        transactionFilterTransport.fromDateText = date?.string(format: Date.DATE_FORMAT_SERVER)
        let fromDateText = date!.string(format: Date.DATE_FORMAT_DAY_MONTH_YEAR_SHORT).capitalized
        date = Date.date(fromLocalTimeZoneString: "2017-12-01", withFormat: Date.DATE_FORMAT_SERVER)
        transactionFilterTransport.toDateText = date?.string(format: Date.DATE_FORMAT_SERVER)
        let toDateText = date!.string(format: Date.DATE_FORMAT_DAY_MONTH_YEAR_SHORT).capitalized
        let filter1 = FilterScrollDisplayData(title: "Concepto",
                                              filterType: "concept",
                                              isRemoveAllFiltersButton: false,
                                              labelAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_CONCEPT_FILTER_DETAIL_LABEL_ID,
                                              buttonAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_CONCEPT_FILTER_DETAIL_CLOSE_BUTTON_ID)
        let filter2 = FilterScrollDisplayData(title: fromDateText + " - " + toDateText,
                                              filterType: "date",
                                              isRemoveAllFiltersButton: false,
                                              labelAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_DATE_FILTER_DETAIL_LABEL_ID,
                                              buttonAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_DATE_FILTER_DETAIL_CLOSE_BUTTON_ID)
        let filterRemoveAll = FilterScrollDisplayData(title: Localizables.transactions.key_transactions_deleteall_text,
                                                      filterType: "removeAll",
                                                      isRemoveAllFiltersButton: true,
                                                      labelAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_REMOVE_ALL_FILTERS_DETAIL_LABEL_ID,
                                                      buttonAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_REMOVE_ALL_FILTERS_DETAIL_CLOSE_BUTTON_ID)
        let filters = [filter1, filter2]
        dummyView.filters = filters
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.removeFilterLonglyPushed(inFiltersScrollView: FiltersScrollView())

        // then
        XCTAssert(dummyView.isCalledShowFiltersScrollView == true)
        XCTAssert(dummyView.isCalledHideFiltersScrollView == false)
        XCTAssert(dummyView.filters! == [filterRemoveAll])
        XCTAssert(sut.transactionFilterTransport == transactionFilterTransport)
        XCTAssert(dummyInteractor.isCalledProvideTransactions == false)
    }

    func test_givenACardBOAndAPresenter_whenICallRemoveAllFilters_thenCallProvideTransactions() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        let filter = FilterScrollDisplayData(title: "Concepto",
                                             filterType: "concept",
                                             isRemoveAllFiltersButton: false,
                                             labelAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_REMOVE_ALL_FILTERS_DETAIL_LABEL_ID,
                                             buttonAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_REMOVE_ALL_FILTERS_DETAIL_CLOSE_BUTTON_ID)
        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = filter.title
        sut.transactionFilterTransport = transactionFilterTransport

        let dummyInteractor = TransactionListDummyInteractor()
        sut.interactor = dummyInteractor

        // when
        sut.removeAllFilters(inFiltersScrollView: FiltersScrollView())

        // then
        XCTAssert(dummyInteractor.isCalledProvideTransactions == true)
    }

    func test_givenACardBOAndAPresenter_whenICallRemoveAllFilters_thenCallHideFilterScrollView() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        let dummyInteractor = TransactionListDummyInteractor()
        sut.interactor = dummyInteractor
        let dummyView = TransactionsListDummyView()
        sut.view = dummyView
        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.removeAllFilters(inFiltersScrollView: FiltersScrollView())

        // then
        XCTAssert(dummyView.isCalledShowFiltersScrollView == false)
        XCTAssert(dummyView.isCalledHideFiltersScrollView == true)
    }

    // MARK: - onMessage

    func test_givenAFilterPropertyAndTransactionFilterTransport_whenICallOnMessage_thenTransactionsFilterTransportShouldBeFilled() {

        // given

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"

        // when
        sut.setModel(model: transactionFilterTransport)

        // then
        XCTAssert(sut.transactionFilterTransport != nil)

    }

    func test_givenAFilterPropertyAndTransactionFilterTransport_whenICallOnMessage_thenTransactionsFilterTransportShouldMatchWithParam() {

        // given

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"

        // when
        sut.setModel(model: transactionFilterTransport)

        // then
        XCTAssert(sut.transactionFilterTransport == transactionFilterTransport)

    }

    func test_givenAFilterPropertyAndTransactionFilterTransportAsParamAndTransctionFilterSetBeforeAreTheSame_whenICallOnMessage_thenShouldLoadFiltersShouldBeFalse() {

        // given

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.setModel(model: transactionFilterTransport)

        // then
        XCTAssert(sut.shouldLoadFilters == false)

    }

    func test_givenAFilterPropertyAndTransactionFilterTransportAsParamAndTransctionFilterSetBeforeAreTheDifferents_whenICallOnMessage_thenShouldLoadFiltersShouldBeTrue() {

        // given

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        sut.transactionFilterTransport = transactionFilterTransport

        let transactionFilterTransportNew = TransactionFilterTransport()
        transactionFilterTransportNew.conceptText = "Concepto diferente"

        // when
        sut.setModel(model: transactionFilterTransportNew)

        // then
        XCTAssert(sut.shouldLoadFilters == true)

    }

    func test_givenAFilterTransportWithCardBO_whenICallSetModel_thenCardBOShouldMatch() {

        // given
        let transactionFilterTransportNew = TransactionFilterTransport()
        transactionFilterTransportNew.cardBO = CardsGenerator.getCardBOCreditCard().cards[0]

        // when
        sut.setModel(model: transactionFilterTransportNew)

        // then
        XCTAssert(sut.cardBO === transactionFilterTransportNew.cardBO)

    }

    func test_givenAPresenter_whenICallGoToTransactionDetail_thenICallRouterPresentTransactionsDetail() {

        // given
        let transactionEntity = TransactionEntityDummyGenerator.transactionEntity()
        let transactionBO = TransactionBO(transactionEntity: transactionEntity)!

        let transactionDisplayItem = TransactionDisplayItem(fromTransactionBO: transactionBO)

        // when
        sut.goToTransactionDetail(withTransactionDisplayItem: transactionDisplayItem)

        // then
        XCTAssert(dummyBusManager.isCalledNavigateNextTransactionDetailScreen == true)

    }

    // MARK: - retryButtonPressed

    func test_givenAny_whenICallRetryButtonPressed_thenICallShowSkeleton() {

        // given
        let dummyView = TransactionsListDummyView()
        sut.view = dummyView

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(dummyView.isCalledShowSkeleton == true)

    }

    func test_givenCardBONil_whenICallRetryButtonPressed_thenINotCallInteractorProvideTransactions() {

        // given
        sut.cardBO = nil

        let dummyInteractor = TransactionListDummyInteractor()
        sut.interactor = dummyInteractor

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(dummyInteractor.isCalledProvideTransactions == false)

    }

    func test_givenCardBO_whenICallRetryButtonPressed_thenICallInteractorProvideTransactions() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        let dummyInteractor = TransactionListDummyInteractor()
        sut.interactor = dummyInteractor

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(dummyInteractor.isCalledProvideTransactions == true)

    }

    func test_givenCardBO_whenICallRetryButtonPressed_thenCallProvideTransactionsInteractorWithTheCardIdOfCard() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        let dummyInteractor = TransactionListDummyInteractor()
        sut.interactor = dummyInteractor

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(dummyInteractor.cardId == cardBO.cardId)

    }

    func test_givenCardBO_whenICallRetryButtonPressed_thenTransactionsBOFilled() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        let dummyInteractor = TransactionListDummyInteractor()
        sut.interactor = dummyInteractor

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(sut.transactionsBO != nil)

    }

    func test_givenCardBOAndNoFilters_whenICallRetryButtonPressedAndInteractorReturnSuccess204_thenICallViewShowNoContent() {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 204
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        let dummyView = TransactionsListDummyView()
        sut.view = dummyView

        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.transactionsBO = transactionsBO
        sut.interactor = dummyInteractor

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(dummyView.isCalledShowNoContent == true)
    }

    func test_givenCardBOAndSomeFilters_whenICallRetryButtonPressedAndInteractorReturnSuccess204_thenICallViewShowNoResults() {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 204
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        let dummyView = TransactionsListDummyView()
        sut.view = dummyView

        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.transactionsBO = transactionsBO
        sut.interactor = dummyInteractor

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "concept"
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(dummyView.isCalledShowNoResults == true)
    }

    func test_givenCardBO_whenICallRetryButtonPressedAndInteractorReturnSuccess200_thenICallViewHideSkeleton() {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 200
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        let dummyView = TransactionsListDummyView()
        sut.view = dummyView

        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.transactionsBO = transactionsBO
        sut.interactor = dummyInteractor

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(dummyView.isCalledHideSkeleton == true)
    }

    func test_givenCardBO_whenICallRetryButtonPressedAndInteractorReturnSuccess200_thenICallViewUpdateForNoMoreContent() {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 200
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        let dummyView = TransactionsListDummyView()
        sut.view = dummyView

        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.transactionsBO = transactionsBO
        sut.interactor = dummyInteractor

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == true)
    }

    func test_givenCardBO_whenICallRetryButtonPressedAndInteractorReturnSuccess200WithTransactions_thenICallViewShowTransactions() {

        // given
        let transactionsBO = TransactionEntityDummyGenerator.transactionsBOWithTransactions()

        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        let dummyView = TransactionsListDummyView()
        sut.view = dummyView

        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.transactionsBO = transactionsBO
        sut.interactor = dummyInteractor

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(dummyView.isCalledShowTransactions == true)
    }

    func test_givenCardBO_whenICallRetryButtonPressedAndInteractorReturnSuccess200WithTransactions_thenICallViewShowTransactionsWithDisplayData() {

        // given
        let transactionsBO = TransactionEntityDummyGenerator.transactionsBOWithTransactions()

        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        let dummyView = TransactionsListDummyView()
        sut.view = dummyView

        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.transactionsBO = transactionsBO
        sut.interactor = dummyInteractor

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(dummyView.transactionsDisplayData != nil)
    }

    func test_givenCardBO_whenICallRetryButtonPressedAndInteratorReturnError_thenTransactionsBOShouldBeNil() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.forceError = true
        sut.interactor = dummyInteractor

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(sut.transactionsBO == nil)
    }

    func test_givenCardBO_whenICallRetryButtonPressedAndInteratorReturnError_thenICallViewShowErrorContent() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        let dummyView = TransactionsListDummyView()
        sut.view = dummyView

        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.forceError = true
        sut.interactor = dummyInteractor

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(dummyView.isCalledShowErrorContent == true)
    }

    func test_givenCardBO_whenICallRetryButtonPressedAndInteratorReturnError_thenErrorBOShouldBeFilled() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.forceError = true
        sut.interactor = dummyInteractor

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(sut.errorBO != nil)
    }

    func test_givenCardBOAndTransctionFilterTransport_whenICallRetryButtonPressed_thenTransactionFilterTransportMatchToInteractor() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        sut.transactionFilterTransport = transactionFilterTransport

        let dummyInteractor = TransactionListDummyInteractor()
        sut.interactor = dummyInteractor

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(dummyInteractor.transactionFilterSent == transactionFilterTransport)

    }
    
    func test_givenCardBO_whenICallRetryButtonPressedAndInteractorReturnSuccess200WithTransactions_thenINotCallShowNoContent() {
        
        // given
        let transactionsBO = TransactionEntityDummyGenerator.transactionsBOWithTransactions()
        
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO
        
        let dummyView = TransactionsListDummyView()
        sut.view = dummyView
        
        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.transactionsBO = transactionsBO
        sut.interactor = dummyInteractor
        
        // when
        sut.retryButtonPressed()
        
        // then
        XCTAssert(dummyView.isCalledShowNoContent == false)
    }
    
    func test_givenCardBO_whenICallRetryButtonPressedAndInteractorReturnSuccess200WithoutTransactions_thenICallShowNoContent() {
        
        // given
        let transactionsBO = TransactionEntityDummyGenerator.transactionsBOWithoutTransactions()
        
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO
        
        let dummyView = TransactionsListDummyView()
        sut.view = dummyView
        
        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.transactionsBO = transactionsBO
        sut.interactor = dummyInteractor
        
        // when
        sut.retryButtonPressed()
        
        // then
        XCTAssert(dummyView.isCalledShowNoContent == true)
    }

    // MARK: - reloadTransactionsList

    func test_givenAFilterPropertyAndTransactionFilterTransport_whenICallReloadTransactionsList_thenTransactionsFilterTransportShouldBeFilled() {

        // given

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"

        // when
        sut.reloadTransactionsList(model: transactionFilterTransport)

        // then
        XCTAssert(sut.transactionFilterTransport != nil)

    }

    func test_givenAFilterPropertyAndTransactionFilterTransport_whenICallReloadTransactionsList_thenTransactionsFilterTransportShouldMatchWithParam() {

        // given

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"

        // when
        sut.reloadTransactionsList(model: transactionFilterTransport)

        // then
        XCTAssert(sut.transactionFilterTransport == transactionFilterTransport)

    }

    func test_givenAFilterPropertyAndTransactionFilterTransportAsParamAndTransctionFilterSetBeforeAreTheSame_whenICallReloadTransactionsList_thenShouldLoadFiltersShouldBeFalse() {

        // given

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.reloadTransactionsList(model: transactionFilterTransport)

        // then
        XCTAssert(sut.shouldLoadFilters == false)

    }

    func test_givenAFilterPropertyAndTransactionFilterTransportAsParamAndTransctionFilterSetBeforeAreTheDifferents_whenICallReloadTransactionsList_thenShouldLoadFiltersShouldBeTrue() {

        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.presenter = sut
        let dummyView = TransactionsListDummyView()

        sut.interactor = dummyInteractor
        sut.view = dummyView
        sut.transactionsBO = nil
        sut.interactor = dummyInteractor

        let dummyCardBO = CardsGenerator.getCardBOWithAlias().cards[0]

        // given
        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        transactionFilterTransport.cardBO = dummyCardBO
        sut.transactionFilterTransport = transactionFilterTransport

        let transactionFilterTransportNew = TransactionFilterTransport()
        transactionFilterTransportNew.cardBO = dummyCardBO
        transactionFilterTransportNew.conceptText = "Concepto diferente"

        // when
        sut.reloadTransactionsList(model: transactionFilterTransportNew)

        // then
        XCTAssert(dummyInteractor.wasShouldLoadFilters == true)

    }

    // MARK: reloadTransactionsList (loadData)

    func test_givenTransactionsBONilAndViewDidLoadFalse_whenICallReloadTransactionsList_thenICallViewShowSkeleton() {

        // given
        let dummyInteractor = TransactionListDummyInteractor()
        let dummyView = TransactionsListDummyView()

        sut.interactor = dummyInteractor
        sut.view = dummyView
        sut.transactionsBO = nil

        let dummyCardBO = CardsGenerator.getCardBOWithAlias().cards[0]

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        transactionFilterTransport.cardBO = dummyCardBO
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.reloadTransactionsList(model: transactionFilterTransport)

        // then
        XCTAssert(dummyView.isCalledShowSkeleton == true)

    }

    func test_givenTransactionsBONilAndViewDidLoadTrue_whenICallReloadTransactionsList_thenICallViewShowSkeleton() {

        // given
        let dummyInteractor = TransactionListDummyInteractor()
        let dummyView = TransactionsListDummyView()

        sut.interactor = dummyInteractor
        sut.view = dummyView
        sut.transactionsBO = nil
        sut.viewDidLoad = true

        let dummyCardBO = CardsGenerator.getCardBOWithAlias().cards[0]

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        transactionFilterTransport.cardBO = dummyCardBO
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.reloadTransactionsList(model: transactionFilterTransport)

        // then
        XCTAssert(dummyView.isCalledShowSkeleton == false)

    }

    func test_givenTransactionsBONilAndCardBOAndViewDidLoadFalse_whenICallReloadTransactionsList_thenICallInteractorProvideTransactions() {

        let dummyInteractor = TransactionListDummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let dummyCardBO = CardsGenerator.getCardBOWithAlias().cards[0]

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        transactionFilterTransport.cardBO = dummyCardBO
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.reloadTransactionsList(model: transactionFilterTransport)

        // then
        XCTAssert(dummyInteractor.isCalledProvideTransactions == true)

    }

    func test_givenTransactionsBONilAndCardBOAndViewDidLoadFalse_whenICallReloadTransactionsList_thenCallProvideTransactionsInteractorWithTheCardIdOfCard() {

        let dummyInteractor = TransactionListDummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        transactionFilterTransport.cardBO = cardBO
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.reloadTransactionsList(model: transactionFilterTransport)

        // then
        XCTAssert(dummyInteractor.cardId == cardBO.cardId)

    }

    func test_givenTransactionsBONilAndCardBOAndViewDidLoadFalse_whenICallReloadTransactionsListAndInteractorReturnSuccess_thenTransactionsBOFilled() {

        let dummyInteractor = TransactionListDummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        transactionFilterTransport.cardBO = cardBO
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.reloadTransactionsList(model: transactionFilterTransport)

        // then
        XCTAssert(sut.transactionsBO != nil)
    }

    func test_givenTransactionsBONilAndCardBOAndViewDidLoadFalseAndNoFilters_whenICallReloadTransactionsListAndInteractorReturnSuccess204_thenICallViewShowNoContent() {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 204
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        sut.viewDidLoad = false

        let dummyView = TransactionsListDummyView()
        sut.view = dummyView

        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.transactionsBO = transactionsBO
        sut.interactor = dummyInteractor

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.cardBO = cardBO
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.reloadTransactionsList(model: transactionFilterTransport)

        // then
        XCTAssert(dummyView.isCalledShowNoContent == true)
    }

    func test_givenTransactionsBONilAndCardBOAndViewDidLoadFalseAndSomeFilters_whenICallReloadTransactionsListAndInteractorReturnSuccess204_thenICallViewShowNoResults() {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 204
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        sut.viewDidLoad = false

        let dummyView = TransactionsListDummyView()
        sut.view = dummyView

        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.transactionsBO = transactionsBO
        sut.interactor = dummyInteractor

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "concept"
        transactionFilterTransport.cardBO = cardBO
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.loadData()

        // then
        XCTAssert(dummyView.isCalledShowNoResults == true)
    }

    func test_givenTransactionsBONilAndCardBOAndViewDidLoadFalse_whenICallReloadTransactionsListAndInteractorReturnSuccess200_thenICallViewHideNoContent() {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 200
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        sut.viewDidLoad = false

        let dummyView = TransactionsListDummyView()
        sut.view = dummyView

        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.transactionsBO = transactionsBO

        sut.interactor = dummyInteractor

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        transactionFilterTransport.cardBO = cardBO
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.reloadTransactionsList(model: transactionFilterTransport)

        // then
        XCTAssert(dummyView.isCalledHideSkeleton == true)
    }

    func test_givenTransactionsBONilAndCardBOAndViewDidLoadFalse_whenICallReloadTransactionsListAndInteractorReturnSuccess200_thenICallViewUpdateForNoMoreContent() {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 200
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        sut.viewDidLoad = false

        let dummyView = TransactionsListDummyView()
        sut.view = dummyView

        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.transactionsBO = transactionsBO

        sut.interactor = dummyInteractor

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        transactionFilterTransport.cardBO = cardBO
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.reloadTransactionsList(model: transactionFilterTransport)

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == true)
    }

    func test_givenTransactionsBONilAndCardBOAndViewDidLoadFalse_whenICallReloadTransactionsListAndInteractorReturnSuccess200WithTransactions_thenICallViewShowTransactions() {

        // given
        let transactionsBO = TransactionEntityDummyGenerator.transactionsBOWithTransactions()

        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        let dummyView = TransactionsListDummyView()

        sut.view = dummyView

        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.transactionsBO = transactionsBO

        sut.interactor = dummyInteractor

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        transactionFilterTransport.cardBO = cardBO
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.reloadTransactionsList(model: transactionFilterTransport)

        // then
        XCTAssert(dummyView.isCalledShowTransactions == true)
    }

    func test_givenTransactionsBONilAndCardBOAndViewDidLoadFalse_whenICallReloadTransactionsListAndInteractorReturnSuccess200WithTransactions_thenICallViewShowTransactionsWithDisplayData() {

        // given
        let transactionsBO = TransactionEntityDummyGenerator.transactionsBOWithTransactions()

        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        let dummyView = TransactionsListDummyView()

        sut.view = dummyView

        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.transactionsBO = transactionsBO

        sut.interactor = dummyInteractor

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        transactionFilterTransport.cardBO = cardBO
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.reloadTransactionsList(model: transactionFilterTransport)

        // then
        XCTAssert(dummyView.transactionsDisplayData != nil)
    }

    func test_givenTransactionsBONilAndCardBOAndViewDidLoadFalse_whenICallReloadTransactionsListAndInteratorReturnError_thenTransactionsBOShouldBeNil() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.forceError = true

        sut.interactor = dummyInteractor

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        transactionFilterTransport.cardBO = cardBO
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.reloadTransactionsList(model: transactionFilterTransport)

        // then
        XCTAssert(sut.transactionsBO == nil)
    }

    func test_givenTransactionsBONilAndCardBOAndViewDidLoadFalse_whenICallReloadTransactionsListAndInteratorReturnError_thenICallViewShowErrorContent() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        let dummyView = TransactionsListDummyView()

        sut.view = dummyView

        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.forceError = true

        sut.interactor = dummyInteractor

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        transactionFilterTransport.cardBO = cardBO
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.reloadTransactionsList(model: transactionFilterTransport)

        // then
        XCTAssert(dummyView.isCalledShowErrorContent == true)
    }

    func test_givenTransactionsBONilAndCardBOAndViewDidLoadFalse_whenICallReloadTransactionsListAndInteratorReturnError_thenErrorBOShouldBeFilled() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.forceError = true

        sut.interactor = dummyInteractor

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        transactionFilterTransport.cardBO = cardBO
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.reloadTransactionsList(model: transactionFilterTransport)

        // then
        XCTAssert(sut.errorBO != nil)
    }

    func test_givenTransactionsBOWithStatus204AndViewDidLoadFalseAndNoFilters_whenICallReloadTransactionsList_thenPresenterCallViewShowNoContent() {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 204
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        let dummyView = TransactionsListDummyView()

        sut.view = dummyView
        sut.transactionsBO = transactionsBO

        let dummyCardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = dummyCardBO

        let transactionFilterTransport = TransactionFilterTransport()

        // when
        sut.reloadTransactionsList(model: transactionFilterTransport)

        // then
        XCTAssert(dummyView.isCalledShowNoContent == true)
        XCTAssert(dummyView.isCalledShowTransactions == false)

    }

    func test_givenTransactionsBOWithStatus204AndViewDidLoadFalseAndSomeFilters_whenICallReloadTransactionsList_thenPresenterCallViewShowNoResults() {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 204
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        let dummyView = TransactionsListDummyView()

        sut.view = dummyView
        sut.transactionsBO = transactionsBO

        let dummyCardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = dummyCardBO

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        transactionFilterTransport.cardBO = dummyCardBO
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.reloadTransactionsList(model: transactionFilterTransport)

        // then
        XCTAssert(dummyView.isCalledShowNoResults == true)
        XCTAssert(dummyView.isCalledShowTransactions == false)

    }

    func test_givenTransactionsBOWithStatus200AndWithTransactionsAndViewDidLoadFalse_whenICallReloadTransactionsList_thenPresenterCallViewShowTransactions() {

        // given
        let transactionsBO = TransactionEntityDummyGenerator.transactionsBOWithTransactions()

        let dummyView = TransactionsListDummyView()

        sut.view = dummyView
        sut.transactionsBO = transactionsBO

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.reloadTransactionsList(model: transactionFilterTransport)

        // then
        XCTAssert(dummyView.isCalledShowTransactions == true)
        XCTAssert(dummyView.isCalledShowNoContent == false)
        XCTAssert(dummyView.isCalledShowNoResults == false)

    }

    func test_givenTransactionsBOWithStatus208AndViewDidLoadFalse_whenICallReloadTransactionsList_thenNeitherViewShowTransactionsNorViewShowNoContentNorViewShowNoResultsAreCalled() {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 208
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        let dummyView = TransactionsListDummyView()

        sut.view = dummyView
        sut.transactionsBO = transactionsBO

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.reloadTransactionsList(model: transactionFilterTransport)

        // then
        XCTAssert(dummyView.isCalledShowTransactions == false)
        XCTAssert(dummyView.isCalledShowNoContent == false)
        XCTAssert(dummyView.isCalledShowNoResults == false)

    }

    func test_givenTransactionsBOWithStatus200AndWithTransactionsAndViewDidLoadFalse_whenICallReloadTransactionsList_thenPresenterCallViewShowTransactionsWithATransactionsDisplayData() {

        // given
        let transactionsBO = TransactionEntityDummyGenerator.transactionsBOWithTransactions()

        let dummyView = TransactionsListDummyView()

        sut.view = dummyView
        sut.transactionsBO = transactionsBO

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.reloadTransactionsList(model: transactionFilterTransport)

        // then
        XCTAssert(dummyView.transactionsDisplayData != nil)
    }

    func test_givenTransactionsBOWithStatus200AndWithTransactionsAndViewDidLoadFalse_whenICallReloadTransactionsList_thenCallAddTransactionsDisplayData() {

        // given
        let transactionsBO = TransactionEntityDummyGenerator.transactionsBOWithTransactions()

        let dummyView = TransactionsListDummyView()

        let dummyTransactionsDisplayData = DummyTransactionsDisplayData()

        sut.displayData = dummyTransactionsDisplayData
        sut.view = dummyView
        sut.transactionsBO = transactionsBO

        dummyView.transactionsBO = transactionsBO

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.reloadTransactionsList(model: transactionFilterTransport)

        // then
        XCTAssert(dummyTransactionsDisplayData.isCallAddTransactionsDisplayData == true)
    }

    func test_givenTransactionsBOWithStatus200AndWithTransactionsAndViewDidLoadFalse_whenICallReloadTransactionsList_thenTransactionsDisplayDataShouldInitializatedWithGivenTransactionsOfTransactionsBO() {

        // given
        let transactionsBO = TransactionEntityDummyGenerator.transactionsBOWithTransactions()

        let dummyView = TransactionsListDummyView()

        let dummyTransactionsDisplayData = DummyTransactionsDisplayData()

        sut.displayData = dummyTransactionsDisplayData
        sut.view = dummyView
        sut.transactionsBO = transactionsBO

        dummyView.transactionsBO = transactionsBO

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.reloadTransactionsList(model: transactionFilterTransport)

        // then
        XCTAssert(dummyTransactionsDisplayData.transactions?.count == transactionsBO.transactions.count)
    }

    func test_givenTransactionsBOWithStatus200AndViewDidLoadFalse_whenICallReloadTransactionsList_thenCallViewNoMoreContent() {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 200
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        let dummyView = TransactionsListDummyView()

        let dummyTransactionsDisplayData = DummyTransactionsDisplayData()

        sut.displayData = dummyTransactionsDisplayData
        sut.view = dummyView
        sut.transactionsBO = transactionsBO

        dummyView.transactionsBO = transactionsBO

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.reloadTransactionsList(model: transactionFilterTransport)

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == true)
    }

    func test_givenAny_whenICallReloadTransactionsList_thenICallViewSendScreenOmniture() {

        // given
        let dummyView = TransactionsListDummyView()
        sut.view = dummyView

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.reloadTransactionsList(model: transactionFilterTransport)

        // then
        XCTAssert(dummyView.isCalledSendScreenOmniture == true)

    }

    func test_givenTransactionsBOWithStatus204AndViewDidLoadTrue_whenICallReloadTransactionsList_thenNeitherViewShowTransactionsNorViewShowNoContentNorViewShowNoResultsAreCalled() {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 204
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        let dummyView = TransactionsListDummyView()

        sut.viewDidLoad = true
        sut.view = dummyView
        sut.transactionsBO = transactionsBO

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.reloadTransactionsList(model: transactionFilterTransport)

        // then
        XCTAssert(dummyView.isCalledShowTransactions == false)
        XCTAssert(dummyView.isCalledShowNoContent == false)
        XCTAssert(dummyView.isCalledShowNoResults == false)

    }

    func test_givenTransactionsBOWithStatus200AndViewDidLoadTrueAndNoFilters_whenICallReloadTransactionsList_thenNeitherViewShowTransactionsNorViewShowNoContentNorViewShowNoResultsAreCalled() {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 200
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        let dummyView = TransactionsListDummyView()

        sut.viewDidLoad = true
        sut.view = dummyView
        sut.transactionsBO = transactionsBO

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.reloadTransactionsList(model: transactionFilterTransport)

        // then
        XCTAssert(dummyView.isCalledShowTransactions == false)
        XCTAssert(dummyView.isCalledShowNoContent == false)
        XCTAssert(dummyView.isCalledShowNoResults == false)

    }

    func test_givenTransactionsBOWithStatus200AndViewDidLoadFalseAndNoFilters_whenICallReloadTransactionsList_thenNeitherViewShowTransactionsNorViewShowNoContentNorViewShowNoResultsAreCalled() {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 200
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        let dummyView = TransactionsListDummyView()

        sut.viewDidLoad = true
        sut.view = dummyView
        sut.transactionsBO = transactionsBO

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.reloadTransactionsList(model: transactionFilterTransport)

        // then
        XCTAssert(dummyView.isCalledShowTransactions == false)
        XCTAssert(dummyView.isCalledShowNoContent == false)
        XCTAssert(dummyView.isCalledShowNoResults == false)

    }

    func test_givenTransactionsBOWithStatus208AndViewDidLoadTrueAndNoFilters_whenICallReloadTransactionsList_thenNeitherViewShowTransactionsNorViewShowNoContentNorViewShowNoResultsAreCalled() {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 208
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        let dummyView = TransactionsListDummyView()

        sut.viewDidLoad = true
        sut.view = dummyView
        sut.transactionsBO = transactionsBO

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.reloadTransactionsList(model: transactionFilterTransport)

        // then
        XCTAssert(dummyView.isCalledShowTransactions == false)
        XCTAssert(dummyView.isCalledShowNoContent == false)
        XCTAssert(dummyView.isCalledShowNoResults == false)

    }

    func test_givenTransactionsBOWithStatus200AndViewDidLoadTrue_whenICallReloadTransactionsList_thenDoNotCallAddTransactionsDisplayData() {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 200
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        let dummyView = TransactionsListDummyView()

        let dummyTransactionsDisplayData = DummyTransactionsDisplayData()

        sut.viewDidLoad = true
        sut.displayData = dummyTransactionsDisplayData
        sut.view = dummyView
        sut.transactionsBO = transactionsBO

        dummyView.transactionsBO = transactionsBO

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.reloadTransactionsList(model: transactionFilterTransport)

        // then
        XCTAssert(dummyTransactionsDisplayData.isCallAddTransactionsDisplayData == false)
    }

    func test_givenTransactionsBOWithStatus200AndViewDidLoadTrue_whenICallReloadTransactionsList_thenDoNotCallViewNoMoreContent() {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 200
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        let dummyView = TransactionsListDummyView()

        let dummyTransactionsDisplayData = DummyTransactionsDisplayData()

        sut.viewDidLoad = true
        sut.displayData = dummyTransactionsDisplayData
        sut.view = dummyView
        sut.transactionsBO = transactionsBO

        dummyView.transactionsBO = transactionsBO

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.reloadTransactionsList(model: transactionFilterTransport)

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == false)
    }
    
    func test_givenAny_whenICallReloadTransactionsList_thenViewDidLoadShouldBeTrue() {

        // given
        let dummyInteractor = TransactionListDummyInteractor()
        sut.interactor = dummyInteractor

        let dummyCardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = dummyCardBO

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.reloadTransactionsList(model: transactionFilterTransport)

        // then
        XCTAssert(sut.viewDidLoad == true)

    }
    
    func test_givenTransactionsBONilAndCardBOAndViewDidLoadFalse_whenICallReloadTransactionsListAndInteractorReturnSuccess200WithTransactions_thenINotCallShowNoContent() {
        
        // given
        let transactionsBO = TransactionEntityDummyGenerator.transactionsBOWithTransactions()
        
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO
        
        let dummyView = TransactionsListDummyView()
        
        sut.view = dummyView
        
        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.transactionsBO = transactionsBO
        
        sut.interactor = dummyInteractor
        
        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        transactionFilterTransport.cardBO = cardBO
        sut.transactionFilterTransport = transactionFilterTransport
        
        // when
        sut.reloadTransactionsList(model: transactionFilterTransport)
        
        // then
        XCTAssert(dummyView.isCalledShowNoContent == false)
    }
    
    func test_givenTransactionsBONilAndCardBOAndViewDidLoadFalse_whenICallReloadTransactionsListAndInteractorReturnSuccess200WithoutTransactions_thenICallShowNoContent() {
        
        // given
        let transactionsBO = TransactionEntityDummyGenerator.transactionsBOWithoutTransactions()
        
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO
        
        let dummyView = TransactionsListDummyView()
        
        sut.view = dummyView
        
        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.transactionsBO = transactionsBO
        
        sut.interactor = dummyInteractor
        
        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        transactionFilterTransport.cardBO = cardBO
        sut.transactionFilterTransport = transactionFilterTransport
        
        // when
        sut.reloadTransactionsList(model: transactionFilterTransport)
        
        // then
        XCTAssert(dummyView.isCalledShowNoContent == true)
    }
    
    func test_givenTransactionsBOWithStatus200AndWithTransactionsAndViewDidLoadFalse_whenICallReloadTransactionsList_thenINotCallShowNoContent() {
        
        // given
        let transactionsBO = TransactionEntityDummyGenerator.transactionsBOWithTransactions()
        
        let dummyView = TransactionsListDummyView()
        
        sut.view = dummyView
        sut.transactionsBO = transactionsBO
        
        dummyView.transactionsBO = transactionsBO
        
        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        sut.transactionFilterTransport = transactionFilterTransport
        
        // when
        sut.reloadTransactionsList(model: transactionFilterTransport)
        
        // then
        XCTAssert(dummyView.isCalledShowNoContent == false)
    }
    
    func test_givenTransactionsBOWithStatus200AndWithoutTransactionsAndViewDidLoadFalse_whenICallReloadTransactionsList_thenICallShowNoContent() {
        
        // given
        let transactionsBO = TransactionEntityDummyGenerator.transactionsBOWithoutTransactions()
        
        let dummyView = TransactionsListDummyView()
        
        sut.view = dummyView
        sut.transactionsBO = transactionsBO
        
        dummyView.transactionsBO = transactionsBO
        
        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        sut.transactionFilterTransport = transactionFilterTransport
        
        // when
        sut.reloadTransactionsList(model: transactionFilterTransport)
        
        // then
        XCTAssert(dummyView.isCalledShowNoContent == true)
    }

    // MARK: reloadTransactionsList (checkFilterData)

    func test_givenACardBOAndTransactionsFilterTransport_whenICallReloadTransactionsList_thenShouldLoadFiltersShouldBeFalse() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        let dummyInteractor = TransactionListDummyInteractor()

        sut.interactor = dummyInteractor

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        transactionFilterTransport.cardBO = cardBO
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.reloadTransactionsList(model: transactionFilterTransport)

        // then
        XCTAssert(sut.shouldLoadFilters == false)

    }

    func test_givenACardBOATransactionsFilterTransportAndShouldLoadFiltersTrue_whenICallReloadTransactionsList_thenCallProvideTransactions() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO
        sut.shouldLoadFilters = true

        let dummyInteractor = TransactionListDummyInteractor()

        sut.interactor = dummyInteractor

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        transactionFilterTransport.cardBO = cardBO
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.reloadTransactionsList(model: transactionFilterTransport)

        // then
        XCTAssert(dummyInteractor.isCalledProvideTransactions == true)

    }

    func test_givenATransactionsFilterTransportAndShouldLoadFiltersFalse_whenICallReloadTransactionsList_thenCallDoNotProvideTransactions() {

        // given
        sut.shouldLoadFilters = false

        let dummyInteractor = TransactionListDummyInteractor()
        sut.interactor = dummyInteractor

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.reloadTransactionsList(model: transactionFilterTransport)

        // then
        XCTAssert(dummyInteractor.isCalledProvideTransactions == false)

    }

    func test_givenACardBOATransactionsFilterTransportAndShouldLoadFiltersTrue_whenICallReloadTransactionsList_thenCallViewShowSkeleton() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO
        sut.shouldLoadFilters = true

        let dummyInteractor = TransactionListDummyInteractor()

        let dummyView = TransactionsListDummyView()
        sut.view = dummyView

        sut.interactor = dummyInteractor

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        transactionFilterTransport.cardBO = cardBO
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.reloadTransactionsList(model: transactionFilterTransport)

        // then
        XCTAssert(dummyView.isCalledShowSkeleton == true)

    }

    func test_givenATransactionsFilterTransportAndShouldLoadFiltersFalse_whenICallReloadTransactionsList_thenCallDoNotViewShowSkeleton() {

        // given
        sut.shouldLoadFilters = false

        let dummyInteractor = TransactionListDummyInteractor()

        let dummyView = TransactionsListDummyView()
        sut.view = dummyView

        sut.interactor = dummyInteractor

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.reloadTransactionsList(model: transactionFilterTransport)

        // then
        XCTAssert(dummyView.isCalledShowSkeleton == false)

    }

    func test_givenACardBOAndAPresenterWithOutTransactionFilterTransport_whenICallReloadTransactionsList_thenNotCallProvideTransactions() {

        // given
        let dummyInteractor = TransactionListDummyInteractor()
        sut.interactor = dummyInteractor

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.reloadTransactionsList(model: transactionFilterTransport)

        // then
        XCTAssert(dummyInteractor.isCalledProvideTransactions == false)

    }

    func test_givenACard_whenICallReloadTransactionsListAndShouldLoadFilters_thenCallProvideTransactionsInteractorWithTheCardIdOfCard() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO
        sut.shouldLoadFilters = true

        let dummyInteractor = TransactionListDummyInteractor()
        sut.interactor = dummyInteractor

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        transactionFilterTransport.cardBO = cardBO
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.reloadTransactionsList(model: transactionFilterTransport)

        // then
        XCTAssert(dummyInteractor.cardId == cardBO.cardId)

    }

    func test_givenACardWithCardIdAndShouldLoadFiltersTrue_whenICallReloadTransactionsListAndInteractorReturnSuccess_thenTransactionsBOFilled() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO
        sut.shouldLoadFilters = true

        let dummyInteractor = TransactionListDummyInteractor()

        sut.interactor = dummyInteractor

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        transactionFilterTransport.cardBO = cardBO
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.reloadTransactionsList(model: transactionFilterTransport)

        // then
        XCTAssert(sut.transactionsBO != nil)
    }

    func test_givenACardWithCardId_whenICallReloadTransactionsListAndInteratorReturnError_thenTransactionsBOShouldBeNil() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.forceError = true

        let dummyTransactionsEntity = TransactionsEntity()
        dummyTransactionsEntity.status = 200

        sut.interactor = dummyInteractor

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        transactionFilterTransport.cardBO = cardBO
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.reloadTransactionsList(model: transactionFilterTransport)

        // then
        XCTAssert(sut.transactionsBO == nil)
    }

    func test_givenACardWithCardId_whenICallReloadTransactionsListAndInteratorReturnError_thenICallViewShowErrorContent() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        let dummyView = TransactionsListDummyView()
        sut.view = dummyView

        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.forceError = true

        let dummyTransactionsEntity = TransactionsEntity()
        dummyTransactionsEntity.status = 200

        sut.interactor = dummyInteractor

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        transactionFilterTransport.cardBO = cardBO
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.reloadTransactionsList(model: transactionFilterTransport)

        // then
        XCTAssert(dummyView.isCalledShowErrorContent == true)
    }

    func test_givenACardWithCardId_whenICallReloadTransactionsListAndInteratorReturnError_thenErrorBOShouldBeFilled() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        let dummyView = TransactionsListDummyView()
        sut.view = dummyView

        let dummyInteractor = TransactionListDummyInteractor()
        dummyInteractor.forceError = true

        let dummyTransactionsEntity = TransactionsEntity()
        dummyTransactionsEntity.status = 200

        sut.interactor = dummyInteractor

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        transactionFilterTransport.cardBO = cardBO
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.reloadTransactionsList(model: transactionFilterTransport)

        // then
        XCTAssert(sut.errorBO != nil)
    }

    func test_givenACardBOAndAPresenterWithTransactionFilterTransportAndShouldLoadFiltersTrue_whenICallReloadTransactionsList_thenCallShowFilterScrollView() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO
        sut.shouldLoadFilters = true

        let dummyInteractor = TransactionListDummyInteractor()
        sut.interactor = dummyInteractor
        let dummyView = TransactionsListDummyView()
        sut.view = dummyView

        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        transactionFilterTransport.cardBO = cardBO
        sut.transactionFilterTransport = transactionFilterTransport

        let transactionFilterTransportNew = TransactionFilterTransport()
        transactionFilterTransportNew.conceptText = "Concepto diferente"

        // when
        sut.reloadTransactionsList(model: transactionFilterTransportNew)

        // then
        XCTAssert(dummyView.isCalledShowFiltersScrollView == true)
        XCTAssert(dummyView.isCalledHideFiltersScrollView == false)

    }

    func test_givenACardBOAndAPresenterWithoutTransactionFilterTransportAndShouldLoadFiltersTrue_whenICallReloadTransactionsList_thenCallShowFilterScrollView() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO
        sut.shouldLoadFilters = true

        let dummyInteractor = TransactionListDummyInteractor()
        sut.interactor = dummyInteractor
        let dummyView = TransactionsListDummyView()
        sut.view = dummyView
        let transactionFilterTransport = TransactionFilterTransport()

        // when
        sut.reloadTransactionsList(model: transactionFilterTransport)

        // then
        XCTAssert(dummyView.isCalledShowFiltersScrollView == false)
        XCTAssert(dummyView.isCalledHideFiltersScrollView == true)

    }

    func test_givenACardBOAndAPresenterWithTransactionFilterTransportAndShouldLoadFiltersFalse_whenICallReloadTransactionsList_thenCallDoNotShowFilterScrollView() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO
        sut.shouldLoadFilters = false

        let dummyInteractor = TransactionListDummyInteractor()
        sut.interactor = dummyInteractor
        let dummyView = TransactionsListDummyView()
        sut.view = dummyView
        let transactionFilterTransport = TransactionFilterTransport()
        transactionFilterTransport.conceptText = "Concepto"
        transactionFilterTransport.cardBO = cardBO
        sut.transactionFilterTransport = transactionFilterTransport

        // when
        sut.reloadTransactionsList(model: transactionFilterTransport)

        // then
        XCTAssert(dummyView.isCalledShowFiltersScrollView == false)
        XCTAssert(dummyView.isCalledHideFiltersScrollView == false)

    }

    func test_givenACardBOAndAPresenterWithoutTransactionFilterTransportAndShouldLoadFiltersFalse_whenICallReloadTransactionsList_thenCallDoNotShowFilterScrollView() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO
        sut.shouldLoadFilters = false

        let dummyInteractor = TransactionListDummyInteractor()
        sut.interactor = dummyInteractor
        let dummyView = TransactionsListDummyView()
        sut.view = dummyView
        let transactionFilterTransport = TransactionFilterTransport()

        // when
        sut.reloadTransactionsList(model: transactionFilterTransport)

        // then
        XCTAssert(dummyView.isCalledShowFiltersScrollView == false)
        XCTAssert(dummyView.isCalledHideFiltersScrollView == false)

    }
}

// MARK: DummyData

class TransactionsListDummyBusManager: BusManager {
    
    var isCalledNavigateNextTransactionsListScreen = false
    var isCalledGetViewControllerFromNavigation = false
    var isCalledNavigateNextTransactionDetailScreen = false
    
    var classToPop: AnyClass?
    var cardBO: CardBO?
    
    override init() {
        super.init()
        routerFactory = RouterInitFactory()
    }
    
    override func navigateScreen<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {
        if tag == TransactionListPageReaction.ROUTER_TAG && event === TransactionListPageReaction.EVENT_NAV_TO_NEXT_SCREEN {
            isCalledNavigateNextTransactionsListScreen = true
            cardBO = values as? CardBO
        } else if tag == TransactionListPageReaction.ROUTER_TAG && event === TransactionListPageReaction.EVENT_NAV_TO_TRANSACTION_DETAIL_SCREEN {
            isCalledNavigateNextTransactionDetailScreen = true
        }
    }
    
    override func getViewControllerFromNavigation(route: AnyClass) -> BaseViewController {
        isCalledGetViewControllerFromNavigation = true
        classToPop = route
        return CardsViewController()
    }
    
}

class DummyTransactionsDisplayData: TransactionsDisplayData {
    
    var isCallAddTransactionsDisplayData = false
    
    var transactions: [TransactionBO]?
    
    override func add(transactions: [TransactionBO]) {
        self.transactions = transactions
        self.isCallAddTransactionsDisplayData = true
    }
}

class DummyUiNavigationController: UINavigationController {
    
    var isCalledPopToViewController = false
    
    override func popToViewController(_ viewController: UIViewController, animated: Bool) -> [UIViewController]? {
        isCalledPopToViewController = true
        return []
    }
    
}

class TransactionsListDummyView: TransactionListViewProtocol, ViewProtocol {
    
    var transactionsBO: TransactionsBO?
    var dummyTransactionsDisplay: DummyTransactionsDisplayData?
    
    var transactionsDisplayData: TransactionsDisplayData?
    
    var isCalledShowTransactions = false
    
    var isCalledShowNoContent = false
    var isCalledShowNoResults = false
    
    var isCalledUpdateForNoMoreContent = false
    var isCalledUpdateForMoreContent = false
    
    var isShowErrorCalled = false
    
    var isCalledShowNextPageAnimation = false
    var isCalledHideNextPageAnimation = false
    
    var isCalledShowFiltersScrollView = false
    var isCalledHideFiltersScrollView = false
    
    var isCalledShowErrorContent = false
    var isCalledHideSkeleton = true
    
    var isCalledSendScreenOmniture = false
    
    var filters: [FilterScrollDisplayData]?
    
    var isCalledShowSkeleton = false
    
    func showNoContent() {
        isCalledShowNoContent = true
    }
    
    func showNoResults() {
        isCalledShowNoResults = true
    }
    
    func showTransactions(transactionsDisplayData: TransactionsDisplayData) {
        self.transactionsDisplayData = transactionsDisplayData
        self.isCalledShowTransactions = true
    }
    
    func updateForNoMoreContent() {
        self.isCalledUpdateForNoMoreContent = true
    }
    
    func updateForMoreContent() {
        self.isCalledUpdateForMoreContent = true
    }
    
    func showError(error: ModelBO) {
        self.isShowErrorCalled = true
    }
    
    func changeColorEditTexts() {
    }
    
    func showNextPageAnimation() {
        self.isCalledShowNextPageAnimation = true
    }
    
    func hideNextPageAnimation() {
        self.isCalledHideNextPageAnimation = true
    }
    
    func showSkeleton() {
        isCalledShowSkeleton = true
    }
    
    func hideSkeleton() {
        isCalledHideSkeleton = true
    }
    
    func showFiltersScrollView(withFilters filters: [FilterScrollDisplayData]?) {
        
        self.isCalledShowFiltersScrollView = true
        self.filters = filters
    }
    
    func hideFiltersScrollView() {
        self.isCalledHideFiltersScrollView = true
    }
    
    func sendOmnitureScreen() {
        isCalledSendScreenOmniture = true
    }
    
    func showErrorContent() {
        isCalledShowErrorContent = true
    }
}

class TransactionListDummyInteractor: TransactionListInteractorProtocol {
    
    var notReturnPagination = false
    var isCalledProvideTransactions = false
    var isCallProvideTransactionsNextPage = false
    var forceError = false
    var wasLoadingNextPage = true
    var wasShouldLoadFilters = false
    
    var statusToReturn = 200
    
    var nextPage: String?
    var pagination: PaginationBO!
    var presenter: TransactionsListPresenter<TransactionsListDummyView>?
    var errorBO: ErrorBO?
    var cardId: String?
    var transactionFilterSent: TransactionFilterTransport?
    var transactionsBO: TransactionsBO?
    
    func provideTransactions(fromNextPage nextPage: String) -> Observable <ModelBO> {
        
        self.isCallProvideTransactionsNextPage = true
        
        self.nextPage = nextPage
        
        let dummyTransactionsEntity = TransactionsEntity()
        dummyTransactionsEntity.status = statusToReturn
        
        if !notReturnPagination {
            dummyTransactionsEntity.pagination = GeneratorDummyPaginationEntity.completePaginationEntity()
        }
        
        if transactionsBO == nil {
            transactionsBO = TransactionsBO(transactionsEntity: dummyTransactionsEntity)
        }
        
        self.pagination = transactionsBO!.pagination
        
        if !forceError {
            
            if let presenter = self.presenter {
                self.wasLoadingNextPage = presenter.isLoadingNextPage
            }
            
            return Observable <ModelBO>.just(transactionsBO! as ModelBO)
        } else {
            
            let entity = ErrorEntity(message: "Error")
            entity.code = "80"
            entity.httpStatus = 404
            
            errorBO = ErrorBO(error: entity)
            errorBO!.isErrorShown = false
            
            return Observable.error(ServiceError.GenericErrorBO(error: errorBO!))
        }
    }
    
    func provideTransaction(withTransactionFilterTransport transactionFilterTransport: TransactionFilterTransport?, forCardId cardId: String) -> Observable<ModelBO> {
        
        self.cardId = cardId
        
        transactionFilterSent = transactionFilterTransport
        
        self.isCalledProvideTransactions = true
        
        if let presenter = presenter {
            wasShouldLoadFilters = presenter.shouldLoadFilters
        }
        
        let dummyTransactionsEntity = TransactionsEntity()
        dummyTransactionsEntity.status = 200
        
        if transactionsBO == nil {
            transactionsBO = TransactionsBO(transactionsEntity: dummyTransactionsEntity)
        }
        if !forceError {
            return Observable <ModelBO>.just(transactionsBO! as ModelBO)
        } else {
            let entity = ErrorEntity(message: "Error")
            entity.code = "80"
            entity.httpStatus = 404
            
            errorBO = ErrorBO(error: entity)
            errorBO!.isErrorShown = false
            return Observable.error(ServiceError.GenericErrorBO(error: errorBO!))
        }
    }
}
