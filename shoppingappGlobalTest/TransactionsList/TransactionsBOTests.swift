//
//  TransactionsBOTests.swift
//  shoppingapp
//
//  Created by jesus.martinez on 13/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class TransactionsBOTests: XCTestCase {

    // MARK: TransactionsBO

    func test_givenTransactionsEntityWithNoData_whenICreateTrasactionsBO_thenTransactionShouldBeEmpty() {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 200

        // when
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        // then
        XCTAssert(transactionsBO.transactions.isEmpty)

    }

    func test_givenTransactionsEntityWithData_whenICreateTrasactionsBO_thenTransactionsShouldMatchWithEntity() {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 200

        var transactions = [TransactionEntity]()

        for _ in 1...3 {
            transactions.append(TransactionEntityDummyGenerator.transactionEntity())
        }

        transactionsEntity.data = transactions

        // when
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        // then
        XCTAssert(transactionsBO.transactions.count == transactions.count)
    }

    func test_givenTransactionsEntityWithoutPagination_whenICreateTrasactionsBO_thenPaginationShouldBeNil() {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 200

        // when
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        // then
        XCTAssert(transactionsBO.pagination == nil)
    }

    func test_givenTransactionsEntityWithPagination_whenICreateTrasactionsBO_thenPaginationShouldNotBeNil() {

        // given
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 200
        transactionsEntity.pagination = GeneratorDummyPaginationEntity.completePaginationEntity()

        // when
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        // then
        XCTAssert(transactionsBO.pagination != nil)
    }

    // MARK: TransactionBO

    func test_givenTransactionEntityWithFinancingTypeFINANCING_AVAILABLE_whenICreateTransactionBO_thenfinancingTypeShouldBeFinanciable() {

        // given
        var entity = TransactionEntityDummyGenerator.transactionEntity()
        entity.financingType = IdNameEntity(id: "FINANCING_AVAILABLE", name: "FINANCING_AVAILABLE")

        // when
        let transactionBO = TransactionBO(transactionEntity: entity)!

        // then
        XCTAssert(transactionBO.financingType == .financiable)
    }

    func test_givenTransactionEntityWithFinancingTypeFINANCED_AMOUNT_whenICreateTransactionBO_thenfinancingTypeShouldBeFinancied() {

        // given
        var entity = TransactionEntityDummyGenerator.transactionEntity()
        entity.financingType = IdNameEntity(id: "FINANCED_AMOUNT", name: "FINANCED_AMOUNT")

        // when
        let transactionBO = TransactionBO(transactionEntity: entity)!

        // then
        XCTAssert(transactionBO.financingType == .financed)
    }

    func test_givenTransactionEntityWithFinancingTypeNON_FINANCING_whenICreateTransactionBO_thenfinancingTypeShouldBeNone() {

        // given
        var entity = TransactionEntityDummyGenerator.transactionEntity()
        entity.financingType = IdNameEntity(id: "NON_FINANCING", name: "NON_FINANCING")

        // when
        let transactionBO = TransactionBO(transactionEntity: entity)!

        // then
        XCTAssert(transactionBO.financingType == .none)
    }

    func test_givenTransactionEntityWithFinancingTypeINVALID_whenICreateTransactionBO_thenfinancingTypeShouldBeNone() {

        // given
        var entity = TransactionEntityDummyGenerator.transactionEntity()
        entity.financingType = IdNameEntity(id: "INVALID", name: "INVALID")

        // when
        let transactionBO = TransactionBO(transactionEntity: entity)!

        // then
        XCTAssert(transactionBO.financingType == .none)
    }

    func test_givenTransactionEntityWithTransactionStatusSETTLED_whenICreateTransactionBO_thenStatusShouldBeSettled() {

        // given
        var entity = TransactionEntityDummyGenerator.transactionEntity()
        entity.status = IdNameEntity(id: "SETTLED", name: "SETTLED")

        // when
        let transactionBO = TransactionBO(transactionEntity: entity)!

        // then
        XCTAssert(transactionBO.status == .settled)
    }

    func test_givenTransactionEntityWithTransactionStatusPENDING_whenICreateTransactionBO_thenStatusShouldBePending() {

        // given
        var entity = TransactionEntityDummyGenerator.transactionEntity()
        entity.status = IdNameEntity(id: "PENDING", name: "PENDING")

        // when
        let transactionBO = TransactionBO(transactionEntity: entity)!

        // then
        XCTAssert(transactionBO.status == .pending)

    }

    func test_givenTransactionEntityWithTransactionStatusINVALID_whenICreateTransactionBO_thenStatusShouldBeSettled() {

        // given
        var entity = TransactionEntityDummyGenerator.transactionEntity()
        entity.status = IdNameEntity(id: "INVALID", name: "INVALID")

        // when
        let transactionBO = TransactionBO(transactionEntity: entity)!

        // then
        XCTAssert(transactionBO.status == .settled)
    }

    func test_givenTransactionEntityWithTransactionTypePURCHASE_whenICreateTransactionBO_thenTransactionTypeShouldBePurchase() {

        // given
        var entity = TransactionEntityDummyGenerator.transactionEntity()
        entity.transactionType = TransactionTypeEntity(id: "PURCHASE", name: "PURCHASE", internalCode: nil)

        // when
        let transactionBO = TransactionBO(transactionEntity: entity)!

        // then
        XCTAssert(transactionBO.transactionType == .purchase)
    }

    func test_givenTransactionEntityWithTransactionTypeREFUND_whenICreateTransactionBO_thenTransactionTypeShouldBeRefund() {

        // given
        var entity = TransactionEntityDummyGenerator.transactionEntity()
        entity.transactionType = TransactionTypeEntity(id: "REFUND", name: "REFUND", internalCode: nil)

        // when
        let transactionBO = TransactionBO(transactionEntity: entity)!

        // then
        XCTAssert(transactionBO.transactionType == .refund)
    }

    func test_givenTransactionEntityWithTransactionTypeCASH_WITHDRAWAL_whenICreateTransactionBO_thenTransactionTypeShouldBeCashWithdrawal() {

        // given
        var entity = TransactionEntityDummyGenerator.transactionEntity()
        entity.transactionType = TransactionTypeEntity(id: "CASH_WITHDRAWAL", name: "CASH_WITHDRAWAL", internalCode: nil)

        // when
        let transactionBO = TransactionBO(transactionEntity: entity)!

        // then
        XCTAssert(transactionBO.transactionType == .cashWithdrawal)
    }

    func test_givenTransactionEntityWithTransactionTypeCASH_INCOME_whenICreateTransactionBO_thenTransactionTypeShouldBeCashIncome() {

        // given
        var entity = TransactionEntityDummyGenerator.transactionEntity()
        entity.transactionType = TransactionTypeEntity(id: "CASH_INCOME", name: "CASH_INCOME", internalCode: nil)

        // when
        let transactionBO = TransactionBO(transactionEntity: entity)!

        // then
        XCTAssert(transactionBO.transactionType == .cashIncome)
    }

    func test_givenTransactionEntityWithTransactionTypeUNCATEGORIZED_whenICreateTransactionBO_thenTransactionTypeShouldBeUncategorized() {

        // given
        var entity = TransactionEntityDummyGenerator.transactionEntity()
        entity.transactionType = TransactionTypeEntity(id: "UNCATEGORIZED", name: "UNCATEGORIZED", internalCode: nil)

        // when
        let transactionBO = TransactionBO(transactionEntity: entity)!

        // then
        XCTAssert(transactionBO.transactionType == .uncategorized)
    }

    func test_givenTransactionEntityWithTransactionTypeINVALID_whenICreateTransactionBO_thenTransactionTypeShouldBeUncategorized() {

        // given
        var entity = TransactionEntityDummyGenerator.transactionEntity()
        entity.transactionType = TransactionTypeEntity(id: "INVALID", name: "INVALID", internalCode: nil)

        // when
        let transactionBO = TransactionBO(transactionEntity: entity)!

        // then
        XCTAssert(transactionBO.transactionType == .uncategorized)
    }
}
