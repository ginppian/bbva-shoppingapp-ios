//
//  TransactionsListPresenterATest.swift
//  shoppingappMXTest
//
//  Created by Armando Vazquez on 9/27/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest
import RxSwift
import CellsNativeComponents

#if TESTMX
@testable import shoppingappMX
#endif

class TransactionsListPresenterATest: XCTestCase {

    var sut: TransactionsListPresenterA<TransactionsListDummyView>!
    var dummyBusManager: DummyBusManager!
    var dummyUiNavigationController: DummyUiNavigationController!

    override func setUp() {
        super.setUp()
        dummyBusManager = DummyBusManager()
        dummyUiNavigationController = DummyUiNavigationController()
        dummyBusManager.navigationController = dummyUiNavigationController
        sut = TransactionsListPresenterA<TransactionsListDummyView>(busManager: dummyBusManager)
    }

    // MARK: LoadData for debit

    func test_givenCardBOOfTypeDebit_whenICallLoadDataFromService_thenICallInteractorProvideTransactionsForAccounts() {

        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.accountID = "100"
        sut.cardBO = cardBO

        let dummyInteractor = TransactionListDummyInteractorA()
        sut.interactor = dummyInteractor

        // when
        sut.loadDataFromService()

        // then
        XCTAssert(dummyInteractor.isCalledProvideTransactionsForAccount == true)

    }
    
    func test_givenCardBOOfTypeDebit_whenICallLoadDataFromService_thenIDoNotCallInteractorProvideTransactions() {
        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.accountID = "100"
        sut.cardBO = cardBO
        
        let dummyInteractor = TransactionListDummyInteractorA()
        sut.interactor = dummyInteractor
        
        // when
        sut.loadDataFromService()
        
        // then
        XCTAssert(dummyInteractor.isCalledProvideTransactions == false)

    }
    
    func test_givenCardBOOfOtherThanTypeDebit_whenICallLoadDataFromService_thenIDoNotCallInteractorProvideTransactionsForAccounts() {
        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "ANY_OTHER").cards[0]
        cardBO.accountID = "100"
        sut.cardBO = cardBO
        
        let dummyInteractor = TransactionListDummyInteractorA()
        sut.interactor = dummyInteractor
        
        // when
        sut.loadDataFromService()
        
        // then
        XCTAssert(dummyInteractor.isCalledProvideTransactionsForAccount == false)
        
    }
    
    func test_givenCardBOOfOtherThanTypeDebit_whenICallLoadDataFromService_thenICallInteractorProvideTransactions() {
        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "ANY_OTHER").cards[0]
        cardBO.accountID = "100"
        sut.cardBO = cardBO
        
        let dummyInteractor = TransactionListDummyInteractorA()
        sut.interactor = dummyInteractor
        
        // when
        sut.loadDataFromService()
        
        // then
        XCTAssert(dummyInteractor.isCalledProvideTransactions == true)
        
    }
    
    func test_givenCardBOOfOtherThanTypeDebit_whenICallLoadDataFromService_thenIDontCallInteractorProvideTransactionsForAccounts() {
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "ANY_OTHER").cards[0]
        cardBO.accountID = "100"
        sut.cardBO = cardBO
        
        let dummyInteractor = TransactionListDummyInteractorA()
        sut.interactor = dummyInteractor
        
        // when
        sut.loadDataFromService()
        
        // then
        XCTAssert(dummyInteractor.isCalledProvideTransactionsForAccount == false)

    }

    func test_givenCardBOOfTypeDebit_whenICallLoadDataFromService_thenCallProvideTransactionsInteractorWithAccountID() {

        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.accountID = "100"
        sut.cardBO = cardBO

        let dummyInteractor = TransactionListDummyInteractorA()
        sut.interactor = dummyInteractor

        // when
        sut.loadDataFromService()

        // then
        XCTAssert(dummyInteractor.cardId == cardBO.accountID)

    }

    func test_givenCardBOOfTypeDebit_whenICallLoadDataAndInteractorReturnSuccess_thenTransactionsBOFilled() {

        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.accountID = "100"
        sut.cardBO = cardBO

        let dummyInteractor = TransactionListDummyInteractorA()

        sut.interactor = dummyInteractor

        // when
        sut.loadDataFromService()

        // then
        XCTAssert(sut.transactionsBO != nil)
    }

    func test_givenTransactionsBONilAndCardBOAndViewDidLoadFalse_whenICallLoadDataAndInteractorReturnSuccess200WithTransactions_thenICallViewShowTransactions() {

        // given
        let transactionsBO = TransactionEntityDummyGenerator.transactionsBOWithTransactions()

        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.accountID = "100"
        sut.cardBO = cardBO

        let dummyView = TransactionsListDummyView()

        sut.view = dummyView

        let dummyInteractor = TransactionListDummyInteractorA()
        dummyInteractor.transactionsBO = transactionsBO

        sut.interactor = dummyInteractor

        // when
        sut.loadDataFromService()

        // then
        XCTAssert(dummyView.isCalledShowTransactions == true)
    }

    func test_givenCardBOOfTypeDebit_whenICallLoadDataAndInteratorReturnError_thenTransactionsBOShouldBeNil() {

        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.accountID = "100"
        sut.cardBO = cardBO

        let dummyInteractor = TransactionListDummyInteractorA()
        dummyInteractor.forceError = true

        sut.interactor = dummyInteractor

        // when
        sut.loadDataFromService()

        // then
        XCTAssert(sut.transactionsBO == nil)
    }
    
    func test_givenCardBOOfOtherThanTypeDebit_whenICallLoadDataAndInteratorReturnError_thenTransactionsBOShouldBeNil() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "Other").cards[0]
        cardBO.accountID = "100"
        sut.cardBO = cardBO
        
        let dummyInteractor = TransactionListDummyInteractorA()
        dummyInteractor.forceError = true
        
        sut.interactor = dummyInteractor
        
        // when
        sut.loadDataFromService()
        
        // then
        XCTAssert(sut.transactionsBO == nil)
    }

    func test_givenTransactionsBONilAndCardBOAndViewDidLoadFalse_whenICallLoadDataAndInteratorReturnError_thenErrorBOShouldBeFilled() {

        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.accountID = "100"
        sut.cardBO = cardBO

        let dummyInteractor = TransactionListDummyInteractorA()
        dummyInteractor.forceError = true

        sut.interactor = dummyInteractor

        // when
        sut.loadDataFromService()

        // then
        XCTAssert(sut.errorBO != nil)
    }

    func test_givenCardBOOfOtherThanTypeDebit_whenICallLoadDataAndInteratorReturnError_thenErrorBOShouldBeFilled() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "Other").cards[0]
        cardBO.accountID = "100"
        sut.cardBO = cardBO
        
        let dummyInteractor = TransactionListDummyInteractorA()
        dummyInteractor.forceError = true
        
        sut.interactor = dummyInteractor
        
        // when
        sut.loadDataFromService()
        
        // then
        XCTAssert(sut.errorBO != nil)
    }
    
    func test_givenNoAccountID_whenICallLoadDataFromService_thenErrorBOShouldBeFilled() {

        let dummyInteractor = TransactionListDummyInteractorA()
        let dummyView = TransactionsListDummyView()
        
        sut.interactor = dummyInteractor
        sut.view = dummyView
        let dummyCardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        sut.cardBO = dummyCardBO
        
        // given
        sut.cardBO?.accountID = nil
        
        // when
        sut.loadDataFromService()
        
        XCTAssert(sut.errorBO != nil)
    }
    
    func test_givenNoAccountIDForCredit_whenICallLoadDataFromService_thenErrorBOShouldBeNil() {
        
        let dummyInteractor = TransactionListDummyInteractorA()
        let dummyView = TransactionsListDummyView()
        
        sut.interactor = dummyInteractor
        sut.view = dummyView
        let dummyCardBO = CardsGenerator.getCardBOWithCardType(withCardType: "Other_Type").cards[0]
        sut.cardBO = dummyCardBO
        
        // given
        sut.cardBO?.accountID = nil
        
        // when
        sut.loadDataFromService()
        
        XCTAssert(sut.errorBO == nil)
    }
    
    func test_givenACreditCard_whenICallLoadDataFromService_thenICallProvideTransaction() {
        let dummyInteractor = TransactionListDummyInteractorA()
        sut.interactor = dummyInteractor

        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "CREDIT_CARD").cards[0]
        sut.cardBO = cardBO
        //when
        sut.loadDataFromService()
        //then
        XCTAssert(dummyInteractor.isCalledProvideTransactions == true)
        XCTAssert(dummyInteractor.isCalledProvideTransactionsForAccount == false)

    }
    
    func test_givenTransactionsBONilAndCardBOAndViewDidLoadFalse_whenICallLoadDataAndInteractorReturnSuccess200WithTransactions_thenINotCallShowNoContent() {
        
        // given
        let transactionsBO = TransactionEntityDummyGenerator.transactionsBOWithTransactions()
        
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.accountID = "100"
        sut.cardBO = cardBO
        
        let dummyView = TransactionsListDummyView()
        
        sut.view = dummyView
        
        let dummyInteractor = TransactionListDummyInteractorA()
        dummyInteractor.transactionsBO = transactionsBO
        
        sut.interactor = dummyInteractor
        
        // when
        sut.loadDataFromService()
        
        // then
        XCTAssert(dummyView.isCalledShowNoContent == false)
    }

    func test_givenTransactionsBONilAndCardBOAndViewDidLoadFalse_whenICallLoadDataAndInteractorReturnSuccess200WithoutTransactions_thenICallShowNoContent() {
        
        // given
        let transactionsBO = TransactionEntityDummyGenerator.transactionsBOWithoutTransactions()
        
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.accountID = "100"
        sut.cardBO = cardBO
        
        let dummyView = TransactionsListDummyView()
        
        sut.view = dummyView
        
        let dummyInteractor = TransactionListDummyInteractorA()
        dummyInteractor.transactionsBO = transactionsBO
        
        sut.interactor = dummyInteractor
        
        // when
        sut.loadDataFromService()
        
        // then
        XCTAssert(dummyView.isCalledShowNoContent == true)
    }
}

// MARK: Helper Classes

class TransactionListDummyInteractorA: TransactionListDummyInteractor, TransactionListInteractorProtocolA {
    
    var isCalledProvideTransactionsForAccount = false
    
    func provideTransactionsForAccount(filteredBy transactionFilterTransport: TransactionFilterTransport?, with accountId: String) -> Observable<ModelBO> {
        self.cardId = accountId
        
        transactionFilterSent = transactionFilterTransport
        
        isCalledProvideTransactionsForAccount = true
        
        if let presenter = presenter {
            wasShouldLoadFilters = presenter.shouldLoadFilters
        }
        
        let dummyTransactionsEntity = TransactionsEntity()
        dummyTransactionsEntity.status = 200
        
        if transactionsBO == nil {
            transactionsBO = TransactionsBO(transactionsEntity: dummyTransactionsEntity)
        }
        if !forceError {
            return Observable <ModelBO>.just(transactionsBO! as ModelBO)
        } else {
            let entity = ErrorEntity(message: "Error")
            entity.code = "80"
            entity.httpStatus = 404
            
            errorBO = ErrorBO(error: entity)
            errorBO!.isErrorShown = false
            return Observable.error(ServiceError.GenericErrorBO(error: errorBO!))
        }
    }
}
