//
//  TransactionDisplayItemTests.swift
//  shoppingapp
//
//  Created by jesus.martinez on 13/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class TransactionDisplayItemTests: XCTestCase {

    func test_givenTransactionBOSettledAndFinancingStatusNone_whenICallGenerateTransactionDisplayItem_thenItemReturnedShouldHavePendingEmptyFinancingEmptyAndIsNotFinanciable() {

        // given
        var transactionEntity = TransactionEntityDummyGenerator.transactionEntity()
        transactionEntity.status = IdNameEntity(id: "SETTLED", name: "SETTLED")
        transactionEntity.financingType = IdNameEntity(id: "NON_FINANCING", name: "NON_FINANCING")
        transactionEntity.accountedDate = nil
        let transactionBO = TransactionBO(transactionEntity: transactionEntity)!

        // when
        let item = TransactionDisplayItem(fromTransactionBO: transactionBO)

        // then
        XCTAssert(item.pending.isEmpty)
        XCTAssert(item.financing.isEmpty)
        XCTAssert(item.isFinanciable == false)
        XCTAssert(item.accountedDate == nil)
        XCTAssert(item.operationDate == Date.string(fromDate: transactionBO.operationDate, withFormat: Date.DATE_FORMAT_MONTH_NAME_YEAR_HOUR))

    }

    func test_givenTransactionBOSettledAndFinancingStatusFinanciable_whenICallGenerateTransactionDisplayItem_thenItemReturnedShouldHavePendingEmptyFinancingFinanciableAndIsFinanciable() {

        // given
        var transactionEntity = TransactionEntityDummyGenerator.transactionEntity()
        transactionEntity.status = IdNameEntity(id: "SETTLED", name: "SETTLED")
        transactionEntity.financingType = IdNameEntity(id: "FINANCING_AVAILABLE", name: "FINANCING_AVAILABLE")

        let transactionBO = TransactionBO(transactionEntity: transactionEntity)!

        // when
        let item = TransactionDisplayItem(fromTransactionBO: transactionBO)

        // then
        XCTAssert(item.pending.isEmpty)
        XCTAssert(item.financing == Localizables.transactions.key_transactions_financiable_text)
        XCTAssert(item.isFinanciable == true)
        XCTAssert(item.accountedDate == Date.string(fromDate: transactionBO.accountedDate!, withFormat: Date.DATE_FORMAT_MONTH_NAME_YEAR_HOUR))
        XCTAssert(item.operationDate == Date.string(fromDate: transactionBO.operationDate, withFormat: Date.DATE_FORMAT_MONTH_NAME_YEAR_HOUR))

    }

    func test_givenTransactionBOSettledAndFinancingStatusFinancied_whenICallGenerateTransactionDisplayItem_thenItemReturnedShouldHavePendingEmptyFinancingFinanciedAndIsNotFinanciable() {

        // given
        var transactionEntity = TransactionEntityDummyGenerator.transactionEntity()
        transactionEntity.status = IdNameEntity(id: "SETTLED", name: "SETTLED")
        transactionEntity.financingType = IdNameEntity(id: "FINANCED_AMOUNT", name: "FINANCED_AMOUNT")

        let transactionBO = TransactionBO(transactionEntity: transactionEntity)!

        // when
        let item = TransactionDisplayItem(fromTransactionBO: transactionBO)

        // then
        XCTAssert(item.pending.isEmpty)
        XCTAssert(item.financing == Localizables.transactions.key_transactions_financed_text)
        XCTAssert(item.isFinanciable == false)
        XCTAssert(item.accountedDate == Date.string(fromDate: transactionBO.accountedDate!, withFormat: Date.DATE_FORMAT_MONTH_NAME_YEAR_HOUR))
        XCTAssert(item.operationDate == Date.string(fromDate: transactionBO.operationDate, withFormat: Date.DATE_FORMAT_MONTH_NAME_YEAR_HOUR))

    }

    func test_givenTransactionBOPendingAndFinancingStatusNone_whenICallGenerateTransactionDisplayItem_thenItemReturnedShouldHavePendingFilledFinancingEmptyAndIsNotFinanciable() {

        // given
        var transactionEntity = TransactionEntityDummyGenerator.transactionEntity()
        transactionEntity.status = IdNameEntity(id: "PENDING", name: "PENDING")
        transactionEntity.financingType = IdNameEntity(id: "NON_FINANCING", name: "NON_FINANCING")

        let transactionBO = TransactionBO(transactionEntity: transactionEntity)!

        // when
        let item = TransactionDisplayItem(fromTransactionBO: transactionBO)

        // then
        XCTAssert(item.pending == Localizables.transactions.key_transactions_pending_text)
        XCTAssert(item.financing.isEmpty)
        XCTAssert(item.isFinanciable == false)
        XCTAssert(item.accountedDate == Date.string(fromDate: transactionBO.accountedDate!, withFormat: Date.DATE_FORMAT_MONTH_NAME_YEAR_HOUR))
        XCTAssert(item.operationDate == Date.string(fromDate: transactionBO.operationDate, withFormat: Date.DATE_FORMAT_MONTH_NAME_YEAR_HOUR))

    }

    func test_givenTransactionBOPendingAndFinancingStatusFinanciable_whenICallGenerateTransactionDisplayItem_thenItemReturnedShouldHavePendingFilledFinancingFinanciableAndIsFinanciable() {

        // given
        var transactionEntity = TransactionEntityDummyGenerator.transactionEntity()
        transactionEntity.status = IdNameEntity(id: "PENDING", name: "PENDING")
        transactionEntity.financingType = IdNameEntity(id: "FINANCING_AVAILABLE", name: "FINANCING_AVAILABLE")

        let transactionBO = TransactionBO(transactionEntity: transactionEntity)!

        // when
        let item = TransactionDisplayItem(fromTransactionBO: transactionBO)

        // then
        XCTAssert(item.pending == Localizables.transactions.key_transactions_pending_text)
        XCTAssert(item.financing == Localizables.transactions.key_transactions_financiable_text)
        XCTAssert(item.isFinanciable == true)
        XCTAssert(item.accountedDate == Date.string(fromDate: transactionBO.accountedDate!, withFormat: Date.DATE_FORMAT_MONTH_NAME_YEAR_HOUR))
        XCTAssert(item.operationDate == Date.string(fromDate: transactionBO.operationDate, withFormat: Date.DATE_FORMAT_MONTH_NAME_YEAR_HOUR))

    }

    func test_givenTransactionBOPendingAndFinancingStatusFinancied_whenICallGenerateTransactionDisplayItem_thenItemReturnedShouldHavePendingFilledFinancingFinanciedAndIsNotFinanciable() {

        // given
        var transactionEntity = TransactionEntityDummyGenerator.transactionEntity()
        transactionEntity.status = IdNameEntity(id: "PENDING", name: "PENDING")
        transactionEntity.financingType = IdNameEntity(id: "FINANCED_AMOUNT", name: "FINANCED_AMOUNT")

        let transactionBO = TransactionBO(transactionEntity: transactionEntity)!

        // when
        let item = TransactionDisplayItem(fromTransactionBO: transactionBO)

        // then
        XCTAssert(item.pending == Localizables.transactions.key_transactions_pending_text)
        XCTAssert(item.financing == Localizables.transactions.key_transactions_financed_text)
        XCTAssert(item.isFinanciable == false)
        XCTAssert(item.accountedDate == Date.string(fromDate: transactionBO.accountedDate!, withFormat: Date.DATE_FORMAT_MONTH_NAME_YEAR_HOUR))
        XCTAssert(item.operationDate == Date.string(fromDate: transactionBO.operationDate, withFormat: Date.DATE_FORMAT_MONTH_NAME_YEAR_HOUR))

    }

}
