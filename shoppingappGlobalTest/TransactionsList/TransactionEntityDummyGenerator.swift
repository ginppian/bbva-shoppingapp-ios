//
//  TransactionEntityDummyGenerator.swift
//  shoppingapp
//
//  Created by jesus.martinez on 13/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class TransactionEntityDummyGenerator {

    class func transactionEntity() -> TransactionEntity {

        var transactionEntity = TransactionEntity()

        transactionEntity.id = "ID"
        transactionEntity.operationDate = "2016-02-28T16:41:41.090Z"
        transactionEntity.accountedDate = "2016-02-28T16:41:41.090Z"
        transactionEntity.concept = "concept"
        transactionEntity.financingType = IdNameEntity(id: "NON_FINANCING", name: "NON_FINANCING")
        transactionEntity.status = IdNameEntity(id: "SETTLED", name: "SETTLED")
        transactionEntity.transactionType = TransactionTypeEntity(id: "PURCHASE", name: "PURCHASE", internalCode: nil)
        transactionEntity.localAmount = LocalAmountEntity(amount: NSDecimalNumber(value: 20), currency: "EUR")
        transactionEntity.contract = ContractEntity(id: "0005", number: "124124124124124", numberType: IdNameEntity(id: "PAN", name: "Permanent Account Number"), product: IdNameEntity(id: "CARDS", name: "Tarjetas"), alias: "fasdas" )
        transactionEntity.additionalInformation = AdditionalInformationEntity(reference: "Test Reference", additionalData: "Test")

        return transactionEntity
    }
    
    class func transactionWithAccountProductIDEntity() -> TransactionEntity {
        
        var transactionEntity = TransactionEntity()
        
        transactionEntity.id = "ID"
        transactionEntity.operationDate = "2016-02-28T16:41:41.090Z"
        transactionEntity.accountedDate = "2016-02-28T16:41:41.090Z"
        transactionEntity.concept = "concept"
        transactionEntity.financingType = IdNameEntity(id: "NON_FINANCING", name: "NON_FINANCING")
        transactionEntity.status = IdNameEntity(id: "SETTLED", name: "SETTLED")
        transactionEntity.transactionType = TransactionTypeEntity(id: "PURCHASE", name: "PURCHASE", internalCode: nil)
        transactionEntity.localAmount = LocalAmountEntity(amount: NSDecimalNumber(value: 20), currency: "EUR")
        transactionEntity.contract = ContractEntity(id: "0005", number: "124124124124124", numberType: IdNameEntity(id: "PAN", name: "Permanent Account Number"), product: IdNameEntity(id: "ACCOUNTS", name: "Tarjetas"), alias: "fasdas" )
        transactionEntity.additionalInformation = AdditionalInformationEntity(reference: "Test Reference", additionalData: "Test")
        
        return transactionEntity
    }
    
    class func transactionsBOWithTransactions() -> TransactionsBO {
        
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 200
        transactionsEntity.data = [transactionEntity()]
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)
        
        return transactionsBO
    }
    
    class func transactionsBOWithoutTransactions() -> TransactionsBO {
        
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 200
        let transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)
        
        return transactionsBO
    }
}

class GeneratorDummyPaginationEntity {

    class func completePaginationEntity() -> PaginationEntity {

        var paginationEntity = PaginationEntity()

        paginationEntity.links = self.completeLinksEntity()
        paginationEntity.page = 0
        paginationEntity.totalPages = 2
        paginationEntity.totalElements = 3
        paginationEntity.pageSize = 3

        return paginationEntity
    }

    class func completeLinksEntity() -> LinksEntity {

        var linksEntity = LinksEntity()

        linksEntity.first = "first"
        linksEntity.last = "last"
        linksEntity.previous = "previous"
        linksEntity.next = "next"

        return linksEntity
    }

}
