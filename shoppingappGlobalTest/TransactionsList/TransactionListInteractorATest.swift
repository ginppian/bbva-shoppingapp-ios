//
//  TransactionListInteractorATest.swift
//  shoppingappMXTest
//
//  Created by Armando Vazquez on 10/15/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest
import RxSwift

#if TESTMX
@testable import shoppingappMX
#endif

class TransactionsListInteractorATest: XCTestCase {

    let sut = TransactionsListInteractorA()
    
    // MARK: provideTransactionsForAccount
    
    func test_givenNoTransactionFilter_whenICallprovideTransactionsForAccount_thenISet3MonthsRange() {
        let dummyDataManager = TransactionsDataManagerDummy()
        sut.transactionDataManager = dummyDataManager
        
        _ = sut.provideTransactionsForAccount(filteredBy: nil, with: "").subscribe()
        XCTAssert(dummyDataManager.datesAreSet == true)
    }
    
    func test_givenNoTransactionFilter_whenICallprovideTransactionsForAccount_thenISetCorrectDate() {
        let dummyDataManager = TransactionsDataManagerDummy()
        sut.transactionDataManager = dummyDataManager
        let filter: TransactionFilterTransport? = nil
        _ = sut.provideTransactionsForAccount(filteredBy: filter, with: "123").subscribe()
        XCTAssert(dummyDataManager.transactionRangeIsThreeMonths == true)
        
    }
    
    func test_givenNoTransactionFilter_whenICallprovideTransactionsForAccount_thenPaginationIsSetAndMatch() {
        
        let dummyDataManager = TransactionsDataManagerDummy()
        sut.transactionDataManager = dummyDataManager
        
        // given
        let filter: TransactionFilterTransport? = nil
        
        // when
        _ = sut.provideTransactionsForAccount(filteredBy: filter, with: "123").subscribe()

        // then
        XCTAssert(dummyDataManager.pageSize != nil)
        XCTAssert(dummyDataManager.pageSize == sut.pageSize)
    }
    
    func test_givenATransactionFilter_whenICallprovideTransactionsForAccount_thenIDontSetDate() {
        let dummyDataManager = TransactionsDataManagerDummy()
        sut.transactionDataManager = dummyDataManager
        let filter = TransactionFilterTransport()
        _ = sut.provideTransactionsForAccount(filteredBy: filter, with: "123").subscribe()
        XCTAssert(dummyDataManager.datesAreSet == false)
    }
    
}

class TransactionsDataManagerDummy: TransactionsDataManager {
    
    var datesAreSet = false
    var transactionRangeIsThreeMonths = false
    var pageSize: Int?
    
    override func provideAccountTransactions(filteredBy transactionTransport: TransactionFilterTransport?, for accountID: String) -> Observable<ModelEntity> {
        if transactionTransport?.fromDateText != nil, transactionTransport?.toDateText != nil {
            datesAreSet = true
        }
        
        let date = Date()
        let calendar = Calendar.current
        let currentDate = date.string(format: Date.DATE_FORMAT_SERVER)
        var components = calendar.dateComponents([.day, .month, .year], from: date)
        components.month = components.month! - 2
        let threeMonthsAgo = calendar.date(from: components)?.string(format: Date.DATE_FORMAT_SERVER)
        
        if transactionTransport?.toDateText == currentDate, transactionTransport?.fromDateText == threeMonthsAgo {
            transactionRangeIsThreeMonths = true
        }
        
        pageSize = transactionTransport?.pageSize

        let transaction = TransactionsEntity()
        transaction.status = 204

        return Observable<ModelEntity>.just(transaction)

    }
}
