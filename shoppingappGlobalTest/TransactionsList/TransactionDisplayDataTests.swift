//
//  TransactionDisplayDataTests.swift
//  shoppingapp
//
//  Created by jesus.martinez on 13/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class TransactionDisplayDataTests: XCTestCase {

    var sut: TransactionsDisplayData!

    override func setUp() {
        super.setUp()
        self.sut = TransactionsDisplayData()
    }

    func test_givenAnArrayOfTransactionBOWithDEqualDate_whenICallAdd_thenNumberOfSectionsShouldBeOneAndShouldHaveNumberOfItemsAsTransactions() {

        // given
        var transactions = [TransactionBO]()

        for _ in 0...9 {

            transactions.append(TransactionBO(transactionEntity: TransactionEntityDummyGenerator.transactionEntity())!)

        }

        // when
        sut.add(transactions: transactions)

        // then
        XCTAssert(sut.sections.count == 1)
        XCTAssert(sut.sections[0].items.count == transactions.count)

    }

    func test_givenAnArrayOfTransactionBOWithDifferentDates_whenICallAdd_thenNumberOfSectionsShouldBeEqualsOfNumberTransactionsAndShouldHaveOnlyOneItemPerSection() {

        // given
        var transactions = [TransactionBO]()

        for i in 0...9 {

            var entity = TransactionEntityDummyGenerator.transactionEntity()
            entity.operationDate = "20\(i)-10-16T00:00:00.000-05:00"

            transactions.append(TransactionBO(transactionEntity: entity)!)

        }

        // when
        sut.add(transactions: transactions)

        // then
        XCTAssert(sut.sections.count == transactions.count)
        for section in sut.sections {
            XCTAssert(section.items.count == 1)
        }

    }
    
    func test_givenAnArrayOfTransactionsBOWithProductTypeAccounts_whenICallAdd_thenShouldBeTheSameNumberOfSectionsThanTransactions() {
        
        // given
        var transactions = [TransactionBO]()
        
        for i in 0...9 {
            var entity = TransactionEntityDummyGenerator.transactionWithAccountProductIDEntity()
            entity.operationDate = "200\(i)-10-16T00:00:00.000-05:00"
            entity.transactionType = TransactionTypeEntity(id: "UNCATEGORIZED", name: "UNCATEGORIZED", internalCode: nil)
            transactions.append(TransactionBO(transactionEntity: entity)!)
        }
        
        // when
        sut.add(transactions: transactions)

        //then
        XCTAssert(sut.sections.count == transactions.count)
    }

    func test_givenAnArrayOfTransactionBOWithDifferentDates_whenICallAdd_thenReturnASectionSortedFromMoreRecent() {

        // given
        var transactions = [TransactionBO]()

        for i in 0...9 {

            var entity = TransactionEntityDummyGenerator.transactionEntity()
            entity.operationDate = "200\(i)-10-16T00:00:00.000-05:00"

            transactions.append(TransactionBO(transactionEntity: entity)!)

        }

        // when
        sut.add(transactions: transactions)

        // then
        for i in 0...(sut.sections.count - 2) {

            let date = Date.date(fromLocalTimeZoneString: sut.sections[i].name, withFormat: Date.DATE_FORMAT_DAY_MONTH_YEAR)
            let date2 = Date.date(fromLocalTimeZoneString: sut.sections[i + 1].name, withFormat: Date.DATE_FORMAT_DAY_MONTH_YEAR)

            XCTAssert(date! > date2!)

        }
    }

    func test_givenAnArrayOfTransactionBOWithATransactionBOWithDifferentYearOfThisYear_whenICallAdd_thenReturnSectionWithNameFormatDD_MMMM_YYYY() {

        // given
        var transactions = [TransactionBO]()
        var entity = TransactionEntityDummyGenerator.transactionEntity()
        entity.operationDate = "2017-10-16T00:00:00.000-05:00"

        transactions.append(TransactionBO(transactionEntity: entity)!)

        // when
        sut.add(transactions: transactions)

        // then
        XCTAssert(Date.date(fromLocalTimeZoneString: sut.sections[0].name, withFormat: Date.DATE_FORMAT_DAY_MONTH_YEAR) != nil)

    }

    func test_givenAnArrayOfTransactionBOWithATransactionBOInThisYear_whenICallAdd_thenReturnSectionWithNameFormatDD_MMMM() {

        // given
        var transactions = [TransactionBO]()
        var entity = TransactionEntityDummyGenerator.transactionEntity()
        entity.operationDate = Date().string(format: Date.DATE_FORMAT_LONG_SERVER)

        transactions.append(TransactionBO(transactionEntity: entity)!)

        // when
        sut.add(transactions: transactions)

        // then
        XCTAssert(Date.date(fromLocalTimeZoneString: sut.sections[0].name, withFormat: Date.DATE_FORMAT_DAY_MONTH) != nil)

    }

}
