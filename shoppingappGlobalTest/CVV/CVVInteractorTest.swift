//
//  CVVInteractorTest.swift
//  shoppingapp
//
//  Created by Armando Vazquez on 5/22/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import XCTest
import RxSwift

#if TESTMX
@testable import shoppingappMX
#elseif TESTPE
@testable import shoppingappPE
#endif

class CVVInteractorTest: XCTestCase {

    let sut = CVVInteractor()

    // MARK: provideCVV

    func test_givenAny_whenICallProvideCVV_thenICallProvideCVVDataManager() {
        let dummyDataManager = DummyCVVDataManager()
        sut.cvvDataManager = dummyDataManager
        dummyDataManager.cvvEntity = CVVGenerator.getCVVEntityFromJSON()
        // given
        let any = ""
        // when
        _ = sut.provideCVVForCard(withID: any).subscribe()
        // then
        XCTAssert(dummyDataManager.isProvideCVVCalled == true)
    }

    func test_givenAnError_whenICallProvideCVV_thenICallOnError() {
        let dummyDataManager = DummyCVVDataManager()
        sut.cvvDataManager = dummyDataManager
        dummyDataManager.cvvEntity = CVVGenerator.getCVVEntityFromJSON()
        let any = ""

        // given
        dummyDataManager.forceError = true
        // when
        _ = sut.provideCVVForCard(withID: any).subscribe(
            onNext: { _ in
                XCTFail()
            },
            //then
            onError: { _ in
                XCTAssert(true)
            })
    }
    
    func test_givenAValidCVVEntity_WhenICallProvideCVV_thenICallOnNext() {
        let dummyDataManager = DummyCVVDataManager()
        sut.cvvDataManager = dummyDataManager
        let dummyCrypto = DummyCryptoDataManager()
        sut.cryptoDataManager = dummyCrypto

        let any = ""

        // given
        dummyDataManager.cvvEntity = CVVGenerator.getCVVEntityFromJSON()
        // when
        _ = sut.provideCVVForCard(withID: any).subscribe(
            onNext: { _ in
                XCTAssert(true)
            },
            //then
            onError: { _ in
                XCTFail()
            })

    }

    func test_givenACVVEntity_WhenProvideCVVReturnsACVV_thenICallOnNextWithTheSameCVV() {
        let dummyDataManager = DummyCVVDataManager()
        sut.cvvDataManager = dummyDataManager
        let dummyCrypto = DummyCryptoDataManager()
        sut.cryptoDataManager = dummyCrypto

        let any = ""

        // given
        dummyDataManager.cvvEntity = CVVGenerator.getCVVEntityFromJSON()
        // when
        _ = sut.provideCVVForCard(withID: any).subscribe(
            onNext: { result in
                //then
                let cvvBO = result as! CVVBO
                XCTAssert(cvvBO.cardCVV?.id == dummyDataManager.cvvEntity.data?.first?.id)
            },
            onError: { _ in
                XCTFail()
            })
    }

    func test_givenAnEmptyCode_whenIcallProvideCVV_thenIGetGenericErrorTest() {
        let dummyDataManager = DummyCVVDataManager()
        sut.cvvDataManager = dummyDataManager
        dummyDataManager.cvvEntity = CVVGenerator.getCVVEntityFromJSON()
        let any = ""

        // given
        dummyDataManager.cvvEntity.data = nil
        // when
        _ = sut.provideCVVForCard(withID: any).subscribe(
            onNext: { _ in
                XCTFail()
            },
            onError: { error in
                let evaluate: ServiceError = (error as? ServiceError)!
                switch evaluate {
                case .GenericErrorBO(let errorBO):
                    XCTAssert(errorBO.errorMessage() == Localizables.common.key_common_generic_error_text)
                default:
                    break
                }
            })
    }

    // MARK: - LibMCrypt SDK

    func test_givenAny_whenICallProvideCVV_thenGeneratePublicKeyIsCalled() {
        let dummyCVVDataManager = DummyCVVDataManager()
        sut.cvvDataManager = dummyCVVDataManager
        dummyCVVDataManager.cvvEntity = CVVGenerator.getCVVEntityFromJSON()

        let dummyCrypto = DummyCryptoDataManager()
        sut.cryptoDataManager = dummyCrypto
        let any = ""
        //when
        _ = sut.provideCVVForCard(withID: any).subscribe()
        //then
        XCTAssert(dummyCrypto.isgenerateKeyPairCalled == true)
    }

    func test_givenAny_whenICallProvideCVV_thenDecryptionIsCalled() {
        let dummyCVVDataManager = DummyCVVDataManager()
        dummyCVVDataManager.cvvEntity = CVVGenerator.getCVVEntityFromJSON()
        sut.cvvDataManager = dummyCVVDataManager

        let dummyCrypto = DummyCryptoDataManager()
        sut.cryptoDataManager = dummyCrypto
        let any = ""
        //when
        _ = sut.provideCVVForCard(withID: any).subscribe()
        //then
        XCTAssert(dummyCrypto.isDecryptFromHostCalled == true)
    }

    func test_givenABadPublicKey_whenICallProvideCVV_thenIGetAnError() {
        let dummyCVVDataManager = DummyCVVDataManager()
        sut.cvvDataManager = dummyCVVDataManager
        dummyCVVDataManager.cvvEntity = CVVGenerator.getCVVEntityFromJSON()

        let dummyCrypto = DummyCryptoDataManager()
        sut.cryptoDataManager = dummyCrypto
        //given
        dummyCrypto.forcePubKeyError = true
        let any = ""
        //when
        _ = sut.provideCVVForCard(withID: any).subscribe(
            onNext: { _ in XCTFail() },
            //then
            onError: { _ in XCTAssert(true) })
    }

    func test_givenADecryptionError_whenICallProvideCVV_thenIGetAnError() {
        let dummyCVVDataManager = DummyCVVDataManager()
        sut.cvvDataManager = dummyCVVDataManager
        dummyCVVDataManager.cvvEntity = CVVGenerator.getCVVEntityFromJSON()

        let dummyCrypto = DummyCryptoDataManager()
        sut.cryptoDataManager = dummyCrypto
        //given
        dummyCrypto.forceDecryptError = true
        let any = ""
        //when
        _ = sut.provideCVVForCard(withID: any).subscribe(
            onNext: { _ in XCTFail() },
            //then
            onError: { _ in XCTAssert(true) })
    }

    func test_givenACVVEntityWithNilData_whenICallProvideCVV_thenIGetAGenericErrorText() {
        let dummyCVVDataManager = DummyCVVDataManager()
        sut.cvvDataManager = dummyCVVDataManager

        dummyCVVDataManager.cvvEntity = CVVGenerator.getCVVEntityFromJSON()

        let dummyCrypto = DummyCryptoDataManager()
        sut.cryptoDataManager = dummyCrypto
        let any = ""
        //given
        dummyCVVDataManager.cvvEntity.data = nil
        //when
        _ = sut.provideCVVForCard(withID: any).subscribe(
            onNext: { _ in XCTFail() },
            //then
            onError: { error in
                if let evaluate = error as? ServiceError {
                    switch evaluate {
                    case .GenericErrorBO(let errorBO):
                        XCTAssert(errorBO.errorMessage() == Localizables.common.key_common_generic_error_text)
                    default:
                        XCTFail()
                    }
                }
            }
        )
    }

    func test_givenAnEmptyCVVCode_whenICallProvideCVV_thenIGetAGenericErrorText() {
        let dummyCVVDataManager = DummyCVVDataManager()
        sut.cvvDataManager = dummyCVVDataManager

        let dummyCrypto = DummyCryptoDataManager()
        sut.cryptoDataManager = dummyCrypto
        let any = ""

        //given
        dummyCVVDataManager.cvvEntity = CVVGenerator.getCVVEntityFromJSONWithEmptyCode()
        //when
        _ = sut.provideCVVForCard(withID: any).subscribe(
            onNext: { _ in XCTFail() },
            //then
            onError: { error in
                if let evaluate = error as? ServiceError {
                    switch evaluate {
                    case .GenericErrorBO(let errorBO):
                        //then
                        XCTAssert(errorBO.errorMessage() == Localizables.common.key_common_generic_error_text)
                    default:
                        XCTFail()
                    }
                }
            }
        )
    }

    func test_givenAnEmptyCVVCode_whenICallProvideCVV_thenIDoNotCallDecryptFromHost() {
        let dummyCVVDataManager = DummyCVVDataManager()
        sut.cvvDataManager = dummyCVVDataManager

        let dummyCrypto = DummyCryptoDataManager()
        sut.cryptoDataManager = dummyCrypto
        let any = ""

        //given
        dummyCVVDataManager.cvvEntity = CVVGenerator.getCVVEntityFromJSONWithEmptyCode()
        //when
        _ = sut.provideCVVForCard(withID: any).subscribe()
        //then
        XCTAssert(dummyCrypto.isDecryptFromHostCalled == false)

    }

    func test_givenAny_whenICallProvideCVVT_thenTheDecryptedCVVIsTheExpectedCVV() {
        let expectedCVV = "249"
        let dummyCVVDataManager = DummyCVVDataManager()
        sut.cvvDataManager = dummyCVVDataManager
        dummyCVVDataManager.cvvEntity = CVVGenerator.getCVVEntityFromJSON()

        let dummyCrypto = DummyCryptoDataManager()
        sut.cryptoDataManager = dummyCrypto
        //given
        let any = ""
        //when
        _ = sut.provideCVVForCard(withID: any).subscribe(
            onNext: { result in
                let cvvBO = result as! CVVBO
                if let cardCVV = cvvBO.cardCVV {
                    XCTAssert(cardCVV.code == expectedCVV)
                } else { XCTFail() }
            },
            onError: { _ in XCTFail() })
    }

}

class DummyCVVDataManager: CVVDataManager {
    var isProvideCVVCalled = false
    var forceError = false
    var cvvEntity: CVVEntity! // To be set as needed in the test cases
    override func provideCvv(forCardId cardId: String, publicKey: String) -> Observable<CVVEntity> {
        isProvideCVVCalled = true
        if forceError == true {
            let errorEntity = ErrorEntity()
            errorEntity.message = "Error"
            return Observable.error(ServiceError.GenericErrorEntity(error: errorEntity))
        } else {
            return Observable <CVVEntity>.just(cvvEntity)
        }
    }
}
