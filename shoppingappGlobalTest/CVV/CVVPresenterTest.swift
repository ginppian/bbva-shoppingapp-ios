//
//  CVVPresenterTest.swift
//  shoppingappMX
//
//  Created by Daniel Garcia on 08/01/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest
import RxSwift

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class CVVPresenterTest: XCTestCase {

    var sut: CVVPresenter<CVVDummyView>!
    var dummyBusManager: DummyBusManager!
    var serviceResponseDummy: ResponseServiceDummy!
    var dummyInteractor: CVVDummyInteractor!
    var dummyView: CVVDummyView!

    override func setUp() {

        super.setUp()
        sut = CVVPresenter<CVVDummyView>()
        dummyInteractor = CVVDummyInteractor()
        dummyView = CVVDummyView()

        dummyBusManager = DummyBusManager()
        serviceResponseDummy = ResponseServiceDummy()
        sut.view = dummyView
        sut.interactor = dummyInteractor
        sut.busManager = dummyBusManager

    }

    // MARK: setModel

    func test_givenCVVTransport_whenICallSetModel_thenCardIDIsSet() {

        let cvvbo: CVVBO = serviceResponseDummy.requestCvvCard()
        let cardId = cvvbo.cardId

        //given
        let cvvTransport = CVVTransport(with: cardId!)

        // when
        sut.setModel(model: cvvTransport)

        // then
        XCTAssert(sut.cardID != nil)
    }

    func test_givenCVVTransport_whenICallSetModel_thenCardCvvsIDMatch() {
        let cvvbo: CVVBO = ResponseServiceDummy().requestCvvCard()
        let cardId = cvvbo.cardId

        //given
        let cvvTransport = CVVTransport(with: cardId!)

        // when
        sut.setModel(model: cvvTransport)

        // then
        XCTAssert(sut.cardID == cardId)
    }

    func test_givenAnInvalidCVVTransport_whenICallSetModel_thenShowErrorIsCalled() {
        sut.errorBO = ErrorBO()
        //given
        let model = CardsGenerator.getCardBOWithAlias()
        //when
        sut.setModel(model: model)
        // then
        XCTAssert(sut.view?.isCalledShowError == true)

    }

    // MARK: getCVV

    func test_givenATimeForTimerInSeconds_whenICallGetCVV_thenRemainingTimeShouldMatchTimeWithTimerInSeconds() {
        
        let cvvBO: CVVBO = ResponseServiceDummy().requestCvvCard()
        sut.cardID = cvvBO.cardId

        //given
        sut.timeForTimerInSeconds = 30
        //when
        sut.getCvv()
        //then
        XCTAssert(sut.remainingTime == sut.timeForTimerInSeconds)
    }

    func test_givenCorrectCVVCard_whenICallGetCVV_thenICallUpdateCVVView() {

        let dummyView = CVVDummyView()
        let cvvBO: CVVBO = ResponseServiceDummy().requestCvvCard()
        sut.view = dummyView
        // given
        sut.cardID = cvvBO.cardId
        // when
        sut.getCvv()

        // then
        XCTAssert(dummyView.isCalledUpdateCVV == true)

    }

    func test_givenCorrectCVVCard_whenICallGetCVV_thenICallUpdateCVVViewWithProperValue() {

        let dummyView = CVVDummyView()
        // given
        sut.view = dummyView
        let cvvBO: CVVBO = serviceResponseDummy.requestCvvCard()
        sut.cardID = cvvBO.cardId

        // when
        sut.getCvv()

        // then
        XCTAssert(dummyView.value == sut.view?.value)

    }

    func test_givenCorrectCVVCard_whenICallGetCVV_thenICallStartTimer() {

        let dummyView = CVVDummyView()

        // given
        sut.view = dummyView
        let cvvBO: CVVBO = ResponseServiceDummy().requestCvvCard()
        sut.cardID = cvvBO.cardId

        // when
        sut.getCvv()

        // then
        XCTAssert(dummyView.isCalledStartTimer == true)
    }
    
    func test_givenAnError_whenICallGetCVV_thenIDontCallStartTimer() {
        
        let errorBO = ErrorBO(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text))
        errorBO.status = ConstantsHTTPCodes.NO_CONTENT
        sut.cardID = "999"
        //given
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true
        // when
        sut.getCvv()
        
        // then
        XCTAssert(dummyView.isCalledUpdateTimer == false)

    }
    
    func test_givenCorrectCVVCard_whenICallGetCVV_thenICallUpdateTimer() {
        // given
        let cvvBO: CVVBO = ResponseServiceDummy().requestCvvCard()
        sut.cardID = cvvBO.cardId

        // when
        sut.getCvv()

        // then
        XCTAssert(dummyView.isCalledUpdateTimer == true)

    }
    
    func test_givenASuccesfulResponseWhitoutCardCVV_whenICallGetCvvForCard_thenErrorMessageShouldBeEqualsToGenericMessage() {

        //given
        sut.cardID = nil
        
        //when
        sut.getCvv()
        
        //then
        XCTAssert(sut.errorBO?.errorMessage() == Localizables.common.key_common_generic_error_text)
    }
    
    func test_givenASuccesfulResponseWithoutCardCvv_whenICallGetCvvForCard_thenIShowWarningTypeError() {
        
        //given
        sut.cardID = nil
        
        //when
        sut.getCvv()
        
        //then
        XCTAssert(sut.errorBO?.errorType == .warning)
    }
    
    func test_givenError204_whenICallGetCvvForCard_thenErrorMessageShouldBeEqualsToGenericMessage() {

        let errorBO = ErrorBO(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text))
        errorBO.status = ConstantsHTTPCodes.NO_CONTENT
        dummyInteractor.forceError = true
        sut.cardID = "005"
        
        //given
        dummyInteractor.errorBO = errorBO
        
        //when
        sut.getCvv()

        //then
        XCTAssert(sut.errorBO?.errorMessage() == Localizables.common.key_common_generic_error_text)
    }

    func test_givenAremainingTime_whenICallGetCVV_thenICallUpdateTimerWithAppropiateValue() {

        let dummyView = CVVDummyView()

        // given
        sut.remainingTime = 20
        sut.view = dummyView

        sut.cardID = "005"
        // when
        sut.getCvv()

        // then
        let minutes = Int(sut.remainingTime) / 60 % 60
        let second = Int(sut.remainingTime) % 60
        let valueExpected = String(format: "%02i:%02i", minutes, second)

        XCTAssert(dummyView.time == valueExpected)
    }
    
    func test_givenAValidCardId_whenICallGetCvv_thenICallProvideCVVCard() {
        
        //given
        sut.cardID = "005"
        
        //when
        sut.getCvv()
        
        //then
        XCTAssert(dummyInteractor.isCalledProvideCardCVV == true)
    }
    
    func test_givenAValidCardId_whenICallGetCvv_thenICallShowLoading() {

        //given
        sut.cardID = "005"
        
        //when
        sut.getCvv()

        //then
        XCTAssert(dummyBusManager.isCalledShowLoading == true)
    }

    func test_givenSuccesfulResponse_whenICallGetCvvForCard_thenICallHideLoading() {

        //given
        sut.cardID = "005"

        //when
        sut.getCvv()

        //then
        XCTAssert(dummyBusManager.isCalledHideLoading == true)
    }

    func test_givenAnError_whenICallGetCvvForCard_thenICallHideLoading() {
        
        sut.cardID = "005"
        dummyInteractor.forceError = true

        //given
        dummyInteractor.errorBO = ErrorBO()

        //when
        sut.getCvv()

        //then
        XCTAssert(dummyBusManager.isCalledHideLoading == true)
    }

    func test_givenAnError403_whenICallGetCvvForCard_thenICallExpiredSession() {
        
        let errorBO = ErrorBO()

        errorBO.status = ConstantsHTTPCodes.STATUS_403
        errorBO.code = ErrorCode.expiredSession.rawValue
        errorBO.isErrorShown = true

        dummyInteractor.forceError = true
        dummyInteractor.errorBO = errorBO

        //given
        sut.cardID = "005"

        //when
        sut.getCvv()

        //then
        XCTAssert(dummyBusManager.isCalledExpiredSession == true)
    }

    func test_givenASuccesfulResponseWithoutCardCvv_whenICallGetCvvForCard_thenICallShowError() {
        //given
        sut.cardID = nil
        //when
        sut.getCvv()

        //then
        XCTAssert(sut.view?.isCalledShowError == true)
    }
    
    func test_givenACardId_whenICallGetCvv_thenIDontCallReloadView() {
        //given
        sut.cardID = "005"
        
        //when
        sut.getCvv()

        //then
        XCTAssert(sut.view?.isCalledReloadView == false)
    }
    
    func test_givenACardId_whenIJustCallGetCVV_thenIsUpdatingCVVIsFalse() {
        //given
        sut.cardID = "005"
        
        //when
        sut.getCvv()
        
        //then
        XCTAssert(sut.isUpdatingCVV == false)
    }
    
    func test_givenIsUpdatingCVVIsTrue_whenAndICallGetCVV_thenIReloadView() {
        sut.cardID = "005"
        //given
        sut.isUpdatingCVV = true
        //when
        sut.getCvv()
        //then
        XCTAssert(sut.view?.isCalledReloadView == true)
    }
    
    // MARK: getNewCVV
    
    func test_givenASuccesfulResponseWhitoutCardCVV_whenICallGetNemCvv_thenErrorMessageShouldBeEqualsToGenericMessage() {
        
        //given
        sut.cardID = nil
        
        //when
        sut.getNewCvv()
        
        //then
        XCTAssert(sut.errorBO?.errorMessage() == Localizables.common.key_common_generic_error_text)
    }
    
    func test_givenASuccesfulResponseWithoutCardCvv_whenICallGetNewCvv_thenIShowWarningTypeError() {
        
        //given
        sut.cardID = nil
        
        //when
        sut.getNewCvv()
        
        //then
        XCTAssert(sut.errorBO?.errorType == .warning)
    }
    
    func test_givenError204_whenICallGetCvv_thenErrorMessageShouldBeEqualsToGenericMessage() {
        
        let errorBO = ErrorBO(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text))
        errorBO.status = ConstantsHTTPCodes.NO_CONTENT
        dummyInteractor.forceError = true
        sut.cardID = "005"
        
        //given
        dummyInteractor.errorBO = errorBO
        
        //when
        sut.getNewCvv()
        
        //then
        XCTAssert(sut.errorBO?.errorMessage() == Localizables.common.key_common_generic_error_text)
    }
    
    func test_givenACardID_whenICallGetNewCVV_thenIsUpdatingCVVIsSetToTrue() {
        //given
        sut.cardID = "005"
        //when
        sut.getNewCvv()
        //then
        XCTAssert(sut.isUpdatingCVV == true)
    }

    func test_givenCardId_whenICallGetNewCVV_thenICallShowLoading() {

        let dummyView = CVVDummyView()
        let dummyInteractor = CVVDummyInteractor()
        
        sut.view = dummyView
        sut.interactor = dummyInteractor

        //given
        sut.cardID = "005"

        // when
        sut.getNewCvv()
        // then
        XCTAssert(dummyBusManager.isCalledShowLoading == true)
    }

    func test_givenCardId_whenICallGetNewCVV_thenICallStartTimer() {

        let dummyView = CVVDummyView()
        let dummyInteractor = CVVDummyInteractor()

        sut.view = dummyView
        sut.interactor = dummyInteractor

        //given
        sut.cardID = "005"
        // when
        sut.getNewCvv()
        // then
        XCTAssert(dummyView.isCalledStartTimer == true)
    }

    func test_givenCardId_whenICallGetNewCVV_thenICallUpdateTimer() {
        let dummyView = CVVDummyView()
        let dummyInteractor = CVVDummyInteractor()

        sut.view = dummyView
        sut.interactor = dummyInteractor

        //given
        sut.cardID = "005"

        // when
        sut.getNewCvv()

        // then
        XCTAssert(dummyView.isCalledUpdateTimer == true)

    }

    func test_givenAnEmptyCardCVV_whenICallGetNewCvv_thenICallProceessGenericError() {
        let dummyInteractor = CVVDummyInteractor()
        let dummyView = CVVDummyView()

        dummyInteractor.forceNoCardCVVResponse = true

        sut.interactor = dummyInteractor
        sut.view = dummyView

        //given
        sut.cardID = "005"
        //when
        sut.getNewCvv()
        //then
        XCTAssert(dummyBusManager.isCalledHideLoading == true)
        XCTAssert(sut.view?.isCalledShowError == true)
    }

    func test_givenAnRemainigTimeEqualsZero_whenICallGetNewCVV_thenICallTheInteractor() {

        let dummyView = CVVDummyView()
        let dummyInteractor = CVVDummyInteractor()

        // given
        sut.view = dummyView
        sut.interactor = dummyInteractor
        sut.cardID  = "005"

        // when
        sut.getNewCvv()

        XCTAssert(dummyInteractor.isCalledProvideCardCVV == true)
    }

    func test_givenAnRemainigTimeEqualsZero_whenICallGetNewCVV_thenICallTheInteractorWithCardId() {

        let dummyView = CVVDummyView()
        let dummyInteractor = CVVDummyInteractor()

        // given
        sut.view = dummyView
        sut.interactor = dummyInteractor
        sut.cardID = "005"

        // when
        sut.getNewCvv()

        XCTAssert(dummyInteractor.cardId == sut.cardID)
    }

    func test_givenAnGetNewCVVRequest_whenICallTheInteractorAndResponseWithCVVCard_thenICallReloadView() {

        let dummyView = CVVDummyView()
        let dummyInteractor = CVVDummyInteractor()
        sut.view = dummyView
        sut.interactor = dummyInteractor

        // given
        sut.cardID = "005"
        // when
        sut.getNewCvv()
        //then
        XCTAssert(dummyView.isCalledReloadView == true)
    }

    func test_givenACard_whenICallTheInteractorAndResponseWithCVVCard_thenICallUpdateCVV() {

        // given
        sut.cardID = "005"

        // when
        sut.getNewCvv()
        //then
        XCTAssert(dummyView.isCalledUpdateCVV == true)
    }

    func test_givenACard_whenICallTheInteractorAndResponseWithCVVCard_thenICallUpdateTimer() {

        let dummyView = CVVDummyView()
        let dummyInteractor = CVVDummyInteractor()
        sut.view = dummyView
        sut.interactor = dummyInteractor

        // given
        sut.cardID = "005"

        // when
        sut.getNewCvv()
        //then
        XCTAssert(dummyView.isCalledUpdateTimer == true)
    }

    func test_givenACard_whenICallTheInteractorAndResponseWithCVVCard_thenICallUpdateTimerWithAppropiateCode() {

        // given
        sut.cardID = "005"

        // when
        sut.getNewCvv()
        //then
        XCTAssert(dummyView.value == dummyInteractor.cvvBO?.cardCVV?.code)
    }

    func test_givenACard_whenICallTheInteractorAndResponseWithCVVCard_thenICallStartTimer() {

        // given

        sut.cardID = "005"
        
        // when
        sut.getNewCvv()
        //then
        XCTAssert(dummyView.isCalledStartTimer == true)
    }

    func test_givenACard_whenICallTheInteractorAndResponseWithTsecError_thenICallShowError() {

        let dummyView = CVVDummyView()
        let dummyInteractor = CVVDummyInteractor()

        // given

        dummyInteractor.isRequestedResponseWithCVVCard = false
        dummyInteractor.isRequestedCorrectResponse = false
        dummyInteractor.isRequestedResponseWithTSECError = true

        dummyView.presenter = sut

        sut.view = dummyView
        sut.interactor = dummyInteractor
        sut.cardID = "005"

        // when
        sut.getNewCvv()

        // then
        XCTAssert(dummyView.isCalledShowError == true)
        XCTAssert(dummyInteractor.isCalledProvideCardCVV == true)
        XCTAssert(sut.errorBO?.status == ConstantsHTTPCodes.STATUS_403)

    }

    func test_givenACardWithNoCVVID_WhenICallGetNewCVV_thenISetTheErrorBO() {
        let dummyView = CVVDummyView()
        sut.view = dummyView
        //given
        sut.cardID = nil
        //when
        sut.getNewCvv()
        //then
        XCTAssert(sut.errorBO?.errorMessage() == Localizables.common.key_common_generic_error_text)
    }

    func test_givenAcardWithInvalidSesion_WhenICallGetNewCVV_thenIExpireTheSession() {
        let dummyView = CVVDummyView()
        sut.view = dummyView
        //given
        sut.cardID = nil
        //when
        sut.getNewCvv()

    }

    func test_givenAValidCard_WhenICallGetNewCVV_ThenICallProvideCVV() {
        let dummyInteractor = CVVDummyInteractor()
        sut.interactor = dummyInteractor
        //given
        sut.cardID = "005"
        //when
        sut.getNewCvv()
        //then
        XCTAssert(dummyInteractor.isCalledProvideCardCVV == true)
    }

    // MARK: updateTimer

    func test_givenZeroTimeLeft_WhenIcallUpdateTimer_ThenIShouldCallFinishTimer() {
        let dummyView = CVVDummyView()
        sut.view = dummyView
        //given
        sut.remainingTime = 0
        //when
        sut.updateTimer()
        //then
        XCTAssert(dummyView.isCalledFinishTimer == true)
    }

    func test_givenCorrectCVVCard_whenICallUpdateTimerAndRemainingTimeIsEqualsToAlertTime_thenICallUpdateView() {

        let dummyView = CVVDummyView()

        // given
        sut.view = dummyView
        sut.cardID = "005"

        // when
        sut.remainingTime = sut.alertTime + 1

        sut.updateTimer()

        XCTAssert(dummyView.isCalledUpdateView == true)
    }

    func test_givenCorrectCVVCard_whenICallUpdateTimerAndRemainingTimeIsEqualsToZero_thenICallFinishTimer() {

        let dummyView = CVVDummyView()

        // given
        sut.view = dummyView
        sut.cardID = "005"

        // when
        sut.remainingTime = 0
        sut.updateTimer()

        XCTAssert(dummyView.isCalledFinishTimer == true)
    }

    // MARK: closeView
    
    func test_givenNewCVV_whenICallCloseViewIsCalled_thenICallBack() {
        //given
        sut.isUpdatingCVV = false
        //when
        sut.closeView()
        //then
        XCTAssert(dummyBusManager.isCalledBack == true)
    }

    func test_givenUpdatedCVV_whenICallCloseViewIsCalled_thenIDontCallBack() {
        //given
        sut.isUpdatingCVV = true
        //when
        sut.closeView()
        //then
        XCTAssert(dummyBusManager.isCalledBack == false)
    }
    
    // MARK: accptErrorModal
    
    func test_givenAnErrorAndIsUpdatingIsFalse_whenIAcceptErrorModal_thenICloseView() {
        //given
        sut.isUpdatingCVV = false
        sut.errorBO = ErrorBO()
        //when
        sut.acceptErrorModal()
        //then
        XCTAssert(dummyBusManager.isCalledBack == true)
    }
    
    func test_givenAnErrorAndIsUpdatingIsTrue_whenIAcceptErrorModal_thenIDontCloseView() {
        //given
        sut.isUpdatingCVV = true
        sut.errorBO = ErrorBO()
        //when
        sut.acceptErrorModal()
        //then
        XCTAssert(dummyBusManager.isCalledBack == false)
    }
    
    func test_givenAnErrorWithSessionExpired_whenIAcceptErrorModal_thenICallBack() {
        //given
        let errorBO = ErrorBO()
        errorBO.status = ConstantsHTTPCodes.STATUS_403
        errorBO.code = ErrorCode.expiredSession.rawValue
        sut.errorBO = errorBO
        //when
        sut.acceptErrorModal()
        //then
        XCTAssert(dummyBusManager.isCalledBack == true)
    }
    
    func test_givenAnErrorWithSessionExpired_whenIAcceptErrorModal_thenIExpireSession() {
        //given
        let errorBO = ErrorBO()
        errorBO.status = ConstantsHTTPCodes.STATUS_403
        errorBO.code = ErrorCode.expiredSession.rawValue
        sut.errorBO = errorBO
        //then
        sut.acceptErrorModal()
        //when
        XCTAssert(dummyBusManager.isSetRootWhenNoSessionCalled == true)
    }
    
}

class DummyBusManager: BusManager {
    var isCalledExpiredSession = false
    var isCalledShowLoading = false
    var isCalledHideLoading = false
    var isCalledDismissViewController = false
    var isCalledBack = false
    var isSetRootWhenNoSessionCalled = false

    override init() {
        super.init()
        routerFactory = RouterInitFactory()
    }

    override func expiredSession() {
        isCalledExpiredSession = true
    }

    override func showLoading(completion: (() -> Void)?) {
        isCalledShowLoading = true
    }

    override  func hideLoading(completion: (() -> Void)? ) {
        if let completion = completion {
            completion()
        }
        isCalledHideLoading = true
    }

    override func back() {
        isCalledBack = true
    }
    
    override func dismissViewController(animated: Bool, completion: (() -> Void)?) {
        isCalledDismissViewController = true
    }
    
    override func setRootWhenNoSession() {
        isSetRootWhenNoSessionCalled = true
    }
    
}

class CVVDummyView: CVVViewProtocol {
    
    var isCalledUpdateCVV: Bool = false
    var isCalledStartTimer: Bool = false
    var isCalledUpdateTimer: Bool = false
    var isCalledFinishTimer: Bool = false
    var isCalledUpdateView: Bool = false
    var isCalledShowError: Bool = false
    var isCalledReloadView: Bool = false
    var isCalledDissmisLoadingView: Bool = false
    var isCalledupdateImageTimer: Bool = false

    var presenter: CVVPresenterProtocol!

    var value: String?
    var time: String?
    
    func updateCVV(withValue cvv: String) {
        isCalledUpdateCVV = true

        value = cvv
    }

    func startTimer() {
        isCalledStartTimer = true
    }

    func updateTimer(withText text: String) {
        isCalledUpdateTimer = true
        time = text
    }

    func finishTimer() {
        isCalledFinishTimer = true
    }

    func updateView() {
        isCalledUpdateView = true
    }

    func reloadView() {
        isCalledReloadView = true
    }

    func showError(error: ModelBO) {
        isCalledShowError = true
    }

    func changeColorEditTexts() {

    }
}

class ResponseServiceDummy {

    func requestCvvCard() -> CVVBO {

        let cvvBO: CVVBO

        var cvvEntity: SecurityDataEntity = SecurityDataEntity()

        cvvEntity.id = Settings.CardCvv.cvvId
        cvvEntity.name = "Card Verification Value"
        cvvEntity.code = "123"

        let data: [SecurityDataEntity] = [cvvEntity]

        let cardCvvDummy: CVVEntity = CVVEntity()

        cardCvvDummy.data = data

        cvvBO = CVVBO(cvvEntity: cardCvvDummy)

        cvvBO.cardId = "005"

        return cvvBO
    }

    func requestCvvCard() -> CVVEntity {

        var cvvEntity: SecurityDataEntity = SecurityDataEntity()

        cvvEntity.id = Settings.CardCvv.cvvId
        cvvEntity.name = "Card Verification Value"
        cvvEntity.code = "1234567"

        let data: [SecurityDataEntity] = [cvvEntity]

        let cardCvvDummy: CVVEntity = CVVEntity()

        cardCvvDummy.data = data

        return cardCvvDummy
    }

    func requestWithoutCvvCard() -> CVVEntity {

        var cvvEntity: SecurityDataEntity = SecurityDataEntity()

        cvvEntity.id = "PIN"
        cvvEntity.name = "Personal Identification Number"
        cvvEntity.code = "1234"

        let data: [SecurityDataEntity] = [cvvEntity]

        let cardCvvDummy: CVVEntity = CVVEntity()

        cardCvvDummy.data = data

        return cardCvvDummy
    }

}

class CVVDummyInteractor: CVVInteractorProtocol, InteractorProtocol {

    var isCalledProvideCardCVV: Bool = false
    var isRequestedCorrectResponse: Bool = true
    var isRequestedResponseWithCVVCard: Bool = true
    var isRequestedResponseWithTSECError: Bool = false

    var forceNoCardCVVResponse = false

    var forceError = false

    var cardCvvDummy: CVVEntity = CVVEntity()
    var cardId: String?

    var errorBO: ErrorBO?
    var cvvBO: CVVBO?

    var serviceResponseDummy = ResponseServiceDummy()

    func provideCVVForCard(withID cardId: String) -> Observable<ModelBO> {

        self.cardId = cardId
        isCalledProvideCardCVV = true

        if forceNoCardCVVResponse == true {

            return Observable <ModelBO>.just(CVVBO(cvvEntity: CVVEntity()))
        }

        if forceError == true {
            return Observable.error(ServiceError.GenericErrorBO(error: errorBO!))
        }

        if isRequestedCorrectResponse {

            if isRequestedResponseWithCVVCard {

                cardCvvDummy = serviceResponseDummy.requestCvvCard()

            } else {

                cardCvvDummy = serviceResponseDummy.requestWithoutCvvCard()

            }

            cvvBO = ResponseServiceDummy().requestCvvCard()

            guard cvvBO?.cardCVV != nil && cvvBO?.cardCVV?.code != "" else {

                let errorBO = ErrorBO(error: ErrorEntity(message: Localizables.common.key_common_generic_error_text))

                return Observable.error(ServiceError.GenericErrorBO(error: errorBO))

            }

            return Observable <ModelBO>.just(cvvBO!)

        } else if isRequestedResponseWithTSECError {

            errorBO = CardsGenerator.getTsecError()
            errorBO!.isErrorShown = false

            return Observable.error(ServiceError.GenericErrorBO(error: errorBO!))
        } else {

            let entity = ErrorEntity(message: "Error")
            entity.code = "80"
            entity.httpStatus = 404
            errorBO!.isErrorShown = false

            return Observable.error(ServiceError.GenericErrorBO(error: errorBO!))

        }

    }

}
