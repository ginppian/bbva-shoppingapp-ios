//
//  CVVPresenterATest.swift
//  shoppingappMXTest
//
//  Created by Armando Vazquez on 8/13/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest
import RxSwift

#if TESTMX
@testable import shoppingappMX
#endif

class CVVPresenterATest: XCTestCase {

    var sut: CVVPresenterA<CVVDummyViewA>!
    var dummyBusManager: DummyBusManager!
    var serviceResponseDummy: ResponseServiceDummy!
    var dummyInteractor: CVVDummyInteractor!
    var dummyView: CVVDummyViewA!

    override func setUp() {

        super.setUp()

        dummyView = CVVDummyViewA()
        dummyBusManager = DummyBusManager()
        sut = CVVPresenterA<CVVDummyViewA>()
        dummyInteractor = CVVDummyInteractor()
        serviceResponseDummy = ResponseServiceDummy()

        sut.view = dummyView
        sut.interactor = dummyInteractor
        sut.busManager = dummyBusManager
        
    }
    
    // MARK: getCVV
    
    func test_givenAValidCardId_whenICallGetCvv_thenICallShowLoadingPoints() {
        //given
        sut.cardID = "005"
        
        //when
        sut.getCvv()
        
        //then
        XCTAssert(sut.view?.addLoadingDotsIsCalled == true)
    }
    
    func test_givenAValidCardId_whenICallGetCvv_thenICallRemoveLoadingDots() {
        //given
        sut.cardID = "005"
        
        //when
        sut.getCvv()
        
        //then
        XCTAssert(sut.view?.removeLoadignDotsIsCalled == true)
    }
    
    func test_givenAnError_whenICallGetCvv_thenIDoNotCallRemoveLoadingDots() {
        //given
        dummyInteractor.forceError = true
        
        //when
        sut.getCvv()
        
        //then
        XCTAssert(sut.view?.removeLoadignDotsIsCalled == false)
    }
    
    func test_givenAnError_whenICallGetCvv_thenICallLoadingDots() {
        //given
        dummyInteractor.forceError = true
        
        //when
        sut.getCvv()
        
        //then
        XCTAssert(sut.view?.addLoadingDotsIsCalled == true)
    }
    
    // MARK: getNewCVV
    
    func test_givenAValidCardId_whenICallGetNewCvv_thenICallShowLoadingPoints() {
        //given
        sut.cardID = "005"
        
        //when
        sut.getNewCvv()
        
        //then
        XCTAssert(sut.view?.addLoadingDotsIsCalled == true)
    }
    
    func test_givenAValidCardId_whenICallGetNewCvv_thenICallRemoveLoadingDots() {
        //given
        sut.cardID = "005"
        
        //when
        sut.getNewCvv()
        
        //then
        XCTAssert(sut.view?.removeLoadignDotsIsCalled == true)
    }
    
    func test_givenAnError_whenICallGetNewCvv_thenIDoNotCallRemoveLoadingDots() {
        //given
        dummyInteractor.forceError = true
        
        //when
        sut.getNewCvv()
        
        //then
        XCTAssert(sut.view?.removeLoadignDotsIsCalled == false)
    }
    
    func test_givenAnError_whenICallGetNewCvv_thenICallLoadingDots() {
        //given
        dummyInteractor.forceError = true
        
        //when
        sut.getNewCvv()
        
        //then
        XCTAssert(sut.view?.addLoadingDotsIsCalled == true)
    }
    
    // MARK: accptErrorModal
    
    func test_givenAnErrorAndIsUpdatingIsFalse_whenIAcceptErrorModal_thenICallDismissViewConroller() {
        //given
        sut.isUpdatingCVV = false
        sut.errorBO = ErrorBO()
        //when
        sut.acceptErrorModal()
        //then
        XCTAssert(dummyBusManager.isCalledDismissViewController == true)
    }
    
    func test_givenAnErrorAndIsUpdatingIsTrue_whenIAcceptErrorModal_thenIDontCloseView() {
        //given
        sut.isUpdatingCVV = true
        sut.errorBO = ErrorBO()
        //when
        sut.acceptErrorModal()
        //then
        XCTAssert(dummyBusManager.isCalledDismissViewController == false)
    }
    
    func test_givenAnErrorWithSessionExpired_whenIAcceptErrorModal_thenICallSetRootWhenNoSessionCalled() {
        //given
        let errorBO = ErrorBO()
        errorBO.status = ConstantsHTTPCodes.STATUS_403
        errorBO.code = ErrorCode.expiredSession.rawValue
        sut.errorBO = errorBO
        //when
        sut.acceptErrorModal()
        //then
        XCTAssert(dummyBusManager.isSetRootWhenNoSessionCalled == true)
    }
    
    func test_givenAnErrorWithSessionExpired_whenIAcceptErrorModal_thenICallDismissViewcontroller() {
        //given
        let errorBO = ErrorBO()
        errorBO.status = ConstantsHTTPCodes.STATUS_403
        errorBO.code = ErrorCode.expiredSession.rawValue
        sut.errorBO = errorBO
        //when
        sut.acceptErrorModal()
        //then
        XCTAssert(dummyBusManager.isSetRootWhenNoSessionCalled == true)
    }
    
    func test_givenAnErrorWithSessionExpired_whenIAcceptErrorModal_thenIExpireSession() {
        //given
        let errorBO = ErrorBO()
        errorBO.status = ConstantsHTTPCodes.STATUS_403
        errorBO.code = ErrorCode.expiredSession.rawValue
        sut.errorBO = errorBO
        //then
        sut.acceptErrorModal()
        //when
        XCTAssert(dummyBusManager.isSetRootWhenNoSessionCalled == true)
    }
    
    // MARK: getCVVTime

    func test_givenAnyTime_whenICallGetCvvTime_thenICallUpdateTimer() {
        //given
        sut.timeForTimerInSeconds = 300
        //when
        sut.getCVVTime()
        //given
        XCTAssert(sut.view?.isCalledUpdateTimer == true)
    }
    
    func test_givenAnyTime_whenICallGetCvvTime_thenIGetFormatedText() {
        //given
        sut.timeForTimerInSeconds = 300
        //when
        sut.getCVVTime()
        //given
        XCTAssert(sut.view?.formatedText == "05:00")
    }

    // MARK: closeView
    
    func test_givenNewCVV_whenICallCloseViewIsCalled_ThenICallDismiss() {
        //given
        sut.isUpdatingCVV = false
        //when
        sut.closeView()
        //then
        XCTAssert(dummyBusManager.isCalledDismissViewController == true)
    }

    func test_givenUpdatedCVV_whenICallCloseViewIsCalled_ThenIDontCallDismiss() {
        //given
        sut.isUpdatingCVV = true
        //when
        sut.closeView()
        //then
        XCTAssert(dummyBusManager.isCalledDismissViewController == false)
    }

}

class CVVDummyViewA: CVVDummyView, CVVViewProtocolA {
    var addLoadingDotsIsCalled = false
    var removeLoadignDotsIsCalled = false
    var formatedText = ""
    
    override func updateTimer(withText text: String) {
        isCalledUpdateTimer = true
        formatedText = text
    }

    func addLoadingDots() {
        addLoadingDotsIsCalled = true
    }
    
    func removeLoadingDots() {
        removeLoadignDotsIsCalled = true
    }
}
