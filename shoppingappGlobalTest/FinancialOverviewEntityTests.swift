//
//  FinancialOverviewEntityTests.swift
//  shoppingapp
//
//  Created by jesus.martinez on 17/7/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
@testable import shoppingappMX
#elseif TESTPE
@testable import shoppingappPE
#endif

class FinancialOverviewEntityTests: XCTestCase {

    // MARK: - applyContractRelation

    func test_givenAny_whenICallApplyContractRelation_thenAnyrelatedContractsEntityShouldNotBeNil() {

        // given
        let sut = FinancialOverviewEntityGenerator.financialOverview()

        // when
        sut.applyContractRelation()

        // then
        for contract in sut.data!.contracts! {
            XCTAssert(contract.relatedContractsEntity != nil)
        }
    }

    func test_givenAny_whenICallApplyContractRelation_thenrelatedContractsEntityShouldHaveACorrectRelation() {

        // given
        let sut = FinancialOverviewEntityGenerator.financialOverview()

        // when
        sut.applyContractRelation()

        // then
        XCTAssert(sut.data!.contracts![11].relatedContractsEntity!.count == 2)

        XCTAssert(sut.data!.contracts![11].relatedContractsEntity![0].relatedContractId == "1039485357")
        XCTAssert(sut.data!.contracts![11].relatedContractsEntity![0].contractId == "8QJZ9PD6PW8M7JDSGCSER9RE9ZVCP8AQWZEZE33AATJMM620R8Z0")
        XCTAssert(sut.data!.contracts![11].relatedContractsEntity![0].number == "4815160009396192")
        XCTAssert(sut.data!.contracts![11].relatedContractsEntity![0].numberType?.id == "PAN")
        XCTAssert(sut.data!.contracts![11].relatedContractsEntity![0].product?.id == "CARDS")
        XCTAssert(sut.data!.contracts![11].relatedContractsEntity![0].relationType?.id == "LINKED_WITH")

        XCTAssert(sut.data!.contracts![11].relatedContractsEntity![1].relatedContractId == "1039485357")
        XCTAssert(sut.data!.contracts![11].relatedContractsEntity![1].contractId == "RYNKN9XY5XV5V3KN52CAJECW01Q4KGG6PMD6DDRAACP2YMK2MMD0")
        XCTAssert(sut.data!.contracts![11].relatedContractsEntity![1].number == "4172123000009991")
        XCTAssert(sut.data!.contracts![11].relatedContractsEntity![0].numberType?.id == "PAN")
        XCTAssert(sut.data!.contracts![11].relatedContractsEntity![0].product?.id == "CARDS")
        XCTAssert(sut.data!.contracts![11].relatedContractsEntity![0].relationType?.id == "LINKED_WITH")

        XCTAssert(sut.data!.contracts![12].relatedContractsEntity!.count == 1)

        XCTAssert(sut.data!.contracts![12].relatedContractsEntity![0].relatedContractId == "1039485357")
        XCTAssert(sut.data!.contracts![12].relatedContractsEntity![0].contractId == "Z8P0E6Q107GY0A4VWC0Y84CAX7TSC6GJR4SE86PSNK6P3ESEE5RG")
        XCTAssert(sut.data!.contracts![12].relatedContractsEntity![0].number == "4152313302204560")
        XCTAssert(sut.data!.contracts![12].relatedContractsEntity![0].numberType?.id == "PAN")
        XCTAssert(sut.data!.contracts![12].relatedContractsEntity![0].product?.id == "CARDS")
        XCTAssert(sut.data!.contracts![12].relatedContractsEntity![0].relationType?.id == "LINKED_WITH")

        XCTAssert(sut.data!.contracts![13].relatedContractsEntity!.count == 1)

        XCTAssert(sut.data!.contracts![13].relatedContractsEntity![0].relatedContractId == "1039485357")
        XCTAssert(sut.data!.contracts![13].relatedContractsEntity![0].contractId == "Z8P0E6Q107GY0A4VWC0Y84CAX7TSC6GJR4SE86PSNK6P3ESEE5RG")
        XCTAssert(sut.data!.contracts![13].relatedContractsEntity![0].number == "4152313302204560")
        XCTAssert(sut.data!.contracts![13].relatedContractsEntity![0].numberType?.id == "PAN")
        XCTAssert(sut.data!.contracts![13].relatedContractsEntity![0].product?.id == "CARDS")
        XCTAssert(sut.data!.contracts![13].relatedContractsEntity![0].relationType?.id == "LINKED_WITH")

        XCTAssert(sut.data!.contracts![14].relatedContractsEntity!.isEmpty)
    }

    func test_givenFinancialOverviewComplete_whenICallApplyContractRelation_thenrelatedContractsEntityShouldHaveACorrectRelation() {

        // given
        let sut = FinancialOverviewEntityGenerator.financialOverviewComplete()

        // when
        sut.applyContractRelation()

        // then
        XCTAssert(sut.data!.contracts![0].relatedContractsEntity!.isEmpty)

        XCTAssert(sut.data!.contracts![1].relatedContractsEntity!.isEmpty)

        XCTAssert(sut.data!.contracts![2].relatedContractsEntity!.count == 2)

        XCTAssert(sut.data!.contracts![2].relatedContractsEntity![0].relatedContractId == "8016")
        XCTAssert(sut.data!.contracts![2].relatedContractsEntity![0].contractId == "0018")
        XCTAssert(sut.data!.contracts![2].relatedContractsEntity![0].number == "4555457083070018")
        XCTAssert(sut.data!.contracts![2].relatedContractsEntity![0].numberType?.id == "PAN")
        XCTAssert(sut.data!.contracts![2].relatedContractsEntity![0].product?.id == "CARDS")
        XCTAssert(sut.data!.contracts![2].relatedContractsEntity![0].relationType?.id == "LINKED_WITH")

        XCTAssert(sut.data!.contracts![2].relatedContractsEntity![1].relatedContractId == "8016")
        XCTAssert(sut.data!.contracts![2].relatedContractsEntity![1].contractId == "0019")
        XCTAssert(sut.data!.contracts![2].relatedContractsEntity![1].number == "4555457083070019")
        XCTAssert(sut.data!.contracts![2].relatedContractsEntity![0].numberType?.id == "PAN")
        XCTAssert(sut.data!.contracts![2].relatedContractsEntity![0].product?.id == "CARDS")
        XCTAssert(sut.data!.contracts![2].relatedContractsEntity![0].relationType?.id == "LINKED_WITH")

        XCTAssert(sut.data!.contracts![3].relatedContractsEntity!.count == 1)

        XCTAssert(sut.data!.contracts![3].relatedContractsEntity![0].relatedContractId == "8017")
        XCTAssert(sut.data!.contracts![3].relatedContractsEntity![0].contractId == "0020")
        XCTAssert(sut.data!.contracts![3].relatedContractsEntity![0].number == "4555457083070020")
        XCTAssert(sut.data!.contracts![3].relatedContractsEntity![0].numberType?.id == "PAN")
        XCTAssert(sut.data!.contracts![3].relatedContractsEntity![0].product?.id == "CARDS")
        XCTAssert(sut.data!.contracts![3].relatedContractsEntity![0].relationType?.id == "LINKED_WITH")

        XCTAssert(sut.data!.contracts![4].relatedContractsEntity!.count == 1)

        XCTAssert(sut.data!.contracts![4].relatedContractsEntity![0].relatedContractId == "8016")
        XCTAssert(sut.data!.contracts![4].relatedContractsEntity![0].contractId == "0016")
        XCTAssert(sut.data!.contracts![4].relatedContractsEntity![0].number == "4555457083070016")
        XCTAssert(sut.data!.contracts![4].relatedContractsEntity![0].numberType?.id == "PAN")
        XCTAssert(sut.data!.contracts![4].relatedContractsEntity![0].product?.id == "CARDS")
        XCTAssert(sut.data!.contracts![4].relatedContractsEntity![0].relationType?.id == "LINKED_WITH")

        XCTAssert(sut.data!.contracts![5].relatedContractsEntity!.count == 1)

        XCTAssert(sut.data!.contracts![5].relatedContractsEntity![0].relatedContractId == "8016")
        XCTAssert(sut.data!.contracts![5].relatedContractsEntity![0].contractId == "0016")
        XCTAssert(sut.data!.contracts![5].relatedContractsEntity![0].number == "4555457083070016")
        XCTAssert(sut.data!.contracts![5].relatedContractsEntity![0].numberType?.id == "PAN")
        XCTAssert(sut.data!.contracts![5].relatedContractsEntity![0].product?.id == "CARDS")
        XCTAssert(sut.data!.contracts![5].relatedContractsEntity![0].relationType?.id == "LINKED_WITH")

        XCTAssert(sut.data!.contracts![6].relatedContractsEntity!.count == 1)

        XCTAssert(sut.data!.contracts![6].relatedContractsEntity![0].relatedContractId == "8017")
        XCTAssert(sut.data!.contracts![6].relatedContractsEntity![0].contractId == "0017")
        XCTAssert(sut.data!.contracts![6].relatedContractsEntity![0].number == "4555457083070017")
        XCTAssert(sut.data!.contracts![6].relatedContractsEntity![0].numberType?.id == "PAN")
        XCTAssert(sut.data!.contracts![6].relatedContractsEntity![0].product?.id == "CARDS")
        XCTAssert(sut.data!.contracts![6].relatedContractsEntity![0].relationType?.id == "LINKED_WITH")

        XCTAssert(sut.data!.contracts![7].relatedContractsEntity!.count == 1)

        XCTAssert(sut.data!.contracts![7].relatedContractsEntity![0].relatedContractId == "8021")
        XCTAssert(sut.data!.contracts![7].relatedContractsEntity![0].contractId == "0024")
        XCTAssert(sut.data!.contracts![7].relatedContractsEntity![0].number == "4555457083070024")
        XCTAssert(sut.data!.contracts![7].relatedContractsEntity![0].numberType?.id == "PAN")
        XCTAssert(sut.data!.contracts![7].relatedContractsEntity![0].product?.id == "CARDS")
        XCTAssert(sut.data!.contracts![7].relatedContractsEntity![0].relationType?.id == "LINKED_WITH")

        XCTAssert(sut.data!.contracts![8].relatedContractsEntity!.isEmpty)

        XCTAssert(sut.data!.contracts![9].relatedContractsEntity!.isEmpty)

        XCTAssert(sut.data!.contracts![10].relatedContractsEntity!.count == 1)

        XCTAssert(sut.data!.contracts![10].relatedContractsEntity![0].relatedContractId == "8021")
        XCTAssert(sut.data!.contracts![10].relatedContractsEntity![0].contractId == "0021")
        XCTAssert(sut.data!.contracts![10].relatedContractsEntity![0].number == "4555457083070021")
        XCTAssert(sut.data!.contracts![10].relatedContractsEntity![0].numberType?.id == "PAN")
        XCTAssert(sut.data!.contracts![10].relatedContractsEntity![0].product?.id == "CARDS")
        XCTAssert(sut.data!.contracts![10].relatedContractsEntity![0].relationType?.id == "LINKED_WITH")

        XCTAssert(sut.data!.contracts![11].relatedContractsEntity!.isEmpty)
    }

}
