//
//  OnOffHelpPrestenterATest.swift
//  shoppingappMXTest
//
//  Created by Armando Vazquez on 11/20/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest
import RxSwift
import CellsNativeComponents

@testable import shoppingappMX

class OnOffHelpPresenterATests: XCTestCase {
    
    var sut: OnOffHelpPresenterA<OnOffHelpViewDummy>!
    var dummyBusManager: OnOffHelpDummyBusManagerA!
    var dummyPreferencesManager: DummyPreferencesManager!

    override func setUp() {
        super.setUp()
        dummyBusManager = OnOffHelpDummyBusManagerA()
        sut = OnOffHelpPresenterA<OnOffHelpViewDummy>(busManager: dummyBusManager)
        DummyPreferencesManager.configureDummyPreferences()
        dummyPreferencesManager = PreferencesManager.instance as? DummyPreferencesManager

    }
    
    // MARK: - setModel
    
    func test_givenCardsTransportFlagTrue_whenICallSetModel_thenIsCalledFromSwitchCardUpIsTrue() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        let cardsTransport = CardsTransport(cardBO: cardBO)
        cardsTransport.flagNavigation = true
        
        // when
        sut.setModel(model: cardsTransport)
        
        // then
        XCTAssert(sut.isCalledFromSwitchCardUp == true)
        
    }
    
    // MARK: - viewLoaded
    
    func test_givenAnyCardAndANavigationFlag_whenICallViewLoaded_thenICallShowHelpInformation() {
        
        let dummyview = OnOffHelpViewDummy()
        sut.view = dummyview
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.isCalledFromSwitchCardUp = false
    
        // given
        sut.cardBO = cardBO
        
        // when
        sut.viewLoaded()
        
        // then
        XCTAssert(dummyview.isCalledShowHelpInformation == true)
    }
    
    func test_givenOnOffHelpIsNotCalledFromSwitch_whenICallViewLoaded_thenAcceptButtonMatchesAcceptLocale() {
        sut.cardBO = CardsGenerator.getCardBOWithAlias().cards.first
        // given
        sut.isCalledFromSwitchCardUp = false
        
        // when
        sut.viewLoaded()
        
        // then
        XCTAssert(sut.displayData!.acceptButtonTitle == Localizables.common.key_common_accept_text)
    }
    
    func test_givenOnOffHelpIsCalledFromSwitchCard_whenICallViewLoaded_thenAcceptButtonMatchesOnText() {
        let card = CardsGenerator.getCardBOWithAlias().cards.first
        sut.cardBO = card
        let key = Settings.CardHelpOnOff.localizationKeyForHelpOnOffAndCard(card: card)
        let acceptText = NSLocalizedString((key + "ACCEPT"), comment: "")
        
        // given
        sut.isCalledFromSwitchCardUp = true

        // when
        sut.viewLoaded()
        
        // then
        XCTAssert(sut.displayData!.acceptButtonTitle == acceptText)
    }
    
    func test_givenOnOffHelpIsCalledFromSwitchCardAndOffCard_whenICallViewLoaded_thenAcceptButtonMatchesOnText() {
        
        let card = CardsGenerator.getCardBOWithAlias().cards.first
        sut.cardBO = card
        sut.cardBO?.activations.removeAll()
        let key = Settings.CardHelpOnOff.localizationKeyForHelpOnOffAndCard(card: card)
        let acceptText = NSLocalizedString((key + "ACCEPT"), comment: "")
        // given
        sut.isCalledFromSwitchCardUp = true
        
        // when
        sut.viewLoaded()
        
        // then
        XCTAssert(sut.displayData!.acceptButtonTitle == acceptText)
    }
    
    func test_givenAnOnCard_whenICallViewDidLoad_thenTitlesShouldMatch() {
        sut.isCalledFromSwitchCardUp = true

        // given
        let card = CardsGenerator.getCardBOWithAlias().cards.first
        sut.cardBO = card
        let rootKey = Settings.CardHelpOnOff.localizationKeyForHelpOnOffAndCard(card: card)
        
        // when
        sut.viewLoaded()
        
        let titleOneKey = rootKey + "TITLE_1"
        let titleFirst = NSLocalizedString(titleOneKey, comment: "")
        let titleTwoKey = rootKey + "TITLE_2"
        let titleSecond = NSLocalizedString(titleTwoKey, comment: "")
        let titleThreeKey = rootKey + "TITLE_3"
        let titleThird = NSLocalizedString(titleThreeKey, comment: "")

        let screenTitleKey = rootKey + "TITLE"
        let screenTitle = NSLocalizedString(screenTitleKey, comment: "")
        let disclaimerKey = rootKey + "DISCLAIMER"
        let disclaimer = NSLocalizedString(disclaimerKey, comment: "")
        let acceptKey = rootKey + "ACCEPT"
        let acceptText = NSLocalizedString(acceptKey, comment: "")

        // then
        XCTAssert(sut.displayData?.titleOne == titleFirst)
        XCTAssert(sut.displayData?.titleTwo == titleSecond)
        XCTAssert(sut.displayData?.titleThree == titleThird)
        XCTAssert(sut.displayData?.acceptButtonTitle == acceptText)
        XCTAssert(sut.displayData?.screenTitleText == screenTitle)
        XCTAssert(sut.displayData?.disclaimerText == disclaimer)
        
    }
    
    func test_givenAnOffCard_whenICallViewDidLoad_thenTitlesShouldMatch() {
        sut.isCalledFromSwitchCardUp = true

        // given
        let card = CardsGenerator.getCardBOWithAlias().cards.first
        card?.activations.removeAll()
        sut.cardBO = card
        let rootKey = Settings.CardHelpOnOff.localizationKeyForHelpOnOffAndCard(card: card)
        
        // when
        sut.viewLoaded()
        
        let titleOneKey = rootKey + "TITLE_1"
        let titleFirst = NSLocalizedString(titleOneKey, comment: "")
        let titleTwoKey = rootKey + "TITLE_2"
        let titleSecond = NSLocalizedString(titleTwoKey, comment: "")
        let titleThreeKey = rootKey + "TITLE_3"
        let titleThird = NSLocalizedString(titleThreeKey, comment: "")
        
        let screenTitleKey = rootKey + "TITLE"
        let screenTitle = NSLocalizedString(screenTitleKey, comment: "")
        let disclaimerKey = rootKey + "DISCLAIMER"
        let disclaimer = NSLocalizedString(disclaimerKey, comment: "")
        let acceptKey = rootKey + "ACCEPT"
        let acceptText = NSLocalizedString(acceptKey, comment: "")

        // then
        XCTAssert(sut.displayData?.titleOne == titleFirst)
        XCTAssert(sut.displayData?.titleTwo == titleSecond)
        XCTAssert(sut.displayData?.titleThree == titleThird)
        XCTAssert(sut.displayData?.acceptButtonTitle == acceptText)
        XCTAssert(sut.displayData?.screenTitleText == screenTitle)
        XCTAssert(sut.displayData?.disclaimerText == disclaimer)
    }
 
    // MARK: - acceptButtonPressed
    
    func test_giveAny_whenICallAcceptButtonPressed_thenICallDismissViewController() {
        
        // given
        
        // when
        sut.acceptButtonPressed()
        
        // then
        XCTAssert(dummyBusManager.isCalledDismissAction == true)
    }
    
    func test_givenACardBOAndIsCalledFromSwitchCardUpIsTrue_whenICallAcceptButtonPressed_thenICallSwitchCardUpOnSuccess() {
        
        // given
        sut.isCalledFromSwitchCardUp = true
        let cardBO = CardsGenerator.getCardBOWithActivationOff().cards.first
        sut.cardBO = cardBO
        
        //when
        sut.acceptButtonPressed()
        
        //then
        XCTAssert(dummyBusManager.isCalledSwitchUpSuccess == true)
    }
    
    func test_givenACardBOWithActivationOffAndIsCalledFromSwitchCardUpIsTrue_whenICallAcceptButtonPressed_thenICallPreferencesManagerSaveValue() {
        
        // given
        sut.isCalledFromSwitchCardUp = true
        let cardBO = CardsGenerator.getCardBOWithActivationOff().cards.first
        sut.cardBO = cardBO

        //when
        sut.acceptButtonPressed()
        
        //then
        XCTAssert(dummyPreferencesManager.isCalledSaveValueWithPreferencesKey == true)
    }
    
    func test_givenACardBOWithActivationOffAndIsCalledFromSwitchCardUpIsTrue_whenICallAcceptButtonPressed_thenICallPreferencesManagerSaveValueWithRightKey() {
        
        // given
        sut.isCalledFromSwitchCardUp = true
        let cardBO = CardsGenerator.getCardBOWithActivationOff().cards.first
        sut.cardBO = cardBO
        
        //when
        sut.acceptButtonPressed()
        
        //then
        XCTAssert(dummyPreferencesManager.saveValueSentKey == .kSwitchCardOn)
    }
    
    func test_givenACardBOWithActivationOffAndIsCalledFromSwitchCardUpIsTrue_whenICallAcceptButtonPressed_thenICallPreferencesManagerSaveValueWithRightValue() {
        
        // given
        sut.isCalledFromSwitchCardUp = true
        let cardBO = CardsGenerator.getCardBOWithActivationOff().cards.first
        sut.cardBO = cardBO
        
        //when
        sut.acceptButtonPressed()
        
        //then
        XCTAssert(dummyPreferencesManager.saveValueSentValue as! Bool == true)
    }
    
    func test_givenACardBOWithActivationOnAndIsCalledFromSwitchCardUpIsTrue_whenICallAcceptButtonPressed_thenICallPreferencesManagerSaveValue() {
        
        // given
        sut.isCalledFromSwitchCardUp = true
        let cardBO = CardsGenerator.getCardBOWithActivationOn().cards.first
        sut.cardBO = cardBO

        //when
        sut.acceptButtonPressed()
        
        //then
        XCTAssert(dummyPreferencesManager.isCalledSaveValueWithPreferencesKey == true)
    }
    
    func test_givenACardBOWithActivationOnAndIsCalledFromSwitchCardUpIsTrue_whenICallAcceptButtonPressed_thenICallPreferencesManagerSaveValueWithRightKey() {
        
        // given
        sut.isCalledFromSwitchCardUp = true
        let cardBO = CardsGenerator.getCardBOWithActivationOn().cards.first
        sut.cardBO = cardBO
        
        //when
        sut.acceptButtonPressed()
        
        //then
        XCTAssert(dummyPreferencesManager.saveValueSentKey == .kSwitchCardOff)
    }
    
    func test_givenACardBOWithActivationOnAndIsCalledFromSwitchCardUpIsTrue_whenICallAcceptButtonPressed_thenICallPreferencesManagerSaveValueWithRightValue() {
        
        // given
        sut.isCalledFromSwitchCardUp = true
        let cardBO = CardsGenerator.getCardBOWithActivationOn().cards.first
        sut.cardBO = cardBO
        
        //when
        sut.acceptButtonPressed()
        
        //then
        XCTAssert(dummyPreferencesManager.saveValueSentValue as! Bool == true)
    }
}

class OnOffHelpDummyBusManagerA: OnOffHelpDummyBusManager {
    var isCalledSwitchUpSuccess = false
    
    override func publishData<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {
        if tag == OnOffHelpPageReactionA.ROUTER_TAG && event === OnOffHelpPageReactionA.SWITCH_CARD_UP_ON_SUCCES {
            isCalledSwitchUpSuccess = true
        }
    }
}
