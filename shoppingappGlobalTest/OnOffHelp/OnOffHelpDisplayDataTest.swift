//
//  OnOffHelpDisplayDataTest.swift
//  shoppingapp
//
//  Created by ignacio.bonafonte on 05/07/2018.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
@testable import shoppingappMX
#elseif TESTPE
@testable import shoppingappPE
#endif

class OnOffHelpDisplayDataTest: XCTestCase {

    // MARK: - generateOnOffHelpDisplayData

    func test_givenCreditCardBO_whenICreateOnOffHelpDisplayData_thenFieldsAreReadWithTheProperKey() {

        // given
        let cardEntity = CardsGenerator.getCardWithCardTypeRelatedCard(withCardType: "CREDIT_CARD").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)

        // when
        let displayData = OnOffHelpDisplayData(cardBO: cardBO)

        // then
        XCTAssert(displayData.titleOne == NSLocalizedString("KEY_CARD_HELP_ON_OFF_TITLE_1", comment: ""))
    }

    func test_givenDebitCardBO_whenICreateOnOffHelpDisplayData_thenFieldsAreReadWithTheProperKey() {

        // given
        let cardEntity = CardsGenerator.getCardWithCardTypeRelatedCard(withCardType: "DEBIT_CARD").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)

        // when
        let displayData = OnOffHelpDisplayData(cardBO: cardBO)

        // then
        XCTAssert(displayData.titleOne == NSLocalizedString("KEY_CARD_HELP_ON_OFF_TITLE_1", comment: ""))

    }

    func test_givenPrepaidCardBO_whenICreateOnOffHelpDisplayData_thenFieldsAreReadWithTheProperKey() {

        // given
        let cardEntity = CardsGenerator.getCardWithCardTypeRelatedCard(withCardType: "PREPAID_CARD").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)

        // when
        let displayData = OnOffHelpDisplayData(cardBO: cardBO)

        // then
        XCTAssert(displayData.titleOne == NSLocalizedString("KEY_CARD_HELP_ON_OFF_TITLE_1", comment: ""))
    }

    func test_givenCreditCardBO_whenICreateOnOffHelpDisplayData_thenTitleTwoFieldsAreReadWithTheProperKey() {

        // given
        let cardEntity = CardsGenerator.getCardWithCardTypeRelatedCard(withCardType: "CREDIT_CARD").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)

        // when
        let displayData = OnOffHelpDisplayData(cardBO: cardBO)

        // then
        XCTAssert(displayData.titleTwo == NSLocalizedString("KEY_CARD_HELP_ON_OFF_TITLE_2", comment: ""))
    }

    func test_givenDebitCardBO_whenICreateOnOffHelpDisplayData_thenTitleTwoFieldsAreReadWithTheProperKey() {

        // given
        let cardEntity = CardsGenerator.getCardWithCardTypeRelatedCard(withCardType: "DEBIT_CARD").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)

        // when
        let displayData = OnOffHelpDisplayData(cardBO: cardBO)

        // then
        XCTAssert(displayData.titleTwo == NSLocalizedString("KEY_CARD_HELP_ON_OFF_TITLE_2", comment: ""))

    }

    func test_givenPrepaidCardBO_whenICreateOnOffHelpDisplayData_thenTitleTwoFieldsAreReadWithTheProperKey() {

        // given
        let cardEntity = CardsGenerator.getCardWithCardTypeRelatedCard(withCardType: "PREPAID_CARD").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)

        // when
        let displayData = OnOffHelpDisplayData(cardBO: cardBO)

        // then
        XCTAssert(displayData.titleTwo == NSLocalizedString("KEY_CARD_HELP_ON_OFF_TITLE_2", comment: ""))
    }

    func test_givenCardBO_whenICreateOnOffHelpDisplayData_thenImagesAndTextsMatch() {

        // given
        let cardEntity = CardsGenerator.getCardWithCardTypeRelatedCard(withCardType: "CREDIT_CARD").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)

        // when
        let displayData = OnOffHelpDisplayData(cardBO: cardBO)

        // then
        XCTAssert(displayData.headerImageName == ConstantsImages.Card.ilu_turn_on_off)
        XCTAssert(displayData.bulletImageName == ConstantsImages.Common.bullet_icon)
        XCTAssert(displayData.acceptButtonTitle == Localizables.login.key_login_disconnect_understood_text)
        XCTAssert(displayData.disclaimerText == Localizables.cards.key_card_disclaimer_on_off)
    }
    
    func test_givenCardBOAndKeyRootLocalizable_whenICreateOnOffHelpDisplayData_thenDynamicTextsMatch() {

        // given
        let cardEntity = CardsGenerator.getCardWithCardTypeRelatedCard(withCardType: "CREDIT_CARD").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)

        let keyRoot = Settings.CardHelpOnOff.localizationKeyForHelpOnOffAndCard(card: cardBO)

        // when
        let displayData = OnOffHelpDisplayData(cardBO: cardBO)

        // then
        XCTAssert(!displayData.textsOne.isEmpty)
        XCTAssert(!displayData.textsTwo.isEmpty)

        //Dynamic texts
        for index in 1...displayData.textsOne.count {
            XCTAssert(displayData.textsOne[index - 1] == NSLocalizedString(keyRoot + "TEXT_\(index)", comment: ""))
        }
        for index in 1...displayData.textsTwo.count {
            XCTAssert(displayData.textsTwo[index - 1] == NSLocalizedString(keyRoot + "TEXT_TWO_\(index)", comment: ""))
        }
        if !displayData.textsThree!.isEmpty {
            for index in 1...displayData.textsThree!.count {
                XCTAssert(displayData.textsThree![index - 1] == NSLocalizedString(keyRoot + "TEXT_THREE_\(index)", comment: ""))
            }

        }
    }
    
    // MARK: MX specific behavior
    
    #if TESTMX
        
    func test_givenAnOnCard_whenICreateOnOffHelpDisplayData_thenTitleTwoFieldsAreReadWithTheProperKey() {
        
        // given
        let cardEntity = CardsGenerator.getCardWithCardTypeRelatedCard(withCardType: "CREDIT_CARD").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)
        
        // when
        let displayData = OnOffHelpDisplayData(cardBO: cardBO)
        
        // then
        XCTAssert(displayData.titleTwo == NSLocalizedString("KEY_CARD_HELP_OFF_TITLE_2", comment: ""))
    }
    
    func test_givenAnOnCard_whenICreateOnOffHelpDisplayData_thenImagesAndTextsMatch() {
        
        // given
        let cardEntity = CardsGenerator.getCardWithCardTypeRelatedCard(withCardType: "CREDIT_CARD").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)
        
        // when
        let displayData = OnOffHelpDisplayData(cardBO: cardBO)
        
        // then
        XCTAssert(displayData.headerImageName == ConstantsImages.Card.ilu_turn_on_off)
        XCTAssert(displayData.bulletImageName == ConstantsImages.Common.bullet_icon)
        XCTAssert(displayData.disclaimerText == NSLocalizedString("KEY_CARD_HELP_OFF_DISCLAIMER", comment: ""))
        
    }
    
    func test_givenAnOffCard_whenICreateOnOffHelpDisplayData_thenFieldsAreReadWithTheProperKey() {
        
        // given
        let cardEntity = CardsGenerator.getCardWithCardTypeRelatedCard(withCardType: "CREDIT_CARD").data![0]
        let cardBO = CardBODummy(cardEntity: cardEntity)
        cardBO.isOff = true
        
        // when
        let displayData = OnOffHelpDisplayData(cardBO: cardBO)
        
        // then
        XCTAssert(displayData.titleOne == NSLocalizedString("KEY_CARD_HELP_ON_TITLE_1", comment: ""))
    }
    
    func test_givenAnOffCard_whenICreateOnOffHelpDisplayData_thenTitleTwoFieldsAreReadWithTheProperKey() {
        
        // given
        let cardEntity = CardsGenerator.getCardWithCardTypeRelatedCard(withCardType: "CREDIT_CARD").data![0]
        let cardBO = CardBODummy(cardEntity: cardEntity)
        cardBO.isOff = true
        // when
        let displayData = OnOffHelpDisplayData(cardBO: cardBO)
        
        // then
        XCTAssert(displayData.titleTwo == NSLocalizedString("KEY_CARD_HELP_ON_TITLE_2", comment: ""))
    }
    
    func test_givenAnOffCard_whenICreateOnOffHelpDisplayData_thenImagesAndTextsMatch() {
        
        // given
        let cardEntity = CardsGenerator.getCardWithCardTypeRelatedCard(withCardType: "CREDIT_CARD").data![0]
        let cardBO = CardBODummy(cardEntity: cardEntity)
        cardBO.isOff = true
        // when
        let displayData = OnOffHelpDisplayData(cardBO: cardBO)
        
        // then
        XCTAssert(displayData.headerImageName == ConstantsImages.Card.ilu_turn_on_off)
        XCTAssert(displayData.bulletImageName == ConstantsImages.Common.bullet_icon)
        XCTAssert(displayData.disclaimerText == NSLocalizedString("KEY_CARD_HELP_ON_DISCLAIMER", comment: ""))
    }
    
    class CardBODummy: CardBO {
        var isOff = false
        
        override func isCardOn() -> Bool {
            return !isOff
        }
    }
    #endif
    
}
