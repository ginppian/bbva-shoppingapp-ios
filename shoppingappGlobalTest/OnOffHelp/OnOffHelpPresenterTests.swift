//
//  OnOffHelpPresenterTests.swift
//  shoppingappGlobal
//
//  Created by Javier Pino on 6/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class OnOffHelpPresenterTests: XCTestCase {

    var sut: OnOffHelpPresenter<OnOffHelpViewDummy>!
    var dummyBusManager: OnOffHelpDummyBusManager!

    override func setUp() {
        super.setUp()
        dummyBusManager = OnOffHelpDummyBusManager()
        sut = OnOffHelpPresenter<OnOffHelpViewDummy>(busManager: dummyBusManager)
    }

    // MARK: - setModel

    func test_givenCardsTransport_whenICallSetModel_thenCardBOBeFilled() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        let cardsTransport = CardsTransport(cardBO: cardBO)

        // when
        sut.setModel(model: cardsTransport)

        // then
        XCTAssert(sut.cardBO === cardsTransport.cardBO)

    }

    // MARK: - viewLoaded

    func test_givenCard_whenICallViewLoaded_thenICallShowHelpInformation() {

        let dummyview = OnOffHelpViewDummy()
        sut.view = dummyview
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        // given
        sut.cardBO = cardBO

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyview.isCalledShowHelpInformation == true)
    }

    func test_givenCard_whenICallViewLoaded_thenICallShowHelpInformationAndDisplayDataMatch() {

        let dummyview = OnOffHelpViewDummy()
        sut.view = dummyview
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        // given
        sut.cardBO = cardBO
        sut.displayData = OnOffHelpDisplayData(cardBO: cardBO)
        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyview.onOffHelpDisplayDataSent == sut.displayData)
    }

    // MARK: - acceptButtonPressed

    func test_givenAny_whenICallAcceptButtonPressed_thenICallBusManagerDismissViewController() {

        // given

        // when
        sut.acceptButtonPressed()

        // then
        XCTAssert(dummyBusManager.isCalledDismissAction == true)
    }
}

// MARK: - Dummy classes

class OnOffHelpViewDummy: OnOffHelpViewProtocol {
    
    var isCalledShowHelpInformation = false
    var onOffHelpDisplayDataSent: OnOffHelpDisplayData?
    
    func showHelpInformation(displayData: OnOffHelpDisplayData) {
        
        isCalledShowHelpInformation = true
        onOffHelpDisplayDataSent = displayData
    }
    
    func showError(error: ModelBO) {
    }
    
    func changeColorEditTexts() {
    }
}

class OnOffHelpDummyBusManager: BusManager {
    
    var isCalledDismissAction = false
    
    override init() {
        super.init()
        routerFactory = RouterInitFactory()
    }
    
    override func dismissViewController(animated: Bool, completion: (() -> Void)?) {
        super.dismissViewController(animated: animated, completion: completion)
        
        isCalledDismissAction = true
    }
}
