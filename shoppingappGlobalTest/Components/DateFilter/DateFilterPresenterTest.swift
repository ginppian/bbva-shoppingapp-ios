//
//  DateFilterPresenterTest.swift
//  shoppingapp
//
//  Created by jesus.martinez on 28/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class DateFilterPresenterTest: XCTestCase {

    let sut = DateFilterPresenter()

    func test_givenView_whenICallOnConfigureView_thenICallViewShowNormalState() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.onConfigureView()

        // then
        XCTAssert(dummyView.isCalledShowNormalState == true)

    }

    func test_givenView_whenICallOnConfigureView_thenIDoNotCallViewUpdateViewWithDisplayData() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.onConfigureView()

        // then
        XCTAssert(dummyView.isCalledUpdateViewWithDisplayData == false)

    }

    func test_givenViewAndDisplayData_whenICallOnConfigureView_thenICallViewUpdateViewWithDisplayData() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let displayData = DateFilterDisplayData(minDate: nil, maxDate: Date(), title: Localizables.transactions.key_transactions_from_text, dateSelectedTitle: Localizables.transactions.key_transactions_pickdate_text, dateSelected: nil)
        sut.displayData = displayData

        // when
        sut.onConfigureView()

        // then
        XCTAssert(dummyView.isCalledUpdateViewWithDisplayData == true)

    }

    func test_givenViewAndDisplayData_whenICallOnConfigureView_thenICallViewUpdateViewWithDisplayDataWithTheSameDisplayData() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let displayData = DateFilterDisplayData(minDate: nil, maxDate: Date(), title: Localizables.transactions.key_transactions_from_text, dateSelectedTitle: Localizables.transactions.key_transactions_pickdate_text, dateSelected: nil)
        sut.displayData = displayData

        // when
        sut.onConfigureView()

        // then
        XCTAssert(dummyView.displayData! == displayData)

    }

    func test_givenView_whenICallOnShowCalendarAction_thenICallViewShowCalendar() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.onShowCalendarAction()

        // then
        XCTAssert(dummyView.isCalledShowCalendar == true)

    }

    func test_givenView_whenICallOnSelectDate_thenICallViewUpdateDateLabel() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.onSelect(date: Date())

        // then
        XCTAssert(dummyView.isCalledUpdateDateLabel == true)

    }

    func test_givenView_whenICallOnSelectDate_thenICallViewUpdateDateLabelWithTheSameDateFormattedDMMYYYY() {

        // given
        let date = Date.date(fromLocalTimeZoneString: "2012-12-12", withFormat: Date.DATE_FORMAT_SERVER)!
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.onSelect(date: date)

        // then
        XCTAssert(dummyView.date == date.string(format: Date.DATE_FORMAT_DAY_MONTH_YEAR).capitalized)

    }

    func test_givenView_whenICallStartEditing_thenICallViewShowSelectedState() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.startEditing()

        // then
        XCTAssert(dummyView.isCalledShowSelectedState == true)

    }

    func test_givenView_whenICallEndEditing_thenICallViewShowNormalState() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.endEditing()

        // then
        XCTAssert(dummyView.isCalledShowNormalState == true)

    }

    func test_givenView_whenICallStartEditing_thenICallViewInformDelegateStartEditing() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.startEditing()

        // then
        XCTAssert(dummyView.isCalledInformDelegateStartEditing == true)

    }

    func test_givenView_whenICallEndEditing_thenICallViewInformDelegateEndEditing() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.endEditing()

        // then
        XCTAssert(dummyView.isCalledInformDelegateEndEditing == true)

    }

    func test_givenView_whenICallOnSelect_thenICallViewInformDelegateNewDateSelected() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.onSelect(date: Date())

        // then
        XCTAssert(dummyView.isCalledInformDelegateNewDateSelected == true)

    }

    func test_givenView_whenICallOnSelect_thenICallViewInformDelegateNewDateSelectedWithTheSameDate() {

        // expected
        let date = Date()

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.onSelect(date: date)

        // then
        XCTAssert(dummyView.dateSelected == date)

    }

    func test_givenAView_whenICallPresentMinimumDate_thenICallViewUpdateWithMinimumDate() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.present(minimumDate: Date())

        // then
        XCTAssert(dummyView.isCalledUpdateMinimumDate == true)

    }

    func test_givenAViewAndADate_whenICallPresentMinimumDate_thenICallViewUpdateWithMinimumDateWithTheSameDate() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let date = Date()

        // when
        sut.present(minimumDate: date)

        // then
        XCTAssert(dummyView.minimumDate == date)

    }

    func test_givenAView_whenICallPresentMaximumDate_thenICallViewUpdateWithMaximumDate() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.present(maximumDate: Date())

        // then
        XCTAssert(dummyView.isCalledUpdateMaximumDate == true)

    }

    func test_givenAViewAndADate_whenICallPresentMaximumDate_thenICallViewUpdateWithMaximumDateWithTheSameDate() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let date = Date()

        // when
        sut.present(maximumDate: date)

        // then
        XCTAssert(dummyView.maximumDate == date)

    }

    class DummyView: DateFilterViewProtocol {

        var isCalledShowNormalState = false
        var isCalledShowSelectedState = false
        var isCalledShowCalendar = false
        var isCalledUpdateDateLabel = false
        var isCalledUpdateMinimumDate = false
        var isCalledUpdateMaximumDate = false
        var isCalledInformDelegateStartEditing = false
        var isCalledInformDelegateEndEditing = false
        var isCalledInformDelegateNewDateSelected = false
        var isCalledUpdateViewWithDisplayData = false

        var date = ""
        var dateSelected: Date?
        var minimumDate: Date?
        var maximumDate: Date?
        var displayData: DateFilterDisplayData?

        func showNormalState() {
            isCalledShowNormalState = true
        }

        func showSelectedState() {
            isCalledShowSelectedState = true
        }

        func showCalendar() {
            isCalledShowCalendar = true
        }

        func updateDateLabel(withDate date: String) {
            isCalledUpdateDateLabel = true
            self.date = date
        }

        func update(witMinimumDate minimumDate: Date) {
            isCalledUpdateMinimumDate = true
            self.minimumDate = minimumDate
        }

        func update(witMaximum maximumDate: Date) {
            isCalledUpdateMaximumDate = true
            self.maximumDate = maximumDate
        }

        func informDelegateStartEditing() {
            isCalledInformDelegateStartEditing = true
        }

        func informDelegateEndEditing() {
            isCalledInformDelegateEndEditing = true
        }

        func informDelegate(newDateSelected: Date) {
            isCalledInformDelegateNewDateSelected = true
            dateSelected = newDateSelected
        }

        func updateView(withDisplayData displayData: DateFilterDisplayData) {
            isCalledUpdateViewWithDisplayData = true
            self.displayData = displayData
        }

    }

}
