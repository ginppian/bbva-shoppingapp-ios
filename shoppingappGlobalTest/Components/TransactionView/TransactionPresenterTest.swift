//
//  TransactionPresenterTest.swift
//  shoppingapp
//
//  Created by Arturo Marzo de la Fuente on 27/9/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import XCTest
import RxSwift

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class TransactionPresenterTest: XCTestCase {

    var sut: TransactionPresenter!
    let dummyView = DummyView()
    let dummyInteractor = DummyInteractor()

    override func setUp() {
        
        super.setUp()
        
        sut = TransactionPresenter()
        
        sut.view = dummyView
        sut.interactor = dummyInteractor
    }
    
    func test_givenAADisplayDataWithCardId_whenICallLoadTransactionsAndInteractorIsNotInitialized_thenInitializeIntertactor() {

        // given
        let displayData = TransactionDisplayData(numberOfSkeletons: 2, maximumNumberOfTransactions: 7, cardId: "1")

        // when
        sut.loadTransactions(forDisplayData: displayData)

        // then
        XCTAssert(sut.interactor != nil)
    }

    func test_givenADisplayDataWithCardId_whenICallLoadTransactions_thenCallShowSkeleton() {

        // given
        let displayData = TransactionDisplayData(numberOfSkeletons: 2, maximumNumberOfTransactions: 7, cardId: "1")

        // when
        sut.loadTransactions(forDisplayData: displayData)

        // then
        XCTAssert(dummyView.isCalledShowSkeleton == true)
    }

    func test_givenADisplayDataWithCardId_whenICallLoadTransactions_thenCallShowSkeletonWithTheSameNumberOfSkeletonsThanTransactionViewDisplayData() {

        // given
        let displayData = TransactionDisplayData(numberOfSkeletons: 2, maximumNumberOfTransactions: 7, cardId: "1")

        // when
        sut.loadTransactions(forDisplayData: displayData)

        // then
        XCTAssert(dummyView.numberOfSkeletons == displayData.numberOfSkeletons)
    }

    func test_givenADisplayDataWithCardId_whenICallLoadTransactions_thenCallProvideTransactions() {

        // given
        let displayData = TransactionDisplayData(numberOfSkeletons: 2, maximumNumberOfTransactions: 7, cardId: "1")

        // when
        sut.loadTransactions(forDisplayData: displayData)

        // then
        XCTAssert(dummyInteractor.isCalledProvideTransactions == true)
    }

    func test_givenADisplayDataWithCardId_whenICallLoadTransactions_thenCallProvideTransactionsWithTheSameCardId() {

        // given
        let cardId = "1"
        let displayData = TransactionDisplayData(numberOfSkeletons: 2, maximumNumberOfTransactions: 7, cardId: cardId)

        // when
        sut.loadTransactions(forDisplayData: displayData)

        // then
        XCTAssert(dummyInteractor.cardId == cardId)
    }

    func test_givenADisplayDataWithCardId_whenICallLoadTransactions_thenCallAddTransactions() {

        // given
        let dummyTransactionsDisplayData = DummyTransactionsDisplayData()
        sut.displayData = dummyTransactionsDisplayData

        let displayData = TransactionDisplayData(numberOfSkeletons: 2, maximumNumberOfTransactions: 7, cardId: "1")

        // when
        sut.loadTransactions(forDisplayData: displayData)

        // then
        XCTAssert(dummyTransactionsDisplayData.isCalledAddTransactionsDisplayData == true)
    }

    func test_givenADisplayDataWithCardId_whenICallLoadTransactions_thenCallHideSkeleton() {

        // given
        let displayData = TransactionDisplayData(numberOfSkeletons: 2, maximumNumberOfTransactions: 7, cardId: "1")

        // when
        sut.loadTransactions(forDisplayData: displayData)

        // then
        XCTAssert(dummyView.isCalledHideSkeleton == true)
    }

    func test_givenADisplayDataWithCardId_whenICallLoadTransactions_thenCallShowTransactions() {

        // given
        let displayData = TransactionDisplayData(numberOfSkeletons: 2, maximumNumberOfTransactions: 7, cardId: "1")

        // when
        sut.loadTransactions(forDisplayData: displayData)

        // then
        XCTAssert(dummyView.isCalledShowTransactions == true)
    }

    func test_givenADisplayDataWithCardId_whenICallLoadTransactions_thenTransactionDisplayDataIsNotEmpty() {

        // given
        let displayData = TransactionDisplayData(numberOfSkeletons: 2, maximumNumberOfTransactions: 7, cardId: "1")

        // when
        sut.loadTransactions(forDisplayData: displayData)

        // then
        XCTAssert(dummyView.transactionsDisplayData != nil)
    }

    func test_givenADisplayDataWithCardId_whenICallLoadTransactions_thenTransactionViewDisplayDataShouldBeFilled() {

        // given
        let displayData = TransactionDisplayData(numberOfSkeletons: 2, maximumNumberOfTransactions: 7, cardId: "1")

        // when
        sut.loadTransactions(forDisplayData: displayData)

        // then
        XCTAssert(sut.transactionDisplayData != nil)
    }

    func test_givenADisplayDataWithCardId_whenICallLoadTransactions_thenTransactionViewDisplayDataAndTransactionsBOShouldBeMatch() {

        // given
        let displayData = TransactionDisplayData(numberOfSkeletons: 2, maximumNumberOfTransactions: 7, cardId: "1")

        // when
        sut.loadTransactions(forDisplayData: displayData)

        // then
        XCTAssert(sut.transactionDisplayData! == displayData)
        XCTAssert(sut.transactionDisplayData?.transactionsBO! === dummyInteractor.transactionsBO)
        XCTAssert(sut.transactionDisplayData?.transactionsBO?.transactions.count == dummyInteractor.transactionsBO?.transactions.count)
    }

    func test_givenACardIdAndTransactions_whenIPressShowAllTransactions_thenIsCalledShowAllTransactions() {

        // given
        let displayData = TransactionsDisplayData()
        let dummyTransactionsEntity = TransactionsEntity()
        dummyTransactionsEntity.status = 200
        let transactionsBO = TransactionsBO(transactionsEntity: dummyTransactionsEntity)
        displayData.add(transactions: transactionsBO.transactions)
        
        dummyView.transactionsDisplayData = displayData

        // when
        sut.showAllTransactionsPressed()

        // then
        XCTAssert(dummyView.isCalledShowAllTransanctions == true)
    }

    func test_givenADisplayDataWithCardId_whenICallLoadTransactionsAndThereIsAnError_thenIsCalledHideSkeleton() {

        // given
        dummyInteractor.forceError = true

        let displayData = TransactionDisplayData(numberOfSkeletons: 2, maximumNumberOfTransactions: 7, cardId: "1")

        // when
        sut.loadTransactions(forDisplayData: displayData)

        // then
        XCTAssert(dummyView.isCalledHideSkeleton)
    }

    func test_givenADisplayDataWithCardId_whenICallLoadTransactionsAndThereIsAnError_thenIsCalledShowErrorMessage() {

        // given
        dummyInteractor.forceError = true

        let displayData = TransactionDisplayData(numberOfSkeletons: 2, maximumNumberOfTransactions: 7, cardId: "1")

        // when
        sut.loadTransactions(forDisplayData: displayData)

        // then
        XCTAssert(dummyView.isCalledShowMessage)
    }

    func test_givenADisplayDataWithCardId_whenICallLoadTransactionsAndThereIsNoTransactions_thenIsCalledHideSkeleton() {

        // given
        dummyInteractor.forceNoresults = true

        let displayData = TransactionDisplayData(numberOfSkeletons: 2, maximumNumberOfTransactions: 7, cardId: "1")

        // when
        sut.loadTransactions(forDisplayData: displayData)

        // then
        XCTAssert(dummyView.isCalledHideSkeleton)
    }

    func test_givenADisplayDataWithCardId_whenICallLoadTransactionsAndThereIsNoTransactions_thenIsCalledShowNoTransactions() {

        // given
        dummyInteractor.forceNoresults = true

        let displayData = TransactionDisplayData(numberOfSkeletons: 2, maximumNumberOfTransactions: 7, cardId: "1")

        // when
        sut.loadTransactions(forDisplayData: displayData)

        // then
        XCTAssert(dummyView.isCalledShowMessage)
    }

    func test_givenADisplayDataWithCardIdAndMaximumTransactions_whenICallLoadTransactionsAndInteractorReturnsLessTransactionsThanMaximum_thenICallViewShowTransactionsWithTheSameNumberOfTransactionsDisplayDataThanReceived() {

        let maximumNumberOfTransactions = 7
        let numberOfTransactions = 3

        // given
        var transactions = [TransactionEntity]()

        for _ in 0..<numberOfTransactions {

            transactions.append(TransactionEntityDummyGenerator.transactionEntity())

        }

        dummyInteractor.dummyEntities = transactions

        let displayData = TransactionDisplayData(numberOfSkeletons: 2, maximumNumberOfTransactions: maximumNumberOfTransactions, cardId: "1")

        // when
        sut.loadTransactions(forDisplayData: displayData)

        // then
        XCTAssert(dummyView.transactionsDisplayData?.sections[0].items.count == transactions.count)

    }

    func test_givenADisplayDataWithCardIdAndMaximumTransactions_whenICallLoadTransactionsAndInteractorReturnsEqualsTransactionsThanMaximum_thenICallViewShowTransactionsWithTheSameNumberOfTransactionsDisplayDataThanReceived() {

        let maximumNumberOfTransactions = 7
        let numberOfTransactions = maximumNumberOfTransactions

        // given
        var transactions = [TransactionEntity]()

        for _ in 0..<numberOfTransactions {

            transactions.append(TransactionEntityDummyGenerator.transactionEntity())

        }

        dummyInteractor.dummyEntities = transactions

        let displayData = TransactionDisplayData(numberOfSkeletons: 2, maximumNumberOfTransactions: maximumNumberOfTransactions, cardId: "1")

        // when
        sut.loadTransactions(forDisplayData: displayData)

        // then
        XCTAssert(dummyView.transactionsDisplayData?.sections[0].items.count == transactions.count)

    }

    func test_givenADisplayDataWithCardIdAndMaximumTransactions_whenICallLoadTransactionsAndInteractorReturnsMoreTransactionsThanMaximum_thenICallViewShowTransactionsWithTheSameNumberOfTransactionsDisplayDataThanMaximumTransactions() {

        let maximumNumberOfTransactions = 7
        let numberOfTransactions = 20

        // given
        var transactions = [TransactionEntity]()

        for _ in 0..<numberOfTransactions {

            transactions.append(TransactionEntityDummyGenerator.transactionEntity())

        }

        dummyInteractor.dummyEntities = transactions

        let displayData = TransactionDisplayData(numberOfSkeletons: 2, maximumNumberOfTransactions: maximumNumberOfTransactions, cardId: "1")

        // when
        sut.loadTransactions(forDisplayData: displayData)

        // then
        XCTAssert(dummyView.transactionsDisplayData?.sections[0].items.count == maximumNumberOfTransactions)

    }

    func test_givenADisplayDataWithCardIdAndDelegate_whenICallLoadTransactionsAndInteractorReturnsTransactions_thenICallDelegateLoadedTransactionsForCardId() {

        // given
        let dummyPresenterDelegate = DummyPresenterDelegate()
        sut.delegate = dummyPresenterDelegate

        let displayData = TransactionDisplayData(numberOfSkeletons: 2, maximumNumberOfTransactions: 7, cardId: "1")

        // when
        sut.loadTransactions(forDisplayData: displayData)

        // then
        XCTAssert(dummyPresenterDelegate.isCalledLoadedTransactions == true)

    }

    func test_givenADisplayDataWithCardIdAndDelegate_whenICallLoadTransactionsAndInteractorReturnsTransactions_thenICallDelegateLoadedTransactionsForCardIdWithTheSameTransactionsBOThanLoadedAndCardIdOfDisplayData() {

        // given
        let dummyPresenterDelegate = DummyPresenterDelegate()
        sut.delegate = dummyPresenterDelegate

        let displayData = TransactionDisplayData(numberOfSkeletons: 2, maximumNumberOfTransactions: 7, cardId: "1")

        // when
        sut.loadTransactions(forDisplayData: displayData)

        // then
        XCTAssert(dummyPresenterDelegate.loadedTransactions === dummyInteractor.transactionsBO)
        XCTAssert(dummyPresenterDelegate.transactionsForCardId == displayData.cardId)

    }

    func test_givenADisplayDataWithCardIdWithoutDelegate_whenICallLoadTransactionsAndInteractorReturnsTransactions_thenIDoNotCallDelegateLoadedTransactionsForCardId() {

        // given
        let dummyPresenterDelegate = DummyPresenterDelegate()

        let displayData = TransactionDisplayData(numberOfSkeletons: 2, maximumNumberOfTransactions: 7, cardId: "1")

        // when
        sut.loadTransactions(forDisplayData: displayData)

        // then
        XCTAssert(dummyPresenterDelegate.isCalledLoadedTransactions == false)

    }

    // MARK: loadTransactions with TransacationsBO

    func test_givenADisplasDataWithTransactionsDisplayData_whenICallLoadTransactions_thenIDoNotCallViewShowSkeleton() {

        // given
        var displayData = TransactionDisplayData(numberOfSkeletons: 2, maximumNumberOfTransactions: 7, cardId: "1")
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 200
        displayData.transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        // when
        sut.loadTransactions(forDisplayData: displayData)

        // then
        XCTAssert(dummyView.isCalledShowSkeleton == false)

    }

    func test_givenADisplasDataWithTransactionsDisplayData_whenICallLoadTransactions_thenIDoNotCallInteractorProvideTransactions() {

        // given
        var displayData = TransactionDisplayData(numberOfSkeletons: 2, maximumNumberOfTransactions: 7, cardId: "1")
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 200
        displayData.transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        // when
        sut.loadTransactions(forDisplayData: displayData)

        // then
        XCTAssert(dummyInteractor.isCalledProvideTransactions == false)

    }

    func test_givenADisplasDataWithTransactionsDisplayDataWith200_whenICallLoadTransactions_thenICallViewShowTransactions() {

        // given
        var transactions = [TransactionEntity]()

        for _ in 0..<5 {

            transactions.append(TransactionEntityDummyGenerator.transactionEntity())

        }

        var displayData = TransactionDisplayData(numberOfSkeletons: 2, maximumNumberOfTransactions: 7, cardId: "1")
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 200
        transactionsEntity.data = transactions
        displayData.transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        // when
        sut.loadTransactions(forDisplayData: displayData)

        // then
        XCTAssert(dummyView.isCalledShowTransactions == true)
    }

    func test_givenADisplasDataWithTransactionsDisplayDataWith200_whenICallLoadTransactions_thenICallViewTransactionDisplayDataIsNotEmpty() {

        // given
        var transactions = [TransactionEntity]()

        for _ in 0..<5 {

            transactions.append(TransactionEntityDummyGenerator.transactionEntity())

        }

        var displayData = TransactionDisplayData(numberOfSkeletons: 2, maximumNumberOfTransactions: 7, cardId: "1")
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 200
        transactionsEntity.data = transactions
        displayData.transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        // when
        sut.loadTransactions(forDisplayData: displayData)

        // then
        XCTAssert(dummyView.transactionsDisplayData != nil)
    }

    func test_givenADisplasDataWithTransactionsDisplayData_whenICallLoadTransactions_thenTransactionViewDisplayDataShouldBeFilled() {

        // given
        var transactions = [TransactionEntity]()

        for _ in 0..<5 {

            transactions.append(TransactionEntityDummyGenerator.transactionEntity())

        }

        var displayData = TransactionDisplayData(numberOfSkeletons: 2, maximumNumberOfTransactions: 7, cardId: "1")
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 200
        transactionsEntity.data = transactions
        displayData.transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        // when
        sut.loadTransactions(forDisplayData: displayData)

        // then
        XCTAssert(sut.transactionDisplayData != nil)
    }

    func test_givenADisplasDataWithTransactionsDisplayData_whenICallLoadTransactions_thenTransactionsBOShouldBeMatch() {

        // given
        var transactions = [TransactionEntity]()

        for _ in 0..<5 {

            transactions.append(TransactionEntityDummyGenerator.transactionEntity())

        }

        var displayData = TransactionDisplayData(numberOfSkeletons: 2, maximumNumberOfTransactions: 7, cardId: "1")
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 200
        transactionsEntity.data = transactions
        displayData.transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        // when
        sut.loadTransactions(forDisplayData: displayData)

        // then
        XCTAssert(sut.transactionDisplayData! == displayData)
        XCTAssert(sut.transactionDisplayData!.transactionsBO! === displayData.transactionsBO)
        XCTAssert(sut.transactionDisplayData?.transactionsBO?.transactions.count == displayData.transactionsBO?.transactions.count)
    }

    func test_givenADisplasDataWithTransactionsDisplayDataWith500_whenICallLoadTransactions_thenICallViewShowErrorMessage() {

        // given
        var displayData = TransactionDisplayData(numberOfSkeletons: 2, maximumNumberOfTransactions: 7, cardId: "1")
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 500
        displayData.transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        // when
        sut.loadTransactions(forDisplayData: displayData)

        // then
        XCTAssert(dummyView.isCalledShowMessage)
    }

    func test_givenADisplasDataWithTransactionsDisplayDataWith500_whenICallLoadTransactions_thenMessageDisplayDataShouldHaveErrorMessage() {

        // given
        var displayData = TransactionDisplayData(numberOfSkeletons: 2, maximumNumberOfTransactions: 7, cardId: "1")
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 500
        displayData.transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        // when
        sut.loadTransactions(forDisplayData: displayData)

        // then
        XCTAssert(dummyView.message!.messageText == Localizables.transactions.key_transactions_error_text)
        XCTAssert(dummyView.message!.messageButton == Localizables.transactions.key_transactions_reload_text)

    }

    func test_givenADisplasDataWithTransactionsDisplayDataWith204_whenICallLoadTransactions_thenICalleViewShowErrorMessage() {

        // given
        var displayData = TransactionDisplayData(numberOfSkeletons: 2, maximumNumberOfTransactions: 7, cardId: "1")
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 204
        displayData.transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        // when
        sut.loadTransactions(forDisplayData: displayData)

        // then
        XCTAssert(dummyView.isCalledShowMessage)
    }

    func test_givenADisplasDataWithTransactionsDisplayDataWith204_whenICallLoadTransactions_thenMessageDisplayDataShouldHaveNotContentMessage() {

        // given
        var displayData = TransactionDisplayData(numberOfSkeletons: 2, maximumNumberOfTransactions: 7, cardId: "1")
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 204
        displayData.transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        // when
        sut.loadTransactions(forDisplayData: displayData)

        // then
        XCTAssert(dummyView.message!.messageText == Localizables.transactions.key_transactions_noresults_text)
        XCTAssert(dummyView.message!.messageButton == Localizables.transactions.key_transactions_searchtransactions_text)

    }

    func test_givenADisplasDataWithShowDirectAccessTrue_whenICallLoadTransactions_thenICallViewShowDirectAcess() {

        // given
        var displayData = TransactionDisplayData(numberOfSkeletons: 2, maximumNumberOfTransactions: 7, cardId: "1")
        displayData.showDirectAccess = true

        // when
        sut.loadTransactions(forDisplayData: displayData)

        // then
        XCTAssert(dummyView.isCalledShowDirectAccess == true)

    }

    func test_givenADisplasDataWithShowDirectAccessTrue_whenICallLoadTransactions_thenIDoNotCallViewShowSkeleton() {

        // given
        var displayData = TransactionDisplayData(numberOfSkeletons: 2, maximumNumberOfTransactions: 7, cardId: "1")
        displayData.showDirectAccess = true

        // when
        sut.loadTransactions(forDisplayData: displayData)

        // then
        XCTAssert(dummyView.isCalledShowSkeleton == false)

    }

    func test_givenADisplasDataWithShowDirectAccessTrue_whenICallLoadTransactions_thenIDoNotCallInteractorProvideTransactions() {

        // given
        var displayData = TransactionDisplayData(numberOfSkeletons: 2, maximumNumberOfTransactions: 7, cardId: "1")
        displayData.showDirectAccess = true

        // when
        sut.loadTransactions(forDisplayData: displayData)

        // then
        XCTAssert(dummyInteractor.isCalledProvideTransactions == false)

    }

    func test_givenADisplasDataWithShowDirectAccessTrue_whenICallLoadTransactions_thenDoNotCallShowTransactions() {

        // given
        var displayData = TransactionDisplayData(numberOfSkeletons: 2, maximumNumberOfTransactions: 7, cardId: "1")
        displayData.showDirectAccess = true

        // when
        sut.loadTransactions(forDisplayData: displayData)

        // then
        XCTAssert(dummyView.isCalledShowTransactions == false)
    }

    func test_givenADisplasDataWithShowDirectAccessFalse_whenICallLoadTransactions_thenIDoNotCallViewShowDirectAcess() {

        // given
        var displayData = TransactionDisplayData(numberOfSkeletons: 2, maximumNumberOfTransactions: 7, cardId: "1")
        displayData.showDirectAccess = false

        // when
        sut.loadTransactions(forDisplayData: displayData)

        // then
        XCTAssert(dummyView.isCalledShowDirectAccess == false)

    }

    // MARK: messageButtonPressed

    func test_givenTransactionViewDisplayDataWithTransactionsBOWithStatus204_whenICallMessageButtonPressed_thenICallViewShowNoContentAction() {

        // given
        var displayData = TransactionDisplayData(numberOfSkeletons: 2, maximumNumberOfTransactions: 7, cardId: "1")
        let transactionsEntity = TransactionsEntity()
        transactionsEntity.status = 204
        displayData.transactionsBO = TransactionsBO(transactionsEntity: transactionsEntity)

        sut.transactionDisplayData = displayData

        // when
        sut.messageButtonPressed()

        // then
        XCTAssert(dummyView.isCalledShowNoContentAction == true)

    }

    func test_givenTransactionViewDisplayDataWithoutTransactionsBO_whenICallMessageButtonPressed_thenICallViewDoNotShowNoContentAction() {

        // given
        let displayData = TransactionDisplayData(numberOfSkeletons: 2, maximumNumberOfTransactions: 7, cardId: "1")
        sut.transactionDisplayData = displayData

        // when
        sut.messageButtonPressed()

        // then
        XCTAssert(dummyView.isCalledShowNoContentAction == false)

    }

    func test_givenTransactionViewDisplayDataWithoutTransactionsBO_whenICallMessageButtonPressed_thenCallShowSkeleton() {

        // given
        let displayData = TransactionDisplayData(numberOfSkeletons: 2, maximumNumberOfTransactions: 7, cardId: "1")
        sut.transactionDisplayData = displayData

        // when
        sut.messageButtonPressed()

        // then
        XCTAssert(dummyView.isCalledShowSkeleton == true)
    }

    func test_givenTransactionViewDisplayDataWithoutTransactionsBO_whenICallMessageButtonPressed_thenCallShowSkeletonWithTheSameNumberOfSkeletonsThanTransactionViewDisplayData() {

        // given
        let displayData = TransactionDisplayData(numberOfSkeletons: 2, maximumNumberOfTransactions: 7, cardId: "1")
        sut.transactionDisplayData = displayData

        // when
        sut.messageButtonPressed()

        // then
        XCTAssert(dummyView.numberOfSkeletons == displayData.numberOfSkeletons)
    }

    func test_givenTransactionViewDisplayDataWithoutTransactionsBO_whenICallMessageButtonPressed_thenCallProvideTransactions() {

        // given
        let displayData = TransactionDisplayData(numberOfSkeletons: 2, maximumNumberOfTransactions: 7, cardId: "1")
        sut.transactionDisplayData = displayData

        // when
        sut.messageButtonPressed()

        // then
        XCTAssert(dummyInteractor.isCalledProvideTransactions == true)
    }

    func test_givenTransactionViewDisplayDataWithoutTransactionsBO_whenICallMessageButtonPressed_thenCallProvideTransactionsWithTheSameCardId() {

        // given
        let cardId = "1"
        let displayData = TransactionDisplayData(numberOfSkeletons: 2, maximumNumberOfTransactions: 7, cardId: "1")
        sut.transactionDisplayData = displayData

        // when
        sut.messageButtonPressed()

        // then
        XCTAssert(dummyInteractor.cardId == cardId)
    }

    func test_givenTransactionViewDisplayDataWithoutTransactionsBO_whenICallMessageButtonPressed_thenCallAddTransactions() {

        // given
        let dummyTransactionsDisplayData = DummyTransactionsDisplayData()
        sut.displayData = dummyTransactionsDisplayData

        let displayData = TransactionDisplayData(numberOfSkeletons: 2, maximumNumberOfTransactions: 7, cardId: "1")
        sut.transactionDisplayData = displayData

        // when
        sut.messageButtonPressed()

        // then
        XCTAssert(dummyTransactionsDisplayData.isCalledAddTransactionsDisplayData == true)
    }

    func test_givenTransactionViewDisplayDataWithoutTransactionsBO_whenICallMessageButtonPressed_thenCallHideSkeleton() {

        // given
        let displayData = TransactionDisplayData(numberOfSkeletons: 2, maximumNumberOfTransactions: 7, cardId: "1")
        sut.transactionDisplayData = displayData

        // when
        sut.messageButtonPressed()

        // then
        XCTAssert(dummyView.isCalledHideSkeleton == true)
    }

    func test_givenTransactionViewDisplayDataWithoutTransactionsBO_whenICallMessageButtonPressed_thenCallShowTransactions() {

        // given
        let displayData = TransactionDisplayData(numberOfSkeletons: 2, maximumNumberOfTransactions: 7, cardId: "1")
        sut.transactionDisplayData = displayData

        // when
        sut.messageButtonPressed()

        // then
        XCTAssert(dummyView.isCalledShowTransactions == true)
    }

    func test_givenTransactionViewDisplayDataWithoutTransactionsBO_whenICallMessageButtonPressed_thenTransactionDisplayDataIsNotEmpty() {

        // given
        let displayData = TransactionDisplayData(numberOfSkeletons: 2, maximumNumberOfTransactions: 7, cardId: "1")
        sut.transactionDisplayData = displayData

        // when
        sut.messageButtonPressed()

        // then
        XCTAssert(dummyView.transactionsDisplayData != nil)
    }

    func test_givenTransactionViewDisplayDataWithoutTransactionsBO_whenICallMessageButtonPressed_thenTransactionViewDisplayDataShouldBeFilled() {

        // given
        let displayData = TransactionDisplayData(numberOfSkeletons: 2, maximumNumberOfTransactions: 7, cardId: "1")
        sut.transactionDisplayData = displayData

        // when
        sut.messageButtonPressed()

        // then
        XCTAssert(sut.transactionDisplayData != nil)
    }

    func test_givenTransactionViewDisplayDataWithoutTransactionsBO_whenICallMessageButtonPressed_thenTransactionViewDisplayDataAndTransactionsBOShouldBeMatch() {

        // given
        let displayData = TransactionDisplayData(numberOfSkeletons: 2, maximumNumberOfTransactions: 7, cardId: "1")
        sut.transactionDisplayData = displayData

        // when
        sut.messageButtonPressed()

        // then
        XCTAssert(sut.transactionDisplayData! == displayData)
        XCTAssert(sut.transactionDisplayData?.transactionsBO! === dummyInteractor.transactionsBO)
        XCTAssert(sut.transactionDisplayData?.transactionsBO?.transactions.count == dummyInteractor.transactionsBO?.transactions.count)
    }

    // MARK: delegate with error

    func test_givenADisplayDataWithCardIdAndDelegate_whenICallLoadTransactionsAndThereIsAnError_thenICallDelegateError() {

        // given
        dummyInteractor.forceError = true
        let dummyPresenterDelegate = DummyPresenterDelegate()
        sut.delegate = dummyPresenterDelegate

        let displayData = TransactionDisplayData(numberOfSkeletons: 2, maximumNumberOfTransactions: 7, cardId: "1")

        // when
        sut.loadTransactions(forDisplayData: displayData)

        // then
        XCTAssert(dummyPresenterDelegate.isCalledError == true)
    }

    func test_givenTransactionViewDisplayDataWithoutTransactionsBOAndDelegate_whenICallMessageButtonPressed_thenCallProvideTransactions() {

        // given
        dummyInteractor.forceError = true
        let dummyPresenterDelegate = DummyPresenterDelegate()
        sut.delegate = dummyPresenterDelegate

        let displayData = TransactionDisplayData(numberOfSkeletons: 2, maximumNumberOfTransactions: 7, cardId: "1")
        sut.transactionDisplayData = displayData

        // when
        sut.messageButtonPressed()

        // then
        XCTAssert(dummyPresenterDelegate.isCalledError == true)
    }

    class DummyInteractor: TransactionInteractorProtocol {

        var isCalledProvideTransactions = false
        var forceError = false
        var forceNoresults = false

        var cardId: String?
        var transactionsBO: TransactionsBO?
        var dummyEntities: [TransactionEntity]?

        func provideTransaction(forCardId cardId: String) -> Observable <ModelBO> {

            self.cardId = cardId

            self.isCalledProvideTransactions = true

            if forceNoresults {

                let transactionEntity = TransactionsEntity()
                transactionEntity.status = ConstantsHTTPCodes.NO_CONTENT
                let transactionsBO = TransactionsBO(transactionsEntity: transactionEntity)

                return Observable <ModelBO>.just(transactionsBO as ModelBO)
            } else if forceError {

                let errorBO = ErrorBO(error: ErrorEntity(message: "Error"))

                return Observable.error(ServiceError.GenericErrorBO(error: errorBO))
            } else {

                let dummyTransactionsEntity = TransactionsEntity()
                dummyTransactionsEntity.status = 200
                dummyTransactionsEntity.data = dummyEntities
                let transactionsBO = TransactionsBO(transactionsEntity: dummyTransactionsEntity)
                self.transactionsBO = transactionsBO

                return Observable <ModelBO>.just(transactionsBO as ModelBO)
            }
        }
    }

    class DummyView: TransactionViewProtocol {

        var isCalledShowTransactions = false
        var isCalledShowSkeleton = false
        var isCalledHideSkeleton = false
        var isCalledShowMessage = false
        var isCalledShowAllTransanctions = false
        var isCalledShowNoContentAction = false
        var isCalledShowDirectAccess = false

        var numberOfSkeletons = -1

        var transactionsDisplayData: TransactionsDisplayData?
        var dummyTransactionsDisplay: DummyTransactionsDisplayData?
        var message: TransactionMessageDisplayData?

        func loadTransactions(forDisplayData displayData: TransactionDisplayData) {
        }

        func showTransactions(transactionsDisplayData: TransactionsDisplayData) {
            self.transactionsDisplayData = transactionsDisplayData
            self.isCalledShowTransactions = true
        }

        func showSkeleton(rows: Int) {
            numberOfSkeletons = rows
            isCalledShowSkeleton = true
        }

        func hideSkeleton() {
            isCalledHideSkeleton = true
        }

        func show(message: TransactionMessageDisplayData) {
            isCalledShowMessage = true
            self.message = message
        }

        func showAllTransactions() {
            isCalledShowAllTransanctions = true
        }

        func estimateHeightForTransactionView(withTransactionsDisplayData transactionsDisplayData: TransactionsDisplayData) -> CGFloat {
            return 0
        }

        func showNoContentAction() {
            isCalledShowNoContentAction = true
        }

        func showDirectAccess() {
            isCalledShowDirectAccess = true
        }
    }

    class DummyTransactionsDisplayData: TransactionsDisplayData {

        var isCalledAddTransactionsDisplayData = false

        var transactions: [TransactionBO]?

        override func add(transactions: [TransactionBO]) {
            self.transactions = transactions
            self.isCalledAddTransactionsDisplayData = true
        }
    }

    class DummyPresenterDelegate: TransactionPresenterDelegate {

        var isCalledLoadedTransactions = false
        var isCalledError = false

        var loadedTransactions: TransactionsBO?
        var transactionsForCardId: String?

        func transactionPresenter(_ transactionPresenter: TransactionPresenter, loadedTransactions transactions: TransactionsBO, forCardId cardId: String) {
            isCalledLoadedTransactions = true
            loadedTransactions = transactions
            transactionsForCardId = cardId
        }

        func transactionPresenter(_ transactionPresenter: TransactionPresenter, error: ErrorBO) {
            isCalledError = true
        }

    }
}
