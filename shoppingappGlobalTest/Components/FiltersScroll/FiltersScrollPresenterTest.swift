//
//  FiltersScrollPresenterTest.swift
//  shoppingapp
//
//  Created by Arturo Marzo de la Fuente on 31/8/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import XCTest
import RxSwift

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class FiltersScrollPresenterTest: XCTestCase {

    var sut: FiltersScrollPresenter!

    override func setUp() {
        super.setUp()
        sut = FiltersScrollPresenter()
    }

    // MARK: - receivedNewFilters

    func test_givenAPresenter_whenICallReceivedNewFilters_thenCallReloadFilters() {

        let dummyView = DummyView()
        sut.view = dummyView
        let filter = FilterScrollDisplayData(title: "Concepto",
                                             filterType: "concept",
                                             isRemoveAllFiltersButton: false,
                                             labelAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_CONCEPT_FILTER_DETAIL_LABEL_ID,
                                             buttonAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_CONCEPT_FILTER_DETAIL_CLOSE_BUTTON_ID)

        // when
        sut.receivedNewFilters(filters: [filter])

        // then
        XCTAssert(dummyView.isCalledReloadFilters == true)
    }

    func test_givenArrayOfFilterScrollDisplayData_whenICallReceivedNewFilters_thenDisplayDataShouldBeFilled() {

        // given
        let filter = FilterScrollDisplayData(title: "Concepto",
                                             filterType: "concept",
                                             isRemoveAllFiltersButton: false,
                                             labelAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_CONCEPT_FILTER_DETAIL_LABEL_ID,
                                             buttonAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_CONCEPT_FILTER_DETAIL_CLOSE_BUTTON_ID)

        // when
        sut.receivedNewFilters(filters: [filter])

        // then
        XCTAssert(sut.displayData != nil)

    }

    func test_givenArrayOfFilterScrollDisplayData_whenICallReceivedNewFilters_thenDisplayDataShouldMatchWithParemeter() {

        // given
        let filter = FilterScrollDisplayData(title: "Concepto",
                                             filterType: "concept",
                                             isRemoveAllFiltersButton: false,
                                             labelAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_CONCEPT_FILTER_DETAIL_LABEL_ID,
                                             buttonAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_CONCEPT_FILTER_DETAIL_CLOSE_BUTTON_ID)
        let paremter = [filter]

        // when
        sut.receivedNewFilters(filters: [filter])

        // then
        XCTAssert(sut.displayData! == paremter)

    }

    // MARK: - removeFilterPushed

    func test_givenAPresenter_whenICallRemoveFilterPushed_thenCallRemoveFilter() {

        let dummyView = DummyView()
        sut.view = dummyView
        let filter = FilterScrollDisplayData(title: "Concepto",
                                             filterType: "concept",
                                             isRemoveAllFiltersButton: false,
                                             labelAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_CONCEPT_FILTER_DETAIL_LABEL_ID,
                                             buttonAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_CONCEPT_FILTER_DETAIL_CLOSE_BUTTON_ID)

        // when
        sut.removeFilterPushed(filter: filter)

        // then
        XCTAssert(dummyView.isCalledRemoveFilter == true)
    }

    func test_givenADisplayDataAndAFilterOfThisDisplayData_whenICallRemoveFilterPushed_thenTheFilterOfParameterShouldNotBeInDisplayData() {

        // given
        let filter1 = FilterScrollDisplayData(title: "Concepto1",
                                             filterType: "concept1",
                                             isRemoveAllFiltersButton: false,
                                             labelAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_CONCEPT_FILTER_DETAIL_LABEL_ID,
                                             buttonAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_CONCEPT_FILTER_DETAIL_CLOSE_BUTTON_ID)

        let filter2 = FilterScrollDisplayData(title: "Concepto2",
                                             filterType: "concept2",
                                             isRemoveAllFiltersButton: false,
                                             labelAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_CONCEPT_FILTER_DETAIL_LABEL_ID,
                                             buttonAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_CONCEPT_FILTER_DETAIL_CLOSE_BUTTON_ID)

        let filter3 = FilterScrollDisplayData(title: "Concepto3",
                                             filterType: "concept3",
                                             isRemoveAllFiltersButton: false,
                                             labelAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_CONCEPT_FILTER_DETAIL_LABEL_ID,
                                             buttonAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_CONCEPT_FILTER_DETAIL_CLOSE_BUTTON_ID)

        sut.displayData = [filter1, filter2, filter3]

        // when
        sut.removeFilterPushed(filter: filter2)

        // then
        XCTAssert(sut.displayData!.count == 2)
        XCTAssert(sut.displayData?.contains(filter2) == false)
    }

    func test_givenADisplayDataAndAFilterOfThisDisplayData_whenICallRemoveFilterPushed_thenICallViewViewReloadFilters() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let filter1 = FilterScrollDisplayData(title: "Concepto1",
                                              filterType: "concept1",
                                              isRemoveAllFiltersButton: false,
                                              labelAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_CONCEPT_FILTER_DETAIL_LABEL_ID,
                                              buttonAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_CONCEPT_FILTER_DETAIL_CLOSE_BUTTON_ID)

        let filter2 = FilterScrollDisplayData(title: "Concepto2",
                                              filterType: "concept2",
                                              isRemoveAllFiltersButton: false,
                                              labelAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_CONCEPT_FILTER_DETAIL_LABEL_ID,
                                              buttonAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_CONCEPT_FILTER_DETAIL_CLOSE_BUTTON_ID)

        let filter3 = FilterScrollDisplayData(title: "Concepto3",
                                              filterType: "concept3",
                                              isRemoveAllFiltersButton: false,
                                              labelAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_CONCEPT_FILTER_DETAIL_LABEL_ID,
                                              buttonAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_CONCEPT_FILTER_DETAIL_CLOSE_BUTTON_ID)

        sut.displayData = [filter1, filter2, filter3]

        // when
        sut.removeFilterPushed(filter: filter3)

        // then
        XCTAssert(dummyView.isCalledReloadFilters == true)
    }

    func test_givenADisplayDataAndAFilterOfThisDisplayData_whenICallRemoveFilterPushed_thenICallViewViewReloadFiltersWithAppropiateDisplayData() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let filter1 = FilterScrollDisplayData(title: "Concepto1",
                                              filterType: "concept1",
                                              isRemoveAllFiltersButton: false,
                                              labelAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_CONCEPT_FILTER_DETAIL_LABEL_ID,
                                              buttonAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_CONCEPT_FILTER_DETAIL_CLOSE_BUTTON_ID)

        let filter2 = FilterScrollDisplayData(title: "Concepto2",
                                              filterType: "concept2",
                                              isRemoveAllFiltersButton: false,
                                              labelAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_CONCEPT_FILTER_DETAIL_LABEL_ID,
                                              buttonAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_CONCEPT_FILTER_DETAIL_CLOSE_BUTTON_ID)

        let filter3 = FilterScrollDisplayData(title: "Concepto3",
                                              filterType: "concept3",
                                              isRemoveAllFiltersButton: false,
                                              labelAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_CONCEPT_FILTER_DETAIL_LABEL_ID,
                                              buttonAccessibilityId: ConstantsAccessibilities.TRANSACTIONS_LIST_CONCEPT_FILTER_DETAIL_CLOSE_BUTTON_ID)

        sut.displayData = [filter1, filter2, filter3]

        // when
        sut.removeFilterPushed(filter: filter3)

        // then
        XCTAssert(dummyView.filters!.count == 2)
        XCTAssert(dummyView.filters!.contains(filter3) == false)

    }

    // MARK: - removeFilterLonglyPushed

    func test_givenAPresenter_whenICallRemoveFilterLonglyPushed_thenCallRemoveFilterLonglyPushed() {

        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.removeFilterLonglyPushed()

        // then
        XCTAssert(dummyView.isCalledRemoveFilterLonglyPushed == true)
    }

    func test_givenAny_whenICallRemoveFilterLonglyPushed_thenDisplayDataShouldBeFilled() {

        // given

        // when
        sut.removeFilterLonglyPushed()

        // then
        XCTAssert(sut.displayData != nil)
    }

    func test_givenAny_whenICallRemoveFilterLonglyPushed_thenDisplayDataShouldContainsOneItemAndWithTheValidValues() {

        // given

        // when
        sut.removeFilterLonglyPushed()

        // then
        XCTAssert(sut.displayData!.count == 1)
        XCTAssert(sut.displayData![0].title == Localizables.common.key_delete_all_filters_text)
        XCTAssert(sut.displayData![0].filterType == FiltersScrollPresenter.type_filter_remove_all)
        XCTAssert(sut.displayData![0].isRemoveAllFiltersButton == true)

    }

    func test_givenAny_whenICallRemoveFilterLonglyPushed_thenICallViewReloadFilters() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given

        // when
        sut.removeFilterLonglyPushed()

        // then
        XCTAssert(dummyView.isCalledReloadFilters == true)

    }

    func test_givenAny_whenICallRemoveFilterLonglyPushed_thenICallViewReloadFiltersWithAppropiateDisplayData() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given

        // when
        sut.removeFilterLonglyPushed()

        // then
        XCTAssert(dummyView.filters!.count == 1)
        XCTAssert(dummyView.filters![0].title == Localizables.common.key_delete_all_filters_text)
        XCTAssert(dummyView.filters![0].filterType == FiltersScrollPresenter.type_filter_remove_all)
        XCTAssert(dummyView.filters![0].isRemoveAllFiltersButton == true)

    }

    // MARK: - removeAllFiltersPushed

    func test_givenAPresenter_whenICallRemoveAllFiltersPushed_thenCallRemoveAllFilters() {

        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.removeAllFiltersPushed()

        // then
        XCTAssert(dummyView.isCalledRemoveAllFilters == true)
    }

    class DummyView: FiltersScrollViewProtocol {

        var isCalledReloadFilters = false
        var isCalledRemoveFilter = false
        var isCalledRemoveFilterLonglyPushed = false
        var isCalledRemoveAllFilters = false

        var filters: [FilterScrollDisplayData]?

        func loadFilters(filters: [FilterScrollDisplayData]?) {
        }

        func reloadFilters(filters: [FilterScrollDisplayData]?) {
            self.isCalledReloadFilters = true
            self.filters = filters
        }

        func removeFilter(filter: FilterScrollDisplayData) {
            self.isCalledRemoveFilter = true
        }

        func removeFilterLonglyPushed() {
            self.isCalledRemoveFilterLonglyPushed = true
        }

        func removeAllFilters() {
            self.isCalledRemoveAllFilters = true
        }

    }
}
