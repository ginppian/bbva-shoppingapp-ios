//
//  TransactionDetailDisplayDataTest.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 20/11/2017.
//  Copyright © 2017 BBVA. All rights reserved.
//

import XCTest
import RxSwift

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class  TransactionDetailDisplayDataTest: XCTestCase {

    func test_givenTransactionDisplayItem_whenTransactionDetailDataGeneratedWithAllValues_thenIShowGenerateArrayWithFourItems() {

        //given
        let transactionEntity = TransactionEntityDummyGenerator.transactionEntity()

        let transactionBO = TransactionBO(transactionEntity: transactionEntity)

        let transactionDisplayItem = TransactionDisplayItem(fromTransactionBO: transactionBO!)
        // when
        let transactionDetailDisplayData = TransactionDetailDisplayData(withTransactionDetailDisplayData: transactionDisplayItem)

        // then
        XCTAssert(transactionDetailDisplayData.bottomItems.count == 4)
        XCTAssert(transactionDetailDisplayData.bottomItems[0].title == Localizables.transactions.key_card_movement_details_card_text)
        XCTAssert(transactionDetailDisplayData.bottomItems[0].value == transactionDisplayItem.contract.number)
        XCTAssert(transactionDetailDisplayData.bottomItems[0].accessibiltyTitle == ConstantsAccessibilities.TRANSACTION_DETAIL_CARD_TITLE)
        XCTAssert(transactionDetailDisplayData.bottomItems[0].accessibiltyValue == ConstantsAccessibilities.TRANSACTION_DETAIL_CONTRACT_NUMBER)
        XCTAssert(transactionDetailDisplayData.bottomItems[1].title == Localizables.transactions.key_card_movement_details_date_operation_text)
        XCTAssert(transactionDetailDisplayData.bottomItems[1].value == String(format: "%@h", arguments: [ transactionDisplayItem.operationDate!]))
        XCTAssert(transactionDetailDisplayData.bottomItems[1].accessibiltyTitle == ConstantsAccessibilities.TRANSACTION_DETAIL_OPERATION_DATE_TITLE)
        XCTAssert(transactionDetailDisplayData.bottomItems[1].accessibiltyValue == ConstantsAccessibilities.TRANSACTION_DETAIL_OPERATION_DATE_VALUE)
        XCTAssert(transactionDetailDisplayData.bottomItems[2].title == Localizables.transactions.key_card_movement_details_date_application_text)
        XCTAssert(transactionDetailDisplayData.bottomItems[2].value == String(format: "%@h", arguments: [ transactionDisplayItem.accountedDate!]))
        XCTAssert(transactionDetailDisplayData.bottomItems[2].accessibiltyTitle == ConstantsAccessibilities.TRANSACTION_DETAIL_VALUE_DATE_TITLE)
        XCTAssert(transactionDetailDisplayData.bottomItems[2].accessibiltyValue == ConstantsAccessibilities.TRANSACTION_DETAIL_VALUE_DATE_VALUE)
        XCTAssert(transactionDetailDisplayData.bottomItems[3].title == Localizables.transactions.key_card_movement_details_folio_text)
        XCTAssert(transactionDetailDisplayData.bottomItems[3].value == transactionDisplayItem.additionalInformation?.reference)
        XCTAssert(transactionDetailDisplayData.bottomItems[3].accessibiltyTitle == ConstantsAccessibilities.TRANSACTION_DETAIL_FOLIO_TITLE)
        XCTAssert(transactionDetailDisplayData.bottomItems[3].accessibiltyValue == ConstantsAccessibilities.TRANSACTION_DETAIL_FOLIO_VALUE)
    }

    func test_givenTransactionDisplayItem_whenTransactionDetailDataGeneratedAndAccountedDateAndReferenceAndContractAreEmpty_thenIShowGenerateArrayWithOneItems() {

        //given
        var transactionEntity = TransactionEntityDummyGenerator.transactionEntity()
        transactionEntity.accountedDate = nil
        transactionEntity.additionalInformation?.reference = nil
        transactionEntity.contract.number = nil

        let transactionBO = TransactionBO(transactionEntity: transactionEntity)

        let transactionDisplayItem = TransactionDisplayItem(fromTransactionBO: transactionBO!)
        // when
        let transactionDetailDisplayData = TransactionDetailDisplayData(withTransactionDetailDisplayData: transactionDisplayItem)

        // then
        XCTAssert(transactionDetailDisplayData.bottomItems.count == 1)
        XCTAssert(transactionDetailDisplayData.bottomItems[0].title == Localizables.transactions.key_card_movement_details_date_operation_text)
        XCTAssert(transactionDetailDisplayData.bottomItems[0].value == String(format: "%@h", arguments: [ transactionDisplayItem.operationDate!]))
        XCTAssert(transactionDetailDisplayData.bottomItems[0].accessibiltyTitle == ConstantsAccessibilities.TRANSACTION_DETAIL_OPERATION_DATE_TITLE)
        XCTAssert(transactionDetailDisplayData.bottomItems[0].accessibiltyValue == ConstantsAccessibilities.TRANSACTION_DETAIL_OPERATION_DATE_VALUE)
    }

}
