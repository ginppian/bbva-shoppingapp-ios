//
//  TransactionDetailComponentPresenterTest.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 21/11/2017.
//  Copyright © 2017 BBVA. All rights reserved.
//

import XCTest
#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class  TransactionDetailComponentPresenterTest: XCTestCase {

    var sut: TransactionDetailComponentPresenter!

    override func setUp() {
        super.setUp()
        sut = TransactionDetailComponentPresenter()
    }

    override func tearDown() {
        super.tearDown()
    }

    func test_givenTransactionDetailDisplayData_whenICallShowTransactionDetailWithDisplayTransactionDetailAndNotPending_thenICallSetTopTransactionDetailWithoutPendingAndSetTransactionDetailHeight() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        let transactionEntity = TransactionEntityDummyGenerator.transactionEntity()
        let transactionBO = TransactionBO(transactionEntity: transactionEntity)
        let transactionDisplayItem = TransactionDisplayItem(fromTransactionBO: transactionBO!)
        let displayData = TransactionDetailDisplayData(withTransactionDetailDisplayData: transactionDisplayItem)

        // when
        sut.showTransactionDetail(withDisplayData: displayData, withExpandable: false)

        // then
        XCTAssert(dummyView.isCalledTransactionDetailHeight == true)

    }
    func test_givenTransactionDetailDisplayData_whenICallShowTransactionDetailWithDisplayTransactionDetailAndPendingAndCollapsed_thenICallSetTopTransactionDetailWithPendingCollapsedAndSetTransactionDetailHeight() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        var transactionEntity = TransactionEntityDummyGenerator.transactionEntity()
        transactionEntity.status = IdNameEntity(id: "PENDING", name: "PENDING")
        let transactionBO = TransactionBO(transactionEntity: transactionEntity)
        let transactionDisplayItem = TransactionDisplayItem(fromTransactionBO: transactionBO!)
        let displayData = TransactionDetailDisplayData(withTransactionDetailDisplayData: transactionDisplayItem)

        // when
        sut.showTransactionDetail(withDisplayData: displayData, withExpandable: false)

        // then
        XCTAssert(dummyView.isCalledSetTopTransactionDetailWithPendingCollapsed == true)
        XCTAssert(dummyView.isCalledTransactionDetailHeight == true)

    }
    func test_givenTransactionDetailDisplayData_whenICallShowTransactionDetailWithDisplayTransactionDetailAndPendingExpanded_thenICallSetTopTransactionDetailWithPendingExpandedAndSetTransactionDetailHeight() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        var transactionEntity = TransactionEntityDummyGenerator.transactionEntity()
        transactionEntity.status = IdNameEntity(id: "PENDING", name: "PENDING")

        let transactionBO = TransactionBO(transactionEntity: transactionEntity)
        let transactionDisplayItem = TransactionDisplayItem(fromTransactionBO: transactionBO!)
        let displayData = TransactionDetailDisplayData(withTransactionDetailDisplayData: transactionDisplayItem)

        // when
        sut.showTransactionDetail(withDisplayData: displayData, withExpandable: true)

        // then
        XCTAssert(dummyView.isCalledSetTopTransactionDetailWithPendingExpanded == true)
        XCTAssert(dummyView.isCalledTransactionDetailHeight == true)

    }
    class DummyView: TransactionDetailViewComponentProtocol {

        var transactionDetailDisplayData: TransactionDetailDisplayData?
        var isCalledTransactionDetailHeight: Bool = false
        var isCalledSetTopTransactionDetailWithPendingCollapsed: Bool = false
        var isCalledSetTopTransactionDetailWithPendingExpanded: Bool = false

        func setTopTransactionDetailWithPendingExpanded(withExpandableData displayData: ExpandableViewDisplayData) {
            isCalledSetTopTransactionDetailWithPendingExpanded = true
        }

        func setTopTransactionDetailWithPendingCollapsed(withExpandableData displayData: ExpandableViewDisplayData) {
            isCalledSetTopTransactionDetailWithPendingCollapsed = true
        }

        func setTransactionDetailHeight() {
            isCalledTransactionDetailHeight = true
        }

        func setTransactionDetailData(withDisplayData displayTransactionDetail: TransactionDetailDisplayData) {
            transactionDetailDisplayData = displayTransactionDetail
        }

    }

}
