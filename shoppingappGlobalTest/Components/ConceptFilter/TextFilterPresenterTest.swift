//
//  TextFilterPresenterTest.swift
//  shoppingapp
//
//  Created by jesus.martinez on 27/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class TextFilterPresenterTest: XCTestCase {

    let sut = TextFilterPresenter()

    // MARK: - onConfigureView

    func test_givenADisplayData_whenICallOnConfigureView_thenICallViewSetPlaceHolderText() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        sut.displayData = TextFilterDisplayData(placeHolderTitle: "placeHolder", maximumLength: nil, validCharacterSet: nil)

        // when
        sut.onConfigureView()

        // then
        XCTAssert(dummyView.isCalledSetPlaceHolderText == true)

    }

    func test_givenADisplayData_whenICallOnConfigureView_thenICallViewSetPlaceHolderTextWithDisplayDataPlaceHolderValue() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        sut.displayData = TextFilterDisplayData(placeHolderTitle: "placeHolder", maximumLength: nil, validCharacterSet: nil)

        // when
        sut.onConfigureView()

        // then
        XCTAssert(dummyView.placeHolderSent == sut.displayData?.placeHolderTitle)

    }

    func test_givenADisplayDataWithMaximumNumberLength_whenICallOnConfigureView_thenICallViewsetMaximumNumberOfCharacters() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        sut.displayData = TextFilterDisplayData(placeHolderTitle: "placeHolder", maximumLength: 33, validCharacterSet: nil)

        // when
        sut.onConfigureView()

        // then
        XCTAssert(dummyView.isCalledsetMaximumNumberOfCharacters == true)

    }

    func test_givenADisplayDataWithMaximumNumberLength_whenICallOnConfigureView_thenICallViewsetMaximumNumberOfCharactersWithMaximumLengthAsDisplayData() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        sut.displayData = TextFilterDisplayData(placeHolderTitle: "placeHolder", maximumLength: 33, validCharacterSet: nil)

        // when
        sut.onConfigureView()

        // then
        XCTAssert(dummyView.maximumSent == sut.displayData?.maximumLength!)

    }

    func test_givenADisplayDataWithMaximumNumberLength_whenICallOnConfigureView_thenICallViewSetValidCharacters() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        sut.displayData = TextFilterDisplayData(placeHolderTitle: "placeHolder", maximumLength: nil, validCharacterSet: .alphanumerics)

        // when
        sut.onConfigureView()

        // then
        XCTAssert(dummyView.isCalledSetValidCharacters == true)

    }

    func test_givenADisplayDataWithMaximumNumberLength_whenICallOnConfigureView_thenICallViewSetValidCharactersWithValidCharacterSetAsDisplayData() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        sut.displayData = TextFilterDisplayData(placeHolderTitle: "placeHolder", maximumLength: nil, validCharacterSet: .alphanumerics)

        // when
        sut.onConfigureView()

        // then
        XCTAssert(dummyView.charactersSetSent == sut.displayData?.validCharacterSet!)

    }

    func test_givenDisplayDataWithoutDefaultValue_whenICallOnConfigureView_thenIDoNotCallViewSetText() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.displayData = TextFilterDisplayData(placeHolderTitle: "placeHolder", maximumLength: nil, validCharacterSet: .alphanumerics)

        // when
        sut.onConfigureView()

        // then
        XCTAssert(dummyView.isCalledSetText == false)

    }

    func test_givenDisplayDataWithDefaultValue_whenICallOnConfigureView_thenICallViewSetText() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.displayData = TextFilterDisplayData(placeHolderTitle: "placeHolder", maximumLength: nil, validCharacterSet: .alphanumerics, defaultValue: "defaultValue")

        // when
        sut.onConfigureView()

        // then
        XCTAssert(dummyView.isCalledSetText == true)
    }

    func test_givenDisplayDataWithDefaultValue_whenICallOnConfigureView_thenICallViewSetTextWithTheDefaultValue() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.displayData = TextFilterDisplayData(placeHolderTitle: "placeHolder", maximumLength: nil, validCharacterSet: .alphanumerics, defaultValue: "defaultValue")

        // when
        sut.onConfigureView()

        // then
        XCTAssert(dummyView.valueText == sut.displayData?.defaultValue)

    }

    func test_givenDisplayDataWithNotDefaultValue_whenICallOnConfigureView_thenICallViewClearInpunt() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.displayData = TextFilterDisplayData(placeHolderTitle: "placeHolder", maximumLength: nil, validCharacterSet: .alphanumerics, defaultValue: nil)

        // when
        sut.onConfigureView()

        // then
        XCTAssert(dummyView.isCalledClearInput == true)
    }

    func test_givenDisplayDataWithNotDefaultValue_whenICallOnConfigureView_thenICallViewClearInpuntWithEmptyString() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.displayData = TextFilterDisplayData(placeHolderTitle: "placeHolder", maximumLength: nil, validCharacterSet: .alphanumerics, defaultValue: nil)

        // when
        sut.onConfigureView()

        // then
        XCTAssert(dummyView.valueToClear.isEmpty)

    }

    // MARK: - removeAction

    func test_givenAView_whenICallRemoveAction_thenICallViewClearInput() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.removeAction()

        // then
        XCTAssert(dummyView.isCalledClearInput == true)

    }

    func test_givenAView_whenICallRemoveAction_thenICallViewClearInputWithAnEmptyString() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.removeAction()

        // then
        XCTAssert(dummyView.valueToClear.isEmpty)

    }

    // MARK: - textFieldDidBeginEditing

    func test_givenAView_whenICallTextFieldDidBeginEditing_thenICallViewClearPlaceHolderText() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.textFieldDidBeginEditing()

        // then
        XCTAssert(dummyView.isCalledClearPlaceHolderText == true)

    }

        // MARK: - textFieldDidEndEditing

    func test_givenADisplayData_whenICallTextFieldDidEndEditing_thenICallViewSetPlaceHolderText() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        sut.displayData = TextFilterDisplayData(placeHolderTitle: "placeHolder", maximumLength: nil, validCharacterSet: nil)

        // when
        sut.textFieldDidEndEditing()

        // then
        XCTAssert(dummyView.isCalledSetPlaceHolderText == true)

    }

    func test_givenADisplayData_whenICallTextFieldDidEndEditing_thenICallViewSetPlaceHolderTextWithDisplayDataPlaceHolderValue() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        sut.displayData = TextFilterDisplayData(placeHolderTitle: "placeHolder", maximumLength: nil, validCharacterSet: nil)

        // when
        sut.textFieldDidEndEditing()

        // then
        XCTAssert(dummyView.placeHolderSent == sut.displayData?.placeHolderTitle)

    }

    class DummyView: TextFilterViewProtocol {

        var isCalledClearInput = false
        var isCalledClearPlaceHolderText = false
        var isCalledSetPlaceHolderText = false
        var isCalledsetMaximumNumberOfCharacters = false
        var isCalledSetValidCharacters = false
        var isCalledSetText = false

        var valueText = "valueText"
        var valueToClear = "valueToClear"
        var placeHolderSent = "placeHolderSent"
        var maximumSent: Int?
        var charactersSetSent: CharacterSet?

        func clearInput(withValue value: String) {
            isCalledClearInput = true
            valueToClear = value
        }

        func setText(withText text: String) {
            isCalledSetText = true
            valueText = text
        }

        func clearPlaceHolderText() {
            isCalledClearPlaceHolderText = true
        }

        func setPlaceHolder(withText text: String) {
            isCalledSetPlaceHolderText = true
            placeHolderSent = text
        }

        func setMaximumNumberOfCharacters(withMaximum maximum: Int) {
            isCalledsetMaximumNumberOfCharacters = true
            maximumSent = maximum

        }

        func setValidCharacters(withCharactersSet charactersSet: CharacterSet) {
            isCalledSetValidCharacters = true
            charactersSetSent = charactersSet
        }

    }

}
