//
//  AmountTextfieldPresenterTest.swift
//  shoppingapp
//
//  Created by jesus.martinez on 2/8/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import XCTest
import RxSwift

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class AmountTextfieldPresenterTest: XCTestCase {

    let sut = AmountTextfieldPresenter()

    // MARK: - OnConfigureView

    func test_givenViewAndDisplayDataAndCurrency_whenICallOnConfigureView_thenICallViewSetPlaceHolder() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let displayData = AmountTextfieldDisplayData(placeHolder: Localizables.transactions.key_transactions_from_text, initialAmount: nil, currency: nil, shouldHideErrorAutomatically: true)
        sut.displayData = displayData
        sut.currencyBO = CurrencyBO(code: "CLP", position: .left, needDecimals: false)

        // when
        sut.onConfigureView()

        // then
        XCTAssert(dummyView.isCalledSetPlaceHolder == true)

    }

    func test_givenViewAndDisplayDataAndCurrency_whenICallOnConfigureView_thenICallViewSetPlaceHolderWithPlaceHolderSpace() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let displayData = AmountTextfieldDisplayData(placeHolder: Localizables.transactions.key_transactions_from_text, initialAmount: nil, currency: nil, shouldHideErrorAutomatically: true)
        sut.displayData = displayData
        sut.currencyBO = CurrencyBO(code: "CLP", position: .left, needDecimals: false)

        // when
        sut.onConfigureView()

        // then
        XCTAssert(dummyView.placeHolderValue == displayData.placeHolder)

    }

    func test_givenViewAndDisplayDataCurrencyThatAllowsDecimals_whenICallOnConfigureView_thenICallViewConfigureDecimalKeyboard() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let displayData = AmountTextfieldDisplayData(placeHolder: Localizables.transactions.key_transactions_from_text, initialAmount: nil, currency: nil, shouldHideErrorAutomatically: true)
        sut.displayData = displayData

        let dummyInteractor = DummyInteractor()
        dummyInteractor.currencyCode = "USD"
        sut.interactor = dummyInteractor

        // when
        sut.onConfigureView()

        // then
        XCTAssert(dummyView.isCalledDecimalKeyboard == true)

    }

    func test_givenViewAndDisplayDataCurrencyThatDoesNotAllowsDecimals_whenICallOnConfigureView_thenICallViewConfigureNumberKeyboard() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let displayData = AmountTextfieldDisplayData(placeHolder: Localizables.transactions.key_transactions_from_text, initialAmount: nil, currency: nil, shouldHideErrorAutomatically: true)
        sut.displayData = displayData
        sut.currencyBO = CurrencyBO(code: "CLP", position: .left, needDecimals: false)

        let dummyInteractor = DummyInteractor()
        dummyInteractor.currencyCode = "CLP"
        sut.interactor = dummyInteractor

        // when
        sut.onConfigureView()

        // then
        XCTAssert(dummyView.isCalledConfigureNumberKeyboard == true)

    }

    func test_givenView_whenICallOnConfigureView_thenICallViewHideError() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.onConfigureView()

        // then
        XCTAssert(dummyView.isCalledHideError == true)

    }

    func test_givenViewAndInteractorAndDisplayDataWithoutCurrency_whenICallOnConfigureView_thenICallInteractorProvideCurrency() {

        // given

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // when
        sut.onConfigureView()

        // then
        XCTAssert(dummyInteractor.isCalledProvideCurrency == true)

    }

    func test_givenViewAndDisplayDataWithCurrency_whenICallOnConfigureView_thenTheCurrencyShouldBeCorrect() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let currency = CurrencyBO(code: "CLP", position: .left, needDecimals: false)
        let displayData = AmountTextfieldDisplayData(placeHolder: Localizables.transactions.key_transactions_from_text, initialAmount: "25", currency: currency, shouldHideErrorAutomatically: true)
        sut.displayData = displayData

        // when
        sut.onConfigureView()

        // then
        XCTAssert(sut.currencyBO.code == currency.code)

    }

    func test_givenViewAndInteractor_whenICallOnConfigureView_thenFormatterShouldBeFilled() {

        // given

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // when
        sut.onConfigureView()

        // then
        XCTAssert(sut.formatter != nil)

    }

    func test_givenViewAndInteractor_whenICallOnConfigureView_thenCurrencyBOShouldBeFilled() {

        // given

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // when
        sut.onConfigureView()

        // then
        XCTAssert(sut.currencyBO != nil)

    }

    func test_givenViewAndDisplayDataWithInitialAmount_whenICallOnConfigureView_thenICallUpdateViewWithFormattedAmount() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let displayData = AmountTextfieldDisplayData(placeHolder: Localizables.transactions.key_transactions_from_text, initialAmount: "25", currency: nil, shouldHideErrorAutomatically: true)
        sut.displayData = displayData
        sut.currencyBO = CurrencyBO(code: "CLP", position: .left, needDecimals: false)

        // when
        sut.onConfigureView()

        // then
        XCTAssert(dummyView.isCalledUpdateViewAmountText == true)

    }

    func test_givenViewAndDisplayDataWithInitialAmount_whenICallOnConfigureView_thenICallViewUpdateAmountTextWithTheAmountText() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let displayData = AmountTextfieldDisplayData(placeHolder: Localizables.transactions.key_transactions_from_text, initialAmount: "25", currency: nil, shouldHideErrorAutomatically: true)
        sut.displayData = displayData
        sut.currencyBO = CurrencyBO(code: "CLP", position: .left, needDecimals: false)

        // when
        sut.onConfigureView()

        // then
        XCTAssert(sut.amountText == "25")

    }

    // MARK: - crossAction

    func test_givenView_whenICallCrossAction_thenICallViewClearTextfield() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.crossAction()

        // then
        XCTAssert(dummyView.isCalledClearTextfield == true)

    }

    func test_givenView_whenICallCrossAction_thenICallViewClearTextfieldWithEmptyString() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.crossAction()

        // then
        XCTAssert(dummyView.valueToClear.isEmpty)

    }

    // MARK: - textfieldStartEditing

    func test_givenViewAndDisplayData_whenICallTextfieldStartEditing_thenICallViewSetPlaceHolder() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let displayData = AmountTextfieldDisplayData(placeHolder: Localizables.transactions.key_transactions_from_text, initialAmount: nil, currency: nil, shouldHideErrorAutomatically: true)
        sut.displayData = displayData

        // when
        sut.textfieldStartEditing()

        // then
        XCTAssert(dummyView.isCalledSetPlaceHolder == true)

    }

    func test_givenViewAndDisplayData_whenICallTextfieldStartEditing_thenICallViewSetPlaceHolderWithPlaceHolderOfDisplayData() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let displayData = AmountTextfieldDisplayData(placeHolder: Localizables.transactions.key_transactions_from_text, initialAmount: nil, currency: nil, shouldHideErrorAutomatically: true)
        sut.displayData = displayData

        // when
        sut.textfieldStartEditing()

        // then
        XCTAssert(dummyView.placeHolderValue == displayData.placeHolder)

    }

    func test_givenView_whenICallTextfieldStartEditing_thenICallViewNotHideError() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.textfieldStartEditing()

        // then
        XCTAssert(dummyView.isCalledHideError == false)

    }

    func test_givenView_whenICallTextfieldStartEditing_thenICallViewUpdateAmountText() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.textfieldStartEditing()

        // then
        XCTAssert(dummyView.isCalledUpdateViewAmountText == true)

    }

    func test_givenViewAndAmountText_whenICallTextfieldStartEditing_thenICallViewUpdateAmountTextWithTheAmountText() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        sut.amountText = "12"

        // when
        sut.textfieldStartEditing()

        // then
        XCTAssert(sut.amountText == "12")

    }

    func test_givenView_whenICallTextfieldStartEditing_thenICallViewShowRemoveButton() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.textfieldStartEditing()

        // then
        XCTAssert(dummyView.isCalledShowRemoveButton == true)

    }

    // MARK: - textfieldEndEditing

    func test_givenViewAndDisplayData_whenICallTextfieldEndEditingWithEmptyString_thenICallViewSetPlaceHolder() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let displayData = AmountTextfieldDisplayData(placeHolder: Localizables.transactions.key_transactions_from_text, initialAmount: nil, currency: nil, shouldHideErrorAutomatically: true)
        sut.displayData = displayData
        sut.currencyBO = CurrencyBO(code: "CLP", position: .left, needDecimals: true)

        // when
        sut.textfieldEndEditing(withText: "")

        // then
        XCTAssert(dummyView.isCalledUpdateViewAmountText == true)

    }

    func test_givenViewAndDisplayData_whenICallTextfieldEndEditingWithEmptyString_thenICallViewSetPlaceHolderWithPlaceHolder() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let displayData = AmountTextfieldDisplayData(placeHolder: Localizables.transactions.key_transactions_from_text, initialAmount: nil, currency: nil, shouldHideErrorAutomatically: true)
        sut.displayData = displayData
        sut.currencyBO = CurrencyBO(code: "CLP", position: .left, needDecimals: true)

        // when
        sut.textfieldEndEditing(withText: "")

        // then
        XCTAssert(dummyView.placeHolderValue == displayData.placeHolder)

    }

    func test_givenViewAndDisplayData_whenICallTextfieldEndEditingWithNonEmptyString_thenIDoNotCallViewSetPlaceHolder() {

        // given
        let displayData = AmountTextfieldDisplayData(placeHolder: Localizables.transactions.key_transactions_from_text, initialAmount: nil, currency: nil, shouldHideErrorAutomatically: true)
        sut.displayData = displayData
        sut.currencyBO = CurrencyBO(code: "CLP", position: .left, needDecimals: true)

        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.textfieldEndEditing(withText: "12")

        // then
        XCTAssert(dummyView.isCalledSetPlaceHolder == false)

    }

    func test_givenViewAndCurrency_whenICallTextfieldEndEditing_thenICallViewInformEndEditing() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        sut.currencyBO = CurrencyBO(code: "CLP", position: .left, needDecimals: true)

        // when
        sut.textfieldEndEditing(withText: "")

        // then
        XCTAssert(dummyView.isCalledInformEndEditing == true)

    }

    func test_givenView_whenICallTextfieldEndEditing_thenICallViewInformEndEditingWithNSDecimalNumberOfTextAsParamater() {

        // expected
        let text = "12"

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.textfieldEndEditing(withText: text)

        // then
        XCTAssert(dummyView.amountToInform == NSDecimalNumber(string: text))

    }

    func test_givenView_whenICallTextfieldEndEditingWithANonEmptyString_thenICallUpdateViewWithFormattedAmount() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.textfieldEndEditing(withText: "12")

        // then
        XCTAssert(dummyView.isCalledUpdateViewAmountText == true)

    }

    func test_givenViewAndCurrency_whenICallTextfieldEndEditingANonEmptyString_thenICallUpdateViewWithFormattedAmount() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        sut.currencyBO = CurrencyBO(code: "CLP", position: .left, needDecimals: true)

        // when
        sut.textfieldEndEditing(withText: "")

        // then
        XCTAssert(dummyView.isCalledUpdateViewAmountText == true)

    }

    func test_givenViewAndCurrency_whenICallTextfieldEndEditingANonEmptyString_thenICallUpdateViewWithFormattedAmountWithEmptyString() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        sut.currencyBO = CurrencyBO(code: "CLP", position: .left, needDecimals: true)

        // when
        sut.textfieldEndEditing(withText: "")

        // then
        XCTAssert(dummyView.amountText.isEmpty)

    }

    func test_givenViewAndFormatter_whenICallTextfieldEndEditingWithANonEmptyString_thenICallFormatterFormat() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyFormatter = FormatterDummy(codeCurrency: "USD")

        sut.formatter = dummyFormatter

        // when
        sut.textfieldEndEditing(withText: "12")

        // then
        XCTAssert(dummyFormatter.isCalledFormat == true)

    }

    func test_givenView_whenICallTextfieldEndEditing_thenAmountTextShouldMatchWithParameter() {

        // expected
        let amountText = "12"

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.textfieldEndEditing(withText: amountText)

        // then
        XCTAssert(sut.amountText == amountText)

    }

    func test_givenView_whenICallTextfieldEndEditingWithANonEmptyString_thenICallViewUpdateViewWithTheSameTextAsParamter() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.textfieldEndEditing(withText: "12")

        // then
        XCTAssert(dummyView.amountText == "12")

    }

    func test_givenViewAndFormatter_whenICallTextfieldEndEditingWithANonEmptyString_thenICallViewUpdateViewWithAmountFormatted() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let formatter = AmountFormatter(codeCurrency: "USD")

        sut.formatter = formatter

        // when
        sut.textfieldEndEditing(withText: "12")

        // then
        XCTAssert(dummyView.amountText == "$ 12")

    }

    func test_givenView_whenICallTextfieldEndEditing_thenICallViewHideRemoveButton() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.textfieldEndEditing(withText: "12")

        // then
        XCTAssert(dummyView.isCalledHideRemoveButton == true)

    }

    // MARK: - shouldChangeCharacters

    func test_givenCurrency_whenICallShouldChangeCharactersWithTextEmpty_thenIReturnTrue() {

        // given
        sut.currencyBO = CurrencyBO(code: "CLP", position: .left, needDecimals: true)

        // when
        let result = sut.shouldChangeCharacters(forText: "", withNewText: "", inRange: NSRange())

        // then
        XCTAssert(result == true)

    }

    func test_givenACurrencyThatsNotAllowDecimals_whenICallShouldChangeCharacters_thenIReturnTrue() {

        // given
        let currency = CurrencyBO(code: "CLP", position: .left, needDecimals: false)

        sut.currencyBO = currency

        // when
        let result = sut.shouldChangeCharacters(forText: "", withNewText: "", inRange: NSRange())

        // then
        XCTAssert(result == true)

    }

    func test_givenACurrencyWithDecimals_whenICallShouldChangeCharactersWithNumberWithoutLocaleSeparatorAndNewTextWithoutLocaleSeparator_thenIReturnTrue() {

        // given
        sut.currencyBO = CurrencyBO(code: "USD", position: .left, needDecimals: true)

        // when
        let result = sut.shouldChangeCharacters(forText: "12", withNewText: "2", inRange: NSRange())

        // then
        XCTAssert(result == true)

    }

    func test_givenACurrencyWithDecimals_whenICallShouldChangeCharactersWithNumberLocaleSeparatorAndNewTextWithoutLocaleSeparator_thenIReturnTrue() {

        // given
        let localeSeparator = AmountFormatter.decimalFormatter(locale: Locale.current).decimalSeparator!
        sut.currencyBO = CurrencyBO(code: "USD", position: .left, needDecimals: true)

        // when
        let result = sut.shouldChangeCharacters(forText: "12" + localeSeparator, withNewText: "2", inRange: NSRange())

        // then
        XCTAssert(result == true)

    }

    func test_givenACurrencyWithDecimals_whenICallShouldChangeCharactersWithNumberLocaleSeparatorAndNewTextLocaleSeparator_thenIReturnFalse() {

        // given
        let localeSeparator = AmountFormatter.decimalFormatter(locale: Locale.current).decimalSeparator!
        sut.currencyBO = CurrencyBO(code: "USD", position: .left, needDecimals: true)

        // when
        let result = sut.shouldChangeCharacters(forText: "12" + localeSeparator, withNewText: localeSeparator, inRange: NSRange())

        // then
        XCTAssert(result == false)

    }

    func test_givenACurrencyWithDecimals_whenICallShouldChangeCharactersWithNumberWithOneDecimalAndNewTextANumberAtRangeEnd_thenIReturnTrue() {

        // given
        let localeSeparator = AmountFormatter.decimalFormatter(locale: Locale.current).decimalSeparator!
        sut.currencyBO = CurrencyBO(code: "USD", position: .left, needDecimals: true)

        // when
        let oldText = "12" + localeSeparator + "3"
        let newText = "2"

        let result = sut.shouldChangeCharacters(forText: oldText, withNewText: newText, inRange: NSRange(location: oldText.count, length: 0))

        // then
        XCTAssert(result == true)

    }

    func test_givenACurrencyWithDecimals_whenICallShouldChangeCharactersWithNumberWithTwoDecimalAndNewTextANumberAtRangeEnd_thenIReturnFalse() {

        // given
        let localeSeparator = AmountFormatter.decimalFormatter(locale: Locale.current).decimalSeparator!
        sut.currencyBO = CurrencyBO(code: "USD", position: .left, needDecimals: true)

        // when
        let oldText = "12" + localeSeparator + "33"
        let newText = "2"

        let result = sut.shouldChangeCharacters(forText: oldText, withNewText: newText, inRange: NSRange(location: oldText.count, length: 0))

        // then
        XCTAssert(result == false)

    }

    func test_givenACurrencyWithDecimals_whenICallShouldChangeCharactersWithNumberWithTwoDecimalAndNewTextANumberBeforeDecimalSeparator_thenIReturnTrue() {

        // given
        let localeSeparator = AmountFormatter.decimalFormatter(locale: Locale.current).decimalSeparator!
        sut.currencyBO = CurrencyBO(code: "USD", position: .left, needDecimals: true)

        // when
        let oldText = "12" + localeSeparator + "33"
        let newText = "2"

        let result = sut.shouldChangeCharacters(forText: oldText, withNewText: newText, inRange: NSRange(location: 1, length: 0))

        // then
        XCTAssert(result == true)

    }

    //--------------

    func test_givenDisplayDataNil_whenICallShouldChangeCharactersWithTextEmpty_thenINotCallHideError() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        sut.displayData = nil
        dummyView.isCalledHideError = false

        // when
        _ = sut.shouldChangeCharacters(forText: "", withNewText: "", inRange: NSRange())

        // then
        XCTAssert(dummyView.isCalledHideError == false)

    }

    func test_givenDisplayDataWithShouldHideErrorAutomaticallyFalse_whenICallShouldChangeCharactersWithTextEmpty_thenINotCallHideError() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        let displayData = AmountTextfieldDisplayData(placeHolder: Localizables.transactions.key_transactions_from_text, initialAmount: nil, currency: nil, shouldHideErrorAutomatically: false)
        sut.displayData = displayData
        dummyView.isCalledHideError = false

        // when
        _ = sut.shouldChangeCharacters(forText: "", withNewText: "", inRange: NSRange())

        // then
        XCTAssert(dummyView.isCalledHideError == false)

    }

    func test_givenDisplayDataWithShouldHideErrorAutomaticallyTrue_whenICallShouldChangeCharactersWithTextEmpty_thenICallHideError() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let displayData = AmountTextfieldDisplayData(placeHolder: Localizables.transactions.key_transactions_from_text, initialAmount: nil, currency: nil, shouldHideErrorAutomatically: true)
        sut.displayData = displayData
        dummyView.isCalledHideError = false

        // when
        _ = sut.shouldChangeCharacters(forText: "", withNewText: "", inRange: NSRange())

        // then
        XCTAssert(dummyView.isCalledHideError == true)

    }

    func test_givenDisplayDataWithShouldHideErrorAutomaticallyTrueAndCurrencyBOWithDecimals_whenICallShouldChangeCharactersWithTextEmpty_thenICallHideError() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let displayData = AmountTextfieldDisplayData(placeHolder: Localizables.transactions.key_transactions_from_text, initialAmount: nil, currency: nil, shouldHideErrorAutomatically: true)
        sut.displayData = displayData
        dummyView.isCalledHideError = false

        sut.currencyBO = CurrencyBO(code: "CLP", position: .left, needDecimals: true)

        // when
        _ = sut.shouldChangeCharacters(forText: "", withNewText: "", inRange: NSRange())

        // then
        XCTAssert(dummyView.isCalledHideError == true)

    }

    func test_givenDisplayDataWithShouldHideErrorAutomaticallyFalseAndCurrencyBOWithDecimals_whenICallShouldChangeCharactersWithTextEmpty_thenINotCallHideError() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let displayData = AmountTextfieldDisplayData(placeHolder: Localizables.transactions.key_transactions_from_text, initialAmount: nil, currency: nil, shouldHideErrorAutomatically: false)
        sut.displayData = displayData
        dummyView.isCalledHideError = false

        sut.currencyBO = CurrencyBO(code: "CLP", position: .left, needDecimals: true)

        // when
        _ = sut.shouldChangeCharacters(forText: "", withNewText: "", inRange: NSRange())

        // then
        XCTAssert(dummyView.isCalledHideError == false)

    }

    func test_givenDisplayDataWithShouldHideErrorAutomaticallyFalseAndCurrencyBOWithoutDecimals_whenICallShouldChangeCharactersWithTextEmpty_thenINotCallHideError() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let displayData = AmountTextfieldDisplayData(placeHolder: Localizables.transactions.key_transactions_from_text, initialAmount: nil, currency: nil, shouldHideErrorAutomatically: false)
        sut.displayData = displayData
        dummyView.isCalledHideError = false

        sut.currencyBO = CurrencyBO(code: "CLP", position: .left, needDecimals: false)

        // when
        _ = sut.shouldChangeCharacters(forText: "", withNewText: "", inRange: NSRange())

        // then
        XCTAssert(dummyView.isCalledHideError == false)

    }

    func test_givenDisplayDataWithShouldHideErrorAutomaticallyTrueAndCurrencyBOWithoutDecimals_whenICallShouldChangeCharactersWithTextEmpty_thenICallHideError() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let displayData = AmountTextfieldDisplayData(placeHolder: Localizables.transactions.key_transactions_from_text, initialAmount: nil, currency: nil, shouldHideErrorAutomatically: true)
        sut.displayData = displayData
        dummyView.isCalledHideError = false

        sut.currencyBO = CurrencyBO(code: "CLP", position: .left, needDecimals: false)

        // when
        _ = sut.shouldChangeCharacters(forText: "", withNewText: "", inRange: NSRange())

        // then
        XCTAssert(dummyView.isCalledHideError == true)

    }

    func test_givenDisplayDataWithShouldHideErrorAutomaticallyFalseAndCurrencyBOWithDecimals_whenICallShouldChangeCharactersWithNewTextNoNumber_thenINotCallHideError() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let displayData = AmountTextfieldDisplayData(placeHolder: Localizables.transactions.key_transactions_from_text, initialAmount: nil, currency: nil, shouldHideErrorAutomatically: false)
        sut.displayData = displayData
        dummyView.isCalledHideError = false

        sut.currencyBO = CurrencyBO(code: "CLP", position: .left, needDecimals: true)

        // when
        _ = sut.shouldChangeCharacters(forText: "", withNewText: "newText", inRange: NSRange())

        // then
        XCTAssert(dummyView.isCalledHideError == false)

    }

    //------------

    func test_givenDisplayDataWithShouldHideErrorAutomaticallyFalseAndCurrencyBOWithDecimals_whenICallShouldChangeCharactersWithTextWithDecimalNumber_thenINotCallHideError() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let displayData = AmountTextfieldDisplayData(placeHolder: Localizables.transactions.key_transactions_from_text, initialAmount: nil, currency: nil, shouldHideErrorAutomatically: false)
        sut.displayData = displayData
        dummyView.isCalledHideError = false

        let localeSeparator = AmountFormatter.decimalFormatter(locale: Locale.current).decimalSeparator!
        sut.currencyBO = CurrencyBO(code: "USD", position: .left, needDecimals: true)

        let oldText = "12" + localeSeparator + "3"
        let newText = "2"

        // when
        _ = sut.shouldChangeCharacters(forText: oldText, withNewText: newText, inRange: NSRange(location: oldText.count, length: 0))

        // then
        XCTAssert(dummyView.isCalledHideError == false)

    }

    func test_givenDisplayDataWithShouldHideErrorAutomaticallyTrueAndCurrencyBOWithDecimals_whenICallShouldChangeCharactersWithTextWithDecimalNumber_thenICallHideError() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let displayData = AmountTextfieldDisplayData(placeHolder: Localizables.transactions.key_transactions_from_text, initialAmount: nil, currency: nil, shouldHideErrorAutomatically: true)
        sut.displayData = displayData
        dummyView.isCalledHideError = false

        let localeSeparator = AmountFormatter.decimalFormatter(locale: Locale.current).decimalSeparator!
        sut.currencyBO = CurrencyBO(code: "USD", position: .left, needDecimals: true)

        let oldText = "12" + localeSeparator + "3"
        let newText = "2"

        // when
        _ = sut.shouldChangeCharacters(forText: oldText, withNewText: newText, inRange: NSRange(location: oldText.count, length: 0))

        // then
        XCTAssert(dummyView.isCalledHideError == true)

    }

    class DummyInteractor: AmountTextfieldInteractorProtocol {

        var isCalledProvideCurrency = false
        var currencyCode = "USD"

        func provideCurrency() -> Observable<ModelBO> {

            isCalledProvideCurrency = true

            let currencyBO = Currencies.currency(forCode: currencyCode)

            return Observable <ModelBO>.just(currencyBO as ModelBO)
        }
    }

    class FormatterDummy: AmountFormatter {

        var isCalledFormat = false

        override func format(amount: NSDecimalNumber, isNegative: Bool, withSpace: Bool) -> String {
            isCalledFormat = true
            return ""
        }
    }

    class DummyView: AmountTextfieldViewProtocol {

        var isCalledClearTextfield = false
        var isCalledConfigureNumberKeyboard = false
        var isCalledDecimalKeyboard = false
        var isCalledShowError = false
        var isCalledHideError = false
        var isCalledSetPlaceHolder = false
        var isCalledInformEndEditing = false
        var isCalledUpdateViewAmountText = false
        var isCalledShowRemoveButton = false
        var isCalledHideRemoveButton = false

        var valueToClear = "valueToClearConcept"
        var placeHolderValue = ""
        var amountToInform = NSDecimalNumber(string: "0")
        var amountText = "formattedAmount"

        func clearTextfield(withValue value: String) {
            isCalledClearTextfield = true
            valueToClear = value
        }

        func configureNumberKeyboard() {
            isCalledConfigureNumberKeyboard = true
        }

        func configureDecimalKeyboard() {
            isCalledDecimalKeyboard = true
        }

        func showError() {
            isCalledShowError = true
        }

        func hideError() {
            isCalledHideError = true
        }

        func setPlaceHolder(text: String) {
            isCalledSetPlaceHolder = true
            placeHolderValue = text
        }

        func informEndEditing(withAmount amount: NSDecimalNumber) {
            isCalledInformEndEditing = true
            amountToInform = amount
        }

        func updateView(withAmountText amountText: String) {

            isCalledUpdateViewAmountText = true
            self.amountText = amountText
        }

        func showRemoveButton() {
            isCalledShowRemoveButton = true
        }

        func hideRemoveButton() {
            isCalledHideRemoveButton = true
        }
    }

}
