//
//  HelpDisplayDataTest.swift
//  shoppingapp
//
//  Created by AZIZEBULBUL on 26/01/2018.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class HelpDisplayDataTest: XCTestCase {

    func test_givenAHelpBOWithCorrespondingLanguage_whenICallGenerateHelpDisplayData_thenIDisplayCorrectData() {

        //given
        let helpsBO = ConfigGenerator.getHelpBO()
        let helpBO = helpsBO.items[0]

       let selectedLanguageData = helpBO.getSelectedLanguage(forKey: "es")

        //When
        let displayData = HelpDisplayData.generateHelpDisplayData(withConfigBO: helpBO, isFromSection: false)

        // then
        XCTAssert(displayData.titleText == selectedLanguageData?.title)
        XCTAssert(displayData.descriptionText == selectedLanguageData?.description)
        XCTAssert(displayData.backgroundColor == Colors.hexStringToUIColor(hex: helpBO.help.backgroundColor.hexString, alpha: helpBO.help.backgroundColor.alpha))
        XCTAssert(displayData.titleColor == Colors.hexStringToUIColor(hex: helpBO.help.titleColor.hexString, alpha: helpBO.help.titleColor.alpha))
        XCTAssert(displayData.descriptionTextColor == Colors.hexStringToUIColor(hex: helpBO.help.descriptionColor.hexString, alpha: helpBO.help.descriptionColor.alpha))

        XCTAssert(displayData.descriptionOnTapColor == Colors.hexStringToUIColor(hex: (helpBO.help.descriptionOnTapColor.hexString), alpha: helpBO.help.descriptionOnTapColor.alpha))

        XCTAssert(displayData.imageUrl == helpBO.getImageURL(forUrl: helpBO.image!))

        XCTAssert(displayData.backgroundColor == Colors.hexStringToUIColor(hex: helpBO.section.backgroundColor.hexString, alpha: helpBO.section.backgroundColor.alpha))
        XCTAssert(displayData.titleColor == Colors.hexStringToUIColor(hex: helpBO.section.titleColor.hexString, alpha: helpBO.section.titleColor.alpha))
        XCTAssert(displayData.descriptionTextColor == Colors.hexStringToUIColor(hex: helpBO.section.descriptionColor.hexString, alpha: helpBO.section.descriptionColor.alpha))
        
        XCTAssert(displayData.descriptionOnTapColor == Colors.hexStringToUIColor(hex: (helpBO.section.descriptionOnTapColor.hexString), alpha: helpBO.section.descriptionOnTapColor.alpha))
    }

    func test_givenAHelpBOWithOutImage_whenICallGenerateHelpDisplayDataFromHelp_thenIDisplayDefaultsColorsAndBackgroundAndImageDefault() {

        //given
        let helpsBO = ConfigGenerator.getHelpBO()
        let helpBO = helpsBO.items[0]
        helpBO.image = ""

        //When
        let displayData = HelpDisplayData.generateHelpDisplayData(withConfigBO: helpBO, isFromSection: false)

        let defaultDisplayData = displayData.setDefaultColorsAndBackground(withHelpDisplayData: displayData)

        // then
        XCTAssert(displayData.image == ConstantsImages.Help.imageThumbDefault)
        XCTAssert(displayData.backgroundColor == defaultDisplayData.backgroundColor)
        XCTAssert(displayData.titleColor == defaultDisplayData.titleColor)
        XCTAssert(displayData.descriptionTextColor == defaultDisplayData.descriptionTextColor)
        XCTAssert(displayData.descriptionOnTapColor == defaultDisplayData.descriptionOnTapColor)
    }

    func test_givenAHelpBOWithOutImage_whenICallGenerateHelpDisplayDataFromSection_thenIDisplayDefaultsColorsAndBackgroundAndImageDefault() {
        
        //given
        let helpsBO = ConfigGenerator.getHelpBO()
        let helpBO = helpsBO.items[0]
        helpBO.image = ""
        
        //When
        let displayData = HelpDisplayData.generateHelpDisplayData(withConfigBO: helpBO, isFromSection: true)
        
        let defaultDisplayData = displayData.setDefaultColorsAndBackground(withHelpDisplayData: displayData)
        
        // then
        XCTAssert(displayData.image == ConstantsImages.Help.imageThumbDefault)
        XCTAssert(displayData.backgroundColor == defaultDisplayData.backgroundColor)
        XCTAssert(displayData.titleColor == defaultDisplayData.titleColor)
        XCTAssert(displayData.descriptionTextColor == defaultDisplayData.descriptionTextColor)
        XCTAssert(displayData.descriptionOnTapColor == defaultDisplayData.descriptionOnTapColor)
    }
    
    func test_givenAHelpBOWithImageNil_whenICallGenerateHelpDisplayDataFromHelp_rtyuiopthenIDisplayDefaultsColorsAndBackgroundAndImageDefault() {

        //given
        let helpsBO = ConfigGenerator.getHelpBO()
        let helpBO = helpsBO.items[0]
        helpBO.image = nil

        //When
        let displayData = HelpDisplayData.generateHelpDisplayData(withConfigBO: helpBO, isFromSection: false)

        let defaultDisplayData = displayData.setDefaultColorsAndBackground(withHelpDisplayData: displayData)

        // then
        XCTAssert(displayData.image == ConstantsImages.Help.imageThumbDefault)
        XCTAssert(displayData.backgroundColor == defaultDisplayData.backgroundColor)
        XCTAssert(displayData.titleColor == defaultDisplayData.titleColor)
        XCTAssert(displayData.descriptionTextColor == defaultDisplayData.descriptionTextColor)
        XCTAssert(displayData.descriptionOnTapColor == defaultDisplayData.descriptionOnTapColor)
    }

    func test_givenAHelpBOWithImageNil_whenICallGenerateHelpDisplayDataFromSection_thenIDisplayDefaultsColorsAndBackgroundAndImageDefault() {
        
        //given
        let helpsBO = ConfigGenerator.getHelpBO()
        let helpBO = helpsBO.items[0]
        helpBO.image = nil
        
        //When
        let displayData = HelpDisplayData.generateHelpDisplayData(withConfigBO: helpBO, isFromSection: true)
        
        let defaultDisplayData = displayData.setDefaultColorsAndBackground(withHelpDisplayData: displayData)
        
        // then
        XCTAssert(displayData.image == ConstantsImages.Help.imageThumbDefault)
        XCTAssert(displayData.backgroundColor == defaultDisplayData.backgroundColor)
        XCTAssert(displayData.titleColor == defaultDisplayData.titleColor)
        XCTAssert(displayData.descriptionTextColor == defaultDisplayData.descriptionTextColor)
        XCTAssert(displayData.descriptionOnTapColor == defaultDisplayData.descriptionOnTapColor)
    }
    
    func test_givenAHelpBOWithoutAtLeastOneColor_whenICallGenerateHelpDisplayData_thenIDisplayDefaultsColorsAndBackground() {

        //given
        let helpsBO = ConfigGenerator.getHelpBO()
        let helpBO = helpsBO.items[0]
        helpBO.help.titleColor = nil
        helpBO.help.descriptionColor.hexString = ""

        //When
        let displayData = HelpDisplayData.generateHelpDisplayData(withConfigBO: helpBO, isFromSection: false)

        let defaultDisplayData = displayData.setDefaultColorsAndBackground(withHelpDisplayData: displayData)

        // then
        XCTAssert(displayData.backgroundColor == defaultDisplayData.backgroundColor)
        XCTAssert(displayData.titleColor == defaultDisplayData.titleColor)
        XCTAssert(displayData.descriptionTextColor == defaultDisplayData.descriptionTextColor)
        XCTAssert(displayData.descriptionOnTapColor == defaultDisplayData.descriptionOnTapColor)
    }

    func test_givenAHelpBOFromSectionWithoutAtLeastOneColor_whenICallGenerateHelpDisplayData_thenIDisplayDefaultsColorsAndBackground() {
        
        //given
        let helpsBO = ConfigGenerator.getHelpBO()
        let helpBO = helpsBO.items[0]
        helpBO.section.titleColor = nil
        helpBO.section.descriptionColor.hexString = ""
        
        //When
        let displayData = HelpDisplayData.generateHelpDisplayData(withConfigBO: helpBO, isFromSection: true)
        
        let defaultDisplayData = displayData.setDefaultColorsAndBackground(withHelpDisplayData: displayData)
        
        // then
        XCTAssert(displayData.backgroundColor == defaultDisplayData.backgroundColor)
        XCTAssert(displayData.titleColor == defaultDisplayData.titleColor)
        XCTAssert(displayData.descriptionTextColor == defaultDisplayData.descriptionTextColor)
        XCTAssert(displayData.descriptionOnTapColor == defaultDisplayData.descriptionOnTapColor)
    }
}
