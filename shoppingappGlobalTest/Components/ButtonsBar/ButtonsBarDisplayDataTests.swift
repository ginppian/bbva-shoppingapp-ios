//
//  ButtonsBarDisplayDataTests.swift
//  shoppingapp
//
//  Created by jesus.martinez on 13/10/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class ButtonsBarDisplayDataTests: XCTestCase {

    var sut: ButtonsBarDisplayData!

    override func setUp() {
        super.setUp()
        self.sut = ButtonsBarDisplayData()
    }

    // MARK: - Init

    func test_givenAny_whenInit_thenButtonsArraysShouldBeEmpty() {

        // given

        // when
        let dummyButtonsBarDisplayData = ButtonsBarDisplayData()

        // then
        XCTAssert(dummyButtonsBarDisplayData.buttons.isEmpty == true)

    }

    // MARK: - Init

    func test_givenButtonBarDisplayData_whenICallAdd_thenButtonBarDisplayDataShouldBeInButtons() {

        // given
        let dummyButtonBarDisplayData = ButtonBarDisplayData(imageNamed: "imageNamed", titleKey: "titleKey", optionKey: .callBBVA)

        // when
        sut.add(button: dummyButtonBarDisplayData)

        // then
        XCTAssert(sut.buttons.count == 1)
        XCTAssert(sut.buttons[0] == dummyButtonBarDisplayData)

    }

    // MARK: - generateButtonsBarDisplayData

    func test_givenAnInoperativeCard_whenICallGenerateButtonsBarDisplayData_thenDisplayDataShouldNotHaveLimitsOption() {
        
        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "INOPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        
        // when
        let displayData = ButtonsBarDisplayData.generateButtonsBarDisplayData(fromCardBO: cardBO)
        
        // then
        let limitsButtonDisplayData = ButtonBarDisplayData(imageNamed: ConstantsImages.Card.limits, titleKey: Localizables.cards.key_cards_set_up_limits_text, optionKey: .limits)
        XCTAssert(displayData.buttons.contains(limitsButtonDisplayData) == false)
    }
    
    func test_givenAnInoperativeCard_whenICallGenerateButtonsBarDisplayData_thenDisplayDataShouldNotHaveBlockOption() {
        
        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "INOPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        
        // when
        let displayData = ButtonsBarDisplayData.generateButtonsBarDisplayData(fromCardBO: cardBO)
        
        // then
        let blockButtonDisplayData = ButtonBarDisplayData(imageNamed: ConstantsImages.Card.blockcard, titleKey: Localizables.cards.key_card_block_card_text, optionKey: .block)
        XCTAssert(displayData.buttons.contains(blockButtonDisplayData) == false)
    }
    
    func test_givenAnInoperativeCard_whenICallGenerateButtonsBarDisplayData_thenDisplayDataShouldHaveCancelOption() {
        
        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "INOPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        
        // when
        let displayData = ButtonsBarDisplayData.generateButtonsBarDisplayData(fromCardBO: cardBO)
        
        // then
        let cancelButtonDisplayData = ButtonBarDisplayData(imageNamed: ConstantsImages.Card.blockcard, titleKey: Localizables.cards.key_card_cancel, optionKey: .cancel)
        XCTAssert(displayData.buttons.contains(cancelButtonDisplayData) == true)
    }
    
    func test_givenAPendingEmbossingCard_whenICallGenerateButtonsBarDisplayData_thenDisplayDataShouldNotHaveLimitsOption() {
        
        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "PENDING_EMBOSSING", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        
        // when
        let displayData = ButtonsBarDisplayData.generateButtonsBarDisplayData(fromCardBO: cardBO)
        
        // then
        let limitsButtonDisplayData = ButtonBarDisplayData(imageNamed: ConstantsImages.Card.limits, titleKey: Localizables.cards.key_cards_set_up_limits_text, optionKey: .limits)
        XCTAssert(displayData.buttons.contains(limitsButtonDisplayData) == false)
    }
    
    func test_givenAPendingDeliveryCard_whenICallGenerateButtonsBarDisplayData_thenDisplayDataShouldNotHaveLimitsOption() {
        
        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "PENDING_DELIVERY", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        
        // when
        let displayData = ButtonsBarDisplayData.generateButtonsBarDisplayData(fromCardBO: cardBO)
        
        // then
        let limitsButtonDisplayData = ButtonBarDisplayData(imageNamed: ConstantsImages.Card.limits, titleKey: Localizables.cards.key_cards_set_up_limits_text, optionKey: .limits)
        XCTAssert(displayData.buttons.contains(limitsButtonDisplayData) == false)
    }

    func test_givenAnOperativeCard_whenICallGenerateButtonsBarDisplayData_thenDisplayDataShouldHaveLimitsOption() {

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        // when
        let displayData = ButtonsBarDisplayData.generateButtonsBarDisplayData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.buttons.count == 3)
        XCTAssert(displayData.buttons[1].imageNamed == ConstantsImages.Card.limits)
        XCTAssert(displayData.buttons[1].titleKey == Localizables.cards.key_cards_set_up_limits_text)
        XCTAssert(displayData.buttons[1].optionKey == .limits)
    }

    func test_givenAnBlockedCard_whenICallGenerateButtonsBarDisplayData_thenDisplayDataShouldHaveLimitsOption() {

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "BLOCKED", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        // when
        let displayData = ButtonsBarDisplayData.generateButtonsBarDisplayData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.buttons.count == 1)
        XCTAssert(displayData.buttons[0].imageNamed == ConstantsImages.Card.limits)
        XCTAssert(displayData.buttons[0].titleKey == Localizables.cards.key_cards_set_up_limits_text)
        XCTAssert(displayData.buttons[0].optionKey == .limits)
    }

    func test_givenAnCanceledCard_whenICallGenerateButtonsBarDisplayData_thenDisplayDataShouldHaveLimitsOption() {

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "CANCELED", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        // when
        let displayData = ButtonsBarDisplayData.generateButtonsBarDisplayData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.buttons.count == 2)
        XCTAssert(displayData.buttons[0].imageNamed == ConstantsImages.Card.limits)
        XCTAssert(displayData.buttons[0].titleKey == Localizables.cards.key_cards_set_up_limits_text)
        XCTAssert(displayData.buttons[0].optionKey == .limits)
    }

    func test_givenACardBOOperativeWithIndicatorBlockableTrue_whenICallGenerateButtonsBarDisplayData_thenDisplayDataShouldHaveBlockOption() {

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        cardBO.indicators[0].indicatorId = .blockable
        cardBO.indicators[0].isActive = true

        // when
        let displayData = ButtonsBarDisplayData.generateButtonsBarDisplayData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.buttons.count == 3)
        XCTAssert(displayData.buttons[0].imageNamed == ConstantsImages.Card.blockcard)
        XCTAssert(displayData.buttons[0].titleKey == Localizables.cards.key_card_block_card_text)
        XCTAssert(displayData.buttons[0].optionKey == .block)
    }

    func test_givenACardBOOperativeWithNotBlockableIndicator_whenICallGenerateButtonsBarDisplayData_thenDisplayDataShouldNotHaveBlockOption() {

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        cardBO.indicators[3].indicatorId = .cancelable

        // when
        let displayData = ButtonsBarDisplayData.generateButtonsBarDisplayData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.buttons.count == 2)
        XCTAssert(displayData.buttons[0].imageNamed != ConstantsImages.Card.creditcard_icon)
        XCTAssert(displayData.buttons[0].titleKey != Localizables.cards.key_card_block_card_text)
        XCTAssert(displayData.buttons[0].optionKey != .block)
    }

    func test_givenACardBOBlockedWithIndicatorBlockableTrue_whenICallGenerateButtonsBarDisplayData_thenDisplayDataShouldNotHaveBlockOption() {

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "BLOCKED", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        cardBO.indicators[0].indicatorId = .blockable
        cardBO.indicators[0].isActive = true

        // when
        let displayData = ButtonsBarDisplayData.generateButtonsBarDisplayData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.buttons.count == 1)
        XCTAssert(displayData.buttons[0].imageNamed == ConstantsImages.Card.limits)
        XCTAssert(displayData.buttons[0].titleKey == Localizables.cards.key_cards_set_up_limits_text)
        XCTAssert(displayData.buttons[0].optionKey == .limits)
    }

    func test_givenACardBOCanceledWithIndicatorBlockableTrue_whenICallGenerateButtonsBarDisplayData_thenDisplayDataShouldNotHaveBlockOption() {

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "CANCELED", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        cardBO.indicators[0].indicatorId = .blockable
        cardBO.indicators[0].isActive = true

        // when
        let displayData = ButtonsBarDisplayData.generateButtonsBarDisplayData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.buttons.count == 2)
        XCTAssert(displayData.buttons[0].imageNamed == ConstantsImages.Card.limits)
        XCTAssert(displayData.buttons[0].titleKey == Localizables.cards.key_cards_set_up_limits_text)
        XCTAssert(displayData.buttons[0].optionKey == .limits)
    }

    func test_givenACardBOPendingEmbossingWithIndicatorBlockableTrue_whenICallGenerateButtonsBarDisplayData_thenDisplayDataShouldNotHaveBlockOption() {

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "PENDING_EMBOSSING", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        cardBO.indicators[0].indicatorId = .blockable
        cardBO.indicators[0].isActive = true

        // when
        let displayData = ButtonsBarDisplayData.generateButtonsBarDisplayData(fromCardBO: cardBO)

        // then
        let blockButtonDisplayData = ButtonBarDisplayData(imageNamed: ConstantsImages.Card.blockcard, titleKey: Localizables.cards.key_card_block_card_text, optionKey: .block)
        XCTAssert(displayData.buttons.contains(blockButtonDisplayData) == false)
    }

    func test_givenACardBOPendingDeliveryWithIndicatorBlockableTrue_whenICallGenerateButtonsBarDisplayData_thenDisplayDataShouldNotHaveBlockOption() {

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "PENDING_DELIVERY", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        cardBO.indicators[0].indicatorId = .blockable
        cardBO.indicators[0].isActive = true

        // when
        let displayData = ButtonsBarDisplayData.generateButtonsBarDisplayData(fromCardBO: cardBO)

        // then
        let blockButtonDisplayData = ButtonBarDisplayData(imageNamed: ConstantsImages.Card.blockcard, titleKey: Localizables.cards.key_card_block_card_text, optionKey: .block)
        XCTAssert(displayData.buttons.contains(blockButtonDisplayData) == false)
    }

    func test_givenACardBOOperativeWithIndicatorBlockableFalse_whenICallGenerateButtonsBarDisplayData_thenDisplayDataShouldNotHaveBlockOption() {

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        cardBO.indicators[0].indicatorId = .blockable
        cardBO.indicators[0].isActive = false

        // when
        let displayData = ButtonsBarDisplayData.generateButtonsBarDisplayData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.buttons.count == 2)
        XCTAssert(displayData.buttons[0].imageNamed != ConstantsImages.Card.creditcard_icon)
        XCTAssert(displayData.buttons[0].titleKey != Localizables.cards.key_card_block_card_text)
        XCTAssert(displayData.buttons[0].optionKey != .block)
    }

    // MARK: Cancelables

    func test_givenACardBOBlocked_whenICallGenerateButtonsBarDisplayData_thenDisplayDataShouldNotHaveCancelOption() {

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "BLOCKED", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        // when
        let displayData = ButtonsBarDisplayData.generateButtonsBarDisplayData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.buttons.count == 1)
        XCTAssert(displayData.buttons[0].imageNamed != ConstantsImages.Card.blockcard)
        XCTAssert(displayData.buttons[0].titleKey != Localizables.cards.key_card_cancel)
        XCTAssert(displayData.buttons[0].optionKey != .cancel)

    }

    func test_givenAnCanceledCardWithIndicatorCancelableFalse_whenICallGenerateButtonsBarDisplayData_thenDisplayDataShouldNotHaveCancelOption() {

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "CANCELED", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        cardBO.indicators[0].indicatorId = .cancelable
        cardBO.indicators[0].isActive = false

        // when
        let displayData = ButtonsBarDisplayData.generateButtonsBarDisplayData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.buttons.count == 1)
        XCTAssert(displayData.buttons[0].imageNamed != ConstantsImages.Card.blockcard)
        XCTAssert(displayData.buttons[0].titleKey != Localizables.cards.key_card_cancel)
        XCTAssert(displayData.buttons[0].optionKey != .cancel)

    }

    func test_givenAnCanceledCardWithIndicatorCancelableTrue_whenICallGenerateButtonsBarDisplayData_thenDisplayDataShouldHaveCancelOption() {

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "CANCELED", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        cardBO.indicators[0].indicatorId = .cancelable
        cardBO.indicators[0].isActive = true

        // when
        let displayData = ButtonsBarDisplayData.generateButtonsBarDisplayData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.buttons.last?.imageNamed == ConstantsImages.Card.blockcard)
        XCTAssert(displayData.buttons.last?.titleKey == Localizables.cards.key_card_cancel)
        XCTAssert(displayData.buttons.last?.optionKey == .cancel)

    }

    func test_givenAnPendingEmbossingCardWithIndicatorCancelableFalse_whenICallGenerateButtonsBarDisplayData_thenDisplayDataShoulNotdHaveCancelOption() {

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "PENDING_EMBOSSING", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        cardBO.indicators[0].indicatorId = .cancelable
        cardBO.indicators[0].isActive = false

        // when
        let displayData = ButtonsBarDisplayData.generateButtonsBarDisplayData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.buttons.last?.imageNamed != ConstantsImages.Card.blockcard)
        XCTAssert(displayData.buttons.last?.titleKey != Localizables.cards.key_card_cancel)
        XCTAssert(displayData.buttons.last?.optionKey != .cancel)

    }

    func test_givenAnPendingEmbossingCardWithIndicatorCancelableTrue_whenICallGenerateButtonsBarDisplayData_thenDisplayDataShouldHaveCancelOption() {

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "PENDING_EMBOSSING", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        cardBO.indicators[0].indicatorId = .cancelable
        cardBO.indicators[0].isActive = true

        // when
        let displayData = ButtonsBarDisplayData.generateButtonsBarDisplayData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.buttons.last?.imageNamed == ConstantsImages.Card.blockcard)
        XCTAssert(displayData.buttons.last?.titleKey == Localizables.cards.key_card_cancel)
        XCTAssert(displayData.buttons.last?.optionKey == .cancel)

    }

    func test_givenAnPendingDeliveryCardWithIndicatorCancelableFalse_whenICallGenerateButtonsBarDisplayData_thenDisplayDataShouldNotHaveCancelOption() {

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "PENDING_DELIVERY", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        cardBO.indicators[0].indicatorId = .cancelable
        cardBO.indicators[0].isActive = false

        // when
        let displayData = ButtonsBarDisplayData.generateButtonsBarDisplayData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.buttons.last?.imageNamed != ConstantsImages.Card.blockcard)
        XCTAssert(displayData.buttons.last?.titleKey != Localizables.cards.key_card_cancel)
        XCTAssert(displayData.buttons.last?.optionKey != .cancel)

    }

    func test_givenAnPendingDeliveryCardWithIndicatorCancelableTrue_whenICallGenerateButtonsBarDisplayData_thenDisplayDataShouldHaveCancelOption() {

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "PENDING_DELIVERY", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        cardBO.indicators[0].indicatorId = .cancelable
        cardBO.indicators[0].isActive = true

        // when
        let displayData = ButtonsBarDisplayData.generateButtonsBarDisplayData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.buttons.last?.imageNamed == ConstantsImages.Card.blockcard)
        XCTAssert(displayData.buttons.last?.titleKey == Localizables.cards.key_card_cancel)
        XCTAssert(displayData.buttons.last?.optionKey == .cancel)

    }
}
