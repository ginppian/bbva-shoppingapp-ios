//
//  ContactUsDisplayDataTest.swift
//  shoppingapp
//
//  Created by Luis de la Cruz on 19/10/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
@testable import shoppingappMX
#elseif TESTPE
@testable import shoppingappPE
#endif

class ContactUsDisplayDataTest: XCTestCase {

    // MARK: - generateContactUsDisplay
    
    func test_givenAPromotionBOWithContactDetailsIsEmpty_whenICallGenerateContactUsDisplay_thenDisplayDataWithDefaultValues() {
        
        //given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        promotionBO.contactDetails = nil
        
        //when
        let displayData = ContactUsDisplayData.generateContactUsDisplay(fromPromotion: promotionBO)
        
        //then
        XCTAssert(displayData.description == Localizables.promotions.key_any_questions_need_a_quick_response_text)
        XCTAssert(displayData.showMoreInfo == false)
    }
    
    func test_givenAPromotionBOWithContactDetailsAndContactTypeIsEmail_whenICallGenerateContactUsDisplay_thenDisplayDataWithCorrectValues() {
        
        //given
        let promotionBO = PromotionsGenerator.getPromotionDetailWhitContactDetails(contacTypeIsEmail: true)
        
        //when
        let displayData = ContactUsDisplayData.generateContactUsDisplay(fromPromotion: promotionBO)
        
        //then
        XCTAssert(displayData.description == Localizables.promotions.key_any_questions_need_a_quick_response_text)
        XCTAssert(displayData.showMoreInfo == false)
    }
    
    func test_givenAPromotionBOWithContactDetailsAndContactTypeIsNotEmail_whenICallGenerateContactUsDisplay_thenDisplayDataWithCorrectValue() {
        
        //given
        let promotionBO = PromotionsGenerator.getPromotionDetailWhitContactDetails(contacTypeIsEmail: false)
        
        //when
        let displayData = ContactUsDisplayData.generateContactUsDisplay(fromPromotion: promotionBO)
        
        //then
        XCTAssert(displayData.description == Localizables.promotions.key_didnt_give_promotion_we_guarantee_it_text)
        XCTAssert(displayData.showMoreInfo == true)
    }
    
}
