//
//  ContactUsPresenterTest.swift
//  shoppingapp
//
//  Created by Luis de la Cruz on 19/10/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
@testable import shoppingappMX
#elseif TESTPE
@testable import shoppingappPE
#endif

class ContactUsPresenterTest: XCTestCase {
    
    let sut = ContactUsPresenter()
    
    // MARK: - receiveforShowContactUsDisplay
    func test_givenAViewAndDisplayData_whenICallReceiveforShowContactUsDisplay_thenICallShowDisplayData() {
        
        //given
        let dummyView = DummyView()
        sut.view = dummyView
        
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        let displayContactUs = ContactUsDisplayData.generateContactUsDisplay(fromPromotion: promotionBO)
        
        //when
        sut.receiveforShowContactUsDisplay(withContactUsDisplay: displayContactUs)
        
        //then
        XCTAssert(dummyView.isCalledShowDisplayData == true)
    }
    
    func test_givenAViewAndDisplayData_whenICallReceiveforShowContactUsDisplay_thenICallShowDisplayDataWithContactUsDisplayIsNotEmpty() {
        
        //given
        let dummyView = DummyView()
        sut.view = dummyView
        
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        let displayContactUs = ContactUsDisplayData.generateContactUsDisplay(fromPromotion: promotionBO)
        
        //when
        sut.receiveforShowContactUsDisplay(withContactUsDisplay: displayContactUs)
        
        //then
        XCTAssert(dummyView.isCalledShowDisplayData == true)
        XCTAssert(dummyView.displayDataContactUs != nil)
    }
    
    func test_givenAViewAndDisplayData_whenICallReceiveforShowContactUsDisplay_thenHaveCorrectValuesToShow() {
        
        //given
        let dummyView = DummyView()
        sut.view = dummyView
        
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        let displayContactUs = ContactUsDisplayData.generateContactUsDisplay(fromPromotion: promotionBO)
        
        //when
        sut.receiveforShowContactUsDisplay(withContactUsDisplay: displayContactUs)
        
        //then
        XCTAssert(dummyView.displayDataContactUs.description == displayContactUs.description)
        XCTAssert(dummyView.displayDataContactUs.showMoreInfo == displayContactUs.showMoreInfo)
    }
    
    // MARK: - DummyData

    class DummyView: ContactUsViewProtocol {
        var isCalledShowDisplayData = false
        var displayDataContactUs: ContactUsDisplayData!
        
        func showDisplayData(withContactUsDisplay displayContactUs: ContactUsDisplayData) {

            isCalledShowDisplayData = true
            displayDataContactUs = displayContactUs
        }
        
    }

}
