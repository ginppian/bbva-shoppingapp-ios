//
//  AssociateCommercePresenterTest.swift
//  shoppingapp
//
//  Created by Luis Monroy on 24/01/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#endif

class AssociateCommercePresenterTest: XCTestCase {

    let sut = AssociateCommercePresenter()

    // MARK: - receiveforShowAssociateCommerceDisplay

    func test_givenAViewAndDisplayData_whenICallReceiveforShowAssociateCommerceDisplay_thenICallViewShowDisplayData() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let promotionBO = PromotionsGenerator.getPromotionDetail()
        let associateCommerceData = AssociateCommerceDisplayData.generateAssociateCommerceDisplay(fromPromotion: promotionBO)

        // when
        sut.receiveforShowAssociateCommerceDisplay(withAssociateCommerceDisplay: associateCommerceData)

        // then
        XCTAssert(dummyView.isCalledShowDisplayData == true)
    }

    func test_givenAViewAndDisplayData_whenICallReceiveforShowAssociateCommerceDisplay_thenHaveCorrectValuesToShow() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let promotionBO = PromotionsGenerator.getPromotionDetail()
        let associateCommerceData = AssociateCommerceDisplayData.generateAssociateCommerceDisplay(fromPromotion: promotionBO)

        // when
        sut.receiveforShowAssociateCommerceDisplay(withAssociateCommerceDisplay: associateCommerceData)

        // then
        XCTAssert(dummyView.associateCommerceDisplayData.commerceAboutName == associateCommerceData.commerceAboutName)
        XCTAssert(dummyView.associateCommerceDisplayData.commerceDescription == associateCommerceData.commerceDescription)
        XCTAssert(dummyView.associateCommerceDisplayData.commerceWebPage == associateCommerceData.commerceWebPage)
        XCTAssert(dummyView.associateCommerceDisplayData.stores?.count == associateCommerceData.stores?.count)
        XCTAssert(dummyView.associateCommerceDisplayData.storesDescription == associateCommerceData.storesDescription)
        XCTAssert(dummyView.associateCommerceDisplayData.showStores == associateCommerceData.showStores)
    }

    // MARK: - DummyData

    class DummyView: AssociateCommerceViewProtocol {

        var isCalledShowDisplayData = false
        var associateCommerceDisplayData: AssociateCommerceDisplayData!

        func showDisplayData(withAssociateCommerceDisplay displayAssociateCommerce: AssociateCommerceDisplayData) {

            isCalledShowDisplayData = true
            associateCommerceDisplayData = displayAssociateCommerce
        }
    }
}
