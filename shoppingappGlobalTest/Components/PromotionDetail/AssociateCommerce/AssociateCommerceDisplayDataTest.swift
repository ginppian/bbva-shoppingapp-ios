//
//  AssociateCommerceDisplayDataTest.swift
//  shoppingapp
//
//  Created by Luis Monroy on 25/05/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#endif

class AssociateCommerceDisplayDataTest: XCTestCase {

    // MARK: - generateAssociateCommerceDisplay

    func test_givenAPromotionBO_whenICallGenerateAssociateCommerceDisplay_thenDisplayDataShouldBeMatchWithPromotionBO() {

        //given
        let promotionBO = PromotionsGenerator.getPromotionDetail()

        //When
        let displayData = AssociateCommerceDisplayData.generateAssociateCommerceDisplay(fromPromotion: promotionBO)

        // then
        XCTAssert(displayData.commerceAboutName == Localizables.promotions.key_promotions_about_text + " " + promotionBO.commerceInformation.name)
        XCTAssert(displayData.commerceDescription == promotionBO.commerceInformation.description)
        XCTAssert(displayData.commerceWebPage == promotionBO.commerceInformation.web)
        XCTAssert(displayData.stores?.count == promotionBO.stores?.count)
    }

    func test_givenAPromotionBO_whenICallGenerateAssociateCommerceDisplayAndHaveMoreThanOneStore_thenDisplayDataHaveStoreDescription() {

        //given
        let promotionBO = PromotionsGenerator.getPromotionDetail()

        //When
        let displayData = AssociateCommerceDisplayData.generateAssociateCommerceDisplay(fromPromotion: promotionBO)

        // then
        let stores = promotionBO.stores?.count

        XCTAssert(displayData.storesDescription == Localizables.promotions.key_promotions_see_establishment_text + " (\(stores!))" )
    }

    func test_givenAPromotionBO_whenICallGenerateAssociateCommerceDisplayAndNoHaveMoreThanOneStore_thenDisplayDataNoHaveStoreDescription() {

        //given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        promotionBO.stores = nil

        //When
        let displayData = AssociateCommerceDisplayData.generateAssociateCommerceDisplay(fromPromotion: promotionBO)

        // then
        XCTAssert(displayData.storesDescription == nil)
    }

    func test_givenAPromotionBOWithOneStore_whenICallGenerateAssociateCommerceAndHaveOnePhysicalUsageType_thenDisplayDataShowStoresIsFalse() {

        //given
        let promotionBO = PromotionsGenerator.getPromotionDetail(physical: true, oneStore: true)

        //When
        let displayData = AssociateCommerceDisplayData.generateAssociateCommerceDisplay(fromPromotion: promotionBO)

        // then
        XCTAssert(displayData.storesDescription == nil)
    }

    func test_givenAPromotionBO_whenICallGenerateAssociateCommerceAndNoHavePhysicalUsageType_thenDisplayDataShowStoresIsFalse() {

        //given
        let promotionBO = PromotionsGenerator.getPromotionDetail(physical: false, oneStore: false)

        //When
        let displayData = AssociateCommerceDisplayData.generateAssociateCommerceDisplay(fromPromotion: promotionBO)

        // then
        XCTAssert(displayData.showStores == false)
    }

    func test_givenAPromotionBO_whenICallGenerateAssociateCommerceAndHaveOnePhysicalUsageType_thenDisplayDataShowStoresIsFalse() {

        //given
        let promotionBO = PromotionsGenerator.getPromotionDetail(physical: true, oneStore: true)

        //When
        let displayData = AssociateCommerceDisplayData.generateAssociateCommerceDisplay(fromPromotion: promotionBO)

        // then
        XCTAssert(displayData.showStores == false)
    }

    func test_givenAPromotionBO_whenICallGenerateAssociateCommerceAndHaveMoreThanOnePhysicalUsageType_thenDisplayDataShowStoresIsTrue() {

        //given
        let promotionBO = PromotionsGenerator.getPromotionDetail(physical: true, oneStore: false)

        //When
        let displayData = AssociateCommerceDisplayData.generateAssociateCommerceDisplay(fromPromotion: promotionBO)

        // then
        XCTAssert(displayData.showStores == true)
    }

    func test_givenAPromotionBO_whenICallGenerateAssociateCommerceAndNoHaveCommerceInformation_thenDisplayDataNoValues() {

        //given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        promotionBO.commerceInformation = nil

        //When
        let displayData = AssociateCommerceDisplayData.generateAssociateCommerceDisplay(fromPromotion: promotionBO)

        // then
        XCTAssert(displayData.commerceAboutName == nil)
        XCTAssert(displayData.commerceDescription == nil)
        XCTAssert(displayData.commerceWebPage == nil)
    }

    func test_givenAPromotionBOWithCommerceInformationAndNoHaveDescription_whenICallGenerateAssociateCommerce_thenDisplayDataNoHaveCommerceDescription() {

        //given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        promotionBO.commerceInformation.description = nil

        //When
        let displayData = AssociateCommerceDisplayData.generateAssociateCommerceDisplay(fromPromotion: promotionBO)

        // then
        XCTAssert(displayData.commerceAboutName == Localizables.promotions.key_promotions_about_text + " " + promotionBO.commerceInformation.name)
        XCTAssert(displayData.commerceDescription == nil)
        XCTAssert(displayData.commerceWebPage == promotionBO.commerceInformation.web)
    }

    func test_givenAPromotionBOWithCommerceInformationAndNoHaveWeb_whenICallGenerateAssociateCommerce_thenDisplayDataNoHaveCommerceWebPage() {

        //given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        promotionBO.commerceInformation.web = nil

        //When
        let displayData = AssociateCommerceDisplayData.generateAssociateCommerceDisplay(fromPromotion: promotionBO)

        // then
        XCTAssert(displayData.commerceAboutName == Localizables.promotions.key_promotions_about_text + " " + promotionBO.commerceInformation.name)
        XCTAssert(displayData.commerceDescription == promotionBO.commerceInformation.description)
        XCTAssert(displayData.commerceWebPage == nil)
    }

    // MARK: - generateStoreDisplay

    func test_givenAStoreBO_whenICallGenerateStoreDisplay_thenDisplayDataShouldBeMatchWithStoreBO() {

        //given
        let storeBO = PromotionsGenerator.getPromotionStore()

        //When
        let displayData = StoreDisplay.generateStoreDisplay(fromStore: storeBO)

        // then
        XCTAssert(displayData.storeName == storeBO.name)
        XCTAssert(displayData.address == storeBO.location?.addressName)
        XCTAssert(displayData.city == storeBO.location?.city)
        XCTAssert(displayData.state == storeBO.location?.state)
        XCTAssert(displayData.zipCode == storeBO.location?.zipCode)
        XCTAssert(displayData.country == storeBO.location?.country?.name)
    }
}
