//
//  AssociatedTradeMapDisplayDataTest.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 21/6/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
@testable import shoppingappMX
#endif

class AssociatedTradeMapDisplayDataTest: XCTestCase {

    func test_givenPromotionBOWithoutStores_whenICallInit_thenAllAttributesAreNil() {

        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        promotionBO.stores = nil

        // when
        let displayData = AssociatedTradeMapDisplayData(promotion: promotionBO)

        // then
        XCTAssert(displayData.distance == nil)
        XCTAssert(displayData.address == nil)
        XCTAssert(displayData.additionalInfo == nil)
        XCTAssert(displayData.latitude == nil)
        XCTAssert(displayData.longitude == nil)
        XCTAssert(displayData.web == nil)
        XCTAssert(displayData.email == nil)
        XCTAssert(displayData.phone == nil)
        XCTAssert(displayData.mobile == nil)

    }

    func test_givenPromotionBO_whenICallInit_thenAllAttributesAreFilled() {

        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        let storeBO = promotionBO.stores![0]
        let location = storeBO.location!
        let address = "\(location.addressName!.capitalized), \(location.zipCode!.capitalized), \(location.city!.capitalized), \(location.state!.capitalized)"

        // when
        let displayData = AssociatedTradeMapDisplayData(promotion: promotionBO)

        // then
        XCTAssert(displayData.distance == nil)
        XCTAssert(displayData.address == address)
        XCTAssert(displayData.additionalInfo == location.additionalInformation)
        XCTAssert(displayData.latitude == location.geolocation?.latitude)
        XCTAssert(displayData.longitude == location.geolocation?.longitude)
        XCTAssert(displayData.web == storeBO.contactDetails![3].contact)
        XCTAssert(displayData.email == storeBO.contactDetails![0].contact)
        XCTAssert(displayData.phone == storeBO.contactDetails![1].contact)
        XCTAssert(displayData.mobile == storeBO.contactDetails![2].contact)

    }

    func test_givenPromotionBOWithDistanceInMetersAndUserLocation_whenICallInit_thenDistanceShowInMetters() {

        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        let storeBO = promotionBO.stores![0]
        storeBO.distance!.lengthType?.id = .metters
        let formattedDistance = storeBO.formattedDistance()!

        let userLocation = GeoLocationBO(withLatitude: 40.0, withLongitude: -3.3)

        // when
        let displayData = AssociatedTradeMapDisplayData(promotion: promotionBO, userLocation: userLocation)

        // then
        XCTAssert(displayData.distance == String(format: Localizables.promotions.key_promotions_near_you_text, formattedDistance))

    }

    func test_givenPromotionBOWithDistanceInMetersAndUserLocation_whenICallInit_thenDistanceShowInKilometters() {

        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        let storeBO = promotionBO.stores![0]
        storeBO.distance!.lengthType?.id = .kilometers
        let formattedDistance = storeBO.formattedDistance()!

        let userLocation = GeoLocationBO(withLatitude: 40.0, withLongitude: -3.3)

        // when
        let displayData = AssociatedTradeMapDisplayData(promotion: promotionBO, userLocation: userLocation)

        // then
        XCTAssert(displayData.distance == String(format: Localizables.promotions.key_promotions_near_you_text, formattedDistance))

    }

    func test_givenPromotionBOWithDistanceInMeters_whenICallInit_thenDistanceIsNil() {

        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        let storeBO = promotionBO.stores![0]
        storeBO.distance!.lengthType?.id = .metters

        // when
        let displayData = AssociatedTradeMapDisplayData(promotion: promotionBO)

        // then
        XCTAssert(displayData.distance == nil)

    }

    func test_givenPromotionBOWithoutZipCodeAndState_whenICallInit_thenAddressOnlyContainNameAndCity() {

        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        let storeBO = promotionBO.stores![0]
        storeBO.location?.zipCode = nil
        storeBO.location?.state = nil
        let location = storeBO.location!
        let address = "\(location.addressName!.capitalized), \(location.city!.capitalized)"

        // when
        let displayData = AssociatedTradeMapDisplayData(promotion: promotionBO)

        // then
        XCTAssert(displayData.address == address)

    }

    func test_givenPromotionBOWithOnlyContactPhone_whenICallInit_thenOnlyContactPhoneIsFilled() {

        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        let storeBO = promotionBO.stores![0]
        storeBO.contactDetails![0].contact = ""
        storeBO.contactDetails![2].contact = ""
        storeBO.contactDetails![3].contact = ""

        // when
        let displayData = AssociatedTradeMapDisplayData(promotion: promotionBO)

        // then
        XCTAssert(displayData.web == nil)
        XCTAssert(displayData.email == nil)
        XCTAssert(displayData.phone == storeBO.contactDetails![1].contact)
        XCTAssert(displayData.mobile == nil)

    }

    func test_givenPromotionBOWithAdditionalInformationEmpty_whenICallInit_thenAdditionalInformationIsNil() {

        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        let storeBO = promotionBO.stores![0]
        storeBO.location?.additionalInformation = ""

        // when
        let displayData = AssociatedTradeMapDisplayData(promotion: promotionBO)

        // then
        XCTAssert(displayData.additionalInfo == nil)

    }

    func test_givenPromotionBOWitoutPromotionType_whenICallInit_thenPromotionTypeColorIsNil() {

        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        promotionBO.promotionType = nil

        // when
        let displayData = AssociatedTradeMapDisplayData(promotion: promotionBO)

        // then
        XCTAssert(displayData.promotionTypeColor == nil)

    }

    func test_givenPromotionBOWitoutPromotionTypeId_whenICallInit_thenPromotionTypeColorIsNil() {

        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        promotionBO.promotionType.id = nil

        // when
        let displayData = AssociatedTradeMapDisplayData(promotion: promotionBO)

        // then
        XCTAssert(displayData.promotionTypeColor == nil)

    }

    func test_givenPromotionBO_whenICallInit_thenPromotionTypeColorMatch() {

        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()

        // when
        let displayData = AssociatedTradeMapDisplayData(promotion: promotionBO)

        // then
        XCTAssert(displayData.promotionTypeColor == promotionBO.promotionType.id.color)

    }
}
