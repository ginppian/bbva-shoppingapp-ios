//
//  BasicDetailPresenterTest.swift
//  shoppingapp
//
//  Created by Luis Monroy on 01/05/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class BasicDetailPresenterTest: XCTestCase {

    let sut = BasicDetailPresenter()

    // MARK: - receiveforShowDisplayItemPromotionDetail

    func test_givenAViewAndDisplayData_whenICallReceiveforShowDisplayItemPromotionDetail_thenICallViewShowDisplayData() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let promotionBO = PromotionsGenerator.getPromotionDetail()
        let displayDataDetail = DisplayItemPromotionDetail.generateDisplayItemPromotionDetail(fromPromotion: promotionBO)

        // when
        sut.receiveforShowDisplayItemPromotionDetail(withDisplayItemPromotionDetail: displayDataDetail)

        // then
        XCTAssert(dummyView.isCalledShowDisplayData == true)
    }

    func test_givenAViewAndDisplayData_whenICallReceiveforShowDisplayItemPromotionDetail_thenHaveCorrectValuesToShow() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let promotionBO = PromotionsGenerator.getPromotionDetail()
        let displayDataDetail = DisplayItemPromotionDetail.generateDisplayItemPromotionDetail(fromPromotion: promotionBO)

        // when
        sut.receiveforShowDisplayItemPromotionDetail(withDisplayItemPromotionDetail: displayDataDetail)

        // then
        XCTAssert(dummyView.displayItemDetail.legalConditions == displayDataDetail.legalConditions)
        XCTAssert(dummyView.displayItemDetail.legalConditionsURL == displayDataDetail.legalConditionsURL)
        XCTAssert(dummyView.displayItemDetail.commerceName == displayDataDetail.commerceName)
        XCTAssert(dummyView.displayItemDetail.commerceDescription == displayDataDetail.commerceDescription)
        XCTAssert(dummyView.displayItemDetail.extendedDescription == displayDataDetail.extendedDescription)
    }

    // MARK: - receiveforUpdateDisplayItemPromotionDetail

    func test_givenAViewAndDisplayData_whenICallReceiveforUpdateDisplayItemPromotionDetail_thenICallViewUpdateDisplayData() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let promotionBO = PromotionsGenerator.getPromotionDetail()
        let displayDataDetail = DisplayItemPromotionDetail.generateDisplayItemPromotionDetail(fromPromotion: promotionBO)

        // when
        sut.receiveforUpdateDisplayItemPromotionDetail(withDisplayItemPromotionDetail: displayDataDetail)

        // then
        XCTAssert(dummyView.isCalledUpdateDisplayData == true)
    }

    func test_givenAViewAndDisplayData_whenICallReceiveforUpdateDisplayItemPromotionDetail_thenHaveCorrectValuesToShow() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let promotionBO = PromotionsGenerator.getPromotionDetail()
        let displayDataDetail = DisplayItemPromotionDetail.generateDisplayItemPromotionDetail(fromPromotion: promotionBO)

        // when
        sut.receiveforUpdateDisplayItemPromotionDetail(withDisplayItemPromotionDetail: displayDataDetail)

        // then
        XCTAssert(dummyView.displayItemDetail.legalConditions == displayDataDetail.legalConditions)
        XCTAssert(dummyView.displayItemDetail.legalConditionsURL == displayDataDetail.legalConditionsURL)
        XCTAssert(dummyView.displayItemDetail.commerceName == displayDataDetail.commerceName)
        XCTAssert(dummyView.displayItemDetail.commerceDescription == displayDataDetail.commerceDescription)
        XCTAssert(dummyView.displayItemDetail.extendedDescription == displayDataDetail.extendedDescription)
    }

    // MARK: - DummyData

    class DummyView: BasicDetailViewProtocol {

        var isCalledShowDisplayData = false
        var isCalledUpdateDisplayData = false
        var displayItemDetail: DisplayItemPromotionDetail!

        func showDisplayData(withDisplayItemPromotionDetail displayItemPromotionDetail: DisplayItemPromotionDetail) {

            isCalledShowDisplayData = true
            displayItemDetail = displayItemPromotionDetail
        }

        func updateDisplayData(withDisplayItemPromotionDetail displayItemPromotionDetail: DisplayItemPromotionDetail) {

            isCalledUpdateDisplayData = true
            displayItemDetail = displayItemPromotionDetail
        }
    }
}
