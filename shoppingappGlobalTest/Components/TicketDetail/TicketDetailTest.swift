//
//  TicketDetailTest.swift
//  shoppingapp
//
//  Created by ruben.fernandez on 9/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class TicketDetailTest: XCTestCase {

    let sut = TicketDetailComponentPresenter()

    func test_givenAView_whenIReceiveDisplayData_thenICallShowData() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        let displayTicket = TicketDisplayData()

        // when
        sut.showTicketdata(withTicketData: displayTicket)

        // then
        XCTAssert(dummyView.isCalledShowInfo == true)

    }

    func test_givenAView_whenIReceiveDisplayData_thenHaveData() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        let displayTicket = TicketDisplayData()

        // when
        sut.showTicketdata(withTicketData: displayTicket)

        // then
        XCTAssert(dummyView.ticketDisplayData != nil)

    }

    class DummyView: TicketDetailViewComponentProtocol {

        var isCalledShowInfo = false
        var ticketDisplayData: TicketDisplayData?

        func showTicketdata(withTicketData ticketData: TicketDisplayData) {
            isCalledShowInfo = true
            ticketDisplayData = ticketData
        }
    }

}
