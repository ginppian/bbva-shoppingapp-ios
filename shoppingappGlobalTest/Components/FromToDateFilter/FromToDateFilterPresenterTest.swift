//
//  FromToDateFilterPresenterTest.swift
//  shoppingapp
//
//  Created by jesus.martinez on 28/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class FromToDateFilterPresenterTest: XCTestCase {

    let sut = FromToDateFilterPresenter()

    // MARK: - OnConfigureView

    func test_givenAnyView_whenICallOnConfigureView_thenICallViewRotateSeparator() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.onConfigureView()

        // then
        XCTAssert(dummyView.isCalledRotationSeparatorView == true)

    }

    func test_givenAnyView_whenICallOnConfigureView_thenICallViewRotateSeparatorWithCenterValue() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.onConfigureView()

        // then
        XCTAssert(dummyView.rotatationTo == .center)

    }

    func test_givenAnyView_whenICallOnConfigureView_thenIDoNotCallViewUpdateViewWithDisplayData() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.onConfigureView()

        // then
        XCTAssert(dummyView.isCalledUpdateViewWithDisplayData == false)

    }

    func test_givenAnyViewAndDisplayData_whenICallOnConfigureView_thenICallViewUpdateViewWithDisplayData() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let fromDisplayData = DateFilterDisplayData(minDate: nil, maxDate: Date(), title: Localizables.transactions.key_transactions_from_text, dateSelectedTitle: Localizables.transactions.key_transactions_pickdate_text, dateSelected: nil)

        let toDisplayData = DateFilterDisplayData(minDate: nil, maxDate: Date(), title: Localizables.transactions.key_transactions_to_text, dateSelectedTitle: Localizables.transactions.key_transactions_pickdate_text, dateSelected: Date().string(format: Date.DATE_FORMAT_DAY_MONTH_YEAR).capitalized)

        let fromToDisplayData = FromToFilterDisplayData(fromDisplayData: fromDisplayData, toDisplayData: toDisplayData)

        sut.displayData = fromToDisplayData

        // when
        sut.onConfigureView()

        // then
        XCTAssert(dummyView.isCalledUpdateViewWithDisplayData == true)

    }

    func test_givenAnyViewAndDisplayData_whenICallOnConfigureView_thenICallViewUpdateViewWithDisplayDataWithTheSameDisplayData() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let fromDisplayData = DateFilterDisplayData(minDate: nil, maxDate: Date(), title: Localizables.transactions.key_transactions_from_text, dateSelectedTitle: Localizables.transactions.key_transactions_pickdate_text, dateSelected: nil)

        let toDisplayData = DateFilterDisplayData(minDate: nil, maxDate: Date(), title: Localizables.transactions.key_transactions_to_text, dateSelectedTitle: Localizables.transactions.key_transactions_pickdate_text, dateSelected: Date().string(format: Date.DATE_FORMAT_DAY_MONTH_YEAR).capitalized
        )

        let fromToDisplayData = FromToFilterDisplayData(fromDisplayData: fromDisplayData, toDisplayData: toDisplayData)

        sut.displayData = fromToDisplayData

        // when
        sut.onConfigureView()

        // then
        XCTAssert(dummyView.displayData! == fromToDisplayData)

    }

    // MARK: - startEditingFromFilter

    func test_givenAnyView_whenICallStartEditing_thenICallViewRotateSeparator() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.startEditingFromFilter()

        // then
        XCTAssert(dummyView.isCalledRotationSeparatorView == true)

    }

    func test_givenAnyView_whenICallStartEditing_thenICallViewRotateSeparatorWithLeftValue() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.startEditingFromFilter()

        // then
        XCTAssert(dummyView.rotatationTo == .left)

    }

    // MARK: - endEditingFromFilter

    func test_givenAnyView_whenICallEndEditingFromFilter_thenICallViewRotateSeparator() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.endEditingFromFilter()

        // then
        XCTAssert(dummyView.isCalledRotationSeparatorView == true)

    }

    func test_givenAnyView_whenICallEndEditingFromFilter_thenICallViewRotateSeparatorWithCenterValue() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.endEditingFromFilter()

        // then
        XCTAssert(dummyView.rotatationTo == .center)

    }

    // MARK: - startEditingToFilter

    func test_givenAnyView_whenICallStartEditingToFilter_thenICallViewRotateSeparator() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.startEditingToFilter()

        // then
        XCTAssert(dummyView.isCalledRotationSeparatorView == true)

    }

    func test_givenAnyView_whenICallStartEditingToFiler_thenICallViewRotateSeparatorWithRightValue() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.startEditingToFilter()

        // then
        XCTAssert(dummyView.rotatationTo == .right)

    }

    // MARK: - endEditingToFilter

    func test_givenAnyView_whenICallEndEditingToFilter_thenICallViewRotateSeparator() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.endEditingToFilter()

        // then
        XCTAssert(dummyView.isCalledRotationSeparatorView == true)

    }

    func test_givenAnyView_whenICallEndEditingToFilter_thenICallViewRotateSeparatorWithCenterValue() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.endEditingToFilter()

        // then
        XCTAssert(dummyView.rotatationTo == .center)

    }

    // MARK: - fromDateFilterChanged

    func test_givenAView_whenICallFromDateFilterChanged_thenICallViewUpdateToDateFilterWithMinimumDate() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.fromDateFilterChanged(date: Date())

        // then
        XCTAssert(dummyView.isCalledUpdateToDateFilterWithMinimumDate == true)

    }

    func test_givenAViewAndADate_whenICallFromDateFilterChanged_thenICallViewUpdateToDateFilterWithMinimumDateWithTheSameDate() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let date = Date()

        // when
        sut.fromDateFilterChanged(date: date)

        // then
        XCTAssert(dummyView.date == date)

    }

    // MARK: - toDateFilterChanged

    func test_givenAView_whenICallToDateFilterChanged_thenICallViewUpdateFromDateFilterWithMaximumDate() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.toDateFilterChanged(date: Date())

        // then
        XCTAssert(dummyView.isCalledUpdateFromDateFilterWithMaximumDate == true)

    }

    func test_givenAViewAndADate_whenICallToDateFilterChanged_thenICallViewUpdateFromDateFilterWithMaximumDateWithTheSameDate() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let date = Date()

        // when
        sut.fromDateFilterChanged(date: date)

        // then
        XCTAssert(dummyView.date == date)

    }

    class DummyView: FromToDateFilterViewProtocol {

        var isCalledRotationSeparatorView = false
        var isCalledUpdateFromDateFilterWithMaximumDate = false
        var isCalledUpdateToDateFilterWithMinimumDate = false
        var isCalledUpdateViewWithDisplayData = false

        var displayData: FromToFilterDisplayData?
        var rotatationTo: SeparatorPosition?

        var date: Date?

        func rotateSeparatorView(to: SeparatorPosition) {
            isCalledRotationSeparatorView = true
            rotatationTo = to
        }

        func updateFromDateFilter(withMaximumDate maximumDate: Date) {
            isCalledUpdateFromDateFilterWithMaximumDate = true
            self.date = maximumDate
        }

        func updateToDateFilter(withMiniumDate minimumDate: Date) {
            isCalledUpdateToDateFilterWithMinimumDate = true
            self.date = minimumDate
        }

        func updateView(withDisplayData displayData: FromToFilterDisplayData) {
            isCalledUpdateViewWithDisplayData = true
            self.displayData = displayData
        }
    }

}
