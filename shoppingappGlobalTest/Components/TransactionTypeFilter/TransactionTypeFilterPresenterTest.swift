//
//  TransactionTypeFilterPresenterTest.swift
//  shoppingapp
//
//  Created by jesus.martinez on 3/8/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import XCTest
import RxSwift

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class TransactionTypeFilterPresenterTest: XCTestCase {

    let sut = TransactionTypeFilterPresenter()

    // MARK: - onConfigureView

    func test_givenView_whenICallOnConfigureView_thenICallViewShowMoreOptionsView() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.onConfigureView()

        // then
        XCTAssert(dummyView.isCalledShowMoreOptionsView == true)

    }

    func test_givenAnInteractor_whenICallOnConfigureView_thenICallInteractorProvideTransactionFilterTypes() {

        // given
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // when
        sut.onConfigureView()

        // then
        XCTAssert(dummyInteractor.isCalledProvideTransactionFilterTypes == true)

    }

    func test_givenAnInteractor_whenICallOnConfigureView_thenFilterTypesMatchWithReturnedByInteractor() {

        //given
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        //when
        sut.onConfigureView()

        //then
        XCTAssert(  (sut.transactionTypes?[0] === dummyInteractor.transactionTypes[0]) &&
            (sut.transactionTypes?[1] === dummyInteractor.transactionTypes[1]) &&
            (sut.transactionTypes?[2] === dummyInteractor.transactionTypes[2]))
    }

    // MARK: - showMoreOptionsAction

    func test_givenViewAndTransactionTypes_whenICallShowMoreOptionsAction_thenICallViewShowTransactionOptionsView() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        var transactionTypes = [TransactionsTypeFilterBO]()
        transactionTypes.append(TransactionsTypeFilterBO(type: "ALL", titleKey: "titleKey1"))
        transactionTypes.append(TransactionsTypeFilterBO(type: "INCOMES", titleKey: "titleKey2"))
        sut.transactionTypes = transactionTypes

        // when
        sut.showMoreOptionsAction()

        // then
        XCTAssert(dummyView.isCalledTransactionFilterTypes == true)

    }

    func test_givenViewAndTransactionTypes_whenICallShowMoreOptionsAction_thenICallViewShowTransactionOptionsViewWithTitleKeysOfTransactionTypes() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        var transactionTypes = [TransactionsTypeFilterBO]()
        transactionTypes.append(TransactionsTypeFilterBO(type: "ALL", titleKey: "titleKey1"))
        transactionTypes.append(TransactionsTypeFilterBO(type: "INCOMES", titleKey: "titleKey2"))
        sut.transactionTypes = transactionTypes

        // when
        sut.showMoreOptionsAction()

        // then
        XCTAssert(dummyView.transactionTitles.count == 2)

        for i in 0..<dummyView.transactionTitles.count {
            XCTAssert(transactionTypes[i].titleKey == dummyView.transactionTitles[i])
        }

    }

    func test_givenViewAndTransactionTypes_whenICallShowMoreOptionsAction_thenICallViewInforMoreFilters() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        var transactionTypes = [TransactionsTypeFilterBO]()
        transactionTypes.append(TransactionsTypeFilterBO(type: "ALL", titleKey: "titleKey1"))
        transactionTypes.append(TransactionsTypeFilterBO(type: "INCOMES", titleKey: "titleKey2"))
        sut.transactionTypes = transactionTypes

        // when
        sut.showMoreOptionsAction()

        // then
        XCTAssert(dummyView.isCalledInformMoreFilters == true)

    }

    class DummyView: TransactionTypeFilterViewProtocol {

        var isCalledShowMoreOptionsView = false
        var isCalledTransactionFilterTypes = false
        var isCalledInformMoreFilters = false

        var transactionTitles = [String]()

        func showMoreOptionsView() {
            isCalledShowMoreOptionsView = true
        }

        func showTransaction(filterTypes: [String]) {
            isCalledTransactionFilterTypes = true
            transactionTitles = filterTypes
        }

        func informMoreFilters() {
            isCalledInformMoreFilters = true
        }

        func informSelectedType(_ type: TransactionFilterType) {
        }

        func setSelectedOption(atIndex index: Int) {
        }

    }

    class DummyInteractor: TransactionTypeFilterInteractorProtocol {

        var isCalledProvideTransactionFilterTypes = false

        var transactionTypes: [TransactionsTypeFilterBO] = []

        func provideTransactionFilterTypes() -> Observable<[ModelBO]> {

            isCalledProvideTransactionFilterTypes = true

            let type1 = TransactionsTypeFilterBO(type: "ALL", titleKey: Localizables.transactions.key_transactions_all_text)
            let type2 = TransactionsTypeFilterBO(type: "INCOMES", titleKey: Localizables.transactions.key_transactions_income_text)
            let type3 = TransactionsTypeFilterBO(type: "EXPENSES", titleKey: Localizables.transactions.key_transactions_expenses_text)
            let modelBO: [ModelBO] = [type1, type2, type3]

            transactionTypes = modelBO as! [TransactionsTypeFilterBO]

            return Observable<[ModelBO]>.just(modelBO as [ModelBO])
        }
    }

}
