//
//  FromToAmountFilterPresenterTest.swift
//  shoppingapp
//
//  Created by jesus.martinez on 2/8/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class FromToAmountFilterPresenterTest: XCTestCase {

    let sut = FromToAmountFilterPresenter()

    // MARK: - fromChanged

    func test_givenView_whenICallFromChangedAmount_thenFromAmountShouldMatchWithAmountFromParameter() {

        // expected
        let amountChanged = NSDecimalNumber(string: "12")

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.fromChanged(amount: amountChanged)

        // then
        XCTAssert(sut.fromAmount == amountChanged)

    }

    func test_givenViewAndToAmount_whenICallFromChangeAmountBiggerThanToAmount_thenICallViewShowError() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let displayData = FromToAmountFilterDisplayData(error: "Importe incorrecto")
        sut.displayData = displayData

        sut.toAmount = NSDecimalNumber(string: "20")

        // when
        sut.fromChanged(amount: NSDecimalNumber(string: "30"))

        // then
        XCTAssert(dummyView.isCalledShowError == true)

    }

    func test_givenViewAndToAmount_whenICallFromChangeAmountSmallerThanToAmount_thenICallViewShowNormal() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        sut.toAmount = NSDecimalNumber(string: "30")

        // when
        sut.fromChanged(amount: NSDecimalNumber(string: "20"))

        // then
        XCTAssert(dummyView.isCalledShowNormal == true)

    }

    func test_givenView_whenICallFromChangeAmount_thenIDoNotCallViewShowError() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.fromChanged(amount: NSDecimalNumber(string: "20"))

        // then
        XCTAssert(dummyView.isCalledShowError == false)

    }

    func test_givenViewAndToAmount_whenICallFromChangeWithAnNaN_thenFromAmountShouldBeNil() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let displayData = FromToAmountFilterDisplayData(error: "Importe incorrecto")
        sut.displayData = displayData

        sut.toAmount = NSDecimalNumber(string: "20")

        // when
        sut.fromChanged(amount: NSDecimalNumber(string: ""))

        // then
        XCTAssert(sut.fromAmount == nil)

    }

    func test_givenViewAndToAmount_whenICallFromChangeWithAnNaN_thenICallViewShowNormal() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let displayData = FromToAmountFilterDisplayData(error: "Importe incorrecto")
        sut.displayData = displayData

        sut.toAmount = NSDecimalNumber(string: "20")

        // when
        sut.fromChanged(amount: NSDecimalNumber(string: ""))

        // then
        XCTAssert(dummyView.isCalledShowNormal == true)

    }

    func test_givenViewAndToAmount_whenICallFromChangeWithAnNaN_thenIDoNotCallViewShowError() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let displayData = FromToAmountFilterDisplayData(error: "Importe incorrecto")
        sut.displayData = displayData

        sut.toAmount = NSDecimalNumber(string: "20")

        // when
        sut.fromChanged(amount: NSDecimalNumber(string: ""))

        // then
        XCTAssert(dummyView.isCalledShowError == false)

    }

    // MARK: - toChanged

    func test_givenView_whenICallToChangedAmount_thenFromAmountShouldMatchWithAmountFromParameter() {

        // expected
        let amountChanged = NSDecimalNumber(string: "12")

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.toChanged(amount: amountChanged)

        // then
        XCTAssert(sut.toAmount == amountChanged)
    }

    func test_givenViewAndFromAmount_whenICallToChangeAmountSmallerThanToAmount_thenICallViewShowError() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let displayData = FromToAmountFilterDisplayData(error: "Importe incorrecto")
        sut.displayData = displayData

        sut.fromAmount = NSDecimalNumber(string: "30")

        // when
        sut.toChanged(amount: NSDecimalNumber(string: "20"))

        // then
        XCTAssert(dummyView.isCalledShowError == true)

    }

    func test_givenViewAndFromAmountAndDisplayData_whenICallToChangeAmountSmallerThanToAmount_thenICallViewShowErrorWithErrorOfDisplayData() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let displayData = FromToAmountFilterDisplayData(error: "Importe incorrecto")
        sut.displayData = displayData

        sut.fromAmount = NSDecimalNumber(string: "30")

        // when
        sut.toChanged(amount: NSDecimalNumber(string: "20"))

        // then
        XCTAssert(dummyView.error == displayData.error)

    }

    func test_givenViewAndFromAmount_whenICallTohangeAmountBiggerThanToAmount_thenICallViewShowNormal() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        sut.fromAmount = NSDecimalNumber(string: "20")

        // when
        sut.toChanged(amount: NSDecimalNumber(string: "30"))

        // then
        XCTAssert(dummyView.isCalledShowNormal == true)

    }

    func test_givenView_whenICallToChangeAmount_thenIDoNotCallViewShowError() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.toChanged(amount: NSDecimalNumber(string: "20"))

        // then
        XCTAssert(dummyView.isCalledShowError == false)

    }

    func test_givenViewAndFromAmount_whenICallToChangeWithAnNaN_thenToAmountShouldBeNil() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let displayData = FromToAmountFilterDisplayData(error: "Importe incorrecto")
        sut.displayData = displayData

        sut.fromAmount = NSDecimalNumber(string: "20")

        // when
        sut.toChanged(amount: NSDecimalNumber(string: ""))

        // then
        XCTAssert(sut.toAmount == nil)

    }

    func test_givenViewAndFromAmount_whenICallToChangeWithAnNaN_thenICallViewShowNormal() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let displayData = FromToAmountFilterDisplayData(error: "Importe incorrecto")
        sut.displayData = displayData

        sut.fromAmount = NSDecimalNumber(string: "20")

        // when
        sut.toChanged(amount: NSDecimalNumber(string: ""))

        // then
        XCTAssert(dummyView.isCalledShowNormal == true)

    }

    func test_givenViewAndFromAmount_whenICallToChangeWithAnNaN_thenIDoNotCallViewShowError() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let displayData = FromToAmountFilterDisplayData(error: "Importe incorrecto")
        sut.displayData = displayData

        sut.fromAmount = NSDecimalNumber(string: "20")

        // when
        sut.toChanged(amount: NSDecimalNumber(string: ""))

        // then
        XCTAssert(dummyView.isCalledShowError == false)

    }

    func test_givenView_whenICallToRemoveAmountValues_thenICallisCalledRemoveAmountValues() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.removeAmountValues()

        // then
        XCTAssert(dummyView.isCalledRemoveAmountValues == true)

    }

    class DummyView: FromToAmountFilterViewProtocol {

        var isCalledShowNormal = false
        var isCalledShowError = false
        var isCalledRemoveAmountValues = false

        var error: String?

        func showNormal() {
            isCalledShowNormal = true
        }

        func show(error: String) {
            isCalledShowError = true
            self.error = error
        }

        func removeAmountValues() {
            isCalledRemoveAmountValues = true
        }
    }

}
