//
//  BalancesPresenterTest.swift
//  shoppingapp
//
//  Created by ruben.fernandez on 22/9/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class PointsPresenterTest: XCTestCase {

    let sut = PointsPresenter()

    func test_givenAView_whenIReceiveDisplayData_thenICallShowData() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        let displayPoints = PointsDisplayData()

        // when
        sut.receivePoinsDisplayData(withPointsDisplayData: displayPoints)

        // then
        XCTAssert(dummyView.isCalledShowInfo == true)

    }

    class DummyView: PointsViewProtocol {

        var isCalledShowInfo = false

        func showDisplayData(withPointsDisplayData pointsDisplayData: PointsDisplayData) {
            isCalledShowInfo = true
        }
    }

}
