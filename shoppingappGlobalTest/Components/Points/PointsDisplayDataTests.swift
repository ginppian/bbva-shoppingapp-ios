//
//  PointsDisplayDataTests.swift
//  shoppingapp
//
//  Created by Ruben Fernandez on 25/01/18.
//  Copyright © 2017 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class PointsDisplayDataTests: XCTestCase {

    // MARK: - generatePointsDisplayData

    func test_givenCardsBO_whenICallGeneratePointsDisplayData_thenHaveCorrectValuesToShow() {

        // given
        let cardsBO = CardsGenerator.getCardsWithRewards()

        // when
        let displayData0 = PointsDisplayData.generatePointsDisplayData(withCardBO: cardsBO.cards[0])
        let displayData1 = PointsDisplayData.generatePointsDisplayData(withCardBO: cardsBO.cards[1])
        let displayData2 = PointsDisplayData.generatePointsDisplayData(withCardBO: cardsBO.cards[2])
        let displayData3 = PointsDisplayData.generatePointsDisplayData(withCardBO: cardsBO.cards[3])
        let displayData4 = PointsDisplayData.generatePointsDisplayData(withCardBO: cardsBO.cards[4])
        let displayData5 = PointsDisplayData.generatePointsDisplayData(withCardBO: cardsBO.cards[5])
        let displayData6 = PointsDisplayData.generatePointsDisplayData(withCardBO: cardsBO.cards[6])
        let displayData7 = PointsDisplayData.generatePointsDisplayData(withCardBO: cardsBO.cards[7])

        // then
        XCTAssert(displayData0.needShowPoinst == false)
        XCTAssert(displayData1.needShowPoinst == false)
        XCTAssert(displayData2.needShowPoinst == true)
        XCTAssert(displayData3.needShowPoinst == false)
        XCTAssert(displayData4.needShowPoinst == false)
        XCTAssert(displayData5.needShowPoinst == true)
        XCTAssert(displayData6.needShowPoinst == true)
        XCTAssert(displayData7.needShowPoinst == true)

    }

    func test_givenCardsBO_whenICallGeneratePointsDisplayData_thenHaveCorrectValuesForPoints() {

        // given
        let cardsBO = CardsGenerator.getCardsWithRewards()

        // when
        let displayData0 = PointsDisplayData.generatePointsDisplayData(withCardBO: cardsBO.cards[0])
        let displayData1 = PointsDisplayData.generatePointsDisplayData(withCardBO: cardsBO.cards[1])
        let displayData2 = PointsDisplayData.generatePointsDisplayData(withCardBO: cardsBO.cards[2])
        let displayData3 = PointsDisplayData.generatePointsDisplayData(withCardBO: cardsBO.cards[3])
        let displayData4 = PointsDisplayData.generatePointsDisplayData(withCardBO: cardsBO.cards[4])
        let displayData5 = PointsDisplayData.generatePointsDisplayData(withCardBO: cardsBO.cards[5])
        let displayData6 = PointsDisplayData.generatePointsDisplayData(withCardBO: cardsBO.cards[6])
        let displayData7 = PointsDisplayData.generatePointsDisplayData(withCardBO: cardsBO.cards[7])

        //then
        XCTAssert(displayData0.points == nil)
        XCTAssert(displayData1.points == nil)

        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = .decimal

        XCTAssert(displayData2.points == NSAttributedString(string: numberFormatter.string(from: 4000000)!, attributes: [
            NSAttributedStringKey.foregroundColor: UIColor.BBVA600,
            NSAttributedStringKey.font: Tools.setFontBook(size: 24)
            ]))
        XCTAssert(displayData3.points == nil)
        XCTAssert(displayData4.points == nil)
        XCTAssert(displayData5.points == NSAttributedString(string: numberFormatter.string(from: 8000)!, attributes: [
            NSAttributedStringKey.foregroundColor: UIColor.BBVA600,
            NSAttributedStringKey.font: Tools.setFontBook(size: 24)
            ]))
        XCTAssert(displayData6.points == nil)
        XCTAssert(displayData7.points == NSAttributedString(string: "123", attributes: [
            NSAttributedStringKey.foregroundColor: UIColor.BBVA600,
            NSAttributedStringKey.font: Tools.setFontBook(size: 24)
            ]))

    }

    func test_givenCardsBO_whenICallGeneratePointsDisplayData_thenHaveCorrectValuesForDetail() {

        // given
        let cardsBO = CardsGenerator.getCardsWithRewards()

        // when
        let displayData0 = PointsDisplayData.generatePointsDisplayData(withCardBO: cardsBO.cards[0])
        let displayData1 = PointsDisplayData.generatePointsDisplayData(withCardBO: cardsBO.cards[1])
        let displayData2 = PointsDisplayData.generatePointsDisplayData(withCardBO: cardsBO.cards[2])
        let displayData3 = PointsDisplayData.generatePointsDisplayData(withCardBO: cardsBO.cards[3])
        let displayData4 = PointsDisplayData.generatePointsDisplayData(withCardBO: cardsBO.cards[4])
        let displayData5 = PointsDisplayData.generatePointsDisplayData(withCardBO: cardsBO.cards[5])
        let displayData6 = PointsDisplayData.generatePointsDisplayData(withCardBO: cardsBO.cards[6])
        let displayData7 = PointsDisplayData.generatePointsDisplayData(withCardBO: cardsBO.cards[7])

        //then
        XCTAssert(displayData0.descriptionPoints == nil)
        XCTAssert(displayData1.descriptionPoints == nil)
        XCTAssert(displayData2.descriptionPoints == NSAttributedString(string: "Puntosbancomer", attributes: [
            NSAttributedStringKey.foregroundColor: UIColor.BBVA600,
            NSAttributedStringKey.font: Tools.setFontBookItalic(size: 14)
            ]))
        XCTAssert(displayData3.descriptionPoints == nil)
        XCTAssert(displayData4.descriptionPoints == nil)
        XCTAssert(displayData5.descriptionPoints == NSAttributedString(string: "Puntosbancomer", attributes: [
            NSAttributedStringKey.foregroundColor: UIColor.BBVA600,
            NSAttributedStringKey.font: Tools.setFontBookItalic(size: 14)
            ]))
        XCTAssert(displayData6.descriptionPoints == NSAttributedString(string: Localizables.cards.key_cards_accumulate_points_text, attributes: [
            NSAttributedStringKey.foregroundColor: UIColor.BBVA600,
            NSAttributedStringKey.font: Tools.setFontBookItalic(size: 14)
            ]))

        XCTAssert(displayData7.descriptionPoints == NSAttributedString(string: "Puntosbancomer", attributes: [
            NSAttributedStringKey.foregroundColor: UIColor.BBVA600,
            NSAttributedStringKey.font: Tools.setFontBookItalic(size: 14)
            ]))

    }

    func test_givenCardsBO_whenICallGeneratePointsDisplayData_thenHaveCorrectValuesForImage() {

        // given
        let cardsBO = CardsGenerator.getCardsWithRewards()

        // when
        let displayData0 = PointsDisplayData.generatePointsDisplayData(withCardBO: cardsBO.cards[0])
        let displayData1 = PointsDisplayData.generatePointsDisplayData(withCardBO: cardsBO.cards[1])
        let displayData2 = PointsDisplayData.generatePointsDisplayData(withCardBO: cardsBO.cards[2])
        let displayData3 = PointsDisplayData.generatePointsDisplayData(withCardBO: cardsBO.cards[3])
        let displayData4 = PointsDisplayData.generatePointsDisplayData(withCardBO: cardsBO.cards[4])
        let displayData5 = PointsDisplayData.generatePointsDisplayData(withCardBO: cardsBO.cards[5])
        let displayData6 = PointsDisplayData.generatePointsDisplayData(withCardBO: cardsBO.cards[6])
        let displayData7 = PointsDisplayData.generatePointsDisplayData(withCardBO: cardsBO.cards[7])

        //then
        XCTAssert(displayData0.imageBackgroundName == "ilu_points_card_landing")
        XCTAssert(displayData1.imageBackgroundName == "ilu_points_card_landing")
        XCTAssert(displayData2.imageBackgroundName == "ilu_points_card_landing")
        XCTAssert(displayData3.imageBackgroundName == "ilu_points_card_landing")
        XCTAssert(displayData4.imageBackgroundName == "ilu_points_card_landing")
        XCTAssert(displayData5.imageBackgroundName == "ilu_points_card_landing")
        XCTAssert(displayData6.imageBackgroundName == "ilu_points_card_landing")
        XCTAssert(displayData7.imageBackgroundName == "ilu_points_card_landing")

    }

}
