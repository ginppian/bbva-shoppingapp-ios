//
//  ShoppingContactPresenterTest.swift
//  shoppingapp
//
//  Created by Rubén Jacobo on 10/17/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest
import RxSwift
import CellsNativeComponents

#if TESTMX
@testable import shoppingappMX
#elseif TESTPE
@testable import shoppingappPE
#endif

class ShoppingContactPresenterTest: XCTestCase {
    
    var sut: ShoppingContactPresenter<DummyView>!
    
    override func setUp() {
        
        super.setUp()
        DummyPreferencesManager.configureDummyPreferences()
        sut = ShoppingContactPresenter<DummyView>()
    }
    
    // MARK: - ViewLoaded
    
    // MARK: - ViewLoaded - Show contact details
    
    func test_givenPromotionBO_whenICallViewLoaded_thenICallShowContactDetails() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        
        sut.promotionBO = promotionBO
        
        // when
        sut.viewLoaded()
        
        //then
        XCTAssert(dummyView.isCalledShowContactDetails)
        
    }
    
    // MARK: - ViewLoaded - ContactDisplayData should be correct
    
    func test_givenContactDisplayData_whenICallViewLoaded_thenContactDisplayDataShouldBeCorrect() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        
        sut.promotionBO = promotionBO
        
        let contactDisplayData = ContactDisplayData.generateContactDisplayData(fromPromotion: promotionBO)
        
        // when
        sut.viewLoaded()
        
        //then
        XCTAssert(dummyView.contactDisplayData == contactDisplayData)
        
    }
    
    // MARK: - Pressed send email contact
    
    func test_givenContactDisplayData_whenICallPressedSendEmailContact_thenICallSendEmail() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        
        sut.promotionBO = promotionBO
        
        let contactDisplayData = ContactDisplayData.generateContactDisplayData(fromPromotion: promotionBO)[0]
        
        // when
        sut.pressedSendEmailContact(contactemail: contactDisplayData.info)
        
        //then
        XCTAssert(dummyView.isCalledPressedSendEmailContact)
        
    }
    
    func test_givenContactDisplayData_whenICallPressedCallToPhoneContact_thenICallToPhone() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        
        sut.promotionBO = promotionBO
        
        let contactDisplayData = ContactDisplayData(id: .phone, info: Settings.Global.default_bbva_phone_number)
        
        // when
        sut.pressedCallToPhoneContact(contactPhone: contactDisplayData.info)
        
        //then
        XCTAssert(dummyView.isCalledPressedCallToPhoneContact)
        
    }
    
    // MARK: - DummyData
    
    class DummyInteractor: ShoppingContactInteractorProtocol {
        
        var presenter: ShoppingContactPresenter<DummyView>?
    }
    
    class DummyView: ShoppingContactViewProtocol, ViewProtocol {
        
        var contactDisplayData: [ContactDisplayData]?
        var isCalledShowContactDetails = false
        var isCalledPressedCallToPhoneContact = false
        var isCalledPressedSendEmailContact = false
        
        func showContactDetails(details: [ContactDisplayData]) {
            isCalledShowContactDetails = true
            contactDisplayData = details
        }
        
        func callToPhone(withPhone phone: String) {
            isCalledPressedCallToPhoneContact = true
        }
        
        func sendEmail(withEmail email: String) {
            isCalledPressedSendEmailContact = true
        }
        
        func showError(error: ModelBO) {
        }
        
        func changeColorEditTexts() {
            
        }
    }
}
