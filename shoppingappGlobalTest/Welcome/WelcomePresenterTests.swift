//
//  WelcomePresenterTests.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 25/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest
import CellsNativeComponents

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class WelcomePresenterTests: XCTestCase {

    var sut: WelcomePresenter<WelcomeViewProtocolDummy>!
    var dummyBusManager: DummyBusManager!
    var dummyPreferencesManager: DummyPreferencesManager!
    var dummyView: WelcomeViewProtocolDummy!

    override func setUp() {
        super.setUp()
        dummyBusManager = DummyBusManager()
        sut = WelcomePresenter<WelcomeViewProtocolDummy>(busManager: dummyBusManager)
        dummyView = WelcomeViewProtocolDummy()
        sut.view = dummyView
        
        dummyPreferencesManager = DummyPreferencesManager()
        PreferencesManager.instance = dummyPreferencesManager
    }
    
    // MARK: - viewDidLoad
    
    func test_givenMigrationKeyAndValueSaved_whenICallViewDidLoad_thenICallViewShowUpdate() {
        
        // given
        _ = dummyPreferencesManager.saveValue(forValue: "1.3" as AnyObject, withKey: sut.migrationKey)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowUpdate == true)
    }
    
    func test_givenMigrationKeyAndValueNotSaved_whenICallViewDidLoad_thenICallViewShowInit() {
        
        // given
        dummyPreferencesManager.key = sut.migrationKey
        dummyPreferencesManager.value = nil
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowFirstInit == true)
    }

    // MARK: - acceptPressed
    
    func test_givenAny_whenICallAcceptPressed_thenICalledSetRootWhenNoSession() {

        //given

        //when
        sut.acceptPressed()

        //then
        XCTAssert(dummyBusManager.isCalledSetRootWhenNoSession == true)
    }

    class DummyBusManager: BusManager {

        override init() {
            super.init()
            routerFactory = RouterInitFactory()
        }

        var isCalledSetRootWhenNoSession = false
        
        override func setRootWhenNoSession() {
            isCalledSetRootWhenNoSession = true
        }
    }

    class WelcomeViewProtocolDummy: WelcomeViewProtocol {
        
        var isCalledShowFirstInit = false
        var isCalledShowUpdate = false

        func showError(error: ModelBO) {
        }

        func changeColorEditTexts() {
        }
        
        func showFirstInit() {
            isCalledShowFirstInit = true
        }
        
        func showUpdate() {
            isCalledShowUpdate = true
        }
    }

}
