//
//  DataPoiTests.swift
//  shoppingapp
//
//  Created by jesus.martinez on 6/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class DataPoiTests: XCTestCase {

    // MARK: - Init

    func test_givenLatitudeLongitudAndStoreId_whenICallInit_thenLatitudeLongitudeMatchAndStoreIdsShouldContainOnlyTheStoreId() {

        // given
        let latitue = 2.0
        let longitude = 3.0
        let storeId = "3"

        // when
        let sut = DataPoi(latitude: latitue, longitude: longitude, storeId: storeId)

        // then
        XCTAssert(sut.latitude == latitue)
        XCTAssert(sut.longitude == longitude)
        XCTAssert(sut.storeIds.count == 1)
        XCTAssert(sut.storeIds[0] == storeId)

    }

    // MARK: - append storeId

    func test_givenAStoreIdsAndStoreIdNotContained_whenICallAppendStoreId_thenStoreIdsShouldContainsTheNewOne() {

        // given
        let sut = DataPoi(latitude: 2.0, longitude: 3.0, storeId: "1")
        let newStoreId = "2"

        // when
        sut.append(storeId: newStoreId)

        // then
        XCTAssert(sut.storeIds.count == 2)
        XCTAssert(sut.storeIds.contains(newStoreId))

    }

    func test_givenAStoreIdsAndStoreIdContained_whenICallAppendStoreId_thenStoreIdsShouldKeepEquals() {

        // given
        let sut = DataPoi(latitude: 2.0, longitude: 3.0, storeId: "1")
        sut.storeIds = ["1", "2", "3"]

        // when
        sut.append(storeId: "2")

        // then
        XCTAssert(sut.storeIds.count == 3)

    }

    // MARK: - append promotion

    func test_givenPromotionsAndPromotionNotContained_whenICallAppendPromotion_thenPromotionsShouldContainTheNewOne() {

        // given
        let sut = DataPoi(latitude: 2.0, longitude: 3.0, storeId: "1")

        let promotions = PromotionsGenerator.getPromotionsBO()

        sut.promotions = [promotions.promotions.first!]

        // when
        sut.append(promotion: promotions.promotions[1])

        // then
        XCTAssert(sut.promotions.count == 2)
        XCTAssert(sut.promotions.contains(where: { promotion -> Bool in
            promotion.id == promotions.promotions[1].id
        }))

    }

    func test_givenPromotionsAndPromotionContained_whenICallAppendPromotion_thenPromotionsShouldKeepEquals() {

        // given
        let sut = DataPoi(latitude: 2.0, longitude: 3.0, storeId: "1")
        let promotions = PromotionsGenerator.getPromotionsBO()

        sut.promotions = [promotions.promotions.first!, promotions.promotions[1]]

        // when
        sut.append(promotion: promotions.promotions[1])

        // then
        XCTAssert(sut.promotions.count == 2)

    }

    // MARK: - categoriesEquals

    func test_givenPromotionsWithtAtLeastOnePromotionWithCategoriesNil_whenICallCategoriesEquals_thenReturnNil() {

        // given
        let sut = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "1")
        let promo1 = PromotionsGenerator.getPromotionsBOWithStores().promotions[0]
        promo1.categories = nil

        let promo2 = PromotionsGenerator.getPromotionsBOWithStores().promotions[1]
        promo2.categories = [CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.dietAndNutrition.rawValue, name: "dietAndNutrition", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.foodAndGourmet.rawValue, name: "foodAndGourmet", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.ecommerce.rawValue, name: "ecommerce", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.toyStore.rawValue, name: "toyStore", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.auto.rawValue, name: "auto", isFavourite: nil))]

        let promo3 = PromotionsGenerator.getPromotionsBOWithStores().promotions[2]
        promo3.categories = [CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.auto.rawValue, name: "auto", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.bar.rawValue, name: "bar", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.ecommerce.rawValue, name: "ecommerce", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.toyStore.rawValue, name: "toyStore", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.bookStore.rawValue, name: "bookStore", isFavourite: nil))]

        let promo4 = PromotionsGenerator.getPromotionsBOWithStores().promotions[3]
        promo4.categories = [CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.bookStore.rawValue, name: "bookStore", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.bar.rawValue, name: "bar", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.ecommerce.rawValue, name: "ecommerce", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.boutique.rawValue, name: "boutique", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.bookStore.rawValue, name: "bookStore", isFavourite: nil))]

        sut.append(promotion: promo1)
        sut.append(promotion: promo2)
        sut.append(promotion: promo3)
        sut.append(promotion: promo4)

        // when
        let categoriesEquals = sut.categoriesEquals()

        // then
        XCTAssert(categoriesEquals == nil)

    }

    func test_givenPromotionsWithoutAnyCategoryEqualBetweenThen_whenICallCategoriesEquals_thenReturnEmptyArray() {

        // given
        let sut = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        let promo1 = PromotionsGenerator.getPromotionsBOWithStores().promotions[0]
        promo1.categories = [CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.activities.rawValue, name: "Actividades", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.auto.rawValue, name: "auto", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.beautyAndGragances.rawValue, name: "beautyAndGragances", isFavourite: nil))]

        let promo2 = PromotionsGenerator.getPromotionsBOWithStores().promotions[1]
        promo2.categories = [CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.dietAndNutrition.rawValue, name: "dietAndNutrition", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.foodAndGourmet.rawValue, name: "foodAndGourmet", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.hobbies.rawValue, name: "hobbies", isFavourite: nil))]

        let promo3 = PromotionsGenerator.getPromotionsBOWithStores().promotions[2]
        promo3.categories = [CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.auto.rawValue, name: "auto", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.bar.rawValue, name: "bar", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.bookStore.rawValue, name: "bookStore", isFavourite: nil))]

        sut.append(promotion: promo1)
        sut.append(promotion: promo2)
        sut.append(promotion: promo3)

        // when
        let categoriesEquals = sut.categoriesEquals()

        // then
        XCTAssert(categoriesEquals != nil)
        XCTAssert(categoriesEquals!.isEmpty == true)

    }

    func test_givenPromotionsWithtTwoCategoriesEqualBetweenThen_whenICallCategoriesEquals_thenReturnTwoCategoryType() {

        // given
        let sut = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        let promo1 = PromotionsGenerator.getPromotionsBOWithStores().promotions[0]
        promo1.categories = [CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.activities.rawValue, name: "Actividades", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.auto.rawValue, name: "auto", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.beautyAndGragances.rawValue, name: "beautyAndGragances", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.bookStore.rawValue, name: "bookStore", isFavourite: nil))]

        let promo2 = PromotionsGenerator.getPromotionsBOWithStores().promotions[1]
        promo2.categories = [CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.dietAndNutrition.rawValue, name: "dietAndNutrition", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.foodAndGourmet.rawValue, name: "foodAndGourmet", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.bookStore.rawValue, name: "bookStore", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.auto.rawValue, name: "auto", isFavourite: nil))]

        let promo3 = PromotionsGenerator.getPromotionsBOWithStores().promotions[2]
        promo3.categories = [CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.auto.rawValue, name: "auto", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.bookStore.rawValue, name: "bookStore", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.bar.rawValue, name: "bar", isFavourite: nil))]

        sut.append(promotion: promo1)
        sut.append(promotion: promo2)
        sut.append(promotion: promo3)

        // when
        let categoriesEquals = sut.categoriesEquals()

        // then
        XCTAssert(categoriesEquals != nil)
        XCTAssert(categoriesEquals!.count == 2)
        XCTAssert(categoriesEquals!.contains(.bookStore))
        XCTAssert(categoriesEquals!.contains(.auto))

    }

    // MARK: - hasStoresEqual

    func test_givenPromotionsWithAllStoresEquals_whenICallHasStoresEqual_thenReturnTrue() {

        var storeEntity1 = StoresEntity()
        storeEntity1.id = "1"
        var storeEntity2 = StoresEntity()
        storeEntity2.id = "2"
        var storeEntity3 = StoresEntity()
        storeEntity3.id = "3"
        var storeEntity4 = StoresEntity()
        storeEntity4.id = "4"

        // given
        let sut = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "1")
        let promo1 = PromotionsGenerator.getPromotionsBOWithStores().promotions[0]
        promo1.stores = [StoreBO(storeEntity: storeEntity1),
                         StoreBO(storeEntity: storeEntity4),
                         StoreBO(storeEntity: storeEntity2)]

        let promo2 = PromotionsGenerator.getPromotionsBOWithStores().promotions[1]
        promo2.stores = [StoreBO(storeEntity: storeEntity4),
                         StoreBO(storeEntity: storeEntity2),
                         StoreBO(storeEntity: storeEntity1)]

        let promo3 = PromotionsGenerator.getPromotionsBOWithStores().promotions[2]
        promo3.stores = [StoreBO(storeEntity: storeEntity1),
                         StoreBO(storeEntity: storeEntity2),
                         StoreBO(storeEntity: storeEntity4)]

        let promo4 = PromotionsGenerator.getPromotionsBOWithStores().promotions[3]
        promo4.stores = [StoreBO(storeEntity: storeEntity1),
                         StoreBO(storeEntity: storeEntity2),
                         StoreBO(storeEntity: storeEntity4)]

        sut.append(promotion: promo1)
        sut.append(promotion: promo2)
        sut.append(promotion: promo3)
        sut.append(promotion: promo4)

        // when
        let equals = sut.hasStoresEqual()

        // then
        XCTAssert(equals == true)

    }

    func test_givenPromotionsWithSomeStoreDifferent_whenICallHasStoresEqual_thenReturnFalse() {

        var storeEntity1 = StoresEntity()
        storeEntity1.id = "1"
        var storeEntity2 = StoresEntity()
        storeEntity2.id = "2"
        var storeEntity3 = StoresEntity()
        storeEntity3.id = "3"
        var storeEntity4 = StoresEntity()
        storeEntity4.id = "4"

        // given
        let sut = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "1")
        let promo1 = PromotionsGenerator.getPromotionsBOWithStores().promotions[0]
        promo1.stores = [StoreBO(storeEntity: storeEntity1),
                         StoreBO(storeEntity: storeEntity3),
                         StoreBO(storeEntity: storeEntity2)]

        let promo2 = PromotionsGenerator.getPromotionsBOWithStores().promotions[1]
        promo2.stores = [StoreBO(storeEntity: storeEntity4),
                         StoreBO(storeEntity: storeEntity2),
                         StoreBO(storeEntity: storeEntity1)]

        let promo3 = PromotionsGenerator.getPromotionsBOWithStores().promotions[2]
        promo3.stores = [StoreBO(storeEntity: storeEntity1),
                         StoreBO(storeEntity: storeEntity2),
                         StoreBO(storeEntity: storeEntity4)]

        let promo4 = PromotionsGenerator.getPromotionsBOWithStores().promotions[3]
        promo4.stores = [StoreBO(storeEntity: storeEntity1),
                         StoreBO(storeEntity: storeEntity2),
                         StoreBO(storeEntity: storeEntity4)]

        sut.append(promotion: promo1)
        sut.append(promotion: promo2)
        sut.append(promotion: promo3)
        sut.append(promotion: promo4)

        // when
        let equals = sut.hasStoresEqual()

        // then
        XCTAssert(equals == false)

    }
}
