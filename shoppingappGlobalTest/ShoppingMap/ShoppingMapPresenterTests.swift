//
//  ShoppingMapPresenterTests.swift
//  shoppingapp
//
//  Created by jesus.martinez on 9/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest
import RxSwift
import CellsNativeComponents
import CoreLocation

#if TESTMX
    @testable import shoppingappMX

#endif

class ShoppingMapPresenterTests: XCTestCase {

    var sut: ShoppingMapPresenter<DummyView>!
    var dummyBusManager: DummyBusManager!
    var dummyLocationManager = DummyLocationManager()
    var dummySessionManager: DummySessionManager!

    override func setUp() {
        super.setUp()
        dummyBusManager = DummyBusManager()
        sut = ShoppingMapPresenter<DummyView>(busManager: dummyBusManager)

        dummyLocationManager = DummyLocationManager()
        sut.locationManager = dummyLocationManager
        
        DummySessionManager.setupMock()
        dummySessionManager = (SessionDataManager.sessionDataInstance() as! DummySessionManager)
    }

    // MARK: - ViewDidLoad

    func test_givenNoCategoryBONeitherTrendingBO_whenICallViewDidLoad_thenICallShowFilterButton() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.categoryBO = nil
        sut.trendingType = nil

        // when
        sut.viewDidLoad()

        // then
        XCTAssert(dummyView.isCalledShowFilterButton == true)

    }

    func test_givenNoCategoryBO_whenICallViewDidLoad_thenIDoNotCallShowFilterButton() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let idType = CategoryEntity(id: "ACTIVITIES", name: "actividades", isFavourite: false)
        sut.categoryBO = CategoryBO(categoryEntity: idType)
        sut.trendingType = nil

        // when
        sut.viewDidLoad()

        // then
        XCTAssert(dummyView.isCalledShowFilterButton == false)

    }

    func test_givenTrendingBO_whenICallViewDidLoad_thenIDoNotCallShowFilterButton() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.categoryBO = nil
        sut.trendingType = TrendingType(rawValue: "HOT")

        // when
        sut.viewDidLoad()

        // then
        XCTAssert(dummyView.isCalledShowFilterButton == false)

    }

    // MARK: - viewWillAppear

    func test_givenGPSDisabledAndErrorBOWithErrorInfoTypeAndLastLocationRetrieveNil_whenICallViewWillAppear_thenErrorBOShouldBeNil() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        sut.errorBO = ErrorBO(message: Localizables.promotions.key_enable_gps_advise_text, errorType: .info, allowCancel: true)
        sut.isGPSDisabled = true
        sut.lastLocationRetrieve = nil

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(sut.errorBO == nil)
    }

    func test_givenGPSDisabledAndLastLocationRetrieveNil_whenICallViewWillAppear_thenICallViewShowToastWithInfoText() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        sut.isGPSDisabled = true
        sut.lastLocationRetrieve = nil

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowToastWithInfoText == true)
    }

    func test_givenGPSDisabledAndLastLocationRetrieveNil_whenICallViewWillAppear_thenICallViewShowToastWithInfoTextWithCorrectText() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        sut.isGPSDisabled = true
        sut.lastLocationRetrieve = nil

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.infoText == Localizables.promotions.key_enable_gps_advise_text)
    }

    func test_givenGPSDisabledAndLastLocationRetrieveNil_whenICallViewWillAppear_thenICallViewShowCenterMap() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        sut.isGPSDisabled = true
        sut.lastLocationRetrieve = nil

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledCenterMap == true)
    }

    func test_givenGPSDisabledAndLastLocationRetrieveNil_whenICallViewWillAppear_thenICallViewShowCenterMapWithDefaultLatitudeAndLongitude() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        sut.isGPSDisabled = true
        sut.lastLocationRetrieve = nil

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.latitude == sut.defaultLocation.latitude)
        XCTAssert(dummyView.longitude == sut.defaultLocation.longitude)
        XCTAssert(dummyView.radiousInMetersSent == Double(sut.defaultDistance * DistanceConstants.meters_to_kilometers))

    }

    func test_givenGPSDisabledAndLastLocationRetrieveNil_whenICallViewWillAppear_thenICallViewHideUserLocation() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        sut.isGPSDisabled = true
        sut.lastLocationRetrieve = nil

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledHideUserLocation == true)
    }

    func test_givenNotCategoryBONeitherTrendingAndGPSDisabledAndLastLocationRetrieveNil_whenICallViewWillAppear_thenICallInteractorProvidePromotions() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        sut.isGPSDisabled = true
        sut.lastLocationRetrieve = nil

        // given
        sut.categoryBO = nil
        sut.trendingType = nil

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotions == true)
    }

    func test_givenNotCategoryBONeitherTrendingAndGPSDisabledAndLastLocationRetrieveNil_whenICallViewWillAppear_thenLastLocationRetrieveShouldBeFilled() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        dummyInteractor.presenter = sut
        sut.isGPSDisabled = true
        sut.lastLocationRetrieve = nil

        // given
        sut.categoryBO = nil
        sut.trendingType = nil

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(sut.lastLocationRetrieve != nil)
    }

    func test_givenNotCategoryBONeitherTrendingAndGPSDisabledAndLastLocationRetrieveNil_whenICallViewWillAppear_thenLastLocationRetrieveShouldHaveLongitudeAndLatitudeDefault() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        dummyInteractor.presenter = sut
        sut.isGPSDisabled = true
        sut.lastLocationRetrieve = nil

        // given
        sut.categoryBO = nil
        sut.trendingType = nil

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(sut.lastLocationRetrieve?.latitude == sut.defaultLocation.latitude)
        XCTAssert(sut.lastLocationRetrieve?.longitude == sut.defaultLocation.longitude)
    }

    func test_givenNotCategoryBONeitherTrendingAndGPSDisabledAndLastLocationRetrieveNil_whenICallViewWillAppear_thenICallShowLoader() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        sut.isGPSDisabled = true
        sut.lastLocationRetrieve = nil

        // given
        sut.categoryBO = nil
        sut.trendingType = nil

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowLoader == true)
    }

    func test_givenNotCategoryBONeitherTrendingAndGPSDisabledAndLastLocationRetrieveNil_whenICallViewWillAppear_thenICallInteractorProvidePromotionsWithDefaultValues() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.categoryBO = nil
        sut.trendingType = nil
        sut.isGPSDisabled = true
        sut.lastLocationRetrieve = nil

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyInteractor.latitudeSent == sut.defaultLocation.latitude)
        XCTAssert(dummyInteractor.longitudeSent == sut.defaultLocation.longitude)
        XCTAssert(dummyInteractor.distanceSent == sut.defaultDistance)
        XCTAssert(dummyInteractor.lengthTypeSent == sut.lengthType)
        XCTAssert(dummyInteractor.pageSizeSent == sut.pageSize)
        XCTAssert(dummyInteractor.categoryIdSent == nil)
        XCTAssert(dummyInteractor.trendingIdSent == nil)

    }

    func test_givenWithCategoryBOAndGPSDisabledAndLastLocationRetrieveNil_whenICallViewWillAppear_thenICallInteractorProvidePromotionsWithDefaultValuesAndCategoryId() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let idType = CategoryEntity(id: "ACTIVITIES", name: "actividades", isFavourite: false)
        sut.categoryBO = CategoryBO(categoryEntity: idType)
        sut.trendingType = nil
        sut.isGPSDisabled = true
        sut.lastLocationRetrieve = nil

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyInteractor.latitudeSent == sut.defaultLocation.latitude)
        XCTAssert(dummyInteractor.longitudeSent == sut.defaultLocation.longitude)
        XCTAssert(dummyInteractor.distanceSent == sut.defaultDistance)
        XCTAssert(dummyInteractor.lengthTypeSent == sut.lengthType)
        XCTAssert(dummyInteractor.pageSizeSent == sut.pageSize)
        XCTAssert(dummyInteractor.categoryIdSent == sut.categoryBO?.id.rawValue)
        XCTAssert(dummyInteractor.trendingIdSent == nil)

    }

    func test_givenWithTrendingTypeAndGPSDisabledAndLastLocationRetrieveNil_whenICallViewWillAppear_thenICallInteractorProvidePromotionsWithDefaultValuesAndTrendingId() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.categoryBO = nil
        sut.trendingType = TrendingType(rawValue: "HOT")
        sut.isGPSDisabled = true
        sut.lastLocationRetrieve = nil

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyInteractor.latitudeSent == sut.defaultLocation.latitude)
        XCTAssert(dummyInteractor.longitudeSent == sut.defaultLocation.longitude)
        XCTAssert(dummyInteractor.distanceSent == sut.defaultDistance)
        XCTAssert(dummyInteractor.lengthTypeSent == sut.lengthType)
        XCTAssert(dummyInteractor.pageSizeSent == sut.pageSize)
        XCTAssert(dummyInteractor.categoryIdSent == nil)
        XCTAssert(dummyInteractor.trendingIdSent == sut.trendingType?.rawValue)

    }

    func test_givenNotCategoryBONeitherTrendingAndGPSDisabledAndLastLocationRetrieveNil_whenICallViewWillAppearAndInteractorReturnError_thenICallHideLoader() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let errorEntity = ErrorEntity()
        errorEntity.code = "500"
        errorEntity.message = "Error 500"

        dummyInteractor.forceError = true
        dummyInteractor.errorBO = ErrorBO(error: errorEntity)

        // given
        sut.categoryBO = nil
        sut.trendingType = nil
        sut.isGPSDisabled = true
        sut.lastLocationRetrieve = nil

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledHideLoader == true)
    }

    func test_givenNotCategoryBONeitherTrendingAndGPSDisabledAndLastLocationRetrieveNil_whenICallViewWillAppearAndInteractorReturnError_thenICallHidePromotions() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let errorEntity = ErrorEntity()
        errorEntity.code = "500"
        errorEntity.message = "Error 500"

        dummyInteractor.forceError = true
        dummyInteractor.errorBO = ErrorBO(error: errorEntity)

        // given
        sut.categoryBO = nil
        sut.trendingType = nil
        sut.isGPSDisabled = true
        sut.lastLocationRetrieve = nil

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledHidePromotions == true)
    }

    func test_givenNotCategoryBONeitherTrendingAndGPSDisabledAndLastLocationRetrieveNil_whenICallViewWillAppearAndInteractorReturnError_thenErrorBOShouldBeFilled() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let errorEntity = ErrorEntity()
        errorEntity.code = "500"
        errorEntity.message = "Error 500"

        dummyInteractor.forceError = true
        dummyInteractor.errorBO = ErrorBO(error: errorEntity)

        // given
        sut.categoryBO = nil
        sut.trendingType = nil
        sut.isGPSDisabled = true
        sut.lastLocationRetrieve = nil

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(sut.errorBO != nil)
        XCTAssert(sut.errorBO! === dummyInteractor.errorBO!)
    }

    func test_givenNotCategoryBONeitherTrendingAndGPSDisabledAndLastLocationRetrieveNil_whenICallViewWillAppearAndInteractorReturnError_thenICallViewShowError() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let errorEntity = ErrorEntity()
        errorEntity.code = "500"
        errorEntity.message = "Error 500"

        dummyInteractor.forceError = true
        dummyInteractor.errorBO = ErrorBO(error: errorEntity)

        // given
        sut.categoryBO = nil
        sut.trendingType = nil
        sut.isGPSDisabled = true
        sut.lastLocationRetrieve = nil

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowError == true)
    }

    func test_givenNotCategoryBONeitherTrendingAndGPSDisabledAndLastLocationRetrieveNil_whenICallViewWillAppearAndInteractorReturnSuccess_thenICallHideLoader() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.categoryBO = nil
        sut.trendingType = nil
        sut.isGPSDisabled = true
        sut.lastLocationRetrieve = nil

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledHideLoader == true)
    }

    func test_givenNotCategoryBONeitherTrendingAndGPSDisabledAndLastLocationRetrieveNil_whenICallViewWillAppearAndInteractorReturnSuccess_thenDataPoisShouldBeFilled() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.categoryBO = nil
        sut.trendingType = nil
        sut.isGPSDisabled = true
        sut.lastLocationRetrieve = nil

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(sut.dataPois.isEmpty == false)
    }

    func test_givenNotCategoryBONeitherTrendingAndGPSDisabledAndLastLocationRetrieveNil_whenICallViewWillAppearAndInteractorReturnSuccess_thenICallShowStores() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.categoryBO = nil
        sut.trendingType = nil
        sut.isGPSDisabled = true
        sut.lastLocationRetrieve = nil

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowStores == true)
    }

    func test_givenNotCategoryBONeitherTrendingAndGPSDisabledAndLastLocationRetrieveNil_whenICallViewWillAppearAndInteractorReturnSuccess_thenICallHidePromotions() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.categoryBO = nil
        sut.trendingType = nil
        sut.isGPSDisabled = true
        sut.lastLocationRetrieve = nil

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledHidePromotions == true)
    }

    func test_givenNotCategoryBONeitherTrendingAndGPSDisabledAndLastLocationRetrieveNil_whenICallViewWillAppearAndInteractorReturnSuccess_thenICallShowStoresWithTheSameNumberOfItemsAsStoresReturned() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        let promotions = PromotionsGenerator.getPromotionsBO()

        for promotion in promotions.promotions {
            promotion.promotionType.id = .discount
        }

        dummyInteractor.promotionsBO = promotions

        // given
        sut.categoryBO = nil
        sut.trendingType = nil
        sut.isGPSDisabled = true
        sut.lastLocationRetrieve = nil

        // when
        sut.viewWillAppear()

        // then

        var numberOfStoresReturned = 0

        for promotion in dummyInteractor.promotionsBO!.promotions {
            numberOfStoresReturned += (promotion.stores?.count ?? 0)
        }

        XCTAssert(dummyView.displayDataSent!.items.count == numberOfStoresReturned)
    }
    
    func test_givenADisplayDataMapNotCategoryBONeitherTrendingAndGPSDisabledAndLastLocationRetrieveNil_whenICallViewWillAppearAndInteractorReturnSuccess_thenICallShowStoresWithTheSameNumberOfItemsAsStoresReturned() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        let promotions = PromotionsGenerator.getPromotionsBO()

        for promotion in promotions.promotions {
            promotion.promotionType.id = .discount
        }

        dummyInteractor.promotionsBO = promotions

        let dataPoi = DataPoi(latitude: 3, longitude: 4, storeId: promotions.promotions[0].stores![0].id)
        dataPoi.append(promotion: promotions.promotions[0])

        let displayDataMap = MapDisplayData()
        displayDataMap.items.append(MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)!)

        // given
        sut.categoryBO = nil
        sut.trendingType = nil
        sut.isGPSDisabled = true
        sut.displayData = displayDataMap

        // when
        sut.viewWillAppear()

        // then

        var numberOfStoresReturned = 0

        for promotion in dummyInteractor.promotionsBO!.promotions {
            numberOfStoresReturned += (promotion.stores?.count ?? 0)
        }

        XCTAssert(dummyView.displayDataSent!.items.count == numberOfStoresReturned)
    }

    func test_givenWithCategoryBOAndGPSDisabledAndLastLocationRetrieveNil_whenICallViewWillAppearAndInteractorReturnSuccess_thenICallShowStores() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let idType = CategoryEntity(id: "ACTIVITIES", name: "actividades", isFavourite: false)
        sut.categoryBO = CategoryBO(categoryEntity: idType)
        sut.trendingType = nil
        sut.isGPSDisabled = true
        sut.lastLocationRetrieve = nil

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowStores == true)
    }

    func test_givenWithCategoryBOAndGPSDisabledAndLastLocationRetrieveNil_whenICallViewWillAppearAndInteractorReturnSuccess_thenICallHidePromotions() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let idType = CategoryEntity(id: "ACTIVITIES", name: "actividades", isFavourite: false)
        sut.categoryBO = CategoryBO(categoryEntity: idType)
        sut.trendingType = nil
        sut.isGPSDisabled = true
        sut.lastLocationRetrieve = nil

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledHidePromotions == true)
    }

    func test_givenWithCategoryBOAndGPSDisabledAndLastLocationRetrieveNil_whenICallViewWillAppearAndInteractorReturnSuccess_thenICallShowStoresWithTheSameNumberOfItemsAsStoresReturned() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        let promotions = PromotionsGenerator.getPromotionsBO()

        for promotion in promotions.promotions {
            promotion.promotionType.id = .discount
        }

        dummyInteractor.promotionsBO = promotions

        // given
        let idType = CategoryEntity(id: "ACTIVITIES", name: "actividades", isFavourite: false)
        sut.categoryBO = CategoryBO(categoryEntity: idType)
        sut.trendingType = nil
        sut.isGPSDisabled = true
        sut.lastLocationRetrieve = nil

        // when
        sut.viewWillAppear()

        // then

        var numberOfStoresReturned = 0

        for promotion in dummyInteractor.promotionsBO!.promotions {
            numberOfStoresReturned += (promotion.stores?.count ?? 0)
        }

        XCTAssert(dummyView.displayDataSent!.items.count == numberOfStoresReturned)
    }

    func test_givenADisplayDataMapWithCategoryBOAndGPSDisabledAndLastLocationRetrieveNil_whenICallViewWillAppearAndInteractorReturnSuccess_thenICallShowStoresWithTheSameNumberOfItemsAsStoresReturned() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        let promotions = PromotionsGenerator.getPromotionsBO()

        for promotion in promotions.promotions {
            promotion.promotionType.id = .discount
        }

        dummyInteractor.promotionsBO = promotions

        let dataPoi = DataPoi(latitude: 3, longitude: 4, storeId: promotions.promotions[0].stores![0].id)
        dataPoi.append(promotion: promotions.promotions[0])

        let displayDataMap = MapDisplayData()
        displayDataMap.items.append(MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)!)

        // given
        let idType = CategoryEntity(id: "ACTIVITIES", name: "actividades", isFavourite: false)
        sut.categoryBO = CategoryBO(categoryEntity: idType)
        sut.trendingType = nil
        sut.isGPSDisabled = true
        sut.displayData = displayDataMap
        sut.lastLocationRetrieve = nil

        // when
        sut.viewWillAppear()

        // then

        var numberOfStoresReturned = 0

        for promotion in dummyInteractor.promotionsBO!.promotions {
            numberOfStoresReturned += (promotion.stores?.count ?? 0)
        }

        XCTAssert(dummyView.displayDataSent!.items.count == numberOfStoresReturned)
    }

    func test_givenWithTrendingTypeAndGPSDisabledAndLastLocationRetrieveNil_whenICallViewWillAppearAndInteractorReturnSuccess_thenICallShowStores() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.categoryBO = nil
        sut.trendingType = TrendingType(rawValue: "HOT")
        sut.isGPSDisabled = true
        sut.lastLocationRetrieve = nil

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowStores == true)
    }

    func test_givenWithTrendingTypeAndGPSDisabledAndLastLocationRetrieveNil_whenICallViewWillAppearAndInteractorReturnSuccess_thenICallHidePromotions() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.categoryBO = nil
        sut.trendingType = TrendingType(rawValue: "HOT")
        sut.isGPSDisabled = true
        sut.lastLocationRetrieve = nil

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledHidePromotions == true)
    }

    func test_givenWithTrendingTypeAndGPSDisabledAndLastLocationRetrieveNil_whenICallViewWillAppearAndInteractorReturnSuccess_thenICallShowStoresWithTheSameNumberOfItemsAsStoresReturned() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        let promotions = PromotionsGenerator.getPromotionsBO()

        for promotion in promotions.promotions {
            promotion.promotionType.id = .discount
        }

        dummyInteractor.promotionsBO = promotions

        // given
        sut.categoryBO = nil
        sut.trendingType = TrendingType(rawValue: "HOT")
        sut.isGPSDisabled = true
        sut.lastLocationRetrieve = nil

        // when
        sut.viewWillAppear()

        // then

        var numberOfStoresReturned = 0

        for promotion in dummyInteractor.promotionsBO!.promotions {
            numberOfStoresReturned += (promotion.stores?.count ?? 0)
        }

        XCTAssert(dummyView.displayDataSent!.items.count == numberOfStoresReturned)
    }

    func test_givenADisplayDataMapWithTrendingTypeAndGPSDisabledAndLastLocationRetrieveNil_whenICallViewWillAppearAndInteractorReturnSuccess_thenICallShowStoresWithTheSameNumberOfItemsAsStoresReturned() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        let promotions = PromotionsGenerator.getPromotionsBO()

        for promotion in promotions.promotions {
            promotion.promotionType.id = .discount
        }

        dummyInteractor.promotionsBO = promotions

        let dataPoi = DataPoi(latitude: 3, longitude: 4, storeId: promotions.promotions[0].stores![0].id)
        dataPoi.append(promotion: promotions.promotions[0])

        let displayDataMap = MapDisplayData()
        displayDataMap.items.append(MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)!)

        // given
        sut.categoryBO = nil
        sut.trendingType = TrendingType(rawValue: "HOT")
        sut.isGPSDisabled = true
        sut.displayData = displayDataMap
        sut.lastLocationRetrieve = nil

        // when
        sut.viewWillAppear()

        // then

        var numberOfStoresReturned = 0

        for promotion in dummyInteractor.promotionsBO!.promotions {
            numberOfStoresReturned += (promotion.stores?.count ?? 0)
        }

        XCTAssert(dummyView.displayDataSent!.items.count == numberOfStoresReturned)
    }

    func test_givenGPSDisabledAndLastLocationRetrieveNil_whenICallViewWillAppearAndInteractorReturnPromotionsWithStores_thenIDoNotCallShowToast() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        sut.isGPSDisabled = true
        sut.lastLocationRetrieve = nil

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowToastNotStore == false)

    }

    func test_givenCategoryBOAndGPSDisabledAndLastLocationRetrieveNil_whenICallViewWillAppearAndInteractorReturnPromotionsWithStores_thenIDoNotCallShowToast() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let idType = CategoryEntity(id: "ACTIVITIES", name: "actividades", isFavourite: false)
        sut.categoryBO = CategoryBO(categoryEntity: idType)
        sut.isGPSDisabled = true
        sut.lastLocationRetrieve = nil

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowToastNotStore == false)

    }

    func test_givenTrendingBOAndGPSDisabledAndLastLocationRetrieveNil_whenICallViewWillAppearAndInteractorReturnPromotionsWithStores_thenIDoNotCallShowToast() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.trendingType = TrendingType(rawValue: "HOT")

        sut.isGPSDisabled = true
        sut.lastLocationRetrieve = nil

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowToastNotStore == false)

    }

    func test_givenGPSDisabledAndLastLocationRetrieveNil_whenICallViewWillAppearAndInteractorReturnPromotionsWithoutStores_thenICallShowToast() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let promotions = PromotionsGenerator.getPromotionsBO()

        for promotion in promotions.promotions {

            promotion.stores = nil

        }

        dummyInteractor.promotionsBO = promotions

        sut.isGPSDisabled = true
        sut.lastLocationRetrieve = nil

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowToastNotStore == true)

    }

    func test_givenCategoryBOAndGPSDisabledAndLastLocationRetrieveNil_whenICallViewWillAppearAndInteractorReturnPromotionsWithoutStores_thenICallShowToast() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let idType = CategoryEntity(id: "ACTIVITIES", name: "actividades", isFavourite: false)
        sut.categoryBO = CategoryBO(categoryEntity: idType)

        let promotions = PromotionsGenerator.getPromotionsBO()

        for promotion in promotions.promotions {

            promotion.stores = nil

        }

        dummyInteractor.promotionsBO = promotions

        sut.isGPSDisabled = true
        sut.lastLocationRetrieve = nil

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowToastNotStore == true)

    }

    func test_givenTrendingBOAndGPSDisabledAndLastLocationRetrieveNil_whenICallViewWillAppearAndInteractorReturnPromotionsWithoutStores_thenICallShowToast() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.trendingType = TrendingType(rawValue: "HOT")

        let promotions = PromotionsGenerator.getPromotionsBO()

        for promotion in promotions.promotions {

            promotion.stores = nil

        }

        dummyInteractor.promotionsBO = promotions

        sut.isGPSDisabled = true
        sut.lastLocationRetrieve = nil

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowToastNotStore == true)

    }

    // MARK: - locationPermissionRestricted

    func test_givenAny_whenICallViewWillAppearAndPermissionRestricted_thenICallViewShowError() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        dummyLocationManager.locationErrorType = .permissionDenied

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowError == true)

    }

    func test_givenErrorBO_whenICallViewWillAppearAndPermissionRestricted_thenIDoNotCallViewShowError() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        sut.errorBO = ErrorBO(message: "message", errorType: .info, allowCancel: true)
        dummyLocationManager.locationErrorType = .permissionDenied

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowError == false)

    }

    func test_givenAny_whenICallViewWillAppearAndPermissionRestricted_thenErrorBOShouldBeFilled() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        dummyLocationManager.locationErrorType = .permissionDenied

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(sut.errorBO != nil)
    }

    func test_givenAny_whenICallViewWillAppearAndPermissionRestricted_thenErrorBOShouldHaveCorrectValues() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        dummyLocationManager.locationErrorType = .permissionDenied

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(sut.errorBO!.errorMessage() == nil)
        XCTAssert(sut.errorBO!.messageAttributed != nil)
        XCTAssert(sut.errorBO!.errorType == .info)
        XCTAssert(sut.errorBO!.allowCancel == true)
        XCTAssert(sut.errorBO!.okButtonTitle == Localizables.common.key_common_accept_text)

    }

    func test_givenAny_whenICallViewWillAppearAndPermissionRestricted_thenICallViewShowErrorWithCorrectValues() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        dummyLocationManager.locationErrorType = .permissionDenied

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.errorBO!.errorMessage() == nil)
        XCTAssert(dummyView.errorBO!.messageAttributed != nil)
        XCTAssert(dummyView.errorBO!.errorType == .info)
        XCTAssert(dummyView.errorBO!.allowCancel == true)
        XCTAssert(dummyView.errorBO!.okButtonTitle == Localizables.common.key_common_accept_text)

    }

    func test_givenAny_whenICallViewWillAppearAndPermissionRestricted_thenIsGPSEnabled() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        dummyLocationManager.locationErrorType = .permissionDenied

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(sut.isGPSDisabled == false)

    }

    func test_givenAny_whenICallViewWillAppearAndPermissionRestricted_thenUserLocationIsNil() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        dummyLocationManager.locationErrorType = .permissionDenied

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(sut.userLocation == nil)

    }

    // MARK: - locationPermissionGPSDisabled

    func test_givenAny_whenICallViewWillAppearAndGPSDisabled_thenICallViewShowError() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        dummyLocationManager.locationErrorType = .gpsOff

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowError == true)

    }

    func test_givenErrorBO_whenICallViewWillAppearAndGPSDisabled_thenIDoNotCallViewShowError() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        sut.errorBO = ErrorBO(message: "message", errorType: .info, allowCancel: true)
        dummyLocationManager.locationErrorType = .gpsOff

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowError == false)

    }

    func test_givenAny_whenICallViewWillAppearAndGPSDisabled_thenErrorBOShouldBeFilled() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        dummyLocationManager.locationErrorType = .gpsOff

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(sut.errorBO != nil)
    }

    func test_givenAny_whenICallViewWillAppearAndGPSDisabled_thenErrorBOShouldHaveCorrectValues() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        dummyLocationManager.locationErrorType = .gpsOff

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(sut.errorBO!.errorMessage() == nil)
        XCTAssert(sut.errorBO!.messageAttributed != nil)
        XCTAssert(sut.errorBO!.errorType == .info)
        XCTAssert(sut.errorBO!.allowCancel == true)
        XCTAssert(sut.errorBO!.okButtonTitle == Localizables.common.key_common_accept_text)

    }

    func test_givenAny_whenICallViewWillAppearAndGPSDisabled_thenICallViewShowErrorWithCorrectValues() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        dummyLocationManager.locationErrorType = .gpsOff

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.errorBO!.errorMessage() == nil)
        XCTAssert(dummyView.errorBO!.messageAttributed != nil)
        XCTAssert(dummyView.errorBO!.errorType == .info)
        XCTAssert(dummyView.errorBO!.allowCancel == true)
        XCTAssert(dummyView.errorBO!.okButtonTitle == Localizables.common.key_common_accept_text)

    }

    func test_givenAny_whenICallViewWillAppearAndGPSDisabled_thenIsGPSDisabled() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        dummyLocationManager.locationErrorType = .gpsOff

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(sut.isGPSDisabled == true)
    }

    func test_givenAny_whenICallViewWillAppearAndGPSDisabled_thenUserLocationIsNil() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        dummyLocationManager.locationErrorType = .gpsOff

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(sut.userLocation == nil)

    }

    // MARK: - errorAcceptPressed

    func test_givenErrorBOWithErrorInfoTypeAndGPSEnabled_whenICallErrorAcceptPressed_thenICallViewGoToLocationSettings() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        sut.errorBO = ErrorBO(message: Localizables.promotions.key_enable_gps_advise_text, errorType: .info, allowCancel: true)
        sut.isGPSDisabled = false

        // when
        sut.errorAcceptPressed()

        // then
        XCTAssert(dummyView.isCalledGoToLocationSettings == true)
    }

    func test_givenErrorBOWithErrorInfoTypeAndGPSDisabled_whenICallErrorAcceptPressed_thenIDoNotCallViewGoToLocationSettings() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        sut.errorBO = ErrorBO(message: Localizables.promotions.key_enable_gps_advise_text, errorType: .info, allowCancel: true)
        sut.isGPSDisabled = true

        // when
        sut.errorAcceptPressed()

        // then
        XCTAssert(dummyView.isCalledGoToLocationSettings == false)
    }

    func test_givenErrorBOWithErrorInfoTypeAndGPSEnabled_whenICallErrorAcceptPressed_thenIDoNotCallRouterPresentGPSHelp() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        sut.errorBO = ErrorBO(message: Localizables.promotions.key_enable_gps_advise_text, errorType: .info, allowCancel: true)
        sut.isGPSDisabled = false

        // when
        sut.errorAcceptPressed()

        // then
        XCTAssert(dummyBusManager.isCalledNavigateNextShoppingMapScreen == false)
    }

    func test_givenErrorBOWithErrorInfoTypeAndGPSDisabled_whenICallErrorAcceptPressed_thenICallRouterPresentGPSHelp() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        sut.errorBO = ErrorBO(message: Localizables.promotions.key_enable_gps_advise_text, errorType: .info, allowCancel: true)
        sut.isGPSDisabled = true

        // when
        sut.errorAcceptPressed()

        // then
        XCTAssert(dummyBusManager.isCalledNavigateNextShoppingMapScreen == true)
    }

    func test_givenErrorBOWithErrorInfoType_whenICallErrorAcceptPressed_thenErrorBOShouldBeNil() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        sut.errorBO = ErrorBO(message: Localizables.promotions.key_enable_gps_advise_text, errorType: .info, allowCancel: true)

        // when
        sut.errorAcceptPressed()

        // then
        XCTAssert(sut.errorBO == nil)
    }

    func test_givenErrorBOWithErrorNotInfoType_whenICallErrorAcceptPressed_thenIDoNotCallViewGoToLocationSettings() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        sut.errorBO = ErrorBO(message: Localizables.promotions.key_enable_gps_advise_text, errorType: .warning, allowCancel: true)

        // when
        sut.errorAcceptPressed()

        // then
        XCTAssert(dummyView.isCalledGoToLocationSettings == false)
    }

    func test_givenErroBOWith403_whenICallErrorAcceptPressed_thenCallCloseSessionOfSessionManager() {

        // given
        let errorBO = ErrorBO(error: ErrorEntity(message: "Sesión expirada"))
        errorBO.isErrorShown = true
        errorBO.status = ConstantsHTTPCodes.STATUS_403
        errorBO.code = ErrorCode.expiredSession.rawValue
        sut.errorBO = errorBO

        // when
        sut.errorAcceptPressed()

        // then
        XCTAssert(dummySessionManager.isCalledCloseSession == true)
        XCTAssert(dummyBusManager.isCalledExpiredSession == true)

    }
    
    func test_givenErrorBOInfoAndIsGPSDisabledFalse_whenICallErrorAcceptPressed_thenICallViewShowCenterMap() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.errorBO = ErrorBO(message: Localizables.promotions.key_enable_gps_advise_text, errorType: .info, allowCancel: true)
        sut.isGPSDisabled = false
        
        // when
        sut.errorAcceptPressed()
        
        // then
        XCTAssert(dummyView.isCalledCenterMap == true)
    }
    
    func test_givenErrorBOInfoAndIsGPSDisabledFalse_whenICallErrorAcceptPressed_thenICallViewShowCenterMapWithDefaultLatitudeAndLongitude() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.errorBO = ErrorBO(message: Localizables.promotions.key_enable_gps_advise_text, errorType: .info, allowCancel: true)
        sut.isGPSDisabled = false
        // when
        sut.errorAcceptPressed()
        
        // then
        XCTAssert(dummyView.latitude == sut.defaultLocation.latitude)
        XCTAssert(dummyView.longitude == sut.defaultLocation.longitude)
        XCTAssert(dummyView.radiousInMetersSent == Double(sut.defaultDistance * DistanceConstants.meters_to_kilometers))
        
    }
    
    func test_givenErrorBOInfoAndIsGPSDisabledFalse_whenICallErrorAcceptPressed_thenICallViewHideUserLocation() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.errorBO = ErrorBO(message: Localizables.promotions.key_enable_gps_advise_text, errorType: .info, allowCancel: true)
        sut.isGPSDisabled = false
        
        // when
        sut.errorAcceptPressed()
        
        // then
        XCTAssert(dummyView.isCalledHideUserLocation == true)
    }
    
    func test_givenErrorBOInfoAndIsGPSDisabledFalse_whenICallErrorAcceptPressed_thenICallInteractorProvidePromotions() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        sut.errorBO = ErrorBO(message: Localizables.promotions.key_enable_gps_advise_text, errorType: .info, allowCancel: true)
        sut.isGPSDisabled = false
        
        // when
        sut.dismissedGPSHelpScreen()
        
        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotions == true)
    }
    
    func test_givenErrorBOInfoAndIsGPSDisabledFalse_whenICallErrorAcceptPressed_thenLastLocationRetrieveShouldBeFilled() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        dummyInteractor.presenter = sut
        sut.lastLocationRetrieve = nil
        
        // given
        sut.errorBO = ErrorBO(message: Localizables.promotions.key_enable_gps_advise_text, errorType: .info, allowCancel: true)
        sut.isGPSDisabled = false
        
        // when
        sut.errorAcceptPressed()
        
        // then
        XCTAssert(sut.lastLocationRetrieve != nil)
    }
    
    func test_givenErrorBOInfoAndIsGPSDisabledFalse_whenICallErrorAcceptPressed_thenLastLocationRetrieveShouldHaveLongitudeAndLatitudeDefault() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        dummyInteractor.presenter = sut
        
        // given
        sut.errorBO = ErrorBO(message: Localizables.promotions.key_enable_gps_advise_text, errorType: .info, allowCancel: true)
        sut.isGPSDisabled = false
        
        // when
        sut.errorAcceptPressed()
        
        // then
        XCTAssert(sut.lastLocationRetrieve?.latitude == sut.defaultLocation.latitude)
        XCTAssert(sut.lastLocationRetrieve?.longitude == sut.defaultLocation.longitude)
    }
    
    func test_givenErrorBOInfoAndIsGPSDisabledFalse_whenICallErrorAcceptPressed_thenICallShowLoader() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        sut.errorBO = ErrorBO(message: Localizables.promotions.key_enable_gps_advise_text, errorType: .info, allowCancel: true)
        sut.isGPSDisabled = false
        
        // when
        sut.errorAcceptPressed()
        
        // then
        XCTAssert(dummyView.isCalledShowLoader == true)
    }
    
    func test_givenErrorBOInfoAndIsGPSDisabledFalse_whenICallErrorAcceptPressed_thenICallInteractorProvidePromotionsWithDefaultValues() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        sut.errorBO = ErrorBO(message: Localizables.promotions.key_enable_gps_advise_text, errorType: .info, allowCancel: true)
        sut.isGPSDisabled = false
        
        // when
        sut.errorAcceptPressed()
        
        // then
        XCTAssert(dummyInteractor.latitudeSent == sut.defaultLocation.latitude)
        XCTAssert(dummyInteractor.longitudeSent == sut.defaultLocation.longitude)
        XCTAssert(dummyInteractor.distanceSent == sut.defaultDistance)
        XCTAssert(dummyInteractor.lengthTypeSent == sut.lengthType)
        XCTAssert(dummyInteractor.pageSizeSent == sut.pageSize)
        XCTAssert(dummyInteractor.categoryIdSent == nil)
        XCTAssert(dummyInteractor.trendingIdSent == nil)
        
    }
    
    func test_givenErrorBOInfoAndIsGPSDisabledFalseAndCategoryBO_whenICallErrorAcceptPressed_thenICallInteractorProvidePromotionsWithDefaultValuesAndCategoryId() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        let idType = CategoryEntity(id: "ACTIVITIES", name: "actividades", isFavourite: false)
        sut.categoryBO = CategoryBO(categoryEntity: idType)
        sut.errorBO = ErrorBO(message: Localizables.promotions.key_enable_gps_advise_text, errorType: .info, allowCancel: true)
        sut.isGPSDisabled = false
        
        // when
        sut.errorAcceptPressed()
        
        // then
        XCTAssert(dummyInteractor.latitudeSent == sut.defaultLocation.latitude)
        XCTAssert(dummyInteractor.longitudeSent == sut.defaultLocation.longitude)
        XCTAssert(dummyInteractor.distanceSent == sut.defaultDistance)
        XCTAssert(dummyInteractor.lengthTypeSent == sut.lengthType)
        XCTAssert(dummyInteractor.pageSizeSent == sut.pageSize)
        XCTAssert(dummyInteractor.categoryIdSent == sut.categoryBO?.id.rawValue)
        XCTAssert(dummyInteractor.trendingIdSent == nil)
        
    }
    
    func test_givenErrorBOInfoAndIsGPSDisabledFalseAndTrendingType_whenICallErrorAcceptPressed_thenICallInteractorProvidePromotionsWithDefaultValuesAndTrendingId() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        sut.trendingType = TrendingType(rawValue: "HOT")
        sut.errorBO = ErrorBO(message: Localizables.promotions.key_enable_gps_advise_text, errorType: .info, allowCancel: true)
        sut.isGPSDisabled = false
        
        // when
        sut.errorAcceptPressed()
        
        // then
        XCTAssert(dummyInteractor.latitudeSent == sut.defaultLocation.latitude)
        XCTAssert(dummyInteractor.longitudeSent == sut.defaultLocation.longitude)
        XCTAssert(dummyInteractor.distanceSent == sut.defaultDistance)
        XCTAssert(dummyInteractor.lengthTypeSent == sut.lengthType)
        XCTAssert(dummyInteractor.pageSizeSent == sut.pageSize)
        XCTAssert(dummyInteractor.categoryIdSent == nil)
        XCTAssert(dummyInteractor.trendingIdSent == sut.trendingType?.rawValue)
        
    }
    
    func test_givenErrorBOInfoAndIsGPSDisabledFalse_whenICallErrorAcceptPressedAndInteractorReturnError_thenICallHideLoader() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let errorEntity = ErrorEntity()
        errorEntity.code = "500"
        errorEntity.message = "Error 500"
        
        dummyInteractor.forceError = true
        dummyInteractor.errorBO = ErrorBO(error: errorEntity)
        
        // given
        sut.errorBO = ErrorBO(message: Localizables.promotions.key_enable_gps_advise_text, errorType: .info, allowCancel: true)
        sut.isGPSDisabled = false
        
        // when
        sut.errorAcceptPressed()
        
        // then
        XCTAssert(dummyView.isCalledHideLoader == true)
    }
    
    func test_givenErrorBOInfoAndIsGPSDisabledFalse_whenICallErrorAcceptPressedAndInteractorReturnError_thenICallHidePromotions() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let errorEntity = ErrorEntity()
        errorEntity.code = "500"
        errorEntity.message = "Error 500"
        
        dummyInteractor.forceError = true
        dummyInteractor.errorBO = ErrorBO(error: errorEntity)
        
        // given
        sut.errorBO = ErrorBO(message: Localizables.promotions.key_enable_gps_advise_text, errorType: .info, allowCancel: true)
        sut.isGPSDisabled = false
        
        // when
        sut.errorAcceptPressed()
        
        // then
        XCTAssert(dummyView.isCalledHidePromotions == true)
    }
    
    func test_givenErrorBOInfoAndIsGPSDisabledFalse_whenICallErrorAcceptPressedAndInteractorReturnError_thenICallViewShowError() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let errorEntity = ErrorEntity()
        errorEntity.code = "500"
        errorEntity.message = "Error 500"
        
        dummyInteractor.forceError = true
        dummyInteractor.errorBO = ErrorBO(error: errorEntity)
        
        // given
        sut.errorBO = ErrorBO(message: Localizables.promotions.key_enable_gps_advise_text, errorType: .info, allowCancel: true)
        sut.isGPSDisabled = false
        
        // when
        sut.errorAcceptPressed()
        
        // then
        XCTAssert(dummyView.isCalledShowError == true)
    }
    
    func test_givenErrorBOInfoAndIsGPSDisabledFalse_whenICallErrorAcceptPressedAndInteractorReturnError_thenICallViewShowErrorWithAppropiatedError() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let errorEntity = ErrorEntity()
        errorEntity.code = "500"
        errorEntity.message = "Error 500"
        
        dummyInteractor.forceError = true
        dummyInteractor.errorBO = ErrorBO(error: errorEntity)
        
        // given
        sut.errorBO = ErrorBO(message: Localizables.promotions.key_enable_gps_advise_text, errorType: .info, allowCancel: true)
        sut.isGPSDisabled = false
        
        // when
        sut.errorAcceptPressed()
        
        // then
        XCTAssert(dummyView.errorBO! === dummyInteractor.errorBO!)
    }
    
    func test_givenErrorBOInfoAndIsGPSDisabledFalse_whenICallErrorAcceptPressedAndInteractorReturnSuccess_thenICallHideLoader() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        sut.errorBO = ErrorBO(message: Localizables.promotions.key_enable_gps_advise_text, errorType: .info, allowCancel: true)
        sut.isGPSDisabled = false
        
        // when
        sut.errorAcceptPressed()
        
        // then
        XCTAssert(dummyView.isCalledHideLoader == true)
    }
    
    func test_givenErrorBOInfoAndIsGPSDisabledFalse_whenICallErrorAcceptPressedAndInteractorReturnSuccess_thenDataPoisShouldBeFilled() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        sut.errorBO = ErrorBO(message: Localizables.promotions.key_enable_gps_advise_text, errorType: .info, allowCancel: true)
        sut.isGPSDisabled = false
        
        // when
        sut.errorAcceptPressed()
        
        // then
        XCTAssert(sut.dataPois.isEmpty == false)
    }
    
    func test_givenErrorBOInfoAndIsGPSDisabledFalse_whenICallErrorAcceptPressedAndInteractorReturnSuccess_thenICallShowStores() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        sut.errorBO = ErrorBO(message: Localizables.promotions.key_enable_gps_advise_text, errorType: .info, allowCancel: true)
        sut.isGPSDisabled = false
        
        // when
        sut.errorAcceptPressed()
        
        // then
        XCTAssert(dummyView.isCalledShowStores == true)
    }
    
    func test_givenErrorBOInfoAndIsGPSDisabledFalse_whenICallErrorAcceptPressedAndInteractorReturnSuccess_thenICallHidePromotions() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        sut.errorBO = ErrorBO(message: Localizables.promotions.key_enable_gps_advise_text, errorType: .info, allowCancel: true)
        sut.isGPSDisabled = false
        
        // when
        sut.errorAcceptPressed()
        
        // then
        XCTAssert(dummyView.isCalledHidePromotions == true)
    }
    
    func test_givenErrorBOInfoAndIsGPSDisabledFalse_whenICallErrorAcceptPressedAndInteractorReturnSuccess_thenICallShowStoresWithTheSameNumberOfItemsAsStoresReturned() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        let promotions = PromotionsGenerator.getPromotionsBO()
        
        for promotion in promotions.promotions {
            promotion.promotionType.id = .discount
        }
        
        dummyInteractor.promotionsBO = promotions
        
        // given
        sut.errorBO = ErrorBO(message: Localizables.promotions.key_enable_gps_advise_text, errorType: .info, allowCancel: true)
        sut.isGPSDisabled = false
        
        // when
        sut.errorAcceptPressed()
        
        // then
        
        var numberOfStoresReturned = 0
        
        for promotion in dummyInteractor.promotionsBO!.promotions {
            numberOfStoresReturned += (promotion.stores?.count ?? 0)
        }
        
        XCTAssert(dummyView.displayDataSent!.items.count == numberOfStoresReturned)
    }
    
    func test_givenErrorBOInfoAndIsGPSDisabledFalse_whenICallErrorAcceptPressedAndInteractorReturnSuccessWithoutPromotions_thenICallViewShowToastWithInfoText() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        sut.errorBO = ErrorBO(message: Localizables.promotions.key_enable_gps_advise_text, errorType: .info, allowCancel: true)
        sut.isGPSDisabled = false
        
        let promotions = PromotionsGenerator.getPromotionsBO()
        promotions.promotions = [PromotionBO]()
        dummyInteractor.promotionsBO = promotions
        
        // when
        sut.errorAcceptPressed()
        
        // then
        XCTAssert(dummyView.isCalledShowToastWithInfoText == true)
    }

    // MARK: - errorCancelPressed

    func test_givenAny_whenICallErrorCancelPressed_thenErrorBOShouldBeNil() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        sut.errorBO = ErrorBO(message: Localizables.promotions.key_enable_gps_advise_text, errorType: .info, allowCancel: true)

        // when
        sut.errorCancelPressed()

        // then
        XCTAssert(sut.errorBO == nil)
    }

    func test_givenAny_whenICallErrorCancelPressed_thenICallViewShowToastWithInfoText() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.errorCancelPressed()

        // then
        XCTAssert(dummyView.isCalledShowToastWithInfoText == true)
    }

    func test_givenAny_whenICallErrorCancelPressed_thenICallViewShowToastWithInfoTextWithCorrectText() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.errorCancelPressed()

        // then
        XCTAssert(dummyView.infoText == Localizables.promotions.key_enable_gps_advise_text)
    }

    func test_givenAny_whenICallErrorCancelPressed_thenICallViewShowCenterMap() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.errorCancelPressed()

        // then
        XCTAssert(dummyView.isCalledCenterMap == true)
    }

    func test_givenAny_whenICallErrorCancelPressed_thenICallViewShowCenterMapWithDefaultLatitudeAndLongitude() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.errorCancelPressed()

        // then
        XCTAssert(dummyView.latitude == sut.defaultLocation.latitude)
        XCTAssert(dummyView.longitude == sut.defaultLocation.longitude)
        XCTAssert(dummyView.radiousInMetersSent == Double(sut.defaultDistance * DistanceConstants.meters_to_kilometers))

    }

    func test_givenAny_whenICallErrorCancelPressed_thenICallViewHideUserLocation() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.errorCancelPressed()

        // then
        XCTAssert(dummyView.isCalledHideUserLocation == true)
    }

    func test_givenNotCategoryBONeitherTrending_whenICallErrorCancelPressed_thenICallInteractorProvidePromotions() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.categoryBO = nil
        sut.trendingType = nil

        // when
        sut.errorCancelPressed()

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotions == true)
    }

    func test_givenNotCategoryBONeitherTrending_whenICallErrorCancelPressed_thenLastLocationRetrieveShouldBeFilled() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        dummyInteractor.presenter = sut

        // given
        sut.categoryBO = nil
        sut.trendingType = nil

        // when
        sut.errorCancelPressed()

        // then
        XCTAssert(sut.lastLocationRetrieve != nil)
    }

    func test_givenNotCategoryBONeitherTrending_whenICallErrorCancelPressed_thenLastLocationRetrieveShouldHaveLongitudeAndLatitudeDefault() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        dummyInteractor.presenter = sut

        // given
        sut.categoryBO = nil
        sut.trendingType = nil

        // when
        sut.errorCancelPressed()

        // then
        XCTAssert(sut.lastLocationRetrieve?.latitude == sut.defaultLocation.latitude)
        XCTAssert(sut.lastLocationRetrieve?.longitude == sut.defaultLocation.longitude)
    }

    func test_givenNotCategoryBONeitherTrending_whenICallErrorCancelPressed_thenICallShowLoader() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        dummyInteractor.presenter = sut

        // given
        sut.categoryBO = nil
        sut.trendingType = nil

        // when
        sut.errorCancelPressed()

        // then
        XCTAssert(dummyView.isCalledShowLoader == true)
    }

    func test_givenNotCategoryBONeitherTrending_whenICallErrorCancelPressed_thenICallInteractorProvidePromotionsWithDefaultValues() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.categoryBO = nil
        sut.trendingType = nil

        // when
        sut.errorCancelPressed()

        // then
        XCTAssert(dummyInteractor.latitudeSent == sut.defaultLocation.latitude)
        XCTAssert(dummyInteractor.longitudeSent == sut.defaultLocation.longitude)
        XCTAssert(dummyInteractor.distanceSent == sut.defaultDistance)
        XCTAssert(dummyInteractor.lengthTypeSent == sut.lengthType)
        XCTAssert(dummyInteractor.pageSizeSent == sut.pageSize)
        XCTAssert(dummyInteractor.categoryIdSent == nil)
        XCTAssert(dummyInteractor.trendingIdSent == nil)

    }

    func test_givenWithCategoryBO_whenICallErrorCancelPressed_thenICallInteractorProvidePromotionsWithDefaultValuesAndCategoryId() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let idType = CategoryEntity(id: "ACTIVITIES", name: "actividades", isFavourite: false)
        sut.categoryBO = CategoryBO(categoryEntity: idType)
        sut.trendingType = nil

        // when
        sut.errorCancelPressed()

        // then
        XCTAssert(dummyInteractor.latitudeSent == sut.defaultLocation.latitude)
        XCTAssert(dummyInteractor.longitudeSent == sut.defaultLocation.longitude)
        XCTAssert(dummyInteractor.distanceSent == sut.defaultDistance)
        XCTAssert(dummyInteractor.lengthTypeSent == sut.lengthType)
        XCTAssert(dummyInteractor.pageSizeSent == sut.pageSize)
        XCTAssert(dummyInteractor.categoryIdSent == sut.categoryBO?.id.rawValue)
        XCTAssert(dummyInteractor.trendingIdSent == nil)

    }

    func test_givenWithTrendingType_whenICallErrorCancelPressed_thenICallInteractorProvidePromotionsWithDefaultValuesAndTrendingId() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.categoryBO = nil
        sut.trendingType = TrendingType(rawValue: "HOT")

        // when
        sut.errorCancelPressed()

        // then
        XCTAssert(dummyInteractor.latitudeSent == sut.defaultLocation.latitude)
        XCTAssert(dummyInteractor.longitudeSent == sut.defaultLocation.longitude)
        XCTAssert(dummyInteractor.distanceSent == sut.defaultDistance)
        XCTAssert(dummyInteractor.lengthTypeSent == sut.lengthType)
        XCTAssert(dummyInteractor.pageSizeSent == sut.pageSize)
        XCTAssert(dummyInteractor.categoryIdSent == nil)
        XCTAssert(dummyInteractor.trendingIdSent == sut.trendingType?.rawValue)

    }

    func test_givenNotCategoryBONeitherTrending_whenICallErrorCancelPressedAndInteractorReturnError_thenICallHideLoader() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let errorEntity = ErrorEntity()
        errorEntity.code = "500"
        errorEntity.message = "Error 500"

        dummyInteractor.forceError = true
        dummyInteractor.errorBO = ErrorBO(error: errorEntity)

        // given
        sut.categoryBO = nil
        sut.trendingType = nil

        // when
        sut.errorCancelPressed()

        // then
        XCTAssert(dummyView.isCalledHideLoader == true)
    }

    func test_givenNotCategoryBONeitherTrending_whenICallErrorCancelPressedAndInteractorReturnError_thenICallHidePromotions() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let errorEntity = ErrorEntity()
        errorEntity.code = "500"
        errorEntity.message = "Error 500"

        dummyInteractor.forceError = true
        dummyInteractor.errorBO = ErrorBO(error: errorEntity)

        // given
        sut.categoryBO = nil
        sut.trendingType = nil

        // when
        sut.errorCancelPressed()

        // then
        XCTAssert(dummyView.isCalledHidePromotions == true)
    }

    func test_givenNotCategoryBONeitherTrending_whenICallErrorCancelPressedAndInteractorReturnError_thenErrorBOShouldBeFilled() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let errorEntity = ErrorEntity()
        errorEntity.code = "500"
        errorEntity.message = "Error 500"

        dummyInteractor.forceError = true
        dummyInteractor.errorBO = ErrorBO(error: errorEntity)

        // given
        sut.categoryBO = nil
        sut.trendingType = nil

        // when
        sut.errorCancelPressed()

        // then
        XCTAssert(sut.errorBO != nil)
        XCTAssert(sut.errorBO! === dummyInteractor.errorBO!)
    }

    func test_givenNotCategoryBONeitherTrending_whenICallErrorCancelPressedAndInteractorReturnError_thenICallViewShowError() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let errorEntity = ErrorEntity()
        errorEntity.code = "500"
        errorEntity.message = "Error 500"

        dummyInteractor.forceError = true
        dummyInteractor.errorBO = ErrorBO(error: errorEntity)

        // given
        sut.categoryBO = nil
        sut.trendingType = nil

        // when
        sut.errorCancelPressed()

        // then
        XCTAssert(dummyView.isCalledShowError == true)
    }

    func test_givenNotCategoryBONeitherTrending_whenICallErrorCancelPressedAndInteractorReturnSuccess_thenICallHideLoader() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.categoryBO = nil
        sut.trendingType = nil

        // when
        sut.errorCancelPressed()

        // then
        XCTAssert(dummyView.isCalledHideLoader == true)
    }

    func test_givenNotCategoryBONeitherTrending_whenICallErrorCancelPressedAndInteractorReturnSuccess_thenDataPoisShouldBeFilled() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.categoryBO = nil
        sut.trendingType = nil

        // when
        sut.errorCancelPressed()

        // then
        XCTAssert(sut.dataPois.isEmpty == false)
    }

    func test_givenNotCategoryBONeitherTrending_whenICallErrorCancelPressedAndInteractorReturnSuccess_thenICallShowStores() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.categoryBO = nil
        sut.trendingType = nil

        // when
        sut.errorCancelPressed()

        // then
        XCTAssert(dummyView.isCalledShowStores == true)
    }

    func test_givenNotCategoryBONeitherTrending_whenICallErrorCancelPressedAndInteractorReturnSuccess_thenICallHidePromotions() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.categoryBO = nil
        sut.trendingType = nil

        // when
        sut.errorCancelPressed()

        // then
        XCTAssert(dummyView.isCalledHidePromotions == true)
    }

    func test_givenNotCategoryBONeitherTrendingAndGPSDisabled_whenICallErrorCancelPressedAndInteractorReturnSuccess_thenICallShowStoresWithTheSameNumberOfItemsAsStoresReturned() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        let promotions = PromotionsGenerator.getPromotionsBO()

        for promotion in promotions.promotions {
            promotion.promotionType.id = .discount
        }

        dummyInteractor.promotionsBO = promotions

        // given
        sut.categoryBO = nil
        sut.trendingType = nil
        sut.isGPSDisabled = true

        // when
        sut.errorCancelPressed()

        // then

        var numberOfStoresReturned = 0

        for promotion in dummyInteractor.promotionsBO!.promotions {
            numberOfStoresReturned += (promotion.stores?.count ?? 0)
        }

        XCTAssert(dummyView.displayDataSent!.items.count == numberOfStoresReturned)
    }

    func test_givenADisplayDataMapNotCategoryBONeitherTrendingAndGPSDisabled_whenICallErrorCancelPressedAndInteractorReturnSuccess_thenICallShowStoresWithTheSameNumberOfItemsAsStoresReturned() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        let promotions = PromotionsGenerator.getPromotionsBO()

        for promotion in promotions.promotions {
            promotion.promotionType.id = .discount
        }

        dummyInteractor.promotionsBO = promotions

        let dataPoi = DataPoi(latitude: 3, longitude: 4, storeId: promotions.promotions[0].stores![0].id)
        dataPoi.append(promotion: promotions.promotions[0])

        let displayDataMap = MapDisplayData()
        displayDataMap.items.append(MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)!)

        // given
        sut.categoryBO = nil
        sut.trendingType = nil
        sut.isGPSDisabled = true
        sut.displayData = displayDataMap

        // when
        sut.errorCancelPressed()

        // then

        var numberOfStoresReturned = 0

        for promotion in dummyInteractor.promotionsBO!.promotions {
            numberOfStoresReturned += (promotion.stores?.count ?? 0)
        }

        XCTAssert(dummyView.displayDataSent!.items.count == numberOfStoresReturned)
    }

    func test_givenWithCategoryBO_whenICallErrorCancelPressedAndInteractorReturnSuccess_thenICallShowStores() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let idType = CategoryEntity(id: "ACTIVITIES", name: "actividades", isFavourite: false)
        sut.categoryBO = CategoryBO(categoryEntity: idType)
        sut.trendingType = nil

        // when
        sut.errorCancelPressed()

        // then
        XCTAssert(dummyView.isCalledShowStores == true)
    }

    func test_givenWithCategoryBO_whenICallErrorCancelPressedAndInteractorReturnSuccess_thenICallHidePromotions() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let idType = CategoryEntity(id: "ACTIVITIES", name: "actividades", isFavourite: false)
        sut.categoryBO = CategoryBO(categoryEntity: idType)
        sut.trendingType = nil

        // when
        sut.errorCancelPressed()

        // then
        XCTAssert(dummyView.isCalledHidePromotions == true)
    }

    func test_givenWithCategoryBO_whenICallErrorCancelPressedAndInteractorReturnSuccess_thenICallShowStoresWithTheSameNumberOfItemsAsStoresReturned() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        let promotions = PromotionsGenerator.getPromotionsBO()

        for promotion in promotions.promotions {
            promotion.promotionType.id = .discount
        }

        dummyInteractor.promotionsBO = promotions

        // given
        let idType = CategoryEntity(id: "ACTIVITIES", name: "actividades", isFavourite: false)
        sut.categoryBO = CategoryBO(categoryEntity: idType)
        sut.trendingType = nil
        sut.isGPSDisabled = true

        // when
        sut.errorCancelPressed()

        // then

        var numberOfStoresReturned = 0

        for promotion in dummyInteractor.promotionsBO!.promotions {
            numberOfStoresReturned += (promotion.stores?.count ?? 0)
        }

        XCTAssert(dummyView.displayDataSent!.items.count == numberOfStoresReturned)
    }

    func test_givenADisplayDataMapWithCategoryBO_whenICallErrorCancelPressedAndInteractorReturnSuccess_thenICallShowStoresWithTheSameNumberOfItemsAsStoresReturned() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        let promotions = PromotionsGenerator.getPromotionsBO()

        for promotion in promotions.promotions {
            promotion.promotionType.id = .discount
        }

        dummyInteractor.promotionsBO = promotions

        let dataPoi = DataPoi(latitude: 3, longitude: 4, storeId: promotions.promotions[0].stores![0].id)
        dataPoi.append(promotion: promotions.promotions[0])

        let displayDataMap = MapDisplayData()
        displayDataMap.items.append(MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)!)

        // given
        let idType = CategoryEntity(id: "ACTIVITIES", name: "actividades", isFavourite: false)
        sut.categoryBO = CategoryBO(categoryEntity: idType)
        sut.trendingType = nil
        sut.isGPSDisabled = true
        sut.displayData = displayDataMap

        // when
        sut.errorCancelPressed()

        // then

        var numberOfStoresReturned = 0

        for promotion in dummyInteractor.promotionsBO!.promotions {
            numberOfStoresReturned += (promotion.stores?.count ?? 0)
        }

        XCTAssert(dummyView.displayDataSent!.items.count == numberOfStoresReturned)
    }

    func test_givenWithTrendingType_whenICallErrorCancelPressedAndInteractorReturnSuccess_thenICallShowStores() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.categoryBO = nil
        sut.trendingType = TrendingType(rawValue: "HOT")

        // when
        sut.errorCancelPressed()

        // then
        XCTAssert(dummyView.isCalledShowStores == true)
    }

    func test_givenWithTrendingType_whenICallErrorCancelPressedAndInteractorReturnSuccess_thenICallHidePromotions() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.categoryBO = nil
        sut.trendingType = TrendingType(rawValue: "HOT")

        // when
        sut.errorCancelPressed()

        // then
        XCTAssert(dummyView.isCalledHidePromotions == true)
    }

    func test_givenWithTrendingType_whenICallErrorCancelPressedAndInteractorReturnSuccess_thenICallShowStoresWithTheSameNumberOfItemsAsStoresReturned() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        let promotions = PromotionsGenerator.getPromotionsBO()

        for promotion in promotions.promotions {
            promotion.promotionType.id = .discount
        }

        dummyInteractor.promotionsBO = promotions

        // given
        sut.categoryBO = nil
        sut.trendingType = TrendingType(rawValue: "HOT")
        sut.isGPSDisabled = true

        // when
        sut.errorCancelPressed()

        // then

        var numberOfStoresReturned = 0

        for promotion in dummyInteractor.promotionsBO!.promotions {
            numberOfStoresReturned += (promotion.stores?.count ?? 0)
        }

        XCTAssert(dummyView.displayDataSent!.items.count == numberOfStoresReturned)
    }

    func test_givenADisplayDataMapWithTrendingType_whenICallErrorCancelPressedAndInteractorReturnSuccess_thenICallShowStoresWithTheSameNumberOfItemsAsStoresReturned() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        let promotions = PromotionsGenerator.getPromotionsBO()

        for promotion in promotions.promotions {
            promotion.promotionType.id = .discount
        }

        dummyInteractor.promotionsBO = promotions

        let dataPoi = DataPoi(latitude: 3, longitude: 4, storeId: promotions.promotions[0].stores![0].id)
        dataPoi.append(promotion: promotions.promotions[0])

        let displayDataMap = MapDisplayData()
        displayDataMap.items.append(MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)!)

        // given
        sut.categoryBO = nil
        sut.trendingType = TrendingType(rawValue: "HOT")
        sut.isGPSDisabled = true
        sut.displayData = displayDataMap

        // when
        sut.errorCancelPressed()

        // then

        var numberOfStoresReturned = 0

        for promotion in dummyInteractor.promotionsBO!.promotions {
            numberOfStoresReturned += (promotion.stores?.count ?? 0)
        }

        XCTAssert(dummyView.displayDataSent!.items.count == numberOfStoresReturned)
    }

    func test_givenAny_whenICallErrorCancelAndInteractorReturnPromotionsWithStores_thenIDoNotCallShowToast() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // when
        sut.errorCancelPressed()

        // then
        XCTAssert(dummyView.isCalledShowToastNotStore == false)

    }

    func test_givenCategoryBO_whenICallErrorCancelAndInteractorReturnPromotionsWithStores_thenIDoNotCallShowToast() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let idType = CategoryEntity(id: "ACTIVITIES", name: "actividades", isFavourite: false)
        sut.categoryBO = CategoryBO(categoryEntity: idType)

        // when
        sut.errorCancelPressed()

        // then
        XCTAssert(dummyView.isCalledShowToastNotStore == false)

    }

    func test_givenTrendingBO_whenICallErrorCancelAndInteractorReturnPromotionsWithStores_thenIDoNotCallShowToast() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.trendingType = TrendingType(rawValue: "HOT")

        // when
        sut.errorCancelPressed()

        // then
        XCTAssert(dummyView.isCalledShowToastNotStore == false)

    }

    func test_givenAny_whenICallErrorCancelAndInteractorReturnPromotionsWithoutStores_thenICallShowToast() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let promotions = PromotionsGenerator.getPromotionsBO()

        for promotion in promotions.promotions {

            promotion.stores = nil

        }

        dummyInteractor.promotionsBO = promotions

        // when
        sut.errorCancelPressed()

        // then
        XCTAssert(dummyView.isCalledShowToastNotStore == true)

    }

    func test_givenCategoryBO_whenICallErrorCancelAndInteractorReturnPromotionsWithoutStores_thenICallShowToast() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let idType = CategoryEntity(id: "ACTIVITIES", name: "actividades", isFavourite: false)
        sut.categoryBO = CategoryBO(categoryEntity: idType)

        let promotions = PromotionsGenerator.getPromotionsBO()

        for promotion in promotions.promotions {

            promotion.stores = nil

        }

        dummyInteractor.promotionsBO = promotions

        // when
        sut.errorCancelPressed()

        // then
        XCTAssert(dummyView.isCalledShowToastNotStore == true)

    }

    func test_givenTrendingBO_whenICallErrorCancelAndInteractorReturnPromotionsWithoutStores_thenICallShowToast() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.trendingType = TrendingType(rawValue: "HOT")

        let promotions = PromotionsGenerator.getPromotionsBO()

        for promotion in promotions.promotions {

            promotion.stores = nil

        }

        dummyInteractor.promotionsBO = promotions

        // when
        sut.errorCancelPressed()

        // then
        XCTAssert(dummyView.isCalledShowToastNotStore == true)

    }

    // MARK: - SetModel

    func test_givenAPromotionsTransportWithCategory_whenICallOnMessage_thenDataShouldBeFilled() {

        // given
        let promotionsTransport = PromotionsTransport()
        promotionsTransport.category = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.setModel(model: promotionsTransport)

        // then
        XCTAssert(sut.categoryBO === promotionsTransport.category)
        XCTAssert(sut.trendingType == nil)

    }

    func test_givenAPromotionsTransportWithTrending_whenICallOnMessage_thenDataShouldBeFilled() {

        // given

        let promotionsTransport = PromotionsTransport()
        promotionsTransport.trendingType = .hot

        // when
        sut.setModel(model: promotionsTransport)

        // then
        XCTAssert(sut.categoryBO == nil)
        XCTAssert(sut.trendingType == promotionsTransport.trendingType)

    }

    func test_givenAPromotionsTransportWithCategories_whenICallOnMessage_thenCategoriesBOShouldMatch() {

        // given

        let promotionsTransport = PromotionsTransport()
        promotionsTransport.categories = PromotionsGenerator.getCategoryBO()

        // when
        sut.setModel(model: promotionsTransport)

        // then
        XCTAssert(sut.categoryBO == nil)
        XCTAssert(sut.categoriesBO! === promotionsTransport.categories)

    }
    
    // MARK: - obtainLocationFail

    func test_givenAny_whenICallViewWillAppearAndObtainLocationFail_thenErrorBOShouldBeNil() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        sut.errorBO = ErrorBO(message: Localizables.promotions.key_enable_gps_advise_text, errorType: .info, allowCancel: true)
        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(sut.errorBO == nil)
    }

    func test_givenAny_whenICallViewWillAppearAndObtainLocationFail_thenICallViewShowCenterMap() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledCenterMap == true)
    }

    func test_givenAny_whenICallViewWillAppearAndObtainLocationFail_thenICallViewShowCenterMapWithDefaultLatitudeAndLongitude() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.latitude == sut.defaultLocation.latitude)
        XCTAssert(dummyView.longitude == sut.defaultLocation.longitude)
        XCTAssert(dummyView.radiousInMetersSent == Double(sut.defaultDistance * DistanceConstants.meters_to_kilometers))

    }

    func test_givenAny_whenICallViewWillAppearAndObtainLocationFail_thenICallViewHideUserLocation() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledHideUserLocation == true)
    }

    func test_givenNotCategoryBONeitherTrendingWithIsRetrivingStoresFalse_whenICallViewWillAppearAndObtainLocationFail_thenICallInteractorProvidePromotions() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.categoryBO = nil
        sut.trendingType = nil
        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotions == true)
    }

    func test_givenNotCategoryBONeitherTrendingWithIsRetrivingStoresFalse_whenICallViewWillAppearAndObtainLocationFail_thenLastLocationRetrieveShouldBeFilled() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        dummyInteractor.presenter = sut

        // given
        sut.categoryBO = nil
        sut.trendingType = nil

        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(sut.lastLocationRetrieve != nil)
    }

    func test_givenNotCategoryBONeitherTrendingWithIsRetrivingStoresFalse_whenICallViewWillAppearAndObtainLocationFail_thenLastLocationRetrieveShouldHaveLongitudeAndLatitudeDefault() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        dummyInteractor.presenter = sut

        // given
        sut.categoryBO = nil
        sut.trendingType = nil

        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(sut.lastLocationRetrieve?.latitude == sut.defaultLocation.latitude)
        XCTAssert(sut.lastLocationRetrieve?.longitude == sut.defaultLocation.longitude)

    }

    func test_givenNotCategoryBONeitherTrendingWithIsRetrivingStoresFalse_whenICallViewWillAppearAndObtainLocationFail_thenICallShowLoader() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        dummyInteractor.presenter = sut

        // given
        sut.categoryBO = nil
        sut.trendingType = nil

        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowLoader == true)
    }

    func test_givenNotCategoryBONeitherTrending_whenICallViewWillAppearAndObtainLocationFail_thenICallInteractorProvidePromotionsWithDefaultValues() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.categoryBO = nil
        sut.trendingType = nil
        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyInteractor.latitudeSent == sut.defaultLocation.latitude)
        XCTAssert(dummyInteractor.longitudeSent == sut.defaultLocation.longitude)
        XCTAssert(dummyInteractor.distanceSent == sut.defaultDistance)
        XCTAssert(dummyInteractor.lengthTypeSent == sut.lengthType)
        XCTAssert(dummyInteractor.pageSizeSent == sut.pageSize)
        XCTAssert(dummyInteractor.categoryIdSent == nil)
        XCTAssert(dummyInteractor.trendingIdSent == nil)

    }

    func test_givenWithCategoryBO_whenICallViewWillAppearAndObtainLocationFail_thenICallInteractorProvidePromotionsWithDefaultValuesAndCategoryId() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let idType = CategoryEntity(id: "ACTIVITIES", name: "actividades", isFavourite: false)
        sut.categoryBO = CategoryBO(categoryEntity: idType)
        sut.trendingType = nil
        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyInteractor.latitudeSent == sut.defaultLocation.latitude)
        XCTAssert(dummyInteractor.longitudeSent == sut.defaultLocation.longitude)
        XCTAssert(dummyInteractor.distanceSent == sut.defaultDistance)
        XCTAssert(dummyInteractor.lengthTypeSent == sut.lengthType)
        XCTAssert(dummyInteractor.pageSizeSent == sut.pageSize)
        XCTAssert(dummyInteractor.categoryIdSent == sut.categoryBO?.id.rawValue)
        XCTAssert(dummyInteractor.trendingIdSent == nil)

    }

    func test_givenWithTrendingType_whenICallViewWillAppearAndObtainLocationFail_thenICallInteractorProvidePromotionsWithDefaultValuesAndTrendingId() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.categoryBO = nil
        sut.trendingType = TrendingType(rawValue: "HOT")
        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyInteractor.latitudeSent == sut.defaultLocation.latitude)
        XCTAssert(dummyInteractor.longitudeSent == sut.defaultLocation.longitude)
        XCTAssert(dummyInteractor.distanceSent == sut.defaultDistance)
        XCTAssert(dummyInteractor.lengthTypeSent == sut.lengthType)
        XCTAssert(dummyInteractor.pageSizeSent == sut.pageSize)
        XCTAssert(dummyInteractor.categoryIdSent == nil)
        XCTAssert(dummyInteractor.trendingIdSent == sut.trendingType?.rawValue)

    }

    func test_givenNotCategoryBONeitherTrending_whenICallViewWillAppearAndObtainLocationFailAndInteractorReturnError_thenErrorBOShouldBeFilled() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let errorEntity = ErrorEntity()
        errorEntity.code = "500"
        errorEntity.message = "Error 500"

        dummyInteractor.forceError = true
        dummyInteractor.errorBO = ErrorBO(error: errorEntity)

        // given
        sut.categoryBO = nil
        sut.trendingType = nil
        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(sut.errorBO != nil)
        XCTAssert(sut.errorBO! === dummyInteractor.errorBO!)
    }

    func test_givenNotCategoryBONeitherTrending_whenICallViewWillAppearAndObtainLocationFailAndInteractorReturnError_thenICallHideLoader() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let errorEntity = ErrorEntity()
        errorEntity.code = "500"
        errorEntity.message = "Error 500"

        dummyInteractor.forceError = true
        dummyInteractor.errorBO = ErrorBO(error: errorEntity)

        // given
        sut.categoryBO = nil
        sut.trendingType = nil
        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledHideLoader == true)
    }

    func test_givenNotCategoryBONeitherTrending_whenICallViewWillAppearAndObtainLocationFailAndInteractorReturnError_thenICallHidePromotions() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let errorEntity = ErrorEntity()
        errorEntity.code = "500"
        errorEntity.message = "Error 500"

        dummyInteractor.forceError = true
        dummyInteractor.errorBO = ErrorBO(error: errorEntity)

        // given
        sut.categoryBO = nil
        sut.trendingType = nil
        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledHidePromotions == true)
    }

    func test_givenNotCategoryBONeitherTrending_whenICallViewWillAppearAndObtainLocationFailAndInteractorReturnError_thenICallViewShowError() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let errorEntity = ErrorEntity()
        errorEntity.code = "500"
        errorEntity.message = "Error 500"

        dummyInteractor.forceError = true
        dummyInteractor.errorBO = ErrorBO(error: errorEntity)

        // given
        sut.categoryBO = nil
        sut.trendingType = nil
        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowError == true)
    }

    func test_givenNotCategoryBONeitherTrending_whenICallViewWillAppearAndObtainLocationFailAndInteractorReturnSuccess_thenDataPoisShouldBeFilled() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.categoryBO = nil
        sut.trendingType = nil
        sut.isGPSDisabled = true
        sut.lastLocationRetrieve = nil
        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(sut.dataPois.isEmpty == false)
    }

    func test_givenNotCategoryBONeitherTrending_whenICallViewWillAppearAndObtainLocationFailAndInteractorReturnSuccess_thenICallHideLoader() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.categoryBO = nil
        sut.trendingType = nil
        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledHideLoader == true)
    }

    func test_givenNotCategoryBONeitherTrending_whenICallViewWillAppearAndObtainLocationFailAndInteractorReturnSuccess_thenICallShowStores() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.categoryBO = nil
        sut.trendingType = nil
        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowStores == true)
    }

    func test_givenNotCategoryBONeitherTrending_whenICallViewWillAppearAndObtainLocationFailAndInteractorReturnSuccess_thenICallHidePromotions() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.categoryBO = nil
        sut.trendingType = nil
        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledHidePromotions == true)
    }

    func test_givenNotCategoryBONeitherTrending_whenICallViewWillAppearAndObtainLocationFailAndInteractorReturnSuccess_thenICallShowStoresWithTheSameNumberOfItemsAsStoresReturned() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        let promotions = PromotionsGenerator.getPromotionsBO()

        for promotion in promotions.promotions {
            promotion.promotionType.id = .discount
        }

        dummyInteractor.promotionsBO = promotions

        // given
        sut.categoryBO = nil
        sut.trendingType = nil
        sut.isGPSDisabled = true
        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        // then

        var numberOfStoresReturned = 0

        for promotion in dummyInteractor.promotionsBO!.promotions {
            numberOfStoresReturned += (promotion.stores?.count ?? 0)
        }

        XCTAssert(dummyView.displayDataSent!.items.count == numberOfStoresReturned)
    }

    func test_givenADisplayDataMapNotCategoryBONeitherTrending_whenICallViewWillAppearAndObtainLocationFailAndInteractorReturnSuccess_thenICallShowStoresWithTheSameNumberOfItemsAsStoresReturned() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        let promotions = PromotionsGenerator.getPromotionsBO()

        for promotion in promotions.promotions {
            promotion.promotionType.id = .discount
        }

        dummyInteractor.promotionsBO = promotions

        let dataPoi = DataPoi(latitude: 3, longitude: 4, storeId: promotions.promotions[0].stores![0].id)
        dataPoi.append(promotion: promotions.promotions[0])

        let displayDataMap = MapDisplayData()
        displayDataMap.items.append(MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)!)

        // given
        sut.categoryBO = nil
        sut.trendingType = nil
        sut.isGPSDisabled = true
        sut.displayData = displayDataMap
        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        // then

        var numberOfStoresReturned = 0

        for promotion in dummyInteractor.promotionsBO!.promotions {
            numberOfStoresReturned += (promotion.stores?.count ?? 0)
        }

        XCTAssert(dummyView.displayDataSent!.items.count == numberOfStoresReturned)
    }

    func test_givenWithCategoryBO_whenICallViewWillAppearAndObtainLocationFailAndInteractorReturnSuccess_thenICallShowStores() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let idType = CategoryEntity(id: "ACTIVITIES", name: "actividades", isFavourite: false)
        sut.categoryBO = CategoryBO(categoryEntity: idType)
        sut.trendingType = nil
        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowStores == true)
    }

    func test_givenWithCategoryBO_whenICallViewWillAppearAndObtainLocationFailAndInteractorReturnSuccess_thenICallHidePromotions() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let idType = CategoryEntity(id: "ACTIVITIES", name: "actividades", isFavourite: false)
        sut.categoryBO = CategoryBO(categoryEntity: idType)
        sut.trendingType = nil
        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledHidePromotions == true)
    }

    func test_givenWithCategoryBO_whenICallViewWillAppearAndObtainLocationFailAndInteractorReturnSuccess_thenICallShowStoresWithTheSameNumberOfItemsAsStoresReturned() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        let promotions = PromotionsGenerator.getPromotionsBO()

        for promotion in promotions.promotions {
            promotion.promotionType.id = .discount
        }

        dummyInteractor.promotionsBO = promotions

        // given
        let idType = CategoryEntity(id: "ACTIVITIES", name: "actividades", isFavourite: false)
        sut.categoryBO = CategoryBO(categoryEntity: idType)
        sut.trendingType = nil
        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        // then

        var numberOfStoresReturned = 0

        for promotion in dummyInteractor.promotionsBO!.promotions {
            numberOfStoresReturned += (promotion.stores?.count ?? 0)
        }

        XCTAssert(dummyView.displayDataSent!.items.count == numberOfStoresReturned)
    }

    func test_givenADisplayDataMapWithCategoryBO_whenICallViewWillAppearAndObtainLocationFailAndInteractorReturnSuccess_thenICallShowStoresWithTheSameNumberOfItemsAsStoresReturned() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        let promotions = PromotionsGenerator.getPromotionsBO()

        for promotion in promotions.promotions {
            promotion.promotionType.id = .discount
        }

        dummyInteractor.promotionsBO = promotions

        let dataPoi = DataPoi(latitude: 3, longitude: 4, storeId: promotions.promotions[0].stores![0].id)
        dataPoi.append(promotion: promotions.promotions[0])

        let displayDataMap = MapDisplayData()
        displayDataMap.items.append(MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)!)

        // given
        let idType = CategoryEntity(id: "ACTIVITIES", name: "actividades", isFavourite: false)
        sut.categoryBO = CategoryBO(categoryEntity: idType)
        sut.trendingType = nil
        sut.displayData = displayDataMap
        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        // then

        var numberOfStoresReturned = 0

        for promotion in dummyInteractor.promotionsBO!.promotions {
            numberOfStoresReturned += (promotion.stores?.count ?? 0)
        }

        XCTAssert(dummyView.displayDataSent!.items.count == numberOfStoresReturned)
    }

    func test_givenWithTrendingType_whenICallViewWillAppearAndObtainLocationFailAndInteractorReturnSuccess_thenICallShowStores() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.categoryBO = nil
        sut.trendingType = TrendingType(rawValue: "HOT")
        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowStores == true)
    }

    func test_givenWithTrendingType_whenICallViewWillAppearAndObtainLocationFailAndInteractorReturnSuccess_thenICallHidePromotions() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.categoryBO = nil
        sut.trendingType = TrendingType(rawValue: "HOT")
        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledHidePromotions == true)
    }

    func test_givenWithTrendingType_whenICallViewWillAppearAndObtainLocationFailAndInteractorReturnSuccess_thenICallShowStoresWithTheSameNumberOfItemsAsStoresReturned() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        let promotions = PromotionsGenerator.getPromotionsBO()

        for promotion in promotions.promotions {
            promotion.promotionType.id = .discount
        }

        dummyInteractor.promotionsBO = promotions

        // given
        sut.categoryBO = nil
        sut.trendingType = TrendingType(rawValue: "HOT")
        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        // then

        var numberOfStoresReturned = 0

        for promotion in dummyInteractor.promotionsBO!.promotions {
            numberOfStoresReturned += (promotion.stores?.count ?? 0)
        }

        XCTAssert(dummyView.displayDataSent!.items.count == numberOfStoresReturned)
    }

    func test_givenADisplayDataMapWithTrendingType_whenICallViewWillAppearAndObtainLocationFailAndInteractorReturnSuccess_thenICallShowStoresWithTheSameNumberOfItemsAsStoresReturned() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        let promotions = PromotionsGenerator.getPromotionsBO()

        for promotion in promotions.promotions {
            promotion.promotionType.id = .discount
        }

        dummyInteractor.promotionsBO = promotions

        let dataPoi = DataPoi(latitude: 3, longitude: 4, storeId: promotions.promotions[0].stores![0].id)
        dataPoi.append(promotion: promotions.promotions[0])

        let displayDataMap = MapDisplayData()
        displayDataMap.items.append(MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)!)

        // given
        sut.categoryBO = nil
        sut.trendingType = TrendingType(rawValue: "HOT")
        sut.displayData = displayDataMap
        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        // then

        var numberOfStoresReturned = 0

        for promotion in dummyInteractor.promotionsBO!.promotions {
            numberOfStoresReturned += (promotion.stores?.count ?? 0)
        }

        XCTAssert(dummyView.displayDataSent!.items.count == numberOfStoresReturned)
    }

    func test_givenAny_whenICallViewWillAppearAndObtainLocationFailAndInteractorReturnPromotionsWithStores_thenIDoNotCallShowToast() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowToastNotStore == false)

    }

    func test_givenCategoryBO_whenICallViewWillAppearAndObtainLocationFailAndInteractorReturnPromotionsWithStores_thenIDoNotCallShowToast() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let idType = CategoryEntity(id: "ACTIVITIES", name: "actividades", isFavourite: false)
        sut.categoryBO = CategoryBO(categoryEntity: idType)
        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowToastNotStore == false)

    }

    func test_givenTrendingBO_whenICallViewWillAppearAndObtainLocationFailAndInteractorReturnPromotionsWithStores_thenIDoNotCallShowToast() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.trendingType = TrendingType(rawValue: "HOT")

        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowToastNotStore == false)

    }

    func test_givenAny_whenICallViewWillAppearAndObtainLocationFailAndInteractorReturnPromotionsWithoutStores_thenICallShowToast() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let promotions = PromotionsGenerator.getPromotionsBO()

        for promotion in promotions.promotions {

            promotion.stores = nil

        }

        dummyInteractor.promotionsBO = promotions

        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowToastNotStore == true)

    }

    func test_givenCategoryBO_whenICallViewWillAppearAndObtainLocationFailAndInteractorReturnPromotionsWithoutStores_thenICallShowToast() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let idType = CategoryEntity(id: "ACTIVITIES", name: "actividades", isFavourite: false)
        sut.categoryBO = CategoryBO(categoryEntity: idType)

        let promotions = PromotionsGenerator.getPromotionsBO()

        for promotion in promotions.promotions {

            promotion.stores = nil

        }

        dummyInteractor.promotionsBO = promotions

        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowToastNotStore == true)

    }

    func test_givenTrendingBO_whenICallViewWillAppearAndObtainLocationFailAndInteractorReturnPromotionsWithoutStores_thenICallShowToast() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.trendingType = TrendingType(rawValue: "HOT")

        let promotions = PromotionsGenerator.getPromotionsBO()

        for promotion in promotions.promotions {

            promotion.stores = nil

        }

        dummyInteractor.promotionsBO = promotions

        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowToastNotStore == true)

    }

    func test_givenAny_whenICallViewWillAppearAndObtainLocationFail_thenUserLocationIsNil() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        dummyLocationManager.locationErrorType = .permissionDenied

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(sut.userLocation == nil)

    }

    // MARK: - obtainedLocation

    func test_givenUserLocation_whenICallViewWillAppearAndObtainedLocation_thenUserLatitudeAndUserLongitudeShouldMatch() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let latitude = 33.11
        let longitude = 44.22
         dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()
        // then
        XCTAssert(sut.userLocation?.latitude == latitude)
        XCTAssert(sut.userLocation?.longitude == longitude)

    }

    func test_givenAny_whenICallViewWillAppearAndObtainedLocation_thenICallViewShowUserLocation() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let latitude = 33.11
        let longitude = 44.22
        dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowUserLocation == true)

    }

    func test_givenAny_whenICallViewWillAppearAndObtainedLocation_thenICallViewShowCenterMap() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let latitude = 4.0
        let longitude = 2.0
         dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()
        // then
        XCTAssert(dummyView.isCalledCenterMap == true)
    }

    func test_givenAny_whenICallViewWillAppearAndObtainedLocation_thenICallViewShowCenterMapWithLatitudeAndLongitudeAsParameterAndRadiousDefault() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let latitude = 4.0
        let longitude = 2.0
         dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()
        // then
        XCTAssert(dummyView.latitude == latitude)
        XCTAssert(dummyView.longitude == longitude)
        XCTAssert(dummyView.radiousInMetersSent == Double(sut.defaultDistance * DistanceConstants.meters_to_kilometers))

    }

    func test_givenNotCategoryBONeitherTrendingAndLastLocationRetrieveNil_whenICallViewWillAppearAndObtainedLocation_thenICallInteractorProvidePromotions() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.categoryBO = nil
        sut.trendingType = nil
        sut.lastLocationRetrieve = nil

        let latitude = 4.0
        let longitude = 2.0
         dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()
        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotions == true)
    }

    func test_givenNotCategoryBONeitherTrending_whenICallViewWillAppearAndObtainedLocation_thenLastLocationRetrieveShouldBeFilled() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        dummyInteractor.presenter = sut

        // given
        sut.categoryBO = nil
        sut.trendingType = nil
        sut.lastLocationRetrieve = nil

        let latitude = 4.0
        let longitude = 2.0
         dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()
        // then
        XCTAssert(sut.lastLocationRetrieve != nil)
    }

    func test_givenNotCategoryBONeitherTrending_whenICallViewWillAppearAndObtainedLocation_thenLastLocationRetrieveShouldBeLatitudeAndLongitudeSent() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        dummyInteractor.presenter = sut

        // given
        sut.categoryBO = nil
        sut.trendingType = nil
        sut.lastLocationRetrieve = nil

        let latitude = 4.0
        let longitude = 2.0
         dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()
        // then
        XCTAssert(sut.lastLocationRetrieve?.latitude == latitude)
        XCTAssert(sut.lastLocationRetrieve?.longitude == longitude)
    }

    func test_givenLastLocatinRetrieveNotCategoryBONeitherTrending_whenICallObtainedLocationWithDistanceLessThanHalftDefaultDistance_thenICallInteractorProvidePromotions() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.categoryBO = nil
        sut.trendingType = nil
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 40.424188, withLongitude: -3.695200)

        let latitude = 40.426834
        let longitude = -3.692153
         dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()
        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotions == false)
    }

    func test_givenNotCategoryBONeitherTrending_whenICallViewWillAppearAndObtainedLocation_thenICallShowLoader() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        dummyInteractor.presenter = sut

        // given
        sut.categoryBO = nil
        sut.trendingType = nil
        sut.lastLocationRetrieve = nil

        let latitude = 4.0
        let longitude = 2.0
         dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()
        // then
        XCTAssert(dummyView.isCalledShowLoader == true)
    }

    func test_givenLastLocationRetrieveNotCategoryBONeitherTrending_whenICallObtainedLocationLessThanHalftDefaultDistance_thenIDoNotCallShowLoader() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.categoryBO = nil
        sut.trendingType = nil
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 40.424188, withLongitude: -3.695200)

        let latitude = 40.426834
        let longitude = -3.692153
         dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()
        // then
        XCTAssert(dummyView.isCalledShowLoader == false)
    }

    func test_givenNotCategoryBONeitherTrending_whenICallViewWillAppearAndObtainedLocation_thenICallInteractorProvidePromotionsWithLatitudeAndLongitudeAsParameterAndDistanceAndTypeLengthDefault() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.categoryBO = nil
        sut.trendingType = nil
        sut.lastLocationRetrieve = nil

        let latitude = 4.0
        let longitude = 2.0
         dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()
        // then
        XCTAssert(dummyInteractor.latitudeSent == latitude)
        XCTAssert(dummyInteractor.longitudeSent == longitude)
        XCTAssert(dummyInteractor.distanceSent == sut.defaultDistance)
        XCTAssert(dummyInteractor.lengthTypeSent == sut.lengthType)
        XCTAssert(dummyInteractor.pageSizeSent == sut.pageSize)
        XCTAssert(dummyInteractor.categoryIdSent == nil)
        XCTAssert(dummyInteractor.trendingIdSent == nil)

    }

    func test_givenWithCategoryBO_whenICallViewWillAppearAndObtainedLocation_thenICallInteractorProvidePromotionsWithLatitudeAndLongitudeAsParameterAndDistanceAndTypeLengthDefaultAndCategoryId() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let idType = CategoryEntity(id: "ACTIVITIES", name: "actividades", isFavourite: false)
        sut.categoryBO = CategoryBO(categoryEntity: idType)
        sut.trendingType = nil
        sut.lastLocationRetrieve = nil

        let latitude = 4.0
        let longitude = 2.0
         dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()
        // then
        XCTAssert(dummyInteractor.latitudeSent == latitude)
        XCTAssert(dummyInteractor.longitudeSent == longitude)
        XCTAssert(dummyInteractor.distanceSent == sut.defaultDistance)
        XCTAssert(dummyInteractor.lengthTypeSent == sut.lengthType)
        XCTAssert(dummyInteractor.pageSizeSent == sut.pageSize)
        XCTAssert(dummyInteractor.categoryIdSent == sut.categoryBO?.id.rawValue)
        XCTAssert(dummyInteractor.trendingIdSent == nil)

    }

    func test_givenWithTrendingType_whenICallViewWillAppearAndObtainedLocation_thenICallInteractorProvidePromotionsWithLatitudeAndLongitudeAsParameterAndDistanceAndTypeLengthDefaultAndTrendingId() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.categoryBO = nil
        sut.trendingType = TrendingType(rawValue: "HOT")
        sut.lastLocationRetrieve = nil

        let latitude = 4.0
        let longitude = 2.0
        dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyInteractor.latitudeSent == latitude)
        XCTAssert(dummyInteractor.longitudeSent == longitude)
        XCTAssert(dummyInteractor.distanceSent == sut.defaultDistance)
        XCTAssert(dummyInteractor.lengthTypeSent == sut.lengthType)
        XCTAssert(dummyInteractor.pageSizeSent == sut.pageSize)
        XCTAssert(dummyInteractor.categoryIdSent == nil)
        XCTAssert(dummyInteractor.trendingIdSent == sut.trendingType?.rawValue)

    }

    func test_givenNotCategoryBONeitherTrending_whenICallViewWillAppearAndObtainedLocation_thenErrorBOShouldBeFilled() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let errorEntity = ErrorEntity()
        errorEntity.code = "500"
        errorEntity.message = "Error 500"

        dummyInteractor.forceError = true
        dummyInteractor.errorBO = ErrorBO(error: errorEntity)

        // given
        sut.categoryBO = nil
        sut.trendingType = nil
        sut.lastLocationRetrieve = nil

        let latitude = 4.0
        let longitude = 2.0
         dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()
        // then
        XCTAssert(sut.errorBO != nil)
        XCTAssert(sut.errorBO! === dummyInteractor.errorBO!)
    }

    func test_givenNotCategoryBONeitherTrending_whenICallViewWillAppearAndObtainedLocation_thenICallHideLoader() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let errorEntity = ErrorEntity()
        errorEntity.code = "500"
        errorEntity.message = "Error 500"

        dummyInteractor.forceError = true
        dummyInteractor.errorBO = ErrorBO(error: errorEntity)

        // given
        sut.categoryBO = nil
        sut.trendingType = nil
        sut.lastLocationRetrieve = nil

        let latitude = 4.0
        let longitude = 2.0
         dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()
        // then
        XCTAssert(dummyView.isCalledHideLoader == true)
    }

    func test_givenNotCategoryBONeitherTrending_whenICallViewWillAppearAndObtainedLocation_thenICallHidePromotions() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let errorEntity = ErrorEntity()
        errorEntity.code = "500"
        errorEntity.message = "Error 500"

        dummyInteractor.forceError = true
        dummyInteractor.errorBO = ErrorBO(error: errorEntity)

        // given
        sut.categoryBO = nil
        sut.trendingType = nil
        sut.lastLocationRetrieve = nil

        let latitude = 4.0
        let longitude = 2.0
         dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()
        // then
        XCTAssert(dummyView.isCalledHidePromotions == true)
    }

    func test_givenNotCategoryBONeitherTrending_whenICallObtainedLocationAndInteractorReturnError_thenICallViewShowError() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let errorEntity = ErrorEntity()
        errorEntity.code = "500"
        errorEntity.message = "Error 500"

        dummyInteractor.forceError = true
        dummyInteractor.errorBO = ErrorBO(error: errorEntity)

        // given
        sut.categoryBO = nil
        sut.trendingType = nil
        sut.lastLocationRetrieve = nil

        let latitude = 4.0
        let longitude = 2.0
         dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()
        // then
        XCTAssert(dummyView.isCalledShowError == true)
    }

    func test_givenNotCategoryBONeitherTrending_whenICallObtainedLocationAndInteractorReturnSuccess_thenDataPoisShouldBeFilled() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.categoryBO = nil
        sut.trendingType = nil
        sut.lastLocationRetrieve = nil

        let latitude = 4.0
        let longitude = 2.0
         dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()
        // then
        XCTAssert(sut.dataPois.isEmpty == false)
    }

    func test_givenNotCategoryBONeitherTrending_whenICallObtainedLocationAndInteractorReturnSuccess_thenICallHideLoader() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.categoryBO = nil
        sut.trendingType = nil
        sut.lastLocationRetrieve = nil

        let latitude = 4.0
        let longitude = 2.0
         dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()
        // then
        XCTAssert(dummyView.isCalledHideLoader == true)
    }

    func test_givenNotCategoryBONeitherTrending_whenICallObtainedLocationAndInteractorReturnSuccess_thenICallShowStores() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.categoryBO = nil
        sut.trendingType = nil
        sut.lastLocationRetrieve = nil

        let latitude = 4.0
        let longitude = 2.0
         dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()
        // then
        XCTAssert(dummyView.isCalledShowStores == true)
    }

    func test_givenNotCategoryBONeitherTrending_whenICallObtainedLocationAndInteractorReturnSuccess_thenICallHidePromotions() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.categoryBO = nil
        sut.trendingType = nil
        sut.lastLocationRetrieve = nil

        let latitude = 4.0
        let longitude = 2.0
         dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()
        // then
        XCTAssert(dummyView.isCalledHidePromotions == true)
    }

    func test_givenNotCategoryBONeitherTrending_whenICallObtainedLocationAndInteractorReturnSuccess_thenICallShowStoresWithTheSameNumberOfItemsAsStoresReturned() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        let promotions = PromotionsGenerator.getPromotionsBO()

        for promotion in promotions.promotions {
            promotion.promotionType.id = .discount
        }

        dummyInteractor.promotionsBO = promotions

        // given
        sut.categoryBO = nil
        sut.trendingType = nil
        sut.lastLocationRetrieve = nil

        let latitude = 4.0
        let longitude = 2.0
         dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()
        // then

        var numberOfStoresReturned = 0

        for promotion in dummyInteractor.promotionsBO!.promotions {
            numberOfStoresReturned += (promotion.stores?.count ?? 0)
        }

        XCTAssert(dummyView.displayDataSent!.items.count == numberOfStoresReturned)
    }

    func test_givenADisplayDataMapWithNotCategoryBONeitherTrending_whenICallObtainedLocationAndInteractorReturnSuccess_thenICallShowStoresWithTheSameNumberOfItemsAsStoresReturned() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        let promotions = PromotionsGenerator.getPromotionsBO()

        for promotion in promotions.promotions {
            promotion.promotionType.id = .discount
        }

        dummyInteractor.promotionsBO = promotions

        let dataPoi = DataPoi(latitude: 3, longitude: 4, storeId: promotions.promotions[0].stores![0].id)
        dataPoi.append(promotion: promotions.promotions[0])

        let displayDataMap = MapDisplayData()
        displayDataMap.items.append(MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)!)

        // given
        sut.categoryBO = nil
        sut.trendingType = nil
        sut.displayData = displayDataMap
        sut.lastLocationRetrieve = nil

        let latitude = 4.0
        let longitude = 2.0
         dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()
        // then

        var numberOfStoresReturned = 0

        for promotion in dummyInteractor.promotionsBO!.promotions {
            numberOfStoresReturned += (promotion.stores?.count ?? 0)
        }

        XCTAssert(dummyView.displayDataSent!.items.count == numberOfStoresReturned)
    }

    func test_givenWithCategoryBO_whenICallObtainedLocationAndInteractorReturnSuccess_thenICallShowStores() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let idType = CategoryEntity(id: "ACTIVITIES", name: "actividades", isFavourite: false)
        sut.categoryBO = CategoryBO(categoryEntity: idType)
        sut.trendingType = nil
        sut.lastLocationRetrieve = nil

        let latitude = 4.0
        let longitude = 2.0
         dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()
        // then
        XCTAssert(dummyView.isCalledShowStores == true)
    }

    func test_givenWithCategoryBO_whenICallObtainedLocationAndInteractorReturnSuccess_thenICallHidePromotions() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let idType = CategoryEntity(id: "ACTIVITIES", name: "actividades", isFavourite: false)
        sut.categoryBO = CategoryBO(categoryEntity: idType)
        sut.trendingType = nil
        sut.lastLocationRetrieve = nil

        let latitude = 4.0
        let longitude = 2.0
         dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()
        // then
        XCTAssert(dummyView.isCalledHidePromotions == true)
    }

    func test_giveWithCategoryBO_whenICallObtainedLocationAndInteractorReturnSuccess_thenICallShowStoresWithTheSameNumberOfItemsAsStoresReturned() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        let promotions = PromotionsGenerator.getPromotionsBO()

        for promotion in promotions.promotions {
            promotion.promotionType.id = .discount
        }

        dummyInteractor.promotionsBO = promotions

        // given
        let idType = CategoryEntity(id: "ACTIVITIES", name: "actividades", isFavourite: false)
        sut.categoryBO = CategoryBO(categoryEntity: idType)
        sut.trendingType = nil
        sut.lastLocationRetrieve = nil

        let latitude = 4.0
        let longitude = 2.0
         dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()
        // then

        var numberOfStoresReturned = 0

        for promotion in dummyInteractor.promotionsBO!.promotions {
            numberOfStoresReturned += (promotion.stores?.count ?? 0)
        }

        XCTAssert(dummyView.displayDataSent!.items.count == numberOfStoresReturned)
    }

    func test_givenADisplayDataMapWithCategoryBO_whenICallObtainedLocationAndInteractorReturnSuccess_thenICallShowStoresWithTheSameNumberOfItemsAsStoresReturned() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        let promotions = PromotionsGenerator.getPromotionsBO()

        for promotion in promotions.promotions {
            promotion.promotionType.id = .discount
        }

        dummyInteractor.promotionsBO = promotions

        let dataPoi = DataPoi(latitude: 3, longitude: 4, storeId: promotions.promotions[0].stores![0].id)
        dataPoi.append(promotion: promotions.promotions[0])

        let displayDataMap = MapDisplayData()
        displayDataMap.items.append(MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)!)

        // given
        let idType = CategoryEntity(id: "ACTIVITIES", name: "actividades", isFavourite: false)
        sut.categoryBO = CategoryBO(categoryEntity: idType)
        sut.trendingType = nil
        sut.displayData = displayDataMap
        sut.lastLocationRetrieve = nil

        let latitude = 4.0
        let longitude = 2.0
         dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()
        // then

        var numberOfStoresReturned = 0

        for promotion in dummyInteractor.promotionsBO!.promotions {
            numberOfStoresReturned += (promotion.stores?.count ?? 0)
        }

        XCTAssert(dummyView.displayDataSent!.items.count == numberOfStoresReturned)
    }

    func test_givenWithTrendingType_whenICallObtainedLocationAndInteractorReturnSuccess_thenICallShowStores() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.categoryBO = nil
        sut.trendingType = TrendingType(rawValue: "HOT")
        sut.lastLocationRetrieve = nil

        let latitude = 4.0
        let longitude = 2.0
         dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()
        // then
        XCTAssert(dummyView.isCalledShowStores == true)
    }

    func test_givenWithTrendingType_whenICallObtainedLocationAndInteractorReturnSuccess_thenICallHidePromotions() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.categoryBO = nil
        sut.trendingType = TrendingType(rawValue: "HOT")
        sut.lastLocationRetrieve = nil

        let latitude = 4.0
        let longitude = 2.0
         dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()
        // then
        XCTAssert(dummyView.isCalledHidePromotions == true)
    }

    func test_giveWithTrendingType_whenICallObtainedLocationAndInteractorReturnSuccess_thenICallShowStoresWithTheSameNumberOfItemsAsStoresReturned() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        let promotions = PromotionsGenerator.getPromotionsBO()

        for promotion in promotions.promotions {
            promotion.promotionType.id = .discount
        }

        dummyInteractor.promotionsBO = promotions

        // given
        sut.categoryBO = nil
        sut.trendingType = TrendingType(rawValue: "HOT")
        sut.lastLocationRetrieve = nil

        let latitude = 4.0
        let longitude = 2.0
         dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()
        // then

        var numberOfStoresReturned = 0

        for promotion in dummyInteractor.promotionsBO!.promotions {
            numberOfStoresReturned += (promotion.stores?.count ?? 0)
        }

        XCTAssert(dummyView.displayDataSent!.items.count == numberOfStoresReturned)
    }

    func test_givenADisplayDataMapWithTrendingType_whenICallObtainedLocationAndInteractorReturnSuccess_thenICallShowStoresWithTheSameNumberOfItemsAsStoresReturned() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        let promotions = PromotionsGenerator.getPromotionsBO()

        for promotion in promotions.promotions {
            promotion.promotionType.id = .discount
        }

        dummyInteractor.promotionsBO = promotions

        let dataPoi = DataPoi(latitude: 3, longitude: 4, storeId: promotions.promotions[0].stores![0].id)
        dataPoi.append(promotion: promotions.promotions[0])

        let displayDataMap = MapDisplayData()
        displayDataMap.items.append(MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)!)

        // given
        sut.categoryBO = nil
        sut.trendingType = TrendingType(rawValue: "HOT")
        sut.displayData = displayDataMap
        sut.lastLocationRetrieve = nil

        let latitude = 4.0
        let longitude = 2.0
         dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()
        // then

        var numberOfStoresReturned = 0

        for promotion in dummyInteractor.promotionsBO!.promotions {
            numberOfStoresReturned += (promotion.stores?.count ?? 0)
        }

        XCTAssert(dummyView.displayDataSent!.items.count == numberOfStoresReturned)
    }

    func test_givenAny_whenICallObtainedLocationAndInteractorReturnPromotionsWithStores_thenIDoNotCallShowToast() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let latitude = 4.0
        let longitude = 2.0
         dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()
        // then
        XCTAssert(dummyView.isCalledShowToastNotStore == false)

    }

    func test_givenCategoryBO_whenICallObtainedLocationAndInteractorReturnPromotionsWithStores_thenIDoNotCallShowToast() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let latitude = 4.0
        let longitude = 2.0

        let idType = CategoryEntity(id: "ACTIVITIES", name: "actividades", isFavourite: false)
        sut.categoryBO = CategoryBO(categoryEntity: idType)
         dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()
        // then
        XCTAssert(dummyView.isCalledShowToastNotStore == false)

    }

    func test_givenTrendingBO_whenICallObtainedLocationAndInteractorReturnPromotionsWithStores_thenIDoNotCallShowToast() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let latitude = 4.0
        let longitude = 2.0

        sut.trendingType = TrendingType(rawValue: "HOT")

         dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()
        // then
        XCTAssert(dummyView.isCalledShowToastNotStore == false)

    }

    func test_givenAny_whenICallObtainedLocationAndInteractorReturnPromotionsWithoutStores_thenICallShowToast() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let latitude = 4.0
        let longitude = 2.0

        // given
        let promotions = PromotionsGenerator.getPromotionsBO()

        for promotion in promotions.promotions {

            promotion.stores = nil

        }

        dummyInteractor.promotionsBO = promotions

         dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()
        // then
        XCTAssert(dummyView.isCalledShowToastNotStore == true)

    }

    func test_givenCategoryBO_whenICallObtainedLocationAndInteractorReturnPromotionsWithoutStores_thenICallShowToast() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let latitude = 4.0
        let longitude = 2.0

        // given
        let idType = CategoryEntity(id: "ACTIVITIES", name: "actividades", isFavourite: false)
        sut.categoryBO = CategoryBO(categoryEntity: idType)

        let promotions = PromotionsGenerator.getPromotionsBO()

        for promotion in promotions.promotions {

            promotion.stores = nil

        }

        dummyInteractor.promotionsBO = promotions

         dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()
        // then
        XCTAssert(dummyView.isCalledShowToastNotStore == true)

    }

    func test_givenTrendingBO_whenICallObtainedLocationAndInteractorReturnPromotionsWithoutStores_thenICallShowToast() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let latitude = 4.0
        let longitude = 2.0

        // given
        sut.trendingType = TrendingType(rawValue: "HOT")

        let promotions = PromotionsGenerator.getPromotionsBO()

        for promotion in promotions.promotions {

            promotion.stores = nil

        }

        dummyInteractor.promotionsBO = promotions

         dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()
        // then
        XCTAssert(dummyView.isCalledShowToastNotStore == true)

    }

    // MARK: - didTapInStore

    func test_givenMapDisplayDataItemAndDataPoisEmpty_whenICallDidTapInStore_thenIDoNotCallShowPromotions() {

        let dummyView = DummyView()
        sut.view = dummyView

        let promotion = PromotionsGenerator.getPromotionsBO().promotions[0]
        promotion.promotionType.id = .discount

        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        dataPoi.append(promotion: promotion)

        // given
        let displayDataMapItem = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)!
        sut.dataPois = [DataPoi]()

        // when
        sut.didTapInStore(withDisplayData: displayDataMapItem)

        // then
        XCTAssert(dummyView.isCallediShowPromotions == false)

    }

    func test_givenMapDisplayDataItemAndDataPoisThatNotMatchWithLatitudeAndLongitudeOfMapDisplayDataItem_whenICallDidTapInStore_thenICallShowPromotions() {

        let dummyView = DummyView()
        sut.view = dummyView

        let promotion = PromotionsGenerator.getPromotionsBO().promotions[0]
        promotion.promotionType.id = .discount

        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        dataPoi.append(promotion: promotion)

        // given
        let displayDataMapItem = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)!
        sut.dataPois = [DataPoi(latitude: 5.0, longitude: 8.0, storeId: "33")]

        // when
        sut.didTapInStore(withDisplayData: displayDataMapItem)

        // then
        XCTAssert(dummyView.isCallediShowPromotions == true)

    }

    func test_givenMapDisplayDataItemAndDataPoisThatNotMatchWithLatitudeAndLongitudeOfMapDisplayDataItem_whenICallDidTapInStore_thenICallShowPromotionsWithEmptyDisplayData() {

        let dummyView = DummyView()
        sut.view = dummyView

        let promotion = PromotionsGenerator.getPromotionsBO().promotions[0]
        promotion.promotionType.id = .discount

        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        dataPoi.append(promotion: promotion)

        // given
        let displayDataMapItem = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)!
        sut.dataPois = [DataPoi(latitude: 5.0, longitude: 8.0, storeId: "33")]

        // when
        sut.didTapInStore(withDisplayData: displayDataMapItem)

        // then
        XCTAssert(dummyView.sentDisplayItemsPromotions?.itemsToShow.isEmpty == true)

    }

    func test_givenMapDisplayDataItemAndDataPoisWithoutPromotionsThatMatchWithLatitudeAndLongitudeOfMapDisplayDataItem_whenICallDidTapInStore_thenICallShowPromotions() {

        let dummyView = DummyView()
        sut.view = dummyView

        let promotion = PromotionsGenerator.getPromotionsBO().promotions[0]
        promotion.promotionType.id = .discount

        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        dataPoi.append(promotion: promotion)

        // given
        let displayDataMapItem = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)!
        sut.dataPois = [DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")]

        // when
        sut.didTapInStore(withDisplayData: displayDataMapItem)

        // then
        XCTAssert(dummyView.isCallediShowPromotions == true)

    }

    func test_givenMapDisplayDataItemAndDataPoisWithoutPromtionsThatMatchWithLatitudeAndLongitudeOfMapDisplayDataItem_whenICallDidTapInStore_thenICallShowPromotionsWithEmptyDisplayData() {

        let dummyView = DummyView()
        sut.view = dummyView

        let promotion = PromotionsGenerator.getPromotionsBO().promotions[0]
        promotion.promotionType.id = .discount

        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        dataPoi.append(promotion: promotion)

        // given
        let displayDataMapItem = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)!
        sut.dataPois = [DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")]

        // when
        sut.didTapInStore(withDisplayData: displayDataMapItem)

        // then
        XCTAssert(dummyView.sentDisplayItemsPromotions?.itemsToShow.isEmpty == true)

    }

    func test_givenMapDisplayDataItemAndDataPoisWithtPromtionsThatMatchWithLatitudeAndLongitudeOfMapDisplayDataItem_whenICallDidTapInStore_thenICallShowPromotions() {

        let dummyView = DummyView()
        sut.view = dummyView

        let promotion = PromotionsGenerator.getPromotionsBO().promotions[0]
        promotion.promotionType.id = .discount

        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "4410")
        dataPoi.append(promotion: promotion)

        // given
        let displayDataMapItem = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)!
        sut.dataPois = [dataPoi]

        // when
        sut.didTapInStore(withDisplayData: displayDataMapItem)

        // then
        XCTAssert(dummyView.isCallediShowPromotions == true)

    }

    func test_givenMapDisplayDataItemAndDataPoisWithOnePromtionThatMatchWithLatitudeAndLongitudeOfMapDisplayDataItem_whenICallDidTapInStore_thenICallShowPromotionsWithOneDisplayData() {

        let dummyView = DummyView()
        sut.view = dummyView

        let promotion = PromotionsGenerator.getPromotionsBO().promotions[0]
        promotion.promotionType.id = .discount

        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "4410")
        dataPoi.append(promotion: promotion)

        // given
        let displayDataMapItem = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)!
        sut.dataPois = [dataPoi]

        // when
        sut.didTapInStore(withDisplayData: displayDataMapItem)

        // then
        XCTAssert(dummyView.sentDisplayItemsPromotions?.itemsToShow.count == 1)

    }

    func test_givenMapDisplayDataItemAndMaxPromotionsInCarrouselAndDataPoisWithPromotionsThatMatchWithLatitudeAndLongitudeOfMapDisplayDataItemButLessNumberOfPromotionsThanMaxPromotionsInCarrousel_whenICallDidTapInStore_thenICallShowPromotionsWithTheNumberOfItemsThatMatchWithPromotionsInDataPoi() {

        let dummyView = DummyView()
        sut.view = dummyView

        let promotion = PromotionsGenerator.getPromotionsBOWithStores().promotions[0]
        promotion.promotionType.id = .discount

        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "4410")
        dataPoi.append(promotion: promotion)
        dataPoi.append(promotion: PromotionsGenerator.getPromotionsBOWithStores().promotions[1])
        dataPoi.append(promotion: PromotionsGenerator.getPromotionsBOWithStores().promotions[2])

        // given
        let displayDataMapItem = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)!
        sut.dataPois = [dataPoi]
        sut.maxPromotionsInCarrousel = 4

        // when
        sut.didTapInStore(withDisplayData: displayDataMapItem)

        // then
        XCTAssert(dummyView.sentDisplayItemsPromotions?.itemsToShow.count == dataPoi.promotions.count)

    }

    func test_givenMapDisplayDataItemAndMaxPromotionsInCarrouselAndDataPoisWithPromotionsThatMatchWithLatitudeAndLongitudeOfMapDisplayDataItemButLessNumberOfPromotionsThanMaxPromotionsInCarrousel_whenICallDidTapInStore_thenICallShowPromotionsWithShouldShowMorePromotionFalse() {

        let dummyView = DummyView()
        sut.view = dummyView

        let promotion = PromotionsGenerator.getPromotionsBOWithStores().promotions[0]
        promotion.promotionType.id = .discount

        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "4410")
        dataPoi.append(promotion: promotion)
        dataPoi.append(promotion: PromotionsGenerator.getPromotionsBOWithStores().promotions[1])
        dataPoi.append(promotion: PromotionsGenerator.getPromotionsBOWithStores().promotions[2])

        // given
        let displayDataMapItem = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)!
        sut.dataPois = [dataPoi]
        sut.maxPromotionsInCarrousel = 4

        // when
        sut.didTapInStore(withDisplayData: displayDataMapItem)

        // then
        XCTAssert(dummyView.sentDisplayItemsPromotions?.shouldShowMorePromotions == false)

    }

    func test_givenMapDisplayDataItemAndMaxPromotionsInCarrouselAndDataPoisWithPromotionsThatMatchWithLatitudeAndLongitudeOfMapDisplayDataItemButEqualsNumberOfPromotionsThanMaxPromotionsInCarrousel_whenICallDidTapInStore_thenICallShowPromotionsWithShouldShowMorePromotionFalse() {

        let dummyView = DummyView()
        sut.view = dummyView

        let promotion = PromotionsGenerator.getPromotionsBOWithStores().promotions[0]
        promotion.promotionType.id = .discount

        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "4410")
        dataPoi.append(promotion: promotion)
        dataPoi.append(promotion: PromotionsGenerator.getPromotionsBOWithStores().promotions[1])
        dataPoi.append(promotion: PromotionsGenerator.getPromotionsBOWithStores().promotions[2])
        dataPoi.append(promotion: PromotionsGenerator.getPromotionsBOWithStores().promotions[3])

        // given
        let displayDataMapItem = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)!
        sut.dataPois = [dataPoi]
        sut.maxPromotionsInCarrousel = 4

        // when
        sut.didTapInStore(withDisplayData: displayDataMapItem)

        // then
        XCTAssert(dummyView.sentDisplayItemsPromotions?.shouldShowMorePromotions == false)

    }

    func test_givenMapDisplayDataItemAndMaxPromotionsInCarrouselAndDataPoisWithPromotionsThatMatchWithLatitudeAndLongitudeOfMapDisplayDataItemButMoreNumberOfPromotionsThanMaxPromotionsInCarrousel_whenICallDidTapInStore_thenICallShowPromotionsWithTheNumberOfItemsAsMaxPromotionsInCarrousel() {

        let dummyView = DummyView()
        sut.view = dummyView

        let promotion = PromotionsGenerator.getPromotionsBOWithStores().promotions[0]
        promotion.promotionType.id = .discount

        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "4410")
        dataPoi.append(promotion: promotion)
        dataPoi.append(promotion: PromotionsGenerator.getPromotionsBOWithStores().promotions[1])
        dataPoi.append(promotion: PromotionsGenerator.getPromotionsBOWithStores().promotions[2])
        dataPoi.append(promotion: PromotionsGenerator.getPromotionsBOWithStores().promotions[3])
        dataPoi.append(promotion: PromotionsGenerator.getPromotionsBOWithStores().promotions[4])

        // given
        let displayDataMapItem = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)!
        sut.dataPois = [dataPoi]
        sut.maxPromotionsInCarrousel = 4

        // when
        sut.didTapInStore(withDisplayData: displayDataMapItem)

        // then
        XCTAssert(dummyView.sentDisplayItemsPromotions?.itemsToShow.count == sut.maxPromotionsInCarrousel)

    }

    func test_givenMapDisplayDataItemAndDataPoisNotEmtpy_whenICallDidTapInStore_thenICallShowPromotionsWithTotalItemsCountEqualsToPromotionsInLatitudeLongitude() {

        let dummyView = DummyView()
        sut.view = dummyView

        let promotion = PromotionsGenerator.getPromotionsBOWithStores().promotions[0]
        promotion.promotionType.id = .discount

        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "4410")
        dataPoi.append(promotion: promotion)
        dataPoi.append(promotion: PromotionsGenerator.getPromotionsBOWithStores().promotions[1])
        dataPoi.append(promotion: PromotionsGenerator.getPromotionsBOWithStores().promotions[2])
        dataPoi.append(promotion: PromotionsGenerator.getPromotionsBOWithStores().promotions[3])
        dataPoi.append(promotion: PromotionsGenerator.getPromotionsBOWithStores().promotions[4])
        dataPoi.append(promotion: PromotionsGenerator.getPromotionsBOWithStores().promotions[5])

        // given
        let displayDataMapItem = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)!
        sut.dataPois = [dataPoi]
        sut.maxPromotionsInCarrousel = 4

        // when
        sut.didTapInStore(withDisplayData: displayDataMapItem)

        // then
        XCTAssert(dummyView.sentDisplayItemsPromotions?.totalItemsCount == sut.dataPois[0].promotions.count)

    }

    func test_givenMapDisplayDataItemAndMaxPromotionsInCarrouselAndDataPoisWithPromotionsThatMatchWithLatitudeAndLongitudeOfMapDisplayDataItemButMoreNumberOfPromotionsThanMaxPromotionsInCarrousel_whenICallDidTapInStore_thenICallShowPromotionsWithShouldShowMorePromotionTrue() {

        let dummyView = DummyView()
        sut.view = dummyView

        let promotion = PromotionsGenerator.getPromotionsBOWithStores().promotions[0]
        promotion.promotionType.id = .discount

        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "4410")
        dataPoi.append(promotion: promotion)
        dataPoi.append(promotion: PromotionsGenerator.getPromotionsBOWithStores().promotions[1])
        dataPoi.append(promotion: PromotionsGenerator.getPromotionsBOWithStores().promotions[2])
        dataPoi.append(promotion: PromotionsGenerator.getPromotionsBOWithStores().promotions[3])
        dataPoi.append(promotion: PromotionsGenerator.getPromotionsBOWithStores().promotions[4])

        // given
        let displayDataMapItem = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)!
        sut.dataPois = [dataPoi]
        sut.maxPromotionsInCarrousel = 4

        // when
        sut.didTapInStore(withDisplayData: displayDataMapItem)

        // then
        XCTAssert(dummyView.sentDisplayItemsPromotions?.shouldShowMorePromotions == true)

    }

    func test_givenMapDisplayDataItemAndDataPoisNotEmpty_whenICallDidTapInStore_thenICallShowHideUserLocation() {

        let dummyView = DummyView()
        sut.view = dummyView

        let promotion = PromotionsGenerator.getPromotionsBO().promotions[0]
        promotion.promotionType.id = .discount

        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "4410")
        dataPoi.append(promotion: promotion)

        // given
        let displayDataMapItem = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)!
        sut.dataPois = [dataPoi]

        // when
        sut.didTapInStore(withDisplayData: displayDataMapItem)

        // then
        XCTAssert(dummyView.isCalledHideUserLocation == true)

    }

    // MARK: - didTapInMap

    func test_givenAny_whenICallDidTapInMap_thenIHidePromotions() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given

        // when
        sut.didTapInMap()

        // then
        XCTAssert(dummyView.isCalledHidePromotions == true)

    }

    func test_givenUserLatitudeAndLongitude_whenICallDidTapInMap_thenICallShowUserLocation() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.userLocation = GeoLocationBO(withLatitude: 33.00, withLongitude: 44.00)

        // when
        sut.didTapInMap()

        // then
        XCTAssert(dummyView.isCalledShowUserLocation == true)

    }

    func test_givenNotUserLatitudeAndLongitude_whenICallDidTapInMap_thenIDoNotCallShowUserLocation() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given

        // when
        sut.didTapInMap()

        // then
        XCTAssert(dummyView.isCalledShowUserLocation == false)

    }

    // MARK: - movePosition

    func test_givenLastLocationAndLatitudeAndLongitudeLessThanHalfDefaultDistance_whenICallMovePosition_thenIDoNotCallInteractorProvidePromotions() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 40.424188, withLongitude: -3.695200)

        // when
        sut.movePosition(withLatitude: 40.426834, andLongitude: -3.692153)

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotions == false)

    }

    func test_givenNotCategoryBONeitherTrendingAndLastLocationAndLatitudeAndLongitudeMoreThanHalfDefaultDistance_whenICallMovePosition_thenICallInteractorProvidePromotions() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 40.424188, withLongitude: -3.695200)
        sut.categoryBO = nil
        sut.trendingType = nil

        // when
        sut.movePosition(withLatitude: 40.432109, andLongitude: -3.684180)

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotions == true)

    }

    func test_givenNotCategoryBONeitherTrendingAndLastLocationAndLatitudeAndLongitudeMoreThanHalfDefaultDistance_whenICallMovePosition_thenLastLocationRetrieveShouldBeFilled() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        dummyInteractor.presenter = sut

        // given
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 40.424188, withLongitude: -3.695200)
        sut.categoryBO = nil
        sut.trendingType = nil

        // when
        sut.movePosition(withLatitude: 40.432109, andLongitude: -3.684180)

        // then
        XCTAssert(sut.lastLocationRetrieve != nil)
    }

    func test_givenNotCategoryBONeitherTrendingAndLastLocationAndLatitudeAndLongitudeMoreThanHalfDefaultDistance_whenICallMovePosition_thenLastLocationRetrieveShouldBeLatitudeAndLongitudeSent() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        dummyInteractor.presenter = sut

        // given
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 40.424188, withLongitude: -3.695200)
        sut.categoryBO = nil
        sut.trendingType = nil

        let latitude = 40.432109
        let longitude = -3.684180

        // when
        sut.movePosition(withLatitude: latitude, andLongitude: longitude)

        // then
        XCTAssert(sut.lastLocationRetrieve?.latitude == latitude)
        XCTAssert(sut.lastLocationRetrieve?.longitude == longitude)
    }

    func test_givenNotCategoryBONeitherTrendingAndLastLocationAndLatitudeAndLongitudeMoreThanHalfDefaultDistance_whenICallMovePosition_thenICallShowLoader() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        dummyInteractor.presenter = sut

        // given
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 40.424188, withLongitude: -3.695200)
        sut.categoryBO = nil
        sut.trendingType = nil

        let latitude = 40.432109
        let longitude = -3.684180

        // when
        sut.movePosition(withLatitude: latitude, andLongitude: longitude)

        // then
        XCTAssert(dummyView.isCalledShowLoader == true)
    }

    func test_givenNotCategoryBONeitherTrendingAndLastLocationAndLatitudeAndLongitudeMoreThanHalfDefaultDistance_whenICallMovePosition_thenICallInteractorProvidePromotionsWithLatitudeAndLongitudeAsParameterAndDistanceAndTypeLengthDefault() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 40.424188, withLongitude: -3.695200)
        sut.categoryBO = nil
        sut.trendingType = nil

        let latitude = 40.432109
        let longitude = -3.684180

        // when
        sut.movePosition(withLatitude: latitude, andLongitude: longitude)

        // then
        XCTAssert(dummyInteractor.latitudeSent == latitude)
        XCTAssert(dummyInteractor.longitudeSent == longitude)
        XCTAssert(dummyInteractor.distanceSent == sut.defaultDistance)
        XCTAssert(dummyInteractor.lengthTypeSent == sut.lengthType)
        XCTAssert(dummyInteractor.pageSizeSent == sut.pageSize)
        XCTAssert(dummyInteractor.categoryIdSent == nil)
        XCTAssert(dummyInteractor.trendingIdSent == nil)

    }

    func test_givenWithCategoryBOAndLastLocationAndLatitudeAndLongitudeMoreThanHalfDefaultDistance_whenICallMovePosition_thenICallInteractorProvidePromotionsWithLatitudeAndLongitudeAsParameterAndDistanceAndTypeLengthDefaultAndCategoryId() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 40.424188, withLongitude: -3.695200)
        let idType = CategoryEntity(id: "ACTIVITIES", name: "actividades", isFavourite: false)
        sut.categoryBO = CategoryBO(categoryEntity: idType)
        sut.trendingType = nil

        let latitude = 40.432109
        let longitude = -3.684180

        // when
        sut.movePosition(withLatitude: latitude, andLongitude: longitude)

        // then
        XCTAssert(dummyInteractor.latitudeSent == latitude)
        XCTAssert(dummyInteractor.longitudeSent == longitude)
        XCTAssert(dummyInteractor.distanceSent == sut.defaultDistance)
        XCTAssert(dummyInteractor.lengthTypeSent == sut.lengthType)
        XCTAssert(dummyInteractor.pageSizeSent == sut.pageSize)
        XCTAssert(dummyInteractor.categoryIdSent == sut.categoryBO?.id.rawValue)
        XCTAssert(dummyInteractor.trendingIdSent == nil)

    }

    func test_givenWithTrendingTypeAndLastLocationAndLatitudeAndLongitudeMoreThanHalfDefaultDistance_whenICallMovePosition_thenICallInteractorProvidePromotionsWithLatitudeAndLongitudeAsParameterAndDistanceAndTypeLengthDefaultAndTrendingId() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 40.424188, withLongitude: -3.695200)
        sut.categoryBO = nil
        sut.trendingType = TrendingType(rawValue: "HOT")

        let latitude = 40.432109
        let longitude = -3.684180

        // when
        sut.movePosition(withLatitude: latitude, andLongitude: longitude)

        // then
        XCTAssert(dummyInteractor.latitudeSent == latitude)
        XCTAssert(dummyInteractor.longitudeSent == longitude)
        XCTAssert(dummyInteractor.distanceSent == sut.defaultDistance)
        XCTAssert(dummyInteractor.lengthTypeSent == sut.lengthType)
        XCTAssert(dummyInteractor.pageSizeSent == sut.pageSize)
        XCTAssert(dummyInteractor.categoryIdSent == nil)
        XCTAssert(dummyInteractor.trendingIdSent == sut.trendingType?.rawValue)

    }

    func test_givenNotCategoryBONeitherTrendingAndLastLocationAndLatitudeAndLongitudeMoreThanHalfDefaultDistance_whenICallMovePosition_whenICallMovePosition_thenErrorBOShouldBeFilled() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let errorEntity = ErrorEntity()
        errorEntity.code = "500"
        errorEntity.message = "Error 500"

        dummyInteractor.forceError = true
        dummyInteractor.errorBO = ErrorBO(error: errorEntity)

        // given
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 40.424188, withLongitude: -3.695200)
        sut.categoryBO = nil
        sut.trendingType = nil

        let latitude = 40.432109
        let longitude = -3.684180

        // when
        sut.movePosition(withLatitude: latitude, andLongitude: longitude)

        // then
        XCTAssert(sut.errorBO != nil)
        XCTAssert(sut.errorBO! === dummyInteractor.errorBO!)
    }

    func test_givenNotCategoryBONeitherTrendingAndLastLocationAndLatitudeAndLongitudeMoreThanHalfDefaultDistance_whenICallMovePosition_thenICallHideLoader() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let errorEntity = ErrorEntity()
        errorEntity.code = "500"
        errorEntity.message = "Error 500"

        dummyInteractor.forceError = true
        dummyInteractor.errorBO = ErrorBO(error: errorEntity)

        // given
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 40.424188, withLongitude: -3.695200)
        sut.categoryBO = nil
        sut.trendingType = nil

        let latitude = 40.432109
        let longitude = -3.684180

        // when
        sut.movePosition(withLatitude: latitude, andLongitude: longitude)

        // then
        XCTAssert(dummyView.isCalledHideLoader == true)
    }

    func test_givenNotCategoryBONeitherTrendingAndLastLocationAndLatitudeAndLongitudeMoreThanHalfDefaultDistance_whenICallMovePosition_thenICallHidePromotions() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let errorEntity = ErrorEntity()
        errorEntity.code = "500"
        errorEntity.message = "Error 500"

        dummyInteractor.forceError = true
        dummyInteractor.errorBO = ErrorBO(error: errorEntity)

        // given
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 40.424188, withLongitude: -3.695200)
        sut.categoryBO = nil
        sut.trendingType = nil

        let latitude = 40.432109
        let longitude = -3.684180

        // when
        sut.movePosition(withLatitude: latitude, andLongitude: longitude)

        // then
        XCTAssert(dummyView.isCalledHidePromotions == true)
    }

    func test_givenNotCategoryBONeitherTrendingAndLastLocationAndLatitudeAndLongitudeMoreThanHalfDefaultDistance_whenICallMovePositionAndInteractorReturnError_thenICallViewShowError() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let errorEntity = ErrorEntity()
        errorEntity.code = "500"
        errorEntity.message = "Error 500"

        dummyInteractor.forceError = true
        dummyInteractor.errorBO = ErrorBO(error: errorEntity)

        // given
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 40.424188, withLongitude: -3.695200)
        sut.categoryBO = nil
        sut.trendingType = nil

        let latitude = 40.432109
        let longitude = -3.684180

        // when
        sut.movePosition(withLatitude: latitude, andLongitude: longitude)

        // then
        XCTAssert(dummyView.isCalledShowError == true)
    }

    func test_givenNotCategoryBONeitherTrendingAndLastLocationAndLatitudeAndLongitudeMoreThanHalfDefaultDistance_whenICallMovePositionAndInteractorReturnSuccess_thenDataPoisShouldBeFilled() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 40.424188, withLongitude: -3.695200)
        sut.categoryBO = nil
        sut.trendingType = nil

        let latitude = 40.432109
        let longitude = -3.684180

        // when
        sut.movePosition(withLatitude: latitude, andLongitude: longitude)

        // then
        XCTAssert(sut.dataPois.isEmpty == false)
    }

    func test_givenNotCategoryBONeitherTrendingLastLocationAndLatitudeAndLongitudeMoreThanHalfDefaultDistance_whenICallMovePositionAndInteractorReturnSuccess_thenICallHideLoader() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 40.424188, withLongitude: -3.695200)
        sut.categoryBO = nil
        sut.trendingType = nil

        let latitude = 40.432109
        let longitude = -3.684180

        // when
        sut.movePosition(withLatitude: latitude, andLongitude: longitude)

        // then
        XCTAssert(dummyView.isCalledHideLoader == true)
    }

    func test_givenNotCategoryBONeitherTrendingLastLocationAndLatitudeAndLongitudeMoreThanHalfDefaultDistance_whenICallMovePositionInteractorReturnSuccess_thenICallShowStores() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 40.424188, withLongitude: -3.695200)
        sut.categoryBO = nil
        sut.trendingType = nil

        let latitude = 40.432109
        let longitude = -3.684180

        // when
        sut.movePosition(withLatitude: latitude, andLongitude: longitude)

        // then
        XCTAssert(dummyView.isCalledShowStores == true)
    }

    func test_givenNotCategoryBONeitherTrendingLastLocationAndLatitudeAndLongitudeMoreThanHalfDefaultDistance_whenICallMovePositionInteractorReturnSuccess_thenICallHidePromotions() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 40.424188, withLongitude: -3.695200)
        sut.categoryBO = nil
        sut.trendingType = nil

        let latitude = 40.432109
        let longitude = -3.684180

        // when
        sut.movePosition(withLatitude: latitude, andLongitude: longitude)

        // then
        XCTAssert(dummyView.isCalledHidePromotions == true)
    }

    func test_givenNotCategoryBONeitherTrendingLastLocationAndLatitudeAndLongitudeMoreThanHalfDefaultDistance_whenICallMovePositionAndInteractorReturnSuccess_thenICallShowStoresWithTheSameNumberOfItemsAsStoresReturned() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        let promotions = PromotionsGenerator.getPromotionsBO()

        for promotion in promotions.promotions {
            promotion.promotionType.id = .discount
        }

        dummyInteractor.promotionsBO = promotions

        // given
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 40.424188, withLongitude: -3.695200)
        sut.categoryBO = nil

        sut.trendingType = nil

        let latitude = 40.432109
        let longitude = -3.684180

        // when
        sut.movePosition(withLatitude: latitude, andLongitude: longitude)

        // then

        var numberOfStoresReturned = 0

        for promotion in dummyInteractor.promotionsBO!.promotions {
            numberOfStoresReturned += (promotion.stores?.count ?? 0)
        }

        XCTAssert(dummyView.displayDataSent!.items.count == numberOfStoresReturned)
    }

    func test_givenADisplayDataMapWithNotCategoryBONeitherTrendingLastLocationAndLatitudeAndLongitudeMoreThanHalfDefaultDistance_whenICallMovePositionAndInteractorReturnSuccess_thenICallShowStoresWithTheSameNumberOfItemsAsStoresReturned() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        let promotions = PromotionsGenerator.getPromotionsBO()

        for promotion in promotions.promotions {
            promotion.promotionType.id = .discount
        }

        dummyInteractor.promotionsBO = promotions

        let dataPoi = DataPoi(latitude: 3, longitude: 4, storeId: promotions.promotions[0].stores![0].id)
        dataPoi.append(promotion: promotions.promotions[0])

        let displayDataMap = MapDisplayData()
        displayDataMap.items.append(MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)!)

        // given
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 40.424188, withLongitude: -3.695200)
        sut.categoryBO = nil
        sut.trendingType = nil
        sut.displayData = displayDataMap

        let latitude = 40.432109
        let longitude = -3.684180

        // when
        sut.movePosition(withLatitude: latitude, andLongitude: longitude)

        // then

        var numberOfStoresReturned = 0

        for promotion in dummyInteractor.promotionsBO!.promotions {
            numberOfStoresReturned += (promotion.stores?.count ?? 0)
        }

        XCTAssert(dummyView.displayDataSent!.items.count == numberOfStoresReturned)
    }

    func test_givenWithCategoryBOLastLocationAndLatitudeAndLongitudeMoreThanHalfDefaultDistance_whenICallMovePositionAndInteractorReturnSuccess_thenICallShowStores() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 40.424188, withLongitude: -3.695200)
        let idType = CategoryEntity(id: "ACTIVITIES", name: "actividades", isFavourite: false)
        sut.categoryBO = CategoryBO(categoryEntity: idType)
        sut.trendingType = nil

        let latitude = 40.432109
        let longitude = -3.684180

        // when
        sut.movePosition(withLatitude: latitude, andLongitude: longitude)

        // then
        XCTAssert(dummyView.isCalledShowStores == true)
    }

    func test_givenWithCategoryBOLastLocationAndLatitudeAndLongitudeMoreThanHalfDefaultDistance_whenICallMovePositionAndInteractorReturnSuccess_thenICallHidePromotions() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 40.424188, withLongitude: -3.695200)
        let idType = CategoryEntity(id: "ACTIVITIES", name: "actividades", isFavourite: false)
        sut.categoryBO = CategoryBO(categoryEntity: idType)
        sut.trendingType = nil

        let latitude = 40.432109
        let longitude = -3.684180

        // when
        sut.movePosition(withLatitude: latitude, andLongitude: longitude)

        // then
        XCTAssert(dummyView.isCalledHidePromotions == true)
    }

    func test_giveWithCategoryBOLastLocationAndLatitudeAndLongitudeMoreThanHalfDefaultDistance_whenICallMovePositionAndInteractorReturnSuccess_thenICallShowStoresWithTheSameNumberOfItemsAsStoresReturned() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        let promotions = PromotionsGenerator.getPromotionsBO()

        for promotion in promotions.promotions {
            promotion.promotionType.id = .discount
        }

        dummyInteractor.promotionsBO = promotions

        // given
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 40.424188, withLongitude: -3.695200)
        let idType = CategoryEntity(id: "ACTIVITIES", name: "actividades", isFavourite: false)
        sut.categoryBO = CategoryBO(categoryEntity: idType)
        sut.trendingType = nil

        let latitude = 40.432109
        let longitude = -3.684180

        // when
        sut.movePosition(withLatitude: latitude, andLongitude: longitude)

        // then

        var numberOfStoresReturned = 0

        for promotion in dummyInteractor.promotionsBO!.promotions {
            numberOfStoresReturned += (promotion.stores?.count ?? 0)
        }

        XCTAssert(dummyView.displayDataSent!.items.count == numberOfStoresReturned)
    }

    func test_givenADisplayDataMapWithCategoryBOLastLocationAndLatitudeAndLongitudeMoreThanHalfDefaultDistance_whenICallMovePositionAndInteractorReturnSuccess_thenICallShowStoresWithTheSameNumberOfItemsAsStoresReturned() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        let promotions = PromotionsGenerator.getPromotionsBO()

        for promotion in promotions.promotions {
            promotion.promotionType.id = .discount
        }

        dummyInteractor.promotionsBO = promotions

        let dataPoi = DataPoi(latitude: 3, longitude: 4, storeId: promotions.promotions[0].stores![0].id)
        dataPoi.append(promotion: promotions.promotions[0])

        let displayDataMap = MapDisplayData()
        displayDataMap.items.append(MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)!)

        // given
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 40.424188, withLongitude: -3.695200)
        let idType = CategoryEntity(id: "ACTIVITIES", name: "actividades", isFavourite: false)
        sut.categoryBO = CategoryBO(categoryEntity: idType)
        sut.trendingType = nil
        sut.displayData = displayDataMap

        let latitude = 40.432109
        let longitude = -3.684180

        // when
        sut.movePosition(withLatitude: latitude, andLongitude: longitude)

        // then

        var numberOfStoresReturned = 0

        for promotion in dummyInteractor.promotionsBO!.promotions {
            numberOfStoresReturned += (promotion.stores?.count ?? 0)
        }

        XCTAssert(dummyView.displayDataSent!.items.count == numberOfStoresReturned)
    }

    func test_givenWithTrendingTypeLastLocationAndLatitudeAndLongitudeMoreThanHalfDefaultDistance_whenICallMovePositionAndInteractorReturnSuccess_thenICallShowStores() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 40.424188, withLongitude: -3.695200)
        sut.categoryBO = nil
        sut.trendingType = TrendingType(rawValue: "HOT")

        let latitude = 40.432109
        let longitude = -3.684180

        // when
        sut.movePosition(withLatitude: latitude, andLongitude: longitude)

        // then
        XCTAssert(dummyView.isCalledShowStores == true)
    }

    func test_givenWithTrendingTypeLastLocationAndLatitudeAndLongitudeMoreThanHalfDefaultDistance_whenICallMovePositionAndInteractorReturnSuccess_thenICallHidePromotions() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 40.424188, withLongitude: -3.695200)
        sut.categoryBO = nil
        sut.trendingType = TrendingType(rawValue: "HOT")

        let latitude = 40.432109
        let longitude = -3.684180

        // when
        sut.movePosition(withLatitude: latitude, andLongitude: longitude)

        // then
        XCTAssert(dummyView.isCalledHidePromotions == true)
    }

    func test_giveWithTrendingTypeLastLocationAndLatitudeAndLongitudeMoreThanHalfDefaultDistance_whenICallMovePositionAndInteractorReturnSuccess_thenICallShowStoresWithTheSameNumberOfItemsAsStoresReturned() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        let promotions = PromotionsGenerator.getPromotionsBO()

        for promotion in promotions.promotions {
            promotion.promotionType.id = .discount
        }

        dummyInteractor.promotionsBO = promotions

        // given
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 40.424188, withLongitude: -3.695200)
        sut.categoryBO = nil
        sut.trendingType = TrendingType(rawValue: "HOT")

        let latitude = 40.432109
        let longitude = -3.684180

        // when
        sut.movePosition(withLatitude: latitude, andLongitude: longitude)

        // then

        var numberOfStoresReturned = 0

        for promotion in dummyInteractor.promotionsBO!.promotions {
            numberOfStoresReturned += (promotion.stores?.count ?? 0)
        }

        XCTAssert(dummyView.displayDataSent!.items.count == numberOfStoresReturned)
    }

    func test_givenADisplayDataMapWithTrendingTypeLastLocationAndLatitudeAndLongitudeMoreThanHalfDefaultDistance_whenICallMovePositionAndInteractorReturnSuccess_thenICallShowStoresWithTheSameNumberOfItemsAsStoresReturned() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        let promotions = PromotionsGenerator.getPromotionsBO()

        for promotion in promotions.promotions {
            promotion.promotionType.id = .discount
        }

        dummyInteractor.promotionsBO = promotions

        let dataPoi = DataPoi(latitude: 3, longitude: 4, storeId: promotions.promotions[0].stores![0].id)
        dataPoi.append(promotion: promotions.promotions[0])

        let displayDataMap = MapDisplayData()
        displayDataMap.items.append(MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)!)

        // given
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 40.424188, withLongitude: -3.695200)
        sut.categoryBO = nil
        sut.trendingType = TrendingType(rawValue: "HOT")
        sut.displayData = displayDataMap

        let latitude = 40.432109
        let longitude = -3.684180

        // when
        sut.movePosition(withLatitude: latitude, andLongitude: longitude)

        // then

        var numberOfStoresReturned = 0

        for promotion in dummyInteractor.promotionsBO!.promotions {
            numberOfStoresReturned += (promotion.stores?.count ?? 0)
        }

        XCTAssert(dummyView.displayDataSent!.items.count == numberOfStoresReturned)
    }

    func test_givenLastLocationAndLatitudeAndLongitudeMoreThanHalfDefaultDistance_whenICallMovePositionAndInteractorReturnPromotionsWithStores_thenIDoNotCallShowToast() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 40.424188, withLongitude: -3.695200)

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let latitude = 40.432109
        let longitude = -3.684180

        // when
        sut.movePosition(withLatitude: latitude, andLongitude: longitude)

        // then
        XCTAssert(dummyView.isCalledShowToastNotStore == false)

    }

    func test_givenCategoryBOLastLocationAndLatitudeAndLongitudeMoreThanHalfDefaultDistance_whenICallMovePositionAndInteractorReturnPromotionsWithStores_thenIDoNotCallShowToast() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 40.424188, withLongitude: -3.695200)

        let idType = CategoryEntity(id: "ACTIVITIES", name: "actividades", isFavourite: false)
        sut.categoryBO = CategoryBO(categoryEntity: idType)

        let latitude = 40.432109
        let longitude = -3.684180

        // when
        sut.movePosition(withLatitude: latitude, andLongitude: longitude)

        // then
        XCTAssert(dummyView.isCalledShowToastNotStore == false)

    }

    func test_givenTrendingBOLastLocationAndLatitudeAndLongitudeMoreThanHalfDefaultDistance_whenICallMovePositionAndInteractorReturnPromotionsWithStores_thenIDoNotCallShowToast() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 40.424188, withLongitude: -3.695200)

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.trendingType = TrendingType(rawValue: "HOT")

        let latitude = 40.432109
        let longitude = -3.684180

        // when
        sut.movePosition(withLatitude: latitude, andLongitude: longitude)

        // then
        XCTAssert(dummyView.isCalledShowToastNotStore == false)

    }

    func test_givenLastLocationAndLatitudeAndLongitudeMoreThanHalfDefaultDistance_whenICallMovePositionAndInteractorReturnPromotionsWithoutStores_thenICallShowToast() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 40.424188, withLongitude: -3.695200)

        // given
        let promotions = PromotionsGenerator.getPromotionsBO()

        for promotion in promotions.promotions {

            promotion.stores = nil

        }

        dummyInteractor.promotionsBO = promotions

        let latitude = 40.432109
        let longitude = -3.684180

        // when
        sut.movePosition(withLatitude: latitude, andLongitude: longitude)

        // then
        XCTAssert(dummyView.isCalledShowToastNotStore == true)

    }

    func test_givenCategoryBOLastLocationAndLatitudeAndLongitudeMoreThanHalfDefaultDistance_whenICallMovePositionAndInteractorReturnPromotionsWithoutStores_thenICallShowToast() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 40.424188, withLongitude: -3.695200)

        let idType = CategoryEntity(id: "ACTIVITIES", name: "actividades", isFavourite: false)
        sut.categoryBO = CategoryBO(categoryEntity: idType)

        let promotions = PromotionsGenerator.getPromotionsBO()

        for promotion in promotions.promotions {

            promotion.stores = nil

        }

        dummyInteractor.promotionsBO = promotions

        let latitude = 40.432109
        let longitude = -3.684180

        // when
        sut.movePosition(withLatitude: latitude, andLongitude: longitude)

        // then
        XCTAssert(dummyView.isCalledShowToastNotStore == true)

    }

    func test_givenTrendingBOLastLocationAndLatitudeAndLongitudeMoreThanHalfDefaultDistance_whenICallMovePositionAndInteractorReturnPromotionsWithoutStores_thenICallShowToast() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 40.424188, withLongitude: -3.695200)

        sut.trendingType = TrendingType(rawValue: "HOT")

        let promotions = PromotionsGenerator.getPromotionsBO()

        for promotion in promotions.promotions {

            promotion.stores = nil

        }

        dummyInteractor.promotionsBO = promotions

        let latitude = 40.432109
        let longitude = -3.684180

        // when
        sut.movePosition(withLatitude: latitude, andLongitude: longitude)

        // then
        XCTAssert(dummyView.isCalledShowToastNotStore == true)

    }

    func test_givenFiltersAndLastLocationRetrieveAndLatitudeAndLongitudeMoreThanHalfDefaultDistance_whenICallMovePosition_thenLastLocationRetrieveShouldMatchWithTheNewOne() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let filters = ShoppingFilter()
        sut.filters = filters
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 40.424188, withLongitude: -3.695200)

        let latitude = 40.432109
        let longitude = -3.684180

        // when
        sut.movePosition(withLatitude: latitude, andLongitude: longitude)

        // then
        XCTAssert(sut.lastLocationRetrieve?.latitude == latitude)
        XCTAssert(sut.lastLocationRetrieve?.longitude == longitude)

    }

    func test_givenFiltersAndLastLocationRetrieveAndLatitudeAndLongitudeMoreThanHalfDefaultDistance_whenICallMovePosition_thenICallViewShowLoader() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let filters = ShoppingFilter()
        sut.filters = filters
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 40.424188, withLongitude: -3.695200)

        let latitude = 40.432109
        let longitude = -3.684180

        // when
        sut.movePosition(withLatitude: latitude, andLongitude: longitude)

        // then
        XCTAssert(dummyView.isCalledShowLoader == true)

    }

    func test_givenFiltersAndLastLocationRetrieveAndLatitudeAndLongitudeLessThanHalfDefaultDistance_whenICallMovePosition_thenIDoNotCallInteractorProvidePromotionsWithFilters() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let filters = ShoppingFilter()
        sut.filters = filters
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 40.424188, withLongitude: -3.695200)

        // when
        sut.movePosition(withLatitude: 40.426834, andLongitude: -3.692153)

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionsWithFilters == false)

    }

    func test_givenFiltersAndLastLocationRetrieveAndLatitudeAndLongitudeMoreThanHalfDefaultDistance_whenICallMovePosition_thenICallInteractorProvidePromotionsWithFilters() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let filters = ShoppingFilter()
        sut.filters = filters
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 40.424188, withLongitude: -3.695200)

        let latitude = 40.432109
        let longitude = -3.684180

        // when
        sut.movePosition(withLatitude: latitude, andLongitude: longitude)

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionsWithFilters == true)

    }

    func test_givenFiltersAndLastLocationRetrieveAndLatitudeAndLongitudeMoreThanHalfDefaultDistance_whenICallMovePosition_thenICallInteractorProvidePromotionsWithFiltersThatMatchAndUsageShouldBePhysicalButFiltersShouldKeepEquals() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let filters = ShoppingFilter()
        filters.textFilter = "textFilter"
        filters.promotionTypeFilter = [.discount]
        filters.categoriesFilter = [.home]
        sut.filters = filters
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 40.424188, withLongitude: -3.695200)

        let latitude = 40.432109
        let longitude = -3.684180

        // when
        sut.movePosition(withLatitude: latitude, andLongitude: longitude)

        // then
        XCTAssert(dummyInteractor.filtersSent?.textFilter == sut.filters?.textFilter)
        XCTAssert(dummyInteractor.filtersSent!.promotionTypeFilter! == sut.filters!.promotionTypeFilter!)
        XCTAssert(dummyInteractor.filtersSent!.promotionUsageTypeFilter! == [PromotionUsageType.physical])
        XCTAssert(dummyInteractor.filtersSent!.categoriesFilter! == sut.filters!.categoriesFilter!)
        XCTAssert(sut.filters == filters)
        XCTAssert(sut.filters?.promotionUsageTypeFilter == nil)

    }

    func test_givenFiltersAndLastLocationRetrieveAndLatitudeAndLongitudeMoreThanHalfDefaultDistance_whenICallMovePosition_thenICallInteractorProvidePromotionsWithFiltersWithTheNewLocation() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let filters = ShoppingFilter()
        sut.filters = filters
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 40.424188, withLongitude: -3.695200)

        let latitude = 40.432109
        let longitude = -3.684180

        // when
        sut.movePosition(withLatitude: latitude, andLongitude: longitude)

        // then
        XCTAssert(dummyInteractor.geoLocationBO.latitude == latitude)
        XCTAssert(dummyInteractor.geoLocationBO.longitude == longitude)

    }

    func test_givenFiltersAndLastLocationRetrieveAndLatitudeAndLongitudeMoreThanHalfDefaultDistance_whenICallMovePosition_thenICallInteractorProvidePromotionsWithFiltersWithDistanceDefaultAndLengthTypeDefault() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let filters = ShoppingFilter()
        sut.filters = filters
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 40.424188, withLongitude: -3.695200)

        let latitude = 40.432109
        let longitude = -3.684180

        // when
        sut.movePosition(withLatitude: latitude, andLongitude: longitude)

        // then
        XCTAssert(dummyInteractor.distanceSent == sut.defaultDistance)
        XCTAssert(dummyInteractor.lengthTypeSent == sut.lengthType)

    }

    func test_givenFiltersAndLastLocationRetrieveAndLatitudeAndLongitudeMoreThanHalfDefaultDistance_whenICallMovePositionAndInteractorReturnsError_thenICallHideLoader() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let filters = ShoppingFilter()
        sut.filters = filters
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 40.424188, withLongitude: -3.695200)

        dummyInteractor.forceError = true

        let latitude = 40.432109
        let longitude = -3.684180

        // when
        sut.movePosition(withLatitude: latitude, andLongitude: longitude)

        // then
        XCTAssert(dummyView.isCalledHideLoader == true)

    }

    func test_givenFiltersAndLastLocationRetrieveAndLatitudeAndLongitudeMoreThanHalfDefaultDistance_whenICallMovePositionAndInteractorReturnsError_thenICallHidePromotions() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let filters = ShoppingFilter()
        sut.filters = filters
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 40.424188, withLongitude: -3.695200)

        dummyInteractor.forceError = true

        let latitude = 40.432109
        let longitude = -3.684180

        // when
        sut.movePosition(withLatitude: latitude, andLongitude: longitude)

        // then
        XCTAssert(dummyView.isCalledHidePromotions == true)

    }

    func test_givenFiltersAndLastLocationRetrieveAndLatitudeAndLongitudeMoreThanHalfDefaultDistance_whenICallMovePositionAndInteractorReturnsError_thenErrorBOShouldBeFilled() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let filters = ShoppingFilter()
        sut.filters = filters
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 40.424188, withLongitude: -3.695200)

        dummyInteractor.forceError = true

        let latitude = 40.432109
        let longitude = -3.684180

        // when
        sut.movePosition(withLatitude: latitude, andLongitude: longitude)

        // then
        XCTAssert(sut.errorBO != nil)
        XCTAssert(sut.errorBO! === dummyInteractor.errorBO!)

    }

    func test_givenFiltersAndLastLocationRetrieveAndLatitudeAndLongitudeMoreThanHalfDefaultDistance_whenICallMovePositionAndInteractorReturnsError_thenICallViewShowError() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let filters = ShoppingFilter()
        sut.filters = filters
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 40.424188, withLongitude: -3.695200)

        dummyInteractor.forceError = true

        let latitude = 40.432109
        let longitude = -3.684180

        // when
        sut.movePosition(withLatitude: latitude, andLongitude: longitude)

        // then
        XCTAssert(dummyView.isCalledShowError == true)

    }

    func test_givenFiltersAndLastLocationRetrieveAndLatitudeAndLongitudeMoreThanHalfDefaultDistance_whenICallMovePositionAndInteractorReturnsSuccess_thenICallHideLoader() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let filters = ShoppingFilter()
        sut.filters = filters
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 40.424188, withLongitude: -3.695200)

        let latitude = 40.432109
        let longitude = -3.684180

        // when
        sut.movePosition(withLatitude: latitude, andLongitude: longitude)

        // then
        XCTAssert(dummyView.isCalledHideLoader == true)

    }

    func test_givenFiltersAndLastLocationRetrieveAndLatitudeAndLongitudeMoreThanHalfDefaultDistance_whenICallMovePositionAndInteractorReturnsSuccess_thenDataPoisShouldBeFilled() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let filters = ShoppingFilter()
        sut.filters = filters
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 40.424188, withLongitude: -3.695200)

        let latitude = 40.432109
        let longitude = -3.684180

        // when
        sut.movePosition(withLatitude: latitude, andLongitude: longitude)

        // then
        XCTAssert(sut.dataPois.isEmpty == false)
    }

    func test_givenFiltersAndLastLocationRetrieveAndLatitudeAndLongitudeMoreThanHalfDefaultDistance_whenICallMovePositionAndInteractorReturnsSuccess_thenICallShowStores() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let filters = ShoppingFilter()
        sut.filters = filters
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 40.424188, withLongitude: -3.695200)

        let latitude = 40.432109
        let longitude = -3.684180

        // when
        sut.movePosition(withLatitude: latitude, andLongitude: longitude)

        // then
        XCTAssert(dummyView.isCalledShowStores == true)

    }

    func test_givenFiltersAndLastLocationRetrieveAndLatitudeAndLongitudeMoreThanHalfDefaultDistance_whenICallMovePositionAndInteractorReturnsSuccess_thenICallHidePromotions() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let filters = ShoppingFilter()
        sut.filters = filters
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 40.424188, withLongitude: -3.695200)

        let latitude = 40.432109
        let longitude = -3.684180

        // when
        sut.movePosition(withLatitude: latitude, andLongitude: longitude)

        // then
        XCTAssert(dummyView.isCalledHidePromotions == true)

    }

    func test_givenFiltersAndLastLocationRetrieveAndLatitudeAndLongitudeMoreThanHalfDefaultDistance_whenICallMovePositionAndInteractorReturnsSuccess_thenICallShowStoresWithTheSameNumberOfItemsAsStoresReturned() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        let promotions = PromotionsGenerator.getPromotionsBO()

        for promotion in promotions.promotions {
            promotion.promotionType.id = .discount
        }

        dummyInteractor.promotionsBO = promotions

        // given
        let filters = ShoppingFilter()
        sut.filters = filters
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 40.424188, withLongitude: -3.695200)

        let latitude = 40.432109
        let longitude = -3.684180

        // when
        sut.movePosition(withLatitude: latitude, andLongitude: longitude)

        // then
        var numberOfStoresReturned = 0

        for promotion in dummyInteractor.promotionsBO!.promotions {
            numberOfStoresReturned += (promotion.stores?.count ?? 0)
        }

        XCTAssert(dummyView.displayDataSent!.items.count == numberOfStoresReturned)
    }

    func test_givenFiltersAndLastLocationRetrieveAndLatitudeAndLongitudeMoreThanHalfDefaultDistance_whenICallMovePositionAndInteractorReturnsPromotionsWithStores_thenIDoNotCallShowToast() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let filters = ShoppingFilter()
        sut.filters = filters
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 40.424188, withLongitude: -3.695200)

        let latitude = 40.432109
        let longitude = -3.684180

        // when
        sut.movePosition(withLatitude: latitude, andLongitude: longitude)

        // then
        XCTAssert(dummyView.isCalledShowToastNotStore == false)

    }

    func test_givenFiltersAndLastLocationRetrieveAndLatitudeAndLongitudeMoreThanHalfDefaultDistance_whenICallMovePositionAndInteractorReturnsPromotionsWithoutStores_thenICallShowToast() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let promotions = PromotionsGenerator.getPromotionsBO()

        for promotion in promotions.promotions {

            promotion.stores = nil

        }

        // given
        let filters = ShoppingFilter()
        sut.filters = filters
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 40.424188, withLongitude: -3.695200)

        dummyInteractor.promotionsBO = promotions

        let latitude = 40.432109
        let longitude = -3.684180

        // when
        sut.movePosition(withLatitude: latitude, andLongitude: longitude)

        // then
        XCTAssert(dummyView.isCalledShowToastNotStore == true)

    }

    // MARK: filterPressed

    func test_givenAny_whenICallFilterPressed_thenICallBusManagerNavigateScreenToFilterScreen() {

        // given

        // when
        sut.filterPressed()

        // then
        XCTAssert(dummyBusManager.isCalledNavigateToFilterScreen == true)

    }

    func test_givenCategoriesBO_whenICallFilterPressed_thenICallBusManagerNavigateScreenToFilterScreenWithFilterTransportCorrectlyFilled() {

        // given
        sut.categoriesBO = PromotionsGenerator.getCategoryBO()

        // when
        sut.filterPressed()

        // then
        XCTAssert(dummyBusManager.filterTransportSent != nil)
        XCTAssert(dummyBusManager.filterTransportSent?.shouldAllowSelectUsageType == false)
        XCTAssert(dummyBusManager.filterTransportSent!.filters == sut.filters)
        XCTAssert(dummyBusManager.filterTransportSent!.categoriesBO === sut.categoriesBO)
        XCTAssert(dummyBusManager.filterTransportSent?.shouldFilterInPreviousScreen == true)

    }

    func test_givenCategoriesBOAndFilters_whenICallFilterPressed_thenICallBusManagerNavigateScreenToFilterScreenWithFilterTransportCorrectlyFilled() {

        // given
        let filters = ShoppingFilter()
        filters.textFilter = "TextFilter"
        filters.categoriesFilter = [.auto]
        filters.promotionTypeFilter = [.discount]
        sut.filters = filters

        // when
        sut.filterPressed()

        // then
        XCTAssert(dummyBusManager.filterTransportSent != nil)
        XCTAssert(dummyBusManager.filterTransportSent?.shouldAllowSelectUsageType == false)

        XCTAssert(dummyBusManager.filterTransportSent!.filters?.textFilter == filters.textFilter)
        XCTAssert(dummyBusManager.filterTransportSent!.filters!.promotionTypeFilter! == filters.promotionTypeFilter!)
        XCTAssert(dummyBusManager.filterTransportSent!.filters?.promotionUsageTypeFilter == nil)
        XCTAssert(dummyBusManager.filterTransportSent!.filters!.categoriesFilter! == filters.categoriesFilter!)

        XCTAssert(dummyBusManager.filterTransportSent!.categoriesBO === sut.categoriesBO)
        XCTAssert(dummyBusManager.filterTransportSent?.shouldFilterInPreviousScreen == true)

    }

    // MARK: - removedFilterWithDisplayData

    func test_givenFiltersNil_whenICallRemovedFilter_thenICallViewHideFilters() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.filters = nil
        let displayDataRemoved = FilterScrollDisplayData(title: "Title1", filterType: "filterType1")

        // when
        sut.removedFilter(withDisplayData: displayDataRemoved)

        // then
        XCTAssert(dummyView.isCalledHideFilters == true)

    }

    func test_givenFilters_whenICallRemovedFilter_thenIFiltersRemoveByFilterType() {

        // given
        let dummyFilters = DummyShoppingFilter()
        sut.filters = dummyFilters
        let displayDataRemoved = FilterScrollDisplayData(title: "Title1", filterType: "filterType1")

        // when
        sut.removedFilter(withDisplayData: displayDataRemoved)

        // then
        XCTAssert(dummyFilters.isCalledRemoveByFilterType == true)

    }

    func test_givenFilters_whenICallRemovedFilter_thenIFiltersRemoveByFilterTypeWithFilterTypeOfDisplayDataOfParameter() {

        // given
        let dummyFilters = DummyShoppingFilter()
        sut.filters = dummyFilters
        let displayDataRemoved = FilterScrollDisplayData(title: "Title1", filterType: "filterType1")

        // when
        sut.removedFilter(withDisplayData: displayDataRemoved)

        // then
        XCTAssert(dummyFilters.sentFilterType! == displayDataRemoved.filterType)

    }

    func test_givenFiltersEmpty_whenICallRemovedFilter_thenICallViewHideFilters() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.filters = ShoppingFilter()
        let displayDataRemoved = FilterScrollDisplayData(title: "Title1", filterType: "filterType1")

        // when
        sut.removedFilter(withDisplayData: displayDataRemoved)

        // then
        XCTAssert(dummyView.isCalledHideFilters == true)

    }

    func test_givenFiltersEmpty_whenICallRemovedFilter_thenFiltersShouldBeNil() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.filters = ShoppingFilter()
        let displayDataRemoved = FilterScrollDisplayData(title: "Title1", filterType: "filterType1")

        // when
        sut.removedFilter(withDisplayData: displayDataRemoved)

        // then
        XCTAssert(sut.filters == nil)

    }

    func test_givenFiltersAndLastLocationRetrieve_whenICallRemovedFilter_thenICallViewShowLoader() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let filters = ShoppingFilter()
        sut.filters = filters
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)

        let displayDataRemoved = FilterScrollDisplayData(title: "Title1", filterType: "filterType1")

        // when
        sut.removedFilter(withDisplayData: displayDataRemoved)

        // then
        XCTAssert(dummyView.isCalledShowLoader == true)

    }

    func test_givenFiltersAndLastLocationRetrieve_whenICallRemovedFilter_thenICallInteractorProvidePromotionsWithFilters() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let filters = ShoppingFilter()
        sut.filters = filters
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)

        let displayDataRemoved = FilterScrollDisplayData(title: "Title1", filterType: "filterType1")

        // when
        sut.removedFilter(withDisplayData: displayDataRemoved)

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionsWithFilters == true)

    }

    func test_givenFiltersAndLastLocationRetrieve_whenICallRemovedFilter_thenICallInteractorProvidePromotionsWithFiltersThatMatchAndUsageShouldBePhysicalButFiltersShouldKeepEquals() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let filters = ShoppingFilter()
        filters.textFilter = "textFilter"
        filters.promotionTypeFilter = [.discount]
        filters.categoriesFilter = [.home]
        sut.filters = filters
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)

        let displayDataRemoved = FilterScrollDisplayData(title: "Title1", filterType: "filterType1")

        // when
        sut.removedFilter(withDisplayData: displayDataRemoved)

        // then
        XCTAssert(dummyInteractor.filtersSent?.textFilter == sut.filters?.textFilter)
        XCTAssert(dummyInteractor.filtersSent!.promotionTypeFilter! == sut.filters!.promotionTypeFilter!)
        XCTAssert(dummyInteractor.filtersSent!.promotionUsageTypeFilter! == [PromotionUsageType.physical])
        XCTAssert(dummyInteractor.filtersSent!.categoriesFilter! == sut.filters!.categoriesFilter!)
        XCTAssert(sut.filters == filters)
        XCTAssert(sut.filters?.promotionUsageTypeFilter == nil)

    }

    func test_givenFiltersAndLastLocationRetrieve_whenICallRemovedFilter_thenICallInteractorProvidePromotionsWithFiltersWithLastGeolocationRetrieve() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let filters = ShoppingFilter()
        sut.filters = filters
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)

        let displayDataRemoved = FilterScrollDisplayData(title: "Title1", filterType: "filterType1")

        // when
        sut.removedFilter(withDisplayData: displayDataRemoved)

        // then
        XCTAssert(dummyInteractor.geoLocationBO.latitude == sut.lastLocationRetrieve?.latitude)
        XCTAssert(dummyInteractor.geoLocationBO.longitude == sut.lastLocationRetrieve?.longitude)

    }

    func test_givenFiltersAndLastLocationRetrieve_whenICallRemovedFilter_thenICallInteractorProvidePromotionsWithFiltersWithDistanceDefaultAndLengthTypeDefault() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let filters = ShoppingFilter()
        sut.filters = filters
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)

        let displayDataRemoved = FilterScrollDisplayData(title: "Title1", filterType: "filterType1")

        // when
        sut.removedFilter(withDisplayData: displayDataRemoved)

        // then
        XCTAssert(dummyInteractor.distanceSent == sut.defaultDistance)
        XCTAssert(dummyInteractor.lengthTypeSent == sut.lengthType)

    }

    func test_givenFiltersAndLastLocationRetrieve_whenICallRemovedFilterAndInteractorReturnsError_thenICallHideLoader() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let filters = ShoppingFilter()
        sut.filters = filters
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)

        dummyInteractor.forceError = true

        let displayDataRemoved = FilterScrollDisplayData(title: "Title1", filterType: "filterType1")

        // when
        sut.removedFilter(withDisplayData: displayDataRemoved)

        // then
        XCTAssert(dummyView.isCalledHideLoader == true)

    }

    func test_givenFiltersAndLastLocationRetrieve_whenICallRemovedFilterAndInteractorReturnsError_thenICallHidePromotions() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let filters = ShoppingFilter()
        sut.filters = filters
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)

        dummyInteractor.forceError = true

        let displayDataRemoved = FilterScrollDisplayData(title: "Title1", filterType: "filterType1")

        // when
        sut.removedFilter(withDisplayData: displayDataRemoved)

        // then
        XCTAssert(dummyView.isCalledHidePromotions == true)

    }

    func test_givenFiltersAndLastLocationRetrieve_whenICallRemovedFilterAndInteractorReturnsError_thenErrorBOShouldBeFilled() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let filters = ShoppingFilter()
        sut.filters = filters
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)

        dummyInteractor.forceError = true

        let displayDataRemoved = FilterScrollDisplayData(title: "Title1", filterType: "filterType1")

        // when
        sut.removedFilter(withDisplayData: displayDataRemoved)

        // then
        XCTAssert(sut.errorBO != nil)
        XCTAssert(sut.errorBO! === dummyInteractor.errorBO!)

    }

    func test_givenFiltersAndLastLocationRetrieve_whenICallRemovedFilterAndInteractorReturnsError_thenICallViewShowError() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let filters = ShoppingFilter()
        sut.filters = filters
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)

        dummyInteractor.forceError = true

        let displayDataRemoved = FilterScrollDisplayData(title: "Title1", filterType: "filterType1")

        // when
        sut.removedFilter(withDisplayData: displayDataRemoved)

        // then
        XCTAssert(dummyView.isCalledShowError == true)

    }

    func test_givenFiltersAndLastLocationRetrieve_whenICallRemovedFilterAndInteractorReturnsSuccess_thenICallHideLoader() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let filters = ShoppingFilter()
        sut.filters = filters
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)

        let displayDataRemoved = FilterScrollDisplayData(title: "Title1", filterType: "filterType1")

        // when
        sut.removedFilter(withDisplayData: displayDataRemoved)

        // then
        XCTAssert(dummyView.isCalledHideLoader == true)

    }

    func test_givenFiltersAndLastLocationRetrieve_whenICallRemovedFilterAndInteractorReturnsSuccess_thenDataPoisShouldBeFilled() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let filters = ShoppingFilter()
        sut.filters = filters
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)

        let displayDataRemoved = FilterScrollDisplayData(title: "Title1", filterType: "filterType1")

        // when
        sut.removedFilter(withDisplayData: displayDataRemoved)

        // then
        XCTAssert(sut.dataPois.isEmpty == false)
    }

    func test_givenFiltersAndLastLocationRetrieve_whenICallRemovedFilterAndInteractorReturnsSuccess_thenICallShowStores() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let filters = ShoppingFilter()
        sut.filters = filters
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)

        let displayDataRemoved = FilterScrollDisplayData(title: "Title1", filterType: "filterType1")

        // when
        sut.removedFilter(withDisplayData: displayDataRemoved)

        // then
        XCTAssert(dummyView.isCalledShowStores == true)

    }

    func test_givenFiltersAndLastLocationRetrieve_whenICallRemovedFilterAndInteractorReturnsSuccess_thenICallHidePromotions() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let filters = ShoppingFilter()
        sut.filters = filters
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)

        let displayDataRemoved = FilterScrollDisplayData(title: "Title1", filterType: "filterType1")

        // when
        sut.removedFilter(withDisplayData: displayDataRemoved)

        // then
        XCTAssert(dummyView.isCalledHidePromotions == true)

    }

    func test_givenFiltersAndLastLocationRetrieve_whenICallRemovedFilterAndInteractorReturnsSuccess_thenICallShowStoresWithTheSameNumberOfItemsAsStoresReturned() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        let promotions = PromotionsGenerator.getPromotionsBO()

        for promotion in promotions.promotions {
            promotion.promotionType.id = .discount
        }

        dummyInteractor.promotionsBO = promotions

        // given
        let filters = ShoppingFilter()
        sut.filters = filters
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)

        let displayDataRemoved = FilterScrollDisplayData(title: "Title1", filterType: "filterType1")

        // when
        sut.removedFilter(withDisplayData: displayDataRemoved)

        // then
        var numberOfStoresReturned = 0

        for promotion in dummyInteractor.promotionsBO!.promotions {
            numberOfStoresReturned += (promotion.stores?.count ?? 0)
        }

        XCTAssert(dummyView.displayDataSent!.items.count == numberOfStoresReturned)
    }

    func test_givenFiltersAndLastLocationRetrieve_whenICallRemovedFilterAndInteractorReturnsPromotionsWithStores_thenIDoNotCallShowToast() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let filters = ShoppingFilter()
        sut.filters = filters
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)

        let displayDataRemoved = FilterScrollDisplayData(title: "Title1", filterType: "filterType1")

        // when
        sut.removedFilter(withDisplayData: displayDataRemoved)

        // then
        XCTAssert(dummyView.isCalledShowToastNotStore == false)

    }

    func test_givenFiltersAndLastLocationRetrieve_whenICallRemovedFilterAndInteractorReturnsPromotionsWithoutStores_thenICallShowToast() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let promotions = PromotionsGenerator.getPromotionsBO()

        for promotion in promotions.promotions {

            promotion.stores = nil

        }

        // given
        let filters = ShoppingFilter()
        sut.filters = filters
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)

        dummyInteractor.promotionsBO = promotions

        let displayDataRemoved = FilterScrollDisplayData(title: "Title1", filterType: "filterType1")

        // when
        sut.removedFilter(withDisplayData: displayDataRemoved)

        // then
        XCTAssert(dummyView.isCalledShowToastNotStore == true)

    }

    // MARK: - removedAllFilters

    func test_givenAny_whenICallRemovedAllFilters_thenFiltersShouldBeNil() {

        // given
        sut.filters = ShoppingFilter()

        // when
        sut.removedAllFilters()

        // then
        XCTAssert(sut.filters == nil)

    }

    func test_givenAny_whenICallRemovedAllFilters_thenICallViewHideFilters() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given

        // when
        sut.removedAllFilters()

        // then
        XCTAssert(dummyView.isCalledHideFilters == true)

    }

    func test_givenFiltersAndLastLocationRetrieve_whenICallRemovedAllFilters_thenICallViewShowLoader() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let filters = ShoppingFilter()
        sut.filters = filters
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)

        // when
        sut.removedAllFilters()

        // then
        XCTAssert(dummyView.isCalledShowLoader == true)

    }

    func test_givenFiltersAndLastLocationRetrieve_whenICallRemovedAllFilters_thenICallInteractorProvidePromotionsWithFilters() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let filters = ShoppingFilter()
        sut.filters = filters
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)

        // when
        sut.removedAllFilters()

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionsWithFilters == true)

    }

    func test_givenFiltersAndLastLocationRetrieve_whenICallRemovedAllFilters_thenICallInteractorProvidePromotionsWithFiltersWithPhysicalPromotionUsageAndOthersNil() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let filters = ShoppingFilter()
        filters.textFilter = "textFilter"
        filters.promotionTypeFilter = [.discount]
        filters.categoriesFilter = [.home]
        sut.filters = filters
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)

        // when
        sut.removedAllFilters()

        // then
        XCTAssert(dummyInteractor.filtersSent?.textFilter == nil)
        XCTAssert(dummyInteractor.filtersSent!.promotionTypeFilter == nil)
        XCTAssert(dummyInteractor.filtersSent!.promotionUsageTypeFilter! == [PromotionUsageType.physical])
        XCTAssert(dummyInteractor.filtersSent!.categoriesFilter == nil)
        XCTAssert(sut.filters?.promotionUsageTypeFilter == nil)

    }

    func test_givenFiltersAndLastLocationRetrieve_whenICallRemovedAllFilters_thenICallInteractorProvidePromotionsWithFiltersWithLastGeolocationRetrieve() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let filters = ShoppingFilter()
        sut.filters = filters
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)

        // when
        sut.removedAllFilters()

        // then
        XCTAssert(dummyInteractor.geoLocationBO.latitude == sut.lastLocationRetrieve?.latitude)
        XCTAssert(dummyInteractor.geoLocationBO.longitude == sut.lastLocationRetrieve?.longitude)

    }

    func test_givenFiltersAndLastLocationRetrieve_whenICallRemovedAllFilters_thenICallInteractorProvidePromotionsWithFiltersWithDistanceDefaultAndLengthTypeDefault() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let filters = ShoppingFilter()
        sut.filters = filters
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)

        // when
        sut.removedAllFilters()

        // then
        XCTAssert(dummyInteractor.distanceSent == sut.defaultDistance)
        XCTAssert(dummyInteractor.lengthTypeSent == sut.lengthType)

    }

    func test_givenFiltersAndLastLocationRetrieve_whenICallRemovedAllFiltersAndInteractorReturnsError_thenICallHideLoader() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let filters = ShoppingFilter()
        sut.filters = filters
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)

        dummyInteractor.forceError = true

        // when
        sut.removedAllFilters()

        // then
        XCTAssert(dummyView.isCalledHideLoader == true)

    }

    func test_givenFiltersAndLastLocationRetrieve_whenICallRemovedAllFiltersAndInteractorReturnsError_thenICallHidePromotions() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let filters = ShoppingFilter()
        sut.filters = filters
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)

        dummyInteractor.forceError = true

        // when
        sut.removedAllFilters()

        // then
        XCTAssert(dummyView.isCalledHidePromotions == true)

    }

    func test_givenFiltersAndLastLocationRetrieve_whenICallRemovedAllFiltersAndInteractorReturnsError_thenErrorBOShouldBeFilled() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let filters = ShoppingFilter()
        sut.filters = filters
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)

        dummyInteractor.forceError = true

        // when
        sut.removedAllFilters()

        // then
        XCTAssert(sut.errorBO != nil)
        XCTAssert(sut.errorBO! === dummyInteractor.errorBO!)

    }

    func test_givenFiltersAndLastLocationRetrieve_whenICallRemovedAllFiltersAndInteractorReturnsError_thenICallViewShowError() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let filters = ShoppingFilter()
        sut.filters = filters
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)

        dummyInteractor.forceError = true

        // when
        sut.removedAllFilters()

        // then
        XCTAssert(dummyView.isCalledShowError == true)

    }

    func test_givenFiltersAndLastLocationRetrieve_whenICallRemovedAllFiltersAndInteractorReturnsSuccess_thenICallHideLoader() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let filters = ShoppingFilter()
        sut.filters = filters
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)

        // when
        sut.removedAllFilters()

        // then
        XCTAssert(dummyView.isCalledHideLoader == true)

    }

    func test_givenFiltersAndLastLocationRetrieve_whenICallRemovedAllFiltersAndInteractorReturnsSuccess_thenDataPoisShouldBeFilled() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let filters = ShoppingFilter()
        sut.filters = filters
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)

        // when
        sut.removedAllFilters()

        // then
        XCTAssert(sut.dataPois.isEmpty == false)
    }

    func test_givenFiltersAndLastLocationRetrieve_whenICallRemovedAllFiltersAndInteractorReturnsSuccess_thenICallShowStores() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let filters = ShoppingFilter()
        sut.filters = filters
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)

        // when
        sut.removedAllFilters()

        // then
        XCTAssert(dummyView.isCalledShowStores == true)

    }

    func test_givenFiltersAndLastLocationRetrieve_whenICallRemovedAllFiltersAndInteractorReturnsSuccess_thenICallHidePromotions() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let filters = ShoppingFilter()
        sut.filters = filters
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)

        // when
        sut.removedAllFilters()

        // then
        XCTAssert(dummyView.isCalledHidePromotions == true)

    }

    func test_givenFiltersAndLastLocationRetrieve_whenICallRemovedAllFiltersAndInteractorReturnsSuccess_thenICallShowStoresWithTheSameNumberOfItemsAsStoresReturned() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        let promotions = PromotionsGenerator.getPromotionsBO()

        for promotion in promotions.promotions {
            promotion.promotionType.id = .discount
        }

        dummyInteractor.promotionsBO = promotions

        // given
        let filters = ShoppingFilter()
        sut.filters = filters
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)

        // when
        sut.removedAllFilters()

        // then
        var numberOfStoresReturned = 0

        for promotion in dummyInteractor.promotionsBO!.promotions {
            numberOfStoresReturned += (promotion.stores?.count ?? 0)
        }

        XCTAssert(dummyView.displayDataSent!.items.count == numberOfStoresReturned)
    }

    func test_givenFiltersAndLastLocationRetrieve_whenICallRemovedAllFiltersAndInteractorReturnsPromotionsWithStores_thenIDoNotCallShowToast() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let filters = ShoppingFilter()
        sut.filters = filters
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)

        // when
        sut.removedAllFilters()

        // then
        XCTAssert(dummyView.isCalledShowToastNotStore == false)

    }

    func test_givenFiltersAndLastLocationRetrieve_whenICallRemovedAllFiltersAndInteractorReturnsPromotionsWithoutStores_thenICallShowToast() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let promotions = PromotionsGenerator.getPromotionsBO()

        for promotion in promotions.promotions {

            promotion.stores = nil

        }

        // given
        let filters = ShoppingFilter()
        sut.filters = filters
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)

        dummyInteractor.promotionsBO = promotions

        // when
        sut.removedAllFilters()

        // then
        XCTAssert(dummyView.isCalledShowToastNotStore == true)

    }

    // MARK: Promotion plus seleted

    func test_givenAny_whenDidTapInPromotionPlusCell_thenICallNavigateToShoppingMapList() {

        //given
        dummyBusManager.isCalledNavigatedToShoppingMapList = false

        //when
        sut.didTapInPromotionPlusCell()

        //then
        XCTAssert(dummyBusManager.isCalledNavigatedToShoppingMapList == true)

    }

    func test_givenADisplayDataPromotion_whenDidTapInPromotionPlusCellWithAMultiplePromotion_thenICallNavigateToShoppingWithTransportPromotionsBOFilled() {

        //given
        sut.promotionsInCarrousel = PromotionsGenerator.getPromotionsBO().promotions
        sut.userLocation = GeoLocationBO(withLatitude: 0.0, withLongitude: 0.0)
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 0.5, withLongitude: 0.0)

        dummyBusManager.isCalledNavigatedToShoppingMapList = false
        dummyBusManager.shoppiongMapListTransportSent = nil

        //when
        sut.didTapInPromotionPlusCell()

        //then
        XCTAssert(dummyBusManager.shoppiongMapListTransportSent != nil)
        XCTAssert(dummyBusManager.shoppiongMapListTransportSent!.promotions == sut.promotionsInCarrousel)
        XCTAssert(dummyBusManager.shoppiongMapListTransportSent!.userLocation! == sut.userLocation!)
        XCTAssert(dummyBusManager.shoppiongMapListTransportSent!.storeLocation! == sut.lastLocationRetrieve!)

    }

    // MARK: Promotion selected promotion form collectionView

    func test_givenADisplayDataPromotion_whenDidTapInPromotionCellWithAPromotion_thenCallNavigateToDetailPromotion() {

        //given
        sut.promotionsInCarrousel = PromotionsGenerator.getPromotionsBO().promotions
        dummyBusManager.isCalledNavigateToShoppingDetail = false

        //when
        sut.didTapInPromotionCell(atIndex: 0)

        //then
        XCTAssert(dummyBusManager.isCalledNavigateToShoppingDetail == true)

    }

    func test_givenADisplayDataPromotion_whenDidTapInPromotionCellWithAPromotion_thenCallNavigateToDetailPromotionWithTransportPromotionsBOFilled() {

        //given
        sut.promotionsInCarrousel = PromotionsGenerator.getPromotionsBO().promotions

        dummyBusManager.isCalledNavigateToShoppingDetail = false
        dummyBusManager.promotionDetailTransportSent = nil
        let index = 0

        //when
        sut.didTapInPromotionCell(atIndex: index)

        //then
        XCTAssert(dummyBusManager.promotionDetailTransportSent != nil)
        XCTAssert(dummyBusManager.promotionDetailTransportSent!.promotionBO == sut.promotionsInCarrousel[0])
        XCTAssert(dummyBusManager.promotionDetailTransportSent!.category?.id == sut.categoryBO?.id)
        XCTAssert(dummyBusManager.promotionDetailTransportSent?.distanceAndDateToShow == sut.displayDataPromotions.itemsToShow[safeElement:index]?.dateDiffPlusDistance ?? sut.displayDataPromotions.itemsToShow[safeElement:index]?.dateDiff)
        XCTAssert(dummyBusManager.promotionDetailTransportSent!.userLocation == sut.userLocation)
        XCTAssert(dummyBusManager.promotionDetailTransportSent!.shownOnMap == true)

    }
    
    // MARK: - filter withModel

    func test_givenShoppingTransportWithFilters_whenICallFilterWithModel_thenFiltersShouldBeFilled() {
        
        // given
        let shoppingFilterTransport = ShoppingFilterTransport()
        shoppingFilterTransport.filters = ShoppingFilter()
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 0.5, withLongitude: 0.0)
        
        // when
        sut.filter(withModel: shoppingFilterTransport)

        // then
        XCTAssert(sut.filters != nil)
        
    }
    
    func test_givenShoppingTransportWithFilters_whenICallFilterWithModel_thenFiltersShouldMatchWithTransport() {
        
        // given
        let shoppingFilterTransport = ShoppingFilterTransport()
        shoppingFilterTransport.filters = ShoppingFilter()
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 0.5, withLongitude: 0.0)
        
        // when
        sut.filter(withModel: shoppingFilterTransport)

        // then
        XCTAssert(sut.filters == shoppingFilterTransport.filters)
        
    }
    
    func test_givenFiltersNotEmtpyAndCategoriesBOAndLastLocationRetrieve_whenICallFilterWithModel_thenICallViewShowFilters() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let shoppingFilterTransport = ShoppingFilterTransport()

        let filters = ShoppingFilter()
        shoppingFilterTransport.filters = filters
        filters.textFilter = "textFilter"
    
        sut.categoriesBO = PromotionsGenerator.getCategoryBO()
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)
        
        // when
        sut.filter(withModel: shoppingFilterTransport)

        // then
        XCTAssert(dummyView.isCalledShowFiltersWithDisplayData == true)
        
    }
    
    func test_givenFiltersAndNotCategoriesBOAndLastLocationRetrieve_whenICallFilterWithModel_thenICallViewHideFilters() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let shoppingFilterTransport = ShoppingFilterTransport()

        let filters = ShoppingFilter()
        shoppingFilterTransport.filters = filters
        filters.textFilter = "textFilter"
        
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)
        
        // when
        sut.filter(withModel: shoppingFilterTransport)

        // then
        XCTAssert(dummyView.isCalledHideFilters == true)
        
    }
    
    func test_givenFiltersEmptyAndCategoriesBOAndLastLocationRetrieve_whenICallFilterWithModel_thenICallViewHideFilters() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let shoppingFilterTransport = ShoppingFilterTransport()

        let filters = ShoppingFilter()
        shoppingFilterTransport.filters = filters
        
        sut.categoriesBO = PromotionsGenerator.getCategoryBO()
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)
        
        // when
        sut.filter(withModel: shoppingFilterTransport)

        // then
        XCTAssert(dummyView.isCalledHideFilters == true)
    }
    
    func test_givenFiltersNotEmtpyAndCategoriesBOAndLastLocationRetrieve_whenICallFilterWithModel_thenICallFiltersRetrieveViewLiterals() {
        
        let dummyShoppingFilter = DummyShoppingFilter()
        
        // given
        let shoppingFilterTransport = ShoppingFilterTransport()
        
        shoppingFilterTransport.filters = dummyShoppingFilter
        dummyShoppingFilter.textFilter = "textFilter"
        
        sut.categoriesBO = PromotionsGenerator.getCategoryBO()
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)
        
        // when
        sut.filter(withModel: shoppingFilterTransport)

        // then
        XCTAssert(dummyShoppingFilter.isCalledRetrieveLiterals == true)
        
    }
    
    func test_givenFiltersNotEmptyAndCategoriesBOAndLastLocationRetrieve_whenICallFilterWithModel_thenICallViewShowFiltersWithTheSameNumberOfDisplayDataThanFiltersViewLiteralsAndMatchTheTitleAndKey() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let shoppingFilterTransport = ShoppingFilterTransport()

        let dummyShoppingFilter = DummyShoppingFilter()
        shoppingFilterTransport.filters = dummyShoppingFilter
        dummyShoppingFilter.defaultLiterals = [(title: "title1", key: "key1"), (title: "title2", key: "key2"), (title: "title3", key: "key3"), (title: "title4", key: "key4")]
        
        // given
        dummyShoppingFilter.textFilter = "textfilter"
        sut.categoriesBO = PromotionsGenerator.getCategoryBO()
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)
        
        // when
        sut.filter(withModel: shoppingFilterTransport)

        // then
        XCTAssert(dummyView.sentDisplayFilters!.count == dummyShoppingFilter.defaultLiterals!.count)
        
        for i in 0..<dummyShoppingFilter.defaultLiterals!.count {
            
            XCTAssert(dummyView.sentDisplayFilters![i].title == dummyShoppingFilter.defaultLiterals![i].title)
            XCTAssert(dummyView.sentDisplayFilters![i].filterType == dummyShoppingFilter.defaultLiterals![i].key)
            
        }
        
    }
    
    func test_givenFiltersAndAreTheSameFilters_whenICallFilterWithModel_thenIDoNotCallViewShowFilters() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let shoppingFilterTransport = ShoppingFilterTransport()

        let filters = ShoppingFilter()
        shoppingFilterTransport.filters = filters
        filters.textFilter = "textFilter"
        
        // when
        sut.filter(withModel: shoppingFilterTransport)

        // then
        XCTAssert(dummyView.isCalledShowFiltersWithDisplayData == false)
        
    }
    
    func test_givenFiltersAndAreTheSameFilters_whenICallFilterWithModel_thenIDoNotCallViewHideFilters() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let shoppingFilterTransport = ShoppingFilterTransport()
        
        let filters = ShoppingFilter()
        shoppingFilterTransport.filters = filters
        filters.textFilter = "textFilter"
        
        // when
        sut.filter(withModel: shoppingFilterTransport)

        // then
        XCTAssert(dummyView.isCalledHideFilters == false)
        
    }
    
    func test_givenFiltersAndAreTheSameFilters_whenICallFilterWithModel_thenIDoNotCallInteractorProvidePromotionsWithFilters() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        let shoppingFilterTransport = ShoppingFilterTransport()

        let filters = ShoppingFilter()
        shoppingFilterTransport.filters = filters
        filters.textFilter = "textFilter"
        
        // when
        sut.filter(withModel: shoppingFilterTransport)

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionsWithFilters == false)
        
    }
    
    func test_givenFiltersCategoriesBOAndLastLocationRetrieve_whenICallFilterWithModel_thenICallViewShowLoader() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let shoppingFilterTransport = ShoppingFilterTransport()

        let filters = ShoppingFilter()
        shoppingFilterTransport.filters = filters
        
        sut.categoriesBO = PromotionsGenerator.getCategoryBO()
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)
        
        // when
        sut.filter(withModel: shoppingFilterTransport)

        // then
        XCTAssert(dummyView.isCalledShowLoader == true)
        
    }
    
    func test_givenFiltersCategoriesBOAndLastLocationRetrieve_whenICallFilterWithModel_thenICallInteractorProvidePromotionsWithFilters() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        let shoppingFilterTransport = ShoppingFilterTransport()

        let filters = ShoppingFilter()
        shoppingFilterTransport.filters = filters
        sut.categoriesBO = PromotionsGenerator.getCategoryBO()
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)
        
        // when
        sut.filter(withModel: shoppingFilterTransport)

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionsWithFilters == true)
        
    }
    
    func test_givenFiltersCategoriesBOAndLastLocationRetrieve_whenICallFilterWithModel_thenICallInteractorProvidePromotionsWithFiltersThatMatchAndUsageShouldBePhysicalButFiltersShouldKeepEquals() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        let shoppingFilterTransport = ShoppingFilterTransport()

        let filters = ShoppingFilter()
        filters.textFilter = "textFilter"
        filters.promotionTypeFilter = [.discount]
        filters.categoriesFilter = [.home]
        shoppingFilterTransport.filters = filters

        sut.categoriesBO = PromotionsGenerator.getCategoryBO()
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)
        
        // when
        sut.filter(withModel: shoppingFilterTransport)

        // then
        XCTAssert(dummyInteractor.filtersSent?.textFilter == sut.filters?.textFilter)
        XCTAssert(dummyInteractor.filtersSent!.promotionTypeFilter! == sut.filters!.promotionTypeFilter!)
        XCTAssert(dummyInteractor.filtersSent!.promotionUsageTypeFilter! == [PromotionUsageType.physical])
        XCTAssert(dummyInteractor.filtersSent!.categoriesFilter! == sut.filters!.categoriesFilter!)
        XCTAssert(sut.filters == filters)
        XCTAssert(sut.filters?.promotionUsageTypeFilter == nil)
        
    }
    
    func test_givenFiltersCategoriesBOAndLastLocationRetrieve_whenICallFilterWithModel_thenICallInteractorProvidePromotionsWithFiltersWithLastGeolocationRetrieve() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        let shoppingFilterTransport = ShoppingFilterTransport()
        
        let filters = ShoppingFilter()
        shoppingFilterTransport.filters = filters
        filters.textFilter = "textFilter"
        
        sut.categoriesBO = PromotionsGenerator.getCategoryBO()
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)
        
        // when
        sut.filter(withModel: shoppingFilterTransport)

        // then
        XCTAssert(dummyInteractor.geoLocationBO.latitude == sut.lastLocationRetrieve?.latitude)
        XCTAssert(dummyInteractor.geoLocationBO.longitude == sut.lastLocationRetrieve?.longitude)
        
    }
    
    // Aqui
    
    func test_givenFiltersCategoriesBOAndLastLocationRetrieve_whenICallFilterWithModel_thenICallInteractorProvidePromotionsWithFiltersWithDistanceDefaultAndLengthTypeDefault() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        let shoppingFilterTransport = ShoppingFilterTransport()

        let filters = ShoppingFilter()
        shoppingFilterTransport.filters = filters
        filters.textFilter = "textFilter"

        sut.categoriesBO = PromotionsGenerator.getCategoryBO()
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)
        
        // when
        sut.filter(withModel: shoppingFilterTransport)

        // then
        XCTAssert(dummyInteractor.distanceSent == sut.defaultDistance)
        XCTAssert(dummyInteractor.lengthTypeSent == sut.lengthType)
        
    }
    
    func test_givenFiltersCategoriesBOAndLastLocationRetrieve_whenICallFilterWithModelAndInteractorReturnsError_thenICallHideLoader() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        let shoppingFilterTransport = ShoppingFilterTransport()

        let filters = ShoppingFilter()
        shoppingFilterTransport.filters = filters
        filters.textFilter = "textFilter"

        sut.categoriesBO = PromotionsGenerator.getCategoryBO()
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)
        
        dummyInteractor.forceError = true
        
        // when
        sut.filter(withModel: shoppingFilterTransport)

        // then
        XCTAssert(dummyView.isCalledHideLoader == true)
        
    }
    
    func test_givenFiltersCategoriesBOAndLastLocationRetrieveAndShouldLoadFiltersTrue_whenICallFilterWithModelAndInteractorReturnsError_thenICallHidePromotions() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        let shoppingFilterTransport = ShoppingFilterTransport()

        let filters = ShoppingFilter()
        shoppingFilterTransport.filters = filters
        filters.textFilter = "textFilter"
        
        sut.categoriesBO = PromotionsGenerator.getCategoryBO()
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)
        
        dummyInteractor.forceError = true
        
        // when
        sut.filter(withModel: shoppingFilterTransport)

        // then
        XCTAssert(dummyView.isCalledHidePromotions == true)
        
    }
    
    func test_givenFiltersCategoriesBOAndLastLocationRetrieve_whenICallFilterWithModelAndInteractorReturnsError_thenErrorBOShouldBeFilled() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        let shoppingFilterTransport = ShoppingFilterTransport()

        let filters = ShoppingFilter()
        shoppingFilterTransport.filters = filters
        filters.textFilter = "textFilter"
        
        sut.categoriesBO = PromotionsGenerator.getCategoryBO()
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)
        
        dummyInteractor.forceError = true
        
        // when
        sut.filter(withModel: shoppingFilterTransport)

        // then
        XCTAssert(sut.errorBO != nil)
        XCTAssert(sut.errorBO! === dummyInteractor.errorBO!)
        
    }
    
    func test_givenFiltersCategoriesBOAndLastLocationRetrieve_whenICallFilterWithModelAndInteractorReturnsError_thenICallViewShowError() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        let shoppingFilterTransport = ShoppingFilterTransport()

        let filters = ShoppingFilter()
        shoppingFilterTransport.filters = filters
        filters.textFilter = "textFilter"
        
        sut.categoriesBO = PromotionsGenerator.getCategoryBO()
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)
        
        dummyInteractor.forceError = true
        
        // when
        sut.filter(withModel: shoppingFilterTransport)

        // then
        XCTAssert(dummyView.isCalledShowError == true)
        
    }
    
    func test_givenFiltersCategoriesBOAndLastLocationRetrieve_whenICallFilterWithModelAndInteractorReturnsSuccess_thenICallHideLoader() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        let shoppingFilterTransport = ShoppingFilterTransport()

        let filters = ShoppingFilter()
        shoppingFilterTransport.filters = filters
        filters.textFilter = "textFilter"
        
        sut.categoriesBO = PromotionsGenerator.getCategoryBO()
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)
        
        // when
        sut.filter(withModel: shoppingFilterTransport)

        // then
        XCTAssert(dummyView.isCalledHideLoader == true)
        
    }
    
    func test_givenFiltersCategoriesBOAndLastLocationRetrieveAndShouldLoadFiltersTrue_whenICallFilterWithModelAndInteractorReturnsSuccess_thenDataPoisShouldBeFilled() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        let shoppingFilterTransport = ShoppingFilterTransport()

        let filters = ShoppingFilter()
        shoppingFilterTransport.filters = filters
        filters.textFilter = "textFilter"
        
        sut.categoriesBO = PromotionsGenerator.getCategoryBO()
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)
        
        // when
        sut.filter(withModel: shoppingFilterTransport)

        // then
        XCTAssert(sut.dataPois.isEmpty == false)
    }
    
    func test_givenFiltersCategoriesBOAndLastLocationRetrieve_whenICallFilterWithModelAndInteractorReturnsSuccess_thenICallShowStores() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        let shoppingFilterTransport = ShoppingFilterTransport()

        let filters = ShoppingFilter()
        shoppingFilterTransport.filters = filters
        filters.textFilter = "textFilter"
        
        sut.categoriesBO = PromotionsGenerator.getCategoryBO()
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)
        
        // when
        sut.filter(withModel: shoppingFilterTransport)

        // then
        XCTAssert(dummyView.isCalledShowStores == true)
        
    }
    
    func test_givenFiltersCategoriesBOAndLastLocationRetrieve_whenICallFilterWithModelAndInteractorReturnsSuccess_thenICallHidePromotions() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        let shoppingFilterTransport = ShoppingFilterTransport()

        let filters = ShoppingFilter()
        shoppingFilterTransport.filters = filters
        filters.textFilter = "textFilter"
        
        sut.categoriesBO = PromotionsGenerator.getCategoryBO()
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)
        
        // when
        sut.filter(withModel: shoppingFilterTransport)

        // then
        XCTAssert(dummyView.isCalledHidePromotions == true)
        
    }
    
    func test_givenFiltersCategoriesBOAndLastLocationRetrieve_whenICallFilterWithModelAndInteractorReturnsSuccess_thenICallShowStoresWithTheSameNumberOfItemsAsStoresReturned() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        let promotions = PromotionsGenerator.getPromotionsBO()
        
        for promotion in promotions.promotions {
            promotion.promotionType.id = .discount
        }
        
        dummyInteractor.promotionsBO = promotions
        
        // given
        let shoppingFilterTransport = ShoppingFilterTransport()

        let filters = ShoppingFilter()
        shoppingFilterTransport.filters = filters
        filters.textFilter = "textFilter"
        
        sut.categoriesBO = PromotionsGenerator.getCategoryBO()
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)
        
        // when
        sut.filter(withModel: shoppingFilterTransport)

        // then
        var numberOfStoresReturned = 0
        
        for promotion in dummyInteractor.promotionsBO!.promotions {
            numberOfStoresReturned += (promotion.stores?.count ?? 0)
        }
        
        XCTAssert(dummyView.displayDataSent!.items.count == numberOfStoresReturned)
    }
    
    func test_givenFiltersCategoriesBOAndLastLocationRetrieve_whenICallFilterWithModelAndInteractorReturnsPromotionsWithStores_thenIDoNotCallShowToast() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        let shoppingFilterTransport = ShoppingFilterTransport()

        let filters = ShoppingFilter()
        shoppingFilterTransport.filters = filters
        filters.textFilter = "textFilter"
        
        sut.categoriesBO = PromotionsGenerator.getCategoryBO()
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)
        
        // when
        sut.filter(withModel: shoppingFilterTransport)

        // then
        XCTAssert(dummyView.isCalledShowToastNotStore == false)
        
    }
    
    func test_givenFiltersCategoriesBOAndLastLocationRetrieve_whenICallFilterWithModelAndInteractorReturnsPromotionsWithoutStores_thenICallShowToast() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let promotions = PromotionsGenerator.getPromotionsBO()
        
        for promotion in promotions.promotions {
            
            promotion.stores = nil
            
        }
        
        // given
        let shoppingFilterTransport = ShoppingFilterTransport()
        
        let filters = ShoppingFilter()
        shoppingFilterTransport.filters = filters
        filters.textFilter = "textFilter"
        
        sut.categoriesBO = PromotionsGenerator.getCategoryBO()
        sut.lastLocationRetrieve = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)
        
        dummyInteractor.promotionsBO = promotions
        
        // when
        sut.filter(withModel: shoppingFilterTransport)

        // then
        XCTAssert(dummyView.isCalledShowToastNotStore == true)
        
    }
    
    // MARK: - dismissedGPSHelpScreen
    
    func test_givenAny_whenICallDismissedGPSHelpScreen_thenICallViewShowCenterMap() {
        
        // given
        let dummyView = DummyView()
        sut.view = dummyView
        
        // when
        sut.dismissedGPSHelpScreen()
        
        // then
        XCTAssert(dummyView.isCalledCenterMap == true)
    }
    
    func test_givenAny_whenICallDismissedGPSHelpScreen_thenICallViewShowCenterMapWithDefaultLatitudeAndLongitude() {
        
        // given
        let dummyView = DummyView()
        sut.view = dummyView
        
        // when
        sut.dismissedGPSHelpScreen()
        
        // then
        XCTAssert(dummyView.latitude == sut.defaultLocation.latitude)
        XCTAssert(dummyView.longitude == sut.defaultLocation.longitude)
        XCTAssert(dummyView.radiousInMetersSent == Double(sut.defaultDistance * DistanceConstants.meters_to_kilometers))
        
    }
    
    func test_givenAny_whenICallDismissedGPSHelpScreen_thenICallViewHideUserLocation() {
        
        // given
        let dummyView = DummyView()
        sut.view = dummyView
        
        // when
        sut.dismissedGPSHelpScreen()
        
        // then
        XCTAssert(dummyView.isCalledHideUserLocation == true)
    }
    
    func test_givenAny_whenICallDismissedGPSHelpScreen_thenICallInteractorProvidePromotions() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        
        // when
        sut.dismissedGPSHelpScreen()
        
        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotions == true)
    }
    
    func test_givenAny_whenICallDismissedGPSHelpScreen_thenLastLocationRetrieveShouldBeFilled() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        dummyInteractor.presenter = sut
        sut.lastLocationRetrieve = nil
        
        // given
        
        // when
        sut.dismissedGPSHelpScreen()
        
        // then
        XCTAssert(sut.lastLocationRetrieve != nil)
    }
    
    func test_givenAny_whenICallDismissedGPSHelpScreen_thenLastLocationRetrieveShouldHaveLongitudeAndLatitudeDefault() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        dummyInteractor.presenter = sut
        
        // given
        
        // when
        sut.dismissedGPSHelpScreen()
        
        // then
        XCTAssert(sut.lastLocationRetrieve?.latitude == sut.defaultLocation.latitude)
        XCTAssert(sut.lastLocationRetrieve?.longitude == sut.defaultLocation.longitude)
    }
    
    func test_givenAny_whenICallDismissedGPSHelpScreen_thenICallShowLoader() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        
        // when
        sut.dismissedGPSHelpScreen()
        
        // then
        XCTAssert(dummyView.isCalledShowLoader == true)
    }
    
    func test_givenAny_whenICallDismissedGPSHelpScreen_thenICallInteractorProvidePromotionsWithDefaultValues() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        
        // when
        sut.dismissedGPSHelpScreen()
        
        // then
        XCTAssert(dummyInteractor.latitudeSent == sut.defaultLocation.latitude)
        XCTAssert(dummyInteractor.longitudeSent == sut.defaultLocation.longitude)
        XCTAssert(dummyInteractor.distanceSent == sut.defaultDistance)
        XCTAssert(dummyInteractor.lengthTypeSent == sut.lengthType)
        XCTAssert(dummyInteractor.pageSizeSent == sut.pageSize)
        XCTAssert(dummyInteractor.categoryIdSent == nil)
        XCTAssert(dummyInteractor.trendingIdSent == nil)
        
    }
    
    func test_givenWithCategoryBO_whenICallDismissedGPSHelpScreen_thenICallInteractorProvidePromotionsWithDefaultValuesAndCategoryId() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        let idType = CategoryEntity(id: "ACTIVITIES", name: "actividades", isFavourite: false)
        sut.categoryBO = CategoryBO(categoryEntity: idType)
        
        // when
        sut.dismissedGPSHelpScreen()
        
        // then
        XCTAssert(dummyInteractor.latitudeSent == sut.defaultLocation.latitude)
        XCTAssert(dummyInteractor.longitudeSent == sut.defaultLocation.longitude)
        XCTAssert(dummyInteractor.distanceSent == sut.defaultDistance)
        XCTAssert(dummyInteractor.lengthTypeSent == sut.lengthType)
        XCTAssert(dummyInteractor.pageSizeSent == sut.pageSize)
        XCTAssert(dummyInteractor.categoryIdSent == sut.categoryBO?.id.rawValue)
        XCTAssert(dummyInteractor.trendingIdSent == nil)
        
    }
    
    func test_givenWithTrendingType_whenICallDismissedGPSHelpScreen_thenICallInteractorProvidePromotionsWithDefaultValuesAndTrendingId() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        sut.trendingType = TrendingType(rawValue: "HOT")
        
        // when
        sut.dismissedGPSHelpScreen()
        
        // then
        XCTAssert(dummyInteractor.latitudeSent == sut.defaultLocation.latitude)
        XCTAssert(dummyInteractor.longitudeSent == sut.defaultLocation.longitude)
        XCTAssert(dummyInteractor.distanceSent == sut.defaultDistance)
        XCTAssert(dummyInteractor.lengthTypeSent == sut.lengthType)
        XCTAssert(dummyInteractor.pageSizeSent == sut.pageSize)
        XCTAssert(dummyInteractor.categoryIdSent == nil)
        XCTAssert(dummyInteractor.trendingIdSent == sut.trendingType?.rawValue)
        
    }
    
    func test_givenAny_whenICallDismissedGPSHelpScreenAndInteractorReturnError_thenICallHideLoader() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let errorEntity = ErrorEntity()
        errorEntity.code = "500"
        errorEntity.message = "Error 500"
        
        dummyInteractor.forceError = true
        dummyInteractor.errorBO = ErrorBO(error: errorEntity)
        
        // given
        
        // when
        sut.dismissedGPSHelpScreen()
        
        // then
        XCTAssert(dummyView.isCalledHideLoader == true)
    }
    
    func test_givenAny_whenICallDismissedGPSHelpScreenAndInteractorReturnError_thenICallHidePromotions() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let errorEntity = ErrorEntity()
        errorEntity.code = "500"
        errorEntity.message = "Error 500"
        
        dummyInteractor.forceError = true
        dummyInteractor.errorBO = ErrorBO(error: errorEntity)
        
        // given
        
        // when
        sut.dismissedGPSHelpScreen()
        
        // then
        XCTAssert(dummyView.isCalledHidePromotions == true)
    }
    
    func test_givenAny_whenICallDismissedGPSHelpScreenAndInteractorReturnError_thenErrorBOShouldBeFilled() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let errorEntity = ErrorEntity()
        errorEntity.code = "500"
        errorEntity.message = "Error 500"
        
        dummyInteractor.forceError = true
        dummyInteractor.errorBO = ErrorBO(error: errorEntity)
        
        // given
        
        // when
        sut.dismissedGPSHelpScreen()
        
        // then
        XCTAssert(sut.errorBO != nil)
        XCTAssert(sut.errorBO! === dummyInteractor.errorBO!)
    }
    
    func test_givenAny_whenICallDismissedGPSHelpScreenAndInteractorReturnError_thenICallViewShowError() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let errorEntity = ErrorEntity()
        errorEntity.code = "500"
        errorEntity.message = "Error 500"
        
        dummyInteractor.forceError = true
        dummyInteractor.errorBO = ErrorBO(error: errorEntity)
        
        // given
        
        // when
        sut.dismissedGPSHelpScreen()
        
        // then
        XCTAssert(dummyView.isCalledShowError == true)
    }
    
    func test_givenAny_whenICallDismissedGPSHelpScreenAndInteractorReturnSuccess_thenICallHideLoader() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        
        // when
        sut.dismissedGPSHelpScreen()
        
        // then
        XCTAssert(dummyView.isCalledHideLoader == true)
    }
    
    func test_givenAny_whenICallDismissedGPSHelpScreenAndInteractorReturnSuccess_thenDataPoisShouldBeFilled() {
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        
        // when
        sut.dismissedGPSHelpScreen()
        
        // then
        XCTAssert(sut.dataPois.isEmpty == false)
    }
    
    func test_givenAny_whenICallDismissedGPSHelpScreenAndInteractorReturnSuccess_thenICallShowStores() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        
        // when
        sut.dismissedGPSHelpScreen()
        
        // then
        XCTAssert(dummyView.isCalledShowStores == true)
    }
    
    func test_givenAny_whenICallDismissedGPSHelpScreenAndInteractorReturnSuccess_thenICallHidePromotions() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        // given
        
        // when
        sut.dismissedGPSHelpScreen()
        
        // then
        XCTAssert(dummyView.isCalledHidePromotions == true)
    }
    
    func test_givenAny_whenICallDismissedGPSHelpScreenAndInteractorReturnSuccess_thenICallShowStoresWithTheSameNumberOfItemsAsStoresReturned() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        let promotions = PromotionsGenerator.getPromotionsBO()
        
        for promotion in promotions.promotions {
            promotion.promotionType.id = .discount
        }
        
        dummyInteractor.promotionsBO = promotions
        
        // given
        
        // when
        sut.dismissedGPSHelpScreen()
        
        // then
        
        var numberOfStoresReturned = 0
        
        for promotion in dummyInteractor.promotionsBO!.promotions {
            numberOfStoresReturned += (promotion.stores?.count ?? 0)
        }
        
        XCTAssert(dummyView.displayDataSent!.items.count == numberOfStoresReturned)
    }
    
    func test_givenAny_whenICallDismissedGPSHelpScreenAndInteractorReturnSuccessWithoutPromotions_thenICallViewShowToastWithInfoText() {
        
        // given
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        let promotions = PromotionsGenerator.getPromotionsBO()
        promotions.promotions = [PromotionBO]()
        dummyInteractor.promotionsBO = promotions
        
        // when
        sut.dismissedGPSHelpScreen()
        
        // then
        XCTAssert(dummyView.isCalledShowToastWithInfoText == true)
    }

    // MARK: - Dummy data

    class DummyView: ShoppingMapViewProtocol, ViewProtocol {

        var isCalledShowError = false
        var isCalledShowToastWithInfoText = false
        var isCalledHideToast = false
        var isCalledCenterMap = false
        var isCalledGoToLocationSettings = false
        var isCalledHideUserLocation = false
        var isCalledShowStores = false
        var isCalledShowUserLocation = false
        var isCalledShowToastNotStore = false
        var isCalledShowLoader = false
        var isCalledHideLoader = false
        var isCallediShowPromotions = false
        var isCalledHidePromotions = false
        var isCalledShowFilterButton = false
        var isCalledShowFiltersWithDisplayData = false
        var isCalledHideFilters = false

        var errorBO: ErrorBO?

        var infoText = ""

        var latitude = -3.0
        var longitude = -3.0
        var radiousInMetersSent = -1.0
        var distanceFilterSent = 0.0

        var displayDataSent: MapDisplayData?
        var sentDisplayItemsPromotions: MapDisplayDataPromotions?
        var sentDisplayFilters: [FilterScrollDisplayData]?

        func showError(error: ModelBO) {

            errorBO = error as? ErrorBO

            isCalledShowError = true
        }

        func changeColorEditTexts() {

        }

        func showToast(withInfoText text: String) {

            infoText = text

            isCalledShowToastWithInfoText = true

            if text == Localizables.promotions.key_promotions_stores_zero_result_text {
                isCalledShowToastNotStore = true
            }
        }

        func hideToast() {
            isCalledHideToast = true
        }

        func centerMap(withLatitude latitude: Double, andLongitude longitude: Double, withRadiousInMeters radiousInMeters: Double) {

            self.latitude = latitude
            self.longitude = longitude
            radiousInMetersSent = radiousInMeters

            isCalledCenterMap = true

        }

        func goToLocationSettings() {

            isCalledGoToLocationSettings = true

        }

        func hideUserLocation() {
            isCalledHideUserLocation = true
        }

        func showStores(withDisplayData displayData: MapDisplayData) {

            isCalledShowStores = true
            displayDataSent = displayData
        }

        func showUserLocation() {
            isCalledShowUserLocation = true
        }

        func showLoader() {
            isCalledShowLoader = true
        }

        func hideLoader() {
            isCalledHideLoader = true
        }

        func showPromotions(withDisplayData displayData: MapDisplayDataPromotions) {
            isCallediShowPromotions = true
            sentDisplayItemsPromotions = displayData
        }

        func hidePromotions() {
            isCalledHidePromotions = true
        }

        func showFilterButton() {
            isCalledShowFilterButton = true
        }

        func showFilters(withDisplayData displayData: [FilterScrollDisplayData]) {
            isCalledShowFiltersWithDisplayData = true
            sentDisplayFilters = displayData
        }

        func hideFilters() {
            isCalledHideFilters = true
        }
    }

    class DummyBusManager: BusManager {

        var isCalledNavigateNextShoppingMapScreen = false
        var isCalledNavigateToFilterScreen = false
        var isCalledNavigateToShoppingDetail = false
        var isCalledSetRootWhenNoSession = false
        var isCalledExpiredSession = false
        var isCalledNavigatedToShoppingMapList = false
        var filterTransportSent: ShoppingFilterTransport?
        var shoppiongMapListTransportSent: ShoppingMapListTransport?
        var promotionDetailTransportSent: PromotionDetailTransport?

        override init() {
            super.init()
            routerFactory = RouterInitFactory()
        }

        override func navigateScreen<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {
            if tag == ShoppingMapPageReaction.ROUTER_TAG && event === ShoppingMapPageReaction.EVENT_NAV_TO_NEXT_SCREEN {
                isCalledNavigateNextShoppingMapScreen = true
            } else if tag == ShoppingMapPageReaction.ROUTER_TAG && event === ShoppingMapPageReaction.EVENT_NAV_TO_FILTER_SCREEN {
                isCalledNavigateToFilterScreen = true
                filterTransportSent = values as? ShoppingFilterTransport
            } else if tag == ShoppingMapPageReaction.ROUTER_TAG && event === ShoppingMapPageReaction.EVENT_NAV_TO_SHOPPING_MAP_LIST {
                isCalledNavigatedToShoppingMapList = true
                shoppiongMapListTransportSent = values as? ShoppingMapListTransport
            } else if tag == ShoppingMapPageReaction.ROUTER_TAG && event === ShoppingMapPageReaction.EVENT_NAV_TO_PROMOTION_DETAIL_SCREEN {
                isCalledNavigateToShoppingDetail = true
                promotionDetailTransportSent = values as? PromotionDetailTransport
            }
        }

        override func expiredSession() {
            isCalledExpiredSession = true
        }

    }

    class DummyInteractor: ShoppingMapInteractorProtocol {

        var isCalledProvidePromotions = false
        var isCalledProvidePromotionsWithFilters = false

        var geoLocationBO: GeoLocationBO!

        var promotionsBO: PromotionsBO!
        var responseStatus = 200

        var latitudeSent: Double?
        var longitudeSent: Double?
        var distanceSent: Int?
        var lengthTypeSent: String?
        var categoryIdSent: String?
        var trendingIdSent: String?
        var pageSizeSent: Int?

        var errorBO: ErrorBO?
        var forceError = false

        var filtersSent: ShoppingFilter?

        var presenter: ShoppingMapPresenter<DummyView>?

        func providePromotions(byParams params: ShoppingParamsTransport) -> Observable <ModelBO> {

            latitudeSent = params.location?.geolocation.latitude
            longitudeSent = params.location?.geolocation.longitude
            distanceSent = params.location?.distanceParam?.distance
            lengthTypeSent = params.location?.distanceParam?.lengthType
            categoryIdSent = params.categoryId
            trendingIdSent = params.trendingId
            pageSizeSent = params.pageSize

            isCalledProvidePromotions = true

            if !forceError {

                if promotionsBO == nil {
                    promotionsBO = PromotionsGenerator.getPromotionsBO()
                }

                promotionsBO.status = responseStatus

                return Observable <ModelBO>.just(promotionsBO!)
            } else {

                if errorBO == nil {
                    errorBO = ErrorBO(error: ErrorEntity(message: "Error"))
                    errorBO!.isErrorShown = false
                }

                return Observable.error(ServiceError.GenericErrorBO(error: errorBO!))

            }

        }

        func providePromotions(byParams params: ShoppingParamsTransport, withFilters filters: ShoppingFilter?) -> Observable<ModelBO> {

            isCalledProvidePromotionsWithFilters = true

            geoLocationBO = params.location?.geolocation
            distanceSent = params.location?.distanceParam?.distance
            lengthTypeSent = params.location?.distanceParam?.lengthType
            filtersSent = filters
            pageSizeSent = params.pageSize

            if !forceError {

                if promotionsBO == nil {
                    promotionsBO = PromotionsGenerator.getPromotionsBO()
                }

                promotionsBO.status = responseStatus

                return Observable <ModelBO>.just(promotionsBO!)
            } else {

                if errorBO == nil {
                    errorBO = ErrorBO(error: ErrorEntity(message: "Error"))
                    errorBO!.isErrorShown = false
                }

                return Observable.error(ServiceError.GenericErrorBO(error: errorBO!))

            }

        }

    }

    class DummyShoppingFilter: ShoppingFilter {

        var isCalledRetrieveLiterals = false
        var isCalledRemoveByFilterType = false

        var defaultLiterals: [(title: String, key: String)]?
        var sentFilterType: String?

        override func retrieveLiterals(withCategories categoriesBO: CategoriesBO) -> [(title: String, key: String)] {

            isCalledRetrieveLiterals = true

            if let defaultLiterals = defaultLiterals {
                return defaultLiterals
            } else {
                return super.retrieveLiterals(withCategories: categoriesBO)
            }

        }

        override func remove(byFilterType filterType: String) {

            isCalledRemoveByFilterType = true
            sentFilterType = filterType

        }

    }

}
