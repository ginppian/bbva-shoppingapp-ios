//
//  MapDisplayDataItemTests.swift
//  shoppingapp
//
//  Created by jesus.martinez on 12/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#endif

class MapDisplayDataItemTests: XCTestCase {

    // MARK: Init

    // MARK: 1 Promotion --> Normal Poi

    func test_givenDataPoiWithOnlyOnePromotionWithPromotionTypeDifferentThanUnknowm_whenICallInit_thenReturnNotNilAndShouldHaveTheCorrectValue() {

        // given

        let promotion = PromotionsGenerator.getPromotionsBO().promotions[0]
        promotion.promotionType.id = .discount
        promotion.categories![0].id = .unknown

        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        dataPoi.append(promotion: promotion)

        // when
        let sut = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)

        // then
        XCTAssert(sut != nil)
        XCTAssert(sut!.idStores == dataPoi.storeIds)
        XCTAssert(sut?.latitude == dataPoi.latitude)
        XCTAssert(sut?.longitude == dataPoi.longitude)
        XCTAssert(sut?.imageColor == .BBVAWHITE)
        XCTAssert(sut?.title == nil)
        XCTAssert(sut?.borderColors == nil)
        XCTAssert(sut?.backgroundColor == .BBVADARKPINK)
        XCTAssert(sut?.imageNamed == ConstantsImages.Promotions.promotion)

    }

    func test_givenDataPoiWithOnlyOnePromotionWithAPromotionDiscount_whenICallInit_thenColorShouldBeDarkPink() {

        // given
        let promotion = PromotionsGenerator.getPromotionsBO().promotions[0]
        promotion.promotionType.id = .discount

        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        dataPoi.append(promotion: promotion)

        // when
        let sut = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)

        // then
        XCTAssert(sut?.backgroundColor == .BBVADARKPINK)

    }

    func test_givenDataPoiWithOnlyOnePromotionWithAPromotionToMonths_whenICallInit_thenColorShouldBeMediumYellow() {

        // given
        let promotion = PromotionsGenerator.getPromotionsBO().promotions[0]
        promotion.promotionType.id = .toMonths

        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        dataPoi.append(promotion: promotion)

        // when
        let sut = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)

        // then
        XCTAssert(sut?.backgroundColor == .BBVAMEDIUMYELLOW)

    }

    func test_givenDataPoiWithOnlyOnePromotionWithAPromotionPoints_whenICallInit_thenColorShouldBeDarkAqua() {

        // given
        let promotion = PromotionsGenerator.getPromotionsBO().promotions[0]
        promotion.promotionType.id = .point

        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        dataPoi.append(promotion: promotion)

        // when
        let sut = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)

        // then
        XCTAssert(sut?.backgroundColor == .DARKAQUA)
    }

    func test_givenDataPoiWithOnlyOnePromotionWithAPromotionUnknowm_whenICallInit_thenReturnNil() {

        // given
        let promotion = PromotionsGenerator.getPromotionsBO().promotions[0]
        promotion.promotionType.id = .unknown

        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        dataPoi.append(promotion: promotion)

        // when
        let sut = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)

        // then
        XCTAssert(sut == nil)

    }

    func test_givenDataPoiWithOnlyOnePromotionWithAPromotionWithCategoryActivities_whenICallInit_thenImageShouldBeTheCorrect() {

        // given
        let promotion = PromotionsGenerator.getPromotionsBO().promotions[0]
        promotion.categories![0].id = .activities

        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        dataPoi.append(promotion: promotion)

        // when
        let sut = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)

        // then
        XCTAssert(sut?.imageNamed == ConstantsImages.Promotions.activities)
    }

    func test_givenDataPoiWithOnlyOnePromotionWithAPromotionWithCategoryAuto_whenICallInit_thenImageShouldBeTheCorrect() {

        // given
        let promotion = PromotionsGenerator.getPromotionsBO().promotions[0]
        promotion.categories![0].id = .auto

        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        dataPoi.append(promotion: promotion)

        // when
        let sut = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)

        // then
        XCTAssert(sut?.imageNamed == ConstantsImages.Promotions.auto)
    }

    func test_givenDataPoiWithOnlyOnePromotionWithAPromotionWithCategoryBeautyAndGragances_whenICallInit_thenImageShouldBeTheCorrect() {

        // given
        let promotion = PromotionsGenerator.getPromotionsBO().promotions[0]
        promotion.categories![0].id = .beautyAndGragances

        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        dataPoi.append(promotion: promotion)

        // when
        let sut = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)

        // then
        XCTAssert(sut?.imageNamed == ConstantsImages.Promotions.beautyAndFragances)
    }

    func test_givenDataPoiWithOnlyOnePromotionWithAPromotionWithCategoryDietAndNutrition_whenICallInit_thenImageShouldBeTheCorrect() {

        // given
        let promotion = PromotionsGenerator.getPromotionsBO().promotions[0]
        promotion.categories![0].id = .dietAndNutrition

        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        dataPoi.append(promotion: promotion)

        // when
        let sut = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)

        // then
        XCTAssert(sut?.imageNamed == ConstantsImages.Promotions.diet)
    }

    func test_givenDataPoiWithOnlyOnePromotionWithAPromotionWithCategoryFoodAndGourmet_whenICallInit_thenImageShouldBeTheCorrect() {

        // given
        let promotion = PromotionsGenerator.getPromotionsBO().promotions[0]
        promotion.categories![0].id = .foodAndGourmet

        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        dataPoi.append(promotion: promotion)

        // when
        let sut = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)

        // then
        XCTAssert(sut?.imageNamed == ConstantsImages.Promotions.foodAndGourmet)
    }

    func test_givenDataPoiWithOnlyOnePromotionWithAPromotionWithCategoryHobbies_whenICallInit_thenImageShouldBeTheCorrect() {

        // given
        let promotion = PromotionsGenerator.getPromotionsBO().promotions[0]
        promotion.categories![0].id = .hobbies

        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        dataPoi.append(promotion: promotion)

        // when
        let sut = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)

        // then
        XCTAssert(sut?.imageNamed == ConstantsImages.Promotions.hobbies)
    }

    func test_givenDataPoiWithOnlyOnePromotionWithAPromotionWithCategoryHome_whenICallInit_thenImageShouldBeTheCorrect() {

        // given
        let promotion = PromotionsGenerator.getPromotionsBO().promotions[0]
        promotion.categories![0].id = .home

        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        dataPoi.append(promotion: promotion)

        // when
        let sut = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)

        // then
        XCTAssert(sut?.imageNamed == ConstantsImages.Promotions.home)
    }

    func test_givenDataPoiWithOnlyOnePromotionWithAPromotionWithCategoryLearning_whenICallInit_thenImageShouldBeTheCorrect() {

        // given
        let promotion = PromotionsGenerator.getPromotionsBO().promotions[0]
        promotion.categories![0].id = .learning

        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        dataPoi.append(promotion: promotion)

        // when
        let sut = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)

        // then
        XCTAssert(sut?.imageNamed == ConstantsImages.Promotions.learning)
    }

    func test_givenDataPoiWithOnlyOnePromotionWithAPromotionWithCategoryOther_whenICallInit_thenImageShouldBeTheCorrect() {

        // given
        let promotion = PromotionsGenerator.getPromotionsBO().promotions[0]
        promotion.categories![0].id = .other

        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        dataPoi.append(promotion: promotion)

        // when
        let sut = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)

        // then
        XCTAssert(sut?.imageNamed == ConstantsImages.Promotions.other)
    }

    func test_givenDataPoiWithOnlyOnePromotionWithAPromotionWithCategoryRent_whenICallInit_thenImageShouldBeTheCorrect() {

        // given
        let promotion = PromotionsGenerator.getPromotionsBO().promotions[0]
        promotion.categories![0].id = .rent

        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        dataPoi.append(promotion: promotion)

        // when
        let sut = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)

        // then
        XCTAssert(sut?.imageNamed == ConstantsImages.Promotions.rent)
    }

    func test_givenDataPoiWithOnlyOnePromotionWithAPromotionWithCategoryRestaurant_whenICallInit_thenImageShouldBeTheCorrect() {

        // given
        let promotion = PromotionsGenerator.getPromotionsBO().promotions[0]
        promotion.categories![0].id = .restaurants

        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        dataPoi.append(promotion: promotion)

        // when
        let sut = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)

        // then
        XCTAssert(sut?.imageNamed == ConstantsImages.Promotions.restaurant)
    }

    func test_givenDataPoiWithOnlyOnePromotionWithAPromotionWithCategoryShopping_whenICallInit_thenImageShouldBeTheCorrect() {

        // given
        let promotion = PromotionsGenerator.getPromotionsBO().promotions[0]
        promotion.categories![0].id = .shopping

        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        dataPoi.append(promotion: promotion)

        // when
        let sut = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)

        // then
        XCTAssert(sut?.imageNamed == ConstantsImages.Promotions.shopping)
    }

    func test_givenDataPoiWithOnlyOnePromotionWithAPromotionWithCategoryhows_whenICallInit_thenImageShouldBeTheCorrect() {

        // given
        let promotion = PromotionsGenerator.getPromotionsBO().promotions[0]
        promotion.categories![0].id = .shows

        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        dataPoi.append(promotion: promotion)

        // when
        let sut = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)

        // then
        XCTAssert(sut?.imageNamed == ConstantsImages.Promotions.shows)
    }

    func test_givenDataPoiWithOnlyOnePromotionWithAPromotionWithCategoryTechnology_whenICallInit_thenImageShouldBeTheCorrect() {

        // given
        let promotion = PromotionsGenerator.getPromotionsBO().promotions[0]
        promotion.categories![0].id = .technology

        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        dataPoi.append(promotion: promotion)

        // when
        let sut = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)

        // then
        XCTAssert(sut?.imageNamed == ConstantsImages.Promotions.technology)
    }

    func test_givenDataPoiWithOnlyOnePromotionWithAPromotionWithCategoryTravel_whenICallInit_thenImageShouldBeTheCorrect() {

        // given
        let promotion = PromotionsGenerator.getPromotionsBO().promotions[0]
        promotion.categories![0].id = .travel

        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        dataPoi.append(promotion: promotion)

        // when
        let sut = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)

        // then
        XCTAssert(sut?.imageNamed == ConstantsImages.Promotions.travel)
    }

    func test_givenDataPoiWithOnlyOnePromotionWithAPromotionWithCategoryUnknown_whenICallInit_thenImageShouldBeTheCorrect() {

        // given
        let promotion = PromotionsGenerator.getPromotionsBO().promotions[0]
        promotion.categories![0].id = .unknown

        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        dataPoi.append(promotion: promotion)

        // when
        let sut = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)

        // then
        XCTAssert(sut?.imageNamed == ConstantsImages.Promotions.promotion)
    }

    func test_givenDataPoiWithOnlyOnePromotionWithAPromotionAndCategoryActivities_whenICallInit_thenImageShouldBeTheCorrectForCategoryAsParamter() {

        // given
        let promotion = PromotionsGenerator.getPromotionsBO().promotions[0]
        promotion.categories![0].id = .unknown
        let category = PromotionsGenerator.getCategoryBO().categories[0]
        category.id = .activities

        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        dataPoi.append(promotion: promotion)

        // when
        let sut = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: category)

        // then
        XCTAssert(sut?.imageNamed == ConstantsImages.Promotions.activities)
    }

    func test_givenDataPoiWithOnlyOnePromotionWithAPromotionAndCategoryAuto_whenICallInit_thenImageShouldBeTheCorrectForCategoryAsParamter() {

        // given
        let promotion = PromotionsGenerator.getPromotionsBO().promotions[0]
        promotion.categories![0].id = .unknown
        let category = PromotionsGenerator.getCategoryBO().categories[0]
        category.id = .auto

        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        dataPoi.append(promotion: promotion)

        // when
        let sut = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: category)

        // then
        XCTAssert(sut?.imageNamed == ConstantsImages.Promotions.auto)
    }

    func test_givenDataPoiWithOnlyOnePromotionWithAPromotionAndCategoryBeautyAndGragances_whenICallInit_thenImageShouldBeTheCorrectForCategoryAsParamter() {

        // given
        let promotion = PromotionsGenerator.getPromotionsBO().promotions[0]
        promotion.categories![0].id = .unknown
        let category = PromotionsGenerator.getCategoryBO().categories[0]
        category.id = .beautyAndGragances

        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        dataPoi.append(promotion: promotion)

        // when
        let sut = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: category)

        // then
        XCTAssert(sut?.imageNamed == ConstantsImages.Promotions.beautyAndFragances)
    }

    func test_givenDataPoiWithOnlyOnePromotionWithAPromotionAndCategoryDietAndNutrition_whenICallInit_thenImageShouldBeTheCorrectForCategoryAsParamter() {

        // given
        let promotion = PromotionsGenerator.getPromotionsBO().promotions[0]
        promotion.categories![0].id = .unknown
        let category = PromotionsGenerator.getCategoryBO().categories[0]
        category.id = .dietAndNutrition

        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        dataPoi.append(promotion: promotion)

        // when
        let sut = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: category)

        // then
        XCTAssert(sut?.imageNamed == ConstantsImages.Promotions.diet)
    }

    func test_givenDataPoiWithOnlyOnePromotionWithAPromotionAndCategoryFoodAndGourmet_whenICallInit_thenImageShouldBeTheCorrectForCategoryAsParamter() {

        // given
        let promotion = PromotionsGenerator.getPromotionsBO().promotions[0]
        promotion.categories![0].id = .unknown
        let category = PromotionsGenerator.getCategoryBO().categories[0]
        category.id = .foodAndGourmet

        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        dataPoi.append(promotion: promotion)

        // when
        let sut = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: category)

        // then
        XCTAssert(sut?.imageNamed == ConstantsImages.Promotions.foodAndGourmet)
    }

    func test_givenDataPoiWithOnlyOnePromotionWithAPromotionAndCategoryHobbies_whenICallInit_thenImageShouldBeTheCorrectForCategoryAsParamter() {

        // given
        let promotion = PromotionsGenerator.getPromotionsBO().promotions[0]
        promotion.categories![0].id = .unknown
        let category = PromotionsGenerator.getCategoryBO().categories[0]
        category.id = .hobbies

        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        dataPoi.append(promotion: promotion)

        // when
        let sut = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: category)

        // then
        XCTAssert(sut?.imageNamed == ConstantsImages.Promotions.hobbies)
    }

    func test_givenDataPoiWithOnlyOnePromotionWithAPromotionAndCategoryHome_whenICallInit_thenImageShouldBeTheCorrectForCategoryAsParamter() {

        // given
        let promotion = PromotionsGenerator.getPromotionsBO().promotions[0]
        promotion.categories![0].id = .unknown
        let category = PromotionsGenerator.getCategoryBO().categories[0]
        category.id = .home

        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        dataPoi.append(promotion: promotion)

        // when
        let sut = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: category)

        // then
        XCTAssert(sut?.imageNamed == ConstantsImages.Promotions.home)
    }

    func test_givenDataPoiWithOnlyOnePromotionWithAPromotionAndCategoryLearning_whenICallInit_thenImageShouldBeTheCorrectForCategoryAsParamter() {

        // given
        let promotion = PromotionsGenerator.getPromotionsBO().promotions[0]
        promotion.categories![0].id = .unknown
        let category = PromotionsGenerator.getCategoryBO().categories[0]
        category.id = .learning

        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        dataPoi.append(promotion: promotion)

        // when
        let sut = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: category)

        // then
        XCTAssert(sut?.imageNamed == ConstantsImages.Promotions.learning)
    }

    func test_givenDataPoiWithOnlyOnePromotionWithAPromotionAndCategoryOther_whenICallInit_thenImageShouldBeTheCorrectForCategoryAsParamter() {

        // given
        let promotion = PromotionsGenerator.getPromotionsBO().promotions[0]
        promotion.categories![0].id = .unknown
        let category = PromotionsGenerator.getCategoryBO().categories[0]
        category.id = .other

        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        dataPoi.append(promotion: promotion)

        // when
        let sut = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: category)

        // then
        XCTAssert(sut?.imageNamed == ConstantsImages.Promotions.other)
    }

    func test_givenDataPoiWithOnlyOnePromotionWithAPromotionAndCategoryRent_whenICallInit_thenImageShouldBeTheCorrectForCategoryAsParamter() {

        // given
        let promotion = PromotionsGenerator.getPromotionsBO().promotions[0]
        promotion.categories![0].id = .unknown
        let category = PromotionsGenerator.getCategoryBO().categories[0]
        category.id = .rent

        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        dataPoi.append(promotion: promotion)

        // when
        let sut = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: category)

        // then
        XCTAssert(sut?.imageNamed == ConstantsImages.Promotions.rent)
    }

    func test_givenDataPoiWithOnlyOnePromotionWithAPromotionAndCategoryRestaurants_whenICallInit_thenImageShouldBeTheCorrectForCategoryAsParamter() {

        // given
        let promotion = PromotionsGenerator.getPromotionsBO().promotions[0]
        promotion.categories![0].id = .unknown
        let category = PromotionsGenerator.getCategoryBO().categories[0]
        category.id = .restaurants

        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        dataPoi.append(promotion: promotion)

        // when
        let sut = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: category)

        // then
        XCTAssert(sut?.imageNamed == ConstantsImages.Promotions.restaurant)
    }

    func test_givenDataPoiWithOnlyOnePromotionWithAPromotionAndCategoryShopping_whenICallInit_thenImageShouldBeTheCorrectForCategoryAsParamter() {

        // given
        let promotion = PromotionsGenerator.getPromotionsBO().promotions[0]
        promotion.categories![0].id = .unknown
        let category = PromotionsGenerator.getCategoryBO().categories[0]
        category.id = .shopping

        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        dataPoi.append(promotion: promotion)

        // when
        let sut = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: category)

        // then
        XCTAssert(sut?.imageNamed == ConstantsImages.Promotions.shopping)
    }

    func test_givenDataPoiWithOnlyOnePromotionWithAPromotionAndCategoryShows_whenICallInit_thenImageShouldBeTheCorrectForCategoryAsParamter() {

        // given
        let promotion = PromotionsGenerator.getPromotionsBO().promotions[0]
        promotion.categories![0].id = .unknown
        let category = PromotionsGenerator.getCategoryBO().categories[0]
        category.id = .shows

        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        dataPoi.append(promotion: promotion)

        // when
        let sut = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: category)

        // then
        XCTAssert(sut?.imageNamed == ConstantsImages.Promotions.shows)
    }

    func test_givenDataPoiWithOnlyOnePromotionWithAPromotionAndCategoryTechnology_whenICallInit_thenImageShouldBeTheCorrectForCategoryAsParamter() {

        // given
        let promotion = PromotionsGenerator.getPromotionsBO().promotions[0]
        promotion.categories![0].id = .unknown
        let category = PromotionsGenerator.getCategoryBO().categories[0]
        category.id = .technology

        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        dataPoi.append(promotion: promotion)

        // when
        let sut = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: category)

        // then
        XCTAssert(sut?.imageNamed == ConstantsImages.Promotions.technology)
    }

    func test_givenDataPoiWithOnlyOnePromotionWithAPromotionAndCategoryTravel_whenICallInit_thenImageShouldBeTheCorrectForCategoryAsParamter() {

        // given
        let promotion = PromotionsGenerator.getPromotionsBO().promotions[0]
        promotion.categories![0].id = .unknown
        let category = PromotionsGenerator.getCategoryBO().categories[0]
        category.id = .travel

        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        dataPoi.append(promotion: promotion)

        // when
        let sut = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: category)

        // then
        XCTAssert(sut?.imageNamed == ConstantsImages.Promotions.travel)
    }

    func test_givenDataPoiWithOnlyOnePromotionWithAPromotionAndCategoryUnknown_whenICallInit_thenImageShouldBeTheCorrectForCategoryAsParamter() {

        // given
        let promotion = PromotionsGenerator.getPromotionsBO().promotions[0]
        promotion.categories![0].id = .activities
        let category = PromotionsGenerator.getCategoryBO().categories[0]
        category.id = .unknown

        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        dataPoi.append(promotion: promotion)

        // when
        let sut = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: category)

        // then
        XCTAssert(sut?.imageNamed == ConstantsImages.Promotions.promotion)
    }

    // MARK: Multiples promotions

    func test_givenDataPoiWithMoreThanOnePromotion_whenICallInit_thenHaveDefaultValuesForMapDisplayDataItemWithMultiplesPromotions() {

        // given
        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        dataPoi.append(promotion: PromotionsGenerator.getPromotionsBOWithStores().promotions[0])
        dataPoi.append(promotion: PromotionsGenerator.getPromotionsBOWithStores().promotions[1])

        // when
        let sut = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)

        // then
        XCTAssert(sut!.idStores == dataPoi.storeIds)
        XCTAssert(sut?.backgroundColor == .BBVAWHITE)
        XCTAssert(sut?.imageColor == .BBVA600)
        XCTAssert(sut?.latitude == dataPoi.latitude)
        XCTAssert(sut?.longitude == dataPoi.longitude)

    }

    func test_givenDataPoiWithMoreThanOnePromotionWithAllPromotionTypeUnknown_whenICallInit_thenReturnNil() {

        // given
        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        let promo1 = PromotionsGenerator.getPromotionsBOWithStores().promotions[0]
        promo1.promotionType.id = .unknown
        let promo2 = PromotionsGenerator.getPromotionsBOWithStores().promotions[1]
        promo2.promotionType.id = .unknown

        dataPoi.append(promotion: promo1)
        dataPoi.append(promotion: promo2)

        // when
        let sut = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)

        // then
        XCTAssert(sut == nil)

    }

    func test_givenDataPoiWithMoreThanOnePromotionWithOnlyDiscountPromotionType_whenICallInit_thenBorderColorsShouldContainsOnlyColorDarkPink() {

        // given
        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        let promo1 = PromotionsGenerator.getPromotionsBOWithStores().promotions[0]
        promo1.promotionType.id = .discount
        let promo2 = PromotionsGenerator.getPromotionsBOWithStores().promotions[1]
        promo2.promotionType.id = .discount

        dataPoi.append(promotion: promo1)
        dataPoi.append(promotion: promo2)

        // when
        let sut = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)

        // then
        XCTAssert(sut?.borderColors?.count == 1)
        XCTAssert(sut?.borderColors!.contains(.BBVADARKPINK) == true)

    }

    func test_givenDataPoiWithMoreThanOnePromotionWithOnlyToMonthsPromotionType_whenICallInit_thenBorderColorsShouldContainsOnlyColorYellow() {

        // given
        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        let promo1 = PromotionsGenerator.getPromotionsBOWithStores().promotions[0]
        promo1.promotionType.id = .toMonths
        let promo2 = PromotionsGenerator.getPromotionsBOWithStores().promotions[1]
        promo2.promotionType.id = .toMonths

        dataPoi.append(promotion: promo1)
        dataPoi.append(promotion: promo2)

        // when
        let sut = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)

        // then
        XCTAssert(sut?.borderColors?.count == 1)
        XCTAssert(sut?.borderColors!.contains(.BBVAMEDIUMYELLOW) == true)

    }

    func test_givenDataPoiWithMoreThanOnePromotionWithOnlyPointsPromotionType_whenICallInit_thenBorderColorsShouldContainsOnlyDarkAqua() {

        // given
        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        let promo1 = PromotionsGenerator.getPromotionsBOWithStores().promotions[0]
        promo1.promotionType.id = .point
        let promo2 = PromotionsGenerator.getPromotionsBOWithStores().promotions[1]
        promo2.promotionType.id = .point

        dataPoi.append(promotion: promo1)
        dataPoi.append(promotion: promo2)

        // when
        let sut = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)

        // then
        XCTAssert(sut?.borderColors?.count == 1)
        XCTAssert(sut?.borderColors!.contains(.DARKAQUA) == true)

    }

    func test_givenDataPoiWithMoreThanOnePromotionWithOnlyThreeDifferentPromotionTypes_whenICallInit_thenHaveTheThreeColors() {

        // given
        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        let promo1 = PromotionsGenerator.getPromotionsBOWithStores().promotions[0]
        promo1.promotionType.id = .toMonths
        let promo2 = PromotionsGenerator.getPromotionsBOWithStores().promotions[1]
        promo2.promotionType.id = .point
        let promo3 = PromotionsGenerator.getPromotionsBOWithStores().promotions[2]
        promo3.promotionType.id = .discount

        dataPoi.append(promotion: promo1)
        dataPoi.append(promotion: promo2)
        dataPoi.append(promotion: promo3)

        // when
        let sut = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)

        // then
        XCTAssert(sut?.borderColors?.count == 3)
        XCTAssert(sut?.borderColors!.contains(.BBVAMEDIUMYELLOW) == true)
        XCTAssert(sut?.borderColors!.contains(.DARKAQUA) == true)
        XCTAssert(sut?.borderColors!.contains(.BBVADARKPINK) == true)

    }

    func test_givenDataPoiWithMoreThanOnePromotionWithSomePromotionTypeDuplicate_whenICallInit_thenNotHaveDuplicateColors() {

        // given
        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        let promo1 = PromotionsGenerator.getPromotionsBOWithStores().promotions[0]
        promo1.promotionType.id = .discount
        let promo2 = PromotionsGenerator.getPromotionsBOWithStores().promotions[1]
        promo2.promotionType.id = .point
        let promo3 = PromotionsGenerator.getPromotionsBOWithStores().promotions[2]
        promo3.promotionType.id = .discount

        dataPoi.append(promotion: promo1)
        dataPoi.append(promotion: promo2)
        dataPoi.append(promotion: promo3)

        // when
        let sut = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)

        // then
        XCTAssert(sut?.borderColors?.count == 2)
        XCTAssert(sut?.borderColors!.contains(.DARKAQUA) == true)
        XCTAssert(sut?.borderColors!.contains(.BBVADARKPINK) == true)

    }

    func test_givenDataPoiWithMoreThanOnePromotionWithAllCategoriesDifferent_whenICallInit_thenTitleShouldBeFilledAndImageNamedShouldBeNil() {

        // given
        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        let promo1 = PromotionsGenerator.getPromotionsBOWithStores().promotions[0]
        promo1.categories = [CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.activities.rawValue, name: "Actividades", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.auto.rawValue, name: "auto", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.beautyAndGragances.rawValue, name: "beautyAndGragances", isFavourite: nil))]

        let promo2 = PromotionsGenerator.getPromotionsBOWithStores().promotions[1]
        promo2.categories = [CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.dietAndNutrition.rawValue, name: "dietAndNutrition", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.foodAndGourmet.rawValue, name: "foodAndGourmet", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.hobbies.rawValue, name: "hobbies", isFavourite: nil))]

        let promo3 = PromotionsGenerator.getPromotionsBOWithStores().promotions[2]
        promo3.categories = [CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.auto.rawValue, name: "auto", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.bar.rawValue, name: "bar", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.bookStore.rawValue, name: "bookStore", isFavourite: nil))]

        dataPoi.append(promotion: promo1)
        dataPoi.append(promotion: promo2)
        dataPoi.append(promotion: promo3)

        // when
        let sut = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)

        // then
        XCTAssert(sut?.title == MapDisplayDataItem.defaultTitle)
        XCTAssert(sut?.imageNamed == nil)

    }

    func test_givenDataPoiWithMoreThanOnePromotionWithAtLeastOneCategoryTypeEqualAndStoreIdsDifferent_whenICallInit_thenTitleShouldBeFilledAndImageNamedShouldBeNil() {

        var storeEntity1 = StoresEntity()
        storeEntity1.id = "1"
        var storeEntity2 = StoresEntity()
        storeEntity2.id = "2"
        var storeEntity3 = StoresEntity()
        storeEntity3.id = "3"
        var storeEntity4 = StoresEntity()
        storeEntity4.id = "4"

        // given
        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        let promo1 = PromotionsGenerator.getPromotionsBOWithStores().promotions[0]
        promo1.categories = [CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.activities.rawValue, name: "Actividades", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.auto.rawValue, name: "auto", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.beautyAndGragances.rawValue, name: "beautyAndGragances", isFavourite: nil))]
        promo1.stores = [StoreBO(storeEntity: storeEntity1),
                         StoreBO(storeEntity: storeEntity2),
                         StoreBO(storeEntity: storeEntity3)]

        let promo2 = PromotionsGenerator.getPromotionsBOWithStores().promotions[1]
        promo2.categories = [CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.dietAndNutrition.rawValue, name: "dietAndNutrition", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.foodAndGourmet.rawValue, name: "foodAndGourmet", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.auto.rawValue, name: "auto", isFavourite: nil))]
        promo2.stores = [StoreBO(storeEntity: storeEntity1),
                         StoreBO(storeEntity: storeEntity2),
                         StoreBO(storeEntity: storeEntity3)]

        let promo3 = PromotionsGenerator.getPromotionsBOWithStores().promotions[2]
        promo3.categories = [CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.auto.rawValue, name: "auto", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.bar.rawValue, name: "bar", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.bookStore.rawValue, name: "bookStore", isFavourite: nil))]
        promo3.stores = [StoreBO(storeEntity: storeEntity1),
                         StoreBO(storeEntity: storeEntity2),
                         StoreBO(storeEntity: storeEntity4)]

        dataPoi.append(promotion: promo1)
        dataPoi.append(promotion: promo2)
        dataPoi.append(promotion: promo3)

        // when
        let sut = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)

        // then
        XCTAssert(sut?.title == MapDisplayDataItem.defaultTitle)
        XCTAssert(sut?.imageNamed == nil)

    }

    func test_givenDataPoiWithMoreThanOnePromotionWithAtLeastOneCategoryTypeEqualAndAllStoresEqualsAndACategory_whenICallInit_thenTitleShouldBeNilAndImageNamedShouldAppropiateForCategory() {

        var storeEntity1 = StoresEntity()
        storeEntity1.id = "1"
        var storeEntity2 = StoresEntity()
        storeEntity2.id = "2"
        var storeEntity3 = StoresEntity()
        storeEntity3.id = "3"
        var storeEntity4 = StoresEntity()
        storeEntity4.id = "4"

        // given
        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "1")
        dataPoi.append(storeId: "3")
        let promo1 = PromotionsGenerator.getPromotionsBOWithStores().promotions[0]
        promo1.categories = [CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.activities.rawValue, name: "Actividades", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.auto.rawValue, name: "auto", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.beautyAndGragances.rawValue, name: "beautyAndGragances", isFavourite: nil))]
        promo1.stores = [StoreBO(storeEntity: storeEntity1),
                         StoreBO(storeEntity: storeEntity4),
                         StoreBO(storeEntity: storeEntity2)]

        let promo2 = PromotionsGenerator.getPromotionsBOWithStores().promotions[1]
        promo2.categories = [CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.dietAndNutrition.rawValue, name: "dietAndNutrition", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.foodAndGourmet.rawValue, name: "foodAndGourmet", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.auto.rawValue, name: "auto", isFavourite: nil))]
        promo2.stores = [StoreBO(storeEntity: storeEntity4),
                         StoreBO(storeEntity: storeEntity2),
                         StoreBO(storeEntity: storeEntity1)]

        let promo3 = PromotionsGenerator.getPromotionsBOWithStores().promotions[2]
        promo3.categories = [CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.auto.rawValue, name: "auto", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.bar.rawValue, name: "bar", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.bookStore.rawValue, name: "bookStore", isFavourite: nil))]
        promo3.stores = [StoreBO(storeEntity: storeEntity1),
                         StoreBO(storeEntity: storeEntity2),
                         StoreBO(storeEntity: storeEntity4)]

        dataPoi.append(promotion: promo1)
        dataPoi.append(promotion: promo2)
        dataPoi.append(promotion: promo3)

        // when
        let sut = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.pharmacy.rawValue, name: "pharmacy", isFavourite: nil)))

        // then
        XCTAssert(sut?.title == nil)
        XCTAssert(sut?.imageNamed == ConstantsImages.Promotions.pharmacy)

    }

    func test_givenDataPoiWithMoreThanOnePromotionWithAtLeastOneCategoryTypeEqualAndAllStoresEquals_whenICallInit_thenTitleShouldBeNilAndImageNamedShouldBeTheCategoryIdMostRepited() {

        var storeEntity1 = StoresEntity()
        storeEntity1.id = "1"
        var storeEntity2 = StoresEntity()
        storeEntity2.id = "2"
        var storeEntity3 = StoresEntity()
        storeEntity3.id = "3"
        var storeEntity4 = StoresEntity()
        storeEntity4.id = "4"

        // given
        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "1")
        let promo1 = PromotionsGenerator.getPromotionsBOWithStores().promotions[0]
        promo1.categories = [CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.activities.rawValue, name: "Actividades", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.auto.rawValue, name: "auto", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.beautyAndGragances.rawValue, name: "beautyAndGragances", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.ecommerce.rawValue, name: "ecommerce", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.toyStore.rawValue, name: "toyStore", isFavourite: nil))]
        promo1.stores = [StoreBO(storeEntity: storeEntity1),
                         StoreBO(storeEntity: storeEntity4),
                         StoreBO(storeEntity: storeEntity2)]

        let promo2 = PromotionsGenerator.getPromotionsBOWithStores().promotions[1]
        promo2.categories = [CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.dietAndNutrition.rawValue, name: "dietAndNutrition", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.foodAndGourmet.rawValue, name: "foodAndGourmet", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.ecommerce.rawValue, name: "ecommerce", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.toyStore.rawValue, name: "toyStore", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.auto.rawValue, name: "auto", isFavourite: nil))]
        promo2.stores = [StoreBO(storeEntity: storeEntity4),
                         StoreBO(storeEntity: storeEntity2),
                         StoreBO(storeEntity: storeEntity1)]

        let promo3 = PromotionsGenerator.getPromotionsBOWithStores().promotions[2]
        promo3.categories = [CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.auto.rawValue, name: "auto", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.bar.rawValue, name: "bar", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.ecommerce.rawValue, name: "ecommerce", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.toyStore.rawValue, name: "toyStore", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.bookStore.rawValue, name: "bookStore", isFavourite: nil))]
        promo3.stores = [StoreBO(storeEntity: storeEntity1),
                         StoreBO(storeEntity: storeEntity2),
                         StoreBO(storeEntity: storeEntity4)]

        let promo4 = PromotionsGenerator.getPromotionsBOWithStores().promotions[3]
        promo4.categories = [CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.bookStore.rawValue, name: "bookStore", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.bar.rawValue, name: "bar", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.ecommerce.rawValue, name: "ecommerce", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.boutique.rawValue, name: "boutique", isFavourite: nil)),
                             CategoryBO(categoryEntity: CategoryEntity(id: CategoryType.bookStore.rawValue, name: "bookStore", isFavourite: nil))]
        promo4.stores = [StoreBO(storeEntity: storeEntity1),
                         StoreBO(storeEntity: storeEntity2),
                         StoreBO(storeEntity: storeEntity4)]

        dataPoi.append(promotion: promo1)
        dataPoi.append(promotion: promo2)
        dataPoi.append(promotion: promo3)
        dataPoi.append(promotion: promo4)

        // when
        let sut = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)

        // then
        XCTAssert(sut?.title == nil)
        XCTAssert(sut?.imageNamed == ConstantsImages.Promotions.ecommerce)

    }
    
    // MARK: 1 Promotion --> Poi With Store
    
    func test_givenDataPoiWithOnlyOnePromotionWithStore_whenICallInit_thenReturnNotNilAndShouldHaveTheCorrectValues() {
        
        // given
        
        let promotion = PromotionsGenerator.getPromotionsBO().promotions[0]
        promotion.promotionType.id = .discount
        promotion.categories![0].id = .unknown
        
        let dataPoi = DataPoi(latitude: 3.0, longitude: 4.0, storeId: "33")
        dataPoi.append(promotion: promotion)
        
        // when
        let sut = MapDisplayDataItem(withDataPoi: dataPoi, andCategory: nil)
        
        // then
        XCTAssert(sut != nil)
        XCTAssert(sut!.idStores == dataPoi.storeIds)
        XCTAssert(sut?.latitude == dataPoi.latitude)
        XCTAssert(sut?.longitude == dataPoi.longitude)
        XCTAssert(sut?.imageColor == .BBVAWHITE)
        XCTAssert(sut?.title == nil)
        XCTAssert(sut?.borderColors == nil)
        XCTAssert(sut?.backgroundColor == .BBVADARKPINK)
        XCTAssert(sut?.imageNamed == ConstantsImages.Promotions.promotion)
    }
}
