//
//  MapDisplayDataTests.swift
//  shoppingapp
//
//  Created by jesus.martinez on 15/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#endif

class MapDisplayDataTests: XCTestCase {

    // MARK: - Init

    func test_givenDataPoisEmpty_whenICallInitWithPromotions_thenItemsShouldBeEmpty() {

        // given
        let dataPois = [DataPoi]()

        // when
        let sut = MapDisplayData(withDataPois: dataPois, forCategory: nil)

        // then
        XCTAssert(sut.items.isEmpty == true)

    }

    func test_givenPromotionsWithStores_whenICallInitWithPromotions_thenItemsShouldMatchWithNumberOfStores() {

        // given
        let promotions = PromotionsGenerator.getPromotionsBO()
        var dataPois = [DataPoi]()

        for promotion in promotions.promotions {
            promotion.promotionType.id = .discount

            let dataPoi = DataPoi(latitude: 3, longitude: 4, storeId: "2")
            dataPoi.append(promotion: promotion)

            dataPois.append(dataPoi)
        }

        // when
        let sut = MapDisplayData(withDataPois: dataPois, forCategory: nil)

        // then
        XCTAssert(sut.items.count == dataPois.count)

    }

}
