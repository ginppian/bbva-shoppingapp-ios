//
//  ShoppingListFilteredPresenterTests.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 9/3/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest
import RxSwift
import CellsNativeComponents
import CoreLocation

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class ShoppingListFilteredPresenterTests: XCTestCase {

    var sut: ShoppingListFilteredPresenter<DummyView>!
    var dummyBusManager: DummyBusManager!
    var dummyLocationManager = DummyLocationManager()

    override func setUp() {
        super.setUp()
        dummyBusManager = DummyBusManager()
        sut = ShoppingListFilteredPresenter<DummyView>(busManager: dummyBusManager)

        dummyLocationManager = DummyLocationManager()
        sut.locationManager = dummyLocationManager
    }

    // MARK: - viewWillAppear

    func test_givenAny_whenICallviewWillAppear_thenICalledGetLocation() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyLocationManager.isCalledGetLocation == true)

    }

    func test_givenAny_whenICallviewWillAppear_thenICalledCheckLocationServices() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyLocationManager.isCalledStartLocationObserverAndCheckStatus == true)

    }

    func test_givenFiltersAndCategoriesBO_whenICallviewWillAppear_thenICallViewShowFilters() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.filters = ShoppingFilter()
        sut.filters?.textFilter = "textfilter"
        sut.categoriesBO = PromotionsGenerator.getCategoryBO()

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowFilters == true)

    }

    func test_givenFiltersEmpty_whenICallviewWillAppear_thenINotCallViewShowFilters() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.filters = nil

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowFilters == false)

    }

    func test_givenFiltersAndNotCategoriesBO_whenICallviewWillAppear_thenICallViewHideFilters() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.filters = ShoppingFilter()

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledHideFilters == true)

    }

    func test_givenFiltersEmptyAndCategoriesBO_whenICallviewWillAppear_thenICallViewHideFilters() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.filters = ShoppingFilter()
        sut.categoriesBO = PromotionsGenerator.getCategoryBO()

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledHideFilters == true)

    }

    func test_givenFiltersAndCategoriesBO_whenICallviewWillAppear_thenICallFiltersRetrieveViewLiterals() {

        let dummyShoppingFilter = DummyShoppingFilter()
        sut.filters = dummyShoppingFilter

        // given
        sut.filters?.textFilter = "textfilter"
        sut.categoriesBO = PromotionsGenerator.getCategoryBO()

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyShoppingFilter.isCalledRetrieveLiterals == true)

    }

    func test_givenFiltersAndCategoriesBO_whenICallviewWillAppear_thenICallViewShowFiltersWithTheSameNumberOfDisplayDataThanFiltersViewLiteralsAndMatchTheTitleAndKey() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyShoppingFilter = DummyShoppingFilter()
        dummyShoppingFilter.defaultLiterals = [(title: "title1", key: "key1"), (title: "title2", key: "key2"), (title: "title3", key: "key3"), (title: "title4", key: "key4")]

        // given
        sut.filters = dummyShoppingFilter
        sut.filters?.textFilter = "textfilter"
        sut.categoriesBO = PromotionsGenerator.getCategoryBO()

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.displayDataFilterScrollSent!.count == dummyShoppingFilter.defaultLiterals!.count)

        for i in 0..<dummyShoppingFilter.defaultLiterals!.count {

            XCTAssert(dummyView.displayDataFilterScrollSent![i].title == dummyShoppingFilter.defaultLiterals![i].title)
            XCTAssert(dummyView.displayDataFilterScrollSent![i].filterType == dummyShoppingFilter.defaultLiterals![i].key)

        }

    }

        // MARK: - viewWillAppear - obtainedLocation

    func test_givenUserLocation_whenICallViewWillAppearAndGetLocationReturnsSuccess_thenUserLatitudeAndUserLongitudeShouldMatch() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let latitude = 33.11
        let longitude = 44.22
        dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(sut.userLocation?.latitude == latitude)
        XCTAssert(sut.userLocation?.longitude == longitude)

    }

    func test_givenAny_whenICallViewWillAppearAndGetLocationReturnsSuccess_thenICalledShowSkeleton() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let latitude = 22.0
        let longitude = 40.0
        dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowSkeleton == true)

    }

    func test_givenUserLocationNil_whenICallViewWillAppearAndGetLocationReturnsSuccess_thenICallInteractorProvidePromotionsWithFilters() {

        // given
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.userLocation = nil

        let latitude = 22.0
        let longitude = 40.0
        dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionsWithFilters == true)

    }

    func test_givenUserLocationFilled_whenICallViewWillAppearAndGetLocationReturnsSuccess_thenINotCallInteractorProvidePromotionsWithFilters() {

        // given
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.userLocation = GeoLocationBO(withLatitude: 22.0, withLongitude: 40.0)

        let latitude = 22.0
        let longitude = 40.0
        dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionsWithFilters == false)

    }

    func test_givenFiltersNil_whenICallViewWillAppearAndGetLocationReturnsSuccess_thenICallInteractorProvidePromotionsWithFiltersWithDefaultValues() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.filters = nil

        let latitude = 22.0
        let longitude = 40.0
        dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()

        // then
        let shoppingParamLocationExpected = ShoppingParamLocation(geolocation: sut.userLocation!, distanceParam: ShoppingDistanceParam(distance: sut.defaultDistance, lengthType: sut.lengthType))

        XCTAssert(dummyInteractor.paramsSent == ShoppingParamsTransport(categoryId: nil, trendingId: nil, location: shoppingParamLocationExpected, titleIds: nil, pageSize: nil, orderBy: nil))
        XCTAssert(dummyInteractor.filtersSent == nil)

    }

    func test_givenFilters_whenICallViewWillAppearAndGetLocationReturnsSuccess_thenICallInteractorProvidePromotionsWithFiltersWithDefaultValuesAndFilters() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.filters?.textFilter = "text filter"
        sut.filters?.promotionUsageTypeFilter = [.physical, .online]
        sut.filters?.promotionTypeFilter = [.point]
        sut.filters?.categoriesFilter = [.activities]

        let latitude = 22.0
        let longitude = 40.0
        dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()

        // then
        let shoppingParamLocationExpected = ShoppingParamLocation(geolocation: sut.userLocation!, distanceParam: ShoppingDistanceParam(distance: sut.defaultDistance, lengthType: sut.lengthType))

        XCTAssert(dummyInteractor.paramsSent == ShoppingParamsTransport(categoryId: nil, trendingId: nil, location: shoppingParamLocationExpected, titleIds: nil, pageSize: nil, orderBy: nil))
        XCTAssert(dummyInteractor.filtersSent == sut.filters)

    }

    func test_givenNothing_whenICallViewWillAppearAndGetLocationReturnsSuccessAndInteractorReturnSuccess_thenICallHideSkeleton() {

        let dummyInteractor = DummyInteractor()
        dummyInteractor.responseStatus = ConstantsHTTPCodes.STATUS_OK
        sut.interactor = dummyInteractor

        let dummyView = DummyView()
        sut.view = dummyView

        // given

        let latitude = 22.0
        let longitude = 40.0
        dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledHideSkeleton == true)
    }

    func test_givenNothing_whenICallViewWillAppearAndGetLocationReturnsSuccessAndNoContentResponse_thenINotCallHideSkeleton() {

        let dummyInteractor = DummyInteractor()
        dummyInteractor.responseStatus = ConstantsHTTPCodes.NO_CONTENT
        sut.interactor = dummyInteractor

        let dummyView = DummyView()
        sut.view = dummyView

        // given

        let latitude = 22.0
        let longitude = 40.0
        dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledHideSkeleton == false)
    }

    func test_givenNothing_whenICallViewWillAppearAndGetLocationReturnsSuccessAndInteractorReturnError_thenICallHideSkeleton() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let errorEntity = ErrorEntity()
        errorEntity.code = "500"
        errorEntity.message = "Error 500"

        dummyInteractor.forceError = true
        dummyInteractor.errorBO = ErrorBO(error: errorEntity)

        let dummyView = DummyView()
        sut.view = dummyView

        // given

        let latitude = 22.0
        let longitude = 40.0
        dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledHideSkeleton == true)
    }

    func test_givenPromotions_whenICallViewWillAppearAndGetLocationReturnsSuccessAndIsASuccessfulResponse_thenCallShowPromotions() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let latitude = 22.0
        let longitude = 40.0
        dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()

        //then
        XCTAssert(dummyView.isCalledShowPromotions == true)

    }

    func test_givenPromotions_whenICallViewWillAppearAndGetLocationReturnsSuccessAndNoContentResponse_thenINotCallShowPromotions() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.responseStatus = ConstantsHTTPCodes.NO_CONTENT
        sut.interactor = dummyInteractor

        let latitude = 22.0
        let longitude = 40.0
        dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()

        //then
        XCTAssert(dummyView.isCalledShowPromotions == false)

    }

    func test_givenPromotions_whenICallViewWillAppearAndGetLocationReturnsSuccessAndIsAErrorResponse_thenINotCallShowPromotions() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        dummyInteractor.forceError = true

        let latitude = 22.0
        let longitude = 40.0
        dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()

        //then
        XCTAssert(dummyView.isCalledShowPromotions == false)

    }

    func test_givenPromotions_whenICallViewWillAppearAndGetLocationReturnsSuccessAndIsASuccessfulResponse_thenCreateDisplayItems() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let latitude = 22.0
        let longitude = 40.0
        dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()

        //then
        XCTAssert(!sut.displayItems.items.isEmpty)

    }

    func test_givenPromotions_whenICallViewWillAppearAndGetLocationReturnsSuccessAndIsASuccessfulResponse_thenDisplayItemsMatchWithReturnedDisplay() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let latitude = 22.0
        let longitude = 40.0
        dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()

        //then
        XCTAssert(sut.displayItems === dummyView.displayItemPromotionsSent)

    }

    func test_givenPromotions_whenICallViewWillAppearAndGetLocationReturnsSuccessAndNotContentResponse_thenICallShowNoContentView() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.responseStatus = ConstantsHTTPCodes.NO_CONTENT
        sut.interactor = dummyInteractor

        let latitude = 22.0
        let longitude = 40.0
        dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowNoContentView == true)

    }

    func test_givenPromotions_whenICallViewWillAppearAndGetLocationReturnsSuccessAndInteractorReturnError_thenICallViewShowEmptyTsec() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()

        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        sut.interactor = dummyInteractor

        let latitude = 22.0
        let longitude = 40.0
        dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowError == true)

    }

    // MARK: - obtainedLocation - Pagination

    func test_givenAPromotionsBOWithoutPagination_whenICallViewWillAppearAndGetLocationReturnsSuccess_thenICallViewUpdateForNoMoreContent() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBOWithoutPagination()
        sut.interactor = dummyInteractor

        let latitude = 22.0
        let longitude = 40.0
        dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == true)

    }

    func test_givenAPromotionsBOWithLinkNextEmpty_whenICallViewWillAppearAndGetLocationReturnsSuccess_thenICallViewUpdateForNoMoreContent() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        dummyInteractor.promotionsBO.pagination?.links?.next = ""
        sut.interactor = dummyInteractor

        let latitude = 22.0
        let longitude = 40.0
        dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == true)

    }

    func test_givenAPromotionsBOWithLinkNextNil_whenICallViewWillAppearAndGetLocationReturnsSuccess_thenICallViewUpdateForNoMoreContent() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        dummyInteractor.promotionsBO.pagination?.links?.next = nil
        sut.interactor = dummyInteractor

        let latitude = 22.0
        let longitude = 40.0
        dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == true)

    }

    func test_givenAPromotionsBOWithoutPagination_whenICallViewWillAppearAndGetLocationReturnsSuccess_thenINotCallViewUpdateForMoreContent() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBOWithoutPagination()
        sut.interactor = dummyInteractor

        let latitude = 22.0
        let longitude = 40.0
        dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledUpdateForMoreContent == false)

    }

    func test_givenAPromotionsBOWithLinkNextEmpty_whenICallViewWillAppearAndGetLocationReturnsSuccess_thenINotCallViewUpdateForMoreContent() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        dummyInteractor.promotionsBO.pagination?.links?.next = ""
        sut.interactor = dummyInteractor

        let latitude = 22.0
        let longitude = 40.0
        dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledUpdateForMoreContent == false)

    }

    func test_givenAPromotionsBOWithLinkNextNil_whenICallViewWillAppearAndGetLocationReturnsSuccess_thenINotCallViewUpdateForMoreContent() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        dummyInteractor.promotionsBO.pagination?.links?.next = nil
        sut.interactor = dummyInteractor

        let latitude = 22.0
        let longitude = 40.0
        dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledUpdateForMoreContent == false)

    }

    func test_givenAPromotionsBOWithLinkNext_whenICallViewWillAppearAndGetLocationReturnsSuccess_thenICallViewUpdateForMoreContent() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        sut.interactor = dummyInteractor

        let latitude = 22.0
        let longitude = 40.0
        dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledUpdateForMoreContent == true)

    }

    func test_givenAPromotionsBOWithPaginationAndLinkNext_whenICallViewWillAppearAndGetLocationReturnsSuccess_thenIDoNotCallViewUpdateForNoMoreContent() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        sut.interactor = dummyInteractor

        let latitude = 22.0
        let longitude = 40.0
        dummyLocationManager.location = CLLocation(latitude: latitude, longitude: longitude)

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == false)

    }

    // MARK: - obtainLocationFail

    func test_givenAny_whenICallViewWillAppearAndObtainLocationFail_thenICalledShowSkeleton() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowSkeleton == true)

    }

    func test_givenNothing_whenICallViewWillAppearAndObtainLocationFail_thenICallInteractorProvidePromotionsWithFilters() {

        // given
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionsWithFilters == true)

    }

    func test_givenFiltersNil_whenICallViewWillAppearAndObtainLocationFail_thenICallInteractorProvidePromotionsWithFiltersWithDefaultValues() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.filters = nil
        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        // then
        let shoppingParamLocationExpected = ShoppingParamLocation(geolocation: sut.defaultLocation, distanceParam: ShoppingDistanceParam(distance: sut.defaultDistance, lengthType: sut.lengthType))

        XCTAssert(dummyInteractor.paramsSent == ShoppingParamsTransport(categoryId: nil, trendingId: nil, location: shoppingParamLocationExpected, titleIds: nil, pageSize: nil, orderBy: nil))
        XCTAssert(dummyInteractor.filtersSent == nil)

    }

    func test_givenFilters_whenICallViewWillAppearAndObtainLocationFail_thenICallInteractorProvidePromotionsWithFiltersWithDefaultValuesAndFilters() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.filters?.textFilter = "text filter"
        sut.filters?.promotionUsageTypeFilter = [.physical, .online]
        sut.filters?.promotionTypeFilter = [.point]
        sut.filters?.categoriesFilter = [.activities]

        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        // then
        let shoppingParamLocationExpected = ShoppingParamLocation(geolocation: sut.defaultLocation, distanceParam: ShoppingDistanceParam(distance: sut.defaultDistance, lengthType: sut.lengthType))

        XCTAssert(dummyInteractor.paramsSent == ShoppingParamsTransport(categoryId: nil, trendingId: nil, location: shoppingParamLocationExpected, titleIds: nil, pageSize: nil, orderBy: nil))
        XCTAssert(dummyInteractor.filtersSent == sut.filters)

    }

    func test_givenNothing_whenICallViewWillAppearAndObtainLocationFailAndInteractorReturnSuccess_thenICallHideSkeleton() {

        let dummyInteractor = DummyInteractor()
        dummyInteractor.responseStatus = ConstantsHTTPCodes.STATUS_OK
        sut.interactor = dummyInteractor

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledHideSkeleton == true)
    }

    func test_givenNothing_whenICallViewWillAppearAndObtainLocationFailAndNoContentResponse_thenINotCallHideSkeleton() {

        let dummyInteractor = DummyInteractor()
        dummyInteractor.responseStatus = ConstantsHTTPCodes.NO_CONTENT
        sut.interactor = dummyInteractor

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledHideSkeleton == false)
    }

    func test_givenNothing_whenICallViewWillAppearAndObtainLocationFailAndInteractorReturnError_thenICallHideSkeleton() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let errorEntity = ErrorEntity()
        errorEntity.code = "500"
        errorEntity.message = "Error 500"

        dummyInteractor.forceError = true
        dummyInteractor.errorBO = ErrorBO(error: errorEntity)

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledHideSkeleton == true)
    }

    func test_givenPromotions_whenICallViewWillAppearAndObtainLocationFailAndIsASuccessfulResponse_thenCallShowPromotions() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        //then
        XCTAssert(dummyView.isCalledShowPromotions == true)

    }

    func test_givenPromotions_whenICallViewWillAppearAndObtainLocationFailAndNoContentResponse_thenINotCallShowPromotions() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.responseStatus = ConstantsHTTPCodes.NO_CONTENT
        sut.interactor = dummyInteractor

        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        //then
        XCTAssert(dummyView.isCalledShowPromotions == false)

    }

    func test_givenPromotions_whenICallViewWillAppearAndObtainLocationFailAndIsAErrorResponse_thenINotCallShowPromotions() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        dummyInteractor.forceError = true

        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        //then
        XCTAssert(dummyView.isCalledShowPromotions == false)

    }

    func test_givenPromotions_whenICallViewWillAppearAndObtainLocationFailAndIsASuccessfulResponse_thenCreateDisplayItems() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        //then
        XCTAssert(!sut.displayItems.items.isEmpty)

    }

    func test_givenPromotions_whenICallViewWillAppearAndObtainLocationFailAndIsASuccessfulResponse_thenDisplayItemsMatchWithReturnedDisplay() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        //then
        XCTAssert(sut.displayItems === dummyView.displayItemPromotionsSent)

    }

    func test_givenPromotions_whenICallViewWillAppearAndObtainLocationFailAndNotContentResponse_thenICallShowNoContentView() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.responseStatus = ConstantsHTTPCodes.NO_CONTENT
        sut.interactor = dummyInteractor

        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowNoContentView == true)

    }

    func test_givenPromotions_whenICallViewWillAppearAndObtainLocationFailAndInteractorReturnError_thenICallViewShowEmptyTsec() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()

        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        sut.interactor = dummyInteractor

        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowError == true)

    }

    // MARK: - obtainLocationFail - Pagination

    func test_givenAPromotionsBOWithoutPagination_whenICallViewWillAppearAndObtainLocationFail_thenICallViewUpdateForNoMoreContent() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBOWithoutPagination()
        sut.interactor = dummyInteractor

        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == true)

    }

    func test_givenAPromotionsBOWithLinkNextEmpty_whenICallViewWillAppearAndObtainLocationFail_thenICallViewUpdateForNoMoreContent() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        dummyInteractor.promotionsBO.pagination?.links?.next = ""
        sut.interactor = dummyInteractor

        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == true)

    }

    func test_givenAPromotionsBOWithLinkNextNil_whenICallViewWillAppearAndObtainLocationFail_thenICallViewUpdateForNoMoreContent() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        dummyInteractor.promotionsBO.pagination?.links?.next = nil
        sut.interactor = dummyInteractor

        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == true)

    }

    func test_givenAPromotionsBOWithoutPagination_whenICallViewWillAppearAndObtainLocationFail_thenINotCallViewUpdateForMoreContent() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBOWithoutPagination()
        sut.interactor = dummyInteractor

        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledUpdateForMoreContent == false)

    }

    func test_givenAPromotionsBOWithLinkNextEmpty_whenICallViewWillAppearAndObtainLocationFail_thenINotCallViewUpdateForMoreContent() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        dummyInteractor.promotionsBO.pagination?.links?.next = ""
        sut.interactor = dummyInteractor

        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledUpdateForMoreContent == false)

    }

    func test_givenAPromotionsBOWithLinkNextNil_whenICallViewWillAppearAndObtainLocationFail_thenINotCallViewUpdateForMoreContent() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        dummyInteractor.promotionsBO.pagination?.links?.next = nil
        sut.interactor = dummyInteractor

        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledUpdateForMoreContent == false)

    }

    func test_givenAPromotionsBOWithLinkNext_whenICallViewWillAppearAndObtainLocationFail_thenICallViewUpdateForMoreContent() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        sut.interactor = dummyInteractor

        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledUpdateForMoreContent == true)

    }

    func test_givenAPromotionsBOWithPaginationAndLinkNext_whenICallViewWillAppearAndObtainLocationFail_thenIDoNotCallViewUpdateForNoMoreContent() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        sut.interactor = dummyInteractor

        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == false)

    }

    // MARK: Pagination - loadDataNextPage

    func test_givenIsLoadingNextPageTrue_whenICallLoadDataNextPage_thenIDoNotCallViewShowNextPageAnimation() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = true

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyView.isCalledShowNextPageAnimation == false)

    }

    func test_givenIsLoadingNextPageFalseWithoutPromotions_whenICallLoadDataNextPage_thenIDoNotCallViewShowNextPageAnimation() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = false

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyView.isCalledShowNextPageAnimation == false)

    }

    func test_givenIsLoadingNextPageFalseWithPromotionsAndNotPaginationNext_whenICallLoadDataNextPage_thenIDoNotCallViewShowNextPageAnimation() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = false
        sut.promotionsBO = PromotionsGenerator.getPromotionsBOWithoutPagination()

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyView.isCalledShowNextPageAnimation == false)

    }

    func test_givenPromotionsWithoutPaginationNext_whenICallLoadDataNextPage_thenIDoNotCallViewShowNextPageAnimation() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.promotionsBO = PromotionsGenerator.getPromotionsBOWithoutPagination()

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyView.isCalledShowNextPageAnimation == false)

    }

    func test_givenIsLoadingNextPageFalseWithPromotionsAndPaginationNextEmpty_whenICallLoadDataNextPage_thenICallViewShowNextPageAnimation() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()
        sut.promotionsBO?.pagination?.links?.next = ""

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyView.isCalledShowNextPageAnimation == false)

    }

    func test_givenIsLoadingNextPageFalseWithPromotionsAndPaginationNext_whenICallLoadDataNextPage_thenICallViewShowNextPageAnimation() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = false
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyView.isCalledShowNextPageAnimation == true)

    }

    //-----------

    func test_givenIsLoadingNextPageTrue_whenICallLoadDataNextPage_thenIDoNotCallInteractorProvidePromotionsWithNextPage() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = true

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionsWithFiltersNextPage == false)

    }

    func test_givenIsLoadingNextPageFalseWithoutPromotions_whenICallLoadDataNextPage_thenIDoNotCallInteractorProvidePromotionsWithNextPage() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = false

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionsWithFiltersNextPage == false)

    }

    func test_givenIsLoadingNextPageFalseWithPromotionsAndNotPaginationNext_whenICallLoadDataNextPage_thenIDoNotCallInteractorProvidePromotionsWithNextPage() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = false
        sut.promotionsBO = PromotionsGenerator.getPromotionsBOWithoutPagination()

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionsWithFiltersNextPage == false)

    }

    func test_givenIsLoadingNextPageFalseWithPromotionsWithLinkNextEmpty_whenICallLoadDataNextPage_thenIDoNotCallInteractorProvidePromotionsWithNextPage() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = false
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()
        sut.promotionsBO?.pagination?.links?.next = ""

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionsWithFiltersNextPage == false)
    }

    func test_givenIsLoadingNextPageFalseWithPromotionsAndPaginationNext_whenICallLoadDataNextPage_thenICallInteractorProvidePromotionsWithNextPage() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = false
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionsWithFiltersNextPage == true)
    }

    //-----------

    func test_givenIsLoadingNextPageFalseWithPromotionsAndPaginationNext_whenICallLoadDataNextPageAndReturnSuccess_thenICallViewHideNextPageAnimation() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = false
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyView.isCalledHideNextPageAnimation == true)
    }

    func test_givenIsLoadingNextPageFalseWithPromotionsWithStatus206AndPaginationNext_whenwhenICallLoadDataNextPageAndReturnError_thenCallViewHideNextPageAnimation() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceError = true
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = false
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyView.isCalledHideNextPageAnimation == true)

    }

    //-----------

    func test_givenIsLoadingNextPageFalseWithPaginationNext_whenICallLoadDataNextPageAndReturnSuccess200_thenICallViewShowPromotions() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        dummyInteractor.promotionsBO.pagination?.links?.next = "next"
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = false
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyView.isCalledShowPromotions == true)
    }

    func test_givenIsLoadingNextPageFalseWithPromotionsAndPaginationNext_whenICallLoadDataNextPageAndReturnSuccess206_thenICallViewShowPromotions() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.responseStatus = ConstantsHTTPCodes.STATUS_OK
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = false
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyView.isCalledShowPromotions == true)
    }

    //-----------

    func test_givenIsLoadingNextPageFalseWithPromotionsAndPaginationNext_whenICallLoadDataNextPage_thenICallInteractorProvidePromotionsWithNextPageWithTheNextPageOfPagination() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = false
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()
        let nextPageLink = sut.promotionsBO?.pagination?.links?.generateNextURL(isFinancial: false)

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyInteractor.nextPageLink == nextPageLink)
    }

    func test_givenIsLoadingNextPageFalseWithPromotionsAndPaginationNext_whenICallLoadDataNextPageAndReturnSuccess_thenIsLoadingNextPageShouldBeFalse() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = false
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(sut.isLoadingNextPage == false)
    }

    func test_givenIsLoadingNextPageFalseWithPromotionsAndPaginationNext_whenwhenICallLoadDataNextPageAndReturnError_thenIsLoadingNextPageShouldBeFalse() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceError = true
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = false
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(sut.isLoadingNextPage == false)
    }

    func test_givenIsLoadingNextPageFalseWithPromotionsAndPaginationNext_whenICallLoadDataNextPage_thenIsLoadingNextPageShouldBeTrue() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.presenter = sut
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = false
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyInteractor.wasLoadingNextPage == true)

    }

    func test_givenIsLoadingNextPageFalseWithPromotionsAndPaginationNext_whenICallLoadDataNextPageAndReturnSuccess_thenPromotionsBOShouldMustWithPaginationReturned() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBOWithoutPagination()
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = false
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(sut.promotionsBO?.pagination === dummyInteractor.promotionsBO.pagination)
    }

    func test_givenIsLoadingNextPageFalseWithPromotionsAndDisplayItemsAndPaginationNext_whenICallLoadDataNextPageAndReturnSuccess206_thenICallViewShowPromotionsWithTheNumberOfDisplayItemsPreviousPlusNewPromotions() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.responseStatus = ConstantsHTTPCodes.STATUS_OK
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = false
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        let displayItems = DisplayItemsPromotions()
        displayItems.items.append(DisplayItemPromotion.generateDisplayItemPromotionTrending(withTrendingPromotions: sut.promotionsBO!))

        sut.displayItems = displayItems

        let displayItemsCountBefore = sut.displayItems.items.count

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyView.displayItemPromotionsSent!.items.count == (displayItemsCountBefore + dummyInteractor.promotionsBO.promotions.count))
    }

    func test_givenIsLoadingNextPageFalseWithPromotionsAndPaginationNext_whenwhenICallLoadDataNextPageAndReturnError_thenErrorBOShouldMatchWithReturned() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceError = true
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = false
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(sut.errorBO === dummyInteractor.errorBO)

    }

    func test_givenIsLoadingNextPageFalseWithPromotionsAndPaginationNext_whenwhenICallLoadDataNextPageAndReturnError_thenCallViewShowError() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceError = true
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = false
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyView.isShowErrorCalled == true)

    }

    func test_givenIsLoadingNextPageFalseWithPromotionsAndPaginationNext_whenICallLoadDataNextPageAndReturnSuccesDiferrentThanOK_thenICallUpdateForNoMoreContent() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = false
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        for i in 0 ... 600 where i != ConstantsHTTPCodes.STATUS_OK {

            dummyInteractor.responseStatus = i
            sut.promotionsBO?.status = i

            // when
            sut.loadDataNextPage()

            // then
            XCTAssert(dummyView.isCalledUpdateForNoMoreContent == true)
        }
    }

    func test_givenIsLoadingNextPageFalseWithPromotionsAndPaginationNext_whenICallLoadDataNextPageAndReturnSuccesDiferrentThanOK_thenINotCallShowPromotions() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = false
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        for i in 0 ... 600 where i != ConstantsHTTPCodes.STATUS_OK {

                dummyInteractor.responseStatus = i
                sut.promotionsBO?.status = i

                // when
                sut.loadDataNextPage()

                // then
                XCTAssert(dummyView.isCalledShowPromotions == false)
        }
    }

    func test_givenIsLoadingNextPageFalseWithPromotionsAndPaginationNext_whenICallLoadDataNextPageAndReturnStatusOK_thenICallShowPromotions() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.responseStatus = ConstantsHTTPCodes.STATUS_OK
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = false
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyView.isCalledShowPromotions == true)
    }

    func test_givenIsLoadingNextPageFalseWithPromotionsAndPaginationNext_whenICallLoadDataNextPageAndReturnStatusOK_thenICallUpdateForMoreContent() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.responseStatus = ConstantsHTTPCodes.STATUS_OK
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = false
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyView.isCalledUpdateForMoreContent == true)
    }

    func test_givenIsLoadingNextPageFalseWithPromotionsWithoutPagination_whenICallLoadDataNextPageAndReturnStatusOK_thenINotCallUpdateForMoreContent() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.responseStatus = ConstantsHTTPCodes.STATUS_OK
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBOWithoutPagination()
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = false
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyView.isCalledUpdateForMoreContent == false)
    }

    func test_givenIsLoadingNextPageFalseWithPromotionsWithoutPagination_whenICallLoadDataNextPageAndReturnStatusOK_thenICallUpdateForNoMoreContent() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.responseStatus = ConstantsHTTPCodes.STATUS_OK
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBOWithoutPagination()
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = false
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == true)
    }

    func test_givenIsLoadingNextPageFalseWithPromotionsAndPaginationNextEmpty_whenICallLoadDataNextPageAndReturnStatusOK_thenICallUpdateForMoreContent() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.responseStatus = ConstantsHTTPCodes.STATUS_OK
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        dummyInteractor.promotionsBO.pagination?.links?.next = ""
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = false
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == true)
    }

    func test_givenIsLoadingNextPageFalseWithPromotionsAndPaginationNext_whenICallLoadDataNextPageAndReturnStatusOK_thenINotCallUpdateForNoMoreContent() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.responseStatus = ConstantsHTTPCodes.STATUS_OK
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = false
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == false)
    }

    // MARK: - SetModel

    func test_givenShoppingFilterTransportWithCategories_whenICallSetModel_thenCategoriesBOShouldMatch() {

        // given
        let shoppingFilterTransport = ShoppingFilterTransport()
        shoppingFilterTransport.categoriesBO = PromotionsGenerator.getCategoryBO()

        // when
        sut.setModel(model: shoppingFilterTransport)

        // then
        XCTAssert(sut.categoriesBO! === shoppingFilterTransport.categoriesBO)

    }

    func test_givenShoppingFilterTransportWithFilters_whenICallSetModel_thenFiltersShouldBeFilled() {

        // given
        let shoppingFilterTransport = ShoppingFilterTransport()
        shoppingFilterTransport.filters = ShoppingFilter()

        // when
        sut.setModel(model: shoppingFilterTransport)

        // then
        XCTAssert(sut.filters != nil)

    }

    func test_givenShoppingFilterTransportWithFilters_whenICallSetModel_thenFiltersShouldMatchWithTransport() {

        // given
        let shoppingFilterTransport = ShoppingFilterTransport()
        shoppingFilterTransport.filters = ShoppingFilter()

        // when
        sut.setModel(model: shoppingFilterTransport)

        // then
        XCTAssert(sut.filters == shoppingFilterTransport.filters)

    }

    // MARK: - removedFilter

    func test_givenFiltersNil_whenICallRemovedFilter_thenICallViewHideFilters() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.filters = nil
        let displayDataRemoved = FilterScrollDisplayData(title: "Title1", filterType: "filterType1")

        // when
        sut.removedFilter(withDisplayData: displayDataRemoved)

        // then
        XCTAssert(dummyView.isCalledHideFilters == true)

    }

    func test_givenFilters_whenICallRemovedFilter_thenIFiltersRemoveByFilterType() {

        // given
        let dummyFilters = DummyShoppingFilter()
        sut.filters = dummyFilters

        let displayDataRemoved = FilterScrollDisplayData(title: "Title1", filterType: "filterType1")

        // when
        sut.removedFilter(withDisplayData: displayDataRemoved)

        // then
        XCTAssert(dummyFilters.isCalledRemoveByFilterType == true)

    }

    func test_givenFilters_whenICallRemovedFilter_thenIFiltersRemoveByFilterTypeWithFilterTypeOfDisplayDataOfParameter() {

        // given
        let dummyFilters = DummyShoppingFilter()
        sut.filters = dummyFilters

        let displayDataRemoved = FilterScrollDisplayData(title: "Title1", filterType: "filterType1")

        // when
        sut.removedFilter(withDisplayData: displayDataRemoved)

        // then
        XCTAssert(dummyFilters.sentFilterType! == displayDataRemoved.filterType)

    }

    func test_givenFiltersEmpty_whenICallRemovedFilter_thenICallViewHideFilters() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let displayDataRemoved = FilterScrollDisplayData(title: "Title1", filterType: "filterType1")

        // when
        sut.removedFilter(withDisplayData: displayDataRemoved)

        // then
        XCTAssert(dummyView.isCalledHideFilters == true)

    }

    func test_givenFiltersEmpty_whenICallRemovedFilter_thenFiltersShouldBeNil() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let displayDataRemoved = FilterScrollDisplayData(title: "Title1", filterType: "filterType1")

        // when
        sut.removedFilter(withDisplayData: displayDataRemoved)

        // then
        XCTAssert(sut.filters == nil)

    }

    func test_givenFilters_whenICallRemovedFilter_thenICalledShowSkeleton() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let filters = ShoppingFilter()
        sut.filters = filters

        let displayDataRemoved = FilterScrollDisplayData(title: "Title1", filterType: "filterType1")

        // when
        sut.removedFilter(withDisplayData: displayDataRemoved)

        // then
        XCTAssert(dummyView.isCalledShowSkeleton == true)

    }

    func test_givenFilters_whenICallRemovedFilter_thenICallInteractorProvidePromotionsWithFilters() {

        // given
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let filters = ShoppingFilter()
        sut.filters = filters

        let displayDataRemoved = FilterScrollDisplayData(title: "Title1", filterType: "filterType1")

        // when
        sut.removedFilter(withDisplayData: displayDataRemoved)

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionsWithFilters == true)

    }

    func test_givenFiltersAndUserLocationNil_whenICallRemovedFilter_thenICallInteractorProvidePromotionsWithFiltersWithDefaultValues() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let filters = ShoppingFilter()
        sut.filters = filters
        sut.userLocation = nil
        let displayDataRemoved = FilterScrollDisplayData(title: "Title1", filterType: "filterType1")

        // when
        sut.removedFilter(withDisplayData: displayDataRemoved)

        // then
        let shoppingParamLocationExpected = ShoppingParamLocation(geolocation: sut.defaultLocation, distanceParam: ShoppingDistanceParam(distance: sut.defaultDistance, lengthType: sut.lengthType))

        XCTAssert(dummyInteractor.paramsSent == ShoppingParamsTransport(categoryId: nil, trendingId: nil, location: shoppingParamLocationExpected, titleIds: nil, pageSize: nil, orderBy: nil))
        XCTAssert(dummyInteractor.filtersSent == sut.filters)

    }

    func test_givenFiltersAndUserLocationFilled_whenICallRemovedFilter_thenICallInteractorProvidePromotionsWithFiltersWithDefaultValues() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let filters = ShoppingFilter()
        sut.filters = filters
        sut.userLocation = GeoLocationBO(withLatitude: 22.0, withLongitude: 40.0)
        let displayDataRemoved = FilterScrollDisplayData(title: "Title1", filterType: "filterType1")

        // when
        sut.removedFilter(withDisplayData: displayDataRemoved)

        // then
        let shoppingParamLocationExpected = ShoppingParamLocation(geolocation: sut.userLocation!, distanceParam: ShoppingDistanceParam(distance: sut.defaultDistance, lengthType: sut.lengthType))

        XCTAssert(dummyInteractor.paramsSent == ShoppingParamsTransport(categoryId: nil, trendingId: nil, location: shoppingParamLocationExpected, titleIds: nil, pageSize: nil, orderBy: nil))
        XCTAssert(dummyInteractor.filtersSent == nil)

    }

    func test_givenFiltersAndUserLocationNil_whenICallRemovedFilter_thenICallInteractorProvidePromotionsWithFiltersWithDefaultValuesAndFilters() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let filters = ShoppingFilter()
        sut.filters = filters

        sut.filters?.textFilter = "text filter"
        sut.filters?.promotionUsageTypeFilter = [.physical, .online]
        sut.filters?.promotionTypeFilter = [.point]
        sut.filters?.categoriesFilter = [.activities]
        sut.userLocation = nil
        let displayDataRemoved = FilterScrollDisplayData(title: "Title1", filterType: "filterType1")

        // when
        sut.removedFilter(withDisplayData: displayDataRemoved)

        // then
        let shoppingParamLocationExpected = ShoppingParamLocation(geolocation: sut.defaultLocation, distanceParam: ShoppingDistanceParam(distance: sut.defaultDistance, lengthType: sut.lengthType))

        XCTAssert(dummyInteractor.paramsSent == ShoppingParamsTransport(categoryId: nil, trendingId: nil, location: shoppingParamLocationExpected, titleIds: nil, pageSize: nil, orderBy: nil))
        XCTAssert(dummyInteractor.filtersSent == sut.filters)

    }

    func test_givenNothing_whenICallRemovedFilterAndInteractorReturnSuccess_thenICallHideSkeleton() {

        let dummyInteractor = DummyInteractor()
        dummyInteractor.responseStatus = ConstantsHTTPCodes.STATUS_OK
        sut.interactor = dummyInteractor

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.filters = ShoppingFilter()
        let displayDataRemoved = FilterScrollDisplayData(title: "Title1", filterType: "filterType1")

        // when
        sut.removedFilter(withDisplayData: displayDataRemoved)

        // then
        XCTAssert(dummyView.isCalledHideSkeleton == true)
    }

    func test_givenNothing_whenICallRemovedFilterAndNotContentResponse_thenINotCallHideSkeleton() {

        let dummyInteractor = DummyInteractor()
        dummyInteractor.responseStatus = ConstantsHTTPCodes.NO_CONTENT
        sut.interactor = dummyInteractor

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.filters = ShoppingFilter()
        let displayDataRemoved = FilterScrollDisplayData(title: "Title1", filterType: "filterType1")

        // when
        sut.removedFilter(withDisplayData: displayDataRemoved)

        // then
        XCTAssert(dummyView.isCalledHideSkeleton == false)
    }

    func test_givenNothing_whenICallRemovedFilterAndInteractorReturnError_thenICallHideSkeleton() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let errorEntity = ErrorEntity()
        errorEntity.code = "500"
        errorEntity.message = "Error 500"

        dummyInteractor.forceError = true
        dummyInteractor.errorBO = ErrorBO(error: errorEntity)

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.filters = ShoppingFilter()
        let displayDataRemoved = FilterScrollDisplayData(title: "Title1", filterType: "filterType1")

        // when
        sut.removedFilter(withDisplayData: displayDataRemoved)

        // then
        XCTAssert(dummyView.isCalledHideSkeleton == true)
    }

    func test_givenPromotions_whenICallRemovedFilterAndIsASuccessfulResponse_thenCallShowPromotions() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.filters = ShoppingFilter()
        let displayDataRemoved = FilterScrollDisplayData(title: "Title1", filterType: "filterType1")

        // when
        sut.removedFilter(withDisplayData: displayDataRemoved)

        //then
        XCTAssert(dummyView.isCalledShowPromotions == true)

    }

    func test_givenPromotions_whenICallRemovedFilterAndNoContentResponse_thenINotCallShowPromotions() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.responseStatus = ConstantsHTTPCodes.NO_CONTENT
        sut.interactor = dummyInteractor

        sut.filters = ShoppingFilter()
        let displayDataRemoved = FilterScrollDisplayData(title: "Title1", filterType: "filterType1")

        // when
        sut.removedFilter(withDisplayData: displayDataRemoved)

        //then
        XCTAssert(dummyView.isCalledShowPromotions == false)

    }

    func test_givenPromotions_whenICallRemovedFilterAndIsAErrorResponse_thenINotCallShowPromotions() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        dummyInteractor.forceError = true

        sut.filters = ShoppingFilter()
        let displayDataRemoved = FilterScrollDisplayData(title: "Title1", filterType: "filterType1")

        // when
        sut.removedFilter(withDisplayData: displayDataRemoved)

        //then
        XCTAssert(dummyView.isCalledShowPromotions == false)

    }

    func test_givenPromotions_whenICallRemovedFilterAndIsASuccessfulResponse_thenCreateDisplayItems() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.filters = ShoppingFilter()
        let displayDataRemoved = FilterScrollDisplayData(title: "Title1", filterType: "filterType1")

        // when
        sut.removedFilter(withDisplayData: displayDataRemoved)

        //then
        XCTAssert(!sut.displayItems.items.isEmpty)

    }

    func test_givenPromotions_whenICallRemovedFilterAndIsASuccessfulResponse_thenDisplayItemsMatchWithReturnedDisplay() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.filters = ShoppingFilter()
        let displayDataRemoved = FilterScrollDisplayData(title: "Title1", filterType: "filterType1")

        // when
        sut.removedFilter(withDisplayData: displayDataRemoved)

        //then
        XCTAssert(sut.displayItems === dummyView.displayItemPromotionsSent)

    }

    func test_givenPromotions_whenICallRemovedFilterAndNotContentResponse_thenICallShowNoContentView() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.responseStatus = ConstantsHTTPCodes.NO_CONTENT
        sut.interactor = dummyInteractor

        sut.filters = ShoppingFilter()
        let displayDataRemoved = FilterScrollDisplayData(title: "Title1", filterType: "filterType1")

        // when
        sut.removedFilter(withDisplayData: displayDataRemoved)

        // then
        XCTAssert(dummyView.isCalledShowNoContentView == true)

    }

    func test_givenPromotions_whenICallRemovedFilterAndInteractorReturnError_thenICallViewShowEmptyTsec() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()

        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        sut.interactor = dummyInteractor

        sut.filters = ShoppingFilter()
        let displayDataRemoved = FilterScrollDisplayData(title: "Title1", filterType: "filterType1")

        // when
        sut.removedFilter(withDisplayData: displayDataRemoved)

        // then
        XCTAssert(dummyView.isCalledShowError == true)

    }

    // MARK: - removedAllFilters

    func test_givenAny_whenICallRemovedAllFilters_thenFiltersShouldBeNil() {

        // given
        sut.filters = ShoppingFilter()

        // when
        sut.removedAllFilters()

        // then
        XCTAssert(sut.filters == nil)

    }

    func test_givenAny_whenICallRemovedAllFilters_thenICallViewHideFilters() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given

        // when
        sut.removedAllFilters()

        // then
        XCTAssert(dummyView.isCalledHideFilters == true)

    }

    func test_givenFilters_whenICallRemovedAllFilters_thenICalledShowSkeleton() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let filters = ShoppingFilter()
        sut.filters = filters

        // when
        sut.removedAllFilters()

        // then
        XCTAssert(dummyView.isCalledShowSkeleton == true)

    }

    func test_givenFilters_whenICallRemovedAllFilters_thenICallInteractorProvidePromotionsWithFilters() {

        // given
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let filters = ShoppingFilter()
        sut.filters = filters

        // when
        sut.removedAllFilters()

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionsWithFilters == true)

    }

    func test_givenFiltersAndUserLocationNil_whenICallRemovedAllFilters_thenICallInteractorProvidePromotionsWithFiltersWithDefaultValues() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let filters = ShoppingFilter()
        sut.filters = filters
        sut.userLocation = nil

        // when
        sut.removedAllFilters()

        // then
        let shoppingParamLocationExpected = ShoppingParamLocation(geolocation: sut.defaultLocation, distanceParam: ShoppingDistanceParam(distance: sut.defaultDistance, lengthType: sut.lengthType))

        XCTAssert(dummyInteractor.paramsSent == ShoppingParamsTransport(categoryId: nil, trendingId: nil, location: shoppingParamLocationExpected, titleIds: nil, pageSize: nil, orderBy: nil))
        XCTAssert(dummyInteractor.filtersSent == sut.filters)

    }

    func test_givenFiltersAndUserLocationFilled_whenICallRemovedAllFilters_thenICallInteractorProvidePromotionsWithFiltersWithDefaultValues() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let filters = ShoppingFilter()
        sut.filters = filters
        sut.userLocation = GeoLocationBO(withLatitude: 22.0, withLongitude: 40.0)

        // when
        sut.removedAllFilters()

        // then
        let shoppingParamLocationExpected = ShoppingParamLocation(geolocation: sut.userLocation!, distanceParam: ShoppingDistanceParam(distance: sut.defaultDistance, lengthType: sut.lengthType))

        XCTAssert(dummyInteractor.paramsSent == ShoppingParamsTransport(categoryId: nil, trendingId: nil, location: shoppingParamLocationExpected, titleIds: nil, pageSize: nil, orderBy: nil))
        XCTAssert(dummyInteractor.filtersSent == nil)

    }

    func test_givenFiltersAndUserLocationNil_whenICallRemovedAllFilters_thenICallInteractorProvidePromotionsWithFiltersWithDefaultValuesAndFilters() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let filters = ShoppingFilter()
        sut.filters = filters

        sut.filters?.textFilter = "text filter"
        sut.filters?.promotionUsageTypeFilter = [.physical, .online]
        sut.filters?.promotionTypeFilter = [.point]
        sut.filters?.categoriesFilter = [.activities]
        sut.userLocation = nil

        // when
        sut.removedAllFilters()

        // then
        let shoppingParamLocationExpected = ShoppingParamLocation(geolocation: sut.defaultLocation, distanceParam: ShoppingDistanceParam(distance: sut.defaultDistance, lengthType: sut.lengthType))

        XCTAssert(dummyInteractor.paramsSent == ShoppingParamsTransport(categoryId: nil, trendingId: nil, location: shoppingParamLocationExpected, titleIds: nil, pageSize: nil, orderBy: nil))
        XCTAssert(dummyInteractor.filtersSent == sut.filters)

    }

    func test_givenNothing_whenICallRemovedAllFiltersAndInteractorReturnSuccess_thenICallHideSkeleton() {

        let dummyInteractor = DummyInteractor()
        dummyInteractor.responseStatus = ConstantsHTTPCodes.STATUS_OK
        sut.interactor = dummyInteractor

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.filters = ShoppingFilter()

        // when
        sut.removedAllFilters()

        // then
        XCTAssert(dummyView.isCalledHideSkeleton == true)
    }

    func test_givenNothing_whenICallRemovedAllFiltersAndNotContentResponse_thenINotCallHideSkeleton() {

        let dummyInteractor = DummyInteractor()
        dummyInteractor.responseStatus = ConstantsHTTPCodes.NO_CONTENT
        sut.interactor = dummyInteractor

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.filters = ShoppingFilter()

        // when
        sut.removedAllFilters()

        // then
        XCTAssert(dummyView.isCalledHideSkeleton == false)
    }

    func test_givenNothing_whenICallRemovedAllFiltersAndInteractorReturnError_thenICallHideSkeleton() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let errorEntity = ErrorEntity()
        errorEntity.code = "500"
        errorEntity.message = "Error 500"

        dummyInteractor.forceError = true
        dummyInteractor.errorBO = ErrorBO(error: errorEntity)

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.filters = ShoppingFilter()

        // when
        sut.removedAllFilters()

        // then
        XCTAssert(dummyView.isCalledHideSkeleton == true)
    }

    func test_givenPromotions_whenICallRemovedAllFiltersAndIsASuccessfulResponse_thenCallShowPromotions() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.filters = ShoppingFilter()

        // when
        sut.removedAllFilters()

        //then
        XCTAssert(dummyView.isCalledShowPromotions == true)

    }

    func test_givenPromotions_whenICallRemovedAllFiltersAndNoContentResponse_thenINotCallShowPromotions() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.responseStatus = ConstantsHTTPCodes.NO_CONTENT
        sut.interactor = dummyInteractor

        sut.filters = ShoppingFilter()

        // when
        sut.removedAllFilters()

        //then
        XCTAssert(dummyView.isCalledShowPromotions == false)

    }

    func test_givenPromotions_whenICallRemovedAllFiltersAndIsAErrorResponse_thenINotCallShowPromotions() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        dummyInteractor.forceError = true

        sut.filters = ShoppingFilter()

        // when
        sut.removedAllFilters()

        //then
        XCTAssert(dummyView.isCalledShowPromotions == false)

    }

    func test_givenPromotions_whenICallRemovedAllFiltersAndIsASuccessfulResponse_thenCreateDisplayItems() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.filters = ShoppingFilter()

        // when
        sut.removedAllFilters()

        //then
        XCTAssert(!sut.displayItems.items.isEmpty)

    }

    func test_givenPromotions_whenICallRemovedAllFiltersAndIsASuccessfulResponse_thenDisplayItemsMatchWithReturnedDisplay() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.filters = ShoppingFilter()

        // when
        sut.removedAllFilters()

        //then
        XCTAssert(sut.displayItems === dummyView.displayItemPromotionsSent)

    }

    func test_givenPromotions_whenICallRemovedAllFiltersAndNotContentResponse_thenICallShowNoContentView() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.responseStatus = ConstantsHTTPCodes.NO_CONTENT
        sut.interactor = dummyInteractor

        sut.filters = ShoppingFilter()

        // when
        sut.removedAllFilters()

        // then
        XCTAssert(dummyView.isCalledShowNoContentView == true)

    }

    func test_givenPromotions_whenICallRemovedAllFiltersAndInteractorReturnError_thenICallViewShowEmptyTsec() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()

        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        sut.interactor = dummyInteractor

        sut.filters = ShoppingFilter()

        // when
        sut.removedAllFilters()

        // then
        XCTAssert(dummyView.isCalledShowError == true)

    }

    // MARK: - GoBackAction

    func test_givenAny_whenICallGoBackAction_thenCallRouterPresentShoppingFilter() {

        // given

        // when
        sut.goBackAction()

        // then
        XCTAssert(dummyBusManager.isCalledNavigateShoppingFilterFromShoppingListFiltered == true)

    }

    func test_givenCategoriesBO_whenICallGoBackAction_thenFilterTransportShouldBeFilled() {

        // given
        sut.categoriesBO = PromotionsGenerator.getCategoryBO()

        // when
        sut.goBackAction()

        // then
        XCTAssert(dummyBusManager.shoppingFilterTransport != nil)

    }

    func test_givenCategoriesBO_whenICallGoBackAction_thenCategoriesBOShouldMatch() {

        // given
        sut.categoriesBO = PromotionsGenerator.getCategoryBO()

        // when
        sut.goBackAction()

        // then
        XCTAssert(dummyBusManager.shoppingFilterTransport?.categoriesBO === sut.categoriesBO)

    }

    func test_givenFilters_whenICallGoBackAction_thenFilterTransportShouldBeFilled() {

        // given
        sut.filters = ShoppingFilter()

        // when
        sut.goBackAction()

        // then
        XCTAssert(dummyBusManager.shoppingFilterTransport != nil)

    }

    func test_givenFilters_whenICallGoBackAction_thenFiltersShouldMatchWithTransport() {

        // given
        sut.filters = ShoppingFilter()

        // when
        sut.goBackAction()

        // then
        XCTAssert(dummyBusManager.shoppingFilterTransport?.filters == sut.filters)

    }

    func test_givenFiltersAndAllowSelectUsageTypeTrue_whenICallGoBackAction_thenShouldAllowSelectUsageTypeIsTrue() {

        // given
        sut.filters = ShoppingFilter()
        dummyBusManager.forceAllowSelectUsageType = true

        // when
        sut.goBackAction()

        // then
        XCTAssert(dummyBusManager.shoppingFilterTransport?.shouldAllowSelectUsageType == true)

    }

    func test_givenFiltersNil_whenICallGoBackAction_thenFilterTransportFiltersShouldBeNil() {

        // given
        sut.filters = nil

        // when
        sut.goBackAction()

        // then
        XCTAssert(dummyBusManager.shoppingFilterTransport.filters == nil)

    }

    func test_givenCategoriesBONil_whenICallGoBackAction_thenFilterTransportCategoriesBOShouldBeNil() {

        // given
        sut.categoriesBO = nil

        // when
        sut.goBackAction()

        // then
        XCTAssert(dummyBusManager.shoppingFilterTransport.categoriesBO == nil)

    }

    // MARK: - retryButtonPressed

    func test_givenAny_whenICallRetryButtonPressed_thenICallViewShowSkeleton() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(dummyView.isCalledShowSkeleton == true)

    }

    func test_givenAny_whenICallRetryButtonPressed_thenICallInteractorProvidePromotionsWithFilters() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionsWithFilters == true)
    }

    func test_givenADefaultLocationAndNoUserLocation_whenICallRetryButtonPressed_thenICallInteractorProvidePromotionsWithFiltersWithDefaultLocation() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let dummyLocation = GeoLocationBO(withLatitude: 22.0, withLongitude: 40.0)
        sut.defaultLocation = dummyLocation
        sut.userLocation = nil

        // when
        sut.retryButtonPressed()

        // then
        let shoppingParamLocationExpected = ShoppingParamLocation(geolocation: sut.defaultLocation, distanceParam: ShoppingDistanceParam(distance: sut.defaultDistance, lengthType: sut.lengthType))

        XCTAssert(dummyInteractor.paramsSent == ShoppingParamsTransport(categoryId: nil, trendingId: nil, location: shoppingParamLocationExpected, titleIds: nil, pageSize: nil, orderBy: nil))    }

    func test_givenAUserLocation_whenICallRetryButtonPressed_thenICallInteractorProvidePromotionsWithFiltersWithUserLocation() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let dummyLocation = GeoLocationBO(withLatitude: 22.0, withLongitude: 40.0)
        sut.userLocation = dummyLocation

        // when
        sut.retryButtonPressed()

        // then
        let shoppingParamLocationExpected = ShoppingParamLocation(geolocation: sut.userLocation!, distanceParam: ShoppingDistanceParam(distance: sut.defaultDistance, lengthType: sut.lengthType))

        XCTAssert(dummyInteractor.paramsSent == ShoppingParamsTransport(categoryId: nil, trendingId: nil, location: shoppingParamLocationExpected, titleIds: nil, pageSize: nil, orderBy: nil))    }

    func test_givenAny_whenICallRetryButtonPressedAndInteractorReturnedError_thenICallViewShowError() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()

        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        sut.interactor = dummyInteractor

        // given

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(dummyView.isCalledShowError == true)
    }

    func test_givenAny_whenICallRetryButtonPressedAndInteractorReturnedNoContentResponse_thenICallViewShowNoContentView() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.responseStatus = ConstantsHTTPCodes.NO_CONTENT
        sut.interactor = dummyInteractor

        // given

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(dummyView.isCalledShowNoContentView == true)
    }

    func test_givenAny_whenICallRetryButtonPressedAndInteractorReturnedAPromotionsBO_thenICallViewHideSkeleton() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        sut.interactor = dummyInteractor

        // given

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(dummyView.isCalledHideSkeleton == true)
    }

    func test_givenAny_whenICallRetryButtonPressedAndInteractorReturnedAPromotionsBO_thenICallViewShowPromotions() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        sut.interactor = dummyInteractor

        // given

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(dummyView.isCalledShowPromotions == true)
    }

    func test_givenAny_whenICallRetryButtonPressedAndInteractorReturnedAPromotionsBO_thenICallViewShowPromotionsWithRightDisplayData() {

        let dummyView = DummyView()
        sut.view = dummyView

        let promotionsBO = PromotionsGenerator.getPromotionsBO()
        let displayItems = DisplayItemsPromotions()

        for promotionBO in promotionsBO.promotions {

            let displayItemPromotion = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionBO, withTypeCell: CellPromotionType.small)
            displayItems.items?.append(displayItemPromotion)
        }

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = promotionsBO
        sut.interactor = dummyInteractor

        // given

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(dummyView.displayItemPromotionsSent! == displayItems)
    }

    // MARK: - retryButtonPressed - Pagination

    func test_givenAPromotionsBOWithoutPagination_whenICallRetryButtonPressed_thenICallViewUpdateForNoMoreContent() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBOWithoutPagination()
        sut.interactor = dummyInteractor

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == true)
    }

    func test_givenAPromotionsBOWithLinkNextNil_whenICallRetryButtonPressed_thenICallViewUpdateForNoMoreContent() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        dummyInteractor.promotionsBO.pagination?.links?.next = nil
        sut.interactor = dummyInteractor

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == true)
    }

    func test_givenAPromotionsBOWithLinkNextEmpty_whenICallRetryButtonPressed_thenICallViewUpdateForNoMoreContent() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        dummyInteractor.promotionsBO.pagination?.links?.next = ""
        sut.interactor = dummyInteractor

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == true)
    }

    func test_givenAPromotionsBOWithoutPagination_whenICallRetryButtonPressed_thenINotCallViewUpdateForMoreContent() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBOWithoutPagination()
        sut.interactor = dummyInteractor

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(dummyView.isCalledUpdateForMoreContent == false)
    }

    func test_givenAPromotionsBOWithLinkNextNil_whenICallRetryButtonPressed_thenINotCallViewUpdateForMoreContent() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        dummyInteractor.promotionsBO.pagination?.links?.next = nil
        sut.interactor = dummyInteractor

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(dummyView.isCalledUpdateForMoreContent == false)
    }

    func test_givenAPromotionsBOWithLinkNextEmpty_whenICallRetryButtonPressed_thenINotCallViewUpdateForMoreContent() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        dummyInteractor.promotionsBO.pagination?.links?.next = ""
        sut.interactor = dummyInteractor

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(dummyView.isCalledUpdateForMoreContent == false)
    }

    func test_givenAPromotionsBOWithLinkNext_whenICallRetryButtonPressed_thenICallViewUpdateForMoreContent() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        sut.interactor = dummyInteractor

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(dummyView.isCalledUpdateForMoreContent == true)
    }

    func test_givenAPromotionsBOWithPaginationAndLinkNext_whenICallRetryButtonPressed_thenIDoNotCallViewUpdateForNoMoreContent() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        sut.interactor = dummyInteractor

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == false)
    }

    // MARK: - itemSelected

    func test_givenADisplayItemPromotion_whenICallItemSelected_thenICallBusNavigateToDetailScreen() {

        // given
        let promotionsBO = PromotionsGenerator.getPromotionsBO()
        let displayItem = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionsBO.promotions[0])

        // when
        sut.itemSelected(withDisplayItem: displayItem)

        // then
        XCTAssert(dummyBusManager.isCalledNavigateToDetailScreen == true)

    }

    func test_givenNothing_whenICallItemSelected_thenINotCallBusNavigateToDetailScreen() {

        // given
        let displayItem = DisplayItemPromotion()

        // when
        sut.itemSelected(withDisplayItem: displayItem)

        // then
        XCTAssert(dummyBusManager.isCalledNavigateToDetailScreen == false)

    }

    func test_givenADisplayItemPromotion_whenICallItemSelected_thenICallBusNavigateToDetailScreenAndMatchPromotion() {

        // given
        let promotionsBO = PromotionsGenerator.getPromotionsBO()
        let displayItem = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionsBO.promotions[0])

        // when
        sut.itemSelected(withDisplayItem: displayItem)

        // then
        XCTAssert(dummyBusManager.promotionDetailTransport.promotionBO == displayItem.promotionBO)
        XCTAssert(dummyBusManager.promotionDetailTransport.userLocation == sut.userLocation)

    }

    // MARK: DummyData

    class DummyInteractor: ShoppingListFilteredInteractorProtocol {

        var isCalledProvidePromotionsWithFilters = false
        var isCalledProvidePromotionsWithFiltersNextPage = false

        var promotionsBO: PromotionsBO!
        var responseStatus = 200

        var paramsSent = ShoppingParamsTransport()
        var filtersSent: ShoppingFilter?

        var errorBO: ErrorBO?
        var forceError = false

        var presenter: ShoppingListFilteredPresenter<DummyView>?

        var nextPageLink: String?

        var wasLoadingNextPage = true

        func providePromotions(byParams params: ShoppingParamsTransport, withFilters filters: ShoppingFilter?) -> Observable<ModelBO> {

            paramsSent = params
            filtersSent = filters

            isCalledProvidePromotionsWithFilters = true

            if !forceError {

                if promotionsBO == nil {
                    promotionsBO = PromotionsGenerator.getPromotionsBO()
                }

                promotionsBO.status = responseStatus

                if let presenter = self.presenter {
                    self.wasLoadingNextPage = presenter.isLoadingNextPage
                }

                return Observable <ModelBO>.just(promotionsBO!)

            } else {

                if errorBO == nil {
                    errorBO = ErrorBO(error: ErrorEntity(message: "Error"))
                    errorBO!.isErrorShown = false
                }

                return Observable.error(ServiceError.GenericErrorBO(error: errorBO!))
            }
        }

        func providePromotionsWithFilters(withNextPage nextPage: String) -> Observable<ModelBO> {

            isCalledProvidePromotionsWithFiltersNextPage = true

            nextPageLink = nextPage

            if !forceError {

                if promotionsBO == nil {
                    promotionsBO = PromotionsGenerator.getPromotionsBO()
                }

                promotionsBO.status = responseStatus

                return Observable <ModelBO>.just(promotionsBO!)

            } else {

                if errorBO == nil {
                    errorBO = ErrorBO(error: ErrorEntity(message: "Error"))
                    errorBO!.isErrorShown = false
                }

                return Observable.error(ServiceError.GenericErrorBO(error: errorBO!))
            }
        }

    }

    class DummyView: ShoppingListFilteredViewProtocol, ViewProtocol {

        var isShowErrorCalled = false
        var isCalledShowSkeleton = false
        var isCalledHideSkeleton = false
        var distanceFilterSent: Double?
        var isCalledShowPromotions = false
        var displayItemPromotionsSent: DisplayItemsPromotions?

        var isCalledUpdateForNoMoreContent = false
        var isCalledUpdateForMoreContent = false
        var isCalledShowNextPageAnimation = false
        var isCalledHideNextPageAnimation = false

        var isCalledShowError = false
        var isCalledShowNoContentView = false

        var isCalledShowFilters = false
        var isCalledHideFilters = false

        var displayDataFilterScrollSent: [FilterScrollDisplayData]?

        func showError(error: ModelBO) {
            isShowErrorCalled = true
        }

        func changeColorEditTexts() {
        }

        func showSkeleton() {
            isCalledShowSkeleton = true
        }

        func hideSkeleton() {
            isCalledHideSkeleton = true
        }

        func showPromotions(withDisplayItemsPromotions displayItemPromotions: DisplayItemsPromotions) {
            isCalledShowPromotions = true
            displayItemPromotionsSent = displayItemPromotions
        }

        func updateForNoMoreContent() {
            isCalledUpdateForNoMoreContent = true
        }

        func updateForMoreContent() {
            isCalledUpdateForMoreContent = true
        }

        func showNextPageAnimation() {
            isCalledShowNextPageAnimation = true
        }

        func hideNextPageAnimation() {
            isCalledHideNextPageAnimation = true
        }

        func showError() {
            isCalledShowError = true
        }

        func showNoContentView() {
            isCalledShowNoContentView = true
        }

        func showFilters(withDisplayData displayData: [FilterScrollDisplayData]) {
            isCalledShowFilters = true
            displayDataFilterScrollSent = displayData
        }

        func hideFilters() {
            isCalledHideFilters = true
        }

    }

    class DummyShoppingFilter: ShoppingFilter {

        var isCalledRetrieveLiterals = false
        var isCalledRemoveByFilterType = false

        var defaultLiterals: [(title: String, key: String)]?
        var sentFilterType: String?

        override func retrieveLiterals(withCategories categoriesBO: CategoriesBO) -> [(title: String, key: String)] {

            isCalledRetrieveLiterals = true

            if let defaultLiterals = defaultLiterals {
                return defaultLiterals
            } else {
                return super.retrieveLiterals(withCategories: categoriesBO)
            }

        }

        override func remove(byFilterType filterType: String) {

            isCalledRemoveByFilterType = true
            sentFilterType = filterType

        }

    }

    class DummyBusManager: BusManager {

        var isCalledNavigateShoppingFilterFromShoppingListFiltered = false
        var isCalledGetViewControllerFromNavigation = false

        var shoppingFilterTransport: ShoppingFilterTransport!
        var forceAllowSelectUsageType = false

        var isCalledNavigateToDetailScreen = false
        var promotionDetailTransport: PromotionDetailTransport!

        override init() {
            super.init()
            routerFactory = RouterInitFactory()
        }

        override func navigateScreen<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {

            if tag == ShoppingListFilteredPageReaction.ROUTER_TAG && event === ShoppingListFilteredPageReaction.EVENT_NAV_TO_SHOPPING_FILTER {
                isCalledNavigateShoppingFilterFromShoppingListFiltered = true

                shoppingFilterTransport = (values as! ShoppingFilterTransport)

                if forceAllowSelectUsageType {
                    shoppingFilterTransport.shouldAllowSelectUsageType = true
                }

            } else if tag == ShoppingListFilteredPageReaction.ROUTER_TAG && event === ShoppingListFilteredPageReaction.EVENT_NAV_TO_DETAIL_SCREEN {

                isCalledNavigateToDetailScreen = true
                promotionDetailTransport = (values as! PromotionDetailTransport)
            }
        }
    }
}
