//
//  HelpDetailDisplayDataTests.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 16/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class HelpDetailDisplayDataTests: XCTestCase {

    func test_givenHelpDetailBOWithLanguageMatch_whenICallInit_thenHelpDetailDisplayDataBeFilled() {

        // given
        let helpDetailBO = HelpDetailGenerator.getHelpDetailBO()
        HelpDetailDisplayData.language = "es"

        // when
        let displayData = HelpDetailDisplayData(helpDetailBO)

        // then
        XCTAssert(displayData.titleHeader != nil)
        XCTAssert(displayData.imageHeaderURL != nil)
        XCTAssert(displayData.videoHeaderURL != nil)
        XCTAssert(!displayData.questions.isEmpty)

        XCTAssert(displayData.questions[0].title != nil)
        XCTAssert(displayData.questions[1].title != nil)
        XCTAssert(displayData.questions[2].title != nil)
        XCTAssert(displayData.questions[3].title != nil)
        XCTAssert(displayData.questions[4].title != nil)
        XCTAssert(displayData.questions[5].title != nil)

        XCTAssert(displayData.questions[0].answers[0].data != nil)
        XCTAssert(displayData.questions[0].answers[1].data != nil)
        XCTAssert(displayData.questions[0].answers[1].extraData != nil)
        XCTAssert(displayData.questions[1].answers[0].data != nil)
    }

    func test_givenHelpDetailBOWithoutNothing_whenICallInit_thenHelpDetailDisplayDataBeEmpty() {

        // given
        let helpDetailBO = HelpDetailGenerator.getHelpDetailBO()
        HelpDetailDisplayData.language = "es"
        helpDetailBO.title = nil
        helpDetailBO.image = nil
        helpDetailBO.video = nil
        helpDetailBO.questions = nil

        // when
        let displayData = HelpDetailDisplayData(helpDetailBO)

        // then
        XCTAssert(displayData.titleHeader == nil)
        XCTAssert(displayData.imageHeaderURL == nil)
        XCTAssert(displayData.videoHeaderURL == nil)
        XCTAssert(displayData.questions.isEmpty)

    }

    func test_givenHelpDetailBOWithNilProperties_whenICallInit_thenHelpDetailDisplayDataBeEmpty() {

        // given
        let helpDetailBO = HelpDetailGenerator.getHelpDetailBO()
        HelpDetailDisplayData.language = "es"
        helpDetailBO.title![0].content = nil
        helpDetailBO.image![0].content = nil
        helpDetailBO.video![0].content = nil
        helpDetailBO.questions = nil

        // when
        let displayData = HelpDetailDisplayData(helpDetailBO)

        // then
        XCTAssert(displayData.titleHeader == nil)
        XCTAssert(displayData.imageHeaderURL == nil)
        XCTAssert(displayData.videoHeaderURL == nil)
        XCTAssert(displayData.questions.isEmpty)

    }

    func test_givenHelpDetailBOWithoutEmptyProperties_whenICallInit_thenHelpDetailDisplayDataBeEmpty() {

        // given
        let helpDetailBO = HelpDetailGenerator.getHelpDetailBO()
        HelpDetailDisplayData.language = "es"
        helpDetailBO.title![0].content = ""
        helpDetailBO.image![0].content = ""
        helpDetailBO.video![0].content = ""
        helpDetailBO.questions = nil

        // when
        let displayData = HelpDetailDisplayData(helpDetailBO)

        // then
        XCTAssert(displayData.titleHeader == nil)
        XCTAssert(displayData.imageHeaderURL == nil)
        XCTAssert(displayData.videoHeaderURL == nil)
        XCTAssert(displayData.questions.isEmpty)

    }

    func test_givenHelpDetailBOWithLanguageNoMatch_whenICallInit_thenHelpDetailDisplayDataBeEmpty() {

        // given
        let helpDetailBO = HelpDetailGenerator.getHelpDetailBO()
        HelpDetailDisplayData.language = "mx"

        // when
        let displayData = HelpDetailDisplayData(helpDetailBO)

        // then
        XCTAssert(displayData.titleHeader == nil)
        XCTAssert(displayData.imageHeaderURL == nil)
        XCTAssert(displayData.videoHeaderURL == nil)
        XCTAssert(displayData.questions[0].title == nil)
        XCTAssert(displayData.questions[1].title == nil)
        XCTAssert(displayData.questions[2].title == nil)
        XCTAssert(displayData.questions[3].title == nil)
        XCTAssert(displayData.questions[4].title == nil)
        XCTAssert(displayData.questions[5].title == nil)

        XCTAssert(displayData.questions[0].answers[0].data == nil)
        XCTAssert(displayData.questions[0].answers[0].extraData == nil)
        XCTAssert(displayData.questions[0].answers[1].data == nil)
        XCTAssert(displayData.questions[0].answers[1].extraData == nil)
        XCTAssert(displayData.questions[1].answers[0].data == nil)
        XCTAssert(displayData.questions[1].answers[0].extraData == nil)

    }

    func test_givenHelpDetailBOWithLanguageMatchAndTypeVideo_whenICallInit_thenHelpDetailDisplayDataBeFilledAndAnswersHasExtraData() {

        // given
        let helpDetailBO = HelpDetailGenerator.getHelpDetailBO()
        HelpDetailDisplayData.language = "es"
        helpDetailBO.questions![0].answers![1].type = .video
        helpDetailBO.questions![0].answers![1].data![0].content = "data"
        helpDetailBO.questions![0].answers![1].extraData![0].content = "extraData"

        // when
        let displayData = HelpDetailDisplayData(helpDetailBO)

        // then
        XCTAssert(displayData.questions[0].answers[1].extraData != nil)
        XCTAssert(displayData.questions[0].answers[1].extraData?.isEmpty == false)
    }

    func test_givenHelpDetailBOWithLanguageMatchAndTypeText_whenICallInit_thenHelpDetailDisplayDataBeFilledAndAnswersHasDataButNotExtraData() {

        // given
        let helpDetailBO = HelpDetailGenerator.getHelpDetailBO()
        HelpDetailDisplayData.language = "es"

        helpDetailBO.questions![0].answers![0].type = .text
        helpDetailBO.questions![0].answers![0].data![0].content = "data"
        helpDetailBO.questions![0].answers![0].extraData = nil

        // when
        let displayData = HelpDetailDisplayData(helpDetailBO)

        // then
        XCTAssert(displayData.questions[0].answers[0].data != nil)
        XCTAssert(displayData.questions[0].answers[0].data?.isEmpty == false)
        XCTAssert(displayData.questions[0].answers[0].extraData == nil)
    }

    func test_givenHelpDetailBOWithLanguageMatchAndTypeText_whenICallInit_thenHelpDetailDisplayDataBeFilledWithTypeText() {

        // given
        let helpDetailBO = HelpDetailGenerator.getHelpDetailBO()
        HelpDetailDisplayData.language = "es"
        helpDetailBO.questions![0].answers![0].type = .text

        // when
        let displayData = HelpDetailDisplayData(helpDetailBO)

        // then
        XCTAssert(displayData.questions[0].answers[0].type == .text)
    }

    func test_givenHelpDetailBOWithLanguageMatchAndTypeImage_whenICallInit_thenHelpDetailDisplayDataBeFilledWithTypeImage() {

        // given
        let helpDetailBO = HelpDetailGenerator.getHelpDetailBO()
        HelpDetailDisplayData.language = "es"
        helpDetailBO.questions![0].answers![0].type = .image

        // when
        let displayData = HelpDetailDisplayData(helpDetailBO)

        // then
        XCTAssert(displayData.questions[0].answers[0].type == .image)
    }

    func test_givenHelpDetailBOWithLanguageMatchAndTypeVideo_whenICallInit_thenHelpDetailDisplayDataBeFilledWithTypeVideo() {

        // given
        let helpDetailBO = HelpDetailGenerator.getHelpDetailBO()
        HelpDetailDisplayData.language = "es"
        helpDetailBO.questions![0].answers![0].type = .video

        // when
        let displayData = HelpDetailDisplayData(helpDetailBO)

        // then
        XCTAssert(displayData.questions[0].answers[0].type == .video)
    }
}
