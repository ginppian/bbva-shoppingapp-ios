//
//  HelpDetailPresenterTests.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 17/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest
import CellsNativeComponents

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class HelpDetailPresenterTests: XCTestCase {

    var sut: HelpDetailPresenter<HelpDetailViewDummy>!
    var dummyBusManager: DummyBusManager!

    override func setUp() {
        super.setUp()
        dummyBusManager = DummyBusManager()
        sut = HelpDetailPresenter<HelpDetailViewDummy>(busManager: dummyBusManager)
    }

    // MARK: - setModel

    func test_givenHelpDetailTransport_whenICallSetModel_thenHelpIdShouldBeFilled() {

        // given
        let helpDetailTransport = HelpDetailTransport("helpId")

        // when
        sut.setModel(model: helpDetailTransport)

        // then
        XCTAssert(sut.helpId != nil)

    }

    func test_givenHelpDetailTransport_whenICallSetModel_thenHelpIdShouldMatch() {

        // given
        let helpDetailTransport = HelpDetailTransport("helpId")

        // when
        sut.setModel(model: helpDetailTransport)

        // then
        XCTAssert(sut.helpId == helpDetailTransport.helpId)

    }

    // MARK: - getHelpDetail

    func test_giveHelpIdEmpty_whenICallGetHelpDetail_thenINotCallReadResourceFromBundleAsString() {

        //given
        let dummyView = HelpDetailViewDummy()
        sut.view = dummyView
        sut.helpId = ""

        let dummyFileProvider = DummyFileProvider()
        sut.fileProvider = dummyFileProvider

        //When
        sut.getHelpDetail()

        //then
        XCTAssert(dummyFileProvider.isCalledReadResourceFromBundleAsString == false)

    }

    func test_giveHelpIdNil_whenICallGetHelpDetail_thenINotCallReadResourceFromBundleAsString() {

        //given
        let dummyView = HelpDetailViewDummy()
        sut.view = dummyView
        sut.helpId = nil

        let dummyFileProvider = DummyFileProvider()
        sut.fileProvider = dummyFileProvider

        //When
        sut.getHelpDetail()

        //then
        XCTAssert(dummyFileProvider.isCalledReadResourceFromBundleAsString == false)

    }

    func test_giveHelpIdEmpty_whenICallGetHelpDetail_thenINotCallShowHelpDetail() {

        //given
        let dummyView = HelpDetailViewDummy()
        sut.view = dummyView
        sut.helpId = ""

        //When
        sut.getHelpDetail()

        //then
        XCTAssert(dummyView.isCalledShowHelpDetail == false)

    }

    func test_giveHelpIdNil_whenICallGetHelpDetail_thenINotCallShowHelpDetail() {

        //given
        let dummyView = HelpDetailViewDummy()
        sut.view = dummyView
        sut.helpId = nil

        //When
        sut.getHelpDetail()

        //then
        XCTAssert(dummyView.isCalledShowHelpDetail == false)

    }

    func test_giveHelpId_whenICallGetHelpDetail_thenICallReadResourceFromBundleAsString() {

        //given
        let dummyView = HelpDetailViewDummy()
        sut.view = dummyView
        sut.helpId = "helpId"

        let dummyFileProvider = DummyFileProvider()
        sut.fileProvider = dummyFileProvider

        //When
        sut.getHelpDetail()

        //then
        XCTAssert(dummyFileProvider.isCalledReadResourceFromBundleAsString == true)

    }

    func test_giveHelpId_whenICallGetHelpDetail_thenResourceMatchToHelpId() {

        //given
        let dummyView = HelpDetailViewDummy()
        sut.view = dummyView
        sut.helpId = "helpId"

        let dummyFileProvider = DummyFileProvider()
        sut.fileProvider = dummyFileProvider

        //When
        sut.getHelpDetail()

        //then
        XCTAssert(dummyFileProvider.resourceSent == sut.helpId)

    }

    func test_giveHelpId_whenICallGetHelpDetail_thenTypeResourceIsJson() {

        //given
        let dummyView = HelpDetailViewDummy()
        sut.view = dummyView
        sut.helpId = "helpId"

        let dummyFileProvider = DummyFileProvider()
        sut.fileProvider = dummyFileProvider

        //When
        sut.getHelpDetail()

        //then
        XCTAssert(dummyFileProvider.typeResourceSent == "json")

    }

    func test_giveHelpIdAndCorrectJson_whenICallGetHelpDetail_thenICallShowHelpDetail() {

        //given
        let dummyView = HelpDetailViewDummy()
        sut.view = dummyView
        sut.helpId = "helpId"

        let dummyFileProvider = DummyFileProvider()
        sut.fileProvider = dummyFileProvider
        dummyFileProvider.forceCorrectJson = true

        //When
        sut.getHelpDetail()

        //then
        XCTAssert(dummyView.isCalledShowHelpDetail == true)

    }

    func test_giveHelpIdAndFileEmpty_whenICallGetHelpDetail_thenINotCallShowHelpDetail() {

        //given
        let dummyView = HelpDetailViewDummy()
        sut.view = dummyView
        sut.helpId = "helpId"

        let dummyFileProvider = DummyFileProvider()
        sut.fileProvider = dummyFileProvider
        dummyFileProvider.forceCorrectJson = false

        //When
        sut.getHelpDetail()

        //then
        XCTAssert(dummyView.isCalledShowHelpDetail == false)

    }

    func test_giveHelpIdAndIncorrectJson_whenICallGetHelpDetail_thenINotCallViewShowHelpDetail() {

        //given
        let dummyView = HelpDetailViewDummy()
        sut.view = dummyView
        sut.helpId = "helpId"

        let dummyFileProvider = DummyFileProvider()
        sut.fileProvider = dummyFileProvider
        dummyFileProvider.forceCorrectJson = false
        dummyFileProvider.forceInCorrectJson = true

        //When
        sut.getHelpDetail()

        //then
        XCTAssert(dummyView.isCalledShowHelpDetail == false)

    }

    func test_giveHelpIdEmpty_whenICallGetHelpDetail_thenICallViewShowEmptyView() {

        //given
        let dummyView = HelpDetailViewDummy()
        sut.view = dummyView
        sut.helpId = ""

        //When
        sut.getHelpDetail()

        //then
        XCTAssert(dummyView.isCalledShowEmptyScreen == true)

    }

    func test_giveHelpIdNil_whenICallGetHelpDetail_thenICallViewShowEmptyView() {

        //given
        let dummyView = HelpDetailViewDummy()
        sut.view = dummyView
        sut.helpId = nil

        //When
        sut.getHelpDetail()

        //then
        XCTAssert(dummyView.isCalledShowEmptyScreen == true)

    }

    func test_giveHelpIdAndNotResource_whenICallGetHelpDetail_thenICallViewShowEmptyView() {

        //given
        let dummyView = HelpDetailViewDummy()
        sut.view = dummyView
        sut.helpId = "helpId"

        let dummyFileProvider = DummyFileProvider()
        sut.fileProvider = dummyFileProvider

        //When
        sut.getHelpDetail()

        //then
        XCTAssert(dummyView.isCalledShowEmptyScreen == true)

    }

    func test_giveHelpIdAndIncorrectJSON_whenICallGetHelpDetail_thenICallViewShowEmptyView() {

        //given
        let dummyView = HelpDetailViewDummy()
        sut.view = dummyView
        sut.helpId = "helpId"

        let dummyFileProvider = DummyFileProvider()
        sut.fileProvider = dummyFileProvider
        dummyFileProvider.forceCorrectJson = false
        dummyFileProvider.forceInCorrectJson = true

        //When
        sut.getHelpDetail()

        //then
        XCTAssert(dummyView.isCalledShowEmptyScreen == true)

    }

    func test_giveHelpIdAndCorrectJson_whenICallGetHelpDetail_thenDisplayDataIsFilled() {

        //given
        let dummyView = HelpDetailViewDummy()
        sut.view = dummyView
        sut.helpId = "helpId"

        let dummyFileProvider = DummyFileProvider()
        sut.fileProvider = dummyFileProvider
        dummyFileProvider.forceCorrectJson = true

        //When
        sut.getHelpDetail()

        //then
        XCTAssert(dummyView.displayDataHelpDetail != nil)

    }

    func test_giveHelpIdAndCorrectJson_whenICallGetHelpDetail_thenDisplayDataMatch() {

        //given
        let dummyView = HelpDetailViewDummy()
        sut.view = dummyView
        sut.helpId = "helpId"

        let dummyFileProvider = DummyFileProvider()
        sut.fileProvider = dummyFileProvider
        dummyFileProvider.forceCorrectJson = true

        let helpDetailBO = HelpDetailGenerator.getHelpDetailBO()
        let displayData = HelpDetailDisplayData(helpDetailBO)

        //When
        sut.getHelpDetail()

        //then
        XCTAssert(dummyView.displayDataHelpDetail?.titleHeader == displayData.titleHeader)
        XCTAssert(dummyView.displayDataHelpDetail?.imageHeaderURL == displayData.imageHeaderURL)
        XCTAssert(dummyView.displayDataHelpDetail?.videoHeaderURL == displayData.videoHeaderURL)
        XCTAssert(dummyView.displayDataHelpDetail?.questions.count == displayData.questions.count)
        XCTAssert(dummyView.displayDataHelpDetail?.questions[0].title == displayData.questions[0].title)
        XCTAssert(dummyView.displayDataHelpDetail?.questions[1].title == displayData.questions[1].title)
        XCTAssert(dummyView.displayDataHelpDetail?.questions[0].answers.count == displayData.questions[0].answers.count)
        XCTAssert(dummyView.displayDataHelpDetail?.questions[1].answers.count == displayData.questions[1].answers.count)

        XCTAssert(dummyView.displayDataHelpDetail?.questions[0].answers[0].type == displayData.questions[0].answers[0].type)
        XCTAssert(dummyView.displayDataHelpDetail?.questions[0].answers[0].data == displayData.questions[0].answers[0].data)

        XCTAssert(dummyView.displayDataHelpDetail?.questions[0].answers[1].type == displayData.questions[0].answers[1].type)
        XCTAssert(dummyView.displayDataHelpDetail?.questions[0].answers[1].data == displayData.questions[0].answers[1].data)
        XCTAssert(dummyView.displayDataHelpDetail?.questions[0].answers[1].extraData == displayData.questions[0].answers[1].extraData)
    }
    
    // MARK: - navigateToWebPageScreen
    
    func test_giveAny_whenICallNavigateToWebPageScreen_thenICallBusNavigateToWebPageScreen() {
        
        //given
        let dummyView = HelpDetailViewDummy()
        sut.view = dummyView
        
        let url = URL(string: "https://bbva.com")
        
        //When
        sut.navigateToWebPageScreen(withURL: url!)
        
        //then
        XCTAssert(dummyBusManager.isCalledNavigateToWebPageScreen == true)
        
    }
    
    func test_giveAny_whenICallNavigateToWebPageScreen_thenICallBusNavigateToWebPageScreenWithCorrectTransport() {
        
        //given
        let dummyView = HelpDetailViewDummy()
        sut.view = dummyView
        
        let url = URL(string: "https://bbva.com")
        
        //When
        sut.navigateToWebPageScreen(withURL: url!)
        
        //then
        XCTAssert(dummyBusManager.dummyWebValue.webPage == url?.absoluteString)
        XCTAssert(dummyBusManager.dummyWebValue.webTitle == url?.absoluteString)
        
    }

    class HelpDetailViewDummy: HelpDetailViewProtocol {

        var isCalledShowHelpDetail = false
        var isCalledShowEmptyScreen = false

        var displayDataHelpDetail: HelpDetailDisplayData?

        func showError(error: ModelBO) {
        }

        func changeColorEditTexts() {
        }

        func showHelpDetail(withDisplayData displayData: HelpDetailDisplayData) {
            isCalledShowHelpDetail = true
            displayDataHelpDetail = displayData
        }

        func showEmptyView() {
            isCalledShowEmptyScreen = true
        }

    }

    class DummyBusManager: BusManager {

        var isCalledNavigateToWebPageScreen = false
        var dummyWebValue = WebTransport()
        
        override init() {
            super.init()
            
            routerFactory = RouterInitFactory()
        }
        
        override func navigateScreen<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {
            
            if tag == HelpDetailPageReaction.ROUTER_TAG && event === HelpDetailPageReaction.EVENT_NAV_TO_WEBPAGE_SCREEN {
                isCalledNavigateToWebPageScreen = true
                dummyWebValue = values as! WebTransport
            }
        }
    }

    class DummyFileProvider: FileProvider {

        var isCalledReadResourceFromBundleAsString = false
        var resourceSent = ""
        var typeResourceSent = ""
        var forceCorrectJson = false
        var forceInCorrectJson = false

        override func readResourceFromBundleAsString(forResource resource: String, ofType type: String) -> String {

            isCalledReadResourceFromBundleAsString = true
            resourceSent = resource
            typeResourceSent = type

            if forceCorrectJson {
                return HelpDetailGenerator.getHelpDetailEntity().toJSONString()!
            }

            if forceInCorrectJson {
                return "Incorrect JSON"
            }

            return ""
        }
    }
}
