//
//  RewardBreakDownDisplayDataTests.swift
//  shoppingapp
//
//  Created by jesus.martinez on 19/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class RewardBreakDownDisplayDataTests: XCTestCase {

    // MARK: - init rewards

    func test_givenBalanceRewardsComplete_whenICallInit_thenAllFieldsShouldMatch() {

        // given
        let balanceRewards = BalanceRewardsGenerator.balanceCompleteBO()

        // when
        let sut = RewardBreakDownDisplayData(rewards: balanceRewards)

        // then      
        let startDateFormatted = balanceRewards.startDate!.string(format: Date.DATE_FORMAT_DAY_MONTH_SHORT)
        let endDateFormatted = balanceRewards.endDate!.string(format: Date.DATE_FORMAT_DAY_MONTH_YEAR_SHORT)
        XCTAssert(sut.dateFromToTitle == Localizables.common.key_from_text + " \(startDateFormatted) " + Localizables.common.key_to_text +  " \(endDateFormatted)")

        XCTAssert(sut.currentPoints == NumberFormatter.stringDecimalToStringWithSeparator(withString: "\(balanceRewards.rewardBreakDown(.current)!.nonMonetaryValue)", withLocale: .current))
        XCTAssert(sut.previousPoints == NumberFormatter.stringDecimalToStringWithSeparator(withString: "\(balanceRewards.rewardBreakDown(.previous)!.nonMonetaryValue)", withLocale: .current))
        XCTAssert(sut.generatedPoints == NumberFormatter.stringDecimalToStringWithSeparator(withString: "\(balanceRewards.rewardBreakDown(.generated)!.nonMonetaryValue)", withLocale: .current))
    }

    func test_givenBalanceRewardsWithAppliedAndExpiredBiggerThanZero_whenICallInit_thenShouldHaveThenonMonetaryValueAndNegativeSymbol() {

        // given
        let balanceRewards = BalanceRewardsGenerator.balanceCompleteBO()

        // when
        let sut = RewardBreakDownDisplayData(rewards: balanceRewards)

        // then
        XCTAssert(sut.appliedPoints == NumberFormatter.stringDecimalToStringWithSeparator(withString: "-\(balanceRewards.rewardBreakDown(.applied)!.nonMonetaryValue)", withLocale: .current))
        XCTAssert(sut.expiredPoints == NumberFormatter.stringDecimalToStringWithSeparator(withString: "-\(balanceRewards.rewardBreakDown(.expired)!.nonMonetaryValue)", withLocale: .current))
    }

    func test_givenBalanceRewardsWithAppliedAndExpiredLessThanZero_whenICallInit_thenShouldHaveThenonMonetaryValueAndNegativeSymbol() {

        // given
        let balanceRewards = BalanceRewardsGenerator.balanceCompleteBO()
        balanceRewards.rewardBreakDown(.applied)!.nonMonetaryValue = -10
        balanceRewards.rewardBreakDown(.expired)!.nonMonetaryValue = -10

        // when
        let sut = RewardBreakDownDisplayData(rewards: balanceRewards)

        // then
        XCTAssert(sut.appliedPoints == NumberFormatter.stringDecimalToStringWithSeparator(withString: "\(balanceRewards.rewardBreakDown(.applied)!.nonMonetaryValue)", withLocale: .current))
        XCTAssert(sut.expiredPoints == NumberFormatter.stringDecimalToStringWithSeparator(withString: "\(balanceRewards.rewardBreakDown(.expired)!.nonMonetaryValue)", withLocale: .current))

    }

    func test_givenBalanceRewardsWithAppliedAndExpiredLEqualsZero_whenICallInit_thenShouldHaveThenonMonetaryValue() {

        // given
        let balanceRewards = BalanceRewardsGenerator.balanceCompleteBO()
        balanceRewards.rewardBreakDown(.applied)!.nonMonetaryValue = 0
        balanceRewards.rewardBreakDown(.expired)!.nonMonetaryValue = 0

        // when
        let sut = RewardBreakDownDisplayData(rewards: balanceRewards)

        // then
        XCTAssert(sut.appliedPoints == NumberFormatter.stringDecimalToStringWithSeparator(withString: "\(balanceRewards.rewardBreakDown(.applied)!.nonMonetaryValue)", withLocale: .current))
        XCTAssert(sut.expiredPoints == NumberFormatter.stringDecimalToStringWithSeparator(withString: "\(balanceRewards.rewardBreakDown(.expired)!.nonMonetaryValue)", withLocale: .current))

    }

    func test_givenBalanceRewardsEmpty_whenICallInit_thenAllFieldsShouldMatch() {

        // given
        let balanceRewards = BalanceRewardsGenerator.balanceEmptyBO()

        // when
        let sut = RewardBreakDownDisplayData(rewards: balanceRewards)

        // then
        XCTAssert(sut.dateFromToTitle == Date.currentYear())
        XCTAssert(sut.currentPoints == sut.defaultValue)
        XCTAssert(sut.previousPoints == sut.defaultValue)
        XCTAssert(sut.generatedPoints == sut.defaultValue)
        XCTAssert(sut.appliedPoints == sut.defaultValue)
        XCTAssert(sut.expiredPoints == sut.defaultValue)
        XCTAssert(sut.showInfoPoints == true)
        XCTAssert(sut.showInfoPointsWithError == false)

    }

    func test_givenBalanceRewardsWithUninformedValues_whenICallInit_thenAllFieldsShouldMatch() {

        // given
        let balanceRewards = BalanceRewardsGenerator.balanceWithUninformedValuesBO()

        // when
        let sut = RewardBreakDownDisplayData(rewards: balanceRewards)

        // then
        XCTAssert(sut.dateFromToTitle == sut.uninformedValue)
        XCTAssert(sut.currentPoints == NumberFormatter.stringDecimalToStringWithSeparator(withString: "\(balanceRewards.rewardBreakDown(.current)!.nonMonetaryValue)", withLocale: .current))
        XCTAssert(sut.previousPoints == sut.uninformedValue)
        XCTAssert(sut.generatedPoints == sut.uninformedValue)

        if let reward = balanceRewards.rewardBreakDown(.applied) {
            let appliedPointsValue = reward.nonMonetaryValue > 0 ? -reward.nonMonetaryValue : reward.nonMonetaryValue
            XCTAssert(sut.appliedPoints == NumberFormatter.stringDecimalToStringWithSeparator(withString: "\(appliedPointsValue)", withLocale: .current))
            
        }

        if let reward = balanceRewards.rewardBreakDown(.expired) {
            let expiredPointsValue = reward.nonMonetaryValue > 0 ? -reward.nonMonetaryValue : reward.nonMonetaryValue
            XCTAssert(sut.expiredPoints == NumberFormatter.stringDecimalToStringWithSeparator(withString: "\(expiredPointsValue)", withLocale: .current))
            
        }

        XCTAssert(sut.showInfoPoints == false)
        XCTAssert(sut.showInfoPointsWithError == true)
    }

    func test_givenBalanceRewardsWithOnlyDate_whenICallInit_thenAllFieldsShouldMatch() {

        // given
        let balanceRewards = BalanceRewardsGenerator.balanceWithOnlyDateBO()

        // when
        let sut = RewardBreakDownDisplayData(rewards: balanceRewards)

        // then
        XCTAssert(sut.dateFromToTitle == Date.currentYear())
        XCTAssert(sut.currentPoints == sut.defaultValue)
        XCTAssert(sut.previousPoints == sut.defaultValue)
        XCTAssert(sut.generatedPoints == sut.defaultValue)
        XCTAssert(sut.appliedPoints == sut.defaultValue)
        XCTAssert(sut.expiredPoints == sut.defaultValue)
        XCTAssert(sut.showInfoPoints == true)
        XCTAssert(sut.showInfoPointsWithError == false)

    }

}
