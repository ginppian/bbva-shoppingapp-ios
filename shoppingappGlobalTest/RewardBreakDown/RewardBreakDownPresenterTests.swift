//
//  RewardBreakDownPresenterTests.swift
//  shoppingapp
//
//  Created by jesus.martinez on 18/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest
import RxSwift

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class RewardBreakDownPresenterTests: XCTestCase {

    var sut: RewardBreakDownPresenter!
    var dummyView = RewardBreakDownDummyView()
    var dummyInteractor = DummyInteractor()
    
    override func setUp() {
        
        super.setUp()
        
        sut = RewardBreakDownPresenter()
        
        sut.view = dummyView
        sut.interactor = dummyInteractor
    }
    
    // MARK: - setModel

    func test_givenModel_whenICallSetModel_thenModelShouldBeFilled() {
        
        // given
        let model = RewardBreakDownModel(cardId: "cardId", balanceRewards: nil, showError: false)

        // when
        sut.setModel(model)

        // then
        XCTAssert(sut.model != nil)

    }

    func test_givenModel_whenICallSetModel_thenModelShouldMatchAsParameter() {
        
        // given
        let model = RewardBreakDownModel(cardId: "cardId", balanceRewards: nil, showError: false)

        // when
        sut.setModel(model)

        // then
        XCTAssert((sut.model as! RewardBreakDownModel) == model)

    }

    func test_givenModelWithoutRewards_whenICallSetModelAndInteractor_thenICallViewShowSkeleton() {

        dummyInteractor.forceError = true

        // given
        let model = RewardBreakDownModel(cardId: "cardId", balanceRewards: nil, showError: false)

        // when
        sut.setModel(model)

        // then
        XCTAssert(dummyView.isCalledShowSkeleton == true)

    }
    
    func test_givenModelWithoutRewardsAndDelegateSet_whenICallSetModelAndInteractor_thenICallDelegateWillRetrieveBalanceForCardId() {
        
        let dummyDelegate = RewardBreakDownPresenterDummyDelegate()
        sut.delegate = dummyDelegate
        
        // given
        let model = RewardBreakDownModel(cardId: "cardId", balanceRewards: nil, showError: false)

        // when
        sut.setModel(model)

        // then
        XCTAssert(dummyDelegate.isCalledWillRetrieveBalanceForCardId == true)
        
    }
    
    func test_givenModelWithoutRewardsAndDelegateSet_whenICallSetModelAndInteractor_thenICallDelegateWillRetrieveBalanceForCardIdWithAppropiateCardId() {
        
        let dummyDelegate = RewardBreakDownPresenterDummyDelegate()
        sut.delegate = dummyDelegate
        
        // given
        let model = RewardBreakDownModel(cardId: "cardId", balanceRewards: nil, showError: false)

        // when
        sut.setModel(model)

        // then
        XCTAssert(dummyDelegate.cardISent == sut.model?.cardId)
        
    }

    func test_givenModelWithoutRewards_whenICallSetModel_thenICallInteractorProvideRewards() {

        // given
        let model = RewardBreakDownModel(cardId: "cardId", balanceRewards: nil, showError: false)

        // when
        sut.setModel(model)

        // then
        XCTAssert(dummyInteractor.isCalledRetrieveBalanceRewards == true)

    }

    func test_givenModelWithoutRewards_whenICallSetModel_thenICallInteractorProvideRewardsWithCardIdAndRewardIdOfModel() {

        // given
        let model = RewardBreakDownModel(cardId: "cardId", balanceRewards: nil, showError: false)

        // when
        sut.setModel(model)

        // then
        XCTAssert(dummyInteractor.cardIdSent == model.cardId)

    }

    func test_givenModelWithoutRewards_whenICallSetModelAndInteractorReturnsAnError_thenICallViewHideSkeleton() {

        dummyInteractor.forceError = true

        // given
        let model = RewardBreakDownModel(cardId: "cardId", balanceRewards: nil, showError: false)

        // when
        sut.setModel(model)

        // then
        XCTAssert(dummyView.isCalledHideSkeleton == true)

    }

    func test_givenModelWithoutRewards_whenICallSetModelAndInteractorReturnsAnError_thenICallViewShowError() {

        dummyInteractor.forceError = true

        // given
        let model = RewardBreakDownModel(cardId: "cardId", balanceRewards: nil, showError: false)

        // when
        sut.setModel(model)

        // then
        XCTAssert(dummyView.isCalledShowError == true)

    }

    func test_givenModelWithoutRewards_whenICallSetModelAndInteractorReturnsAnError_thenICallViewShowErrorWithTheSameTitleAsErrorReturned() {

        dummyInteractor.forceError = true

        // given
        let model = RewardBreakDownModel(cardId: "cardId", balanceRewards: nil, showError: false)

        // when
        sut.setModel(model)

        // then
        XCTAssert(dummyView.errorTitle == Localizables.points.key_points_error_loading_try_again_text)

    }

    func test_givenModelWithoutRewardsAndDelegate_whenICallSetModelAndInteractorReturnsAnError403_thenICallDelegateRetrieveError() {

        dummyInteractor.forceError = true
        let error = ErrorBO()
        error.status = ConstantsHTTPCodes.STATUS_403
        error.code = ErrorCode.expiredSession.rawValue
        dummyInteractor.errorReturned = error

        let dummyDelegate = RewardBreakDownPresenterDummyDelegate()
        sut.delegate = dummyDelegate

        // given
        let model = RewardBreakDownModel(cardId: "cardId", balanceRewards: nil, showError: false)

        // when
        sut.setModel(model)

        // then
        XCTAssert(dummyDelegate.iCalledRewardBreakDownRetrieveError == true)

    }

    func test_givenModelWithoutRewardsAndDelegate_whenICallSetModelAndInteractorReturnsAnError403_thenICallDelegateRetrieveErrorWithErrorReturned() {

        dummyInteractor.forceError = true
        let error = ErrorBO()
        error.status = ConstantsHTTPCodes.STATUS_403
        error.code = ErrorCode.expiredSession.rawValue
        dummyInteractor.errorReturned = error

        let dummyDelegate = RewardBreakDownPresenterDummyDelegate()
        sut.delegate = dummyDelegate

        // given
        let model = RewardBreakDownModel(cardId: "cardId", balanceRewards: nil, showError: false)

        // when
        sut.setModel(model)

        // then
        XCTAssert(dummyDelegate.errorSent === dummyInteractor.errorReturned)

    }

    func test_givenModelWithoutRewards_whenICallSetModelAndInteractorReturnsSuccess_thenICallHideSkeleton() {

        // given
        let model = RewardBreakDownModel(cardId: "cardId", balanceRewards: nil, showError: false)

        // when
        sut.setModel(model)

        // then
        XCTAssert(dummyView.isCalledHideSkeleton == true)

    }

    func test_givenModelWithoutRewards_whenICallSetModelAndInteractorReturnsSuccess_thenICallViewShowRewardBreakDown() {

        // given
        let model = RewardBreakDownModel(cardId: "cardId", balanceRewards: nil, showError: false)

        // when
        sut.setModel(model)

        // then
        XCTAssert(dummyView.isCalledShowRewardBreakDown == true)

    }

    func test_givenModelWithoutRewards_whenICallSetModelAndInteractorReturnsSuccess_thenICallViewShowRewardBreakDownWithDisplayData() {

        // given
        let model = RewardBreakDownModel(cardId: "cardId", balanceRewards: nil, showError: false)

        // when
        sut.setModel(model)

        // then
        XCTAssert(dummyView.displayDataSent != nil)

    }

    func test_givenModelWithoutRewards_whenICallSetModelAndInteractorReturnsSuccess_thenICallShowInfoPointsView() {

        // given
        let model = RewardBreakDownModel(cardId: "cardId", balanceRewards: nil, showError: false)

        // when
        sut.setModel(model)

        // then
        XCTAssert(dummyView.isCalledShowInfoPointsView == true)

    }

    func test_givenModelWithoutRewardBreakDown_whenICallSetModelAndInteractorReturnsSuccess_thenICallShowInfoPointsView() {

        // given
        let model = RewardBreakDownModel(cardId: "cardId", balanceRewards: BalanceRewardsGenerator.balanceEmptyBO(), showError: false)

        // when
        sut.setModel(model)

        // then
        XCTAssert(dummyView.isCalledShowInfoPointsView == true)

    }

    func test_givenModelWithUninformedRewardsValues_whenICallSetModelAndInteractorReturnsSuccess_thenICallShowInfoPointsViewWithError() {

        // given
        let model = RewardBreakDownModel(cardId: "cardId", balanceRewards: BalanceRewardsGenerator.balanceWithUninformedValuesBO(), showError: false)

        // when
        sut.setModel(model)

        // then
        XCTAssert(dummyView.isCalledShowInfoPointsViewWithError == true)

    }

    func test_givenModelWithCompleteRewards_whenICallSetModelAndInteractorReturnsSuccess_thenICallHiddenInfoPointsView() {

        // given
        let model = RewardBreakDownModel(cardId: "cardId", balanceRewards: BalanceRewardsGenerator.balanceCompleteBO(), showError: false)

        // when
        sut.setModel(model)

        // then
        XCTAssert(dummyView.isCalledHiddenInfoPointsView == true)

    }

    func test_givenModelWithoutRewardsAndDelegate_whenICallSetModelAndInteractorReturnsSuccess_thenICallDelegateRetrieveBalance() {

        let dummyDelegate = RewardBreakDownPresenterDummyDelegate()
        sut.delegate = dummyDelegate

        // given
        let model = RewardBreakDownModel(cardId: "cardId", balanceRewards: nil, showError: false)

        // when
        sut.setModel(model)

        // then
        XCTAssert(dummyDelegate.iCalledRewardBreakDownRetrieveBalanceRewards == true)

    }

    func test_givenModelWithoutRewardsAndDelegate_whenICallSetModelAndInteractorReturnsSuccess_thenICallDelegateRetrieveBalanceWithAppropiateValues() {

        let dummyDelegate = RewardBreakDownPresenterDummyDelegate()
        sut.delegate = dummyDelegate

        // given
        let model = RewardBreakDownModel(cardId: "cardId", balanceRewards: nil, showError: false)

        // when
        sut.setModel(model)

        // then
        XCTAssert(dummyDelegate.cardISent == sut.model?.cardId)
        XCTAssert(dummyDelegate.retrieveBalanceRewardsSent == dummyInteractor.retrieveRewards)

    }

    func test_givenModelWithRewards_whenICallSetModel_thenIDoNotCallInteractorProvideRewards() {

        // given
        let model = RewardBreakDownModel(cardId: "cardId", balanceRewards: BalanceRewardsGenerator.balanceCompleteBO(), showError: false)

        // when
        sut.setModel(model)

        // then
        XCTAssert(dummyInteractor.isCalledRetrieveBalanceRewards == false)

    }

    func test_givenModelWithRewards_whenICallSetModel_thenICallViewShowRewardBreakDown() {
        
        // given
        let model = RewardBreakDownModel(cardId: "cardId", balanceRewards: BalanceRewardsGenerator.balanceCompleteBO(), showError: false)

        // when
        sut.setModel(model)

        // then
        XCTAssert(dummyView.isCalledShowRewardBreakDown == true)

    }

    func test_givenModelWithRewards_whenICallSetModel_thenICallViewShowRewardBreakDownWithDisplayData() {
        
        // given
        let model = RewardBreakDownModel(cardId: "cardId", balanceRewards: BalanceRewardsGenerator.balanceCompleteBO(), showError: false)

        // when
        sut.setModel(model)

        // then
        XCTAssert(dummyView.displayDataSent != nil)

    }
    
    func test_givenModelWithShowErrorTrue_whenICallSetModel_thenICallViewShowError() {
        
        // given
        let model = RewardBreakDownModel(cardId: "cardId", balanceRewards: BalanceRewardsGenerator.balanceCompleteBO(), showError: true)
        
        // when
        sut.setModel(model)
        
        // then
        XCTAssert(dummyView.isCalledShowError == true)
        
    }
    
    func test_givenModelWithShowErrorTrue_whenICallSetModel_thenICallViewShowErrorWithTitleErrorCorrect() {
        
        // given
        let model = RewardBreakDownModel(cardId: "cardId", balanceRewards: BalanceRewardsGenerator.balanceCompleteBO(), showError: true)
        
        // when
        sut.setModel(model)
        
        // then
        XCTAssert(dummyView.errorTitle == Localizables.points.key_points_error_loading_try_again_text)
        
    }
    
    func test_givenModelWithShowErrorFalse_whenICallSetModel_thenIDoNotCallViewShowError() {
        
        // given
        let model = RewardBreakDownModel(cardId: "cardId", balanceRewards: BalanceRewardsGenerator.balanceCompleteBO(), showError: false)
        
        // when
        sut.setModel(model)
        
        // then
        XCTAssert(dummyView.isCalledShowError == false)
        
    }
    
    func test_givenModelWithShowErrorTrue_whenICallSetModel_thenIDoNotCallInteractorRetrieveBalanceRewards() {
        
        // given
        let model = RewardBreakDownModel(cardId: "cardId", balanceRewards: BalanceRewardsGenerator.balanceCompleteBO(), showError: true)
        
        // when
        sut.setModel(model)
        
        // then
        XCTAssert(dummyInteractor.isCalledRetrieveBalanceRewards == false)
        
    }

    // MARK: - retryPressed

    func test_givenAny_whenICallRetryPressed_thenICallViewHideError() {

        // given

        // when
        sut.retryPressed()

        // then
        XCTAssert(dummyView.isCalledHideError == true)

    }

    func test_givenModel_whenICallRetryPressedAndInteractor_thenICallViewShowSkeleton() {

        dummyInteractor.forceError = true

        // given
        sut.model = RewardBreakDownModel(cardId: "cardId", balanceRewards: nil, showError: false)

        // when
        sut.retryPressed()

        // then
        XCTAssert(dummyView.isCalledShowSkeleton == true)

    }
    
    func test_givenModelWithoutRewardsAndDelegateSet_whenICallRetryPressed_thenICallDelegateWillRetrieveBalanceForCardId() {
        
        let dummyDelegate = RewardBreakDownPresenterDummyDelegate()
        sut.delegate = dummyDelegate
        
        // given
        sut.model = RewardBreakDownModel(cardId: "cardId", balanceRewards: nil, showError: false)
        
        // when
        sut.retryPressed()
        
        // then
        XCTAssert(dummyDelegate.isCalledWillRetrieveBalanceForCardId == true)
        
    }
    
    func test_givenModelWithoutRewardsAndDelegateSet_whenICallRetryPressed_thenICallDelegateWillRetrieveBalanceForCardIdWithAppropiateCardId() {
        
        let dummyDelegate = RewardBreakDownPresenterDummyDelegate()
        sut.delegate = dummyDelegate
        
        // given
        sut.model = RewardBreakDownModel(cardId: "cardId", balanceRewards: nil, showError: false)
        
        // when
        sut.retryPressed()
        
        // then
        XCTAssert(dummyDelegate.cardISent == sut.model?.cardId)
        
    }

    func test_givenModel_whenICallRetryPressed_thenICallInteractorProvideRewards() {

        // given
        sut.model = RewardBreakDownModel(cardId: "cardId", balanceRewards: nil, showError: false)

        // when
        sut.retryPressed()

        // then
        XCTAssert(dummyInteractor.isCalledRetrieveBalanceRewards == true)

    }

    func test_givenModel_whenICallRetryPressed_thenICallInteractorProvideRewardsWithCardIdAndRewardIdOfModel() {

        // given
        sut.model = RewardBreakDownModel(cardId: "cardId", balanceRewards: nil, showError: false)

        // when
        sut.retryPressed()

        // then
        XCTAssert(dummyInteractor.cardIdSent == sut.model!.cardId)

    }

    func test_givenModel_whenICallRetryPressedAndInteractorReturnsAnError_thenICallViewHideSkeleton() {

        dummyInteractor.forceError = true

        // given
        sut.model = RewardBreakDownModel(cardId: "cardId", balanceRewards: nil, showError: false)

        // when
        sut.retryPressed()

        // then
        XCTAssert(dummyView.isCalledHideSkeleton == true)

    }

    func test_givenModel_whenICallRetryPressedAndInteractorReturnsAnError_thenICallViewShowError() {

        dummyInteractor.forceError = true

        // given
        sut.model = RewardBreakDownModel(cardId: "cardId", balanceRewards: nil, showError: false)

        // when
        sut.retryPressed()

        // then
        XCTAssert(dummyView.isCalledShowError == true)

    }

    func test_givenModel_whenICallRetryPressedAndInteractorReturnsAnError_thenICallViewShowErrorWithTheSameTitleAsErrorReturned() {

        dummyInteractor.forceError = true

        // given
        sut.model = RewardBreakDownModel(cardId: "cardId", balanceRewards: nil, showError: false)

        // when
        sut.retryPressed()

        // then
        XCTAssert(dummyView.errorTitle == Localizables.points.key_points_error_loading_try_again_text)

    }

    func test_givenModelAndDelegate_whenICallRetryPressedAndInteractorReturnsAnError403_thenICallDelegateRetrieveError() {

        dummyInteractor.forceError = true
        let error = ErrorBO()
        error.status = ConstantsHTTPCodes.STATUS_403
        error.code = ErrorCode.expiredSession.rawValue
        dummyInteractor.errorReturned = error

        let dummyDelegate = RewardBreakDownPresenterDummyDelegate()
        sut.delegate = dummyDelegate

        // given
        sut.model = RewardBreakDownModel(cardId: "cardId", balanceRewards: nil, showError: false)

        // when
        sut.retryPressed()

        // then
        XCTAssert(dummyDelegate.iCalledRewardBreakDownRetrieveError == true)

    }

    func test_givenModelAndDelegate_whenICallRetryPressedAndInteractorReturnsAnError403_thenICallDelegateRetrieveErrorWithErrorReturned() {
        
        dummyInteractor.forceError = true
        let error = ErrorBO()
        error.status = ConstantsHTTPCodes.STATUS_403
        error.code = ErrorCode.expiredSession.rawValue
        dummyInteractor.errorReturned = error

        let dummyDelegate = RewardBreakDownPresenterDummyDelegate()
        sut.delegate = dummyDelegate

        // given
        sut.model = RewardBreakDownModel(cardId: "cardId", balanceRewards: nil, showError: false)

        // when
        sut.retryPressed()

        // then
        XCTAssert(dummyDelegate.errorSent === dummyInteractor.errorReturned)

    }

    func test_givenModel_whenICallRetryPressedAndInteractorReturnsSuccess_thenICallHideSkeleton() {

        // given
        sut.model = RewardBreakDownModel(cardId: "cardId", balanceRewards: nil, showError: false)

        // when
        sut.retryPressed()

        // then
        XCTAssert(dummyView.isCalledHideSkeleton == true)

    }

    func test_givenModel_whenICallRetryPressedAndInteractorReturnsSuccess_thenICallViewShowRewardBreakDown() {

        // given
        sut.model = RewardBreakDownModel(cardId: "cardId", balanceRewards: nil, showError: false)

        // when
        sut.retryPressed()

        // then
        XCTAssert(dummyView.isCalledShowRewardBreakDown == true)

    }

    func test_givenModel_whenICallRetryPressedAndInteractorReturnsSuccess_thenICallViewShowRewardBreakDownWithDisplayData() {

        // given
        sut.model = RewardBreakDownModel(cardId: "cardId", balanceRewards: nil, showError: false)

        // when
        sut.retryPressed()

        // then
        XCTAssert(dummyView.displayDataSent != nil)

    }

    func test_givenModelWithoutRewards_whenICallRetryPressedAndInteractorReturnsSuccess_thenICallShowInfoPointsView() {

        // given
        sut.model = RewardBreakDownModel(cardId: "cardId", balanceRewards: nil, showError: false)

        // when
        sut.retryPressed()

        // then
        XCTAssert(dummyView.isCalledShowInfoPointsView == true)

    }

    func test_givenModelWithoutRewardBreakDown_whenICallRetryPressedAndInteractorReturnsSuccess_thenICallShowInfoPointsView() {

        // given
        sut.model = RewardBreakDownModel(cardId: "cardId", balanceRewards: BalanceRewardsGenerator.balanceEmptyBO(), showError: false)

        // when
        sut.retryPressed()

        // then
        XCTAssert(dummyView.isCalledShowInfoPointsView == true)

    }

    func test_givenModelWithUninformedRewardsValues_whenICallRetryPressedAndInteractorReturnsSuccess_thenICallShowInfoPointsViewWithError() {

        dummyInteractor.forceBalanceWithUninformedValuesBO = true

        // given
        sut.model = RewardBreakDownModel(cardId: "cardId", balanceRewards: BalanceRewardsGenerator.balanceWithUninformedValuesBO(), showError: false)

        // when
        sut.retryPressed()

        // then
        XCTAssert(dummyView.isCalledShowInfoPointsViewWithError == true)

    }

    func test_givenModelWithCompleteRewards_whenICallRetryPressedAndInteractorReturnsSuccess_thenICallHiddenInfoPointsView() {
        
        dummyInteractor.forceBalanceCompleteBO = true

        // given
        sut.model = RewardBreakDownModel(cardId: "cardId", balanceRewards: BalanceRewardsGenerator.balanceCompleteBO(), showError: false)

        // when
        sut.retryPressed()

        // then
        XCTAssert(dummyView.isCalledHiddenInfoPointsView == true)

    }

    func test_givenModelAndDelegate_whenICallRetryPressedAndInteractorReturnsSuccess_thenICallDelegateRetrieveBalance() {

        let dummyDelegate = RewardBreakDownPresenterDummyDelegate()
        sut.delegate = dummyDelegate

        // given
        sut.model = RewardBreakDownModel(cardId: "cardId", balanceRewards: nil, showError: false)

        // when
        sut.retryPressed()

        // then
        XCTAssert(dummyDelegate.iCalledRewardBreakDownRetrieveBalanceRewards == true)

    }

    func test_givenModelAndDelegate_whenICallRetryPressedAndInteractorReturnsSuccess_thenICallDelegateRetrieveBalanceWithAppropiateValues() {

        let dummyDelegate = RewardBreakDownPresenterDummyDelegate()
        sut.delegate = dummyDelegate

        // given
        sut.model = RewardBreakDownModel(cardId: "cardId", balanceRewards: nil, showError: false)

        // when
        sut.retryPressed()

        // then
        XCTAssert(dummyDelegate.cardISent == sut.model?.cardId)
        XCTAssert(dummyDelegate.retrieveBalanceRewardsSent == dummyInteractor.retrieveRewards)

    }

    // MARK: - Dummydata

    class RewardBreakDownPresenterDummyDelegate: RewardBreakDownPresenterDelegate {

        var isCalledWillRetrieveBalanceForCardId = false
        var iCalledRewardBreakDownRetrieveError = false
        var iCalledRewardBreakDownRetrieveBalanceRewards = false

        var cardISent: String?
        var errorSent: ErrorBO?
        var retrieveBalanceRewardsSent: BalanceRewardsBO?
        
        func rewardBreakDownPresenter(_ rewardBreakDownPresenter: RewardBreakDownPresenter, willRetrieveBalanceForCardId cardId: String) {
            
            isCalledWillRetrieveBalanceForCardId = true
            cardISent = cardId
        }

        func rewardBreakDownPresenter(_ rewardBreakDownPresenter: RewardBreakDownPresenter, cardId: String, retrieveError: ErrorBO) {
            
            iCalledRewardBreakDownRetrieveError = true
            cardISent = cardId
            errorSent = retrieveError
        }

        func rewardBreakDownPresenter(_ rewardBreakDownPresenter: RewardBreakDownPresenter, cardId: String, retrieveBalanceRewards: BalanceRewardsBO) {
            
            iCalledRewardBreakDownRetrieveBalanceRewards = true
            retrieveBalanceRewardsSent = retrieveBalanceRewards
            cardISent = cardId
        }
    }

    class RewardBreakDownDummyView: RewardBreakDownViewProtocol {

        var isCalledShowSkeleton = false
        var isCalledHideSkeleton = false
        var isCalledShowError = false
        var isCalledShowRewardBreakDown = false
        var isCalledHideError = false

        var errorTitle = ""
        var displayDataSent: RewardBreakDownDisplayDataProtocol?

        var isCalledHiddenInfoPointsView = false
        var isCalledShowInfoPointsView = false
        var isCalledShowInfoPointsViewWithError = false

        func showSkeleton() {
            isCalledShowSkeleton = true
        }

        func hideSkeleton() {
            isCalledHideSkeleton = true
        }

        func showError(text: String) {
            isCalledShowError = true
            errorTitle = text
        }

        func hideError() {
            isCalledHideError = true
        }

        func showRewardBreakDown(withDisplayData displayData: RewardBreakDownDisplayDataProtocol) {
            isCalledShowRewardBreakDown = true
            displayDataSent = displayData
        }

        func hiddenInfoPointsView() {
            isCalledHiddenInfoPointsView = true
        }

        func showInfoPointsView() {
            isCalledShowInfoPointsView = true
        }

        func showInfoPointsViewWithError() {
            isCalledShowInfoPointsViewWithError = true
        }
    }

    class DummyInteractor: RewardBreakDownInteractorProtocol {

        var isCalledRetrieveBalanceRewards = false
        var forceError = false
        var forceBalanceWithUninformedValuesBO = false
        var forceBalanceCompleteBO = false

        var cardIdSent: String?
        var errorReturned: ErrorBO?
        var retrieveRewards: BalanceRewardsBO?

        func retrieveBalanceRewards(cardId: String) -> Observable <ModelBO> {

            cardIdSent = cardId

            isCalledRetrieveBalanceRewards = true

            if forceError {

                if errorReturned == nil {
                    let entity = ErrorEntity(message: "Error")
                    entity.code = "80"
                    entity.httpStatus = 404
                    errorReturned = ErrorBO(error: entity)
                }

                return Observable.error(ServiceError.GenericErrorBO(error: errorReturned!))
            } else {

                let dummyEntity = BalanceRewardsEntity()
                dummyEntity.status = 200

                if forceBalanceWithUninformedValuesBO {
                    dummyEntity.data = BalanceRewardsGenerator.balanceWithUninformedValuesEntity().data
                }

                if forceBalanceCompleteBO {
                    dummyEntity.data = BalanceRewardsGenerator.balanceCompleteEntity().data
                }

                let bo = BalanceRewardsBO(entity: dummyEntity)
                retrieveRewards = bo

                return Observable <ModelBO>.just(bo as ModelBO)
            }
        }
    }

}
