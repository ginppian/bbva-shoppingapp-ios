//
//  SplashPresenterTests.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 24/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest
import CellsNativeComponents
import RxSwift

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class SplashPresenterTests: XCTestCase {

    var sut: SplashPresenter<DummyView>!
    var dummyBusManager: DummyBusManager!
    var dummyPublicConfigurationManager: DummyPublicConfigurationManager!
    var dummyView = DummyView()
    var dummyVersionControlManager: DummyVersionControlManager!
    var dummyPreferencesManager: DummyPreferencesManager!

    override func setUp() {

        super.setUp()
        dummyBusManager = DummyBusManager()
        DummyPublicConfigurationManager.setupMock()
        dummyPublicConfigurationManager = (PublicConfigurationManager.sharedInstance() as! DummyPublicConfigurationManager)
        sut = SplashPresenter<DummyView>(busManager: dummyBusManager)
        sut.view = dummyView

        DummyVersionControlManager.setupMock()
        dummyVersionControlManager = (VersionControlManager.sharedInstance() as! DummyVersionControlManager)
        
        DummyPreferencesManager.configureDummyPreferences()
        dummyPreferencesManager = PreferencesManager.instance as? DummyPreferencesManager
    }

    // MARK: - viewDidLoad
    
    func test_givenAny_whenICallViewDidLoad_thenICallPublicConfigurationGetPublicConfiguration() {
        
        //given
        
        //when
        sut.viewDidLoad()
        
        //then
        XCTAssert(dummyPublicConfigurationManager.isCalledGetPublicConfiguration == true)
    }

    func test_givenAKeyWelcomeInPreferencesAndAnimationFinishedIsTrueAndWebIsReadyTrue_whenICallViewDidLoadAndDownloadConfigurationIsOk_thenICallGetPreferencesWithWelcomeKey() {

        //given
        sut.splashAnimationFinished = true
        sut.webIsReady = true

        //when
        sut.viewDidLoad()
        dummyPublicConfigurationManager.downloadPublicConfigurationPublisher.onNext(PublicConfigurationDTO())

        //then
        XCTAssert(dummyPreferencesManager.key == PreferencesManagerKeys.kWelcomeShowed.rawValue)
    }

    func test_givenAKeyWelcomeInPreferencesAndAnimationFinishedIsTrueAndWebIsReadyTrue_whenICallViewDidLoadAndDownloadConfigurationIsOk_thenICallBusManagerNativateLoginScreen() {

        //given
        dummyPreferencesManager.key = PreferencesManagerKeys.kWelcomeShowed.rawValue
        dummyPreferencesManager.value = true as AnyObject
        
        sut.splashAnimationFinished = true
        sut.webIsReady = true

        //when
        sut.viewDidLoad()
        dummyPublicConfigurationManager.downloadPublicConfigurationPublisher.onNext(PublicConfigurationDTO())

        //then
        XCTAssert(dummyBusManager.isCalledNavigateToLoginPage == true)
    }

    func test_givenNotAKeyWelcomeInPreferencesAndAnimationFinishedIsTrueAndWebIsReadyTrue_whenICallViewDidLoadAndDownloadConfigurationIsOk_thenICallBusManagerNativateWelcomeScreen() {

        //given
        sut.splashAnimationFinished = true
        sut.webIsReady = true

        //when
        sut.viewDidLoad()
        dummyPublicConfigurationManager.downloadPublicConfigurationPublisher.onNext(PublicConfigurationDTO())

        //then
        XCTAssert(dummyBusManager.isCalledNavigateToWelcomePage == true)
    }
    
    func test_givenAnimationFinishedIsTrueAndWebIsReadyTrue_whenICallViewDidLoadAndDownloadConfigurationFinish_thenICalledCheckVersion() {
        
        //given
        sut.splashAnimationFinished = true
        sut.webIsReady = true
        
        //when
        sut.viewDidLoad()
        dummyPublicConfigurationManager.downloadPublicConfigurationPublisher.onNext(PublicConfigurationDTO())
        
        //then
        XCTAssert(dummyVersionControlManager.isCalledCheckVersion == true)
    }
    
    func test_givenAnimationFinishedIsTrueAndWebIsReadyTrue_whenICallViewDidLoadAndDownloadConfigurationFinish_thenICalledCheckVersionAndPublicConfigurationDTOMatch() {
        
        //given
        sut.splashAnimationFinished = true
        sut.webIsReady = true
        
        //when
        let publicConfigurationDTO = dummyPublicConfigurationManager.localPublicConfiguration()
        sut.viewDidLoad()
        dummyPublicConfigurationManager.downloadPublicConfigurationPublisher.onNext(publicConfigurationDTO)
        
        //then
        XCTAssert(dummyVersionControlManager.publicConfigurationDTOSended != nil)
        XCTAssert(dummyVersionControlManager.publicConfigurationDTOSended?.publicConfig == publicConfigurationDTO.publicConfig)
        XCTAssert(dummyVersionControlManager.shouldShowNoMandatoryUpdateSent == true)
    }
    
    // MARK: - viewDidAppear
    
    func test_givenAny_whenICallViewDidAppear_thenICallViewStartAnimation() {
        
        //given
        
        //when
        sut.viewDidAppear()
        
        //then
        XCTAssert(dummyView.isCalledStartAnimation == true)
    }
    
    // MARK: - animationFinished
    
    func test_givenAKeyWelcomeInPreferencesAndPublicConfigurationAndWebIsReadyTrue_whenICallAnimationFinished_thenICallGetPreferencesWithWelcomeKey() {
        
        //given
        sut.publicConfiguration = PublicConfigurationDTO()
        sut.webIsReady = true
        
        //when
        sut.animationFinished()
        
        //then
        XCTAssert(dummyPreferencesManager.key == PreferencesManagerKeys.kWelcomeShowed.rawValue)
    }
    
    func test_givenAKeyWelcomeInPreferencesAndPublicConfigurationAndWebIsReadyTrue_whenICallAnimationFinished_thenICallBusManagerNativateLoginScreen() {
        
        //given
        dummyPreferencesManager.key = PreferencesManagerKeys.kWelcomeShowed.rawValue
        dummyPreferencesManager.value = true as AnyObject
        
        sut.publicConfiguration = PublicConfigurationDTO()
        sut.webIsReady = true
        
        //when
        sut.animationFinished()
        
        //then
        XCTAssert(dummyBusManager.isCalledNavigateToLoginPage == true)
    }
    
    func test_givenNotAKeyWelcomeInPreferencesAndPublicConfigurationAndWebIsReadyTrue_whenICallAnimationFinished_thenICallBusManagerNativateWelcomeScreen() {
        
        //given
        sut.publicConfiguration = PublicConfigurationDTO()
        sut.webIsReady = true
        
        //when
        sut.animationFinished()
        
        //then
        XCTAssert(dummyBusManager.isCalledNavigateToWelcomePage == true)
    }
    
    // MARK: - notifyWebIsReady
    
    func test_givenAKeyWelcomeInPreferencesAndPublicConfigurationAndAnimationFinishedIsTrue_whenICallNotifyWebIsReady_thenICallGetPreferencesWithWelcomeKey() {
        
        //given
        sut.publicConfiguration = PublicConfigurationDTO()
        sut.splashAnimationFinished = true
        
        //when
        sut.notifyWebIsReady()
        
        //then
        XCTAssert(dummyPreferencesManager.key == PreferencesManagerKeys.kWelcomeShowed.rawValue)
    }
    
    func test_givenAKeyWelcomeInPreferencesAndPublicConfigurationAndAnimationFinishedIsTrue_whenICallNotifyWebIsReady_thenICallBusManagerNativateLoginScreen() {
        
        //given
        dummyPreferencesManager.key = PreferencesManagerKeys.kWelcomeShowed.rawValue
        dummyPreferencesManager.value = true as AnyObject
        
        sut.publicConfiguration = PublicConfigurationDTO()
        sut.splashAnimationFinished = true
        
        //when
        sut.notifyWebIsReady()
        
        //then
        XCTAssert(dummyBusManager.isCalledNavigateToLoginPage == true)
    }
    
    func test_givenNotAKeyWelcomeInPreferencesAndPublicConfigurationAndAnimationFinishedIsTrue_whenICallNotifyWebIsReady_thenICallBusManagerNativateWelcomeScreen() {
        
        //given
        sut.publicConfiguration = PublicConfigurationDTO()
        sut.splashAnimationFinished = true
        
        //when
        sut.notifyWebIsReady()
        
        //then
        XCTAssert(dummyBusManager.isCalledNavigateToWelcomePage == true)
    }

    // MARK: - Dummies

    class DummyView: SplashViewProtocol {
        
        var isCalledStartAnimation = false
        
        func startAnimation() {
            
            isCalledStartAnimation = true
        }

        func showError(error: ModelBO) {
        }

        func changeColorEditTexts() {
        }

    }

    class DummyBusManager: BusManager {

        var isCalledNavigateToLoginPage = false

        var isCalledNavigateToWelcomePage = false

        override init() {
            
            super.init()
            routerFactory = RouterInitFactory()
        }

        override func navigateScreen<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {

            if tag == SplashPageReaction.ROUTER_TAG && event === SplashPageReaction.EVENT_NAV_TO_LOGIN_SCREEN {
                isCalledNavigateToLoginPage = true
            } else if tag == SplashPageReaction.ROUTER_TAG && event === SplashPageReaction.EVENT_NAV_TO_WELCOME {
                isCalledNavigateToWelcomePage = true
            }
        }
    }
}
