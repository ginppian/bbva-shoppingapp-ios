//
//  CardCancelationPresenterTests.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 17/7/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest
import CellsNativeComponents

#if TESTMX
@testable import shoppingappMX
#elseif TESTPE
@testable import shoppingappPE
#endif

class CardCancelationPresenterTests: XCTestCase {

    var sut: CardCancelationPresenter<CardCancelationPresenterViewProtocolDummy>!
    var dummyBusManager: DummyBusManager!
    var dummyView: CardCancelationPresenterViewProtocolDummy!

    override func setUp() {
        super.setUp()
        dummyBusManager = DummyBusManager()
        sut = CardCancelationPresenter<CardCancelationPresenterViewProtocolDummy>(busManager: dummyBusManager)

        dummyView = CardCancelationPresenterViewProtocolDummy()
        sut.view = dummyView
    }

    // MARK: - setModel

    func test_givenErrorBO_whenICallSetModel_thenErrorBOBeFilled() {

        // given
        let errorBO = ErrorBO(error: ErrorEntity(message: "Error"))

        // when
        sut.setModel(model: errorBO)

        // then
        XCTAssert(sut.errorBO === errorBO)

    }

    func test_givenCancelCardObjectCells_whenICallSetModel_thenCancelCardObjectCellsBeFilled() {

        // given
        let cancelCardObjectCells = RequestCancelCardObjectCells(id: "id")

        // when
        sut.setModel(model: cancelCardObjectCells)

        // then
        XCTAssert(sut.cancelCardObjectCells?.id == cancelCardObjectCells.id)

    }

    // MARK: - showPageSuccess

    func test_givenCancelCardObjectCellsAndBlockCardDTO_whenICallShowPageSuccess_thenICalledNavigateToCardCancelationSuccessPage() {

        //given
        let cancelCardObjectCells = RequestCancelCardObjectCells(id: "id")
        sut.cancelCardObjectCells = cancelCardObjectCells

        let blockCardDTO = BlockCardDTO()
        blockCardDTO.data = BlockCardData()
        blockCardDTO.data?.blockId = "id"
        blockCardDTO.data?.reference = "1234"
        let blockCardBO = BlockCardBO(blockCardDTO: blockCardDTO)

        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        //when
        sut.showPageSuccess(modelBO: blockCardBO)

        //then
        XCTAssert(dummyBusManager.isCalledNavigateToCardCancelationSuccessPage == true)

    }

    func test_givenCancelCardObjectCellsAndBlockCardDTO_whenICallShowPageSuccess_thenICalledPublishParamToCardCancelationSuccessPage() {

        //given
        let cancelCardObjectCells = RequestCancelCardObjectCells(id: "id")
        sut.cancelCardObjectCells = cancelCardObjectCells

        let blockCardDTO = BlockCardDTO()
        blockCardDTO.data = BlockCardData()
        blockCardDTO.data?.blockId = "id"
        blockCardDTO.data?.reference = "1234"
        let blockCardBO = BlockCardBO(blockCardDTO: blockCardDTO)

        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        //when
        sut.showPageSuccess(modelBO: blockCardBO)

        //then
        XCTAssert(dummyBusManager.isCalledPublishParamCardCancelationSuccess == true)

    }

    func test_givenCancelCardObjectCellsAndBlockCardDTO_whenICallShowPageSuccess_thenPublishParamMatch() {

        //given
        let cancelCardObjectCells = RequestCancelCardObjectCells(id: "id")
        sut.cancelCardObjectCells = cancelCardObjectCells

        let blockCardDTO = BlockCardDTO()
        blockCardDTO.data = BlockCardData()
        blockCardDTO.data?.blockId = "id"
        blockCardDTO.data?.reference = "1234"
        blockCardDTO.data?.blockDate = "2018-07-18T06:52:04.695Z"
        let blockCardBO = BlockCardBO(blockCardDTO: blockCardDTO)

        let date = Date.getDateFormatForCellsModule(withDate: blockCardDTO.data?.blockDate ?? "")

        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        //when
        sut.showPageSuccess(modelBO: blockCardBO)

        //then
        XCTAssert(dummyBusManager.blockCardSuccessSent.cardNumber == String(sut.cardBO!.number.suffix(4)))
        XCTAssert(dummyBusManager.blockCardSuccessSent.folio == blockCardDTO.data?.reference)
        XCTAssert(dummyBusManager.blockCardSuccessSent.date == date)

    }

    // MARK: - registerOmniture

    func test_givenPage_whenICallRegisterOmniture_thenICallViewSendTrackCellsPageCancelationInfo() {

        // given
        let page = CardCancelationPageReaction.CELLS_PAGE_CARD_CANCEL_INFO

        // when
        sut.registerOmniture(page: page)

        // then
        XCTAssert(dummyView.isCalledSendTrackCellsPageCardCancelationInfo == true)

    }

    func test_givenPage_whenICallRegisterOmniture_thenICallViewSendTrackCellsCardCancelationSuccess() {

        // given
        let page = CardCancelationPageReaction.CELLS_PAGE_CANCEL_CARD_SUCCESS

        // when
        sut.registerOmniture(page: page)

        // then
        XCTAssert(dummyView.isCalledSendTrackCellsPageCardCancelationSuccess == true)

    }

    func test_givenBlockCardObjectCellsAndBlockCardDTO_whenICallShowPageSuccess_thenNotCallPublishNeededRefreshCards() {

        //given
        let cancelCardObjectCells = RequestCancelCardObjectCells(id: "id")
        sut.cancelCardObjectCells = cancelCardObjectCells

        let blockCardDTO = BlockCardDTO()
        blockCardDTO.data = BlockCardData()
        blockCardDTO.data?.blockId = "id"
        blockCardDTO.data?.reference = "1234"
        blockCardDTO.data?.blockDate = "2018-07-18T06:52:04.695Z"
        let blockCardBO = BlockCardBO(blockCardDTO: blockCardDTO)

        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        //when
        sut.showPageSuccess(modelBO: blockCardBO)

        //then
        XCTAssert(dummyBusManager.isCalledPublishNeededCardsRefresh == false)

    }

    func test_givenBlockCardObjectCellsAndBlockCardDTO_whenICallShowPageSuccess_thenICalledPublishParamToBlockCardSuccess() {

        //given
        let cancelCardObjectCells = RequestCancelCardObjectCells(id: "id")
        sut.cancelCardObjectCells = cancelCardObjectCells

        let blockCardDTO = BlockCardDTO()
        blockCardDTO.data = BlockCardData()
        blockCardDTO.data?.blockId = "id"
        blockCardDTO.data?.reference = "1234"
        blockCardDTO.data?.blockDate = "2018-07-18T06:52:04.695Z"
        let blockCardBO = BlockCardBO(blockCardDTO: blockCardDTO)

        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        let cardOperationInfo = CardOperationInfo(cardID: "111", fisicalCardID: nil, cardType: CardDataType.digital, cardOperation: .cancel)
        sut.cardOperationInfo = cardOperationInfo

        //when
        sut.showPageSuccess(modelBO: blockCardBO)

        //then
        XCTAssert(dummyBusManager.isCalledPublishParamBlockCardInfoSuccess == true)

    }

    func test_givendBlockCardDTOAndCardOperationInfo_whenICallShowPageSuccess_thenCallPublishOperationCardInfoSuccessWithRightData() {

        //given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        let blockCardDTO = BlockCardDTO()
        blockCardDTO.data = BlockCardData()
        blockCardDTO.data?.blockId = "id"
        blockCardDTO.data?.reference = "1234"
        blockCardDTO.data?.blockDate = "2018-07-18T06:52:04.695Z"
        let blockCardBO = BlockCardBO(blockCardDTO: blockCardDTO)

        let cardOperationInfo = CardOperationInfo(cardID: "111", fisicalCardID: nil, cardType: CardDataType.digital, cardOperation: .cancel)
        sut.cardOperationInfo = cardOperationInfo

        // when
        sut.showPageSuccess(modelBO: blockCardBO)

        //then
        XCTAssert(dummyBusManager.cardOperationInfo?.cardID == cardOperationInfo.cardID)
        XCTAssert(dummyBusManager.cardOperationInfo?.fisicalCardID == cardOperationInfo.fisicalCardID)
        XCTAssert(dummyBusManager.cardOperationInfo?.cardType == cardOperationInfo.cardType)
        XCTAssert(dummyBusManager.cardOperationInfo?.cardOperation == cardOperationInfo.cardOperation)

    }

    // MARK: - viewWasDissmised

    func test_givenSuccessOk_whenICallViewWasDismiss_thenCallPublishNeededRefreshCards() {

        //given
        let cancelCardObjectCells = RequestCancelCardObjectCells(id: "id")
        sut.cancelCardObjectCells = cancelCardObjectCells

        let blockCardDTO = BlockCardDTO()
        blockCardDTO.data = BlockCardData()
        blockCardDTO.data?.blockId = "id"
        blockCardDTO.data?.reference = "1234"
        blockCardDTO.data?.blockDate = "2018-07-18T06:52:04.695Z"
        let blockCardBO = BlockCardBO(blockCardDTO: blockCardDTO)

        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        sut.cardBO = cardBO

        sut.showPageSuccess(modelBO: blockCardBO)
        
        //when
        sut.viewWasDismissed()

        //then
        XCTAssert(dummyBusManager.isCalledPublishNeededCardsRefresh == true)

    }

    class CardCancelationPresenterViewProtocolDummy: CardCancelationViewProtocol {

        var isCalledSendTrackCellsPageCardCancelationInfo = false
        var isCalledSendTrackCellsPageCardCancelationSuccess = false

        func showError(error: ModelBO) {
        }

        func changeColorEditTexts() {
        }

        func sendTrackCellsPageCardCancelationInfo() {
            isCalledSendTrackCellsPageCardCancelationInfo = true
        }

        func sendTrackCellsPageCardCancelationSuccess() {
            isCalledSendTrackCellsPageCardCancelationSuccess = true
        }
    }

    class DummyBusManager: BusManager {

        var isCalledNavigateToCardCancelationSuccessPage = false
        var isCalledPublishParamCardCancelationSuccess = false
        var isCalledPublishNeededCardsRefresh = false
        var isCalledPublishParamBlockCardInfoSuccess = false

        var blockCardSuccessSent: BlockCardSuccess!
        var cardOperationInfo: CardOperationInfo?

        override init() {
            super.init()
            routerFactory = RouterInitFactory()
        }

        override func navigateScreen<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {

            if tag == CardCancelationPageReaction.ROUTER_TAG && event === CardCancelationPageReaction.EVENT_CARD_CANCEL_SUCCESS {
                isCalledNavigateToCardCancelationSuccessPage = true
            }
        }

        override func publishData<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {

            if tag == CardCancelationPageReaction.ROUTER_TAG && event === CardCancelationPageReaction.PARAMS_CANCEL_CARD_SUCCESS {
                isCalledPublishParamCardCancelationSuccess = true
                blockCardSuccessSent = BlockCardSuccess.deserialize(from: values as? [String: Any])
            } else if tag == CardCancelationPageReaction.ROUTER_TAG && event === CardsPageReaction.PARAMS_OPERATION_CARD_INFO_SUCCESS {
                isCalledPublishParamBlockCardInfoSuccess = true
                cardOperationInfo = values as? CardOperationInfo
            }
        }
        
        override func notify<T>(tag: String, _ event: ActionSpec<T>) {
            if tag == CardsPageReaction.ROUTER_TAG && event === CardsPageReaction.NEEDED_CARDS_REFRESH {
                isCalledPublishNeededCardsRefresh = true
            }
        }
    }

}
