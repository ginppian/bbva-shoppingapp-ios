//
//  SecurityDisplayDataTest.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 28/12/2017.
//  Copyright © 2017 BBVA. All rights reserved.
//

import XCTest
#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class SecurityDisplayDataTest: XCTestCase {

    func test_givenAny_whenSecurityDisplayDataGenerated_thenIShowCorrectImagesAndValues() {

        // when
        let securityDisplayData = SecurityDisplayData.generateSecurityDisplayData()
        let fullText = Localizables.common.key_introduce_key_from_sms
        let attributedString = NSMutableAttributedString(string: fullText, attributes: [
            NSAttributedStringKey.font: Tools.setFontBook(size: 14),
            NSAttributedStringKey.foregroundColor: UIColor.BBVA600
            ])
        let paragraphStyle = NSMutableParagraphStyle()
            paragraphStyle.lineSpacing = 4
            paragraphStyle.alignment = .left

            attributedString.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: attributedString.length))
            attributedString.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBold(size: 14), range: NSRange(location: fullText.count - 3, length: 3))

        // then
        XCTAssert(securityDisplayData.informationIcon == ConstantsImages.Common.info_icon)
        XCTAssert(securityDisplayData.introducePinTitle == attributedString)
        XCTAssert(securityDisplayData.requestPinDescription == Localizables.common.key_request_new_sms_description)
        XCTAssert(securityDisplayData.requestPinTitle == Localizables.common.key_request_new_sms)
        XCTAssert(securityDisplayData.timerIcon == ConstantsImages.Common.clock_icon)
        XCTAssert(securityDisplayData.placeHolderText == Localizables.common.key_placeholder_text)

    }

}
