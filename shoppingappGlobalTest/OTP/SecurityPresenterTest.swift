//
//  SecurityPresenterTest.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 28/12/2017.
//  Copyright © 2017 BBVA. All rights reserved.
//

import XCTest
import BBVA_Network
#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class SecurityPresenterTest: XCTestCase {

    var sut: SecurityPresenter<DummyView>!
    var dummyBusManager: DummyBusManager!
    var dummySecurityInterceptorStore: DummySecurityInterceptorStore!

    override func setUp() {
        super.setUp()
        dummyBusManager = DummyBusManager()
        dummySecurityInterceptorStore = DummySecurityInterceptorStore()
        dummyBusManager.securityInterceptorStore = dummySecurityInterceptorStore
        sut = SecurityPresenter<DummyView>(busManager: dummyBusManager)
    }

    // MARK: - getSecurityData

    func test_givenASecurityDisplayData_whenICallGetSecurityData_thenICallShowCorrectSecurityDataAndDisplaySecurityIsTheSame() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.getSecurityData()

        // then
        XCTAssert(dummyView.securityData.informationIcon == sut.securityData?.informationIcon)
        XCTAssert(dummyView.securityData.introducePinTitle == sut.securityData?.introducePinTitle)
        XCTAssert(dummyView.securityData.timerIcon == sut.securityData?.timerIcon)
        XCTAssert(dummyView.securityData.requestPinDescription == sut.securityData?.requestPinDescription)
        XCTAssert(dummyView.securityData.requestPinTitle == sut.securityData?.requestPinTitle)
        XCTAssert(dummyView.isCalledGetSecurityInformation == true)

    }

    func test_givenASecurityDisplayData_whenICallGetSecurityData_thenICallViewStartTimer() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.getSecurityData()

        // then
        XCTAssert(dummyView.isCalledStartTimer == true)

    }

    func test_givenASecurityDisplayData_whenICallGetSecurityData_thenICallViewUpdateTimer() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.getSecurityData()

        // then
        XCTAssert(dummyView.isCalledUpdateTime == true)

    }

    func test_givenASecurityDisplayData_whenICallGetSecurityData_thenICallViewWithTextFormatWithNumberOfSecondsAsTimeForTimerInSeconds() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.getSecurityData()

        // then
        XCTAssert(dummyView.updateText == sut.calculateMinSecond(forSeconds: sut.timeForTimerInSeconds))

    }

    func test_givenASecurityDisplayData_whenICallGetSecurityData_thenRemainingTimeShouldMatchWithTimeForTimerInSeconds() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.getSecurityData()

        // then
        XCTAssert(sut.remainingTime == sut.timeForTimerInSeconds)

    }

    // MARK: - clearText

    func test_givenAnyValueForSecurityCode_whenICallClearText_thenICallClearSecurityCodeInput() {

        //given
        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.clearText()

        //then
        XCTAssert(dummyView.isCalledClearText == true)

    }

    func test_givenAnyValueForSecurityCode_whenICallClearText_thenTextSentToViewShouldMatchAndConfirmButtonDisabled() {

        //given
        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.clearText()

        //then
        XCTAssert(dummyView.clearText.isEmpty)
        XCTAssert(dummyView.isCalledDisableConfirm)

    }

    // MARK: - shouldChangeCharacters

    func test_givenAnyValueForSecurityCode_whenICallShould_thenIDontAllowMoreThen6Characters() {

        //given

        //when
       let result = sut.shouldChangeCharacters(textValue: "", withNewText: "1234567", inRange: NSRange())

        //then
        XCTAssert(result == false)

    }
    func test_givenAnyValueForSecurityCode_whenICallShould_thenIAllow6Characters() {

        //given

        //when
        let result = sut.shouldChangeCharacters(textValue: "", withNewText: "123456", inRange: NSRange())

        //then
        XCTAssert(result == true)

    }
    func test_givenAnyValueForSecurityCode_whenICallShould_thenIAllowOnlyNumber() {

        //given

        //when
        let result = sut.shouldChangeCharacters(textValue: "", withNewText: "AB123", inRange: NSRange())

        //then
        XCTAssert(result == false)

    }

    // MARK: - updateTimer

    func test_givenSecurityScreen_whenICallUpdateTimer_thenICallViewUpdateTimer() {

        //given
        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.updateTimer()

        //then
        XCTAssert(dummyView.isCalledUpdateTime == true)

    }

    func test_givenRemainingTime_whenICallUpdateTimer_thenICallViewUpdateTimerWithRemainingTimeFormatted() {

        //given
        let dummyView = DummyView()
        sut.view = dummyView

        sut.remainingTime = 60

        //when
        sut.updateTimer()

        //then
        XCTAssert(dummyView.updateText == sut.calculateMinSecond(forSeconds: sut.remainingTime))

    }

    func test_givenRemainingTimeBiggerThanOne_whenICallUpdateTimer_thenRemainingTimeShouldDecreaseOne() {

        //given
        let givenTime = 60

        let dummyView = DummyView()
        sut.view = dummyView

        sut.remainingTime = givenTime

        //when
        sut.updateTimer()

        //then
        XCTAssert(sut.remainingTime == (givenTime - 1))

    }

    func test_givenRemainingTimeEqualsOne_whenICallUpdateTimer_thenRemainingTimeShouldDecreaseOne() {

        //given
        let givenTime = 1

        let dummyView = DummyView()
        sut.view = dummyView

        sut.remainingTime = givenTime

        //when
        sut.updateTimer()

        //then
        XCTAssert(sut.remainingTime == (givenTime - 1))

    }

    func test_givenRemainingTimeEqualsZero_whenICallUpdateTimer_thenICallDisableConfirmButton() {

        //given
        let givenTime = 0

        let dummyView = DummyView()
        sut.view = dummyView

        sut.remainingTime = givenTime

        //when
        sut.updateTimer()

        //then
        XCTAssert(dummyView.isCalledDisableConfirm)

    }
    func test_givenRemainingTimeEqualsZero_whenICallUpdateTimer_thenRemainingTimeShouldBeRemainInZero() {

        //given
        let givenTime = 0

        let dummyView = DummyView()
        sut.view = dummyView

        sut.remainingTime = givenTime

        //when
        sut.updateTimer()

        //then
        XCTAssert(sut.remainingTime == givenTime)

    }
    func test_givenRemainingTimeEqualsOne_whenICallUpdateTimer_thenICallViewFinishTimer() {

        //given
        let givenTime = 1

        let dummyView = DummyView()
        sut.view = dummyView

        sut.remainingTime = givenTime

        //when
        sut.updateTimer()

        //then
        XCTAssert(dummyView.isCalledFinishTimer == true)

    }

    func test_givenRemainingTimeBiggerThanOne_whenICallUpdateTimer_thenIDoNotCallViewFinishTimer() {

        //given
        let givenTime = 60

        let dummyView = DummyView()
        sut.view = dummyView

        sut.remainingTime = givenTime

        //when
        sut.updateTimer()

        //then
        XCTAssert(dummyView.isCalledFinishTimer == false)

    }

    // MARK: - codeWrote

    func test_givenAnyText_whenICallCodeWroteText_thenICodeShouldMatchWithParameterText() {

        //given
        let text = "code"

        //when
        sut.codeWrote(text: text)

        //then
        XCTAssert(sut.code == text)

    }

    func test_givenTextWithMaximumLengthAllowed_whenICallCodeWroteText_thenICalledEnableConfirm() {

        //given
        var text = ""

        for i in 1 ... Settings.SecurityControllerGlobal.security_code_maximum_length {
            text += "\(i)"
        }

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.codeWrote(text: text)

        //then
        XCTAssert(dummyView.isCalledEnableConfirm == true)

    }

    func test_givenTextWithLessengthAllowed_whenICallCodeWroteText_thenIDoNotCalledEnableConfirm() {

        //given
        var text = ""

        for i in 1 ... (Settings.SecurityControllerGlobal.security_code_maximum_length - 2) {
            text += "\(i)"
        }

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.codeWrote(text: text)

        //then
        XCTAssert(dummyView.isCalledEnableConfirm == false)

    }

    func test_givenTextWithMaximumLengthAllowed_whenICallCodeWroteText_thenIDoNotCallDisableConfirm() {

        //given
        var text = ""

        for i in 1 ... Settings.SecurityControllerGlobal.security_code_maximum_length {
            text += "\(i)"
        }

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.codeWrote(text: text)

        //then
        XCTAssert(dummyView.isCalledDisableConfirm == false)

    }

    func test_givenTextWithLessLengthAllowed_whenICallCodeWroteText_thenICallDisableConfirm() {

        //given
        var text = ""

        for i in 1 ... (Settings.SecurityControllerGlobal.security_code_maximum_length - 2) {
            text += "\(i)"
        }

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.codeWrote(text: text)

        //then
        XCTAssert(dummyView.isCalledDisableConfirm == true)

    }

    // MARK: - requestNewPin

    func test_givenAny_whenICallRequestNewPin_thenICallViewStartTimer() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.requestNewPin()

        // then
        XCTAssert(dummyView.isCalledStartTimer == true)

    }

    func test_givenAny_whenICallRequestNewPin_thenICallViewUpdateTimer() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.requestNewPin()

        // then
        XCTAssert(dummyView.isCalledUpdateTime == true)

    }

    func test_givenAny_whenICallRequestNewPin_thenICallViewWithTextFormatWithNumberOfSecondsAsTimeForTimerInSeconds() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.requestNewPin()

        // then
        XCTAssert(dummyView.updateText == sut.calculateMinSecond(forSeconds: sut.timeForTimerInSeconds))

    }

    func test_givenAny_whenICallRequestNewPin_thenRemainingTimeShouldMatchWithTimeForTimerInSeconds() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.requestNewPin()

        // then
        XCTAssert(sut.remainingTime == sut.timeForTimerInSeconds)

    }

    func test_givenAny_whenICallRequestNewPin_thenIClearText() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.requestNewPin()

        // then
        XCTAssert(dummyView.clearText.isEmpty)

    }

    // MARK: - replayServiceSecurity

    func test_givenAny_whenICallReplayServiceSecurity_thenICallBusManagerDismissController() {

        // given
        let code = ""
        let headers = [String: String]()

        // when
        sut.replayServiceSecurity(withHeader: headers, andSecurityCode: code)

        // then
        XCTAssert(dummyBusManager.isCalledDismissAction == true)

    }

    func test_givenAny_whenICallReplayServiceSecurity_thenICallShowLoading() {

        // given
        let code = ""
        let headers = [String: String]()

        // when
        sut.replayServiceSecurity(withHeader: headers, andSecurityCode: code)

        // then
        XCTAssert(dummyBusManager.isCalledShowLoading == true)

    }

    func test_givenResponseHeadersAndSecurityCode_whenICallReplayServiceSecurity_thenICallSecurityInterceptorStoreUnlock() {

        // given
        let code = "123456"

        var headers = [String: String]()
        headers["authenticationtype"] = "05"
        headers["authenticationdata"] = "otp-token"

        // when
        sut.replayServiceSecurity(withHeader: headers, andSecurityCode: code)

        // then
        XCTAssert(dummySecurityInterceptorStore.isCalledUnlock == true)

    }

    func test_givenResponseHeadersAndSecurityCode_whenICallReplayServiceSecurity_thenHeadersMatch() {

        // given
        let code = "123456"

        var headers = [String: String]()
        headers["authenticationtype"] = "05"
        headers["authenticationdata"] = "otp-token"

        // when
        sut.replayServiceSecurity(withHeader: headers, andSecurityCode: code)

        let authenticationdata = "\(headers["authenticationdata"]!)=\(code)"

        // then
        XCTAssert(dummySecurityInterceptorStore.headersSent["authenticationtype"] == headers["authenticationtype"])
        XCTAssert(dummySecurityInterceptorStore.headersSent["authenticationdata"] == authenticationdata)

    }

    class DummyView: SecurityViewProtocol {

        var isCalledGetSecurityInformation = false
        var isCalledClearText = false
        var isCalledUpdateTime = false
        var isCalledStartTimer = false
        var isCalledFinishTimer = false
        var isCalledEnableConfirm = false
        var isCalledDisableConfirm = false

        var securityData = SecurityDisplayData()
        var clearText = "clearText"
        var updateText = ""

        func showSecurityData(withSecurityDisplayItems displayItems: SecurityDisplayData) {

            securityData = displayItems
            isCalledGetSecurityInformation = true

        }
        func clearSecurityCodeInput(value: String) {

            clearText = value
            isCalledClearText = true

        }

        func updateTimer(withText text: String) {

            isCalledUpdateTime = true
            updateText = text

        }

        func startTimer() {
            isCalledStartTimer = true
        }

        func finishTimer() {
            isCalledFinishTimer = true
        }

        func enableConfirm() {
            isCalledEnableConfirm = true
        }

        func disableConfirm() {
            isCalledDisableConfirm = true
        }

        func showError(error: ModelBO) {
        }

        func changeColorEditTexts() {
        }

    }

    class DummyBusManager: BusManager {

        var isCalledBackAction = false
        var isCalledShowLoading = false
        var isCalledDismissAction = false

        override init() {
            super.init()
        }

        override func back() {
            isCalledBackAction = true
        }

        override func showLoading(completion: (() -> Void)?) {
            if let completion = completion {
                completion()
            }
            isCalledShowLoading = true
        }

        override func dismissViewController(animated: Bool, completion: (() -> Void)?) {
            if let completion = completion {
                completion()
            }

            isCalledDismissAction = true
        }

    }

    class DummySecurityInterceptorStore: SecurityInterceptorStore {

        var isCalledUnlock = false
        var headersSent = [String: String]()

        override init(worker: NetworkWorker) {
            super.init(worker: worker)
        }

        required convenience init() {
            self.init(worker: NetworkWorker())
        }

        override func unLock(headers: [String: String]) {

            isCalledUnlock = true
            headersSent = headers
        }
    }
}
