//
//  SoftokenSecurityPresenterTest.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 20/2/19.
//  Copyright © 2019 BBVA. All rights reserved.
//

import XCTest
import BBVA_Network

#if TESTMX
@testable import shoppingappMX
#endif

class SoftokenSecurityPresenterTest: XCTestCase {
    
    var sut: SoftokenSecurityPresenter<DummyView>!
    var dummyBusManager: DummyBusManager!
    var dummySecurityInterceptorStore: DummySecurityInterceptorStore!
    
    override func setUp() {
        super.setUp()
        
        dummyBusManager = DummyBusManager()
        dummySecurityInterceptorStore = DummySecurityInterceptorStore()
        dummyBusManager.securityInterceptorStore = dummySecurityInterceptorStore
        sut = SoftokenSecurityPresenter<DummyView>(busManager: dummyBusManager)
    }
    
    // MARK: - acceptButtonPressed
    
    func test_givenAny_whenICallAcceptButtonPressed_thenICallShowLoading() {
        
        // given
        
        // when
        sut.acceptButtonPressed()
        
        // then
        XCTAssert(dummyBusManager.isCalledShowLoading == true)
    }
    
    func test_givenAny_whenICallAcceptButtonPressed_thenICallDismissViewController() {
        
        // given
        
        // when
        sut.acceptButtonPressed()
        
        // then
        XCTAssert(dummyBusManager.isCalledDismissViewController == true)
    }
    
    func test_givenOtpTokenNil_whenICallAcceptButtonPressed_thenICallSecurityInterceptorStoreUnlockStop() {
        
        // given
        sut.otpToken = nil
        
        // when
        sut.acceptButtonPressed()
        
        // then
        XCTAssert(dummySecurityInterceptorStore.isCalledUnLockStop == true)
    }
    
    func test_givenOtpTokenEmpty_whenICallAcceptButtonPressed_thenICallSecurityInterceptorStoreUnlockStop() {
        
        // given
        sut.otpToken = ""
        
        // when
        sut.acceptButtonPressed()
        
        // then
        XCTAssert(dummySecurityInterceptorStore.isCalledUnLockStop == true)
    }
    
    func test_givenOtpToken_whenICallAcceptButtonPressed_thenINotCallSecurityInterceptorStoreUnlockStop() {
        
        // given
        sut.otpToken = "123456"
        
        // when
        sut.acceptButtonPressed()
        
        // then
        XCTAssert(dummySecurityInterceptorStore.isCalledUnLockStop == false)
    }
    
    func test_givenOtpTokenNil_whenICallAcceptButtonPressed_thenINotCallSecurityInterceptorStoreUnlock() {
        
        // given
        sut.otpToken = nil
        
        // when
        sut.acceptButtonPressed()
        
        // then
        XCTAssert(dummySecurityInterceptorStore.isCalledUnlock == false)
    }
    
    func test_givenOtpTokenEmpty_whenICallAcceptButtonPressed_thenINotCallSecurityInterceptorStoreUnlock() {
        
        // given
        sut.otpToken = ""
        
        // when
        sut.acceptButtonPressed()
        
        // then
        XCTAssert(dummySecurityInterceptorStore.isCalledUnlock == false)
    }
    
    func test_givenOtpToken_whenICallAcceptButtonPressed_thenICallSecurityInterceptorStoreUnlock() {
        
        // given
        sut.otpToken = "123456"
        
        // when
        sut.acceptButtonPressed()
        
        // then
        XCTAssert(dummySecurityInterceptorStore.isCalledUnlock == true)
    }
    
    func test_givenOtpToken_whenICallAcceptButtonPressed_thenHeadersMatch() {
        
        // given
        sut.otpToken = "123456"
        
        var headers = [String: String]()
        headers["authenticationtype"] = "33"
        headers["authenticationdata"] = "otp-token"
        
        let authenticationdata = "\(headers["authenticationdata"]!)=\(sut.otpToken!)"
        
        // when
        sut.acceptButtonPressed()
        
        // then
        XCTAssert(dummySecurityInterceptorStore.headersSent["authenticationtype"] == headers["authenticationtype"])
        XCTAssert(dummySecurityInterceptorStore.headersSent["authenticationdata"] == authenticationdata)
    }
    
    // MARK: - Dummys
    
    class DummyView: SoftokenSecurityViewProtocol {
        
        func showError(error: ModelBO) {
        }
        
        func changeColorEditTexts() {
        }
    }
    
    class DummyBusManager: BusManager {
        
        var isCalledShowLoading = false
        var isCalledDismissViewController = false
        
        override init() {
            super.init()
        }
        
        override func showLoading(completion: (() -> Void)?) {
            if let completion = completion {
                completion()
            }
            isCalledShowLoading = true
        }
        
        override func dismissViewController(animated: Bool, completion: (() -> Void)?) {
            if let completion = completion {
                completion()
            }
            
            isCalledDismissViewController = true
        }
    }
    
    class DummySecurityInterceptorStore: SecurityInterceptorStore {
        
        var isCalledUnlock = false
        var headersSent = [String: String]()
        var isCalledUnLockStop = false
        
        override init(worker: NetworkWorker) {
            super.init(worker: worker)
        }
        
        required convenience init() {
            self.init(worker: NetworkWorker())
        }
        
        override func unLock(headers: [String: String]) {
            
            isCalledUnlock = true
            headersSent = headers
        }
        
        override func unLockStop() {
            isCalledUnLockStop = true
        }
    }
}
