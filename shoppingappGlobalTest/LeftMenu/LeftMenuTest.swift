//
//  LeftMenuTest.swift
//  shoppingapp
//
//  Created by Javier Dominguez on 13/10/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import XCTest
import RxSwift
import CellsNativeComponents

#if TESTMX
@testable import shoppingappMX
#elseif TESTPE
@testable import shoppingappPE
#endif

class LeftMenuTest: XCTestCase {

    // MARK: vars
    var sut: LeftMenuPresenter<LeftMenuDummyView>!
    var dummyBusManager: DummyBusManager!
    var dummyPreferencesManager: DummyPreferencesManager!
    var dummyDeviceManager: DummyDeviceManager!
    var dummySessionManager: DummySessionManager!

    // MARK: setup
    override func setUp() {
        super.setUp()
        KeychainManagerTestHelper.configureKeychainManagerForTesting()
        dummyBusManager = DummyBusManager()
        sut = LeftMenuPresenter<LeftMenuDummyView>(busManager: dummyBusManager)
        
        dummyPreferencesManager = DummyPreferencesManager()
        PreferencesManager.instance = dummyPreferencesManager

        dummyDeviceManager = DummyDeviceManager()
        DeviceManager.instance = dummyDeviceManager

        sut.notificationManager = DummyNotificationManager()
        
        DummySessionManager.setupMock()
        dummySessionManager = (SessionDataManager.sessionDataInstance() as! DummySessionManager)
    }

    override func tearDown() {
        super.tearDown()
    }

    // MARK: createDisplayData
    func test_givenAnInteractor_whenCallCreateDisplayData_thenProvideLeftMenuOptionsIsCalled() {

        //given
        let dummyInteractor = LeftMenuDummyInteractor()
        sut.interactor = dummyInteractor

        //when
        sut.createDisplayData()

        //then
        XCTAssert(dummyInteractor.isCalledProvideLeftMenuOptions == true)
    }

    func test_givenAny_whenCallCreateDisplayData_thenSetDisplayDataIsCalled() {

        //given
        let dummyView = LeftMenuDummyView()
        sut.view = dummyView
        let dummyInteractor = LeftMenuDummyInteractor()
        sut.interactor = dummyInteractor

        //when
        sut.createDisplayData()

        //then
        XCTAssert(dummyView.isCalledSetDisplayData == true)
    }
    
    // MARK: - checkSession

    func test_givenAny_whenCallCheckSession_andSessionIsClosed_thenCloseMenu() {

        //given
        let dummyView = LeftMenuDummyView()
        sut.view = dummyView

        //when
        sut.checkSession()

        //then
        XCTAssert(dummyView.isCalledCloseMenu == true)

    }

    func test_givenAnUserNotSaved_whenCallCheckSessionAndSessionIsUserIsLogged_thenPresentLoginAndCloseSessionAndUserNotLogged() {

        //given
        let dummyView = LeftMenuDummyView()
        KeychainManager.shared.resetKeyChainItems()

        dummySessionManager.isUserLogged = true
        sut.view = dummyView

        //when
        sut.checkSession()

        //then
        XCTAssert(dummyView.isCalledPresentLoginAndCloseSession == true)

    }

    func test_givenASavedUser_whenCallCheckSessionAndUserIsLogged_thenICallSessionDataManagerClearTabBarSelected() {

        //given
        let dummyView = LeftMenuDummyView()
        KeychainManager.shared.resetKeyChainItems()

        dummySessionManager.isUserLogged = true
        sut.view = dummyView

        //when
        sut.checkSession()

        //then
        XCTAssert(dummySessionManager.isCalledClearTabBarSelected)

    }

    func test_givenASavedUser_whenCallCheckSessionAndUserIsLogged_thenPresentLoginAndCloseSessionAndUserNotLogged() {

        //given
        let dummyView = LeftMenuDummyView()
        KeychainManager.shared.saveUserId(forUserId: "userId")
        KeychainManager.shared.saveFirstname(forFirstname: "firstname")

        dummySessionManager.isUserLogged = true
        sut.view = dummyView

        //when
        sut.checkSession()

        //then
        XCTAssert(dummyView.isCalledPresentLoginAndCloseSession == true)

    }

    func test_givenAnonymousSession_whenWhenICallCheckSession_thenICallSessionDataManagerClearTabBarSelected() {
        
        // given
        dummySessionManager.isUserAnonymous = true

        // when
        sut.checkSession()

        // then
        XCTAssert(dummySessionManager.isCalledClearTabBarSelected == true)

    }

    func test_givenAnonymousSession_whenWhenICallCheckSession_thenICallViewPresentLoginAndCloseSession() {

        let dummyView = LeftMenuDummyView()
        sut.view = dummyView

        // given
        dummySessionManager.isUserAnonymous = true

        // when
        sut.checkSession()

        // then
        XCTAssert(dummyView.isCalledPresentLoginAndCloseSession == true)

    }

    func test_givenAnonymousSessionFalseAndIsUserLoggedFalse_whenWhenICallCheckSession_thenICallViewCloseMenu() {

        let dummyView = LeftMenuDummyView()
        sut.view = dummyView

        // given
        dummySessionManager.isUserAnonymous = false
        dummySessionManager.isUserLogged = false

        // when
        sut.checkSession()

        // then
        XCTAssert(dummyView.isCalledCloseMenu == true)

    }
    
    // MARK: - presentLoginAndCloseSession

    func test_givenAny_whenCallPresentLoginAndCloseSession_thenGoLoginScreen() {

        //given
        let dummyView = LeftMenuDummyView()
        let dummyInteractor = LeftMenuDummyInteractor()
        sut.view = dummyView
        sut.interactor = dummyInteractor

        //when
        sut.presentLoginAndCloseSession()

        //then
        XCTAssert(dummyBusManager.isCalledSetRootWhenNoSession == true)

    }

    func test_givenASessionManager_whenCallPresentLoginAndCloseSession_andSessionIsNotClosed_thenCallProvideLogoutData() {

        //given
        let dummyView = LeftMenuDummyView()
        sut.view = dummyView

        //when
        sut.presentLoginAndCloseSession()

        //then
        XCTAssert(dummySessionManager.isCalledRequestLogout == true)

    }

    func test_givenAny_whenCallPresentLoginAndCloseSession_thenCallUpdateOptionsMenu() {

        //given
        let dummyInteractor = LeftMenuDummyInteractor()
        let dummyView = LeftMenuDummyView()
        sut.view = dummyView
        sut.interactor = dummyInteractor

        //when
        sut.presentLoginAndCloseSession()

        //then
        XCTAssert(dummyView.isCalledUpdateOptionsMenu == true)

    }

    func test_givenAny_whenCallPresentLoginAndCloseSession_andSessionIsNotClosed_thenCallSessionDataManagerCloseSession() {

        //given
        let dummyInteractor = LeftMenuDummyInteractor()
        let dummyView = LeftMenuDummyView()
        
        sut.interactor = dummyInteractor
        sut.view = dummyView

        //when
        sut.presentLoginAndCloseSession()

        //then
        XCTAssert(dummySessionManager.isCalledCloseSession == true)
    }

    // MARK: - leftMenuOptionPressed
    
    // MARK: - leftMenuOptionPressed CALL

    func test_givenDisplayItemLeftMenuCall_whenICallLeftMenuOptionPressedWithDisplayItem_thenICallViewShowCallBBVA() {

        // given
        let dummyView = LeftMenuDummyView()
        sut.view = dummyView

        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "title", option: .call)

        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)

        // then
        XCTAssert(dummyView.isCalledShowCallBBVA == true)
    }

    func test_givenDisplayItemLeftMenuCall_whenICallLeftMenuOptionPressedWithDisplayItem_thenICallViewShowCallBBVAWithSettingsTelephone() {

        // given
        let dummyView = LeftMenuDummyView()
        sut.view = dummyView

        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "title", option: .call)

        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)

        // then
        XCTAssert(dummyView.telephone == Settings.Global.default_bbva_phone_number)
    }
    
    func test_givenDisplayItemLeftMenuCall_whenICallLeftMenuOptionPressedWithDisplayItem_thenINotCallViewCloseMenu() {
        
        // given
        let dummyView = LeftMenuDummyView()
        sut.view = dummyView
        
        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "title", option: .call)
        
        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)
        
        // then
        XCTAssert(dummyView.isCalledCloseMenu == false)
    }
    
    func test_givenDisplayItemLeftMenuCall_whenICallLeftMenuOptionPressedWithDisplayItem_thenINotCallRouterNavigateToHelpScreen() {
        
        // given
        let dummyView = LeftMenuDummyView()
        sut.view = dummyView
        
        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "title", option: .call)
        
        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)
        
        // then
        XCTAssert(dummyBusManager.isCalledShowHelp == false)
    }
    
    func test_givenDisplayItemLeftMenuCall_whenICallLeftMenuOptionPressedWithDisplayItem_thenINotCallViewShowBankingMobile() {
        
        // given
        let dummyView = LeftMenuDummyView()
        sut.view = dummyView
        
        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "title", option: .call)
        
        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)
        
        // then
        XCTAssert(dummyView.isCalledShowBankingMobile == false)
    }
    
    // MARK: - leftMenuOptionPressed HELP

    func test_givenDisplayItemLeftMenuHelp_whenICallLeftMenuOptionPressedWithDisplayItem_thenIDoNotCallViewShowCallBBVA() {

        // given
        let dummyView = LeftMenuDummyView()
        sut.view = dummyView

        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "title", option: .help)

        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)

        // then
        XCTAssert(dummyView.isCalledShowCallBBVA == false)
    }
    
    func test_givenDisplayItemLeftMenuHelp_whenICallLeftMenuOptionPressedWithDisplayItem_thenICallViewCloseMenu() {
        
        // given
        let dummyView = LeftMenuDummyView()
        sut.view = dummyView
        
        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "title", option: .help)
        
        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)
        
        // then
        XCTAssert(dummyView.isCalledCloseMenu == true)
    }
    
    func test_givenDisplayItemLeftMenuHelp_whenICallLeftMenuOptionPressedWithDisplayItem_thenICallRouterNavigateToHelpScreen() {
        
        // given
        let dummyView = LeftMenuDummyView()
        sut.view = dummyView
        
        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "title", option: .help)
        
        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)
        
        // then
        XCTAssert(dummyBusManager.isCalledShowHelp == true)
    }
    
    func test_givenDisplayItemLeftMenuHelp_whenICallLeftMenuOptionPressedWithDisplayItem_thenINotCallViewShowBankingMobile() {
        
        // given
        let dummyView = LeftMenuDummyView()
        sut.view = dummyView
        
        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "title", option: .help)
        
        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)
        
        // then
        XCTAssert(dummyView.isCalledShowBankingMobile == false)
    }
    
    // MARK: - leftMenuOptionPressed WEB

    func test_givenDisplayItemLeftMenuWeb_whenICallLeftMenuOptionPressedWithDisplayItem_thenIDoNotCallViewShowCallBBVA() {

        // given
        let dummyView = LeftMenuDummyView()
        sut.view = dummyView

        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "title", option: .web)

        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)

        // then
        XCTAssert(dummyView.isCalledShowCallBBVA == false)
    }
    
    func test_givenDisplayItemLeftMenuWeb_whenICallLeftMenuOptionPressedWithDisplayItem_thenICallViewShowBankingMobile() {
        
        // given
        let dummyView = LeftMenuDummyView()
        sut.view = dummyView
        
        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "title", option: .web)
        
        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)
        
        // then
        XCTAssert(dummyView.isCalledShowBankingMobile == true)
    }
    
    // MARK: - leftMenuOptionPressed UNKNOWM

    func test_givenDisplayItemLeftMenuUnknowm_whenICallLeftMenuOptionPressedWithDisplayItem_thenICallViewShowCallBBVA() {

        // given
        let dummyView = LeftMenuDummyView()
        sut.view = dummyView

        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "title", option: .unknowm)

        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)

        // then
        XCTAssert(dummyView.isCalledShowCallBBVA == false)
    }

    func test_givenDisplayItemLeftMenuUnknowm_whenICallLeftMenuOptionPressedWithDisplayItem_thenINotCallViewShowBankingMobile() {

        // given
        let dummyView = LeftMenuDummyView()
        sut.view = dummyView

        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "title", option: .unknowm)

        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)

        // then
        XCTAssert(dummyView.isCalledShowBankingMobile == false)
    }
    
    // MARK: - leftMenuOptionPressed SETTINGS
    
    func test_givenDisplayItemLeftMenuSettings_whenICallLeftMenuOptionPressedWithDisplayItem_thenIDoNotCallViewShowCallBBVA() {
        
        // given
        let dummyView = LeftMenuDummyView()
        sut.view = dummyView
        
        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "title", option: .settings)
        
        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)
        
        // then
        XCTAssert(dummyView.isCalledShowCallBBVA == false)
    }

    func test_givenDisplayItemLeftMenuSettings_whenICallLeftMenuOptionPressedWithDisplayItem_thenINotCallViewShowBankingMobile() {

        // given
        let dummyView = LeftMenuDummyView()
        sut.view = dummyView

        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "title", option: .settings)

        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)

        // then
        XCTAssert(dummyView.isCalledShowBankingMobile == false)
    }
    
    func test_givenDisplayItemLeftMenuSettings_whenICallLeftMenuOptionPressedWithDisplayItem_thenICallViewCloseMenu() {
        
        let dummyView = LeftMenuDummyView()
        sut.view = dummyView
        
        // given
        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "title", option: .settings)
        
        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)
        
        // then
        XCTAssert(dummyView.isCalledCloseMenu == true)
    }
    
    func test_givenDisplayItemLeftMenuSettingsAndDisplaySettingTransport_whenICallLeftMenuOptionPressedWithDisplayItem_thenIPublishTheSameSettingTransport() {

        let dummyView = LeftMenuDummyView()
        sut.view = dummyView

        // given
        
        (sut.notificationManager as! DummyNotificationManager).notificationsIsAuthorized = [true]
        
        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "settings", option: .settings)

        let keyBalancesAvailable = PreferencesManagerKeys.kConfigBalancesAvailable
        _ = dummyPreferencesManager.getValueWithPreferencesKey(forKey: keyBalancesAvailable)
        
        let keyVibrationEnabled = PreferencesManagerKeys.kConfigVibrationEnabled
        _ = dummyPreferencesManager.getValueWithPreferencesKey(forKey: keyVibrationEnabled)
        
        let keyTicketPromotionsEnabled = PreferencesManagerKeys.kConfigTicketPromotionsEnabled
        _ = dummyPreferencesManager.getValueWithPreferencesKey(forKey: keyTicketPromotionsEnabled)

        let settingsTransport = SettingsTransport(isNotificationsAvailable: true, isBalancesAvailable: dummyPreferencesManager.isBalancesAvailable, isVibrationEnabled: dummyPreferencesManager.isVibrationEnabled, isTicketPromotionsEnabled: dummyPreferencesManager.isTicketPromotionsEnabled)

        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)

        // then
        XCTAssert(settingsTransport.isBalancesAvailable == dummyPreferencesManager.isBalancesAvailable)
        XCTAssert(settingsTransport.isNotificationsAvailable == true)
        XCTAssert(settingsTransport.isVibrationEnabled == dummyPreferencesManager.isVibrationEnabled)
        XCTAssert(settingsTransport.isTicketPromotionsEnabled == dummyPreferencesManager.isTicketPromotionsEnabled)
    }

    func test_givenDisplayItemLeftMenuSettings_whenICallLeftMenuOptionPressedWithDisplayItem_thenICallGetValueWithPreferencesKey() {

        let dummyView = LeftMenuDummyView()
        sut.view = dummyView

        // given
        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "settings", option: .settings)

        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)

        // then
        wait(for: [dummyBusManager.expectationNavigateScreen, dummyBusManager.expectationPublishData], timeout: 10)
        XCTAssert(dummyPreferencesManager.isCalledGetValueWithPreferencesKey == true)
    }

    func test_givenDisplayItemLeftMenuSettings_whenICallLeftMenuOptionPressedWithDisplayItem_thenICallBusShowConfiguration() {

        let dummyView = LeftMenuDummyView()
        sut.view = dummyView

        //given
        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "settings", option: .settings)

        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)

        // then
        wait(for: [dummyBusManager.expectationNavigateScreen, dummyBusManager.expectationPublishData], timeout: 10)
        XCTAssert(dummyBusManager.isCalledConfiguration == true)
    }

    func test_givenDisplayItemLeftMenuSettingsAndTokenNotMatchWithRegisteredToken_whenICallLeftMenuOptionPressedWithDisplayItem_thenICallBusPublishWithTheSameSettingsTransport() {

        let dummyView = LeftMenuDummyView()
        sut.view = dummyView

        //given
        (sut.notificationManager as! DummyNotificationManager).notificationsIsAuthorized = [true]
        
        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "settings", option: .settings)
        
        dummyPreferencesManager.registerDeviceToken = "token"
        dummyPreferencesManager.apnsToken = "1234"

        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)
        
        //then
        let settingsTransport = SettingsTransport(isNotificationsAvailable: false, isBalancesAvailable: dummyPreferencesManager.isBalancesAvailable, isVibrationEnabled: dummyPreferencesManager.isVibrationEnabled, isTicketPromotionsEnabled: dummyPreferencesManager.isTicketPromotionsEnabled)
        
        wait(for: [dummyBusManager.expectationNavigateScreen, dummyBusManager.expectationPublishData], timeout: 10)
        XCTAssert(dummyBusManager.isCalledPublishDataSetting == true)
        XCTAssert(dummyBusManager.dummySettingsTransport == settingsTransport)
    }
    
    func test_givenDisplayItemLeftMenuSettingsAndTokenMatchWithRegisteredToken_whenICallLeftMenuOptionPressedWithDisplayItem_thenICallBusPublishWithTheSameSettingsTransport() {
        
        let dummyView = LeftMenuDummyView()
        sut.view = dummyView
        
        //given
        (sut.notificationManager as! DummyNotificationManager).notificationsIsAuthorized = [true]
        
        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "settings", option: .settings)
        
        let token = "token"
        dummyPreferencesManager.registerDeviceToken = token
        dummyPreferencesManager.apnsToken = token
        
        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)
        
        //then
        let settingsTransport = SettingsTransport(isNotificationsAvailable: true, isBalancesAvailable: dummyPreferencesManager.isBalancesAvailable, isVibrationEnabled: dummyPreferencesManager.isVibrationEnabled, isTicketPromotionsEnabled: dummyPreferencesManager.isTicketPromotionsEnabled)
        
        wait(for: [dummyBusManager.expectationNavigateScreen, dummyBusManager.expectationPublishData], timeout: 10)
        XCTAssert(dummyBusManager.isCalledPublishDataSetting == true)
        XCTAssert(dummyBusManager.dummySettingsTransport == settingsTransport)
    }
    
    // MARK: - leftMenuOptionPressed ABOUT

    func test_givenDisplayItemLeftMenuSettingsAndDisplayAboutTransport_whenICallLeftMenuOptionPressedWithDisplayItem_thenICallBusPublishWithTheSameAbout() {

        let dummyView = LeftMenuDummyView()
        sut.view = dummyView

        //given
        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "title", option: .about)

        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)

        //then
        XCTAssert(dummyBusManager.isCalledPublishDataAbout == true)
        XCTAssert(dummyBusManager.dummyValueEventAbout.deviceModel == dummyDeviceManager.modelName)
        XCTAssert(dummyBusManager.dummyValueEventAbout.appVersion == dummyDeviceManager.bundleVersion)
        XCTAssert(dummyBusManager.dummyValueEventAbout.operatingSystem == dummyDeviceManager.operatingSystem)
    }

    func test_givenDisplayItemLeftMenuSettingsAndDisplayAboutTransport_whenICallLeftMenuOptionPressedWithDisplayItem_thenICallBusPublishOptionsWithTheSameAbout() {

        let dummyView = LeftMenuDummyView()
        sut.view = dummyView

        //given
        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "title", option: .about)

        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)

        //then
        XCTAssert(dummyBusManager.isCalledPublishDataAboutOptions == true)
        XCTAssert(dummyBusManager.dummyValueEventOptionsAbout.isLegalAdviceLinkHidden == false)
        XCTAssert(dummyBusManager.dummyValueEventOptionsAbout.isRateLinkHidden == true)
        XCTAssert(dummyBusManager.dummyValueEventOptionsAbout.isShareLinkHidden == false)
        XCTAssert(dummyBusManager.dummyValueEventOptionsAbout.isVersionLinkHidden == false)
    }

    func test_givenDisplayItemLeftMenuSettings_whenICallLeftMenuOptionPressedWithDisplayItem_thenICallBusShowAbout() {

        let dummyView = LeftMenuDummyView()
        sut.view = dummyView

        //given
        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "title", option: .about)

        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)

        // then
        XCTAssert(dummyBusManager.isCalledAbout == true)
    }

    // MARK: Private Methods
    
    private func createHomeSectionAbout() -> AboutTransport {

        return AboutTransport()
    }

    // MARK: dummy classes

    class LeftMenuDummyView: LeftMenuViewProtocol {

        var isCalledSetDisplayData = false
        var isCalledUpdateOptionsMenu = false
        var isCalledCloseMenu = false
        var isCalledPresentLoginAndCloseSession = false
        var isCalledShowCallBBVA = false
        var displayItemLeftMenu: DisplayItemLeftMenu?
        var isCalledShowBankingMobile = false

        var telephone = ""

        func setDisplayData(displayData: [DisplayItemLeftMenu]) {
            isCalledSetDisplayData = true
        }

        func updateOptionsMenu(isUserLogged: Bool) {
            isCalledUpdateOptionsMenu = true
        }

        func presentLoginAndCloseSession() {
            isCalledPresentLoginAndCloseSession = true
        }

        func closeMenu() {
            isCalledCloseMenu = true
        }

        func showError(error: ModelBO) {
        }

        func changeColorEditTexts() {
        }

        func showCallBBVA(withTel tel: String) {
            isCalledShowCallBBVA = true
            telephone = tel
        }

        func showBankingMobile() {
            isCalledShowBankingMobile = true
        }
    }

    class LeftMenuDummyInteractor: LeftMenuInteractorProtocol {

        var isCalledProvideLeftMenuOptions = false
        var forceError = false
        var errorLogout: ErrorBO?
        var leftMenuOptions: [LeftMenuOptionBO] = []

        func provideOptionsLeftMenu() -> Observable<[ModelBO]> {

            isCalledProvideLeftMenuOptions = true

            let option1 = LeftMenuOptionBO(image: "1", title: "1", option: "settings")
            let option2 = LeftMenuOptionBO(image: "2", title: "2", option: "help")
            let modelBO: [ModelBO] = [option1, option2]

            leftMenuOptions = modelBO as! [LeftMenuOptionBO]

            return Observable<[ModelBO]>.just(modelBO as [ModelBO])
        }

    }

    class DummyBusManager: BusManager {

        var isCalledSetRootWhenNoSession = false
        var isCalledShowHelp = false
        var isCalledShowSettings = false
        var isCalledPublishDataSetting = false
        var isCalledPublishDataAbout = false
        var isCalledPublishDataAboutOptions = false
        var isCalledConfiguration = false
        var isCalledAbout = false

        var dummyTag: String = ""
        var dummyEvent: ActionSpec<Any>?
        var dummySettingsTransport: SettingsTransport?
        var dummyValueEventAbout: AboutTransport!
        var dummyValueEventOptionsAbout: AboutOptionsTransport!
        
        let expectationNavigateScreen = XCTestExpectation(description: "Checking notifications authorization")
        let expectationPublishData = XCTestExpectation(description: "Checking notifications authorization")

        override init() {
            super.init()
            routerFactory = RouterInitFactory()
        }
        
        override func setRootWhenNoSession() {
            isCalledSetRootWhenNoSession = true
        }

        override func navigateScreen<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {
            
            if tag == LeftMenuPageReaction.ROUTER_TAG && event === LeftMenuPageReaction.EVENT_NAV_TO_HELP {
                isCalledShowHelp = true
            } else if tag == LeftMenuPageReaction.ROUTER_TAG && event === LeftMenuPageReaction.EVENT_NAV_TO_CONFIGURATION {
                isCalledConfiguration = true
            } else if tag == LeftMenuPageReaction.ROUTER_TAG && event === LeftMenuPageReaction.EVENT_NAV_TO_ABOUT {
                isCalledAbout = true
            }
            
            expectationNavigateScreen.fulfill()
        }

        override func publishData<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {

            dummyTag = tag
            dummyEvent = event as? ActionSpec<Any>
            
            if tag == LeftMenuPageReaction.ROUTER_TAG && event === LeftMenuPageReaction.EVENT_PARAMS_CONFIGURATION {
                
                isCalledPublishDataSetting = true
                dummySettingsTransport = SettingsTransport.deserialize(from: values as? [String: Any])
                
            } else if tag == LeftMenuPageReaction.ROUTER_TAG && event === LeftMenuPageReaction.EVENT_PARAMS_ABOUT {
                
                isCalledPublishDataAbout = true
                dummyValueEventAbout = AboutTransport.deserialize(from: values as? [String: Any])
                
            } else if tag == LeftMenuPageReaction.ROUTER_TAG && event === LeftMenuPageReaction.EVENT_PARAMS_OPTIONS_ABOUT {
                
                isCalledPublishDataAboutOptions = true
                dummyValueEventOptionsAbout = AboutOptionsTransport.deserialize(from: values as? [String: Any])
            }
            
            expectationPublishData.fulfill()
        }
    }

    class DummyTrackerHelper: TrackerHelper {

        var isCalledTrackLogout = false

        override func trackLogoutSuccessEvent(_ pageName: String!) {
            isCalledTrackLogout = true
        }
    }

    class DummyPreferencesManager: PreferencesManager {

        var isCalledGetValueWithPreferencesKey = false
        var isCalledSaveValueWithPreferencesKey = false
        var isBalancesAvailable = false
        var isNotificationsAvailable = false
        var isVibrationEnabled = false
        var isTicketPromotionsEnabled = false
        var registerDeviceToken: String?
        var apnsToken: String?

        override func getValueWithPreferencesKey(forKey key: PreferencesManagerKeys) -> AnyObject? {

            isCalledGetValueWithPreferencesKey = true

            switch key {
            case PreferencesManagerKeys.kConfigBalancesAvailable:
                isBalancesAvailable = true
                return isBalancesAvailable as AnyObject
            case PreferencesManagerKeys.kConfigNotificationsAvailable:
                isNotificationsAvailable = true
                return isNotificationsAvailable as AnyObject
            case PreferencesManagerKeys.kConfigVibrationEnabled:
                isVibrationEnabled = false
                return isVibrationEnabled as AnyObject
            case PreferencesManagerKeys.kConfigTicketPromotionsEnabled:
                isTicketPromotionsEnabled = true
                return isTicketPromotionsEnabled as AnyObject
            case PreferencesManagerKeys.kRegisterDeviceToken:
                return registerDeviceToken as AnyObject
            case PreferencesManagerKeys.kApnsToken:
                return apnsToken as AnyObject
            default:
                break
            }
            
            return 0 as AnyObject
        }
    }
}
