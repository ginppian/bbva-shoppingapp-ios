//
//  LeftMenuPresenterATests.swift
//  shoppingappMXTest
//
//  Created by jesus.martinez on 26/12/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest
import CellsNativeComponents

#if TESTMX
@testable import shoppingappMX
#endif

class LeftMenuPresenterATests: XCTestCase {
    
    // MARK: vars
    var sut: LeftMenuPresenterA<LeftMenuDummyView>!
    var dummyBusManager: LeftMenuDummyBusManager!
    var dummyView = LeftMenuDummyView()
    var dummyPreferencesManager: DummyPreferencesManager!
    var dummyDeviceManager: DummyDeviceManager!
    
    // MARK: setup
    override func setUp() {
        super.setUp()

        dummyBusManager = LeftMenuDummyBusManager()
        sut = LeftMenuPresenterA<LeftMenuDummyView>(busManager: dummyBusManager)
        
        dummyView = LeftMenuDummyView()
        sut.view = dummyView
        
        dummyPreferencesManager = DummyPreferencesManager()
        PreferencesManager.instance = dummyPreferencesManager
        
        dummyDeviceManager = DummyDeviceManager()
        DeviceManager.instance = dummyDeviceManager
        
        sut.notificationManager = DummyNotificationManager()
    }

    // MARK: - leftMenuOptionPressed
    
    // MARK: - leftMenuOptionPressed MOBILE TOKEN
    
    func test_givenADisplayItemLeftMenuMobileToken_whenICallLeftMenuOptionPressed_thenICallViewCloseMenu() {
        
        // given
        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "title", option: .mobileToken)
        dummyView.softTokenSerialDummy = "tokenSerial"
        
        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)
        
        // then
        XCTAssert(dummyView.isCalledCloseMenu == true)
    }
    
    func test_givenADisplayItemLeftMenuMobileToken_whenICallLeftMenuOptionPressed_thenIOpenMobileTokenFlow() {
        // given
        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "title", option: .mobileToken)
        dummyView.softTokenSerialDummy = "tokenSerial"
        
        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)
        
        // then
        XCTAssert(dummyView.isOpenedMobileTokenFlow == true)
    }

    func test_givenADisplayItemLeftMenuMobileToken_whenICallLeftMenuOptionPressed_thenICallBusManagerNavigateToMobileToken() {

        // given
        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "title", option: .mobileToken)
        dummyView.softTokenSerialDummy = nil
        
        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)

        // then
        XCTAssert(dummyBusManager.isCalledNavigateMobileToken == true)
    }
    
    func test_givenADisplayItemLeftMenuMobileToken_whenICallLeftMenuOptionPressed_thenICallBusManagerPublishDataInEventParamsMobileToken() {

        // given
        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "title", option: .mobileToken)
        dummyView.softTokenSerialDummy = nil

        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)

        // then
        XCTAssert(dummyBusManager.isCalledPublishDataMobileToken == true)
    }
    
    func test_givenADisplayItemLeftMenuMobileTokenAndGlomoInstalled_whenICallLeftMenuOptionPressed_thenICallBusManagerPublishDataInEventParamsMobileTokenGlomo() {

        // given
        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "title", option: .mobileToken)
        let dummyPublicConfigurationDTO = MockPublicConfigurationDTO()
        dummyPublicConfigurationDTO.mockIsGlomoInstalled = true
        sut.publicConfiguration = dummyPublicConfigurationDTO
        dummyView.softTokenSerialDummy = nil
        
        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)

        // then
        XCTAssert(dummyBusManager.dummyValueEventMobileToken == AppInstalledTransport(appName: AppInstalledTransport.glomo))
    }
    
    func test_givenADisplayItemLeftMenuMobileTokenAndBancomerInstalled_whenICallLeftMenuOptionPressed_thenICallBusManagerPublishDataInEventParamsMobileTokenBancomer() {

        // given
        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "title", option: .mobileToken)
        let dummyPublicConfigurationDTO = MockPublicConfigurationDTO()
        dummyPublicConfigurationDTO.mockIsGlomoInstalled = false
        dummyPublicConfigurationDTO.mockIsBancomerInstalled = true
        sut.publicConfiguration = dummyPublicConfigurationDTO
        dummyView.softTokenSerialDummy = nil

        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)

        // then
        XCTAssert(dummyBusManager.dummyValueEventMobileToken == AppInstalledTransport(appName: AppInstalledTransport.bancomer))
    }
    
    func test_givenADisplayItemLeftMenuMobileTokenAndNoGlomoNeitherBancomerInstalled_whenICallLeftMenuOptionPressed_thenICallBusManagerPublishDataInEventParamsMobileTokenStore() {

        // given
        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "title", option: .mobileToken)
        let dummyPublicConfigurationDTO = MockPublicConfigurationDTO()
        dummyPublicConfigurationDTO.mockIsGlomoInstalled = false
        dummyPublicConfigurationDTO.mockIsBancomerInstalled = false
        sut.publicConfiguration = dummyPublicConfigurationDTO
        dummyView.softTokenSerialDummy = nil

        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)

        // then
        XCTAssert(dummyBusManager.dummyValueEventMobileToken == AppInstalledTransport(appName: AppInstalledTransport.store))
    }
    
    // MARK: - leftMenuOptionPressed CALL
    
    func test_givenDisplayItemLeftMenuCall_whenICallLeftMenuOptionPressedWithDisplayItem_thenICallViewShowCallBBVA() {
        
        // given
        let dummyView = LeftMenuDummyView()
        sut.view = dummyView
        
        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "title", option: .call)
        
        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)
        
        // then
        XCTAssert(dummyView.isCalledShowCallBBVA == true)
    }
    
    func test_givenDisplayItemLeftMenuCall_whenICallLeftMenuOptionPressedWithDisplayItem_thenICallViewShowCallBBVAWithSettingsTelephone() {
        
        // given
        let dummyView = LeftMenuDummyView()
        sut.view = dummyView
        
        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "title", option: .call)
        
        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)
        
        // then
        XCTAssert(dummyView.telephone == Settings.Global.default_bbva_phone_number)
    }
    
    func test_givenDisplayItemLeftMenuCall_whenICallLeftMenuOptionPressedWithDisplayItem_thenINotCallViewCloseMenu() {
        
        // given
        let dummyView = LeftMenuDummyView()
        sut.view = dummyView
        
        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "title", option: .call)
        
        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)
        
        // then
        XCTAssert(dummyView.isCalledCloseMenu == false)
    }
    
    func test_givenDisplayItemLeftMenuCall_whenICallLeftMenuOptionPressedWithDisplayItem_thenINotCallRouterNavigateToHelpScreen() {
        
        // given
        let dummyView = LeftMenuDummyView()
        sut.view = dummyView
        
        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "title", option: .call)
        
        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)
        
        // then
        XCTAssert(dummyBusManager.isCalledShowHelp == false)
    }
    
    func test_givenDisplayItemLeftMenuCall_whenICallLeftMenuOptionPressedWithDisplayItem_thenINotCallViewShowBankingMobile() {
        
        // given
        let dummyView = LeftMenuDummyView()
        sut.view = dummyView
        
        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "title", option: .call)
        
        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)
        
        // then
        XCTAssert(dummyView.isCalledShowBankingMobile == false)
    }
    
    // MARK: - leftMenuOptionPressed HELP
    
    func test_givenDisplayItemLeftMenuHelp_whenICallLeftMenuOptionPressedWithDisplayItem_thenIDoNotCallViewShowCallBBVA() {
        
        // given
        let dummyView = LeftMenuDummyView()
        sut.view = dummyView
        
        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "title", option: .help)
        
        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)
        
        // then
        XCTAssert(dummyView.isCalledShowCallBBVA == false)
    }
    
    func test_givenDisplayItemLeftMenuHelp_whenICallLeftMenuOptionPressedWithDisplayItem_thenICallViewCloseMenu() {
        
        // given
        let dummyView = LeftMenuDummyView()
        sut.view = dummyView
        
        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "title", option: .help)
        
        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)
        
        // then
        XCTAssert(dummyView.isCalledCloseMenu == true)
    }
    
    func test_givenDisplayItemLeftMenuHelp_whenICallLeftMenuOptionPressedWithDisplayItem_thenICallRouterNavigateToHelpScreen() {
        
        // given
        let dummyView = LeftMenuDummyView()
        sut.view = dummyView
        
        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "title", option: .help)
        
        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)
        
        // then
        XCTAssert(dummyBusManager.isCalledShowHelp == true)
    }
    
    func test_givenDisplayItemLeftMenuHelp_whenICallLeftMenuOptionPressedWithDisplayItem_thenINotCallViewShowBankingMobile() {
        
        // given
        let dummyView = LeftMenuDummyView()
        sut.view = dummyView
        
        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "title", option: .help)
        
        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)
        
        // then
        XCTAssert(dummyView.isCalledShowBankingMobile == false)
    }
    
    // MARK: - leftMenuOptionPressed WEB
    
    func test_givenDisplayItemLeftMenuWeb_whenICallLeftMenuOptionPressedWithDisplayItem_thenIDoNotCallViewShowCallBBVA() {
        
        // given
        let dummyView = LeftMenuDummyView()
        sut.view = dummyView
        
        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "title", option: .web)
        
        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)
        
        // then
        XCTAssert(dummyView.isCalledShowCallBBVA == false)
    }
    
    func test_givenDisplayItemLeftMenuWeb_whenICallLeftMenuOptionPressedWithDisplayItem_thenICallViewShowBankingMobile() {
        
        // given
        let dummyView = LeftMenuDummyView()
        sut.view = dummyView
        
        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "title", option: .web)
        
        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)
        
        // then
        XCTAssert(dummyView.isCalledShowBankingMobile == true)
    }
    
    // MARK: - leftMenuOptionPressed UNKNOWM
    
    func test_givenDisplayItemLeftMenuUnknowm_whenICallLeftMenuOptionPressedWithDisplayItem_thenICallViewShowCallBBVA() {
        
        // given
        let dummyView = LeftMenuDummyView()
        sut.view = dummyView
        
        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "title", option: .unknowm)
        
        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)
        
        // then
        XCTAssert(dummyView.isCalledShowCallBBVA == false)
    }
    
    func test_givenDisplayItemLeftMenuUnknowm_whenICallLeftMenuOptionPressedWithDisplayItem_thenINotCallViewShowBankingMobile() {
        
        // given
        let dummyView = LeftMenuDummyView()
        sut.view = dummyView
        
        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "title", option: .unknowm)
        
        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)
        
        // then
        XCTAssert(dummyView.isCalledShowBankingMobile == false)
    }
    
    // MARK: - leftMenuOptionPressed SETTINGS
    
    func test_givenDisplayItemLeftMenuSettings_whenICallLeftMenuOptionPressedWithDisplayItem_thenIDoNotCallViewShowCallBBVA() {
        
        // given
        let dummyView = LeftMenuDummyView()
        sut.view = dummyView
        
        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "title", option: .settings)
        
        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)
        
        // then
        XCTAssert(dummyView.isCalledShowCallBBVA == false)
    }
    
    func test_givenDisplayItemLeftMenuSettings_whenICallLeftMenuOptionPressedWithDisplayItem_thenINotCallViewShowBankingMobile() {
        
        // given
        let dummyView = LeftMenuDummyView()
        sut.view = dummyView
        
        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "title", option: .settings)
        
        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)
        
        // then
        XCTAssert(dummyView.isCalledShowBankingMobile == false)
    }
    
    func test_givenDisplayItemLeftMenuSettings_whenICallLeftMenuOptionPressedWithDisplayItem_thenICallViewCloseMenu() {
        
        let dummyView = LeftMenuDummyView()
        sut.view = dummyView
        
        // given
        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "title", option: .settings)
        
        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)
        
        // then
        XCTAssert(dummyView.isCalledCloseMenu == true)
    }
    
    func test_givenDisplayItemLeftMenuSettingsAndDisplaySettingTransport_whenICallLeftMenuOptionPressedWithDisplayItem_thenIPublishTheSameSettingTransport() {
        
        let dummyView = LeftMenuDummyView()
        sut.view = dummyView
        
        // given
        
        (sut.notificationManager as! DummyNotificationManager).notificationsIsAuthorized = [true]
        
        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "settings", option: .settings)
        
        let keyBalancesAvailable = PreferencesManagerKeys.kConfigBalancesAvailable
        _ = dummyPreferencesManager.getValueWithPreferencesKey(forKey: keyBalancesAvailable)
        
        let keyVibrationEnabled = PreferencesManagerKeys.kConfigVibrationEnabled
        _ = dummyPreferencesManager.getValueWithPreferencesKey(forKey: keyVibrationEnabled)
        
        let keyTicketPromotionsEnabled = PreferencesManagerKeys.kConfigTicketPromotionsEnabled
        _ = dummyPreferencesManager.getValueWithPreferencesKey(forKey: keyTicketPromotionsEnabled)
        
        let settingsTransport = SettingsTransport(isNotificationsAvailable: true, isBalancesAvailable: dummyPreferencesManager.isBalancesAvailable, isVibrationEnabled: dummyPreferencesManager.isVibrationEnabled, isTicketPromotionsEnabled: dummyPreferencesManager.isTicketPromotionsEnabled)
        
        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)
        
        // then
        XCTAssert(settingsTransport.isBalancesAvailable == dummyPreferencesManager.isBalancesAvailable)
        XCTAssert(settingsTransport.isNotificationsAvailable == true)
        XCTAssert(settingsTransport.isVibrationEnabled == dummyPreferencesManager.isVibrationEnabled)
        XCTAssert(settingsTransport.isTicketPromotionsEnabled == dummyPreferencesManager.isTicketPromotionsEnabled)
    }
    
    func test_givenDisplayItemLeftMenuSettings_whenICallLeftMenuOptionPressedWithDisplayItem_thenICallGetValueWithPreferencesKey() {
        
        let dummyView = LeftMenuDummyView()
        sut.view = dummyView
        
        // given
        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "settings", option: .settings)
        
        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)
        
        // then
        wait(for: [dummyBusManager.expectationNavigateScreen, dummyBusManager.expectationPublishData], timeout: 10)
        XCTAssert(dummyPreferencesManager.isCalledGetValueWithPreferencesKey == true)
    }
    
    func test_givenDisplayItemLeftMenuSettings_whenICallLeftMenuOptionPressedWithDisplayItem_thenICallBusShowConfiguration() {
        
        let dummyView = LeftMenuDummyView()
        sut.view = dummyView
        
        //given
        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "settings", option: .settings)
        
        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)
        
        // then
        wait(for: [dummyBusManager.expectationNavigateScreen, dummyBusManager.expectationPublishData], timeout: 10)
        XCTAssert(dummyBusManager.isCalledConfiguration == true)
    }
    
    func test_givenDisplayItemLeftMenuSettingsAndTokenNotMatchWithRegisteredToken_whenICallLeftMenuOptionPressedWithDisplayItem_thenICallBusPublishWithTheSameSettingsTransport() {
        
        let dummyView = LeftMenuDummyView()
        sut.view = dummyView
        
        //given
        (sut.notificationManager as! DummyNotificationManager).notificationsIsAuthorized = [true]
        
        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "settings", option: .settings)
        
        dummyPreferencesManager.registerDeviceToken = "token"
        dummyPreferencesManager.apnsToken = "1234"
        
        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)
        
        //then
        let settingsTransport = SettingsTransport(isNotificationsAvailable: false, isBalancesAvailable: dummyPreferencesManager.isBalancesAvailable, isVibrationEnabled: dummyPreferencesManager.isVibrationEnabled, isTicketPromotionsEnabled: dummyPreferencesManager.isTicketPromotionsEnabled)
        
        wait(for: [dummyBusManager.expectationNavigateScreen, dummyBusManager.expectationPublishData], timeout: 10)
        XCTAssert(dummyBusManager.isCalledPublishDataSetting == true)
        XCTAssert(dummyBusManager.dummySettingsTransport == settingsTransport)
    }
    
    func test_givenDisplayItemLeftMenuSettingsAndTokenMatchWithRegisteredToken_whenICallLeftMenuOptionPressedWithDisplayItem_thenICallBusPublishWithTheSameSettingsTransport() {
        
        let dummyView = LeftMenuDummyView()
        sut.view = dummyView
        
        //given
        (sut.notificationManager as! DummyNotificationManager).notificationsIsAuthorized = [true]
        
        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "settings", option: .settings)
        
        let token = "token"
        dummyPreferencesManager.registerDeviceToken = token
        dummyPreferencesManager.apnsToken = token
        
        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)
        
        //then
        let settingsTransport = SettingsTransport(isNotificationsAvailable: true, isBalancesAvailable: dummyPreferencesManager.isBalancesAvailable, isVibrationEnabled: dummyPreferencesManager.isVibrationEnabled, isTicketPromotionsEnabled: dummyPreferencesManager.isTicketPromotionsEnabled)
        
        wait(for: [dummyBusManager.expectationNavigateScreen, dummyBusManager.expectationPublishData], timeout: 10)
        XCTAssert(dummyBusManager.isCalledPublishDataSetting == true)
        XCTAssert(dummyBusManager.dummySettingsTransport == settingsTransport)
    }
    
    // MARK: - leftMenuOptionPressed ABOUT
    
    func test_givenDisplayItemLeftMenuSettingsAndDisplayAboutTransport_whenICallLeftMenuOptionPressedWithDisplayItem_thenICallBusPublishWithTheSameAbout() {
        
        let dummyView = LeftMenuDummyView()
        sut.view = dummyView
        
        //given
        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "title", option: .about)
        
        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)
        
        //then
        XCTAssert(dummyBusManager.isCalledPublishDataAbout == true)
        XCTAssert(dummyBusManager.dummyValueEventAbout.deviceModel == dummyDeviceManager.modelName)
        XCTAssert(dummyBusManager.dummyValueEventAbout.appVersion == dummyDeviceManager.bundleVersion)
        XCTAssert(dummyBusManager.dummyValueEventAbout.operatingSystem == dummyDeviceManager.operatingSystem)
    }
    
    func test_givenDisplayItemLeftMenuSettingsAndDisplayAboutTransport_whenICallLeftMenuOptionPressedWithDisplayItem_thenICallBusPublishOptionsWithTheSameAbout() {
        
        let dummyView = LeftMenuDummyView()
        sut.view = dummyView
        
        //given
        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "title", option: .about)
        
        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)
        
        //then
        XCTAssert(dummyBusManager.isCalledPublishDataAboutOptions == true)
        XCTAssert(dummyBusManager.dummyValueEventOptionsAbout.isLegalAdviceLinkHidden == false)
        XCTAssert(dummyBusManager.dummyValueEventOptionsAbout.isRateLinkHidden == true)
        XCTAssert(dummyBusManager.dummyValueEventOptionsAbout.isShareLinkHidden == false)
        XCTAssert(dummyBusManager.dummyValueEventOptionsAbout.isVersionLinkHidden == false)
    }
    
    func test_givenDisplayItemLeftMenuSettings_whenICallLeftMenuOptionPressedWithDisplayItem_thenICallBusShowAbout() {
        
        let dummyView = LeftMenuDummyView()
        sut.view = dummyView
        
        //given
        let dummyDisplayItemLeftMenu = DisplayItemLeftMenu(image: "image", title: "title", option: .about)
        
        // when
        sut.leftMenuOptionPressed(withDisplayItem: dummyDisplayItemLeftMenu)
        
        // then
        XCTAssert(dummyBusManager.isCalledAbout == true)
    }
    
    // MARK: - dummy data
    
    class LeftMenuDummyView: LeftMenuViewProtocolA {

        var isCalledSetDisplayData = false
        var isCalledUpdateOptionsMenu = false
        var isCalledCloseMenu = false
        var isCalledPresentLoginAndCloseSession = false
        var isCalledShowCallBBVA = false
        var displayItemLeftMenu: DisplayItemLeftMenu?
        var isCalledShowBankingMobile = false
        var isOpenedMobileTokenFlow = false
        
        var telephone = ""
        var softTokenSerialDummy: String?
        
        func setDisplayData(displayData: [DisplayItemLeftMenu]) {
            isCalledSetDisplayData = true
        }
        
        func updateOptionsMenu(isUserLogged: Bool) {
            isCalledUpdateOptionsMenu = true
        }
        
        func presentLoginAndCloseSession() {
            isCalledPresentLoginAndCloseSession = true
        }
        
        func closeMenu() {
            isCalledCloseMenu = true
        }
        
        func showError(error: ModelBO) {
        }
        
        func changeColorEditTexts() {
        }
        
        func showCallBBVA(withTel tel: String) {
            isCalledShowCallBBVA = true
            telephone = tel
        }
        
        func showBankingMobile() {
            isCalledShowBankingMobile = true
        }
        
        func openMobileTokenFlow() {
            isOpenedMobileTokenFlow = true
        }
        
        var softTokenSerial: String? {
            return softTokenSerialDummy
        }
    }
    
    class LeftMenuDummyBusManager: BusManager {
        
        var isCalledSetRootWhenNoSession = false
        var isCalledShowHelp = false
        var isCalledShowSettings = false
        var isCalledPublishDataSetting = false
        var isCalledPublishDataAbout = false
        var isCalledPublishDataAboutOptions = false
        var isCalledConfiguration = false
        var isCalledAbout = false
        var isCalledNavigateMobileToken = false
        var isCalledPublishDataMobileToken = false
        
        var dummyTag: String = ""
        var dummyEvent: ActionSpec<Any>?
        var dummySettingsTransport: SettingsTransport?
        var dummyValueEventAbout: AboutTransport!
        var dummyValueEventOptionsAbout: AboutOptionsTransport!
        var dummyValueEventMobileToken = AppInstalledTransport(appName: "dummy")
        
        let expectationNavigateScreen = XCTestExpectation(description: "Checking notifications authorization")
        let expectationPublishData = XCTestExpectation(description: "Checking notifications authorization")
        
        override init() {
            super.init()
            routerFactory = RouterInitFactory()
        }
        
        override func setRootWhenNoSession() {
            isCalledSetRootWhenNoSession = true
        }
        
        override func navigateScreen<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {
            
            if tag == LeftMenuPageReaction.ROUTER_TAG && event === LeftMenuPageReaction.EVENT_NAV_TO_HELP {
                isCalledShowHelp = true
            } else if tag == LeftMenuPageReaction.ROUTER_TAG && event === LeftMenuPageReaction.EVENT_NAV_TO_CONFIGURATION {
                isCalledConfiguration = true
            } else if tag == LeftMenuPageReaction.ROUTER_TAG && event === LeftMenuPageReaction.EVENT_NAV_TO_ABOUT {
                isCalledAbout = true
            } else if tag == LeftMenuPageReaction.ROUTER_TAG && event === LeftMenuPageReactionA.eventNavMobileToken {
                isCalledNavigateMobileToken = true
            }
            
            expectationNavigateScreen.fulfill()
        }
        
        override func publishData<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {
            
            dummyTag = tag
            dummyEvent = event as? ActionSpec<Any>
            
            if tag == LeftMenuPageReaction.ROUTER_TAG && event === LeftMenuPageReaction.EVENT_PARAMS_CONFIGURATION {
                
                isCalledPublishDataSetting = true
                dummySettingsTransport = SettingsTransport.deserialize(from: values as? [String: Any])
                
            } else if tag == LeftMenuPageReaction.ROUTER_TAG && event === LeftMenuPageReaction.EVENT_PARAMS_ABOUT {
                
                isCalledPublishDataAbout = true
                dummyValueEventAbout = AboutTransport.deserialize(from: values as? [String: Any])
                
            } else if tag == LeftMenuPageReaction.ROUTER_TAG && event === LeftMenuPageReaction.EVENT_PARAMS_OPTIONS_ABOUT {
                
                isCalledPublishDataAboutOptions = true
                dummyValueEventOptionsAbout = AboutOptionsTransport.deserialize(from: values as? [String: Any])
            } else if tag == LeftMenuPageReactionA.ROUTER_TAG && event === LeftMenuPageReactionA.eventParamsMobileToken {
                
                isCalledPublishDataMobileToken = true
                dummyValueEventMobileToken = AppInstalledTransport.deserialize(from: values as? [String: Any]) ?? AppInstalledTransport(appName: "dummy")
            }
            
            expectationPublishData.fulfill()
        }
    }
    
    class DummyPreferencesManager: PreferencesManager {
        
        var isCalledGetValueWithPreferencesKey = false
        var isCalledSaveValueWithPreferencesKey = false
        var isBalancesAvailable = false
        var isNotificationsAvailable = false
        var isVibrationEnabled = false
        var isTicketPromotionsEnabled = false
        var registerDeviceToken: String?
        var apnsToken: String?
        
        override func getValueWithPreferencesKey(forKey key: PreferencesManagerKeys) -> AnyObject? {
            
            isCalledGetValueWithPreferencesKey = true
            
            switch key {
            case PreferencesManagerKeys.kConfigBalancesAvailable:
                isBalancesAvailable = true
                return isBalancesAvailable as AnyObject
            case PreferencesManagerKeys.kConfigNotificationsAvailable:
                isNotificationsAvailable = true
                return isNotificationsAvailable as AnyObject
            case PreferencesManagerKeys.kConfigVibrationEnabled:
                isVibrationEnabled = false
                return isVibrationEnabled as AnyObject
            case PreferencesManagerKeys.kConfigTicketPromotionsEnabled:
                isTicketPromotionsEnabled = true
                return isTicketPromotionsEnabled as AnyObject
            case PreferencesManagerKeys.kRegisterDeviceToken:
                return registerDeviceToken as AnyObject
            case PreferencesManagerKeys.kApnsToken:
                return apnsToken as AnyObject
            default:
                break
            }
            
            return 0 as AnyObject
        }
    }
    
}
