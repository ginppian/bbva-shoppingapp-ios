//
//  ShoppingFilterTransportTests.swift
//  shoppingapp
//
//  Created by jesus.martinez on 21/2/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class ShoppingFilterTransportTests: XCTestCase {

    let sut = ShoppingFilter()

    // MARK: - isEmtpy

    func test_givenNoTextFilterNeitherPromotionTypeFilterNeitherPromotionUsageTypeNeitherCategoriesFilter_whenICallIsEmpty_thenReturnTrue() {

        // given
        sut.textFilter = nil
        sut.promotionTypeFilter = nil
        sut.promotionUsageTypeFilter = nil
        sut.categoriesFilter = nil

        // when
        let result = sut.isEmpty()

        // then
        XCTAssert(result == true)

    }

    func test_givenTextFilterEmptyStringPromotionTypeFilterIsEmptyAndPromotionUsageTypeFilterIsEmptyNeitherCategoriesEmpty_whenICallIsEmpty_thenReturnTrue() {

        // given
        sut.textFilter = ""
        sut.promotionTypeFilter = [PromotionType]()
        sut.promotionUsageTypeFilter = [PromotionUsageType]()
        sut.categoriesFilter = [CategoryType]()

        // when
        let result = sut.isEmpty()

        // then
        XCTAssert(result == true)

    }

    func test_givenTextFilterValueOthersNil_whenICallIsEmpty_thenReturnFalse() {

        // given
        sut.promotionTypeFilter = nil
        sut.promotionUsageTypeFilter = nil
        sut.textFilter = "value"

        // when
        let result = sut.isEmpty()

        // then
        XCTAssert(result == false)

    }

    func test_givenPromotionTypeFilterHasAValueOthersNil_whenICallIsEmpty_thenReturnFalse() {

        // given
        sut.textFilter = nil
        sut.promotionUsageTypeFilter = nil
        sut.promotionTypeFilter = [.discount]

        // when
        let result = sut.isEmpty()

        // then
        XCTAssert(result == false)

    }

    func test_givenPromotionUsageTypeFilterHasAValueOthersNil_whenICallIsEmpty_thenReturnFalse() {

        // given
        sut.textFilter = nil
        sut.promotionTypeFilter = nil
        sut.promotionUsageTypeFilter = [.online]

        // when
        let result = sut.isEmpty()

        // then
        XCTAssert(result == false)

    }

    func test_givenTextFilterValueOthersEmpty_whenICallIsEmpty_thenReturnFalse() {

        // given
        sut.promotionTypeFilter = [PromotionType]()
        sut.promotionUsageTypeFilter = [PromotionUsageType]()
        sut.textFilter = "value"

        // when
        let result = sut.isEmpty()

        // then
        XCTAssert(result == false)

    }

    func test_givenPromotionTypeFilterHasAValueOthersEmpty_whenICallIsEmpty_thenReturnFalse() {

        // given
        sut.textFilter = ""
        sut.promotionUsageTypeFilter = [PromotionUsageType]()
        sut.promotionTypeFilter = [.discount]

        // when
        let result = sut.isEmpty()

        // then
        XCTAssert(result == false)

    }

    func test_givenPromotionUsageTypeFilterHasAValueOthersEmpty_whenICallIsEmpty_thenReturnFalse() {

        // given
        sut.textFilter = ""
        sut.promotionTypeFilter = [PromotionType]()
        sut.promotionUsageTypeFilter = [.online]

        // when
        let result = sut.isEmpty()

        // then
        XCTAssert(result == false)

    }

    func test_givenCategoriesFilterHasAValueOthersEmpty_whenICallIsEmpty_thenReturnFalse() {

        // given
        sut.textFilter = ""
        sut.promotionTypeFilter = [PromotionType]()
        sut.promotionUsageTypeFilter = [PromotionUsageType]()
        sut.categoriesFilter = [.activities]

        // when
        let result = sut.isEmpty()

        // then
        XCTAssert(result == false)

    }

    // MARK: add promotionType:

    func test_givenPromotionTypeAndPromotionTypeFilterNil_whenICallAddPromotion_thenPromotionTypeFilterShouldBeOneAndContainTheNewType() {

        // given
        sut.promotionTypeFilter = nil

        // when
        sut.add(promotionType: .discount)

        // then
        XCTAssert(sut.promotionTypeFilter?.count == 1)
        XCTAssert(sut.promotionTypeFilter!.contains(.discount) == true)

    }

    func test_givenPromotionTypeAndPromotionTypeFilterEmpty_whenICallAddPromotion_thenPromotionTypeFilterShouldBeOneAndContainTheNewType() {

        // given
        sut.promotionTypeFilter = [PromotionType]()

        // when
        sut.add(promotionType: .discount)

        // then
        XCTAssert(sut.promotionTypeFilter?.count == 1)
        XCTAssert(sut.promotionTypeFilter!.contains(.discount) == true)

    }

    func test_givenPromotionTypeAndPromotionTypeFilterWithTheSamePromtionType_whenICallAddPromotion_thenPromotionTypeFilterShouldBeOneAndShouldNotRepeatTheNewOne() {

        // given
        sut.promotionTypeFilter = [.discount]

        // when
        sut.add(promotionType: .discount)

        // then
        XCTAssert(sut.promotionTypeFilter?.count == 1)
        XCTAssert(sut.promotionTypeFilter!.contains(.discount) == true)

    }

    // MARK: - add promotionUsageType:

    func test_givenPromotionTypeAndPromotionTypeFilterNil_whenICallAddPromotionUsageType_thenPromotionTypeFilterShouldBeOneAndContainTheNewType() {

        // given
        sut.promotionUsageTypeFilter = nil

        // when
        sut.add(promotionUsageType: .online)

        // then
        XCTAssert(sut.promotionUsageTypeFilter?.count == 1)
        XCTAssert(sut.promotionUsageTypeFilter!.contains(.online) == true)

    }

    func test_givenPromotionTypeAndPromotionTypeFilterEmpty_whenICallAddPromotionUsageType_thenPromotionTypeFilterShouldBeOneAndContainTheNewType() {

        // given
        sut.promotionUsageTypeFilter = [PromotionUsageType]()

        // when
        sut.add(promotionUsageType: .online)

        // then
        XCTAssert(sut.promotionUsageTypeFilter?.count == 1)
        XCTAssert(sut.promotionUsageTypeFilter!.contains(.online) == true)

    }

    func test_givenPromotionTypeAndPromotionTypeFilterWithTheSamePromtionType_whenICallAddPromotionUsageType_thenPromotionTypeFilterShouldBeOneAndShouldNotRepeatTheNewOne() {

        // given
        sut.promotionUsageTypeFilter = [.online]

        // when
        sut.add(promotionUsageType: .online)

        // then
        XCTAssert(sut.promotionUsageTypeFilter?.count == 1)
        XCTAssert(sut.promotionUsageTypeFilter!.contains(.online) == true)

    }

    // MARK: - reset

    func test_givenAny_whenICallReset_thenTextFilterShouldBeNil() {

        // given

        // when
        sut.reset()

        // then
        XCTAssert(sut.textFilter == nil)

    }

    func test_givenAny_whenICallReset_thenPromotionTypeFilterShouldBeNil() {

        // given

        // when
        sut.reset()

        // then
        XCTAssert(sut.promotionTypeFilter == nil)

    }

    func test_givenAny_whenICallReset_thenPromotionUsageTypeFilterShouldBeNil() {

        // given

        // when
        sut.reset()

        // then
        XCTAssert(sut.promotionUsageTypeFilter == nil)

    }

    func test_givenAny_whenICallReset_thenCategoriesFilterShouldBeNil() {

        // given

        // when
        sut.reset()

        // then
        XCTAssert(sut.categoriesFilter == nil)

    }

    // MARK: - removePromotionType

    func test_givenAPromotionTypeFilter_whenICallRemovePromotionType_thenThereLessPromotionTypeFiltersAndDoNotContainsTheTypeRemoved() {

        // given
        sut.promotionTypeFilter = [.discount, .point]

        // when
        sut.remove(promotionType: .discount)

        // then
        XCTAssert(sut.promotionTypeFilter?.count == 1)
        XCTAssert(sut.promotionTypeFilter!.contains(.discount) == false)

    }

    func test_givenAPromotionTypeFilterNotContained_whenICallRemovePromotionType_thenThereAreTheSameNumberOfPromotionTypesThanBefore() {

        // given
        sut.promotionTypeFilter = [.discount, .point]

        // when
        sut.remove(promotionType: .toMonths)

        // then
        XCTAssert(sut.promotionTypeFilter?.count == sut.promotionTypeFilter!.count)

    }

    func test_givenAPromotionTypeFilterNil_whenICallRemovePromotionType_thenContinueNilAndDoNotCrash() {

        // given
        sut.promotionTypeFilter = nil

        // when
        sut.remove(promotionType: .discount)

        // then
        XCTAssert(sut.promotionTypeFilter == nil)

    }

    // MARK: - removePromotionUsageType

    func test_givenAPromotionUsageTypeFilter_whenICallRemovePromotionUsageType_thenThereLessPromotionUsageTypeFiltersAndDoNotContainsTheTypeRemoved() {

        // given
        sut.promotionUsageTypeFilter = [.online, .physical]

        // when
        sut.remove(promotionUsageType: .physical)

        // then
        XCTAssert(sut.promotionUsageTypeFilter?.count == 1)
        XCTAssert(sut.promotionUsageTypeFilter!.contains(.physical) == false)

    }

    func test_givenAPromotionUsageTypeFilterNotContained_whenICallRemovePromotionUsageType_thenThereAreTheSameNumberOfPromotionUsageTypesThanBefore() {

        // given
        sut.promotionUsageTypeFilter = [.online, .physical]

        // when
        sut.remove(promotionUsageType: .unknown)

        // then
        XCTAssert(sut.promotionUsageTypeFilter?.count == sut.promotionUsageTypeFilter!.count)

    }

    func test_givenAPromotionUsageTypeFilterNil_whenICallRemovePromotionUsageType_thenContinueNilAndDoNotCrash() {

        // given
        sut.promotionUsageTypeFilter = nil

        // when
        sut.remove(promotionUsageType: .physical)

        // then
        XCTAssert(sut.promotionUsageTypeFilter == nil)

    }

    // MARK: - add categoryType:

    func test_givenCategoryTypeAndCategoriesFilterNil_whenICallAddCategoryType_thenCategoriesFilterShouldBeOneAndContainTheNewType() {

        // given
        sut.categoriesFilter = nil

        // when
        sut.add(categoryType: .activities)

        // then
        XCTAssert(sut.categoriesFilter?.count == 1)
        XCTAssert(sut.categoriesFilter!.contains(.activities) == true)

    }

    func test_givenCategoryTypeAndCategoriesFilterEmpty_whenICallAddCategoryType_thenCategorieseFilterShouldBeOneAndContainTheNewType() {

        // given
        sut.categoriesFilter = [CategoryType]()

        // when
        sut.add(categoryType: .activities)

        // then
        XCTAssert(sut.categoriesFilter?.count == 1)
        XCTAssert(sut.categoriesFilter!.contains(.activities) == true)

    }

    func test_givenCategoryTypeAndCategoriesFilterWithTheSamePromtionType_whenICallAddCategoryType_thenCategoriesFilterShouldBeOneAndShouldNotRepeatTheNewOne() {

        // given
        sut.categoriesFilter = [.activities]

        // when
        sut.add(categoryType: .activities)

        // then
        XCTAssert(sut.categoriesFilter?.count == 1)
        XCTAssert(sut.categoriesFilter!.contains(.activities) == true)

    }

    // MARK: - removecategoryType

    func test_givenACategoryTypeAndCategoriesFilter_whenICallRemovecategoryType_thenThereLessCategoriesFiltersAndDoNotContainsTheTypeRemoved() {

        // given
        sut.categoriesFilter = [.activities, .auto]

        // when
        sut.remove(categoryType: .activities)

        // then
        XCTAssert(sut.categoriesFilter?.count == 1)
        XCTAssert(sut.categoriesFilter!.contains(.activities) == false)

    }

    func test_givenACategoryTypeAndCategoriesFilterNotContained_whenICallRemovecategoryType_thenThereAreTheSameNumberOfCategoriesThanBefore() {

        // given
        sut.categoriesFilter = [.activities, .auto]

        // when
        sut.remove(categoryType: .unknown)

        // then
        XCTAssert(sut.categoriesFilter?.count == sut.categoriesFilter!.count)

    }

    func test_givenACategoryTypeAndCategoriesFilterNil_whenICallRemovecategoryType_thenContinueNilAndDoNotCrash() {

        // given
        sut.categoriesFilter = nil

        // when
        sut.remove(categoryType: .activities)

        // then
        XCTAssert(sut.categoriesFilter == nil)

    }

    // MARK: - retrieveServiceParameters

    func test_givenAllPropertiesNil_whenICallRetrieveServiceParameters_thenParametersIsEmpty() {

        // given
        sut.textFilter = nil
        sut.promotionTypeFilter = nil
        sut.promotionUsageTypeFilter = nil
        sut.categoriesFilter = nil

        // when
        let result = sut.retrieveServiceParameters()

        // then
        XCTAssert(result.isEmpty == true)

    }

    func test_givenAllPropertiesEmpty_whenICallRetrieveServiceParameters_thenParametersIsEmpty() {

        // given
        sut.textFilter = ""
        sut.promotionTypeFilter = [PromotionType]()
        sut.promotionUsageTypeFilter = [PromotionUsageType]()
        sut.categoriesFilter = [CategoryType]()

        // when
        let result = sut.retrieveServiceParameters()

        // then
        XCTAssert(result.isEmpty == true)

    }

    func test_givenATextFilter_whenICallRetrieveServiceParameters_thenParametersIsCorrect() {

        // given
        sut.textFilter = "text filter"

        // when
        let result = sut.retrieveServiceParameters()

        // then
        XCTAssert(result == "&commerceInformation.name=text filter")

    }

    func test_givenAPromotionUsageTypeFilter_whenICallRetrieveServiceParameters_thenParametersIsCorrect() {

        // given
        sut.promotionUsageTypeFilter = [.online]

        // when
        let result = sut.retrieveServiceParameters()

        // then
        XCTAssert(result == "&usageTypes.id=in=(\(PromotionUsageType.online.rawValue))")

    }

    func test_givenAPromotionTypeFilter_whenICallRetrieveServiceParameters_thenParametersIsCorrect() {

        // given
        sut.promotionTypeFilter = [.discount]

        // when
        let result = sut.retrieveServiceParameters()

        // then
        XCTAssert(result == "&promotionType.id=in=(\(PromotionType.discount.toService))")

    }

    func test_givenACategoriesFilter_whenICallRetrieveServiceParameters_thenParametersIsCorrect() {

        // given
        sut.categoriesFilter = [.activities]

        // when
        let result = sut.retrieveServiceParameters()

        // then
        XCTAssert(result == "&categories.id=in=(\(CategoryType.activities.rawValue))")

    }

    func test_givenATwoPromotionUsageTypeFilter_whenICallRetrieveServiceParameters_thenParametersIsCorrect() {

        // given
        sut.promotionUsageTypeFilter = [.online, .physical]

        // when
        let result = sut.retrieveServiceParameters()

        // then
        XCTAssert(result == "&usageTypes.id=in=(\(PromotionUsageType.online.rawValue),\(PromotionUsageType.physical.rawValue))")

    }

    func test_givenATwoPromotionTypeFilter_whenICallRetrieveServiceParameters_thenParametersIsCorrect() {

        // given
        sut.promotionTypeFilter = [.discount, .point]

        // when
        let result = sut.retrieveServiceParameters()

        // then
        XCTAssert(result == "&promotionType.id=in=(\(PromotionType.discount.toService),\(PromotionType.point.toService))")

    }

    func test_givenATwoCategoriesFilter_whenICallRetrieveServiceParameters_thenParametersIsCorrect() {

        // given
        sut.categoriesFilter = [.activities, .hobbies]

        // when
        let result = sut.retrieveServiceParameters()

        // then
        XCTAssert(result == "&categories.id=in=(\(CategoryType.activities.rawValue),\(CategoryType.hobbies.rawValue))")

    }

    func test_givenATextFilterAndPromotionTypeFilter_whenICallRetrieveServiceParameters_thenParametersIsCorrect() {

        // given
        sut.textFilter = "text filter"
        sut.promotionTypeFilter = [.discount]

        // when
        let result = sut.retrieveServiceParameters()

        // then
        XCTAssert(result == "&commerceInformation.name=text filter&promotionType.id=in=(\(PromotionType.discount.toService))")

    }

    func test_givenAPromotionTypeFilterAndTwoCategoriesFilter_whenICallRetrieveServiceParameters_thenParametersIsCorrect() {

        // given
        sut.promotionTypeFilter = [.discount]
        sut.categoriesFilter = [.activities, .hobbies]

        // when
        let result = sut.retrieveServiceParameters()

        // then
        XCTAssert(result == "&promotionType.id=in=(\(PromotionType.discount.toService))&categories.id=in=(\(CategoryType.activities.rawValue),\(CategoryType.hobbies.rawValue))")

    }

    func test_givenATextFilterAndPromotionTypeFilterAndTwoCategoriesFilterAndPromotionUsageTypeFilter_whenICallRetrieveServiceParameters_thenParametersIsCorrect() {

        // given
        sut.textFilter = "text filter"
        sut.promotionTypeFilter = [.discount]
        sut.categoriesFilter = [.activities, .hobbies]
        sut.promotionUsageTypeFilter = [.online]

        // when
        let result = sut.retrieveServiceParameters()

        // then
        XCTAssert(result == "&commerceInformation.name=text filter&usageTypes.id=in=(\(PromotionUsageType.online.rawValue))&promotionType.id=in=(\(PromotionType.discount.toService))&categories.id=in=(\(CategoryType.activities.rawValue),\(CategoryType.hobbies.rawValue))")

    }

    // MARK: - retrieveLiterals

    func test_givenCategoriesAndshoppingFilterEmtpy_whenICallRetrieveLiterals_thenReturnEmtpyArray() {

        // given
        let categoriesBO = PromotionsGenerator.getCategoryBO()

        // when
        let result = sut.retrieveLiterals(withCategories: categoriesBO)

        // then
        XCTAssert(result.isEmpty == true)

    }

    func test_givenCategoriesAndshoppingFilterWithTextEmpty_whenICallRetrieveLiterals_thenReturnEmtpyArray() {

        // given
        let categoriesBO = PromotionsGenerator.getCategoryBO()
        sut.textFilter = ""

        // when
        let result = sut.retrieveLiterals(withCategories: categoriesBO)

        // then
        XCTAssert(result.isEmpty == true)

    }

    func test_givenCategoriesAndshoppingFilterWithTextNotEmpty_whenICallRetrieveLiterals_thenConstainsOneItemAndTitleShouldMatchWithTextAndKeyWithTextFilterKey() {

        // given
        let categoriesBO = PromotionsGenerator.getCategoryBO()
        sut.textFilter = "textFilter"

        // when
        let result = sut.retrieveLiterals(withCategories: categoriesBO)

        // then
        XCTAssert(result.count == 1)
        XCTAssert(result[0].title == sut.textFilter)
        XCTAssert(result[0].key == ShoppingFilter.text_filter_key)

    }

    func test_givenCategoriesAndOnlyPromotionTypeUnknowm_whenICallRetrieveLiterals_thenReturnEmptyArray() {

        // given
        let categoriesBO = PromotionsGenerator.getCategoryBO()
        sut.promotionTypeFilter = [.unknown]

        // when
        let result = sut.retrieveLiterals(withCategories: categoriesBO)

        // then
        XCTAssert(result.isEmpty == true)

    }

    func test_givenCategoriesAndPromotionTypeDiscountGiftAndPoint_whenICallRetrieveLiterals_thenReturnShouldHaveThreeItemsAndShouldMatchTitleAndKey() {

        // given
        let categoriesBO = PromotionsGenerator.getCategoryBO()
        sut.promotionTypeFilter = [.discount, .point, .toMonths]

        // when
        let result = sut.retrieveLiterals(withCategories: categoriesBO)

        // then
        XCTAssert(result.count == 3)
        XCTAssert(result[0].title == sut.promotionTypeFilter![0].literal)
        XCTAssert(result[0].key == sut.promotionTypeFilter![0].rawValue)
        XCTAssert(result[1].title == sut.promotionTypeFilter![1].literal)
        XCTAssert(result[1].key == sut.promotionTypeFilter![1].rawValue)
        XCTAssert(result[2].title == sut.promotionTypeFilter![2].literal)
        XCTAssert(result[2].key == sut.promotionTypeFilter![2].rawValue)

    }

    func test_givenCategoriesAndPromotionTypeDiscountUnknowmAndPoint_whenICallRetrieveLiterals_thenReturnShouldHaveTwoItemsAndShouldMatchTitleAndKey() {

        // given
        let categoriesBO = PromotionsGenerator.getCategoryBO()
        sut.promotionTypeFilter = [.discount, .unknown, .toMonths]

        // when
        let result = sut.retrieveLiterals(withCategories: categoriesBO)

        // then
        XCTAssert(result.count == 2)
        XCTAssert(result[0].title == sut.promotionTypeFilter![0].literal)
        XCTAssert(result[0].key == sut.promotionTypeFilter![0].rawValue)
        XCTAssert(result[1].title == sut.promotionTypeFilter![2].literal)
        XCTAssert(result[1].key == sut.promotionTypeFilter![2].rawValue)

    }

    func test_givenCategoriesAndOnlyPromotionUsageTypeUnknowm_whenICallRetrieveLiterals_thenReturnEmptyArray() {

        // given
        let categoriesBO = PromotionsGenerator.getCategoryBO()
        sut.promotionUsageTypeFilter = [.unknown]

        // when
        let result = sut.retrieveLiterals(withCategories: categoriesBO)

        // then
        XCTAssert(result.isEmpty == true)

    }

    func test_givenCategoriesAndPromotionUsageTypeOnlineAndPhysical_whenICallRetrieveLiterals_thenReturnShouldHaveTwoItemsAndShouldMatchTitleAndKey() {

        // given
        let categoriesBO = PromotionsGenerator.getCategoryBO()
        sut.promotionUsageTypeFilter = [.online, .physical]

        // when
        let result = sut.retrieveLiterals(withCategories: categoriesBO)

        // then
        XCTAssert(result.count == 2)
        XCTAssert(result[0].title == sut.promotionUsageTypeFilter![0].literal)
        XCTAssert(result[0].key == sut.promotionUsageTypeFilter![0].rawValue)
        XCTAssert(result[1].title == sut.promotionUsageTypeFilter![1].literal)
        XCTAssert(result[1].key == sut.promotionUsageTypeFilter![1].rawValue)

    }

    func test_givenCategoriesAndPromotionUsageTypePhysicalAndUnknowm_whenICallRetrieveLiterals_thenReturnShouldHaveOneItemsAndShouldMatchTitleAndKey() {

        // given
        let categoriesBO = PromotionsGenerator.getCategoryBO()
        sut.promotionUsageTypeFilter = [.physical, .unknown]

        // when
        let result = sut.retrieveLiterals(withCategories: categoriesBO)

        // then
        XCTAssert(result.count == 1)
        XCTAssert(result[0].title == sut.promotionUsageTypeFilter![0].literal)
        XCTAssert(result[0].key == sut.promotionUsageTypeFilter![0].rawValue)

    }

    func test_givenCategoriesAndOnlyCategoryTypeUnknowm_whenICallRetrieveLiterals_thenReturnEmptyArray() {

        // given
        let categoriesBO = PromotionsGenerator.getCategoryBO()
        sut.categoriesFilter = [.unknown]

        // when
        let result = sut.retrieveLiterals(withCategories: categoriesBO)

        // then
        XCTAssert(result.isEmpty == true)

    }

    func test_givenCategoriesAndCategoryTypeAutoEcommcerceAndHome_whenICallRetrieveLiterals_thenReturnShouldHaveThreeItemsAndTitleShouldMatchWithCategoryNameAndKeyWithTheRawValue() {

        // given
        let categoriesBO = PromotionsGenerator.getCategoryBO()
        sut.categoriesFilter = [.auto, .ecommerce, .home]

        // when
        let result = sut.retrieveLiterals(withCategories: categoriesBO)

        // then
        XCTAssert(result.count == 3)
        XCTAssert(result[0].title == categoriesBO.categories[1].name)
        XCTAssert(result[0].key == sut.categoriesFilter![0].rawValue)
        XCTAssert(result[1].title == categoriesBO.categories[28].name)
        XCTAssert(result[1].key == sut.categoriesFilter![1].rawValue)
        XCTAssert(result[2].title == categoriesBO.categories[6].name)
        XCTAssert(result[2].key == sut.categoriesFilter![2].rawValue)

    }

    func test_givenCategoriesAndshoppingFilterWithMultipleValues_whenICallRetrieveLiterals_thenReturnShouldMatchTitleAndKey() {

        // given
        let categoriesBO = PromotionsGenerator.getCategoryBO()
        sut.textFilter = "textFilter"
        sut.promotionTypeFilter = [.discount, .toMonths]
        sut.promotionUsageTypeFilter = [.physical]
        sut.categoriesFilter = [.ecommerce, .home]

        // when
        let result = sut.retrieveLiterals(withCategories: categoriesBO)

        // then
        XCTAssert(result.count == 6)

        XCTAssert(result[0].title == sut.textFilter)
        XCTAssert(result[0].key == ShoppingFilter.text_filter_key)

        XCTAssert(result[1].title == sut.promotionTypeFilter![0].literal)
        XCTAssert(result[1].key == sut.promotionTypeFilter![0].rawValue)
        XCTAssert(result[2].title == sut.promotionTypeFilter![1].literal)
        XCTAssert(result[2].key == sut.promotionTypeFilter![1].rawValue)

        XCTAssert(result[3].title == sut.promotionUsageTypeFilter![0].literal)
        XCTAssert(result[3].key == sut.promotionUsageTypeFilter![0].rawValue)

        XCTAssert(result[4].title == categoriesBO.categories[28].name)
        XCTAssert(result[4].key == sut.categoriesFilter![0].rawValue)
        XCTAssert(result[5].title == categoriesBO.categories[6].name)
        XCTAssert(result[5].key == sut.categoriesFilter![1].rawValue)

    }

    // MARK: - removeByFilterType

    func test_givenFiltersAndFilterTypeEmpty_whenICallRemoveByFilterType_thenFiltersShoulKeepEquals() {

        let textFilter = "textFilter"
        let promotionTypeFilter: [PromotionType] = [.discount, .toMonths]
        let promotionUsageTypeFilter: [PromotionUsageType] = [.physical]
        let categoriesFilter: [CategoryType] = [.ecommerce, .home]

        // given
        sut.textFilter = textFilter
        sut.promotionTypeFilter = promotionTypeFilter
        sut.promotionUsageTypeFilter = promotionUsageTypeFilter
        sut.categoriesFilter = categoriesFilter

        // when
        sut.remove(byFilterType: "")

        // then
        XCTAssert(sut.textFilter == textFilter)
        XCTAssert(sut.promotionTypeFilter! == promotionTypeFilter)
        XCTAssert(sut.promotionUsageTypeFilter! == promotionUsageTypeFilter)
        XCTAssert(sut.categoriesFilter! == categoriesFilter)

    }

    func test_givenFiltersAndInvalidFilterType_whenICallRemoveByFilterType_thenFiltersShoulKeepEquals() {

        let textFilter = "textFilter"
        let promotionTypeFilter: [PromotionType] = [.discount, .toMonths]
        let promotionUsageTypeFilter: [PromotionUsageType] = [.physical]
        let categoriesFilter: [CategoryType] = [.ecommerce, .home]

        // given
        sut.textFilter = textFilter
        sut.promotionTypeFilter = promotionTypeFilter
        sut.promotionUsageTypeFilter = promotionUsageTypeFilter
        sut.categoriesFilter = categoriesFilter

        // when
        sut.remove(byFilterType: "Invalid_Type")

        // then
        XCTAssert(sut.textFilter == textFilter)
        XCTAssert(sut.promotionTypeFilter! == promotionTypeFilter)
        XCTAssert(sut.promotionUsageTypeFilter! == promotionUsageTypeFilter)
        XCTAssert(sut.categoriesFilter! == categoriesFilter)

    }

    func test_givenFiltersAndFilterTypeText_whenICallRemoveByFilterType_thenTextFilterShouldBeNilAndOthersShouldKeepEquals() {

        let textFilter = "textFilter"
        let promotionTypeFilter: [PromotionType] = [.discount, .toMonths]
        let promotionUsageTypeFilter: [PromotionUsageType] = [.physical]
        let categoriesFilter: [CategoryType] = [.ecommerce, .home]

        // given
        sut.textFilter = textFilter
        sut.promotionTypeFilter = promotionTypeFilter
        sut.promotionUsageTypeFilter = promotionUsageTypeFilter
        sut.categoriesFilter = categoriesFilter

        // when
        sut.remove(byFilterType: ShoppingFilter.text_filter_key)

        // then
        XCTAssert(sut.textFilter == nil)
        XCTAssert(sut.promotionTypeFilter! == promotionTypeFilter)
        XCTAssert(sut.promotionUsageTypeFilter! == promotionUsageTypeFilter)
        XCTAssert(sut.categoriesFilter! == categoriesFilter)

    }

    func test_givenFiltersAndPromotionTypeValid_whenICallRemoveByFilterType_thenPromotionTypeFilterShouldNotContainThePromotionTypeAsParameterOthersShouldKeepEquals() {

        let textFilter = "textFilter"
        let promotionTypeFilter: [PromotionType] = [.discount, .toMonths]
        let promotionUsageTypeFilter: [PromotionUsageType] = [.physical]
        let categoriesFilter: [CategoryType] = [.ecommerce, .home]

        // given
        sut.textFilter = textFilter
        sut.promotionTypeFilter = promotionTypeFilter
        sut.promotionUsageTypeFilter = promotionUsageTypeFilter
        sut.categoriesFilter = categoriesFilter

        // when
        sut.remove(byFilterType: PromotionType.discount.rawValue)

        // then
        XCTAssert(sut.textFilter == textFilter)

        XCTAssert(sut.promotionTypeFilter!.count == 1)
        XCTAssert(sut.promotionTypeFilter!.contains(.discount) == false)
        XCTAssert(sut.promotionTypeFilter!.contains(.toMonths) == true)

        XCTAssert(sut.promotionUsageTypeFilter! == promotionUsageTypeFilter)
        XCTAssert(sut.categoriesFilter! == categoriesFilter)

    }

    func test_givenFiltersAndPromotionUsageTypeValid_whenICallRemoveByFilterType_thenPromotionUsageTypeFilterShouldNotContainThePromotionUsageTypeAsParameterOthersShouldKeepEquals() {

        let textFilter = "textFilter"
        let promotionTypeFilter: [PromotionType] = [.discount, .toMonths]
        let promotionUsageTypeFilter: [PromotionUsageType] = [.physical, .online]
        let categoriesFilter: [CategoryType] = [.ecommerce, .home]

        // given
        sut.textFilter = textFilter
        sut.promotionTypeFilter = promotionTypeFilter
        sut.promotionUsageTypeFilter = promotionUsageTypeFilter
        sut.categoriesFilter = categoriesFilter

        // when
        sut.remove(byFilterType: PromotionUsageType.physical.rawValue)

        // then
        XCTAssert(sut.textFilter == textFilter)
        XCTAssert(sut.promotionTypeFilter! == promotionTypeFilter)

        XCTAssert(sut.promotionUsageTypeFilter!.count == 1)
        XCTAssert(sut.promotionUsageTypeFilter!.contains(.physical) == false)
        XCTAssert(sut.promotionUsageTypeFilter!.contains(.online) == true)

        XCTAssert(sut.categoriesFilter! == categoriesFilter)

    }

    func test_givenFiltersAndCategoryTypeValid_whenICallRemoveByFilterType_thenCategoryTypeFilterShouldNotContainTheCategoryTypeAsParameterOthersShouldKeepEquals() {

        let textFilter = "textFilter"
        let promotionTypeFilter: [PromotionType] = [.discount, .toMonths]
        let promotionUsageTypeFilter: [PromotionUsageType] = [.physical, .online]
        let categoriesFilter: [CategoryType] = [.ecommerce, .home]

        // given
        sut.textFilter = textFilter
        sut.promotionTypeFilter = promotionTypeFilter
        sut.promotionUsageTypeFilter = promotionUsageTypeFilter
        sut.categoriesFilter = categoriesFilter

        // when
        sut.remove(byFilterType: CategoryType.ecommerce.rawValue)

        // then
        XCTAssert(sut.textFilter == textFilter)
        XCTAssert(sut.promotionTypeFilter! == promotionTypeFilter)

        XCTAssert(sut.categoriesFilter!.count == 1)
        XCTAssert(sut.categoriesFilter!.contains(.ecommerce) == false)
        XCTAssert(sut.categoriesFilter!.contains(.home) == true)
    }

}
