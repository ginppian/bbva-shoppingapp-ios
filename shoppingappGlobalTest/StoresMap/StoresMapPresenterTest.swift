//
//  StoresMapPresenterTest.swift
//  shoppingapp
//
//  Created by developer on 21/09/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest
import CoreLocation
import CellsNativeComponents

#if TESTMX
@testable import shoppingappMX
#endif

class StoresMapPresenterTest: XCTestCase {
    
    var sut: StoresMapPresenter<DummyView>!
    var dummyView: DummyView?
    var dummyBusManager: DummyBusManager!

    override func setUp() {
        
        super.setUp()
        
        dummyView = DummyView()
        dummyBusManager = DummyBusManager()
        
        sut = StoresMapPresenter<DummyView>(busManager: dummyBusManager)
        sut.view = dummyView
    }
    
    // MARK: - setModel
    
    func test_givenStoresMapTransportWithPromotionBO_whenICallSetModel_thenStoresNumberShouldMatch() {
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]
        let promotionDetailTransport = StoresMapTransport()
        promotionDetailTransport.promotionBO = promotionBO

        // when
        sut.setModel(model: promotionDetailTransport)
        
        // then
        XCTAssert(sut.stores.count == promotionDetailTransport.promotionBO?.stores?.count)
    }
    
    func test_givenStoresMapTransportWithPromotionBO_whenICallSetModel_thenStoresShouldMatch() {
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]
        let promotionDetailTransport = StoresMapTransport()
        promotionDetailTransport.promotionBO = promotionBO
        
        // when
        sut.setModel(model: promotionDetailTransport)
        
        // then
        XCTAssert(sut.stores[0] === promotionDetailTransport.promotionBO?.stores?[0])
    }
    
    func test_givenStoresMapTransportWithPromotionBO_whenICallSetModel_thenSelectedStoreShouldMatch() {
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]
        let promotionDetailTransport = StoresMapTransport()
        promotionDetailTransport.promotionBO = promotionBO
        promotionDetailTransport.selectedStore = promotionBO.stores?[0]

        // when
        sut.setModel(model: promotionDetailTransport)
        
        // then
        XCTAssert(sut.selectedStore === promotionBO.stores?[0])
    }
    
    func test_givenStoresMapTransportWithPromotionBO_whenICallSetModel_thenCategoryBOIdShouldMatch() {
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]
        let promotionDetailTransport = StoresMapTransport()
        promotionDetailTransport.promotionBO = promotionBO
        promotionDetailTransport.selectedStore = promotionBO.stores?[0]
        promotionDetailTransport.category = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.setModel(model: promotionDetailTransport)
        
        // then
        XCTAssert(sut.categoryBO?.id == PromotionsGenerator.getCategoryBO().categories[0].id)
    }
    
    // MARK: - ViewDidLoad
    
    func test_givenAny_whenICallViewDidLoad_thenICallViewConfigureView() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledConfigureViewWithTitle == true)
    }
    
    func test_givenAny_whenICallViewDidLoad_thenICallViewConfigureViewWithTheCorrectTitle() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        
        sut.selectedStore = PromotionsGenerator.getPromotionsBO().promotions[0].stores?[0]
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.selectedStoreName == sut.selectedStore!.name)
    }

    func test_givenAPromotion_whenICallViewDidLoad_thenPromotionStoresShouldBeMatchWithMapDisplayData() {
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]

        let promotions = PromotionsBO(promotionBO: sut.promotionBO!)
        let dataPois = promotions.groupByLatitudeLongitudeStoreAndPromotions()
        let displayData = MapDisplayData(withDataPois: dataPois, forCategory: sut.categoryBO)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(sut.displayData.items.count == promotions.promotions[0].stores?.count)
        XCTAssert(sut.displayData.items.count == displayData.items.count)
    }
    
    // MARK: - ViewDidLoad - ShowStoresWithMapDisplayData
    
    func test_givenAPromotionAndASelectedStore_whenICallViewDidLoad_thenICalledShowStoresWithMapDisplayData() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]
        sut.selectedStore = PromotionsGenerator.getPromotionsBO().promotions[0].stores![0]
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowStoresWithMapDisplayData == true)
    }
    
    // MARK: - ViewDidLoad - ShowSelectedStore

    func test_givenAPromotionAndASelectedStore_whenICallViewDidLoad_thenICalledUpdateSelectedStore() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]
        sut.selectedStore = PromotionsGenerator.getPromotionsBO().promotions[0].stores![0]
        sut.stores = PromotionsGenerator.getPromotionsBO().promotions[0].stores!

        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowSelectedStore == true)
    }
    
    // MARK: - ViewDidLoad - isCalledShowSelectedStore
    
    func test_givenAPromotionAndASelectedStore_whenICallViewDidLoad_thenICalledShowSelectedStore() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]
        sut.selectedStore = PromotionsGenerator.getPromotionsBO().promotions[0].stores![0]
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowSelectedStore == true)
    }
    
    // MARK: - ViewDidLoad - isCalledShowUserLocation
    
    func test_givenAPromotionBOAndUserLocation_whenICallViewDidLoad_thenICallShowUserLocation() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]
        sut.promotionBO = promotionBO
        sut.userLocation = GeoLocationBO(withLatitude: 44.0, withLongitude: 3.0)

        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowUserLocation == true)
    }
    
    func test_givenAPromotionBOAndUserLocationNil_whenICallViewDidLoad_thenINotCallShowUserLocation() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]
        sut.promotionBO = promotionBO
        sut.userLocation = nil
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowUserLocation == false)
    }
    
    // MARK: - ViewDidLoad - isCalledHideUserLocation
    
    func test_givenAPromotionBOAndUserLocationNil_whenICallViewDidLoad_thenICallHideUserLocation() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]
        sut.promotionBO = promotionBO
        sut.userLocation = nil
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledHideUserLocation == true)
    }
    
    func test_givenAPromotionBOAndUserLocation_whenICallViewDidLoad_thenINotCallHideUserLocation() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]
        sut.promotionBO = promotionBO
        sut.userLocation = GeoLocationBO(withLatitude: 44.0, withLongitude: 3.0)
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledHideUserLocation == false)
    }
    
    // MARK: - SelectStore
    
    func test_givenMapDisplayDataItem_whenICallSelectStore_thenICallShowSelectedStore() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]
        sut.stores = PromotionsGenerator.getPromotionsBO().promotions[0].stores!

        let promotions = PromotionsBO(promotionBO: sut.promotionBO!)
        let dataPois = promotions.groupByLatitudeLongitudeStoreAndPromotions()
        let displayData = MapDisplayData(withDataPois: dataPois, forCategory: sut.categoryBO)
        let displayDataItem = displayData.items[0]
        
        // when
        sut.selectStore(withDisplayData: displayDataItem)
        
        // then
        XCTAssert(dummyView.isCalledShowSelectedStore == true)
    }
    
    func test_givenMapDisplayDataItem_whenICallSelectStore_thenICallHideUserLocation() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]
        sut.stores = PromotionsGenerator.getPromotionsBO().promotions[0].stores!
        
        let promotions = PromotionsBO(promotionBO: sut.promotionBO!)
        let dataPois = promotions.groupByLatitudeLongitudeStoreAndPromotions()
        let displayData = MapDisplayData(withDataPois: dataPois, forCategory: sut.categoryBO)
        let displayDataItem = displayData.items[0]
        
        // when
        sut.selectStore(withDisplayData: displayDataItem)
        
        // then
        XCTAssert(dummyView.isCalledHideUserLocation == true)
    }
    
    func test_givenMapDisplayDataItem_whenICallSelectStore_thenSelectedStoreShouldMatchWithTheStoreMapDetailDisplayData() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]
        sut.stores = PromotionsGenerator.getPromotionsBO().promotions[0].stores!
        sut.selectedStore = PromotionsGenerator.getPromotionsBO().promotions[0].stores![0]

        let promotions = PromotionsBO(promotionBO: sut.promotionBO!)
        let dataPois = promotions.groupByLatitudeLongitudeStoreAndPromotions()
        let displayData = MapDisplayData(withDataPois: dataPois, forCategory: sut.categoryBO)
        let displayDataItem = displayData.items[0]
        
        let storeMapDetailDisplayData = StoreMapDetailDisplayData(storeBO: sut.selectedStore!, userLocation: sut.userLocation, promotionType: sut.promotionBO?.promotionType)
        
        // when
        sut.selectStore(withDisplayData: displayDataItem)
        
        // then
        XCTAssert(storeMapDetailDisplayData == sut.currentStoreMapDetailDisplayData!)
    }
    
    // MARK: - DidTapInMap
    
    func test_givenSelectedStore_whenICalldidTapInMap_thenSelectedStoreShouldBeNil() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.selectedStore = PromotionsGenerator.getPromotionsBO().promotions[0].stores![0]

        // when
        sut.didTapInMap()
        
        // then
        XCTAssert(sut.selectedStore == nil)
    }
    
    func test_givenSelectedStore_whenICalldidTapInMap_thenICallHidePromo() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.selectedStore = PromotionsGenerator.getPromotionsBO().promotions[0].stores![0]

        // when
        sut.didTapInMap()
        
        // then
        XCTAssert(dummyView.isCalledHideStore == true)
    }
    
    func test_givenSelectedStoreAndUserLocation_whenICalldidTapInMap_thenICallShowUserLocation() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.selectedStore = PromotionsGenerator.getPromotionsBO().promotions[0].stores![0]
        sut.userLocation = GeoLocationBO(withLatitude: 44.0, withLongitude: 3.0)

        // when
        sut.didTapInMap()
        
        // then
        XCTAssert(dummyView.isCalledShowUserLocation == true)
    }
    
    // MARK: - PressedCallToPhoneOfStore - callToPhone
    
    func test_givenStoreMapDetailDisplayData_whenICallPressedCallToPhoneOfStore_thenICallCallToPhone() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]
        sut.stores = PromotionsGenerator.getPromotionsBO().promotions[0].stores!
        sut.selectedStore = PromotionsGenerator.getPromotionsBO().promotions[0].stores![0]

        sut.currentStoreMapDetailDisplayData = StoreMapDetailDisplayData(storeBO: sut.selectedStore!, userLocation: sut.userLocation, promotionType: sut.promotionBO?.promotionType)
        
        // when
        sut.pressedCallToPhoneOfStore()
        
        // then
        XCTAssert(dummyView.isCalledCallToPhone == true)
    }
    
    func test_givenStoreMapDetailDisplayData_whenICallPressedCallToPhoneOfStore_thenSelectedStorePhoneNumberShouldMatchWithTheStoreMapDetailDisplayDataPhoneNumber() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]
        sut.stores = PromotionsGenerator.getPromotionsBO().promotions[0].stores!
        sut.selectedStore = PromotionsGenerator.getPromotionsBO().promotions[0].stores![0]
        
        sut.currentStoreMapDetailDisplayData = StoreMapDetailDisplayData(storeBO: sut.selectedStore!, userLocation: sut.userLocation, promotionType: sut.promotionBO?.promotionType)
        
        // when
        sut.pressedCallToPhoneOfStore()
        
        // then
        XCTAssert(dummyView.phone == sut.currentStoreMapDetailDisplayData?.getPhoneContact())
    }
    
    // MARK: - PressedWebsiteOfStore - NavigateToStoresWebPage
    
    func test_givenStoreMapDetailDisplayData_whenICallPressedWebsiteOfStore_thenICalleNavigateToStoresWebPage() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]
        sut.stores = PromotionsGenerator.getPromotionsBO().promotions[0].stores!
        sut.selectedStore = PromotionsGenerator.getPromotionsBO().promotions[0].stores![0]
        
        sut.currentStoreMapDetailDisplayData = StoreMapDetailDisplayData(storeBO: sut.selectedStore!, userLocation: sut.userLocation, promotionType: sut.promotionBO?.promotionType)
        sut.currentStoreMapDetailDisplayData?.web = "www.example.com"

        // when
        sut.pressedWebsiteOfStore()
        
        // then
        XCTAssert(dummyBusManager.isCalledNavigateToStoresWebPage == true)
    }
    
    func test_givenAPromotion_whenICallPressedWebsiteOfStore_thenIValuesShouldMatch() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]
        sut.stores = PromotionsGenerator.getPromotionsBO().promotions[0].stores!
        sut.selectedStore = PromotionsGenerator.getPromotionsBO().promotions[0].stores![0]
        
        sut.currentStoreMapDetailDisplayData = StoreMapDetailDisplayData(storeBO: sut.selectedStore!, userLocation: sut.userLocation, promotionType: sut.promotionBO?.promotionType)
        sut.currentStoreMapDetailDisplayData?.web = "www.example.com"

        // when
        
        sut.pressedWebsiteOfStore()
        
        // then
        XCTAssert(dummyBusManager.dummyWebValue.webPage == sut.currentStoreMapDetailDisplayData?.web)
        XCTAssert(dummyBusManager.dummyWebValue.webTitle == sut.promotionBO?.commerceInformation.name)
    }
    
    // MARK: - PressedSendEmailOfStore - SendEmail
    
    func test_givenStoreMapDetailDisplayData_whenICallPressedSendEmailOfStore_thenICallCallToPhone() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]
        sut.stores = PromotionsGenerator.getPromotionsBO().promotions[0].stores!
        sut.selectedStore = PromotionsGenerator.getPromotionsBO().promotions[0].stores![0]
        
        sut.currentStoreMapDetailDisplayData = StoreMapDetailDisplayData(storeBO: sut.selectedStore!, userLocation: sut.userLocation, promotionType: sut.promotionBO?.promotionType)
        sut.currentStoreMapDetailDisplayData?.email = "example@gmail.com"
        // when
        sut.pressedSendEmailOfStore()
        
        // then
        XCTAssert(dummyView.isCalledSendEmail == true)
    }
    
    func test_givenStoreMapDetailDisplayData_whenICallPressedSendEmailOfStore_thenSelectedStoreEmailShouldMatchWithTheStoreMapDetailDisplayDataEmail() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]
        sut.stores = PromotionsGenerator.getPromotionsBO().promotions[0].stores!
        sut.selectedStore = PromotionsGenerator.getPromotionsBO().promotions[0].stores![0]
        
        sut.currentStoreMapDetailDisplayData = StoreMapDetailDisplayData(storeBO: sut.selectedStore!, userLocation: sut.userLocation, promotionType: sut.promotionBO?.promotionType)
        
        // when
        sut.pressedSendEmailOfStore()
        
        // then
        XCTAssert(dummyView.email == sut.currentStoreMapDetailDisplayData?.email)
    }
    
    // MARK: - PressedLaunchMapOfStore - LaunchMap
    
    func test_givenStoreMapDetailDisplayData_whenICallPressedLaunchMapOfStore_thenICallCallToPhone() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]
        sut.stores = PromotionsGenerator.getPromotionsBO().promotions[0].stores!
        sut.selectedStore = PromotionsGenerator.getPromotionsBO().promotions[0].stores![0]
        
        sut.currentStoreMapDetailDisplayData = StoreMapDetailDisplayData(storeBO: sut.selectedStore!, userLocation: sut.userLocation, promotionType: sut.promotionBO?.promotionType)
        
        // when
        sut.pressedLaunchMapOfStore()
        
        // then
        XCTAssert(dummyView.isCalledLaunchMap == true)
    }
    
    func test_givenAPromotionAndStoreMapDetailDisplayDataAndUserLocationNil_whenICallPressedLaunchMapOfStore_thenICallViewLaunchMapWithDefaultCoordinates() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]
        sut.stores = PromotionsGenerator.getPromotionsBO().promotions[0].stores!
        sut.selectedStore = PromotionsGenerator.getPromotionsBO().promotions[0].stores![0]
        sut.userLocation = GeoLocationBO(withLatitude: 44.0, withLongitude: 3.0)
        
        sut.currentStoreMapDetailDisplayData = StoreMapDetailDisplayData(storeBO: sut.selectedStore!, userLocation: sut.userLocation, promotionType: sut.promotionBO?.promotionType)
        
        sut.userLocation = nil
        
        // when
        sut.pressedLaunchMapOfStore()
        
        // then
        XCTAssert(dummyView.destinationCoordinate?.latitude == sut.currentStoreMapDetailDisplayData?.latitude)
        XCTAssert(dummyView.destinationCoordinate?.longitude == sut.currentStoreMapDetailDisplayData?.longitude)
        XCTAssert(dummyView.originCoordinate?.latitude == sut.defaultLocation.latitude)
        XCTAssert(dummyView.originCoordinate?.longitude == sut.defaultLocation.longitude)
    }
    
    func test_givenAPromotionAndStoreMapDetailDisplayDataAndUserLocation_whenICallPressedLaunchMapOfStore_thenICallViewLaunchMapWithCoordinatesInvalidButMethodUseUserLocationCoordinates() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        sut.promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]
        sut.stores = PromotionsGenerator.getPromotionsBO().promotions[0].stores!
        sut.selectedStore = PromotionsGenerator.getPromotionsBO().promotions[0].stores![0]
        sut.userLocation = GeoLocationBO(withLatitude: 44.0, withLongitude: 3.0)
        
        sut.currentStoreMapDetailDisplayData = StoreMapDetailDisplayData(storeBO: sut.selectedStore!, userLocation: sut.userLocation, promotionType: sut.promotionBO?.promotionType)
        
        sut.userLocation = GeoLocationBO(withLatitude: 40.0, withLongitude: -3.3)
        
        // when
        sut.pressedLaunchMapOfStore()
        
        // then
        XCTAssert(dummyView.destinationCoordinate?.latitude == sut.currentStoreMapDetailDisplayData?.latitude)
        XCTAssert(dummyView.destinationCoordinate?.longitude == sut.currentStoreMapDetailDisplayData?.longitude)
        XCTAssert(dummyView.originCoordinate?.latitude == sut.userLocation?.latitude)
        XCTAssert(dummyView.originCoordinate?.longitude == sut.userLocation?.longitude)
    }
    
    // MARK: - Dummy data
    
    class DummyView: StoresMapViewProtocol {
        
        var isCalledConfigureViewWithTitle = false
        var isCalledShowStoresWithMapDisplayData = false
        var isCalledShowUserLocation = false
        var isCalledHideUserLocation = false
        var isCalledShowSelectedStore = false
        var isCalledHideStore = false
        var isCalledCallToPhone = false
        var isCalledSendEmail = false
        var isCalledLaunchMap = false

        var stores = [StoreBO]()
        var mapDisplayData = MapDisplayData()
        var storeMapDetailDisplayData = StoreMapDetailDisplayData()
        var selectedStoreName: String?
        var phone: String?
        var email: String?
        var destinationCoordinate: CLLocationCoordinate2D?
        var originCoordinate: CLLocationCoordinate2D?

        func configureTitle(selectedStoreName: String) {
            isCalledConfigureViewWithTitle = true
            self.selectedStoreName = selectedStoreName
        }
        
        func showStores(withDisplayData displayData: MapDisplayData) {
            isCalledShowStoresWithMapDisplayData = true
            mapDisplayData = displayData
            
            for displayDataItem in displayData.items where displayDataItem.selectedItem {
                isCalledShowSelectedStore = true
            }
        }
        
        func showUserLocation() {
            isCalledShowUserLocation = true
        }
        
        func hideUserLocation() {
            isCalledHideUserLocation = true
        }
        
        func showSelectedStore(withDisplayData displayData: StoreMapDetailDisplayData) {
            isCalledShowSelectedStore = true
            storeMapDetailDisplayData = displayData
        }
        
        func hideStore() {
            isCalledHideStore = true
        }
        
        func callToPhone(withPhone phone: String) {
            self.phone = phone
            isCalledCallToPhone = true
        }
        
        func sendEmail(withEmail email: String) {
            self.email = email
            isCalledSendEmail = true
        }
        
        func launchMap(withDestinationCoordinate destinationCoordinate: CLLocationCoordinate2D, andOriginCoordinate originCoordinate: CLLocationCoordinate2D?) {
            self.destinationCoordinate = destinationCoordinate
            self.originCoordinate = originCoordinate
            isCalledLaunchMap = true
        }
        
        func showError(error: ModelBO) { }
        
        func changeColorEditTexts() { }
    }
    
    class DummyBusManager: BusManager {
        
        var isCalledNavigateToStoresWebPage = false
        
        var dummyWebValue = WebTransport()
        
        override init() {
            super.init()
            routerFactory = RouterInitFactory()
        }
        
        override func navigateScreen<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {
            if tag == "shopping-store-map-tag" && event === StoresMapPageReaction.EVENT_NAV_WEB_PAGE_FROM_MAP {
                isCalledNavigateToStoresWebPage = true
                dummyWebValue = values as! WebTransport
            }
        }
    }
}
