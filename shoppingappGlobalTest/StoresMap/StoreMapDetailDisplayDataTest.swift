//
//  StoreMapDetailDisplayDataTest.swift
//  shoppingapp
//
//  Created by developer on 08/10/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
@testable import shoppingappMX
#endif

class StoreMapDetailDisplayDataTest: XCTestCase {
    
    func test_givenAStoreBO_whenICallInit_thenAllAttributesAreFilled() {
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        let storeBO = promotionBO.stores![0]
        let location = storeBO.location!
        let address = "\(location.addressName!.capitalized), \(location.zipCode!.capitalized), \(location.city!.capitalized), \(location.state!.capitalized)"
        let userLocation = GeoLocationBO(withLatitude: 44.0, withLongitude: 3.0)
        
        // when
        let displayData = StoreMapDetailDisplayData(storeBO: storeBO, userLocation: userLocation, promotionType: promotionBO.promotionType)

        // then
        XCTAssert(displayData.distance != nil)
        XCTAssert(displayData.address == address)
        XCTAssert(displayData.additionalInfo == location.additionalInformation)
        XCTAssert(displayData.latitude == location.geolocation?.latitude)
        XCTAssert(displayData.longitude == location.geolocation?.longitude)
        XCTAssert(displayData.web == storeBO.contactDetails![3].contact)
        XCTAssert(displayData.email == storeBO.contactDetails![0].contact)
        XCTAssert(displayData.phone == storeBO.contactDetails![1].contact)
        XCTAssert(displayData.mobile == storeBO.contactDetails![2].contact)
        
    }
    
    func test_givenAStoreBOWithDistanceInMetersAndUserLocation_whenICallInit_thenDistanceShowInMetters() {
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        let storeBO = promotionBO.stores![0]
        storeBO.distance!.lengthType?.id = .metters
        let formattedDistance = storeBO.formattedDistance()!
        
        let userLocation = GeoLocationBO(withLatitude: 40.0, withLongitude: -3.3)
        
        // when
        let displayData = StoreMapDetailDisplayData(storeBO: storeBO, userLocation: userLocation, promotionType: promotionBO.promotionType)

        // then
        XCTAssert(displayData.distance == String(format: "%@ %@", Localizables.promotions.key_from_distance_text, formattedDistance))
        
    }
    
    func test_givenAStoreBOWithDistanceInMetersAndUserLocation_whenICallInit_thenDistanceShowInKilometters() {
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        let storeBO = promotionBO.stores![0]
        storeBO.distance!.lengthType?.id = .kilometers
        let formattedDistance = storeBO.formattedDistance()!
        
        let userLocation = GeoLocationBO(withLatitude: 40.0, withLongitude: -3.3)
        
        // when
        let displayData = StoreMapDetailDisplayData(storeBO: storeBO, userLocation: userLocation, promotionType: promotionBO.promotionType)

        // then
        XCTAssert(displayData.distance == String(format: "%@ %@", Localizables.promotions.key_from_distance_text, formattedDistance))
        
    }
    
    func test_givenAStoreBOWithDistanceInMeters_whenICallInit_thenDistanceIsEmpty() {
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        let storeBO = promotionBO.stores![0]
        storeBO.distance!.lengthType?.id = .metters
        
        // when
        let displayData = StoreMapDetailDisplayData(storeBO: storeBO)

        // then
        XCTAssert(displayData.distance == "")
        
    }
    
    func test_givenAStoreBOWithoutZipCodeAndState_whenICallInit_thenAddressOnlyContainNameAndCity() {
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        let storeBO = promotionBO.stores![0]
        storeBO.location?.zipCode = nil
        storeBO.location?.state = nil
        let location = storeBO.location!
        let address = "\(location.addressName!.capitalized), \(location.city!.capitalized)"
        let userLocation = GeoLocationBO(withLatitude: 40.0, withLongitude: -3.3)

        // when
        let displayData = StoreMapDetailDisplayData(storeBO: storeBO, userLocation: userLocation, promotionType: promotionBO.promotionType)

        // then
        XCTAssert(displayData.address == address)
        
    }
    
    func test_givenAStoreBOWithOnlyContactPhone_whenICallInit_thenOnlyContactPhoneIsFilled() {
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        let storeBO = promotionBO.stores![0]
        storeBO.contactDetails![0].contact = ""
        storeBO.contactDetails![2].contact = ""
        storeBO.contactDetails![3].contact = ""
        let userLocation = GeoLocationBO(withLatitude: 40.0, withLongitude: -3.3)

        // when
        let displayData = StoreMapDetailDisplayData(storeBO: storeBO, userLocation: userLocation, promotionType: promotionBO.promotionType)

        // then
        XCTAssert(displayData.web == nil)
        XCTAssert(displayData.email == nil)
        XCTAssert(displayData.phone == storeBO.contactDetails![1].contact)
        XCTAssert(displayData.mobile == nil)
        
    }
    
    func test_givenAStoreBOWithAdditionalInformationEmpty_whenICallInit_thenAdditionalInformationIsNil() {
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        let storeBO = promotionBO.stores![0]
        storeBO.location?.additionalInformation = ""
        let userLocation = GeoLocationBO(withLatitude: 40.0, withLongitude: -3.3)

        // when
        let displayData = StoreMapDetailDisplayData(storeBO: storeBO, userLocation: userLocation, promotionType: promotionBO.promotionType)

        // then
        XCTAssert(displayData.additionalInfo == nil)
        
    }
    
    func test_givenAStoreBOWitoutPromotionType_whenICallInit_thenPromotionTypeColorIsNil() {
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        let storeBO = promotionBO.stores![0]
        promotionBO.promotionType = nil
        let userLocation = GeoLocationBO(withLatitude: 40.0, withLongitude: -3.3)

        // when
        let displayData = StoreMapDetailDisplayData(storeBO: storeBO, userLocation: userLocation, promotionType: promotionBO.promotionType)

        // then
        XCTAssert(displayData.promotionTypeColor == nil)
        
    }
    
    func test_givenAStoreBOWitoutPromotionTypeId_whenICallInit_thenPromotionTypeColorIsNil() {
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        let storeBO = promotionBO.stores![0]
        promotionBO.promotionType.id = nil
        let userLocation = GeoLocationBO(withLatitude: 40.0, withLongitude: -3.3)

        // when
        let displayData = StoreMapDetailDisplayData(storeBO: storeBO, userLocation: userLocation, promotionType: promotionBO.promotionType)

        // then
        XCTAssert(displayData.promotionTypeColor == nil)
        
    }
    
    func test_givenAStoreBO_whenICallInit_thenPromotionTypeColorMatch() {
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        let storeBO = promotionBO.stores![0]
        let userLocation = GeoLocationBO(withLatitude: 40.0, withLongitude: -3.3)
        
        // when
        let displayData = StoreMapDetailDisplayData(storeBO: storeBO, userLocation: userLocation, promotionType: promotionBO.promotionType)

        // then
        XCTAssert(displayData.promotionTypeColor == promotionBO.promotionType.id.color)
        
    }
}
