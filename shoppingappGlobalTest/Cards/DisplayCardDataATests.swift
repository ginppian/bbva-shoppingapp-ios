//
//  DisplayCardDataATests.swift
//  shoppingappMXTest
//
//  Created by jesus.martinez on 15/12/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#endif

class DisplayCardDataATests: XCTestCase {

    // MARK: - generateDisplayCardDataA virtualInfoDisplayData

    func test_givenAVirtualCardBONilAndCardBOAssociableToVirtualCard_whenICallGenerateDisplayCardDataA_thenVirtualInfoDisplayDataShouldBeFilledAndVirtualCardDisplayDataShouldBeNil() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        cardBO.indicators[2].indicatorId = .associableToVirtualCards
        cardBO.indicators[2].isActive = true

        //When
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.virtualCardDisplayData == nil)
        XCTAssert(displayData.virtualInfoDisplayData != nil)

    }

    func test_givenAVirtualCardBONilAndCardBONotAssociableToVirtualCard_whenICallGenerateDisplayCardDataA_thenVirtualInfoDisplayDataShouldBeFilledAndVirtualCardDisplayDataShouldBeNil() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        cardBO.indicators[2].indicatorId = .associableToVirtualCards
        cardBO.indicators[2].isActive = false

        //When
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.virtualCardDisplayData == nil)
        XCTAssert(displayData.virtualInfoDisplayData != nil)

    }

    func test_givenAVirtualCardBONilAndCardBOAssociableToVirtualCard_whenICallGenerateDisplayCardDataA_thenVirtualInfoDisplayDataShouldBeFilledCorrectly() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        cardBO.indicators[2].indicatorId = .associableToVirtualCards
        cardBO.indicators[2].isActive = true

        //When
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.virtualInfoDisplayData?.title == Localizables.cards.key_card_digital_text)
        XCTAssert(displayData.virtualInfoDisplayData?.description == Localizables.cards.key_cards_create_digital_card_text)
        XCTAssert(displayData.virtualInfoDisplayData?.link == Localizables.cards.key_cards_get_digital_card_text)
        XCTAssert(displayData.virtualInfoDisplayData?.alpha == 1.0)

    }

    func test_givenAVirtualCardBONilAndCardBONotAssociableToVirtualCard_whenICallGenerateDisplayCardDataA_thenVirtualInfoDisplayDataShouldBeFilledCorrectly() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        cardBO.indicators[2].indicatorId = .associableToVirtualCards
        cardBO.indicators[2].isActive = false

        //When
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.virtualInfoDisplayData?.title == Localizables.cards.key_card_digital_text)
        XCTAssert(displayData.virtualInfoDisplayData?.description == Localizables.cards.key_cards_digitalize_disable_for_this_card_text)
        XCTAssert(displayData.virtualInfoDisplayData?.link == nil)
        XCTAssert(displayData.virtualInfoDisplayData?.alpha == 0.5)

    }

    // MARK: - Physical

    func test_givenACardBO_whenICallGenerateDisplayCardDataA_thenDisplayDataPanShouldBeMatchWithCardBONumberValue() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        //When
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.pan == DisplayCardData.formatTextUsingSeparator(text: cardBO.number))

    }

    func test_givenACardBO_whenICallGenerateDisplayCardDataA_thenDisplayDataPanShouldBeMatchWithCardBOHolderNameValueUppercased() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        //When
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.holderName == cardBO.holderName.uppercased())

    }

    func test_givenACardBOAndCardsWithExpirationDateWithCorrectFormat_whenICallGenerateDisplayCardDataA_thenDisplayDataHasExpirationDateFilled() {

        //given
        let cardBO = CardsGenerator.getCardBO(withExpirationDate: "2020-12-14").cards[0]

        //When
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.expirationDate != nil)

    }

    func test_givenACardBOAndCardsWithExpirationDateWithoutCorrectFormat_whenICallGenerateDisplayCardDataA_thenDisplayDataDoesNotHaveExpirationDateFilled() {

        //given
        let cardBO = CardsGenerator.getCardBO(withExpirationDate: "14/12").cards[0]

        //When
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.expirationDate == nil)

    }

    func test_givenACardBOAndCardsWithExpirationDateWithCorrectFormat_whenICallGenerateDisplayCardDataA_thenDisplayDataHasExpirationDateWithTheCorrectFormat() {

        //given
        let cardBO = CardsGenerator.getCardBO(withExpirationDate: "2020-12-14").cards[0]

        //When
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.expirationDate == "12/20")

    }

    func test_givenACardBO_whenICallGenerateDisplayCardDataA_thenDisplayDataImageCardIsNotEmpty() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        //When
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: nil)

        // then
        XCTAssertFalse((displayData.imageCard?.isEmpty)!)

    }

    func test_givenACard_whenICallGenerateDisplayCardDataA_thenDisplayDataImageCardIsTheSameThatCard() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.imageCard == cardBO.imageFront)
        XCTAssert(displayData.backImageCard == cardBO.imageBack)

    }

    func test_givenACard_whenICallGenerateDisplayCardDataA_thenDisplayDataTextStatusEmpty() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: nil)

        // then
        XCTAssert((displayData.imageStatus?.isEmpty)!)
        XCTAssert(displayData.bottomTitleText == nil)
        XCTAssert(displayData.bottomImageNamed == nil)

    }

    func test_givenACardBloqued_whenICallGenerateDisplayCardDataA_thenDisplayDataImageStatusBlocked() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "BLOCKED", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.imageStatus == ConstantsImages.Card.card_blocked)
        XCTAssert(displayData.bottomTitleText == nil)
        XCTAssert(displayData.bottomImageNamed == nil)

    }

    func test_givenACardCanceled_whenICallGenerateDisplayCardDataA_thenDisplayDataImageStatusBlocked() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "CANCELED", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.imageStatus == ConstantsImages.Card.card_blocked)
        XCTAssert(displayData.bottomTitleText == nil)
        XCTAssert(displayData.bottomImageNamed == nil)

    }

    func test_givenACardPendingEmbossing_whenICallGenerateDisplayCardDataA_thenDisplayDataImageStatusDelivery() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "PENDING_EMBOSSING", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.imageStatus == ConstantsImages.Card.card_delivery)
        XCTAssert(displayData.bottomTitleText == nil)
        XCTAssert(displayData.bottomImageNamed == nil)

    }

    func test_givenACardPendingDelivery_whenICallGenerateDisplayCardDataA_thenDisplayDataImageStatusDelivery() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "PENDING_DELIVERY", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.imageStatus == ConstantsImages.Card.card_delivery)
        XCTAssert(displayData.bottomTitleText == nil)
        XCTAssert(displayData.bottomImageNamed == nil)

    }

    func test_givenACardUnknowm_whenICallGenerateDisplayCardDataA_thenDisplayDataImageStatusBlocked() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "UNKNOWM", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.imageStatus == ConstantsImages.Card.card_blocked)
        XCTAssert(displayData.bottomTitleText == nil)
        XCTAssert(displayData.bottomImageNamed == nil)

    }

    func test_givenACardBloqued_whenICallGenerateDisplayCardDataA_thenDisplayDataTextStatusOFF() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "false").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: nil)
        // then
        XCTAssert(displayData.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(displayData.bottomTitleText == nil)
        XCTAssert(displayData.bottomImageNamed == nil)

    }

    func test_givenACardPendingToActive_whenICallGenerateDisplayCardDataA_thenDisplayDataStatusPendingToActive() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "INOPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.imageStatus == ConstantsImages.Card.card_activate)

    }

    func test_givenACardWithEcommerceActivationFalse_whenICallGenerateDisplayCardDataA_thenDisplayDataTextStatusSemiOff() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ECOMMERCE_ACTIVATION", withIsActive: "false").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(displayData.bottomTitleText == nil)
        XCTAssert(displayData.bottomImageNamed == nil)

    }

    func test_givenACardWithCashWithDrawalActivationFalse_whenICallGenerateDisplayCardDataA_thenDisplayDataTextStatusSemiOff() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "CASHWITHDRAWAL_ACTIVATION", withIsActive: "false").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(displayData.bottomTitleText == nil)
        XCTAssert(displayData.bottomImageNamed == nil)

    }

    func test_givenACardWithPurchaseActivationFalse_whenICallGenerateDisplayCardDataA_thenDisplayDataTextStatusSemiOff() {

        //given

        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "PURCHASES_ACTIVATION", withIsActive: "false").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(displayData.bottomTitleText == nil)
        XCTAssert(displayData.bottomImageNamed == nil)

    }

    func test_givenACardWithForeignCashWithDrawalActivationFalse_whenICallGenerateDisplayCardDataA_thenDisplayDataTextStatusSemiOff() {

        //given

        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "FOREIGN_CASHWITHDRAWAL_ACTIVATION", withIsActive: "false").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(displayData.bottomTitleText == nil)
        XCTAssert(displayData.bottomImageNamed == nil)

    }

    func test_givenACardWithForeignPurchaseActivationFalse_whenICallGenerateDisplayCardDataA_thenDisplayDataTextStatusSemiOff() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "FOREIGN_PURCHASES_ACTIVATION", withIsActive: "false").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(displayData.bottomTitleText == nil)
        XCTAssert(displayData.bottomImageNamed == nil)

    }

    func test_givenAAtivationOnOffFalseAndForeignPurchaseFasle_whenICallGenerateDisplayCardDataA_thenDisplayDataTextStatusOff() {

        //given
        let cardBO = CardsGenerator.getCardBOMultipleActivation(withStatusId: "OPERATIVE", withActivationId: "FOREIGN_PURCHASES_ACTIVATION", withIsActive: "false", withActivationId: "ON_OFF", withIsActive2: "false").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(displayData.bottomTitleText == nil)
        XCTAssert(displayData.bottomImageNamed == nil)

    }

    func test_givenAAtivationOnOffFalseAndForeignPurchaseTrue_whenICallGenerateDisplayCardDataA_thenDisplayDataTextStatusSemiOff() {

        //given
        let cardBO = CardsGenerator.getCardBOMultipleActivation(withStatusId: "OPERATIVE", withActivationId: "FOREIGN_PURCHASES_ACTIVATION", withIsActive: "false", withActivationId: "ON_OFF", withIsActive2: "true").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(displayData.bottomTitleText == nil)
        XCTAssert(displayData.bottomImageNamed == nil)

    }

    func test_givenAAtivationPurchaseFalseAndForeignPurchasefalse_whenICallGenerateDisplayCardDataA_thenDisplayDataTextStatusSemiOff() {

        //given
        let cardBO = CardsGenerator.getCardBOMultipleActivation(withStatusId: "OPERATIVE", withActivationId: "FOREIGN_PURCHASES_ACTIVATION", withIsActive: "false", withActivationId: "PURCHASES_ACTIVATION", withIsActive2: "false").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(displayData.bottomTitleText == nil)
        XCTAssert(displayData.bottomImageNamed == nil)

    }

    func test_givenAAtivationPurchaseTrueAndForeignPurchaseTrue_whenICallGenerateDisplayCardDataA_thenDisplayDataTextStatusEmpty() {

        //given
        let cardBO = CardsGenerator.getCardBOMultipleActivation(withStatusId: "OPERATIVE", withActivationId: "FOREIGN_PURCHASES_ACTIVATION", withIsActive: "true", withActivationId: "PURCHASES_ACTIVATION", withIsActive2: "true").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(displayData.bottomTitleText == nil)
        XCTAssert(displayData.bottomImageNamed == nil)

    }

    func test_givenAnActivationPurchaseTrueAndActivationsIsEmpty_whenICallGenerateDisplayCardDataA_thenDisplayDataTextStatusEmpty() {

        //given

        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "PURCHASES_ACTIVATION", withIsActive: "true").cards[0]
        cardBO.activations = Array()

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(displayData.bottomTitleText == nil)
        XCTAssert(displayData.bottomImageNamed == nil)

    }

    func test_givenAliasInCard_whenICallGenerateDisplayCardDataA_thenDisplayTheCardAliasValue() {

        //given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.titleCard == cardBO.alias)

    }

    func test_givenAliasEmpty_whenICallGenerateDisplayCardDataA_thenDisplayTheCardTitleNameValue() {

        //given
        let cardBO = CardsGenerator.getCardBOWithAliasEmpty().cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.titleCard == cardBO.title?.name)

    }

    func test_givenAnInOperativeCardAndNotSetteableToOperative_whenICallGenerateDisplayCardDataA_thenLeftButtonDisplayDataIsFilledAndRightNot() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "INOPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        cardBO.indicators[1].indicatorId = .settableToOperative
        cardBO.indicators[1].isActive = false

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.leftButtonDisplayData != nil)
        XCTAssert(displayData.rightButtonDisplayData == nil)
    }

    func test_givenAnInOperativeCardAndNotSetteableToOperative_whenICallGenerateDisplayCardDataA_thenLeftButtonDisplayDataIsActivateCard() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "INOPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        cardBO.indicators[1].indicatorId = .settableToOperative
        cardBO.indicators[1].isActive = false

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.leftButtonDisplayData?.imageNamed == ConstantsImages.Common.help_icon_on_off)
        XCTAssert(displayData.leftButtonDisplayData?.title == Localizables.cards.key_cards_how_activate_card_text)
        XCTAssert(displayData.leftButtonDisplayData?.actionType == .activateCardExplanation)
        XCTAssert(displayData.leftButtonDisplayData?.accesibility == ConstantsAccessibilities.CARD_BOTTOM_ACTION_BUTTON)

    }

    func test_givenAnInOperativeCardAndSetteableToOperative_whenICallGenerateDisplayCardDataA_thenLeftButtonDisplayDataIsFilledAndRightNot() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "INOPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        cardBO.indicators[1].indicatorId = .settableToOperative
        cardBO.indicators[1].isActive = true

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.leftButtonDisplayData != nil)
        XCTAssert(displayData.rightButtonDisplayData == nil)
    }

    func test_givenAnInOperativeCardAndSetteableToOperative_whenICallGenerateDisplayCardDataA_thenLeftButtonDisplayDataIsActivateCardExplanation() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "INOPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        cardBO.indicators[1].indicatorId = .settableToOperative
        cardBO.indicators[1].isActive = true

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.leftButtonDisplayData?.imageNamed == ConstantsImages.Card.creditcard_icon)
        XCTAssert(displayData.leftButtonDisplayData?.title == Localizables.cards.key_cards_activate_card_text)
        XCTAssert(displayData.leftButtonDisplayData?.actionType == .activateCard)
        XCTAssert(displayData.leftButtonDisplayData?.accesibility == ConstantsAccessibilities.CARD_BOTTOM_ACTION_BUTTON)

    }

    func test_givenAnOperativeCardAndReadableSecurityData_whenICallGenerateDisplayCardDataA_thenLeftAndRightButtonDisplayShouldBeFilled() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        cardBO.indicators[1].indicatorId = .readableSecurityData
        cardBO.indicators[1].isActive = true

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.leftButtonDisplayData != nil)
        XCTAssert(displayData.rightButtonDisplayData != nil)
    }

    func test_givenAnOperativeCardAndReadableSecurityData_whenICallGenerateDisplayCardDataA_thenLeftButtonDisplayDataIsCopyPanAndRightSeeCVV() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        cardBO.indicators[1].indicatorId = .readableSecurityData
        cardBO.indicators[1].isActive = true

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.leftButtonDisplayData?.imageNamed == nil)
        XCTAssert(displayData.leftButtonDisplayData?.title == Localizables.cards.key_cards_copy_text)
        XCTAssert(displayData.leftButtonDisplayData?.actionType == .copyPan)
        XCTAssert(displayData.leftButtonDisplayData?.accesibility == ConstantsAccessibilities.CARD_COPY_PAN_ACTION_BUTTON)

        XCTAssert(displayData.rightButtonDisplayData?.imageNamed == nil)
        XCTAssert(displayData.rightButtonDisplayData?.title == Localizables.cards.key_card_see_cvv_text)
        XCTAssert(displayData.rightButtonDisplayData?.actionType == .seeCVV)
        XCTAssert(displayData.rightButtonDisplayData?.accesibility == ConstantsAccessibilities.CARD_BOTTOM_ACTION_BUTTON_VER_CVV)

    }

    func test_givenAnOperativeCardANotReadableSecurityDataAndNotSetteableToOperative_whenICallGenerateDisplayCardDataA_thenLeftButtonDisplayDataIsFilledAndRightNot() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        cardBO.indicators[1].indicatorId = .readableSecurityData
        cardBO.indicators[1].isActive = false
        cardBO.indicators[2].indicatorId = .settableToOperative
        cardBO.indicators[2].isActive = false

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.leftButtonDisplayData != nil)
        XCTAssert(displayData.rightButtonDisplayData == nil)
    }

    func test_givenAnOperativeCardANotReadableSecurityDataAndNotSetteableToOperative_whenICallGenerateDisplayCardDataA_thenLeftButtonDisplayDataIsCopyPan() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        cardBO.indicators[1].indicatorId = .readableSecurityData
        cardBO.indicators[1].isActive = false
        cardBO.indicators[2].indicatorId = .settableToOperative
        cardBO.indicators[2].isActive = false

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.leftButtonDisplayData?.imageNamed == nil)
        XCTAssert(displayData.leftButtonDisplayData?.title == Localizables.cards.key_cards_copy_text)
        XCTAssert(displayData.leftButtonDisplayData?.actionType == .copyPan)
        XCTAssert(displayData.leftButtonDisplayData?.accesibility == ConstantsAccessibilities.CARD_COPY_PAN_ACTION_BUTTON)

    }

    func test_givenAnOperativeCardANotReadableSecurityDataAndSetteableToOperative_whenICallGenerateDisplayCardDataA_thenLeftButtonDisplayDataIsFilledAndRightNot() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        cardBO.indicators[1].indicatorId = .readableSecurityData
        cardBO.indicators[1].isActive = false
        cardBO.indicators[2].indicatorId = .settableToOperative
        cardBO.indicators[2].isActive = true

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.leftButtonDisplayData != nil)
        XCTAssert(displayData.rightButtonDisplayData == nil)
    }

    func test_givenAnOperativeCardANotReadableSecurityDataAndSetteableToOperative_whenICallGenerateDisplayCardDataA_thenLeftButtonDisplayDataIsCopyPan() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        cardBO.indicators[1].indicatorId = .readableSecurityData
        cardBO.indicators[1].isActive = false
        cardBO.indicators[2].indicatorId = .settableToOperative
        cardBO.indicators[2].isActive = true

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.leftButtonDisplayData?.imageNamed == nil)
        XCTAssert(displayData.leftButtonDisplayData?.title == Localizables.cards.key_cards_copy_text)
        XCTAssert(displayData.leftButtonDisplayData?.actionType == .copyPan)
        XCTAssert(displayData.leftButtonDisplayData?.accesibility == ConstantsAccessibilities.CARD_COPY_PAN_ACTION_BUTTON)

    }

    // MARK: - generateDisplayCardDataA virtualCardDisplayData

    func test_givenAVirtualCardBOAndCardBOAssociableToVirtualCard_whenICallGenerateDisplayCardDataA_thenVirtualInfoDisplayDataShouldBeNilAndVirtualCardDisplayDataShouldBeFilled() {

        //given
        let cardBO = CardsGenerator.getThreeCards().cards[0]
        cardBO.indicators[2].indicatorId = .associableToVirtualCards
        cardBO.indicators[2].isActive = true

        let virtualBO = CardsGenerator.getThreeCards().cards[1]

        //When
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: virtualBO, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.virtualCardDisplayData != nil)
        XCTAssert(displayData.virtualInfoDisplayData == nil)

    }

    func test_givenAVirtualCardBOAndCardBONotAssociableToVirtualCard_whenICallGenerateDisplayCardDataA_thenVirtualInfoDisplayDataShouldBeNilAndVirtualCardDisplayDataShouldBeFilled() {

        //given
        let cardBO = CardsGenerator.getThreeCards().cards[0]
        cardBO.indicators[2].indicatorId = .associableToVirtualCards
        cardBO.indicators[2].isActive = false

        let virtualBO = CardsGenerator.getThreeCards().cards[1]

        //When
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: virtualBO, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.virtualCardDisplayData != nil)
        XCTAssert(displayData.virtualInfoDisplayData == nil)

    }

    func test_givenVirtualCardBOACardBO_whenICallGenerateDisplayCardDataA_thenDisplayDataPanShouldBeMatchWithCardBONumberValue() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        let virtualBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        //When
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: virtualBO, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.virtualCardDisplayData!.pan == DisplayCardData.formatTextUsingSeparator(text: cardBO.number))

    }

    func test_givenVirtualCardBOACardBO_whenICallGenerateDisplayCardDataA_thenDisplayDataPanShouldBeMatchWithCardBOHolderNameValueUppercased() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        let virtualBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        //When
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: virtualBO, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.virtualCardDisplayData!.holderName == cardBO.holderName.uppercased())

    }

    func test_givenVirtualCardBOACardBOAndCardsWithExpirationDateWithCorrectFormat_whenICallGenerateDisplayCardDataA_thenDisplayDataHasExpirationDateFilled() {

        //given
        let cardBO = CardsGenerator.getCardBO(withExpirationDate: "2020-12-14").cards[0]
        let virtualBO = CardsGenerator.getCardBO(withExpirationDate: "2020-12-14").cards[0]

        //When
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: virtualBO, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.virtualCardDisplayData!.expirationDate != nil)

    }

    func test_givenVirtualCardBOACardBOAndCardsWithExpirationDateWithoutCorrectFormat_whenICallGenerateDisplayCardDataA_thenDisplayDataDoesNotHaveExpirationDateFilled() {

        //given
        let cardBO = CardsGenerator.getCardBO(withExpirationDate: "14/12").cards[0]
        let virtualBO = CardsGenerator.getCardBO(withExpirationDate: "14/12").cards[0]

        //When
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: virtualBO, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.virtualCardDisplayData!.expirationDate == nil)

    }

    func test_givenVirtualCardBOACardBOAndCardsWithExpirationDateWithCorrectFormat_whenICallGenerateDisplayCardDataA_thenDisplayDataHasExpirationDateWithTheCorrectFormat() {

        //given
        let cardBO = CardsGenerator.getCardBO(withExpirationDate: "2020-12-14").cards[0]
        let virtualBO = CardsGenerator.getCardBO(withExpirationDate: "2020-12-14").cards[0]

        //When
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: virtualBO, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.virtualCardDisplayData!.expirationDate == "12/20")

    }

    func test_givenVirtualCardBOACardBO_whenICallGenerateDisplayCardDataA_thenDisplayDataImageCardIsNotEmpty() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        let virtualBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        //When
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: virtualBO, withStickerCardBO: nil)

        // then
        XCTAssertFalse((displayData.virtualCardDisplayData!.imageCard?.isEmpty)!)

    }

    func test_givenVirtualCardBOACard_whenICallGenerateDisplayCardDataA_thenDisplayDataImageCardIsTheSameThatCard() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        let virtualBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: virtualBO, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.virtualCardDisplayData!.imageCard == virtualBO.imageFront)
        XCTAssert(displayData.virtualCardDisplayData!.backImageCard == virtualBO.imageBack)
    }

    func test_givenVirtualCardBOACard_whenICallGenerateDisplayCardDataA_thenDisplayDataTextStatusEmpty() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        let virtualBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: virtualBO, withStickerCardBO: nil)

        // then
        XCTAssert((displayData.virtualCardDisplayData!.imageStatus?.isEmpty)!)
        XCTAssert(displayData.virtualCardDisplayData!.bottomTitleText == nil)
        XCTAssert(displayData.virtualCardDisplayData!.bottomImageNamed == nil)

    }

    func test_givenVirtualCardBOACardBloqued_whenICallGenerateDisplayCardDataA_thenDisplayDataImageStatusBlocked() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "BLOCKED", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        let virtualBO = CardsGenerator.getCardBO(withStatusId: "BLOCKED", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: virtualBO, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.virtualCardDisplayData!.imageStatus == ConstantsImages.Card.card_blocked)
        XCTAssert(displayData.virtualCardDisplayData!.bottomTitleText == nil)
        XCTAssert(displayData.virtualCardDisplayData!.bottomImageNamed == nil)

    }

    func test_givenVirtualCardBOACardCanceled_whenICallGenerateDisplayCardDataA_thenDisplayDataImageStatusBlocked() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "CANCELED", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        let virtualBO = CardsGenerator.getCardBO(withStatusId: "CANCELED", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: virtualBO, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.virtualCardDisplayData!.imageStatus == ConstantsImages.Card.card_blocked)
        XCTAssert(displayData.virtualCardDisplayData!.bottomTitleText == nil)
        XCTAssert(displayData.virtualCardDisplayData!.bottomImageNamed == nil)

    }

    func test_givenVirtualCardBOACardPendingEmbossing_whenICallGenerateDisplayCardDataA_thenDisplayDataImageStatusDelivery() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "PENDING_EMBOSSING", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        let virtualBO = CardsGenerator.getCardBO(withStatusId: "PENDING_EMBOSSING", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: virtualBO, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.virtualCardDisplayData!.imageStatus == ConstantsImages.Card.card_delivery)
        XCTAssert(displayData.virtualCardDisplayData!.bottomTitleText == nil)
        XCTAssert(displayData.virtualCardDisplayData!.bottomImageNamed == nil)

    }

    func test_givenVirtualCardBOACardPendingDelivery_whenICallGenerateDisplayCardDataA_thenDisplayDataImageStatusDelivery() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "PENDING_DELIVERY", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        let virtualBO = CardsGenerator.getCardBO(withStatusId: "PENDING_DELIVERY", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: virtualBO, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.virtualCardDisplayData!.imageStatus == ConstantsImages.Card.card_delivery)
        XCTAssert(displayData.virtualCardDisplayData!.bottomTitleText == nil)
        XCTAssert(displayData.virtualCardDisplayData!.bottomImageNamed == nil)

    }

    func test_givenVirtualCardBOACardUnknowm_whenICallGenerateDisplayCardDataA_thenDisplayDataImageStatusBlocked() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "UNKNOWM", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        let virtualBO = CardsGenerator.getCardBO(withStatusId: "UNKNOWM", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: virtualBO, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.virtualCardDisplayData!.imageStatus == ConstantsImages.Card.card_blocked)
        XCTAssert(displayData.virtualCardDisplayData!.bottomTitleText == nil)
        XCTAssert(displayData.virtualCardDisplayData!.bottomImageNamed == nil)

    }

    func test_givenVirtualCardBOACardBloqued_whenICallGenerateDisplayCardDataA_thenDisplayDataTextStatusOFF() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "false").cards[0]
        let virtualBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "false").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: virtualBO, withStickerCardBO: nil)
        // then
        XCTAssert(displayData.virtualCardDisplayData!.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(displayData.virtualCardDisplayData!.bottomTitleText == nil)
        XCTAssert(displayData.virtualCardDisplayData!.bottomImageNamed == nil)

    }

    func test_givenVirtualCardBOACardPendingToActive_whenICallGenerateDisplayCardDataA_thenDisplayDataStatusPendingToActive() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "INOPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        let virtualBO = CardsGenerator.getCardBO(withStatusId: "INOPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: virtualBO, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.virtualCardDisplayData!.imageStatus == ConstantsImages.Card.card_activate)

    }

    func test_givenVirtualCardBOACardWithEcommerceActivationFalse_whenICallGenerateDisplayCardDataA_thenDisplayDataTextStatusSemiOff() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ECOMMERCE_ACTIVATION", withIsActive: "false").cards[0]
        let virtualBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ECOMMERCE_ACTIVATION", withIsActive: "false").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: virtualBO, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.virtualCardDisplayData!.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(displayData.virtualCardDisplayData!.bottomTitleText == nil)
        XCTAssert(displayData.virtualCardDisplayData!.bottomImageNamed == nil)

    }

    func test_givenVirtualCardBOACardWithCashWithDrawalActivationFalse_whenICallGenerateDisplayCardDataA_thenDisplayDataTextStatusSemiOff() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "CASHWITHDRAWAL_ACTIVATION", withIsActive: "false").cards[0]
        let virtualBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "CASHWITHDRAWAL_ACTIVATION", withIsActive: "false").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: virtualBO, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.virtualCardDisplayData!.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(displayData.virtualCardDisplayData!.bottomTitleText == nil)
        XCTAssert(displayData.virtualCardDisplayData!.bottomImageNamed == nil)

    }

    func test_givenVirtualCardBOACardWithPurchaseActivationFalse_whenICallGenerateDisplayCardDataA_thenDisplayDataTextStatusSemiOff() {

        //given

        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "PURCHASES_ACTIVATION", withIsActive: "false").cards[0]
        let virtualBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "PURCHASES_ACTIVATION", withIsActive: "false").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: virtualBO, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.virtualCardDisplayData!.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(displayData.virtualCardDisplayData!.bottomTitleText == nil)
        XCTAssert(displayData.virtualCardDisplayData!.bottomImageNamed == nil)

    }

    func test_givenVirtualCardBOACardWithForeignCashWithDrawalActivationFalse_whenICallGenerateDisplayCardDataA_thenDisplayDataTextStatusSemiOff() {

        //given

        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "FOREIGN_CASHWITHDRAWAL_ACTIVATION", withIsActive: "false").cards[0]
        let virtualBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "FOREIGN_CASHWITHDRAWAL_ACTIVATION", withIsActive: "false").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: virtualBO, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.virtualCardDisplayData!.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(displayData.virtualCardDisplayData!.bottomTitleText == nil)
        XCTAssert(displayData.virtualCardDisplayData!.bottomImageNamed == nil)

    }

    func test_givenVirtualCardBOACardWithForeignPurchaseActivationFalse_whenICallGenerateDisplayCardDataA_thenDisplayDataTextStatusSemiOff() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "FOREIGN_PURCHASES_ACTIVATION", withIsActive: "false").cards[0]
        let virtualBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "FOREIGN_PURCHASES_ACTIVATION", withIsActive: "false").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: virtualBO, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.virtualCardDisplayData!.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(displayData.virtualCardDisplayData!.bottomTitleText == nil)
        XCTAssert(displayData.virtualCardDisplayData!.bottomImageNamed == nil)

    }

    func test_givenVirtualCardBOAAtivationOnOffFalseAndForeignPurchaseFasle_whenICallGenerateDisplayCardDataA_thenDisplayDataTextStatusOff() {

        //given
        let cardBO = CardsGenerator.getCardBOMultipleActivation(withStatusId: "OPERATIVE", withActivationId: "FOREIGN_PURCHASES_ACTIVATION", withIsActive: "false", withActivationId: "ON_OFF", withIsActive2: "false").cards[0]
        let virtualBO = CardsGenerator.getCardBOMultipleActivation(withStatusId: "OPERATIVE", withActivationId: "FOREIGN_PURCHASES_ACTIVATION", withIsActive: "false", withActivationId: "ON_OFF", withIsActive2: "false").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: virtualBO, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.virtualCardDisplayData!.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(displayData.virtualCardDisplayData!.bottomTitleText == nil)
        XCTAssert(displayData.virtualCardDisplayData!.bottomImageNamed == nil)

    }

    func test_givenVirtualCardBOAAtivationOnOffFalseAndForeignPurchaseTrue_whenICallGenerateDisplayCardDataA_thenDisplayDataTextStatusSemiOff() {

        //given
        let cardBO = CardsGenerator.getCardBOMultipleActivation(withStatusId: "OPERATIVE", withActivationId: "FOREIGN_PURCHASES_ACTIVATION", withIsActive: "false", withActivationId: "ON_OFF", withIsActive2: "true").cards[0]
        let virtualBO = CardsGenerator.getCardBOMultipleActivation(withStatusId: "OPERATIVE", withActivationId: "FOREIGN_PURCHASES_ACTIVATION", withIsActive: "false", withActivationId: "ON_OFF", withIsActive2: "true").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: virtualBO, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.virtualCardDisplayData!.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(displayData.virtualCardDisplayData!.bottomTitleText == nil)
        XCTAssert(displayData.virtualCardDisplayData!.bottomImageNamed == nil)

    }

    func test_givenVirtualCardBOAAtivationPurchaseFalseAndForeignPurchasefalse_whenICallGenerateDisplayCardDataA_thenDisplayDataTextStatusSemiOff() {

        //given
        let cardBO = CardsGenerator.getCardBOMultipleActivation(withStatusId: "OPERATIVE", withActivationId: "FOREIGN_PURCHASES_ACTIVATION", withIsActive: "false", withActivationId: "PURCHASES_ACTIVATION", withIsActive2: "false").cards[0]
        let virtualBO = CardsGenerator.getCardBOMultipleActivation(withStatusId: "OPERATIVE", withActivationId: "FOREIGN_PURCHASES_ACTIVATION", withIsActive: "false", withActivationId: "PURCHASES_ACTIVATION", withIsActive2: "false").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: virtualBO, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.virtualCardDisplayData!.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(displayData.virtualCardDisplayData!.bottomTitleText == nil)
        XCTAssert(displayData.virtualCardDisplayData!.bottomImageNamed == nil)

    }

    func test_givenVirtualCardBOAAtivationPurchaseTrueAndForeignPurchaseTrue_whenICallGenerateDisplayCardDataA_thenDisplayDataTextStatusEmpty() {

        //given
        let cardBO = CardsGenerator.getCardBOMultipleActivation(withStatusId: "OPERATIVE", withActivationId: "FOREIGN_PURCHASES_ACTIVATION", withIsActive: "true", withActivationId: "PURCHASES_ACTIVATION", withIsActive2: "true").cards[0]
        let virtualBO = CardsGenerator.getCardBOMultipleActivation(withStatusId: "OPERATIVE", withActivationId: "FOREIGN_PURCHASES_ACTIVATION", withIsActive: "true", withActivationId: "PURCHASES_ACTIVATION", withIsActive2: "true").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: virtualBO, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.virtualCardDisplayData!.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(displayData.virtualCardDisplayData!.bottomTitleText == nil)
        XCTAssert(displayData.virtualCardDisplayData!.bottomImageNamed == nil)

    }

    func test_givenVirtualCardBOAnActivationPurchaseTrueAndActivationsIsEmpty_whenICallGenerateDisplayCardDataA_thenDisplayDataTextStatusEmpty() {

        //given

        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "PURCHASES_ACTIVATION", withIsActive: "true").cards[0]
        cardBO.activations = Array()
        let virtualBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "PURCHASES_ACTIVATION", withIsActive: "true").cards[0]
        virtualBO.activations = Array()

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: virtualBO, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.virtualCardDisplayData!.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(displayData.virtualCardDisplayData!.bottomTitleText == nil)
        XCTAssert(displayData.virtualCardDisplayData!.bottomImageNamed == nil)

    }

    func test_givenVirtualCardBOAliasInCard_whenICallGenerateDisplayCardDataA_thenDisplayTheCardAliasValue() {

        //given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        let virtualBO = CardsGenerator.getCardBOWithAlias().cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: virtualBO, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.virtualCardDisplayData!.titleCard == cardBO.alias)

    }

    func test_givenVirtualCardBOAliasEmpty_whenICallGenerateDisplayCardDataA_thenDisplayTheCardTitleNameValue() {

        //given
        let cardBO = CardsGenerator.getCardBOWithAliasEmpty().cards[0]
        let virtualBO = CardsGenerator.getCardBOWithAliasEmpty().cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: virtualBO, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.virtualCardDisplayData!.titleCard == cardBO.title?.name)

    }

    func test_givenAnInOperativeCardAndNotSetteableToOperative_whenICallGenerateDisplayCardDataA_thenVirtualDisplayCardDataLeftButtonDisplayDataIsFilledAndRightNot() {

        //given
        let cardBO = CardsGenerator.getCardBOWithAliasEmpty().cards[0]
        let virtualBO = CardsGenerator.getCardBO(withStatusId: "INOPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        virtualBO.indicators[1].indicatorId = .settableToOperative
        virtualBO.indicators[1].isActive = false

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: virtualBO, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.virtualCardDisplayData!.leftButtonDisplayData != nil)
        XCTAssert(displayData.virtualCardDisplayData!.rightButtonDisplayData == nil)
    }

    func test_givenAnInOperativeCardAndNotSetteableToOperative_whenICallGenerateDisplayCardDataA_thenVirtualDisplayCardDataLeftButtonDisplayDataIsActivateCard() {

        //given
        let cardBO = CardsGenerator.getCardBOWithAliasEmpty().cards[0]
        let virtualBO = CardsGenerator.getCardBO(withStatusId: "INOPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        virtualBO.indicators[1].indicatorId = .settableToOperative
        virtualBO.indicators[1].isActive = false

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: virtualBO, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.virtualCardDisplayData!.leftButtonDisplayData?.imageNamed == ConstantsImages.Common.help_icon_on_off)
        XCTAssert(displayData.virtualCardDisplayData!.leftButtonDisplayData?.title == Localizables.cards.key_cards_how_activate_card_text)
        XCTAssert(displayData.virtualCardDisplayData!.leftButtonDisplayData?.actionType == .activateCardExplanation)
        XCTAssert(displayData.virtualCardDisplayData!.leftButtonDisplayData?.accesibility == ConstantsAccessibilities.CARD_BOTTOM_ACTION_BUTTON)

    }

    func test_givenAnInOperativeCardAndSetteableToOperative_whenICallGenerateDisplayCardDataA_thenVirtualDisplayCardDataLeftButtonDisplayDataIsFilledAndRightNot() {

        //given
        let cardBO = CardsGenerator.getCardBOWithAliasEmpty().cards[0]
        let virtualBO = CardsGenerator.getCardBO(withStatusId: "INOPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        virtualBO.indicators[1].indicatorId = .settableToOperative
        virtualBO.indicators[1].isActive = true

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: virtualBO, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.virtualCardDisplayData!.leftButtonDisplayData != nil)
        XCTAssert(displayData.virtualCardDisplayData!.rightButtonDisplayData == nil)
    }

    func test_givenAnInOperativeCardAndSetteableToOperative_whenICallGenerateDisplayCardDataA_thenVirtualDisplayCardDataLeftButtonDisplayDataIsActivateCardExplanation() {

        //given
        let cardBO = CardsGenerator.getCardBOWithAliasEmpty().cards[0]
        let virtualBO = CardsGenerator.getCardBO(withStatusId: "INOPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        virtualBO.indicators[1].indicatorId = .settableToOperative
        virtualBO.indicators[1].isActive = true

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: virtualBO, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.virtualCardDisplayData!.leftButtonDisplayData?.imageNamed == ConstantsImages.Card.creditcard_icon)
        XCTAssert(displayData.virtualCardDisplayData!.leftButtonDisplayData?.title == Localizables.cards.key_cards_activate_card_text)
        XCTAssert(displayData.virtualCardDisplayData!.leftButtonDisplayData?.actionType == .activateCard)
        XCTAssert(displayData.virtualCardDisplayData!.leftButtonDisplayData?.accesibility == ConstantsAccessibilities.CARD_BOTTOM_ACTION_BUTTON)

    }

    func test_givenAnOperativeCardAndReadableSecurityData_whenICallGenerateDisplayCardDataA_thenVirtualDisplayCardDataLeftAndRightButtonDisplayShouldBeFilled() {

        //given
        let cardBO = CardsGenerator.getCardBOWithAliasEmpty().cards[0]
        let virtualBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        virtualBO.indicators[1].indicatorId = .readableSecurityData
        virtualBO.indicators[1].isActive = true

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: virtualBO, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.virtualCardDisplayData!.leftButtonDisplayData != nil)
        XCTAssert(displayData.virtualCardDisplayData!.rightButtonDisplayData != nil)
    }

    func test_givenAnOperativeCardAndReadableSecurityData_whenICallGenerateDisplayCardDataA_thenVirtualDisplayCardDataLeftButtonDisplayDataIsCopyPanAndRightSeeCVV() {

        //given
        let cardBO = CardsGenerator.getCardBOWithAliasEmpty().cards[0]
        let virtualBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        virtualBO.indicators[1].indicatorId = .readableSecurityData
        virtualBO.indicators[1].isActive = true

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: virtualBO, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.virtualCardDisplayData!.leftButtonDisplayData?.imageNamed == nil)
        XCTAssert(displayData.virtualCardDisplayData!.leftButtonDisplayData?.title == Localizables.cards.key_cards_copy_text)
        XCTAssert(displayData.virtualCardDisplayData!.leftButtonDisplayData?.actionType == .copyPan)
        XCTAssert(displayData.virtualCardDisplayData!.leftButtonDisplayData?.accesibility == ConstantsAccessibilities.CARD_COPY_PAN_ACTION_BUTTON)

        XCTAssert(displayData.virtualCardDisplayData!.rightButtonDisplayData?.imageNamed == nil)
        XCTAssert(displayData.virtualCardDisplayData!.rightButtonDisplayData?.title == Localizables.cards.key_card_see_cvv_text)
        XCTAssert(displayData.virtualCardDisplayData!.rightButtonDisplayData?.actionType == .seeCVV)
        XCTAssert(displayData.virtualCardDisplayData!.rightButtonDisplayData?.accesibility == ConstantsAccessibilities.CARD_BOTTOM_ACTION_BUTTON_VER_CVV)

    }

    func test_givenAnOperativeCardANotReadableSecurityDataAndNotSetteableToOperative_whenICallGenerateDisplayCardDataA_thenVirtualDisplayCardDataLeftButtonDisplayDataIsFilledAndRightNot() {

        //given
        let cardBO = CardsGenerator.getCardBOWithAliasEmpty().cards[0]
        let virtualBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        virtualBO.indicators[1].indicatorId = .readableSecurityData
        virtualBO.indicators[1].isActive = false
        virtualBO.indicators[2].indicatorId = .settableToOperative
        virtualBO.indicators[2].isActive = false

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: virtualBO, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.virtualCardDisplayData!.leftButtonDisplayData != nil)
        XCTAssert(displayData.virtualCardDisplayData!.rightButtonDisplayData == nil)
    }

    func test_givenAnOperativeCardANotReadableSecurityDataAndNotSetteableToOperative_whenICallGenerateDisplayCardDataA_thenVirtualDisplayCardDataLeftButtonDisplayDataIsCopyPan() {

        //given
        let cardBO = CardsGenerator.getCardBOWithAliasEmpty().cards[0]
        let virtualBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        virtualBO.indicators[1].indicatorId = .readableSecurityData
        virtualBO.indicators[1].isActive = false
        virtualBO.indicators[2].indicatorId = .settableToOperative
        virtualBO.indicators[2].isActive = false

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: virtualBO, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.virtualCardDisplayData!.leftButtonDisplayData?.imageNamed == nil)
        XCTAssert(displayData.virtualCardDisplayData!.leftButtonDisplayData?.title == Localizables.cards.key_cards_copy_text)
        XCTAssert(displayData.virtualCardDisplayData!.leftButtonDisplayData?.actionType == .copyPan)
        XCTAssert(displayData.virtualCardDisplayData!.leftButtonDisplayData?.accesibility == ConstantsAccessibilities.CARD_COPY_PAN_ACTION_BUTTON)

    }

    func test_givenAnOperativeCardANotReadableSecurityDataAndSetteableToOperative_whenICallGenerateDisplayCardDataA_thenVirtualDisplayCardDataLeftButtonDisplayDataIsFilledAndRightNot() {

        //given
        let cardBO = CardsGenerator.getCardBOWithAliasEmpty().cards[0]
        let virtualBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        virtualBO.indicators[1].indicatorId = .readableSecurityData
        virtualBO.indicators[1].isActive = false
        virtualBO.indicators[2].indicatorId = .settableToOperative
        virtualBO.indicators[2].isActive = true

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: virtualBO, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.virtualCardDisplayData!.leftButtonDisplayData != nil)
        XCTAssert(displayData.virtualCardDisplayData!.rightButtonDisplayData == nil)
    }

    func test_givenAnOperativeCardANotReadableSecurityDataAndSetteableToOperative_whenICallGenerateDisplayCardDataA_thenVirtualDisplayCardDataLeftButtonDisplayDataIsCopyPan() {

        //given
        let cardBO = CardsGenerator.getCardBOWithAliasEmpty().cards[0]
        let virtualBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        virtualBO.indicators[1].indicatorId = .readableSecurityData
        virtualBO.indicators[1].isActive = false
        virtualBO.indicators[2].indicatorId = .settableToOperative
        virtualBO.indicators[2].isActive = true

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: virtualBO, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.virtualCardDisplayData!.leftButtonDisplayData?.imageNamed == nil)
        XCTAssert(displayData.virtualCardDisplayData!.leftButtonDisplayData?.title == Localizables.cards.key_cards_copy_text)
        XCTAssert(displayData.virtualCardDisplayData!.leftButtonDisplayData?.actionType == .copyPan)
        XCTAssert(displayData.virtualCardDisplayData!.leftButtonDisplayData?.accesibility == ConstantsAccessibilities.CARD_COPY_PAN_ACTION_BUTTON)

    }

    func test_givenAnyCardBOAndVirtualBOBlocked_whenICallGenerateDisplayCardDataA_thenVirtualDisplayCardDataLeftButtonDisplayDataIsGenerateDigitalCard() {

        //given
        let cardBO = CardsGenerator.getCardBOWithAliasEmpty().cards[0]
        let virtualBO = CardsGenerator.getCardBO(withStatusId: "BLOCKED", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        virtualBO.indicators[1].indicatorId = .readableSecurityData
        virtualBO.indicators[1].isActive = false
        virtualBO.indicators[2].indicatorId = .settableToOperative
        virtualBO.indicators[2].isActive = true

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: virtualBO, withStickerCardBO: nil)

        // then
        XCTAssert(displayData.virtualCardDisplayData!.leftButtonDisplayData?.imageNamed == ConstantsImages.Card.creditcard_icon)
        XCTAssert(displayData.virtualCardDisplayData!.leftButtonDisplayData?.title == Localizables.cards.key_card_generate_digital_card_text)
        XCTAssert(displayData.virtualCardDisplayData!.leftButtonDisplayData?.actionType == .generateDigitalCard)
        XCTAssert(displayData.virtualCardDisplayData!.leftButtonDisplayData?.accesibility == ConstantsAccessibilities.CARD_GENERATE_CARD_BUTTON)

    }

    // MARK: - generateDisplayCardDataA stickerCardDisplayData

    func test_givenAStickerCardBOAndCardBOAssociableToVirtualCard_whenICallGenerateDisplayCardDataA_thenVirtualInfoDisplayDataShouldBeNilAndstickerCardDisplayDataShouldBeFilled() {

        //given
        let cardBO = CardsGenerator.getThreeCards().cards[0]
        cardBO.indicators[2].indicatorId = .associableToVirtualCards
        cardBO.indicators[2].isActive = true

        let stickerCardBO = CardsGenerator.getThreeCards().cards[1]

        //When
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: stickerCardBO)

        // then
        XCTAssert(displayData.stickerCardDisplayData != nil)

    }

    func test_givenAStickerCardBOAndCardBONotAssociableToVirtualCard_whenICallGenerateDisplayCardDataA_thenVirtualInfoDisplayDataShouldBeNilAndstickerCardDisplayDataShouldBeFilled() {

        //given
        let cardBO = CardsGenerator.getThreeCards().cards[0]
        cardBO.indicators[2].indicatorId = .associableToVirtualCards
        cardBO.indicators[2].isActive = false

        let stickerCardBO = CardsGenerator.getThreeCards().cards[1]

        //When
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: stickerCardBO)

        // then
        XCTAssert(displayData.stickerCardDisplayData != nil)

    }

    func test_givenAStickerCardBOACardBO_whenICallGenerateDisplayCardDataA_thenDisplayDataPanShouldBeMatchWithCardBONumberValue() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        let stickerCardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        //When
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: stickerCardBO)

        // then
        XCTAssert(displayData.stickerCardDisplayData!.pan == DisplayCardData.formatTextUsingSeparator(text: cardBO.number))

    }

    func test_givenAStickerCardBOACardBO_whenICallGenerateDisplayCardDataA_thenDisplayDataPanShouldBeMatchWithCardBOHolderNameValueUppercased() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        let stickerCardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        //When
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: stickerCardBO)

        // then
        XCTAssert(displayData.stickerCardDisplayData!.holderName == cardBO.holderName.uppercased())

    }

    func test_givenAStickerCardBOACardBOAndCardsWithExpirationDateWithCorrectFormat_whenICallGenerateDisplayCardDataA_thenDisplayDataHasExpirationDateFilled() {

        //given
        let cardBO = CardsGenerator.getCardBO(withExpirationDate: "2020-12-14").cards[0]
        let stickerCardBO = CardsGenerator.getCardBO(withExpirationDate: "2020-12-14").cards[0]

        //When
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: stickerCardBO)

        // then
        XCTAssert(displayData.stickerCardDisplayData!.expirationDate != nil)

    }

    func test_givenAStickerCardBOACardBOAndCardsWithExpirationDateWithoutCorrectFormat_whenICallGenerateDisplayCardDataA_thenDisplayDataDoesNotHaveExpirationDateFilled() {

        //given
        let cardBO = CardsGenerator.getCardBO(withExpirationDate: "14/12").cards[0]
        let stickerCardBO = CardsGenerator.getCardBO(withExpirationDate: "14/12").cards[0]

        //When
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: stickerCardBO)

        // then
        XCTAssert(displayData.stickerCardDisplayData!.expirationDate == nil)

    }

    func test_givenAStickerCardBOACardBOAndCardsWithExpirationDateWithCorrectFormat_whenICallGenerateDisplayCardDataA_thenDisplayDataHasExpirationDateWithTheCorrectFormat() {

        //given
        let cardBO = CardsGenerator.getCardBO(withExpirationDate: "2020-12-14").cards[0]
        let stickerCardBO = CardsGenerator.getCardBO(withExpirationDate: "2020-12-14").cards[0]

        //When
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: stickerCardBO)

        // then
        XCTAssert(displayData.stickerCardDisplayData!.expirationDate == "12/20")

    }

    func test_givenAStickerCardBOACardBO_whenICallGenerateDisplayCardDataA_thenDisplayDataImageCardIsNotEmpty() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        let stickerCardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        //When
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: stickerCardBO)

        // then
        XCTAssertFalse((displayData.stickerCardDisplayData!.imageCard?.isEmpty)!)

    }

    func test_givenAStickerCardBOACard_whenICallGenerateDisplayCardDataA_thenDisplayDataImageCardIsTheSameThatCard() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        let stickerCardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: stickerCardBO)

        // then
        XCTAssert(displayData.stickerCardDisplayData!.imageCard == stickerCardBO.imageFront)
        XCTAssert(displayData.stickerCardDisplayData!.backImageCard == stickerCardBO.imageBack)

    }

    func test_givenAStickerCardBOACard_whenICallGenerateDisplayCardDataA_thenDisplayDataTextStatusEmpty() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        let stickerCardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: stickerCardBO)

        // then
        XCTAssert((displayData.stickerCardDisplayData!.imageStatus?.isEmpty)!)
        XCTAssert(displayData.stickerCardDisplayData!.bottomTitleText == nil)
        XCTAssert(displayData.stickerCardDisplayData!.bottomImageNamed == nil)

    }

    func test_givenAStickerCardBOACardBloqued_whenICallGenerateDisplayCardDataA_thenDisplayDataImageStatusBlocked() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "BLOCKED", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        let stickerCardBO = CardsGenerator.getCardBO(withStatusId: "BLOCKED", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: stickerCardBO)

        // then
        XCTAssert(displayData.stickerCardDisplayData!.imageStatus == ConstantsImages.Card.card_blocked)
        XCTAssert(displayData.stickerCardDisplayData!.bottomTitleText == nil)
        XCTAssert(displayData.stickerCardDisplayData!.bottomImageNamed == nil)

    }

    func test_givenAStickerCardBOACardCanceled_whenICallGenerateDisplayCardDataA_thenDisplayDataImageStatusBlocked() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "CANCELED", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        let stickerCardBO = CardsGenerator.getCardBO(withStatusId: "CANCELED", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: stickerCardBO)

        // then
        XCTAssert(displayData.stickerCardDisplayData!.imageStatus == ConstantsImages.Card.card_blocked)
        XCTAssert(displayData.stickerCardDisplayData!.bottomTitleText == nil)
        XCTAssert(displayData.stickerCardDisplayData!.bottomImageNamed == nil)

    }

    func test_givenAStickerCardBOACardPendingEmbossing_whenICallGenerateDisplayCardDataA_thenDisplayDataImageStatusDelivery() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "PENDING_EMBOSSING", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        let stickerCardBO = CardsGenerator.getCardBO(withStatusId: "PENDING_EMBOSSING", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: stickerCardBO)

        // then
        XCTAssert(displayData.stickerCardDisplayData!.imageStatus == ConstantsImages.Card.card_delivery)
        XCTAssert(displayData.stickerCardDisplayData!.bottomTitleText == nil)
        XCTAssert(displayData.stickerCardDisplayData!.bottomImageNamed == nil)

    }

    func test_givenAStickerCardBOACardPendingDelivery_whenICallGenerateDisplayCardDataA_thenDisplayDataImageStatusDelivery() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "PENDING_DELIVERY", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        let stickerCardBO = CardsGenerator.getCardBO(withStatusId: "PENDING_DELIVERY", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: stickerCardBO)

        // then
        XCTAssert(displayData.stickerCardDisplayData!.imageStatus == ConstantsImages.Card.card_delivery)
        XCTAssert(displayData.stickerCardDisplayData!.bottomTitleText == nil)
        XCTAssert(displayData.stickerCardDisplayData!.bottomImageNamed == nil)

    }

    func test_givenAStickerCardBOACardUnknowm_whenICallGenerateDisplayCardDataA_thenDisplayDataImageStatusBlocked() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "UNKNOWM", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        let stickerCardBO = CardsGenerator.getCardBO(withStatusId: "UNKNOWM", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: stickerCardBO)

        // then
        XCTAssert(displayData.stickerCardDisplayData!.imageStatus == ConstantsImages.Card.card_blocked)
        XCTAssert(displayData.stickerCardDisplayData!.bottomTitleText == nil)
        XCTAssert(displayData.stickerCardDisplayData!.bottomImageNamed == nil)

    }

    func test_givenAStickerCardBOACardBloqued_whenICallGenerateDisplayCardDataA_thenDisplayDataTextStatusOFF() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "false").cards[0]
        let stickerCardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "false").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: stickerCardBO)
        // then
        XCTAssert(displayData.stickerCardDisplayData!.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(displayData.stickerCardDisplayData!.bottomTitleText == nil)
        XCTAssert(displayData.stickerCardDisplayData!.bottomImageNamed == nil)

    }

    func test_givenAStickerCardBOACardPendingToActive_whenICallGenerateDisplayCardDataA_thenDisplayDataStatusPendingToActive() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "INOPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        let stickerCardBO = CardsGenerator.getCardBO(withStatusId: "INOPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: stickerCardBO)

        // then
        XCTAssert(displayData.stickerCardDisplayData!.imageStatus == ConstantsImages.Card.card_activate)

    }

    func test_givenAStickerCardBOACardWithEcommerceActivationFalse_whenICallGenerateDisplayCardDataA_thenDisplayDataTextStatusSemiOff() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ECOMMERCE_ACTIVATION", withIsActive: "false").cards[0]
        let stickerCardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ECOMMERCE_ACTIVATION", withIsActive: "false").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: stickerCardBO)

        // then
        XCTAssert(displayData.stickerCardDisplayData!.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(displayData.stickerCardDisplayData!.bottomTitleText == nil)
        XCTAssert(displayData.stickerCardDisplayData!.bottomImageNamed == nil)

    }

    func test_givenAStickerCardBOACardWithCashWithDrawalActivationFalse_whenICallGenerateDisplayCardDataA_thenDisplayDataTextStatusSemiOff() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "CASHWITHDRAWAL_ACTIVATION", withIsActive: "false").cards[0]
        let stickerCardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "CASHWITHDRAWAL_ACTIVATION", withIsActive: "false").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: stickerCardBO)

        // then
        XCTAssert(displayData.stickerCardDisplayData!.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(displayData.stickerCardDisplayData!.bottomTitleText == nil)
        XCTAssert(displayData.stickerCardDisplayData!.bottomImageNamed == nil)

    }

    func test_givenAStickerCardBOACardWithPurchaseActivationFalse_whenICallGenerateDisplayCardDataA_thenDisplayDataTextStatusSemiOff() {

        //given

        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "PURCHASES_ACTIVATION", withIsActive: "false").cards[0]
        let stickerCardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "PURCHASES_ACTIVATION", withIsActive: "false").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: stickerCardBO)

        // then
        XCTAssert(displayData.stickerCardDisplayData!.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(displayData.stickerCardDisplayData!.bottomTitleText == nil)
        XCTAssert(displayData.stickerCardDisplayData!.bottomImageNamed == nil)

    }

    func test_givenAStickerCardBOACardWithForeignCashWithDrawalActivationFalse_whenICallGenerateDisplayCardDataA_thenDisplayDataTextStatusSemiOff() {

        //given

        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "FOREIGN_CASHWITHDRAWAL_ACTIVATION", withIsActive: "false").cards[0]
        let stickerCardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "FOREIGN_CASHWITHDRAWAL_ACTIVATION", withIsActive: "false").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: stickerCardBO)

        // then
        XCTAssert(displayData.stickerCardDisplayData!.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(displayData.stickerCardDisplayData!.bottomTitleText == nil)
        XCTAssert(displayData.stickerCardDisplayData!.bottomImageNamed == nil)

    }

    func test_givenAStickerCardBOACardWithForeignPurchaseActivationFalse_whenICallGenerateDisplayCardDataA_thenDisplayDataTextStatusSemiOff() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "FOREIGN_PURCHASES_ACTIVATION", withIsActive: "false").cards[0]
        let stickerCardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "FOREIGN_PURCHASES_ACTIVATION", withIsActive: "false").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: stickerCardBO)

        // then
        XCTAssert(displayData.stickerCardDisplayData!.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(displayData.stickerCardDisplayData!.bottomTitleText == nil)
        XCTAssert(displayData.stickerCardDisplayData!.bottomImageNamed == nil)

    }

    func test_givenAStickerCardBOAAtivationOnOffFalseAndForeignPurchaseFasle_whenICallGenerateDisplayCardDataA_thenDisplayDataTextStatusOff() {

        //given
        let cardBO = CardsGenerator.getCardBOMultipleActivation(withStatusId: "OPERATIVE", withActivationId: "FOREIGN_PURCHASES_ACTIVATION", withIsActive: "false", withActivationId: "ON_OFF", withIsActive2: "false").cards[0]
        let stickerCardBO = CardsGenerator.getCardBOMultipleActivation(withStatusId: "OPERATIVE", withActivationId: "FOREIGN_PURCHASES_ACTIVATION", withIsActive: "false", withActivationId: "ON_OFF", withIsActive2: "false").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: stickerCardBO)

        // then
        XCTAssert(displayData.stickerCardDisplayData!.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(displayData.stickerCardDisplayData!.bottomTitleText == nil)
        XCTAssert(displayData.stickerCardDisplayData!.bottomImageNamed == nil)

    }

    func test_givenAStickerCardBOAAtivationOnOffFalseAndForeignPurchaseTrue_whenICallGenerateDisplayCardDataA_thenDisplayDataTextStatusSemiOff() {

        //given
        let cardBO = CardsGenerator.getCardBOMultipleActivation(withStatusId: "OPERATIVE", withActivationId: "FOREIGN_PURCHASES_ACTIVATION", withIsActive: "false", withActivationId: "ON_OFF", withIsActive2: "true").cards[0]
        let stickerCardBO = CardsGenerator.getCardBOMultipleActivation(withStatusId: "OPERATIVE", withActivationId: "FOREIGN_PURCHASES_ACTIVATION", withIsActive: "false", withActivationId: "ON_OFF", withIsActive2: "true").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: stickerCardBO)

        // then
        XCTAssert(displayData.stickerCardDisplayData!.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(displayData.stickerCardDisplayData!.bottomTitleText == nil)
        XCTAssert(displayData.stickerCardDisplayData!.bottomImageNamed == nil)

    }

    func test_givenAStickerCardBOAAtivationPurchaseFalseAndForeignPurchasefalse_whenICallGenerateDisplayCardDataA_thenDisplayDataTextStatusSemiOff() {

        //given
        let cardBO = CardsGenerator.getCardBOMultipleActivation(withStatusId: "OPERATIVE", withActivationId: "FOREIGN_PURCHASES_ACTIVATION", withIsActive: "false", withActivationId: "PURCHASES_ACTIVATION", withIsActive2: "false").cards[0]
        let stickerCardBO = CardsGenerator.getCardBOMultipleActivation(withStatusId: "OPERATIVE", withActivationId: "FOREIGN_PURCHASES_ACTIVATION", withIsActive: "false", withActivationId: "PURCHASES_ACTIVATION", withIsActive2: "false").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: stickerCardBO)

        // then
        XCTAssert(displayData.stickerCardDisplayData!.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(displayData.stickerCardDisplayData!.bottomTitleText == nil)
        XCTAssert(displayData.stickerCardDisplayData!.bottomImageNamed == nil)

    }

    func test_givenAStickerCardBOAAtivationPurchaseTrueAndForeignPurchaseTrue_whenICallGenerateDisplayCardDataA_thenDisplayDataTextStatusEmpty() {

        //given
        let cardBO = CardsGenerator.getCardBOMultipleActivation(withStatusId: "OPERATIVE", withActivationId: "FOREIGN_PURCHASES_ACTIVATION", withIsActive: "true", withActivationId: "PURCHASES_ACTIVATION", withIsActive2: "true").cards[0]
        let stickerCardBO = CardsGenerator.getCardBOMultipleActivation(withStatusId: "OPERATIVE", withActivationId: "FOREIGN_PURCHASES_ACTIVATION", withIsActive: "true", withActivationId: "PURCHASES_ACTIVATION", withIsActive2: "true").cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: stickerCardBO)

        // then
        XCTAssert(displayData.stickerCardDisplayData!.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(displayData.stickerCardDisplayData!.bottomTitleText == nil)
        XCTAssert(displayData.stickerCardDisplayData!.bottomImageNamed == nil)

    }

    func test_givenAStickerCardBOAnActivationPurchaseTrueAndActivationsIsEmpty_whenICallGenerateDisplayCardDataA_thenDisplayDataTextStatusEmpty() {

        //given

        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "PURCHASES_ACTIVATION", withIsActive: "true").cards[0]
        cardBO.activations = Array()
        let stickerCardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "PURCHASES_ACTIVATION", withIsActive: "true").cards[0]
        stickerCardBO.activations = Array()

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: stickerCardBO)

        // then
        XCTAssert(displayData.stickerCardDisplayData!.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(displayData.stickerCardDisplayData!.bottomTitleText == nil)
        XCTAssert(displayData.stickerCardDisplayData!.bottomImageNamed == nil)

    }

    func test_givenAStickerCardBOAliasInCard_whenICallGenerateDisplayCardDataA_thenDisplayTheCardAliasValue() {

        //given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        let stickerCardBO = CardsGenerator.getCardBOWithAlias().cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: stickerCardBO)

        // then
        XCTAssert(displayData.stickerCardDisplayData!.titleCard == cardBO.alias)

    }

    func test_givenAStickerCardBOAliasEmpty_whenICallGenerateDisplayCardDataA_thenDisplayTheCardTitleNameValue() {

        //given
        let cardBO = CardsGenerator.getCardBOWithAliasEmpty().cards[0]
        let stickerCardBO = CardsGenerator.getCardBOWithAliasEmpty().cards[0]

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: stickerCardBO)

        // then
        XCTAssert(displayData.stickerCardDisplayData!.titleCard == cardBO.title?.name)

    }

    func test_givenAnInOperativeCardAndNotSetteableToOperative_whenICallGenerateDisplayCardDataA_thenStickerDisplayCardDataLeftButtonDisplayDataIsFilledAndRightNot() {

        //given
        let cardBO = CardsGenerator.getCardBOWithAliasEmpty().cards[0]
        let stickerCardBO = CardsGenerator.getCardBO(withStatusId: "INOPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        stickerCardBO.indicators[1].indicatorId = .settableToOperative
        stickerCardBO.indicators[1].isActive = false

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: stickerCardBO)

        // then
        XCTAssert(displayData.stickerCardDisplayData!.leftButtonDisplayData != nil)
        XCTAssert(displayData.stickerCardDisplayData!.rightButtonDisplayData == nil)
    }

    func test_givenAnInOperativeCardAndNotSetteableToOperative_whenICallGenerateDisplayCardDataA_thenStickerDisplayCardDataLeftButtonDisplayDataIsActivateCard() {

        //given
        let cardBO = CardsGenerator.getCardBOWithAliasEmpty().cards[0]
        let stickerCardBO = CardsGenerator.getCardBO(withStatusId: "INOPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        stickerCardBO.indicators[1].indicatorId = .settableToOperative
        stickerCardBO.indicators[1].isActive = false

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: stickerCardBO)

        // then
        XCTAssert(displayData.stickerCardDisplayData!.leftButtonDisplayData?.imageNamed == ConstantsImages.Common.help_icon_on_off)
        XCTAssert(displayData.stickerCardDisplayData!.leftButtonDisplayData?.title == Localizables.cards.key_cards_how_activate_card_text)
        XCTAssert(displayData.stickerCardDisplayData!.leftButtonDisplayData?.actionType == .activateCardExplanation)
        XCTAssert(displayData.stickerCardDisplayData!.leftButtonDisplayData?.accesibility == ConstantsAccessibilities.CARD_BOTTOM_ACTION_BUTTON)

    }

    func test_givenAnInOperativeCardAndSetteableToOperative_whenICallGenerateDisplayCardDataA_thenStickerDisplayCardDataLeftButtonDisplayDataIsFilledAndRightNot() {

        //given
        let cardBO = CardsGenerator.getCardBOWithAliasEmpty().cards[0]
        let stickerCardBO = CardsGenerator.getCardBO(withStatusId: "INOPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        stickerCardBO.indicators[1].indicatorId = .settableToOperative
        stickerCardBO.indicators[1].isActive = true

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: stickerCardBO)

        // then
        XCTAssert(displayData.stickerCardDisplayData!.leftButtonDisplayData != nil)
        XCTAssert(displayData.stickerCardDisplayData!.rightButtonDisplayData == nil)
    }

    func test_givenAnInOperativeCardAndSetteableToOperative_whenICallGenerateDisplayCardDataA_thenStickerDisplayCardDataLeftButtonDisplayDataIsActivateCardExplanation() {

        //given
        let cardBO = CardsGenerator.getCardBOWithAliasEmpty().cards[0]
        let stickerCardBO = CardsGenerator.getCardBO(withStatusId: "INOPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        stickerCardBO.indicators[1].indicatorId = .settableToOperative
        stickerCardBO.indicators[1].isActive = true

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: stickerCardBO)

        // then
        XCTAssert(displayData.stickerCardDisplayData!.leftButtonDisplayData?.imageNamed == ConstantsImages.Card.creditcard_icon)
        XCTAssert(displayData.stickerCardDisplayData!.leftButtonDisplayData?.title == Localizables.cards.key_cards_activate_card_text)
        XCTAssert(displayData.stickerCardDisplayData!.leftButtonDisplayData?.actionType == .activateCard)
        XCTAssert(displayData.stickerCardDisplayData!.leftButtonDisplayData?.accesibility == ConstantsAccessibilities.CARD_BOTTOM_ACTION_BUTTON)

    }

    func test_givenAnOperativeCardAndReadableSecurityData_whenICallGenerateDisplayCardDataA_thenStickerDisplayCardDataLeftAndRightButtonDisplayShouldBeFilled() {

        //given
        let cardBO = CardsGenerator.getCardBOWithAliasEmpty().cards[0]
        let stickerCardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        stickerCardBO.indicators[1].indicatorId = .readableSecurityData
        stickerCardBO.indicators[1].isActive = true

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: stickerCardBO)

        // then
        XCTAssert(displayData.stickerCardDisplayData!.leftButtonDisplayData != nil)
        XCTAssert(displayData.stickerCardDisplayData!.rightButtonDisplayData != nil)
    }

    func test_givenAnOperativeCardAndReadableSecurityData_whenICallGenerateDisplayCardDataA_thenStickerDisplayCardDataLeftButtonDisplayDataIsCopyPanAndRightSeeCVV() {

        //given
        let cardBO = CardsGenerator.getCardBOWithAliasEmpty().cards[0]
        let stickerCardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        stickerCardBO.indicators[1].indicatorId = .readableSecurityData
        stickerCardBO.indicators[1].isActive = true

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: stickerCardBO)

        // then
        XCTAssert(displayData.stickerCardDisplayData!.leftButtonDisplayData?.imageNamed == nil)
        XCTAssert(displayData.stickerCardDisplayData!.leftButtonDisplayData?.title == Localizables.cards.key_cards_copy_text)
        XCTAssert(displayData.stickerCardDisplayData!.leftButtonDisplayData?.actionType == .copyPan)
        XCTAssert(displayData.stickerCardDisplayData!.leftButtonDisplayData?.accesibility == ConstantsAccessibilities.CARD_COPY_PAN_ACTION_BUTTON)

        XCTAssert(displayData.stickerCardDisplayData!.rightButtonDisplayData?.imageNamed == nil)
        XCTAssert(displayData.stickerCardDisplayData!.rightButtonDisplayData?.title == Localizables.cards.key_card_see_cvv_text)
        XCTAssert(displayData.stickerCardDisplayData!.rightButtonDisplayData?.actionType == .seeCVV)
        XCTAssert(displayData.stickerCardDisplayData!.rightButtonDisplayData?.accesibility == ConstantsAccessibilities.CARD_BOTTOM_ACTION_BUTTON_VER_CVV)

    }

    func test_givenAnOperativeCardANotReadableSecurityDataAndNotSetteableToOperative_whenICallGenerateDisplayCardDataA_thenStickerDisplayCardDataLeftButtonDisplayDataIsFilledAndRightNot() {

        //given
        let cardBO = CardsGenerator.getCardBOWithAliasEmpty().cards[0]
        let stickerCardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        stickerCardBO.indicators[1].indicatorId = .readableSecurityData
        stickerCardBO.indicators[1].isActive = false
        stickerCardBO.indicators[2].indicatorId = .settableToOperative
        stickerCardBO.indicators[2].isActive = false

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: stickerCardBO)

        // then
        XCTAssert(displayData.stickerCardDisplayData!.leftButtonDisplayData != nil)
        XCTAssert(displayData.stickerCardDisplayData!.rightButtonDisplayData == nil)
    }

    func test_givenAnOperativeCardANotReadableSecurityDataAndNotSetteableToOperative_whenICallGenerateDisplayCardDataA_thenStickerDisplayCardDataLeftButtonDisplayDataIsCopyPan() {

        //given
        let cardBO = CardsGenerator.getCardBOWithAliasEmpty().cards[0]
        let stickerCardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        stickerCardBO.indicators[1].indicatorId = .readableSecurityData
        stickerCardBO.indicators[1].isActive = false
        stickerCardBO.indicators[2].indicatorId = .settableToOperative
        stickerCardBO.indicators[2].isActive = false

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: stickerCardBO)

        // then
        XCTAssert(displayData.stickerCardDisplayData!.leftButtonDisplayData?.imageNamed == nil)
        XCTAssert(displayData.stickerCardDisplayData!.leftButtonDisplayData?.title == Localizables.cards.key_cards_copy_text)
        XCTAssert(displayData.stickerCardDisplayData!.leftButtonDisplayData?.actionType == .copyPan)
        XCTAssert(displayData.stickerCardDisplayData!.leftButtonDisplayData?.accesibility == ConstantsAccessibilities.CARD_COPY_PAN_ACTION_BUTTON)

    }

    func test_givenAnOperativeCardANotReadableSecurityDataAndSetteableToOperative_whenICallGenerateDisplayCardDataA_thenStickerDisplayCardDataLeftButtonDisplayDataIsFilledAndRightNot() {

        //given
        let cardBO = CardsGenerator.getCardBOWithAliasEmpty().cards[0]
        let stickerCardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        stickerCardBO.indicators[1].indicatorId = .readableSecurityData
        stickerCardBO.indicators[1].isActive = false
        stickerCardBO.indicators[2].indicatorId = .settableToOperative
        stickerCardBO.indicators[2].isActive = true

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: stickerCardBO)

        // then
        XCTAssert(displayData.stickerCardDisplayData!.leftButtonDisplayData != nil)
        XCTAssert(displayData.stickerCardDisplayData!.rightButtonDisplayData == nil)
    }

    func test_givenAnOperativeCardANotReadableSecurityDataAndSetteableToOperative_whenICallGenerateDisplayCardDataA_thenStickerDisplayCardDataLeftButtonDisplayDataIsCopyPan() {

        //given
        let cardBO = CardsGenerator.getCardBOWithAliasEmpty().cards[0]
        let stickerCardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        stickerCardBO.indicators[1].indicatorId = .readableSecurityData
        stickerCardBO.indicators[1].isActive = false
        stickerCardBO.indicators[2].indicatorId = .settableToOperative
        stickerCardBO.indicators[2].isActive = true

        //when
        let displayData = DisplayCardDataA.generateDisplayCardDataA(fromCardBO: cardBO, withVirtualCardBO: nil, withStickerCardBO: stickerCardBO)

        // then
        XCTAssert(displayData.stickerCardDisplayData!.leftButtonDisplayData?.imageNamed == nil)
        XCTAssert(displayData.stickerCardDisplayData!.leftButtonDisplayData?.title == Localizables.cards.key_cards_copy_text)
        XCTAssert(displayData.stickerCardDisplayData!.leftButtonDisplayData?.actionType == .copyPan)
        XCTAssert(displayData.stickerCardDisplayData!.leftButtonDisplayData?.accesibility == ConstantsAccessibilities.CARD_COPY_PAN_ACTION_BUTTON)

    }

}
