//
//  BVATransactionsListPresenterTest.swift
//  shoppingapp
//
//  Created by jesus.martinez on 7/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import XCTest
import RxSwift
import CellsNativeComponents

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class CardsPresenterTest: XCTestCase {

    var sut: CardsPresenter<DummyView>!
    var dummyBusManager: DummyBusManager!
    var dummyConfiguration: ConfigurationDataManagerDummy!
    var dummySessionManager: DummySessionManager!

    override func setUp() {
        super.setUp()
        dummyBusManager = DummyBusManager()
        dummyConfiguration = ConfigurationDataManagerDummy()
        ConfigurationDataManagerDummy.sharedInstance = dummyConfiguration
        sut = CardsPresenter<DummyView>(busManager: dummyBusManager)
        
        DummySessionManager.setupMock()
        dummySessionManager = (SessionDataManager.sessionDataInstance() as! DummySessionManager)
    }

    override func tearDown() {
        super.tearDown()
    }

    // MARK: ConfigureData

    func test_givenAPresenter_whenPresenterIsCreate_thenICallGetCurrencyBO() {

        // then
        XCTAssert(dummyConfiguration.isCalledGetCurrencyEntity == true)
    }

    func test_givenAPresenter_whenPresenterIsCreate_thenTheCurrencyBOHaveValuesCurrencyEntity() {

        // then
        XCTAssert(sut.currencyBO.code == dummyConfiguration.currencyEntity?.code)
    }

    // MARK: - OnMessageWithCardsCards

    func test_givenAPresenter_whenIReceiveModel_thenThePresenterRetriveCards() {

        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()

        sut.view = dummyView
        sut.interactor = dummyInteractor

        // when
        let cards = CardsGenerator.getCardBOWithAlias()

        sut.setModel(model: cards)

        // then
        XCTAssert(cards === sut.cards)

    }

    func test_givenCardsTransport_whenICallSetModel_thenShowCardIdShouldBeFilled() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        let cardsTransport = CardsTransport(cardBO: cardBO)

        // when
        sut.setModel(model: cardsTransport)

        // then
        XCTAssert(sut.showCardId != nil)

    }

    func test_givenCardsTransport_whenICallSetModel_thenShowCardIdShouldMatchWithCardIdOfCardBOOfCardsTransport() {

        // given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        let cardsTransport = CardsTransport(cardBO: cardBO)

        // when
        sut.setModel(model: cardsTransport)

        // then
        XCTAssert(sut.showCardId == cardBO.cardId)

    }

    // MARK: - receive model

    func test_givenCardsBOAndCardsNil_whenICallReceivedModel_thenCardsBOShouldBeFilled() {

        // given
        let cardsBO = CardsGenerator.getCardBOWithAlias()

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(sut.cards != nil)

    }

    func test_givenCardsBOAndCardsNil_whenICallReceivedModel_thenCardsBOShouldMatch() {

        // given
        let cardsBO = CardsGenerator.getCardBOWithAlias()

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(sut.cards === cardsBO)

    }

    func test_givenCardsBOAndCardsNotNilButDifferentTimestamp_whenICallReceivedModel_thenCardsBOShouldMatch() {

        // given
        let cardsBefore = CardsGenerator.getCardBOWithAlias()
        sut.cards = cardsBefore
        let cardsBO = CardsGenerator.getCardBOWithAlias()

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(sut.cards === cardsBO)
    }

    func test_givenCardsBOAndCardsNotNilAndSameTimestamp_whenICallReceivedModel_thenCardsBOShouldMatch() {

        // given
        let cardsBO = CardsGenerator.getCardBOWithAlias()
        sut.cards = cardsBO

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(sut.cards === cardsBO)
    }

    func test_givenACardsBOAndCardsNil_whenICallReceiveModel_thenThePresenterCallToShowData() {

        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()

        sut.view = dummyView
        sut.interactor = dummyInteractor

        //given
        let cardsBO = CardsGenerator.getCardBOWithAlias()

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.showInfoDataIsCalled == true)

    }

    func test_givenACardsBOAndCardsNotNilWithTimestampDifferent_whenICallReceiveModel_thenThePresenterCallToShowData() {

        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()

        sut.view = dummyView
        sut.interactor = dummyInteractor

        //given
        let cardsBO = CardsGenerator.getCardBOWithAlias()
        sut.cards = CardsGenerator.getCardBOWithAlias()

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.showInfoDataIsCalled == true)

    }

    func test_givenACardsBOAndCardsNotNilWithTimestampDifferent_whenICallReceiveModel_thenThePresenterDoNotCallToShowData() {

        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()

        sut.view = dummyView
        sut.interactor = dummyInteractor

        //given
        let cardsBO = CardsGenerator.getCardBOWithAlias()
        sut.cards = cardsBO

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.showInfoDataIsCalled == false)

    }

    func test_givenACardsBOAndCardsNil_whenICallReceiveModel_thenDisplayCardDataPanShouldBeMatchWithCardBONumberValue() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let cardsBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        let cardBO = cardsBO.cards[0]

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.pan == DisplayCardData.formatTextUsingSeparator(text: cardBO.number))

    }

    func test_givenACardsBOAndCardsNil_whenICallReceiveModel_thenDisplayCardDataPanShouldBeMatchWithCardBOHolderNameValueUppercased() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let cardsBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        let cardBO = cardsBO.cards[0]
        cardBO.holderName = "Jose Maria Sanchez"

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.holderName == cardBO.holderName.uppercased())

    }

    func test_givenACardsBOAndCardsNilWithExpirationDateWithCorrectFormat_whenICallReceiveModel_thenDisplayCardDataHasExpirationDateFilled() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let cardsBO = CardsGenerator.getCardBO(withExpirationDate: "2020-12-14")

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.expirationDate != nil)

    }

    func test_givenACardsBOAndCardsNilWithExpirationDateWithoutCorrectFormat_whenICallReceiveModel_thenDisplayCardDataDoesNotHaveExpirationDateFilled() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let cardsBO = CardsGenerator.getCardBO(withExpirationDate: "14/12")

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.expirationDate == nil)

    }

    func test_givenACardsBOAndCardsNilWithExpirationDateWithCorrectFormat_whenICallReceiveModel_thenDisplayDataHasExpirationDateWithTheCorrectFormat() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let cardsBO = CardsGenerator.getCardBO(withExpirationDate: "2020-12-14")

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.expirationDate == "12/20")

    }

    func test_givenACardsBOAndCardsNil_whenICallReceiveModel_thenDisplayCardDataImageCardIsNotEmpty() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let cardsBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssertFalse((dummyView.diplayItems!.items![0].displayCardData!.imageCard?.isEmpty)!)

    }

    func test_givenACard_whenIReceiveModel_thenDisplayCardDataImageCardIsTheSameThatCard() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let cardsBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.imageCard == "imageFront.jpg")

    }

    func test_givenACard_whenIReceiveModel_thenDisplayCardDataTextStatusEmpty() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let cardsBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert((dummyView.diplayItems!.items![0].displayCardData!.imageStatus?.isEmpty)!)
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.bottomTitleText == nil)
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.bottomImageNamed == nil)

    }

    func test_givenACardBloqued_whenIReceiveModel_thenDisplayCardDataImageStatusBlocked() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let cardsBO = CardsGenerator.getCardBO(withStatusId: "BLOCKED", withActivationId: "ON_OFF", withIsActive: "true")

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.imageStatus == ConstantsImages.Card.card_blocked)
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.bottomTitleText == nil)
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.bottomImageNamed == nil)

    }

    func test_givenACardCanceled_whenIReceiveModel_thenDisplayCardDataImageStatusBlocked() {

        // given
        let dummyView = DummyView()

        sut.view = dummyView

        let cardsBO = CardsGenerator.getCardBO(withStatusId: "CANCELED", withActivationId: "ON_OFF", withIsActive: "true")

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.imageStatus == ConstantsImages.Card.card_blocked)
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.bottomTitleText == nil)
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.bottomImageNamed == nil)

    }

    func test_givenACardPendingEmbossing_whenIReceiveModel_thenDisplayCardDataImageStatusDelivery() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let cardsBO = CardsGenerator.getCardBO(withStatusId: "PENDING_EMBOSSING", withActivationId: "ON_OFF", withIsActive: "true")

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.imageStatus == ConstantsImages.Card.card_delivery)
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.bottomTitleText == nil)
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.bottomImageNamed == nil)

    }

    func test_givenACardPendingDelivery_whenIReceiveModel_thenDisplayCardDataImageStatusDelivery() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let cardsBO = CardsGenerator.getCardBO(withStatusId: "PENDING_DELIVERY", withActivationId: "ON_OFF", withIsActive: "true")

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.imageStatus == ConstantsImages.Card.card_delivery)
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.bottomTitleText == nil)
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.bottomImageNamed == nil)

    }

    func test_givenACardUnknowm_whenIReceiveModel_thenDisplayCardDataImageStatusBlocked() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let cardsBO = CardsGenerator.getCardBO(withStatusId: "UNKNOWM", withActivationId: "ON_OFF", withIsActive: "true")

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.imageStatus == ConstantsImages.Card.card_blocked)
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.bottomTitleText == nil)
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.bottomImageNamed == nil)

    }

    func test_givenACardBloqued_whenIReceiveModel_thenDisplayCardDataTextStatusOFF() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let cardsBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "false")

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.bottomTitleText == nil)
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.bottomImageNamed == nil)

    }

    func test_givenACardPendingToActive_whenIReceiveModel_thenDisplayCardDataStatusPendingToActive() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let cardsBO = CardsGenerator.getCardBO(withStatusId: "INOPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.imageStatus == ConstantsImages.Card.card_activate)
    }

    func test_givenACardWithEcommerceActivationFalse_whenIReceiveModel_thenDisplayCardDataTextStatusSemiOff() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let cardsBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ECOMMERCE_ACTIVATION", withIsActive: "false")

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.bottomTitleText == nil)
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.bottomImageNamed == nil)

    }

    func test_givenACardWithCashWithDrawalActivationFalse_whenIReceiveModel_thenDisplayCardDataTextStatusSemiOff() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let cardsBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "CASHWITHDRAWAL_ACTIVATION", withIsActive: "false")

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.bottomTitleText == nil)
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.bottomImageNamed == nil)

    }

    func test_givenACardWithPurchaseActivationFalse_whenIReceiveModel_thenDisplayCardDataTextStatusSemiOff() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let cardsBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "PURCHASES_ACTIVATION", withIsActive: "false")

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.bottomTitleText == nil)
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.bottomImageNamed == nil)

    }

    func test_givenACardWithForeignCashWithDrawalActivationFalse_whenIReceiveModel_thenDisplayCardDataTextStatusSemiOff() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let cardsBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "FOREIGN_CASHWITHDRAWAL_ACTIVATION", withIsActive: "false")

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.bottomTitleText == nil)
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.bottomImageNamed == nil)

    }

    func test_givenACardWithForeignPurchaseActivationFalse_whenIReceiveModel_thenDisplayCardDataTextStatusSemiOff() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let cardsBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "FOREIGN_PURCHASES_ACTIVATION", withIsActive: "false")

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.bottomTitleText == nil)
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.bottomImageNamed == nil)

    }

    func test_givenAAtivationOnOffFalseAndForeignPurchaseFasle_whenIReceiveModel_thenDisplayCardDataTextStatusOff() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        let cardsBO = CardsGenerator.getCardBOMultipleActivation(withStatusId: "OPERATIVE", withActivationId: "FOREIGN_PURCHASES_ACTIVATION", withIsActive: "false", withActivationId: "ON_OFF", withIsActive2: "false")

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.bottomTitleText == nil)
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.bottomImageNamed == nil)

    }

    func test_givenAAtivationOnOffFalseAndForeignPurchaseTrue_whenIReceiveModel_thenDisplayCardDataTextStatusSemiOff() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let cardsBO = CardsGenerator.getCardBOMultipleActivation(withStatusId: "OPERATIVE", withActivationId: "FOREIGN_PURCHASES_ACTIVATION", withIsActive: "false", withActivationId: "ON_OFF", withIsActive2: "true")

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.bottomTitleText == nil)
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.bottomImageNamed == nil)

    }

    func test_givenAAtivationPurchaseFalseAndForeignPurchasefalse_whenIReceiveModel_thenDisplayCardDataTextStatusSemiOff() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let cardsBO = CardsGenerator.getCardBOMultipleActivation(withStatusId: "OPERATIVE", withActivationId: "FOREIGN_PURCHASES_ACTIVATION", withIsActive: "false", withActivationId: "PURCHASES_ACTIVATION", withIsActive2: "false")

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.bottomTitleText == nil)
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.bottomImageNamed == nil)

    }

    func test_givenAAtivationPurchaseTrueAndForeignPurchaseTrue_whenIReceiveModel_thenDisplayCardDataTextStatusEmpty() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let cardsBO = CardsGenerator.getCardBOMultipleActivation(withStatusId: "OPERATIVE", withActivationId: "FOREIGN_PURCHASES_ACTIVATION", withIsActive: "true", withActivationId: "PURCHASES_ACTIVATION", withIsActive2: "true")

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.bottomTitleText == nil)
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.bottomImageNamed == nil)

    }

    func test_givenAnActivationPurchaseTrueAndActivationsIsEmpty_whenIReceiveModel_thenDisplayCardDataTextStatusEmpty() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let cardsBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "PURCHASES_ACTIVATION", withIsActive: "true")
        cardsBO.cards[0].activations = Array()

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.bottomTitleText == nil)
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.bottomImageNamed == nil)

    }

    func test_givenAliasInCard_whenIReceiveModel_thenDisplayCardDataTheCardAliasValue() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let cardsBO = CardsGenerator.getCardBOWithAlias()

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.titleCard == sut.cards?.cards[0].alias)

    }

    func test_givenAliasEmpty_whenIReceiveModel_thenDisplayCardDataTheCardTitleNameValue() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let cardsBO = CardsGenerator.getCardBOWithAliasEmpty()

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.titleCard == sut.cards?.cards[0].title?.name)

    }

    func test_givenACardBOWithOutCurrencies_whenIReceiveModel_thenTheFormatterWithValueCodeCurrencFromConfiguration() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let cardsBO = CardsGenerator.getCardBOWithOUTCurrencies()

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(sut.formatter?.codeCurrency == sut.currencyBO.code)

    }

    func test_givenAnInOperativeCardAndNotSetteableToOperative_whenIReceiveModel_thenLeftButtonDisplayDataIsFilledAndShouldBeActivateCardExplanationAndRightNot() {

        // given
        let cardsBO = CardsGenerator.getCardBO(withStatusId: "INOPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        cardsBO.cards[0].indicators[1].indicatorId = .settableToOperative
        cardsBO.cards[0].indicators[1].isActive = false

        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.leftButtonDisplayData != nil)
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.leftButtonDisplayData?.actionType == .activateCardExplanation)
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.rightButtonDisplayData == nil)

    }

    func test_givenAnInOperativeCardAndSetteableToOperative_whenIReceiveModel_thenLeftButtonDisplayDataIsFilledAndShouldBeActivateCardAndRightNot() {

        // given
        let cardsBO = CardsGenerator.getCardBO(withStatusId: "INOPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        cardsBO.cards[0].indicators[1].indicatorId = .settableToOperative
        cardsBO.cards[0].indicators[1].isActive = true

        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.leftButtonDisplayData != nil)
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.leftButtonDisplayData?.actionType == .activateCard)
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.rightButtonDisplayData == nil)

    }

    func test_givenAnOperativeCardAndReadableSecurityData_whenIReceiveModel_thenLeftAndRightButtonDisplayShouldBeFilledLeftIsCopyPanAndRightIsSeeCVV() {

        // given
        let cardsBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        cardsBO.cards[0].indicators[1].indicatorId = .readableSecurityData
        cardsBO.cards[0].indicators[1].isActive = true

        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.leftButtonDisplayData != nil)
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.leftButtonDisplayData?.actionType == .copyPan)
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.rightButtonDisplayData != nil)
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.rightButtonDisplayData?.actionType == .seeCVV)

    }

    func test_givenAnOperativeCardANotReadableSecurityDataAndNotSetteableToOperative_whenIReceiveModel_thenLeftButtonDisplayShouldBeFilledLeftIsCopyPanAndRightShouldBeNil() {

        // given
        let cardsBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        cardsBO.cards[0].indicators[1].indicatorId = .readableSecurityData
        cardsBO.cards[0].indicators[1].isActive = false
        cardsBO.cards[0].indicators[2].indicatorId = .settableToOperative
        cardsBO.cards[0].indicators[2].isActive = false

        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.leftButtonDisplayData != nil)
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.leftButtonDisplayData?.actionType == .copyPan)
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.rightButtonDisplayData == nil)

    }

    func test_givenAnOperativeCardANotReadableSecurityDataAndSetteableToOperative_whenIReceiveModel_thenLeftButtonDisplayShouldBeFilledLeftIsCopyPanAndRightShouldBeNil() {

        // given
        let cardsBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        cardsBO.cards[0].indicators[1].indicatorId = .readableSecurityData
        cardsBO.cards[0].indicators[1].isActive = false
        cardsBO.cards[0].indicators[2].indicatorId = .settableToOperative
        cardsBO.cards[0].indicators[2].isActive = true

        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.leftButtonDisplayData != nil)
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.leftButtonDisplayData?.actionType == .copyPan)
        XCTAssert(dummyView.diplayItems!.items![0].displayCardData!.rightButtonDisplayData == nil)

    }

    func test_givenAPresenter_whenIReceiveModel_thenThePresenterCallToShowDataWithDisplayData() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let cardsBO = CardsGenerator.getThreeCards()

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert((dummyView.diplayItems?.items?.count)! > 0)

    }

    func test_givenAPresenter_whenIReceiveModelAndNeedRefreshPagerViewFalse_thenThePresenterNotCallToShowDataWithDisplayData() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        sut.needRefreshPagerView = false

        let cardsBO = CardsGenerator.getThreeCards()

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.isCalledShowCardsAtIndex == false)

    }

    func test_givenAPresenter_whenIReceiveModelAndNeedRefreshPagerViewTrueAndNotShowCardIdSetted_thenThePresenterNotCallToShowDataWithDisplayData() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        sut.needRefreshPagerView = true

        let cardsBO = CardsGenerator.getThreeCards()

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.isCalledShowCardsAtIndex == false)

    }

    func test_givenAPresenter_whenIReceiveModelAndNeedRefreshPagerViewTrueAndShowCardIdSetted_thenThePresenterCallToShowDataWithDisplayData() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        sut.needRefreshPagerView = true

        let cardsBO = CardsGenerator.getThreeCards()
        sut.showCardId = cardsBO.cards[0].cardId

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.isCalledShowCardsAtIndex == true)

    }

    func test_givenAPresenter_whenIReceiveModelAndNeedRefreshPagerViewTrueAndShowCardIdSetted_thenViewHideCardInformationFromVisibleCellIsTrue() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        sut.needRefreshPagerView = true

        let cardsBO = CardsGenerator.getThreeCards()
        sut.showCardId = cardsBO.cards[0].cardId

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.hideCardInformationFromVisibleCell == true)

    }

    func test_givenAPresenter_whenIReceiveModelAndNeedRefreshPagerViewTrue_thenNeedRefreshPagerViewFalse() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        sut.needRefreshPagerView = true

        let cardsBO = CardsGenerator.getThreeCards()

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(sut.needRefreshPagerView == false)

    }

    // MARK: - goToTransactionForCards

    func test_givenACards_whenICallGoToTransactionsAtIndex_thenICallRouterPresentTransactionsList() {

        let cardIndex = 0

        // given
        sut.cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")

        // when
        sut.goToTransactionsForCard(atIndex: cardIndex)

        // then
        XCTAssert(dummyBusManager.isCalledNavigateNextCardsScreen == true)

    }

    func test_givenACards_whenICallGoToTransactionsAtIndex_thenIPublishCardBOOfCardAsIndex() {

        let index = 0

        let dummyView = DummyView()

        sut.view = dummyView

        //given
        sut.cards = CardsGenerator.getCardBOWithCardTypeWithOutRelatedCard(withCardType: "PREPAID_CARD")

        let cardBO = sut.cards!.cards[index]

        //when
        sut.goToTransactionsForCard(atIndex: index)

        //then
        XCTAssert(cardBO === dummyBusManager.dummyCardBO)

    }

    // MARK: - createDisplayDataItems

    func test_givenACardsInPresenter_whenICallCreateDisplayItems_thenHaveADisplayItem() {

        //given
        sut.cards = CardsGenerator.getCardBOWithCardTypeWithOutRelatedCard(withCardType: "PREPAID_CARD")

        //when
        let displayItems = sut.createDisplayDataItems()

        //then
        XCTAssert(displayItems.items?.count == 1)

    }

    func test_givenACardsInPresenter_whenICallCreateDisplayItems_thenDisplayItemHaveDisplayCard() {

        //given
        sut.cards = CardsGenerator.getCardBOWithCardTypeWithOutRelatedCard(withCardType: "PREPAID_CARD")

        //when
        let displayItems = sut.createDisplayDataItems()

        let displayItem = displayItems.items?[0]
        //then
        XCTAssert(displayItem?.displayCardData != nil)

    }

    // MARK: loadedTransactions

    func test_givenCardsBO_whenICallLoadedTransactionsForCardId_thenTransactionsBOOfCardBOWithTheSameCardIdThanParamaterShouldBeFilled() {

        // given
        sut.cards = CardsGenerator.getCardBOWithCardTypeWithOutRelatedCard(withCardType: "PREPAID_CARD")

        let cardBO = sut.cards!.cards[0]

        var dummyEntities = [TransactionEntity]()

        for _ in 0..<3 {

            dummyEntities.append(TransactionEntityDummyGenerator.transactionEntity())

        }

        let dummyTransactionsEntity = TransactionsEntity()
        dummyTransactionsEntity.status = 200
        dummyTransactionsEntity.data = dummyEntities
        let transactionsBO = TransactionsBO(transactionsEntity: dummyTransactionsEntity)

        // when
        sut.transactionPresenter(TransactionPresenter(), loadedTransactions: transactionsBO, forCardId: cardBO.cardId)

        // then
        XCTAssert(cardBO.transactions != nil)

    }

    func test_givenCardsBO_whenICallLoadedTransactionsForCardId_thenTransactionsBOOfCardBOWithTheSameCardIdThanParamaterShouldMatchWithTransactionsBOAsParameter() {

        // given
        sut.cards = CardsGenerator.getCardBOWithCardTypeWithOutRelatedCard(withCardType: "PREPAID_CARD")

        let cardBO = sut.cards!.cards[0]

        var dummyEntities = [TransactionEntity]()

        for _ in 0..<3 {

            dummyEntities.append(TransactionEntityDummyGenerator.transactionEntity())

        }

        let dummyTransactionsEntity = TransactionsEntity()
        dummyTransactionsEntity.status = 200
        dummyTransactionsEntity.data = dummyEntities
        let transactionsBO = TransactionsBO(transactionsEntity: dummyTransactionsEntity)

        // when
        sut.transactionPresenter(TransactionPresenter(), loadedTransactions: transactionsBO, forCardId: cardBO.cardId)

        // then
        XCTAssert(cardBO.transactions === transactionsBO)
        XCTAssert(cardBO.transactions?.transactions.count == transactionsBO.transactions.count)

    }

    func test_givenCardsBO_whenICallLoadedTransactionsForCardIdWithAndInvalidCardId_thenAnyCardBOShouldSetTransactionsBOAsParameter() {

        // given
        sut.cards = CardsGenerator.getCardBOWithCardTypeWithOutRelatedCard(withCardType: "PREPAID_CARD")

        var dummyEntities = [TransactionEntity]()

        for _ in 0..<3 {

            dummyEntities.append(TransactionEntityDummyGenerator.transactionEntity())

        }

        let dummyTransactionsEntity = TransactionsEntity()
        dummyTransactionsEntity.status = 200
        dummyTransactionsEntity.data = dummyEntities
        let transactionsBO = TransactionsBO(transactionsEntity: dummyTransactionsEntity)

        // when
        sut.transactionPresenter(TransactionPresenter(), loadedTransactions: transactionsBO, forCardId: "Invalid_card_id")

        // then
        for cardBO in sut.cards!.cards {

            XCTAssert(cardBO.transactions == nil)

        }

    }

    // MARK: card showed at index

    func test_givenACardsInPresenter_whenICallCardShowedAtIndex_thenICallViewShowTransactionsWithDisplayData() {

        let dummyView = DummyView()

        sut.view = dummyView

        //given
        sut.cards = CardsGenerator.getCardBOWithCardTypeWithOutRelatedCard(withCardType: "PREPAID_CARD")

        //when
        sut.cardShowed(atIndex: 0)

        //then
        XCTAssert(dummyView.isCalledShowTransactionsWithDisplayData == true)

    }

    func test_givenACardsAndMaximumNumberAndCardWithoutTransactionsBO_whenICallCardShowedAtIndex_thenDisplayTransactionsCardMaximumNumberAndCardIdAndTransactionsBOShouldMatch() {

        let dummyView = DummyView()

        sut.view = dummyView

        //given
        sut.cards = CardsGenerator.getCardBOWithCardTypeWithOutRelatedCard(withCardType: "PREPAID_CARD")

        //when
        sut.cardShowed(atIndex: 0)

        //then
        XCTAssert(dummyView.transactionsDisplayData!.cardId == sut.cards?.cards[0].cardId)
        XCTAssert(dummyView.transactionsDisplayData!.maximumNumberOfTransactions == sut.maximumTransactionsInTransactionView)
        XCTAssert(dummyView.transactionsDisplayData!.transactionsBO == nil)

    }

    func test_givenACardsAndMaximumNumberAndCardWithTransactionsBO_whenICallCardShowedAtIndex_thenDisplayTransactionsCardMaximumNumberAndCardIdAndTransactionsBOShouldMatch() {

        let dummyView = DummyView()
        let cardIndex = 0

        sut.view = dummyView

        //given
        sut.cards = CardsGenerator.getCardBOWithCardTypeWithOutRelatedCard(withCardType: "PREPAID_CARD")

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.transactionsDisplayData!.cardId == sut.cards?.cards[0].cardId)
        XCTAssert(dummyView.transactionsDisplayData!.maximumNumberOfTransactions == sut.maximumTransactionsInTransactionView)
        XCTAssert(dummyView.transactionsDisplayData!.transactionsBO === sut.cards!.cards[cardIndex].transactions)
        XCTAssert(dummyView.transactionsDisplayData!.transactionsBO?.transactions.count == sut.cards!.cards[cardIndex].transactions?.transactions.count)
        XCTAssert(dummyView.transactionsDisplayData!.showDirectAccess == sut.showDirectAccessToTransactions)

    }

    func test_givenACards_whenICallCardShowedAtIndex_thenICallViewShowNavigationTitle() {

        let dummyView = DummyView()
        sut.view = dummyView
        let cardIndex = 0

        //given
        sut.cards = CardsGenerator.getCardBOWithCardTypeWithOutRelatedCard(withCardType: "PREPAID_CARD")

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.isCalledShowNavigationTitle == true)

    }

    func test_givenACards_whenICallCardShowedAtIndexOfACardWithAlias_thenICallViewShowNavigationTitleWithCardAlias() {

        let dummyView = DummyView()
        sut.view = dummyView
        let cardIndex = 0

        //given
        sut.cards = CardsGenerator.getCardBOWithCardTypeWithOutRelatedCard(withCardType: "PREPAID_CARD")
        sut.cards!.cards[cardIndex].alias = "alias"

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.navigationTitle == sut.cards!.cards[cardIndex].alias)

    }

    func test_givenACards_whenICallCardShowedAtIndexOfACardWithoutAlias_thenICallViewShowNavigationTitleWithCardName() {

        let dummyView = DummyView()
        sut.view = dummyView
        let cardIndex = 0

        //given
        sut.cards = CardsGenerator.getCardBOWithCardTypeWithOutRelatedCard(withCardType: "PREPAID_CARD")
        sut.cards!.cards[cardIndex].alias = ""

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.navigationTitle == sut.cards!.cards[cardIndex].title?.name)

    }

    func test_givenACards_whenICallCardShowedAtIndex_thenICallViewShowBalancesInfo() {

        let dummyView = DummyView()
        sut.view = dummyView
        let cardIndex = 0

        //given
        sut.cards = CardsGenerator.getCardBOWithCardTypeWithOutRelatedCard(withCardType: "PREPAID_CARD")

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.isCalledShowBalancesInfo == true)

    }

    func test_givenACards_whenICallCardShowedAtIndex_thenICallViewShowBalancesInfoWithDisplayInfo() {

        let dummyView = DummyView()
        sut.view = dummyView
        let cardIndex = 0

        //given
        sut.cards = CardsGenerator.getCardBOWithCardTypeWithOutRelatedCard(withCardType: "PREPAID_CARD")

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.displayDataBalances != nil)

    }

    func test_givenACardWithCurrenciesWithIsMajorCLPAndAvailableBalances_whenICallCardShowedAtIndex_thenDisplayCardHaveAAmountHaveValue() {

        // given
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyView = DummyView()
        let cardIndex = 0

        sut.cards = cards
        sut.view = dummyView

        // when
        sut.cardShowed(atIndex: cardIndex)

        // then
        XCTAssert(dummyView.displayDataBalances!.amount != nil)

    }

    func test_givenACreditCardWithCurrenciesWithIsMajorErrorAndAvailableBalancesWithErrorCurrency_whenICallCardShowedAtIndex_thenDisplayCardHaveAAmountWithOutValue() {

        // given
        let cards = CardsGenerator.getCardBOMultipleActivation(withStatusId: "OPERATIVE", withActivationId: "FOREIGN_PURCHASES_ACTIVATION", withIsActive: "false", withActivationId: "ON_OFF", withIsActive2: "false")
        cards.cards.first?.cardType.id = .credit_card
        let dummyView = DummyView()
        let cardIndex = 0

        sut.cards = cards
        sut.view = dummyView

        // when
        sut.cardShowed(atIndex: cardIndex)

        // then
        XCTAssert(dummyView.displayDataBalances!.amount == nil)

    }

    func test_givenACardBOWithCurrenciesAndIsMajor_whenICallCardShowedAtIndex_thenTheFormatterWithValueCodeCurrencyIsTheSame() {

        // given
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyView = DummyView()
        let cardIndex = 0

        sut.cards = cards
        sut.view = dummyView

        // when
        sut.cardShowed(atIndex: cardIndex)

        // then
        XCTAssert(sut.formatter?.codeCurrency == dummyView.displayDataBalances!.cardBO?.currencies[1].currency)

    }

    func test_givenACreditCardBOWithoutAvailableCurrencies_whenICallCardShowedAtIndex_thenDisplayDataCardAmountIsNil() {

        // given
        let cards = CardsGenerator.getCardBOWWithOutAvailableCurrencies()
        cards.cards[0].cardType.id = .credit_card
        let dummyView = DummyView()
        let cardIndex = 0

        sut.cards = cards
        sut.view = dummyView

        // when
        sut.cardShowed(atIndex: cardIndex)

        // then
        XCTAssert(dummyView.displayDataBalances!.amount == nil)

    }
    
    func test_givenADebitCardBOWithoutGrantedCredits_whenICallCardShowedAtIndex_thenDisplayDataCardAmountIsNil() {
        
        // given
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        cards.cards[0].grantedCredits = [AmountCurrencyBO]()
        cards.cards[0].cardType.id = .debit_card
        let dummyView = DummyView()
        let cardIndex = 0
        
        sut.cards = cards
        sut.view = dummyView
        
        // when
        sut.cardShowed(atIndex: cardIndex)
        
        // then
        XCTAssert(dummyView.displayDataBalances!.amount == nil)
        
    }

    func test_givenACardBOWithCurrencyMajorAndAvailableBalances_whenICallCardShowedAtIndex_thenDisplayDataCardAmountIsNotNil() {

        // given
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyView = DummyView()
        let cardIndex = 0

        sut.cards = cards
        sut.view = dummyView

        // when
        sut.cardShowed(atIndex: cardIndex)

        // then
        XCTAssert(dummyView.displayDataBalances!.amount != nil)

    }
    
    func test_givenADebitCardBOWithCurrencyMajorAndAvailableBalances_whenICallCardShowedAtIndex_thenAmountShownShouldMatch() {
        
        // given
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        cards.cards.first?.cardType.id = .debit_card
        let dummyView = DummyView()
        let cardIndex = 0
        
        sut.cards = cards
        sut.view = dummyView
        
        // when
        sut.cardShowed(atIndex: cardIndex)
        
        let cardBO = sut.cards!.cards[0]
        
        let balanceCurrency = cardBO.availableBalance.currentBalances.first { $0.currency == cardBO.currencyMajor }?.currency
        let formatter = AmountFormatter(codeCurrency: balanceCurrency!)
        
        // then
        let amount = sut.cards?.cards[0].grantedCredits.first { $0.currency == balanceCurrency }?.amount
        XCTAssert(dummyView.displayDataBalances!.amount == formatter.attributtedText(forAmount: amount!, isNegative: false, bigFontSize: 36, smallFontSize: 22))

    }

    func test_givenACreditCardBOWithCurrencyMajorAndAvailableBalances_whenICallCardShowedAtIndex_thenICallAttributtedText() {

        // given
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        cards.cards.first?.cardType.id = .credit_card
        let dummyView = DummyView()
        let cardIndex = 0

        sut.cards = cards
        sut.view = dummyView

        // when
        sut.cardShowed(atIndex: cardIndex)

        let formatter = AmountFormatter(codeCurrency: (sut.cards?.cards[0].currencies[1].currency)!)

        // then
        XCTAssert(dummyView.displayDataBalances!.amount == formatter.attributtedText(forAmount: (sut.cards?.cards[0].availableBalance.currentBalances[1].amount)!, isNegative: true, bigFontSize: 36, smallFontSize: 22))

    }

    func test_givenACardBOWithOutCurrencyMajorAndAvailableBalances_whenICallCardShowedAtIndex_thenICallAttributtedText() {

        // given
        let cards = CardsGenerator.getCardBOWithOUTCurrencies()
        let dummyView = DummyView()
        let cardIndex = 0

        sut.cards = cards
        sut.view = dummyView

        // when
        sut.cardShowed(atIndex: cardIndex)

        let formatter = AmountFormatter(codeCurrency: sut.currencyBO.code)

        // then
        XCTAssert(dummyView.displayDataBalances!.amount == formatter.attributtedText(forAmount: (sut.cards?.cards[0].availableBalance.currentBalances[0].amount)!, isNegative: true, bigFontSize: 36, smallFontSize: 22))

    }

    func test_givenACardBOWithCardTypeCreditCard_whenICallCardShowedAtIndex_thenIHaveValue() {

        // given
        let cards = CardsGenerator.getCardBOWithCardType(withCardType: "CREDIT_CARD")
        let dummyView = DummyView()
        let cardIndex = 0

        sut.cards = cards
        sut.view = dummyView

        // when
        sut.cardShowed(atIndex: cardIndex)

        // then
        XCTAssert(dummyView.displayDataBalances!.balanceText != nil)

    }

    func test_givenACardBOWithCardTypeCreditCard_whenICallCardShowedAtIndex_thenIHaveTheCorrectValue() {

        // given
        let cards = CardsGenerator.getCardBOWithCardType(withCardType: "CREDIT_CARD")
        let dummyView = DummyView()
        let cardIndex = 0

        sut.cards = cards
        sut.view = dummyView

        // when
        sut.cardShowed(atIndex: cardIndex)

        // then
        XCTAssert(dummyView.displayDataBalances!.balanceText == Localizables.cards.key_cards_availablecredit_text)

    }

    func test_givenACardBOWithCardTypeDebitCard_whenICallCardShowedAtIndex_thenIHaveValue() {

        // given
        let cards = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD")
        let dummyView = DummyView()
        let cardIndex = 0

        sut.cards = cards
        sut.view = dummyView

        // when
        sut.cardShowed(atIndex: cardIndex)

        // then
        XCTAssert(dummyView.displayDataBalances!.balanceText != nil)

    }

    func test_givenACardBOWithCardTypeDebitCard_whenICallCardShowedAtIndex_thenIHaveTheCorrectValue() {

        // given
        let cards = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD")
        let dummyView = DummyView()
        let cardIndex = 0

        sut.cards = cards
        sut.view = dummyView

        // when
        sut.cardShowed(atIndex: cardIndex)

        // then
        XCTAssert(dummyView.displayDataBalances!.balanceText == Localizables.cards.key_cards_availablebalance_text)

    }

    func test_givenACardBOWithCardTypePrepaidCardWithRelatedContractAccount_whenICallCardShowedAtIndex_thenIHaveValue() {

        // given
        let cards = CardsGenerator.getCardBOWithCardType(withCardType: "PREPAID_CARD")
        let dummyView = DummyView()
        let cardIndex = 0

        sut.cards = cards
        sut.view = dummyView

        // when
        sut.cardShowed(atIndex: cardIndex)

        // then
        XCTAssert(dummyView.displayDataBalances!.balanceText != nil)
        XCTAssert(dummyView.displayDataBalances!.balance2Text != nil)
        XCTAssert(dummyView.displayDataBalances!.balance3Text != nil)
        XCTAssert(dummyView.displayDataBalances!.icon1Image != nil)
        XCTAssert(dummyView.displayDataBalances!.icon2Image != nil)

    }

    func test_givenACardBOWithCardTypePrepaidCardWithRelatedContractAccount_whenICallCardShowedAtIndex_thenIHaveTheCorrectValue() {

        // given
        let cards = CardsGenerator.getCardBOWithCardType(withCardType: "PREPAID_CARD")
        let dummyView = DummyView()
        let cardIndex = 0

        sut.cards = cards
        sut.view = dummyView

        // when
        sut.cardShowed(atIndex: cardIndex)

        let number: String = (sut.cards?.cards[0].relatedContracts[0].number)!

        let last4 = number.suffix(4)

        // then
        XCTAssert(dummyView.displayDataBalances!.balanceText == Localizables.cards.key_cards_linkedbalance_text)
        XCTAssert(dummyView.displayDataBalances!.balance2Text == Localizables.cards.key_cards_account_text)
        XCTAssert(dummyView.displayDataBalances!.balance3Text == String(last4))
        XCTAssert(dummyView.displayDataBalances!.icon1Image == ConstantsImages.Card.link_card_icon)
        XCTAssert(dummyView.displayDataBalances!.icon2Image == ConstantsImages.Common.bullet)
    }

    func test_givenACardBOWithCardTypePrepaidCardWithRelatedContractCard_whenICallCardShowedAtIndex_thenIHaveTheCorrectValue() {

        // given
        let cards = CardsGenerator.getCardBOWithCardTypeRelatedCard(withCardType: "PREPAID_CARD")
        let dummyView = DummyView()
        let cardIndex = 0

        sut.cards = cards
        sut.view = dummyView

        // when
        sut.cardShowed(atIndex: cardIndex)

        let number: String = (sut.cards?.cards[0].relatedContracts[0].number)!

        let last4 = number.suffix(4)

        // then
        XCTAssert(dummyView.displayDataBalances!.balanceText == Localizables.cards.key_cards_linkedbalance_text)
        XCTAssert(dummyView.displayDataBalances!.balance2Text == Localizables.cards.key_cards_card_text)
        XCTAssert(dummyView.displayDataBalances!.balance3Text == String(last4))
        XCTAssert(dummyView.displayDataBalances!.icon1Image == ConstantsImages.Card.link_card_icon)
        XCTAssert(dummyView.displayDataBalances!.icon2Image == ConstantsImages.Common.bullet)
    }

    func test_givenACardBOWithCardTypeDebitCardWithOutRelatedContract_whenICallCardShowedAtIndex_thenIHaveTheCorrectValue() {

        // given
        let cards = CardsGenerator.getCardBOWithCardTypeWithOutRelatedCard(withCardType: "PREPAID_CARD")
        let dummyView = DummyView()
        let cardIndex = 0

        sut.cards = cards
        sut.view = dummyView

        // when
        sut.cardShowed(atIndex: cardIndex)

        // then
        XCTAssert(dummyView.displayDataBalances!.balanceText == Localizables.cards.key_cards_prepaidbalance_text)

    }

    //Credit Card
    func test_givenACardBOWithCreditCardType_whenICallCardShowedAtIndex_thenIHaveTheCorrectLocalizableValues() {

        // given
        let cards = CardsGenerator.getCardBOWithCardType(withCardType: "CREDIT_CARD")
        let dummyView = DummyView()
        let cardIndex = 0

        sut.cards = cards
        sut.view = dummyView

        // when
        sut.cardShowed(atIndex: cardIndex)

        // then
        XCTAssert(dummyView.displayDataBalances!.balanceLimitTitle == Localizables.cards.key_cards_limit_text)
        XCTAssert(dummyView.displayDataBalances!.balanceConsumedTitle == Localizables.cards.key_cards_consumed_text)

    }

    func test_givenACardBOWithCreditCardTypeWithCurrency_whenICallCardShowedAtIndex_thenICallAttributtedTextForGrantedCredits() {

        // given
        let cards = CardsGenerator.getCardBOWithCardTypeWithCurrency(withCardType: "CREDIT_CARD")
        let dummyView = DummyView()
        let cardIndex = 0

        sut.cards = cards
        sut.view = dummyView

        // when
        sut.cardShowed(atIndex: cardIndex)

        let formatter = AmountFormatter(codeCurrency: (sut.cards?.cards[0].currencies[1].currency)!)

        // then
        XCTAssert((dummyView.displayDataBalances!.balanceLimitValue?.isEqual(to: formatter.attributtedText(forAmount: (sut.cards?.cards[0].grantedCredits[1].amount)!, isNegative: false, bigFontSize: 16, smallFontSize: 10)))!)

    }

    func test_givenACardBOWithCreditCardTypeWithCurrency_whenICallCardShowedAtIndex_thenICallAttributtedTextForDisposedCredits() {

        // given
        let cards = CardsGenerator.getCardBOWithCardTypeWithCurrency(withCardType: "CREDIT_CARD")
        let dummyView = DummyView()
        let cardIndex = 0

        sut.cards = cards
        sut.view = dummyView

        // when
        sut.cardShowed(atIndex: cardIndex)

        let formatter = AmountFormatter(codeCurrency: (sut.cards?.cards[0].currencies[1].currency)!)

        // then
        XCTAssert(dummyView.displayDataBalances!.balanceConsumedValue == formatter.attributtedText(forAmount: (sut.cards?.cards[0].disposedBalance.currentBalances[1].amount)!, isNegative: true, bigFontSize: 16, smallFontSize: 10))
    }

    func test_givenACardBOWithoutCurrencyMajorAndDisposedBalances_whenICallCardShowedAtIndex_thenICallAttributtedTextForDisposedCredits() {

        // given
        let cards = CardsGenerator.getCardBOWithCardType(withCardType: "CREDIT_CARD")
        let dummyView = DummyView()
        let cardIndex = 0

        sut.cards = cards
        sut.view = dummyView

        // when
        sut.cardShowed(atIndex: cardIndex)

        let formatter = AmountFormatter(codeCurrency: sut.currencyBO.code)

        // then
        XCTAssert(dummyView.displayDataBalances!.balanceConsumedValue == formatter.attributtedText(forAmount: (sut.cards?.cards[0].disposedBalance.currentBalances[0].amount)!, isNegative: false, bigFontSize: 16, smallFontSize: 10))

    }

    func test_givenACardBOWithoutCurrencyMajorAndGrantedBalance_whenICallCardShowedAtIndex_thenICallAttributtedTextForGrantedCredit() {

        // given
        let cards = CardsGenerator.getCardBOWithCardType(withCardType: "CREDIT_CARD")
        let dummyView = DummyView()
        let cardIndex = 0

        sut.cards = cards
        sut.view = dummyView

        // when
        sut.cardShowed(atIndex: cardIndex)

        let formatter = AmountFormatter(codeCurrency: sut.currencyBO.code)

        // then
        XCTAssert(dummyView.displayDataBalances!.balanceLimitValue == formatter.attributtedText(forAmount: (sut.cards?.cards[0].grantedCredits[0].amount)!, isNegative: false, bigFontSize: 16, smallFontSize: 10))

    }

    func test_givenACreditCardTypeWithCurrencyWithDisposedBalanceSmallerThanZero_whenICallCardShowedAtIndex_thenIGetProgressValues() {

        // given
        let cards = CardsGenerator.getCardBOWithCardTypeWithCurrency(withCardType: "CREDIT_CARD")
        let dummyView = DummyView()
        let cardIndex = 0

        sut.cards = cards
        sut.view = dummyView

        // when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert (dummyView.displayDataBalances!.balanceDisposed == 0.0 && dummyView.displayDataBalances!.balanceGranted == 1.0)

    }

    func test_givenACreditCardTypeWithoutCurrencyWithDisposedBalanceSmallerThanZero_whenICallCardShowedAtIndex_thenIGetProgressValues() {

        // given
        let cards = CardsGenerator.getCardBOWithOUTCurrencies()
        let dummyView = DummyView()
        let cardIndex = 0

        sut.cards = cards
        sut.view = dummyView

        // when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert (dummyView.displayDataBalances!.balanceDisposed == 0.0 && dummyView.displayDataBalances!.balanceGranted == 1.0)

    }

    func test_givenACreditCardTypeWithCurrencyWithDisposedBalanceCurrentBalancesDoesNotExist_whenICallCardShowedAtIndex_thenIGetProgressValues() {

        // given
        let cards = CardsGenerator.getCardBOWIthCreditCardWithoutCurrencyAndWithoutDisposed()
        let dummyView = DummyView()
        let cardIndex = 0

        sut.cards = cards
        sut.view = dummyView

        // when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert (dummyView.displayDataBalances!.balanceDisposed == 0.0 && dummyView.displayDataBalances!.balanceGranted == 1.0)

    }

    func test_givenACreditCardTypeWithoutCurrencyWithDisposedBalanceCurrentBalancesDoesNotExist_whenICallCardShowedAtIndex_thenIGetProgressValues() {

        // given
        let cards = CardsGenerator.getCardBOCreditCard()
        let dummyView = DummyView()
        let cardIndex = 0

        sut.cards = cards
        sut.view = dummyView

        // when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert (dummyView.displayDataBalances!.balanceDisposed == 0.0 && dummyView.displayDataBalances!.balanceGranted == 1.0)

    }
    func test_givenACreditCardTypeWithoutCurrencyWithDisposedBalanceCurrentBalancesNotFoundWithCountryCurrency_whenICallCardShowedAtIndex_thenIGetProgressValues() {

        // given
        let cards = CardsGenerator.getCreditCardBOWithoutCurrencyAndDoesNotExistDisposedCurrentBalanceCountryCurrency()
        let dummyView = DummyView()
        let cardIndex = 0

        sut.cards = cards
        sut.view = dummyView

        // when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert (dummyView.displayDataBalances!.balanceDisposed == 0.0 && dummyView.displayDataBalances!.balanceGranted == 1.0)
    }

    func test_givenACreditCardTypeWithCurrencyWithDisposedBalanceCurrentBalancesNotFoundWithCountryCurrency_whenICallCardShowedAtIndex_thenIGetProgressValues() {

        // given
        let cards = CardsGenerator.getCreditCardBOWithCurrencyAndDoesNotExistDisposedCurrentBalanceCountryCurrency()
        let dummyView = DummyView()
        let cardIndex = 0

        sut.cards = cards
        sut.view = dummyView

        // when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert (dummyView.displayDataBalances!.balanceDisposed == 0.0 && dummyView.displayDataBalances!.balanceGranted == 1.0)

    }

    func test_givenACreditCardTypeWithCurrencyWithGrantedCreditBalanceSmallerThanDisposedBalance_whenICallCardShowedAtIndex_thenIGetProgressValues() {

        // given
        let cards = CardsGenerator.getCardWithCardTypeWithCurrencyGrantedSmallerThanDisposed()
        let dummyView = DummyView()
        let cardIndex = 0

        sut.cards = cards
        sut.view = dummyView

        // when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert (dummyView.displayDataBalances!.balanceDisposed == 1.0 && dummyView.displayDataBalances!.balanceGranted == 1.0)

    }

    func test_givenACreditCardTypeWithoutCurrencyWithGrantedCreditBalanceSmallerThanDisposedBalance_whenICallCardShowedAtIndex_thenIGetProgressValues() {

        // given
        let cards = CardsGenerator.getCardBOWithCardTypeWithOutRelatedCard(withCardType: "CREDIT_CARD")
        let dummyView = DummyView()
        let cardIndex = 0

        sut.cards = cards
        sut.view = dummyView

        // when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert (dummyView.displayDataBalances!.balanceDisposed == 1.0 && dummyView.displayDataBalances!.balanceGranted == 1.0)

    }

    func test_givenACreditCardTypeWithCurrencyWithDisposedBalanceEqualToGrantedCredits_whenICallCardShowedAtIndex_thenIGetProgressValues() {

        // given
        let cards = CardsGenerator.getCardWithCardTypeWithCurrencyGrantedSmallerThanDisposed()
        cards.cards[0].grantedCredits[1].amount = 550
        let dummyView = DummyView()
        let cardIndex = 0

        sut.cards = cards
        sut.view = dummyView

        // when
        sut.cardShowed(atIndex: cardIndex)

        // then
        XCTAssert (dummyView.displayDataBalances!.balanceDisposed == 1.0 && dummyView.displayDataBalances!.balanceGranted == 1.0)

    }

    func test_givenACreditCardTypeWithoutCurrencyWhitDisposedBalanceEqualToGrantedCredits_whenICallCardShowedAtIndex_thenIGetProgressValues() {

        // given
        let cards = CardsGenerator.getCardBOWithCardType(withCardType: "CREDIT_CARD")
        cards.cards[0].grantedCredits[1].amount = 550
        let dummyView = DummyView()
        let cardIndex = 0

        sut.cards = cards
        sut.view = dummyView

        // when
        sut.cardShowed(atIndex: cardIndex)

        // then
        XCTAssert (dummyView.displayDataBalances!.balanceDisposed == 1.0 && dummyView.displayDataBalances!.balanceGranted == 1.0)

    }

    //3.equation
    func test_givenACreditCardTypeWithCurrencyWhitDisposedBalanceSmallerThanGrantCreditBalance_whenICallCardShowedAtIndex_thenIGetProgressValues() {

        // given
        let cards = CardsGenerator.getCardWithCardTypeWithCurrencyGrantedSmallerThanDisposed()
        cards.cards[0].grantedCredits[1].amount = 560
        let dummyView = DummyView()
        let cardIndex = 0

        sut.cards = cards
        sut.view = dummyView

        // when
        sut.cardShowed(atIndex: cardIndex)

        // then
        XCTAssert (dummyView.displayDataBalances!.balanceGranted == Float(truncating: (sut.cards?.cards[0].grantedCredits[1].amount)!))
        XCTAssert (dummyView.displayDataBalances!.balanceDisposed == Float(truncating: (sut.cards?.cards[0].disposedBalance.currentBalances[1].amount)!))

    }

    func test_givenACreditCardTypeWithoutCurrencyWhitDisposedBalanceSmallerThanGrantCreditBalance_whenICallCardShowedAtIndex_thenIGetProgressValues() {

        // given
        let cards = CardsGenerator.getCardBOWithCardTypeWithOutRelatedCard(withCardType: "CREDIT_CARD")
        cards.cards[0].grantedCredits[0].amount = 2200000
        let dummyView = DummyView()
        let cardIndex = 0

        sut.cards = cards
        sut.view = dummyView

        // when
        sut.cardShowed(atIndex: cardIndex)

        // then
        XCTAssert (dummyView.displayDataBalances!.balanceGranted == Float(truncating: (sut.cards?.cards[0].grantedCredits[0].amount)!))
        XCTAssert (dummyView.displayDataBalances!.balanceDisposed == Float(truncating: (sut.cards?.cards[0].disposedBalance.currentBalances[0].amount)!))

    }

    func test_givenACards_whenICallCardShowedAtIndex_thenICallViewShowOnOffInfo() {

        let dummyView = DummyView()
        sut.view = dummyView
        let cardIndex = 0

        //given
        sut.cards = CardsGenerator.getCardBOWithCardTypeWithOutRelatedCard(withCardType: "PREPAID_CARD")

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.isCalledShowOnOffInfo == true)

    }

    func test_givenACards_whenICallCardShowedAtIndex_thenICallViewShowOnOffInfoWithDisplayInfo() {

        let dummyView = DummyView()
        sut.view = dummyView
        let cardIndex = 0

        //given
        sut.cards = CardsGenerator.getCardBOWithCardTypeWithOutRelatedCard(withCardType: "PREPAID_CARD")

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.displayDataOnOff != nil)

    }

    func test_givenAnOperativeCardAndCardIsOn_whenICallCardShowedAtIndex_thenDisplayTheCorrectValues() {

        // given
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyView = DummyView()
        let cardIndex = 0

        sut.cards = cards
        sut.view = dummyView

        // when
        sut.cardShowed(atIndex: cardIndex)

        // then
        XCTAssert(dummyView.displayDataOnOff!.switchText == Localizables.cards.key_cards_off_text )
        XCTAssert(dummyView.displayDataOnOff!.switchImage == ConstantsImages.Card.on_off_switch_on)
        XCTAssert(dummyView.displayDataOnOff!.switchStatus == true)
        XCTAssert(dummyView.displayDataOnOff!.switchValue == true)
        XCTAssert(dummyView.displayDataOnOff!.accesibilityValue == "1")

    }

    func test_givenAnOperativeCardAndCardIsOff_whenICallCardShowedAtIndex_thenDisplayTheCorrectValues() {

        // given
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "false")
        let dummyView = DummyView()
        let cardIndex = 0

        sut.cards = cards
        sut.view = dummyView

        // when
        sut.cardShowed(atIndex: cardIndex)

        // then
        XCTAssert(dummyView.displayDataOnOff!.switchText == Localizables.cards.key_cards_on_text )
        XCTAssert(dummyView.displayDataOnOff!.switchImage == ConstantsImages.Card.on_off_switch_off)
        XCTAssert(dummyView.displayDataOnOff!.switchStatus == true)
        XCTAssert(dummyView.displayDataOnOff!.switchValue == false)
        XCTAssert(dummyView.displayDataOnOff!.accesibilityValue == "0")

    }

    func test_givenANonOperativeCard_whenICallCardShowedAtIndex_thenDisplayTheCorrectValues() {

        // given
        let cards = CardsGenerator.getCardBO(withStatusId: "INOPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyView = DummyView()
        let cardIndex = 0

        sut.cards = cards
        sut.view = dummyView

        // when
        sut.cardShowed(atIndex: cardIndex)

        // then
        XCTAssert(dummyView.displayDataOnOff!.switchText == Localizables.cards.key_cards_off_text)
        XCTAssert(dummyView.displayDataOnOff!.switchImage == ConstantsImages.Card.on_off_switch_on)
        XCTAssert(dummyView.displayDataOnOff!.switchStatus == false)
        XCTAssert(dummyView.displayDataOnOff!.switchValue == true)
        XCTAssert(dummyView.displayDataOnOff!.accesibilityValue == "1")
        
    }

    func test_givenAnInOperativeCardWithOnOffNotActive_whenICallCardShowedAtIndex_thenDisplayTheCorrectValues() {

        // given
        let cards = CardsGenerator.getCardBO(withStatusId: "INOPERATIVE", withActivationId: "ON_OFF", withIsActive: "false")
        let dummyView = DummyView()
        let cardIndex = 0

        sut.cards = cards
        sut.view = dummyView

        // when
        sut.cardShowed(atIndex: cardIndex)

        // then
        XCTAssert(dummyView.displayDataOnOff!.switchText == Localizables.cards.key_cards_on_text)
        XCTAssert(dummyView.displayDataOnOff!.switchImage == ConstantsImages.Card.on_off_switch_off)
        XCTAssert(dummyView.displayDataOnOff!.switchStatus == false)
        XCTAssert(dummyView.displayDataOnOff!.switchValue == false)
        XCTAssert(dummyView.displayDataOnOff!.accesibilityValue == "0")

    }

    func test_givenAInoperativeCard_whenICallCardShowedAtIndex_thenICallViewHideOperativeBar() {

        // given
        let cards = CardsGenerator.getCardBO(withStatusId: "INOPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyView = DummyView()
        let cardIndex = 0
        cards.cards[cardIndex].indicators[4].indicatorId = .cancelable
        cards.cards[cardIndex].indicators[4].isActive = false

        sut.cards = cards
        sut.view = dummyView

        // when
        sut.cardShowed(atIndex: cardIndex)

        // then
        XCTAssert(dummyView.isCalledHideOperativesBar == true)

    }

    func test_givenAInoperativeCard_whenICallCardShowedAtIndex_thenIDoNotCallViewShowOperativeBar() {

        // given
        let cards = CardsGenerator.getCardBO(withStatusId: "INOPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyView = DummyView()
        let cardIndex = 0
        cards.cards[cardIndex].indicators[4].indicatorId = .cancelable
        cards.cards[cardIndex].indicators[4].isActive = false

        sut.cards = cards
        sut.view = dummyView

        // when
        sut.cardShowed(atIndex: cardIndex)

        // then
        XCTAssert(dummyView.isCalledShowOperativeBarInfo == false)

    }

    func test_givenAnOperativeCard_whenICallCardShowedAtIndex_thenICallViewShowOperativesBar() {

        // given
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyView = DummyView()
        let cardIndex = 0

        sut.cards = cards
        sut.view = dummyView

        // when
        sut.cardShowed(atIndex: cardIndex)

        // then
        XCTAssert(dummyView.isCalledShowOperativeBarInfo == true)

    }

    func test_givenAnOperativeCard_whenICallCardShowedAtIndex_thenIDoNotCallViewHideOperativesBar() {

        // given
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyView = DummyView()
        let cardIndex = 0

        sut.cards = cards
        sut.view = dummyView

        // when
        sut.cardShowed(atIndex: cardIndex)

        // then
        XCTAssert(dummyView.isCalledHideOperativesBar == false)

    }

    func test_givenAnOperativeCard_whenICallCardShowedAtIndex_thenDisplayOperativesBarShouldHaveLimitsButton() {

        // given
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyView = DummyView()
        let cardIndex = 0

        sut.cards = cards
        sut.view = dummyView

        // when
        sut.cardShowed(atIndex: cardIndex)

        // then
        XCTAssert(dummyView.displayDataOperativesBarInfo!.buttons.count == 3)
        XCTAssert(dummyView.displayDataOperativesBarInfo!.buttons[1].imageNamed == ConstantsImages.Card.limits)
        XCTAssert(dummyView.displayDataOperativesBarInfo!.buttons[1].titleKey == Localizables.cards.key_cards_set_up_limits_text)
        XCTAssert(dummyView.displayDataOperativesBarInfo!.buttons[1].optionKey == .limits)

    }

    func test_givenABlockedCard_whenICallCardShowedAtIndex_thenICallViewShowOperativesBar() {

        // given
        let cards = CardsGenerator.getCardBO(withStatusId: "BLOCKED", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyView = DummyView()
        let cardIndex = 0

        sut.cards = cards
        sut.view = dummyView

        // when
        sut.cardShowed(atIndex: cardIndex)

        // then
        XCTAssert(dummyView.isCalledShowOperativeBarInfo == true)

    }

    func test_givenABlockedCard_whenICallCardShowedAtIndex_thenIDoNotCallViewHideOperativesBar() {

        // given
        let cards = CardsGenerator.getCardBO(withStatusId: "BLOCKED", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyView = DummyView()
        let cardIndex = 0

        sut.cards = cards
        sut.view = dummyView

        // when
        sut.cardShowed(atIndex: cardIndex)

        // then
        XCTAssert(dummyView.isCalledHideOperativesBar == false)

    }

    func test_givenABlockedCard_whenICallCardShowedAtIndex_thenIViewCallShowOperativeBar() {

        // given
        let cards = CardsGenerator.getCardBO(withStatusId: "BLOCKED", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyView = DummyView()
        let cardIndex = 0

        sut.cards = cards
        sut.view = dummyView

        // when
        sut.cardShowed(atIndex: cardIndex)

        // then
        XCTAssert(dummyView.isCalledShowOperativeBarInfo == true)

    }

    func test_givenABlockedCard_whenICallCardShowedAtIndex_thenIDoNotViewCallHideOperativeBar() {

        // given
        let cards = CardsGenerator.getCardBO(withStatusId: "BLOCKED", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyView = DummyView()
        let cardIndex = 0

        sut.cards = cards
        sut.view = dummyView

        // when
        sut.cardShowed(atIndex: cardIndex)

        // then
        XCTAssert(dummyView.isCalledHideOperativesBar == false)

    }

    func test_givenACanceledCard_whenICallCardShowedAtIndex_thenIViewCallShowOperativeBar() {

        // given
        let cards = CardsGenerator.getCardBO(withStatusId: "CANCELED", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyView = DummyView()
        let cardIndex = 0

        sut.cards = cards
        sut.view = dummyView

        // when
        sut.cardShowed(atIndex: cardIndex)

        // then
        XCTAssert(dummyView.isCalledShowOperativeBarInfo == true)

    }

    func test_givenACanceledCard_whenICallCardShowedAtIndex_thenIDoNotCallViewHideOperativesBar() {

        // given
        let cards = CardsGenerator.getCardBO(withStatusId: "CANCELED", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyView = DummyView()
        let cardIndex = 0

        sut.cards = cards
        sut.view = dummyView

        // when
        sut.cardShowed(atIndex: cardIndex)

        // then
        XCTAssert(dummyView.isCalledHideOperativesBar == false)

    }

    func test_givenACanceledCard_whenICallCardShowedAtIndex_thenDisplayOperativesBarShouldHaveLimitsButton() {

        // given
        let cards = CardsGenerator.getCardBO(withStatusId: "CANCELED", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyView = DummyView()
        let cardIndex = 0

        sut.cards = cards
        sut.view = dummyView

        // when
        sut.cardShowed(atIndex: cardIndex)

        // then
        XCTAssert(dummyView.displayDataOperativesBarInfo!.buttons.count == 2)
        XCTAssert(dummyView.displayDataOperativesBarInfo!.buttons[0].imageNamed == ConstantsImages.Card.limits)
        XCTAssert(dummyView.displayDataOperativesBarInfo!.buttons[0].titleKey == Localizables.cards.key_cards_set_up_limits_text)
        XCTAssert(dummyView.displayDataOperativesBarInfo!.buttons[0].optionKey == .limits)

    }

    func test_givenAPendingEmbossingCard_whenICallCardShowedAtIndex_thenIViewCallShowOperativeBar() {

        // given
        let cards = CardsGenerator.getCardBO(withStatusId: "PENDING_EMBOSSING", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyView = DummyView()
        let cardIndex = 0

        sut.cards = cards
        sut.view = dummyView

        // when
        sut.cardShowed(atIndex: cardIndex)

        // then
        XCTAssert(dummyView.isCalledShowOperativeBarInfo == true)
    }

    func test_givenAPendingEmbossingCard_whenICallCardShowedAtIndex_thenIDoNotCallViewHideOperativesBar() {

        // given
        let cards = CardsGenerator.getCardBO(withStatusId: "PENDING_EMBOSSING", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyView = DummyView()
        let cardIndex = 0

        sut.cards = cards
        sut.view = dummyView

        // when
        sut.cardShowed(atIndex: cardIndex)

        // then
        XCTAssert(dummyView.isCalledHideOperativesBar == false)
    }

    func test_givenAPendingDeliveryCard_whenICallCardShowedAtIndex_thenIViewCallShowOperativeBar() {

        // given
        let cards = CardsGenerator.getCardBO(withStatusId: "PENDING_DELIVERY", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyView = DummyView()
        let cardIndex = 0

        sut.cards = cards
        sut.view = dummyView

        // when
        sut.cardShowed(atIndex: cardIndex)

        // then
        XCTAssert(dummyView.isCalledShowOperativeBarInfo == true)

    }

    func test_givenAPendingDeliveryCard_whenICallCardShowedAtIndex_thenIDoNotCallViewHideOperativesBar() {

        // given
        let cards = CardsGenerator.getCardBO(withStatusId: "PENDING_DELIVERY", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyView = DummyView()
        let cardIndex = 0

        sut.cards = cards
        sut.view = dummyView

        // when
        sut.cardShowed(atIndex: cardIndex)

        // then
        XCTAssert(dummyView.isCalledHideOperativesBar == false)

    }

    // MARK: - getHelp

    func test_givenConfigurationWithValuesForKeyCard_whenICallGetHelp_thenICallShowHelpView() {
        //given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyPublicManager = DummyPublicManager()
        ConfigPublicManager.instance = dummyPublicManager

        dummyPublicManager.configs = ConfigGenerator.getHelpBO()

        //When
        sut.getHelp()

        //then
        XCTAssert(dummyView.isCalledHelpView == true)

    }

    func test_givenConfigurationWithValuesForKeyCard_whenICallGetHelp_thenHelpDisplayDataIsNotEmpty() {

        //given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyPublicManager = DummyPublicManager()
        ConfigPublicManager.instance = dummyPublicManager

        dummyPublicManager.configs = ConfigGenerator.getHelpBO()

        //when
        sut.getHelp()

        //then
        XCTAssert(dummyView.displayDataHelp != nil)

    }
    func test_givenConfigurationWithValuesForKeyCard_whenICallGetHelp_thenICallGetConfigBOBySectionAndCorrectKeyAndWithSectionValues() {
        //given

        let dummyPublicManager = DummyPublicManager()
        ConfigPublicManager.instance = dummyPublicManager

        dummyPublicManager.configs = ConfigGenerator.getHelpBO()

        //When
        sut.getHelp()

        //then
        XCTAssert(dummyPublicManager.isCalledGetConfigBOBySection == true)
        XCTAssert(dummyPublicManager.sentCardKey == "cards")

    }

    func test_givenNilConfigurationForKeyCard_whenICallGetHelp_thenIDontCallShowHelpView() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyPublicManager = DummyPublicManager()
        ConfigPublicManager.instance = dummyPublicManager

        dummyPublicManager.configs = nil

        //when
        sut.getHelp()

        //then
        XCTAssert(dummyView.isCalledHelpView == false)

    }

    func test_givenEmptyConfigurationForKeyCard_whenICallGetHelp_thenIDontCallShowHelpView() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyPublicManager = DummyPublicManager()
        ConfigPublicManager.instance = dummyPublicManager

        dummyPublicManager.configs!.items = [ConfigFileBO]()
        //when
        sut.getHelp()

        //then
        XCTAssert(dummyView.isCalledHelpView == false)

    }

    // MARK: goToSearchTransactionsForCard

    func test_givenACards_whenICallGoToSearchTransacactionsForCardAtIndex_thenICallBusManagerSecondScreen() {

        let index = 0

        let dummyView = DummyView()

        sut.view = dummyView

        //given
        sut.cards = CardsGenerator.getCardBOWithCardTypeWithOutRelatedCard(withCardType: "PREPAID_CARD")

        //when
        sut.goToSearchTransactionsForCard(atIndex: index)

        //then
        XCTAssert(dummyBusManager.isCalledNavigateSecondCardsScreen == true)

    }

    func test_givenACards_whenICallGoToSearchTransacactionsForCardAtIndex_thenIPublishCardBOOfCardAsIndex() {

        let index = 0

        let dummyView = DummyView()

        sut.view = dummyView

        //given
        sut.cards = CardsGenerator.getCardBOWithCardTypeWithOutRelatedCard(withCardType: "PREPAID_CARD")

        let cardBO = sut.cards!.cards[index]

        //when
        sut.goToSearchTransactionsForCard(atIndex: index)

        //then
        XCTAssert(cardBO === dummyBusManager.dummyCardsTransport.cardBO)

    }

    // MARK: transactionView error

    func test_givenAnError_whenICallTransactionPresenterError_thenErrorBOShouldBeFilled() {

        // given
        let errorBO = ErrorBO()

        // when
        sut.transactionPresenter(TransactionPresenter(), error: errorBO)

        // then
        XCTAssert(sut.errorBO != nil)

    }

    func test_givenAnError_whenICallTransactionPresenterError_thenErrorBOAndErrorAsParameterShouldMatch() {

        // given
        let errorBO = ErrorBO()

        // when
        sut.transactionPresenter(TransactionPresenter(), error: errorBO)

        // then
        XCTAssert(sut.errorBO === errorBO)

    }

    func test_givenAnError403AndErrorNotShown_whenICallTransactionPresenterError_thenICallViewShowError() {

        // given
        let errorBO = ErrorBO()
        errorBO.status = ConstantsHTTPCodes.STATUS_403
        errorBO.code = ErrorCode.expiredSession.rawValue
        errorBO.isErrorShown = false

        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.transactionPresenter(TransactionPresenter(), error: errorBO)

        // then
        XCTAssert(dummyView.isCalledShowError == true)

    }

    func test_givenAnErrorDifferentThan403AndErrorNotShown_whenICallTransactionPresenterError_thenIDoNotCallViewShowError() {

        // given
        let errorBO = ErrorBO()
        errorBO.isErrorShown = false

        let dummyView = DummyView()
        sut.view = dummyView

        for i in 0...600 where i != ConstantsHTTPCodes.STATUS_403 {

                // given
                errorBO.status = i
                // when
                sut.transactionPresenter(TransactionPresenter(), error: errorBO)

                // then
                XCTAssert(dummyView.isCalledShowError == false)

        }

    }

    func test_givenAnError403AndErrorShown_whenICallTransactionPresenterError_thenICallCloseSessionOfSessionManager() {

        // given
        let errorBO = ErrorBO()
        errorBO.status = ConstantsHTTPCodes.STATUS_403
        errorBO.code = ErrorCode.expiredSession.rawValue
        errorBO.isErrorShown = true

        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.transactionPresenter(TransactionPresenter(), error: errorBO)

        // then
        XCTAssert(dummySessionManager.isCalledCloseSession == true)

    }

    func test_givenAnErrorDifferentThan403AAndErrorShown_whenICallTransactionPresenterError_thenIDoNotCallViewShowError() {

        // given
        let errorBO = ErrorBO()
        errorBO.isErrorShown = true

        let dummyView = DummyView()
        sut.view = dummyView

        for i in 0...600 where i != ConstantsHTTPCodes.STATUS_403 {

                // given
                errorBO.status = i
                // when
                sut.transactionPresenter(TransactionPresenter(), error: errorBO)

                // then
                XCTAssert(dummySessionManager.isCalledCloseSession == false)
        }

    }

    func test_givenAnError403AndErrorShown_whenICallTransactionPresenterError_thenICallPresentLoginOfRouter() {

        // given
        let errorBO = ErrorBO()
        errorBO.status = ConstantsHTTPCodes.STATUS_403
        errorBO.code = ErrorCode.expiredSession.rawValue
        errorBO.isErrorShown = true

        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.transactionPresenter(TransactionPresenter(), error: errorBO)

        // then
        XCTAssert(dummyBusManager.isCalledExpiredSession == true)

    }

    func test_givenAnErrorDifferentThan403AndErrorShown_whenICallTransactionPresenterError_thenIDoNotCallPresentLoginOfRouter() {

        // given
        let errorBO = ErrorBO()
        errorBO.isErrorShown = true

        let dummyView = DummyView()
        sut.view = dummyView

        for i in 0...600 where i != ConstantsHTTPCodes.STATUS_403 {

                // given
                errorBO.status = i
                // when
                sut.transactionPresenter(TransactionPresenter(), error: errorBO)

                // then
                XCTAssert(dummyBusManager.isCalledExpiredSession == false)

        }

    }

    // MARK: - leftCardButtonPressed with copyPan option

    func test_givenAnOperativeCardAndDisplayLeftButtonItemToSeeCVV_whenIleftCardButtonAtIndex_thenIDoNotCallViewShowToastSuccess() {

        // given
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyView = DummyView()
        sut.view = dummyView

        sut.cards = cards

        let displayItems = sut.createDisplayDataItems()

        sut.displayItems = displayItems
        sut.displayItems.items![0].displayCardData?.leftButtonDisplayData?.actionType = .seeCVV

        // when
        sut.leftCardButtonPressed(atIndex: 0)

        // then
        XCTAssert(dummyView.isCalledShowSuccessToast == false)

    }

    func test_givenAnOperativeCardAndDisplayLeftButtonItemToActivateCard_whenIleftCardButtonAtIndex_thenIDoNotCallViewShowToastSuccess() {

        // given
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyView = DummyView()
        sut.view = dummyView

        sut.cards = cards

        let displayItems = sut.createDisplayDataItems()

        sut.displayItems = displayItems
        sut.displayItems.items![0].displayCardData?.leftButtonDisplayData?.actionType = .activateCard

        // when
        sut.leftCardButtonPressed(atIndex: 0)

        // then
        XCTAssert(dummyView.isCalledShowSuccessToast == false)

    }

    func test_givenAnOperativeCardAndDisplayLeftButtonItemToActivateCardExplanation_whenIleftCardButtonAtIndex_thenIDoNotCallViewShowToastSuccess() {

        // given
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyView = DummyView()
        sut.view = dummyView

        sut.cards = cards

        let displayItems = sut.createDisplayDataItems()

        sut.displayItems = displayItems
        sut.displayItems.items![0].displayCardData?.leftButtonDisplayData?.actionType = .activateCardExplanation

        // when
        sut.leftCardButtonPressed(atIndex: 0)

        // then
        XCTAssert(dummyView.isCalledShowSuccessToast == false)

    }

    func test_givenAnOperativeCardAndDisplayLeftButtonItemToCopyPan_whenIleftCardButtonAtIndex_thenICallViewShowToastSuccess() {

        // given
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyView = DummyView()
        sut.view = dummyView

        sut.cards = cards

        let displayItems = sut.createDisplayDataItems()

        sut.displayItems = displayItems
        sut.displayItems.items![0].displayCardData?.leftButtonDisplayData?.actionType = .copyPan

        // when
        sut.leftCardButtonPressed(atIndex: 0)

        // then
        XCTAssert(dummyView.isCalledShowSuccessToast == true)

    }

    func test_givenAnOperativeCardAndDisplayLeftButtonItemToCopyPan_whenIleftCardButtonAtIndex_thenICallViewShowToastSuccessWithCopySuccessText() {

        // given
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyView = DummyView()
        sut.view = dummyView

        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        sut.displayItems.items![0].displayCardData?.leftButtonDisplayData?.actionType = .copyPan

        // when
        sut.leftCardButtonPressed(atIndex: 0)

        // then
        XCTAssert(dummyView.toastMessage == Localizables.cards.key_cards_copy_success_text)

    }

    func test_givenAnOperativeCardAndDisplayLeftButtonItemToSeeCVV_whenIleftCardButtonAtIndex_thenICallViewCopyPan() {

        // given
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyView = DummyView()
        sut.view = dummyView

        sut.cards = cards

        let displayItems = sut.createDisplayDataItems()

        sut.displayItems = displayItems
        sut.displayItems.items![0].displayCardData?.leftButtonDisplayData?.actionType = .seeCVV

        // when
        sut.leftCardButtonPressed(atIndex: 0)

        // then
        XCTAssert(dummyView.isCalledCopyPan == false)

    }

    func test_givenAnOperativeCardAndDisplayLeftButtonItemToActivateCard_whenIleftCardButtonAtIndex_thenIDoNotCallViewCopyPan() {

        // given
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyView = DummyView()
        sut.view = dummyView

        sut.cards = cards

        let displayItems = sut.createDisplayDataItems()

        sut.displayItems = displayItems
        sut.displayItems.items![0].displayCardData?.leftButtonDisplayData?.actionType = .activateCard

        // when
        sut.leftCardButtonPressed(atIndex: 0)

        // then
        XCTAssert(dummyView.isCalledCopyPan == false)

    }

    func test_givenAnOperativeCardAndDisplayLeftButtonItemToActivateCard_whenIleftCardButtonAtIndex_thenPublishCardOperationInfo() {

        // given
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyView = DummyView()
        sut.view = dummyView

        sut.cards = cards

        let displayItems = sut.createDisplayDataItems()

        sut.displayItems = displayItems
        sut.displayItems.items![0].displayCardData?.leftButtonDisplayData?.actionType = .activateCard

        // when
        sut.leftCardButtonPressed(atIndex: 0)

        // then
        XCTAssert(dummyBusManager.isCalledPublishParamsOperationCardsInfo)

    }

    func test_givenAnOperativeCardAndDisplayLeftButtonItemToActivateCardExplanation_whenIleftCardButtonAtIndex_thenIDoNotCallViewCopyPan() {

        // given
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyView = DummyView()
        sut.view = dummyView

        sut.cards = cards

        let displayItems = sut.createDisplayDataItems()

        sut.displayItems = displayItems
        sut.displayItems.items![0].displayCardData?.leftButtonDisplayData?.actionType = .activateCardExplanation

        // when
        sut.leftCardButtonPressed(atIndex: 0)

        // then
        XCTAssert(dummyView.isCalledCopyPan == false)

    }

    func test_givenAnOperativeCardAndDisplayLeftButtonItemToCopyPan_whenIleftCardButtonAtIndex_thenICallViewCopyPan() {

        // given
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyView = DummyView()
        sut.view = dummyView

        sut.cards = cards

        let displayItems = sut.createDisplayDataItems()

        sut.displayItems = displayItems
        sut.displayItems.items![0].displayCardData?.leftButtonDisplayData?.actionType = .copyPan

        // when
        sut.leftCardButtonPressed(atIndex: 0)

        // then
        XCTAssert(dummyView.isCalledCopyPan == true)

    }

    func test_givenAnOperativeCardAndDisplayLeftButtonItemToCopyPan_whenIleftCardButtonAtIndex_thenICallViewCopyPanWithPanWithPanNumberTrimmedAsOfCardsAsParameter() {

        // given
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyView = DummyView()
        sut.view = dummyView

        sut.cards = cards

        let displayItems = sut.createDisplayDataItems()

        sut.displayItems = displayItems
        sut.displayItems.items![0].displayCardData?.leftButtonDisplayData?.actionType = .copyPan

        // when
        sut.leftCardButtonPressed(atIndex: 0)

        // then
        XCTAssert(dummyView.panCopied == sut.cards?.cards[0].number.replacingOccurrences(of: " ", with: ""))

    }
    
    // MARK: - leftCardButtonPressed (generate card)
    
    func test_givenAnyCardBO_whenICallLeftCardButtonPressedAtIndex_thenICallBusManagerPublishDataGenerateCard() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let cards = CardsGenerator.getCardBOWithAlias()
        sut.cards = cards
        
        let displayItems = sut.createDisplayDataItems()
        sut.displayItems = displayItems
        sut.displayItems.items![0].displayCardData?.leftButtonDisplayData?.actionType = .generateDigitalCard
        
        // when
        sut.leftCardButtonPressed(atIndex: 0)
        
        //then
        XCTAssert(dummyBusManager.isCalledPublishDataGenerateCard == true)
    }

    func test_givenAnyCardBO_whenICallLeftCardButtonPressedAtIndex_thenICallBusManagerPublishCardOperationInfo() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let cards = CardsGenerator.getCardBOWithAlias()
        sut.cards = cards

        let displayItems = sut.createDisplayDataItems()
        sut.displayItems = displayItems
        sut.displayItems.items![0].displayCardData?.leftButtonDisplayData?.actionType = .generateDigitalCard

        // when
        sut.leftCardButtonPressed(atIndex: 0)

        //then
        XCTAssert(dummyBusManager.isCalledPublishParamsOperationCardsInfo == true)
    }
    
    func test_givenAnyCardBO_whenICallLeftCardButtonPressedAtIndex_thenICallNavigateGenerateCard() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let cards = CardsGenerator.getCardBOWithAlias()
        sut.cards = cards
        
        let displayItems = sut.createDisplayDataItems()
        sut.displayItems = displayItems
        sut.displayItems.items![0].displayCardData?.leftButtonDisplayData?.actionType = .generateDigitalCard
        
        // when
        sut.leftCardButtonPressed(atIndex: 0)
        
        //then
        XCTAssert(dummyBusManager.isCalledEventNavToGenerateDigitalCard == true)
    }
    
    // MARK: - leftCardButtonPressed (activateCardExplanation)
    
    func test_givenAnyCardBOAndDisplayLeftButtonItemToActivateCardExplanation_whenICallLeftCardButtonPressedAtIndex_thenICallBusManagerDetailHelpScreen() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let cards = CardsGenerator.getCardBOWithAlias()
        sut.cards = cards
        
        let displayItems = sut.createDisplayDataItems()
        sut.displayItems = displayItems
        sut.displayItems.items![0].displayCardData?.leftButtonDisplayData?.actionType = .activateCardExplanation
        
        // when
        sut.leftCardButtonPressed(atIndex: 0)
        
        //then
        XCTAssert(dummyBusManager.isCalledNavigateToDetailHelpScreen == true)
    }
    
    func test_givenAnyCardBOAndDisplayLeftButtonItemToActivateCardExplanation_whenICallLeftCardButtonPressedAtIndex_thenICallPublishHelpDetailTransportWithHelpId() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let cards = CardsGenerator.getCardBOWithAlias()
        sut.cards = cards
        
        let displayItems = sut.createDisplayDataItems()
        sut.displayItems = displayItems
        sut.displayItems.items![0].displayCardData?.leftButtonDisplayData?.actionType = .activateCardExplanation
        
        // when
        sut.leftCardButtonPressed(atIndex: 0)
        
        //then
        XCTAssert(dummyBusManager.dummyHelpDetailTransport.helpId == sut.idHelpOnOff)
    }

    // MARK: - OnOff WS

    func test_givenAnOperativeCard_whenICallChangeCardStatus_thenICallTheInteractor() {

        // given
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")

        sut.view = dummyView
        sut.interactor = dummyInteractor

        // when
        sut.changeCardStatus(withCardStatus: false, andCard: cards.cards[0])

        // then
        XCTAssert(dummyInteractor.isCalledProvideCardStatus == true)

    }

    func test_givenAnOperativeCard_whenICallChangeCardStatus_thenThePresenterGetTheUpdatedActivations() {

        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")

        sut.view = dummyView
        sut.interactor = dummyInteractor

        // when
        sut.changeCardStatus(withCardStatus: false, andCard: cards.cards[0])

        // then
        XCTAssert(dummyInteractor.activations === sut.activations)

    }

    func test_givenAnOperativeCard_whenICallChangeCardStatusAndFail_thenICallDismisLoadingView() {

        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")

        sut.view = dummyView
        sut.interactor = dummyInteractor
        dummyInteractor.forceError = true

        // when
        sut.changeCardStatus(withCardStatus: false, andCard: cards.cards[0])

        // then
        XCTAssert(dummyBusManager.isCalledHideLoading == true)

    }

    func test_givenAnOperativeCard_whenICallChangeCardStatusAndFail_thenICallViewCheckError() {

        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")

        sut.view = dummyView
        sut.interactor = dummyInteractor
        dummyInteractor.forceError = true

        // when
        sut.changeCardStatus(withCardStatus: false, andCard: cards.cards[0])

        // then
        XCTAssert(dummyView.isCalledShowError == true)

    }

    func test_givenAnOperativeCard_whenICallChangeCardStatus_thenCardActivationsHasTheSameActivations() {

        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")

        sut.view = dummyView
        sut.interactor = dummyInteractor

        // when
        sut.changeCardStatus(withCardStatus: false, andCard: cards.cards[0])

        // then
        XCTAssert(cards.cards[0].activations[0] === sut.activations?.data)

    }

    func test_givenAnOperativeCard_whenICallChangeCardStatus_thenICallUpdateActivations() {

        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()

        let cardsEntity = CardsGenerator.getCardEntity(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyCardBO = DummyCardBO(cardEntity: cardsEntity.data![0])

        sut.view = dummyView
        sut.interactor = dummyInteractor

        // when
        sut.changeCardStatus(withCardStatus: false, andCard: dummyCardBO)

        // then
        XCTAssert(dummyCardBO.isCalledUpdateActivations == true)

    }

    func test_givenAnOperativeCard_whenICallChangeCardStatus_thenDummyActivationBOHasTheSameActivations() {

        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()

        let cardsEntity = CardsGenerator.getCardEntity(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyCardBO = DummyCardBO(cardEntity: cardsEntity.data![0])

        sut.view = dummyView
        sut.interactor = dummyInteractor

        // when
        sut.changeCardStatus(withCardStatus: false, andCard: dummyCardBO)

        // then
        XCTAssert(dummyCardBO.dummyActivationBO === sut.activations?.data)

    }

    func test_givenAnOperativeCard_whenICallChangeCardStatusAndGetCards_thenIUpdateOnOffAndCardData() {
        //given
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        sut.cards = cards

        var displayItemCard = DisplayItemCard()
        displayItemCard.displayCardData = DisplayCardData.generateDisplayCardData(fromCardBO: cards.cards[0])
        sut.displayItems.items?.append(displayItemCard)

        sut.view = dummyView
        sut.interactor = dummyInteractor

        // when
        sut.changeCardStatus(withCardStatus: false, andCard: cards.cards[0])

        // then
        XCTAssert(sut.cards!.cards[0].activations[0].isActive == false)

    }

    func test_givenAnOperativeCard_whenICallChangeCardStatusAndGetCards_thenIDontUpdateOnOffAndCardData() {
        //given
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()

        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        cards.cards[0].cardId = "3212"

        // when
        sut.view = dummyView
        sut.interactor = dummyInteractor

        sut.setModel(model: CardsGenerator.getCardBOWithAlias())

        sut.changeCardStatus(withCardStatus: false, andCard: cards.cards[0])

        // then
        XCTAssert(cards.cards[0].cardId != sut.cards?.cards[0].cardId)

    }

    func test_givenAnOperativeCard_whenICallChangeCardStatusToTurnOff_thenICallToastMessageCorrectly() {

        //given
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")

        sut.view = dummyView
        sut.interactor = dummyInteractor
        dummyView.toastMessage = Localizables.cards.key_cards_on_successful_text

        // when
        sut.changeCardStatus(withCardStatus: false, andCard: cards.cards[0])

        // then
        XCTAssert(dummyView.isCalledShowSuccessToast == true)

    }

    func test_givenAnOperativeCard_whenICallChangeCardStatusToTurnOn_thenICallToastMessageCorrectly() {

        //given
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "false")

        sut.view = dummyView
        sut.interactor = dummyInteractor
        dummyView.toastMessage = Localizables.cards.key_cards_off_successful_text

        // when
        sut.changeCardStatus(withCardStatus: true, andCard: cards.cards[0])

        // then
        XCTAssert(dummyView.isCalledShowSuccessToast == true)

    }
    func test_givenAnOperativeCard_whenICallChangeCardStatus_thenCallProvideUpdateCardWithTheSameCardIdAndStatusOn() {

        // given
        let dummyInteractor = DummyInteractor()
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "false")

        sut.interactor = dummyInteractor
        sut.cards = cards

        // when
        sut.changeCardStatus(withCardStatus: true, andCard: cards.cards[0])

        // then
        XCTAssert(dummyInteractor.cardId == sut.cards?.cards[0].cardId)
        XCTAssert( dummyInteractor.status == false )

    }
    func test_givenAnOperativeCard_whenICallGetChangeCardStatus_thenCallProvideUpdateCardWithTheStatusOff() {

        // given
        let dummyInteractor = DummyInteractor()
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "false")

        sut.interactor = dummyInteractor
        sut.cards = cards

        // when
        sut.changeCardStatus(withCardStatus: false, andCard: cards.cards[0])

        // then
        XCTAssert(dummyInteractor.cardId == sut.cards?.cards[0].cardId)
        XCTAssert(dummyInteractor.status == true )

    }

    func test_givenUpdateCardStatusSuccess_whenICallChangeCardsStatusAndUpdateCard_thenUpdateCardStatusAndSwitchStatus() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        let dummyInteractor = DummyInteractor()
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")

        sut.interactor = dummyInteractor
        sut.cards = cards
        dummyView.diplayItems = sut.createDisplayDataItems()

        // when
        sut.changeCardStatus(withCardStatus: false, andCard: cards.cards[0])

        // then
        XCTAssert(dummyView.showInfoDataIsCalled == true)

    }

    func test_givenUpdateCardStatusSuccess_whenICallChangeCardsStatusAndUpdateCard_thenIPublishEVENT_CARD_UPDATEDReaction() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        let dummyInteractor = DummyInteractor()
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")

        sut.interactor = dummyInteractor
        sut.cards = cards
        dummyView.diplayItems = sut.createDisplayDataItems()

        BusManagerGlobal.sessionDataInstance = dummyBusManager

        // when
        sut.changeCardStatus(withCardStatus: false, andCard: cards.cards[0])

        // then
        XCTAssert(dummyBusManager.isCalledPublishData == true)

    }

    func test_givenUpdateCardStatusSuccess_whenICallChangeCardsStatusAndUpdateCard_thenIPublishEVENT_CARD_UPDATEDReactionWithCorrectParams() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        let dummyInteractor = DummyInteractor()
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")

        sut.interactor = dummyInteractor
        sut.cards = cards
        dummyView.diplayItems = sut.createDisplayDataItems()

        BusManagerGlobal.sessionDataInstance = dummyBusManager

        // when
        sut.changeCardStatus(withCardStatus: false, andCard: cards.cards[0])

        // then
        XCTAssert(dummyBusManager.dummyTag == CardsPageReaction.ROUTER_TAG)
        XCTAssert(dummyBusManager.dummyEvent?.getId() == CardsPageReaction.EVENT_CARD_UPDATED.getId())
        let dummyCardBO = (dummyBusManager.dummyValueEvent as! CardModel)
        XCTAssert(dummyCardBO.cardBO.cardId == cards.cards[0].cardId)

    }

    func test_givenAnCardStatusFalseAndCardBO_whenICallChangeCardStatusAndInteractorReturnsAnError403_thenICallViewShowError() {
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "false")

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        let errorBO = ErrorBO()
        errorBO.status = ConstantsHTTPCodes.STATUS_403
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        // when
        sut.changeCardStatus(withCardStatus: false, andCard: cards.cards[0])

        // then
        XCTAssert(dummyView.isCalledShowError == true)

    }
    func test_givenAnError_whenICallChangeCardStatus_thenICallInitialStatusOfSwitch() {
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "false")

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()

        let errorBO = ErrorBO()
        errorBO.status = ConstantsHTTPCodes.STATUS_403
        errorBO.isErrorShown = true
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        sut.interactor = dummyInteractor

        // when
        sut.changeCardStatus(withCardStatus: false, andCard: cards.cards[0])

        // then
        XCTAssert(dummyView.isCalledSwitchInitialStateAfterError == true)

    }

    func test_givenACards_whenICallGoToSearchTransacactionsForCardAtIndex_thenIPublishShouldNavigateFlagTrue() {

        let index = 0

        let dummyView = DummyView()

        sut.view = dummyView

        //given
        sut.cards = CardsGenerator.getCardBOWithCardTypeWithOutRelatedCard(withCardType: "PREPAID_CARD")

        //when
        sut.goToSearchTransactionsForCard(atIndex: index)

        //then
        XCTAssert(dummyBusManager.dummyCardsTransport.flagNavigation == true)

    }

    func test_givenAnOperativeCard_whenICallChangeCardStatusToTurnOff_thenINotCallSendOmnitureScreenCardOffSuccess() {

        //given
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")

        sut.view = dummyView
        sut.interactor = dummyInteractor
        dummyView.toastMessage = Localizables.cards.key_cards_on_successful_text

        // when
        sut.changeCardStatus(withCardStatus: false, andCard: cards.cards[0])

        // then
        XCTAssert(dummyView.isCalledSendOmnitureScreenCardOffSuccess == false)

    }

    func test_givenAnOperativeCard_whenICallChangeCardStatusToTurnOn_thenICallSendOmnitureScreenCardOffSuccess() {

        //given
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "false")

        sut.view = dummyView
        sut.interactor = dummyInteractor
        dummyView.toastMessage = Localizables.cards.key_cards_off_successful_text

        // when
        sut.changeCardStatus(withCardStatus: true, andCard: cards.cards[0])

        // then
        XCTAssert(dummyView.isCalledSendOmnitureScreenCardOffSuccess == true)

    }

    // MARK: - goToTransactionDetail

    func test_givenAPresenter_whenICallGoToTransactionDetail_thenICallRouterPresentTransactionsDetail() {

        let cardIndex = 0

        // given
        let transactionEntity = TransactionEntityDummyGenerator.transactionEntity()
        let transactionBO = TransactionBO(transactionEntity: transactionEntity)

        sut.cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "false")

        let transactionDisplayItem = TransactionDisplayItem(fromTransactionBO: transactionBO!)

        // when
        sut.goToTransactionDetail(withTransactionDisplayItem: transactionDisplayItem, forCardAtIndex: cardIndex)

        // then
        XCTAssert(dummyBusManager.isCalledNavigateThirdCardsScreen == true)

    }

    func test_givenAPresenter_whenICallGoToTransactionDetail_thenIPublishCardBOOfCardAsIndex() {

        let cardIndex = 0

        // given
        let transactionEntity = TransactionEntityDummyGenerator.transactionEntity()
        let transactionBO = TransactionBO(transactionEntity: transactionEntity)

        sut.cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "false")

        let transactionDisplayItem = TransactionDisplayItem(fromTransactionBO: transactionBO!)

        // when
        sut.goToTransactionDetail(withTransactionDisplayItem: transactionDisplayItem, forCardAtIndex: cardIndex)

        // then
        XCTAssert(sut.cards?.cards[cardIndex] === dummyBusManager.dummyTransactionsTransport.cardBO)

    }

    // MARK: - viewWillAppear

    func test_givenAPresenterWithCards_whenCallViewWillAppear_thenCallShowDetailAnimationAndCards() {

        //given
        let dummyView = DummyView()
        sut.view = dummyView

        let cards = CardsGenerator.getThreeCards()
        sut.cards = cards

        dummySessionManager.isShowCardAnimation = false

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledAnimationDetail == true)
        XCTAssert(dummyView.isCalledAnimationCards == true)

    }

    func test_givenAPresenterWithOneCard_whenICallViewWillAppear_thenCallShowDetailAnimationAndCards() {

        //given
        let dummyView = DummyView()
        sut.view = dummyView

        let cards = CardsGenerator.getCardBOWithAliasEmpty()
        dummySessionManager.isShowCardAnimation = false
        sut.cards = cards

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledAnimationDetail == true)
        XCTAssert(dummyView.isCalledAnimationCards == false)

    }

    func test_givenAPresenterWithOneCard_whenICallViewWillAppear_thenSessionVarAnimationDoneIsTrue() {

        //given
        let dummyView = DummyView()
        sut.view = dummyView

        let cards = CardsGenerator.getCardBOWithAliasEmpty()
        dummySessionManager.isShowCardAnimation = false
        sut.cards = cards

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummySessionManager.isShowCardAnimation == true)

    }

    func test_givenASessionAnimationDone_whenICallViewWillAppear_thenNotCallAnimations() {

        //given
        let dummyView = DummyView()
        sut.view = dummyView

        let cards = CardsGenerator.getCardBOWithAliasEmpty()
        sut.cards = cards
        dummySessionManager.isShowCardAnimation = true

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledAnimationDetail == false)
        XCTAssert(dummyView.isCalledAnimationCards == false)

    }

    func test_givenAPresenterWithOutCards_whenICallViewWillAppear_thenCallShowInfo() {

        //given
        let dummyView = DummyView()
        sut.view = dummyView

        let cards = CardsGenerator.getCardBOWithAliasEmpty()
        sut.cards = cards

        dummySessionManager.isShowCardAnimation = true

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledAnimationDetail == false)
        XCTAssert(dummyView.isCalledAnimationCards == false)

    }

    func test_givenShowCardId_whenICallViewWillAppear_thenShowCardIdShouldBeNil() {

        // given
        sut.showCardId = "01"

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(sut.showCardId == nil)

    }

    func test_givenCardsAndShowCardIdThatExists_whenICallViewWillAppear_thenICallViewShowCard() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let cards = CardsGenerator.getCardsWithRewards()
        sut.cards = cards
        sut.showCardId = "0006"

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowCardsAtIndex == true)

    }

    func test_givenCardsAndShowCardIdThatExists_whenICallViewWillAppear_thenViewHideCardInformationFromVisibleCellIsFalse() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let cards = CardsGenerator.getCardsWithRewards()
        sut.cards = cards
        sut.showCardId = "0006"

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.hideCardInformationFromVisibleCell == false)

    }

    func test_givenShowCardId_whenICallViewWillAppear_thenNeedRefreshPagerViewShouldBeFalse() {

        // given
        sut.showCardId = "01"

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(sut.needRefreshPagerView == false)

    }

    func test_givenCardsAndShowCardIdThatExists_whenICallViewWillAppear_thenICallViewShowCardWithTheIndexOfCard() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let cards = CardsGenerator.getCardsWithRewards()
        sut.cards = cards
        sut.showCardId = "0006"

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.sentIndex == 1)

    }

    func test_givenCardsAndShowCardIdThatNotExists_whenICallViewWillAppear_thenIDoNotCallViewShowCard() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let cards = CardsGenerator.getCardsWithRewards()
        sut.cards = cards
        sut.showCardId = "3123"

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowCardsAtIndex == false)

    }

    func test_givenCardsAndShowCardIdNil_whenICallViewWillAppear_thenIDoNotCallViewShowCard() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let cards = CardsGenerator.getCardsWithRewards()
        sut.cards = cards
        sut.showCardId = nil

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowCardsAtIndex == false)

    }

    func test_givenCardsNilAndShowCardId_whenICallViewWillAppear_thenIDoNotCallViewShowCard() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.cards = nil
        sut.showCardId = "0006"

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowCardsAtIndex == false)

    }

    // MARK: - pressedOperativeButton

    func test_givenCardsNil_whenICallPressedOperativeButton_thenINotCallAnyBusManagerNavigate() {

        // given
        sut.cards = nil

        let displayData = ButtonBarDisplayData(imageNamed: "", titleKey: "", optionKey: .block)

        // when
        sut.pressedOperativeButton(atIndex: 0, withDisplayData: displayData)

        // then
        XCTAssert(dummyBusManager.isCalledNavigateFourCardsScreen == false)
        XCTAssert(dummyBusManager.isCalledEventNavToCancelCard == false)
        XCTAssert(dummyBusManager.isCalledEventNavToBlockCard == false)

    }

    func test_givenDisplayDataWithLimitsOptionAndValidIndex_whenICallPressedOperativeButton_thenICallBusManagerNavigateFourScreen() {

        // given
        sut.cards = CardsGenerator.getCardBOWithAliasEmpty()

        let displayData = ButtonBarDisplayData(imageNamed: "", titleKey: "", optionKey: .limits)

        // when
        sut.pressedOperativeButton(atIndex: 0, withDisplayData: displayData)

        // then
        XCTAssert(dummyBusManager.isCalledNavigateFourCardsScreen == true)

    }

    func test_givenDisplayDataWithCancelOptionAndValidIndex_whenICallPressedOperativeButton_thenICallBusManagerNavigateNavigateCancelCard() {

        // given
        sut.cards = CardsGenerator.getCardBOWithAliasEmpty()

        let displayData = ButtonBarDisplayData(imageNamed: "", titleKey: "", optionKey: .cancel)

        // when
        sut.pressedOperativeButton(atIndex: 0, withDisplayData: displayData)

        // then
        XCTAssert(dummyBusManager.isCalledEventNavToCancelCard == true)

    }

    func test_givenDisplayDataWithBlockOptionAndValidIndexBlock_whenICallPressedOperativeButton_thenICallBusManagerNavigateToBlockCard() {

        // given
        sut.cards = CardsGenerator.getThreeCards()

        let displayData = ButtonBarDisplayData(imageNamed: "", titleKey: "", optionKey: .block)

        // when
        sut.pressedOperativeButton(atIndex: 0, withDisplayData: displayData)

        // then
        XCTAssert(dummyBusManager.isCalledEventNavToBlockCard == true)

    }

    func test_givenDisplayDataWithBlockOptionAndValidIndexCancel_whenICallPressedOperativeButton_thenICallBusManagerPublishParamsOperationCardsInfo() {

        // given
        sut.cards = CardsGenerator.getThreeCards()

        let displayData = ButtonBarDisplayData(imageNamed: "", titleKey: "", optionKey: .block)

        // when
        sut.pressedOperativeButton(atIndex: 0, withDisplayData: displayData)

        //then
        XCTAssert(dummyBusManager.isCalledPublishParamsOperationCardsInfo == true)
    }

    func test_givenDisplayDataWithBlockOptionAndValidIndexGenerate_whenICallPressedOperativeButton_thenICallBusManagerPublishParamsOperationCardsInfo() {

        // given
        sut.cards = CardsGenerator.getThreeCards()

        let displayData = ButtonBarDisplayData(imageNamed: "", titleKey: "", optionKey: .cancel)

        // when
        sut.pressedOperativeButton(atIndex: 0, withDisplayData: displayData)

        //then
        XCTAssert(dummyBusManager.isCalledPublishParamsOperationCardsInfo == true)
    }

    // MARK: - onOffViewHelpButtonPressed

    func test_givenCardBO_whenICallOnOffViewHelpButtonPressed_thenICallBusManagerOnOffHelpScreen() {

        //given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]

        //when
        sut.onOffViewHelpButtonPressed(forCard: cardBO)

        //then
        XCTAssert(dummyBusManager.isCalledNavigateToOnOffScreen == true)
    }

    func test_givenCardBO_whenICallOnOffViewHelpButtonPressed_thenICallBusManagerWithSameCardBO() {

        //given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]

        //when
        sut.onOffViewHelpButtonPressed(forCard: cardBO)

        //then
        XCTAssert(dummyBusManager.dummyCardsTransport.cardBO === cardBO)
    }

    func test_givenAny_whenICallOnOffViewHelpButtonPressed_thenICallBusManagerOnOffHelpScreen() {

        //given

        //when
        sut.onOffViewHelpButtonPressed(forCard: nil)

        //then
        XCTAssert(dummyBusManager.isCalledNavigateToOnOffScreen == true)
    }

    func test_givenAny_whenICallOnOffViewHelpButtonPressed_thenICallBusManagerWithEmptyModel() {

        //given

        //when
        sut.onOffViewHelpButtonPressed(forCard: nil)

        //then
        XCTAssert(dummyBusManager.dummyEmptyModel != nil)
        XCTAssert(dummyBusManager.dummyCardsTransport == nil)
    }

    // MARK: - helpButtonPressed

    func test_givenHelpId_whenHelpButtonPressed_thenICallBusManagerDetailHelpScreen() {

        //given
        let helpId = "helpId"

        //when
        sut.helpButtonPressed(withHelpId: helpId)

        //then
        XCTAssert(dummyBusManager.isCalledNavigateToDetailHelpScreen == true)

    }

    func test_givenHelpId_whenHelpButtonPressed_thenICallPublishHelpDetailTransportWithHelpId() {

        //given
        let helpId = "helpId"

        //when
        sut.helpButtonPressed(withHelpId: helpId)

        //then
        XCTAssert(helpId == dummyBusManager.dummyHelpDetailTransport.helpId)

    }

    // MARK: - rightCardButtonPressed
    
    func test_givenACardID_whenICallRightCardButtonPressed_thenICallNavigateToCVVScreen() {
        //given
        sut.cards = CardsGenerator.getThreeCards()
        //when
        sut.rightCardButtonPressed(atIndex: 0)
        //then
        XCTAssert(dummyBusManager.isCalledNavigateToCVVScreen == true)
    }
    
    func test_givenACardID_whenICallRightCardButtonPressed_thenICallEventNavToCardCVVScreenReaction() {
        //given
        sut.cards = CardsGenerator.getThreeCards()
        //when
        sut.rightCardButtonPressed(atIndex: 0)
        //then
        XCTAssert(dummyBusManager.isCalledEventNavToCardCVVScreenReaction == true)
    }
    
    func test_givenNoCard_whenICallRightCardButtonPressed_thenIDontNavigateToCVVScreen() {
        sut.cards = nil
        //given
        //when
        sut.rightCardButtonPressed(atIndex: 0)
        //then
        XCTAssert(dummyBusManager.isCalledNavigateToCVVScreen == false)
    }
    
    func test_givenAInvalidCardIndex_whenICallRightCardButtonPressed_thenIDontNavigateToCVVScreen() {
        
        let cardsBO = CardsGenerator.getThreeCards()
        sut.cards = cardsBO
        
        //given
        let cardIndex = sut.cards?.cards.count
        
        //when
        sut.rightCardButtonPressed(atIndex: cardIndex!)
        
        //then
        XCTAssert(dummyBusManager.isCalledNavigateToCVVScreen == false)
    }
    
    func test_givenNoCard_whenICallRightCardButtonPressed_thenIDontCallEventNavToCardCVVScreenReaction() {
        //given
        sut.cards = nil
        //when
        sut.rightCardButtonPressed(atIndex: 0)
        //then
        XCTAssert(dummyBusManager.isCalledNavigateToCVVScreen == false)
    }

    func test_givenNoCardID_whenICallRightCardButtonPressed_thenIShowError() {
        let dummyView = DummyView()
        sut.view = dummyView
        //given
        sut.cards = nil
        //then
        sut.rightCardButtonPressed(atIndex: 0)
        // then
        XCTAssert(dummyView.isCalledShowError == true)
    }
    
    func test_givenAValidCardIndex_whenICallRightCardButtonPressed_thenICallNavigateScreenWhitCardId() {

        let cardsBO = CardsGenerator.getThreeCards()
        let dummyView = DummyView()

        sut.view = dummyView
        sut.cards = cardsBO

        //given
        let cardIndex = 0

        //when
        sut.rightCardButtonPressed(atIndex: cardIndex)

        //then
        XCTAssertTrue(dummyBusManager.dummyCardId == sut.cards?.cards[cardIndex].cardId)
    }

    func test_givenAInvalidCardIndex_whenICallRightCardButtonPressed_thenErrorMessageShouldBeEqualsToGenericMessage() {

        let cardsBO = CardsGenerator.getThreeCards()
        let dummyInteractor = DummyInteractor()

        sut.cards = cardsBO
        sut.interactor = dummyInteractor

        //given
        let invalidCardIndex = sut.cards?.cards.count

        //when
        sut.rightCardButtonPressed(atIndex: invalidCardIndex!)

        //then
        XCTAssert(sut.errorBO?.errorMessage() == Localizables.common.key_common_generic_error_text)
    }

    func test_givenAInvalidCardIndex_whenICallRightCardButtonPressed_thenIShowWarningTypeError() {

        let cardsBO = CardsGenerator.getThreeCards()
        let dummyInteractor = DummyInteractor()

        sut.cards = cardsBO
        sut.interactor = dummyInteractor

        //given
        let invalidCardIndex = sut.cards?.cards.count

        //when
        sut.rightCardButtonPressed(atIndex: invalidCardIndex!)

        //then
        XCTAssert(sut.errorBO?.errorType == .warning)
    }

    func test_givenAInvalidCardIndex_whenICallRightCardButtonPressed_thenICallShowError() {

        let cardsBO = CardsGenerator.getThreeCards()
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()

        sut.view = dummyView
        sut.cards = cardsBO
        sut.interactor = dummyInteractor

        //given
        let invalidCardIndex = sut.cards?.cards.count

        //when
        sut.rightCardButtonPressed(atIndex: invalidCardIndex!)

        //then
        XCTAssert(sut.view?.isCalledShowError == true)
    }

    func test_givenAValidCardIndex_whenICallRightCardButtonPressed_thenINotCallShowError() {

        let cardsBO = CardsGenerator.getThreeCards()
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()

        sut.view = dummyView
        sut.cards = cardsBO
        sut.interactor = dummyInteractor

        //given
        let cardIndex = 0

        //when
        sut.rightCardButtonPressed(atIndex: cardIndex)

        //then
        XCTAssert(sut.view?.isCalledShowError == false)
    }

    func test_givenAEmptyCardsBO_whenICallRightCardButtonPressed_thenICallShowError() {

        let dummyInteractor = DummyInteractor()
        let cardsEntity = CardsEntity()
        let dummyView = DummyView()
        let cardIndex = 0

        cardsEntity.status = 200
        cardsEntity.data = [CardEntity]()

        sut.view = dummyView
        sut.interactor = dummyInteractor

        //given
        sut.cards = CardsBO(cardsEntity: cardsEntity)

        //when
        sut.rightCardButtonPressed(atIndex: cardIndex)

        //then
        XCTAssert(sut.view?.isCalledShowError == true)
    }
    
    func test_givenCardID_whenICallrightCardButtonPressed_thenICallNavigateScreen() {
        
        let cardsBO = CardsGenerator.getThreeCards()
        
        sut.cards = cardsBO
        
        //when
        sut.rightCardButtonPressed(atIndex: 0)
        
        //then
        XCTAssert(dummyBusManager.isCalledNavigateScreen == true)
    }
    
    func test_givenAny_whenICallRightButtonPressed_thenRouterTagShouldBeEqualsToCardsPresenterRouterTag() {
        
        let cardsBO = CardsGenerator.getThreeCards()
        sut.cards = cardsBO
        //given
        //when
        sut.rightCardButtonPressed(atIndex: 0)
        //then
        XCTAssert(dummyBusManager.dummyTag == sut.routerTag)
        
    }
    
    func test_givenCardID_whenICallRightButtonPressed_thenICallPublishCardCVVTransportWithCardId() {
        
        let cardsBO = CardsGenerator.getThreeCards()
        sut.cards = cardsBO
        //given
        //when
        sut.rightCardButtonPressed(atIndex: 0)
        //then
        XCTAssert(dummyBusManager.dummyCardCVVTransport.cardId == sut.cards?.cards[0].cardId)
    }
    
    func test_givenCardID_whenICallNavigateToCVVScreen_thenICallPublishCardCVVTransportWithCardId() {
        
        let cardsBO = CardsGenerator.getThreeCards()
        
        //given
        let cardId = cardsBO.cards.first?.cardId
        
        //when
        sut.navigateToCVVScreen(withCardId: cardId!)
        
        //then
        XCTAssert(dummyBusManager.dummyCardCVVTransport.cardId == cardId)
    }

    // MARK: neededCardsRefresh

    func test_givenAny_whenICallNeededCardsRefresh_thenCallProvideCards() {

        //given
        let dummyInteractor = DummyInteractor()
        //when
        sut.interactor = dummyInteractor
        sut.neededCardsRefresh()

        //then
        XCTAssert(dummyInteractor.isCalledProvideCards == true)
    }

    func test_givenAny_whenICallNeededCardsRefresh_thenCallViewShowLoading() {

        //given
        let dummyInteractor = DummyInteractor()

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.interactor = dummyInteractor
        sut.neededCardsRefresh()

        //then
        XCTAssert(dummyBusManager.isCalledShowLoading == true)
    }

    func test_givenAny_whenICallNeededCardsRefreshAndSuccess_thenCallViewHideLoading() {

        //given
        let dummyInteractor = DummyInteractor()

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.interactor = dummyInteractor
        sut.neededCardsRefresh()

        //then
        XCTAssert(dummyBusManager.isCalledHideLoading == true)
    }

    func test_givenAny_whenICallNeededCardsRefreshAndSuccess_thenPublishDataWithCards() {

        //given
        let dummyInteractor = DummyInteractor()

        //when
        sut.interactor = dummyInteractor
        sut.neededCardsRefresh()

        //then
        XCTAssert(dummyBusManager.saveCards === dummyInteractor.cards)
    }

    func test_givenAny_whenICallNeededCardsRefreshAndError_thenCallViewHideLoading() {

        //given
        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceErrorCards = true

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.interactor = dummyInteractor
        sut.neededCardsRefresh()

        //then
        XCTAssert(dummyBusManager.isCalledHideLoading == true)
    }

    func test_givenAny_whenICallNeededCardsRefresh_thenNeedRefreshPagerViewTrue() {

        //given
        let dummyInteractor = DummyInteractor()
        //when
        sut.interactor = dummyInteractor
        sut.neededCardsRefresh()

        //then
        XCTAssert(sut.needRefreshPagerView == true)
    }

    // MARK: - DummyData

    class DummyInteractor: CardsInteractorProtocol {

        var statusToReturn = 206

        var forceError = false

        var presenter: CardsPresenter<DummyView>?

        var errorBO: ErrorBO?

        var isCalledProvideCardStatus = false

        var dummyActivations: ActivationsBO?

        var activations: ActivationsBO?

        var cardId: String?

        var status: Bool = false

        var serviceResponseDummy = ResponseServiceDummy()

        var isForceError = false

        var cards: CardsBO?
        var isCalledProvideCards = false
        var forceErrorCards = false

        func provideCards(user: UserBO?) -> Single<ModelBO> {
            self.isCalledProvideCards = true

            if forceErrorCards {

                let errorBO = ErrorBO(error: ErrorEntity(message: "Error"))

                return Single.create(subscribe: { single in
                    single(.error(ServiceError.GenericErrorBO(error: errorBO)))
                    return Disposables.create()
                })
            } else {
                self.cards = CardsGenerator.getCardBOWithAlias()

                return Single<ModelBO>.just(self.cards!)
            }
        }

        func provideUpdatedCard(withCardId cardId: String, withStatus status: Bool) -> Observable<ModelBO> {

            isCalledProvideCardStatus = true
            self.cardId = cardId
            self.status = status

            let dummyCardsEntity = CardsEntity()
            dummyCardsEntity.status = statusToReturn

            if  let dummyActivations = dummyActivations {
                self.activations = dummyActivations
            } else {
                 self.activations = CardsGenerator.getAnOperativeCardBOWithChangeStatus()
            }

            if !forceError {
                return Observable <ModelBO>.just(self.activations!)
            } else {

                errorBO = ErrorBO(error: ErrorEntity(message: "Error"))
                errorBO!.isErrorShown = false

                return Observable.error(ServiceError.GenericErrorBO(error: errorBO!))
            }
        }
    }

    class ConfigurationDataManagerDummy: ConfigurationDataManager {

        var isCalledGetCurrencyEntity = false
        var currencyEntity: CurrencyEntity?

        override init() {
        }

        override func provideMainCurrency() -> Observable<ModelEntity> {

            isCalledGetCurrencyEntity = true
            currencyEntity = CurrencyEntity(code: "CLP")

            return Observable <ModelEntity>.just(currencyEntity!)
        }
    }

    class DummyView: CardsViewProtocol, ViewProtocol {

        var isCalledCopyPan = false
        var isCalledShowTransactionsWithDisplayData = false
        var isCalledShowError = false
        var isCalledShowSuccessToast = false
        var showInfoDataIsCalled = false
        var isCalledSwitchInitialStateAfterError = false

        var diplayItems: DisplayItemsCards?
        var transactionsDisplayData: TransactionDisplayData?

        var navigationTitle = ""
        var displayDataBalances: DisplayBalancesCard?
        var displayDataOnOff: OnOffDisplayData?
        var displayDataOperativesBarInfo: ButtonsBarDisplayData?
        var displayDataHelp: HelpDisplayData?

        var isCalledShowNavigationTitle = false
        var isCalledShowBalancesInfo = false
        var isCalledShowOnOffInfo = false
        var isCalledShowOperativeBarInfo = false
        var isCalledHelpView = false

        var toastMessage = ""
        var panCopied = ""

        var isCalledAnimationDetail = false
        var isCalledAnimationCards = false

        var isCalledHideOperativesBar = false

        var isCalledShowCardsAtIndex = false
        var hideCardInformationFromVisibleCell = false
        var sentIndex: Int?

        var isCalledSendOmnitureScreenCardOffSuccess = false

        func showCard(atIndex index: Int, hideCardInformationFromVisibleCell: Bool) {
            isCalledShowCardsAtIndex = true
            self.hideCardInformationFromVisibleCell = hideCardInformationFromVisibleCell
            sentIndex = index
        }

        func showError(error: ModelBO) {
            isCalledShowError = true

        }

        func changeColorEditTexts( ) {
        }

        func showInfoCards(withDisplayItems displayItems: DisplayItemsCards) {
            diplayItems = displayItems
            showInfoDataIsCalled = true
        }

        func showTransactions(withDisplayData displayData: TransactionDisplayData) {
            transactionsDisplayData = displayData
            isCalledShowTransactionsWithDisplayData = true
        }

        func showToastSuccess(withSuccessMessage message: String, andBackgroundColor color: UIColor) {

            toastMessage = message

            if message == toastMessage && color == .BBVADARKGREEN {
                isCalledShowSuccessToast = true
            }
        }

        func switchInitialStateAfterReceiveError(withSwitchStatus status: Bool) {
            isCalledSwitchInitialStateAfterError = true
        }

        func showBalancesInfo(withDisplayData displayData: DisplayBalancesCard) {
            isCalledShowBalancesInfo = true
            displayDataBalances = displayData
        }

        func showOnOffInfo(withDisplayData displayData: OnOffDisplayData) {
            isCalledShowOnOffInfo = true
            displayDataOnOff = displayData
        }

        func showOperativeBarInfo(withDisplayData displayData: ButtonsBarDisplayData) {
            isCalledShowOperativeBarInfo = true
            displayDataOperativesBarInfo = displayData
        }

        func showHelp(withDisplayData displayData: HelpDisplayData) {

            displayDataHelp = displayData
            isCalledHelpView = true
        }

        func showNavigation(withTitle title: String) {
            isCalledShowNavigationTitle = true
            navigationTitle = title
        }

        func copyPan(withNumber number: String) {
            isCalledCopyPan = true
            panCopied = number
        }

        func doAnimationDetail() {
            isCalledAnimationDetail = true
        }

        func doAnimationCards() {
            isCalledAnimationCards = true
        }

        func hideOperativesBar() {
            isCalledHideOperativesBar = true
        }

        func sendOmnitureScreenCardOffSuccess() {
            isCalledSendOmnitureScreenCardOffSuccess = true
        }
        
        func refreshPagerView(atIndex index: Int) {
        }
    }

    class DummyBusManager: BusManager {

        var isCalledNavigateNextCardsScreen = false
        var isCalledNavigateSecondCardsScreen = false
        var isCalledNavigateThirdCardsScreen = false
        var isCalledNavigateFourCardsScreen = false
        var isCalledNavigateToOnOffScreen = false
        var isCalledNavigateToDetailHelpScreen = false
        var isCalledExpiredSession = false
        var isCalledHideLoading = false
        var isCalledPublishData = false
        var isCalledNavigateToCVVScreen = false
        var isCalledShowLoading = false
        var isCalledShowModalError = false
        var isCalledNavigateScreen = false
        var isCalledEventNavToCardCVVScreenReaction = false
        var isCalledEventNavToCancelCard = false
        var isCalledEventNavToBlockCard = false
        var isCalledPublishDataGenerateCard = false
        var isCalledEventNavToGenerateDigitalCard = false
        var isCalledPublishParamsOperationCardsInfo = false

        var dummyCardsTransport: CardsTransport!
        var dummyCardBO: CardBO!
        var dummyTransactionsTransport: TransactionsTransport!
        var dummyHelpDetailTransport: HelpDetailTransport!
        var dummyCardCVVTransport: CVVTransport!
        var dummyEmptyModel: EmptyModel!
        var dummyGenerateCardBO: GenerateCardBO!

        var dummyCardId: String = ""

        var dummyTag: String = ""
        var dummyEvent: ActionSpec<Any>?
        var dummyValueEvent: Any?
        var saveCards: CardsBO!

        override init() {
            super.init()
            routerFactory = RouterInitFactory()
        }

        override func expiredSession() {
            isCalledExpiredSession = true
        }

        override func navigateScreen<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {

            isCalledNavigateScreen = true

            if tag == CardsPageReaction.ROUTER_TAG && event === CardsPageReaction.EVENT_NAV_TO_SECOND_SCREEN {
                isCalledNavigateSecondCardsScreen = true
                dummyCardsTransport = (values as! CardsTransport)
            } else if tag == CardsPageReaction.ROUTER_TAG && event === CardsPageReaction.EVENT_NAV_TO_NEXT_SCREEN {
                isCalledNavigateNextCardsScreen = true
                dummyCardBO = (values as! CardBO)
            } else if tag == CardsPageReaction.ROUTER_TAG && event === CardsPageReaction.EVENT_NAV_TO_THIRD_SCREEN {
                isCalledNavigateThirdCardsScreen = true
                dummyTransactionsTransport = (values as! TransactionsTransport)
            } else if tag == CardsPageReaction.ROUTER_TAG && event === CardsPageReaction.EVENT_NAV_TO_FOUR_SCREEN {
                isCalledNavigateFourCardsScreen = true
                dummyCardBO = (values as! CardBO)
            } else if tag == CardsPageReaction.ROUTER_TAG && event === CardsPageReaction.EVENT_NAV_TO_ON_OFF_HELP {
                isCalledNavigateToOnOffScreen = true

                if let cardReceived = values as? CardsTransport {
                    dummyCardsTransport = cardReceived
                } else {
                    dummyEmptyModel = (values as! EmptyModel)
                }
            } else if tag == CardsPageReaction.ROUTER_TAG && event === CardsPageReaction.EVENT_NAV_TO_DETAIL_HELP {
                isCalledNavigateToDetailHelpScreen = true
                dummyHelpDetailTransport = (values as! HelpDetailTransport)
            } else if tag == CardsPageReaction.ROUTER_TAG && event === CardsPageReaction.EVENT_NAV_TO_CARD_CVV_SCREEN {
                dummyTag = tag
                isCalledNavigateToCVVScreen = true
                isCalledEventNavToCardCVVScreenReaction = event === CardsPageReaction.EVENT_NAV_TO_CARD_CVV_SCREEN
                dummyCardCVVTransport = (values as! CVVTransport)

                dummyCardId = dummyCardCVVTransport.cardId
            } else if tag == CardsPageReaction.ROUTER_TAG && event === CardsPageReaction.EVENT_NAV_TO_CANCEL_CARD {
                dummyTag = tag
                isCalledEventNavToCancelCard = true
            } else if tag == CardsPageReaction.ROUTER_TAG && event === CardsPageReaction.EVENT_NAV_TO_BLOCK_CARD {

                dummyTag = tag
                isCalledEventNavToBlockCard = true
            } else if tag == CardsPageReaction.ROUTER_TAG && event === CardsPageReaction.EVENT_NAV_TO_GENERATE_CARD {
                
                dummyTag = tag
                isCalledEventNavToGenerateDigitalCard = true
            }
        }

        override func showModalError(error: ModelBO, delegate: CloseModalDelegate?) {
            errorBO = error as? ErrorBO
            isCalledShowModalError = true
        }

        override  func hideLoading(completion: (() -> Void)? ) {
            if let completion = completion {
                completion()
            }
            isCalledHideLoading = true
        }

        override func showLoading(completion: (() -> Void)?) {
            completion?()
            isCalledShowLoading = true
        }

        override func publishData<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {

            if tag == ShoppingCrossReaction.TAG && event === PageReactionConstants.PARAMS_CARDS {
                
                saveCards = (values as! CardsBO)
            } else if tag == CardsPageReaction.ROUTER_TAG && event === CardsPageReaction.PARAMS_GENERATE_CARD {
                
                isCalledPublishDataGenerateCard = true
                dummyGenerateCardBO = GenerateCardBO.deserialize(from: values as? [String: Any])
            } else if tag == CardsPageReaction.ROUTER_TAG && event === CardsPageReaction.PARAMS_OPERATION_CARD_INFO {

                isCalledPublishParamsOperationCardsInfo = true
            } else {
                
                isCalledPublishData = true
                dummyTag = tag
                dummyEvent = event as? ActionSpec<Any>
                dummyValueEvent = values
            }
        }
    }

    class FormatterDummy: AmountFormatter {

        var isCalledAttributtedText = false
        var attributtedText: NSAttributedString?

        override func attributtedText(forAmount amount: NSDecimalNumber, isNegative: Bool, bigFontSize bigSize: Int, smallFontSize smallSize: Int) -> NSAttributedString {

            isCalledAttributtedText = true
            attributtedText = NSAttributedString()

            return attributtedText!
        }
    }
    
    class DummyPublicManager: ConfigPublicManager {

        var sentCardKey = ""
        var isCalledGetConfigBOBySection = false

        override func getConfigBOBySection(forKey key: String) -> [ConfigFileBO] {

            isCalledGetConfigBOBySection = true
            sentCardKey = key

            return super.getConfigBOBySection(forKey: key)
        }
    }

    class DummyCardBO: CardBO {

        var isCalledUpdateActivations = false
        var dummyActivationBO: ActivationBO?

        override func updateActivations(activationBO: ActivationBO) {
            isCalledUpdateActivations = true
            dummyActivationBO = activationBO
        }

    }

}
