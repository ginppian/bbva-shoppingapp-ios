//
//  CardsPresenterATest.swift
//  shoppingappMXTest
//
//  Created by jesus.martinez on 13/12/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import XCTest
import RxSwift
import CellsNativeComponents

#if TESTMX
@testable import shoppingappMX
#endif

class CardsPresenterATest: XCTestCase {

    var sut: CardsPresenterA<DummyView>!
    var dummyInteractor: DummyInteractor!
    var dummyBusManager: DummyBusManager!
    var dummyConfiguration: ConfigurationDataManagerDummy!
    var dummySessionManager: DummySessionManager!

    override func setUp() {
        super.setUp()

        dummyBusManager = DummyBusManager()
        sut = CardsPresenterA<DummyView>(busManager: dummyBusManager)

        dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        dummyConfiguration = ConfigurationDataManagerDummy()
        sut.interactor = dummyInteractor

        ConfigurationDataManagerDummy.sharedInstance = dummyConfiguration
        
        DummySessionManager.setupMock()
        dummySessionManager = (SessionDataManager.sessionDataInstance() as! DummySessionManager)
    }

    // MARK: - receive model

    func test_givenAPresenter_whenIReceiveCardsAndReturnsOnlyNormalPlasticCards_thenDisplayDataShouldMatchWithNumberOfCards() {

        //given
        let dummyView = DummyView()
        sut.view = dummyView

        // when
        let cardsBO = CardsGenerator.getThreeCards()

        for i in 0 ..< cardsBO.cards.count {
            cardsBO.cards[i].physicalSupport?.id = .normalPlastic
        }

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.sentDiplayItems != nil)
        XCTAssert(dummyView.sentDiplayItems?.items?.count == sut.cards?.cards.count)

    }

    func test_givenAPresenter_whenIReceiveCardsAndReturnsTwoNormalPlasticAndOneVirtual_thenDisplayDataShouldMatchWithTheNumberOfNormalPlastic() {

        //given
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()

        sut.view = dummyView
        sut.interactor = dummyInteractor

        let cardsBO = CardsGenerator.getThreeCards()

        for i in 0 ..< cardsBO.cards.count {
            cardsBO.cards[i].physicalSupport?.id = .normalPlastic
        }

        cardsBO.cards[cardsBO.cards.count - 1].physicalSupport?.id = .virtual

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.sentDiplayItems != nil)
        XCTAssert(dummyView.sentDiplayItems?.items?.count == sut.cards!.cards.count - 1)

    }

    func test_givenAPresenter_whenIReceiveCardsAndReturnsTwoNormalPlasticAndOneSticker_thenDisplayDataShouldMatchWithTheNumberOfNormalPlastic() {

        //given
        let dummyView = DummyView()

        sut.view = dummyView

        let cardsBO = CardsGenerator.getThreeCards()

        for i in 0 ..< cardsBO.cards.count {
            cardsBO.cards[i].physicalSupport?.id = .normalPlastic
        }

        cardsBO.cards[cardsBO.cards.count - 1].physicalSupport?.id = .sticker

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.sentDiplayItems != nil)
        XCTAssert(dummyView.sentDiplayItems?.items?.count == sut.cards!.cards.count - 1)

    }

    func test_givenAPresenter_whenIReceiveCardsAndReturnsOneNormalPlasticAndOneVirtualAndOneSticker_thenDisplayDataShouldMatchWithTheNumberOfNormalPlastic() {

        //given
        let dummyView = DummyView()

        sut.view = dummyView

        let cardsBO = CardsGenerator.getThreeCards()

        cardsBO.cards[0].physicalSupport?.id = .normalPlastic
        cardsBO.cards[1].physicalSupport?.id = .virtual
        cardsBO.cards[2].physicalSupport?.id = .sticker

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.sentDiplayItems != nil)
        XCTAssert(dummyView.sentDiplayItems?.items?.count == 1)

    }

    func test_givenAPresenter_whenIReceiveCardsAndReturnsOneNormalPlasticAndOneVirtualAndOneSticker_thenDisplayDataForVirtualAndStickerShouldBeFilledCorrectly() {

        //given
        let dummyView = DummyView()

        sut.view = dummyView

        let cardsBO = CardsGenerator.getThreeCards()

        cardsBO.cards[0].physicalSupport?.id = .normalPlastic
        cardsBO.cards[1].physicalSupport?.id = .virtual
        cardsBO.cards[2].physicalSupport?.id = .sticker

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert((dummyView.sentDiplayItems?.items![0].displayCardData as! DisplayCardDataA).virtualInfoDisplayData != nil)
        XCTAssert((dummyView.sentDiplayItems?.items![0].displayCardData as! DisplayCardDataA).virtualCardDisplayData == nil)
        XCTAssert((dummyView.sentDiplayItems?.items![0].displayCardData as! DisplayCardDataA).stickerCardDisplayData == nil)

    }

    func test_givenAPresenter_whenIReceiveCardsAndReturnsOneNormalPlasticAndOneVirtualAndOneStickerWithVirtualAssociatedToNormal_thenDisplayDataVirtualCardShouldBeFilledStickerShouldBeNil() {

        //given
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()

        sut.view = dummyView
        sut.interactor = dummyInteractor

        let cardsBO = CardsGenerator.getThreeCards()

        cardsBO.cards[0].physicalSupport?.id = .normalPlastic
        cardsBO.cards[1].physicalSupport?.id = .virtual
        cardsBO.cards[2].physicalSupport?.id = .sticker

        cardsBO.cards[1].cardId = "222"
        cardsBO.cards[0].relatedContracts[0].relationType!.id = .linkedWith
        cardsBO.cards[0].relatedContracts[0].contractId = cardsBO.cards[1].cardId

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert((dummyView.sentDiplayItems?.items![0].displayCardData as! DisplayCardDataA).virtualInfoDisplayData == nil)
        XCTAssert((dummyView.sentDiplayItems?.items![0].displayCardData as! DisplayCardDataA).virtualCardDisplayData != nil)
        XCTAssert((dummyView.sentDiplayItems?.items![0].displayCardData as! DisplayCardDataA).stickerCardDisplayData == nil)

    }

    func test_givenAPresenter_whenICallGetCardsAndReturnsOneNormalPlasticAndOneVirtualAndOneStickerWithStickerAssociatedToNormal_thenDisplayDataVirtualCardShouldBeFilledStickerShouldBeNil() {

        //given
        let dummyView = DummyView()

        sut.view = dummyView

        let cardsBO = CardsGenerator.getThreeCards()

        cardsBO.cards[0].physicalSupport?.id = .normalPlastic
        cardsBO.cards[1].physicalSupport?.id = .virtual
        cardsBO.cards[2].physicalSupport?.id = .sticker

        cardsBO.cards[2].cardId = "222"
        cardsBO.cards[0].relatedContracts[0].relationType!.id = .linkedWith
        cardsBO.cards[0].relatedContracts[0].contractId = cardsBO.cards[2].cardId

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert((dummyView.sentDiplayItems?.items![0].displayCardData as! DisplayCardDataA).virtualInfoDisplayData != nil)
        XCTAssert((dummyView.sentDiplayItems?.items![0].displayCardData as! DisplayCardDataA).virtualCardDisplayData == nil)
        XCTAssert((dummyView.sentDiplayItems?.items![0].displayCardData as! DisplayCardDataA).stickerCardDisplayData != nil)

    }

    func test_givenAPresenter_whenIReceiveCardsAndReturnsOneNormalPlasticAndOneVirtualAndOneStickerWithStickerAndVirtualAssociatedToNormal_thenDisplayDataVirtualCardShouldBeFilledStickerShouldBeNil() {

        //given
        let dummyView = DummyView()
        sut.view = dummyView

        let cardsBO = CardsGenerator.getThreeCards()

        cardsBO.cards[0].physicalSupport?.id = .normalPlastic
        cardsBO.cards[1].physicalSupport?.id = .virtual
        cardsBO.cards[2].physicalSupport?.id = .sticker

        cardsBO.cards[1].cardId = "222"
        cardsBO.cards[2].cardId = "222"

        cardsBO.cards[0].relatedContracts[0].relationType!.id = .linkedWith
        cardsBO.cards[0].relatedContracts[0].contractId = cardsBO.cards[1].cardId
        cardsBO.cards[0].relatedContracts[1].relationType!.id = .linkedWith
        cardsBO.cards[0].relatedContracts[1].contractId = cardsBO.cards[2].cardId

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert((dummyView.sentDiplayItems?.items![0].displayCardData as! DisplayCardDataA).virtualInfoDisplayData == nil)
        XCTAssert((dummyView.sentDiplayItems?.items![0].displayCardData as! DisplayCardDataA).virtualCardDisplayData != nil)
        XCTAssert((dummyView.sentDiplayItems?.items![0].displayCardData as! DisplayCardDataA).stickerCardDisplayData != nil)

    }

    // MARK: - viewWillAppear

    func test_givenAPresenterWithCards_whenCallViewWillAppear_thenCallShowDetailAnimationAndCards() {

        //given
        let dummyView = DummyView()
        sut.view = dummyView

        let cards = CardsGenerator.getThreeCards()
        sut.cards = cards

        dummySessionManager.isShowCardAnimation = false

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledAnimationDetail == true)
        XCTAssert(dummyView.isCalledAnimationCards == true)

    }

    func test_givenAPresenterWithOneCard_whenICallViewWillAppear_thenCallShowDetailAnimationAndCards() {

        //given
        let dummyView = DummyView()
        sut.view = dummyView

        let cards = CardsGenerator.getCardBOWithAliasEmpty()
        dummySessionManager.isShowCardAnimation = false
        sut.cards = cards

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledAnimationDetail == true)
        XCTAssert(dummyView.isCalledAnimationCards == false)

    }

    func test_givenAPresenterWithOneCard_whenICallViewWillAppear_thenSessionVarAnimationDoneIsTrue() {

        //given
        let dummyView = DummyView()
        sut.view = dummyView

        let cards = CardsGenerator.getCardBOWithAliasEmpty()
        dummySessionManager.isShowCardAnimation = false
        sut.cards = cards

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummySessionManager.isShowCardAnimation == true)

    }

    func test_givenASessionAnimationDone_whenICallViewWillAppear_thenNotCallAnimations() {

        //given
        let dummyView = DummyView()
        sut.view = dummyView

        let cards = CardsGenerator.getCardBOWithAliasEmpty()
        sut.cards = cards

        dummySessionManager.isShowCardAnimation = true

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledAnimationDetail == false)
        XCTAssert(dummyView.isCalledAnimationCards == false)

    }

    func test_givenAPresenterWithOutCards_whenICallViewWillAppear_thenCallShowInfo() {

        //given
        let dummyView = DummyView()
        sut.view = dummyView

        let cards = CardsGenerator.getCardBOWithAliasEmpty()
        sut.cards = cards

        dummySessionManager.isShowCardAnimation = true

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledAnimationDetail == false)
        XCTAssert(dummyView.isCalledAnimationCards == false)

    }

    func test_givenShowCardId_whenICallViewWillAppear_thenShowCardIdShouldBeNil() {

        // given
        sut.showCardId = "01"

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(sut.showCardId == nil)

    }

    func test_givenDisplayItemsNil_whenICallViewWillAppear_thenIDoNotCallViewShowCard() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.displayItems.items = nil

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowCardAtIndex == false)

    }

    func test_givenDisplayItemsAndShowCardIdNil_whenICallViewWillAppear_thenIDoNotCallViewShowCard() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.cards = CardsGenerator.getCardBOWithCardTypeWithOutRelatedCard(withCardType: "PREPAID_CARD")
        sut.displayItems = sut.createDisplayDataItems()
        sut.showCardId = nil

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowCardAtIndex == false)

    }

    func test_givenCardsDisplayItemsAndShowCardIdOfPhysicalThatMatch_whenICallViewWillAppear_thenICallViewShowCard() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.cards = CardsGenerator.getCardBOWithCardTypeWithOutRelatedCard(withCardType: "PREPAID_CARD")
        sut.displayItems = sut.createDisplayDataItems()
        sut.showCardId = "2110"

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowCardAtIndex == true)

    }

    func test_givenCardsDisplayItemsAndShowCardIdOfPhysicalThatMatch_whenICallViewWillAppear_thenICallViewShowCardWithCorrectIndex() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.cards = CardsGenerator.getCardBOWithCardTypeWithOutRelatedCard(withCardType: "PREPAID_CARD")
        sut.displayItems = sut.createDisplayDataItems()
        sut.showCardId = "2110"

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.sentIndex == 0)

    }

    func test_givenCardsDisplayItemsAndShowCardIdOfPhysicalThatMatch_whenICallViewWillAppear_thenSelectedCardAtIndexShouldBePhysical() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.cards = CardsGenerator.getCardBOWithCardTypeWithOutRelatedCard(withCardType: "PREPAID_CARD")
        sut.displayItems = sut.createDisplayDataItems()
        sut.showCardId = "2110"

        // when
        sut.viewWillAppear()

        // then
        XCTAssert((sut.displayItems.items![0].displayCardData as? DisplayCardDataA)?.selectedCard == .physical)

    }

    func test_givenCardsDisplayItemsAndShowCardIdOfDigitalThatMatch_whenICallViewWillAppear_thenICallViewShowDataInfo() {

        let dummyView = DummyView()
        sut.view = dummyView

        //given
        let cards = CardsGenerator.getThreeCardsWithStickerAndDigital()

        let cardOperationInfo = CardOperationInfo(cardID: cards.cards[11].cardId, fisicalCardID: nil, cardType: CardDataType.digital, cardOperation: .cancel)
        sut.cardOperationInfo = cardOperationInfo

        sut.cards = cards

        sut.displayItems = sut.createDisplayDataItems()
        sut.showCardId = cards.cards[11].cardId

        // when
        sut.viewWillAppear()

        //then
        XCTAssert(dummyView.isCalledShowCardDataInfo == true)

    }

    func test_givenCardsDisplayItemsAndShowCardIdOfDigitalThatMatch_whenICallViewWillAppear_thenICallViewShowTransactionsWithDisplayData() {

        let dummyView = DummyView()
        sut.view = dummyView

        //given
        let cards = CardsGenerator.getThreeCardsWithStickerAndDigital()

        let cardOperationInfo = CardOperationInfo(cardID: cards.cards[11].cardId, fisicalCardID: nil, cardType: CardDataType.digital, cardOperation: .cancel)
        sut.cardOperationInfo = cardOperationInfo

        sut.cards = cards

        sut.displayItems = sut.createDisplayDataItems()
        sut.showCardId = cards.cards[11].cardId

        //when
        sut.viewWillAppear()

        //then
        XCTAssert(dummyView.isCalledShowTransactionsWithDisplayData == true)

    }

    func test_givenADisplayDataWithCardBOAndMaximumNumberAndCardWithoutTransactionsBOAndShowCardIdOfDigitalThatMatch_whenICallTabPressedWithDisplayData_thenDisplayTransactionsCardMaximumNumberAndCardIdAndTransactionsBOShouldMatch() {

        let dummyView = DummyView()
        sut.view = dummyView

        //given
        let cards = CardsGenerator.getThreeCardsWithStickerAndDigital()

        let cardOperationInfo = CardOperationInfo(cardID: cards.cards[11].cardId, fisicalCardID: nil, cardType: CardDataType.digital, cardOperation: .cancel)
        sut.cardOperationInfo = cardOperationInfo

        sut.cards = cards

        sut.displayItems = sut.createDisplayDataItems()
        sut.showCardId = cards.cards[11].cardId

        //when
        sut.viewWillAppear()

        //then
        XCTAssert(dummyView.transactionsDisplayData!.cardId == cards.cards[11].cardId)
        XCTAssert(dummyView.transactionsDisplayData!.maximumNumberOfTransactions == sut.maximumTransactionsInTransactionView)
        XCTAssert(dummyView.transactionsDisplayData!.transactionsBO == nil)

    }

    func test_givenADisplayDataWithCardBOAndMaximumNumberAndCardWithTransactionsBOAndShowCardIdOfDigitalThatMatch_whenICallTabPressedWithDisplayData_thenDisplayTransactionsCardMaximumNumberAndCardIdAndTransactionsBOShouldMatch() {

        let dummyView = DummyView()
        sut.view = dummyView

        //given
        let cards = CardsGenerator.getThreeCardsWithStickerAndDigital()

        let cardOperationInfo = CardOperationInfo(cardID: cards.cards[11].cardId, fisicalCardID: nil, cardType: CardDataType.digital, cardOperation: .cancel)
        sut.cardOperationInfo = cardOperationInfo

        sut.cards = cards

        sut.displayItems = sut.createDisplayDataItems()
        sut.showCardId = cards.cards[11].cardId

        //when
        sut.viewWillAppear()

        //then
        XCTAssert(dummyView.transactionsDisplayData!.cardId == cards.cards[11].cardId)
        XCTAssert(dummyView.transactionsDisplayData!.maximumNumberOfTransactions == sut.maximumTransactionsInTransactionView)
        XCTAssert(dummyView.transactionsDisplayData!.transactionsBO === sut.displayItems.items![0].displayCardData?.cardBO?.transactions)
        XCTAssert(dummyView.transactionsDisplayData!.transactionsBO?.transactions.count == sut.displayItems.items![0].displayCardData?.cardBO?.transactions?.transactions.count)

    }

    func test_givenCardsDisplayItemsAndShowCardIdOfDigitalThatMatch_whenICallViewWillAppear_thenICallViewShowNavigationTitle() {

        let dummyView = DummyView()
        sut.view = dummyView

        //given
        let cards = CardsGenerator.getThreeCardsWithStickerAndDigital()

        let cardOperationInfo = CardOperationInfo(cardID: cards.cards[11].cardId, fisicalCardID: nil, cardType: CardDataType.digital, cardOperation: .cancel)
        sut.cardOperationInfo = cardOperationInfo

        sut.cards = cards

        sut.displayItems = sut.createDisplayDataItems()
        sut.showCardId = cards.cards[11].cardId

        //when
        sut.viewWillAppear()

        //then
        XCTAssert(dummyView.isCalledShowNavigationTitle == true)

    }

    func test_givenCardsDisplayItemsAndShowCardIdOfDigitalThatMatch_whenICallViewWillAppear_thenICallViewShowNavigationTitleWithCardTitle() {

        let dummyView = DummyView()
        sut.view = dummyView

        //given
        let cards = CardsGenerator.getThreeCardsWithStickerAndDigital()

        let cardOperationInfo = CardOperationInfo(cardID: cards.cards[11].cardId, fisicalCardID: nil, cardType: CardDataType.digital, cardOperation: .cancel)
        sut.cardOperationInfo = cardOperationInfo

        sut.cards = cards

        _ = sut.createDisplayDataItems()
        sut.showCardId = cards.cards[11].cardId

        //when
        sut.viewWillAppear()

        //then
        XCTAssert(dummyView.navigationTitle == (sut.displayItems.items![0].displayCardData as? DisplayCardDataA)?.virtualCardDisplayData?.titleCard)

    }

    func test_givenCardsDisplayItemsAndShowCardIdOfDigitalThatMatch_whenICallViewWillAppear_thenICallViewShowBalancesInfo() {

        let dummyView = DummyView()
        sut.view = dummyView

        //given
        let cards = CardsGenerator.getThreeCardsWithStickerAndDigital()

        let cardOperationInfo = CardOperationInfo(cardID: cards.cards[11].cardId, fisicalCardID: nil, cardType: CardDataType.digital, cardOperation: .cancel)
        sut.cardOperationInfo = cardOperationInfo

        sut.cards = cards

        _ = sut.createDisplayDataItems()
        sut.showCardId = cards.cards[11].cardId

        //when
        sut.viewWillAppear()

        //then
        XCTAssert(dummyView.isCalledShowBalancesInfo == true)

    }

    func test_givenCardsDisplayItemsAndShowCardIdOfDigitalThatMatch_whenICallViewWillAppear_thenICallViewShowBalancesInfoWithDisplayInfo() {

        let dummyView = DummyView()
        sut.view = dummyView

        //given
        let cards = CardsGenerator.getThreeCardsWithStickerAndDigital()

        let cardOperationInfo = CardOperationInfo(cardID: cards.cards[11].cardId, fisicalCardID: nil, cardType: CardDataType.digital, cardOperation: .cancel)
        sut.cardOperationInfo = cardOperationInfo

        sut.cards = cards

        _ = sut.createDisplayDataItems()
        sut.showCardId = cards.cards[11].cardId

        //when
        sut.viewWillAppear()

        //then
        XCTAssert(dummyView.displayDataBalances != nil)

    }

    func test_givenCardsDisplayItemsAndShowCardIdOfDigitalThatMatch_whenICallViewWillAppear_thenICallViewShowOnOfInfo() {

        let dummyView = DummyView()
        sut.view = dummyView

        //given
        let cards = CardsGenerator.getThreeCardsWithStickerAndDigital()

        let cardOperationInfo = CardOperationInfo(cardID: cards.cards[11].cardId, fisicalCardID: nil, cardType: CardDataType.digital, cardOperation: .cancel)
        sut.cardOperationInfo = cardOperationInfo

        sut.cards = cards

        _ = sut.createDisplayDataItems()
        sut.showCardId = cards.cards[11].cardId

        //when
        sut.viewWillAppear()

        //then
        XCTAssert(dummyView.isCalledShowOnOffInfo == true)

    }

    func test_givenCardsDisplayItemsAndShowCardIdOfDigitalThatMatch_whenICallViewWillAppear_thenICallViewShowOnOfInfoWithDisplayInfo() {

        let dummyView = DummyView()
        sut.view = dummyView

        //given
        let cards = CardsGenerator.getThreeCardsWithStickerAndDigital()

        let cardOperationInfo = CardOperationInfo(cardID: cards.cards[11].cardId, fisicalCardID: nil, cardType: CardDataType.digital, cardOperation: .cancel)
        sut.cardOperationInfo = cardOperationInfo

        sut.cards = cards

        _ = sut.createDisplayDataItems()
        sut.showCardId = cards.cards[11].cardId

        //when
        sut.viewWillAppear()

        //then
        XCTAssert(dummyView.displayDataOnOff != nil)

    }

    func test_givenCardsDisplayItemsAndShowCardIdOfDigitalAndInoperativeCardThatMatch_whenICallViewWillAppear_thenICallViewHideOperativeBar() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "INOPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        cardBO.indicators[4].indicatorId = .cancelable
        cardBO.indicators[4].isActive = false

        cardBO.physicalSupport?.id = .virtual

        let cardOperationInfo = CardOperationInfo(cardID: cardBO.cardId, fisicalCardID: nil, cardType: CardDataType.digital, cardOperation: .cancel)
        sut.cardOperationInfo = cardOperationInfo

        let cards = CardsGenerator.getThreeCardsWithStickerAndDigital()
        cards.cards[0].relatedContracts[0].contractId = cardBO.cardId

        cards.cards.append(cardBO)

        sut.cards = cards

        sut.displayItems = sut.createDisplayDataItems()
        sut.displayItems.items![0].displayCardData?.cardBO = cardBO
        sut.showCardId = "2110"

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledHideOperativesBar == true)

    }

    func test_givenCardsDisplayItemsAndShowCardIdOfPhysicalAndInoperativeCardThatMatch_whenICallViewWillAppear_thenIDoNotCallViewShowOperativeBar() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "INOPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        cardBO.indicators[4].indicatorId = .cancelable
        cardBO.indicators[4].isActive = false
        sut.cards = CardsGenerator.getCardBOWithAlias()
        sut.displayItems = sut.createDisplayDataItems()
        sut.displayItems.items![0].displayCardData?.cardBO = cardBO
        sut.showCardId = "2110"

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowOperativeBarInfo == false)

    }

    func test_givenCardsDisplayItemsAndShowCardIdOfDigitalAndInoperativeCardThatMatch_whenICallViewWillAppear_thenICallViewShowOperativesBar() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        cardBO.physicalSupport?.id = .virtual

        let cardOperationInfo = CardOperationInfo(cardID: cardBO.cardId, fisicalCardID: nil, cardType: CardDataType.digital, cardOperation: .cancel)
        sut.cardOperationInfo = cardOperationInfo

        let cards = CardsGenerator.getThreeCardsWithStickerAndDigital()
        cards.cards[0].relatedContracts[0].contractId = cardBO.cardId

        cards.cards.append(cardBO)

        sut.cards = cards

        sut.displayItems = sut.createDisplayDataItems()
        sut.displayItems.items![0].displayCardData?.cardBO = cardBO
        sut.showCardId = "2110"

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowOperativeBarInfo == true)

    }

    func test_givenCardsDisplayItemsAndShowCardIdOfPhysicalAndOperativeCardThatMatch_whenICallViewWillAppear_thenIDoNotCallViewHideOperativesBar() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        sut.cards = CardsGenerator.getCardBOWithAlias()
        sut.displayItems = sut.createDisplayDataItems()
        sut.displayItems.items![0].displayCardData?.cardBO = cardBO
        sut.showCardId = "2110"

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledHideOperativesBar == false)

    }

    func test_givenCardsDisplayItemsAndShowCardIdOfPhysicalAndOperativeCardThatMatch_whenICallViewWillAppear_thenDisplayOperativesBarShouldHaveLimitsButton() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        cardBO.physicalSupport?.id = .virtual

        let cardOperationInfo = CardOperationInfo(cardID: cardBO.cardId, fisicalCardID: nil, cardType: CardDataType.digital, cardOperation: .cancel)
        sut.cardOperationInfo = cardOperationInfo

        let cards = CardsGenerator.getThreeCardsWithStickerAndDigital()
        cards.cards[0].relatedContracts[0].contractId = cardBO.cardId

        cards.cards.append(cardBO)

        sut.cards = cards

        sut.displayItems = sut.createDisplayDataItems()
        sut.displayItems.items![0].displayCardData?.cardBO = cardBO
        sut.showCardId = "2110"

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.displayDataOperativesBarInfo!.buttons.count == 3)
        XCTAssert(dummyView.displayDataOperativesBarInfo!.buttons[1].imageNamed == ConstantsImages.Card.limits)
        XCTAssert(dummyView.displayDataOperativesBarInfo!.buttons[1].titleKey == Localizables.cards.key_cards_set_up_limits_text)
        XCTAssert(dummyView.displayDataOperativesBarInfo!.buttons[1].optionKey == .limits)

    }

    func test_givenCardsDisplayItemsAndShowCardIdOfDigitalAndBlockedCardThatMatch_whenICallViewWillAppear_thenICallViewShowOperativesBar() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "BLOCKED", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        cardBO.physicalSupport?.id = .virtual

        let cardOperationInfo = CardOperationInfo(cardID: cardBO.cardId, fisicalCardID: nil, cardType: CardDataType.digital, cardOperation: .cancel)
        sut.cardOperationInfo = cardOperationInfo

        let cards = CardsGenerator.getThreeCardsWithStickerAndDigital()

        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        sut.displayItems.items![0].displayCardData?.cardBO = cardBO
        sut.showCardId = "2110"

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowOperativeBarInfo == true)

    }

    func test_givenCardsDisplayItemsAndShowCardIdOfPhysicalAndBlockedCardThatMatch_whenICallViewWillAppear_thenIDoNotCallViewHideOperativesBar() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "BLOCKED", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        sut.cards = CardsGenerator.getCardBOWithAlias()
        sut.displayItems = sut.createDisplayDataItems()
        sut.displayItems.items![0].displayCardData?.cardBO = cardBO
        sut.showCardId = "2110"

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledHideOperativesBar == false)

    }

    func test_givenCardsDisplayItemsAndShowCardIdOfDigitalAndBlockedCardThatMatch_whenICallViewWillAppear_thenIViewCallShowOperativeBar() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "BLOCKED", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        cardBO.physicalSupport?.id = .virtual

        let cardOperationInfo = CardOperationInfo(cardID: cardBO.cardId, fisicalCardID: nil, cardType: CardDataType.digital, cardOperation: .cancel)
        sut.cardOperationInfo = cardOperationInfo

        let cards = CardsGenerator.getThreeCardsWithStickerAndDigital()
        cards.cards[0].relatedContracts[0].contractId = cardBO.cardId

        cards.cards.append(cardBO)

        sut.cards = cards

        sut.displayItems = sut.createDisplayDataItems()
        sut.displayItems.items![0].displayCardData?.cardBO = cardBO
        sut.showCardId = "2110"

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowOperativeBarInfo == true)

    }

    func test_givenCardsDisplayItemsAndShowCardIdOfPhysicalAndBlockedCardThatMatch_whenICallViewWillAppear_thenIDoNotViewCallHideOperativeBar() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "BLOCKED", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        sut.cards = CardsGenerator.getCardBOWithAlias()
        sut.displayItems = sut.createDisplayDataItems()
        sut.displayItems.items![0].displayCardData?.cardBO = cardBO
        sut.showCardId = "2110"

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledHideOperativesBar == false)

    }

    func test_givenCardsDisplayItemsAndShowCardIdOfDigitalAndCanceledCardThatMatch_whenICallViewWillAppear_thenIViewCallShowOperativeBar() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "CANCELED", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        cardBO.physicalSupport?.id = .virtual

        let cardOperationInfo = CardOperationInfo(cardID: cardBO.cardId, fisicalCardID: nil, cardType: CardDataType.digital, cardOperation: .cancel)
        sut.cardOperationInfo = cardOperationInfo

        let cards = CardsGenerator.getThreeCardsWithStickerAndDigital()
        cards.cards[0].relatedContracts[0].contractId = cardBO.cardId

        cards.cards.append(cardBO)

        sut.cards = cards

        sut.displayItems = sut.createDisplayDataItems()
        sut.displayItems.items![0].displayCardData?.cardBO = cardBO
        sut.showCardId = "2110"

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowOperativeBarInfo == true)

    }

    func test_givenCardsDisplayItemsAndShowCardIdOfPhysicalAndCanceledCardThatMatch_whenICallViewWillAppear_thenIDoNotCallViewHideOperativesBar() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "CANCELED", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        sut.cards = CardsGenerator.getCardBOWithAlias()
        sut.displayItems = sut.createDisplayDataItems()
        sut.displayItems.items![0].displayCardData?.cardBO = cardBO
        sut.showCardId = "2110"

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledHideOperativesBar == false)

    }

    func test_givenCardsDisplayItemsAndShowCardIdOfDifgitalAndCanceledCardThatMatch_whenICallViewWillAppear_thenDisplayOperativesBarShouldHaveLimitsButton() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "CANCELED", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        cardBO.physicalSupport?.id = .virtual

        let cardOperationInfo = CardOperationInfo(cardID: cardBO.cardId, fisicalCardID: nil, cardType: CardDataType.digital, cardOperation: .cancel)
        sut.cardOperationInfo = cardOperationInfo

        let cards = CardsGenerator.getThreeCardsWithStickerAndDigital()
        cards.cards[0].relatedContracts[0].contractId = cardBO.cardId

        cards.cards.append(cardBO)

        sut.cards = cards

        sut.displayItems = sut.createDisplayDataItems()
        sut.displayItems.items![0].displayCardData?.cardBO = cardBO
        sut.showCardId = "2110"

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.displayDataOperativesBarInfo!.buttons.count == 2)
        XCTAssert(dummyView.displayDataOperativesBarInfo!.buttons[0].imageNamed == ConstantsImages.Card.limits)
        XCTAssert(dummyView.displayDataOperativesBarInfo!.buttons[0].titleKey == Localizables.cards.key_cards_set_up_limits_text)
        XCTAssert(dummyView.displayDataOperativesBarInfo!.buttons[0].optionKey == .limits)

    }

    func test_givenCardsDisplayItemsAndShowCardIdOfDigitalAndPendingEmbossingdCardThatMatch_whenICallViewWillAppear_thenIViewCallShowOperativeBar() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "PENDING_EMBOSSING", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        cardBO.physicalSupport?.id = .virtual

        let cardOperationInfo = CardOperationInfo(cardID: cardBO.cardId, fisicalCardID: nil, cardType: CardDataType.digital, cardOperation: .cancel)
        sut.cardOperationInfo = cardOperationInfo

        let cards = CardsGenerator.getThreeCardsWithStickerAndDigital()
        cards.cards[0].relatedContracts[0].contractId = cardBO.cardId

        cards.cards.append(cardBO)

        sut.cards = cards

        sut.displayItems = sut.createDisplayDataItems()
        sut.displayItems.items![0].displayCardData?.cardBO = cardBO
        sut.showCardId = "2110"

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowOperativeBarInfo == true)
    }

    func test_givenCardsDisplayItemsAndShowCardIdOfPhysicalAndPendingEmbossingdCardThatMatch_whenICallViewWillAppear_thenIDoNotCallViewHideOperativesBar() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "PENDING_EMBOSSING", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        sut.cards = CardsGenerator.getCardBOWithAlias()
        sut.displayItems = sut.createDisplayDataItems()
        sut.displayItems.items![0].displayCardData?.cardBO = cardBO
        sut.showCardId = "2110"

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledHideOperativesBar == false)
    }

    func test_givenCardsDisplayItemsAndShowCardIdOfPhysicalAndPendingDeliveryCardThatMatch_whenICallViewWillAppear_thenIViewCallShowOperativeBar() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "PENDING_DELIVERY", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        let cardOperationInfo = CardOperationInfo(cardID: cardBO.cardId, fisicalCardID: nil, cardType: CardDataType.digital, cardOperation: .cancel)
        sut.cardOperationInfo = cardOperationInfo

        let cards = CardsGenerator.getThreeCardsWithStickerAndDigital()
        cards.cards[0].relatedContracts[0].contractId = cardBO.cardId

        cards.cards.append(cardBO)

        sut.cards = cards

        sut.displayItems = sut.createDisplayDataItems()
        sut.displayItems.items![0].displayCardData?.cardBO = cardBO
        sut.showCardId = "2110"

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowOperativeBarInfo == true)

    }

    func test_givenCardsDisplayItemsAndShowCardIdOfPhysicalAndPendingDeliveryCardThatMatch_whenICallViewWillAppear_thenIDoNotCallViewHideOperativesBar() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "PENDING_DELIVERY", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        sut.cards = CardsGenerator.getCardBOWithAlias()
        sut.displayItems = sut.createDisplayDataItems()
        sut.displayItems.items![0].displayCardData?.cardBO = cardBO
        sut.showCardId = "2110"

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledHideOperativesBar == false)

    }

    func test_givenCardsDisplayItemsAndShowCardIdOfVirtualAndCardWithRewardButDebitThatMatch_whenICallViewWillAppear_thenICallViewHidePoints() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let cardBO = CardsGenerator.getCardsWithRewards().cards[0]

        cardBO.physicalSupport?.id = .virtual
        let cardOperationInfo = CardOperationInfo(cardID: cardBO.cardId, fisicalCardID: nil, cardType: CardDataType.digital, cardOperation: .cancel)
        sut.cardOperationInfo = cardOperationInfo

        let cards = CardsGenerator.getThreeCardsWithStickerAndDigital()
        cards.cards[0].relatedContracts[0].contractId = cardBO.cardId

        cards.cards.append(cardBO)

        sut.cards = cards
        
        sut.displayItems = sut.createDisplayDataItems()
        sut.displayItems.items![0].displayCardData?.cardBO = cardBO
        sut.showCardId = cardBO.cardId

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledHidePoints == true)

    }

    func test_givenCardsDisplayItemsAndShowCardIdOfVirtualAndCardWithRewardThatMatch_whenICallViewWillAppear_thenICallViewShowPoints() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let cardBO = CardsGenerator.getCardsWithRewards().cards[7]

        cardBO.physicalSupport?.id = .virtual
        let cardOperationInfo = CardOperationInfo(cardID: cardBO.cardId, fisicalCardID: nil, cardType: CardDataType.digital, cardOperation: .cancel)
        sut.cardOperationInfo = cardOperationInfo

        let cards = CardsGenerator.getThreeCardsWithStickerAndDigital()
        cards.cards[0].relatedContracts[0].contractId = cardBO.cardId

        cards.cards.append(cardBO)

        sut.cards = cards

        sut.displayItems = sut.createDisplayDataItems()
        sut.displayItems.items![0].displayCardData?.cardBO = cardBO
        sut.showCardId = cardBO.cardId

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowPoints == true)

    }

    func test_givenCardsDisplayItemsAndShowCardIdOfVirtualAndCardWithRewardButNotBancomerPointsThatMatch_whenICallViewWillAppear_thenICallViewHidePoints() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let cardBO = CardsGenerator.getCardsWithRewards().cards[4]

        cardBO.physicalSupport?.id = .virtual
        let cardOperationInfo = CardOperationInfo(cardID: cardBO.cardId, fisicalCardID: nil, cardType: CardDataType.digital, cardOperation: .cancel)
        sut.cardOperationInfo = cardOperationInfo

        let cards = CardsGenerator.getThreeCardsWithStickerAndDigital()
        cards.cards[0].relatedContracts[0].contractId = cardBO.cardId

        cards.cards.append(cardBO)

        sut.cards = cards

        sut.displayItems = sut.createDisplayDataItems()
        sut.displayItems.items![0].displayCardData?.cardBO = cardBO
        sut.showCardId = cardBO.cardId

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledHidePoints == true)

    }

    func test_givenCardsDisplayItemsAndShowCardIdOfVirtualThatMatch_whenICallViewWillAppear_thenICallViewShowCard() {

        let dummyView = DummyView()
        sut.view = dummyView

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .virtual
        }

        cards.cards[1].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[1].cardId

        // given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        sut.showCardId = "3333"

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowCardAtIndex == true)

    }

    func test_givenCardsDisplayItemsAndShowCardIdOfVirtualThatMatch_whenICallViewWillAppear_thenICallViewShowCardWithCorrectIndex() {

        let dummyView = DummyView()
        sut.view = dummyView

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .virtual
        }

        cards.cards[1].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[1].cardId

        // given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        sut.showCardId = "3333"

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.sentIndex == 0)

    }

    func test_givenCardsDisplayItemsAndShowCardIdOfVirtualThatMatch_whenICallViewWillAppear_thenSelectedCardAtIndexShouldBeDigital() {

        let dummyView = DummyView()
        sut.view = dummyView

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .virtual
        }

        cards.cards[1].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[1].cardId

        // given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        sut.showCardId = "3333"

        // when
        sut.viewWillAppear()

        // then
        XCTAssert((sut.displayItems.items![0].displayCardData as? DisplayCardDataA)?.selectedCard == .digital)

    }

    func test_givenCardsDisplayItemsAndShowCardIdOfStickerThatMatch_whenICallViewWillAppear_thenICallViewShowCard() {

        let dummyView = DummyView()
        sut.view = dummyView

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .sticker
        }

        cards.cards[1].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[1].cardId

        // given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        sut.showCardId = "3333"

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.isCalledShowCardAtIndex == true)

    }

    func test_givenCardsDisplayItemsAndShowCardIdOfStickerThatMatch_whenICallViewWillAppear_thenICallViewShowCardWithCorrectIndex() {

        let dummyView = DummyView()
        sut.view = dummyView

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .sticker
        }

        cards.cards[1].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[1].cardId

        // given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        sut.showCardId = "3333"

        // when
        sut.viewWillAppear()

        // then
        XCTAssert(dummyView.sentIndex == 0)

    }

    func test_givenCardsDisplayItemsAndShowCardIdOfStickerThatMatch_whenICallViewWillAppear_thenSelectedCardAtIndexShouldBeSticker() {

        let dummyView = DummyView()
        sut.view = dummyView

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .sticker
        }

        cards.cards[1].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[1].cardId

        // given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        sut.showCardId = "3333"

        // when
        sut.viewWillAppear()

        // then
        XCTAssert((sut.displayItems.items![0].displayCardData as? DisplayCardDataA)?.selectedCard == .sticker)

    }

    // MARK: - tabPressed

    func test_givenNotDisplayData_whenICallTabPressedWithDisplayData_thenICallViewHideDataInfo() {

        let dummyView = DummyView()
        sut.view = dummyView

        //given

        //when
        sut.tabPressed(withDisplayData: nil)

        //then
        XCTAssert(dummyView.isCalledHideDataInfo == true)

    }

    func test_givenADisplayDataWithoutCardBO_whenICallTabPressedWithDisplayData_thenICallViewHideDataInfo() {

        let dummyView = DummyView()
        sut.view = dummyView

        //given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)
        displayData.cardBO = nil

        //when
        sut.tabPressed(withDisplayData: displayData)

        //then
        XCTAssert(dummyView.isCalledHideDataInfo == true)

    }

    func test_givenADisplayDataWithoutDisplayData_whenICallTabPressedWithDisplayData_thenICallViewShowNavigationTitle() {

        let dummyView = DummyView()
        sut.view = dummyView

        //given

        //when
        sut.tabPressed(withDisplayData: nil)

        //then
        XCTAssert(dummyView.isCalledShowNavigationTitle == true)

    }

    func test_givenADisplayDataWithouthDisplayData_whenICallTabPressedWithDisplayData_thenICallViewShowNavigationTitleWithVirtualDefaultTitle() {

        let dummyView = DummyView()
        sut.view = dummyView

        //given

        //when
        sut.tabPressed(withDisplayData: nil)

        //then
        XCTAssert(dummyView.navigationTitle == Localizables.cards.key_card_digital_text)

    }

    func test_givenADisplayDataWithDisplayDataWithoutCardBO_whenICallTabPressedWithDisplayData_thenIDoNotCallViewShowNavigationTitle() {

        let dummyView = DummyView()
        sut.view = dummyView

        //given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)
        displayData.cardBO = nil

        //when
        sut.tabPressed(withDisplayData: displayData)

        //then
        XCTAssert(dummyView.isCalledShowNavigationTitle == false)

    }

    func test_givenDisplayDataWithCardBO_whenICallTabPressedWithDisplayData_thenICallViewShowDataInfo() {

        let dummyView = DummyView()
        sut.view = dummyView

        //given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        //when
        sut.tabPressed(withDisplayData: displayData)

        //then
        XCTAssert(dummyView.isCalledShowCardDataInfo == true)

    }

    func test_givenDisplayDataWithCardBO_whenICallTabPressedWithDisplayData_thenICallViewShowTransactionsWithDisplayData() {

        let dummyView = DummyView()
        sut.view = dummyView

        //given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        //when
        sut.tabPressed(withDisplayData: displayData)

        //then
        XCTAssert(dummyView.isCalledShowTransactionsWithDisplayData == true)

    }

    func test_givenADisplayDataWithCardBOAndMaximumNumberAndCardWithoutTransactionsBO_whenICallTabPressedWithDisplayData_thenDisplayTransactionsCardMaximumNumberAndCardIdAndTransactionsBOShouldMatch() {

        let dummyView = DummyView()
        sut.view = dummyView

        //given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)
        //when
        sut.tabPressed(withDisplayData: displayData)

        //then
        XCTAssert(dummyView.transactionsDisplayData!.cardId == displayData.cardBO?.cardId)
        XCTAssert(dummyView.transactionsDisplayData!.maximumNumberOfTransactions == sut.maximumTransactionsInTransactionView)
        XCTAssert(dummyView.transactionsDisplayData!.transactionsBO == nil)

    }

    func test_givenADisplayDataWithCardBOAndMaximumNumberAndCardWithTransactionsBO_whenICallTabPressedWithDisplayData_thenDisplayTransactionsCardMaximumNumberAndCardIdAndTransactionsBOShouldMatch() {

        let dummyView = DummyView()
        sut.view = dummyView

        //given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)
        //when
        sut.tabPressed(withDisplayData: displayData)

        //then
        XCTAssert(dummyView.transactionsDisplayData!.cardId == displayData.cardBO?.cardId)
        XCTAssert(dummyView.transactionsDisplayData!.maximumNumberOfTransactions == sut.maximumTransactionsInTransactionView)
        XCTAssert(dummyView.transactionsDisplayData!.transactionsBO === displayData.cardBO?.transactions)
        XCTAssert(dummyView.transactionsDisplayData!.transactionsBO?.transactions.count == displayData.cardBO?.transactions?.transactions.count)

    }

    func test_givenDisplayDataWithCardBO_whenICallTabPressedWithDisplayData_thenICallViewShowNavigationTitle() {

        let dummyView = DummyView()
        sut.view = dummyView

        //given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        //when
        sut.tabPressed(withDisplayData: displayData)

        //then
        XCTAssert(dummyView.isCalledShowNavigationTitle == true)

    }

    func test_givenDisplayDataWithCardBO_whenICallTabPressedWithDisplayData_thenICallViewShowNavigationTitleWithCardTitle() {

        let dummyView = DummyView()
        sut.view = dummyView

        //given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        //when
        sut.tabPressed(withDisplayData: displayData)

        //then
        XCTAssert(dummyView.navigationTitle == displayData.titleCard)

    }

    func test_givenDisplayDataWithCardBO_whenICallTabPressedWithDisplayData_thenICallViewShowBalancesInfo() {

        let dummyView = DummyView()
        sut.view = dummyView

        //given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        //when
        sut.tabPressed(withDisplayData: displayData)

        //then
        XCTAssert(dummyView.isCalledShowBalancesInfo == true)

    }

    func test_givenDisplayDataWithCardBO_whenICallTabPressedWithDisplayData_thenICallViewShowBalancesInfoWithDisplayInfo() {

        let dummyView = DummyView()
        sut.view = dummyView

        //given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        //when
        sut.tabPressed(withDisplayData: displayData)

        //then
        XCTAssert(dummyView.displayDataBalances != nil)

    }

    func test_givenDisplayDataWithCardBO_whenICallTabPressedWithDisplayData_thenICallViewShowOnOfInfo() {

        let dummyView = DummyView()
        sut.view = dummyView

        //given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        //when
        sut.tabPressed(withDisplayData: displayData)

        //then
        XCTAssert(dummyView.isCalledShowOnOffInfo == true)

    }

    func test_givenDisplayDataWithCardBO_whenICallTabPressedWithDisplayData_thenICallViewShowOnOfInfoWithDisplayInfo() {

        let dummyView = DummyView()
        sut.view = dummyView

        //given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        //when
        sut.tabPressed(withDisplayData: displayData)

        //then
        XCTAssert(dummyView.displayDataOnOff != nil)

    }

    func test_givenADisplayDataWithInoperativeCard_whenICallTabPressedWithDisplayData_thenICallViewHideOperativeBar() {

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "INOPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        cardBO.indicators[4].indicatorId = .cancelable
        cardBO.indicators[4].isActive = false
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.tabPressed(withDisplayData: displayData)

        // then
        XCTAssert(dummyView.isCalledHideOperativesBar == true)

    }

    func test_givenADisplayDataWithInoperativeCard_whenICallTabPressedWithDisplayData_thenIDoNotCallViewShowOperativeBar() {

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "INOPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        cardBO.indicators[4].indicatorId = .cancelable
        cardBO.indicators[4].isActive = false
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.tabPressed(withDisplayData: displayData)

        // then
        XCTAssert(dummyView.isCalledShowOperativeBarInfo == false)

    }

    func test_givenADisplayDataWithOperativeCard_whenICallTabPressedWithDisplayData_thenICallViewShowOperativesBar() {

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.tabPressed(withDisplayData: displayData)

        // then
        XCTAssert(dummyView.isCalledShowOperativeBarInfo == true)

    }

    func test_givenAnDisplayDataWithOperativeCard_whenICallTabPressedWithDisplayData_thenIDoNotCallViewHideOperativesBar() {

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.tabPressed(withDisplayData: displayData)

        // then
        XCTAssert(dummyView.isCalledHideOperativesBar == false)

    }

    func test_givenAnDisplayDataWithOperativeCard_whenICallTabPressedWithDisplayData_thenDisplayOperativesBarShouldHaveLimitsButton() {

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.tabPressed(withDisplayData: displayData)

        // then
        XCTAssert(dummyView.displayDataOperativesBarInfo!.buttons.count == 3)
        XCTAssert(dummyView.displayDataOperativesBarInfo!.buttons[1].imageNamed == ConstantsImages.Card.limits)
        XCTAssert(dummyView.displayDataOperativesBarInfo!.buttons[1].titleKey == Localizables.cards.key_cards_set_up_limits_text)
        XCTAssert(dummyView.displayDataOperativesBarInfo!.buttons[1].optionKey == .limits)

    }

    func test_givenADisplayDataWithBlockedCard_whenICallTabPressedWithDisplayData_thenICallViewShowOperativesBar() {

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "BLOCKED", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.tabPressed(withDisplayData: displayData)

        // then
        XCTAssert(dummyView.isCalledShowOperativeBarInfo == true)

    }

    func test_givenADisplayDataWithBlockedCard_whenICallTabPressedWithDisplayData_thenIDoNotCallViewHideOperativesBar() {

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "BLOCKED", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.tabPressed(withDisplayData: displayData)

        // then
        XCTAssert(dummyView.isCalledHideOperativesBar == false)

    }

    func test_givenADisplayDataWithBlockedCard_whenICallTabPressedWithDisplayData_thenIViewCallShowOperativeBar() {

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "BLOCKED", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.tabPressed(withDisplayData: displayData)

        // then
        XCTAssert(dummyView.isCalledShowOperativeBarInfo == true)

    }

    func test_givenADisplayDataWithBlockedCard_whenICallTabPressedWithDisplayData_thenIDoNotViewCallHideOperativeBar() {

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "BLOCKED", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.tabPressed(withDisplayData: displayData)

        // then
        XCTAssert(dummyView.isCalledHideOperativesBar == false)

    }

    func test_givenADisplayDataWithCanceledCard_whenICallTabPressedWithDisplayData_thenIViewCallShowOperativeBar() {

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "CANCELED", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.tabPressed(withDisplayData: displayData)

        // then
        XCTAssert(dummyView.isCalledShowOperativeBarInfo == true)

    }

    func test_givenADisplayDataWithCanceledCard_whenICallTabPressedWithDisplayData_thenIDoNotCallViewHideOperativesBar() {

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "CANCELED", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.tabPressed(withDisplayData: displayData)

        // then
        XCTAssert(dummyView.isCalledHideOperativesBar == false)

    }

    func test_givenADisplayDataWithCanceledCard_whenICallTabPressedWithDisplayData_thenDisplayOperativesBarShouldHaveLimitsButton() {

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "CANCELED", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.tabPressed(withDisplayData: displayData)

        // then
        XCTAssert(dummyView.displayDataOperativesBarInfo!.buttons.count == 2)
        XCTAssert(dummyView.displayDataOperativesBarInfo!.buttons[0].imageNamed == ConstantsImages.Card.limits)
        XCTAssert(dummyView.displayDataOperativesBarInfo!.buttons[0].titleKey == Localizables.cards.key_cards_set_up_limits_text)
        XCTAssert(dummyView.displayDataOperativesBarInfo!.buttons[0].optionKey == .limits)

    }

    func test_givenADisplayDataWithPendingEmbossingCard_whenICallTabPressedWithDisplayData_thenIViewCallShowOperativeBar() {

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "PENDING_EMBOSSING", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.tabPressed(withDisplayData: displayData)

        // then
        XCTAssert(dummyView.isCalledShowOperativeBarInfo == true)
    }

    func test_givenADisplayDataWithPendingEmbossingCard_whenICallTabPressedWithDisplayData_thenIDoNotCallViewHideOperativesBar() {

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "PENDING_EMBOSSING", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.tabPressed(withDisplayData: displayData)

        // then
        XCTAssert(dummyView.isCalledHideOperativesBar == false)
    }

    func test_givenADisplayDataWithPendingDeliveryCard_whenICallTabPressedWithDisplayData_thenIViewCallShowOperativeBar() {

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "PENDING_DELIVERY", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.tabPressed(withDisplayData: displayData)

        // then
        XCTAssert(dummyView.isCalledShowOperativeBarInfo == true)

    }

    func test_givenADisplayDataWithPendingDeliveryCard_whenICallTabPressedWithDisplayData_thenIDoNotCallViewHideOperativesBar() {

        // given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "PENDING_DELIVERY", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        let dummyView = DummyView()
        sut.view = dummyView

        // when
        sut.tabPressed(withDisplayData: displayData)

        // then
        XCTAssert(dummyView.isCalledHideOperativesBar == false)

    }

    // MARK: - cardShowed at index (physical)

    func test_givenDisplayDataWithCardBO_whenICallCardShowedAtIndexAndSelectedCardPhysical_thenICallViewShowDataInfo() {

        let dummyView = DummyView()

        sut.view = dummyView
        let cardIndex = 0

        //given
        sut.cards = CardsGenerator.getCardBOWithCardTypeWithOutRelatedCard(withCardType: "PREPAID_CARD")
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .physical
        
        dummySessionManager.isShowCardAnimation = true
        
        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.isCalledShowCardDataInfo == true)
    }

    func test_givenACardsAndDisplayItemsInPresenter_whenICallCardShowedAtIndexAndSelectedCardPhysical_thenICallViewShowTransactionsWithDisplayData() {

        let dummyView = DummyView()

        sut.view = dummyView
        let cardIndex = 0

        //given
        sut.cards = CardsGenerator.getCardBOWithCardTypeWithOutRelatedCard(withCardType: "PREPAID_CARD")
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .physical

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.isCalledShowTransactionsWithDisplayData == true)

    }

    func test_givenACardsAndDisplayItemsAndMaximumNumberAndCardWithoutTransactionsBO_whenICallCardShowedAtIndexAndSelectedCardPhysical_thenDisplayTransactionsCardMaximumNumberAndCardIdAndTransactionsBOShouldMatchOfPhysical() {

        let dummyView = DummyView()

        sut.view = dummyView
        let cardIndex = 0

        //given
        sut.cards = CardsGenerator.getCardBOWithCardTypeWithOutRelatedCard(withCardType: "PREPAID_CARD")
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .physical

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.transactionsDisplayData!.cardId == sut.cards?.cards[0].cardId)
        XCTAssert(dummyView.transactionsDisplayData!.maximumNumberOfTransactions == sut.maximumTransactionsInTransactionView)
        XCTAssert(dummyView.transactionsDisplayData!.transactionsBO == nil)

    }

    func test_givenACardsAndDisplayItemsAndMaximumNumberAndCardWithTransactionsBO_whenICallCardShowedAtIndexAndSelectedCardPhysical_thenDisplayTransactionsCardMaximumNumberAndCardIdAndTransactionsBOShouldMatchOfPhysical() {

        let dummyView = DummyView()
        let cardIndex = 0

        sut.view = dummyView

        //given
        sut.cards = CardsGenerator.getCardBOWithCardTypeWithOutRelatedCard(withCardType: "PREPAID_CARD")
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .physical

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.transactionsDisplayData!.cardId == sut.cards?.cards[0].cardId)
        XCTAssert(dummyView.transactionsDisplayData!.maximumNumberOfTransactions == sut.maximumTransactionsInTransactionView)
        XCTAssert(dummyView.transactionsDisplayData!.transactionsBO === sut.cards!.cards[cardIndex].transactions)
        XCTAssert(dummyView.transactionsDisplayData!.transactionsBO?.transactions.count == sut.cards!.cards[cardIndex].transactions?.transactions.count)
    }

    func test_givenACardsAndDisplayItems_whenICallCardShowedAtIndexAndSelectedCardPhysical_thenICallViewShowNavigationTitle() {

        let dummyView = DummyView()
        sut.view = dummyView
        let cardIndex = 0

        //given
        sut.cards = CardsGenerator.getCardBOWithCardTypeWithOutRelatedCard(withCardType: "PREPAID_CARD")
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .physical

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.isCalledShowNavigationTitle == true)

    }

    func test_givenACardsAndDisplayItems_whenICallCardShowedAtIndexOfACardWithAliasAndSelectedCardPhysical_thenICallViewShowNavigationTitleWithCardAliasOfPhysical() {

        let dummyView = DummyView()
        sut.view = dummyView
        let cardIndex = 0

        //given
        sut.cards = CardsGenerator.getCardBOWithCardTypeWithOutRelatedCard(withCardType: "PREPAID_CARD")
        sut.cards!.cards[cardIndex].alias = "alias"
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .physical

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.navigationTitle == sut.cards!.cards[cardIndex].alias)

    }

    func test_givenACardsAndDisplayItems_whenICallCardShowedAtIndexOfACardWithoutAliasAndSelectedCardPhysical_thenICallViewShowNavigationTitleWithCardNameOfPhysical() {

        let dummyView = DummyView()
        sut.view = dummyView
        let cardIndex = 0

        //given
        sut.cards = CardsGenerator.getCardBOWithCardTypeWithOutRelatedCard(withCardType: "PREPAID_CARD")
        sut.cards!.cards[cardIndex].alias = ""
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .physical

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.navigationTitle == sut.cards!.cards[cardIndex].title?.name)

    }

    func test_givenACardsAndDisplayItems_whenICallCardShowedAtIndexAndSelectedCardPhysical_thenICallViewShowBalancesInfo() {

        let dummyView = DummyView()
        sut.view = dummyView
        let cardIndex = 0

        //given
        sut.cards = CardsGenerator.getCardBOWithCardTypeWithOutRelatedCard(withCardType: "PREPAID_CARD")
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .physical

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.isCalledShowBalancesInfo == true)

    }

    func test_givenACardsAndDisplayItems_whenICallCardShowedAtIndexAndSelectedCardPhysical_thenICallViewShowBalancesInfoWithDisplayInfo() {

        let dummyView = DummyView()
        sut.view = dummyView
        let cardIndex = 0

        //given
        sut.cards = CardsGenerator.getCardBOWithCardTypeWithOutRelatedCard(withCardType: "PREPAID_CARD")
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .physical

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.displayDataBalances != nil)

    }

    func test_givenACardsAndDisplayItems_whenICallCardShowedAtIndexAndSelectedCardPhysical_thenICallViewShowBalancesInfoWithDisplayInfoAtIndexOfPhysical() {

        let dummyView = DummyView()
        sut.view = dummyView
        let cardIndex = 0

        //given
        sut.cards = CardsGenerator.getCardBOWithCardTypeWithOutRelatedCard(withCardType: "PREPAID_CARD")
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .physical

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.displayDataBalances?.cardBO === sut.cards!.cards[cardIndex])

    }

    func test_givenACardsAndDisplayItems_whenICallCardShowedAtIndexAndSelectedCardPhysical_thenICallViewShowOnOfInfo() {

        let dummyView = DummyView()
        sut.view = dummyView
        let cardIndex = 0

        //given
        sut.cards = CardsGenerator.getCardBOWithCardTypeWithOutRelatedCard(withCardType: "PREPAID_CARD")
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .physical

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.isCalledShowOnOffInfo == true)

    }

    func test_givenACardsAndDisplayItems_whenICallCardShowedAtIndexAndSelectedCardPhysical_thenICallViewShowOnOfInfoWithDisplayInfo() {

        let dummyView = DummyView()
        sut.view = dummyView
        let cardIndex = 0

        //given
        sut.cards = CardsGenerator.getCardBOWithCardTypeWithOutRelatedCard(withCardType: "PREPAID_CARD")
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .physical

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.displayDataOnOff != nil)

    }

    func test_givenACardsAndDisplayItems_whenICallCardShowedAtIndexAndSelectedCardPhysical_thenICallViewShowOnOffInfoWithDisplayInfoWithCardBOAtIndexOfPhysical() {

        let dummyView = DummyView()
        sut.view = dummyView
        let cardIndex = 0

        //given
        sut.cards = CardsGenerator.getCardBOWithCardTypeWithOutRelatedCard(withCardType: "PREPAID_CARD")
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .physical

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.displayDataOnOff?.cardBO === sut.cards!.cards[cardIndex])

    }

    func test_givenACardsAndDisplayItems_whenICallCardShowedAtIndexAndSelectedCardPhysical_thenICallViewShowOperativeBarInfo() {

        let dummyView = DummyView()
        sut.view = dummyView
        let cardIndex = 0

        //given
        sut.cards = CardsGenerator.getCardBOWithCardTypeWithOutRelatedCard(withCardType: "PREPAID_CARD")
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .physical

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.isCalledShowOperativeBarInfo == true)

    }

    func test_givenACardsAndDisplayItems_whenICallCardShowedAtIndexAndSelectedCardPhysical_thenICallViewShowOperativeBarInfoWithDisplayInfo() {

        let dummyView = DummyView()
        sut.view = dummyView
        let cardIndex = 0

        //given
        sut.cards = CardsGenerator.getCardBOWithCardTypeWithOutRelatedCard(withCardType: "PREPAID_CARD")
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .physical

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.displayDataOperativesBarInfo != nil)

    }

    // MARK: - cardShowed at index (virtual)

    func test_givenACardsAndDisplayItemsWithVirtualCardRelatedInPresenter_whenICallCardShowedAtIndexAndSelectedCardVirtual_thenICallViewShowDataInfo() {

        let dummyView = DummyView()

        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .virtual
        }

        cards.cards[1].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[1].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .digital

        //when
        dummySessionManager.isShowCardAnimation = true

        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.isCalledShowCardDataInfo == true)

    }

    func test_givenACardsAndDisplayItemsWithVirtualCardRelatedInPresenter_whenICallCardShowedAtIndexAndSelectedCardVirtual_thenICallViewShowTransactionsWithDisplayData() {

        let dummyView = DummyView()

        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .virtual
        }

        cards.cards[1].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[1].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .digital

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.isCalledShowTransactionsWithDisplayData == true)

    }

    func test_givenACardsAndDisplayItemsWithVirtualCardRelatedAndMaximumNumberAndCardWithoutTransactionsBO_whenICallCardShowedAtIndexAndSelectedCardVirtual_thenDisplayTransactionsCardMaximumNumberAndCardIdAndTransactionsBOShouldMatchOfVirtual() {

        let dummyView = DummyView()

        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .virtual
        }

        cards.cards[1].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[1].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .digital

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.transactionsDisplayData!.cardId == sut.cards?.cards[1].cardId)
        XCTAssert(dummyView.transactionsDisplayData!.maximumNumberOfTransactions == sut.maximumTransactionsInTransactionView)
        XCTAssert(dummyView.transactionsDisplayData!.transactionsBO == nil)

    }

    func test_givenACardsAndDisplayItemsWithVirtualCardRelatedAndMaximumNumberAndCardWithTransactionsBO_whenICallCardShowedAtIndexAndSelectedCardVirtual_thenDisplayTransactionsCardMaximumNumberAndCardIdAndTransactionsBOShouldMatchOfVirtual() {

        let dummyView = DummyView()
        let cardIndex = 0

        sut.view = dummyView

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .virtual
        }

        cards.cards[1].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[1].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .digital

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.transactionsDisplayData!.cardId == sut.cards?.cards[1].cardId)
        XCTAssert(dummyView.transactionsDisplayData!.maximumNumberOfTransactions == sut.maximumTransactionsInTransactionView)
        XCTAssert(dummyView.transactionsDisplayData!.transactionsBO === sut.cards!.cards[cardIndex].transactions)
        XCTAssert(dummyView.transactionsDisplayData!.transactionsBO?.transactions.count == sut.cards!.cards[cardIndex].transactions?.transactions.count)
    }

    func test_givenACardsAndDisplayItemsWithVirtualCardRelated_whenICallCardShowedAtIndexAndSelectedCardVirtual_thenICallViewShowNavigationTitle() {

        let dummyView = DummyView()
        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .virtual
        }

        cards.cards[1].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[1].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .digital

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.isCalledShowNavigationTitle == true)

    }

    func test_givenACardsAndDisplayItemsWithVirtualCardRelated_whenICallCardShowedAtIndexOfACardWithAliasAndSelectedCardVirtual_thenICallViewShowNavigationTitleWithCardAliasOfVirtual() {

        let dummyView = DummyView()
        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .virtual
        }

        cards.cards[1].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[1].cardId

        //given
        sut.cards = cards
        sut.cards!.cards[cardIndex].alias = "alias"
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .digital

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.navigationTitle == sut.cards!.cards[1].alias)

    }

    func test_givenACardsAndDisplayItemsWithVirtualCardRelated_whenICallCardShowedAtIndexOfACardWithoutAliasAndSelectedCardVirtual_thenICallViewShowNavigationTitleWithCardNameOfVirtual() {

        let dummyView = DummyView()
        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .virtual
        }

        cards.cards[1].cardId = "3333"
        cards.cards[1].alias = ""
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[1].cardId

        //given
        sut.cards = cards
        sut.cards!.cards[cardIndex].alias = ""
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .digital

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.navigationTitle == sut.cards!.cards[1].title?.name)

    }

    func test_givenACardsAndDisplayItemsWithVirtualCardRelated_whenICallCardShowedAtIndexAndSelectedCardVirtual_thenICallViewShowBalancesInfo() {

        let dummyView = DummyView()
        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .virtual
        }

        cards.cards[1].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[1].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .digital

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.isCalledShowBalancesInfo == true)

    }

    func test_givenACardsAndDisplayItemsWithVirtualCardRelated_whenICallCardShowedAtIndexAndSelectedCardVirtual_thenICallViewShowBalancesInfoWithDisplayInfo() {

        let dummyView = DummyView()
        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .virtual
        }

        cards.cards[1].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[1].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .digital

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.displayDataBalances != nil)

    }

    func test_givenACardsAndDisplayItemsWithVirtualCardRelated_whenICallCardShowedAtIndexAndSelectedCardVirtual_thenICallViewShowBalancesInfoWithDisplayInfoAtIndexOfVirtual() {

        let dummyView = DummyView()
        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .virtual
        }

        cards.cards[1].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[1].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .digital

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.displayDataBalances?.cardBO === sut.cards!.cards[1])

    }

    func test_givenACardsAndDisplayItemsWithVirtualCardRelated_whenICallCardShowedAtIndexAndSelectedCardVirtual_thenICallViewShowOnOfInfo() {

        let dummyView = DummyView()
        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .virtual
        }

        cards.cards[1].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[1].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .digital

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.isCalledShowOnOffInfo == true)

    }

    func test_givenACardsAndDisplayItemsWithVirtualCardRelated_whenICallCardShowedAtIndexAndSelectedCardVirtual_thenICallViewShowOnOfInfoWithDisplayInfo() {

        let dummyView = DummyView()
        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .virtual
        }

        cards.cards[1].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[1].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .digital

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.displayDataOnOff != nil)

    }

    func test_givenACardsAndDisplayItemsWithVirtualCardRelated_whenICallCardShowedAtIndexAndSelectedCardVirtual_thenICallViewShowOnOffInfoWithDisplayInfoWithCardBOAtIndexOfVirtual() {

        let dummyView = DummyView()
        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .virtual
        }

        cards.cards[1].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[1].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .digital

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.displayDataOnOff?.cardBO === sut.cards!.cards[1])

    }

    func test_givenACardsAndDisplayItemsWithVirtualCardRelated_whenICallCardShowedAtIndexAndSelectedCardVirtual_thenICallViewShowOperativeBarInfo() {

        let dummyView = DummyView()
        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .virtual
        }

        cards.cards[1].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[1].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .digital

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.isCalledShowOperativeBarInfo == true)

    }

    func test_givenACardsAndDisplayItemsWithVirtualCardRelated_whenICallCardShowedAtIndexAndSelectedCardVirtual_thenICallViewShowOperativeBarInfoWithDisplayInfo() {

        let dummyView = DummyView()
        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .virtual
        }

        cards.cards[1].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[1].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .digital

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.displayDataOperativesBarInfo != nil)

    }

    // MARK: - cardShowed at index (virtual not linked)

    func test_givenACardsAndDisplayItemsWithNotVirtualCardRelated_whenICallCardShowedAtIndexAndSelectedCardVirtual_thenICallViewHideDataInfo() {

        let dummyView = DummyView()
        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .virtual
        }

        cards.cards[1].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .digital

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.isCalledHideDataInfo == true)

    }

    func test_givenACardsAndDisplayItemsWithNotVirtualCardRelated_whenICallCardShowedAtIndexAndSelectedCardVirtual_thenICallViewShowNavigationTitle() {

        let dummyView = DummyView()
        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .virtual
        }

        cards.cards[1].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[1].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .digital

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.isCalledShowNavigationTitle == true)

    }

    func test_givenACardsAndDisplayItemsWithNotVirtualCardRelated_whenICallCardShowedAtIndexAndSelectedCardVirtual_thenICallViewShowNavigationTitleWithVirtualDefaultTitle() {

        let dummyView = DummyView()
        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .virtual
        }

        cards.cards[1].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .digital

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.navigationTitle == Localizables.cards.key_card_digital_text)

    }

    // MARK: - cardShowed at index (sticker)

    func test_givenACardsAndDisplayItemsWithStickerCardRelatedInPresenter_whenICallCardShowedAtIndexAndSelectedCardSticker_thenICallViewShowDataInfo() {

        let dummyView = DummyView()

        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .sticker
        }

        cards.cards[2].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[2].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .sticker
        dummySessionManager.isShowCardAnimation = true

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.isCalledShowCardDataInfo == true)

    }

    func test_givenACardsAndDisplayItemsWithStickerCardRelatedInPresenter_whenICallCardShowedAtIndexAndSelectedCardSticker_thenICallViewShowTransactionsWithDisplayData() {

        let dummyView = DummyView()

        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .sticker
        }

        cards.cards[2].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[2].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .sticker

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.isCalledShowTransactionsWithDisplayData == true)

    }

    func test_givenACardsAndDisplayItemsWithStickerCardRelatedAndMaximumNumberAndCardWithoutTransactionsBO_whenICallCardShowedAtIndexAndSelectedCardSticker_thenDisplayTransactionsCardMaximumNumberAndCardIdAndTransactionsBOShouldMatchOfSticker() {

        let dummyView = DummyView()

        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .sticker
        }

        cards.cards[2].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[2].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .sticker

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.transactionsDisplayData!.cardId == sut.cards?.cards[2].cardId)
        XCTAssert(dummyView.transactionsDisplayData!.maximumNumberOfTransactions == sut.maximumTransactionsInTransactionView)
        XCTAssert(dummyView.transactionsDisplayData!.transactionsBO == nil)

    }

    func test_givenACardsAndDisplayItemsWithStickerCardRelatedAndMaximumNumberAndCardWithTransactionsBO_whenICallCardShowedAtIndexAndSelectedCardSticker_thenDisplayTransactionsCardMaximumNumberAndCardIdAndTransactionsBOShouldMatchOfSticker() {

        let dummyView = DummyView()
        let cardIndex = 0

        sut.view = dummyView

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .sticker
        }

        cards.cards[2].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[2].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .sticker

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.transactionsDisplayData!.cardId == sut.cards?.cards[2].cardId)
        XCTAssert(dummyView.transactionsDisplayData!.maximumNumberOfTransactions == sut.maximumTransactionsInTransactionView)
        XCTAssert(dummyView.transactionsDisplayData!.transactionsBO === sut.cards!.cards[cardIndex].transactions)
        XCTAssert(dummyView.transactionsDisplayData!.transactionsBO?.transactions.count == sut.cards!.cards[cardIndex].transactions?.transactions.count)
    }

    func test_givenACardsAndDisplayItemsWithStickerCardRelated_whenICallCardShowedAtIndexAndSelectedCardSticker_thenICallViewShowNavigationTitle() {

        let dummyView = DummyView()
        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .sticker
        }

        cards.cards[2].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[2].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .sticker

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.isCalledShowNavigationTitle == true)

    }

    func test_givenACardsAndDisplayItemsWithStickerCardRelated_whenICallCardShowedAtIndexOfACardWithAliasAndSelectedCardSticker_thenICallViewShowNavigationTitleWithCardAliasOfSticker() {

        let dummyView = DummyView()
        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .sticker
        }

        cards.cards[2].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[2].cardId

        //given
        sut.cards = cards
        sut.cards!.cards[cardIndex].alias = "alias"
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .sticker

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.navigationTitle == sut.cards!.cards[2].alias)

    }

    func test_givenACardsAndDisplayItemsWithStickerCardRelated_whenICallCardShowedAtIndexOfACardWithoutAliasAndSelectedCardSticker_thenICallViewShowNavigationTitleWithCardNameOfSticker() {

        let dummyView = DummyView()
        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .sticker
        }

        cards.cards[2].cardId = "3333"
        cards.cards[2].alias = ""
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[2].cardId

        //given
        sut.cards = cards
        sut.cards!.cards[cardIndex].alias = ""
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .sticker

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.navigationTitle == sut.cards!.cards[2].title?.name)

    }

    func test_givenACardsAndDisplayItemsWithStickerCardRelated_whenICallCardShowedAtIndexAndSelectedCardSticker_thenICallViewShowBalancesInfo() {

        let dummyView = DummyView()
        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .sticker
        }

        cards.cards[2].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[2].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .sticker

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.isCalledShowBalancesInfo == true)

    }

    func test_givenACardsAndDisplayItemsWithStickerCardRelated_whenICallCardShowedAtIndexAndSelectedCardSticker_thenICallViewShowBalancesInfoWithDisplayInfo() {

        let dummyView = DummyView()
        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .sticker
        }

        cards.cards[2].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[2].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .sticker

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.displayDataBalances != nil)

    }

    func test_givenACardsAndDisplayItemsWithStickerCardRelated_whenICallCardShowedAtIndexAndSelectedCardSticker_thenICallViewShowBalancesInfoWithDisplayInfoAtIndexOfSticker() {

        let dummyView = DummyView()
        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .sticker
        }

        cards.cards[2].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[2].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .sticker

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.displayDataBalances?.cardBO === sut.cards!.cards[2])

    }

    func test_givenACardsAndDisplayItemsWithStickerCardRelated_whenICallCardShowedAtIndexAndSelectedCardSticker_thenICallViewShowOnOfInfo() {

        let dummyView = DummyView()
        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .sticker
        }

        cards.cards[2].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[2].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .sticker

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.isCalledShowOnOffInfo == true)

    }

    func test_givenACardsAndDisplayItemsWithStickerCardRelated_whenICallCardShowedAtIndexAndSelectedCardSticker_thenICallViewShowOnOfInfoWithDisplayInfo() {

        let dummyView = DummyView()
        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .sticker
        }

        cards.cards[2].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[2].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .sticker

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.displayDataOnOff != nil)

    }

    func test_givenACardsAndDisplayItemsWithStickerCardRelated_whenICallCardShowedAtIndexAndSelectedCardSticker_thenICallViewShowOnOffInfoWithDisplayInfoWithCardBOAtIndexOfSticker() {

        let dummyView = DummyView()
        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .sticker
        }

        cards.cards[2].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[2].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .sticker

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.displayDataOnOff?.cardBO === sut.cards!.cards[2])

    }

    func test_givenACardsAndDisplayItemsWithStickerCardRelated_whenICallCardShowedAtIndexAndSelectedCardSticker_thenICallViewShowOperativeBarInfo() {

        let dummyView = DummyView()
        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .sticker
        }

        cards.cards[2].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[2].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .sticker

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.isCalledShowOperativeBarInfo == true)

    }

    func test_givenACardsAndDisplayItemsWithStickerCardRelated_whenICallCardShowedAtIndexAndSelectedCardSticker_thenICallViewShowOperativeBarInfoWithDisplayInfo() {

        let dummyView = DummyView()
        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .sticker
        }

        cards.cards[2].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[2].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .sticker

        //when
        sut.cardShowed(atIndex: cardIndex)

        //then
        XCTAssert(dummyView.displayDataOperativesBarInfo != nil)

    }

    // MARK: - override goToSearchTransactionsForCard (physical)

    func test_givenACards_whenICallGoToSearchTransacactionsForCardAtIndexWithSelectedCardPhysical_thenICallBusManagerSecondScreen() {

        let index = 0

        let dummyView = DummyView()

        sut.view = dummyView

        //given
        sut.cards = CardsGenerator.getCardBOWithCardTypeWithOutRelatedCard(withCardType: "PREPAID_CARD")
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![index].displayCardData as! DisplayCardDataA).selectedCard = .physical

        let cardsTransport = CardsTransport(cardBO: sut.cards!.cards[index], flag: true)

        //when
        sut.goToSearchTransactionsForCard(atIndex: index)

        //then
        XCTAssert(dummyBusManager.isCalledNavigateSecondCardsScreen == true)
        XCTAssert(cardsTransport.cardBO === dummyBusManager.dummyCardsTransport.cardBO)
        XCTAssert(cardsTransport.flagNavigation == dummyBusManager.dummyCardsTransport.flagNavigation)

    }

    func test_givenACards_whenICallGoToSearchTransacactionsForCardAtIndexWithSelectedCardPhysical_thenIPublishCardBOOfCardAsIndexWithPhysicalCardBO() {

        let index = 0

        let dummyView = DummyView()

        sut.view = dummyView

        //given
        sut.cards = CardsGenerator.getCardBOWithCardTypeWithOutRelatedCard(withCardType: "PREPAID_CARD")
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![index].displayCardData as! DisplayCardDataA).selectedCard = .physical

        let cardBO = sut.cards!.cards[index]

        //when
        sut.goToSearchTransactionsForCard(atIndex: index)

        //then
        XCTAssert(cardBO === dummyBusManager.dummyCardsTransport.cardBO)
        XCTAssert(dummyBusManager.dummyCardsTransport.flagNavigation == true)

    }

    // MARK: - override goToSearchTransactionsForCard (digital)

    func test_givenACardsAndVirtualCardRelatedToCardBO_whenICallGoToSearchTransacactionsForCardAtIndexWithSelectedCardPhysical_thenICallBusManagerSecondScreen() {

        let dummyView = DummyView()

        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .virtual
        }

        cards.cards[1].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[1].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .digital

        //when
        sut.goToSearchTransactionsForCard(atIndex: cardIndex)

        //then
        XCTAssert(dummyBusManager.isCalledNavigateSecondCardsScreen == true)

    }

    func test_givenACardsAndVirtualCardRelatedToCardBO_whenICallGoToSearchTransacactionsForCardAtIndexWithSelectedCardVirtual_thenIPublishCardBOOfCardAsIndexWithVirtualCardBO() {

        let dummyView = DummyView()

        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .virtual
        }

        cards.cards[1].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[1].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .digital

        let cardBO = sut.cards!.cards[1]

        //when
        sut.goToSearchTransactionsForCard(atIndex: cardIndex)

        //then
        XCTAssert(cardBO === dummyBusManager.dummyCardsTransport.cardBO)
        XCTAssert(dummyBusManager.dummyCardsTransport.flagNavigation == true)
    }

    // MARK: - override goToSearchTransactionsForCard (sticker)

    func test_givenACardsAndStickerCardRelatedToCardBO_whenICallGoToSearchTransacactionsForCardAtIndexWithSelectedCardSticker_thenICallBusManagerSecondScreen() {

        let dummyView = DummyView()

        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .sticker
        }

        cards.cards[2].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[2].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .sticker
        //let cardsTransport = CardsTransport.init(cardBo: sut.cards!.cards[cardIndex], flag: true)
        //when
        sut.goToSearchTransactionsForCard(atIndex: cardIndex)

        //then
        XCTAssert(dummyBusManager.isCalledNavigateSecondCardsScreen == true)

    }

    func test_givenACardsAndStickerCardRelatedToCardBO_whenICallGoToSearchTransacactionsForCardAtIndexWithSelectedCardSticker_thenIPublishCardBOOfCardAsIndexWithStickerCardBO() {

        let dummyView = DummyView()

        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .sticker
        }

        cards.cards[2].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[2].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .sticker

        let cardBO = sut.cards!.cards[2]

        //when
        sut.goToSearchTransactionsForCard(atIndex: cardIndex)

        //then
        XCTAssert(cardBO === dummyBusManager.dummyCardsTransport.cardBO)
        XCTAssert(dummyBusManager.dummyCardsTransport.flagNavigation == true)
    }

    // MARK: - override goToTransactionsForCard (physical)

    func test_givenACards_whenICallGoToTransacactionsForCardAtIndexWithSelectedCardPhysical_thenICallBusManagerSecondScreen() {

        let index = 0

        let dummyView = DummyView()

        sut.view = dummyView

        //given
        sut.cards = CardsGenerator.getCardBOWithCardTypeWithOutRelatedCard(withCardType: "PREPAID_CARD")
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![index].displayCardData as! DisplayCardDataA).selectedCard = .physical
        let cardBo = sut.cards!.cards[index]
        //when
        sut.goToTransactionsForCard(atIndex: index)

        //then
        XCTAssert(dummyBusManager.isCalledNavigateNextCardsScreen == true)
        XCTAssert(cardBo === dummyBusManager.dummyCardBO)

    }

    func test_givenACards_whenICallGoToTransacactionsForCardAtIndexWithSelectedCardPhysical_thenIPublishCardBOOfCardAsIndexWithPhysicalCardBO() {

        let index = 0

        let dummyView = DummyView()

        sut.view = dummyView

        //given
        sut.cards = CardsGenerator.getCardBOWithCardTypeWithOutRelatedCard(withCardType: "PREPAID_CARD")
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![index].displayCardData as! DisplayCardDataA).selectedCard = .physical

        let cardBO = sut.cards!.cards[index]

        //when
        sut.goToTransactionsForCard(atIndex: index)

        //then
        XCTAssert(cardBO === dummyBusManager.dummyCardBO)

    }

    // MARK: - override goToTransactionsForCard (digital)

    func test_givenACardsAndVirtualCardRelatedToCardBO_whenICallGoToTransacactionsForCardAtIndexWithSelectedCardPhysical_thenICallBusManagerSecondScreen() {

        let dummyView = DummyView()

        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .virtual
        }

        cards.cards[1].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[1].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .digital
        let cardBo = sut.cards!.cards[1]
        //when
        sut.goToTransactionsForCard(atIndex: cardIndex)

        //then
        XCTAssert(dummyBusManager.isCalledNavigateNextCardsScreen == true)
        XCTAssert(cardBo === dummyBusManager.dummyCardBO)

    }

    func test_givenACardsAndVirtualCardRelatedToCardBO_whenICallGoToTransacactionsForCardAtIndexWithSelectedCardVirtual_thenIPublishCardBOOfCardAsIndexWithVirtualCardBO() {

        let dummyView = DummyView()

        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .virtual
        }

        cards.cards[1].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[1].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .digital

        let cardBO = sut.cards!.cards[1]

        //when
        sut.goToTransactionsForCard(atIndex: cardIndex)

        //then
        XCTAssert(cardBO === dummyBusManager.dummyCardBO)

    }

    // MARK: - override goToTransactionsForCard (sticker)

    func test_givenACardsAndStickerCardRelatedToCardBO_whenICallGoToTransacactionsForCardAtIndexWithSelectedCardSticker_thenICallBusManagerSecondScreen() {

        let dummyView = DummyView()

        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .sticker
        }

        cards.cards[2].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[2].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .sticker
        let cardBo = sut.cards!.cards[2]
        //when
        sut.goToTransactionsForCard(atIndex: cardIndex)

        //then
        XCTAssert(dummyBusManager.isCalledNavigateNextCardsScreen == true)
        XCTAssert(cardBo === dummyBusManager.dummyCardBO)

    }

    func test_givenACardsAndStickerCardRelatedToCardBO_whenICallGoToTransacactionsForCardAtIndexWithSelectedCardSticker_thenIPublishCardBOOfCardAsIndexWithStickerCardBO() {

        let dummyView = DummyView()

        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .sticker
        }

        cards.cards[2].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[2].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .sticker

        let cardBO = sut.cards!.cards[2]

        //when
        sut.goToTransactionsForCard(atIndex: cardIndex)

        //then
        XCTAssert(cardBO === dummyBusManager.dummyCardBO)

    }

    // MARK: - goToTransactionDetail (physical)

    func test_givenAPresenter_whenICallGoToTransactionDetailWithSelectedCardPhysical_thenICallRouterPresentTransactionsDetail() {

        let cardIndex = 0

        let dummyView = DummyView()

        sut.view = dummyView

        //given
        sut.cards = CardsGenerator.getCardBOWithCardTypeWithOutRelatedCard(withCardType: "PREPAID_CARD")
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .physical

        let transactionEntity = TransactionEntityDummyGenerator.transactionEntity()
        let transactionBO = TransactionBO(transactionEntity: transactionEntity)
        let transactionDisplayItem = TransactionDisplayItem(fromTransactionBO: transactionBO!)

        let cardBO = sut.cards!.cards[cardIndex]

        // when
        sut.goToTransactionDetail(withTransactionDisplayItem: transactionDisplayItem, forCardAtIndex: cardIndex)

        // then
        XCTAssert(dummyBusManager.isCalledNavigateThirdCardsScreen == true)
        XCTAssert(cardBO === dummyBusManager.dummyTransactionsTransport.cardBO)
        //XCTAssert(transactionDisplayItem == dummyBusManager.dummyTransactionsTransport.transactionDisplayItem)

    }

    func test_givenAPresenter_whenICallGoToTransactionDetailWithSelectedCardPhysical_thenIPublishCardBOOfCardAsIndexWithPhysicalCardBO() {

        let cardIndex = 0

        let dummyView = DummyView()

        sut.view = dummyView

        //given
        sut.cards = CardsGenerator.getCardBOWithCardTypeWithOutRelatedCard(withCardType: "PREPAID_CARD")
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .physical

        let transactionEntity = TransactionEntityDummyGenerator.transactionEntity()
        let transactionBO = TransactionBO(transactionEntity: transactionEntity)
        let transactionDisplayItem = TransactionDisplayItem(fromTransactionBO: transactionBO!)

        // when
        sut.goToTransactionDetail(withTransactionDisplayItem: transactionDisplayItem, forCardAtIndex: cardIndex)

        // then
        XCTAssert(sut.cards?.cards[cardIndex] === dummyBusManager.dummyTransactionsTransport.cardBO)

    }

    // MARK: - goToTransactionDetail (virtual)

    func test_givenAPresenter_whenICallGoToTransactionDetailWithSelectedCardVirtual_thenICallRouterPresentTransactionsDetail() {

        let dummyView = DummyView()

        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .virtual
        }

        cards.cards[1].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[1].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .digital

        let transactionEntity = TransactionEntityDummyGenerator.transactionEntity()
        let transactionBO = TransactionBO(transactionEntity: transactionEntity)
        let transactionDisplayItem = TransactionDisplayItem(fromTransactionBO: transactionBO!)

        // when
        sut.goToTransactionDetail(withTransactionDisplayItem: transactionDisplayItem, forCardAtIndex: cardIndex)

        // then
        XCTAssert(dummyBusManager.isCalledNavigateThirdCardsScreen == true)

    }

    func test_givenAPresenter_whenICallGoToTransactionDetailWithSelectedCardPhysical_thenIPublishCardBOOfCardAsIndexWithVirtualCardBO() {

        let dummyView = DummyView()

        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .virtual
        }

        cards.cards[1].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[1].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .digital

        let transactionEntity = TransactionEntityDummyGenerator.transactionEntity()
        let transactionBO = TransactionBO(transactionEntity: transactionEntity)
        let transactionDisplayItem = TransactionDisplayItem(fromTransactionBO: transactionBO!)

        // when
        sut.goToTransactionDetail(withTransactionDisplayItem: transactionDisplayItem, forCardAtIndex: cardIndex)

        // then
        XCTAssert(sut.cards?.cards[1] === dummyBusManager.dummyTransactionsTransport.cardBO)

    }

    // MARK: - goToTransactionDetail (sticker)

    func test_givenAPresenter_whenICallGoToTransactionDetailWithSelectedCardSticker_thenICallRouterPresentTransactionsDetail() {

        let dummyView = DummyView()

        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .sticker
        }

        cards.cards[2].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[2].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .sticker

        let transactionEntity = TransactionEntityDummyGenerator.transactionEntity()
        let transactionBO = TransactionBO(transactionEntity: transactionEntity)
        let transactionDisplayItem = TransactionDisplayItem(fromTransactionBO: transactionBO!)

        // when
        sut.goToTransactionDetail(withTransactionDisplayItem: transactionDisplayItem, forCardAtIndex: cardIndex)

        // then
        XCTAssert(dummyBusManager.isCalledNavigateThirdCardsScreen == true)

    }

    func test_givenAPresenter_whenICallGoToTransactionDetailWithSelectedCardSticker_thenIPublishCardBOOfCardAsIndexWithStickerCardBO() {

        let dummyView = DummyView()

        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .sticker
        }

        cards.cards[2].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[2].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .sticker

        let transactionEntity = TransactionEntityDummyGenerator.transactionEntity()
        let transactionBO = TransactionBO(transactionEntity: transactionEntity)
        let transactionDisplayItem = TransactionDisplayItem(fromTransactionBO: transactionBO!)

        // when
        sut.goToTransactionDetail(withTransactionDisplayItem: transactionDisplayItem, forCardAtIndex: cardIndex)

        // then
        XCTAssert(sut.cards?.cards[2] === dummyBusManager.dummyTransactionsTransport.cardBO)

    }

    // MARK: - leftCardButtonPressed (physical)

    func test_givenAnOperativeCardAndDisplayLeftButtonItemToSeeCVV_whenIleftCardButtonAtIndex_thenIDoNotCallViewShowToastSuccess() {

        // given
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyView = DummyView()
        sut.view = dummyView

        sut.cards = cards

        let displayItems = sut.createDisplayDataItems()

        sut.displayItems = displayItems
        sut.displayItems.items![0].displayCardData?.leftButtonDisplayData?.actionType = .seeCVV

        // when
        sut.leftCardButtonPressed(atIndex: 0)

        // then
        XCTAssert(dummyView.isCalledShowSuccessToast == false)

    }

    func test_givenAnOperativeCardAndDisplayLeftButtonItemToActivateCard_whenIleftCardButtonAtIndex_thenIDoNotCallViewShowToastSuccess() {

        // given
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyView = DummyView()
        sut.view = dummyView

        sut.cards = cards

        let displayItems = sut.createDisplayDataItems()

        sut.displayItems = displayItems
        sut.displayItems.items![0].displayCardData?.leftButtonDisplayData?.actionType = .activateCard

        // when
        sut.leftCardButtonPressed(atIndex: 0)

        // then
        XCTAssert(dummyView.isCalledShowSuccessToast == false)

    }

    func test_givenAnOperativeCardAndDisplayLeftButtonItemToActivateCardExplanation_whenIleftCardButtonAtIndex_thenIDoNotCallViewShowToastSuccess() {

        // given
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyView = DummyView()
        sut.view = dummyView

        sut.cards = cards

        let displayItems = sut.createDisplayDataItems()

        sut.displayItems = displayItems
        sut.displayItems.items![0].displayCardData?.leftButtonDisplayData?.actionType = .activateCardExplanation

        // when
        sut.leftCardButtonPressed(atIndex: 0)

        // then
        XCTAssert(dummyView.isCalledShowSuccessToast == false)

    }

    func test_givenAnOperativeCardAndDisplayLeftButtonItemToCopyPan_whenIleftCardButtonAtIndex_thenICallViewShowToastSuccess() {

        // given
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyView = DummyView()
        sut.view = dummyView

        sut.cards = cards

        let displayItems = sut.createDisplayDataItems()

        sut.displayItems = displayItems
        sut.displayItems.items![0].displayCardData?.leftButtonDisplayData?.actionType = .copyPan

        // when
        sut.leftCardButtonPressed(atIndex: 0)

        // then
        XCTAssert(dummyView.isCalledShowSuccessToast == true)

    }

    func test_givenAnOperativeCardAndDisplayLeftButtonItemToCopyPan_whenIleftCardButtonAtIndex_thenICallViewShowToastSuccessWithCopySuccessText() {

        // given
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyView = DummyView()
        sut.view = dummyView

        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        sut.displayItems.items![0].displayCardData?.leftButtonDisplayData?.actionType = .copyPan

        // when
        sut.leftCardButtonPressed(atIndex: 0)

        // then
        XCTAssert(dummyView.toastMessage == Localizables.cards.key_cards_copy_success_text)

    }

    func test_givenAnOperativeCardAndDisplayLeftButtonItemToSeeCVV_whenIleftCardButtonAtIndex_thenICallViewCopyPan() {

        // given
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyView = DummyView()
        sut.view = dummyView

        sut.cards = cards

        let displayItems = sut.createDisplayDataItems()

        sut.displayItems = displayItems
        sut.displayItems.items![0].displayCardData?.leftButtonDisplayData?.actionType = .seeCVV

        // when
        sut.leftCardButtonPressed(atIndex: 0)

        // then
        XCTAssert(dummyView.isCalledCopyPan == false)

    }

    func test_givenAnOperativeCardAndDisplayLeftButtonItemToActivateCard_whenIleftCardButtonAtIndex_thenIDoNotCallViewCopyPan() {

        // given
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyView = DummyView()
        sut.view = dummyView

        sut.cards = cards

        let displayItems = sut.createDisplayDataItems()

        sut.displayItems = displayItems
        sut.displayItems.items![0].displayCardData?.leftButtonDisplayData?.actionType = .activateCard

        // when
        sut.leftCardButtonPressed(atIndex: 0)

        // then
        XCTAssert(dummyView.isCalledCopyPan == false)

    }

    func test_givenAnOperativeCardAndDisplayLeftButtonItemToActivateCardExplanation_whenIleftCardButtonAtIndex_thenIDoNotCallViewCopyPan() {

        // given
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyView = DummyView()
        sut.view = dummyView

        sut.cards = cards

        let displayItems = sut.createDisplayDataItems()

        sut.displayItems = displayItems
        sut.displayItems.items![0].displayCardData?.leftButtonDisplayData?.actionType = .activateCardExplanation

        // when
        sut.leftCardButtonPressed(atIndex: 0)

        // then
        XCTAssert(dummyView.isCalledCopyPan == false)

    }

    func test_givenAnOperativeCardAndDisplayLeftButtonItemToCopyPan_whenIleftCardButtonAtIndex_thenICallViewCopyPan() {

        // given
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyView = DummyView()
        sut.view = dummyView

        sut.cards = cards

        let displayItems = sut.createDisplayDataItems()

        sut.displayItems = displayItems
        sut.displayItems.items![0].displayCardData?.leftButtonDisplayData?.actionType = .copyPan

        // when
        sut.leftCardButtonPressed(atIndex: 0)

        // then
        XCTAssert(dummyView.isCalledCopyPan == true)

    }

    func test_givenAnOperativeCardAndDisplayLeftButtonItemToActivateCardExplanation_whenIleftCardButtonAtIndex_thenICallViewCopyPanWithPanWithPanNumberTrimmedAsOfCardsAsParameter() {

        // given
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyView = DummyView()
        sut.view = dummyView

        sut.cards = cards

        let displayItems = sut.createDisplayDataItems()

        sut.displayItems = displayItems
        sut.displayItems.items![0].displayCardData?.leftButtonDisplayData?.actionType = .copyPan

        // when
        sut.leftCardButtonPressed(atIndex: 0)

        // then
        XCTAssert(dummyView.panCopied == sut.displayItems.items![0].displayCardData?.cardBO?.number.replacingOccurrences(of: " ", with: ""))

    }

    func test_givenAnOperativeCardAndDisplayLeftButtonItemToActivateCard_whenIleftCardButtonAtIndex_thenICallBusManagerPublishDataActivatedCard() {

        // given
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyView = DummyView()
        sut.view = dummyView

        sut.cards = cards

        let displayItems = sut.createDisplayDataItems()

        sut.displayItems = displayItems
        sut.displayItems.items![0].displayCardData?.leftButtonDisplayData?.actionType = .activateCard

        // when
        sut.leftCardButtonPressed(atIndex: 0)

        //then
        XCTAssert(dummyBusManager.isCalledPublishDataActivateCard == true)
        XCTAssert(sut.displayItems.items![0].displayCardData!.cardBO?.alias == dummyBusManager.dummyActivateCardBO.alias)
        XCTAssert(sut.displayItems.items![0].displayCardData!.cardBO?.number == dummyBusManager.dummyActivateCardBO.cardNumber)
        XCTAssert(sut.displayItems.items![0].displayCardData!.cardBO?.imageFront == dummyBusManager.dummyActivateCardBO.cardImageUrl)
    }

    func test_givenAnOperativeCardAndDisplayLeftButtonItemToActivateCard_whenIleftCardButtonAtIndex_thenICallBusManagerPublishDataActivatedCardSelected() {

        // given
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyView = DummyView()
        sut.view = dummyView

        sut.cards = cards

        let displayItems = sut.createDisplayDataItems()

        sut.displayItems = displayItems
        sut.displayItems.items![0].displayCardData?.leftButtonDisplayData?.actionType = .activateCard

        // when
        sut.leftCardButtonPressed(atIndex: 0)

        //then
        XCTAssert(dummyBusManager.isCalledPublishDataActivateCardSelect == true)

        if let cardNumberSuffix = sut.displayItems.items![0].displayCardData!.cardBO?.number.suffix(4) {
            XCTAssert(cardNumberSuffix == dummyBusManager.dummyActivateCardSuccessObjectCells.cardNumber)
        }

    }

    func test_givenAnOperativeCardAndDisplayLeftButtonItemToActivateCard_whenIleftCardButtonAtIndex_thenICallNavigateActivateCard() {

        // given
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyView = DummyView()
        sut.view = dummyView

        sut.cards = cards

        let displayItems = sut.createDisplayDataItems()

        sut.displayItems = displayItems
        sut.displayItems.items![0].displayCardData?.leftButtonDisplayData?.actionType = .activateCard

        // when
        sut.leftCardButtonPressed(atIndex: 0)

        //then
        XCTAssert(dummyBusManager.isCalledNavigateActivateCard == true)
    }

    func test_givenAnyCardBOAndDisplayLeftButtonItemToActivateCardExplanation_whenICallLeftCardButtonPressedAtIndex_thenICallBusManagerDetailHelpScreen() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let cards = CardsGenerator.getCardBOWithAlias()
        sut.cards = cards

        let displayItems = sut.createDisplayDataItems()
        sut.displayItems = displayItems
        sut.displayItems.items![0].displayCardData?.leftButtonDisplayData?.actionType = .activateCardExplanation

        // when
        sut.leftCardButtonPressed(atIndex: 0)

        //then
        XCTAssert(dummyBusManager.isCalledNavigateToDetailHelpScreen == true)
    }

    func test_givenAnyCardBOAndDisplayLeftButtonItemToActivateCardExplanation_whenICallLeftCardButtonPressedAtIndex_thenICallPublishHelpDetailTransportWithHelpId() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let cards = CardsGenerator.getCardBOWithAlias()
        sut.cards = cards

        let displayItems = sut.createDisplayDataItems()
        sut.displayItems = displayItems
        sut.displayItems.items![0].displayCardData?.leftButtonDisplayData?.actionType = .activateCardExplanation

        // when
        sut.leftCardButtonPressed(atIndex: 0)

        //then
        XCTAssert(dummyBusManager.dummyHelpDetailTransport.helpId == sut.idHelpOnOff)
    }

    // MARK: - leftCardButtonPressed (virtual)

    func test_givenAnOperativeCardAndDisplayLeftButtonItemToSeeCVV_whenIleftCardButtonAtIndexWithSelectedCardDigital_thenIDoNotCallViewShowToastSuccess() {

        let dummyView = DummyView()

        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .virtual
        }

        cards.cards[1].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[1].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .digital
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).virtualCardDisplayData?.leftButtonDisplayData?.actionType = .seeCVV

        // when
        sut.leftCardButtonPressed(atIndex: cardIndex)

        // then
        XCTAssert(dummyView.isCalledShowSuccessToast == false)

    }

    func test_givenAnOperativeCardAndDisplayLeftButtonItemToActivateCard_whenIleftCardButtonAtIndexWithSelectedCardDigital_thenIDoNotCallViewShowToastSuccess() {

        let dummyView = DummyView()

        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .virtual
        }

        cards.cards[1].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[1].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .digital
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).virtualCardDisplayData?.leftButtonDisplayData?.actionType = .activateCard

        // when
        sut.leftCardButtonPressed(atIndex: 0)

        // then
        XCTAssert(dummyView.isCalledShowSuccessToast == false)

    }

    func test_givenAnOperativeCardAndDisplayLeftButtonItemToActivateCardExplanation_whenIleftCardButtonAtIndexWithSelectedCardDigital_thenIDoNotCallViewShowToastSuccess() {

        let dummyView = DummyView()

        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .virtual
        }

        cards.cards[1].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[1].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .digital
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).virtualCardDisplayData?.leftButtonDisplayData?.actionType = .activateCardExplanation

        // when
        sut.leftCardButtonPressed(atIndex: 0)

        // then
        XCTAssert(dummyView.isCalledShowSuccessToast == false)

    }

    func test_givenAnOperativeCardAndDisplayLeftButtonItemToCopyPan_whenIleftCardButtonAtIndexWithSelectedCardDigital_thenICallViewShowToastSuccess() {

        let dummyView = DummyView()

        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .virtual
        }

        cards.cards[1].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[1].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .digital
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).virtualCardDisplayData?.leftButtonDisplayData?.actionType = .copyPan

        // when
        sut.leftCardButtonPressed(atIndex: 0)

        // then
        XCTAssert(dummyView.isCalledShowSuccessToast == true)

    }

    func test_givenAnOperativeCardAndDisplayLeftButtonItemToCopyPan_whenIleftCardButtonAtIndexWithSelectedCardDigital_thenICallViewShowToastSuccessWithCopySuccessText() {

        let dummyView = DummyView()

        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .virtual
        }

        cards.cards[1].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[1].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .digital
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).virtualCardDisplayData?.leftButtonDisplayData?.actionType = .copyPan

        // when
        sut.leftCardButtonPressed(atIndex: 0)

        // then
        XCTAssert(dummyView.toastMessage == Localizables.cards.key_cards_copy_success_text)

    }

    func test_givenAnOperativeCardAndDisplayLeftButtonItemToSeeCVV_whenIleftCardButtonAtIndexWithSelectedCardDigital_thenICallViewCopyPan() {

        let dummyView = DummyView()

        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .virtual
        }

        cards.cards[1].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[1].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .digital
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).virtualCardDisplayData?.leftButtonDisplayData?.actionType = .seeCVV

        // when
        sut.leftCardButtonPressed(atIndex: 0)

        // then
        XCTAssert(dummyView.isCalledCopyPan == false)

    }

    func test_givenAnOperativeCardAndDisplayLeftButtonItemToActivateCard_whenIleftCardButtonAtIndexWithSelectedCardDigital_thenIDoNotCallViewCopyPan() {

        let dummyView = DummyView()

        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .virtual
        }

        cards.cards[1].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[1].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .digital
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).virtualCardDisplayData?.leftButtonDisplayData?.actionType = .activateCard

        // when
        sut.leftCardButtonPressed(atIndex: 0)

        // then
        XCTAssert(dummyView.isCalledCopyPan == false)

    }

    func test_givenAnOperativeCardAndDisplayLeftButtonItemToActivateCardExplanation_whenIleftCardButtonAtIndexWithSelectedCardDigital_thenIDoNotCallViewCopyPan() {

        let dummyView = DummyView()

        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .virtual
        }

        cards.cards[1].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[1].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .digital
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).virtualCardDisplayData?.leftButtonDisplayData?.actionType = .activateCardExplanation

        // when
        sut.leftCardButtonPressed(atIndex: 0)

        // then
        XCTAssert(dummyView.isCalledCopyPan == false)

    }

    func test_givenAnOperativeCardAndDisplayLeftButtonItemToCopyPan_whenIleftCardButtonAtIndexWithSelectedCardDigital_thenICallViewCopyPan() {

        let dummyView = DummyView()

        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .virtual
        }

        cards.cards[1].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[1].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .digital
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).virtualCardDisplayData?.leftButtonDisplayData?.actionType = .copyPan

        // when
        sut.leftCardButtonPressed(atIndex: 0)

        // then
        XCTAssert(dummyView.isCalledCopyPan == true)

    }

    func test_givenAnOperativeCardAndDisplayLeftButtonItemToActivateCardExplanation_whenIleftCardButtonAtIndexWithSelectedCardDigital_thenICallViewCopyPanWithPanWithPanNumberTrimmedAsOfCardsAsParameter() {

        let dummyView = DummyView()

        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .virtual
        }

        cards.cards[1].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[1].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .digital
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).virtualCardDisplayData?.leftButtonDisplayData?.actionType = .copyPan

        // when
        sut.leftCardButtonPressed(atIndex: 0)

        // then
        XCTAssert(dummyView.panCopied == (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).virtualCardDisplayData?.cardBO?.number.replacingOccurrences(of: " ", with: ""))

    }

    // MARK: - leftCardButtonPressed (sticker)

    func test_givenAnOperativeCardAndDisplayLeftButtonItemToSeeCVV_whenIleftCardButtonAtIndexWithSelectedCardSticker_thenIDoNotCallViewShowToastSuccess() {

        let dummyView = DummyView()

        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .sticker
        }

        cards.cards[1].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[1].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .sticker
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).stickerCardDisplayData?.leftButtonDisplayData?.actionType = .seeCVV

        // when
        sut.leftCardButtonPressed(atIndex: cardIndex)

        // then
        XCTAssert(dummyView.isCalledShowSuccessToast == false)

    }

    func test_givenAnOperativeCardAndDisplayLeftButtonItemToActivateCard_whenIleftCardButtonAtIndexWithSelectedCardSticker_thenIDoNotCallViewShowToastSuccess() {

        let dummyView = DummyView()

        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .sticker
        }

        cards.cards[1].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[1].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .sticker
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).stickerCardDisplayData?.leftButtonDisplayData?.actionType = .activateCard

        // when
        sut.leftCardButtonPressed(atIndex: 0)

        // then
        XCTAssert(dummyView.isCalledShowSuccessToast == false)

    }

    func test_givenAnOperativeCardAndDisplayLeftButtonItemToActivateCardExplanation_whenIleftCardButtonAtIndexWithSelectedCardSticker_thenIDoNotCallViewShowToastSuccess() {

        let dummyView = DummyView()

        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .sticker
        }

        cards.cards[1].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[1].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .sticker
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).stickerCardDisplayData?.leftButtonDisplayData?.actionType = .activateCardExplanation

        // when
        sut.leftCardButtonPressed(atIndex: 0)

        // then
        XCTAssert(dummyView.isCalledShowSuccessToast == false)

    }

    func test_givenAnOperativeCardAndDisplayLeftButtonItemToCopyPan_whenIleftCardButtonAtIndexWithSelectedCardSticker_thenICallViewShowToastSuccess() {

        let dummyView = DummyView()

        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .sticker
        }

        cards.cards[1].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[1].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .sticker
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).stickerCardDisplayData?.leftButtonDisplayData?.actionType = .copyPan

        // when
        sut.leftCardButtonPressed(atIndex: 0)

        // then
        XCTAssert(dummyView.isCalledShowSuccessToast == true)

    }

    func test_givenAnOperativeCardAndDisplayLeftButtonItemToCopyPan_whenIleftCardButtonAtIndexWithSelectedCardSticker_thenICallViewShowToastSuccessWithCopySuccessText() {

        let dummyView = DummyView()

        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .sticker
        }

        cards.cards[1].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[1].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .sticker
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).stickerCardDisplayData?.leftButtonDisplayData?.actionType = .copyPan

        // when
        sut.leftCardButtonPressed(atIndex: 0)

        // then
        XCTAssert(dummyView.toastMessage == Localizables.cards.key_cards_copy_success_text)

    }

    func test_givenAnOperativeCardAndDisplayLeftButtonItemToSeeCVV_whenIleftCardButtonAtIndexWithSelectedCardSticker_thenICallViewCopyPan() {

        let dummyView = DummyView()

        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .sticker
        }

        cards.cards[1].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[1].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .sticker
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).stickerCardDisplayData?.leftButtonDisplayData?.actionType = .seeCVV

        // when
        sut.leftCardButtonPressed(atIndex: 0)

        // then
        XCTAssert(dummyView.isCalledCopyPan == false)

    }

    func test_givenAnOperativeCardAndDisplayLeftButtonItemToActivateCard_whenIleftCardButtonAtIndexWithSelectedCardSticker_thenIDoNotCallViewCopyPan() {

        let dummyView = DummyView()

        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .sticker
        }

        cards.cards[1].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[1].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .sticker
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).stickerCardDisplayData?.leftButtonDisplayData?.actionType = .activateCard

        // when
        sut.leftCardButtonPressed(atIndex: 0)

        // then
        XCTAssert(dummyView.isCalledCopyPan == false)

    }

    func test_givenAnOperativeCardAndDisplayLeftButtonItemToActivateCardExplanation_whenIleftCardButtonAtIndexWithSelectedCardSticker_thenIDoNotCallViewCopyPan() {

        let dummyView = DummyView()

        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .sticker
        }

        cards.cards[1].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[1].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .sticker
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).stickerCardDisplayData?.leftButtonDisplayData?.actionType = .activateCardExplanation

        // when
        sut.leftCardButtonPressed(atIndex: 0)

        // then
        XCTAssert(dummyView.isCalledCopyPan == false)

    }

    func test_givenAnOperativeCardAndDisplayLeftButtonItemToCopyPan_whenIleftCardButtonAtIndexWithSelectedCardSticker_thenICallViewCopyPan() {

        let dummyView = DummyView()

        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .sticker
        }

        cards.cards[1].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[1].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .sticker
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).stickerCardDisplayData?.leftButtonDisplayData?.actionType = .copyPan

        // when
        sut.leftCardButtonPressed(atIndex: 0)

        // then
        XCTAssert(dummyView.isCalledCopyPan == true)

    }

    func test_givenAnOperativeCardAndDisplayLeftButtonItemToActivateCardExplanation_whenIleftCardButtonAtIndexWithSelectedCardSticker_thenICallViewCopyPanWithPanWithPanNumberTrimmedAsOfCardsAsParameter() {

        let dummyView = DummyView()

        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .sticker
        }

        cards.cards[1].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[1].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .sticker
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).stickerCardDisplayData?.leftButtonDisplayData?.actionType = .copyPan

        // when
        sut.leftCardButtonPressed(atIndex: 0)

        // then
        XCTAssert(dummyView.panCopied == (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).stickerCardDisplayData?.cardBO?.number.replacingOccurrences(of: " ", with: ""))

    }

    // MARK: - obtainDigitalCardButtonPressed

    func test_givenAnyCardBO_whenICallObtainDigitalCardButtonPressedAtIndex_thenICallBusManagerPublishDataGenerateCard() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let cards = CardsGenerator.getCardBOWithAlias()
        sut.cards = cards

        let displayItems = sut.createDisplayDataItems()
        sut.displayItems = displayItems
        sut.displayItems.items![0].displayCardData?.leftButtonDisplayData?.actionType = .generateDigitalCard

        // when
        sut.obtainDigitalCardButtonPressed(atIndex: 0)

        //then
        XCTAssert(dummyBusManager.isCalledPublishDataGenerateCard == true)
    }

    func test_givenAnyCardBO_whenICallObtainDigitalCardButtonPressedAtIndex_thenICallNavigateGenerateCard() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        let cards = CardsGenerator.getCardBOWithAlias()
        sut.cards = cards

        let displayItems = sut.createDisplayDataItems()
        sut.displayItems = displayItems
        sut.displayItems.items![0].displayCardData?.leftButtonDisplayData?.actionType = .generateDigitalCard

        // when
        sut.obtainDigitalCardButtonPressed(atIndex: 0)

        //then
        XCTAssert(dummyBusManager.isCalledEventNavToGenerateDigitalCard == true)
    }

    // MARK: - pressedOperativeButton (physical)

    func test_givenDisplayDataWithLimitsOptionAndValidIndex_whenICallPressedOperativeButtonForAPhysicalCard_thenICallBusManagerPublishData() {

        // given
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyView = DummyView()
        sut.view = dummyView

        sut.cards = cards

        let displayItems = sut.createDisplayDataItems()

        sut.displayItems = displayItems

        let displayData = ButtonBarDisplayData(imageNamed: "", titleKey: "", optionKey: .limits)

        // when
        sut.pressedOperativeButton(atIndex: 0, withDisplayData: displayData)

        // then
        XCTAssert(dummyBusManager.isCalledNavigateFourCardsScreen == true)
    }

    func test_givenDisplayDataWithLimitsOptionAndValidIndex_whenICallPressedOperativeButtonForAPhysicalCard_thenIPublishCardBOOfCardAsIndexWithPhysicalCardBO() {

        // given
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyView = DummyView()
        sut.view = dummyView

        sut.cards = cards

        let displayItems = sut.createDisplayDataItems()

        sut.displayItems = displayItems

        let displayData = ButtonBarDisplayData(imageNamed: "", titleKey: "", optionKey: .limits)

        // when
        sut.pressedOperativeButton(atIndex: 0, withDisplayData: displayData)

        // then
        XCTAssert(sut.cards?.cards[0] === dummyBusManager.dummyCardBO)

    }

    func test_givenDisplayDataWithBlockOptionAndValidIndex_whenICallPressedOperativeButtonForAPhysicalCard_thenICallBusManagerNavigateNavigateBlockCard() {

        // given
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyView = DummyView()
        sut.view = dummyView

        sut.cards = cards

        let displayItems = sut.createDisplayDataItems()

        sut.displayItems = displayItems

        let displayData = ButtonBarDisplayData(imageNamed: "", titleKey: "", optionKey: .block)

        // when
        sut.pressedOperativeButton(atIndex: 0, withDisplayData: displayData)

        // then
        XCTAssert(dummyBusManager.isCalledEventNavToBlockCard == true)
        XCTAssert(dummyBusManager.isCalledEventNavToCancelCard == false)    }

    // MARK: - pressedOperativeButton (virtual)

    func test_givenDisplayDataWithLimitsOptionAndValidIndex_whenICallPressedOperativeButtonForAVirtualCard_thenICallBusManagerPublishData() {

        let dummyView = DummyView()

        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .virtual
        }

        cards.cards[1].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[1].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .digital

        let displayData = ButtonBarDisplayData(imageNamed: "", titleKey: "", optionKey: .limits)

        // when
        sut.pressedOperativeButton(atIndex: 0, withDisplayData: displayData)

        // then
        XCTAssert(dummyBusManager.isCalledNavigateFourCardsScreen == true)

    }

    func test_givenDisplayDataWithLimitsOptionAndValidIndex_whenICallPressedOperativeButtonForAVirtualCard_thenIPublishCardBOOfCardAsIndexWithVirtualCardBO() {

        let dummyView = DummyView()

        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .virtual
        }

        cards.cards[1].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[1].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .digital

        let displayData = ButtonBarDisplayData(imageNamed: "", titleKey: "", optionKey: .limits)

        // when
        sut.pressedOperativeButton(atIndex: 0, withDisplayData: displayData)

        // then
        XCTAssert(sut.cards?.cards[1] === dummyBusManager.dummyCardBO)

    }

    func test_givenDisplayDataWitBlockOptionAndValidIndex_whenICallPressedOperativeButtonForAVirtualCard_thenICallBusManagerNavigateNavigateCancelCard() {

        let dummyView = DummyView()

        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .virtual
        }

        cards.cards[1].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[1].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .digital

        let displayData = ButtonBarDisplayData(imageNamed: "", titleKey: "", optionKey: .block)

        // when
        sut.pressedOperativeButton(atIndex: 0, withDisplayData: displayData)

        // then
        XCTAssert(dummyBusManager.isCalledEventNavToCancelCard == true)
        XCTAssert(dummyBusManager.isCalledEventNavToBlockCard == false)

    }

    // MARK: - pressedOperativeButton (sticker)

    func test_givenDisplayDataWithLimitsOptionAndValidIndex_whenICallPressedOperativeButtonForAStickerCard_thenICallBusManagerPublishData() {

        let dummyView = DummyView()

        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .sticker
        }

        cards.cards[2].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[2].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .sticker

        let displayData = ButtonBarDisplayData(imageNamed: "", titleKey: "", optionKey: .limits)

        // when
        sut.pressedOperativeButton(atIndex: 0, withDisplayData: displayData)

        // then
        XCTAssert(dummyBusManager.isCalledNavigateFourCardsScreen == true)

    }

    func test_givenDisplayDataWithBlockOptionAndValidIndex_whenICallPressedOperativeButtonForAStickerCard_thenICallBusManagerNavigateNavigateBlockCard() {

        let dummyView = DummyView()

        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .sticker
        }

        cards.cards[2].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[2].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .sticker

        let displayData = ButtonBarDisplayData(imageNamed: "", titleKey: "", optionKey: .block)

        // when
        sut.pressedOperativeButton(atIndex: 0, withDisplayData: displayData)

        // then
        XCTAssert(dummyBusManager.isCalledEventNavToBlockCard == true)
        XCTAssert(dummyBusManager.isCalledEventNavToCancelCard == false)

    }

    func test_givenDisplayDataWithLimitsOptionAndValidIndex_whenICallPressedOperativeButtonForAStickerCard_thenIPublishCardBOOfCardAsIndexWithStickerCardBO() {

        let dummyView = DummyView()

        sut.view = dummyView
        let cardIndex = 0

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .sticker
        }

        cards.cards[2].cardId = "3333"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[2].cardId

        //given
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        (sut.displayItems.items![cardIndex].displayCardData as! DisplayCardDataA).selectedCard = .sticker

        let displayData = ButtonBarDisplayData(imageNamed: "", titleKey: "", optionKey: .limits)

        // when
        sut.pressedOperativeButton(atIndex: 0, withDisplayData: displayData)

        // then
        XCTAssert(sut.cards?.cards[2] === dummyBusManager.dummyCardBO)

    }

    func test_givenCardsWithRewardButDebit_whenCallShowInfo_thenIHidePoints() {

        //given
        let dummyView = DummyView()
        sut.view = dummyView
        sut.cards = CardsGenerator.getCardsWithRewards()

        // when
        sut.showInfo(forCardBO: sut.cards!.cards[0])

        // then
        XCTAssert(dummyView.isCalledHidePoints)

    }

    func test_givenCardsWithReward_whenCallShowInfo_thenIShowPoints() {

        //given
        let dummyView = DummyView()
        sut.view = dummyView
        sut.cards = CardsGenerator.getCardsWithRewards()

        // when
        sut.showInfo(forCardBO: sut.cards!.cards[7])

        // then
        XCTAssert(dummyView.isCalledShowPoints)

    }

    func test_givenCardsWithRewardButNotBancomerPoints_whenCallShowInfo_thenIHidePoints() {

        //given
        let dummyView = DummyView()
        sut.view = dummyView
        sut.cards = CardsGenerator.getCardsWithRewards()

        // when
        sut.showInfo(forCardBO: sut.cards!.cards[4])

        // then
        XCTAssert(dummyView.isCalledHidePoints)

    }

    // MARK: - Override changeCardStatus

    func test_givenACardThatsBeenSwitchUp_whenICallChangeCardStatus_thenICallprovideUpdatedCard() {

        let dummyPreferencesManager = PreferenceCardManagerDummy()
        let dummyCardBO = CardsGenerator.getCardBOCreditCard().cards.first!

        sut.preferences = dummyPreferencesManager

        //given
        dummyPreferencesManager.willSayIsFirstTimeOn = false
        dummyPreferencesManager.willSayIsFirstTimeOff = false

        //when
        sut.changeCardStatus(withCardStatus: true, andCard: dummyCardBO)

        //then
        XCTAssert(dummyInteractor.isCalledUpdateCards == true)
    }

    func test_givenACardThatWillBeTurnedOffForFirstTime_whenICallChangeCardStatus_thenIDontCallProvideUpdateCard() {

        let dummyPreferencesManager = PreferenceCardManagerDummy()
        let dummyCardBO = CardsGenerator.getCardBOCreditCard().cards.first!

        sut.preferences = dummyPreferencesManager

        //given
        dummyPreferencesManager.willSayIsFirstTimeOn = false
        dummyPreferencesManager.willSayIsFirstTimeOff = true

        //when
        sut.changeCardStatus(withCardStatus: true, andCard: dummyCardBO)

        //then
        XCTAssert(dummyInteractor.isCalledUpdateCards == false)

    }

    func test_givenACardThatsOffAndWillBeTurnedOnForFirstTime_whenICallChangeCardStatus_thenIDontCallProvideUpdateCard() {

        let dummyPreferencesManager = PreferenceCardManagerDummy()
        let dummyCardBO = CardsGenerator.getCardBOCreditCard().cards.first!

        dummyCardBO.activations.removeAll()

        sut.preferences = dummyPreferencesManager

        //given
        dummyPreferencesManager.willSayIsFirstTimeOn = true
        dummyPreferencesManager.willSayIsFirstTimeOff = false

        //when
        sut.changeCardStatus(withCardStatus: true, andCard: dummyCardBO)

        //then
        XCTAssert(dummyInteractor.isCalledUpdateCards == false)

    }

    func test_givenACardThatsBeenSwitchedUp_whenICallChangeCardStatus_thenICallShowLoading() {
        let dummyPreferencesManager = PreferenceCardManagerDummy()
        let dummyCardBO = CardsGenerator.getCardBOCreditCard().cards.first!

        sut.preferences = dummyPreferencesManager

        //given
        dummyPreferencesManager.willSayIsFirstTimeOn = false
        dummyPreferencesManager.willSayIsFirstTimeOff = false

        //when
        sut.changeCardStatus(withCardStatus: true, andCard: dummyCardBO)

        //then
        XCTAssert(dummyBusManager.isCalledShowLoading == true)
    }

    func test_givenACardThatsBeenSwitchedUp_whenICallChangeCardStatus_thenICallHideLoading() {
        let dummyPreferencesManager = PreferenceCardManagerDummy()
        let dummyCardBO = CardsGenerator.getCardBOCreditCard().cards.first!

        sut.preferences = dummyPreferencesManager

        //given
        dummyPreferencesManager.willSayIsFirstTimeOn = false
        dummyPreferencesManager.willSayIsFirstTimeOff = false

        //when
        sut.changeCardStatus(withCardStatus: true, andCard: dummyCardBO)

        //then
        XCTAssert(dummyBusManager.isCalledHideLoading == true)
    }

    func test_givenACardThatsOffWillBeSwitchedUp_whenICallChangeCardStatus_thenIDontCallHideLoading() {
        let dummyPreferencesManager = PreferenceCardManagerDummy()
        let dummyCardBO = CardsGenerator.getCardBOCreditCard().cards.first!
        dummyCardBO.activations.removeAll()
        sut.preferences = dummyPreferencesManager

        //given
        dummyPreferencesManager.willSayIsFirstTimeOn = true
        dummyPreferencesManager.willSayIsFirstTimeOff = false

        //when
        sut.changeCardStatus(withCardStatus: true, andCard: dummyCardBO)

        //then
        XCTAssert(dummyBusManager.isCalledHideLoading == false)
    }

    func test_givenACardsOffThatWillBeSwitchedUp_whenICallChangeCardStatus_thenIDontCallShowLoading() {

        let dummyPreferencesManager = PreferenceCardManagerDummy()
        let dummyCardBO = CardsGenerator.getCardBOCreditCard().cards.first!
        dummyCardBO.activations.removeAll()

        sut.preferences = dummyPreferencesManager

        //given
        dummyPreferencesManager.willSayIsFirstTimeOn = true
        dummyPreferencesManager.willSayIsFirstTimeOff = false

        //when
        sut.changeCardStatus(withCardStatus: true, andCard: dummyCardBO)

        //then
        XCTAssert(dummyBusManager.isCalledShowLoading == false)
    }

    func test_givenAnOperativeCard_whenICallChangeCardStatus_thenIDontCallUpdateActivations() {

        let dummyPreferencesManager = PreferenceCardManagerDummy()

        let cardsEntity = CardsGenerator.getCardEntity(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyCardBO = DummyCardBO(cardEntity: cardsEntity.data![0])

        dummyPreferencesManager.willSayIsFirstTimeOn = true

        sut.cardStatus = false
        sut.currentCard = dummyCardBO
        sut.preferences = dummyPreferencesManager

        // when
        sut.changeCardStatus(withCardStatus: true, andCard: dummyCardBO)

        // then
        XCTAssert(dummyInteractor.isCalledUpdateCards == false)

    }

    func test_givenACardThatWillBeSwitchedUp_whenICallChangeCardStatus_thenINavigateToOnOffHelp() {

        let dummyPreferencesManager = PreferenceCardManagerDummy()
        let dummyCardBO = CardsGenerator.getCardBOCreditCard().cards.first!

        sut.preferences = dummyPreferencesManager

        //given
        dummyPreferencesManager.willSayIsFirstTimeOn = true
        dummyPreferencesManager.willSayIsFirstTimeOff = true

        //when
        sut.changeCardStatus(withCardStatus: true, andCard: dummyCardBO)

        //then
        XCTAssert(dummyBusManager.isCalledNavigateToOnOffHelpScreen == true)
    }

    func test_givenACardThatWillBeSwitchedUp_whenICallChangeCardStatus_thenICallOnOffHelpReaction() {

        let dummyPreferencesManager = PreferenceCardManagerDummy()
        let dummyCardBO = CardsGenerator.getCardBOCreditCard().cards.first!

        sut.preferences = dummyPreferencesManager

        //given
        dummyPreferencesManager.willSayIsFirstTimeOn = true
        dummyPreferencesManager.willSayIsFirstTimeOff = true

        //when
        sut.changeCardStatus(withCardStatus: true, andCard: dummyCardBO)

        //then
        XCTAssert(dummyBusManager.isCalledEventNavToOnOffHelpReaction == true)
    }

    func test_givenACardOffAndFirstTimeOn_whenICallChangeCardStatus_thenICallOnOffHelpReaction() {

        let dummyPreferencesManager = PreferenceCardManagerDummy()
        let cardsEntity = CardsGenerator.getCardEntity(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyCardBO = DummyCardBO(cardEntity: cardsEntity.data![0])
        dummyCardBO.isOff = true

        sut.preferences = dummyPreferencesManager

        //given
        dummyPreferencesManager.willSayIsFirstTimeOn = true
        dummyPreferencesManager.willSayIsFirstTimeOff = false

        //when
        sut.changeCardStatus(withCardStatus: true, andCard: dummyCardBO)

        //then
        XCTAssert(dummyBusManager.isCalledEventNavToOnOffHelpReaction == true)
    }

    func test_givenACardOnAndFirstTimeOff_whenICallChangeCardStatus_thenICallOnOffHelpReaction() {

        let dummyPreferencesManager = PreferenceCardManagerDummy()
        let cardsEntity = CardsGenerator.getCardEntity(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyCardBO = DummyCardBO(cardEntity: cardsEntity.data![0])
        dummyCardBO.isOff = false

        sut.preferences = dummyPreferencesManager

        //given
        dummyPreferencesManager.willSayIsFirstTimeOn = false
        dummyPreferencesManager.willSayIsFirstTimeOff = true

        //when
        sut.changeCardStatus(withCardStatus: true, andCard: dummyCardBO)

        //then
        XCTAssert(dummyBusManager.isCalledEventNavToOnOffHelpReaction == true)
    }

    func test_givenACardThatsBeenSwitchedUp_whenICallChangeCardStatus_thenIDontNavigateToOnOffHelp() {

        let dummyPreferencesManager = PreferenceCardManagerDummy()
        let dummyCardBO = CardsGenerator.getCardBOCreditCard().cards.first!

        sut.preferences = dummyPreferencesManager

        //given
        dummyPreferencesManager.willSayIsFirstTimeOn = false
        dummyPreferencesManager.willSayIsFirstTimeOff = false

        //when
        sut.changeCardStatus(withCardStatus: true, andCard: dummyCardBO)

        //then
        XCTAssert(dummyBusManager.isCalledNavigateToOnOffHelpScreen == false)
    }

    func test_givenACardThatsBeenSwitchedUp_whenICallChangeCardStatus_thenIDontCallOnOffHelpReaction() {

        let dummyPreferencesManager = PreferenceCardManagerDummy()
        let dummyCardBO = CardsGenerator.getCardBOCreditCard().cards.first!

        sut.preferences = dummyPreferencesManager

        //given
        dummyPreferencesManager.willSayIsFirstTimeOn = false
        dummyPreferencesManager.willSayIsFirstTimeOff = false

        //when
        sut.changeCardStatus(withCardStatus: true, andCard: dummyCardBO)

        //then
        XCTAssert(dummyBusManager.isCalledEventNavToOnOffHelpReaction == false)
    }

    func test_givenACardThatsNotBeenSwitchedUp_whenICallChangeCardStatus_thenISetTransportNavigationFlagToTrue() {
        let dummyPreferencesManager = PreferenceCardManagerDummy()
        let dummyCardBO = CardsGenerator.getCardBOCreditCard().cards.first!

        sut.preferences = dummyPreferencesManager

        //given
        dummyPreferencesManager.willSayIsFirstTimeOn = true
        dummyPreferencesManager.willSayIsFirstTimeOff = true

        //when
        sut.changeCardStatus(withCardStatus: true, andCard: dummyCardBO)

        //then
        XCTAssert(dummyBusManager.navigationFlag == true)

    }

    // MARK: - switchCardStatus

    func test_givenAnOperativeCard_whenICallChangeCardStatus_thenDummyActivationBOHasTheSameActivations() {

        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()

        let cardsEntity = CardsGenerator.getCardEntity(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        let dummyCardBO = DummyCardBO(cardEntity: cardsEntity.data![0])

        sut.view = dummyView
        sut.interactor = dummyInteractor
        sut.cardStatus = false
        sut.currentCard = dummyCardBO

        // when
        sut.switchCardStatus()

        // then
        XCTAssert(dummyCardBO.dummyActivationBO === sut.activations?.data)

    }

    func test_givenAnOperativeCard_whenICallChangeCardStatusToTurnOff_thenINotCallSendOmnitureScreenCardOffSuccess() {

        //given
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")

        sut.view = dummyView
        sut.interactor = dummyInteractor
        dummyView.toastMessage = Localizables.cards.key_cards_on_successful_text
        sut.cardStatus = false
        sut.currentCard = cards.cards[0]

        // when
        sut.switchCardStatus()

        // then
        XCTAssert(dummyView.isCalledSendOmnitureScreenCardOffSuccess == false)

    }

    func test_givenAnOperativeCard_whenICallSwitchCardUp_thenICallSendOmnitureScreenCardOffSuccess() {

        //given
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()
        let cards = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "false")

        sut.view = dummyView
        sut.interactor = dummyInteractor
        dummyView.toastMessage = Localizables.cards.key_cards_off_successful_text
        sut.cardStatus = true
        sut.currentCard = cards.cards[0]

        // when
        sut.switchCardStatus()

        // then
        XCTAssert(dummyView.isCalledSendOmnitureScreenCardOffSuccess == true)

    }

    // MARK: - Pressed RightCardButton (digital)

    func test_givenAnInvalidCardIndex_whenICallRightCardButtonPressed_thenErrorMessageShouldBeEqualsToGenericMessage() {

        let cardsBO = CardsGenerator.getThreeCards()
        let dummyInteractor = DummyInteractor()

        sut.cards = cardsBO
        sut.interactor = dummyInteractor
        sut.displayItems = sut.createDisplayDataItems()

        //given
        let invalidCardIndex = sut.cards?.cards.count

        //when
        sut.rightCardButtonPressed(atIndex: invalidCardIndex!)

        //then
        XCTAssert(sut.errorBO?.errorMessage() == Localizables.common.key_common_generic_error_text)
    }

    func test_givenAnInvalidCardIndex_whenICallRightCardButtonPressed_thenIShowWarningTypeError() {

        let cardsBO = CardsGenerator.getThreeCards()
        let dummyInteractor = DummyInteractor()

        sut.cards = cardsBO
        sut.interactor = dummyInteractor
        sut.displayItems = sut.createDisplayDataItems()

        //given
        let invalidCardIndex = sut.cards?.cards.count

        //when
        sut.rightCardButtonPressed(atIndex: invalidCardIndex!)

        //then
        XCTAssert(sut.errorBO?.errorType == .warning)
    }

    func test_givenAnInvalidCardIndex_whenICallRightCardButtonPressed_thenICallShowError() {

        let cardsBO = CardsGenerator.getThreeCards()
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()

        sut.view = dummyView
        sut.cards = cardsBO
        sut.interactor = dummyInteractor
        sut.displayItems = sut.createDisplayDataItems()

        //given
        let invalidCardIndex = sut.cards?.cards.count

        //when
        sut.rightCardButtonPressed(atIndex: invalidCardIndex!)

        //then
        XCTAssert(sut.view?.isCalledShowError == true)
    }

    func test_givenAValidCardIndex_whenICallRightCardButtonPressed_thenINotCallShowError() {

        let cardsBO = CardsGenerator.getThreeCards()
        let dummyView = DummyView()
        let dummyInteractor = DummyInteractor()

        sut.view = dummyView
        sut.cards = cardsBO
        sut.interactor = dummyInteractor

        sut.displayItems = sut.createDisplayDataItems()

        //given
        let cardIndex = 0

        //when
        sut.rightCardButtonPressed(atIndex: cardIndex)

        //then
        XCTAssert(sut.view?.isCalledShowError == false)
    }

    func test_givenAEmptyCardsBO_whenICallRightCardButtonPressed_thenICallShowError() {

        let dummyInteractor = DummyInteractor()
        let cardsEntity = CardsEntity()
        let dummyView = DummyView()
        let cardIndex = 0

        cardsEntity.status = 200
        cardsEntity.data = [CardEntity]()

        sut.view = dummyView
        sut.interactor = dummyInteractor

        //given
        sut.cards = CardsBO(cardsEntity: cardsEntity)
        sut.displayItems = sut.createDisplayDataItems()

        //when
        sut.rightCardButtonPressed(atIndex: cardIndex)

        //then
        XCTAssert(sut.view?.isCalledShowError == true)
    }

    func test_givenCardID_whenICallRightCardButton_thenICallNavigateScreen() {

        //given
        sut.cards = CardsGenerator.getThreeCards()
        sut.displayItems = sut.createDisplayDataItems()

        //when
        sut.rightCardButtonPressed(atIndex: 0)

        //then
        XCTAssert(dummyBusManager.isCalledNavigateScreen == true)
    }

    func test_givenCardID_whenICallRightCardButton_thenICallBusManagerCardCVVScreen() {

        //given
        sut.cards = CardsGenerator.getThreeCards()
        sut.displayItems = sut.createDisplayDataItems()

        //when
        sut.rightCardButtonPressed(atIndex: 0)

        //then
        XCTAssert(dummyBusManager.isCalledNavigateToCVVScreen == true)

    }

    func test_givenAny_whenICallNavigateToCVVScreen_thenICallEventNavToCardCVVScreenReaction() {

        //given
        sut.cards = CardsGenerator.getThreeCards()
        sut.displayItems = sut.createDisplayDataItems()

        //when
        sut.rightCardButtonPressed(atIndex: 0)

        //then
        XCTAssert(dummyBusManager.isCalledEventNavToCardCVVScreenReaction == true)
    }

    func test_givenAny_whenICallNavigateToCVVScreen_thenRouterTagShouldBeEqualsToCardsPresenterRouterTag() {

        //given
        sut.cards = CardsGenerator.getThreeCards()
        sut.displayItems = sut.createDisplayDataItems()

        //when
        sut.rightCardButtonPressed(atIndex: 0)

        //then
        XCTAssert(dummyBusManager.dummyTag == sut.routerTag)

    }

    // MARK: - override prepareSelectedCard

    func test_givenCardOperationInfoWithFisicalCardIDOperationBlockAndTypeDigital_whenICallPrepareSelectedCard_thenSelectedCardShouldBeDigital() {

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .virtual
        }

        cards.cards[2].cardId = "3333"
        cards.cards[0].cardId = "8000"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[2].cardId

        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()

        // given
        let cardOperationInfo = CardOperationInfo(cardID: "", fisicalCardID: cards.cards[0].cardId, cardType: .digital, cardOperation: .block)
        sut.cardOperationInfo = cardOperationInfo

        // when
        sut.prepareSelectedCard(showCard: false)

        // then
        XCTAssert((sut.displayItems.items![0].displayCardData as? DisplayCardDataA)?.selectedCard == .digital)

    }

    func test_givenCardOperationInfoWithFisicalCardIDOperationCancelAndTypeDigital_whenICallPrepareSelectedCard_thenSelectedCardShouldBeDigital() {

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .virtual
        }

        cards.cards[2].cardId = "3333"
        cards.cards[0].cardId = "8000"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[2].cardId

        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()

        // given
        let cardOperationInfo = CardOperationInfo(cardID: "", fisicalCardID: cards.cards[0].cardId, cardType: .digital, cardOperation: .cancel)
        sut.cardOperationInfo = cardOperationInfo

        // when
        sut.prepareSelectedCard(showCard: false)

        // then
        XCTAssert((sut.displayItems.items![0].displayCardData as? DisplayCardDataA)?.selectedCard == .digital)

    }

    func test_givenCardOperationInfoWithFisicalCardIDOperationBlockAndTypeSticker_whenICallPrepareSelectedCard_thenSelectedCardShouldBeSticker() {

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .sticker
        }

        cards.cards[2].cardId = "3333"
        cards.cards[0].cardId = "8000"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[2].cardId

        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()

        // given
        let cardOperationInfo = CardOperationInfo(cardID: "", fisicalCardID: cards.cards[0].cardId, cardType: .sticker, cardOperation: .block)
        sut.cardOperationInfo = cardOperationInfo

        // when
        sut.prepareSelectedCard(showCard: false)

        // then
        XCTAssert((sut.displayItems.items![0].displayCardData as? DisplayCardDataA)?.selectedCard == .sticker)

    }

    func test_givenCardOperationInfoWithFisicalCardIDOperationCancelAndTypeSticker_whenICallPrepareSelectedCard_thenSelectedCardShouldSticker() {

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .sticker
        }

        cards.cards[2].cardId = "3333"
        cards.cards[0].cardId = "8000"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[2].cardId

        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()

        // given
        let cardOperationInfo = CardOperationInfo(cardID: "", fisicalCardID: cards.cards[0].cardId, cardType: .sticker, cardOperation: .cancel)
        sut.cardOperationInfo = cardOperationInfo

        // when
        sut.prepareSelectedCard(showCard: false)

        // then
        XCTAssert((sut.displayItems.items![0].displayCardData as? DisplayCardDataA)?.selectedCard == .sticker)

    }

    func test_givenCardOperationInfoWithFisicalCardIDOperationBlockAndTypeStickerThereIsNoSticker_whenICallPrepareSelectedCard_thenSelectedCardShouldBePhysical() {

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .virtual
        }

        cards.cards[2].cardId = "3333"
        cards.cards[0].cardId = "8000"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[2].cardId

        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()

        // given
        let cardOperationInfo = CardOperationInfo(cardID: "", fisicalCardID: cards.cards[0].cardId, cardType: .sticker, cardOperation: .block)
        sut.cardOperationInfo = cardOperationInfo

        // when
        sut.prepareSelectedCard(showCard: false)

        // then
        XCTAssert((sut.displayItems.items![0].displayCardData as? DisplayCardDataA)?.selectedCard == .physical)

    }

    func test_givenCardOperationInfoWithFisicalCardIDOperationCancelAndTypeStickerAndThereIsNoSticker_whenICallPrepareSelectedCard_thenSelectedCardShouldPhysical() {

        let cards = CardsGenerator.getThreeCards()

        for card in cards.cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .virtual
        }

        cards.cards[2].cardId = "3333"
        cards.cards[0].cardId = "8000"
        cards.cards[0].physicalSupport!.id = .normalPlastic
        cards.cards[0].relatedContracts[0].contractId = cards.cards[2].cardId

        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()

        // given
        let cardOperationInfo = CardOperationInfo(cardID: "", fisicalCardID: cards.cards[0].cardId, cardType: .sticker, cardOperation: .cancel)
        sut.cardOperationInfo = cardOperationInfo

        // when
        sut.prepareSelectedCard(showCard: false)

        // then
        XCTAssert((sut.displayItems.items![0].displayCardData as? DisplayCardDataA)?.selectedCard == .physical)
    }
    
    func test_givenCardOperationInfoAndNeedRefreshPagerViewTrue_whenICallPrepareSelectedCard_thenICalledRefreshPagerView() {
        
        // given
        let dummyView = DummyView()
        sut.view = dummyView
        
        let cards = CardsGenerator.getThreeCards()
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        
        let cardOperationInfo = CardOperationInfo(cardID: "", fisicalCardID: cards.cards[0].cardId, cardType: .digital, cardOperation: .block)
        sut.cardOperationInfo = cardOperationInfo
        sut.needRefreshPagerView = true
        
        // when
        sut.prepareSelectedCard(showCard: false)
        
        // then
        XCTAssert(dummyView.isCalledRefreshPagerView == true)
    }
    
    func test_givenCardOperationInfoAndNeedRefreshPagerViewTrue_whenICallPrepareSelectedCard_thenICalledRefreshPagerViewAndIndexMatch() {
        
        // given
        let dummyView = DummyView()
        sut.view = dummyView
        
        let cards = CardsGenerator.getThreeCardsWithStickerAndDigital()
        sut.cards = cards
        sut.displayItems = sut.createDisplayDataItems()
        
        let cardOperationInfo = CardOperationInfo(cardID: "", fisicalCardID: cards.cards[1].cardId, cardType: .sticker, cardOperation: .block)
        sut.cardOperationInfo = cardOperationInfo
        sut.needRefreshPagerView = true
        
        // when
        sut.prepareSelectedCard(showCard: false)
        
        // then
        XCTAssert(dummyView.sentRefreshPagerViewIndex == 1)
    }

    // MARK: - TrackEvent

    func test_givenAny_whenICallNavigateToCVVScreen_thenICallBusPublishDataEventTrackerNewTrackEvent() {

        //given
        sut.cards = CardsGenerator.getThreeCards()
        sut.displayItems = sut.createDisplayDataItems()

        //when
        sut.rightCardButtonPressed(atIndex: 0)

        // then
        XCTAssert(dummyBusManager.isCalledPublishDataEventTrackerNewTrackEvent == true)

    }

    func test_givenAny_whenICallNavigateToCVVScreen_thenICallBusPublishDataEventTrackerNewTrackEventWithAppropiatedEventModel() {

        //given
        sut.cards = CardsGenerator.getThreeCards()
        sut.displayItems = sut.createDisplayDataItems()

        //when
        sut.rightCardButtonPressed(atIndex: 0)

        // then
        XCTAssert(dummyBusManager.eventSent != nil)
        XCTAssert(dummyBusManager.eventSent?.type == .seeCardCVV)
    }

    // MARK: - DummyView

    class DummyView: CardsViewProtocolA, ViewProtocol {

        var isCalledCopyPan = false
        var isCalledShowTransactionsWithDisplayData = false
        var navigationTitle = ""
        var transactionsDisplayData: TransactionDisplayData?
        var displayDataBalances: DisplayBalancesCard?
        var displayDataOnOff: OnOffDisplayData?
        var displayDataOperativesBarInfo: ButtonsBarDisplayData?
        var displayDataHelp: HelpDisplayData?

        var isCalledShowNavigationTitle = false

        var isCalledHideDataInfo = false
        var isCalledShowCardDataInfo = false
        var isCalledShowBalancesInfo = false
        var isCalledShowOnOffInfo = false
        var isCalledShowOperativeBarInfo = false
        var isCalledShowSuccessToast = false
        var isCalledHelpDisplayData = false
        var isCalledHelpView = false

        var isCalledAnimationDetail = false
        var isCalledAnimationCards = false

        var isCalledShowPoints = false
        var isCalledHidePoints = false
        var isCalledShowCardAtIndex = false
        var isCalledShowError = false

        var sentDiplayItems: DisplayItemsCards?

        var toastMessage = ""
        var panCopied = ""
        var sentIndex: Int?

        var isCalledHideOperativesBar = false

        var isCalledSendOmnitureScreenCardOffSuccess = false
        var isCalledShowView = false
        var isCalledHideView = false
        
        var isCalledRefreshPagerView = false
        var sentRefreshPagerViewIndex: Int?

        func showError(error: ModelBO) {
            isCalledShowError = true
        }

        func changeColorEditTexts( ) {
        }

        func showInfoCards(withDisplayItems displayItems: DisplayItemsCards) {

            sentDiplayItems = displayItems

        }

        func showTransactions(withDisplayData displayData: TransactionDisplayData) {
            transactionsDisplayData = displayData
            isCalledShowTransactionsWithDisplayData = true
        }

        func showToastSuccess(withSuccessMessage message: String, andBackgroundColor color: UIColor) {
            toastMessage = message

            if message == toastMessage && color == .BBVADARKGREEN {
                isCalledShowSuccessToast = true
            }
        }
        func switchInitialStateAfterReceiveError(withSwitchStatus status: Bool) {
        }

        func hideCardDataInfo() {
            isCalledHideDataInfo = true
        }

        func showCardDataInfo() {
            isCalledShowCardDataInfo = true
        }

        func showBalancesInfo(withDisplayData displayData: DisplayBalancesCard) {
            isCalledShowBalancesInfo = true
            displayDataBalances = displayData
        }

        func showOnOffInfo(withDisplayData displayData: OnOffDisplayData) {
            isCalledShowOnOffInfo = true
            displayDataOnOff = displayData
        }

        func showOperativeBarInfo(withDisplayData displayData: ButtonsBarDisplayData) {
            isCalledShowOperativeBarInfo = true
            displayDataOperativesBarInfo = displayData
        }
        func showHelp(withDisplayData displayData: HelpDisplayData) {

            displayDataHelp = displayData
            isCalledHelpDisplayData = true
        }

        func showNavigation(withTitle title: String) {
            isCalledShowNavigationTitle = true
            navigationTitle = title
        }

        func copyPan(withNumber number: String) {
            isCalledCopyPan = true
            panCopied = number
        }

        func doAnimationDetail() {
            isCalledAnimationDetail = true
        }

        func doAnimationCards() {
            isCalledAnimationCards = true
        }

        func showPointsView(withDisplayData displayData: PointsDisplayData) {
            isCalledShowPoints = true
        }

        func hidePointsView() {
            isCalledHidePoints = true
        }

        func hideOperativesBar() {
            isCalledHideOperativesBar = true
        }

        func showCard(atIndex index: Int, hideCardInformationFromVisibleCell: Bool) {
            isCalledShowCardAtIndex = true
            sentIndex = index
        }

        func sendOmnitureScreenCardOffSuccess() {
            isCalledSendOmnitureScreenCardOffSuccess = true
        }

        func showLoader() {
            isCalledShowView = true
        }

        func hideLoader() {
            isCalledHideView = true
        }

        func refreshPagerView(atIndex index: Int) {
            
            isCalledRefreshPagerView = true
            sentRefreshPagerViewIndex = index
        }
    }

    class DummyInteractor: CardsInteractorProtocol, InteractorProtocol {

        var cardsToSent: CardsBO?

        var isCardIdValid = [false, false, false]

        var cardId: String?
        var status: Bool = false
        var serviceResponseDummy = ResponseServiceDummy()
        var isForceError = false
        var errorBO: ErrorBO?

        var cards: CardsBO?
        var isCalledProvideCards = false
        var isCalledUpdateCards = false
        var forceErrorCards = false

        func provideCards(user: UserBO?) -> Single<ModelBO> {
            self.isCalledProvideCards = true

            if forceErrorCards {

                let errorBO = ErrorBO(error: ErrorEntity(message: "Error"))

                return Single.create(subscribe: { single in
                    single(.error(ServiceError.GenericErrorBO(error: errorBO)))
                    return Disposables.create()
                })
            } else {
                self.cards = CardsGenerator.getCardBOWithAlias()

                return Single<ModelBO>.just(self.cards!)
            }
        }

        func provideCards() -> Observable <ModelBO> {

            if cardsToSent == nil {
                cardsToSent = CardsGenerator.getCardBOWithAlias()
            }

            return Observable <ModelBO>.just(cardsToSent!)

        }

        func provideUpdatedCard(withCardId cardId: String, withStatus status: Bool) -> Observable<ModelBO> {
            isCalledUpdateCards = true
            return Observable <ModelBO>.just(CardsGenerator.getAnOperativeCardBOWithChangeStatus())

        }

        func provideLimits(withCardId cardId: String) -> Observable <ModelBO> {

            let limitsEntity = LimitsEntity()
            let limitsBO = LimitsBO(limitsEntity: limitsEntity)

            return Observable <ModelBO>.just(limitsBO)
        }
    }

    class DummyBusManager: BusManager {

        var isCalledNavigateNextCardsScreen = false
        var isCalledNavigateSecondCardsScreen = false
        var isCalledNavigateFourCardsScreen = false
        var isCalledPublishDataActivateCard = false
        var isCalledPublishDataActivateCardSelect = false
        var isCalledNavigateActivateCard = false
        var iscalledNavigateToCVVScreen = false
        var isCalledNavigateScreen = false
        var isCalledEventNavToCardCVVScreenReaction = false
        var isCalledNavigateToCVVScreen = false
        var isCalledShowLoading = false
        var isCalledHideLoading = false
        var isCalledExpiredSession = false
        var isCalledPublishDataGenerateCard = false
        var isCalledEventNavToGenerateDigitalCard = false
        var isCalledEventNavToCancelCard = false
        var isCalledEventNavToBlockCard = false
        var isCalledPublishDataEventTrackerNewTrackEvent = false
        var isCalledNavigateToOnOffHelpScreen = false
        var isCalledEventNavToOnOffHelpReaction = false
        var isCalledNavigateToDetailHelpScreen = false

        var isCalledNavigateThirdCardsScreen = false
        var navigationFlag: Bool?
        var dummyTag: String = ""
        var dummyCardsTransport: CardsTransport!
        var dummyCardBO: CardBO!
        var dummyTransactionsTransport: TransactionsTransport!
        var dummyActivateCardBO: ActivateCardCVVTransport!
        var dummyActivateCardSuccessObjectCells: ActivateCardSuccessObjectCells!
        var dummyCardCVVTransport: CVVTransport!
        var dummyGenerateCardBO: GenerateCardBO!
        var eventSent: EventProtocol?
        var dummyOnOffTransport: OnOffCardTransport!
        var dummyHelpDetailTransport: HelpDetailTransport!

        override init() {
            super.init()
            routerFactory = RouterInitFactory()
        }

        override func expiredSession() {
            isCalledExpiredSession = true
        }

        override func showLoading(completion: (() -> Void)?) {
            completion?()
            isCalledShowLoading = true
        }

        override  func hideLoading(completion: (() -> Void)? ) {
            if let completion = completion {
                completion()
            }
            isCalledHideLoading = true
        }

        override func publishData<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {

            if tag == CardsPageReactionA.ROUTER_TAG && event === CardsPageReactionA.PARAMS_ACTIVATE_WITH_CVV_CARD {
                isCalledPublishDataActivateCard = true
                dummyActivateCardBO = ActivateCardCVVTransport.deserialize(from: values as? [String: Any])
            } else if tag == CardsPageReaction.ROUTER_TAG && event === CardsPageReaction.PARAMS_SELECT_CARD_ACTIVATE {
                isCalledPublishDataActivateCardSelect = true
                dummyActivateCardSuccessObjectCells = ActivateCardSuccessObjectCells.deserialize(from: values as? [String: Any])
            } else if tag == CardsPageReaction.ROUTER_TAG && event === CardsPageReaction.PARAMS_GENERATE_CARD {

                isCalledPublishDataGenerateCard = true
                dummyGenerateCardBO = GenerateCardBO.deserialize(from: values as? [String: Any])
            } else if tag == EventTracker.id && event === PageReactionConstants.NEW_TRACK_EVENT {
                isCalledPublishDataEventTrackerNewTrackEvent = true
                eventSent = values as? EventProtocol
            }

        }

        override func navigateScreen<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {

            isCalledNavigateScreen = true

            if tag == CardsPageReaction.ROUTER_TAG && event === CardsPageReaction.EVENT_NAV_TO_SECOND_SCREEN {
                isCalledNavigateSecondCardsScreen = true
                dummyCardsTransport = (values as! CardsTransport)
            } else if tag == CardsPageReaction.ROUTER_TAG && event === CardsPageReaction.EVENT_NAV_TO_NEXT_SCREEN {
                isCalledNavigateNextCardsScreen = true
                dummyCardBO = (values as! CardBO)
            } else if tag == CardsPageReaction.ROUTER_TAG && event === CardsPageReaction.EVENT_NAV_TO_THIRD_SCREEN {
                isCalledNavigateThirdCardsScreen = true
                dummyTransactionsTransport = (values as! TransactionsTransport)
            } else if tag == CardsPageReaction.ROUTER_TAG && event === CardsPageReaction.EVENT_NAV_TO_FOUR_SCREEN {
                isCalledNavigateFourCardsScreen = true
                dummyCardBO = (values as! CardBO)
            } else if tag == CardsPageReactionA.ROUTER_TAG && event === CardsPageReactionA.EVENT_NAV_TO_ACTIVATE_WITH_CVV_CARD {
                isCalledNavigateActivateCard = true
            } else if tag == CardsPageReaction.ROUTER_TAG && event === CardsPageReaction.EVENT_NAV_TO_CARD_CVV_SCREEN {

                dummyTag = tag
                isCalledNavigateToCVVScreen = true
                isCalledEventNavToCardCVVScreenReaction = event === CardsPageReaction.EVENT_NAV_TO_CARD_CVV_SCREEN
                dummyCardCVVTransport = (values as! CVVTransport)

            } else if tag == CardsPageReaction.ROUTER_TAG && event === CardsPageReaction.EVENT_NAV_TO_GENERATE_CARD {

                dummyTag = tag
                isCalledEventNavToGenerateDigitalCard = true
            } else if tag == CardsPageReaction.ROUTER_TAG && event === CardsPageReaction.EVENT_NAV_TO_CANCEL_CARD {
                dummyTag = tag
                isCalledEventNavToCancelCard = true

            } else if tag == CardsPageReactionA.ROUTER_TAG && event === CardsPageReactionA.EVENT_NAV_TO_ON_OFF_HELP {
                let transport = values as! CardsTransport
                navigationFlag = transport.flagNavigation

                dummyTag = tag
                isCalledNavigateToOnOffHelpScreen = true
                isCalledEventNavToOnOffHelpReaction = event === CardsPageReactionA.EVENT_NAV_TO_ON_OFF_HELP

            } else if tag == CardsPageReaction.ROUTER_TAG && event === CardsPageReaction.EVENT_NAV_TO_BLOCK_CARD {
                dummyTag = tag
                isCalledEventNavToBlockCard = true
            } else if tag == CardsPageReaction.ROUTER_TAG && event === CardsPageReaction.EVENT_NAV_TO_DETAIL_HELP {

                isCalledNavigateToDetailHelpScreen = true
                dummyHelpDetailTransport = (values as! HelpDetailTransport)
            }
        }
    }

    class PreferenceCardManagerDummy: PreferencesManager {

        var willSayIsFirstTimeOn = true
        var willSayIsFirstTimeOff = true

        override func isFirstTimeTuringCardOn() -> Bool {
            return willSayIsFirstTimeOn
        }

        override func isFirstTimeTurningCardOff() -> Bool {
            return willSayIsFirstTimeOff
        }
    }

    class ConfigurationDataManagerDummy: ConfigurationDataManager {

        override init() {
        }

        override func provideMainCurrency() -> Observable<ModelEntity> {

            return Observable <ModelEntity>.just(CurrencyEntity(code: "CLP"))
        }
    }

    class DummyCardBO: CardBO {

        var isCalledUpdateActivations = false
        var dummyActivationBO: ActivationBO?
        var isOff = false

        override func updateActivations(activationBO: ActivationBO) {
            isCalledUpdateActivations = true
            dummyActivationBO = activationBO
        }

        override func isCardOn() -> Bool {
            return !isOff
        }

    }

    class ResponseServiceDummy {

        func requestCvvCard() -> CVVBO {

            let cvvBO: CVVBO

            var cvvEntity: SecurityDataEntity = SecurityDataEntity()

            cvvEntity.id = Settings.CardCvv.cvvId
            cvvEntity.name = "Card Verification Value"
            cvvEntity.code = "123"

            let data: [SecurityDataEntity] = [cvvEntity]

            let cardCvvDummy: CVVEntity = CVVEntity()

            cardCvvDummy.data = data

            cvvBO = CVVBO(cvvEntity: cardCvvDummy)

            cvvBO.cardId = "005"

            return cvvBO
        }

        func requestWithoutCvvCard() -> CVVBO {

            let cvvBO: CVVBO

            var cvvEntity: SecurityDataEntity = SecurityDataEntity()

            cvvEntity.id = "PIN"
            cvvEntity.name = "Personal Identificator Number"
            cvvEntity.code = "123"

            let data: [SecurityDataEntity] = [cvvEntity]

            let cardCvvDummy: CVVEntity = CVVEntity()

            cardCvvDummy.data = data

            cvvBO = CVVBO(cvvEntity: cardCvvDummy)

            cvvBO.cardId = "005"

            return cvvBO
        }

    }

}
