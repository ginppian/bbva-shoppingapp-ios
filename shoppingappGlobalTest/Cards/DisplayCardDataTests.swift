//
//  DisplayCardDataTests.swift
//  shoppingapp
//
//  Created by jesus.martinez on 24/10/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class DisplayCardDataTests: XCTestCase {

    // MARK: - generateOnOffDisplayData

    func test_givenACardBO_whenICallGenerateDisplayCardData_thenDisplayDataPanShouldBeMatchWithCardBONumberValue() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        //When
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.pan == DisplayCardData.formatTextUsingSeparator(text: cardBO.number))

    }

    func test_givenACardBO_whenICallGenerateDisplayCardData_thenDisplayDataPanShouldBeMatchWithCardBOHolderNameValueUppercased() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        //When
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.holderName == cardBO.holderName.uppercased())

    }

    func test_givenACardBOAndCardsWithExpirationDateWithCorrectFormat_whenICallGenerateDisplayCardData_thenDisplayDataHasExpirationDateFilled() {

        //given
        let cardBO = CardsGenerator.getCardBO(withExpirationDate: "2020-12-14").cards[0]

        //When
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.expirationDate != nil)

    }

    func test_givenACardBOAndCardsWithExpirationDateWithoutCorrectFormat_whenICallGenerateDisplayCardData_thenDisplayDataDoesNotHaveExpirationDateFilled() {

        //given
        let cardBO = CardsGenerator.getCardBO(withExpirationDate: "14/12").cards[0]

        //When
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.expirationDate == nil)

    }

    func test_givenACardBOAndCardsWithExpirationDateWithCorrectFormat_whenICallGenerateDisplayCardData_thenDisplayDataHasExpirationDateWithTheCorrectFormat() {

        //given
        let cardBO = CardsGenerator.getCardBO(withExpirationDate: "2020-12-14").cards[0]

        //When
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.expirationDate == "12/20")

    }

    func test_givenACardBO_whenICallGenerateDisplayCardData_thenDisplayDataImageCardIsNotEmpty() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        //When
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        // then
        XCTAssertFalse((displayData.imageCard?.isEmpty)!)

    }

    func test_givenACard_whenICallGenerateDisplayCardData_thenDisplayDataImageCardIsTheSameThatCard() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        //when
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.imageCard == cardBO.imageFront)
        XCTAssert(displayData.backImageCard == cardBO.imageBack)

    }

    func test_givenACard_whenICallGenerateDisplayCardData_thenDisplayDataTextStatusEmpty() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        //when
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        // then
        XCTAssert((displayData.imageStatus?.isEmpty)!)
        XCTAssert(displayData.bottomTitleText == nil)
        XCTAssert(displayData.bottomImageNamed == nil)

    }
    
    func test_givenACardOperativeWithActivationsOnOffFalse_whenICallGenerateDisplayCardData_thenImageStatusIsOnOff() {
        
        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "false").cards[0]
        
        //when
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)
        
        // then
        XCTAssert(displayData.imageStatus == ConstantsImages.Card.card_off)
        
    }

    func test_givenACardBloqued_whenICallGenerateDisplayCardData_thenDisplayDataImageStatusBlocked() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "BLOCKED", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        //when
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.imageStatus == ConstantsImages.Card.card_blocked)
        XCTAssert(displayData.bottomTitleText == nil)
        XCTAssert(displayData.bottomImageNamed == nil)

    }

    func test_givenACardCanceled_whenICallGenerateDisplayCardData_thenDisplayDataImageStatusBlocked() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "CANCELED", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        //when
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.imageStatus == ConstantsImages.Card.card_blocked)
        XCTAssert(displayData.bottomTitleText == nil)
        XCTAssert(displayData.bottomImageNamed == nil)

    }

    func test_givenACardPendingEmbossing_whenICallGenerateDisplayCardData_thenDisplayDataImageStatusDelivery() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "PENDING_EMBOSSING", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        //when
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.imageStatus == ConstantsImages.Card.card_delivery)
        XCTAssert(displayData.bottomTitleText == nil)
        XCTAssert(displayData.bottomImageNamed == nil)

    }

    func test_givenACardPendingDelivery_whenICallGenerateDisplayCardData_thenDisplayDataImageStatusDelivery() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "PENDING_DELIVERY", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        //when
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.imageStatus == ConstantsImages.Card.card_delivery)
        XCTAssert(displayData.bottomTitleText == nil)
        XCTAssert(displayData.bottomImageNamed == nil)

    }

    func test_givenACardUnknowm_whenICallGenerateDisplayCardData_thenDisplayDataImageStatusBlocked() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "UNKNOWM", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        //when
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.imageStatus == ConstantsImages.Card.card_blocked)
        XCTAssert(displayData.bottomTitleText == nil)
        XCTAssert(displayData.bottomImageNamed == nil)

    }

    func test_givenACardBloqued_whenICallGenerateDisplayCardData_thenDisplayDataTextStatusOFF() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "false").cards[0]

        //when
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)
        // then
        XCTAssert(displayData.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(displayData.bottomTitleText == nil)
        XCTAssert(displayData.bottomImageNamed == nil)

    }

    func test_givenACardPendingToActive_whenICallGenerateDisplayCardData_thenDisplayDataStatusPendingToActive() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "INOPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]

        //when
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.imageStatus == ConstantsImages.Card.card_activate)

    }

    func test_givenACardWithEcommerceActivationFalse_whenICallGenerateDisplayCardData_thenDisplayDataTextStatusSemiOff() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ECOMMERCE_ACTIVATION", withIsActive: "false").cards[0]

        //when
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(displayData.bottomTitleText == nil)
        XCTAssert(displayData.bottomImageNamed == nil)

    }

    func test_givenACardWithCashWithDrawalActivationFalse_whenICallGenerateDisplayCardData_thenDisplayDataTextStatusSemiOff() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "CASHWITHDRAWAL_ACTIVATION", withIsActive: "false").cards[0]

        //when
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(displayData.bottomTitleText == nil)
        XCTAssert(displayData.bottomImageNamed == nil)

    }

    func test_givenACardWithPurchaseActivationFalse_whenICallGenerateDisplayCardData_thenDisplayDataTextStatusSemiOff() {

        //given

        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "PURCHASES_ACTIVATION", withIsActive: "false").cards[0]

        //when
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(displayData.bottomTitleText == nil)
        XCTAssert(displayData.bottomImageNamed == nil)

    }

    func test_givenACardWithForeignCashWithDrawalActivationFalse_whenICallGenerateDisplayCardData_thenDisplayDataTextStatusSemiOff() {

        //given

        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "FOREIGN_CASHWITHDRAWAL_ACTIVATION", withIsActive: "false").cards[0]

        //when
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(displayData.bottomTitleText == nil)
        XCTAssert(displayData.bottomImageNamed == nil)

    }

    func test_givenACardWithForeignPurchaseActivationFalse_whenICallGenerateDisplayCardData_thenDisplayDataTextStatusSemiOff() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "FOREIGN_PURCHASES_ACTIVATION", withIsActive: "false").cards[0]

        //when
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(displayData.bottomTitleText == nil)
        XCTAssert(displayData.bottomImageNamed == nil)

    }

    func test_givenAAtivationOnOffFalseAndForeignPurchaseFasle_whenICallGenerateDisplayCardData_thenDisplayDataTextStatusOff() {

        //given
        let cardBO = CardsGenerator.getCardBOMultipleActivation(withStatusId: "OPERATIVE", withActivationId: "FOREIGN_PURCHASES_ACTIVATION", withIsActive: "false", withActivationId: "ON_OFF", withIsActive2: "false").cards[0]

        //when
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(displayData.bottomTitleText == nil)
        XCTAssert(displayData.bottomImageNamed == nil)

    }

    func test_givenAAtivationOnOffFalseAndForeignPurchaseTrue_whenICallGenerateDisplayCardData_thenDisplayDataTextStatusSemiOff() {

        //given
        let cardBO = CardsGenerator.getCardBOMultipleActivation(withStatusId: "OPERATIVE", withActivationId: "FOREIGN_PURCHASES_ACTIVATION", withIsActive: "false", withActivationId: "ON_OFF", withIsActive2: "true").cards[0]

        //when
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(displayData.bottomTitleText == nil)
        XCTAssert(displayData.bottomImageNamed == nil)

    }

    func test_givenAAtivationPurchaseFalseAndForeignPurchasefalse_whenICallGenerateDisplayCardData_thenDisplayDataTextStatusSemiOff() {

        //given
        let cardBO = CardsGenerator.getCardBOMultipleActivation(withStatusId: "OPERATIVE", withActivationId: "FOREIGN_PURCHASES_ACTIVATION", withIsActive: "false", withActivationId: "PURCHASES_ACTIVATION", withIsActive2: "false").cards[0]

        //when
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(displayData.bottomTitleText == nil)
        XCTAssert(displayData.bottomImageNamed == nil)

    }

    func test_givenAAtivationPurchaseTrueAndForeignPurchaseTrue_whenICallGenerateDisplayCardData_thenDisplayDataTextStatusEmpty() {

        //given
        let cardBO = CardsGenerator.getCardBOMultipleActivation(withStatusId: "OPERATIVE", withActivationId: "FOREIGN_PURCHASES_ACTIVATION", withIsActive: "true", withActivationId: "PURCHASES_ACTIVATION", withIsActive2: "true").cards[0]

        //when
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(displayData.bottomTitleText == nil)
        XCTAssert(displayData.bottomImageNamed == nil)

    }

    func test_givenAnActivationPurchaseTrueAndActivationsIsEmpty_whenICallGenerateDisplayCardData_thenDisplayDataTextStatusEmpty() {

        //given

        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "PURCHASES_ACTIVATION", withIsActive: "true").cards[0]
        cardBO.activations = Array()

        //when
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.imageStatus == ConstantsImages.Card.card_off)
        XCTAssert(displayData.bottomTitleText == nil)
        XCTAssert(displayData.bottomImageNamed == nil)

    }

    func test_givenAliasInCard_whenICallGenerateDisplayCardData_thenDisplayTheCardAliasValue() {

        //given
        let cardBO = CardsGenerator.getCardBOWithAlias().cards[0]

        //when
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.titleCard == cardBO.alias)

    }

    func test_givenAliasEmpty_whenICallGenerateDisplayCardData_thenDisplayTheCardTitleNameValue() {

        //given
        let cardBO = CardsGenerator.getCardBOWithAliasEmpty().cards[0]

        //when
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.titleCard == cardBO.title?.name)

    }

    func test_givenAStickerCard_whenICallGenerateDisplayCardData_thenPanCardHolderAndExpirationDateShouldBeNil() {

        //given
        let cardBO = CardsGenerator.getCardBO(withExpirationDate: "2020-12-14").cards[0]
        cardBO.physicalSupport!.id = .sticker

        //when
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.pan == "")
        XCTAssert(displayData.holderName == "")
        XCTAssert(displayData.expirationDate == nil)

    }

    func test_givenANormalPlasticCard_whenICallGenerateDisplayCardData_thenPanCardHolderAndExpirationDateShouldMatchWithCardBO() {

        //given
        let cardBO = CardsGenerator.getCardBO(withExpirationDate: "2020-12-14").cards[0]
        cardBO.physicalSupport!.id = .normalPlastic

        //when
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.pan == DisplayCardData.formatTextUsingSeparator(text: cardBO.number))
        XCTAssert(displayData.holderName == cardBO.holderName.uppercased())
        XCTAssert(displayData.expirationDate == "12/20")

    }

    func test_givenAVirtualCard_whenICallGenerateDisplayCardData_thenPanCardHolderAndExpirationDateShouldMatchWithCardBO() {

        //given
        let cardBO = CardsGenerator.getCardBO(withExpirationDate: "2020-12-14").cards[0]
        cardBO.physicalSupport!.id = .virtual

        //when
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.pan == DisplayCardData.formatTextUsingSeparator(text: cardBO.number))
        XCTAssert(displayData.holderName == cardBO.holderName.uppercased())
        XCTAssert(displayData.expirationDate == "12/20")

    }

    // MARK: - Action Buttons

    func test_givenAnInOperativeCardAndNotSetteableToOperative_whenICallGenerateDisplayCardData_thenLeftButtonDisplayDataIsFilledAndRightNot() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "INOPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        cardBO.indicators[1].indicatorId = .settableToOperative
        cardBO.indicators[1].isActive = false

        //when
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.leftButtonDisplayData != nil)
        XCTAssert(displayData.rightButtonDisplayData == nil)
    }

    func test_givenAnInOperativeCardAndNotSetteableToOperative_whenICallGenerateDisplayCardData_thenLeftButtonDisplayDataIsActivateCard() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "INOPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        cardBO.indicators[1].indicatorId = .settableToOperative
        cardBO.indicators[1].isActive = false

        //when
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.leftButtonDisplayData?.imageNamed == ConstantsImages.Common.help_icon_on_off)
        XCTAssert(displayData.leftButtonDisplayData?.title == Localizables.cards.key_cards_how_activate_card_text)
        XCTAssert(displayData.leftButtonDisplayData?.actionType == .activateCardExplanation)
        XCTAssert(displayData.leftButtonDisplayData?.accesibility == ConstantsAccessibilities.CARD_BOTTOM_ACTION_BUTTON)

    }

    func test_givenAnInOperativeCardAndSetteableToOperative_whenICallGenerateDisplayCardData_thenLeftButtonDisplayDataIsFilledAndRightNot() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "INOPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        cardBO.indicators[1].indicatorId = .settableToOperative
        cardBO.indicators[1].isActive = true

        //when
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.leftButtonDisplayData != nil)
        XCTAssert(displayData.rightButtonDisplayData == nil)
    }

    func test_givenAnInOperativeCardAndSetteableToOperative_whenICallGenerateDisplayCardData_thenLeftButtonDisplayDataIsActivateCardExplanation() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "INOPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        cardBO.indicators[1].indicatorId = .settableToOperative
        cardBO.indicators[1].isActive = true

        //when
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.leftButtonDisplayData?.imageNamed == ConstantsImages.Card.creditcard_icon)
        XCTAssert(displayData.leftButtonDisplayData?.title == Localizables.cards.key_cards_activate_card_text)
        XCTAssert(displayData.leftButtonDisplayData?.actionType == .activateCard)
        XCTAssert(displayData.leftButtonDisplayData?.accesibility == ConstantsAccessibilities.CARD_BOTTOM_ACTION_BUTTON)

    }

    func test_givenAnOperativeCardAndReadableSecurityData_whenICallGenerateDisplayCardData_thenLeftAndRightButtonDisplayShouldBeFilled() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        cardBO.indicators[1].indicatorId = .readableSecurityData
        cardBO.indicators[1].isActive = true

        //when
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.leftButtonDisplayData != nil)
        XCTAssert(displayData.rightButtonDisplayData != nil)
    }

    func test_givenAnOperativeCardAndReadableSecurityData_whenICallGenerateDisplayCardData_thenLeftButtonDisplayDataIsCopyPanAndRightSeeCVV() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        cardBO.indicators[1].indicatorId = .readableSecurityData
        cardBO.indicators[1].isActive = true

        //when
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.leftButtonDisplayData?.imageNamed == nil)
        XCTAssert(displayData.leftButtonDisplayData?.title == Localizables.cards.key_cards_copy_text)
        XCTAssert(displayData.leftButtonDisplayData?.actionType == .copyPan)
        XCTAssert(displayData.leftButtonDisplayData?.accesibility == ConstantsAccessibilities.CARD_COPY_PAN_ACTION_BUTTON)

        XCTAssert(displayData.rightButtonDisplayData?.imageNamed == nil)
        XCTAssert(displayData.rightButtonDisplayData?.title == Localizables.cards.key_card_see_cvv_text)
        XCTAssert(displayData.rightButtonDisplayData?.actionType == .seeCVV)
        XCTAssert(displayData.rightButtonDisplayData?.accesibility == ConstantsAccessibilities.CARD_BOTTOM_ACTION_BUTTON_VER_CVV)

    }

    func test_givenAnOperativeCardANotReadableSecurityDataAndNotSetteableToOperative_whenICallGenerateDisplayCardData_thenLeftButtonDisplayDataIsFilledAndRightNot() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        cardBO.indicators[1].indicatorId = .readableSecurityData
        cardBO.indicators[1].isActive = false
        cardBO.indicators[2].indicatorId = .settableToOperative
        cardBO.indicators[2].isActive = false

        //when
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.leftButtonDisplayData != nil)
        XCTAssert(displayData.rightButtonDisplayData == nil)
    }

    func test_givenAnOperativeCardANotReadableSecurityDataAndNotSetteableToOperative_whenICallGenerateDisplayCardData_thenLeftButtonDisplayDataIsCopyPan() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        cardBO.indicators[1].indicatorId = .readableSecurityData
        cardBO.indicators[1].isActive = false
        cardBO.indicators[2].indicatorId = .settableToOperative
        cardBO.indicators[2].isActive = false

        //when
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.leftButtonDisplayData?.imageNamed == nil)
        XCTAssert(displayData.leftButtonDisplayData?.title == Localizables.cards.key_cards_copy_text)
        XCTAssert(displayData.leftButtonDisplayData?.actionType == .copyPan)
        XCTAssert(displayData.leftButtonDisplayData?.accesibility == ConstantsAccessibilities.CARD_COPY_PAN_ACTION_BUTTON)

    }

    func test_givenAnOperativeCardANotReadableSecurityDataAndSetteableToOperative_whenICallGenerateDisplayCardData_thenLeftButtonDisplayDataIsFilledAndRightNot() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        cardBO.indicators[1].indicatorId = .readableSecurityData
        cardBO.indicators[1].isActive = false
        cardBO.indicators[2].indicatorId = .settableToOperative
        cardBO.indicators[2].isActive = true

        //when
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.leftButtonDisplayData != nil)
        XCTAssert(displayData.rightButtonDisplayData == nil)
    }

    func test_givenAnOperativeCardANotReadableSecurityDataAndSetteableToOperative_whenICallGenerateDisplayCardData_thenLeftButtonDisplayDataIsCopyPan() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        cardBO.indicators[1].indicatorId = .readableSecurityData
        cardBO.indicators[1].isActive = false
        cardBO.indicators[2].indicatorId = .settableToOperative
        cardBO.indicators[2].isActive = true

        //when
        let displayData = DisplayCardData.generateDisplayCardData(fromCardBO: cardBO)

        // then
        XCTAssert(displayData.leftButtonDisplayData?.imageNamed == nil)
        XCTAssert(displayData.leftButtonDisplayData?.title == Localizables.cards.key_cards_copy_text)
        XCTAssert(displayData.leftButtonDisplayData?.actionType == .copyPan)
        XCTAssert(displayData.leftButtonDisplayData?.accesibility == ConstantsAccessibilities.CARD_COPY_PAN_ACTION_BUTTON)

    }

}
