//
//  DisplayBalancesCardTests.swift
//  shoppingapp
//
//  Created by jesus.martinez on 25/10/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import RxSwift
import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class DisplayBalancesCardTests: XCTestCase {

    var currencyBO: CurrencyBO!

    override func setUp() {
        super.setUp()
        currencyBO = Currencies.currency(forCode: "CLP")
    }

    func test_givenACardWithCurrenciesWithIsMajorCLPAndAvailableBalancesAndFormatter_whenICallGenerateDisplayBalanceCard_thenDisplayBalancesCardHaveAAmountHaveValue() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        let formatter = AmountFormatter(codeCurrency: currencyBO.code)

        //when
        let displayDataBalance = DisplayBalancesCard.generateDisplayBalanceCard(fromCardBO: cardBO, andCurrencyBO: currencyBO, andFormatter: formatter)

        // then
        XCTAssert(displayDataBalance.amount != nil)

    }
    
    func test_givenADebitCardWithCurrenciesWithIsMajorErrorAndAvailableBalancesWithErrorCurrencyAndFormatter_whenICallGenerateDisplayBalanceCard_thenDisplayBalancesCardHaveAAmountWithOutValue() {
        
        //given
        let cardBO = CardsGenerator.getCardBOMultipleActivation(withStatusId: "OPERATIVE", withActivationId: "FOREIGN_PURCHASES_ACTIVATION", withIsActive: "false", withActivationId: "ON_OFF", withIsActive2: "false").cards[0]
        cardBO.cardType.id = .credit_card
        let formatter = AmountFormatter(codeCurrency: currencyBO.code)
        
        //when
        let displayDataBalance = DisplayBalancesCard.generateDisplayBalanceCard(fromCardBO: cardBO, andCurrencyBO: currencyBO, andFormatter: formatter)
        
        // then
        XCTAssert(displayDataBalance.amount == nil)
        
    }

    func test_givenACardCardWithCurrenciesWithIsMajorErrorAndAvailableBalancesWithErrorCurrencyAndFormatter_whenICallGenerateDisplayBalanceCard_thenDisplayBalancesCardHaveAAmountWithOutValue() {

        //given
        let cardBO = CardsGenerator.getCardBOMultipleActivation(withStatusId: "OPERATIVE", withActivationId: "FOREIGN_PURCHASES_ACTIVATION", withIsActive: "false", withActivationId: "ON_OFF", withIsActive2: "false").cards[0]
        cardBO.cardType.id = .credit_card
        let formatter = AmountFormatter(codeCurrency: currencyBO.code)

        //when
        let displayDataBalance = DisplayBalancesCard.generateDisplayBalanceCard(fromCardBO: cardBO, andCurrencyBO: currencyBO, andFormatter: formatter)

        // then
        XCTAssert(displayDataBalance.amount == nil)

    }

    func test_givenACardBOWithCurrenciesAndIsMajorAndFormatter__whenICallGenerateDisplayBalanceCard_thenTheFormatterWithValueCodeCurrencyIsTheSame() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        let formatter = AmountFormatter(codeCurrency: cardBO.currencies[1].currency)

        //when
        let displayDataBalance = DisplayBalancesCard.generateDisplayBalanceCard(fromCardBO: cardBO, andCurrencyBO: currencyBO, andFormatter: formatter)

        // then
        XCTAssert(formatter.codeCurrency == displayDataBalance.cardBO?.currencies[1].currency)

    }

    func test_givenACreditCardBOWithOutAvailableCurrenciesAndFormatter_WhenICallDisplayData_thenDisplayDataCardAmountIsNil() {

        //given
        let cardBO = CardsGenerator.getCardBOWWithOutAvailableCurrencies().cards[0]
        cardBO.cardType.id = .credit_card
        let formatter = AmountFormatter(codeCurrency: currencyBO.code)

        //when
        let displayDataBalance = DisplayBalancesCard.generateDisplayBalanceCard(fromCardBO: cardBO, andCurrencyBO: currencyBO, andFormatter: formatter)

        // then
        XCTAssert(displayDataBalance.amount == nil)

    }
    
    func test_givenADebitCardBOWithOutGrantedCreditsAndFormatter_WhenICallDisplayData_thenDisplayDataCardAmountIsNil() {
        
        //given
        let cardBO = CardsGenerator.getCardBOWWithOutAvailableCurrencies().cards[0]
        cardBO.grantedCredits = [AmountCurrencyBO]()
        cardBO.cardType.id = .debit_card
        let formatter = AmountFormatter(codeCurrency: currencyBO.code)
        
        //when
        let displayDataBalance = DisplayBalancesCard.generateDisplayBalanceCard(fromCardBO: cardBO, andCurrencyBO: currencyBO, andFormatter: formatter)
        
        // then
        XCTAssert(displayDataBalance.amount == nil)
        
    }

    func test_givenACardBOWithCurrencyMajorAndAvailableBalancesAndFormatter_whenICallGenerateDisplayBalanceCard_thenDisplayDataCardAmountIsNotNil() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        let formatter = AmountFormatter(codeCurrency: cardBO.currencyMajor!)

        //when
        let displayDataBalance = DisplayBalancesCard.generateDisplayBalanceCard(fromCardBO: cardBO, andCurrencyBO: currencyBO, andFormatter: formatter)

        // then
        XCTAssert(displayDataBalance.amount != nil)

    }

    func test_givenACardBOWithCurrencyMajorAndAvailableBalancesAndFormatter_whenICallGenerateDisplayBalanceCard_thenICallAttributtedText() {

        //given
        let cardBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true").cards[0]
        cardBO.cardType.id = .credit_card

        let formatter = AmountFormatter(codeCurrency: cardBO.currencyMajor!)

        //when
        let displayDataBalance = DisplayBalancesCard.generateDisplayBalanceCard(fromCardBO: cardBO, andCurrencyBO: currencyBO, andFormatter: formatter)

        // then
        XCTAssert(displayDataBalance.amount == formatter.attributtedText(forAmount: cardBO.availableBalance.currentBalances[1].amount, isNegative: true, bigFontSize: 36, smallFontSize: 22))

    }

    func test_givenACardBOWithOutCurrencyMajorAndAvailableBalancesAndFormatter_whenICallGenerateDisplayBalanceCard_thenICallAttributtedText() {

        //given
        let cardBO = CardsGenerator.getCardBOWithOUTCurrencies().cards[0]
        let formatter = AmountFormatter(codeCurrency: currencyBO.code)

        //when
        let displayDataBalance = DisplayBalancesCard.generateDisplayBalanceCard(fromCardBO: cardBO, andCurrencyBO: currencyBO, andFormatter: formatter)

        // then
        XCTAssert(displayDataBalance.amount == formatter.attributtedText(forAmount: cardBO.availableBalance.currentBalances[0].amount, isNegative: true, bigFontSize: 36, smallFontSize: 22))

    }

    func test_givenACardBOWithCardTypeCreditCardAndFormatter_whenICallGenerateDisplayBalanceCard_thenIHaveValue() {

        //given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "CREDIT_CARD").cards[0]
        let formatter = AmountFormatter(codeCurrency: currencyBO.code)

        //when
        let displayDataBalance = DisplayBalancesCard.generateDisplayBalanceCard(fromCardBO: cardBO, andCurrencyBO: currencyBO, andFormatter: formatter)

        // then
        XCTAssert(displayDataBalance.balanceText != nil)

    }

    func test_givenACardBOWithCardTypeCreditCardAndFormatter_whenICallGenerateDisplayBalanceCard_thenIHaveTheCorrectValue() {

        //given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "CREDIT_CARD").cards[0]
        let formatter = AmountFormatter(codeCurrency: currencyBO.code)

        //when
        let displayDataBalance = DisplayBalancesCard.generateDisplayBalanceCard(fromCardBO: cardBO, andCurrencyBO: currencyBO, andFormatter: formatter)

        // then
        XCTAssert(displayDataBalance.balanceText == Localizables.cards.key_cards_availablecredit_text)

    }

    func test_givenACardBOWithCardTypeDebitCardAndFormatter_whenICallGenerateDisplayBalanceCard_thenIHaveValue() {

        //given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        let formatter = AmountFormatter(codeCurrency: currencyBO.code)

        let displayDataBalance = DisplayBalancesCard.generateDisplayBalanceCard(fromCardBO: cardBO, andCurrencyBO: currencyBO, andFormatter: formatter)

        // then
        XCTAssert(displayDataBalance.balanceText != nil)

    }

    func test_givenACardBOWithCardTypeDebitCardAndFormatter_whenICallGenerateDisplayBalanceCard_thenIHaveTheCorrectValue() {

        //given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        let formatter = AmountFormatter(codeCurrency: currencyBO.code)

        let displayDataBalance = DisplayBalancesCard.generateDisplayBalanceCard(fromCardBO: cardBO, andCurrencyBO: currencyBO, andFormatter: formatter)

        // then
        XCTAssert(displayDataBalance.balanceText == Localizables.cards.key_cards_availablebalance_text)

    }

    func test_givenACardBOWithCardTypePrepaidCardWithRelatedContractAccountAndFormatter_whenICallGenerateDisplayBalanceCard_thenIHaveValue() {

        //given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "PREPAID_CARD").cards[0]
        let formatter = AmountFormatter(codeCurrency: currencyBO.code)

        let displayDataBalance = DisplayBalancesCard.generateDisplayBalanceCard(fromCardBO: cardBO, andCurrencyBO: currencyBO, andFormatter: formatter)

        // then
        XCTAssert(displayDataBalance.balanceText != nil)
        XCTAssert(displayDataBalance.balance2Text != nil)
        XCTAssert(displayDataBalance.balance3Text != nil)
        XCTAssert(displayDataBalance.icon1Image != nil)
        XCTAssert(displayDataBalance.icon2Image != nil)

    }

    func test_givenACardBOWithCardTypePrepaidCardWithRelatedContractAccountAndFormatter__whenICallGenerateDisplayBalanceCard_thenIHaveTheCorrectValue() {

        //given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "PREPAID_CARD").cards[0]
        let formatter = AmountFormatter(codeCurrency: currencyBO.code)

        let number: String = (cardBO.relatedContracts[0].number)!
        let last4 = number.suffix(4)

        // when
        let displayDataBalance = DisplayBalancesCard.generateDisplayBalanceCard(fromCardBO: cardBO, andCurrencyBO: currencyBO, andFormatter: formatter)

        // then
        XCTAssert(displayDataBalance.balanceText == Localizables.cards.key_cards_linkedbalance_text)
        XCTAssert(displayDataBalance.balance2Text == Localizables.cards.key_cards_account_text)
        XCTAssert(displayDataBalance.balance3Text == String(last4))
        XCTAssert(displayDataBalance.icon1Image == ConstantsImages.Card.link_card_icon)
        XCTAssert(displayDataBalance.icon2Image == ConstantsImages.Common.bullet)
    }

    func test_givenACardBOWithCardTypePrepaidCardWithRelatedContractCardAndFormatter__whenICallGenerateDisplayBalanceCard_thenIHaveTheCorrectValue() {

        //given
        let cardBO = CardsGenerator.getCardBOWithCardTypeRelatedCard(withCardType: "PREPAID_CARD").cards[0]
        let formatter = AmountFormatter(codeCurrency: currencyBO.code)

        let number: String = (cardBO.relatedContracts[0].number)!

        let last4 = number.suffix(4)

        //when
        let displayDataBalance = DisplayBalancesCard.generateDisplayBalanceCard(fromCardBO: cardBO, andCurrencyBO: currencyBO, andFormatter: formatter)

        // then
        XCTAssert(displayDataBalance.balanceText == Localizables.cards.key_cards_linkedbalance_text)
        XCTAssert(displayDataBalance.balance2Text == Localizables.cards.key_cards_card_text)
        XCTAssert(displayDataBalance.balance3Text == String(last4))
        XCTAssert(displayDataBalance.icon1Image == ConstantsImages.Card.link_card_icon)
        XCTAssert(displayDataBalance.icon2Image == ConstantsImages.Common.bullet)
    }

    func test_givenACardBOWithCardTypeDebitCardWithOutRelatedContractAndFormatter__whenICallGenerateDisplayBalanceCard_thenIHaveTheCorrectValue() {

        //given
        let cardBO = CardsGenerator.getCardBOWithCardTypeWithOutRelatedCard(withCardType: "PREPAID_CARD").cards[0]
        let formatter = AmountFormatter(codeCurrency: currencyBO.code)

        //when
        let displayDataBalance = DisplayBalancesCard.generateDisplayBalanceCard(fromCardBO: cardBO, andCurrencyBO: currencyBO, andFormatter: formatter)

        // then
        XCTAssert(displayDataBalance.balanceText == Localizables.cards.key_cards_prepaidbalance_text)

    }

    func test_givenACardBOWithCreditCardTypeAndFormatter__whenICallGenerateDisplayBalanceCard_thenIHaveTheCorrectLocalizableValues() {

        //given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "CREDIT_CARD").cards[0]
        let formatter = AmountFormatter(codeCurrency: currencyBO.code)

        //when
        let displayDataBalance = DisplayBalancesCard.generateDisplayBalanceCard(fromCardBO: cardBO, andCurrencyBO: currencyBO, andFormatter: formatter)

        // then
        XCTAssert(displayDataBalance.balanceLimitTitle == Localizables.cards.key_cards_limit_text)
        XCTAssert(displayDataBalance.balanceConsumedTitle == Localizables.cards.key_cards_consumed_text)

    }

    func test_givenACardBOWithCreditCardTypeWithCurrencyAndFormatter__whenICallGenerateDisplayBalanceCard_thenICallAttributtedTextForGrantedCredits() {

        //given
        let cardBO = CardsGenerator.getCardBOWithCardTypeWithCurrency(withCardType: "CREDIT_CARD").cards[0]
        let formatter = AmountFormatter(codeCurrency: cardBO.currencies[1].currency)

        //when
        let displayDataBalance = DisplayBalancesCard.generateDisplayBalanceCard(fromCardBO: cardBO, andCurrencyBO: currencyBO, andFormatter: formatter)

        // then
        XCTAssert((displayDataBalance.balanceLimitValue?.isEqual(to: formatter.attributtedText(forAmount: cardBO.grantedCredits[1].amount, isNegative: false, bigFontSize: 16, smallFontSize: 10)))!)

    }
    func test_givenACardBOWithCreditCardTypeWithCurrencyAndFormatter__whenICallGenerateDisplayBalanceCard_thenICallAttributtedTextForDisposedCredits() {

        //given
        let cardBO = CardsGenerator.getCardBOWithCardTypeWithCurrency(withCardType: "CREDIT_CARD").cards[0]
        let formatter = AmountFormatter(codeCurrency: cardBO.currencies[1].currency)

        //when
        let displayDataBalance = DisplayBalancesCard.generateDisplayBalanceCard(fromCardBO: cardBO, andCurrencyBO: currencyBO, andFormatter: formatter)

        // then
        XCTAssert(displayDataBalance.balanceConsumedValue == formatter.attributtedText(forAmount: cardBO.disposedBalance.currentBalances[1].amount, isNegative: true, bigFontSize: 16, smallFontSize: 10))
    }

    func test_givenACardBOWithoutCurrencyMajorAndDisposedBalancesAndFormatter__whenICallGenerateDisplayBalanceCard_thenICallAttributtedTextForDisposedCredits() {

        //given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "CREDIT_CARD").cards[0]
        let formatter = AmountFormatter(codeCurrency: currencyBO.code)

        //when
        let displayDataBalance = DisplayBalancesCard.generateDisplayBalanceCard(fromCardBO: cardBO, andCurrencyBO: currencyBO, andFormatter: formatter)

        // then
        XCTAssert(displayDataBalance.balanceConsumedValue == formatter.attributtedText(forAmount: cardBO.disposedBalance.currentBalances[0].amount, isNegative: false, bigFontSize: 16, smallFontSize: 10))

    }

    func test_givenACardBOWithoutCurrencyMajorAndGrantedBalanceAndFormatter__whenICallGenerateDisplayBalanceCard_thenICallAttributtedTextForGrantedCredit() {

        //given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "CREDIT_CARD").cards[0]
        let formatter = AmountFormatter(codeCurrency: currencyBO.code)

        //when
        let displayDataBalance = DisplayBalancesCard.generateDisplayBalanceCard(fromCardBO: cardBO, andCurrencyBO: currencyBO, andFormatter: formatter)

        // then
        XCTAssert(displayDataBalance.balanceLimitValue == formatter.attributtedText(forAmount: cardBO.grantedCredits[0].amount, isNegative: false, bigFontSize: 16, smallFontSize: 10))

    }

    func test_givenACreditCardTypeWithCurrencyAndFormatter__whenDisposedBalanceSmallerThanZero_thenIGetProgressValues() {

        //given
        let cardBO = CardsGenerator.getCardBOWithCardTypeWithCurrency(withCardType: "CREDIT_CARD").cards[0]
        let formatter = AmountFormatter(codeCurrency: currencyBO.code)

        //when
        let displayDataBalance = DisplayBalancesCard.generateDisplayBalanceCard(fromCardBO: cardBO, andCurrencyBO: currencyBO, andFormatter: formatter)

        //then
        XCTAssert (displayDataBalance.balanceDisposed == 0.0 && displayDataBalance.balanceGranted == 1.0)

    }

    func test_givenACreditCardTypeWithoutCurrencyAndFormatter__whenDisposedBalanceSmallerThanZero_thenIGetProgressValues() {

        //given
        let cardBO = CardsGenerator.getCardBOWithOUTCurrencies().cards[0]
        let formatter = AmountFormatter(codeCurrency: currencyBO.code)

        //when
        let displayDataBalance = DisplayBalancesCard.generateDisplayBalanceCard(fromCardBO: cardBO, andCurrencyBO: currencyBO, andFormatter: formatter)

        //then
        XCTAssert (displayDataBalance.balanceDisposed == 0.0 && displayDataBalance.balanceGranted == 1.0)

    }

    func test_givenACreditCardTypeWithCurrencyAndFormatter__whenDisposedBalanceCurrentBalancesDoesNotExis_thenIGetProgressValues() {

        //given
        let cardBO = CardsGenerator.getCardBOWIthCreditCardWithoutCurrencyAndWithoutDisposed().cards[0]
        let formatter = AmountFormatter(codeCurrency: cardBO.currencies[1].currency)

        //when
        let displayDataBalance = DisplayBalancesCard.generateDisplayBalanceCard(fromCardBO: cardBO, andCurrencyBO: currencyBO, andFormatter: formatter)

        //then
        XCTAssert (displayDataBalance.balanceDisposed == 0.0 && displayDataBalance.balanceGranted == 1.0)

    }

    func test_givenACreditCardTypeWithoutCurrencyAndFormatter__whenDisposedBalanceCurrentBalancesDoesNotExis_thenIGetProgressValues() {

        //given
        let cardBO = CardsGenerator.getCardBOCreditCard().cards[0]
        let formatter = AmountFormatter(codeCurrency: currencyBO.code)

        //when
        let displayDataBalance = DisplayBalancesCard.generateDisplayBalanceCard(fromCardBO: cardBO, andCurrencyBO: currencyBO, andFormatter: formatter)

        //then
        XCTAssert (displayDataBalance.balanceDisposed == 0.0 && displayDataBalance.balanceGranted == 1.0)

    }
    func test_givenACreditCardTypeWithoutCurrencyAndFormatter_whenDisposedBalanceCurrentBalancesNotFoundWithCountryCurrency_thenIGetProgressValues() {

        //given
        let cardBO = CardsGenerator.getCreditCardBOWithoutCurrencyAndDoesNotExistDisposedCurrentBalanceCountryCurrency().cards[0]
        let formatter = AmountFormatter(codeCurrency: currencyBO.code)

        //when
        let displayDataBalance = DisplayBalancesCard.generateDisplayBalanceCard(fromCardBO: cardBO, andCurrencyBO: currencyBO, andFormatter: formatter)

        //then
        XCTAssert (displayDataBalance.balanceDisposed == 0.0 && displayDataBalance.balanceGranted == 1.0)

    }

    func test_givenACreditCardTypeWithCurrencyAndFormatter_whenDisposedBalanceCurrentBalancesNotFoundWithCountryCurrency_thenIGetProgressValues() {

        //given
        let cardBO = CardsGenerator.getCreditCardBOWithCurrencyAndDoesNotExistDisposedCurrentBalanceCountryCurrency().cards[0]
        let formatter = AmountFormatter(codeCurrency: cardBO.currencies[1].currency)

        //when
        let displayDataBalance = DisplayBalancesCard.generateDisplayBalanceCard(fromCardBO: cardBO, andCurrencyBO: currencyBO, andFormatter: formatter)

        //then
        XCTAssert (displayDataBalance.balanceDisposed == 0.0 && displayDataBalance.balanceGranted == 1.0)

    }
    func test_givenACreditCardTypeWithCurrencyAndFormatter__whenGrantedCreditBalanceSmallerThanDisposedBalance_thenIGetProgressValues() {

        //given
        let cardBO = CardsGenerator.getCardWithCardTypeWithCurrencyGrantedSmallerThanDisposed().cards[0]
        let formatter = AmountFormatter(codeCurrency: currencyBO.code)

        //when
        let displayDataBalance = DisplayBalancesCard.generateDisplayBalanceCard(fromCardBO: cardBO, andCurrencyBO: currencyBO, andFormatter: formatter)

        //then
        XCTAssert (displayDataBalance.balanceDisposed == 1.0 && displayDataBalance.balanceGranted == 1.0)

    }

    func test_givenACreditCardTypeWithoutCurrencyAndFormatter__whenGrantedCreditBalanceSmallerThanDisposedBalance_thenIGetProgressValues() {

        //given
        let cardBO = CardsGenerator.getCardBOWithCardTypeWithOutRelatedCard(withCardType: "CREDIT_CARD").cards[0]
        let formatter = AmountFormatter(codeCurrency: currencyBO.code)

        //when
        let displayDataBalance = DisplayBalancesCard.generateDisplayBalanceCard(fromCardBO: cardBO, andCurrencyBO: currencyBO, andFormatter: formatter)

        //then
        XCTAssert (displayDataBalance.balanceDisposed == 1.0 && displayDataBalance.balanceGranted == 1.0)

    }

    func test_givenACreditCardTypeWithCurrencyAndFormatter__whenDisposedBalanceEqualToGrantedCredits_thenIGetProgressValues() {

        //given
        let cardBO = CardsGenerator.getCardWithCardTypeWithCurrencyGrantedSmallerThanDisposed().cards[0]
        let formatter = AmountFormatter(codeCurrency: currencyBO.code)

        cardBO.grantedCredits[1].amount = 550

        //when
        let displayDataBalance = DisplayBalancesCard.generateDisplayBalanceCard(fromCardBO: cardBO, andCurrencyBO: currencyBO, andFormatter: formatter)

        // then
        XCTAssert (displayDataBalance.balanceDisposed == 1.0 && displayDataBalance.balanceGranted == 1.0)

    }

    func test_givenACreditCardTypeWithoutCurrencyAndFormatter__whenDisposedBalanceEqualToGrantedCredits_thenIGetProgressValues() {

        //given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "CREDIT_CARD").cards[0]
        let formatter = AmountFormatter(codeCurrency: currencyBO.code)

        //when
        let displayDataBalance = DisplayBalancesCard.generateDisplayBalanceCard(fromCardBO: cardBO, andCurrencyBO: currencyBO, andFormatter: formatter)

        // then
        XCTAssert (displayDataBalance.balanceDisposed == 1.0 && displayDataBalance.balanceGranted == 1.0)

    }

    func test_givenACreditCardTypeWithCurrencyAndFormatter__whenDisposedBalanceSmallerThanGrantCreditBalance_thenIGetProgressValues() {

        //given
        let cardBO = CardsGenerator.getCardWithCardTypeWithCurrencyGrantedSmallerThanDisposed().cards[0]
        let formatter = AmountFormatter(codeCurrency: currencyBO.code)

        cardBO.grantedCredits[1].amount = 560

        //when
        let displayDataBalance = DisplayBalancesCard.generateDisplayBalanceCard(fromCardBO: cardBO, andCurrencyBO: currencyBO, andFormatter: formatter)

        // then
        XCTAssert (displayDataBalance.balanceGranted == Float(truncating: cardBO.grantedCredits[1].amount))
        XCTAssert (displayDataBalance.balanceDisposed == Float(truncating: cardBO.disposedBalance.currentBalances[1].amount))

    }

    func test_givenACreditCardTypeWithoutCurrencyAndFormatter__whenDisposedBalanceSmallerThanGrantCreditBalance_thenIGetProgressValues() {

        //given
        let cardBO = CardsGenerator.getCardBOWithCardTypeWithOutRelatedCard(withCardType: "CREDIT_CARD").cards[0]
        let formatter = AmountFormatter(codeCurrency: currencyBO.code)

        cardBO.grantedCredits[0].amount = 2200000

        //when
        let displayDataBalance = DisplayBalancesCard.generateDisplayBalanceCard(fromCardBO: cardBO, andCurrencyBO: currencyBO, andFormatter: formatter)

        // then
        XCTAssert (displayDataBalance.balanceGranted == Float(truncating: cardBO.grantedCredits[0].amount))
        XCTAssert (displayDataBalance.balanceDisposed == Float(truncating: cardBO.disposedBalance.currentBalances[0].amount))

    }

}
