//
//  CardBOTests.swift
//  shoppingapp
//
//  Created by jesus.martinez on 13/10/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class CardBOTest: XCTestCase {

    // MARK: - Init contractEntity
    
    func test_givenAFinancialOverviewWithSubProductTypeOtherThanDebit_whenICallInitContractEntity_thenDontSetAccountID() {
        //given
        var financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverview().data?.contracts?.first(where: { contractEntity -> Bool in
            contractEntity.productType == FinancialOverviewContractEntity.cardProductType
        })
        financialOverviewEntity?.subProductType?.id = "CREDIT_CARD"
        
        //when
        let sut = CardBO(contractEntity: financialOverviewEntity!, user: UserBO(user: UserEntity()))
        //then
        XCTAssert(sut?.accountID == nil)
    }

    func test_givenFinancialOverviewContractEntityOfProductTypeAccount_whenICallInitContractEntity_thenReturnNil() {

        // given
        var financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverview().data?.contracts?.first!
        financialOverviewEntity?.productType = FinancialOverviewContractEntity.accountProductType

        // when
        let sut = CardBO(contractEntity: financialOverviewEntity!, user: UserBO(user: UserEntity()))

        // then
        XCTAssert(sut == nil)
    }

    func test_givenFinancialOverviewContractEntityOfProductTypeLoan_whenICallInitContractEntity_thenReturnNil() {

        // given
        var financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverview().data?.contracts?.first!
        financialOverviewEntity?.productType = FinancialOverviewContractEntity.loanProductType

        // when
        let sut = CardBO(contractEntity: financialOverviewEntity!, user: UserBO(user: UserEntity()))

        // then
        XCTAssert(sut == nil)
    }

    func test_givenFinancialOverviewContractEntityOfProductTypeDeposit_whenICallInitContractEntity_thenReturnNil() {

        // given
        var financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverview().data?.contracts?.first!
        financialOverviewEntity?.productType = FinancialOverviewContractEntity.depositProductType

        // when
        let sut = CardBO(contractEntity: financialOverviewEntity!, user: UserBO(user: UserEntity()))

        // then
        XCTAssert(sut == nil)
    }

    func test_givenFinancialOverviewContractEntityOfProductTypeInvestment_whenICallInitContractEntity_thenReturnNil() {

        // given
        var financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverview().data?.contracts?.first!
        financialOverviewEntity?.productType = FinancialOverviewContractEntity.investmentFundProductType

        // when
        let sut = CardBO(contractEntity: financialOverviewEntity!, user: UserBO(user: UserEntity()))

        // then
        XCTAssert(sut == nil)
    }

    func test_givenFinancialOverviewContractEntityOfProductTypeCard_whenICallInitContractEntity_thenReturnNotNil() {

        // given
        let financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverview().data?.contracts?.first(where: { contractEntity -> Bool in
            contractEntity.productType == FinancialOverviewContractEntity.cardProductType
        })

        // when
        let sut = CardBO(contractEntity: financialOverviewEntity!, user: UserBO(user: UserEntity()))

        // then
        XCTAssert(sut != nil)
    }

    func test_givenFinancialOverviewContractEntityOfProductTypeCardWithIdNil_whenICallInitContractEntity_thenReturnNil() {

        // given
        var financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverview().data?.contracts?.first(where: { contractEntity -> Bool in
            contractEntity.productType == FinancialOverviewContractEntity.cardProductType
        })
        financialOverviewEntity?.id = nil

        // when
        let sut = CardBO(contractEntity: financialOverviewEntity!, user: UserBO(user: UserEntity()))

        // then
        XCTAssert(sut == nil)
    }

    func test_givenFinancialOverviewContractEntityOfProductTypeCardWithIdNotNil_whenICallInitContractEntity_thenCardIdShouldMatchWithContractId() {

        // given
        var financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverview().data?.contracts?.first(where: { contractEntity -> Bool in
            contractEntity.productType == FinancialOverviewContractEntity.cardProductType
        })
        financialOverviewEntity?.id = "0"

        // when
        let sut = CardBO(contractEntity: financialOverviewEntity!, user: UserBO(user: UserEntity()))

        // then
        XCTAssert(sut?.cardId == financialOverviewEntity?.id)
    }

    func test_givenFinancialOverviewContractEntityOfProductTypeCardWithAlias_whenICallInitContractEntity_thenAliasShouldMatchWithContractAlias() {

        // given
        var financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverview().data?.contracts?.first(where: { contractEntity -> Bool in
            contractEntity.productType == FinancialOverviewContractEntity.cardProductType
        })
        financialOverviewEntity?.alias = "alias"

        // when
        let sut = CardBO(contractEntity: financialOverviewEntity!, user: UserBO(user: UserEntity()))

        // then
        XCTAssert(sut?.alias == financialOverviewEntity?.alias)
    }

    func test_givenFinancialOverviewContractEntityOfProductTypeCardWithAliasNil_whenICallInitContractEntity_thenAliasShouldMatchWithContractAlias() {

        // given
        var financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverview().data?.contracts?.first(where: { contractEntity -> Bool in
            contractEntity.productType == FinancialOverviewContractEntity.cardProductType
        })
        financialOverviewEntity?.alias = nil

        // when
        let sut = CardBO(contractEntity: financialOverviewEntity!, user: UserBO(user: UserEntity()))

        // then
        XCTAssert(sut?.alias == financialOverviewEntity?.alias)
    }

    func test_givenFinancialOverviewContractEntityOfProductTypeCardWithProductNil_whenICallInitContractEntity_thenTitleShouldBeNil() {

        // given
        var financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverview().data?.contracts?.first(where: { contractEntity -> Bool in
            contractEntity.productType == FinancialOverviewContractEntity.cardProductType
        })
        financialOverviewEntity?.product = nil

        // when
        let sut = CardBO(contractEntity: financialOverviewEntity!, user: UserBO(user: UserEntity()))

        // then
        XCTAssert(sut?.title == nil)
    }

    func test_givenFinancialOverviewContractEntityOfProductTypeCardWithProductNotNil_whenICallInitContractEntity_thenTitleIdAndNameShouldMatchWithProductIdAndName() {

        // given
        let financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverview().data?.contracts?.first(where: { contractEntity -> Bool in
            contractEntity.productType == FinancialOverviewContractEntity.cardProductType
        })

        // when
        let sut = CardBO(contractEntity: financialOverviewEntity!, user: UserBO(user: UserEntity()))

        // then
        XCTAssert(sut?.title?.id == financialOverviewEntity?.product?.id)
        XCTAssert(sut?.title?.name == financialOverviewEntity?.product?.name)
    }

    func test_givenFinancialOverviewContractEntityOfProductTypeCardWithNumberNil_whenICallInitContractEntity_thenReturnNil() {

        // given
        var financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverview().data?.contracts?.first(where: { contractEntity -> Bool in
            contractEntity.productType == FinancialOverviewContractEntity.cardProductType
        })
        financialOverviewEntity?.number = nil

        // when
        let sut = CardBO(contractEntity: financialOverviewEntity!, user: UserBO(user: UserEntity()))

        // then
        XCTAssert(sut == nil)
    }

    func test_givenFinancialOverviewContractEntityOfProductTypeCardWithNumberNotNil_whenICallInitContractEntity_thenNumberShouldMatchWithContractNumber() {

        // given
        var financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverview().data?.contracts?.first(where: { contractEntity -> Bool in
            contractEntity.productType == FinancialOverviewContractEntity.cardProductType
        })
        financialOverviewEntity?.number = "0"

        // when
        let sut = CardBO(contractEntity: financialOverviewEntity!, user: UserBO(user: UserEntity()))

        // then
        XCTAssert(sut?.number == financialOverviewEntity?.number)
    }

    func test_givenFinancialOverviewContractEntityOfProductTypeCardWithExpirationDateNil_whenICallInitContractEntity_thenExpirationDateNil() {

        // given
        var financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverview().data?.contracts?.first(where: { contractEntity -> Bool in
            contractEntity.productType == FinancialOverviewContractEntity.cardProductType
        })
        financialOverviewEntity?.detail?.expirationDate = nil

        // when
        let sut = CardBO(contractEntity: financialOverviewEntity!, user: UserBO(user: UserEntity()))

        // then
        XCTAssert(sut?.expirationDate == nil)
    }

    func test_givenFinancialOverviewContractEntityOfProductTypeCardWithExpirationDateNotCorrectFormat_whenICallInitContractEntity_thenExpirationDateNil() {

        // given
        var financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverview().data?.contracts?.first(where: { contractEntity -> Bool in
            contractEntity.productType == FinancialOverviewContractEntity.cardProductType
        })
        financialOverviewEntity?.detail?.expirationDate = "12-11-2010"

        // when
        let sut = CardBO(contractEntity: financialOverviewEntity!, user: UserBO(user: UserEntity()))

        // then
        XCTAssert(sut?.expirationDate == nil)
    }

    func test_givenFinancialOverviewContractEntityOfProductTypeCardWithExpirationDateCorrectFormat_whenICallInitContractEntity_thenExpirationDateShouldBeFilledWithAppropiatedDate() {

        // given
        var financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverview().data?.contracts?.first(where: { contractEntity -> Bool in
            contractEntity.productType == FinancialOverviewContractEntity.cardProductType
        })
        financialOverviewEntity?.detail?.expirationDate = "2025-06-01"

        // when
        let sut = CardBO(contractEntity: financialOverviewEntity!, user: UserBO(user: UserEntity()))

        // then
        XCTAssert(sut?.expirationDate != nil)
        XCTAssert(sut?.expirationDate == Date.date(fromServerString: financialOverviewEntity!.detail!.expirationDate!, withFormat: Date.DATE_FORMAT_SERVER))
    }

    func test_givenFinancialOverviewContractEntityOfProductTypeCardWithContractStatusNil_whenICallInitContractEntity_thenStatusIdShouldBeUnknowm() {

        // given
        var financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverview().data?.contracts?.first(where: { contractEntity -> Bool in
            contractEntity.productType == FinancialOverviewContractEntity.cardProductType
        })
        financialOverviewEntity?.status = nil

        // when
        let sut = CardBO(contractEntity: financialOverviewEntity!, user: UserBO(user: UserEntity()))

        // then
        XCTAssert(sut?.status.id == .unknown)
    }

    func test_givenFinancialOverviewContractEntityOfProductTypeCardWithContractStatusNotNilOperative_whenICallInitContractEntity_thenStatusIdAndNameShouldMatchWithExptected() {

        // given
        var financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverview().data?.contracts?.first(where: { contractEntity -> Bool in
            contractEntity.productType == FinancialOverviewContractEntity.cardProductType
        })
        financialOverviewEntity?.status?.id = "OPERATIVE"

        // when
        let sut = CardBO(contractEntity: financialOverviewEntity!, user: UserBO(user: UserEntity()))

        // then
        XCTAssert(sut?.status.id == .operative)
        XCTAssert(sut?.status.name == financialOverviewEntity?.status?.name)
    }

    func test_givenFinancialOverviewContractEntityOfProductTypeCardWithContractStatusNotNilInperative_whenICallInitContractEntity_thenStatusIdAndNameShouldMatchWithExptected() {

        // given
        var financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverview().data?.contracts?.first(where: { contractEntity -> Bool in
            contractEntity.productType == FinancialOverviewContractEntity.cardProductType
        })
        financialOverviewEntity?.status?.id = "INOPERATIVE"

        // when
        let sut = CardBO(contractEntity: financialOverviewEntity!, user: UserBO(user: UserEntity()))

        // then
        XCTAssert(sut?.status.id == .inoperative)
        XCTAssert(sut?.status.name == financialOverviewEntity?.status?.name)
    }

    func test_givenFinancialOverviewContractEntityOfProductTypeCardWithContractStatusNotNilBlocked_whenICallInitContractEntity_thenStatusIdAndNameShouldMatchWithExptected() {

        // given
        var financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverview().data?.contracts?.first(where: { contractEntity -> Bool in
            contractEntity.productType == FinancialOverviewContractEntity.cardProductType
        })
        financialOverviewEntity?.status?.id = "BLOCKED"

        // when
        let sut = CardBO(contractEntity: financialOverviewEntity!, user: UserBO(user: UserEntity()))

        // then
        XCTAssert(sut?.status.id == .blocked)
        XCTAssert(sut?.status.name == financialOverviewEntity?.status?.name)
    }

    func test_givenFinancialOverviewContractEntityOfProductTypeCardWithContractStatusNotNilCanceled_whenICallInitContractEntity_thenStatusIdAndNameShouldMatchWithExptected() {

        // given
        var financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverview().data?.contracts?.first(where: { contractEntity -> Bool in
            contractEntity.productType == FinancialOverviewContractEntity.cardProductType
        })
        financialOverviewEntity?.status?.id = "CANCELED"

        // when
        let sut = CardBO(contractEntity: financialOverviewEntity!, user: UserBO(user: UserEntity()))

        // then
        XCTAssert(sut?.status.id == .canceled)
        XCTAssert(sut?.status.name == financialOverviewEntity?.status?.name)
    }

    func test_givenFinancialOverviewContractEntityOfProductTypeCardWithContractStatusNotNilPendingEmbossing_whenICallInitContractEntity_thenStatusIdAndNameShouldMatchWithExptected() {

        // given
        var financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverview().data?.contracts?.first(where: { contractEntity -> Bool in
            contractEntity.productType == FinancialOverviewContractEntity.cardProductType
        })
        financialOverviewEntity?.status?.id = "PENDING_EMBOSSING"

        // when
        let sut = CardBO(contractEntity: financialOverviewEntity!, user: UserBO(user: UserEntity()))

        // then
        XCTAssert(sut?.status.id == .pendingEmbossing)
        XCTAssert(sut?.status.name == financialOverviewEntity?.status?.name)
    }

    func test_givenFinancialOverviewContractEntityOfProductTypeCardWithContractStatusNotNilPendingDelivery_whenICallInitContractEntity_thenStatusIdAndNameShouldMatchWithExptected() {

        // given
        var financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverview().data?.contracts?.first(where: { contractEntity -> Bool in
            contractEntity.productType == FinancialOverviewContractEntity.cardProductType
        })
        financialOverviewEntity?.status?.id = "PENDING_DELIVERY"

        // when
        let sut = CardBO(contractEntity: financialOverviewEntity!, user: UserBO(user: UserEntity()))

        // then
        XCTAssert(sut?.status.id == .pendingDelivery)
        XCTAssert(sut?.status.name == financialOverviewEntity?.status?.name)
    }

    func test_givenFinancialOverviewContractEntityOfProductTypeCardWithIndicatorsNil_whenICallInitContractEntity_thenIndicatorsShouldNotBeEmpty() {

        // given
        var financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverview().data?.contracts?.first(where: { contractEntity -> Bool in
            contractEntity.productType == FinancialOverviewContractEntity.cardProductType
        })
        financialOverviewEntity?.detail?.indicators = nil

        // when
        let sut = CardBO(contractEntity: financialOverviewEntity!, user: UserBO(user: UserEntity()))

        // then
        XCTAssert(sut?.indicators.isEmpty == false)
    }

    func test_givenFinancialOverviewContractEntityOfProductTypeCardWithIndicatorsAndUserAccessControl_whenICallInitContractEntity_thenIndicatorsShouldMatch() {

        // given
        var financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverview().data?.contracts?.first(where: { contractEntity -> Bool in
            contractEntity.productType == FinancialOverviewContractEntity.cardProductType
        })
        
        financialOverviewEntity?.detail?.indicators? = [
        IDNameActiveEntity(id: "ASSOCIABLE_TO_NFC", name: "The card can be related to NFC.", isActive: true),
        IDNameActiveEntity(id: "ASSOCIABLE_TO_STICKER_CARDS", name: "The card can be related to sticker cards.", isActive: true),
        IDNameActiveEntity(id: "ASSOCIABLE_TO_VIRTUAL_CARDS", name: "The card can be related to virtual cards.", isActive: false),
        IDNameActiveEntity(id: "BLOCKABLE", name: "The card can be blocked.", isActive: false),
        IDNameActiveEntity(id: "SECURITY_DATA_READ", name: "The security data can be read of the card.", isActive: true),
        IDNameActiveEntity(id: "READABLE_SECURITY_DATA", name: "The security data can be read of the card.", isActive: true),
        IDNameActiveEntity(id: "SETTABLE_TO_OPERATIVE", name: "The security data can be read of the card.", isActive: true),
        IDNameActiveEntity(id: "CANCELABLE", name: "The security data can be read of the card.", isActive: true)
        ]
        financialOverviewEntity?.userAccessControl = []
        let userAccessControlEntity = UserAccessControlEntity(canOperate: true)
        financialOverviewEntity?.userAccessControl?.append(userAccessControlEntity)

        // when
        let sut = CardBO(contractEntity: financialOverviewEntity!, user: UserBO(user: UserEntity()))

        // then        
        XCTAssert(sut?.indicators[0].indicatorId == .associableToNFC)
        XCTAssert(sut?.indicators[0].name == financialOverviewEntity?.detail?.indicators?[0].name)
        XCTAssert(sut?.indicators[0].isActive == financialOverviewEntity?.detail?.indicators?[0].isActive!)

        XCTAssert(sut?.indicators[1].indicatorId == .associableToStickerCards)
        XCTAssert(sut?.indicators[1].name == financialOverviewEntity?.detail?.indicators?[1].name)
        XCTAssert(sut?.indicators[1].isActive == financialOverviewEntity?.detail?.indicators?[1].isActive!)

        XCTAssert(sut?.indicators[2].indicatorId == .associableToVirtualCards)
        XCTAssert(sut?.indicators[2].name == financialOverviewEntity?.detail?.indicators?[2].name)
        XCTAssert(sut?.indicators[2].isActive == financialOverviewEntity?.detail?.indicators?[2].isActive!)

        XCTAssert(sut?.indicators[3].indicatorId == .blockable)
        XCTAssert(sut?.indicators[3].name == financialOverviewEntity?.detail?.indicators?[3].name)
        XCTAssert(sut?.indicators[3].isActive == financialOverviewEntity?.detail?.indicators?[3].isActive!)

        XCTAssert(sut?.indicators[4].indicatorId == .readableSecurityData)
        XCTAssert(sut?.indicators[4].name == financialOverviewEntity?.detail?.indicators?[4].name)
        XCTAssert(sut?.indicators[4].isActive == financialOverviewEntity?.detail?.indicators?[4].isActive!)

        XCTAssert(sut?.indicators[5].indicatorId == .readableSecurityData)
        XCTAssert(sut?.indicators[5].name == financialOverviewEntity?.detail?.indicators?[5].name)
        XCTAssert(sut?.indicators[5].isActive == financialOverviewEntity?.detail?.indicators?[5].isActive!)

        XCTAssert(sut?.indicators[6].indicatorId == .settableToOperative)
        XCTAssert(sut?.indicators[6].name == nil)
        XCTAssert(sut?.indicators[6].isActive == financialOverviewEntity?.userAccessControl?.first?.canOperate)

    }

    func test_givenFinancialOverviewContractEntityOfProductTypeCardWithActivationsNil_whenICallInitContractEntity_thenActivationsShouldBeEmpty() {

        // given
        var financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverview().data?.contracts?.first(where: { contractEntity -> Bool in
            contractEntity.productType == FinancialOverviewContractEntity.cardProductType
        })
        financialOverviewEntity?.detail?.activations = nil

        // when
        let sut = CardBO(contractEntity: financialOverviewEntity!, user: UserBO(user: UserEntity()))

        // then
        XCTAssert(sut?.activations.isEmpty == true)
    }

    func test_givenFinancialOverviewContractEntityOfProductTypeCardWithActivationsNot_whenICallInitContractEntity_thenIndicatorsShouldMatch() {

        // given
        var financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverview().data?.contracts?.first(where: { contractEntity -> Bool in
            contractEntity.productType == FinancialOverviewContractEntity.cardProductType
        })
        financialOverviewEntity?.detail?.activations? = [
            IDNameActiveEntity(id: "ON_OFF", name: "If disabled, none of the other activations has effect; if enabled, the card can be used for perfoming any enabled activation.", isActive: true)
        ]

        // when
        let sut = CardBO(contractEntity: financialOverviewEntity!, user: UserBO(user: UserEntity()))

        // then
        XCTAssert(sut?.activations.count == financialOverviewEntity?.detail?.activations?.count)

        XCTAssert(sut?.activations[0].activationId == .onoff)
        XCTAssert(sut?.activations[0].name == financialOverviewEntity?.detail?.activations?[0].name)
        XCTAssert(sut?.activations[0].isActive == financialOverviewEntity?.detail?.activations?[0].isActive!)
    }

    func test_givenFinancialOverviewContractEntityOfProductTypeCardWithSubProductTypeCredit_whenICallInitContractEntity_thenIndicatorsShouldMatch() {

        // given
        var financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverview().data?.contracts?.first(where: { contractEntity -> Bool in
            contractEntity.productType == FinancialOverviewContractEntity.cardProductType
        })
        financialOverviewEntity?.subProductType?.id = "CREDIT_CARD"

        // when
        let sut = CardBO(contractEntity: financialOverviewEntity!, user: UserBO(user: UserEntity()))

        // then
        XCTAssert(sut?.cardType.id == .credit_card)
        XCTAssert(sut?.cardType.name == financialOverviewEntity?.subProductType?.name)
    }

    func test_givenFinancialOverviewContractEntityOfProductTypeCardWithSubProductTypeDebit_whenICallInitContractEntity_thenIndicatorsShouldMatch() {

        // given
        var financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverview().data?.contracts?.first(where: { contractEntity -> Bool in
            contractEntity.productType == FinancialOverviewContractEntity.cardProductType
        })
        financialOverviewEntity?.subProductType?.id = "DEBIT_CARD"

        // when
        let sut = CardBO(contractEntity: financialOverviewEntity!, user: UserBO(user: UserEntity()))

        // then
        XCTAssert(sut?.cardType.id == .debit_card)
        XCTAssert(sut?.cardType.name == financialOverviewEntity?.subProductType?.name)
    }

    func test_givenFinancialOverviewContractEntityOfProductTypeCardWithSubProductTypePrepaid_whenICallInitContractEntity_thenIndicatorsShouldMatch() {

        // given
        var financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverview().data?.contracts?.first(where: { contractEntity -> Bool in
            contractEntity.productType == FinancialOverviewContractEntity.cardProductType
        })
        financialOverviewEntity?.subProductType?.id = "PREPAID_CARD"

        // when
        let sut = CardBO(contractEntity: financialOverviewEntity!, user: UserBO(user: UserEntity()))

        // then
        XCTAssert(sut?.cardType.id == .prepaid_card)
        XCTAssert(sut?.cardType.name == financialOverviewEntity?.subProductType?.name)
    }

    func test_givenFinancialOverviewContractEntityOfProductTypeCardWithCurrencies_whenICallInitContractEntity_thenCurrenciesShouldMatch() {

        // given
        var financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverview().data?.contracts?.first(where: { contractEntity -> Bool in
            contractEntity.productType == FinancialOverviewContractEntity.cardProductType
        })
        financialOverviewEntity?.currencies = [
            CurrenciesEntity(currency: "USD", isMajor: false),
            CurrenciesEntity(currency: "MXN", isMajor: true)
        ]

        // when
        let sut = CardBO(contractEntity: financialOverviewEntity!, user: UserBO(user: UserEntity()))

        // then
        XCTAssert(sut?.currencies.count == financialOverviewEntity?.currencies?.count)

        for i in 0..<(sut?.currencies.count ?? 0) {
            XCTAssert(sut?.currencies[i].currency == financialOverviewEntity?.currencies?[i].currency)
            XCTAssert(sut?.currencies[i].isMajor == financialOverviewEntity?.currencies?[i].isMajor)
        }

        XCTAssert(sut?.currencyMajor == sut?.currencies[1].currency)
    }

    func test_givenFinancialOverviewContractEntityOfProductTypeCardWithImages_whenICallInitContractEntity_thenImagesShouldMatch() {

        // given
        var financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverview().data?.contracts?.first(where: { contractEntity -> Bool in
            contractEntity.productType == FinancialOverviewContractEntity.cardProductType
        })
        financialOverviewEntity?.detail?.images = [
            ImagesEntity(id: "FRONT_CARD", name: "Frontal", url: "url"),
            ImagesEntity(id: "BACK_CARD", name: "Trasera", url: "url trasera")
        ]

        // when
        let sut = CardBO(contractEntity: financialOverviewEntity!, user: UserBO(user: UserEntity()))

        // then
        XCTAssert(sut?.images.count == financialOverviewEntity?.detail?.images?.count)

        XCTAssert(sut?.images[0].id == financialOverviewEntity?.detail?.images?[0].id)
        XCTAssert(sut?.images[0].name == financialOverviewEntity?.detail?.images?[0].name)
        XCTAssert(sut?.images[0].url == financialOverviewEntity?.detail?.images?[0].url)

        XCTAssert(sut?.images[1].id == financialOverviewEntity?.detail?.images?[1].id)
        XCTAssert(sut?.images[1].name == financialOverviewEntity?.detail?.images?[1].name)
        XCTAssert(sut?.images[1].url == financialOverviewEntity?.detail?.images?[1].url)

        XCTAssert(sut?.imageFront == financialOverviewEntity?.detail?.images?[0].url)
        XCTAssert(sut?.imageBack == financialOverviewEntity?.detail?.images?[1].url)

    }

    func test_givenFinancialOverviewContractEntityOfProductTypeCardWithRewardsNil_whenICallInitContractEntity_thenRewardsShouldBeEmpty() {

        // given
        var financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverview().data?.contracts?.first(where: { contractEntity -> Bool in
            contractEntity.productType == FinancialOverviewContractEntity.cardProductType
        })
        financialOverviewEntity?.detail?.rewards = nil

        // when
        let sut = CardBO(contractEntity: financialOverviewEntity!, user: UserBO(user: UserEntity()))

        // then
        XCTAssert(sut?.rewards.isEmpty == true)
    }

    func test_givenFinancialOverviewContractEntityOfProductTypeCardWithRewards_whenICallInitContractEntity_thenRewardsShouldMatch() {

        // given
        var financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverview().data?.contracts?.first(where: { contractEntity -> Bool in
            contractEntity.productType == FinancialOverviewContractEntity.cardProductType
        })
        financialOverviewEntity?.detail?.rewards = [
            FinancialOverviewRewardEntity(id: RewardBO.bancomer_points, name: "bancomer_points", nonMonetaryValue: 400),
            FinancialOverviewRewardEntity(id: "MILLAS", name: "Millas", nonMonetaryValue: 400)
        ]

        // when
        let sut = CardBO(contractEntity: financialOverviewEntity!, user: UserBO(user: UserEntity()))

        // then
        XCTAssert(sut?.rewards.count == financialOverviewEntity?.detail?.rewards?.count)

        XCTAssert(sut?.rewards[0].rewardId == financialOverviewEntity?.detail?.rewards?[0].id)
        XCTAssert(sut?.rewards[0].name == financialOverviewEntity?.detail?.rewards?[0].name)
        XCTAssert(sut?.rewards[0].nonMonetaryValue == financialOverviewEntity?.detail?.rewards?[0].nonMonetaryValue)

        XCTAssert(sut?.rewards[1].rewardId == financialOverviewEntity?.detail?.rewards?[1].id)
        XCTAssert(sut?.rewards[1].name == financialOverviewEntity?.detail?.rewards?[1].name)
        XCTAssert(sut?.rewards[1].nonMonetaryValue == financialOverviewEntity?.detail?.rewards?[1].nonMonetaryValue)
    }

    func test_givenFinancialOverviewContractEntityOfProductTypeCardWithPhysicalSupportNil_whenICallInitContractEntity_thenPhysicalSupportShouldBeNil() {

        // given
        var financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverview().data?.contracts?.first(where: { contractEntity -> Bool in
            contractEntity.productType == FinancialOverviewContractEntity.cardProductType
        })
        financialOverviewEntity?.detail?.physicalSupport = nil

        // when
        let sut = CardBO(contractEntity: financialOverviewEntity!, user: UserBO(user: UserEntity()))

        // then
        XCTAssert(sut?.physicalSupport == nil)
    }

    func test_givenFinancialOverviewContractEntityOfProductTypeCardWithPhysicalSupportWithTypeNormalPlastic_whenICallInitContractEntity_thenPhysicalSupportShouldBeMatch() {

        // given
        var financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverview().data?.contracts?.first(where: { contractEntity -> Bool in
            contractEntity.productType == FinancialOverviewContractEntity.cardProductType
        })
        financialOverviewEntity?.detail?.physicalSupport = ID_Type(id: "NORMAL_PLASTIC", name: "Plastico")

        // when
        let sut = CardBO(contractEntity: financialOverviewEntity!, user: UserBO(user: UserEntity()))

        // then
        XCTAssert(sut?.physicalSupport?.id == .normalPlastic)
        XCTAssert(sut?.physicalSupport?.name == financialOverviewEntity?.detail?.physicalSupport?.name)
    }

    func test_givenFinancialOverviewContractEntityOfProductTypeCardWithPhysicalSupportWithTypeSticker_whenICallInitContractEntity_thenPhysicalSupportShouldBeMatch() {

        // given
        var financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverview().data?.contracts?.first(where: { contractEntity -> Bool in
            contractEntity.productType == FinancialOverviewContractEntity.cardProductType
        })
        financialOverviewEntity?.detail?.physicalSupport = ID_Type(id: "STICKER", name: "Sticker")

        // when
        let sut = CardBO(contractEntity: financialOverviewEntity!, user: UserBO(user: UserEntity()))

        // then
        XCTAssert(sut?.physicalSupport?.id == .sticker)
        XCTAssert(sut?.physicalSupport?.name == financialOverviewEntity?.detail?.physicalSupport?.name)
    }

    func test_givenFinancialOverviewContractEntityOfProductTypeCardWithPhysicalSupportWithTypeVirtual_whenICallInitContractEntity_thenPhysicalSupportShouldBeMatch() {

        // given
        var financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverview().data?.contracts?.first(where: { contractEntity -> Bool in
            contractEntity.productType == FinancialOverviewContractEntity.cardProductType
        })
        financialOverviewEntity?.detail?.physicalSupport = ID_Type(id: "VIRTUAL", name: "Virtual")

        // when
        let sut = CardBO(contractEntity: financialOverviewEntity!, user: UserBO(user: UserEntity()))

        // then
        XCTAssert(sut?.physicalSupport?.id == .virtual)
        XCTAssert(sut?.physicalSupport?.name == financialOverviewEntity?.detail?.physicalSupport?.name)
    }

    func test_givenFinancialOverviewContractEntityOfProductTypeCardWithPhysicalSupportWithTypeNFC_whenICallInitContractEntity_thenPhysicalSupportShouldBeMatch() {

        // given
        var financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverview().data?.contracts?.first(where: { contractEntity -> Bool in
            contractEntity.productType == FinancialOverviewContractEntity.cardProductType
        })
        financialOverviewEntity?.detail?.physicalSupport = ID_Type(id: "NFC", name: "NFC")

        // when
        let sut = CardBO(contractEntity: financialOverviewEntity!, user: UserBO(user: UserEntity()))

        // then
        XCTAssert(sut?.physicalSupport?.id == .nfc)
        XCTAssert(sut?.physicalSupport?.name == financialOverviewEntity?.detail?.physicalSupport?.name)
    }

    func test_givenFinancialOverviewContractEntityOfProductTypeCardWithPhysicalSupportWithTypeNotValid_whenICallInitContractEntity_thenPhysicalSupportShouldBeMatch() {

        // given
        var financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverview().data?.contracts?.first(where: { contractEntity -> Bool in
            contractEntity.productType == FinancialOverviewContractEntity.cardProductType
        })
        financialOverviewEntity?.detail?.physicalSupport = ID_Type(id: "Not_valid", name: "Not_valid")

        // when
        let sut = CardBO(contractEntity: financialOverviewEntity!, user: UserBO(user: UserEntity()))

        // then
        XCTAssert(sut?.physicalSupport?.id == .unknown)
        XCTAssert(sut?.physicalSupport?.name == financialOverviewEntity?.detail?.physicalSupport?.name)
    }

    func test_givenFinancialOverviewContractEntityOfProductTypeCardWithSpecificAmountsNil_whenICallInitContractEntity_thenGrantedCreditsAvailableBalanceAndDisposedBalanceShouldBeEmpty() {

        // given
        var financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverview().data?.contracts?.first(where: { contractEntity -> Bool in
            contractEntity.productType == FinancialOverviewContractEntity.cardProductType
        })
        financialOverviewEntity?.detail?.specificAmounts = nil

        // when
        let sut = CardBO(contractEntity: financialOverviewEntity!, user: UserBO(user: UserEntity()))

        // then
        XCTAssert(sut?.grantedCredits.isEmpty ?? false)

        XCTAssert(sut?.availableBalance.currentBalances.isEmpty ?? false)
        XCTAssert(sut?.availableBalance.postedBalances.isEmpty ?? false)
        XCTAssert(sut?.availableBalance.pendingBalances.isEmpty ?? false)

        XCTAssert(sut?.disposedBalance.currentBalances.isEmpty ?? false)
        XCTAssert(sut?.disposedBalance.postedBalances.isEmpty ?? false)
        XCTAssert(sut?.disposedBalance.pendingBalances.isEmpty ?? false)
    }
    
    func test_givenFinancialOverviewContractEntityOfProductTypeCardWithSpecificAmounts_whenICallInitContractEntity_thenGrantedCreditsAvailableBalanceAndDisposedBalanceShouldMatch() {

        // given
        var financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverview().data?.contracts?.first(where: { contractEntity -> Bool in
            contractEntity.productType == FinancialOverviewContractEntity.cardProductType
        })
        financialOverviewEntity?.subProductType?.id = "CREDIT_CARD"
        let available = SpecificAmountsEntity(id: SpecificAmountsEntity.availableBalance, amounts: [AmountCurrencyEntity(amount: 2000, currency: "MXN")])
        let pending = SpecificAmountsEntity(id: SpecificAmountsEntity.pendingChargeBalance, amounts: [AmountCurrencyEntity(amount: 3000.70, currency: "EUR")])
        let current = SpecificAmountsEntity(id: SpecificAmountsEntity.currentBalance, amounts: [AmountCurrencyEntity(amount: 4000.50, currency: "MXN")])

        financialOverviewEntity?.detail?.specificAmounts = [
            available,
            pending,
            current
        ]

        // when
        let sut = CardBO(contractEntity: financialOverviewEntity!, user: UserBO(user: UserEntity()))

        // then
        XCTAssert(sut?.grantedCredits.count == current.amounts?.count)
        XCTAssert(sut?.grantedCredits[0].amount == NSDecimalNumber(decimal: NSNumber(value: current.amounts![0].amount).decimalValue))
        XCTAssert(sut?.grantedCredits[0].currency == current.amounts?[0].currency)

        XCTAssert(sut?.availableBalance.currentBalances.count == available.amounts?.count)
        XCTAssert(sut?.availableBalance.currentBalances[0].amount == NSDecimalNumber(decimal: NSNumber(value: available.amounts![0].amount).decimalValue))
        XCTAssert(sut?.availableBalance.currentBalances[0].currency == available.amounts?[0].currency)
        XCTAssert(sut?.availableBalance.postedBalances.count == available.amounts?.count)
        XCTAssert(sut?.availableBalance.postedBalances[0].amount == NSDecimalNumber(decimal: NSNumber(value: available.amounts![0].amount).decimalValue))
        XCTAssert(sut?.availableBalance.postedBalances[0].currency == available.amounts?[0].currency)
        XCTAssert(sut?.availableBalance.pendingBalances.count == available.amounts?.count)
        XCTAssert(sut?.availableBalance.pendingBalances[0].amount == NSDecimalNumber(decimal: NSNumber(value: available.amounts![0].amount).decimalValue))
        XCTAssert(sut?.availableBalance.pendingBalances[0].currency == available.amounts?[0].currency)

        XCTAssert(sut?.disposedBalance.currentBalances.count == pending.amounts?.count)
        XCTAssert(sut?.disposedBalance.currentBalances[0].amount == NSDecimalNumber(decimal: NSNumber(value: pending.amounts![0].amount).decimalValue))
        XCTAssert(sut?.disposedBalance.currentBalances[0].currency == pending.amounts?[0].currency)
        XCTAssert(sut?.disposedBalance.postedBalances.count == pending.amounts?.count)
        XCTAssert(sut?.disposedBalance.postedBalances[0].amount == NSDecimalNumber(decimal: NSNumber(value: pending.amounts![0].amount).decimalValue))
        XCTAssert(sut?.disposedBalance.postedBalances[0].currency == pending.amounts?[0].currency)
        XCTAssert(sut?.disposedBalance.pendingBalances.count == pending.amounts?.count)
        XCTAssert(sut?.disposedBalance.pendingBalances[0].amount == NSDecimalNumber(decimal: NSNumber(value: pending.amounts![0].amount).decimalValue))
        XCTAssert(sut?.disposedBalance.pendingBalances[0].currency == pending.amounts?[0].currency)

    }

    func test_givenFinancialOverviewContractEntityOfProductTypeCardAndUserName_whenICallInitContractEntity_thenHolderShouldMatchWithCompleteName() {

        // given
        let financialOverviewEntity = FinancialOverviewEntityGenerator.financialOverview().data?.contracts?.first(where: { contractEntity -> Bool in
            contractEntity.productType == FinancialOverviewContractEntity.cardProductType
        })

        let userBO = UserBO(user: UserEntity())
        userBO.firstName = "Rodrigo"
        userBO.lastName = "Sanchez"

        // when
        let sut = CardBO(contractEntity: financialOverviewEntity!, user: userBO)

        // then
        XCTAssert(sut?.holderName == userBO.completeUserName())
    }

    // MARK: - Expiration date format

    func test_givenCardEntityWithExpirateDateWithCorrectFormat_whenICreateCardBO_thenExpirationDateShouldBeFilled() {

        // given
        let cardEntity = CardsGenerator.getCardEntity(withExpirationDate: "2020-12-12").data![0]

        // when
        let cardBO = CardBO(cardEntity: cardEntity)

        // then
        XCTAssert(cardBO.expirationDate != nil)

    }

    func test_givenCardEntityWithExpirateDateWithCorrectFormat_whenICreateCardBO_thenExpirationDateShouldHaveTheCorrectValues() {

        // given
        let year = 2011
        let month = 11
        let day = 12
        let dateString = "\(year)-\(month)-\(day)"

        let cardEntity = CardsGenerator.getCardEntity(withExpirationDate: dateString).data![0]

        // when
        let cardBO = CardBO(cardEntity: cardEntity)

        // then
        let calendar = Calendar(identifier: Calendar.Identifier.gregorian)
        let dateComponents = calendar.dateComponents([.year, .month, .day], from: cardBO.expirationDate!)
        XCTAssert(dateComponents.year == year)
        XCTAssert(dateComponents.month == month)
        XCTAssert(dateComponents.day == day)

    }

    func test_givenCardEntityWithExpirateDateWithoutCorrectFormat_whenICreateCardBO_thenExpirationDateShouldNotBeFilled() {

        // given
        let cardEntity = CardsGenerator.getCardEntity(withExpirationDate: "12/12").data![0]

        // when
        let cardBO = CardBO(cardEntity: cardEntity)

        // then
        XCTAssert(cardBO.expirationDate == nil)

    }

    // MARK: - CardBO isPrepaid

    func test_givenCardWithCardTypePrepaid_whenICallIsPrepaid_thenReturnTrue() {

        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "PREPAID_CARD").cards[0]

        // when
        let isPrepaid = cardBO.isPrepaid()

        // then
        XCTAssert(isPrepaid == true)

    }

    func test_givenCardWithCardTypeDebit_whenICallIsPrepaid_thenReturnFalse() {

        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]

        // when
        let isPrepaid = cardBO.isPrepaid()

        // then
        XCTAssert(isPrepaid == false)

    }

    func test_givenCardWithCardTypeCredit_whenICallIsPrepaid_thenReturnFalse() {

        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "CREDIT_CARD").cards[0]

        // when
        let isPrepaid = cardBO.isPrepaid()

        // then
        XCTAssert(isPrepaid == false)

    }

    func test_givenCardWithCardTypeUnknowm_whenICallIsPrepaid_thenReturnFalse() {

        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "unnkonoasdf").cards[0]

        // when
        let isPrepaid = cardBO.isPrepaid()

        // then
        XCTAssert(isPrepaid == false)

    }

    // MARK: - CardBO isDebitCard

    func test_givenCardWithCardTypePrepaid_whenICallIsDebitCard_thenReturnFalse() {

        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "PREPAID_CARD").cards[0]

        // when
        let isDebitCard = cardBO.isDebitCard()

        // then
        XCTAssert(isDebitCard == false)

    }

    func test_givenCardWithCardTypeDebit_whenICallIsDebitCard_thenReturnTrue() {

        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]

        // when
        let isDebitCard = cardBO.isDebitCard()

        // then
        XCTAssert(isDebitCard == true)

    }

    func test_givenCardWithCardTypeCredit_whenICallIsDebitCard_thenReturnFalse() {

        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "CREDIT_CARD").cards[0]

        // when
        let isDebitCard = cardBO.isDebitCard()

        // then
        XCTAssert(isDebitCard == false)

    }

    func test_givenCardWithCardTypeUnknowm_whenICallIsDebitCard_thenReturnFalse() {

        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "unnkonoasdf").cards[0]

        // when
        let isDebitCard = cardBO.isDebitCard()

        // then
        XCTAssert(isDebitCard == false)

    }

    // MARK: - isInoperative

    func test_givenCardWithStatusIDInoperative_whenICallIsInoperative_thenReturnTrue() {

        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.status.id = .inoperative

        // when
        let isInoperative = cardBO.isInoperative()

        // then
        XCTAssert(isInoperative == true)
    }

    func test_givenCardWithStatusIDOperative_whenICallIsInoperative_thenReturnFalse() {

        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.status.id = .operative

        // when
        let isInoperative = cardBO.isInoperative()

        // then
        XCTAssert(isInoperative == false)
    }

    func test_givenCardWithStatusIDBlocked_whenICallIsInoperative_thenReturnFalse() {

        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.status.id = .blocked

        // when
        let isInoperative = cardBO.isInoperative()

        // then
        XCTAssert(isInoperative == false)
    }

    func test_givenCardWithStatusIDCanceled_whenICallIsInoperative_thenReturnFalse() {

        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.status.id = .canceled

        // when
        let isInoperative = cardBO.isInoperative()

        // then
        XCTAssert(isInoperative == false)
    }

    func test_givenCardWithStatusIDPendingembossing_whenICallIsInoperative_thenReturnFalse() {

        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.status.id = .pendingEmbossing

        // when
        let isInoperative = cardBO.isInoperative()

        // then
        XCTAssert(isInoperative == false)
    }

    func test_givenCardWithStatusIDPendingdelivery_whenICallIsInoperative_thenReturnFalse() {

        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.status.id = .pendingDelivery

        // when
        let isInoperative = cardBO.isInoperative()

        // then
        XCTAssert(isInoperative == false)
    }

    func test_givenCardWithStatusIDUnknowm_whenICallIsInoperative_thenReturnFalse() {

        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.status.id = .unknown

        // when
        let isInoperative = cardBO.isInoperative()

        // then
        XCTAssert(isInoperative == false)
    }
    
    // MARK: - isCardOperative
    
    func test_givenCardWithStatusIDInoperative_whenICallIsCardOperative_thenReturnFalse() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.status.id = .inoperative
        
        // when
        let isInoperative = cardBO.isCardOperative()
        
        // then
        XCTAssert(isInoperative == false)
    }
    
    func test_givenCardWithStatusIDOperative_whenICallIsCardOperative_thenReturnTrue() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.status.id = .operative
        
        // when
        let isInoperative = cardBO.isCardOperative()
        
        // then
        XCTAssert(isInoperative == true)
    }
    
    func test_givenCardWithStatusIDBlocked_whenICallIsCardOperative_thenReturnFalse() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.status.id = .blocked
        
        // when
        let isInoperative = cardBO.isCardOperative()
        
        // then
        XCTAssert(isInoperative == false)
    }
    
    func test_givenCardWithStatusIDCanceled_whenICallIsCardOperative_thenReturnFalse() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.status.id = .canceled
        
        // when
        let isInoperative = cardBO.isCardOperative()
        
        // then
        XCTAssert(isInoperative == false)
    }
    
    func test_givenCardWithStatusIDPendingembossing_whenICallIsCardOperative_thenReturnFalse() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.status.id = .pendingEmbossing
        
        // when
        let isInoperative = cardBO.isCardOperative()
        
        // then
        XCTAssert(isInoperative == false)
    }
    
    func test_givenCardWithStatusIDPendingdelivery_whenICallIsCardOperative_thenReturnFalse() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.status.id = .pendingDelivery
        
        // when
        let isInoperative = cardBO.isCardOperative()
        
        // then
        XCTAssert(isInoperative == false)
    }
    
    func test_givenCardWithStatusIDUnknowm_whenICallIsCardOperative_thenReturnFalse() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.status.id = .unknown
        
        // when
        let isInoperative = cardBO.isCardOperative()
        
        // then
        XCTAssert(isInoperative == false)
    }

    // MARK: - isBlocked

    func test_givenCardWithStatusIDInoperative_whenICallIsBlocked_thenReturnFalse() {

        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.status.id = .inoperative

        // when
        let isInoperative = cardBO.isBlocked()

        // then
        XCTAssert(isInoperative == false)
    }

    func test_givenCardWithStatusIDOperative_whenICallIsBlocked_thenReturnFalse() {

        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.status.id = .operative

        // when
        let isInoperative = cardBO.isBlocked()

        // then
        XCTAssert(isInoperative == false)
    }

    func test_givenCardWithStatusIDBlocked_whenICallIsBlocked_thenReturnTrue() {

        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.status.id = .blocked

        // when
        let isInoperative = cardBO.isBlocked()

        // then
        XCTAssert(isInoperative == true)
    }

    func test_givenCardWithStatusIDCanceled_whenICallIsBlocked_thenReturnFalse() {

        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.status.id = .canceled

        // when
        let isInoperative = cardBO.isBlocked()

        // then
        XCTAssert(isInoperative == false)
    }

    func test_givenCardWithStatusIDPendingembossing_whenICallIsBlocked_thenReturnFalse() {

        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.status.id = .pendingEmbossing

        // when
        let isInoperative = cardBO.isBlocked()

        // then
        XCTAssert(isInoperative == false)
    }

    func test_givenCardWithStatusIDPendingdelivery_whenICallIsBlocked_thenReturnFalse() {

        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.status.id = .pendingDelivery

        // when
        let isInoperative = cardBO.isBlocked()

        // then
        XCTAssert(isInoperative == false)
    }

    func test_givenCardWithStatusIDUnknowm_whenICallIsBlocked_thenReturnFalse() {

        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.status.id = .unknown

        // when
        let isInoperative = cardBO.isBlocked()

        // then
        XCTAssert(isInoperative == false)
    }
    
    // MARK: - isCanceled
    
    func test_givenCardWithStatusIDInoperative_whenICallIsCancelled_thenReturnFalse() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.status.id = .inoperative
        
        // when
        let isInoperative = cardBO.isCanceled()
        
        // then
        XCTAssert(isInoperative == false)
    }
    
    func test_givenCardWithStatusIDOperative_whenICallIsCancelled_thenReturnFalse() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.status.id = .operative
        
        // when
        let isInoperative = cardBO.isCanceled()
        
        // then
        XCTAssert(isInoperative == false)
    }
    
    func test_givenCardWithStatusIDBlocked_whenICallIsCancelled_thenReturnFalse() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.status.id = .blocked
        
        // when
        let isInoperative = cardBO.isCanceled()
        
        // then
        XCTAssert(isInoperative == false)
    }
    
    func test_givenCardWithStatusIDCanceled_whenICallIsCancelled_thenReturnTrue() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.status.id = .canceled
        
        // when
        let isInoperative = cardBO.isCanceled()
        
        // then
        XCTAssert(isInoperative == true)
    }
    
    func test_givenCardWithStatusIDPendingembossing_whenICallIsCancelled_thenReturnFalse() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.status.id = .pendingEmbossing
        
        // when
        let isInoperative = cardBO.isCanceled()
        
        // then
        XCTAssert(isInoperative == false)
    }
    
    func test_givenCardWithStatusIDPendingdelivery_whenICallIsCancelled_thenReturnFalse() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.status.id = .pendingDelivery
        
        // when
        let isInoperative = cardBO.isCanceled()
        
        // then
        XCTAssert(isInoperative == false)
    }
    
    func test_givenCardWithStatusIDUnknowm_whenICallIsCancelled_thenReturnFalse() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.status.id = .unknown
        
        // when
        let isInoperative = cardBO.isCanceled()
        
        // then
        XCTAssert(isInoperative == false)
    }
    
    // MARK: - isPendingEmbossing
    
    func test_givenCardWithStatusIDInoperative_whenICallIsPendingEmbossing_thenReturnFalse() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.status.id = .inoperative
        
        // when
        let isInoperative = cardBO.isPendingEmbossing()
        
        // then
        XCTAssert(isInoperative == false)
    }
    
    func test_givenCardWithStatusIDOperative_whenICallIsPendingEmbossing_thenReturnFalse() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.status.id = .operative
        
        // when
        let isInoperative = cardBO.isPendingEmbossing()
        
        // then
        XCTAssert(isInoperative == false)
    }
    
    func test_givenCardWithStatusIDBlocked_whenICallIsPendingEmbossing_thenReturnFalse() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.status.id = .blocked
        
        // when
        let isInoperative = cardBO.isPendingEmbossing()
        
        // then
        XCTAssert(isInoperative == false)
    }
    
    func test_givenCardWithStatusIDCanceled_whenICallIsPendingEmbossing_thenReturnFalse() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.status.id = .canceled
        
        // when
        let isInoperative = cardBO.isPendingEmbossing()
        
        // then
        XCTAssert(isInoperative == false)
    }
    
    func test_givenCardWithStatusIDPendingembossing_whenICallIsPendingEmbossing_thenReturnTrue() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.status.id = .pendingEmbossing
        
        // when
        let isInoperative = cardBO.isPendingEmbossing()
        
        // then
        XCTAssert(isInoperative == true)
    }
    
    func test_givenCardWithStatusIDPendingdelivery_whenICallIsPendingEmbossing_thenReturnFalse() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.status.id = .pendingDelivery
        
        // when
        let isInoperative = cardBO.isPendingEmbossing()
        
        // then
        XCTAssert(isInoperative == false)
    }
    
    func test_givenCardWithStatusIDUnknowm_whenICallIsPendingEmbossing_thenReturnFalse() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.status.id = .unknown
        
        // when
        let isInoperative = cardBO.isPendingEmbossing()
        
        // then
        XCTAssert(isInoperative == false)
    }
    
    // MARK: - isPendingDelivery
    
    func test_givenCardWithStatusIDInoperative_whenICallIsPendingDelivery_thenReturnFalse() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.status.id = .inoperative
        
        // when
        let isInoperative = cardBO.isPendingDelivery()
        
        // then
        XCTAssert(isInoperative == false)
    }
    
    func test_givenCardWithStatusIDOperative_whenICallIsPendingDelivery_thenReturnFalse() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.status.id = .operative
        
        // when
        let isInoperative = cardBO.isPendingDelivery()
        
        // then
        XCTAssert(isInoperative == false)
    }
    
    func test_givenCardWithStatusIDBlocked_whenICallIsPendingDelivery_thenReturnFalse() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.status.id = .blocked
        
        // when
        let isInoperative = cardBO.isPendingDelivery()
        
        // then
        XCTAssert(isInoperative == false)
    }
    
    func test_givenCardWithStatusIDCanceled_whenICallIsPendingDelivery_thenReturnFalse() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.status.id = .canceled
        
        // when
        let isInoperative = cardBO.isPendingDelivery()
        
        // then
        XCTAssert(isInoperative == false)
    }
    
    func test_givenCardWithStatusIDPendingembossing_whenICallIsPendingDelivery_thenReturnFalse() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.status.id = .pendingEmbossing
        
        // when
        let isInoperative = cardBO.isPendingDelivery()
        
        // then
        XCTAssert(isInoperative == false)
    }
    
    func test_givenCardWithStatusIDPendingdelivery_whenICallIsPendingDelivery_thenReturnTrue() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.status.id = .pendingDelivery
        
        // when
        let isInoperative = cardBO.isPendingDelivery()
        
        // then
        XCTAssert(isInoperative == true)
    }
    
    func test_givenCardWithStatusIDUnknowm_whenICallIsPendingDelivery_thenReturnFalse() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.status.id = .unknown
        
        // when
        let isInoperative = cardBO.isPendingDelivery()
        
        // then
        XCTAssert(isInoperative == false)
    }
    
    // MARK: - isUnknown
    
    func test_givenCardWithStatusIDInoperative_whenICallIsUnknown_thenReturnFalse() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.status.id = .inoperative
        
        // when
        let isInoperative = cardBO.isUnknown()
        
        // then
        XCTAssert(isInoperative == false)
    }
    
    func test_givenCardWithStatusIDOperative_whenICallIsUnknown_thenReturnFalse() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.status.id = .operative
        
        // when
        let isInoperative = cardBO.isUnknown()
        
        // then
        XCTAssert(isInoperative == false)
    }
    
    func test_givenCardWithStatusIDBlocked_whenICallIsUnknown_thenReturnFalse() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.status.id = .blocked
        
        // when
        let isInoperative = cardBO.isUnknown()
        
        // then
        XCTAssert(isInoperative == false)
    }
    
    func test_givenCardWithStatusIDCanceled_whenICallIsUnknown_thenReturnFalse() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.status.id = .canceled
        
        // when
        let isInoperative = cardBO.isUnknown()
        
        // then
        XCTAssert(isInoperative == false)
    }
    
    func test_givenCardWithStatusIDPendingembossing_whenICallIsUnknown_thenReturnFalse() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.status.id = .pendingEmbossing
        
        // when
        let isInoperative = cardBO.isUnknown()
        
        // then
        XCTAssert(isInoperative == false)
    }
    
    func test_givenCardWithStatusIDPendingdelivery_whenICallIsUnknown_thenReturnFalse() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.status.id = .pendingDelivery
        
        // when
        let isInoperative = cardBO.isUnknown()
        
        // then
        XCTAssert(isInoperative == false)
    }
    
    func test_givenCardWithStatusIDUnknowm_whenICallIsUnknown_thenReturnTrue() {
        
        // given
        let cardBO = CardsGenerator.getCardBOWithCardType(withCardType: "DEBIT_CARD").cards[0]
        cardBO.status.id = .unknown
        
        // when
        let isInoperative = cardBO.isUnknown()
        
        // then
        XCTAssert(isInoperative == true)
    }

    // MARK: - Init with number(pan)

    func test_givenCardEntityWithPanWithSpaceInFirstPlace_whenICreateCardBO_thenCardBONumberDoNotHaveThatSpace() {

        // given
        let cardEntity = CardsGenerator.geCards(withNumber: " 4444 1111 3333 8888").data![0]

        // when
        let cardBO = CardBO(cardEntity: cardEntity)

        // then
        XCTAssert(cardBO.number == "4444 1111 3333 8888")

    }

    func test_givenCardEntityWithPanWithoutSpaceInFirstPlace_whenICreateCardBO_thenCardBONumberShouldMatchWithEntity() {

        // given
        let cardEntity = CardsGenerator.geCards(withNumber: "4444 1111 3333 8888").data![0]

        // when
        let cardBO = CardBO(cardEntity: cardEntity)

        // then
        XCTAssert(cardBO.number == cardEntity.number)

    }

    // MARK: - physicalSupport

    func test_givenCardWithNotPhysicalSupport_whenICallIsNormalPlastic_thenReturnFalse() {

        // given
        let cardEntity = CardsGenerator.geCards(withNumber: "4444 1111 3333 8888").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)
        cardBO.physicalSupport = nil

        // when
        let result = cardBO.isNormalPlastic()

        // then
        XCTAssert(result == false)

    }

    func test_givenCardWithNotPhysicalSupport_whenICallIsSticker_thenReturnFalse() {

        // given
        let cardEntity = CardsGenerator.geCards(withNumber: "4444 1111 3333 8888").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)
        cardBO.physicalSupport = nil

        // when
        let result = cardBO.isSticker()

        // then
        XCTAssert(result == false)

    }

    func test_givenCardWithNotPhysicalSupport_whenICallIsVirtual_thenReturnFalse() {

        // given
        let cardEntity = CardsGenerator.geCards(withNumber: "4444 1111 3333 8888").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)
        cardBO.physicalSupport = nil

        // when
        let result = cardBO.isVirtual()

        // then
        XCTAssert(result == false)

    }

    func test_givenCardWithPhysicalSupportNormalPlastic_whenICallIsNormalPlastic_thenReturnTrue() {

        // given
        let cardEntity = CardsGenerator.geCards(withNumber: "4444 1111 3333 8888").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)
        cardBO.physicalSupport?.id = .normalPlastic

        // when
        let result = cardBO.isNormalPlastic()

        // then
        XCTAssert(result == true)

    }

    func test_givenCardWithPhysicalSupportSticker_whenICallIsNormalPlastic_thenReturnFalse() {

        // given
        let cardEntity = CardsGenerator.geCards(withNumber: "4444 1111 3333 8888").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)
        cardBO.physicalSupport?.id = .sticker

        // when
        let result = cardBO.isNormalPlastic()

        // then
        XCTAssert(result == false)

    }

    func test_givenCardWithPhysicalSupportVirtual_whenICallIsNormalPlastic_thenReturnFalse() {

        // given
        let cardEntity = CardsGenerator.geCards(withNumber: "4444 1111 3333 8888").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)
        cardBO.physicalSupport?.id = .virtual

        // when
        let result = cardBO.isNormalPlastic()

        // then
        XCTAssert(result == false)

    }

    func test_givenCardWithPhysicalSupportNormalPlastic_whenICallIsSticker_thenReturnFalse() {

        // given
        let cardEntity = CardsGenerator.geCards(withNumber: "4444 1111 3333 8888").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)
        cardBO.physicalSupport?.id = .normalPlastic

        // when
        let result = cardBO.isSticker()

        // then
        XCTAssert(result == false)

    }

    func test_givenCardWithPhysicalSupportSticker_whenICallIsSticker_thenReturnTrue() {

        // given
        let cardEntity = CardsGenerator.geCards(withNumber: "4444 1111 3333 8888").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)
        cardBO.physicalSupport?.id = .sticker

        // when
        let result = cardBO.isSticker()

        // then
        XCTAssert(result == true)

    }

    func test_givenCardWithPhysicalSupportVirtual_whenICallIsSticker_thenReturnFalse() {

        // given
        let cardEntity = CardsGenerator.geCards(withNumber: "4444 1111 3333 8888").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)
        cardBO.physicalSupport?.id = .virtual

        // when
        let result = cardBO.isSticker()

        // then
        XCTAssert(result == false)

    }

    func test_givenCardWithPhysicalSupportNormalPlastic_whenICallIsVirtual_thenReturnFalse() {

        // given
        let cardEntity = CardsGenerator.geCards(withNumber: "4444 1111 3333 8888").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)
        cardBO.physicalSupport?.id = .normalPlastic

        // when
        let result = cardBO.isVirtual()

        // then
        XCTAssert(result == false)

    }

    func test_givenCardWithPhysicalSupportSticker_whenICallIsVirtual_thenReturnFalse() {

        // given
        let cardEntity = CardsGenerator.geCards(withNumber: "4444 1111 3333 8888").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)
        cardBO.physicalSupport?.id = .sticker

        // when
        let result = cardBO.isVirtual()

        // then
        XCTAssert(result == false)

    }

    func test_givenCardWithPhysicalSupportVirtual_whenICallIsVirtual_thenReturnTrue() {

        // given
        let cardEntity = CardsGenerator.geCards(withNumber: "4444 1111 3333 8888").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)
        cardBO.physicalSupport?.id = .virtual

        // when
        let result = cardBO.isVirtual()

        // then
        XCTAssert(result == true)

    }
    
    func test_givenCardWithPhysicalSupportNFC_whenICallIsVirtual_thenReturnTrue() {
        
        // given
        let cardEntity = CardsGenerator.geCards(withNumber: "4444 1111 3333 8888").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)
        cardBO.physicalSupport?.id = .nfc
        
        // when
        let result = cardBO.isVirtual()
        
        // then
        XCTAssert(result == true)
        
    }

    // MARK: - isAssociableToVirtualCards

    func test_givenCardWithIndicatorsEmpty_whenICallIsAssociableToVirtualCards_thenReturnFalse() {

        // given
        let cardEntity = CardsGenerator.geCards(withNumber: "4444 1111 3333 8888").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)
        cardBO.indicators = [IndicatorBO]()

        // when
        let result = cardBO.isAssociableToVirtualCards()

        // then
        XCTAssert(result == false)

    }

    func test_givenCardWithIndicatorsWithoutIndicatorIsAssociableToVirtualCards_whenICallIsAssociableToVirtualCards_thenReturnFalse() {

        // given
        let cardEntity = CardsGenerator.geCards(withNumber: "4444 1111 3333 8888").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)

        // when
        let result = cardBO.isAssociableToVirtualCards()

        // then
        XCTAssert(result == false)

    }

    func test_givenCardWithIndicatorsWithIndicatorIsAssociableToVirtualCardsToFalse_whenICallIsAssociableToVirtualCards_thenReturnFalse() {

        // given
        let cardEntity = CardsGenerator.geCards(withNumber: "4444 1111 3333 8888").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)
        cardBO.indicators[3].indicatorId = .associableToVirtualCards
        cardBO.indicators[3].isActive = false

        // when
        let result = cardBO.isAssociableToVirtualCards()

        // then
        XCTAssert(result == false)

    }

    func test_givenCardWithIndicatorsWithIndicatorIsAssociableToVirtualCardsToTrue_whenICallIsAssociableToVirtualCards_thenReturnTrue() {

        // given
        let cardEntity = CardsGenerator.geCards(withNumber: "4444 1111 3333 8888").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)
        cardBO.indicators[2].indicatorId = .associableToVirtualCards
        cardBO.indicators[2].isActive = true

        // when
        let result = cardBO.isAssociableToVirtualCards()

        // then
        XCTAssert(result == true)

    }

    // MARK: - associatedVirtualCardBO

    func test_givenCardBOWithRelatedContractsEmpty_whenICallAssociatedVirtualCardBO_thenReturnNil() {

        // given
        let cardEntity = CardsGenerator.geCards(withNumber: "4444 1111 3333 8888").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)
        cardBO.relatedContracts = [RelatedContractBO]()

        let cards = CardsGenerator.getThreeCards().cards

        // when
        let virtualCard = cardBO.associatedVirtualCardBO(fromCards: cards)

        // then
        XCTAssert(virtualCard == nil)

    }

    func test_givenCardBOWithRelatedContracts_whenICallAssociatedVirtualCardBOWithEmtpyCards_thenReturnNil() {

        // given
        let cardEntity = CardsGenerator.geCards(withNumber: "4444 1111 3333 8888").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)

        let cards = [CardBO]()

        // when
        let virtualCard = cardBO.associatedVirtualCardBO(fromCards: cards)

        // then
        XCTAssert(virtualCard == nil)

    }

    func test_givenCardBOWithRelatedContractsWithNotLinkedWith_whenICallAssociatedVirtualCardBOWithCards_thenReturnNil() {

        // given
        let cardEntity = CardsGenerator.getCardWithCardTypeRelatedCard(withCardType: "PREPAID_CARD").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)

        for relactedContract in cardBO.relatedContracts {
            relactedContract.relationType!.id = .blockedDueToLost
        }

        let cards = CardsGenerator.getThreeCards().cards

        // when
        let virtualCard = cardBO.associatedVirtualCardBO(fromCards: cards)

        // then
        XCTAssert(virtualCard == nil)

    }

    func test_givenCardBOWithRelatedContractsWithLinkedWith_whenICallAssociatedVirtualCardBOWithCardsWithNoVirtualCards_thenReturnNil() {

        // given
        let cardEntity = CardsGenerator.getCardWithCardTypeRelatedCard(withCardType: "PREPAID_CARD").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)

        for relactedContract in cardBO.relatedContracts {
            relactedContract.relationType!.id = .linkedWith
        }

        let cards = CardsGenerator.getThreeCards().cards

        for card in cards {
            card.physicalSupport!.id = .normalPlastic
        }

        // when
        let virtualCard = cardBO.associatedVirtualCardBO(fromCards: cards)

        // then
        XCTAssert(virtualCard == nil)

    }

    func test_givenCardBOWithRelatedContractsWithLinkedWith_whenICallAssociatedVirtualCardBOWithCardsWithVirtualCardsButAnyCardIdDoesNotMatchWithAnycontractId_thenReturnNil() {

        // given
        let cardEntity = CardsGenerator.getCardWithCardTypeRelatedCard(withCardType: "PREPAID_CARD").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)

        for relactedContract in cardBO.relatedContracts {
            relactedContract.contractId = "3000"
            relactedContract.relationType!.id = .linkedWith
        }

        let cards = CardsGenerator.getThreeCards().cards

        for card in cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .virtual
        }

        // when
        let virtualCard = cardBO.associatedVirtualCardBO(fromCards: cards)

        // then
        XCTAssert(virtualCard == nil)

    }

    func test_givenCardBOWithRelatedContractsWithLinkedWith_whenICallAssociatedVirtualCardBOWithCardsWithoutVirtualCardsAndOneCardIdMatchWithcontractId_thenReturnNil() {

        // given
        let cardEntity = CardsGenerator.getCardWithCardTypeRelatedCard(withCardType: "PREPAID_CARD").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)

        for relactedContract in cardBO.relatedContracts {
            relactedContract.contractId = "3000"
            relactedContract.relationType!.id = .linkedWith
        }

        let cards = CardsGenerator.getThreeCards().cards

        for card in cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .sticker
        }

        cards[0].cardId = "3333"
        cardBO.relatedContracts[0].contractId = cards[0].cardId

        // when
        let virtualCard = cardBO.associatedVirtualCardBO(fromCards: cards)

        // then
        XCTAssert(virtualCard == nil)

    }

    func test_givenCardBOWithRelatedContractsWithLinkedWith_whenICallAssociatedVirtualCardBOWithCardsWithVirtualCardsAndOneVirtualCardIdMatchWithcontractId_thenReturnVirtualCardBOThatMatched() {

        // given
        let cardEntity = CardsGenerator.getCardWithCardTypeRelatedCard(withCardType: "PREPAID_CARD").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)

        for relactedContract in cardBO.relatedContracts {
            relactedContract.contractId = "3000"
            relactedContract.relationType!.id = .linkedWith
        }

        let cards = CardsGenerator.getThreeCards().cards

        for card in cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .virtual
        }

        cards[0].cardId = "3333"
        cardBO.relatedContracts[0].contractId = cards[0].cardId

        // when
        let virtualCard = cardBO.associatedVirtualCardBO(fromCards: cards)

        // then
        XCTAssert(virtualCard != nil)
        XCTAssert(virtualCard === cards[0])

    }

    // MARK: - associatedStickerCardBO

    func test_givenCardBOWithRelatedContractsEmpty_whenICallAssociatedStickerCardBO_thenReturnNil() {

        // given
        let cardEntity = CardsGenerator.geCards(withNumber: "4444 1111 3333 8888").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)
        cardBO.relatedContracts = [RelatedContractBO]()

        let cards = CardsGenerator.getThreeCards().cards

        // when
        let stickerCard = cardBO.associatedStickerCardBO(fromCards: cards)

        // then
        XCTAssert(stickerCard == nil)

    }

    func test_givenCardBOWithRelatedContracts_whenICallAssociatedStickerCardBOWithEmtpyCards_thenReturnNil() {

        // given
        let cardEntity = CardsGenerator.geCards(withNumber: "4444 1111 3333 8888").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)

        let cards = [CardBO]()

        // when
        let stickerCard = cardBO.associatedStickerCardBO(fromCards: cards)

        // then
        XCTAssert(stickerCard == nil)

    }

    func test_givenCardBOWithRelatedContractsWithNotLinkedWith_whenICallAssociatedStickerCardBOWithCards_thenReturnNil() {

        // given
        let cardEntity = CardsGenerator.getCardWithCardTypeRelatedCard(withCardType: "PREPAID_CARD").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)

        for relactedContract in cardBO.relatedContracts {
            relactedContract.relationType!.id = .blockedDueToLost
        }

        let cards = CardsGenerator.getThreeCards().cards

        // when
        let stickerCard = cardBO.associatedStickerCardBO(fromCards: cards)

        // then
        XCTAssert(stickerCard == nil)

    }

    func test_givenCardBOWithRelatedContractsWithLinkedWith_whenICallAssociatedStickerCardBOWithCardsWithNoStickerCards_thenReturnNil() {

        // given
        let cardEntity = CardsGenerator.getCardWithCardTypeRelatedCard(withCardType: "PREPAID_CARD").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)

        for relactedContract in cardBO.relatedContracts {
            relactedContract.relationType!.id = .linkedWith
        }

        let cards = CardsGenerator.getThreeCards().cards

        for card in cards {
            card.physicalSupport!.id = .normalPlastic
        }

        // when
        let stickerCard = cardBO.associatedStickerCardBO(fromCards: cards)

        // then
        XCTAssert(stickerCard == nil)

    }

    func test_givenCardBOWithRelatedContractsWithLinkedWith_whenICallAssociatedStickerCardBOWithCardsWithStickerCardsButAnyCardIdDoesNotMatchWithAnycontractId_thenReturnNil() {

        // given
        let cardEntity = CardsGenerator.getCardWithCardTypeRelatedCard(withCardType: "PREPAID_CARD").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)

        for relactedContract in cardBO.relatedContracts {
            relactedContract.contractId = "3000"
            relactedContract.relationType!.id = .linkedWith
        }

        let cards = CardsGenerator.getThreeCards().cards

        for card in cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .virtual
        }

        // when
        let stickerCard = cardBO.associatedStickerCardBO(fromCards: cards)

        // then
        XCTAssert(stickerCard == nil)

    }

    func test_givenCardBOWithRelatedContractsWithLinkedWith_whenICallAssociatedStickerCardBOWithCardsWithoutStickerCardsAndOneCardIdMatchWithcontractId_thenReturnNil() {

        // given
        let cardEntity = CardsGenerator.getCardWithCardTypeRelatedCard(withCardType: "PREPAID_CARD").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)

        for relactedContract in cardBO.relatedContracts {
            relactedContract.contractId = "3000"
            relactedContract.relationType!.id = .linkedWith
        }

        let cards = CardsGenerator.getThreeCards().cards

        for card in cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .virtual
        }

        cards[0].cardId = "3333"
        cardBO.relatedContracts[0].contractId = cards[0].cardId

        // when
        let stickerCard = cardBO.associatedStickerCardBO(fromCards: cards)

        // then
        XCTAssert(stickerCard == nil)

    }

    func test_givenCardBOWithRelatedContractsWithLinkedWith_whenICallAssociatedStickerCardBOWithCardsWithStickerCardsAndOneStickerCardIdMatchWithcontractId_thenReturnVirtualCardBOThatMatched() {

        // given
        let cardEntity = CardsGenerator.getCardWithCardTypeRelatedCard(withCardType: "PREPAID_CARD").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)

        for relactedContract in cardBO.relatedContracts {
            relactedContract.contractId = "3000"
            relactedContract.relationType!.id = .linkedWith
        }

        let cards = CardsGenerator.getThreeCards().cards

        for card in cards {
            card.cardId = "4000"
            card.physicalSupport!.id = .sticker
        }

        cards[0].cardId = "3333"
        cardBO.relatedContracts[0].contractId = cards[0].cardId

        // when
        let stickerCard = cardBO.associatedStickerCardBO(fromCards: cards)

        // then
        XCTAssert(stickerCard != nil)
        XCTAssert(stickerCard === cards[0])

    }

    // MARK: - isSettableToOperative

    func test_givenCardWithIndicatorsEmpty_whenICallIsSettableToOperative_thenReturnFalse() {

        // given
        let cardEntity = CardsGenerator.geCards(withNumber: "4444 1111 3333 8888").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)
        cardBO.indicators = [IndicatorBO]()

        // when
        let result = cardBO.isSettableToOperative()

        // then
        XCTAssert(result == false)

    }

    func test_givenCardWithIndicatorsWithoutIndicatorIsSettableToOperative_whenICallIsSettableToOperative_thenReturnFalse() {

        // given
        let cardEntity = CardsGenerator.geCards(withNumber: "4444 1111 3333 8888").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)

        // when
        let result = cardBO.isSettableToOperative()

        // then
        XCTAssert(result == false)

    }

    func test_givenCardWithIndicatorsWithIndicatorIsSettableToOperativeToFalse_whenICallIsSettableToOperative_thenReturnFalse() {

        // given
        let cardEntity = CardsGenerator.geCards(withNumber: "4444 1111 3333 8888").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)
        cardBO.indicators[3].indicatorId = .settableToOperative
        cardBO.indicators[3].isActive = false

        // when
        let result = cardBO.isSettableToOperative()

        // then
        XCTAssert(result == false)

    }

    func test_givenCardWithIndicatorsWithIndicatorIsSettableToOperativeToTrue_whenICallIsSettableToOperative_thenReturnTrue() {

        // given
        let cardEntity = CardsGenerator.geCards(withNumber: "4444 1111 3333 8888").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)
        cardBO.indicators[2].indicatorId = .settableToOperative
        cardBO.indicators[2].isActive = true

        // when
        let result = cardBO.isSettableToOperative()

        // then
        XCTAssert(result == true)

    }

    // MARK: - isReadableSecurityData

    func test_givenCardWithIndicatorsEmpty_whenICallIsReadableSecurityData_thenReturnFalse() {

        // given
        let cardEntity = CardsGenerator.geCards(withNumber: "4444 1111 3333 8888").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)
        cardBO.indicators = [IndicatorBO]()

        // when
        let result = cardBO.isReadableSecurityData()

        // then
        XCTAssert(result == false)

    }

    func test_givenCardWithIndicatorsWithoutIndicatorIsReadableSecurityData_whenICallIsReadableSecurityData_thenReturnFalse() {

        // given
        let cardEntity = CardsGenerator.geCards(withNumber: "4444 1111 3333 8888").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)
        cardBO.indicators[1].indicatorId = .cvv3

        // when
        let result = cardBO.isReadableSecurityData()

        // then
        XCTAssert(result == false)

    }

    func test_givenCardWithIndicatorsWithIndicatorIsReadableSecurityDataToFalse_whenICallIsReadableSecurityData_thenReturnFalse() {

        // given
        let cardEntity = CardsGenerator.geCards(withNumber: "4444 1111 3333 8888").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)
        cardBO.indicators[1].indicatorId = .readableSecurityData
        cardBO.indicators[1].isActive = false

        // when
        let result = cardBO.isReadableSecurityData()

        // then
        XCTAssert(result == false)

    }

    func test_givenCardWithIndicatorsWithIndicatorIsReadableSecurityDataToTrue_whenICallIsReadableSecurityData_thenReturnTrue() {

        // given
        let cardEntity = CardsGenerator.geCards(withNumber: "4444 1111 3333 8888").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)
        cardBO.indicators[1].indicatorId = .readableSecurityData
        cardBO.indicators[1].isActive = true

        // when
        let result = cardBO.isReadableSecurityData()

        // then
        XCTAssert(result == true)

    }

    // MARK: - isBlockable

    func test_givenCardWithIndicatorsEmpty_whenICallIsBlockable_thenReturnFalse() {

        // given
        let cardEntity = CardsGenerator.geCards(withNumber: "4444 1111 3333 8888").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)
        cardBO.indicators = [IndicatorBO]()

        // when
        let result = cardBO.isBlockable()

        // then
        XCTAssert(result == false)

    }

    func test_givenCardWithIndicatorsWithoutIndicatorIsBlockable_whenICallIsBlockable_thenReturnFalse() {

        // given
        let cardEntity = CardsGenerator.geCards(withNumber: "4444 1111 3333 8888").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)
        cardBO.indicators[3].indicatorId = .cvv3

        // when
        let result = cardBO.isBlockable()

        // then
        XCTAssert(result == false)

    }

    func test_givenCardWithIndicatorsWithIndicatorIsBlockableToFalse_whenICallIsBlockable_thenReturnFalse() {

        // given
        let cardEntity = CardsGenerator.geCards(withNumber: "4444 1111 3333 8888").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)
        cardBO.indicators[1].indicatorId = .blockable
        cardBO.indicators[1].isActive = false

        // when
        let result = cardBO.isReadableSecurityData()

        // then
        XCTAssert(result == false)

    }

    func test_givenCardWithIndicatorsWithIndicatorIsBlockableToTrue_whenICallIsBlockable_thenReturnTrue() {

        // given
        let cardEntity = CardsGenerator.geCards(withNumber: "4444 1111 3333 8888").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)
        cardBO.indicators[1].indicatorId = .blockable
        cardBO.indicators[1].isActive = true

        // when
        let result = cardBO.isBlockable()

        // then
        XCTAssert(result == true)

    }

    // MARK: - isPartialOff

    func test_givenCardOperativeAndActivationEcommerceFalse_whenICallIsPartialOff_thenReturnTrue() {

        // given
        let cardEntity = CardsGenerator.geCards(withNumber: "4444 1111 3333 8888").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)
        cardBO.status.id  = .operative
        cardBO.activations.append(ActivationBO(activationEntity: ActivationEntity(activationId: "ECOMMERCE_ACTIVATION",
                                                                                               name: "Any",
                                                                                               isActive: false,
                                                                                               isActivationEnabledForUser: true,
                                                                                               startDate: "Any", endDate: "Any",
                                                                                               limits: nil)))

        // when
        let result = cardBO.isPartialOff()

        // then
        XCTAssert(result == true)

    }

    func test_givenCardOperativeAndActivationCashwithdrawalFalse_whenICallIsPartialOff_thenReturnTrue() {

        // given
        let cardEntity = CardsGenerator.geCards(withNumber: "4444 1111 3333 8888").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)
        cardBO.status.id  = .operative
        cardBO.activations.append(ActivationBO(activationEntity: ActivationEntity(activationId: "CASHWITHDRAWAL_ACTIVATION",
                                                                                   name: "Any",
                                                                                   isActive: false,
                                                                                   isActivationEnabledForUser: true,
                                                                                   startDate: "Any", endDate: "Any",
                                                                                   limits: nil)))

        // when
        let result = cardBO.isPartialOff()

        // then
        XCTAssert(result == true)

    }

    func test_givenCardOperativeAndActivationPurchasesFalse_whenICallIsPartialOff_thenReturnTrue() {

        // given
        let cardEntity = CardsGenerator.geCards(withNumber: "4444 1111 3333 8888").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)
        cardBO.status.id  = .operative
        cardBO.activations.append(ActivationBO(activationEntity: ActivationEntity(activationId: "PURCHASES_ACTIVATION",
                                                                                   name: "Any",
                                                                                   isActive: false,
                                                                                   isActivationEnabledForUser: true,
                                                                                   startDate: "Any", endDate: "Any",
                                                                                   limits: nil)))

        // when
        let result = cardBO.isPartialOff()

        // then
        XCTAssert(result == true)

    }

    func test_givenCardOperativeAndActivationForeignCashWithDrawalFalse_whenICallIsPartialOff_thenReturnTrue() {

        // given
        let cardEntity = CardsGenerator.geCards(withNumber: "4444 1111 3333 8888").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)
        cardBO.status.id  = .operative
        cardBO.activations.append(ActivationBO(activationEntity: ActivationEntity(activationId: "FOREIGN_CASHWITHDRAWAL_ACTIVATION",
                                                                                   name: "Any",
                                                                                   isActive: false,
                                                                                   isActivationEnabledForUser: true,
                                                                                   startDate: "Any", endDate: "Any",
                                                                                   limits: nil)))

        // when
        let result = cardBO.isPartialOff()

        // then
        XCTAssert(result == true)

    }

    func test_givenCardOperativeAndActivationForeignPurchasesActivationFalse_whenICallIsPartialOff_thenReturnTrue() {

        // given
        let cardEntity = CardsGenerator.geCards(withNumber: "4444 1111 3333 8888").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)
        cardBO.status.id  = .operative
        cardBO.activations.append(ActivationBO(activationEntity: ActivationEntity(activationId: "FOREIGN_PURCHASES_ACTIVATION",
                                                                                   name: "Any",
                                                                                   isActive: false,
                                                                                   isActivationEnabledForUser: true,
                                                                                   startDate: "Any", endDate: "Any",
                                                                                   limits: nil)))

        // when
        let result = cardBO.isPartialOff()

        // then
        XCTAssert(result == true)

    }

    // MARK: - CurrentBalanceToShow

    func test_givenCardBOWithoutCurrentBalance_whenICallCurrentBalanceToShow_thenShouldReturnNil() {

        //given
        let cardEntity = CardsGenerator.geCards(withNumber: "4444 1111 3333 8888").data![0]
        let cardBO = CardBO(cardEntity: cardEntity)

        //when
        let currentBalance = cardBO.currentBalanceToShow(mainCurrencyCode: "USD")

        //then
        XCTAssert(currentBalance == nil)

    }

    func test_givenCardBOWithoutCurrencyMajorAndMainCurrencyCodeNotInBalances_whenICallCurrentBalanceToShow_thenShouldReturnNil() {

        //given
        let cardEntity = CardsGenerator.getThreeCardsEnttiy().data![0]
        let cardBO = CardBO(cardEntity: cardEntity)
        cardBO.currencyMajor = nil

        //when
        let currentBalance = cardBO.currentBalanceToShow(mainCurrencyCode: "COP")

        //then
        XCTAssert(currentBalance == nil)
    }

    func test_givenCreditCardBOWithtCurrencyMajorAndMainCurrencyCodeNotInBalances_whenICallCurrentBalanceToShow_thenShouldReturnTheCorrectValue() {

        //given
        let cardEntity = CardsGenerator.getThreeCardsEnttiy().data![0]
        let cardBO = CardBO(cardEntity: cardEntity)
        cardBO.cardType.id = .credit_card
        cardBO.currencyMajor = "USD"

        //when
        let currentBalance = cardBO.currentBalanceToShow(mainCurrencyCode: "COP")

        //then
        let currentBalanceExpected = cardBO.availableBalance.currentBalances.first { $0.currency == cardBO.currencyMajor }
        XCTAssert(currentBalance!.amount == currentBalanceExpected?.amount)
        XCTAssert(currentBalance!.currency == cardBO.currencyMajor)
        
    }

    func test_givenCreditCardBOWithoutCurrencyMajorAndMainCurrencyCodeInBalances_whenICallCurrentBalanceToShow_thenShouldReturnTheCorrectValue() {

        //given
        let cardEntity = CardsGenerator.getThreeCardsEnttiy().data![0]
        let cardBO = CardBO(cardEntity: cardEntity)
        cardBO.cardType.id = .credit_card
        cardBO.currencyMajor = nil

        //when
        let currentBalance = cardBO.currentBalanceToShow(mainCurrencyCode: "USD")

        //then
        let currentBalanceExpected = cardBO.availableBalance.currentBalances.first { $0.currency == "USD" }
        XCTAssert(currentBalance!.amount == currentBalanceExpected?.amount)
        XCTAssert(currentBalance!.currency == "USD")
        
    }
    
    func test_givenDebitCardBOWithtCurrencyMajorAndMainCurrencyCodeNotInBalances_whenICallCurrentBalanceToShow_thenShouldReturnTheCorrectValue() {
        
        //given
        let cardEntity = CardsGenerator.getThreeCardsEnttiy().data![0]
        let cardBO = CardBO(cardEntity: cardEntity)
        cardBO.cardType.id = .debit_card
        cardBO.currencyMajor = "USD"
        
        //when
        let currentBalance = cardBO.currentBalanceToShow(mainCurrencyCode: "COP")
        
        //then
        let currentBalanceExpected = cardBO.grantedCredits.first { $0.currency == cardBO.currencyMajor }
        XCTAssert(currentBalance!.amount == currentBalanceExpected?.amount)
        XCTAssert(currentBalance!.currency == cardBO.currencyMajor)

    }
    
    func test_givenDebitCardBOWithoutCurrencyMajorAndMainCurrencyCodeInBalances_whenICallCurrentBalanceToShow_thenShouldReturnTheCorrectValue() {
        
        //given
        let cardEntity = CardsGenerator.getThreeCardsEnttiy().data![0]
        let cardBO = CardBO(cardEntity: cardEntity)
        cardBO.cardType.id = .debit_card
        cardBO.currencyMajor = nil
        
        //when
        let currentBalance = cardBO.currentBalanceToShow(mainCurrencyCode: "USD")
        
        //then
        let currentBalanceExpected = cardBO.grantedCredits.first { $0.currency == "USD" }
        XCTAssert(currentBalance!.amount == currentBalanceExpected?.amount)
        XCTAssert(currentBalance!.currency == "USD")

    }

    // MARK: associatedCard

    func test_givenCardWithoutVirtual_whenICallAssociatedCard_virtual_thenShouldReturnNil() {

        // given
        let cardEntity = CardsGenerator.getThreeCardsEnttiy().data![0]
        let cardBO = CardBO(cardEntity: cardEntity)

        // when
        let cardBOVirtual = cardBO.associatedCard(fronCardList: [cardBO], type: .virtual)

        // then
        XCTAssert(cardBOVirtual == nil)

    }

    func test_givenCardWithVirtualNotAsociated_whenICallAssociatedCard_virtual_thenShouldReturnNTheVirtualCard() {

        // given
        let cardEntity = CardsGenerator.getThreeCardsEnttiy().data![0]
        let cardBO = CardBO(cardEntity: cardEntity)
        let cardEntityVirtual = CardsGenerator.getThreeCardsEnttiy().data![1]
        let cardBOVirtual = CardBO(cardEntity: cardEntityVirtual)
        cardBOVirtual.physicalSupport?.id = .virtual

        cardBO.relatedContracts[0].relationType?.id = .linkedWith
        cardBO.relatedContracts[0].contractId =  "111"

        // when
        let cardBOVirtualResult = cardBO.associatedCard(fronCardList: [cardBO, cardBOVirtual], type: .virtual)

        // then
        XCTAssert(cardBOVirtualResult !== cardBOVirtual)

    }

    func test_givenCardWithVirtual_whenICallAssociatedCard_virtual_thenShouldReturnNTheVirtualCard() {

        // given
        let cardEntity = CardsGenerator.getThreeCardsEnttiy().data![0]
        let cardBO = CardBO(cardEntity: cardEntity)
        let cardEntityVirtual = CardsGenerator.getThreeCardsEnttiy().data![1]
        let cardBOVirtual = CardBO(cardEntity: cardEntityVirtual)
        cardBOVirtual.physicalSupport?.id = .virtual

        cardBO.relatedContracts[0].relationType?.id = .linkedWith
        cardBO.relatedContracts[0].contractId = cardBOVirtual.cardId

        // when
        let cardBOVirtualResult = cardBO.associatedCard(fronCardList: [cardBO, cardBOVirtual], type: .virtual)

        // then
        XCTAssert(cardBOVirtualResult === cardBOVirtual)

    }

    // MARK: - updateActivations

    func test_givenCardBOWithActivationsAndActivationIncludedInCardBO_whenICallUpdateActivations_thenActivationsMatches() {

        // given
        let cardsBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        let cardBO = cardsBO.cards[0]
        cardBO.activations[0].isActive = false

        let activationBO = ActivationBO(activationEntity: ActivationEntity(activationId: "ON_OFF",
                                                                           name: "Any",
                                                                           isActive: true,
                                                                           isActivationEnabledForUser: true,
                                                                           startDate: "Any", endDate: "Any",
                                                                           limits: nil))

        // when
        cardBO.updateActivations(activationBO: activationBO)

        // then
        XCTAssert(cardBO.activations[0].activationId == activationBO.activationId)
        XCTAssert(cardBO.activations[0].isActive == activationBO.isActive)

    }

    func test_givenCardBOWithActivationsAndActivationNotIncludedInCardBO_whenICallUpdateActivations_thenActivationsAppendNewActivationBO() {

        // given
        let cardsBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ECOMMERCE_ACTIVATION", withIsActive: "false")
        let cardBO = cardsBO.cards[0]

        let activationBO = ActivationBO(activationEntity: ActivationEntity(activationId: "ON_OFF",
                                                                           name: "Any",
                                                                           isActive: true,
                                                                           isActivationEnabledForUser: true,
                                                                           startDate: "Any", endDate: "Any",
                                                                           limits: nil))

        // when
        cardBO.updateActivations(activationBO: activationBO)

        // then
        XCTAssert(cardBO.activations[0].activationId == .ecommerceActivation)
        XCTAssert(cardBO.activations[0].isActive == false)
        XCTAssert(cardBO.activations[1].activationId == activationBO.activationId)
        XCTAssert(cardBO.activations[1].isActive == activationBO.isActive)
        XCTAssert(cardBO.activations.count == 2)

    }

    func test_givenCardBOWithActivationsEmptyAndActivation_whenICallUpdateActivations_thenCreateActivationsWithActivationBO() {

        // given
        let cardsBO = CardsGenerator.getCardBO(withStatusId: "OPERATIVE", withActivationId: "ON_OFF", withIsActive: "true")
        let cardBO = cardsBO.cards[0]
        cardBO.activations = [ActivationBO]()

        let activationBO = ActivationBO(activationEntity: ActivationEntity(activationId: "ON_OFF",
                                                                           name: "Any",
                                                                           isActive: true,
                                                                           isActivationEnabledForUser: true,
                                                                           startDate: "Any", endDate: "Any",
                                                                           limits: nil))

        // when
        cardBO.updateActivations(activationBO: activationBO)

        // then
        XCTAssert(cardBO.activations[0].activationId == activationBO.activationId)
        XCTAssert(cardBO.activations[0].isActive == activationBO.isActive)
        XCTAssert(cardBO.activations.count == 1)

    }
}
