//
//  LinksBOTests.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 10/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class LinksBOTests: XCTestCase {

    var dummySessionManager: DummySessionManager!
    
    override func setUp() {
        super.setUp()
        
        DummySessionManager.setupMock()
        dummySessionManager = (SessionDataManager.sessionDataInstance() as! DummySessionManager)
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    // MARK: - generateNextURL

    func test_givenNextLinkNil_whenICallGenerateNextURL_thenNextLinkEqualToServerHost() {

        // given
        let serverHost = dummySessionManager.serverHost!
        let linksBO = LinksBO(linksEntity: LinksEntity(first: "Any", last: "Any", previous: "Any", next: nil))

        // when
        let nextResult = linksBO?.generateNextURL(isFinancial: false)

        // then
        XCTAssert(nextResult == serverHost)

    }

    func test_givenNextLinkEmpty_whenICallGenerateNextURL_thenNextLinkEqualToServerHost() {

        // given
        let serverHost = dummySessionManager.serverHost!
        let linksBO = LinksBO(linksEntity: LinksEntity(first: "Any", last: "Any", previous: "Any", next: ""))

        // when
        let nextResult = linksBO?.generateNextURL(isFinancial: false)

        // then
        XCTAssert(nextResult == serverHost)

    }

    func test_givenNextLinkWithSchemeAndHost_whenICallGenerateNextURL_thenNextLinkEqualToServerHost() {

        // given
        let serverHost = dummySessionManager.serverHost!
        let serverHostURL = URL(string: serverHost)
        let linksBO = LinksBO(linksEntity: LinksEntity(first: "Any", last: "Any", previous: "Any", next: "https://test.com"))

        var urlComponents = URLComponents()
        urlComponents.scheme = serverHostURL?.scheme
        urlComponents.host = serverHostURL?.host
        urlComponents.port = serverHostURL?.port

        // when
        let nextResult = linksBO?.generateNextURL(isFinancial: false)

        // then
        XCTAssert(nextResult == urlComponents.url?.absoluteString)

    }

    func test_givenNextLinkWithSchemeWithHostAndPathAndIsNotFinancial_whenICallGenerateNextURL_thenNextLinkEqualToServerHost() {

        // given
        let serverHost = dummySessionManager.serverHost!
        let serverHostURL = URL(string: serverHost)
        let linksBO = LinksBO(linksEntity: LinksEntity(first: "Any", last: "Any", previous: "Any", next: "https://test.com/test/v1/test"))
        let defaultDirectory = ServerHostURL.defaultDirectory()
        let path = (Configuration.selectedEnvironment() != "mock") ? "/\(defaultDirectory)/test/v1/test" : "/test/v1/test"

        var urlComponents = URLComponents()
        urlComponents.scheme = serverHostURL?.scheme
        urlComponents.host = serverHostURL?.host
        urlComponents.path = path
        urlComponents.port = serverHostURL?.port

        // when
        let nextResult = linksBO?.generateNextURL(isFinancial: false)

        // then
        XCTAssert(nextResult == urlComponents.url?.absoluteString)

    }

    func test_givenNextLinkOnlyWithPathAndIsNotFinancial_whenICallGenerateNextURL_thenNextLinkEqualToServerHost() {

        // given
        let serverHost = dummySessionManager.serverHost!
        let serverHostURL = URL(string: serverHost)
        let linksBO = LinksBO(linksEntity: LinksEntity(first: "Any", last: "Any", previous: "Any", next: "/test/v1/test"))
        let next = (linksBO?.next)!
        let defaultDirectory = ServerHostURL.defaultDirectory()
        let path = (Configuration.selectedEnvironment() != "mock") ? "/\(defaultDirectory)" + next : next

        var urlComponents = URLComponents()
        urlComponents.scheme = serverHostURL?.scheme
        urlComponents.host = serverHostURL?.host
        urlComponents.path = path
        urlComponents.port = serverHostURL?.port

        // when
        let nextResult = linksBO?.generateNextURL(isFinancial: false)

        // then
        XCTAssert(nextResult == urlComponents.url?.absoluteString)

    }

    func test_givenNextLinkOnlyWithPathWrongAndIsNotFinancial_whenICallGenerateNextURL_thenNextLinkEqualToServerHost() {

        // given
        let serverHost = dummySessionManager.serverHost!
        let serverHostURL = URL(string: serverHost)
        let linksBO = LinksBO(linksEntity: LinksEntity(first: "Any", last: "Any", previous: "Any", next: "test/v1/test"))
        let next = (linksBO?.next)!
        let defaultDirectory = ServerHostURL.defaultDirectory()
        let path = (Configuration.selectedEnvironment() != "mock") ? "/\(defaultDirectory)/" + next : "/" + next

        var urlComponents = URLComponents()
        urlComponents.scheme = serverHostURL?.scheme
        urlComponents.host = serverHostURL?.host
        urlComponents.path = path
        urlComponents.port = serverHostURL?.port

        // when
        let nextResult = linksBO?.generateNextURL(isFinancial: false)

        // then
        XCTAssert(nextResult == urlComponents.url?.absoluteString)

    }

    func test_givenNextLinkPathWrongAndIsNotFinancial_whenICallGenerateNextURL_thenNextLinkEqualToServerHost() {

        // given
        let serverHost = dummySessionManager.serverHost!
        let serverHostURL = URL(string: serverHost)
        let linksBO = LinksBO(linksEntity: LinksEntity(first: "Any", last: "Any", previous: "Any", next: "test"))
        let next = (linksBO?.next)!
        let defaultDirectory = ServerHostURL.defaultDirectory()
        let path = (Configuration.selectedEnvironment() != "mock") ? "/\(defaultDirectory)/" + next : "/" + next

        var urlComponents = URLComponents()
        urlComponents.scheme = serverHostURL?.scheme
        urlComponents.host = serverHostURL?.host
        urlComponents.path = path
        urlComponents.port = serverHostURL?.port

        // when
        let nextResult = linksBO?.generateNextURL(isFinancial: false)

        // then
        XCTAssert(nextResult == urlComponents.url?.absoluteString)

    }

    func test_givenNextLinkWithNothing_whenICallGenerateNextURL_thenNextLinkEqualToServerHost() {

        // given
        let serverHost = dummySessionManager.serverHost!
        let serverHostURL = URL(string: serverHost)
        let linksBO = LinksBO(linksEntity: LinksEntity(first: "Any", last: "Any", previous: "Any", next: "?"))

        var urlComponents = URLComponents()
        urlComponents.scheme = serverHostURL?.scheme
        urlComponents.host = serverHostURL?.host
        urlComponents.port = serverHostURL?.port

        // when
        let nextResult = linksBO?.generateNextURL(isFinancial: false)

        // then
        XCTAssert(nextResult == urlComponents.url?.absoluteString)

    }
    
    func test_givenNextLinkWithSchemeWithHostAndPathAndIsFinancial_whenICallGenerateNextURL_thenNextLinkEqualToServerHost() {
        
        // given
        let serverHost = dummySessionManager.serverHost!
        let serverHostURL = URL(string: serverHost)
        let linksBO = LinksBO(linksEntity: LinksEntity(first: "Any", last: "Any", previous: "Any", next: "https://test.com/test/v1/test"))
        let defaultDirectory = ServerHostURL.defaultDirectoryFinancial()
        let path = (Configuration.selectedEnvironment() != "mock") ? "/\(defaultDirectory)/test/v1/test" : "/test/v1/test"
        
        var urlComponents = URLComponents()
        urlComponents.scheme = serverHostURL?.scheme
        urlComponents.host = serverHostURL?.host
        urlComponents.path = path
        urlComponents.port = serverHostURL?.port
        
        // when
        let nextResult = linksBO?.generateNextURL(isFinancial: true)
        
        // then
        XCTAssert(nextResult == urlComponents.url?.absoluteString)
        
    }
    
    func test_givenNextLinkOnlyWithPathAndIsFinancial_whenICallGenerateNextURL_thenNextLinkEqualToServerHost() {
        
        // given
        let serverHost = dummySessionManager.serverHost!
        let serverHostURL = URL(string: serverHost)
        let linksBO = LinksBO(linksEntity: LinksEntity(first: "Any", last: "Any", previous: "Any", next: "/test/v1/test"))
        let next = (linksBO?.next)!
        let defaultDirectory = ServerHostURL.defaultDirectoryFinancial()
        let path = (Configuration.selectedEnvironment() != "mock") ? "/\(defaultDirectory)" + next : next
        
        var urlComponents = URLComponents()
        urlComponents.scheme = serverHostURL?.scheme
        urlComponents.host = serverHostURL?.host
        urlComponents.path = path
        urlComponents.port = serverHostURL?.port
        
        // when
        let nextResult = linksBO?.generateNextURL(isFinancial: true)
        
        // then
        XCTAssert(nextResult == urlComponents.url?.absoluteString)
        
    }
    
    func test_givenNextLinkOnlyWithPathWrongAndIsFinancial_whenICallGenerateNextURL_thenNextLinkEqualToServerHost() {
        
        // given
        let serverHost = dummySessionManager.serverHost!
        let serverHostURL = URL(string: serverHost)
        let linksBO = LinksBO(linksEntity: LinksEntity(first: "Any", last: "Any", previous: "Any", next: "test/v1/test"))
        let next = (linksBO?.next)!
        let defaultDirectory = ServerHostURL.defaultDirectoryFinancial()
        let path = (Configuration.selectedEnvironment() != "mock") ? "/\(defaultDirectory)/" + next : "/" + next
        
        var urlComponents = URLComponents()
        urlComponents.scheme = serverHostURL?.scheme
        urlComponents.host = serverHostURL?.host
        urlComponents.path = path
        urlComponents.port = serverHostURL?.port
        
        // when
        let nextResult = linksBO?.generateNextURL(isFinancial: true)
        
        // then
        XCTAssert(nextResult == urlComponents.url?.absoluteString)
        
    }
    
    func test_givenNextLinkPathWrongAndIsFinancial_whenICallGenerateNextURL_thenNextLinkEqualToServerHost() {
        
        // given
        let serverHost = dummySessionManager.serverHost!
        let serverHostURL = URL(string: serverHost)
        let linksBO = LinksBO(linksEntity: LinksEntity(first: "Any", last: "Any", previous: "Any", next: "test"))
        let next = (linksBO?.next)!
        let defaultDirectory = ServerHostURL.defaultDirectoryFinancial()
        let path = (Configuration.selectedEnvironment() != "mock") ? "/\(defaultDirectory)/" + next : "/" + next
        
        var urlComponents = URLComponents()
        urlComponents.scheme = serverHostURL?.scheme
        urlComponents.host = serverHostURL?.host
        urlComponents.path = path
        urlComponents.port = serverHostURL?.port
        
        // when
        let nextResult = linksBO?.generateNextURL(isFinancial: true)
        
        // then
        XCTAssert(nextResult == urlComponents.url?.absoluteString)
        
    }

}
