//
//  TransactionTypeFilterPresenterTest.swift
//  shoppingapp
//
//  Created by jesus.martinez on 3/8/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import XCTest
import RxSwift

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class PromotionsTrendingPresenterTest: XCTestCase {

    // MARK: vars
    let sut = PromotionsTrendingPresenter()
    var dummySessionManager: DummySessionManager!
    
    // MARK: setup
    override func setUp() {
        super.setUp()
        
        DummySessionManager.setupMock()
        dummySessionManager = (SessionDataManager.sessionDataInstance() as! DummySessionManager)
    }
    
    override func tearDown() {
        super.tearDown()
    }

    // MARK: - configureView

    func test_givenView_whenICallOnConfigureViewWithCategories_thenDisplayItemsHaveValues() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        //given
        let promotionsBO = PromotionsGenerator.getPromotionsBO()
        promotionsBO.promotions[0].endDate = Calendar.current.date(byAdding: .day, value: 2, to: Date())

        //When
        let displayData = DisplayItemPromotion.generateDisplayItemPromotionTrending(withTrendingPromotions: promotionsBO)

        // when
        sut.onConfigureView(withPromotions: displayData.itemsTrending)

        // then
        XCTAssert(sut.displayItems.items?.count == 2 )

        XCTAssert(sut.displayItems.items![0].commerceName == "Repsol".uppercased() )
        XCTAssert(sut.displayItems.items![0].benefit == "Descuentos" )
        XCTAssert(sut.displayItems.items![0].descriptionPromotion == "Save 5% when filling up your car by paying with your BBVA credit card in Repsol stations." )
        XCTAssertTrue(sut.displayItems.items![0].dateDiff != nil)
        XCTAssertTrue(sut.displayItems.items![0].imageList != nil)
        XCTAssertTrue(sut.displayItems.items![0].imagePush == nil)
        XCTAssertTrue(sut.displayItems.items![0].imageDetail != nil)
        XCTAssert(dummyView.prepareTableIsCalled == true)

    }

    // MARK: - configureView

    func test_givenView_whenICallOnConfigureViewWithCategories_thenCallPrepareTable() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let promotionBO = PromotionsGenerator.getPromotionsBO()
        let displayData = DisplayItemPromotion.generateDisplayItemPromotionTrending(withTrendingPromotions: promotionBO)

        // when
        sut.onConfigureView(withPromotions: displayData.itemsTrending)

        // then
        XCTAssert(sut.displayItems.items?.count == 2 )
        XCTAssert(dummyView.prepareTableIsCalled == true)

    }

    func test_givenView_whenICallOnConfigureViewWithPromotionsTrending_thenHaveFivePromotions() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        //given
        let promotionBO = PromotionsGenerator.getPromotionsBOBigger()

        //When
        let displayData = DisplayItemPromotion.generateDisplayItemPromotionTrending(withTrendingPromotions: promotionBO)

        // when
        sut.onConfigureView(withPromotions: displayData.itemsTrending)

        // then
        XCTAssert(sut.displayItems.items?.count == 5)
        XCTAssert(dummyView.addButtonIsCalled == true)

    }

    func test_givenPresenter_whenCallSetAnimationDone_thenSessionManagerIsShowTrendingAnimationIstrue() {

        // given
        
        // when
        sut.setAnimationDone()

        // then
        XCTAssert(dummySessionManager.isShowTrendingAnimation == true)

    }

    func test_givenPresenterWithIsShowTrendingAnimationFalseAndMoreThanOnePromotion_whenCallOnConfigure_thenICallChangeConstraint() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        dummySessionManager.isShowTrendingAnimation = false

        let promotionBO = PromotionsGenerator.getPromotionsBO()
        let displayData = DisplayItemPromotion.generateDisplayItemPromotionTrending(withTrendingPromotions: promotionBO)

        // when
        sut.onConfigureView(withPromotions: displayData.itemsTrending)

        // then
        XCTAssert(dummyView.changeConstrainsAnimationIsCalled == true)

    }

    func test_givenPresenterWithIsShowTrendingAnimationFalseAndMoreThanOnePromotion_whenCallOnConfigure_thenICallRestoreConstraint() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        dummySessionManager.isShowTrendingAnimation = false

        let promotionBO = PromotionsGenerator.getPromotionsBO()
        let displayData = DisplayItemPromotion.generateDisplayItemPromotionTrending(withTrendingPromotions: promotionBO)

        // when
        sut.onConfigureView(withPromotions: displayData.itemsTrending)

        // then
        XCTAssert(dummyView.restoreConstraintAnimationIsCalled == true)

    }

    func test_givenPresenterWithIsShowTrendingAnimationFalseAndOnePromotion_whenCallOnConfigure_thenINotCallChangeConstraint() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        dummySessionManager.isShowTrendingAnimation = false

        let promotionBO = PromotionsGenerator.getPromotionsBO()
        promotionBO.promotions.remove(at: 0)

        let displayData = DisplayItemPromotion.generateDisplayItemPromotionTrending(withTrendingPromotions: promotionBO)

        // when
        sut.onConfigureView(withPromotions: displayData.itemsTrending)

        // then
        XCTAssert(dummyView.changeConstrainsAnimationIsCalled == false)

    }

    func test_givenPresenterWithIsShowTrendingAnimationFalseAndOnePromotion_whenCallOnConfigure_thenINotCallRestoreConstraint() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        dummySessionManager.isShowTrendingAnimation = false

        let promotionBO = PromotionsGenerator.getPromotionsBO()
        promotionBO.promotions.remove(at: 0)
        let displayData = DisplayItemPromotion.generateDisplayItemPromotionTrending(withTrendingPromotions: promotionBO)

        // when
        sut.onConfigureView(withPromotions: displayData.itemsTrending)

        // then
        XCTAssert(dummyView.restoreConstraintAnimationIsCalled == false)

    }

    class DummyView: PromotionsTrendingViewProtocol {

        var prepareTableIsCalled: Bool = false
        var addButtonIsCalled: Bool = false
        var changeConstrainsAnimationIsCalled: Bool = false
        var restoreConstraintAnimationIsCalled: Bool = false

        var displayItemsCount: Int = 0

        func prepareTable(withDisplayItems displayItems: DisplayItemsPromotions) {
            prepareTableIsCalled = true
            displayItemsCount = displayItems.items!.count
        }

        func addButtonSeeAll() {
            addButtonIsCalled = true
        }

        func changeConstraintAnimation() {
            changeConstrainsAnimationIsCalled = true
        }

        func restoreConstraintAnimation() {
            restoreConstraintAnimationIsCalled = true
        }
    }
}
