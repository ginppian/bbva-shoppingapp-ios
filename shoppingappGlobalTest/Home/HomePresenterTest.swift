//
//  File.swift
//  shoppingapp
//
//  Created by jesus.martinez on 3/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import XCTest
import RxSwift
import CellsNativeComponents

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class HomePresenterTest: XCTestCase {

    var sut: HomePresenter<HomeViewDummy>!
    var dummyBusManager: DummyBusManager!
    var dummyInteractor = DummyInteractor()
    var dummyView = HomeViewDummy()
    private var dummyPreferencesManager: DummyPreferencesManager!

    override func setUp() {
        super.setUp()
        
        dummyBusManager = DummyBusManager()
        sut = HomePresenter<HomeViewDummy>(busManager: dummyBusManager)
        dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        dummyView = HomeViewDummy()
        sut.view = dummyView

        sut.notificationManager = DummyNotificationManager()
        
        DummyPreferencesManager.configureDummyPreferences()
        dummyPreferencesManager = PreferencesManager.instance as? DummyPreferencesManager
        
        DummyPublicConfigurationManager.setupMock()
    }

    // MARK: - receive model

    func test_givenCardsBOAndCardsNil_whenICallReceivedModel_thenCardsBOShouldBeFilled() {

        // given
        let cardsBO = CardsGenerator.getCardBOWithAlias()

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(sut.cards != nil)

    }

    func test_givenCardsBOAndCardsNil_whenICallReceivedModel_thenCardsBOShouldMatch() {

        // given
        let cardsBO = CardsGenerator.getCardBOWithAlias()

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(sut.cards === cardsBO)

    }

    func test_givenCardsBOAndCardsNotNilButDifferentTimestamp_whenICallReceivedModel_thenCardsBOShouldMatch() {

        // given
        let cardsBefore = CardsGenerator.getCardBOWithAlias()
        sut.cards = cardsBefore
        let cardsBO = CardsGenerator.getCardBOWithAlias()

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(sut.cards === cardsBO)
    }

    func test_givenCardsBOAndCardsNotNilAndSameTimestamp_whenICallReceivedModel_thenCardsBOShouldMatch() {

        // given
        let cardsBO = CardsGenerator.getCardBOWithAlias()
        sut.cards = cardsBO

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(sut.cards === cardsBO)
    }

    func test_givenCardsBOAndCardsNotNilAndSameTimestamp_whenICallReceivedModel_thenINotCallCardListSetModel() {

        let dummyView = HomeViewDummy()
        sut.view = dummyView

        // given
        let cardsBO = CardsGenerator.getCardBOWithAlias()
        sut.cards = cardsBO

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.isCalledCardListViewSetModel == false)

    }

    func test_givenCardsBOAndCardsNotNilAndDifferentTimestamp_whenICallReceivedModel_thenINotCallCardListSetModel() {

        let dummyView = HomeViewDummy()
        sut.view = dummyView

        // given
        sut.cards = CardsGenerator.getCardBOWithAlias()
        let cardsBO = CardsGenerator.getCardBOWithAlias()

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.isCalledCardListViewSetModel == true)

    }

    func test_givenCardsBOAndCardsNil_whenICallReceivedModel_thenICallCardListSetModel() {

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        let dummyView = HomeViewDummy()
        sut.view = dummyView

        // given
        let cardsBO = CardsGenerator.getCardBOWithAlias()

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.isCalledCardListViewSetModel == true)

    }

    func test_givenCardsBOAndCardsNil_whenICallReceivedModel_thenDataShouldMatch() {

        let dummyView = HomeViewDummy()
        sut.view = dummyView
        sut.interactor = HomeInteractor()

        // given
        let cardsBO = CardsGenerator.getCardBOWithAlias()

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.cards === cardsBO)
        XCTAssert(dummyView.cardTypesToShowInHome! == Settings.Home.cardTypesToShowInHome)

        _ = sut.interactor.provideCurrency().subscribe(
            onNext: { result in

                if let currency = result as? CurrencyBO {
                    XCTAssert(dummyView.currencyCode == currency.code)
                }
            })

    }

    func test_givenCardsBOAndCardsNil_whenICallReceivedModel_thenICallInteractorProvideCurrency() {

        // given
        let cardsBO = CardsGenerator.getCardBOWithAlias()

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyInteractor.isCalledProvideCurrency == true)

    }

    func test_givenCardsBOAndCardsNil_whenICallReceiveModel_thenICallViewShowTrendingPromotions() {

        // given
        let cardsBO = CardsGenerator.getCardBOWithAlias()
        sut.cards = nil

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.isCalledShowTrendingPromotion == true)
    }

    func test_givenCardsBONotNilButDiferrentTimestamp_whenICallReceiveModel_thenICallViewShowTrendingPromotions() {

        // given
        sut.cards = CardsGenerator.getCardBOWithAlias()
        let cardsBO = CardsGenerator.getCardBOWithAlias()

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.isCalledShowTrendingPromotion == true)
    }

    func test_givenCardsBONotNilButEqualsTimestamp_whenICallReceiveModel_thenIDoNotCallViewShowTrendingPromotions() {

        // given
        let cardsBO = CardsGenerator.getCardBOWithAlias()
        sut.cards = cardsBO

        // when
        sut.receive(model: cardsBO)

        // then
        XCTAssert(dummyView.isCalledShowTrendingPromotion == false)
    }

    // MARK: cardStatusUpdated

    func test_givenNotCardsBOAndCardModel_whenICallCardStatusUpdated_thenINOTCallCardListSetModel() {

        // given
        let cardsBO = CardsGenerator.getCardBOWithAlias()
        let dummyView = HomeViewDummy()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let cardModel = CardModel(cardBO: cardsBO.cards.first!)

        // when
        sut.cardStatusUpdated(card: cardModel)

        // then
        XCTAssert(dummyView.isCalledCardListViewSetModel == false)

    }

    func test_givenCardsBOAndCardModel_whenICallCardStatusUpdated_thenICallCardListSetModel() {

        // given
        let cardsBO = CardsGenerator.getCardBOWithAlias()
        let dummyView = HomeViewDummy()
        sut.view = dummyView
        sut.cards = cardsBO

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let cardModel = CardModel(cardBO: sut.cards!.cards.first!)

        // when
        sut.cardStatusUpdated(card: cardModel)

        // then
        XCTAssert(dummyView.isCalledCardListViewSetModel == true)

    }

    func test_givenCardsBOAndCardModel_whenICallCardStatusUpdated_thenDataShoulddMatch() {

        // given
        let cardsBO = CardsGenerator.getCardBOWithAlias()
        let dummyView = HomeViewDummy()
        sut.view = dummyView
        sut.cards = cardsBO

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let cardsCopyBO = CardsGenerator.getCardBOWithAlias()
        cardsCopyBO.cards.first!.currencyMajor = "Test"

        let cardModel = CardModel(cardBO: cardsCopyBO.cards.first!)

        // when
        sut.cardStatusUpdated(card: cardModel)

        // then
        XCTAssert(dummyView.cards?.cards.first?.currencyMajor == cardsCopyBO.cards.first!.currencyMajor)
        XCTAssert(dummyView.cards === cardsBO)
        XCTAssert(dummyView.cardTypesToShowInHome! == Settings.Home.cardTypesToShowInHome)

        _ = sut.interactor.provideCurrency().subscribe(
            onNext: { result in

                if let currency = result as? CurrencyBO {
                    XCTAssert(dummyView.currencyCode == currency.code)
                }
            })

    }

    func test_givenCardsBOAndCardModel_whenICallCardStatusUpdated_thenICallInteractorProvideCurrency() {

        // given
        let cardsBO = CardsGenerator.getCardBOWithAlias()
        let dummyView = HomeViewDummy()
        sut.view = dummyView
        sut.cards = cardsBO

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        let cardModel = CardModel(cardBO: sut.cards!.cards.first!)

        // when
        sut.cardStatusUpdated(card: cardModel)

        // then
        XCTAssert(dummyInteractor.isCalledProvideCurrency == true)

    }

    // MARK: getDidacticArea

    func test_givenAny_whenICallGetCardList_thenICallShowDidacticAreaDisplayData() {

        let dummyView = HomeViewDummy()
        sut.view = dummyView

        // given

        // when
        sut.getDidacticArea()

        // then
        XCTAssert(dummyView.isCalledShowDidacticAreaDisplayData == true)

    }

    func test_givenAny_whenICallGetDidacticArea_thenIGenerateDidacticAreaDisplayData() {

        let dummyView = HomeViewDummy()
        sut.view = dummyView

        // given

        // when
        sut.getDidacticArea()

        // then
        XCTAssert(dummyView.displayDataDidacticArea != nil)

    }

    func test_giveAny_whenICallGetDidacticArea_thenICallGetDidacticAreaToShow() {

        //given
        let dummyView = HomeViewDummy()
        sut.view = dummyView

        let dummyPublicManager = DummyConfigPublicManager()
        ConfigPublicManager.instance = dummyPublicManager

        //When
        sut.getDidacticArea()

        //then
        XCTAssert(dummyPublicManager.isCalledGetDidacticAreaToShow == true)

    }

    // MARK: showNavigationBarAnimation

    func test_giveContentSizeHeightGreaterThanscrollViewHeightPlusOffsetAnimation_whenICallShowNavigationBarAnimation_thenICallChangeNavigationBarColor() {

        //given
        let dummyView = HomeViewDummy()
        sut.view = dummyView
        dummyView.scrollViewHeight = 640
        dummyView.contentSize = CGSize(width: 320, height: 700)
        dummyView.contentOffset = CGPoint(x: 0, y: 60)
        dummyView.offsetAnimation = 30.0

        //When
        sut.showNavigationBarAnimation(byScrollViewHeight: dummyView.scrollViewHeight, withContentSize: dummyView.contentSize, andContentOffset: dummyView.contentOffset)

        //then
        XCTAssert(dummyView.isCalledChangeNavigationBarColor == true)

    }

    func test_giveContentSizeHeightSmallerThanscrollViewHeightPlusOffsetAnimation_whenICallShowNavigationBarAnimation_thenIDoNotCallChangeNavigationBarColor() {

        //given
        let dummyView = HomeViewDummy()
        sut.view = dummyView
        dummyView.scrollViewHeight = 640
        dummyView.contentSize = CGSize(width: 320, height: 640)
        dummyView.contentOffset = CGPoint(x: 0, y: 60)
        dummyView.offsetAnimation = 30.0

        //When
        sut.showNavigationBarAnimation(byScrollViewHeight: dummyView.scrollViewHeight, withContentSize: dummyView.contentSize, andContentOffset: dummyView.contentOffset)

        //then
        XCTAssert(dummyView.isCalledChangeNavigationBarColor == false)

    }

    func test_giveContentSizeHeightGreaterThanscrollViewHeightPlusOffsetAnimation_whenICallShowNavigationBarAnimation_thenICallViewShowNavigationTitle() {

        //given
        let dummyView = HomeViewDummy()
        sut.view = dummyView
        dummyView.scrollViewHeight = 640
        dummyView.contentSize = CGSize(width: 320, height: 700)
        dummyView.contentOffset = CGPoint(x: 0, y: 60)
        dummyView.offsetAnimation = 30.0

        //When
        sut.showNavigationBarAnimation(byScrollViewHeight: dummyView.scrollViewHeight, withContentSize: dummyView.contentSize, andContentOffset: dummyView.contentOffset)

        //then
        XCTAssert(dummyView.isCalledShowNavigationTitle == true)

    }

    func test_giveContentSizeHeightSmallerThanscrollViewHeightPlusOffsetAnimation_whenICallShowNavigationBarAnimation_thenICallViewHideNavigationTitle() {

        //given
        let dummyView = HomeViewDummy()
        sut.view = dummyView
        dummyView.scrollViewHeight = 640
        dummyView.contentSize = CGSize(width: 320, height: 700)
        dummyView.contentOffset = CGPoint(x: 0, y: 20)
        dummyView.offsetAnimation = 30.0

        //When
        sut.showNavigationBarAnimation(byScrollViewHeight: dummyView.scrollViewHeight, withContentSize: dummyView.contentSize, andContentOffset: dummyView.contentOffset)

        //then
        XCTAssert(dummyView.isCalledHideNavigationTitle == true)

    }

    func test_giveContentSizeHeightGreaterThanscrollViewHeightPlusOffsetAnimation_whenICallShowNavigationBarAnimation_thenICallChangeNavigationBarColorWithColor() {

        //given
        let dummyView = HomeViewDummy()
        sut.view = dummyView
        dummyView.scrollViewHeight = 640
        dummyView.contentSize = CGSize(width: 320, height: 700)
        dummyView.contentOffset = CGPoint(x: 0, y: 60)
        dummyView.offsetAnimation = 30.0
        let offset = dummyView.contentOffset.y / dummyView.offsetAnimation
        let color = UIColor.NAVY.withAlphaComponent(offset)

        //When
        sut.showNavigationBarAnimation(byScrollViewHeight: dummyView.scrollViewHeight, withContentSize: dummyView.contentSize, andContentOffset: dummyView.contentOffset)

        //then
        XCTAssert(dummyView.navigationBarColor == color)

    }

    // MARK: - cardsListPresenter didSelectCardId

    func test_givenCardsBOCardListPresenterAndCardId_whenICallCardsListPresenterDidSelectCardId_thenICallBusManagerChangeTabToCards() {

        let cards = CardsGenerator.getCardBOCreditCard()

        // given
        sut.cards = cards
        let cardId = "2110"
        let cardsListPresenter = CardsListPresenter()

        // when
        sut.cardsListPresenter(cardsListPresenter, didSelectCardId: cardId)

        // then
        XCTAssert(dummyBusManager.isCalledChangeTabToCards == true)

    }

    func test_givenCardsBOCardListPresenterAndCardId_whenICallCardsListPresenterDidSelectCardId_thenICallBusManagerChangeTabToCardsWithAppropiateCard() {

        let cards = CardsGenerator.getCardBOCreditCard()

        // given
        sut.cards = cards
        let cardId = "2110"
        let cardsListPresenter = CardsListPresenter()

        // when
        sut.cardsListPresenter(cardsListPresenter, didSelectCardId: cardId)

        // then
        XCTAssert(dummyBusManager.cardTransportSent?.cardBO?.cardId == cardId)

    }

    // MARK: - TrendingPromotionPresenterDelegate didReceiveError

    func test_givenErrorBO_whenICallTrendingPromotionPresenterDidReceiveError_thenErrorBOShouldBeFilled() {

        // given
        let errorBO = ErrorBO()
        let componentPresenter = TrendingPromotionPresenter()

        // when
        sut.trendingPromotionPresenter(componentPresenter, didReceiveError: errorBO)

        // then
        XCTAssert(sut.errorBO != nil)

    }

    func test_givenErrorBO_whenICallTrendingPromotionPresenterDidReceiveError_thenErrorBOShouldMatch() {

        // given
        let errorBO = ErrorBO()
        let componentPresenter = TrendingPromotionPresenter()

        // when
        sut.trendingPromotionPresenter(componentPresenter, didReceiveError: errorBO)

        // then
        XCTAssert(sut.errorBO! === errorBO)

    }

    func test_givenErrorBO_whenICallTrendingPromotionPresenterDidReceiveError_thenICallViewShowError() {

        let dummyView = HomeViewDummy()
        sut.view = dummyView

        // given
        let errorBO = ErrorBO()
        let componentPresenter = TrendingPromotionPresenter()

        // when
        sut.trendingPromotionPresenter(componentPresenter, didReceiveError: errorBO)

        // then
        XCTAssert(dummyView.isCalledShowError == true)

    }

    func test_givenErrorBO_whenICallTrendingPromotionPresenterDidReceiveError_thenICallViewShowErrorWithErrorBO() {

        let dummyView = HomeViewDummy()
        sut.view = dummyView

        // given
        let errorBO = ErrorBO()
        let componentPresenter = TrendingPromotionPresenter()

        // when
        sut.trendingPromotionPresenter(componentPresenter, didReceiveError: errorBO)

        // then
        XCTAssert(dummyView.modelBOSent as? ErrorBO === errorBO)

    }

    // MARK: - TrendingPromotionPresenterDelegate headerPressed

    func test_givenAny_whenICallTrendingPromotionHeaderPressed_thenICallBusManagerChangeTabToPromotions() {

        let componentPresenter = TrendingPromotionPresenter()

        // given

        // when
        sut.trendingPromotionHeaderPressed(componentPresenter)

        // then
        XCTAssert(dummyBusManager.isCalledChangeTabToPromotions == true)

    }

    // MARK: - trendingPromotionCardsForFilter

    func test_givenCardsNil_whenICallTrendingPromotionCardsForFilter_thenIReturnNil() {

        let componentPresenter = TrendingPromotionPresenter()

        // given
        sut.cards = nil

        // when
        let cards = sut.trendingPromotionCardsForFilter(componentPresenter)

        // then
        XCTAssert(cards == nil)

    }

    func test_givenCards_whenICallTrendingPromotionCardsForFilter_thenIReturnCards() {

        let componentPresenter = TrendingPromotionPresenter()

        // given
        sut.cards = CardsGenerator.getCardBOWithAlias()

        // when
        let cards = sut.trendingPromotionCardsForFilter(componentPresenter)

        // then
        XCTAssert(cards?.timestamp == sut.cards?.timestamp)

    }

    // MARK: - didacticAreaPresenterDidacticButtonPressed

    func test_givenHelpId_whenICallDidacticAreaPresenterDidacticButtonPressedWithHelpId_thenICallBusManagerNavigateToDetailHelp() {

        // given
        let helpId = "helpId"

        // when
        sut.didacticAreaPresenterDidacticButtonPressed(DidacticAreaPresenter(), andHelpId: helpId)

        // then
        XCTAssert(dummyBusManager.isCalledNavigateToDetailHelp == true)

    }

    func test_givenHelpIdEmpty_whenICallDidacticAreaPresenterDidacticButtonPressedWithHelpId_thenINotCallBusManagerNavigateToDetailHelp() {

        // given
        let helpId = ""

        // when
        sut.didacticAreaPresenterDidacticButtonPressed(DidacticAreaPresenter(), andHelpId: helpId)

        // then
        XCTAssert(dummyBusManager.isCalledNavigateToDetailHelp == false)

    }

    func test_givenHelpId_whenICallDidacticAreaPresenterDidacticButtonPressedhHelpId_thenICallBusManagerNavigateToDetailHelpWithHelpDetailTransport() {

        // given
        let helpId = "helpId"

        // when
        sut.didacticAreaPresenterDidacticButtonPressed(DidacticAreaPresenter(), andHelpId: helpId)

        // then
        XCTAssert(dummyBusManager.helpDetailTransportSent?.helpId == helpId)

    }

    func test_givenHelpIdEmpty_whenICallDidacticAreaPresenterDidacticButtonPressedHelpId_thenICallBusManagerNavigateToHelp() {

        // given
        let helpId = ""

        // when
        sut.didacticAreaPresenterDidacticButtonPressed(DidacticAreaPresenter(), andHelpId: helpId)

        // then
        XCTAssert(dummyBusManager.isCalledNavigateToHelp == true)

    }

    func test_givenHelpId_whenICallDidacticAreaPresenterDidacticButtonPressedHelpId_thenINotCallBusManagerNavigateToHelp() {

        // given
        let helpId = "helpId"

        // when
        sut.didacticAreaPresenterDidacticButtonPressed(DidacticAreaPresenter(), andHelpId: helpId)

        // then
        XCTAssert(dummyBusManager.isCalledNavigateToHelp == false)

    }
    
    func test_givenCategoryIdAndTitle_whenICallDidacticAreaPresenterOfferButtonPressed_thenICallBusManagerNavigateToPromotionsCategoryScreen() {
        
        // given
        dummyBusManager.isCalledNavigateToPromotionsCategory = false
        let categoryId = CategoryType.activities.rawValue
        let title = "title1"
        
        // when
        sut.didacticAreaPresenterOfferButtonPressed(DidacticAreaPresenter(), categoryId: categoryId, andTitle: title)
        
        // then
        XCTAssert(dummyBusManager.isCalledNavigateToPromotionsCategory == true)
        
    }
    
    func test_givenCategoryIdEmpty_whenICallDidacticAreaPresenterOfferButtonPressed_thenIDontCallBusManagerNavigateToPromotionsCategoryScreen() {
        
        // given
        dummyBusManager.isCalledNavigateToPromotionsCategory = false
        let categoryId = ""
        let title = "title1"
        
        // when
        sut.didacticAreaPresenterOfferButtonPressed(DidacticAreaPresenter(), categoryId: categoryId, andTitle: title)
        
        // then
        XCTAssert(dummyBusManager.isCalledNavigateToPromotionsCategory == false)
    }
    
    func test_givenCategoryIdUnknow_whenICallDidacticAreaPresenterOfferButtonPressed_thenIDontCallBusManagerNavigateToPromotionsCategoryScreen() {
        
        // given
        dummyBusManager.isCalledNavigateToPromotionsCategory = false
        let categoryId = CategoryType.unknown.rawValue
        let title = "title1"
        
        // when
        sut.didacticAreaPresenterOfferButtonPressed(DidacticAreaPresenter(), categoryId: categoryId, andTitle: title)
        
        // then
        XCTAssert(dummyBusManager.isCalledNavigateToPromotionsCategory == false)
        
    }
    
    func test_givenTitleEmpty_whenICallDidacticAreaPresenterOfferButtonPressed_thenIDontCallBusManagerNavigateToPromotionsCategoryScreen() {
        
        // given
        dummyBusManager.isCalledNavigateToPromotionsCategory = false
        let categoryId = CategoryType.activities.rawValue
        let title = ""
        
        // when
        sut.didacticAreaPresenterOfferButtonPressed(DidacticAreaPresenter(), categoryId: categoryId, andTitle: title)
        
        // then
        XCTAssert(dummyBusManager.isCalledNavigateToPromotionsCategory == false)
        
    }
    
    func test_givenCategoryIdAndTitle_whenICallDidacticAreaPresenterOfferButtonPressed_thenICallBusManagerNavigateToPromotionsCategoryScreenWithPromotionsTransport() {
        
        // given
        dummyBusManager.promotionsTransportSent = nil
        let categoryId = CategoryType.activities.rawValue
        let categoryType = CategoryType(rawValue: categoryId)
        let title = "title1"
        
        // when
        sut.didacticAreaPresenterOfferButtonPressed(DidacticAreaPresenter(), categoryId: categoryId, andTitle: title)
        
        // then
        XCTAssert(dummyBusManager.promotionsTransportSent?.category?.id == categoryType)
        XCTAssert(dummyBusManager.promotionsTransportSent?.category?.name == title)
        XCTAssert(dummyBusManager.promotionsTransportSent?.promotionName == title)
    }
    
    func test_givenURLAndTitle_whenICallDidacticAreaPresenterSpecialOfferButtonPressed_thenICallBusManagerNavigateToWebpage() {
        
        // given
        dummyBusManager.isCalledNavigateToWebpage = false
        let url = URL(string: "www.bancomer.com")
        let title = "title1"
        
        // when
        sut.didacticAreaPresenterSpecialOfferButtonPressed(DidacticAreaPresenter(), url!, andTitle: title)
        
        // then
        XCTAssert(dummyBusManager.isCalledNavigateToWebpage == true)
    }
    
    func test_givenURLAndTitleEmpty_whenICallDidacticAreaPresenterSpecialOfferButtonPressed_thenICallBusManagerNavigateToWebpage() {
        
        // given
        dummyBusManager.isCalledNavigateToWebpage = false
        let url = URL(string: "www.bancomer.com")
        let title = ""
        
        // when
        sut.didacticAreaPresenterSpecialOfferButtonPressed(DidacticAreaPresenter(), url!, andTitle: title)
        
        // then
        XCTAssert(dummyBusManager.isCalledNavigateToWebpage == false)
    }
    
    func test_givenURLAndTitle_whenICallDidacticAreaPresenterSpecialOfferButtonPressed_thenICallBusManagerNavigateToWebpageWithWebTransport() {
        
        // given
        let url = URL(string: "www.bancomer.com")
        let title = "title1"
        
        // when
        sut.didacticAreaPresenterSpecialOfferButtonPressed(DidacticAreaPresenter(), url!, andTitle: title)
        
        // then
        XCTAssert(dummyBusManager.webTransportSent?.webPage == url?.absoluteString)
        XCTAssert(dummyBusManager.webTransportSent?.webTitle == title)
    }

    // MARK: - TrendingPromotionPresenterDelegate trendingPromotionViewPressed

    func test_givenAny_whenTrendingPromotionViewPressedWithAPromotion_thenCallNavigateToDetailPromotion() {

        //given
        dummyBusManager.isCalledNavigateToShoppingDetail = false

        //when
        sut.trendingPromotionViewPressed(TrendingPromotionPresenter(), promotion: PromotionsGenerator.getPromotionsBO().promotions[0])

        //then
        XCTAssert(dummyBusManager.isCalledNavigateToShoppingDetail == true)

    }

    func test_givenAny_whenTrendingPromotionViewPressedWithAPromotion_thenCallNavigateToDetailPromotionWithTransportPromotionsBOFilled() {

        //given
        dummyBusManager.isCalledNavigateToShoppingDetail = false
        dummyBusManager.promotionDetailTransportSent = nil

        let promotionBO = PromotionsGenerator.getPromotionsBO().promotions[0]

        //when
        sut.trendingPromotionViewPressed(TrendingPromotionPresenter(), promotion: promotionBO)

        //then
        XCTAssert(dummyBusManager.promotionDetailTransportSent != nil)
        XCTAssert(dummyBusManager.promotionDetailTransportSent!.promotionBO == promotionBO)

    }

    // MARK: - ViewDidLoad

    func test_givenIsFirstLoginAndNotificationsAuthorizationIsNotDetermined_whenICallViewDidLoad_thenICallBusManagerNavigateToNotificationsInformationView() {

        //given
        dummyPreferencesManager.keysWithValues = [PreferencesManagerKeys.kLoginNumber.rawValue: NSNumber(value: 1) as AnyObject]

        let notificationsManager = DummyNotificationManager()
        notificationsManager.authorizationDetermined = false

        sut.notificationManager = notificationsManager

        //when
        sut.viewDidLoad()

        //then
        wait(for: [dummyBusManager.expectationNavigateScreen], timeout: 10)
        XCTAssert(dummyBusManager.isCalledNavigateToNotificationsInformationView == true)

    }
    
    func test_givenIsFirstLoginAndNotificationsAuthorizationIsDetermined_whenICallViewDidLoad_thenICallNotificationManagerIsAuthorizedStatus() {
        
        //given
        dummyPreferencesManager.keysWithValues = [PreferencesManagerKeys.kLoginNumber.rawValue: NSNumber(value: 1) as AnyObject]
        
        let notificationsManager = DummyNotificationManager()
        notificationsManager.authorizationDetermined = true
        sut.notificationManager = notificationsManager
        
        //when
        sut.viewDidLoad()
        
        //then
        wait(for: [dummyBusManager.expectationNavigateScreen], timeout: 10)
        XCTAssert(notificationsManager.isCalledIsAuthorizedStatus == true)
    }
    
    func test_givenIsFirstLoginAndNotificationsAuthorizationIsDetermined_whenICallViewDidLoad_thenICallBusManagerNavigateToNotificationsConfigurationView() {
        
        //given
        dummyPreferencesManager.keysWithValues = [PreferencesManagerKeys.kLoginNumber.rawValue: NSNumber(value: 1) as AnyObject]
        
        let notificationsManager = DummyNotificationManager()
        notificationsManager.authorizationDetermined = true
        sut.notificationManager = notificationsManager
        
        //when
        sut.viewDidLoad()
        
        //then
        wait(for: [dummyBusManager.expectationNavigateScreen], timeout: 10)
        XCTAssert(dummyBusManager.isCalledNavigateToNotificationsConfigurationView == true)
    }
    
    func test_givenIsFirstLoginAndNotificationsAuthorizationIsDeterminedAndAuthorizedFalse_whenICallViewDidLoad_thenICallBusManagerPublishDataWithRightData() {
        
        //given
        dummyPreferencesManager.keysWithValues = [PreferencesManagerKeys.kLoginNumber.rawValue: NSNumber(value: 1) as AnyObject]
        
        let notificationsManager = DummyNotificationManager()
        notificationsManager.authorizationDetermined = true
        notificationsManager.notificationsIsAuthorized = [false]
        sut.notificationManager = notificationsManager
        
        //when
        sut.viewDidLoad()
        
        //then
        wait(for: [dummyBusManager.expectationPublishData], timeout: 10)
        XCTAssert(dummyBusManager.isCalledPublishParamsConfigureDevicePermissions == true)
        XCTAssert(dummyBusManager.paramsConfigureNotificationsSent?.isPermissionAccepted == false)
    }
    
    func test_givenIsFirstLoginAndNotificationsAuthorizationIsDeterminedAndAuthorizedTrue_whenICallViewDidLoad_thenICallBusManagerPublishDataWithRightData() {
        
        //given
        dummyPreferencesManager.keysWithValues = [PreferencesManagerKeys.kLoginNumber.rawValue: NSNumber(value: 1) as AnyObject]
        
        let notificationsManager = DummyNotificationManager()
        notificationsManager.authorizationDetermined = true
        notificationsManager.notificationsIsAuthorized = [true]
        sut.notificationManager = notificationsManager
        
        //when
        sut.viewDidLoad()
        
        //then
        wait(for: [dummyBusManager.expectationPublishData], timeout: 10)
        XCTAssert(dummyBusManager.isCalledPublishParamsConfigureDevicePermissions == true)
        XCTAssert(dummyBusManager.paramsConfigureNotificationsSent?.isPermissionAccepted == true)
    }

    func test_givenIsLessThanFourLoginAndUserHasNotPressedUnderstandAdviceAndNotificationsAuthorizationIsNotDetermined_whenICallViewDidLoad_thenICallBusNavigateToNotificationsInformationView() {

        //given
        dummyPreferencesManager.keysWithValues = [PreferencesManagerKeys.kLoginNumber.rawValue: NSNumber(value: 2) as AnyObject, PreferencesManagerKeys.kUserCloseNotificationsAdvise.rawValue: true as AnyObject]
        
        let notificationsManager = DummyNotificationManager()
        notificationsManager.authorizationDetermined = false

        sut.notificationManager = notificationsManager

        //when
        sut.viewDidLoad()

        //then
        wait(for: [dummyBusManager.expectationNavigateScreen], timeout: 10)
        XCTAssert(dummyBusManager.isCalledNavigateToNotificationsInformationView == true)
    }

    func test_givenIsMoreThanThreeLoginAndUserWasNotSuccessRegisterNotifications_whenICallViewDidLoad_thenIsCallViewRegisterNotificationsToken() {

        //given
        dummyPreferencesManager.keysWithValues = [PreferencesManagerKeys.kLoginNumber.rawValue: NSNumber(value: 4) as AnyObject, PreferencesManagerKeys.kApnsToken.rawValue: "1111" as AnyObject]

        //when
        sut.viewDidLoad()

        //then
        XCTAssert(dummyView.isCalledRegisterNotificationsToken == true)
    }

     func test_givenIsMoreThanThreeLoginAndUserWasNotSuccessRegisterNotifications_whenICallViewDidLoad_thenIsCallViewRegisterNotificationsTokenwithCorrectToken() {

        //given
        dummyPreferencesManager.keysWithValues = [PreferencesManagerKeys.kLoginNumber.rawValue: NSNumber(value: 4) as AnyObject, PreferencesManagerKeys.kApnsToken.rawValue: "1111" as AnyObject]

        //when
        sut.viewDidLoad()

        //then
        XCTAssert(dummyView.tokenRegistered == "1111")
    }
    
    func test_givenPublicConfigHasRateAppFalse_whenICallViewDidLoad_thenINotCallViewShowRateApp() {
        
        // given
        sut.publicConfiguration = PublicConfigurationManager.sharedInstance().localPublicConfiguration()
        sut.publicConfiguration.publicConfig?.hasRateApp = false
        
        // when
        sut.viewDidLoad()
        
        // then
        XCTAssert(dummyView.isCalledShowRateApp == false)
    }
    
    func test_givenPublicConfigHasRateAppTrueAndFiveLoginInIOSLess10_3_AndAppRatedTrueAndUserSkipRateAppFalse_whenICallViewDidLoad_thenINotCallViewShowRateApp() {
        
        // given
        sut.publicConfiguration = PublicConfigurationManager.sharedInstance().localPublicConfiguration()
        sut.publicConfiguration.publicConfig?.hasRateApp = true
        
        dummyPreferencesManager.keysWithValues = [PreferencesManagerKeys.kNumberLoginsRateApp.rawValue: NSNumber(value: 5) as AnyObject, PreferencesManagerKeys.kAppRated.rawValue: true as AnyObject, PreferencesManagerKeys.kUserSkipRateApp.rawValue: false as AnyObject]
        
        // when
        sut.viewDidLoad()
        
        // then
        if #available(iOS 10.3, *) {
            
            //then
            XCTAssert(dummyView.isCalledShowRateApp == true)
        } else {
            
            //then
            XCTAssert(dummyView.isCalledShowRateApp == false)
        }
    }
    
    func test_givenPublicConfigHasRateAppTrueAndFiveLoginInIOSLess10_3_AndAppRatedFalseAndUserSkipRateAppTrue_whenICallViewDidLoad_thenINotCallViewShowRateApp() {
        
        // given
        sut.publicConfiguration = PublicConfigurationManager.sharedInstance().localPublicConfiguration()
        sut.publicConfiguration.publicConfig?.hasRateApp = true
        
        dummyPreferencesManager.keysWithValues = [PreferencesManagerKeys.kNumberLoginsRateApp.rawValue: NSNumber(value: 5) as AnyObject, PreferencesManagerKeys.kAppRated.rawValue: false as AnyObject, PreferencesManagerKeys.kUserSkipRateApp.rawValue: true as AnyObject]
        
        // when
        sut.viewDidLoad()
        
        // then
        if #available(iOS 10.3, *) {
            
            //then
            XCTAssert(dummyView.isCalledShowRateApp == true)
        } else {
            
            //then
            XCTAssert(dummyView.isCalledShowRateApp == false)
        }
    }

    func test_givenPublicConfigHasRateAppTrueAndFiveLoginInIOSLess10_3_whenICallViewDidLoad_thenICallViewShowRateApp() {

        //given
        sut.publicConfiguration = PublicConfigurationManager.sharedInstance().localPublicConfiguration()
        sut.publicConfiguration.publicConfig?.hasRateApp = true
        
        dummyPreferencesManager.keysWithValues = [PreferencesManagerKeys.kNumberLoginsRateApp.rawValue: NSNumber(value: 5) as AnyObject]

        //when
        sut.viewDidLoad()

        if #available(iOS 10.3, *) {

            //then
            XCTAssert(dummyView.isCalledShowRateApp == true)
        } else {

            //then
            XCTAssert(dummyView.isCalledShowRateApp == true)
        }
    }

    func test_givenPublicConfigHasRateAppTrueAndLessFiveLoginInIOSLess10_3_whenICallViewDidLoad_thenINotCallViewShowRateApp() {

        //given
        sut.publicConfiguration = PublicConfigurationManager.sharedInstance().localPublicConfiguration()
        sut.publicConfiguration.publicConfig?.hasRateApp = true
        
        dummyPreferencesManager.keysWithValues = [PreferencesManagerKeys.kNumberLoginsRateApp.rawValue: NSNumber(value: 3) as AnyObject]

        //when
        sut.viewDidLoad()

        if #available(iOS 10.3, *) {

            //then
            XCTAssert(dummyView.isCalledShowRateApp == true)
        } else {

            //then
            XCTAssert(dummyView.isCalledShowRateApp == false)
        }
    }
    
    func test_givenPublicConfigHasRateAppTrueAndLoginMultipleOfFiveInIOSLess10_3_AndAppRatedFalseAndUserSkipRateAppFalse_whenICallViewDidLoad_thenINotCallViewShowRateApp() {
        
        // given
        sut.publicConfiguration = PublicConfigurationManager.sharedInstance().localPublicConfiguration()
        sut.publicConfiguration.publicConfig?.hasRateApp = true
        
        dummyPreferencesManager.keysWithValues = [PreferencesManagerKeys.kNumberLoginsRateApp.rawValue: NSNumber(value: 550) as AnyObject, PreferencesManagerKeys.kAppRated.rawValue: false as AnyObject, PreferencesManagerKeys.kUserSkipRateApp.rawValue: false as AnyObject]
        
        // when
        sut.viewDidLoad()
        
        // then
        if #available(iOS 10.3, *) {
            
            //then
            XCTAssert(dummyView.isCalledShowRateApp == true)
        } else {
            
            //then
            XCTAssert(dummyView.isCalledShowRateApp == true)
        }
    }

    // MARK: - DidSuccessRegisterForRemoteNotificationsWithDeviceToken
    
    func test_givenAny_whenICallDidSuccessRegisterForRemoteNotificationsWithDeviceToken_thenIsCallViewRegisterNotificationsToken() {

        //given

        //when
        sut.didSuccessRegisterForRemoteNotificationsWithDeviceToken("")

        //then
        XCTAssert(dummyView.isCalledRegisterNotificationsToken == true)
    }

    func test_givenAny_whenICallDidSuccessRegisterForRemoteNotificationsWithDeviceToken_thenIsCallViewRegisterNotificationsTokenWithCorrectToken() {

        //given

        //when
        sut.didSuccessRegisterForRemoteNotificationsWithDeviceToken("11111")

        //then
        XCTAssert(dummyView.tokenRegistered == "11111")
    }

    // MARK: - registerTokenSuccess

    func test_given_whenICallRegisterTokenSuccess_thenIWriteTokenInPreferencesManager() {

        //given

        //when
        sut.registerTokenSuccess(withToken: "2222")

        //then
        XCTAssert(dummyPreferencesManager.key == PreferencesManagerKeys.kRegisterDeviceToken.rawValue)
        XCTAssert(dummyPreferencesManager.value as! String == "2222")
    }
    
    // MARK: - rateAppDialogUserChoiceSkip
    
    func test_givenAny_whenICallRateAppDialogUserChoiceSkip_thenSaveValueTrueWithKeyUserSkipRateApp() {
        
        // given
        sut.publicConfiguration = PublicConfigurationManager.sharedInstance().localPublicConfiguration()
        
        // when
        sut.rateAppDialogUserChoiceSkip()
        
        // then
        XCTAssert(dummyPreferencesManager.key == PreferencesManagerKeys.kUserSkipRateApp.rawValue)
        XCTAssert(dummyPreferencesManager.value as! Bool == true)
    }
    
    // MARK: - rateAppDialogUserChoiceRate
    
    func test_givenPublicConfigurationWithoutURLUpdate_whenICallRateAppDialogUserChoiceRate_thenINotCallViewOpenURL() {
        
        // given
        sut.publicConfiguration = PublicConfigurationManager.sharedInstance().localPublicConfiguration()
        sut.publicConfiguration.publicConfig?.update?.iOS?.URL = nil
        
        // when
        sut.rateAppDialogUserChoiceRate()
        
        // then
        XCTAssert(dummyView.isCalledOpenURL == false)
    }
    
    func test_givenPublicConfiguration_whenICallRateAppDialogUserChoiceRate_thenICallViewOpenURL() {
        
        // given
        sut.publicConfiguration = PublicConfigurationManager.sharedInstance().localPublicConfiguration()

        // when
        sut.rateAppDialogUserChoiceRate()
        
        // then
        XCTAssert(dummyView.isCalledOpenURL == true)
    }
    
    func test_givenPublicConfiguration_whenICallRateAppDialogUserChoiceRate_thenICallViewOpenURLWithCorrectURL() {
        
        // given
        sut.publicConfiguration = PublicConfigurationManager.sharedInstance().localPublicConfiguration()
        
        // when
        sut.rateAppDialogUserChoiceRate()
        
        // then
        XCTAssert(dummyView.urlSent == sut.publicConfiguration.publicConfig?.update?.iOS?.URL)
    }
    
    func test_givenPublicConfiguration_whenICallRateAppDialogUserChoiceRate_thenSaveValueTrueWithKeyAppRated() {
        
        // given
        sut.publicConfiguration = PublicConfigurationManager.sharedInstance().localPublicConfiguration()
        
        // when
        sut.rateAppDialogUserChoiceRate()
        
        // then
        XCTAssert(dummyPreferencesManager.key == PreferencesManagerKeys.kAppRated.rawValue)
        XCTAssert(dummyPreferencesManager.value as! Bool == true)
    }

    // MARK: - Dummy Data

    class HomeViewDummy: HomeViewProtocol {

        var isCalledShowDidacticAreaDisplayData = false
        var isCalledCardListViewSetModel = false
        var displayDataDidacticArea: DidacticAreaDisplayData?
        var isCalledChangeNavigationBarColor = false
        var isCalledShowTrendingPromotion = false
        var scrollViewHeight: CGFloat!
        var contentSize: CGSize!
        var contentOffset: CGPoint!
        var offsetAnimation: CGFloat!
        var navigationBarColor: UIColor!
        var isCalledShowNavigationTitle = false
        var isCalledHideNavigationTitle = false
        var isCalledRegisterNotificationsToken = false
        var isCalledOpenURL = false

        var tokenRegistered: String?
        var cards: CardsBO?
        var currencyCode: String?
        var cardTypesToShowInHome: [PhysicalSupportType]?

        var isCalledShowError = false
        var modelBOSent: ModelBO?
        var urlSent = ""

        var isCalledShowRateApp = false

        func showNavigationTitle() {
            isCalledShowNavigationTitle = true
        }

        func hideNavigationTitle() {
            isCalledHideNavigationTitle = true
        }

        func showError(error: ModelBO) {
            isCalledShowError = true
            modelBOSent = error
        }

        func showTrendingPromotion() {
            isCalledShowTrendingPromotion = true
        }

        func changeColorEditTexts() {
        }

        func showDidacticAreaDisplayData(withDisplayData displayData: DidacticAreaDisplayData) {
            isCalledShowDidacticAreaDisplayData = true
            displayDataDidacticArea = displayData
        }

        func changeNavigationBarColor(withColor color: UIColor) {
            isCalledChangeNavigationBarColor = true
            navigationBarColor = color
        }

        func setCardModel(model: CardListModelProtocol) {
            isCalledCardListViewSetModel = true
            cards = model.cardBO
            currencyCode = model.currencyCode
            cardTypesToShowInHome = model.cardTypesToShow
        }

        func registerNotificationsToken(_ registerToken: String) {
            isCalledRegisterNotificationsToken = true
            tokenRegistered = registerToken
        }

        func showRateApp() {

            isCalledShowRateApp = true
        }
        
        func openURL(_ url: String) {
            
            urlSent = url
            isCalledOpenURL = true
        }
    }

    class DummyInteractor: HomeInteractorProtocol {

        var isCalledProvideCurrency = false
        var currencyCode = "USD"

        func provideCurrency() -> Observable<ModelBO> {

            isCalledProvideCurrency = true

            let currencyBO = Currencies.currency(forCode: currencyCode)

            return Observable <ModelBO>.just(currencyBO as ModelBO)
        }

    }

    class DummyBusManager: BusManager {

        var isCalledChangeTabToCards = false
        var isCalledChangeTabToPromotions = false
        var cardTransportSent: CardsTransport?
        var isCalledNavigateToDetailHelp = false
        var isCalledNavigateToShoppingDetail = false
        var isCalledNavigateToPromotionsCategory = false
        var isCalledNavigateToWebpage = false
        var isCalledNavigateToHelp = false
        var isCalledNavigateToNotificationsInformationView = false
        var isCalledNavigateToNotificationsConfigurationView = false
        var isCalledPublishParamsConfigureDevicePermissions = false
        var paramsConfigureNotificationsSent: ConfigureNotificationsTransport?
        var helpDetailTransportSent: HelpDetailTransport?
        var promotionsTransportSent: PromotionsTransport?
        var promotionDetailTransportSent: PromotionDetailTransport?
        var webTransportSent: WebTransport?

        let expectationNavigateScreen = XCTestExpectation(description: "NavigateScreen")
        let expectationPublishData = XCTestExpectation(description: "PublishData")

        override init() {
            
            super.init()
            routerFactory = RouterInitFactory()
        }

        override func navigateScreen<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {
            
            if tag == HomePageReaction.ROUTER_TAG && event === HomePageReaction.EVENT_CHANGE_TAB_TO_CARDS {
                isCalledChangeTabToCards = true
                cardTransportSent = (values as! CardsTransport)
            } else if tag == HomePageReaction.ROUTER_TAG && event === HomePageReaction.EVENT_CHANGE_TAB_TO_PROMOTIONS {
                isCalledChangeTabToPromotions = true
            } else if tag == HomePageReaction.ROUTER_TAG && event === HomePageReaction.EVENT_NAV_TO_DETAIL_HELP {
                isCalledNavigateToDetailHelp = true
                helpDetailTransportSent = (values as! HelpDetailTransport)
            } else if tag == HomePageReaction.ROUTER_TAG && event === HomePageReaction.EVENT_NAV_TO_HELP {
                isCalledNavigateToHelp = true
            } else if tag == HomePageReaction.ROUTER_TAG && event === HomePageReaction.EVENT_NAV_TO_PROMOTION_DETAIL_SCREEN {
                isCalledNavigateToShoppingDetail = true
                promotionDetailTransportSent = values as? PromotionDetailTransport
            } else if tag == HomePageReaction.ROUTER_TAG && event === HomePageReaction.EVENT_NAV_TO_PROMOTIONS_CATEGORY_SCREEN {
                isCalledNavigateToPromotionsCategory = true
                promotionsTransportSent = values as? PromotionsTransport
            } else if tag == HomePageReaction.ROUTER_TAG && event === HomePageReaction.EVENT_NAV_TO_WEBPAGE_SCREEN {
                isCalledNavigateToWebpage = true
                webTransportSent = values as? WebTransport
            } else if tag == HomePageReaction.ROUTER_TAG && event === HomePageReaction.EVENT_NAV_TO_ACTIVATE_DEVICE_PERMISSIONS_SCREEN {
                isCalledNavigateToNotificationsInformationView = true
            } else if tag == HomePageReaction.ROUTER_TAG && event === HomePageReaction.EVENT_NAV_TO_CONFIGURE_DEVICE_PERMISSIONS_SCREEN {
                isCalledNavigateToNotificationsConfigurationView = true
            }
            expectationNavigateScreen.fulfill()
        }
        
        override func publishData<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {
            
            if tag == HomePageReaction.ROUTER_TAG && event === HomePageReaction.PARAMS_CONFIGURE_DEVICE_PERMISSIONS {
                
                isCalledPublishParamsConfigureDevicePermissions = true
                paramsConfigureNotificationsSent = ConfigureNotificationsTransport.deserialize(from: values as? [String: Any])
            }
            expectationPublishData.fulfill()
        }
    }

    class DummyConfigPublicManager: ConfigPublicManager {

        var isCalledGetDidacticAreaToShow = false

        override func getDidacticAreaToShow() -> (didacticAreaBO: DidacticAreaBO?, didacticCategoryBO: DidacticCategoryBO?) {
            isCalledGetDidacticAreaToShow = true
            return super.getDidacticAreaToShow()
        }
    }
}
