//
//  PromotionsAccessMapPresenterTests.swift
//  shoppingapp
//
//  Created by jesus.martinez on 20/3/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class PromotionsAccessMapPresenterTests: XCTestCase {

    let sut = PromotionsAccessMapPresenter()

    // MARK: - startLoading

    func test_givenAny_whenICallStartLoading_thenIsLoadingShouldBeTrue() {

        // given
        sut.isLoading = false

        // when
        sut.startLoading()

        // then
        XCTAssert(sut.isLoading == true)

    }

    func test_givenIsLoadingFalse_whenICallStartLoading_thenICallViewShowLoading() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.isLoading = false

        // when
        sut.startLoading()

        // then
        XCTAssert(dummyView.isCalledShowLoading == true)

    }

    func test_givenIsLoadingTrue_whenICallStartLoading_thenIDoNotCallViewShowLoading() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.isLoading = true

        // when
        sut.startLoading()

        // then
        XCTAssert(dummyView.isCalledShowLoading == false)

    }

    // MARK: - finishLoading

    func test_givenAny_whenICallFinishLoading_thenIsLoadingShouldBeFalse() {

        // given
        sut.isLoading = true

        // when
        sut.finishLoading()

        // then
        XCTAssert(sut.isLoading == false)

    }

    func test_givenIsLoadingTrue_whenICallFinishLoading_thenICallViewHideLoading() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.isLoading = true

        // when
        sut.finishLoading()

        // then
        XCTAssert(dummyView.isCalledHideLoading == true)

    }

    func test_givenIsLoadingTrue_whenICallFinishLoading_thenIDoNotCallViewHideLoading() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.isLoading = false

        // when
        sut.finishLoading()

        // then
        XCTAssert(dummyView.isCalledHideLoading == false)

    }

    class DummyView: PromotionsAccessMapViewProtocol {

        var isCalledShowLoading = false
        var isCalledHideLoading = false

        func showLoading() {
            isCalledShowLoading = true
        }

        func hideLoading() {
            isCalledHideLoading = true
        }

    }

}
