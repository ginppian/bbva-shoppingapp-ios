//
//  ShoppingListPresenterATests.swift
//  shoppingapp
//
//  Created by jesus.martinez on 10/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest
import RxSwift
import CellsNativeComponents
import CoreLocation

#if TESTMX
    @testable import shoppingappMX
#endif

class ShoppingListPresenterATests: XCTestCase {

    var sut: ShoppingListPresenterA<DummyView>!
    var dummyBusManager: DummyBusManager!
    var dummyView = DummyView()
    var dummyInteractor = DummyInteractor()

    var dummyLocationManager = DummyLocationManager()

    override func setUp() {
        super.setUp()
        dummyBusManager = DummyBusManager()
        sut = ShoppingListPresenterA<DummyView>(busManager: dummyBusManager)
        sut.busManager = dummyBusManager

        dummyLocationManager = DummyLocationManager()
        sut.locationManager = dummyLocationManager

        dummyView = DummyView()
        sut.view = dummyView

        dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
    }

    // MARK: - viewDidAppear

    func test_givenACategoryBO_whenICallViewDidAppear_thenICallViewSendOmnitureWithCategory() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewDidAppear()

        // then
        XCTAssert(dummyView.isCalledSendScreenOmnitureWithCategoryName == true)

    }

    func test_givenACategoryBOWithEmptyName_whenICallViewDidAppear_thenINotCallViewSendOmnitureWithCategory() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]
        sut.categoryBO?.name = ""

        // when
        sut.viewDidAppear()

        // then
        XCTAssert(dummyView.isCalledSendScreenOmnitureWithCategoryName == false)

    }

    func test_givenACategoryBO_whenICallViewDidAppear_thenICallViewSendOmnitureWithCategoryIdOfCategoryBO() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewDidAppear()

        // then
        XCTAssert(dummyView.categoryNameForOmniture == sut.categoryBO?.name.diacriticInsensitiveAndLowerCase())

    }

    func test_givenATrendingType_whenICallViewDidAppear_thenICallViewSendOmnitureForTrending() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.trendingType = .hot

        // when
        sut.viewDidAppear()

        // then
        XCTAssert(dummyView.isCalledSendScreenOmnitureForTrending == true)

    }

    func test_givenNotTrendingId_whenICallViewDidAppear_thenICallLocationManagerStartLocationObserverAndCheckStatus() {

        // given
        sut.trendingType = nil

        // when
        sut.viewDidAppear()

        // then
        XCTAssert(dummyLocationManager.isCalledStartLocationObserverAndCheckStatus == true)
    }

    func test_givenTrendingId_whenICallViewDidAppear_thenIDoNotCallLocationManagerStartLocationObserverAndCheckStatus() {

        // given
        sut.trendingType = .hot

        // when
        sut.viewDidAppear()

        // then
        XCTAssert(dummyLocationManager.isCalledStartLocationObserverAndCheckStatus == false)
    }

    // MARK: - viewDidDisappear

    func test_givenNotTrendingId_whenICallViewDidDisappear_thenICallLocationManagerStopLocationObserver() {

        // given
        sut.trendingType = nil

        // when
        sut.viewDidAppear()

        // then
        XCTAssert(dummyLocationManager.isCalledStartLocationObserverAndCheckStatus == true)
    }

    func test_givenTrendingId_whenICallViewDidDisappear_thenIDoNotCallLocationManagerStopLocationObserver() {

        // given
        sut.trendingType = .hot

        // when
        sut.viewDidAppear()

        // then
        XCTAssert(dummyLocationManager.isCalledStopLocationObserver == false)
    }

    // MARK: - toastPressed

    func test_givenLastLocationErrorTypeGpsOff_whenICallToastPressed_thenICallBusManagerNavigateScreenWithEventLocationSettingHelp() {

        // given
        sut.lastLocationErrorType = .gpsOff

        // when
        sut.toastPressed()

        // then
        XCTAssert(dummyBusManager.isCalledNavigateToLocationSettingHelp == true)
    }

    func test_givenLastLocationErrorTypePermissionDenied_whenICallToastPressed_thenICallViewOpenAppSettings() {

        // given
        sut.lastLocationErrorType = .permissionDenied

        // when
        sut.toastPressed()

        // then
        XCTAssert(dummyView.isCalledOpenAppSettings == true)
    }

    // MARK: - viewLoaded

    func test_givenATitle_whenICallViewLoaded_thenCallShowNavTitle() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        sut.titleScreen = "title"

        // when
        sut.viewLoaded()

        //then
        XCTAssert(dummyView.isCallShowNavTitle == true)
        XCTAssert(dummyView.titleScreen == "title")

    }

    func test_givenAPromotions_whenICallViewLoaded_thenCreateDisplayItems() {

        // given
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        // when
        sut.viewLoaded()

        //then
        XCTAssert(!sut.displayItems.items.isEmpty)

    }

    func test_givenAPromotions_whenICallViewLoaded_thenCallShowPromotions() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        // when
        sut.viewLoaded()

        //then
        XCTAssert(dummyView.isCallShowPromotions == true)

    }

    // MARK: - viewLoaded not promotions but categoryBO

    func test_givenACategoriesButNotPromotions_whenICallViewLoaded_thenCallViewShowSkeleton() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        //then
        XCTAssert(dummyView.isCalledShowSkeleton == true)

    }

    func test_givenNotTrending_whenICallViewLoaded_thenCallLocationManagerGeLocation() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.trendingType = nil

        // when
        sut.viewLoaded()

        //then
        XCTAssert(dummyLocationManager.isCalledGetLocation == true)

    }

    func test_givenTrending_whenICallViewLoaded_thenIDoNotCallLocationManagerGeLocation() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.trendingType = .hot

        // when
        sut.viewLoaded()

        //then
        XCTAssert(dummyLocationManager.isCalledGetLocation == false)

    }

    // MARK: - viewLoaded LocationManager --> GetLocation success

    func test_givenACategoriesButNotPromotions_whenICallViewDidLoadedAndLocationManagerSuccess_thenLastLocationErrorTypeShouldBeNil() {

       // given
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(sut.lastLocationErrorType == nil)
    }

    func test_givenACategoriesButNotUserLocation_whenICallViewDidLoadedAndLocationManagerSuccess_thenUserLocationShouldNotBeNil() {

        // given
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(sut.userLocation != nil)
    }

    func test_givenACategoriesButNotUserLocation_whenICallViewDidLoadedAndLocationManagerSuccess_thenUserLocationShouldMatchWithReturned() {

        // given
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(sut.userLocation == GeoLocationBO(withLatitude: dummyLocationManager.location!.coordinate.latitude, withLongitude: dummyLocationManager.location!.coordinate.longitude))
    }

    func test_givenACategoriesPromotionsAndLocationErrorTypePermissionDenied_whenICallViewLoadedAndLocationManagerSuccess_thenICallViewShowToastUpdatedPromotions() {

        // given
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]
        sut.lastLocationErrorType = .permissionDenied
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowToastUpdatedPromotions == true)

    }

    func test_givenACategoriesPromotionsAndLocationErrorTypeGPSOff_whenICallViewLoadedAndLocationManagerSuccess_thenICallViewShowToastUpdatedPromotions() {

        // given
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]
        sut.lastLocationErrorType = .gpsOff
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowToastUpdatedPromotions == true)

    }

    func test_givenACategoriesPromotionsAndLocationErrorTypeFail_whenICallViewLoadedAndLocationManagerSuccess_thenICallViewShowToastUpdatedPromotions() {

        // given
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]
        sut.lastLocationErrorType = .gpsFail
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowToastUpdatedPromotions == true)

    }

    func test_givenACategoriesButUserLocationAndPromotions_whenICallViewLoadedAndLocationManagerSuccess_thenIDoNotCallProvidePromotionsWithCategory() {
        // given
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionByParams == false)

    }

    func test_givenACategoriesButNotUserLocation_whenICallViewLoadedAndLocationManagerSuccess_thenICallProvidePromotionsWithCategory() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionByParams == true)

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndLocationManagerSuccess_thenICallProvidePromotionsWithCategoryWithCorrectParamsWithDefaultLocation() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        let locationParamExpected = ShoppingParamLocation(geolocation: sut.userLocation!, distanceParam: ShoppingDistanceParam(distance: sut.defaultDistance, lengthType: sut.lengthType))

        XCTAssert(dummyInteractor.paramsSent == ShoppingParamsTransport(categoryId: sut.categoryBO!.id.rawValue, location: locationParamExpected))

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndLocationManagerSuccessAndIsASuccessfulResponse_thenICallHideSkeleton() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        // given
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(sut.view?.isCalledHideSkeleton == true)

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndLocationManagerSuccessAndIsASuccessfulResponse_thenPromotionsBOShouldBeFilled() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        // given
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(sut.promotionsBO != nil)

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndLocationManagerSuccessAndIsASuccessfulResponse_thenCallShowPromotions() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        //then
        XCTAssert(dummyView.isCallShowPromotions == true)

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndLocationManagerSuccessAndIsASuccessfulResponse_thenCreateDisplayItems() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)

        // when
        sut.viewLoaded()

        //then
        XCTAssert(!sut.displayItems.items.isEmpty)

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndLocationManagerSuccessAndNotContentResponse_thenIDotNotCallHideSkeleton() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.responseStatus = ConstantsHTTPCodes.NO_CONTENT
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(sut.view?.isCalledHideSkeleton == false)

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndLocationManagerSuccessAndNotContentResponse_thenICallShowContentView() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.responseStatus = ConstantsHTTPCodes.NO_CONTENT
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(sut.view?.isCalledShowNoContent == true)

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndLocationManagerSuccessAndIsAnErrorResponse_thenIDotNotCallHideSkeleton() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceError = true
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(sut.view?.isCalledHideSkeleton == false)

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndLocationManagerSuccessAndIsAnErrorResponse_thenErrorBOShouldBeFilled() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceError = true
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(sut.errorBO != nil)

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndLocationManagerSuccessAndIsAnErrorResponseDifferentThan403_thenICallViewShowEmptyTsec() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()

        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowEmptyTsec == true)

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndLocationManagerSuccessAndReturnErrorStatus403_thenINotCallViewShowEmptyTsec() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()

        let errorBO = ErrorBO()
        errorBO.status = ConstantsHTTPCodes.STATUS_403
        errorBO.code = ErrorCode.expiredSession.rawValue
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        // given
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        sut.interactor = dummyInteractor

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowEmptyTsec == false)

    }

    func test_givenAPromotionsBOWithoutNextPage_whenICallViewLoadedAndLocationManagerSuccess_thenICallViewUpdateForNoMoreContent() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()
        sut.promotionsBO?.pagination?.links?.next = nil
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == true)

    }

    func test_givenAPromotionsBOWithNextPageEmpty_whenICallViewLoadedAndLocationManagerSuccess_thenICallViewUpdateForNoMoreContent() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()
        sut.promotionsBO?.pagination?.links?.next = ""
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == true)

    }

    func test_givenAPromotionsBOWithNextPage_whenICallViewLoadedAndLocationManagerSuccess_thenICallViewUpdateForMoreContent() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForMoreContent == true)

    }

    func test_givenACategoriesButNotPromotionsWithoutPagination_whenICallViewLoadedAndLocationManagerSuccess_thenICallViewUpdateForNoMoreContent() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBOWithoutPagination()
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == true)

    }

    func test_givenACategoriesButNotPromotionsWithLinkNextEmpty_whenICallViewLoadedAndLocationManagerSuccess_thenICallViewUpdateForNoMoreContent() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        dummyInteractor.promotionsBO.pagination?.links?.next = ""
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == true)

    }

    func test_givenACategoriesButNotPromotionsWithLinkNextNil_whenICallViewLoadedAndLocationManagerSuccess_thenICallViewUpdateForNoMoreContent() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        dummyInteractor.promotionsBO.pagination?.links?.next = nil
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == true)

    }

    func test_givenACategoriesButNotPromotionsWithoutPagination_whenICallViewLoadedAndLocationManagerSuccess_thenINotCallViewUpdateForMoreContent() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBOWithoutPagination()
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForMoreContent == false)

    }

    func test_givenACategoriesButNotPromotionsWithLinkNextEmpty_whenICallViewLoadedAndLocationManagerSuccess_thenINotCallViewUpdateForMoreContent() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        dummyInteractor.promotionsBO.pagination?.links?.next = ""
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForMoreContent == false)

    }

    func test_givenACategoriesButNotPromotionsWithLinkNextNil_whenICallViewLoadedAndLocationManagerSuccess_thenINotCallViewUpdateForMoreContent() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        dummyInteractor.promotionsBO.pagination?.links?.next = nil
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForMoreContent == false)

    }

    func test_givenACategoriesButNotPromotionsWithLinkNext_whenICallViewLoadedAndLocationManagerSuccess_thenICallViewUpdateForMoreContent() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForMoreContent == true)

    }

    func test_givenACategoriesButNotPromotionsWithPaginationAndLinkNext_whenICallViewLoadedAndLocationManagerSuccess_thenIDoNotCallViewUpdateForNoMoreContent() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.location = CLLocation(latitude: 3.0, longitude: 4.0)
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == false)

    }

    // MARK: - viewLoaded LocationManager --> Fail

    func test_givenACategoriesButNotPromotions_whenICallViewDidLoadedAndLocationManagerFail_thenLastLocationErrorTypeShouldBeFail() {

        // given
        dummyLocationManager.locationErrorType = .gpsFail
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(sut.lastLocationErrorType == .gpsFail)
    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndLocationManagerFail_thenICallProvidePromotionsWithCategory() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.locationErrorType = .gpsFail
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionByParams == true)

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndLocationManagerFail_thenICallProvidePromotionsWithCategoryWithCorrectParamsWithDefaultLocation() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given

        dummyLocationManager.locationErrorType = .gpsFail
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        let locationParamExpected = ShoppingParamLocation(geolocation: sut.defaultLocation, distanceParam: ShoppingDistanceParam(distance: sut.defaultDistance, lengthType: sut.lengthType))

        XCTAssert(dummyInteractor.paramsSent == ShoppingParamsTransport(categoryId: sut.categoryBO!.id.rawValue, location: locationParamExpected))

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndLocationManagerFailAndIsASuccessfulResponse_thenICallHideSkeleton() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        // given

        dummyLocationManager.locationErrorType = .gpsFail
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(sut.view?.isCalledHideSkeleton == true)

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndLocationManagerFailAndIsASuccessfulResponse_thenPromotionsBOShouldBeFilled() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        // given

        dummyLocationManager.locationErrorType = .gpsFail
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(sut.promotionsBO != nil)

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndLocationManagerFailAndIsASuccessfulResponse_thenCallShowPromotions() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.locationErrorType = .gpsFail
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        //then
        XCTAssert(dummyView.isCallShowPromotions == true)

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndLocationManagerFailAndIsASuccessfulResponse_thenCreateDisplayItems() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()
        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewLoaded()

        //then
        XCTAssert(!sut.displayItems.items.isEmpty)

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndLocationManagerFailAndNotContentResponse_thenIDotNotCallHideSkeleton() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.responseStatus = ConstantsHTTPCodes.NO_CONTENT
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.locationErrorType = .gpsFail
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(sut.view?.isCalledHideSkeleton == false)

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndLocationManagerFailAndNotContentResponse_thenICallShowContentView() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.responseStatus = ConstantsHTTPCodes.NO_CONTENT
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.locationErrorType = .gpsFail
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(sut.view?.isCalledShowNoContent == true)

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndLocationManagerFailAndIsAnErrorResponse_thenIDotNotCallHideSkeleton() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceError = true
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.locationErrorType = .gpsFail
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(sut.view?.isCalledHideSkeleton == false)

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndLocationManagerFailAndIsAnErrorResponse_thenErrorBOShouldBeFilled() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceError = true
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.locationErrorType = .gpsFail
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(sut.errorBO != nil)

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndLocationManagerFailAndIsAnErrorResponseDifferentThan403_thenICallViewShowEmptyTsec() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()

        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.locationErrorType = .gpsFail
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowEmptyTsec == true)

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndLocationManagerFailAndReturnErrorStatus403_thenINotCallViewShowEmptyTsec() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()

        let errorBO = ErrorBO()
        errorBO.status = ConstantsHTTPCodes.STATUS_403
        errorBO.code = ErrorCode.expiredSession.rawValue
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        // given
        dummyLocationManager.locationErrorType = .gpsFail
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        sut.interactor = dummyInteractor

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowEmptyTsec == false)

    }

    func test_givenAPromotionsBOWithoutNextPage_whenICallViewLoadedAndLocationManagerFail_thenICallViewUpdateForNoMoreContent() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()
        sut.promotionsBO?.pagination?.links?.next = nil
        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == true)

    }

    func test_givenAPromotionsBOWithNextPageEmpty_whenICallViewLoadedAndLocationManagerFail_thenICallViewUpdateForNoMoreContent() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()
        sut.promotionsBO?.pagination?.links?.next = ""
        dummyLocationManager.locationErrorType = .gpsFail

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == true)

    }

    func test_givenAPromotionsBOWithNextPage_whenICallViewLoadedAndLocationManagerFail_thenICallViewUpdateForMoreContent() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        dummyLocationManager.locationErrorType = .gpsFail
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForMoreContent == true)

    }

    func test_givenACategoriesButNotPromotionsWithoutPagination_whenICallViewLoadedAndLocationManagerFail_thenICallViewUpdateForNoMoreContent() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBOWithoutPagination()
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.locationErrorType = .gpsFail
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == true)

    }

    func test_givenACategoriesButNotPromotionsWithLinkNextEmpty_whenICallViewLoadedAndLocationManagerFail_thenICallViewUpdateForNoMoreContent() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        dummyInteractor.promotionsBO.pagination?.links?.next = ""
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.locationErrorType = .gpsFail
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == true)

    }

    func test_givenACategoriesButNotPromotionsWithLinkNextNil_whenICallViewLoadedAndLocationManagerFail_thenICallViewUpdateForNoMoreContent() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        dummyInteractor.promotionsBO.pagination?.links?.next = nil
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.locationErrorType = .gpsFail
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == true)

    }

    func test_givenACategoriesButNotPromotionsWithoutPagination_whenICallViewLoadedAndLocationManagerFail_thenINotCallViewUpdateForMoreContent() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBOWithoutPagination()
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.locationErrorType = .gpsFail
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForMoreContent == false)

    }

    func test_givenACategoriesButNotPromotionsWithLinkNextEmpty_whenICallViewLoadedAndLocationManagerFail_thenINotCallViewUpdateForMoreContent() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        dummyInteractor.promotionsBO.pagination?.links?.next = ""
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.locationErrorType = .gpsFail
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForMoreContent == false)

    }

    func test_givenACategoriesButNotPromotionsWithLinkNextNil_whenICallViewLoadedAndLocationManagerFail_thenINotCallViewUpdateForMoreContent() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        dummyInteractor.promotionsBO.pagination?.links?.next = nil
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.locationErrorType = .gpsFail
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForMoreContent == false)

    }

    func test_givenACategoriesButNotPromotionsWithLinkNext_whenICallViewLoadedAndLocationManagerFail_thenICallViewUpdateForMoreContent() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.locationErrorType = .gpsFail
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForMoreContent == true)

    }

    func test_givenACategoriesButNotPromotionsWithPaginationAndLinkNext_whenICallViewLoadedAndLocationManagerFail_thenIDoNotCallViewUpdateForNoMoreContent() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.locationErrorType = .gpsFail
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == false)

    }

    // MARK: - viewLoaded LocationManager --> PermissionDenied

    func test_givenACategoriesButNotPromotions_whenICallViewDidLoadedAndLocationManagerPermissionDenied_thenICallViewShowInfoToastNotPermissions() {

        // given
        dummyLocationManager.locationErrorType = .permissionDenied
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowInfoToastNotPermissions == true)
    }

    func test_givenACategoriesButNotPromotions_whenICallViewDidLoadedAndLocationManagerPermissionDenied_thenLastLocationErrorTypeShouldBePermissionDenied() {

        // given
        dummyLocationManager.locationErrorType = .permissionDenied
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(sut.lastLocationErrorType == .permissionDenied)
    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndLocationManagerPermissionDenied_thenICallProvidePromotionsWithCategory() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.locationErrorType = .permissionDenied
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionByParams == true)

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndLocationManagerPermissionDenied_thenICallProvidePromotionsWithCategoryWithCorrectParamsWithDefaultLocation() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given

        dummyLocationManager.locationErrorType = .permissionDenied
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        let locationParamExpected = ShoppingParamLocation(geolocation: sut.defaultLocation, distanceParam: ShoppingDistanceParam(distance: sut.defaultDistance, lengthType: sut.lengthType))

        XCTAssert(dummyInteractor.paramsSent == ShoppingParamsTransport(categoryId: sut.categoryBO!.id.rawValue, location: locationParamExpected))

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndLocationManagerPermissionDeniedAndIsASuccessfulResponse_thenICallHideSkeleton() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        // given

        dummyLocationManager.locationErrorType = .permissionDenied
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(sut.view?.isCalledHideSkeleton == true)

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndLocationManagerPermissionDeniedAndIsASuccessfulResponse_thenPromotionsBOShouldBeFilled() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        // given

        dummyLocationManager.locationErrorType = .permissionDenied
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(sut.promotionsBO != nil)

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndLocationManagerPermissionDeniedAndIsASuccessfulResponse_thenCallShowPromotions() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.locationErrorType = .permissionDenied
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        //then
        XCTAssert(dummyView.isCallShowPromotions == true)

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndLocationManagerPermissionDeniedAndIsASuccessfulResponse_thenCreateDisplayItems() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()
        dummyLocationManager.locationErrorType = .permissionDenied

        // when
        sut.viewLoaded()

        //then
        XCTAssert(!sut.displayItems.items.isEmpty)

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndLocationManagerPermissionDeniedAndNotContentResponse_thenIDotNotCallHideSkeleton() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.responseStatus = ConstantsHTTPCodes.NO_CONTENT
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.locationErrorType = .permissionDenied
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(sut.view?.isCalledHideSkeleton == false)

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndLocationManagerPermissionDeniedAndNotContentResponse_thenICallShowContentView() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.responseStatus = ConstantsHTTPCodes.NO_CONTENT
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.locationErrorType = .permissionDenied
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(sut.view?.isCalledShowNoContent == true)

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndLocationManagerPermissionDeniedAndIsAnErrorResponse_thenIDotNotCallHideSkeleton() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceError = true
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.locationErrorType = .permissionDenied
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(sut.view?.isCalledHideSkeleton == false)

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndLocationManagerPermissionDeniedAndIsAnErrorResponse_thenErrorBOShouldBeFilled() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceError = true
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.locationErrorType = .permissionDenied
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(sut.errorBO != nil)

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndLocationManagerPermissionDeniedAndIsAnErrorResponseDifferentThan403_thenICallViewShowEmptyTsec() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()

        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.locationErrorType = .permissionDenied
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowEmptyTsec == true)

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndLocationManagerPermissionDeniedAndReturnErrorStatus403_thenINotCallViewShowEmptyTsec() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()

        let errorBO = ErrorBO()
        errorBO.status = ConstantsHTTPCodes.STATUS_403
        errorBO.code = ErrorCode.expiredSession.rawValue
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        // given
        dummyLocationManager.locationErrorType = .permissionDenied
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        sut.interactor = dummyInteractor

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowEmptyTsec == false)

    }

    func test_givenAPromotionsBOWithoutNextPage_whenICallViewLoadedAndLocationManagerPermissionDenied_thenICallViewUpdateForNoMoreContent() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()
        sut.promotionsBO?.pagination?.links?.next = nil
        dummyLocationManager.locationErrorType = .permissionDenied

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == true)

    }

    func test_givenAPromotionsBOWithNextPageEmpty_whenICallViewLoadedAndLocationManagerPermissionDenied_thenICallViewUpdateForNoMoreContent() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()
        sut.promotionsBO?.pagination?.links?.next = ""
        dummyLocationManager.locationErrorType = .permissionDenied

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == true)

    }

    func test_givenAPromotionsBOWithNextPage_whenICallViewLoadedAndLocationManagerPermissionDenied_thenICallViewUpdateForMoreContent() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        dummyLocationManager.locationErrorType = .permissionDenied
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForMoreContent == true)

    }

    func test_givenACategoriesButNotPromotionsWithoutPagination_whenICallViewLoadedAndLocationManagerPermissionDenied_thenICallViewUpdateForNoMoreContent() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBOWithoutPagination()
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.locationErrorType = .permissionDenied
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == true)

    }

    func test_givenACategoriesButNotPromotionsWithLinkNextEmpty_whenICallViewLoadedAndLocationManagerPermissionDenied_thenICallViewUpdateForNoMoreContent() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        dummyInteractor.promotionsBO.pagination?.links?.next = ""
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.locationErrorType = .permissionDenied
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == true)

    }

    func test_givenACategoriesButNotPromotionsWithLinkNextNil_whenICallViewLoadedAndLocationManagerPermissionDenied_thenICallViewUpdateForNoMoreContent() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        dummyInteractor.promotionsBO.pagination?.links?.next = nil
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.locationErrorType = .permissionDenied
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == true)

    }

    func test_givenACategoriesButNotPromotionsWithoutPagination_whenICallViewLoadedAndLocationManagerPermissionDenied_thenINotCallViewUpdateForMoreContent() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBOWithoutPagination()
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.locationErrorType = .permissionDenied
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForMoreContent == false)

    }

    func test_givenACategoriesButNotPromotionsWithLinkNextEmpty_whenICallViewLoadedAndLocationManagerPermissionDenied_thenINotCallViewUpdateForMoreContent() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        dummyInteractor.promotionsBO.pagination?.links?.next = ""
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.locationErrorType = .permissionDenied
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForMoreContent == false)

    }

    func test_givenACategoriesButNotPromotionsWithLinkNextNil_whenICallViewLoadedAndLocationManagerPermissionDenied_thenINotCallViewUpdateForMoreContent() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        dummyInteractor.promotionsBO.pagination?.links?.next = nil
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.locationErrorType = .permissionDenied
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForMoreContent == false)

    }

    func test_givenACategoriesButNotPromotionsWithLinkNext_whenICallViewLoadedAndLocationManagerPermissionDenied_thenICallViewUpdateForMoreContent() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.locationErrorType = .permissionDenied
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForMoreContent == true)

    }

    func test_givenACategoriesButNotPromotionsWithPaginationAndLinkNext_whenICallViewLoadedAndLocationManagerPermissionDenied_thenIDoNotCallViewUpdateForNoMoreContent() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.locationErrorType = .permissionDenied
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == false)

    }

    // MARK: - viewLoaded LocationManager --> GPSOff

    func test_givenACategoriesButNotPromotions_whenICallViewDidLoadedAndLocationManagerGPSOff_thenICallViewShowInfoToastNotGPS() {

        // given
        dummyLocationManager.locationErrorType = .gpsOff
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowInfoToastNotGPS == true)
    }

    func test_givenACategoriesButNotPromotions_whenICallViewDidLoadedAndLocationManagerGPSOff_thenLastLocationErrorTypeShouldBeGPSOff() {

        // given
        dummyLocationManager.locationErrorType = .gpsOff
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(sut.lastLocationErrorType == .gpsOff)
    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndLocationManagerGPSOff_thenICallProvidePromotionsWithCategory() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.locationErrorType = .gpsOff
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionByParams == true)

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndLocationManagerGPSOff_thenICallProvidePromotionsWithCategoryWithCorrectParamsWithDefaultLocation() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given

        dummyLocationManager.locationErrorType = .gpsOff
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        let locationParamExpected = ShoppingParamLocation(geolocation: sut.defaultLocation, distanceParam: ShoppingDistanceParam(distance: sut.defaultDistance, lengthType: sut.lengthType))

        XCTAssert(dummyInteractor.paramsSent == ShoppingParamsTransport(categoryId: sut.categoryBO!.id.rawValue, location: locationParamExpected))

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndLocationManagerGPSOffAndIsASuccessfulResponse_thenICallHideSkeleton() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        // given

        dummyLocationManager.locationErrorType = .gpsOff
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(sut.view?.isCalledHideSkeleton == true)

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndLocationManagerGPSOffAndIsASuccessfulResponse_thenPromotionsBOShouldBeFilled() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        // given

        dummyLocationManager.locationErrorType = .gpsOff
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(sut.promotionsBO != nil)

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndLocationManagerGPSOffAndIsASuccessfulResponse_thenCallShowPromotions() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.locationErrorType = .gpsOff
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        //then
        XCTAssert(dummyView.isCallShowPromotions == true)

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndLocationManagerGPSOffAndIsASuccessfulResponse_thenCreateDisplayItems() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()
        dummyLocationManager.locationErrorType = .gpsOff

        // when
        sut.viewLoaded()

        //then
        XCTAssert(!sut.displayItems.items.isEmpty)

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndLocationManagerGPSOffAndNotContentResponse_thenIDotNotCallHideSkeleton() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.responseStatus = ConstantsHTTPCodes.NO_CONTENT
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.locationErrorType = .gpsOff
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(sut.view?.isCalledHideSkeleton == false)

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndLocationManagerGPSOffAndNotContentResponse_thenICallShowContentView() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.responseStatus = ConstantsHTTPCodes.NO_CONTENT
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.locationErrorType = .gpsOff
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(sut.view?.isCalledShowNoContent == true)

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndLocationManagerGPSOffAndIsAnErrorResponse_thenIDotNotCallHideSkeleton() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceError = true
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.locationErrorType = .gpsOff
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(sut.view?.isCalledHideSkeleton == false)

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndLocationManagerGPSOffAndIsAnErrorResponse_thenErrorBOShouldBeFilled() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceError = true
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.locationErrorType = .gpsOff
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(sut.errorBO != nil)

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndLocationManagerGPSOffAndIsAnErrorResponseDifferentThan403_thenICallViewShowEmptyTsec() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()

        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.locationErrorType = .gpsOff
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowEmptyTsec == true)

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndLocationManagerGPSOffAndReturnErrorStatus403_thenINotCallViewShowEmptyTsec() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()

        let errorBO = ErrorBO()
        errorBO.status = ConstantsHTTPCodes.STATUS_403
        errorBO.code = ErrorCode.expiredSession.rawValue
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        // given
        dummyLocationManager.locationErrorType = .gpsOff
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        sut.interactor = dummyInteractor

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowEmptyTsec == false)

    }

    func test_givenAPromotionsBOWithoutNextPage_whenICallViewLoadedAndLocationManagerGPSOff_thenICallViewUpdateForNoMoreContent() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()
        sut.promotionsBO?.pagination?.links?.next = nil
        dummyLocationManager.locationErrorType = .gpsOff

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == true)

    }

    func test_givenAPromotionsBOWithNextPageEmpty_whenICallViewLoadedAndLocationManagerGPSOff_thenICallViewUpdateForNoMoreContent() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()
        sut.promotionsBO?.pagination?.links?.next = ""
        dummyLocationManager.locationErrorType = .gpsOff

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == true)

    }

    func test_givenAPromotionsBOWithNextPage_whenICallViewLoadedAndLocationManagerGPSOff_thenICallViewUpdateForMoreContent() {

        let dummyView = DummyView()
        sut.view = dummyView

        // given
        dummyLocationManager.locationErrorType = .gpsOff
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForMoreContent == true)

    }

    func test_givenACategoriesButNotPromotionsWithoutPagination_whenICallViewLoadedAndLocationManagerGPSOff_thenICallViewUpdateForNoMoreContent() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBOWithoutPagination()
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.locationErrorType = .gpsOff
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == true)

    }

    func test_givenACategoriesButNotPromotionsWithLinkNextEmpty_whenICallViewLoadedAndLocationManagerGPSOff_thenICallViewUpdateForNoMoreContent() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        dummyInteractor.promotionsBO.pagination?.links?.next = ""
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.locationErrorType = .gpsOff
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == true)

    }

    func test_givenACategoriesButNotPromotionsWithLinkNextNil_whenICallViewLoadedAndLocationManagerGPSOff_thenICallViewUpdateForNoMoreContent() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        dummyInteractor.promotionsBO.pagination?.links?.next = nil
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.locationErrorType = .gpsOff
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == true)

    }

    func test_givenACategoriesButNotPromotionsWithoutPagination_whenICallViewLoadedAndLocationManagerGPSOff_thenINotCallViewUpdateForMoreContent() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBOWithoutPagination()
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.locationErrorType = .gpsOff
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForMoreContent == false)

    }

    func test_givenACategoriesButNotPromotionsWithLinkNextEmpty_whenICallViewLoadedAndLocationManagerGPSOff_thenINotCallViewUpdateForMoreContent() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        dummyInteractor.promotionsBO.pagination?.links?.next = ""
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.locationErrorType = .gpsOff
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForMoreContent == false)

    }

    func test_givenACategoriesButNotPromotionsWithLinkNextNil_whenICallViewLoadedAndLocationManagerGPSOff_thenINotCallViewUpdateForMoreContent() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        dummyInteractor.promotionsBO.pagination?.links?.next = nil
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.locationErrorType = .gpsOff
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForMoreContent == false)

    }

    func test_givenACategoriesButNotPromotionsWithLinkNext_whenICallViewLoadedAndLocationManagerGPSOff_thenICallViewUpdateForMoreContent() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.locationErrorType = .gpsOff
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForMoreContent == true)

    }

    func test_givenACategoriesButNotPromotionsWithPaginationAndLinkNext_whenICallViewLoadedAndLocationManagerGPSOff_thenIDoNotCallViewUpdateForNoMoreContent() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        sut.interactor = dummyInteractor

        // given
        dummyLocationManager.locationErrorType = .gpsOff
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == false)

    }

    // MARK: - retryButtonPressed

    func test_givenACategoriesButNotPromotions_whenICallRetryButtonPressed_thenCallViewShowSkeleton() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.retryButtonPressed()

        //then
        XCTAssert(dummyView.isCalledShowSkeleton == true)

    }

    func test_givenACategoriesButNotPromotions_whenICallRetryButtonPressed_thenICallProvidePromotionsWithCategory() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionByParams == true)

    }

    func test_givenACategoriesButNotPromotions_whenICallRetryButtonPressed_thenICallProvidePromotionsWithCategoryWithCorrectParams() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.retryButtonPressed()

        // then
        let locationParamExpected = ShoppingParamLocation(geolocation: sut.defaultLocation, distanceParam: ShoppingDistanceParam(distance: sut.defaultDistance, lengthType: sut.lengthType))

        XCTAssert(dummyInteractor.paramsSent == ShoppingParamsTransport(categoryId: sut.categoryBO!.id.rawValue, location: locationParamExpected))
    }

    func test_givenACategoriesButNotPromotionsAndUserLocation_whenICallRetryButtonPressed_thenICallProvidePromotionsWithCategoryWithCorrectParams() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // given
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]
        sut.userLocation = GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0)

        // when
        sut.retryButtonPressed()

        // then
        let locationParamExpected = ShoppingParamLocation(geolocation: GeoLocationBO(withLatitude: 3.0, withLongitude: 4.0), distanceParam: ShoppingDistanceParam(distance: sut.defaultDistance, lengthType: sut.lengthType))

        XCTAssert(dummyInteractor.paramsSent == ShoppingParamsTransport(categoryId: sut.categoryBO!.id.rawValue, location: locationParamExpected))

    }

    func test_givenACategoriesButNotPromotions_whenICallRetryButtonPressedAndIsASuccessfulResponse_thenICallHideSkeleton() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(sut.view?.isCalledHideSkeleton == true)

    }

    func test_givenACategoriesButNotPromotions_whenICallRetryButtonPressedAndIsASuccessfulResponse_thenPromotionsBOShouldBeFilled() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(sut.promotionsBO != nil)

    }

    func test_givenACategoriesButNotPromotions_whenICallRetryButtonPressedAndIsASuccessfulResponse_thenCallShowPromotions() {

        // given
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.retryButtonPressed()

        //then
        XCTAssert(dummyView.isCallShowPromotions == true)

    }

    func test_givenACategoriesButNotPromotions_whenICallRetryButtonPressedAndIsASuccessfulResponse_thenCreateDisplayItems() {

        // given
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.retryButtonPressed()

        //then
        XCTAssert(!sut.displayItems.items.isEmpty)

    }

    func test_givenACategoriesButNotPromotions_whenICallRetryButtonPressedAndNotContentResponse_thenIDotNotCallHideSkeleton() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.responseStatus = ConstantsHTTPCodes.NO_CONTENT
        sut.interactor = dummyInteractor

        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(sut.view?.isCalledHideSkeleton == false)

    }

    func test_givenACategoriesButNotPromotions_whenICallRetryButtonPressedAndNotContentResponse_thenICallShowContentView() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.responseStatus = ConstantsHTTPCodes.NO_CONTENT
        sut.interactor = dummyInteractor

        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(sut.view?.isCalledShowNoContent == true)

    }

    func test_givenACategoriesButNotPromotions_whenICallRetryButtonPressedAndIsAnErrorResponse_thenIDotNotCallHideSkeleton() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceError = true
        sut.interactor = dummyInteractor

        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(sut.view?.isCalledHideSkeleton == false)

    }

    func test_givenACategoriesButNotPromotions_whenICallRetryButtonPressedAndIsAnErrorResponse_thenErrorBOShouldBeFilled() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceError = true
        sut.interactor = dummyInteractor

        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(sut.errorBO != nil)

    }

    func test_givenACategoriesButNotPromotions_whenICallRetryButtonPressedAndIsAnErrorResponseDifferentThan403_thenICallViewShowEmptyTsec() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()

        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        sut.interactor = dummyInteractor

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(dummyView.isCalledShowEmptyTsec == true)

    }

    func test_givenACategoriesButNotPromotions_whenICallRetryButtonPressedAndReturnErrorStatus403_thenINotCallViewShowEmptyTsec() {

        // given

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()

        let errorBO = ErrorBO()
        errorBO.status = ConstantsHTTPCodes.STATUS_403
        errorBO.code = ErrorCode.expiredSession.rawValue
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        sut.interactor = dummyInteractor

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(dummyView.isCalledShowEmptyTsec == false)

    }

    // MARK: - accessMapSelected

    func test_givenAny_whenICallAccessMapSelected_thenICallNavigateScreenWithEventSecondScreen() {

        // given

        // when
        sut.accessMapSelected()

        // then
        XCTAssert(dummyBusManager.isCalledNavigateToNextScreen == true)

    }

    // MARK: promotionCellSelected

    func test_givenADisplayItemPromotion_whenICallItemSelected_thenICallBusNavigateToDetailScreen() {

        // given
        let promotionsBO = PromotionsGenerator.getPromotionsBO()
        let displayItem = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionsBO.promotions[0], withTypeCell: .high)

        // when
        sut.itemSelected(withDisplayItem: displayItem)

        // then
        XCTAssert(dummyBusManager.isCalledNavigateToDetailScreen == true)
    }

    func test_givenADisplayItemPromotion_whenICallItemSelected_thenCallBusToNavigateDetailWithObject() {

        // given
        let promotionsBO = PromotionsGenerator.getPromotionsBO()
        let displayItem = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionsBO.promotions[0], withTypeCell: .high, withCategoryType: .activities)

        // when
        sut.itemSelected(withDisplayItem: displayItem)

        // then
        let promotionDetailTransport = dummyBusManager.dummyDetailValue

        XCTAssert(promotionDetailTransport.promotionBO === displayItem.promotionBO)
        XCTAssert(promotionDetailTransport.category === sut.categoryBO)
        XCTAssert(promotionDetailTransport.userLocation === sut.userLocation)
    }

    func test_givenADisplayItemPromotionWithoutPromotionBOAndNotTrending_whenICallItemSelected_thenIDoNotCallBusNavigateToDetailScreen() {

        // given
        let promotionsBO = PromotionsGenerator.getPromotionsBO()
        var displayItem = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionsBO.promotions[0], withTypeCell: .high)
        displayItem.promotionBO = nil

        // when
        sut.itemSelected(withDisplayItem: displayItem)

        // then
        XCTAssert(dummyBusManager.isCalledNavigateToDetailScreen == false)
    }

    func test_givenADisplayItemPromotionAndNotTrending_whenICallItemSelected_thenICallBusNavigateToDetailScreenCompletionNotNil() {

        // given
        let promotionsBO = PromotionsGenerator.getPromotionsBO()
        let displayItem = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionsBO.promotions[0], withTypeCell: .high)

        // when
        sut.itemSelected(withDisplayItem: displayItem)

        // then
        XCTAssert(dummyBusManager.dummyDetailValue.completion != nil)
    }

    func test_givenADisplayItemPromotionAndNotTrending_whenICallItemSelected_thenICallBusNavigateToDetailScreenCompletionShouldStartLocation() {

        // given
        let promotionsBO = PromotionsGenerator.getPromotionsBO()
        let displayItem = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionsBO.promotions[0], withTypeCell: .high)

        // when
        sut.itemSelected(withDisplayItem: displayItem)

        // then
        dummyBusManager.dummyDetailValue.completion?()
        XCTAssert(dummyLocationManager.isCalledStartLocationObserverAndCheckStatus == true)
    }

    func test_givenADisplayItemPromotionAndNotTrending_whenICallItemSelected_thenICallLocationManagerStopLocation() {

        // given
        let promotionsBO = PromotionsGenerator.getPromotionsBO()
        let displayItem = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionsBO.promotions[0], withTypeCell: .high)

        // when
        sut.itemSelected(withDisplayItem: displayItem)

        // then
        XCTAssert(dummyLocationManager.isCalledStopLocationObserver == true)
    }

    // MARK: - setModel

    func test_givenPromotionsTransportAWithoutUserLocation_whenICallSetModel_thenAllFieldsShouldMatch() {

        // given
        let promotionsTransport = PromotionsTransportA()
        promotionsTransport.promotionName = "title"
        promotionsTransport.category = PromotionsGenerator.getCategoryBO().categories[0]

        // when

        sut.setModel(model: promotionsTransport)

        // then
        XCTAssert(sut.promotionsBO == nil)
        XCTAssert(sut.titleScreen == "title")
        XCTAssert(sut.categoryBO === promotionsTransport.category)
        XCTAssert(sut.trendingType == nil)
        XCTAssert(sut.userLocation == promotionsTransport.userLocation)

    }

    func test_givenPromotionsTransportAWithUserLocation_whenICallSetModel_thenAllFieldsShouldMatch() {

        // given
        let promotionsTransport = PromotionsTransportA()
        promotionsTransport.promotionName = "title"
        promotionsTransport.trendingType = TrendingType.hot
        promotionsTransport.userLocation = GeoLocationBO(withLatitude: 4.4, withLongitude: 6.6)

        // when

        sut.setModel(model: promotionsTransport)

        // then
        XCTAssert(sut.promotionsBO == nil)
        XCTAssert(sut.titleScreen == "title")
        XCTAssert(sut.categoryBO === nil)
        XCTAssert(sut.trendingType == TrendingType.hot)
        XCTAssert(sut.userLocation == promotionsTransport.userLocation)

    }

    // MARK: - DummyData

    class DummyInteractor: ShoppingListInteractorProtocol {

        var responseStatus = 200

        var forceError = false

        var wasLoadingNextPage = true

        var presenter: ShoppingListPresenter<DummyView>?

        var errorBO: ErrorBO?

        var isCalledProvidePromotionByParams = false
        var isCalledProvidePromotionsWithNextPage = false

        var promotionsBO: PromotionsBO!

        var nextPageLink = "nextPageLink"
        var paramsSent: ShoppingParamsTransport?

        func providePromotions(byParams params: ShoppingParamsTransport) -> Observable<ModelBO> {
            isCalledProvidePromotionByParams = true

            if promotionsBO == nil {
                promotionsBO = PromotionsGenerator.getPromotionsBO()
            }

            paramsSent = params

            promotionsBO.status = responseStatus

            if !forceError {

                if let presenter = self.presenter {
                    self.wasLoadingNextPage = presenter.isLoadingNextPage
                }

                return Observable <ModelBO>.just(promotionsBO!)

            } else {

                if errorBO == nil {
                    errorBO = ErrorBO(error: ErrorEntity(message: "Error"))

                    errorBO!.isErrorShown = false
                }

                return Observable.error(ServiceError.GenericErrorBO(error: errorBO!))
            }
        }

        func providePromotions(withNextPage nextPage: String) -> Observable <ModelBO> {

            isCalledProvidePromotionsWithNextPage = true

            nextPageLink = nextPage

            if !forceError {

                if promotionsBO == nil {
                    promotionsBO = PromotionsGenerator.getPromotionsBO()
                }

                promotionsBO.status = responseStatus

                return Observable <ModelBO>.just(promotionsBO!)

            } else {

                if errorBO == nil {
                    errorBO = ErrorBO(error: ErrorEntity(message: "Error"))

                    errorBO!.isErrorShown = false
                }

                return Observable.error(ServiceError.GenericErrorBO(error: errorBO!))
            }
        }

    }

    class DummyView: ShoppingListViewProtocolA, ViewProtocol {

        var isCallShowNavTitle = false
        var isCallShowPromotions = false
        var isCalledShowEmptyTsec = false
        var isCalledShowNoContent = false
        var isCalledShowSkeleton = false
        var isCalledHideSkeleton = false
        var isCalledShowInfoToastNotGPS = false
        var isCalledHideToast = false
        var isCalledShowInfoToastNotPermissions = false
        var isCalledShowToastUpdatedPromotions = false

        var isCalledUpdateForNoMoreContent = false
        var isCalledUpdateForMoreContent = false
        var isCalledShowNextPageAnimation = false
        var isCalledHideNextPageAnimation = false
        var isShowErrorCalled = false
        var isCalledSendScreenOmnitureWithCategoryName = false
        var isCalledSendScreenOmnitureForTrending = false
        var isCalledOpenAppSettings = false

        var titleScreen = ""
        var categoryNameForOmniture = ""

        var diplayItems: DisplayItemsPromotions?

        func showError(error: ModelBO) {
            isShowErrorCalled = true
        }

        func changeColorEditTexts() {
        }

        func showNavTitle(withTitle title: String) {
            isCallShowNavTitle = true
            titleScreen = title
        }

        func showPromotions(withDisplayItemsPromotions displayItemPromotions: DisplayItemsPromotions) {
            isCallShowPromotions = true
            diplayItems = displayItemPromotions
        }

        func showErrorEmptyTsec() {
            isCalledShowEmptyTsec = true
        }

        func showNoContentView() {
            isCalledShowNoContent = true
        }

        func showSkeleton() {
            isCalledShowSkeleton = true
        }

        func hideSkeleton() {
            isCalledHideSkeleton = true
        }

        func updateForNoMoreContent() {
            isCalledUpdateForNoMoreContent = true
        }

        func updateForMoreContent() {
            isCalledUpdateForMoreContent = true
        }

        func showNextPageAnimation() {
            isCalledShowNextPageAnimation = true
        }

        func hideNextPageAnimation() {
            isCalledHideNextPageAnimation = true
        }

        func sendScreenOmniture(withCategoryName categoryName: String) {
            isCalledSendScreenOmnitureWithCategoryName = true
            categoryNameForOmniture = categoryName
        }

        func sendScreenOmnitureForTrending() {
            isCalledSendScreenOmnitureForTrending = true
        }

        func showInfoToastNotGPS() {
            isCalledShowInfoToastNotGPS = true
        }

        func showInfoToastNotPermissions() {
            isCalledShowInfoToastNotPermissions = true
        }

        func showToastUpdatedPromotions() {
            isCalledShowToastUpdatedPromotions = true
        }

        func hideToast() {
            isCalledHideToast = true
        }

        func openAppSettings() {
            isCalledOpenAppSettings = true
        }

    }

    class DummyBusManager: BusManager {

        var isCalledNavigateToLocationSettingHelp = false
        var isCalledNavigateToDetailScreen = false

        var isCalledNavigateToNextScreen = false
        var dummyValues = [String: AnyObject]()
        var dummyDetailValue = PromotionDetailTransport()

        override init() {
            super.init()
            routerFactory = RouterInitFactory()
        }

        override func navigateScreen<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {
            if tag == ShoppingListPageReactionA.ROUTER_TAG && event === ShoppingListPageReactionA.EVENT_NAV_TO_NEXT_SCREEN {
                isCalledNavigateToNextScreen = true
            }

            if tag == ShoppingListPageReactionA.ROUTER_TAG && event === ShoppingListPageReactionA.EVENT_NAV_TO_LOCATION_SETTING_HELP {
                isCalledNavigateToLocationSettingHelp = true
            }

            if tag == "shopping-list-tag" && event === ShoppingPageReaction.EVENT_NAV_TO_DETAIL_SCREEN {
                isCalledNavigateToDetailScreen = true
                dummyDetailValue = values as! PromotionDetailTransport
            }
        }

    }

}
