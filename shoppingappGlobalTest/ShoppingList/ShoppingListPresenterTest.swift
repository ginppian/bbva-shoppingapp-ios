//
//  BVATransactionsListPresenterTest.swift
//  shoppingapp
//
//  Created by jesus.martinez on 7/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import XCTest
import RxSwift
import CellsNativeComponents

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class ShoppingListPresenterTest: XCTestCase {

    var sut: ShoppingListPresenter<DummyView>!
    var dummyBusManager: DummyBusManager!

    override func setUp() {
        super.setUp()
        dummyBusManager = DummyBusManager()
        sut = ShoppingListPresenter<DummyView>(busManager: dummyBusManager)
    }

    func test_givenAPromotionsTransportWithCategoryBOAndTitle_whenICallOnMessage_thenDataShouldBeFilled() {

        // given
        let promotionsTransport = PromotionsTransport()
        promotionsTransport.promotionName = "title"
        promotionsTransport.category = PromotionsGenerator.getCategoryBO().categories[0]

        // when

        sut.setModel(model: promotionsTransport)

        // then
        XCTAssert(sut.promotionsBO == nil)
        XCTAssert(sut.titleScreen == "title")
        XCTAssert(sut.categoryBO === promotionsTransport.category)
        XCTAssert(sut.trendingType == nil)

    }

    func test_givenAPromotionsTransportWithPromotionsBOAndTitle_whenICallOnMessage_thenDataShouldBeFilled() {

        // given

        let promotionsTransport = PromotionsTransport()
        promotionsTransport.promotionsBO = PromotionsGenerator.getPromotionsBO()
        promotionsTransport.promotionName = "title"

        // when
        sut.setModel(model: promotionsTransport)

        // then
        XCTAssert(sut.promotionsBO != nil)
        XCTAssert(sut.titleScreen == "title")
        XCTAssert(sut.categoryBO == nil)
        XCTAssert(sut.trendingType == nil)

    }

    func test_givenAPromotionsTransportWithPromotionsBOAndTitleAndTrending_whenICallOnMessage_thenDataShouldBeFilled() {

        // given

        let promotionsTransport = PromotionsTransport()
        promotionsTransport.promotionsBO = PromotionsGenerator.getPromotionsBO()
        promotionsTransport.promotionName = "title"
        promotionsTransport.trendingType = .hot

        // when
        sut.setModel(model: promotionsTransport)

        // then
        XCTAssert(sut.promotionsBO != nil)
        XCTAssert(sut.titleScreen == "title")
        XCTAssert(sut.categoryBO == nil)
        XCTAssert(sut.trendingType == promotionsTransport.trendingType)

    }

    // MARK: - viewDidAppear

    func test_givenACategoryBO_whenICallViewDidAppear_thenICallViewSendOmnitureWithCategory() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewDidAppear()

        // then
        XCTAssert(dummyView.isCalledSendScreenOmnitureWithCategoryName == true)

    }

    func test_givenACategoryBOWithEmptyName_whenICallViewDidAppear_thenINotCallViewSendOmnitureWithCategory() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]
        sut.categoryBO?.name = ""

        // when
        sut.viewDidAppear()

        // then
        XCTAssert(dummyView.isCalledSendScreenOmnitureWithCategoryName == false)

    }

    func test_givenACategoryBO_whenICallViewDidAppear_thenICallViewSendOmnitureWithCategoryIdOfCategoryBO() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewDidAppear()

        // then
        XCTAssert(dummyView.categoryNameForOmniture == sut.categoryBO?.name.diacriticInsensitiveAndLowerCase())

    }

    func test_givenATrendingType_whenICallViewDidAppear_thenICallViewSendOmnitureForTrending() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.trendingType = .hot

        // when
        sut.viewDidAppear()

        // then
        XCTAssert(dummyView.isCalledSendScreenOmnitureForTrending == true)

    }
    
    // MARK: - viewWillAppearAfterDismiss
    
    func test_givenACategoryBO_whenICallViewWillAppearAfterDismiss_thenICallViewSendOmnitureWithCategory() {
        
        // given
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]
        
        // when
        sut.viewWillAppearAfterDismiss()
        
        // then
        XCTAssert(dummyView.isCalledSendScreenOmnitureWithCategoryName == true)
        
    }
    
    func test_givenACategoryBOWithEmptyName_whenICallViewWillAppearAfterDismiss_thenINotCallViewSendOmnitureWithCategory() {
        
        // given
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]
        sut.categoryBO?.name = ""
        
        // when
        sut.viewWillAppearAfterDismiss()
        
        // then
        XCTAssert(dummyView.isCalledSendScreenOmnitureWithCategoryName == false)
        
    }
    
    func test_givenACategoryBO_whenICallViewWillAppearAfterDismiss_thenICallViewSendOmnitureWithCategoryIdOfCategoryBO() {
        
        // given
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]
        
        // when
        sut.viewWillAppearAfterDismiss()
        
        // then
        XCTAssert(dummyView.categoryNameForOmniture == sut.categoryBO?.name.diacriticInsensitiveAndLowerCase())
        
    }
    
    func test_givenATrendingType_whenICallViewWillAppearAfterDismiss_thenICallViewSendOmnitureForTrending() {
        
        // given
        let dummyView = DummyView()
        sut.view = dummyView
        
        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor
        
        sut.trendingType = .hot
        
        // when
        sut.viewWillAppearAfterDismiss()
        
        // then
        XCTAssert(dummyView.isCalledSendScreenOmnitureForTrending == true)
        
    }

    // MARK: - viewLoaded

    func test_givenATitle_whenICallViewLoaded_thenCallShowNavTitle() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView
        sut.titleScreen = "title"

        // when
        sut.viewLoaded()

        //then
        XCTAssert(dummyView.isCallShowNavTitle == true)
        XCTAssert(dummyView.titleScreen == "title")

    }

    func test_givenAPromotions_whenICallViewLoaded_thenCreateDisplayItems() {

        // given
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        // when
        sut.viewLoaded()

        //then
        XCTAssert(!sut.displayItems.items.isEmpty)

    }

    func test_givenAPromotions_whenICallViewLoaded_thenCallShowPromotions() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        // when
        sut.viewLoaded()

        //then
        XCTAssert(dummyView.isCallShowPromotions == true)

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoaded_thenCallViewShowSkeleton() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        //then
        XCTAssert(dummyView.isCalledShowSkeleton == true)

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoaded_thenICallProvidePromotionsWithCategory() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionByParams == true)

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoaded_thenICallProvidePromotionsWithCategoryWithCorrectParams() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyInteractor.paramsSent == ShoppingParamsTransport(categoryId: sut.categoryBO!.id.rawValue))

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndIsASuccessfulResponse_thenICallHideSkeleton() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(sut.view?.isCalledHideSkeleton == true)

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndIsASuccessfulResponse_thenPromotionsBOShouldBeFilled() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(sut.promotionsBO != nil)

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndIsASuccessfulResponse_thenCallShowPromotions() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // when
        sut.viewLoaded()

        //then
        XCTAssert(dummyView.isCallShowPromotions == true)

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndIsASuccessfulResponse_thenCreateDisplayItems() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // when
        sut.viewLoaded()

        //then
        XCTAssert(!sut.displayItems.items.isEmpty)

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndNotContentResponse_thenIDotNotCallHideSkeleton() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.responseStatus = ConstantsHTTPCodes.NO_CONTENT
        sut.interactor = dummyInteractor

        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(sut.view?.isCalledHideSkeleton == false)

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndNotContentResponse_thenICallShowContentView() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.responseStatus = ConstantsHTTPCodes.NO_CONTENT
        sut.interactor = dummyInteractor

        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(sut.view?.isCalledShowNoContent == true)

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndIsAnErrorResponse_thenIDotNotCallHideSkeleton() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceError = true
        sut.interactor = dummyInteractor

        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(sut.view?.isCalledHideSkeleton == false)

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndIsAnErrorResponse_thenErrorBOShouldBeFilled() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceError = true
        sut.interactor = dummyInteractor

        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(sut.errorBO != nil)

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndIsAnErrorResponseDifferentThan403_thenICallViewShowEmptyTsec() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()

        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        sut.interactor = dummyInteractor

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowEmptyTsec == true)

    }

    func test_givenACategoriesButNotPromotions_whenICallViewLoadedAndReturnErrorStatus403_thenINotCallViewShowEmptyTsec() {

        // given

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()

        let errorBO = ErrorBO()
        errorBO.status = ConstantsHTTPCodes.STATUS_403
        errorBO.code = ErrorCode.expiredSession.rawValue
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        sut.interactor = dummyInteractor

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledShowEmptyTsec == false)

    }

    // MARK: Pagination

    func test_givenAPromotionsBOWithoutNextPage_whenICallViewLoaded_thenICallViewUpdateForNoMoreContent() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()
        sut.promotionsBO?.pagination?.links?.next = nil

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == true)

    }

    func test_givenAPromotionsBOWithNextPageEmpty_whenICallViewLoaded_thenICallViewUpdateForNoMoreContent() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()
        sut.promotionsBO?.pagination?.links?.next = ""

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == true)

    }

    func test_givenAPromotionsBOWithNextPage_whenICallViewLoaded_thenICallViewUpdateForMoreContent() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForMoreContent == true)

    }

    //-------------

    func test_givenACategoriesButNotPromotionsWithoutPagination_whenICallViewLoaded_thenICallViewUpdateForNoMoreContent() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBOWithoutPagination()
        sut.interactor = dummyInteractor

        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == true)

    }

    func test_givenACategoriesButNotPromotionsWithLinkNextEmpty_whenICallViewLoaded_thenICallViewUpdateForNoMoreContent() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        dummyInteractor.promotionsBO.pagination?.links?.next = ""
        sut.interactor = dummyInteractor

        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == true)

    }

    func test_givenACategoriesButNotPromotionsWithLinkNextNil_whenICallViewLoaded_thenICallViewUpdateForNoMoreContent() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        dummyInteractor.promotionsBO.pagination?.links?.next = nil
        sut.interactor = dummyInteractor

        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == true)

    }

    func test_givenACategoriesButNotPromotionsWithoutPagination_whenICallViewLoaded_thenINotCallViewUpdateForMoreContent() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBOWithoutPagination()
        sut.interactor = dummyInteractor

        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForMoreContent == false)

    }

    func test_givenACategoriesButNotPromotionsWithLinkNextEmpty_whenICallViewLoaded_thenINotCallViewUpdateForMoreContent() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        dummyInteractor.promotionsBO.pagination?.links?.next = ""
        sut.interactor = dummyInteractor

        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForMoreContent == false)

    }

    func test_givenACategoriesButNotPromotionsWithLinkNextNil_whenICallViewLoaded_thenINotCallViewUpdateForMoreContent() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        dummyInteractor.promotionsBO.pagination?.links?.next = nil
        sut.interactor = dummyInteractor

        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForMoreContent == false)

    }

    func test_givenACategoriesButNotPromotionsWithLinkNext_whenICallViewLoaded_thenICallViewUpdateForMoreContent() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        sut.interactor = dummyInteractor

        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForMoreContent == true)

    }

    func test_givenACategoriesButNotPromotionsWithPaginationAndLinkNext_whenICallViewLoaded_thenIDoNotCallViewUpdateForNoMoreContent() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        sut.interactor = dummyInteractor

        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.viewLoaded()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == false)

    }

    // MARK: Pagination - loadDataNextPage

    func test_givenIsLoadingNextPageTrue_whenICallLoadDataNextPage_thenIDoNotCallViewShowNextPageAnimation() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        sut.isLoadingNextPage = true

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyView.isCalledShowNextPageAnimation == false)

    }

    func test_givenIsLoadingNextPageFalseWithoutPromotions_whenICallLoadDataNextPage_thenIDoNotCallViewShowNextPageAnimation() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        sut.isLoadingNextPage = false

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyView.isCalledShowNextPageAnimation == false)

    }

    func test_givenIsLoadingNextPageFalseWithPromotionsWithoutPagination_whenICallLoadDataNextPage_thenIDoNotCallViewShowNextPageAnimation() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = false
        sut.promotionsBO = PromotionsGenerator.getPromotionsBOWithoutPagination()

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyView.isCalledShowNextPageAnimation == false)
    }

    func test_givenIsLoadingNextPageFalseWithPromotionsWithLinkNextEmpty_whenICallLoadDataNextPage_thenIDoNotCallViewShowNextPageAnimation() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = false
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()
        sut.promotionsBO?.pagination?.links?.next = ""

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyView.isCalledShowNextPageAnimation == false)
    }

    func test_givenIsLoadingNextPageFalseWithPromotionsWithPaginationNext_whenICallLoadDataNextPage_thenICallViewShowNextPageAnimation() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = false
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyView.isCalledShowNextPageAnimation == true)

    }

    //----------

    func test_givenIsLoadingNextPageTrue_whenICallLoadDataNextPage_thenIDoNotCallInteractorProvidePromotionsWithNextPage() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = true

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionsWithNextPage == false)

    }

    func test_givenIsLoadingNextPageFalseWithoutPromotions_whenICallLoadDataNextPage_thenIDoNotCallInteractorProvidePromotionsWithNextPage() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = false

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionsWithNextPage == false)

    }

    func test_givenIsLoadingNextPageFalseWithPromotionsWithStatusDifferentThan206_whenICallLoadDataNextPage_thenIDoNotCallInteractorProvidePromotionsWithNextPage() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = false
        sut.promotionsBO = PromotionsGenerator.getPromotionsBOWithoutPagination()

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionsWithNextPage == false)

    }

    func test_givenIsLoadingNextPageFalseWithPromotionsWithLinkNextEmpty_whenICallLoadDataNextPage_thenIDoNotCallInteractorProvidePromotionsWithNextPage() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = false
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()
        sut.promotionsBO?.pagination?.links?.next = ""

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionsWithNextPage == false)
    }

    func test_givenIsLoadingNextPageFalseWithPromotionsWithStatus206AndPaginationNext_whenICallLoadDataNextPage_thenICallInteractorProvidePromotionsForCategoryWithNextPage() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = false
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionsWithNextPage == true)
    }

    //----------

    func test_givenIsLoadingNextPageFalseWithPromotionsWithPaginationNext_whenICallLoadDataNextPage_thenIsLoadingNextPageShouldBeTrue() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.presenter = sut
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = false
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyInteractor.wasLoadingNextPage == true)

    }

    func test_givenIsLoadingNextPageFalseWithPromotionsWithPaginationNext_whenICallLoadDataNextPage_thenICallInteractorProvidePromotionsForCategoryWithNextPageWithTheNextPageOfPagination() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = false
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        let nextPageLink = sut.promotionsBO?.pagination?.links?.generateNextURL(isFinancial: false)

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyInteractor.nextPageLink == nextPageLink)
    }

    func test_givenIsLoadingNextPageFalseWithPromotionsWithPaginationNext_whenICallLoadDataNextPageAndReturnSuccess_thenIsLoadingNextPageShouldBeFalse() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = false
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(sut.isLoadingNextPage == false)
    }

    //----------

    func test_givenIsLoadingNextPageFalseWithPromotionsWithPaginationNext_whenICallLoadDataNextPageAndReturnSuccess_thenICallViewHideNextPageAnimation() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = false
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyView.isCalledHideNextPageAnimation == true)
    }

    func test_givenIsLoadingNextPageFalseWithPromotionsWithPaginationNext_whenwhenICallLoadDataNextPageAndReturnError_thenCallViewHideNextPageAnimation() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceError = true
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = false
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyView.isCalledHideNextPageAnimation == true)

    }

    //----------

    func test_givenIsLoadingNextPageFalseWithPromotionsWithPaginationNext_whenICallLoadDataNextPageAndReturnSuccess_thenPromotionsBOShouldMustWithPaginationReturned() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBOWithoutPagination()
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = false
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(sut.promotionsBO?.pagination === dummyInteractor.promotionsBO.pagination)
    }

    //----------

    func test_givenIsLoadingNextPageFalseWithPromotionsAndDisplayItemsWithPaginationNext_whenICallLoadDataNextPageAndReturnSuccess206_thenICallViewShowPromotionsWithTheNumberOfDisplayItemsPreviousPlusNewPromotions() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.responseStatus = ConstantsHTTPCodes.STATUS_OK
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = false
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        let displayItems = DisplayItemsPromotions()
        displayItems.items.append(DisplayItemPromotion.generateDisplayItemPromotionTrending(withTrendingPromotions: sut.promotionsBO!))

        sut.displayItems = displayItems

        let displayItemsCountBefore = sut.displayItems.items.count

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyView.diplayItems!.items.count == (displayItemsCountBefore + dummyInteractor.promotionsBO.promotions.count))
    }

    func test_givenIsLoadingNextPageFalseWithPromotionsWithPaginationNext_whenwhenICallLoadDataNextPageAndReturnError_thenErrorBOShouldMatchWithReturned() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceError = true
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = false
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(sut.errorBO === dummyInteractor.errorBO)

    }

    func test_givenIsLoadingNextPageFalseWithPromotionsWithPaginationNext_whenwhenICallLoadDataNextPageAndReturnError_thenCallViewShowError() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceError = true
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = false
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyView.isShowErrorCalled == true)

    }

    func test_givenIsLoadingNextPageFalseWithPromotionsWithPaginationNext_whenwhenICallLoadDataNextPageAndReturnError_thenIsLoadingNextPageShouldBeFalse() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceError = true
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = false
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(sut.isLoadingNextPage == false)
    }

    //----------

    func test_givenIsLoadingNextPageFalseWithPromotionsWithPaginationNext_whenICallLoadDataNextPageAndReturnSuccess200_thenICallViewShowPromotions() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = false
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyView.isCallShowPromotions == true)
    }

    func test_givenIsLoadingNextPageFalseWithPromotionsWithPaginationNext_whenICallLoadDataNextPageAndReturnSuccess206_thenICallViewShowPromotions() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.responseStatus = ConstantsHTTPCodes.STATUS_OK
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = false
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyView.isCallShowPromotions == true)
    }

    func test_givenIsLoadingNextPageFalseWithPromotionsAndPaginationNext_whenICallLoadDataNextPageAndReturnSuccesDiferrentThanOK_thenINotCallShowPromotions() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = false
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        for i in 0 ... 600 where i != ConstantsHTTPCodes.STATUS_OK {

                dummyInteractor.responseStatus = i
                sut.promotionsBO?.status = i

                // when
                sut.loadDataNextPage()

                // then
                XCTAssert(dummyView.isCallShowPromotions == false)
        }
    }

    //----------

    func test_givenIsLoadingNextPageFalseWithPromotionsAndPaginationNext_whenICallLoadDataNextPageAndReturnSuccesDiferrentThanOK_thenICallUpdateForNoMoreContent() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = false
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        for i in 0 ... 600 where i != ConstantsHTTPCodes.STATUS_OK {

                dummyInteractor.responseStatus = i
                sut.promotionsBO?.status = i

                // when
                sut.loadDataNextPage()

                // then
                XCTAssert(dummyView.isCalledUpdateForNoMoreContent == true)
        }
    }

    func test_givenIsLoadingNextPageFalseWithPromotionsAndPaginationNext_whenICallLoadDataNextPageAndReturnStatusOK_thenICallUpdateForMoreContent() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.responseStatus = ConstantsHTTPCodes.STATUS_OK
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = false
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyView.isCalledUpdateForMoreContent == true)
    }

    func test_givenIsLoadingNextPageFalseWithPromotionsWithoutPagination_whenICallLoadDataNextPageAndReturnStatusOK_thenINotCallUpdateForMoreContent() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.responseStatus = ConstantsHTTPCodes.STATUS_OK
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBOWithoutPagination()
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = false
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyView.isCalledUpdateForMoreContent == false)
    }

    func test_givenIsLoadingNextPageFalseWithPromotionsWithoutPagination_whenICallLoadDataNextPageAndReturnStatusOK_thenICallUpdateForNoMoreContent() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.responseStatus = ConstantsHTTPCodes.STATUS_OK
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBOWithoutPagination()
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = false
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == true)
    }

    func test_givenIsLoadingNextPageFalseWithPromotionsAndPaginationNextEmpty_whenICallLoadDataNextPageAndReturnStatusOK_thenICallUpdateForMoreContent() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.responseStatus = ConstantsHTTPCodes.STATUS_OK
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        dummyInteractor.promotionsBO.pagination?.links?.next = ""
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = false
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == true)
    }

    func test_givenIsLoadingNextPageFalseWithPromotionsAndPaginationNext_whenICallLoadDataNextPageAndReturnStatusOK_thenINotCallUpdateForNoMoreContent() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.responseStatus = ConstantsHTTPCodes.STATUS_OK
        dummyInteractor.promotionsBO = PromotionsGenerator.getPromotionsBO()
        sut.interactor = dummyInteractor

        sut.isLoadingNextPage = false
        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()

        // when
        sut.loadDataNextPage()

        // then
        XCTAssert(dummyView.isCalledUpdateForNoMoreContent == false)
    }

    // MARK: promotionCellSelected

    func test_givenADisplayItemPromotion_whenICallItemSelected_thenICallBusNavigateToDetailScreen() {

        // given
        let promotionsBO = PromotionsGenerator.getPromotionsBO()
        let displayItem = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionsBO.promotions[0], withTypeCell: .high)

        // when
        sut.itemSelected(withDisplayItem: displayItem)

        // then
        XCTAssert(dummyBusManager.isCalledNavigateToDetailScreen == true)
    }

    func test_givenADisplayItemPromotion_whenICallItemSelected_thenCallBusToNavigateDetailWithObject() {

        // given
        let promotionsBO = PromotionsGenerator.getPromotionsBO()
        let displayItem = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionsBO.promotions[0], withTypeCell: .high, withCategoryType: .activities)

        // when
        sut.itemSelected(withDisplayItem: displayItem)

        // then
        XCTAssert(dummyBusManager.isCalledPublishDataDetailWithTagPromotionsBO == true)

        let promotionDetailTransport = dummyBusManager.dummyDetailValue

        XCTAssert(promotionDetailTransport.promotionBO === displayItem.promotionBO)
    }

    func test_givenADisplayItemPromotionWithoutPromotionBO_whenICallItemSelected_thenIDoNotCallBusNavigateToDetailScreen() {

        // given
        let promotionsBO = PromotionsGenerator.getPromotionsBO()
        var displayItem = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionsBO.promotions[0], withTypeCell: .high)
        displayItem.promotionBO = nil

        // when
        sut.itemSelected(withDisplayItem: displayItem)

        // then
        XCTAssert(dummyBusManager.isCalledNavigateToDetailScreen == false)
    }

    // MARK: - retryButtonPressed

    func test_givenACategoriesButNotPromotions_whenICallRetryButtonPressed_thenCallViewShowSkeleton() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.retryButtonPressed()

        //then
        XCTAssert(dummyView.isCalledShowSkeleton == true)

    }

    func test_givenACategoriesButNotPromotions_whenICallRetryButtonPressed_thenICallProvidePromotionsWithCategory() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(dummyInteractor.isCalledProvidePromotionByParams == true)

    }

    func test_givenACategoriesButNotPromotions_whenICallRetryButtonPressedAndIsASuccessfulResponse_thenICallHideSkeleton() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(sut.view?.isCalledHideSkeleton == true)

    }

    func test_givenACategoriesButNotPromotions_whenICallRetryButtonPressedAndIsASuccessfulResponse_thenPromotionsBOShouldBeFilled() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(sut.promotionsBO != nil)

    }

    func test_givenACategoriesButNotPromotions_whenICallRetryButtonPressedAndIsASuccessfulResponse_thenCallShowPromotions() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // when
        sut.retryButtonPressed()

        //then
        XCTAssert(dummyView.isCallShowPromotions == true)

    }

    func test_givenACategoriesButNotPromotions_whenICallRetryButtonPressedAndIsASuccessfulResponse_thenCreateDisplayItems() {

        // given
        let dummyView = DummyView()
        sut.view = dummyView

        sut.promotionsBO = PromotionsGenerator.getPromotionsBO()
        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        let dummyInteractor = DummyInteractor()
        sut.interactor = dummyInteractor

        // when
        sut.retryButtonPressed()

        //then
        XCTAssert(!sut.displayItems.items.isEmpty)

    }

    func test_givenACategoriesButNotPromotions_whenICallRetryButtonPressedAndNotContentResponse_thenIDotNotCallHideSkeleton() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.responseStatus = ConstantsHTTPCodes.NO_CONTENT
        sut.interactor = dummyInteractor

        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(sut.view?.isCalledHideSkeleton == false)

    }

    func test_givenACategoriesButNotPromotions_whenICallRetryButtonPressedAndNotContentResponse_thenICallShowContentView() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.responseStatus = ConstantsHTTPCodes.NO_CONTENT
        sut.interactor = dummyInteractor

        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(sut.view?.isCalledShowNoContent == true)

    }

    func test_givenACategoriesButNotPromotions_whenICallRetryButtonPressedAndIsAnErrorResponse_thenIDotNotCallHideSkeleton() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceError = true
        sut.interactor = dummyInteractor

        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(sut.view?.isCalledHideSkeleton == false)

    }

    func test_givenACategoriesButNotPromotions_whenICallRetryButtonPressedAndIsAnErrorResponse_thenErrorBOShouldBeFilled() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()
        dummyInteractor.forceError = true
        sut.interactor = dummyInteractor

        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(sut.errorBO != nil)

    }

    func test_givenACategoriesButNotPromotions_whenICallRetryButtonPressedAndIsAnErrorResponseDifferentThan403_thenICallViewShowEmptyTsec() {

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()

        let errorBO = ErrorBO()
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        sut.interactor = dummyInteractor

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(dummyView.isCalledShowEmptyTsec == true)

    }

    func test_givenACategoriesButNotPromotions_whenICallRetryButtonPressedAndReturnErrorStatus403_thenINotCallViewShowEmptyTsec() {

        // given

        let dummyView = DummyView()
        sut.view = dummyView

        let dummyInteractor = DummyInteractor()

        let errorBO = ErrorBO()
        errorBO.status = ConstantsHTTPCodes.STATUS_403
        errorBO.code = ErrorCode.expiredSession.rawValue
        errorBO.isErrorShown = false
        dummyInteractor.errorBO = errorBO
        dummyInteractor.forceError = true

        sut.categoryBO = PromotionsGenerator.getCategoryBO().categories[0]

        sut.interactor = dummyInteractor

        // when
        sut.retryButtonPressed()

        // then
        XCTAssert(dummyView.isCalledShowEmptyTsec == false)

    }

    // MARK: DummyData

    class DummyInteractor: ShoppingListInteractorProtocol {

        var responseStatus = 200

        var forceError = false

        var wasLoadingNextPage = true

        var presenter: ShoppingListPresenter<DummyView>?

        var errorBO: ErrorBO?

        var isCalledProvidePromotionByParams = false
        var isCalledProvidePromotionsWithNextPage = false

        var promotionsBO: PromotionsBO!

        var nextPageLink = "nextPageLink"
        var paramsSent: ShoppingParamsTransport?

        func providePromotions(byParams params: ShoppingParamsTransport) -> Observable<ModelBO> {
            isCalledProvidePromotionByParams = true

            if promotionsBO == nil {
                promotionsBO = PromotionsGenerator.getPromotionsBO()
            }

            paramsSent = params

            promotionsBO.status = responseStatus

            if !forceError {

                if let presenter = self.presenter {
                    self.wasLoadingNextPage = presenter.isLoadingNextPage
                }

                return Observable <ModelBO>.just(promotionsBO!)

            } else {

                if errorBO == nil {
                    errorBO = ErrorBO(error: ErrorEntity(message: "Error"))

                    errorBO!.isErrorShown = false
                }

                return Observable.error(ServiceError.GenericErrorBO(error: errorBO!))
            }
        }

        func providePromotions(withNextPage nextPage: String) -> Observable <ModelBO> {

            isCalledProvidePromotionsWithNextPage = true

            nextPageLink = nextPage

            if !forceError {

                if promotionsBO == nil {
                    promotionsBO = PromotionsGenerator.getPromotionsBO()
                }

                promotionsBO.status = responseStatus

                return Observable <ModelBO>.just(promotionsBO!)

            } else {

                if errorBO == nil {
                    errorBO = ErrorBO(error: ErrorEntity(message: "Error"))

                    errorBO!.isErrorShown = false
                }

                return Observable.error(ServiceError.GenericErrorBO(error: errorBO!))
            }
        }

    }

    class DummyView: ShoppingListViewProtocol, ViewProtocol {

        var isCallShowNavTitle = false
        var isCallShowPromotions = false
        var isCalledShowEmptyTsec = false
        var isCalledShowNoContent = false
        var isCalledShowSkeleton = false
        var isCalledHideSkeleton = false

        var isCalledUpdateForNoMoreContent = false
        var isCalledUpdateForMoreContent = false
        var isCalledShowNextPageAnimation = false
        var isCalledHideNextPageAnimation = false
        var isShowErrorCalled = false
        var isCalledSendScreenOmnitureWithCategoryName = false
        var isCalledSendScreenOmnitureForTrending = false

        var titleScreen = ""
        var categoryNameForOmniture = ""

        var diplayItems: DisplayItemsPromotions?

        func showError(error: ModelBO) {
            isShowErrorCalled = true
        }

        func changeColorEditTexts() {
        }

        func showNavTitle(withTitle title: String) {
            isCallShowNavTitle = true
            titleScreen = title
        }

        func showPromotions(withDisplayItemsPromotions displayItemPromotions: DisplayItemsPromotions) {
            isCallShowPromotions = true
            diplayItems = displayItemPromotions
        }

        func showErrorEmptyTsec() {
            isCalledShowEmptyTsec = true
        }

        func showNoContentView() {
            isCalledShowNoContent = true
        }

        func showSkeleton() {
            isCalledShowSkeleton = true
        }

        func hideSkeleton() {
            isCalledHideSkeleton = true
        }

        func updateForNoMoreContent() {
            isCalledUpdateForNoMoreContent = true
        }

        func updateForMoreContent() {
            isCalledUpdateForMoreContent = true
        }

        func showNextPageAnimation() {
            isCalledShowNextPageAnimation = true
        }

        func hideNextPageAnimation() {
            isCalledHideNextPageAnimation = true
        }

        func sendScreenOmniture(withCategoryName categoryName: String) {
            isCalledSendScreenOmnitureWithCategoryName = true
            categoryNameForOmniture = categoryName
        }

        func sendScreenOmnitureForTrending() {
            isCalledSendScreenOmnitureForTrending = true
        }

    }

    class DummyBusManager: BusManager {

        var isCalledNavigateToDetailScreen = false
        var isCalledPublishDataDetailWithTagPromotionsBO = false

        var dummyDetailValue = PromotionDetailTransport()

        override init() {
            super.init()
            routerFactory = RouterInitFactory()
        }

        override func navigateScreen<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {
            if tag == "shopping-list-tag" && event === ShoppingPageReaction.EVENT_NAV_TO_DETAIL_SCREEN {
                isCalledNavigateToDetailScreen = true
                isCalledPublishDataDetailWithTagPromotionsBO = true
                dummyDetailValue = values as! PromotionDetailTransport
            }
        }
    }

}
