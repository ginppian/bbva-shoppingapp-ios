//
//  ShoppingMapListPresenterTest.swift
//  shoppingapp
//
//  Created by Marcos on 4/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest
import RxSwift
import CellsNativeComponents
import CoreLocation

#if TESTMX
    @testable import shoppingappMX
#endif

class ShoppingMapListPresenterTest: XCTestCase {

    var sut: ShoppingMapListPresenter<DummyView>!
    var dummyBusManager = DummyBusManager()

    override func setUp() {
        super.setUp()

        dummyBusManager = DummyBusManager()
        
        sut = ShoppingMapListPresenter<DummyView>(busManager: dummyBusManager)
    }
    
    // MARK: - setModel

    func test_givenAPromotionsTransportWithPromotions_whenICallSetModel_thenDataShouldBeFilled() {

        // given
        let promotionsBO = PromotionsGenerator.getPromotionsBO().promotions

        let promotionsTransport = ShoppingMapListTransport()
        promotionsTransport.promotions = promotionsBO
        promotionsTransport.userLocation = GeoLocationBO(withLatitude: 0.0, withLongitude: 0.0)
        promotionsTransport.storeLocation = GeoLocationBO(withLatitude: 0.5, withLongitude: 0.0)

        // when
        sut.setModel(model: promotionsTransport)

        // then
        XCTAssert(promotionsTransport.promotions == sut.promotions!)
        XCTAssert(promotionsTransport.userLocation == sut.userLocation!)
        XCTAssert(promotionsTransport.storeLocation == sut.storeLocation!)

    }
    
    // MARK: - viewLoaded

    func test_givenAPromotionsTransportWithPromotions_whenICallViewLoaded_thenDisplayDataShouldBeCreated() {

        // given
        let promotionsBO = PromotionsGenerator.getPromotionsBO().promotions

        let displayData = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionsBO[0], withTypeCell: CellPromotionType.small)

        // when
        sut.promotions = promotionsBO
        sut.viewLoaded()

        // then
        XCTAssert(displayData == sut.displayItems[0])
    }

    func test_givenAPromotionsTransportWithPromotion_whenICallViewLoaded_thenICallViewShowPromotions() {

        //given
        let promotionsBO = PromotionsGenerator.getPromotionsBO().promotions

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.promotions = promotionsBO
        sut.viewLoaded()

        //then
        XCTAssert(dummyView.isCallShowPromotions == true)

    }

    func test_givenAPromotionsTransportWithPromotions_whenICallViewLoaded_thenICallViewShowPromotionsWithRightDisplayData() {

        let dummyView = DummyView()
        sut.view = dummyView

        //given
        let promotions = PromotionsGenerator.getPromotionsBO().promotions
        var displayItems = [DisplayItemPromotion]()

        for promotionBO in promotions {

            let displayItemPromotion = DisplayItemPromotion.generateDisplayItemPromotion(fromPromotion: promotionBO, withTypeCell: CellPromotionType.small)
            displayItems.append(displayItemPromotion)
        }

        //when
        sut.promotions = promotions
        sut.viewLoaded()

        //then
        XCTAssert(dummyView.diplayItems! == displayItems)

    }

    func test_givenAPromotionsTransportWithoutLocation_whenICallViewLoaded_thenINotCallShowDistance() {

        //given
        let promotionsBO = PromotionsGenerator.getPromotionsBO().promotions

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.promotions = promotionsBO
        sut.viewLoaded()

        //then
        XCTAssert(dummyView.isCalledShowDistance == false)

    }

    func test_givenAPromotionsTransportWithLocation_whenICallViewLoaded_thenICallShowDistance() {

        //given
        let promotionsBO = PromotionsGenerator.getPromotionsBO().promotions

        let dummyView = DummyView()
        sut.view = dummyView

        //when
        sut.promotions = promotionsBO
        sut.userLocation = GeoLocationBO(withLatitude: 0.0, withLongitude: 0.0)
        sut.storeLocation = GeoLocationBO(withLatitude: 0.5, withLongitude: 0.0)

        sut.viewLoaded()

        //then
        XCTAssert(dummyView.isCalledShowDistance == true)

    }

    func test_givenAPromotionsTransportWithLocation_whenICallViewLoaded_thenICallShowDistanceWithRightDistance() {

        let dummyView = DummyView()
        sut.view = dummyView

        //given
        let promotionsBO = PromotionsGenerator.getPromotionsBO().promotions

        sut.promotions = promotionsBO
        let userLocation = GeoLocationBO(withLatitude: 0.0, withLongitude: 0.0)
        let storeLocation = GeoLocationBO(withLatitude: 0.5, withLongitude: 0.0)

        sut.userLocation = userLocation
        sut.storeLocation = storeLocation

        let distance = DisplayItemPromotion.calculateDistanteBetween(storeLatitue: storeLocation.latitude, andStoreLongitude: storeLocation.longitude, withUserLatiude: userLocation.latitude, andUserLongitude: userLocation.longitude)

        //when
        sut.viewLoaded()

        //then
        XCTAssert(dummyView.sentDistance == (Localizables.promotions.key_from_distance_text + " " + distance!))

    }
    
    // MARK: didTapInPromotionCell
    
    func test_givenPromotionsAndAValidIndex_whenICallDidTapInPromotionCell_thenICallBusManagerNavigateToDetailPromotion() {
        
        //given
        sut.promotions = PromotionsGenerator.getPromotionsBO().promotions
        
        //when
        sut.didTapInPromotionCell(atIndex: 0)
        
        //then
        XCTAssert(dummyBusManager.isCalledNavigateToShoppingDetail == true)
    }
    
    func test_givenPromotionsAndAndUserLocationAValidIndex_whenICallDidTapInPromotionCell_thenICallBusManagerNavigateToDetailPromotionWithAppropiateTransport() {
        
        //given
        sut.promotions = PromotionsGenerator.getPromotionsBO().promotions
        sut.userLocation = GeoLocationBO(withLatitude: 2.1, withLongitude: 3.3)
        let index = 0
        
        //when
        sut.didTapInPromotionCell(atIndex: index)
        
        //then
        XCTAssert(dummyBusManager.promotionDetailTransportSent != nil)
        XCTAssert(dummyBusManager.promotionDetailTransportSent!.promotionBO == sut.promotions![0])
        XCTAssert(dummyBusManager.promotionDetailTransportSent!.userLocation == sut.userLocation)
    }
    
    func test_givenPromotionsAndAndNotUserLocationAValidIndex_whenICallDidTapInPromotionCell_thenICallBusManagerNavigateToDetailPromotionWithAppropiateTransport() {
        
        //given
        sut.promotions = PromotionsGenerator.getPromotionsBO().promotions
        sut.userLocation = nil
        let index = 0
        
        //when
        sut.didTapInPromotionCell(atIndex: index)
        
        //then
        XCTAssert(dummyBusManager.promotionDetailTransportSent != nil)
        XCTAssert(dummyBusManager.promotionDetailTransportSent!.promotionBO == sut.promotions![0])
        XCTAssert(dummyBusManager.promotionDetailTransportSent!.userLocation == nil)
    }

    class DummyView: ShoppingMapListViewProtocol {

        var isCallShowPromotions = false
        var isShowErrorCalled = false
        var isCalledShowDistance = false
        var isCallShowLocation = false
        var isCallHideLocation = false

        var diplayItems: [DisplayItemPromotion]?
        var sentDistance: String?

        func showError(error: ModelBO) {
            isShowErrorCalled = true
        }

        func changeColorEditTexts() {
        }

        func showPromotions(withDisplayItemsPromotions displayItemPromotions: [DisplayItemPromotion]) {
            isCallShowPromotions = true
            diplayItems = displayItemPromotions
        }

        func showDistance(_ distance: String) {
            isCalledShowDistance = true
            sentDistance = distance
        }

    }

    class DummyBusManager: BusManager {

        var isCalledNavigateToShoppingDetail = false
        var promotionDetailTransportSent: PromotionDetailTransport?
        
        override init() {
            super.init()
            routerFactory = RouterInitFactory()
        }
        
        override func navigateScreen<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {
            if tag == ShoppingMapListPageReaction.routerTag && event === ShoppingMapListPageReaction.eventNavToPromotionDetailScreen {
                isCalledNavigateToShoppingDetail = true
                promotionDetailTransportSent = values as? PromotionDetailTransport
            }
        }

    }
}
