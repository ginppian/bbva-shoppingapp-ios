//
//  SelectFavoriteCategoryPresenterTests.swift
//  shoppingappMXTest
//
//  Created by jesus.martinez on 4/7/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest
import CellsNativeComponents

#if TESTMX
@testable import shoppingappMX
#endif

class SelectFavoriteCategoryPresenterTests: XCTestCase {

    var sut: SelectFavoriteCategoryPresenter<DummyView>!
    var dummyView = DummyView()
    var dummyBusManager = DummyBusManager()

    override func setUp() {
        super.setUp()

        dummyView = DummyView()
        dummyBusManager = DummyBusManager()

        sut = SelectFavoriteCategoryPresenter<DummyView>()
        sut.view = dummyView
        sut.busManager = dummyBusManager

    }

    // MARK: - setModel

    func test_givenFavoriteCategoriesTransport_whenICallSetModel_thenCategoriesShouldBeFilled() {

        // given
        let categoriesBO = PromotionsGenerator.getCategoryBO()
        let transport = FavoriteCategoriesTransport(categories: categoriesBO)

        // when
        sut.setModel(model: transport)

        // then
        XCTAssert(sut.categories != nil)
    }

    func test_givenFavoriteCategoriesTransport_whenICallSetModel_thenCategoriesAreSortedAlphabeticallyByName() {

        // given
        let categoriesBO = PromotionsGenerator.getCategoryBO()
        let transport = FavoriteCategoriesTransport(categories: categoriesBO)

        // when
        sut.setModel(model: transport)

        // then
        var areSort = true
        for i in 0..<(sut.categories!.categories.count - 1)  where  sut.categories!.categories[i].name > sut.categories!.categories[i + 1].name {
                areSort = false
                break
        }

        XCTAssert(areSort == true)
    }

    func test_givenFavoriteCategoriesTransport_whenICallSetModel_thenSelectedCategoriesShouldContainTheCategoryTypesWithFlagIsFavouriteTrue() {

        // given
        let categoriesBO = PromotionsGenerator.getCategoryBO()
        categoriesBO.sortAlphabeticallyByName()
        for i in 0..<categoriesBO.categories.count {
            categoriesBO.categories[i].isFavourite = false
        }
        categoriesBO.categories[3].isFavourite = true
        categoriesBO.categories[8].isFavourite = true
        categoriesBO.categories[20].isFavourite = true
        let transport = FavoriteCategoriesTransport(categories: categoriesBO)

        // when
        sut.setModel(model: transport)

        // then
        XCTAssert(sut.selectedCategories == [categoriesBO.categories[3].id, categoriesBO.categories[8].id, categoriesBO.categories[20].id])
    }

    // MARK: - viewDidLoad

    func test_givenCategories_whenICallViewDidLoad_thenICallViewShowFavoriteCategories() {

        // given
        sut.categories = PromotionsGenerator.getCategoryBO()

        // when
        sut.viewDidLoad()

        // then
        XCTAssert(dummyView.isCalledShowFavoriteCategories == true)
    }

    func test_givenCategoriesAndNumberOfFavoriteCategories_whenICallViewDidLoad_thenICallViewShowFavoriteCategoriesWithDisplayItemsThatNumberOfFavoriteCategoriesShouldMatch() {

        // given
        sut.categories = PromotionsGenerator.getCategoryBO()

        // when
        sut.viewDidLoad()

        // then
        XCTAssert(dummyView.displayItemsSent?.numberOfFavoriteCategories == sut.numberOfFavoriteCategories)
    }

    func test_givenCategories_whenICallViewDidLoad_thenICallViewShowFavoriteCategoriesWithDisplayItemsThatMatchWithNumberOfCategories() {

        // given
        sut.categories = PromotionsGenerator.getCategoryBO()

        // when
        sut.viewDidLoad()

        // then
        XCTAssert(dummyView.displayItemsSent?.items.count == sut.categories?.categories.count)
    }

    func test_givenSelectedCategoriesEmpty_whenICallViewDidLoad_thenIDoNotCallViewEnableSelectFavoriteCategories() {

        // given
        sut.categories = PromotionsGenerator.getCategoryBO()
        sut.selectedCategories = [CategoryType]()

        // when
        sut.viewDidLoad()

        // then
        XCTAssert(dummyView.isCalledEnableSelectFavoriteCategories == false)
    }

    func test_givenSelectedCategoriesWithLessThanNumberOfFavoriteCategories_whenICallViewDidLoad_thenIDoNotCallViewEnableSelectFavoriteCategories() {

        // given
        sut.categories = PromotionsGenerator.getCategoryBO()
        sut.selectedCategories = [.activities, .beautyAndGragances]
        sut.numberOfFavoriteCategories = 3

        // when
        sut.viewDidLoad()

        // then
        XCTAssert(dummyView.isCalledEnableSelectFavoriteCategories == false)
    }

    func test_givenSelectedCategoriesWithNumberOfFavoriteCategories_whenICallViewDidLoad_thenICallViewEnableSelectFavoriteCategories() {

        // given
        sut.categories = PromotionsGenerator.getCategoryBO()
        sut.selectedCategories = [.activities, .beautyAndGragances, .bookStore]
        sut.numberOfFavoriteCategories = 3

        // when
        sut.viewDidLoad()

        // then
        XCTAssert(dummyView.isCalledEnableSelectFavoriteCategories == true)
    }

    // MARK: - selectedCategory

    func test_givenAValidIndexAndCategoriesCategoriesAndSelectedCategoriesEmpty_whenICallSelectedCategory_thenSelectedCategoriesShouldContainTheCategoryTypeAtIndex() {

        // given
        let index = 3
        sut.categories = PromotionsGenerator.getCategoryBO()

        // when
        sut.selectedCategory(atIndex: index)

        // then
        XCTAssert(sut.selectedCategories.contains(sut.categories!.categories[index].id))
    }

    func test_givenAInValidIndexAndCategoriesCategoriesAndSelectedCategories_whenICallSelectedCategory_thenSelectedCategoriesShouldNotChange() {

        // given
        let index = 1000
        sut.categories = PromotionsGenerator.getCategoryBO()
        sut.selectedCategories = [.activities, .beautyAndGragances]
        let expectedSelected = sut.selectedCategories

        // when
        sut.selectedCategory(atIndex: index)

        // then
        XCTAssert(sut.selectedCategories == expectedSelected)
    }

    func test_givenAValidIndexOfAnyCategorySelectedAlreadyAndCategoriesCategoriesAndSelectedCategories_whenICallSelectedCategory_thenSelectedCategoriesShouldNotChange() {

        // given
        let index = 3
        sut.categories = PromotionsGenerator.getCategoryBO()
        sut.selectedCategories = [.activities, .dietAndNutrition]
        let expectedSelected = sut.selectedCategories

        // when
        sut.selectedCategory(atIndex: index)

        // then
        XCTAssert(sut.selectedCategories == expectedSelected)
    }

    func test_givenAValidIndexCategoriesCategoriesAndSelectedCategoriesWithLessThanOneThanNumberOfFavoriteCategories_whenICallSelectedCategory_thenICallViewEnableSelectFavoriteCategories() {

        // given
        let index = 3
        sut.categories = PromotionsGenerator.getCategoryBO()
        sut.selectedCategories = [.activities, .beautyAndGragances]

        // when
        sut.selectedCategory(atIndex: index)

        // then
        XCTAssert(dummyView.isCalledEnableSelectFavoriteCategories == true)
    }

    // MARK: - deselectedCategory

    func test_givenAValidIndexCategoriesAndSelectedCategoriesAreEmtpy_whenICalldeselectedCategory_thenSelectedCategoriesShouldKeepEmpty() {

        // given
        let index = 5
        sut.categories = PromotionsGenerator.getCategoryBO()

        // when
        sut.deselectedCategory(atIndex: index)

        // then
        XCTAssert(sut.selectedCategories.isEmpty == true)
    }

    func test_givenAValidIndexOIncludedInSelectedCategoriesAndSelectedCategoriesNotEmpty_whenICalldeselectedCategory_thenSelectedCategoriesShouldNotContainTheCategoryTypeAtIndex() {

        // given
        let index = 5
        sut.categories = PromotionsGenerator.getCategoryBO()
        sut.selectedCategories = [.activities, .hobbies, .bookStore]

        // when
        sut.deselectedCategory(atIndex: index)

        // then
        XCTAssert(sut.selectedCategories.contains(.hobbies) == false)
    }

    func test_givenAValidIndexButNotContainedInSelectedCategoriesAndSelectedCategoriesNotEmpty_whenICalldeselectedCategory_thenSelectedCategorieShouldKeepEqual() {

        // given
        let index = 10
        sut.categories = PromotionsGenerator.getCategoryBO()
        sut.selectedCategories = [.activities, .beautyAndGragances, .bookStore]
        let expected = sut.selectedCategories

        // when
        sut.deselectedCategory(atIndex: index)

        // then
        XCTAssert(sut.selectedCategories == expected)
    }

    func test_givenAInvalidIndexCategoriesAndSelectedCategoriesNotEmpty_whenICalldeselectedCategory_thenSelectedCategorieShouldKeepEqual() {

        // given
        let index = 1000
        sut.categories = PromotionsGenerator.getCategoryBO()
        sut.selectedCategories = [.activities, .beautyAndGragances, .bookStore]
        let expected = sut.selectedCategories

        // when
        sut.deselectedCategory(atIndex: index)

        // then
        XCTAssert(sut.selectedCategories == expected)
    }

    func test_givenAValidIndexOIncludedInSelectedCategoriesAndSelectedCategoriesNotEmpty_whenICalldeselectedCategory_thenICallViewDisableSelectFavoriteCategories() {

        // given
        let index = 5
        sut.categories = PromotionsGenerator.getCategoryBO()
        sut.selectedCategories = [.activities, .hobbies, .bookStore]

        // when
        sut.deselectedCategory(atIndex: index)

        // then
        XCTAssert(dummyView.isCalledDisableSelectFavoriteCategories == true)
    }
    
    // MARK: - triedToSelectMoreFavoriteCategoriesThanLimit
    
    func test_givenAny_whenICallTriedToSelectMoreFavoriteCategoriesThanLimit_thenICallViewShowToastAboutFavoriteCategoriesLimit() {
        
        //given
        
        //when
        sut.triedToSelectMoreFavoriteCategoriesThanLimit()
        
        //then
        XCTAssert(dummyView.isCalledShowToastFavoriteCategoriesLimit == true)
    }

    // MARK: - confirmFavoriteCategoriesPressed

    func test_givenSelectedCategoriesEqualsThanCategoriesWithIsFavouriteFlagTrue_whenICallconfirmFavoriteCategoriesPressed_thenIDoNotCallBusManagerShowLoading() {

        // given
        let categoriesBO = PromotionsGenerator.getCategoryBO()
        categoriesBO.sortAlphabeticallyByName()
        for i in 0..<categoriesBO.categories.count {
            categoriesBO.categories[i].isFavourite = false
        }
        categoriesBO.categories[3].isFavourite = true
        categoriesBO.categories[8].isFavourite = true
        categoriesBO.categories[20].isFavourite = true
        sut.categories = categoriesBO
        sut.selectedCategories = [categoriesBO.categories[3].id, categoriesBO.categories[8].id, categoriesBO.categories[20].id]

        // when
        sut.confirmFavoriteCategoriesPressed()

        // then
        XCTAssert(dummyBusManager.isCalledShowLoading == false)
    }

    func test_givenSelectedCategoriesEqualsThanCategoriesWithIsFavouriteFlagTrue_whenICallconfirmFavoriteCategoriesPressed_thenICallBusManagerPublishDataWithAppropiatedTagAndAction() {

        // given
        let categoriesBO = PromotionsGenerator.getCategoryBO()
        categoriesBO.sortAlphabeticallyByName()
        for i in 0..<categoriesBO.categories.count {
            categoriesBO.categories[i].isFavourite = false
        }
        categoriesBO.categories[3].isFavourite = true
        categoriesBO.categories[8].isFavourite = true
        categoriesBO.categories[20].isFavourite = true
        sut.categories = categoriesBO
        sut.selectedCategories = [categoriesBO.categories[3].id, categoriesBO.categories[8].id, categoriesBO.categories[20].id]

        // when
        sut.confirmFavoriteCategoriesPressed()

        // then
        XCTAssert(dummyBusManager.isCalledPublishDataSelectionFavoriteCategoriesDoNotChange == true)
    }

    func test_givenSelectedCategoriesEqualsThanCategoriesWithIsFavouriteFlagTrue_whenICallconfirmFavoriteCategoriesPressed_thenIDoNotCallViewSelectFavoriteCategories() {

        // given
        let categoriesBO = PromotionsGenerator.getCategoryBO()
        categoriesBO.sortAlphabeticallyByName()
        for i in 0..<categoriesBO.categories.count {
            categoriesBO.categories[i].isFavourite = false
        }
        categoriesBO.categories[3].isFavourite = true
        categoriesBO.categories[8].isFavourite = true
        categoriesBO.categories[20].isFavourite = true
        sut.categories = categoriesBO
        sut.selectedCategories = [categoriesBO.categories[3].id, categoriesBO.categories[8].id, categoriesBO.categories[20].id]

        // when
        sut.confirmFavoriteCategoriesPressed()

        // then
        XCTAssert(dummyView.isCalledSelectFavoriteCategories == false)
    }

    func test_givenSelectedCategoriesEqualsThanCategoriesWithIsFavouriteFlagTrue_whenICallConfirmFavoriteCategoriesPressed_thenICallBusManagerDismissViewController() {

        // given
        let categoriesBO = PromotionsGenerator.getCategoryBO()
        categoriesBO.sortAlphabeticallyByName()
        for i in 0..<categoriesBO.categories.count {
            categoriesBO.categories[i].isFavourite = false
        }
        categoriesBO.categories[3].isFavourite = true
        categoriesBO.categories[8].isFavourite = true
        categoriesBO.categories[20].isFavourite = true
        sut.categories = categoriesBO
        sut.selectedCategories = [categoriesBO.categories[3].id, categoriesBO.categories[8].id, categoriesBO.categories[20].id]

        // when
        sut.confirmFavoriteCategoriesPressed()

        // then
        XCTAssert(dummyBusManager.isCalledDismissViewController == true)
    }

    func test_givenSelectedCategoriesDifferentThanCategoriesWithIsFavouriteFlagTrue_whenICallconfirmFavoriteCategoriesPressed_thenICallBusManagerShowLoading() {

        // given
        let categoriesBO = PromotionsGenerator.getCategoryBO()
        categoriesBO.sortAlphabeticallyByName()
        sut.categories = categoriesBO
        sut.selectedCategories = [.activities, .beautyAndGragances, .bookStore]

        // when
        sut.confirmFavoriteCategoriesPressed()

        // then
        XCTAssert(dummyBusManager.isCalledShowLoading == true)
    }

    func test_givenSelectedCategoriesDifferentThanCategoriesWithIsFavouriteFlagTrue_whenICallconfirmFavoriteCategoriesPressed_thenICallViewSelectFavoriteCategories() {

        // given
        let categoriesBO = PromotionsGenerator.getCategoryBO()
        categoriesBO.sortAlphabeticallyByName()
        sut.categories = categoriesBO
        sut.selectedCategories = [.activities, .beautyAndGragances, .bookStore]

        // when
        sut.confirmFavoriteCategoriesPressed()

        // then
        XCTAssert(dummyView.isCalledSelectFavoriteCategories == true)
    }

    func test_givenSelectedCategoriesDifferentThanCategoriesWithIsFavouriteFlagTrue_whenICallconfirmFavoriteCategoriesPressed_thenICallViewSelectFavoriteCategoriesWithDTOAccordingSelecteCategories() {

        // given
        let categoriesBO = PromotionsGenerator.getCategoryBO()
        categoriesBO.sortAlphabeticallyByName()
        sut.categories = categoriesBO
        sut.selectedCategories = [.activities, .beautyAndGragances, .bookStore]

        // when
        sut.confirmFavoriteCategoriesPressed()

        // then
        let dtoExpected = FavouriteCategoriesDTO(favouriteCategories: [FavouriteCategoryDTO(CategoryType.activities.rawValue, isFavourite: true), FavouriteCategoryDTO(CategoryType.beautyAndGragances.rawValue, isFavourite: true), FavouriteCategoryDTO(CategoryType.bookStore.rawValue, isFavourite: true)])

        XCTAssert(dummyView.dtoSent == dtoExpected)
    }
    
    func test_givenSelectedCategories_whenICallConfirmFavoriteCategoriesPressed_thenIRegisterToNotSuggestFavoriteSelection() {
        
        // given
        let categoriesBO = PromotionsGenerator.getCategoryBO()
        categoriesBO.sortAlphabeticallyByName()
        sut.categories = categoriesBO
        sut.selectedCategories = [.activities, .beautyAndGragances, .bookStore]
        
        // when
        sut.confirmFavoriteCategoriesPressed()
        
        // then
        
        XCTAssert(PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kDontSuggestFavoriteSelection) as! Bool == true)
    }
    
    func test_givenSelectedCategories_whenICallConfirmFavoriteCategoriesPressed_thenICallBusPublishDataEventTrackerNewTrackEvent() {
        
        // given
        let categoriesBO = PromotionsGenerator.getCategoryBO()
        categoriesBO.sortAlphabeticallyByName()
        sut.categories = categoriesBO
        sut.selectedCategories = [.activities, .beautyAndGragances, .bookStore]
        
        // when
        sut.confirmFavoriteCategoriesPressed()
        
        // then
        XCTAssert(dummyBusManager.isCalledPublishDataEventTrackerNewTrackEvent == true)
        
    }
    
    func test_givenSelectedCategories_whenICallConfirmFavoriteCategoriesPressed_thenICallBusPublishDataEventTrackerNewTrackEventWithAppropiatedEventModel() {
        
        // given
        let categoriesBO = PromotionsGenerator.getCategoryBO()
        categoriesBO.sortAlphabeticallyByName()
        sut.categories = categoriesBO
        sut.selectedCategories = [.activities, .beautyAndGragances, .bookStore]
        
        // when
        sut.confirmFavoriteCategoriesPressed()
        
        // then
        XCTAssert(dummyBusManager.eventSent != nil)
        XCTAssert(dummyBusManager.eventSent?.type == .favoriteCategories)
    }
    
    // MARK: - dismissViewController
    
    func test_givenAccessToPromotionsAndDontSuggestFavoriteSelectionToTrue_whenIDismissTheView_thenIDontShowAToastAboutFavoriteSelection() {
        
        // given
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: true as AnyObject, withKey: PreferencesManagerKeys.kDontSuggestFavoriteSelection)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 1), withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        
        // when
        sut.dismissViewController()
        
        // then
        XCTAssert(sut.view?.isShownToast == false)
    }
    
    func test_givenSecondAccessToPromotionsAndDontSuggestFavoriteSelectionToFalse_whenIDismissTheView_thenIShowAToastAboutFavoriteSelection() {
        
        // given
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: false as AnyObject, withKey: PreferencesManagerKeys.kDontSuggestFavoriteSelection)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 2), withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        
        // when
        sut.dismissViewController()
        
        // then
        XCTAssert(sut.view?.isShownToast == true)
    }
    
    func test_givenFourthAccessToPromotionsAndWaitForAccessToSuggestFavoriteSelectionEqualToFourAndDontSuggestFavoriteSelectionToFalse_whenIDismissTheView_thenIShowAToastAboutFavoriteSelection() {
        
        // given
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: false as AnyObject, withKey: PreferencesManagerKeys.kDontSuggestFavoriteSelection)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 4), withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 4), withKey: PreferencesManagerKeys.kWaitToAccessToSuggestFavoriteSelection)
        
        // when
        sut.dismissViewController()
        
        // then
        XCTAssert(sut.view?.isShownToast == true)
    }
    
    func test_givenFourthAccessToPromotionsAndWaitForAccessToSuggestFavoriteSelectionEqualToFourAndDontSuggestFavoriteSelectionToFalse_whenIDismissTheView_thenISetDontSuggestFavoriteSelectionToTrue() {
        
        // given
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: false as AnyObject, withKey: PreferencesManagerKeys.kDontSuggestFavoriteSelection)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 4), withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 4), withKey: PreferencesManagerKeys.kWaitToAccessToSuggestFavoriteSelection)
        
        // when
        sut.dismissViewController()
        
        // then
        XCTAssert(PreferencesManager.sharedInstance().getDontSuggestFavoriteSelection() == true)
    }
    
    func test_givenFithAccessToPromotionsAndWaitForAccessToSuggestFavoriteSelectionEqualToFiveAndDontSuggestFavoriteSelectionToFalse_whenIDismissTheView_thenIShowAToastAboutFavoriteSelection() {
        
        // given
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: false as AnyObject, withKey: PreferencesManagerKeys.kDontSuggestFavoriteSelection)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 5), withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 5), withKey: PreferencesManagerKeys.kWaitToAccessToSuggestFavoriteSelection)
        
        // when
        sut.dismissViewController()
        
        // then
        XCTAssert(sut.view?.isShownToast == true)
    }
    
    func test_givenFithAccessToPromotionsAndWaitForAccessToSuggestFavoriteSelectionEqualToFiveAndDontSuggestFavoriteSelectionToFalse_whenIDismissTheView_thenISetDontSuggestFavoriteSelectionToTrue() {
        
        // given
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: false as AnyObject, withKey: PreferencesManagerKeys.kDontSuggestFavoriteSelection)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 5), withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 5), withKey: PreferencesManagerKeys.kWaitToAccessToSuggestFavoriteSelection)
        
        // when
        sut.dismissViewController()
        
        // then
        XCTAssert(PreferencesManager.sharedInstance().getDontSuggestFavoriteSelection() == true)
    }
    
    // MARK: - confirmFavoriteCategoriesFinished

    func test_givenAny_whenICallConfirmFavoriteCategoriesFinished_thenICallBusManagerPublishDataWithAppropiatedTagAndAction() {

        // given

        // when
        sut.confirmFavoriteCategoriesFinished()

        // then
        XCTAssert(dummyBusManager.isCalledPublishDataSelectionFavoriteCategoriesSuccess == true)
    }

    func test_givenAny_whenICallConfirmFavoriteCategoriesFinished_thenICallBusManagerDismissLoading() {

        // given

        // when
        sut.confirmFavoriteCategoriesFinished()

        // then
        XCTAssert(dummyBusManager.isCalledHideLoading == true)
    }

    func test_givenAny_whenICallConfirmFavoriteCategoriesFinished_thenICallBusManagerDismissViewController() {

        // given

        // when
        sut.confirmFavoriteCategoriesFinished()

        // then
        XCTAssert(dummyBusManager.isCalledDismissViewController == true)
    }

    // MARK: - confirmFavoriteCategoriesFinishedWithError

    func test_givenAnError_whenICallConfirmFavoriteCategoriesFinishedWithError_thenErrorBOShouldBeFilled() {

        // given
        let error = ErrorBO(error: ErrorEntity(message: "Error"))

        // when
        sut.confirmFavoriteCategoriesFinished(withError: error)

        // then
        XCTAssert(sut.errorBO != nil)
    }

    func test_givenAnError_whenICallConfirmFavoriteCategoriesFinishedWithError_thenErrorBOShouldMatch() {

        // given
        let error = ErrorBO(error: ErrorEntity(message: "Error"))

        // when
        sut.confirmFavoriteCategoriesFinished(withError: error)

        // then
        XCTAssert(sut.errorBO === error)
    }

    func test_givenAnError_whenICallConfirmFavoriteCategoriesFinishedWithError_thenICallBusManagerDismissLoading() {

        // given
        let error = ErrorBO(error: ErrorEntity(message: "Error"))

        // when
        sut.confirmFavoriteCategoriesFinished(withError: error)

        // then
        XCTAssert(dummyBusManager.isCalledHideLoading == true)
    }

    // MARK: - Dummy classes
    class DummyView: SelectFavoriteCategoryViewProtocol {
        
        var isCalledShowFavoriteCategories = false
        var isCalledEnableSelectFavoriteCategories = false
        var isCalledDisableSelectFavoriteCategories = false
        var isCalledSelectFavoriteCategories = false
        var isCalledShowError = false
        var isShownToast = false
        var isCalledShowToastFavoriteCategoriesLimit = false
        
        var displayItemsSent: FavoriteCategoriesDisplayData?
        var dtoSent = FavouriteCategoriesDTO()

        func showFavoriteCategories(withDisplayData displayData: FavoriteCategoriesDisplayData) {
            
            isCalledShowFavoriteCategories = true
            displayItemsSent = displayData
        }

        func enableSelectFavoriteCategories() {
            
            isCalledEnableSelectFavoriteCategories = true
        }

        func disableSelectFavoriteCategories() {
            
            isCalledDisableSelectFavoriteCategories = true
        }

        func selectFavoriteCategories(withDTO dto: FavouriteCategoriesDTO) {
            
            isCalledSelectFavoriteCategories = true
            dtoSent = dto
        }

        func showError(error: ModelBO) {
            
            isCalledShowError = true
        }

        func changeColorEditTexts() {
            
        }
        
        func showToastAboutSelectFavoriteCategoriesLater() {
            
            isShownToast = true
        }
        
        func showToastAboutFavoriteCategoriesLimit() {
            
            isCalledShowToastFavoriteCategoriesLimit = true
        }
    }

    class DummyBusManager: BusManager {
        var isCalledShowLoading = false
        var isCalledHideLoading = false
        var isCalledDismissViewController = false
        var isCalledPublishDataSelectionFavoriteCategoriesSuccess = false
        var isCalledPublishDataSelectionFavoriteCategoriesDoNotChange = false
        var isCalledPublishDataEventTrackerNewTrackEvent = false
        
        var eventSent: EventProtocol?
        
        override init() {
            super.init()
            routerFactory = RouterInitFactory()
        }

        override func publishData<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {
            if tag == SelectFavoriteCategoryPageReaction.ROUTER_TAG && event === SelectFavoriteCategoryPageReaction.SELECTION_FAVORITE_CATEGORIES_SUCCESS {
                isCalledPublishDataSelectionFavoriteCategoriesSuccess = true
            } else if tag == SelectFavoriteCategoryPageReaction.ROUTER_TAG && event === SelectFavoriteCategoryPageReaction.SELECTION_FAVORITE_CATEGORIES_DO_NOT_CHANGE {
                isCalledPublishDataSelectionFavoriteCategoriesDoNotChange = true
            } else if tag == EventTracker.id && event === PageReactionConstants.NEW_TRACK_EVENT {
                isCalledPublishDataEventTrackerNewTrackEvent = true
                eventSent = values as? EventProtocol
            }
        }

        override func showLoading(completion: (() -> Void)?) {
            if let completion = completion {
                completion()
            }
            isCalledShowLoading = true
        }

        override  func hideLoading(completion: (() -> Void)? ) {
            if let completion = completion {
                completion()
            }
            isCalledHideLoading = true
        }

        override func dismissViewController(animated: Bool, completion: (() -> Void)?) {
            if let completion = completion {
                completion()
            }
            isCalledDismissViewController = true
        }
    }
}
