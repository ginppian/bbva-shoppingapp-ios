//
//  FavoriteCategoriesDisplayDataTests.swift
//  shoppingappMXTest
//
//  Created by jesus.martinez on 4/7/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
@testable import shoppingappMX
#endif

class FavoriteCategoriesDisplayDataTests: XCTestCase {

    func test_givenNumberOfFavoriteCategoriesAndCategories_whenICallInit_thenNumberOfFavoriteCategoriesShouldMatch() {
        // given
        let categories = PromotionsGenerator.getCategoryBO()
        let number = 5

        // when
        let sut = FavoriteCategoriesDisplayData(numberOfFavoriteCategories: number, categories: categories)

        // then
        XCTAssert(sut.numberOfFavoriteCategories == number)
    }

    func test_givenNumberOfFavoriteCategoriesAndCategories_whenICallInit_thenDataItemsShouldMatchWithCategories() {
        // given
        let categories = PromotionsGenerator.getCategoryBO()
        let number = 5

        // when
        let sut = FavoriteCategoriesDisplayData(numberOfFavoriteCategories: number, categories: categories)

        // then
        XCTAssert(sut.items.count == categories.categories.count)

        for i in 0..<sut.items.count {
            XCTAssert(sut.items[i].name == categories.categories[i].name)
            XCTAssert(sut.items[i].isSelected == categories.categories[i].isFavourite)
            XCTAssert(sut.items[i].urlImage == categories.categories[i].image?.url)
        }
    }

}
