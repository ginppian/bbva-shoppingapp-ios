//
//  AnonymousTutorialPresenterTest.swift
//  shoppingapp
//
//  Created by Javier Pino on 5/6/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
@testable import shoppingappMX
#elseif TESTPE
@testable import shoppingappPE
#endif

class AnonymousTutorialPresenterTest: XCTestCase {

    var sut: AnonymousTutorialPresenter<DummyView>!
    var dummyView = DummyView()
    var dummyBusManager = DummyBusManager()
    var dummySessionManager: DummySessionManager!
    var dummyDisplayData = AnonymousTutorialDisplayData(navigationBarTitle: "Home", animationName: "animation", title: "Home title", subtitle: "Home subtitle")

    override func setUp() {
        super.setUp()

        KeychainManagerTestHelper.configureKeychainManagerForTesting()
        sut = AnonymousTutorialPresenter<DummyView>(displayData: dummyDisplayData)
        sut.view = dummyView
        sut.busManager = dummyBusManager
        DummySessionManager.setupMock()
        dummySessionManager = (SessionDataManager.sessionDataInstance() as! DummySessionManager)
    }

    // MARK: - viewLoaded
    func test_givenAny_whenICallViewLoaded_thenICallViewConfigure() {

        //given

        //when
        sut.viewLoaded()

        //then
        XCTAssert(dummyView.isCalledConfigure == true)
    }

    func test_givenAny_whenICallViewLoaded_thenICallViewConfigureWithRightDisplayData() {

        //given

        //when
        sut.viewLoaded()

        //then
        XCTAssert(dummyView.receivedDisplayData == dummyDisplayData)
    }

    // MARK: - viewAppeared
    func test_givenAnimatingFalse_whenICallViewAppeared_thenISetAnimatingTrue() {

        //given
        sut.animating = false

        //when
        sut.viewAppeared()

        //then
        XCTAssert(sut.animating == true)
    }

    func test_givenAnimatingFalse_whenICallViewAppeared_thenISetAnimatedFalse() {

        //given
        sut.animating = false

        //when
        sut.viewAppeared()

        //then
        XCTAssert(sut.animated == false)
    }

    func test_givenAnimatingFalse_whenICallViewAppeared_thenICallViewStartAnimation() {

        //given
        sut.animating = false

        //when
        sut.viewAppeared()

        //then
        XCTAssert(dummyView.isCalledStartAnimation == true)
    }

    func test_givenAnimatingTrue_whenICallViewAppeared_thenIDontCallViewStartAnimation() {

        //given
        sut.animating = true

        //when
        sut.viewAppeared()

        //then
        XCTAssert(dummyView.isCalledStartAnimation == false)
    }

    func test_givenIsRememberedUser_whenICallViewAppearedAndIsRememberedUser_thenIHideNotClientButton() {

        //given
        KeychainManager.shared.saveUserId(forUserId: "666")
        KeychainManager.shared.saveFirstname(forFirstname: "firstname")
        //when
        sut.viewAppeared()

        //then
        XCTAssert(dummyView.isCalledHideNotClientButton == true)
        XCTAssert(dummyView.notClientButtonHidden == true)
    }

    func test_givenIsNotRememberedUser_whenICallViewAppearedAnd_thenIShowNotClientButton() {

        //given
        KeychainManager.shared.resetKeyChainItems()

        //when
        sut.viewAppeared()

        //then
        XCTAssert(dummyView.isCalledHideNotClientButton == true)
        XCTAssert(dummyView.notClientButtonHidden == false)
    }

    // MARK: - viewDisappeared
    func test_givenAnimatingTrue_whenICallViewDisappeared_thenISetAnimatingFalse() {

        //given
        sut.animating = true

        //when
        sut.viewDisappeared()

        //then
        XCTAssert(sut.animating == false)
    }

    func test_givenAnimatingTrue_whenICallViewDisappeared_thenISetAnimatedTrue() {

        //given
        sut.animating = true

        //when
        sut.viewDisappeared()

        //then
        XCTAssert(sut.animated == true)
    }

    func test_givenAny_whenICallViewDisappeared_thenICallViewStopAnimation() {

        //given
        sut.animating = true

        //when
        sut.viewDisappeared()

        //then
        XCTAssert(dummyView.isCalledStopAnimation == true)
    }

    // MARK: - viewEnteredBackground

    func test_givenAny_whenICallViewEnteredForeground_thenICallViewRemoveAnimation() {

        //given

        //when
        sut.viewEnteredBackground()

        //then
        XCTAssert(dummyView.isCalledRemoveAnimation == true)
    }

    // MARK: - viewEnteredForeground

    func test_givenAny_whenICallViewEnteredForeground_thenISetAnimatingTrue() {

        //given

        //when
        sut.viewEnteredForeground()

        //then
        XCTAssert(sut.animating == true)
    }

    func test_givenAny_whenICallViewEnteredForeground_thenISetAnimatedFalse() {

        //given

        //when
        sut.viewEnteredForeground()

        //then
        XCTAssert(sut.animated == false)
    }

    func test_givenAny_whenICallViewEnteredForeground_thenICallViewReloadAnimation() {

        //given

        //when
        sut.viewEnteredForeground()

        //then
        XCTAssert(dummyView.isCalledReloadAnimation == true)
    }

    // MARK: - animationFinished
    func test_givenAny_whenICallAnimationFinished_thenISetAnimatingFalse() {

        //given

        //when
        sut.animationFinished()

        //then
        XCTAssert(sut.animating == false)
    }

    func test_givenAny_whenICallAnimationFinished_thenISetAnimatedTrue() {

        //given

        //when
        sut.animationFinished()

        //then
        XCTAssert(sut.animated == true)
    }

    // MARK: - loginButtonPressed
    func test_givenAny_whenICallLoginButtonPressed_thenICallBusManagerSetRootWhenNoSession() {

        //given

        //when
        sut.loginButtonPressed()

        //then
        XCTAssert(dummyBusManager.isCalledSetRootWhenNoSession == true)
    }

    func test_givenAny_whenICallLoginButtonPressed_thenICallSessionDataManagerCloseSession() {

        //given

        //when
        sut.loginButtonPressed()

        //then
        XCTAssert(dummySessionManager.isCalledCloseSession == true)
    }

    // MARK: - Dummy classes
    class DummyView: AnonymousTutorialViewProtocol {

        var isCalledConfigure = false
        var isCalledStartAnimation = false
        var isCalledStopAnimation = false
        var isCalledRemoveAnimation = false
        var isCalledReloadAnimation = false
        var isCalledDisableInteraction = false
        var isCalledEnableInteraction = false
        var isCalledHideNotClientButton = false
        var notClientButtonHidden = false

        var receivedDisplayData: AnonymousTutorialDisplayData?

        func configure(displayData: AnonymousTutorialDisplayData) {
            isCalledConfigure = true
            receivedDisplayData = displayData
        }

        func startAnimation() {
            isCalledStartAnimation = true
        }

        func stopAnimation() {
            isCalledStopAnimation = true
        }

        func removeAnimation() {
            isCalledRemoveAnimation = true
        }

        func reloadAnimation() {
            isCalledReloadAnimation = true
        }

        func disableInteraction() {
            isCalledDisableInteraction = true
        }

        func enableInteraction() {
            isCalledEnableInteraction = true
        }

        func showError(error: ModelBO) {
        }

        func changeColorEditTexts() {
        }

        func hideNotClientButton(_ hide: Bool) {
            isCalledHideNotClientButton = true
            notClientButtonHidden = hide
        }

    }

    class DummyBusManager: BusManager {

        var isCalledSetRootWhenNoSession = false

        override init() {
            super.init()
            routerFactory = RouterInitFactory()
        }
        
        override func setRootWhenNoSession() {
            isCalledSetRootWhenNoSession = true
        }
    }
}
