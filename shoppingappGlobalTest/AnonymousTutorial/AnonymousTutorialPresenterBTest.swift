//
//  AnonymousTutorialPresenterBTest.swift
//  shoppingappMXTest
//
//  Created by Marcos on 5/2/19.
//  Copyright © 2019 BBVA. All rights reserved.
//

import XCTest
import CellsNativeComponents

#if TESTMX
@testable import shoppingappMX
#endif

class AnonymousTutorialPresenterBTest: XCTestCase {

    var sut: AnonymousTutorialPresenterB<DummyView>!
    var dummyView = DummyView()
    var dummyBusManager = DummyBusManager()
    var dummySessionManager: DummySessionManager!

    override func setUp() {
        super.setUp()

        sut = AnonymousTutorialPresenterB<DummyView>(displayData: AnonymousTutorialDisplayData(navigationBarTitle: "", animationName: "", title: "", subtitle: ""))
        sut.view = dummyView
        dummyBusManager = DummyBusManager()
        sut.busManager = dummyBusManager
        
        DummySessionManager.setupMock()
        dummySessionManager = (SessionDataManager.sessionDataInstance() as! DummySessionManager)
    }

    // MARK: - loginButtonPressed
    func test_givenAny_whenICallNotClientButtonPressed_thenICallBusManagerSetRootWhenNoSession() {

        //given

        //when
        sut.notClientButtonPressed()

        //then
        XCTAssert(dummyBusManager.isCalledSetRootWhenNoSession == true)
    }

    func test_givenAny_whenICallNotClientButtonPressed_thenICallSessionDataManagerCloseSession() {

        //given

        //when
        sut.notClientButtonPressed()

        //then
        XCTAssert(dummySessionManager.isCalledCloseSession == true)
    }

    func test_givenAny_whenICallNotClientButtonPressed__thenICallBusManagerNotifyEventCloseCellStack() {

        //given

        //when
        sut.notClientButtonPressed()

        //then
        XCTAssert(dummyBusManager.isCalledNotifyEventConfigureLoginToRegisterInGlomo == true)
    }

    // MARK: - Dummy classes
    class DummyView: AnonymousTutorialViewProtocol {

        func configure(displayData: AnonymousTutorialDisplayData) {
        }

        func startAnimation() {
        }

        func stopAnimation() {
        }

        func removeAnimation() {
        }

        func reloadAnimation() {
        }

        func disableInteraction() {
        }

        func enableInteraction() {
        }

        func showError(error: ModelBO) {
        }

        func changeColorEditTexts() {
        }

        func hideNotClientButton(_ hide: Bool) {
        }
    }

    class DummyBusManager: BusManager {

        var isCalledSetRootWhenNoSession = false
        var isCalledNotifyEventConfigureLoginToRegisterInGlomo = false

        override init() {
            super.init()
            routerFactory = RouterInitFactory()
        }

        override func setRootWhenNoSession() {
            isCalledSetRootWhenNoSession = true
        }

        override func notify<T>(tag: String, _ event: ActionSpec<T>) {

            if tag == AnonymousTutorialPageReaction.ROUTER_TAG && event === AnonymousTutorialPageReactionB.EVENT_CONFIGURE_LOGIN_TO_REGISTER_IN_GLOMO {

                isCalledNotifyEventConfigureLoginToRegisterInGlomo = true
            }
        }
    }
}
