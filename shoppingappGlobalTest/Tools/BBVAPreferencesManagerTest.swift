//
//  BBVAPreferencesManagerTest.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 15/01/2018.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTCO
    @testable import shoppingappCO
#elseif TESTES
    @testable import shoppingappES
#elseif TESTMX
    @testable import shoppingappMX
#elseif TESTUS
    @testable import shoppingappUS
#elseif TESTPE
    @testable import shoppingappPE
#endif

class BBVAPreferencesManagerTest: XCTestCase {

    override func setUp() {
        super.setUp()

        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    func test_givenAnEmptyUser_whenICallSave_thenIGetEmptyUser() {

        //given
        let expectedUsername = ""
        //when
        BBVAPreferencesManager.save(value: expectedUsername, key: PreferencesManagerKeys.kUser)
        let username = BBVAPreferencesManager.get(key: PreferencesManagerKeys.kUser)

        //then
        XCTAssert(expectedUsername == username)

    }

    func test_givenAnUser_whenICallSaveUser_thenIGetCorrectUsername() {

        //given
        let expectedUsername = "expectedUsername"

        //when
        BBVAPreferencesManager.save(value: expectedUsername, key: PreferencesManagerKeys.kUser)
        let username = BBVAPreferencesManager.get(key: PreferencesManagerKeys.kUser)

        //then
        XCTAssert(expectedUsername == username)

    }

    func test_givenAny_whenICalldelete_thenIGetItemsDeleted() {

        //given
        let expectedUser = "expectedUser"

        BBVAPreferencesManager.save(value: expectedUser, key: PreferencesManagerKeys.kUser)

        //when
        BBVAPreferencesManager.delete(key: PreferencesManagerKeys.kUser)
        let user = BBVAPreferencesManager.get(key: PreferencesManagerKeys.kUser)

        //then
        XCTAssert(user.isEmpty)

    }

}
