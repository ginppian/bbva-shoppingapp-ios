//
//  DummyFinancialOverviewContractEntity.swift
//  shoppingapp
//
//  Created by jesus.martinez on 17/7/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

#if TESTMX
@testable import shoppingappMX
#elseif TESTPE
@testable import shoppingappPE
#endif

class DummyFinancialOverviewContractEntity: FinancialOverviewEntityProtocol {

    var status: Int! = 200
    var data: FinancialOverviewDataEntity? = FinancialOverviewDataEntity()

    var isCalledApplyContractRelation = false
    var productTypeSent = ""

    func applyContractRelation() {

        isCalledApplyContractRelation = true
    }
}
