//
//  ConfigGenerator.swift
//  shoppingapp
//
//  Created by AZIZEBULBUL on 26/01/2018.
//  Copyright © 2018 BBVA. All rights reserved.
//

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class ConfigGenerator {

    class func getHelpEntity() -> ConfigFilesEntity {

        let configsEntity = ConfigFilesEntity.deserialize(from: "{\"version\":\"0.1\",\"data\":[{\"id\":\"id-help-hce\",\"image\":\"http://bbva-files.s3.amazonaws.com/wallet/help/shopping_app/context_help/assets/%so%/ilu_help_mobile%size%.png\",\"help\":{\"backgroundColor\":{\"alpha\":1,\"hexString\":\"#028484\"},\"titleColor\":{\"alpha\":\"1\",\"hexString\":\"#FFFFFF\"},\"descriptionColor\":{\"alpha\":\"1\",\"hexString\":\"#FFFFFF\"},\"descriptionOnTapColor\":{\"alpha\":\"0.3\",\"hexString\":\"#FFFFFF\"}},\"section\":{\"backgroundColor\":{\"alpha\":1,\"hexString\":\"#028484\"},\"titleColor\":{\"alpha\":\"1\",\"hexString\":\"#FFFFFF\"},\"descriptionColor\":{\"alpha\":\"1\",\"hexString\":\"#FFFFFF\"},\"descriptionOnTapColor\":{\"alpha\":\"0.3\",\"hexString\":\"#FFFFFF\"}},\"platform\":\"both\",\"nfc\":true,\"texts\":[{\"language\":\"es\",\"title\":\"Todo sobre mi Pago Móvil\",\"description\":\"Más información\"},{\"language\":\"en\",\"title\":\"Todo sobre mi Pago Móvil\",\"description\":\"Más información\"}],\"sections\":[{\"sectionId\":\"cards\"},{\"sectionId\":\"promotions\"},{\"sectionId\":\"promotions_detail\"}]}]}")

        configsEntity?.status = 200

        return configsEntity!
    }

    class func getHelpBO() -> ConfigFilesBO {

        let configsBO = ConfigFilesBO(configFilesEntity: ConfigGenerator.getHelpEntity() )
        return configsBO

    }

    class func getDidacticAreasEntity() -> DidacticAreasEntity {

        let didacticAreasEntity = DidacticAreasEntity.deserialize(from: "{\"version\":\"0.1\",\"data\":[{\"helpId\":\"id-special\",\"type\":\"offer\",\"imageBackground\":\"\",\"platform\":\"both\",\"startDate\":\"2018-01-30T00:00:00.000Z\",\"endDate\":\"2018-01-31T00:00:00.000Z\",\"titleColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionOnTapColor\":{\"alpha\":0.3,\"hexString\":\"#FFFFFF\"},\"texts\":[{\"language\":\"es\",\"title\":\"Promo especial\",\"description\":\"link\"}]},{\"helpId\":\"id-help-sticker\",\"type\":\"didactic\",\"imageThumb\":\"http://bbva-files.s3.amazonaws.com/wallet/help/shopping_app/context_help/assets/%so%/ilu_sticker_%size%.png\",\"platform\":\"both\",\"nfc\":false,\"titleColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionOnTapColor\":{\"alpha\":0.3,\"hexString\":\"#FFFFFF\"},\"texts\":[{\"language\":\"es\",\"title\":\"¿Dudas con el pago sticker?\",\"description\":\"Más información\"}]},{\"helpId\":\"promotion-list\",\"type\":\"offer\"}],\"categoriesData\":{\"defaultCategory\":[{\"texts\":[{\"language\":\"es\",\"title\":\"¿Qué vas a hacer este finde?\",\"description\":\"Descubre planazos cerca de ti\"}],\"titleColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionOnTapColor\":{\"alpha\":0.3,\"hexString\":\"#FFFFFF\"},\"imageBackground\":\"http://bbva-files.s3.amazonaws.com/wallet/help/shopping_app/didactic/assets/Man-climbing-mountains.jpg\",\"categoryId\":\"HOBBIES\"}],\"saturday\":{\"morning\":[{\"texts\":[{\"language\":\"es\",\"title\":\"El finde te inspira, ¡decora tu casa!\",\"description\":\"Encuentra cerca lo que tu casa necesita.\"}],\"titleColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionOnTapColor\":{\"alpha\":0.3,\"hexString\":\"#FFFFFF\"},\"imageBackground\":\"http://bbva-files.s3.amazonaws.com/wallet/help/shopping_app/didactic/assets/blog-couple-magazine-home.jpg\",\"categoryId\":\"HOME\"}]}}}")

        return didacticAreasEntity!
    }

    class func getDidacticAreasBO() -> DidacticAreasBO {

        let didacticAreasBO = DidacticAreasBO(didacticAreasEntity: ConfigGenerator.getDidacticAreasEntity())
        return didacticAreasBO

    }

    class func getIndexsOfCategories() -> [[String: Any]] {
        return ConfigGenerator.getIndexsOfCategories(fromDidacticAreasBO: ConfigGenerator.getDidacticAreasBO())
    }

    class func getIndexsOfCategories(fromDidacticAreasBO didacticAreasBO: DidacticAreasBO) -> [[String: Any]] {

        let daysOfWeek = [DaysOfWeek.sunday.rawValue, DaysOfWeek.monday.rawValue, DaysOfWeek.tuesday.rawValue, DaysOfWeek.wednesday.rawValue, DaysOfWeek.thursday.rawValue, DaysOfWeek.friday.rawValue, DaysOfWeek.saturday.rawValue]
        let times = [TimesOfDay.morning.rawValue, TimesOfDay.afternoon.rawValue, TimesOfDay.night.rawValue]

        var itemsCategories: [[String: Any]] = []

        for day in daysOfWeek {

            if let weekday = didacticAreasBO.categories?.getTimeBO(byWeekday: day) {

                for time in times {

                    if let timeBO = weekday.getDidacticCategoryBO(byTime: time) {

                        itemsCategories.append([Constants.DidacticArea.day_of_category: "\(day)", Constants.DidacticArea.time_of_category: "\(time)", Constants.DidacticArea.index_of_category: 0, Constants.DidacticArea.count_of_category: timeBO.count])
                    }
                }
            }
        }

        return itemsCategories
    }

    class func getItemsIdSpecial() -> [DidacticAreaBO] {

        var itemsIdSpecial: [DidacticAreaBO]?
        let didacticAreasBO = ConfigGenerator.getDidacticAreasBO()

        itemsIdSpecial = didacticAreasBO.items?.filter { item -> Bool in

            if item.type == .offer {

                if (item.startDate != nil) && (item.endDate != nil) {

                    if item.platform != nil {

                        if item.platform != .both && item.platform != .ios {
                            return false
                        }
                    }

                    return true
                }
            }

            return false
        }

        return itemsIdSpecial!
    }

    class func getDidacticCategoriesEntity() -> DidacticCategoriesEntity {

        let didacticCategoriesEntity = DidacticCategoriesEntity.deserialize(from: "{\"monday\":{\"morning\":[{\"texts\":[{\"language\":\"es\",\"title\":\"¿Te apetece tomar algo calentito?\",\"description\":\"Descubre lugares cerca de ti\"}],\"titleColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionOnTapColor\":{\"alpha\":0.3,\"hexString\":\"#FFFFFF\"},\"imageBackground\":\"http://bbva-files.s3.amazonaws.com/wallet/help/shopping_app/didactic/assets/bg_dashboard_default_promo@3x.png\",\"categoryId\":\"RESTAURANTS\"}],\"afternoon\":[{\"texts\":[{\"language\":\"es\",\"title\":\"Deja el tupper para mañana. ¡Sal a comer!\",\"description\":\"Hay lugares cerca de ti\"}],\"titleColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionOnTapColor\":{\"alpha\":0.3,\"hexString\":\"#FFFFFF\"},\"imageBackground\":\"http://bbva-files.s3.amazonaws.com/wallet/help/shopping_app/didactic/assets/bg_dashboard_default_promo@3x.png\",\"categoryId\":\"RESTAURANTS\"}],\"night\":[{\"texts\":[{\"language\":\"es\",\"title\":\"¿Te da pereza cocinar?\",\"description\":\"¡Sal a cenar!\"}],\"titleColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionOnTapColor\":{\"alpha\":0.3,\"hexString\":\"#FFFFFF\"},\"imageBackground\":\"http://bbva-files.s3.amazonaws.com/wallet/help/shopping_app/didactic/assets/bg_dashboard_default_promo@3x.png\",\"categoryId\":\"RESTAURANTS\"},{\"texts\":[{\"language\":\"es\",\"title\":\"¿Y si sales a tomar algo?\",\"description\":\"Descubre sitios cerca\"}],\"titleColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionOnTapColor\":{\"alpha\":0.3,\"hexString\":\"#FFFFFF\"},\"imageBackground\":\"http://bbva-files.s3.amazonaws.com/wallet/help/shopping_app/didactic/assets/bg_dashboard_default_promo@3x.png\",\"categoryId\":\"RESTAURANTS\"}]},\"tuesday\":{\"morning\":[{\"titleColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionOnTapColor\":{\"alpha\":0.3,\"hexString\":\"#FFFFFF\"},\"imageBackground\":\"http://bbva-files.s3.amazonaws.com/wallet/help/shopping_app/didactic/assets/bg_dashboard_default_promo@3x.png\",\"categoryId\":\"RESTAURANTS\"},{\"texts\":[{\"language\":\"es\",\"title\":\"¿Te apetece tomar algo calentito?\",\"description\":\"Descubre lugares cerca de ti\"}],\"titleColor\":{},\"descriptionColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionOnTapColor\":{\"alpha\":0.3,\"hexString\":\"#FFFFFF\"},\"imageBackground\":\"http://bbva-files.s3.amazonaws.com/wallet/help/shopping_app/didactic/assets/bg_dashboard_default_promo@3x.png\",\"categoryId\":\"RESTAURANTS\"},{\"texts\":[{\"language\":\"es\",\"title\":\"¿Te apetece tomar algo calentito?\",\"description\":\"Descubre lugares cerca de ti\"}],\"titleColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionOnTapColor\":{\"alpha\":0.3,\"hexString\":\"#FFFFFF\"},\"imageBackground\":\"\",\"categoryId\":\"RESTAURANTS\"},{\"texts\":[{\"language\":\"es\",\"title\":\"¿Te apetece tomar algo calentito?\",\"description\":\"Descubre lugares cerca de ti\"}],\"titleColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionOnTapColor\":{\"alpha\":0.3,\"hexString\":\"#FFFFFF\"},\"imageBackground\":\"http://bbva-files.s3.amazonaws.com/wallet/help/shopping_app/didactic/assets/bg_dashboard_default_promo@3x.png\",\"categoryId\":\"RESTAURANTS\"},{\"texts\":[{\"language\":\"es\",\"title\":\"¿Te apetece tomar algo calentito?\",\"description\":\"Descubre lugares cerca de ti\"}],\"titleColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionOnTapColor\":{\"alpha\":0.3,\"hexString\":\"#FFFFFF\"},\"imageBackground\":\"http://bbva-files.s3.amazonaws.com/wallet/help/shopping_app/didactic/assets/bg_dashboard_default_promo@3x.png\",\"categoryId\":\"\"}],\"afternoon\":[{\"titleColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionOnTapColor\":{\"alpha\":0.3,\"hexString\":\"#FFFFFF\"},\"imageBackground\":\"http://bbva-files.s3.amazonaws.com/wallet/help/shopping_app/didactic/assets/bg_dashboard_default_promo@3x.png\",\"categoryId\":\"RESTAURANTS\"},{\"texts\":[{\"language\":\"es\",\"title\":\"¿Te apetece tomar algo calentito?\",\"description\":\"Descubre lugares cerca de ti\"}],\"titleColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionOnTapColor\":{\"alpha\":0.3,\"hexString\":\"#FFFFFF\"},\"imageBackground\":\"\",\"categoryId\":\"RESTAURANTS\"},{\"texts\":[{\"language\":\"es\",\"title\":\"¿Te apetece tomar algo calentito?\",\"description\":\"Descubre lugares cerca de ti\"}],\"titleColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionOnTapColor\":{\"alpha\":0.3,\"hexString\":\"#FFFFFF\"},\"imageBackground\":\"http://bbva-files.s3.amazonaws.com/wallet/help/shopping_app/didactic/assets/bg_dashboard_default_promo@3x.png\",\"categoryId\":\"\"},{\"texts\":[{\"language\":\"es\",\"title\":\"Deja el tupper para mañana. ¡Sal a comer!\",\"description\":\"Hay lugares cerca de ti\"}],\"titleColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionOnTapColor\":{\"alpha\":0.3,\"hexString\":\"#FFFFFF\"},\"imageBackground\":\"http://bbva-files.s3.amazonaws.com/wallet/help/shopping_app/didactic/assets/bg_dashboard_default_promo@3x.png\",\"categoryId\":\"RESTAURANTS\"}],\"night\":[{\"texts\":[{\"language\":\"es\",\"title\":\"¿Te da pereza cocinar?\",\"description\":\"¡Sal a cenar!\"}],\"titleColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionOnTapColor\":{\"alpha\":0.3,\"hexString\":\"#FFFFFF\"},\"imageBackground\":\"http://bbva-files.s3.amazonaws.com/wallet/help/shopping_app/didactic/assets/bg_dashboard_default_promo@3x.png\",\"categoryId\":\"RESTAURANTS\"},{\"texts\":[{\"language\":\"es\",\"title\":\"¿Y si sales a tomar algo?\",\"description\":\"Descubre sitios cerca\"}],\"titleColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionOnTapColor\":{\"alpha\":0.3,\"hexString\":\"#FFFFFF\"},\"imageBackground\":\"http://bbva-files.s3.amazonaws.com/wallet/help/shopping_app/didactic/assets/bg_dashboard_default_promo@3x.png\",\"categoryId\":\"RESTAURANTS\"}]},\"wednesday\":{\"morning\":[{\"texts\":[{\"language\":\"es\",\"title\":\"¿Te apetece tomar algo calentito?\",\"description\":\"Descubre lugares cerca de ti\"}],\"titleColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionOnTapColor\":{\"alpha\":0.3,\"hexString\":\"#FFFFFF\"},\"imageBackground\":\"http://bbva-files.s3.amazonaws.com/wallet/help/shopping_app/didactic/assets/bg_dashboard_default_promo@3x.png\",\"categoryId\":\"RESTAURANTS\"},{\"texts\":[{\"language\":\"es\",\"title\":\"¿Te da pereza cocinar?\",\"description\":\"¡Sal a cenar!\"}],\"titleColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionOnTapColor\":{\"alpha\":0.3,\"hexString\":\"#FFFFFF\"},\"imageBackground\":\"http://bbva-files.s3.amazonaws.com/wallet/help/shopping_app/didactic/assets/bg_dashboard_default_promo@3x.png\",\"categoryId\":\"RESTAURANTS\"},{\"texts\":[{\"language\":\"es\",\"title\":\"¿Y si sales a tomar algo?\",\"description\":\"Descubre sitios cerca\"}],\"titleColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionOnTapColor\":{\"alpha\":0.3,\"hexString\":\"#FFFFFF\"},\"imageBackground\":\"http://bbva-files.s3.amazonaws.com/wallet/help/shopping_app/didactic/assets/bg_dashboard_default_promo@3x.png\",\"categoryId\":\"RESTAURANTS\"}],\"afternoon\":[{\"texts\":[{\"language\":\"es\",\"title\":\"Deja el tupper para mañana. ¡Sal a comer!\",\"description\":\"Hay lugares cerca de ti\"}],\"titleColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionOnTapColor\":{\"alpha\":0.3,\"hexString\":\"#FFFFFF\"},\"imageBackground\":\"http://bbva-files.s3.amazonaws.com/wallet/help/shopping_app/didactic/assets/bg_dashboard_default_promo@3x.png\",\"categoryId\":\"RESTAURANTS\"}],\"night\":[{\"texts\":[{\"language\":\"es\",\"title\":\"¿Te da pereza cocinar?\",\"description\":\"¡Sal a cenar!\"}],\"titleColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionOnTapColor\":{\"alpha\":0.3,\"hexString\":\"#FFFFFF\"},\"imageBackground\":\"http://bbva-files.s3.amazonaws.com/wallet/help/shopping_app/didactic/assets/bg_dashboard_default_promo@3x.png\",\"categoryId\":\"RESTAURANTS\"},{\"texts\":[{\"language\":\"es\",\"title\":\"¿Y si sales a tomar algo?\",\"description\":\"Descubre sitios cerca\"}],\"titleColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionOnTapColor\":{\"alpha\":0.3,\"hexString\":\"#FFFFFF\"},\"imageBackground\":\"http://bbva-files.s3.amazonaws.com/wallet/help/shopping_app/didactic/assets/bg_dashboard_default_promo@3x.png\",\"categoryId\":\"RESTAURANTS\"}]},\"thursday\":{\"morning\":[{\"texts\":[{\"language\":\"es\",\"title\":\"¿Te apetece tomar algo calentito?\",\"description\":\"Descubre lugares cerca de ti\"}],\"titleColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionOnTapColor\":{\"alpha\":0.3,\"hexString\":\"#FFFFFF\"},\"imageBackground\":\"http://bbva-files.s3.amazonaws.com/wallet/help/shopping_app/didactic/assets/bg_dashboard_default_promo@3x.png\",\"categoryId\":\"RESTAURANTS\"}],\"afternoon\":[{\"texts\":[{\"language\":\"es\",\"title\":\"Deja el tupper para mañana. ¡Sal a comer!\",\"description\":\"Hay lugares cerca de ti\"}],\"titleColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionOnTapColor\":{\"alpha\":0.3,\"hexString\":\"#FFFFFF\"},\"imageBackground\":\"http://bbva-files.s3.amazonaws.com/wallet/help/shopping_app/didactic/assets/bg_dashboard_default_promo@3x.png\",\"categoryId\":\"RESTAURANTS\"}],\"night\":[{\"texts\":[{\"language\":\"es\",\"title\":\"¿Te da pereza cocinar?\",\"description\":\"¡Sal a cenar!\"}],\"titleColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionOnTapColor\":{\"alpha\":0.3,\"hexString\":\"#FFFFFF\"},\"imageBackground\":\"http://bbva-files.s3.amazonaws.com/wallet/help/shopping_app/didactic/assets/bg_dashboard_default_promo@3x.png\",\"categoryId\":\"RESTAURANTS\"},{\"texts\":[{\"language\":\"es\",\"title\":\"¿Y si sales a tomar algo?\",\"description\":\"Descubre sitios cerca\"}],\"titleColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionOnTapColor\":{\"alpha\":0.3,\"hexString\":\"#FFFFFF\"},\"imageBackground\":\"http://bbva-files.s3.amazonaws.com/wallet/help/shopping_app/didactic/assets/bg_dashboard_default_promo@3x.png\",\"categoryId\":\"RESTAURANTS\"}]},\"friday\":{\"morning\":[{\"texts\":[{\"language\":\"es\",\"title\":\"¿Te apetece tomar algo calentito?\",\"description\":\"Descubre lugares cerca de ti\"}],\"titleColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionOnTapColor\":{\"alpha\":0.3,\"hexString\":\"#FFFFFF\"},\"imageBackground\":\"http://bbva-files.s3.amazonaws.com/wallet/help/shopping_app/didactic/assets/bg_dashboard_default_promo@3x.png\",\"categoryId\":\"RESTAURANTS\"}],\"afternoon\":[{\"texts\":[{\"language\":\"es\",\"title\":\"Deja el tupper para mañana. ¡Sal a comer!\",\"description\":\"Hay lugares cerca de ti\"}],\"titleColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionOnTapColor\":{\"alpha\":0.3,\"hexString\":\"#FFFFFF\"},\"imageBackground\":\"http://bbva-files.s3.amazonaws.com/wallet/help/shopping_app/didactic/assets/bg_dashboard_default_promo@3x.png\",\"categoryId\":\"RESTAURANTS\"}],\"night\":[{\"texts\":[{\"language\":\"es\",\"title\":\"¿Te da pereza cocinar?\",\"description\":\"¡Sal a cenar!\"}],\"titleColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionOnTapColor\":{\"alpha\":0.3,\"hexString\":\"#FFFFFF\"},\"imageBackground\":\"http://bbva-files.s3.amazonaws.com/wallet/help/shopping_app/didactic/assets/bg_dashboard_default_promo@3x.png\",\"categoryId\":\"RESTAURANTS\"},{\"texts\":[{\"language\":\"es\",\"title\":\"¿Y si sales a tomar algo?\",\"description\":\"Descubre sitios cerca\"}],\"titleColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionOnTapColor\":{\"alpha\":0.3,\"hexString\":\"#FFFFFF\"},\"imageBackground\":\"http://bbva-files.s3.amazonaws.com/wallet/help/shopping_app/didactic/assets/bg_dashboard_default_promo@3x.png\",\"categoryId\":\"RESTAURANTS\"}]},\"saturday\":{\"morning\":[{\"texts\":[{\"language\":\"es\",\"title\":\"El finde te inspira, ¡decora tu casa!\",\"description\":\"Encuentra cerca lo que tu casa necesita.\"}],\"titleColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionOnTapColor\":{\"alpha\":0.3,\"hexString\":\"#FFFFFF\"},\"imageBackground\":\"http://bbva-files.s3.amazonaws.com/wallet/help/shopping_app/didactic/assets/bg_dashboard_default_promo@3x.png\",\"categoryId\":\"HOME\"}],\"afternoon\":[{\"texts\":[{\"language\":\"es\",\"title\":\"Deja el tupper para mañana. ¡Sal a comer!\",\"description\":\"Hay lugares cerca de ti\"}],\"titleColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionOnTapColor\":{\"alpha\":0.3,\"hexString\":\"#FFFFFF\"},\"imageBackground\":\"http://bbva-files.s3.amazonaws.com/wallet/help/shopping_app/didactic/assets/bg_dashboard_default_promo@3x.png\",\"categoryId\":\"RESTAURANTS\"}],\"night\":[{\"texts\":[{\"language\":\"es\",\"title\":\"¿Te da pereza cocinar?\",\"description\":\"¡Sal a cenar!\"}],\"titleColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionOnTapColor\":{\"alpha\":0.3,\"hexString\":\"#FFFFFF\"},\"imageBackground\":\"http://bbva-files.s3.amazonaws.com/wallet/help/shopping_app/didactic/assets/bg_dashboard_default_promo@3x.png\",\"categoryId\":\"RESTAURANTS\"},{\"texts\":[{\"language\":\"es\",\"title\":\"¿Y si sales a tomar algo?\",\"description\":\"Descubre sitios cerca\"}],\"titleColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionOnTapColor\":{\"alpha\":0.3,\"hexString\":\"#FFFFFF\"},\"imageBackground\":\"http://bbva-files.s3.amazonaws.com/wallet/help/shopping_app/didactic/assets/bg_dashboard_default_promo@3x.png\",\"categoryId\":\"RESTAURANTS\"}]},\"sunday\":{\"morning\":[{\"texts\":[{\"language\":\"es\",\"title\":\"¿Te apetece tomar algo calentito?\",\"description\":\"Descubre lugares cerca de ti\"}],\"titleColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionOnTapColor\":{\"alpha\":0.3,\"hexString\":\"#FFFFFF\"},\"imageBackground\":\"http://bbva-files.s3.amazonaws.com/wallet/help/shopping_app/didactic/assets/bg_dashboard_default_promo@3x.png\",\"categoryId\":\"RESTAURANTS\"}],\"afternoon\":[{\"texts\":[{\"language\":\"es\",\"title\":\"Deja el tupper para mañana. ¡Sal a comer!\",\"description\":\"Hay lugares cerca de ti\"}],\"titleColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionOnTapColor\":{\"alpha\":0.3,\"hexString\":\"#FFFFFF\"},\"imageBackground\":\"http://bbva-files.s3.amazonaws.com/wallet/help/shopping_app/didactic/assets/bg_dashboard_default_promo@3x.png\",\"categoryId\":\"RESTAURANTS\"}],\"night\":[{\"texts\":[{\"language\":\"es\",\"title\":\"¿Te da pereza cocinar?\",\"description\":\"¡Sal a cenar!\"}],\"titleColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionOnTapColor\":{\"alpha\":0.3,\"hexString\":\"#FFFFFF\"},\"imageBackground\":\"http://bbva-files.s3.amazonaws.com/wallet/help/shopping_app/didactic/assets/bg_dashboard_default_promo@3x.png\",\"categoryId\":\"RESTAURANTS\"},{\"texts\":[{\"language\":\"es\",\"title\":\"¿Y si sales a tomar algo?\",\"description\":\"Descubre sitios cerca\"}],\"titleColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionColor\":{\"alpha\":1,\"hexString\":\"#FFFFFF\"},\"descriptionOnTapColor\":{\"alpha\":0.3,\"hexString\":\"#FFFFFF\"},\"imageBackground\":\"http://bbva-files.s3.amazonaws.com/wallet/help/shopping_app/didactic/assets/bg_dashboard_default_promo@3x.png\",\"categoryId\":\"RESTAURANTS\"}]}}")

        return didacticCategoriesEntity!
    }

    class func getDidacticCategoriesBO() -> DidacticCategoriesBO {

        let didacticCategoriesBO = DidacticCategoriesBO(didacticCategoriesEntity: ConfigGenerator.getDidacticCategoriesEntity())
        return didacticCategoriesBO
    }

    class func getTimeBO(byWeekday weekday: String) -> TimeBO? {

        switch weekday {
        case DaysOfWeek.monday.rawValue:
            return ConfigGenerator.getDidacticCategoriesBO().monday
        case DaysOfWeek.tuesday.rawValue:
            return ConfigGenerator.getDidacticCategoriesBO().tuesday
        case DaysOfWeek.wednesday.rawValue:
            return ConfigGenerator.getDidacticCategoriesBO().wednesday
        case DaysOfWeek.thursday.rawValue:
            return ConfigGenerator.getDidacticCategoriesBO().thursday
        case DaysOfWeek.friday.rawValue:
            return ConfigGenerator.getDidacticCategoriesBO().friday
        case DaysOfWeek.saturday.rawValue:
            return ConfigGenerator.getDidacticCategoriesBO().saturday
        case DaysOfWeek.sunday.rawValue:
            return ConfigGenerator.getDidacticCategoriesBO().sunday
        default:
            return nil
        }
    }

    class func getWeekDayString() -> String {

        let daysOfWeek = [DaysOfWeek.sunday.rawValue, DaysOfWeek.monday.rawValue, DaysOfWeek.tuesday.rawValue, DaysOfWeek.wednesday.rawValue, DaysOfWeek.thursday.rawValue, DaysOfWeek.friday.rawValue, DaysOfWeek.saturday.rawValue]
        let date = Date()
        let calendar = Calendar.current
        let weekday = calendar.component(.weekday, from: date)

        //Weekday units are the numbers 1 through n, where n is the number of days in the week. For example, in the Gregorian calendar, n is 7 and Sunday is represented by 1.
        let weekdayString = daysOfWeek[weekday - 1]

        return weekdayString
    }

    class func getTimeByHours() -> String {

        let hour = Calendar.current.component(.hour, from: Date())

        let morning = Settings.DidacticArea.times_of_day_morning
        let afternoon = Settings.DidacticArea.times_of_day_afternoon

        switch hour {
        case morning:
            return TimesOfDay.morning.rawValue
        case afternoon:
            return TimesOfDay.afternoon.rawValue
        default:
            return TimesOfDay.night.rawValue
        }
    }
}
