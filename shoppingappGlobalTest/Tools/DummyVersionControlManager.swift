//
//  DummyVersionControlManager.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 28/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

#if TESTMX
@testable import shoppingappMX
#elseif TESTPE
@testable import shoppingappPE
#endif

class DummyVersionControlManager: VersionControlManager {
    
    var isCalledCheckVersion = false
    var publicConfigurationDTOSended: PublicConfigurationDTO?
    var shouldShowNoMandatoryUpdateSent = false
    
    class func setupMock() {
        
        super.instance = DummyVersionControlManager()
    }
    
    override func checkVersion(withPublicConfigurationDTO publicConfigurationDTO: PublicConfigurationDTO, shouldShowNoMandatoryUpdate: Bool) {
        isCalledCheckVersion = true
        publicConfigurationDTOSended = publicConfigurationDTO
        shouldShowNoMandatoryUpdateSent = shouldShowNoMandatoryUpdate
    }
}
