//
//  CustomAttributedTextTest.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 11/01/2018.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class CustomAttributedTextTest: XCTestCase {

    func test_givenAAttributeText_whenICallAttributedBoldText_thenIntroducedValueIsBold() {

        // given
        let fullText = String(format: "%@%@ %@", "Maria", Localizables.login.key_login_disconnect_confirmation_text, Localizables.login.key_login_disconnect_message_info2_text )
        let boldText = Localizables.login.key_login_disconnect_message_info2_text

        let attributedTextExpected = NSMutableAttributedString(string: fullText, attributes: [
            NSAttributedStringKey.font: Tools.setFontBook(size: 14),
            NSAttributedStringKey.foregroundColor: UIColor.BBVA600
            ])

        let alignment = NSTextAlignment.center

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 4
        paragraphStyle.alignment = alignment

        attributedTextExpected.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: attributedTextExpected.length))
        attributedTextExpected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontMedium(size: 14), range: NSRange(location: fullText.count - boldText.count, length: boldText.count))

        // when
        let attributedText = CustomAttributedText.attributedBoldText(forFullText: fullText, withBoldText: Localizables.login.key_login_disconnect_message_info2_text, withFont: Tools.setFontBook(size: 14), withBoldFont: Tools.setFontMedium(size: 14), withAlignment: alignment, andForegroundColor: UIColor.BBVA600)

        // then
        XCTAssert(attributedText == attributedTextExpected)

    }

    func test_givenAAttributeText_whenICallAttributedBoldTextThatNotMatch_thenIntroducedValueIsBold() {

        // given
        let fullText = String(format: "%@%@ %@", "Maria", Localizables.login.key_login_disconnect_confirmation_text, Localizables.login.key_login_disconnect_message_info2_text )

        let attributedTextExpected = NSMutableAttributedString(string: fullText, attributes: [
            NSAttributedStringKey.font: Tools.setFontBook(size: 14),
            NSAttributedStringKey.foregroundColor: UIColor.BBVA600
            ])
        let alignment = NSTextAlignment.center

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.lineSpacing = 4
        paragraphStyle.alignment = alignment

        attributedTextExpected.addAttribute(NSAttributedStringKey.paragraphStyle, value: paragraphStyle, range: NSRange(location: 0, length: attributedTextExpected.length))

        // when
        let attributedText = CustomAttributedText.attributedBoldText(forFullText: fullText, withBoldText: "Not match text", withFont: Tools.setFontBook(size: 14), withBoldFont: Tools.setFontMedium(size: 14), withAlignment: alignment, andForegroundColor: UIColor.BBVA600)

        // then
        XCTAssert(attributedText == attributedTextExpected)

    }

    func test_givenAttributeText_whenICallAttributedBoldText_thenIntroducedValueIsBold() {

        // given
        let fullText = String(format: "%@%@ %@", "Maria", Localizables.login.key_login_disconnect_confirmation_text, Localizables.login.key_login_disconnect_message_info2_text )
        let boldText = Localizables.login.key_login_disconnect_message_info2_text

        let attributedTextExpected = NSMutableAttributedString(string: fullText, attributes: [
            NSAttributedStringKey.font: Tools.setFontBook(size: 14),
            NSAttributedStringKey.foregroundColor: UIColor.BBVA600
            ])

        attributedTextExpected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontMedium(size: 14), range: NSRange(location: fullText.count - boldText.count, length: boldText.count))

        // when
        let attributedText = CustomAttributedText.attributedBoldText(forFullText: fullText, withBoldText: Localizables.login.key_login_disconnect_message_info2_text, withFont: Tools.setFontBook(size: 14), withBoldFont: Tools.setFontMedium(size: 14), andForegroundColor: UIColor.BBVA600)

        // then
        XCTAssert(attributedText == attributedTextExpected)

    }

    func test_givenAttributeText_whenICallAttributedBoldTextThatNotMatch_thenIntroducedValueIsBold() {

        // given
        let fullText = String(format: "%@%@ %@", "Maria", Localizables.login.key_login_disconnect_confirmation_text, Localizables.login.key_login_disconnect_message_info2_text )

        let attributedTextExpected = NSMutableAttributedString(string: fullText, attributes: [
            NSAttributedStringKey.font: Tools.setFontBook(size: 14),
            NSAttributedStringKey.foregroundColor: UIColor.BBVA600
            ])

        // when
        let attributedText = CustomAttributedText.attributedBoldText(forFullText: fullText, withBoldText: "Not match text", withFont: Tools.setFontBook(size: 14), withBoldFont: Tools.setFontMedium(size: 14), andForegroundColor: UIColor.BBVA600)

        // then
        XCTAssert(attributedText == attributedTextExpected)

    }

}
