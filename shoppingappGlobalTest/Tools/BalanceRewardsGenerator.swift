//
//  BalanceRewardsGenerator.swift
//  shoppingapp
//
//  Created by jesus.martinez on 19/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class BalanceRewardsGenerator {

    class func balanceCompleteEntity() -> BalanceRewardsEntity {

        let balancesEntity = BalanceRewardsEntity.deserialize(from: "{ \"data\": [ { \"rewardId\": \"BANCOMER_POINTS\", \"name\": \"Bancomer Points\", \"nonMonetaryValue\": 1470, \"unitType\": { \"id\": \"POINTS\", \"name\": \"Points\" }, \"nonMonetaryValueCloseToExpire\": 290, \"nonMonetaryValueExpirationDate\": \"2019-12-25\", \"isRedeemable\": false, \"nonMonetaryRedemptionSavings\": { \"amount\": 8, \"currency\": \"MXN\" }, \"balance\": { \"startDate\": \"2018-01-01\", \"endDate\": \"2018-01-02\", \"rewardBreakDown\": [ { \"id\":\"GENERATED\", \"nonMonetaryValue\":500 }, { \"id\":\"APPLIED\", \"nonMonetaryValue\":150 }, { \"id\":\"EXPIRED\", \"nonMonetaryValue\":110 }, { \"id\":\"CURRENT\", \"nonMonetaryValue\":1470 }, { \"id\":\"PREVIOUS\", \"nonMonetaryValue\":1230 } ] } } ] }")

        balancesEntity?.status = 200

        return balancesEntity!

    }

    class func balanceEmptyEntity() -> BalanceRewardsEntity {

        let balancesEntity = BalanceRewardsEntity.deserialize(from: "{ \"data\": [ {\"balance\": {} } ] }")

        balancesEntity?.status = 200

        return balancesEntity!

    }

    class func balanceWithOnlyDateEntity() -> BalanceRewardsEntity {

        let balancesEntity = BalanceRewardsEntity.deserialize(from: "{ \"data\": [ {\"balance\": { \"startDate\": \"2018-01-01\", \"endDate\": \"2018-01-02\" } } ] }")

        balancesEntity?.status = 200

        return balancesEntity!

    }

    class func balanceWithUninformedValuesEntity() -> BalanceRewardsEntity {

        let balancesEntity = BalanceRewardsEntity.deserialize(from: "{ \"data\": [ { \"rewardId\": \"BANCOMER_POINTS\", \"name\": \"Bancomer Points\", \"nonMonetaryValue\": 1470, \"unitType\": { \"id\": \"POINTS\", \"name\": \"Points\" }, \"nonMonetaryValueCloseToExpire\": 290, \"nonMonetaryValueExpirationDate\": \"2019-12-25\", \"isRedeemable\": false, \"nonMonetaryRedemptionSavings\": { \"amount\": 8, \"currency\": \"MXN\" }, \"balance\": { \"startDate\": \"\", \"endDate\": \"\", \"rewardBreakDown\": [ { \"id\":\"CURRENT\", \"nonMonetaryValue\":1470 }] } } ] }")

        balancesEntity?.status = 200

        return balancesEntity!

    }

    class func balanceCompleteBO() -> BalanceRewardsBO {

        return BalanceRewardsBO(entity: balanceCompleteEntity())

    }

    class func balanceEmptyBO() -> BalanceRewardsBO {

        return BalanceRewardsBO(entity: balanceEmptyEntity())

    }

    class func balanceWithOnlyDateBO() -> BalanceRewardsBO {

        return BalanceRewardsBO(entity: balanceWithOnlyDateEntity())

    }

    class func balanceWithUninformedValuesBO() -> BalanceRewardsBO {

        return BalanceRewardsBO(entity: balanceWithUninformedValuesEntity())

    }

}
