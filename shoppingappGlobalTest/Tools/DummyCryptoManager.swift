//
//  DummyCryptoManager.swift
//  shoppingapp
//
//  Created by jesus.martinez on 25/10/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import RxSwift

#if TESTMX
@testable import shoppingappMX
#elseif TESTPE
@testable import shoppingappPE
#endif

class DummyCryptoDataManager: LibCryptoDataManager {
    var isDecryptFromHostCalled = false
    var isgenerateKeyPairCalled = false
    var isEncryptDataForHost = false
    var forceDecryptError = false
    var forcePubKeyError = false
    
    var hostDataEntity: HostDataEntity?
    
    override func generateKeyPair(with info: String?) -> Observable<PublicKeyEntity> {
        var cryptoEntity: PublicKeyEntity
        isgenerateKeyPairCalled = true
        if forcePubKeyError == true {
            let cryptoErrorEntity = ErrorEntity("thrownError.localizedDescription", code: -1)
            let cryptoServiceError = ServiceError.GenericErrorEntity(error: cryptoErrorEntity)
            return Observable.error(cryptoServiceError)
        }
        let data = KpubData()
        data.kPubValue = "123456"
        cryptoEntity = PublicKeyEntity(keyData: data)
        return Observable<PublicKeyEntity>.just(cryptoEntity)
        
    }
    
    override func decryptFromHost(hostData: String) -> Observable<HostDataEntity> {
        var cryptoEntity: HostDataEntity
        isDecryptFromHostCalled = true
        if forceDecryptError == true {
            let errorEntity = ErrorEntity()
            errorEntity.message = "Error"
            return Observable.error(ServiceError.GenericErrorEntity(error: errorEntity))
        } else {
            cryptoEntity = HostDataEntity(data: "24249fffffffffff", check: nil)
            return Observable<HostDataEntity>.just(cryptoEntity)
        }
    }
    
    override func encryptDataForHost(data: String, keyTag: String) -> Observable<HostDataEntity> {
        
        isEncryptDataForHost = true
        
        hostDataEntity = HostDataEntity(data: "encryptedData", check: nil)
        return .just(hostDataEntity!)
    }
}
