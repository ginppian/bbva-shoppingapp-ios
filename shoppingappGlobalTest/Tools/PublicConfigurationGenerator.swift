//
//  PublicConfigurationGenerator.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 28/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

#if TESTMX
@testable import shoppingappMX
#elseif TESTPE
@testable import shoppingappPE
#endif

class PublicConfigurationGenerator {
    
    class func getPublicConfigurationJson() -> String {
        
        let json = """
        {"data":{"lastUpdate":201810301712,"publicConfig":{"hasRateApp":true,"update":{"Android":{"message":"Actualiza la app para seguir disfrutando de la mejor experiencia.","isMandatory":true,"URL":"https://play.google.com/store/apps/details?id=com.bbva.bbvawalletmx","version":"0"},"iOS":{"message":"Actualiza la app para seguir disfrutando de la mejor experiencia.","isMandatory":true,"URL":"https://itunes.apple.com/us/app/bbva-wallet-mexico/id970582311?mt=8","version":"0"}},"traceability":{"level":0},"maintenance":{"iOS":{"message":"Estamos trabajando para que BBVA Wallet funcione cada día mejor, es por eso que el servicio no está disponible en este momento. ","maintenancePeriod":{"endTime":"2018-01-20T05:55:22.000+0100","initTime":"2018-01-20T00:55:22.000+0100"}},"Android":{"message":"Estamos trabajando para que BBVA Wallet funcione cada día mejor, es por eso que el servicio no está disponible en este momento. ","maintenancePeriod":{"endTime":"2018-01-20T05:55:22.000+0100","initTime":"2018-01-20T00:55:22.000+0100"}}},"callCenterPhone":"(+52) 5552262663","characterLimits":{"movementsConcept":30,"movementsAmount":10,"limitsAmount":10,"searchPromotionsName":30,"searchPromotionsLocation":30},"help":{"contextHelp":"https://bbva-files.s3.amazonaws.com/wallet/shopping_app/context_help/MX/pro/v0/context_help.json","faq":{"helpAbout":"https://bbva-files.s3.amazonaws.com/wallet/shopping_app/faq/MX/pro/v0/helpAbout.json","helpDigitalCard":"https://bbva-files.s3.amazonaws.com/wallet/shopping_app/faq/MX/pro/v0/helpDigitalCard.json","helpHce":"https://bbva-files.s3.amazonaws.com/wallet/shopping_app/faq/MX/pro/v0/helpHce.json","helpOnOff":"https://bbva-files.s3.amazonaws.com/wallet/shopping_app/faq/MX/pro/v0/helpOnOff.json","helpPoints":"https://bbva-files.s3.amazonaws.com/wallet/shopping_app/faq/MX/pro/v0/helpPoints.json","helpPromotionsDetail":"https://bbva-files.s3.amazonaws.com/wallet/shopping_app/faq/MX/pro/v0/helpPromotionsDetail.json","helpSticker":"https://bbva-files.s3.amazonaws.com/wallet/shopping_app/faq/MX/pro/v0/helpSticker.json"},"didacticArea":"https://bbva-files.s3.amazonaws.com/wallet/shopping_app/didactic/MX/pro/v0/didactic.json"},"terms":{"cud":"https://portal.bancomer.com/fbin/repositorio/CONTRATO_UNICO_DIGITAL.pdf","tyc":"","privacy":"https://bbva-files.s3.amazonaws.com/wallet/shopping_app/terms/AvisoPrivacidadBBVABancomer_v2julio2017.pdf"},"mobileBancomer":{"Android":{"schema":"com.bancomer.mbanking","url":"https://play.google.com/store/apps/details?id=com.bancomer.mbanking"},"iOS":{"schema":"bancomer://","url":"https://itunes.apple.com/es/app/bancomer-movil/id374824226?mt=8"}},"promotionDetailContactPhone":"(+52) 5552262663","patternSMS":"abcd_012%pattern%abcd_012"}}}
        """
        
        return json
    }
    
    class func getPublicConfigurationDataContentJson() -> String {
        
        let json = """
        {"lastUpdate":201810301712,"publicConfig":{"hasRateApp":true,"update":{"Android":{"message":"Actualiza la app para seguir disfrutando de la mejor experiencia.","isMandatory":true,"URL":"https://play.google.com/store/apps/details?id=com.bbva.bbvawalletmx","version":"0"},"iOS":{"message":"Actualiza la app para seguir disfrutando de la mejor experiencia.","isMandatory":true,"URL":"https://itunes.apple.com/us/app/bbva-wallet-mexico/id970582311?mt=8","version":"0"}},"traceability":{"level":0},"maintenance":{"iOS":{"message":"Estamos trabajando para que BBVA Wallet funcione cada día mejor, es por eso que el servicio no está disponible en este momento. ","maintenancePeriod":{"endTime":"2018-01-20T05:55:22.000+0100","initTime":"2018-01-20T00:55:22.000+0100"}},"Android":{"message":"Estamos trabajando para que BBVA Wallet funcione cada día mejor, es por eso que el servicio no está disponible en este momento. ","maintenancePeriod":{"endTime":"2018-01-20T05:55:22.000+0100","initTime":"2018-01-20T00:55:22.000+0100"}}},"callCenterPhone":"(+52) 5552262663","characterLimits":{"movementsConcept":30,"movementsAmount":10,"limitsAmount":10,"searchPromotionsName":30,"searchPromotionsLocation":30},"help":{"contextHelp":"https://bbva-files.s3.amazonaws.com/wallet/shopping_app/context_help/MX/pro/v0/context_help.json","faq":{"helpAbout":"https://bbva-files.s3.amazonaws.com/wallet/shopping_app/faq/MX/pro/v0/helpAbout.json","helpDigitalCard":"https://bbva-files.s3.amazonaws.com/wallet/shopping_app/faq/MX/pro/v0/helpDigitalCard.json","helpHce":"https://bbva-files.s3.amazonaws.com/wallet/shopping_app/faq/MX/pro/v0/helpHce.json","helpOnOff":"https://bbva-files.s3.amazonaws.com/wallet/shopping_app/faq/MX/pro/v0/helpOnOff.json","helpPoints":"https://bbva-files.s3.amazonaws.com/wallet/shopping_app/faq/MX/pro/v0/helpPoints.json","helpPromotionsDetail":"https://bbva-files.s3.amazonaws.com/wallet/shopping_app/faq/MX/pro/v0/helpPromotionsDetail.json","helpSticker":"https://bbva-files.s3.amazonaws.com/wallet/shopping_app/faq/MX/pro/v0/helpSticker.json"},"didacticArea":"https://bbva-files.s3.amazonaws.com/wallet/shopping_app/didactic/MX/pro/v0/didactic.json"},"terms":{"cud":"https://portal.bancomer.com/fbin/repositorio/CONTRATO_UNICO_DIGITAL.pdf","tyc":"","privacy":"https://bbva-files.s3.amazonaws.com/wallet/shopping_app/terms/AvisoPrivacidadBBVABancomer_v2julio2017.pdf"},"mobileBancomer":{"Android":{"schema":"com.bancomer.mbanking","url":"https://play.google.com/store/apps/details?id=com.bancomer.mbanking"},"iOS":{"schema":"bancomer://","url":"https://itunes.apple.com/es/app/bancomer-movil/id374824226?mt=8"}},"promotionDetailContactPhone":"(+52) 5552262663","patternSMS":"abcd_012%pattern%abcd_012"}}
        """
        
        return json
    }
    
    class func getPublicConfiguration() -> PublicConfiguration {
        
        let json = getPublicConfigurationJson()
        
        let publicConfiguration = PublicConfiguration.deserialize(from: json)
        
        return publicConfiguration ?? PublicConfiguration()
    }
    
    class func getPublicConfigurationWithoutIOS() -> PublicConfiguration {
        
        let json = """
        {"data":{"lastUpdate":201810301712,"publicConfig":{"hasRateApp":true,"update":{"Android":{"message":"Actualiza la app para seguir disfrutando de la mejor experiencia.","isMandatory":true,"URL":"https://play.google.com/store/apps/details?id=com.bbva.bbvawalletmx&hl=es","version":"0"}},"traceability":{"level":0},"maintenance":{"iOS":{"message":"Estamos trabajando para que BBVA Wallet funcione cada día mejor, es por eso que el servicio no está disponible en este momento. ","maintenancePeriod":{"endTime":"2018-01-20T05:55:22.000+0100","initTime":"2018-01-20T00:55:22.000+0100"}},"Android":{"message":"Estamos trabajando para que BBVA Wallet funcione cada día mejor, es por eso que el servicio no está disponible en este momento. ","maintenancePeriod":{"endTime":"2018-01-20T05:55:22.000+0100","initTime":"2018-01-20T00:55:22.000+0100"}}},"callCenterPhone":"(+52) 5552262663","characterLimits":{"movementsConcept":30,"movementsAmount":10,"limitsAmount":10,"searchPromotionsName":30,"searchPromotionsLocation":30},"help":{"contextHelp":"https://bbva-files.s3.amazonaws.com/wallet/shopping_app/context_help/MX/pro/v0/context_help.json","faq":{"helpAbout":"https://bbva-files.s3.amazonaws.com/wallet/shopping_app/faq/MX/pro/v0/helpAbout.json","helpDigitalCard":"https://bbva-files.s3.amazonaws.com/wallet/shopping_app/faq/MX/pro/v0/helpDigitalCard.json","helpHce":"https://bbva-files.s3.amazonaws.com/wallet/shopping_app/faq/MX/pro/v0/helpHce.json","helpOnOff":"https://bbva-files.s3.amazonaws.com/wallet/shopping_app/faq/MX/pro/v0/helpOnOff.json","helpPoints":"https://bbva-files.s3.amazonaws.com/wallet/shopping_app/faq/MX/pro/v0/helpPoints.json","helpPromotionsDetail":"https://bbva-files.s3.amazonaws.com/wallet/shopping_app/faq/MX/pro/v0/helpPromotionsDetail.json","helpSticker":"https://bbva-files.s3.amazonaws.com/wallet/shopping_app/faq/MX/pro/v0/helpSticker.json"},"didacticArea":"https://bbva-files.s3.amazonaws.com/wallet/shopping_app/didactic/MX/pro/v0/didactic.json"},"terms":{"cud":"https://portal.bancomer.com/fbin/repositorio/CONTRATO_UNICO_DIGITAL.pdf","tyc":"","privacy":"https://bbva-files.s3.amazonaws.com/wallet/shopping_app/terms/AvisoPrivacidadBBVABancomer_v2julio2017.pdf"},"mobileBancomer":{"Android":{"schema":"com.bancomer.mbanking","url":"https://play.google.com/store/apps/details?id=com.bancomer.mbanking&hl=es"},"iOS":{"schema":"bancomer://","url":"https://itunes.apple.com/es/app/bancomer-movil/id374824226?mt=8"}},"promotionDetailContactPhone":"(+52) 5552262663","patternSMS":"abcd_012%pattern%abcd_012"}}}
        """
        
        let publicConfiguration = PublicConfiguration.deserialize(from: json)
        
        return publicConfiguration ?? PublicConfiguration()
    }
    
    class func getPublicConfigurationWithoutVersionOfUpdate() -> PublicConfiguration {
        
        let json = """
        {"data":{"lastUpdate":201810301712,"publicConfig":{"hasRateApp":true,"update":{"Android":{"message":"Actualiza la app para seguir disfrutando de la mejor experiencia.","isMandatory":true,"URL":"https://play.google.com/store/apps/details?id=com.bbva.bbvawalletmx&hl=es","version":"0"},"iOS":{"message":"Actualiza la app para seguir disfrutando de la mejor experiencia.","isMandatory":true,"URL":"https://itunes.apple.com/us/app/bbva-wallet-mexico/id970582311?mt=8"}},"traceability":{"level":0},"maintenance":{"iOS":{"message":"Estamos trabajando para que BBVA Wallet funcione cada día mejor, es por eso que el servicio no está disponible en este momento. ","maintenancePeriod":{"endTime":"2018-01-20T05:55:22.000+0100","initTime":"2018-01-20T00:55:22.000+0100"}},"Android":{"message":"Estamos trabajando para que BBVA Wallet funcione cada día mejor, es por eso que el servicio no está disponible en este momento. ","maintenancePeriod":{"endTime":"2018-01-20T05:55:22.000+0100","initTime":"2018-01-20T00:55:22.000+0100"}}},"callCenterPhone":"(+52) 5552262663","characterLimits":{"movementsConcept":30,"movementsAmount":10,"limitsAmount":10,"searchPromotionsName":30,"searchPromotionsLocation":30},"help":{"contextHelp":"https://bbva-files.s3.amazonaws.com/wallet/shopping_app/context_help/MX/pro/v0/context_help.json","faq":{"helpAbout":"https://bbva-files.s3.amazonaws.com/wallet/shopping_app/faq/MX/pro/v0/helpAbout.json","helpDigitalCard":"https://bbva-files.s3.amazonaws.com/wallet/shopping_app/faq/MX/pro/v0/helpDigitalCard.json","helpHce":"https://bbva-files.s3.amazonaws.com/wallet/shopping_app/faq/MX/pro/v0/helpHce.json","helpOnOff":"https://bbva-files.s3.amazonaws.com/wallet/shopping_app/faq/MX/pro/v0/helpOnOff.json","helpPoints":"https://bbva-files.s3.amazonaws.com/wallet/shopping_app/faq/MX/pro/v0/helpPoints.json","helpPromotionsDetail":"https://bbva-files.s3.amazonaws.com/wallet/shopping_app/faq/MX/pro/v0/helpPromotionsDetail.json","helpSticker":"https://bbva-files.s3.amazonaws.com/wallet/shopping_app/faq/MX/pro/v0/helpSticker.json"},"didacticArea":"https://bbva-files.s3.amazonaws.com/wallet/shopping_app/didactic/MX/pro/v0/didactic.json"},"terms":{"cud":"https://portal.bancomer.com/fbin/repositorio/CONTRATO_UNICO_DIGITAL.pdf","tyc":"","privacy":"https://bbva-files.s3.amazonaws.com/wallet/shopping_app/terms/AvisoPrivacidadBBVABancomer_v2julio2017.pdf"},"mobileBancomer":{"Android":{"schema":"com.bancomer.mbanking","url":"https://play.google.com/store/apps/details?id=com.bancomer.mbanking&hl=es"},"iOS":{"schema":"bancomer://","url":"https://itunes.apple.com/es/app/bancomer-movil/id374824226?mt=8"}},"promotionDetailContactPhone":"(+52) 5552262663","patternSMS":"abcd_012%pattern%abcd_012"}}}
        """
        
        let publicConfiguration = PublicConfiguration.deserialize(from: json)
        
        return publicConfiguration ?? PublicConfiguration()
    }
}
