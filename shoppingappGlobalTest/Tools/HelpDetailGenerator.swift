//
//  HelpDetailGenerator.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 17/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class HelpDetailGenerator {

    class func getHelpDetailEntity() -> HelpDetailEntity {

        let helpDetailEntity = HelpDetailEntity.deserialize(from: "{\"helpId\":\"id-help-hce\",\"image\":[{\"language\":\"es\",\"content\":\"https://bbva-files.s3.amazonaws.com/wallet/help/shopping_app/help/pagarConTuMovil.jpg\"}],\"video\":[{\"language\":\"es\",\"content\":\"https://bbva-files.s3.amazonaws.com/wallet/help/shopping_app/help/Wallet_Apagar_Encender_Tarjeta.mp4\"}],\"title\":[{\"language\":\"es\",\"content\":\"Pago con celular\"}],\"questions\":[{\"questionId\":\"id1\",\"title\":[{\"language\":\"es\",\"content\":\"¿Qué necesito para realizar pagos con mi celular?\"}],\"answers\":[{\"type\":\"text\",\"data\":[{\"language\":\"es\",\"content\":\"Si tu dispositivo usa el sistema iOS, debes tramitar tu Sticker en una sucursal Bancomer. Si tu dispositivo operativo es Android, debes asegurarte de que cuente con NFC para realizar las operaciones de pago móvil. Solo bastará con activar tu dispositivo y acercarlo a la terminal (TPV) y se hará la transacción.\"}]},{\"type\":\"video\",\"data\":[{\"language\":\"es\",\"content\":\"https://bbva-files.s3.amazonaws.com/wallet/help/shopping_app/help/Wallet_Pago_Movil.mp4\"}],\"extraData\":[{\"language\":\"es\",\"content\":\"https://upload.wikimedia.org/wikipedia/commons/f/fc/Young_Barbary_Ape_sitting_on_Plant_Pot.jpg\"}]}]},{\"questionId\":\"id2\",\"title\":[{\"language\":\"es\",\"content\":\"¿Cómo sé si mi celular sirve para hacer pagos Contactless?\"}],\"answers\":[{\"type\":\"text\",\"data\":[{\"language\":\"es\",\"content\":\"En la pantalla de inicio de sesión o login se muestra el mensaje \\\"Hacer pagos Contacless\\\". La aplicación determina, con base en el sistema operativo y si el dispositivo móvil cuenta con NFC, si muestra o no el mensaje.\"}]}]},{\"questionId\":\"id3\",\"title\":[{\"language\":\"es\",\"content\":\"¿Cómo saber si mi celular necesita un NFC o Sticker para hacer compras?\"}],\"answers\":[{\"type\":\"text\",\"data\":[{\"language\":\"es\",\"content\":\"Si puedes ver el mensaje \\\"Pago móvil\\\" dentro de tus tarjetas, significa que tu dispositivo cuenta con NFC, por lo que podrías realizar tu pago móvil. Si no ves el mensaje, deberás acudir a tu sucursal Bancomer para tramitar un Sticker y así poder realizar tus pagos.\"}]}]},{\"questionId\":\"id4\",\"title\":[{\"language\":\"es\",\"content\":\"¿Cómo deshabilito el pago móvil?\"}],\"answers\":[{\"type\":\"text\",\"data\":[{\"language\":\"es\",\"content\":\"Debes presionar el ícono de pago móvil. Sí se muestra el mensaje de color rojo, significa que se ya se ha apagado; y si se muestra el mensaje de color verde, significa que se ha encendido.\"}]}]},{\"questionId\":\"id5\",\"title\":[{\"language\":\"es\",\"content\":\"¿Qué es NFC?\"}],\"answers\":[{\"type\":\"text\",\"data\":[{\"language\":\"es\",\"content\":\"Es una tecnología inalámbrica Near Field Communication (NFC) te permite hacer pagos acercando tu celular Android a las terminales Bancomer. Solo tendrás que poner tu firma digital para confirmar la compra.\"}]}]},{\"questionId\":\"id6\",\"title\":[{\"language\":\"es\",\"content\":\"Cómo activo el pago con celular\"}],\"answers\":[{\"type\":\"text\",\"data\":[{\"language\":\"es\",\"content\":\"Desde el control de pago con celular en tu listado de tarjetas:\"}]},{\"type\":\"image\",\"data\":[{\"language\":\"es\",\"content\":\"https://bbva-files.s3.amazonaws.com/wallet/help/shopping_app/help/pagoMovilDashboard.png\"}]},{\"type\":\"text\",\"data\":[{\"language\":\"es\",\"content\":\"Desde el detalle de tarjeta de tus tarjetas Digitales:\"}]},{\"type\":\"image\",\"data\":[{\"language\":\"es\",\"content\":\"https://bbva-files.s3.amazonaws.com/wallet/help/shopping_app/help/pagoMovilLandingCards.png\"}]}]}]}")

        return helpDetailEntity!
    }

    class func getHelpDetailBO() -> HelpDetailBO {

        let helpDetailBO = HelpDetailBO(helpDetailEntity: HelpDetailGenerator.getHelpDetailEntity())

        return helpDetailBO
    }

}
