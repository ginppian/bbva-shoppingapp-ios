//
//  ShoppingParamsGenerator.swift
//  shoppingapp
//
//  Created by jesus.martinez on 31/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

#if TESTMX
@testable import shoppingappMX
#elseif TESTPE
@testable import shoppingappPE
#endif

class ShoppingParamsGenerator {

    class func distanceParam() -> ShoppingDistanceParam {

        let distance = 2
        let lengthType = "KILOMETERS"
        return ShoppingDistanceParam(distance: distance, lengthType: lengthType)

    }

    class func locationParam() -> ShoppingParamLocation {

        let latitude = 2.0
        let longitude = 3.0
        let geolocation = GeoLocationBO(withLatitude: latitude, withLongitude: longitude)
        let distanceParam = ShoppingParamsGenerator.distanceParam()
        return ShoppingParamLocation(geolocation: geolocation, distanceParam: distanceParam)

    }

    class func locationParamWithoutDistanceParam() -> ShoppingParamLocation {

        let latitude = 2.0
        let longitude = 3.0
        let geolocation = GeoLocationBO(withLatitude: latitude, withLongitude: longitude)
        return ShoppingParamLocation(geolocation: geolocation)

    }

}
