//
//  PreferencesManagerTest.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 15/01/2018.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class PreferencesManagerTest: XCTestCase {

    override func setUp() {
        
        super.setUp()
        DummyPreferencesManager.configureDummyPreferences()

        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func test_givenAnEmptyString_whenICallSaveValueWithPreferencesKey_thenIGetEmptyString() {

        //given
        let expectedString = ""

        //when
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: expectedString as AnyObject, withKey: PreferencesManagerKeys.kUser)
        let string = PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kUser) as! String

        //then
        XCTAssert(expectedString == string)
    }

    func test_givenAString_whenICallSaveValueWithPreferencesKey_thenIGetCorrectString() {

        //given
        let expectedString = "expectedString"

        //when
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: expectedString as AnyObject, withKey: PreferencesManagerKeys.kUser)
        let string = PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kUser) as! String

        //then
        XCTAssert(expectedString == string)
    }

    func test_givenString_whenICallDeleteValueWithPreferencesKey_thenIGetItemsDeleted() {

        //given
        let expectedString = "expectedString"
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: expectedString as AnyObject, withKey: PreferencesManagerKeys.kUser)

        //when
        PreferencesManager.sharedInstance().deleteValueWithPreferencesKey(forKey: PreferencesManagerKeys.kUser)
        let string = PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kUser) as AnyObject

        //then
        XCTAssert(string.isEmpty == nil)
    }
    
    func test_givenAString_whenICallSaveValue_thenIGetEmptyString() {

        //given
        let expectedStringEmpty = ""

        //when
        PreferencesManager.sharedInstance().saveValue(forValue: expectedStringEmpty as AnyObject, withKey: "expectedStringEmpty")
        let emptyString = PreferencesManager.sharedInstance().getValue(forKey: "expectedStringEmpty") as! String

        //then
        XCTAssert(expectedStringEmpty == emptyString )
    }

    func test_givenAString_whenICallSaveValue_thenIGetCorrectString() {

        //given
        let expectedString = "expectedString"

        //when
        PreferencesManager.sharedInstance().saveValue(forValue: expectedString as AnyObject, withKey: "string")
        let string = PreferencesManager.sharedInstance().getValue(forKey: "string") as! String

        //then
        XCTAssert(expectedString == string)
    }

    func test_givenAString_whenICallDeleteValue_thenIGetItemsDeleted() {

        //given
        let expectedString = "expectedString"

        PreferencesManager.sharedInstance().saveValue(forValue: expectedString as AnyObject, withKey: "expectedString")

        //when
        PreferencesManager.sharedInstance().deleteValue(forKey: "expectedString")
        let string = PreferencesManager.sharedInstance().getValue(forKey: "expectedString") as AnyObject

        //then
        XCTAssert(string.isEmpty == nil)
    }
    
    func test_givenAnInteger_whenICallSaveValue_thenIGetTheSameValue() {

        //given
        let expectedNumber = 1
        PreferencesManager.sharedInstance().saveValue(forValue: expectedNumber as AnyObject, withKey: "savedNumber")

        //when
        let savedNumber = PreferencesManager.sharedInstance().getValue(forKey: "savedNumber") as! Int

        //then
        XCTAssert(savedNumber == expectedNumber )
    }
    
    func test_givenAnInteger_whenICallDeleteValue_thenIGetItemsDeleted() {

        //given
        let expectedNumber = 2

        PreferencesManager.sharedInstance().saveValue(forValue: expectedNumber as AnyObject, withKey: "expectedNumber")

        //when
        PreferencesManager.sharedInstance().deleteValue(forKey: "expectedNumber")
        let number = PreferencesManager.sharedInstance().getValue(forKey: "expectedNumber") as AnyObject

        //then
        XCTAssert(number.isEmpty == nil)
    }
    
    func test_givenFirstRunKeyUninitialized_whenICallIsFirstRunForFirstTime_thenIGetTrue() {
        
        //given
        PreferencesManager.sharedInstance().deleteValueWithPreferencesKey(forKey: PreferencesManagerKeys.kFirstRun)
        
        //when
        let isFirstRun = PreferencesManager.sharedInstance().isFirstRun()
        
        //then
        XCTAssert(isFirstRun == true)
    }
    
    func test_givenFirstRunKeyInitialized_whenICallIsFirstRun_thenIGetFalse() {
        
        //given
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: true as AnyObject, withKey: PreferencesManagerKeys.kFirstRun)
        
        //when
        let isFirstRun = PreferencesManager.sharedInstance().isFirstRun()
        
        //then
        XCTAssert(isFirstRun == false)
    }
    
    func test_givenFirstLoginKeyUninitialized_whenICallIsFirstLoginForFirstTime_thenIGetTrue() {
        
        //given
        PreferencesManager.sharedInstance().deleteValueWithPreferencesKey(forKey: PreferencesManagerKeys.kLoginNumber)
        
        //when
        let isFirstRun = PreferencesManager.sharedInstance().isFirstLogin()
        
        //then
        XCTAssert(isFirstRun == true)
    }
    
    func test_givenFirstLoginKeyInitialized_whenICallIsFirstLogin_thenIGetValue() {
        
        //given
        let value = true
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: value as AnyObject, withKey: PreferencesManagerKeys.kFirstRun)
        
        //when
        let isFirstLogin = PreferencesManager.sharedInstance().isFirstLogin()
        
        //then
        XCTAssert(isFirstLogin == value)
    }
    
    func test_givenICallIsFirstTimeTuringCardOnForTheFirstTime_whenICallisFirstTimeTuringCardOn_thenIGetValue() {
        
        //given
        let value = true

        //when
        let isFirstTimeTuringCardOn = PreferencesManager.sharedInstance().isFirstTimeTuringCardOn()
        
        //then
        XCTAssert(isFirstTimeTuringCardOn == value)
    }
    
    func test_givenICallIsFirstTimeTurningCardOffKeyForTheFirstTime_whenICallisFirstTimeTurningCardOff_thenIGetValue() {
        
        //given
        let value = true

        //when
        let isFirstTimeTurningCardOff = PreferencesManager.sharedInstance().isFirstTimeTurningCardOff()
        
        //then
        XCTAssert(isFirstTimeTurningCardOff == value)
    }
    
    func test_givenisFirstTimeTuringCardOnKeyInitialized_whenICallisFirstTimeTuringCardOn_thenIGetValue() {
        
        //given
        let value = false
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: value as AnyObject, withKey: PreferencesManagerKeys.kSwitchCardOn)
        
        //when
        let isFirstTimeTuringCardOn = PreferencesManager.sharedInstance().isFirstTimeTuringCardOn()
        
        //then
        XCTAssert(isFirstTimeTuringCardOn == value)
    }
    
    func test_givenisisFirstTimeTurningCardOffKeyInitialized_whenICallisFirstTimeTurningCardOff_thenIGetValue() {
        
        //given
        let value = false
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: value as AnyObject, withKey: PreferencesManagerKeys.kSwitchCardOff)
        
        //when
        let isFirstTimeTurningCardOff = PreferencesManager.sharedInstance().isFirstTimeTurningCardOff()
        
        //then
        XCTAssert(isFirstTimeTurningCardOff == value)
    }

    func test_givenPreferencesManagerKeysInitialized_whenICallResetPreferencesData_thenKeysAreRemoved() {
        
        //given
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: Constants.TIMEOUT_COPY_PAN as AnyObject, withKey: PreferencesManagerKeys.kClearCopyPan)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 1), withKey: PreferencesManagerKeys.kLoginNumber)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: true as AnyObject, withKey: PreferencesManagerKeys.kDontSuggestFavoriteSelection)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 666), withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: 666), withKey: PreferencesManagerKeys.kWaitToAccessToSuggestFavoriteSelection)
            PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: "111" as AnyObject, withKey: PreferencesManagerKeys.kRegisterDeviceToken)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: true as AnyObject, withKey: PreferencesManagerKeys.kAppRated)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: true as AnyObject, withKey: PreferencesManagerKeys.kUserSkipRateApp)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: true as AnyObject, withKey: PreferencesManagerKeys.kLimitsHelpDebitCardShowed)
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: true as AnyObject, withKey: PreferencesManagerKeys.kLimitsHelpCreditCardShowed)
        
        //when
        PreferencesManager.sharedInstance().resetPreferencesData()
        let clearCopyPan = PreferencesManager.sharedInstance().getValue(forKey: PreferencesManagerKeys.kClearCopyPan.rawValue) as AnyObject
        let loginNumber = PreferencesManager.sharedInstance().getValue(forKey: PreferencesManagerKeys.kLoginNumber.rawValue) as AnyObject
        let dontSuggestFavoriteSelection = PreferencesManager.sharedInstance().getValue(forKey: PreferencesManagerKeys.kDontSuggestFavoriteSelection.rawValue) as AnyObject
        let promotionsSectionAccessCounter = PreferencesManager.sharedInstance().getValue(forKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter.rawValue) as AnyObject
        let waitToAccessToSuggestFavoriteSelection = PreferencesManager.sharedInstance().getValue(forKey: PreferencesManagerKeys.kWaitToAccessToSuggestFavoriteSelection.rawValue) as AnyObject
        let registerDeviceToken = PreferencesManager.sharedInstance().getValue(forKey: PreferencesManagerKeys.kRegisterDeviceToken.rawValue) as AnyObject
        let clearFirstTimeTurningOn = PreferencesManager.sharedInstance().getValue(forKey: PreferencesManagerKeys.kSwitchCardOn.rawValue) as AnyObject
        let clearFirstTimeTurningOff = PreferencesManager.sharedInstance().getValue(forKey: PreferencesManagerKeys.kSwitchCardOff.rawValue) as AnyObject
        let clearAppRated = PreferencesManager.sharedInstance().getValue(forKey: PreferencesManagerKeys.kAppRated.rawValue) as AnyObject
        let clearSkipRateApp = PreferencesManager.sharedInstance().getValue(forKey: PreferencesManagerKeys.kUserSkipRateApp.rawValue) as AnyObject
        let clearFirstTimeDebitShow = PreferencesManager.sharedInstance().getValue(forKey: PreferencesManagerKeys.kLimitsHelpDebitCardShowed.rawValue) as AnyObject
        let clearFirstTimeCreditShow = PreferencesManager.sharedInstance().getValue(forKey: PreferencesManagerKeys.kLimitsHelpCreditCardShowed.rawValue) as AnyObject

        //then
        XCTAssert(clearCopyPan.isEmpty == nil)
        XCTAssert(loginNumber.isEmpty == nil)
        XCTAssert(dontSuggestFavoriteSelection.isEmpty == nil)
        XCTAssert(promotionsSectionAccessCounter.isEmpty == nil)
        XCTAssert(waitToAccessToSuggestFavoriteSelection.isEmpty == nil)
        XCTAssert(registerDeviceToken.isEmpty == nil)
        XCTAssert(clearFirstTimeTurningOn.isEmpty == nil)
        XCTAssert(clearFirstTimeTurningOff.isEmpty == nil)
        XCTAssert(clearAppRated.isEmpty == nil)
        XCTAssert(clearSkipRateApp.isEmpty == nil)
        XCTAssert(clearFirstTimeDebitShow.isEmpty == nil)
        XCTAssert(clearFirstTimeCreditShow.isEmpty == nil)
    }
    
    func test_givenDontSuggestFavoriteSelectionUninitialized_whenICallGetDontSuggestFavoriteSelection_thenIGetFalse() {

        //given
        PreferencesManager.sharedInstance().deleteValueWithPreferencesKey(forKey: PreferencesManagerKeys.kDontSuggestFavoriteSelection)
        
        //when
        let dontSuggestFavoriteSelection = PreferencesManager.sharedInstance().getDontSuggestFavoriteSelection()

        //then
        XCTAssert(dontSuggestFavoriteSelection == false)
    }

    func test_givenDontSuggestFavoriteSelectionInitialized_whenICallGetDontSuggestFavoriteSelection_thenIGetValue() {
        
        let value = true
        
        //given
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: value), withKey: PreferencesManagerKeys.kDontSuggestFavoriteSelection)
        
        //when
        let retrievedValue = PreferencesManager.sharedInstance().getDontSuggestFavoriteSelection()
        
        //then
        XCTAssert(value == retrievedValue)
    }
    
    func test_givenPromotionsSectionAccessCounterUninitialized_whenICallGetPromotionsSectionAccessCounter_thenIGetZeroValue() {
        
        //given
        PreferencesManager.sharedInstance().deleteValueWithPreferencesKey(forKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        
        //when
        let accessCounter = PreferencesManager.sharedInstance().getPromotionsSectionAccessCounter()
        
        //then
        XCTAssert(accessCounter == 0)
    }
    
    func test_givenPromotionsSectionAccessCounterInitialized_whenICallGetPromotionsSectionAccessCounter_thenIGetAccessCounterValue() {
        
        let accessCounter = 3
        
        //given
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: accessCounter), withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        
        //when
        let retrievedAccessCounter = PreferencesManager.sharedInstance().getPromotionsSectionAccessCounter()
        
        //then
        XCTAssert(accessCounter == retrievedAccessCounter)
    }
    
    func test_givenPromotionsSectionAccessCounterUninitialized_whenICallIncrementPromotionsSectionAccessCounter_thenValueIsIncrementedByOne() {
        
        //given
        PreferencesManager.sharedInstance().deleteValueWithPreferencesKey(forKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        let retrievedAccessCounter = PreferencesManager.sharedInstance().getPromotionsSectionAccessCounter()
        
        //when
        PreferencesManager.sharedInstance().incrementPromotionsSectionAccessCounter()
        
        //then
        let retrievedAccessCounter2 = PreferencesManager.sharedInstance().getPromotionsSectionAccessCounter()
        XCTAssert(retrievedAccessCounter2 == retrievedAccessCounter + 1)
    }
    
    func test_givenPromotionsSectionAccessCounterInitialized_whenICallIncrementPromotionsSectionAccessCounter_thenValueIsIncrementedByOne() {
        
        let accessCounter = 3
        
        //given
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: NSNumber(value: accessCounter), withKey: PreferencesManagerKeys.kPromotionsSectionAccessCounter)
        
        //when
        PreferencesManager.sharedInstance().incrementPromotionsSectionAccessCounter()
        
        //then
        let retrievedAccessCounter = PreferencesManager.sharedInstance().getPromotionsSectionAccessCounter()
        XCTAssert(retrievedAccessCounter == accessCounter + 1)
    }
    
    func test_givenICallIsFirstTimeDebitShowLimitsHelpForTheFirstTime_whenICallisFirstTimeDebitShowLimitsHelp_thenValueMatch() {
        
        //given
        let value = true
        
        //when
        let isFirstTimeDebitShowLimitsHelp = PreferencesManager.sharedInstance().isFirstTimeDebitShowLimitsHelp()
        
        //then
        XCTAssert(isFirstTimeDebitShowLimitsHelp == value)
    }
    
    func test_givenICallIsFirstTimeCreditShowLimitsHelpForTheFirstTime_whenICallIsFirstTimeCreditShowLimitsHelp_thenValueMatch() {
        
        //given
        let value = true
        
        //when
        let isFirstTimeCreditShowLimitsHelp = PreferencesManager.sharedInstance().isFirstTimeCreditShowLimitsHelp()
        
        //then
        XCTAssert(isFirstTimeCreditShowLimitsHelp == value)
    }

    func test_givenisFirstTimeDebitShowLimitsHelpInitialized_whenICallIsFirstTimeDebitShowLimitsHelp_thenValueMatch() {
        
        //given
        let value = false
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: value as AnyObject, withKey: PreferencesManagerKeys.kLimitsHelpDebitCardShowed)
        
        //when
        let isFirstTimeDebitShowLimits = PreferencesManager.sharedInstance().isFirstTimeDebitShowLimitsHelp()
        
        //then
        XCTAssert(isFirstTimeDebitShowLimits == value)
    }
    
    func test_givenisFirstTimeCreditShowLimitsHelpInitialized_whenICallIsFirstTimeCreditShowLimitsHelp_thenValueMatch() {
        
        //given
        let value = false
        PreferencesManager.sharedInstance().saveValueWithPreferencesKey(forValue: value as AnyObject, withKey: PreferencesManagerKeys.kLimitsHelpCreditCardShowed)
        
        //when
        let isFirstTimeCreditShowLimits = PreferencesManager.sharedInstance().isFirstTimeCreditShowLimitsHelp()
        
        //then
        XCTAssert(isFirstTimeCreditShowLimits == value)
    }
    
    func test_givenAny_whenICallisFirstTimeDebitShowLimitsHelp_thenExpectedValueMatch() {
        
        //given
        
        //when
        let isFirstTimeDebitShowLimitsHelp = PreferencesManager.sharedInstance().isFirstTimeDebitShowLimitsHelp()
        let expectedValue = PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kLimitsHelpDebitCardShowed) as? Bool
        
        //then
        XCTAssert(isFirstTimeDebitShowLimitsHelp == expectedValue)
    }
    
    func test_givenAny_whenICallIsFirstTimeCreditShowLimitsHelp_thenExpectedValueMatch() {
        
        //given
        
        //when
        let isFirstTimeCreditShowLimitsHelp = PreferencesManager.sharedInstance().isFirstTimeCreditShowLimitsHelp()
        let expectedValue = PreferencesManager.sharedInstance().getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kLimitsHelpCreditCardShowed) as? Bool
        
        //then
        XCTAssert(isFirstTimeCreditShowLimitsHelp == expectedValue)
    }
}
