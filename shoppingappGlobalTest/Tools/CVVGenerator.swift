//
//  CardCVVGenerator.swift
//  shoppingapp
//
//  Created by Armando Vazquez on 5/18/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class CVVGenerator {

    class func getCVVEntityFromJSON() -> CVVEntity {
        let cvvEntity = CVVEntity.deserialize(from: "{\n  \"data\": [\n    {\n      \"id\": \"\(Settings.CardCvv.cvvId)\",\n      \"name\": \"Card Verification Value\",\n      \"code\": \"000\"\n    }\n  ],\n  \"apiInfo\": {\n    \"category\": \"products\",\n    \"name\": \"cards\",\n    \"version\": \"0.19.0\"\n  }\n}")

        return cvvEntity!
    }

    class func getCVVEntityFromJSONWithEmptyCode() -> CVVEntity {
        let cvvEntity = CVVEntity.deserialize(from: "{\n  \"data\": [\n    {\n      \"id\": \"\(Settings.CardCvv.cvvId)\",\n      \"name\": \"Card Verification Value\",\n      \"code\": \"\"\n    }\n  ],\n  \"apiInfo\": {\n    \"category\": \"products\",\n    \"name\": \"cards\",\n    \"version\": \"0.19.0\"\n  }\n}")

        return cvvEntity!
    }

    class func getCVVBO() -> CVVBO {
        let entity = getCVVEntityFromJSON()
        return CVVBO(cvvEntity: entity)
    }

    class func getSecurityBO() -> SecurityDataBO {
        let cvvBO = getCVVBO()
        return cvvBO.cardCVV!
    }

}
