//
//  NumberFormatterTests.swift
//  shoppingapp
//
//  Created by jesus.martinez on 19/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class NumberFormatterTests: XCTestCase {

    func test_givenAStringWithLocaleES_whenICallStringDecimalToStringWithSeparator_thenReturnWithComa() {

        // given
        let value = "4000000"

        // When then
        XCTAssert(NumberFormatter.stringDecimalToStringWithSeparator(withString: value, withLocale: Locale(identifier: "es_ES")) == "4.000.000")

    }

    func test_givenAStringWithLocaleUS_whenICallStringDecimalToStringWithSeparator_thenReturnWithPoint() {

        // given
        let value = "4000000"

        // When then
        XCTAssert(NumberFormatter.stringDecimalToStringWithSeparator(withString: value, withLocale: Locale(identifier: "en_US")) == "4,000,000")

    }

}
