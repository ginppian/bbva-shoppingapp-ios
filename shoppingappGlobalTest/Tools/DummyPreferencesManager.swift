//
//  DummyPreferencesManager.swift
//  shoppingappGlobal
//
//  Created by ignacio.bonafonte on 13/09/2018.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CoreLocation

#if TESTMX
@testable import shoppingappMX
#elseif TESTPE
@testable import shoppingappPE
#endif

class MockUserDefaults: UserDefaults {

    convenience init() {
        self.init(suiteName: "Mock User Defaults")!
    }

    override init?(suiteName suitename: String?) {
        UserDefaults().removePersistentDomain(forName: suitename!)
        super.init(suiteName: suitename)
    }

}

class DummyPreferencesManager: PreferencesManager {

    static var mockUserDefaults: MockUserDefaults?
    
    var keysWithValues: [String: AnyObject]?
    
    var key: String?
    var value: AnyObject?
    
    var isCalledSaveValueWithPreferencesKey = false
    var saveValueSentKey: PreferencesManagerKeys?
    var saveValueSentKeys: [PreferencesManagerKeys] = []
    var saveValueSentValue: AnyObject?
    var saveValueSentValues: [AnyObject] = []
    var isCalledGetValueWithPreferencesKey = false
    var getValueSentKey: PreferencesManagerKeys?
    var getValueSentKeys: [PreferencesManagerKeys] = []
    var isCalledDeleteValueWithPreferencesKey = false
    var deleteValueSentKey: PreferencesManagerKeys?
    var deleteValueSentKeys: [PreferencesManagerKeys] = []
    
    class func configureDummyPreferences() {
        mockUserDefaults = MockUserDefaults()
        super.instance = DummyPreferencesManager()
    }
    
    override func standard() -> UserDefaults {
        return DummyPreferencesManager.mockUserDefaults!
    }
    
    @discardableResult
    override func saveValue(forValue value: AnyObject, withKey key: String) -> Bool {
        let saved = super.saveValue(forValue: value, withKey: key)
        
        self.key = key
        self.value = value
        
        return saved
    }
    
    override func saveValueWithPreferencesKey(forValue value: AnyObject, withKey key: PreferencesManagerKeys) -> Bool {
        
        saveValueSentKey = key
        saveValueSentValue = value
        saveValueSentKeys.append(key)
        saveValueSentValues.append(value)
        
        let saved = super.saveValueWithPreferencesKey(forValue: value, withKey: key)
        
        self.key = key.rawValue
        self.value = value
        
        isCalledSaveValueWithPreferencesKey = true
        
        return saved
    }
    
    override func getValue(forKey key: String) -> AnyObject? {
        
        if let keysWithValues = keysWithValues {
            return keysWithValues[key]
        } else {
            
            guard key != self.key else {
                self.key = key
                return value
            }
            
            self.key = key
            
            return super.getValue(forKey: key)
        }
    }
    
    override func getValueWithPreferencesKey(forKey key: PreferencesManagerKeys) -> AnyObject? {
        
        getValueSentKey = key
        getValueSentKeys.append(key)
        
        if let keysWithValues = keysWithValues {
            
            isCalledGetValueWithPreferencesKey = true
            return keysWithValues[key.rawValue]
        } else {
            
            guard key.rawValue != self.key else {
                self.key = key.rawValue
                isCalledGetValueWithPreferencesKey = true
                return value
            }
            
            self.key = key.rawValue
            
            isCalledGetValueWithPreferencesKey = true
            return super.getValueWithPreferencesKey(forKey: key)
        }
    }
    
    override func deleteValueWithPreferencesKey(forKey key: PreferencesManagerKeys) -> Bool {
        
        deleteValueSentKey = key
        deleteValueSentKeys.append(key)
        
        if keysWithValues != nil {
            keysWithValues?.removeValue(forKey: key.rawValue)
        }
        
        isCalledDeleteValueWithPreferencesKey = true
        return super.deleteValueWithPreferencesKey(forKey: key)
    }

}
