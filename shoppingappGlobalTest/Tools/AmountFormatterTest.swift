//
//  AmountFormatterTest.swift
//  shoppingapp
//
//  Created by jesus.martinez on 12/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class AmountFormatterTest: XCTestCase {

    let locale = Locale(identifier: "en_US")

    var sutRequiresDecimalsAndSymbolRight: AmountFormatter!
    var sutDoesNotRequireDecimalsAndSymbolLeft: AmountFormatter!
    var sutRequiresDecimalsAndSymbolLeft: AmountFormatter!
    var sutRequiresSymbolWithMoreThanACharacter: AmountFormatter!
    var dummySessionManager: DummySessionManager!

    // MARK: AmountFormatter

    override func setUp() {
        super.setUp()
        sutRequiresDecimalsAndSymbolRight = AmountFormatter(codeCurrency: "EUR", locale: locale)
        sutDoesNotRequireDecimalsAndSymbolLeft = AmountFormatter(codeCurrency: "CLP", locale: locale)
        sutRequiresDecimalsAndSymbolLeft = AmountFormatter(codeCurrency: "USD", locale: locale)
        sutRequiresSymbolWithMoreThanACharacter = AmountFormatter(codeCurrency: "PEN", locale: locale)
        
        DummySessionManager.setupMock()
        dummySessionManager = (SessionDataManager.sessionDataInstance() as! DummySessionManager)
    }

    func test_givenALocale_whenICallDecimalFormatter_thenReturnNumberFormatterDecimalAndInCurrentLocale() {

        // given
        let locale = Locale.current

        // when
        let formatter = AmountFormatter.decimalFormatter(locale: locale)

        // then
        XCTAssert(formatter.numberStyle == .decimal)
        XCTAssert(formatter.locale == locale)

    }

    // MARK: AmountFormatter sutRequiresDecimalsAndSymbolRight

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithoutDecimalsAndNoNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithoutDecimals() {

        // given
        let amount = NSDecimalNumber(value: 20)
        let isNegative = false

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolRight.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "20€")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithoutDecimalsAndNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithoutDecimalsAndNegative() {

        // given
        let amount = NSDecimalNumber(value: 20)
        let isNegative = true

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolRight.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "-20€")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithOneDecimalLessThanFiveAndNoNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 20.3)
        let isNegative = false

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolRight.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "20.30€")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithOneDecimalEqualsToFiveAndNoNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 2000000.59)
        let isNegative = false

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolRight.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "2,000,000.59€")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithOneDecimalUpperThanFiveAndNoNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 20.8)
        let isNegative = false

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolRight.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "20.80€")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithOneDecimalLessThanFiveAndNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimalsAndNegative() {

        // given
        let amount = NSDecimalNumber(value: 20.3)
        let isNegative = true

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolRight.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "-20.30€")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithOneDecimalEqualsToFiveAndNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimalsAndNegative() {

        // given
        let amount = NSDecimalNumber(value: 20.5)
        let isNegative = true

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolRight.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "-20.50€")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithOneDecimalUpperThanFiveAndNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimalsAndNegative() {

        // given
        let amount = NSDecimalNumber(value: 20.8)
        let isNegative = true

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolRight.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "-20.80€")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithTwoDecimalsLessThanFiftyAndNoNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 20.33)
        let isNegative = false

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolRight.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "20.33€")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithTwoDecimalsEqualsToFiftyAndNoNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 20.50)
        let isNegative = false

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolRight.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "20.50€")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithTwoDecimalsUpperThanFiftyAndNoNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 20.88)
        let isNegative = false

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolRight.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "20.88€")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithTwoDecimalsLessThanFiftyAndNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimalsAndNegative() {

        // given
        let amount = NSDecimalNumber(value: 20.33)
        let isNegative = true

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolRight.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "-20.33€")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithTwoDecimalsEqualsToFiftyAndNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimalsAndNegative() {

        // given
        let amount = NSDecimalNumber(value: 20.50)
        let isNegative = true

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolRight.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "-20.50€")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithTwoDecimalsUpperThanFiftyAndNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimalsAndNegative() {

        // given
        let amount = NSDecimalNumber(value: 20.88)
        let isNegative = true

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolRight.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "-20.88€")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithSixDecimalsAndNoNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 9.425110)
        let isNegative = false

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolRight.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "9.43€")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithThreeDecimalsAndSecondPairAndNoNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 7.385)
        let isNegative = false

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolRight.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "7.38€")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithSixDecimalsAndSecondPairAndNoNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 7.385000)
        let isNegative = false

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolRight.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "7.38€")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithThreeDecimalsAndSecondNonPairAndNoNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 6.275)
        let isNegative = false

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolRight.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "6.28€")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithSixDecimalsAndSecondNonPairAndNoNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 6.275000)
        let isNegative = false

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolRight.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "6.28€")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithoutDecimalsAndNoNegative_whenICallAttributtedTextAmountIsNegativeBigFontSizeSmallFontSize_thenAttributtedTextShouldMatch() {

        // expect
        let amountFormatted = "20€"
        let offsetAmount = Tools.setFontBook(size: 16).capHeight - Tools.setFontBook(size: 12).capHeight
        let expected = NSMutableAttributedString(string: amountFormatted, attributes: [NSAttributedStringKey.font: Tools.setFontBook(size: 16)])
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 2, length: 1))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 2, length: 1))

        // given
        let amount = NSDecimalNumber(value: 20)
        let isNegative = false

        // when
        let attributtedText = sutRequiresDecimalsAndSymbolRight.attributtedText(forAmount: amount, isNegative: isNegative, bigFontSize: 16, smallFontSize: 12)

        // then
        XCTAssert(attributtedText == expected)

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithoutDecimalsAndNegative_whenICallAttributtedTextAmountIsNegativeBigFontSizeSmallFontSize_thenAttributtedTextShouldMatch() {

        // expect
        let amountFormatted = "-20€"
        let offsetAmount = Tools.setFontBook(size: 16).capHeight - Tools.setFontBook(size: 12).capHeight
        let expected = NSMutableAttributedString(string: amountFormatted, attributes: [NSAttributedStringKey.font: Tools.setFontBook(size: 16)])
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 3, length: 1))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 3, length: 1))

        // given
        let amount = NSDecimalNumber(value: 20)
        let isNegative = true

        // when
        let attributtedText = sutRequiresDecimalsAndSymbolRight.attributtedText(forAmount: amount, isNegative: isNegative, bigFontSize: 16, smallFontSize: 12)

        // then
        XCTAssert(attributtedText == expected)

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithOneDecimalAndNoNegative_whenICallAttributtedTextAmountIsNegativeBigFontSizeSmallFontSize_thenAttributtedTextShouldMatch() {

        // expect
        let amountFormatted = "2030€"
        let offsetAmount = Tools.setFontBook(size: 16).capHeight - Tools.setFontBook(size: 12).capHeight
        let expected = NSMutableAttributedString(string: amountFormatted, attributes: [NSAttributedStringKey.font: Tools.setFontBook(size: 16)])
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 2, length: 3))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 2, length: 3))

        // given
        let amount = NSDecimalNumber(value: 20.3)
        let isNegative = false

        // when
        let attributtedText = sutRequiresDecimalsAndSymbolRight.attributtedText(forAmount: amount, isNegative: isNegative, bigFontSize: 16, smallFontSize: 12)

        // then
        XCTAssert(attributtedText == expected)

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithOneDecimalAndNegative_whenICallAttributtedTextAmountIsNegativeBigFontSizeSmallFontSize_thenAttributtedTextShouldMatch() {

        // expect
        let amountFormatted = "-2030€"
        let offsetAmount = Tools.setFontBook(size: 16).capHeight - Tools.setFontBook(size: 12).capHeight
        let expected = NSMutableAttributedString(string: amountFormatted, attributes: [NSAttributedStringKey.font: Tools.setFontBook(size: 16)])
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 3, length: 3))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 3, length: 3))

        // given
        let amount = NSDecimalNumber(value: 20.3)
        let isNegative = true

        // when
        let attributtedText = sutRequiresDecimalsAndSymbolRight.attributtedText(forAmount: amount, isNegative: isNegative, bigFontSize: 16, smallFontSize: 12)

        // then
        XCTAssert(attributtedText == expected)

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithTwoDecimalsAndNoNegative_whenICallAttributtedTextAmountIsNegativeBigFontSizeSmallFontSize_thenAttributtedTextShouldMatch() {

        // expect
        let amountFormatted = "2033€"
        let offsetAmount = Tools.setFontBook(size: 16).capHeight - Tools.setFontBook(size: 12).capHeight
        let expected = NSMutableAttributedString(string: amountFormatted, attributes: [NSAttributedStringKey.font: Tools.setFontBook(size: 16)])
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 2, length: 3))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 2, length: 3))

        // given
        let amount = NSDecimalNumber(value: 20.33)
        let isNegative = false

        // when
        let attributtedText = sutRequiresDecimalsAndSymbolRight.attributtedText(forAmount: amount, isNegative: isNegative, bigFontSize: 16, smallFontSize: 12)

        // then
        XCTAssert(attributtedText == expected)

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithTwoDecimalsAndNegative_whenICallAttributtedTextAmountIsNegativeBigFontSizeSmallFontSize_thenAttributtedTextShouldMatch() {

        // expect
        let amountFormatted = "-2033€"
        let offsetAmount = Tools.setFontBook(size: 16).capHeight - Tools.setFontBook(size: 12).capHeight
        let expected = NSMutableAttributedString(string: amountFormatted, attributes: [NSAttributedStringKey.font: Tools.setFontBook(size: 16)])
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 3, length: 3))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 3, length: 3))

        // given
        let amount = NSDecimalNumber(value: 20.33)
        let isNegative = true

        // when
        let attributtedText = sutRequiresDecimalsAndSymbolRight.attributtedText(forAmount: amount, isNegative: isNegative, bigFontSize: 16, smallFontSize: 12)

        // then
        XCTAssert(attributtedText == expected)

    }

    // MARK: AmountFormatter sutRequiresDecimalsAndSymbolRight New Format

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithoutDecimalsAndNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithoutDecimals() {

        // given
        let amount = NSDecimalNumber(value: -20)

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolRight.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "-20€")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithoutDecimalsAndNoNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithoutDecimals() {

        // given
        let amount = NSDecimalNumber(value: 20)

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolRight.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "20€")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithOneDecimalLessThanFiveAndNoNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 20.3)

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolRight.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "20.30€")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithOneDecimalEqualsToFiveAndNoNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 20.5)

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolRight.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "20.50€")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithOneDecimalUpperThanFiveAndNoNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 20.8)

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolRight.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "20.80€")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithOneDecimalLessThanFiveAndNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimalsAndNegative() {

        // given
        let amount = NSDecimalNumber(value: -20.3)

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolRight.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "-20.30€")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithOneDecimalEqualsToFiveAndNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimalsAndNegative() {

        // given
        let amount = NSDecimalNumber(value: -20.5)

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolRight.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "-20.50€")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithOneDecimalUpperThanFiveAndNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimalsAndNegative() {

        // given
        let amount = NSDecimalNumber(value: -20.8)

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolRight.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "-20.80€")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithTwoDecimalsLessThanFiftyAndNoNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 20.33)

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolRight.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "20.33€")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithTwoDecimalsEqualsToFiftyAndNoNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 20.50)

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolRight.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "20.50€")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithTwoDecimalsUpperThanFiftyAndNoNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 20.88)

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolRight.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "20.88€")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithTwoDecimalsLessThanFiftyAndNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimalsAndNegative() {

        // given
        let amount = NSDecimalNumber(value: -20.33)

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolRight.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "-20.33€")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithTwoDecimalsEqualsToFiftyAndNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimalsAndNegative() {

        // given
        let amount = NSDecimalNumber(value: -20.50)

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolRight.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "-20.50€")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithTwoDecimalsUpperThanFiftyAndNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimalsAndNegative() {

        // given
        let amount = NSDecimalNumber(value: -20.88)

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolRight.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "-20.88€")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithSixDecimalsAndNoNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 9.425110)

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolRight.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "9.43€")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithThreeDecimalsAndSecondPairAndNoNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 7.385)

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolRight.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "7.38€")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithSixDecimalsAndSecondPairAndNoNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 7.385000)

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolRight.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "7.38€")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithThreeDecimalsAndSecondNonPairAndNoNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 6.275)

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolRight.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "6.28€")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithSixDecimalsAndSecondNonPairAndNoNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 6.275000)

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolRight.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "6.28€")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithSixDecimalsAndNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: -9.425110)

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolRight.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "-9.43€")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithThreeDecimalsAndSecondPairAndNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: -7.385)

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolRight.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "-7.38€")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithSixDecimalsAndSecondPairAndNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: -7.385000)

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolRight.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "-7.38€")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithThreeDecimalsAndSecondNonPairAndNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: -6.275)

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolRight.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "-6.28€")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithSixDecimalsAndSecondNonPairAndNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: -6.275000)

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolRight.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "-6.28€")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithoutDecimalsAndNoNegativeNumber_whenICallAttributtedTextAmountBigFontSizeSmallFontSize_thenAttributtedTextShouldMatch() {

        // expect
        let amountFormatted = "20€"
        let offsetAmount = Tools.setFontBook(size: 16).capHeight - Tools.setFontBook(size: 12).capHeight
        let expected = NSMutableAttributedString(string: amountFormatted, attributes: [NSAttributedStringKey.font: Tools.setFontBook(size: 16)])
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 2, length: 1))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 2, length: 1))

        // given
        let amount = NSDecimalNumber(value: 20)

        // when
        let attributtedText = sutRequiresDecimalsAndSymbolRight.attributtedText(forAmount: amount, bigFontSize: 16, smallFontSize: 12)

        // then
        XCTAssert(attributtedText == expected)

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithoutDecimalsAndNegativeNumber_whenICallAttributtedTextAmountBigFontSizeSmallFontSize_thenAttributtedTextShouldMatch() {

        // expect
        let amountFormatted = "-20€"
        let offsetAmount = Tools.setFontBook(size: 16).capHeight - Tools.setFontBook(size: 12).capHeight
        let expected = NSMutableAttributedString(string: amountFormatted, attributes: [NSAttributedStringKey.font: Tools.setFontBook(size: 16)])
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 3, length: 1))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 3, length: 1))

        // given
        let amount = NSDecimalNumber(value: -20)

        // when
        let attributtedText = sutRequiresDecimalsAndSymbolRight.attributtedText(forAmount: amount, bigFontSize: 16, smallFontSize: 12)

        // then
        XCTAssert(attributtedText == expected)

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithOneDecimalAndNoNegativeNumber_whenICallAttributtedTextAmountBigFontSizeSmallFontSize_thenAttributtedTextShouldMatch() {

        // expect
        let amountFormatted = "2030€"
        let offsetAmount = Tools.setFontBook(size: 16).capHeight - Tools.setFontBook(size: 12).capHeight
        let expected = NSMutableAttributedString(string: amountFormatted, attributes: [NSAttributedStringKey.font: Tools.setFontBook(size: 16)])
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 2, length: 3))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 2, length: 3))

        // given
        let amount = NSDecimalNumber(value: 20.3)

        // when
        let attributtedText = sutRequiresDecimalsAndSymbolRight.attributtedText(forAmount: amount, bigFontSize: 16, smallFontSize: 12)

        // then
        XCTAssert(attributtedText == expected)

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithOneDecimalAndNegativeNumber_whenICallAttributtedTextAmountBigFontSizeSmallFontSize_thenAttributtedTextShouldMatch() {

        // expect
        let amountFormatted = "-2030€"
        let offsetAmount = Tools.setFontBook(size: 16).capHeight - Tools.setFontBook(size: 12).capHeight
        let expected = NSMutableAttributedString(string: amountFormatted, attributes: [NSAttributedStringKey.font: Tools.setFontBook(size: 16)])
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 3, length: 3))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 3, length: 3))

        // given
        let amount = NSDecimalNumber(value: -20.3)

        // when
        let attributtedText = sutRequiresDecimalsAndSymbolRight.attributtedText(forAmount: amount, bigFontSize: 16, smallFontSize: 12)

        // then
        XCTAssert(attributtedText == expected)

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithTwoDecimalsAndNoNegativeNumber_whenICallAttributtedTextAmountBigFontSizeSmallFontSize_thenAttributtedTextShouldMatch() {

        // expect
        let amountFormatted = "2033€"
        let offsetAmount = Tools.setFontBook(size: 16).capHeight - Tools.setFontBook(size: 12).capHeight
        let expected = NSMutableAttributedString(string: amountFormatted, attributes: [NSAttributedStringKey.font: Tools.setFontBook(size: 16)])
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 2, length: 3))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 2, length: 3))

        // given
        let amount = NSDecimalNumber(value: 20.33)

        // when
        let attributtedText = sutRequiresDecimalsAndSymbolRight.attributtedText(forAmount: amount, bigFontSize: 16, smallFontSize: 12)

        // then
        XCTAssert(attributtedText == expected)

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsRightAnAmountWithTwoDecimalsAndNegativeNumber_whenICallAttributtedTextAmountBigFontSizeSmallFontSize_thenAttributtedTextShouldMatch() {

        // expect
        let amountFormatted = "-2033€"
        let offsetAmount = Tools.setFontBook(size: 16).capHeight - Tools.setFontBook(size: 12).capHeight
        let expected = NSMutableAttributedString(string: amountFormatted, attributes: [NSAttributedStringKey.font: Tools.setFontBook(size: 16)])
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 3, length: 3))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 3, length: 3))

        // given
        let amount = NSDecimalNumber(value: -20.33)

        // when
        let attributtedText = sutRequiresDecimalsAndSymbolRight.attributtedText(forAmount: amount, bigFontSize: 16, smallFontSize: 12)

        // then
        XCTAssert(attributtedText == expected)

    }

    // MARK: AmountFormatter sutDoesNotRequireDecimalsAndSymbolLeft

    func test_givenCurrencyThatDoNotRequiresDecimalsAndItsSymbolIsLeftAnAmountWithoutDecimalsAndNoNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithoutDecimals() {

        // given
        let amount = NSDecimalNumber(value: 20)
        let isNegative = false

        // when
        let formattedAmount = sutDoesNotRequireDecimalsAndSymbolLeft.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "$20")

    }

    func test_givenCurrencyThatDoNotRequiresDecimalsAndItsSymbolIsLeftAnAmountWithoutDecimalsAndNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithoutDecimalsAndNegative() {

        // given
        let amount = NSDecimalNumber(value: 20)
        let isNegative = true

        // when
        let formattedAmount = sutDoesNotRequireDecimalsAndSymbolLeft.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "-$20")

    }

    func test_givenCurrencyThatDoNotRequiresDecimalsAndItsSymbolIsLeftAnAmountWithOneDecimalLessThanFiveAndNoNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithoutDecimals() {

        // given
        let amount = NSDecimalNumber(value: 999.3)
        let isNegative = false

        // when
        let formattedAmount = sutDoesNotRequireDecimalsAndSymbolLeft.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "$999")

    }

    func test_givenCurrencyThatDoNotRequiresDecimalsAndItsSymbolIsLeftAnAmountWithOneDecimalEqualsToFiveAndNoNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithoutDecimals() {

        // given
        let amount = NSDecimalNumber(value: 999.5)
        let isNegative = false

        // when
        let formattedAmount = sutDoesNotRequireDecimalsAndSymbolLeft.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "$999")

    }

    func test_givenCurrencyThatDoNotRequiresDecimalsAndItsSymbolIsLeftAnAmountWithOneDecimalUpperThanFiveAndNoNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithoutDecimals() {

        // given
        let amount = NSDecimalNumber(value: 999.8)
        let isNegative = false

        // when
        let formattedAmount = sutDoesNotRequireDecimalsAndSymbolLeft.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "$999")

    }

    func test_givenCurrencyThatDoNotRequiresDecimalsAndItsSymbolIsLeftAnAmountWithOneDecimalLessThanFiveAndNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithoutDecimalsAndNegative() {

        // given
        let amount = NSDecimalNumber(value: 20.3)
        let isNegative = true

        // when
        let formattedAmount = sutDoesNotRequireDecimalsAndSymbolLeft.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "-$20")

    }

    func test_givenCurrencyThatDoNotRequiresDecimalsAndItsSymbolIsLeftAnAmountWithOneDecimalEqualsToFiveAndNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithoutDecimalsAndNegative() {

        // given
        let amount = NSDecimalNumber(value: 20.5)
        let isNegative = true

        // when
        let formattedAmount = sutDoesNotRequireDecimalsAndSymbolLeft.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "-$20")

    }

    func test_givenCurrencyThatDoNotRequiresDecimalsAndItsSymbolIsLeftAnAmountWithOneDecimalUpperToFiveAndNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithoutDecimalsAndNegative() {

        // given
        let amount = NSDecimalNumber(value: 20.9)
        let isNegative = true

        // when
        let formattedAmount = sutDoesNotRequireDecimalsAndSymbolLeft.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "-$20")

    }

    func test_givenCurrencyThatDoNotRequiresDecimalsAndItsSymbolIsLeftAnAmountWithTwoDecimalsLessThanFiftyAndNoNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithoutDecimals() {

        // given
        let amount = NSDecimalNumber(value: 20.33)
        let isNegative = false

        // when
        let formattedAmount = sutDoesNotRequireDecimalsAndSymbolLeft.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "$20")

    }

    func test_givenCurrencyThatDoNotRequiresDecimalsAndItsSymbolIsLeftAnAmountWithTwoDecimalsEqualsToFiftyAndNoNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithoutDecimals() {

        // given
        let amount = NSDecimalNumber(value: 20.50)
        let isNegative = false

        // when
        let formattedAmount = sutDoesNotRequireDecimalsAndSymbolLeft.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "$20")

    }

    func test_givenCurrencyThatDoNotRequiresDecimalsAndItsSymbolIsLeftAnAmountWithTwoDecimalsUpperThanFiveAndNoNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithoutDecimals() {

        // given
        let amount = NSDecimalNumber(value: 20.88)
        let isNegative = false

        // when
        let formattedAmount = sutDoesNotRequireDecimalsAndSymbolLeft.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "$20")

    }

    func test_givenCurrencyThatDoNotRequiresDecimalsAndItsSymbolIsLeftAnAmountWithTwoDecimalsLessThanFiftyAndNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithoutDecimalsAndNegative() {

        // given
        let amount = NSDecimalNumber(value: 20.33)
        let isNegative = true

        // when
        let formattedAmount = sutDoesNotRequireDecimalsAndSymbolLeft.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "-$20")

    }

    func test_givenCurrencyThatDoNotRequiresDecimalsAndItsSymbolIsLeftAnAmountWithTwoDecimalsEqualsToFiftyAndNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithoutDecimalsAndNegative() {

        // given
        let amount = NSDecimalNumber(value: 20.50)
        let isNegative = true

        // when
        let formattedAmount = sutDoesNotRequireDecimalsAndSymbolLeft.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "-$20")

    }

    func test_givenCurrencyThatDoNotRequiresDecimalsAndItsSymbolIsLeftAnAmountWithTwoDecimalsUpperThanFiftyAndNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithoutDecimalsAndNegative() {

        // given
        let amount = NSDecimalNumber(value: 20.88)
        let isNegative = true

        // when
        let formattedAmount = sutDoesNotRequireDecimalsAndSymbolLeft.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "-$20")

    }

    func test_givenCurrencyThatDoNotRequiresDecimalsAndItsSymbolIsRightAnAmountWithSixDecimalsAndNoNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 9.425110)
        let isNegative = false

        // when
        let formattedAmount = sutDoesNotRequireDecimalsAndSymbolLeft.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "$9")

    }

    func test_givenCurrencyThatDoNotRequiresDecimalsAndItsSymbolIsRightAnAmountWithThreeDecimalsAndSecondPairAndNoNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 7.385)
        let isNegative = false

        // when
        let formattedAmount = sutDoesNotRequireDecimalsAndSymbolLeft.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "$7")

    }

    func test_givenCurrencyThatDoNotRequiresDecimalsAndItsSymbolIsRightAnAmountWithSixDecimalsAndSecondPairAndNoNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 7.385000)
        let isNegative = false

        // when
        let formattedAmount = sutDoesNotRequireDecimalsAndSymbolLeft.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "$7")

    }

    func test_givenCurrencyThatDoNotRequiresDecimalsAndItsSymbolIsRightAnAmountWithThreeDecimalsAndSecondNonPairAndNoNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 6.275)
        let isNegative = false

        // when
        let formattedAmount = sutDoesNotRequireDecimalsAndSymbolLeft.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "$6")

    }

    func test_givenCurrencyThatDoNotRequiresDecimalsAndItsSymbolIsRightAnAmountWithSixDecimalsAndSecondNonPairAndNoNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 6.275000)
        let isNegative = false

        // when
        let formattedAmount = sutDoesNotRequireDecimalsAndSymbolLeft.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "$6")

    }

    func test_givenCurrencyThatDoNotRequiresDecimalsAndItsSymbolIsLeftAnAmountWithoutDecimalsAndNoNegative_whenICallAttributtedTextAmountIsNegativeBigFontSizeSmallFontSize_thenAttributtedTextShouldMatch() {

        // expect
        let amountFormatted = "$20"
        let offsetAmount = Tools.setFontBook(size: 16).capHeight - Tools.setFontBook(size: 12).capHeight
        let expected = NSMutableAttributedString(string: amountFormatted, attributes: [NSAttributedStringKey.font: Tools.setFontBook(size: 16)])
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 0, length: 1))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 0, length: 1))

        // given
        let amount = NSDecimalNumber(value: 20)
        let isNegative = false

        // when
        let attributtedText = sutDoesNotRequireDecimalsAndSymbolLeft.attributtedText(forAmount: amount, isNegative: isNegative, bigFontSize: 16, smallFontSize: 12)

        // then
        XCTAssert(attributtedText == expected)

    }

    func test_givenCurrencyThatDoNotRequiresDecimalsAndItsSymbolIsLeftAnAmountWithoutDecimalsAndNegative_whenICallAttributtedTextAmountIsNegativeBigFontSizeSmallFontSize_thenAttributtedTextShouldMatch() {

        // expect
        let amountFormatted = "-$20"
        let offsetAmount = Tools.setFontBook(size: 16).capHeight - Tools.setFontBook(size: 12).capHeight
        let expected = NSMutableAttributedString(string: amountFormatted, attributes: [NSAttributedStringKey.font: Tools.setFontBook(size: 16)])
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 1, length: 1))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 1, length: 1))

        // given
        let amount = NSDecimalNumber(value: 20)
        let isNegative = true

        // when
        let attributtedText = sutDoesNotRequireDecimalsAndSymbolLeft.attributtedText(forAmount: amount, isNegative: isNegative, bigFontSize: 16, smallFontSize: 12)

        // then
       XCTAssert(attributtedText == expected)

    }

    func test_givenCurrencyThatDoNotRequiresDecimalsAndItsSymbolIsLeftAnAmountWithOneDecimalAndNoNegative_whenICallAttributtedTextAmountIsNegativeBigFontSizeSmallFontSize_thenAttributtedTextShouldMatch() {

        // expect
        let amountFormatted = "$20"
        let offsetAmount = Tools.setFontBook(size: 16).capHeight - Tools.setFontBook(size: 12).capHeight
        let expected = NSMutableAttributedString(string: amountFormatted, attributes: [NSAttributedStringKey.font: Tools.setFontBook(size: 16)])
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 0, length: 1))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 0, length: 1))

        // given
        let amount = NSDecimalNumber(value: 20.3)
        let isNegative = false

        // when
        let attributtedText = sutDoesNotRequireDecimalsAndSymbolLeft.attributtedText(forAmount: amount, isNegative: isNegative, bigFontSize: 16, smallFontSize: 12)

        // then
        XCTAssert(attributtedText == expected)

    }

    func test_givenCurrencyThatDoNotRequiresDecimalsAndItsSymbolIsLeftAnAmountWithOneDecimalAndNegative_whenICallAttributtedTextAmountIsNegativeBigFontSizeSmallFontSize_thenAttributtedTextShouldMatch() {

        // expect
        let amountFormatted = "-$20"
        let offsetAmount = Tools.setFontBook(size: 16).capHeight - Tools.setFontBook(size: 12).capHeight
        let expected = NSMutableAttributedString(string: amountFormatted, attributes: [NSAttributedStringKey.font: Tools.setFontBook(size: 16)])
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 1, length: 1))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 1, length: 1))

        // given
        let amount = NSDecimalNumber(value: 20.3)
        let isNegative = true

        // when
        let attributtedText = sutDoesNotRequireDecimalsAndSymbolLeft.attributtedText(forAmount: amount, isNegative: isNegative, bigFontSize: 16, smallFontSize: 12)

        // then
        XCTAssert(attributtedText == expected)

    }

    func test_givenCurrencyThatDoNotRequiresDecimalsAndItsSymbolIsLeftAnAmountWithTwoDecimalsAndNoNegative_whenICallAttributtedTextAmountIsNegativeBigFontSizeSmallFontSize_thenAttributtedTextShouldMatch() {

        // expect
        let amountFormatted = "$20"
        let offsetAmount = Tools.setFontBook(size: 16).capHeight - Tools.setFontBook(size: 12).capHeight
        let expected = NSMutableAttributedString(string: amountFormatted, attributes: [NSAttributedStringKey.font: Tools.setFontBook(size: 16)])
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 0, length: 1))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 0, length: 1))

        // given
        let amount = NSDecimalNumber(value: 20.33)
        let isNegative = false

        // when
        let attributtedText = sutDoesNotRequireDecimalsAndSymbolLeft.attributtedText(forAmount: amount, isNegative: isNegative, bigFontSize: 16, smallFontSize: 12)

        // then
        XCTAssert(attributtedText == expected)

    }

    func test_givenCurrencyThatDoNotRequiresDecimalsAndItsSymbolIsLeftAnAmountWithTwoDecimalsAndNegative_whenICallAttributtedTextAmountIsNegativeBigFontSizeSmallFontSize_thenAttributtedTextShouldMatch() {

        // expect
        let amountFormatted = "-$20"
        let offsetAmount = Tools.setFontBook(size: 16).capHeight - Tools.setFontBook(size: 12).capHeight
        let expected = NSMutableAttributedString(string: amountFormatted, attributes: [NSAttributedStringKey.font: Tools.setFontBook(size: 16)])
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 1, length: 1))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 1, length: 1))

        // given
        let amount = NSDecimalNumber(value: 20.33)
        let isNegative = true

        // when
        let attributtedText = sutDoesNotRequireDecimalsAndSymbolLeft.attributtedText(forAmount: amount, isNegative: isNegative, bigFontSize: 16, smallFontSize: 12)

        // then
        XCTAssert(attributtedText == expected)

    }

    // MARK: AmountFormatter sutDoesNotRequireDecimalsAndSymbolLeft New Format

    func test_givenCurrencyThatDoNotRequiresDecimalsAndItsSymbolIsLeftAnAmountWithoutDecimalsAndNoNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithoutDecimals() {

        // given
        let amount = NSDecimalNumber(value: 20)

        // when
        let formattedAmount = sutDoesNotRequireDecimalsAndSymbolLeft.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "$20")

    }

    func test_givenCurrencyThatDoNotRequiresDecimalsAndItsSymbolIsLeftAnAmountWithoutDecimalsAndNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithoutDecimalsAndNegative() {

        // given
        let amount = NSDecimalNumber(value: -20)

        // when
        let formattedAmount = sutDoesNotRequireDecimalsAndSymbolLeft.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "-$20")

    }

    func test_givenCurrencyThatDoNotRequiresDecimalsAndItsSymbolIsLeftAnAmountWithOneDecimalLessThanFiveAndNoNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithoutDecimals() {

        // given
        let amount = NSDecimalNumber(value: 999.3)

        // when
        let formattedAmount = sutDoesNotRequireDecimalsAndSymbolLeft.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "$999")

    }

    func test_givenCurrencyThatDoNotRequiresDecimalsAndItsSymbolIsLeftAnAmountWithOneDecimalEqualsToFiveAndNoNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithoutDecimals() {

        // given
        let amount = NSDecimalNumber(value: 999.5)

        // when
        let formattedAmount = sutDoesNotRequireDecimalsAndSymbolLeft.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "$999")

    }

    func test_givenCurrencyThatDoNotRequiresDecimalsAndItsSymbolIsLeftAnAmountWithOneDecimalUpperThanFiveAndNoNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithoutDecimals() {

        // given
        let amount = NSDecimalNumber(value: 999.8)

        // when
        let formattedAmount = sutDoesNotRequireDecimalsAndSymbolLeft.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "$999")

    }

    func test_givenCurrencyThatDoNotRequiresDecimalsAndItsSymbolIsLeftAnAmountWithOneDecimalLessThanFiveAndNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithoutDecimalsAndNegative() {

        // given
        let amount = NSDecimalNumber(value: -20.3)

        // when
        let formattedAmount = sutDoesNotRequireDecimalsAndSymbolLeft.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "-$20")

    }

    func test_givenCurrencyThatDoNotRequiresDecimalsAndItsSymbolIsLeftAnAmountWithOneDecimalEqualsToFiveAndNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithoutDecimalsAndNegative() {

        // given
        let amount = NSDecimalNumber(value: -20.5)

        // when
        let formattedAmount = sutDoesNotRequireDecimalsAndSymbolLeft.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "-$20")

    }

    func test_givenCurrencyThatDoNotRequiresDecimalsAndItsSymbolIsLeftAnAmountWithOneDecimalUpperToFiveAndNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithoutDecimalsAndNegative() {

        // given
        let amount = NSDecimalNumber(value: -20.9)

        // when
        let formattedAmount = sutDoesNotRequireDecimalsAndSymbolLeft.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "-$20")

    }

    func test_givenCurrencyThatDoNotRequiresDecimalsAndItsSymbolIsLeftAnAmountWithTwoDecimalsLessThanFiftyAndNoNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithoutDecimals() {

        // given
        let amount = NSDecimalNumber(value: 20.33)

        // when
        let formattedAmount = sutDoesNotRequireDecimalsAndSymbolLeft.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "$20")

    }

    func test_givenCurrencyThatDoNotRequiresDecimalsAndItsSymbolIsLeftAnAmountWithTwoDecimalsEqualsToFiftyAndNoNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithoutDecimals() {

        // given
        let amount = NSDecimalNumber(value: 20.50)

        // when
        let formattedAmount = sutDoesNotRequireDecimalsAndSymbolLeft.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "$20")

    }

    func test_givenCurrencyThatDoNotRequiresDecimalsAndItsSymbolIsLeftAnAmountWithTwoDecimalsUpperThanFiveAndNoNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithoutDecimals() {

        // given
        let amount = NSDecimalNumber(value: 20.88)

        // when
        let formattedAmount = sutDoesNotRequireDecimalsAndSymbolLeft.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "$20")

    }

    func test_givenCurrencyThatDoNotRequiresDecimalsAndItsSymbolIsLeftAnAmountWithTwoDecimalsLessThanFiftyAndNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithoutDecimalsAndNegative() {

        // given
        let amount = NSDecimalNumber(value: -20.33)

        // when
        let formattedAmount = sutDoesNotRequireDecimalsAndSymbolLeft.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "-$20")

    }

    func test_givenCurrencyThatDoNotRequiresDecimalsAndItsSymbolIsLeftAnAmountWithTwoDecimalsEqualsToFiftyAndNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithoutDecimalsAndNegative() {

        // given
        let amount = NSDecimalNumber(value: -20.50)

        // when
        let formattedAmount = sutDoesNotRequireDecimalsAndSymbolLeft.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "-$20")

    }

    func test_givenCurrencyThatDoNotRequiresDecimalsAndItsSymbolIsLeftAnAmountWithTwoDecimalsUpperThanFiftyAndNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithoutDecimalsAndNegative() {

        // given
        let amount = NSDecimalNumber(value: -20.88)

        // when
        let formattedAmount = sutDoesNotRequireDecimalsAndSymbolLeft.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "-$20")

    }

    func test_givenCurrencyThatDoNotRequiresDecimalsAndItsSymbolIsLeftAnAmountWithSixDecimalsAndNoNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 9.425110)

        // when
        let formattedAmount = sutDoesNotRequireDecimalsAndSymbolLeft.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "$9")

    }

    func test_givenCurrencyThatDoNotRequiresDecimalsAndItsSymbolIsLeftAnAmountWithThreeDecimalsAndSecondPairAndNoNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 7.385)

        // when
        let formattedAmount = sutDoesNotRequireDecimalsAndSymbolLeft.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "$7")

    }

    func test_givenCurrencyThatDoNotRequiresDecimalsAndItsSymbolIsLeftAnAmountWithSixDecimalsAndSecondPairAndNoNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 7.385000)

        // when
        let formattedAmount = sutDoesNotRequireDecimalsAndSymbolLeft.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "$7")

    }

    func test_givenCurrencyThatDoNotRequiresDecimalsAndItsSymbolIsLeftAnAmountWithThreeDecimalsAndSecondNonPairAndNoNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 6.275)

        // when
        let formattedAmount = sutDoesNotRequireDecimalsAndSymbolLeft.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "$6")

    }

    func test_givenCurrencyThatDoNotRequiresDecimalsAndItsSymbolIsLeftAnAmountWithSixDecimalsAndSecondNonPairAndNoNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 6.275000)

        // when
        let formattedAmount = sutDoesNotRequireDecimalsAndSymbolLeft.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "$6")

    }

    func test_givenCurrencyThatDoNotRequiresDecimalsAndItsSymbolIsLeftAnAmountWithoutDecimalsAndNoNegativeNumber_whenICallAttributtedTextAmountBigFontSizeSmallFontSize_thenAttributtedTextShouldMatch() {

        // expect
        let amountFormatted = "$20"
        let offsetAmount = Tools.setFontBook(size: 16).capHeight - Tools.setFontBook(size: 12).capHeight
        let expected = NSMutableAttributedString(string: amountFormatted, attributes: [NSAttributedStringKey.font: Tools.setFontBook(size: 16)])
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 0, length: 1))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 0, length: 1))

        // given
        let amount = NSDecimalNumber(value: 20)

        // when
        let attributtedText = sutDoesNotRequireDecimalsAndSymbolLeft.attributtedText(forAmount: amount, bigFontSize: 16, smallFontSize: 12)

        // then
        XCTAssert(attributtedText == expected)

    }

    func test_givenCurrencyThatDoNotRequiresDecimalsAndItsSymbolIsLeftAnAmountWithoutDecimalsAndNegativeNumber_whenICallAttributtedTextAmountBigFontSizeSmallFontSize_thenAttributtedTextShouldMatch() {

        // expect
        let amountFormatted = "-$20"
        let offsetAmount = Tools.setFontBook(size: 16).capHeight - Tools.setFontBook(size: 12).capHeight
        let expected = NSMutableAttributedString(string: amountFormatted, attributes: [NSAttributedStringKey.font: Tools.setFontBook(size: 16)])
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 1, length: 1))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 1, length: 1))

        // given
        let amount = NSDecimalNumber(value: -20)

        // when
        let attributtedText = sutDoesNotRequireDecimalsAndSymbolLeft.attributtedText(forAmount: amount, bigFontSize: 16, smallFontSize: 12)

        // then
        XCTAssert(attributtedText == expected)

    }

    func test_givenCurrencyThatDoNotRequiresDecimalsAndItsSymbolIsLeftAnAmountWithOneDecimalAndNoNegativeNumber_whenICallAttributtedTextAmountBigFontSizeSmallFontSize_thenAttributtedTextShouldMatch() {

        // expect
        let amountFormatted = "$20"
        let offsetAmount = Tools.setFontBook(size: 16).capHeight - Tools.setFontBook(size: 12).capHeight
        let expected = NSMutableAttributedString(string: amountFormatted, attributes: [NSAttributedStringKey.font: Tools.setFontBook(size: 16)])
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 0, length: 1))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 0, length: 1))

        // given
        let amount = NSDecimalNumber(value: 20.3)

        // when
        let attributtedText = sutDoesNotRequireDecimalsAndSymbolLeft.attributtedText(forAmount: amount, bigFontSize: 16, smallFontSize: 12)

        // then
        XCTAssert(attributtedText == expected)

    }

    func test_givenCurrencyThatDoNotRequiresDecimalsAndItsSymbolIsLeftAnAmountWithOneDecimalAndNegativeNumber_whenICallAttributtedTextAmountBigFontSizeSmallFontSize_thenAttributtedTextShouldMatch() {

        // expect
        let amountFormatted = "-$20"
        let offsetAmount = Tools.setFontBook(size: 16).capHeight - Tools.setFontBook(size: 12).capHeight
        let expected = NSMutableAttributedString(string: amountFormatted, attributes: [NSAttributedStringKey.font: Tools.setFontBook(size: 16)])
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 1, length: 1))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 1, length: 1))

        // given
        let amount = NSDecimalNumber(value: -20.3)

        // when
        let attributtedText = sutDoesNotRequireDecimalsAndSymbolLeft.attributtedText(forAmount: amount, bigFontSize: 16, smallFontSize: 12)

        // then
        XCTAssert(attributtedText == expected)

    }

    func test_givenCurrencyThatDoNotRequiresDecimalsAndItsSymbolIsLeftAnAmountWithTwoDecimalsAndNoNegativeNumber_whenICallAttributtedTextAmountBigFontSizeSmallFontSize_thenAttributtedTextShouldMatch() {

        // expect
        let amountFormatted = "$20"
        let offsetAmount = Tools.setFontBook(size: 16).capHeight - Tools.setFontBook(size: 12).capHeight
        let expected = NSMutableAttributedString(string: amountFormatted, attributes: [NSAttributedStringKey.font: Tools.setFontBook(size: 16)])
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 0, length: 1))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 0, length: 1))

        // given
        let amount = NSDecimalNumber(value: 20.33)

        // when
        let attributtedText = sutDoesNotRequireDecimalsAndSymbolLeft.attributtedText(forAmount: amount, bigFontSize: 16, smallFontSize: 12)

        // then
        XCTAssert(attributtedText == expected)

    }

    func test_givenCurrencyThatDoNotRequiresDecimalsAndItsSymbolIsLeftAnAmountWithTwoDecimalsAndNegativeNumber_whenICallAttributtedTextAmountBigFontSizeSmallFontSize_thenAttributtedTextShouldMatch() {

        // expect
        let amountFormatted = "-$20"
        let offsetAmount = Tools.setFontBook(size: 16).capHeight - Tools.setFontBook(size: 12).capHeight
        let expected = NSMutableAttributedString(string: amountFormatted, attributes: [NSAttributedStringKey.font: Tools.setFontBook(size: 16)])
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 1, length: 1))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 1, length: 1))

        // given
        let amount = NSDecimalNumber(value: -20.33)

        // when
        let attributtedText = sutDoesNotRequireDecimalsAndSymbolLeft.attributtedText(forAmount: amount, bigFontSize: 16, smallFontSize: 12)

        // then
        XCTAssert(attributtedText == expected)

    }

    // MARK: AmountFormatter sutRequiresDecimalsAndSymbolLeft

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAnAmountWithoutDecimalsAndNoNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithoutDecimals() {

        // given
        let amount = NSDecimalNumber(value: 20)
        let isNegative = false

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolLeft.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "$20")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAnAmountWithoutDecimalsAndNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithoutDecimalsAndNegative() {

        // given
        let amount = NSDecimalNumber(value: 20)
        let isNegative = true

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolLeft.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "-$20")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAnAmountWithOneDecimalLessThanFiveAndNoNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 20.3)
        let isNegative = false

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolLeft.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "$20.30")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAnAmountWithOneDecimalEqualsToFiveAndNoNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 20.5)
        let isNegative = false

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolLeft.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "$20.50")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAnAmountWithOneDecimalUpperThanFiveAndNoNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 20.9)
        let isNegative = false

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolLeft.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "$20.90")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAnAmountWithOneDecimalLessThanFiveAndNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimalsAndNegative() {

        // given
        let amount = NSDecimalNumber(value: 20.3)
        let isNegative = true

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolLeft.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "-$20.30")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAnAmountWithOneDecimalEqualsToFiveAndNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimalsAndNegative() {

        // given
        let amount = NSDecimalNumber(value: 20.5)
        let isNegative = true

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolLeft.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "-$20.50")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAnAmountWithOneDecimalUpperToFiveAndNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimalsAndNegative() {

        // given
        let amount = NSDecimalNumber(value: 20.8)
        let isNegative = true

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolLeft.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "-$20.80")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAnAmountWithTwoDecimalsLessThanFiftyAndNoNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 20.33)
        let isNegative = false

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolLeft.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "$20.33")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAnAmountWithTwoDecimalsEqualsToFiftyAndNoNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 20.50)
        let isNegative = false

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolLeft.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "$20.50")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAnAmountWithTwoDecimalsUpperThanFiftyAndNoNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 20.99)
        let isNegative = false

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolLeft.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "$20.99")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAnAmountWithTwoDecimalsLessThanFiftyAndNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimalsAndNegative() {

        // given
        let amount = NSDecimalNumber(value: 20.33)
        let isNegative = true

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolLeft.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "-$20.33")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAnAmountWithTwoDecimalsEqualsToFiftyAndNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimalsAndNegative() {

        // given
        let amount = NSDecimalNumber(value: 20.50)
        let isNegative = true

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolLeft.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "-$20.50")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAnAmountWithTwoDecimalsUpperThanFiftyAndNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimalsAndNegative() {

        // given
        let amount = NSDecimalNumber(value: 20.99)
        let isNegative = true

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolLeft.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "-$20.99")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAnAmountWithSixDecimalsAndNoNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 9.425110)
        let isNegative = false

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolLeft.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "$9.43")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAnAmountWithThreeDecimalsAndSecondPairAndNoNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 7.385)
        let isNegative = false

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolLeft.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "$7.38")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAnAmountWithSixDecimalsAndSecondPairAndNoNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 7.385000)
        let isNegative = false

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolLeft.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "$7.38")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAnAmountWithThreeDecimalsAndSecondNonPairAndNoNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 6.275)
        let isNegative = false

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolLeft.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "$6.28")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAnAmountWithSixDecimalsAndSecondNonPairAndNoNegative_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 6.275000)
        let isNegative = false

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolLeft.format(amount: amount, isNegative: isNegative)

        // then
        XCTAssert(formattedAmount == "$6.28")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAnAmountWithoutDecimalsAndNoNegative_whenICallAttributtedTextAmountIsNegativeBigFontSizeSmallFontSize_thenAttributtedTextShouldMatch() {

        // expect
        let amountFormatted = "$20"
        let offsetAmount = Tools.setFontBook(size: 16).capHeight - Tools.setFontBook(size: 12).capHeight
        let expected = NSMutableAttributedString(string: amountFormatted, attributes: [NSAttributedStringKey.font: Tools.setFontBook(size: 16)])
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 0, length: 1))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 0, length: 1))

        // given
        let amount = NSDecimalNumber(value: 20)
        let isNegative = false

        // when
        let attributtedText = sutRequiresDecimalsAndSymbolLeft.attributtedText(forAmount: amount, isNegative: isNegative, bigFontSize: 16, smallFontSize: 12)

        // then
        XCTAssert(attributtedText == expected)

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAnAmountWithoutDecimalsAndNegative_whenICallAttributtedTextAmountIsNegativeBigFontSizeSmallFontSize_thenAttributtedTextShouldMatch() {

        // expect
        let amountFormatted = "-$20"
        let offsetAmount = Tools.setFontBook(size: 16).capHeight - Tools.setFontBook(size: 12).capHeight
        let expected = NSMutableAttributedString(string: amountFormatted, attributes: [NSAttributedStringKey.font: Tools.setFontBook(size: 16)])
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 1, length: 1))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 1, length: 1))
        // given
        let amount = NSDecimalNumber(value: 20)
        let isNegative = true

        // when
        let attributtedText = sutRequiresDecimalsAndSymbolLeft.attributtedText(forAmount: amount, isNegative: isNegative, bigFontSize: 16, smallFontSize: 12)

        // then
        XCTAssert(attributtedText == expected)

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAnAmountWithOneDecimalAndNoNegative_whenICallAttributtedTextAmountIsNegativeBigFontSizeSmallFontSize_thenAttributtedTextShouldMatch() {

        // expect
        let amountFormatted = "$2030"
        let offsetAmount = Tools.setFontBook(size: 16).capHeight - Tools.setFontBook(size: 12).capHeight
        let expected = NSMutableAttributedString(string: amountFormatted, attributes: [NSAttributedStringKey.font: Tools.setFontBook(size: 16)])
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 0, length: 1))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 0, length: 1))
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 3, length: 2))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 3, length: 2))

        // given
        let amount = NSDecimalNumber(value: 20.3)
        let isNegative = false

        // when
        let attributtedText = sutRequiresDecimalsAndSymbolLeft.attributtedText(forAmount: amount, isNegative: isNegative, bigFontSize: 16, smallFontSize: 12)

        // then
        XCTAssert(attributtedText == expected)

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAnAmountWithOneDecimalAndNegative_whenICallAttributtedTextAmountIsNegativeBigFontSizeSmallFontSize_thenAttributtedTextShouldMatch() {

        // expect
        let amountFormatted = "-$2030"
        let offsetAmount = Tools.setFontBook(size: 16).capHeight - Tools.setFontBook(size: 12).capHeight
        let expected = NSMutableAttributedString(string: amountFormatted, attributes: [NSAttributedStringKey.font: Tools.setFontBook(size: 16)])
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 1, length: 1))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 1, length: 1))
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 4, length: 2))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 4, length: 2))

        // given
        let amount = NSDecimalNumber(value: 20.3)
        let isNegative = true

        // when
        let attributtedText = sutRequiresDecimalsAndSymbolLeft.attributtedText(forAmount: amount, isNegative: isNegative, bigFontSize: 16, smallFontSize: 12)

        // then
        XCTAssert(attributtedText == expected)

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAnAmountWithTwoDecimalsAndNoNegative_whenICallAttributtedTextAmountIsNegativeBigFontSizeSmallFontSize_thenAttributtedTextShouldMatch() {

        // expect
        let amountFormatted = "$2033"
        let offsetAmount = Tools.setFontBook(size: 16).capHeight - Tools.setFontBook(size: 12).capHeight
        let expected = NSMutableAttributedString(string: amountFormatted, attributes: [NSAttributedStringKey.font: Tools.setFontBook(size: 16)])
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 0, length: 1))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 0, length: 1))
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 3, length: 2))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 3, length: 2))

        // given
        let amount = NSDecimalNumber(value: 20.33)
        let isNegative = false

        // when
        let attributtedText = sutRequiresDecimalsAndSymbolLeft.attributtedText(forAmount: amount, isNegative: isNegative, bigFontSize: 16, smallFontSize: 12)

        // then
        XCTAssert(attributtedText == expected)

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAnAmountWithTwoDecimalsAndNegative_whenICallAttributtedTextAmountIsNegativeBigFontSizeSmallFontSize_thenAttributtedTextShouldMatch() {

        // expect
        let amountFormatted = "-$2033"
        let offsetAmount = Tools.setFontBook(size: 16).capHeight - Tools.setFontBook(size: 12).capHeight
        let expected = NSMutableAttributedString(string: amountFormatted, attributes: [NSAttributedStringKey.font: Tools.setFontBook(size: 16)])
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 1, length: 1))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 1, length: 1))
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 4, length: 2))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 4, length: 2))

        // given
        let amount = NSDecimalNumber(value: 20.33)
        let isNegative = true

        // when
        let attributtedText = sutRequiresDecimalsAndSymbolLeft.attributtedText(forAmount: amount, isNegative: isNegative, bigFontSize: 16, smallFontSize: 12)

        // then
        XCTAssert(attributtedText == expected)

    }

    // MARK: AmountFormatter sutRequiresDecimalsAndSymbolLeft New Format

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAnAmountWithoutDecimalsAndNoNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithoutDecimals() {

        // given
        let amount = NSDecimalNumber(value: 20)

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolLeft.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "$20")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAnAmountWithoutDecimalsAndNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithoutDecimalsAndNegative() {

        // given
        let amount = NSDecimalNumber(value: -20)

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolLeft.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "-$20")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAnAmountWithOneDecimalLessThanFiveAndNoNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 20.3)

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolLeft.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "$20.30")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAnAmountWithOneDecimalEqualsToFiveAndNoNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 20.5)

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolLeft.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "$20.50")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAnAmountWithOneDecimalUpperThanFiveAndNoNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 20.9)

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolLeft.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "$20.90")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAnAmountWithOneDecimalLessThanFiveAndNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimalsAndNegative() {

        // given
        let amount = NSDecimalNumber(value: -20.3)

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolLeft.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "-$20.30")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAnAmountWithOneDecimalEqualsToFiveAndNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimalsAndNegative() {

        // given
        let amount = NSDecimalNumber(value: -20.5)

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolLeft.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "-$20.50")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAnAmountWithOneDecimalUpperToFiveAndNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimalsAndNegative() {

        // given
        let amount = NSDecimalNumber(value: -20.8)

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolLeft.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "-$20.80")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAnAmountWithTwoDecimalsLessThanFiftyAndNoNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 20.33)

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolLeft.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "$20.33")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAnAmountWithTwoDecimalsEqualsToFiftyAndNoNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 20.50)

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolLeft.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "$20.50")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAnAmountWithTwoDecimalsUpperThanFiftyAndNoNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 20.99)

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolLeft.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "$20.99")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAnAmountWithTwoDecimalsLessThanFiftyAndNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimalsAndNegative() {

        // given
        let amount = NSDecimalNumber(value: -20.33)

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolLeft.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "-$20.33")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAnAmountWithTwoDecimalsEqualsToFiftyAndNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimalsAndNegative() {

        // given
        let amount = NSDecimalNumber(value: -20.50)

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolLeft.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "-$20.50")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAnAmountWithTwoDecimalsUpperThanFiftyAndNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimalsAndNegative() {

        // given
        let amount = NSDecimalNumber(value: -20.99)

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolLeft.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "-$20.99")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAnAmountWithSixDecimalsAndNoNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 9.425110)

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolLeft.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "$9.43")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAnAmountWithThreeDecimalsAndSecondPairAndNoNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 7.385)

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolLeft.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "$7.38")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAnAmountWithSixDecimalsAndSecondPairAndNoNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 7.385000)

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolLeft.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "$7.38")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAnAmountWithThreeDecimalsAndSecondNonPairAndNoNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 6.275)

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolLeft.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "$6.28")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAnAmountWithSixDecimalsAndSecondNonPairAndNoNegativeNumber_whenICallFormatAmountIsNegative_thenReturnAmountFormattedWithItsSymbolAndWithDecimals() {

        // given
        let amount = NSDecimalNumber(value: 6.275000)

        // when
        let formattedAmount = sutRequiresDecimalsAndSymbolLeft.format(amount: amount)

        // then
        XCTAssert(formattedAmount == "$6.28")

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAnAmountWithoutDecimalsAndNoNegativeNumber_whenICallAttributtedTextAmountBigFontSizeSmallFontSize_thenAttributtedTextShouldMatch() {

        // expect
        let amountFormatted = "$20"
        let offsetAmount = Tools.setFontBook(size: 16).capHeight - Tools.setFontBook(size: 12).capHeight
        let expected = NSMutableAttributedString(string: amountFormatted, attributes: [NSAttributedStringKey.font: Tools.setFontBook(size: 16)])
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 0, length: 1))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 0, length: 1))

        // given
        let amount = NSDecimalNumber(value: 20)

        // when
        let attributtedText = sutRequiresDecimalsAndSymbolLeft.attributtedText(forAmount: amount, bigFontSize: 16, smallFontSize: 12)

        // then
        XCTAssert(attributtedText == expected)

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAnAmountWithoutDecimalsAndNegativeNumber_whenICallAttributtedTextAmountBigFontSizeSmallFontSize_thenAttributtedTextShouldMatch() {

        // expect
        let amountFormatted = "-$20"
        let offsetAmount = Tools.setFontBook(size: 16).capHeight - Tools.setFontBook(size: 12).capHeight
        let expected = NSMutableAttributedString(string: amountFormatted, attributes: [NSAttributedStringKey.font: Tools.setFontBook(size: 16)])
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 1, length: 1))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 1, length: 1))
        // given
        let amount = NSDecimalNumber(value: -20)

        // when
        let attributtedText = sutRequiresDecimalsAndSymbolLeft.attributtedText(forAmount: amount, bigFontSize: 16, smallFontSize: 12)

        // then
        XCTAssert(attributtedText == expected)

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAnAmountWithOneDecimalAndNoNegativeNumber_whenICallAttributtedTextAmountBigFontSizeSmallFontSize_thenAttributtedTextShouldMatch() {

        // expect
        let amountFormatted = "$2030"
        let offsetAmount = Tools.setFontBook(size: 16).capHeight - Tools.setFontBook(size: 12).capHeight
        let expected = NSMutableAttributedString(string: amountFormatted, attributes: [NSAttributedStringKey.font: Tools.setFontBook(size: 16)])
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 0, length: 1))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 0, length: 1))
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 3, length: 2))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 3, length: 2))

        // given
        let amount = NSDecimalNumber(value: 20.3)

        // when
        let attributtedText = sutRequiresDecimalsAndSymbolLeft.attributtedText(forAmount: amount, bigFontSize: 16, smallFontSize: 12)

        // then
        XCTAssert(attributtedText == expected)

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAnAmountWithOneDecimalAndNegativeNumber_whenICallAttributtedTextAmountBigFontSizeSmallFontSize_thenAttributtedTextShouldMatch() {

        // expect
        let amountFormatted = "-$2030"
        let offsetAmount = Tools.setFontBook(size: 16).capHeight - Tools.setFontBook(size: 12).capHeight
        let expected = NSMutableAttributedString(string: amountFormatted, attributes: [NSAttributedStringKey.font: Tools.setFontBook(size: 16)])
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 1, length: 1))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 1, length: 1))
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 4, length: 2))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 4, length: 2))

        // given
        let amount = NSDecimalNumber(value: -20.3)

        // when
        let attributtedText = sutRequiresDecimalsAndSymbolLeft.attributtedText(forAmount: amount, bigFontSize: 16, smallFontSize: 12)

        // then
        XCTAssert(attributtedText == expected)

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAnAmountWithTwoDecimalsAndNoNegativeNumber_whenICallAttributtedTextAmountBigFontSizeSmallFontSize_thenAttributtedTextShouldMatch() {

        // expect
        let amountFormatted = "$2033"
        let offsetAmount = Tools.setFontBook(size: 16).capHeight - Tools.setFontBook(size: 12).capHeight
        let expected = NSMutableAttributedString(string: amountFormatted, attributes: [NSAttributedStringKey.font: Tools.setFontBook(size: 16)])
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 0, length: 1))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 0, length: 1))
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 3, length: 2))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 3, length: 2))

        // given
        let amount = NSDecimalNumber(value: 20.33)

        // when
        let attributtedText = sutRequiresDecimalsAndSymbolLeft.attributtedText(forAmount: amount, bigFontSize: 16, smallFontSize: 12)

        // then
        XCTAssert(attributtedText == expected)

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAnAmountWithTwoDecimalsAndNegativeNumber_whenICallAttributtedTextAmountBigFontSizeSmallFontSize_thenAttributtedTextShouldMatch() {

        // expect
        let amountFormatted = "-$2033"
        let offsetAmount = Tools.setFontBook(size: 16).capHeight - Tools.setFontBook(size: 12).capHeight
        let expected = NSMutableAttributedString(string: amountFormatted, attributes: [NSAttributedStringKey.font: Tools.setFontBook(size: 16)])
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 1, length: 1))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 1, length: 1))
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 4, length: 2))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 4, length: 2))

        // given
        let amount = NSDecimalNumber(value: -20.33)

        // when
        let attributtedText = sutRequiresDecimalsAndSymbolLeft.attributtedText(forAmount: amount, bigFontSize: 16, smallFontSize: 12)

        // then
        XCTAssert(attributtedText == expected)

    }

    // MARK: AmountFormatter sutRequiresSymbolWithMoreThanACharacter

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAndRequiresMoreThanACharacterAnAmountWithoutDecimalsAndNoNegative_whenICallAttributtedTextAmountIsNegativeBigFontSizeSmallFontSize_thenAttributtedTextShouldMatch() {

        // expect
        let amountFormatted = "S/20"
        let offsetAmount = Tools.setFontBook(size: 16).capHeight - Tools.setFontBook(size: 12).capHeight
        let expected = NSMutableAttributedString(string: amountFormatted, attributes: [NSAttributedStringKey.font: Tools.setFontBook(size: 16)])
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 0, length: 2))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 0, length: 2))

        // given
        let amount = NSDecimalNumber(value: 20)
        let isNegative = false

        // when
        let attributtedText = sutRequiresSymbolWithMoreThanACharacter.attributtedText(forAmount: amount, isNegative: isNegative, bigFontSize: 16, smallFontSize: 12)

        // then
        XCTAssert(attributtedText == expected)

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAndRequiresMoreThanACharacterAnAmountWithoutDecimalsAndNegative_whenICallAttributtedTextAmountIsNegativeBigFontSizeSmallFontSize_thenAttributtedTextShouldMatch() {

        // expect
        let amountFormatted = "-S/20"
        let offsetAmount = Tools.setFontBook(size: 16).capHeight - Tools.setFontBook(size: 12).capHeight
        let expected = NSMutableAttributedString(string: amountFormatted, attributes: [NSAttributedStringKey.font: Tools.setFontBook(size: 16)])
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 1, length: 2))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 1, length: 2))

        // given
        let amount = NSDecimalNumber(value: 20)
        let isNegative = true

        // when
        let attributtedText = sutRequiresSymbolWithMoreThanACharacter.attributtedText(forAmount: amount, isNegative: isNegative, bigFontSize: 16, smallFontSize: 12)

        // then
        XCTAssert(attributtedText == expected)

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAndRequiresMoreThanACharacterAnAmountWithOneDecimalAndNoNegative_whenICallAttributtedTextAmountIsNegativeBigFontSizeSmallFontSize_thenAttributtedTextShouldMatch() {

        // expect
        let amountFormatted = "S/2030"
        let offsetAmount = Tools.setFontBook(size: 16).capHeight - Tools.setFontBook(size: 12).capHeight
        let expected = NSMutableAttributedString(string: amountFormatted, attributes: [NSAttributedStringKey.font: Tools.setFontBook(size: 16)])
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 0, length: 2))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 0, length: 2))
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 4, length: 2))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 4, length: 2))

        // given
        let amount = NSDecimalNumber(value: 20.3)
        let isNegative = false

        // when
        let attributtedText = sutRequiresSymbolWithMoreThanACharacter.attributtedText(forAmount: amount, isNegative: isNegative, bigFontSize: 16, smallFontSize: 12)

        // then
        XCTAssert(attributtedText == expected)

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAndRequiresMoreThanACharacterAnAmountWithOneDecimalAndNegative_whenICallAttributtedTextAmountIsNegativeBigFontSizeSmallFontSize_thenAttributtedTextShouldMatch() {

        // expect
        let amountFormatted = "-S/2030"
        let offsetAmount = Tools.setFontBook(size: 16).capHeight - Tools.setFontBook(size: 12).capHeight
        let expected = NSMutableAttributedString(string: amountFormatted, attributes: [NSAttributedStringKey.font: Tools.setFontBook(size: 16)])
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 1, length: 2))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 1, length: 2))
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 5, length: 2))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 5, length: 2))

        // given
        let amount = NSDecimalNumber(value: 20.3)
        let isNegative = true

        // when
        let attributtedText = sutRequiresSymbolWithMoreThanACharacter.attributtedText(forAmount: amount, isNegative: isNegative, bigFontSize: 16, smallFontSize: 12)

        // then
        XCTAssert(attributtedText == expected)

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAndRequiresMoreThanACharacterAnAmountWithTwoDecimalsAndNoNegative_whenICallAttributtedTextAmountIsNegativeBigFontSizeSmallFontSize_thenAttributtedTextShouldMatch() {

        // expect
        let amountFormatted = "S/2033"
        let offsetAmount = Tools.setFontBook(size: 16).capHeight - Tools.setFontBook(size: 12).capHeight
        let expected = NSMutableAttributedString(string: amountFormatted, attributes: [NSAttributedStringKey.font: Tools.setFontBook(size: 16)])
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 0, length: 2))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 0, length: 2))
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 4, length: 2))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 4, length: 2))

        // given
        let amount = NSDecimalNumber(value: 20.33)
        let isNegative = false

        // when
        let attributtedText = sutRequiresSymbolWithMoreThanACharacter.attributtedText(forAmount: amount, isNegative: isNegative, bigFontSize: 16, smallFontSize: 12)

        // then
        XCTAssert(attributtedText == expected)

    }

    func test_givenCurrencyThatRequiresDecimalsAndItsSymbolIsLeftAndRequiresMoreThanACharacterAnAmountWithTwoDecimalsAndNegative_whenICallAttributtedTextAmountIsNegativeBigFontSizeSmallFontSize_thenAttributtedTextShouldMatch() {

        // expect
        let amountFormatted = "-S/2033"
        let offsetAmount = Tools.setFontBook(size: 16).capHeight - Tools.setFontBook(size: 12).capHeight
        let expected = NSMutableAttributedString(string: amountFormatted, attributes: [NSAttributedStringKey.font: Tools.setFontBook(size: 16)])
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 1, length: 2))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 1, length: 2))
        expected.addAttribute(NSAttributedStringKey.font, value: Tools.setFontBook(size: 12), range: NSRange(location: 5, length: 2))
        expected.addAttribute(NSAttributedStringKey.baselineOffset, value: offsetAmount, range: NSRange(location: 5, length: 2))

        // given
        let amount = NSDecimalNumber(value: 20.33)
        let isNegative = true

        // when
        let attributtedText = sutRequiresSymbolWithMoreThanACharacter.attributtedText(forAmount: amount, isNegative: isNegative, bigFontSize: 16, smallFontSize: 12)

        // then
        XCTAssert(attributtedText == expected)

    }

    // MARK: Currency

    func test_givenCurrencyThatDoNotRequiresDecimalsAndItsSymbolIsLeftAValidCurrencyCodePositionNeedsDecimal_whenIInitCurrency_thenThoseDataShouldMatchAndSymbolShouldBeDifferentFromCode() {

        // given
        let code = "EUR"
        let position = CurrencyBO.CurrencyPosition.left
        let needDecimals = true

        // when
        let currency = CurrencyBO(code: code, position: position, needDecimals: needDecimals)

        // then
        XCTAssert(currency.symbol != code)
        XCTAssert(currency.symbol == "€")
        XCTAssert(currency.code == code)
        XCTAssert(currency.position == CurrencyBO.CurrencyPosition.left)
        XCTAssert(currency.needDecimals == needDecimals)

    }

    func test_givenCurrencyThatDoNotRequiresDecimalsAndItsSymbolIsLeftAWrongCurrencyCodePositionNeedsDecimal_whenIInitCurrency_thenThoseDataShouldMatchAndSymbolShouldBeTheSameFromCode() {

        // given
        let code = "XXX"
        let position = CurrencyBO.CurrencyPosition.left
        let needDecimals = true

        // when
        let currency = CurrencyBO(code: code, position: position, needDecimals: needDecimals)

        // then
        XCTAssert(currency.symbol == code)
        XCTAssert(currency.code == code)
        XCTAssert(currency.position == CurrencyBO.CurrencyPosition.left)
        XCTAssert(currency.needDecimals == needDecimals)

    }

    // MARK: Currencies

    func test_givenValidCurrencyCode_whenICallCurrencyForCode_thenReturnItsCurrency() {

        // given
        let code = "EUR"

        // when
        let currency = Currencies.currency(forCode: code)

        // then
        XCTAssert(currency.code == code)

    }

    func test_givenNoExistCurrencyCodeAndMainCurrency_whenICallCurrencyForCode_thenReturnTheCodeAndDefaultValues() {

        // given
        let code = "XXX"
        dummySessionManager.mainCurrency = "USD"

        // when
        let currency = Currencies.currency(forCode: code)

        // then
        XCTAssert(currency.code == code)
        XCTAssert(currency.position == .right)
        XCTAssert(currency.needDecimals == true)

    }

    // MARK: NSDecimalNumber extension

    func test_givenANegativeNSDecimalNumber_whenICallAbs_thenReturnAPositiveValue() {

        // given
        let valueExpected = NSDecimalNumber(value: 20.33)
        let decimalNumberNegative = NSDecimalNumber(value: -valueExpected.doubleValue)

        // when
        let positiveDecimalNumber = decimalNumberNegative.abs()

        // then
        XCTAssert(positiveDecimalNumber.doubleValue > 0)

    }

    func test_givenANegativeNSDecimalNumber_whenICallAbs_thenReturnTheSameValue() {

        // given
        let valueExpected = NSDecimalNumber(value: 20.33)
        let decimalNumberNegative = NSDecimalNumber(value: -valueExpected.doubleValue)

        // when
        let positiveDecimalNumber = decimalNumberNegative.abs()

        // then
        XCTAssert(positiveDecimalNumber == valueExpected)
    }

    func test_givenAPositiveNSDecimalNumber_whenICallAbs_thenReturnTheSameValue() {

        // given
        let valueExpected = NSDecimalNumber(value: 20.33)
        let decimalNumberGiven = NSDecimalNumber(value: valueExpected.doubleValue)

        // when
        let positiveDecimalNumber = decimalNumberGiven.abs()

        // then
        XCTAssert(decimalNumberGiven == positiveDecimalNumber)
    }

    // MARK: Decimal extension

    func test_givenDecimalWithDecimals_whenICallSignificantFractionalDecimalDigits_thenReturnTheNumberOfDecimals() {

        // given
        let decimal = Decimal(20.22)

        // when
        let numberOfDecimals = decimal.significantFractionalDecimalDigits

        // then
        XCTAssert(numberOfDecimals == 2)

    }

    func test_givenDecimalWithoutDecimals_whenICallSignificantFractionalDecimalDigits_thenReturnZero() {

        // given
        let decimal = Decimal(20)

        // when
        let numberOfDecimals = decimal.significantFractionalDecimalDigits

        // then
        XCTAssert(numberOfDecimals == 0)

    }

}
