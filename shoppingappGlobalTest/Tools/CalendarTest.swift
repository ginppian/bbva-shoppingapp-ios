//
//  CalendarTest.swift
//  shoppingapp
//
//  Created by jesus.martinez on 12/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class CalendarTest: XCTestCase {

    var sut: Calendar!

    override func setUp() {
        super.setUp()
        self.sut = Calendar.gregorianCalendar()
    }

    func test_givenTwoDatesInTheSameYear_whenICallisDateDuringSameYearAsDate_thenReturnTrue() {

        // given
        let format = Date.DATE_FORMAT_SERVER
        let date1 = Date.date(fromLocalTimeZoneString: "2012-12-12", withFormat: format)!
        let date2 = Date.date(fromLocalTimeZoneString: "2012-12-30", withFormat: format)!

        // when
        let result = self.sut.isDate(date1, duringSameYearAsDate: date2)

        // then
        XCTAssert(result == true)

    }

    func test_givenTwoDatesInTheDifferentYear_whenICallisDateDuringSameYearAsDate_thenReturnFalse() {

        // given
        let format = Date.DATE_FORMAT_SERVER
        let date1 = Date.date(fromLocalTimeZoneString: "2012-12-12", withFormat: format)!
        let date2 = Date.date(fromLocalTimeZoneString: "2013-12-30", withFormat: format)!

        // when
        let result = self.sut.isDate(date1, duringSameYearAsDate: date2)

        // then
        XCTAssert(result == false)

    }

}
