//
//  DummyLocationManager.swift
//  shoppingapp
//
//  Created by jesus.martinez on 27/6/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import CoreLocation

#if TESTMX
@testable import shoppingappMX
#elseif TESTPE
@testable import shoppingappPE
#endif

class DummyLocationManager: LocationManagerProtocol {

    var isCalledGetLocation = false

    var completionHandlerSuccess: LocationManager.Success?
    var completionHandlerFailure: LocationManager.Failure?

    var locationErrorType: LocationErrorType?
    var location: CLLocation?

    var isCalledStartLocationObserverAndCheckStatus = false
    var isCalledStopLocationObserver = false
    
    var isCalledForceStartUpdatingLocation = false

    func getLocation(onSuccess success: @escaping LocationManager.Success, onFail fail: @escaping LocationManager.Failure) {

        isCalledGetLocation = true

        completionHandlerSuccess = success
        completionHandlerFailure = fail

        if let completionFailure = completionHandlerFailure, let locationErrorType = locationErrorType {

            completionFailure(locationErrorType)

        } else if let completionSuccess = completionHandlerSuccess, let location = location {

            completionSuccess(location)
        }

    }

    func startLocationObserverAndCheckStatus() {

        isCalledStartLocationObserverAndCheckStatus = true

    }

    func stopLocationObserver() {

        isCalledStopLocationObserver = true

    }

    func startLocationObserver() {

    }
    
    func forceStartUpdatingLocation() {
        
    }
}
