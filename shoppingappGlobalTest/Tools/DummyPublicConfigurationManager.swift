//
//  DummyPublicConfigurationManager.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 3/12/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation
import RxSwift

#if TESTMX
@testable import shoppingappMX
#elseif TESTPE
@testable import shoppingappPE
#endif

class DummyPublicConfigurationManager: PublicConfigurationManager {
    
    var isCalledGetPublicConfiguration = false
    var isCalledLoadDefaultPublicConfigurationInformation = false
    var isCalledSavePublicConfiguration = false
    var isCalledSaveDefaultPublicConfiguration = false
    
    class func setupMock() {
        
        super.instance = DummyPublicConfigurationManager()
    }
    
    override func getPublicConfiguration() -> Observable<PublicConfigurationDTO> {
        
        isCalledGetPublicConfiguration = true
        
        return downloadPublicConfigurationPublisher.asObservable()
    }
    
    override func loadDefaultPublicConfigurationInformation() -> PublicConfigurationDTO {
        
        isCalledLoadDefaultPublicConfigurationInformation = true
        
        return super.loadDefaultPublicConfigurationInformation()
    }
    
    override func savePublicConfiguration(_ data: PublicConfigurationDTO) {
        
        isCalledSavePublicConfiguration = true
        super.savePublicConfiguration(data)
    }
    
    override func saveDefaultPublicConfiguration() {
        
        isCalledSaveDefaultPublicConfiguration = true
        super.saveDefaultPublicConfiguration()
    }
}
