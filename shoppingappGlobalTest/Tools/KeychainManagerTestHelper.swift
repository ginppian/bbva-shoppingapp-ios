//
//  KeychainManagerTestHelper.swift
//  shoppingapp
//
//  Created by ignacio.bonafonte on 03/01/2019.
//  Copyright © 2019 BBVA. All rights reserved.
//

import Foundation

#if TESTMX
@testable import shoppingappMX
#elseif TESTPE
@testable import shoppingappPE
#endif

@testable import SwiftKeychainWrapper

class KeychainManagerTestHelper: KeychainManager {

    private let customKeychainWrapperInstance = KeychainWrapper(serviceName: "tests", accessGroup: nil)

    override var keyChainWrapper: KeychainWrapper {
        return customKeychainWrapperInstance
    }

    static func configureKeychainManagerForTesting() {
        internalInstance = KeychainManagerTestHelper()
        shared.resetKeyChainItems()
    }

}
