//
//  DummyURLOpener.swift
//  shoppingapp
//
//  Created by jesus.martinez on 27/12/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

#if TESTMX
@testable import shoppingappMX
#elseif TESTPE
@testable import shoppingappPE
#endif

class DummyURLOpener: URLOpenerProtocol {
    
    var isCalledCanOpenURLString = false
    var isCalledOpenURLString = false
    var isCalledCanOpenURL = false
    
    var dummyValueCanOpenURLString = false
    var dummyValueOpenURLString = false
    
    var urlStringSent = ""
    var urlSent = URL(fileURLWithPath: "dummy")
    
    func canOpen(_ urlString: String) -> Bool {
        
        isCalledCanOpenURLString = true
        
        urlStringSent = urlString
        
        return dummyValueCanOpenURLString
    }
    
    func openURL(_ urlString: String) -> Bool {
        
        isCalledOpenURLString = true
        
        urlStringSent = urlString
        
        return dummyValueOpenURLString
    }
    
    func openURL(_ url: URL) {
        
        isCalledCanOpenURL = true
        
        urlSent = url
    }
}
