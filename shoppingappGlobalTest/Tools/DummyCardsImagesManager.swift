//
//  DummyCardsImagesManager.swift
//  shoppingapp
//
//  Created by jesus.martinez on 10/7/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

#if TESTMX
@testable import shoppingappMX
#elseif TESTPE
@testable import shoppingappPE
#endif

class DummyCardsImagesManager: CardsImagesManager {

    var isCalledSetupImageTasks = false

    var cardsSent: [CardBO]?

    override init() {
        super.init()
    }

    override func setupImageTasks(withCards cards: [CardBO]) {

        isCalledSetupImageTasks = true

        cardsSent = cards
    }

}
