//
//  DummyNotificationManager.swift
//  shoppingapp
//
//  Created by jesus.martinez on 19/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

#if TESTMX
@testable import shoppingappMX
#elseif TESTPE
@testable import shoppingappPE
#endif

import XCTest

class DummyNotificationManager: NotificationManager {
    
    var timesCalled = 0
    var notificationsIsAuthorized = [false]
    var notificationsIsGranted = false
    var authorizationDetermined = false
    
    var isCalledIsAuthorizedStatus = false
    var isCalledShowRequestNotificationAutorization = false
    var isCalledCheckAuthorizedStatus = false
    
    func isAuthorizedStatus(_ isAuthorized: @escaping (Bool) -> Void) {
        
        isCalledIsAuthorizedStatus = true
        
        if timesCalled < notificationsIsAuthorized.count {
            isAuthorized(notificationsIsAuthorized[timesCalled])
            timesCalled += 1
        } else {
            isAuthorized(notificationsIsAuthorized[timesCalled - 1])
        }
    }
    
    func showRequestNotificationAutorization(_ isGranted: @escaping (Bool) -> Void) {
        
        isCalledShowRequestNotificationAutorization = true
        isGranted(notificationsIsGranted)
    }
    
    func checkAuthorizedStatus(_ isAuthorized: @escaping (Bool) -> Void) {
        
        isCalledCheckAuthorizedStatus = true
        isAuthorized(authorizationDetermined)
    }
}
