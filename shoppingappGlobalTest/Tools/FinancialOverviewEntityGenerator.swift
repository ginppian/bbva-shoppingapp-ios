//
//  FinancialOverviewEntityGenerator.swift
//  shoppingappMX
//
//  Created by jesus.martinez on 10/7/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

#if TESTMX
@testable import shoppingappMX
#elseif TESTPE
@testable import shoppingappPE
#endif

class FinancialOverviewEntityGenerator {

    class func financialOverview() -> FinancialOverviewEntity {

        if let path = Bundle(for: self).path(forResource: "financialOverview", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)

                let financialOverviewEntity = try JSONDecoder().decode(FinancialOverviewEntity.self, from: data)
                financialOverviewEntity.status = ConstantsHTTPCodes.STATUS_OK
                return financialOverviewEntity

            } catch {
                print ("Error: \(error)")
                return FinancialOverviewEntity()
            }
        }

        return FinancialOverviewEntity()
    }
    
    class func financialOverviewWithAccountID() -> FinancialOverviewEntity {
        
        if let path = Bundle(for: self).path(forResource: "financialWithAccountIDs", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                
                let financialOverviewEntity = try JSONDecoder().decode(FinancialOverviewEntity.self, from: data)
                financialOverviewEntity.status = ConstantsHTTPCodes.STATUS_OK
                return financialOverviewEntity
                
            } catch {
                print ("Error: \(error)")
                return FinancialOverviewEntity()
            }
        }
        
        return FinancialOverviewEntity()
    }
    
    class func financialOverviewWithDigitalCards() -> FinancialOverviewEntity {
        
        if let path = Bundle(for: self).path(forResource: "financialOverviewDebitAndCreditDigital", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                
                let financialOverviewEntity = try JSONDecoder().decode(FinancialOverviewEntity.self, from: data)
                financialOverviewEntity.status = ConstantsHTTPCodes.STATUS_OK
                return financialOverviewEntity
                
            } catch {
                print ("Error: \(error)")
                return FinancialOverviewEntity()
            }
        }
        
        return FinancialOverviewEntity()
    }

    class func financialOverviewComplete() -> FinancialOverviewEntity {

        if let path = Bundle(for: self).path(forResource: "financialOverviewComplete", ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)

                let financialOverviewEntity = try JSONDecoder().decode(FinancialOverviewEntity.self, from: data)
                financialOverviewEntity.status = ConstantsHTTPCodes.STATUS_OK
                return financialOverviewEntity

            } catch {
                print ("Error: \(error)")
                return FinancialOverviewEntity()
            }
        }

        return FinancialOverviewEntity()
    }

}
