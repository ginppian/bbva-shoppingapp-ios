//
//  File.swift
//  shoppingapp
//
//  Created by jesus.martinez on 25/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class LimitsGenerator {

    class func getThreeLimitsEntity() -> LimitsEntity {

        let limitsEntity = LimitsEntity.deserialize(from: "{\"data\":[{\"id\":\"ATM_DAILY\",\"name\":\"Retiro de efectivo en cajeros\",\"amountLimits\":[{\"amount\":3000000,\"currency\":\"MXN\"}],\"isEditable\":true,\"disposedLimits\":[{\"amount\":3000000,\"currency\":\"MXN\"}],\"allowedInterval\":{\"maximumAmount\":{\"amount\":500,\"currency\":\"MXN\"},\"minimumAmount\":{\"amount\":0,\"currency\":\"MXN\"}}},{\"id\":\"GRANTED_CREDIT\",\"name\":\"Límite máximo de consumo\",\"amountLimits\":[{\"amount\":500000,\"currency\":\"MXN\"}],\"isEditable\":false,\"disposedLimits\":[{\"amount\":250000,\"currency\":\"MXN\"}],\"allowedInterval\":{\"maximumAmount\":{\"amount\":1000,\"currency\":\"MXN\"},\"minimumAmount\":{\"amount\":0,\"currency\":\"MXN\"}}},{\"id\":\"POS_DAILY\",\"name\":\"Retirada de efectivo en sucursal\",\"amountLimits\":[{\"amount\":2000000,\"currency\":\"\"}],\"isEditable\":true,\"disposedLimits\":[{\"amount\":150000,\"currency\":\"MXN\"}],\"allowedInterval\":{\"maximumAmount\":{\"amount\":2000,\"currency\":\"MXN\"},\"minimumAmount\":{\"amount\":500,\"currency\":\"MXN\"}}}],\"apiInfo\":{\"category\":\"products\",\"name\":\"cards\",\"version\":\"0.19.0\"}}")

        limitsEntity?.status = 200

        return limitsEntity!
    }

    class func getThreeLimits() -> LimitsBO {

        let limitsBO = LimitsBO(limitsEntity: LimitsGenerator.getThreeLimitsEntity())

        return limitsBO

    }

}
