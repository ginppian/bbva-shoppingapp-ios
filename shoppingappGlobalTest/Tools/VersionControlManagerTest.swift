//
//  VersionControlManagerTest.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 27/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest
import CellsNativeComponents
import RxSwift
import CellsNativeCore

#if TESTMX
@testable import shoppingappMX
#elseif TESTPE
@testable import shoppingappPE
#endif

class VersionControlManagerTest: XCTestCase {
    
    private var sut: VersionControlManager!
    var dummyBusManager = DummyBusManager()
    var dummyUpgradeManager: DummyUpgradeManager!
    private var dummyPreferencesManager: DummyPreferencesManager!
    
    override func setUp() {
        super.setUp()
        
        sut = VersionControlManager()
        
        BusManager.sessionDataInstance = dummyBusManager
        
        DummyUpgradeManager.setupMock()
        dummyUpgradeManager = (UpgradeManager.sharedInstance() as! DummyUpgradeManager)
        
        DummyPreferencesManager.configureDummyPreferences()
        dummyPreferencesManager = PreferencesManager.instance as? DummyPreferencesManager
    }
 
    // MARK: - checkVersion

    func test_givenIsMandatoryTrueVersionSmallerThanBundleVersionAndShouldShowNoMandatoryUpdateTrue_whenICallCheckVersion_thenUpgradeViewNotShow() {
        
        // given
        let publicConfigurationDTO = PublicConfigurationGenerator.getPublicConfiguration().data!
        publicConfigurationDTO.publicConfig?.update?.iOS?.isMandatory = true
        publicConfigurationDTO.publicConfig?.update?.iOS?.version = "-1"
        
        // when
        sut.checkVersion(withPublicConfigurationDTO: publicConfigurationDTO, shouldShowNoMandatoryUpdate: true)
        
        // then
        XCTAssert(dummyUpgradeManager.isCalledShow == false)
    }
    
    func test_givenIsMandatoryTrueVersionHigherThanBundleVersionAndShouldShowNoMandatoryUpdateTrue_whenICallCheckVersion_thenUpgradeViewShow() {
        
        // given
        let publicConfigurationDTO = PublicConfigurationGenerator.getPublicConfiguration().data!
        publicConfigurationDTO.publicConfig?.update?.iOS?.isMandatory = true
        publicConfigurationDTO.publicConfig?.update?.iOS?.version = "\(Int.max)"
        
        // when
        sut.checkVersion(withPublicConfigurationDTO: publicConfigurationDTO, shouldShowNoMandatoryUpdate: true)
        
        // then
        XCTAssert(dummyUpgradeManager.isCalledShow == true)
    }
    
    func test_givenIsMandatoryTrueVersionHigherThanBundleVersionAndShouldShowNoMandatoryUpdateTrue_whenICallCheckVersion_thenUpgradeViewShowWithTransportAppropiated() {
        
        // given
        let publicConfigurationDTO = PublicConfigurationGenerator.getPublicConfiguration().data!
        publicConfigurationDTO.publicConfig?.update?.iOS?.isMandatory = true
        publicConfigurationDTO.publicConfig?.update?.iOS?.version = "\(Int.max)"
        
        // when
        sut.checkVersion(withPublicConfigurationDTO: publicConfigurationDTO, shouldShowNoMandatoryUpdate: true)
        
        // then
        let iOS = publicConfigurationDTO.publicConfig!.update!.iOS!
        XCTAssert(dummyUpgradeManager.modelSent == UpgradeNotificationTransport(iOS.isMandatory, iOS.URL, iOS.message))
    }
    
    func test_givenIsMandatoryTrueVersionSmallerThanBundleVersionAndShouldShowNoMandatoryUpdateFalse_whenICallCheckVersion_thenUpgradeViewNotShow() {
        
        // given
        let publicConfigurationDTO = PublicConfigurationGenerator.getPublicConfiguration().data!
        publicConfigurationDTO.publicConfig?.update?.iOS?.isMandatory = true
        publicConfigurationDTO.publicConfig?.update?.iOS?.version = "-1"
        
        // when
        sut.checkVersion(withPublicConfigurationDTO: publicConfigurationDTO, shouldShowNoMandatoryUpdate: false)
        
        // then
        XCTAssert(dummyUpgradeManager.isCalledShow == false)
    }
    
    func test_givenIsMandatoryTrueVersionHigherThanBundleVersionAndShouldShowNoMandatoryUpdateFalse_whenICallCheckVersion_thenUpgradeViewShow() {
        
        // given
        let publicConfigurationDTO = PublicConfigurationGenerator.getPublicConfiguration().data!
        publicConfigurationDTO.publicConfig?.update?.iOS?.isMandatory = true
        publicConfigurationDTO.publicConfig?.update?.iOS?.version = "\(Int.max)"
        
        // when
        sut.checkVersion(withPublicConfigurationDTO: publicConfigurationDTO, shouldShowNoMandatoryUpdate: false)
        
        // then
        XCTAssert(dummyUpgradeManager.isCalledShow == true)
    }
    
    func test_givenIsMandatoryTrueVersionHigherThanBundleVersionAndShouldShowNoMandatoryUpdateFalse_whenICallCheckVersion_thenUpgradeViewShowWithTransportAppropiated() {
        
        // given
        let publicConfigurationDTO = PublicConfigurationGenerator.getPublicConfiguration().data!
        publicConfigurationDTO.publicConfig?.update?.iOS?.isMandatory = true
        publicConfigurationDTO.publicConfig?.update?.iOS?.version = "\(Int.max)"
        
        // when
        sut.checkVersion(withPublicConfigurationDTO: publicConfigurationDTO, shouldShowNoMandatoryUpdate: false)
        
        // then
        let iOS = publicConfigurationDTO.publicConfig!.update!.iOS!
        XCTAssert(dummyUpgradeManager.modelSent == UpgradeNotificationTransport(iOS.isMandatory, iOS.URL, iOS.message))
    }
    
    func test_givenIsMandatoryFalseVersionSmallerThanBundleVersionAndShouldShowNoMandatoryUpdateTrue_whenICallCheckVersion_thenUpgradeViewNotShow() {
        
        // given
        let publicConfigurationDTO = PublicConfigurationGenerator.getPublicConfiguration().data!
        publicConfigurationDTO.publicConfig?.update?.iOS?.isMandatory = false
        publicConfigurationDTO.publicConfig?.update?.iOS?.version = "-1"
        
        // when
        sut.checkVersion(withPublicConfigurationDTO: publicConfigurationDTO, shouldShowNoMandatoryUpdate: true)
        
        // then
        XCTAssert(dummyUpgradeManager.isCalledShow == false)
    }
    
    func test_givenIsMandatoryFalseVersionHigherThanBundleVersionAndShouldShowNoMandatoryUpdateTrue_whenICallCheckVersion_thenUpgradeViewShow() {
        
        // given
        let publicConfigurationDTO = PublicConfigurationGenerator.getPublicConfiguration().data!
        publicConfigurationDTO.publicConfig?.update?.iOS?.isMandatory = false
        publicConfigurationDTO.publicConfig?.update?.iOS?.version = "\(Int.max)"
        
        // when
        sut.checkVersion(withPublicConfigurationDTO: publicConfigurationDTO, shouldShowNoMandatoryUpdate: true)
        
        // then
        XCTAssert(dummyUpgradeManager.isCalledShow == true)
    }
    
    func test_givenIsMandatoryFalseVersionHigherThanBundleVersionAndShouldShowNoMandatoryUpdateTrue_whenICallCheckVersion_thenUpgradeViewShowWithTransportAppropiated() {
        
        // given
        let publicConfigurationDTO = PublicConfigurationGenerator.getPublicConfiguration().data!
        publicConfigurationDTO.publicConfig?.update?.iOS?.isMandatory = false
        publicConfigurationDTO.publicConfig?.update?.iOS?.version = "\(Int.max)"
        
        // when
        sut.checkVersion(withPublicConfigurationDTO: publicConfigurationDTO, shouldShowNoMandatoryUpdate: true)
        
        // then
        let iOS = publicConfigurationDTO.publicConfig!.update!.iOS!
        XCTAssert(dummyUpgradeManager.modelSent == UpgradeNotificationTransport(iOS.isMandatory, iOS.URL, iOS.message))
    }
    
    func test_givenIsMandatoryFalseVersionSmallerThanBundleVersionAndShouldShowNoMandatoryUpdateFalse_whenICallCheckVersion_thenUpgradeViewNotShow() {
        
        // given
        let publicConfigurationDTO = PublicConfigurationGenerator.getPublicConfiguration().data!
        publicConfigurationDTO.publicConfig?.update?.iOS?.isMandatory = false
        publicConfigurationDTO.publicConfig?.update?.iOS?.version = "-1"
        
        // when
        sut.checkVersion(withPublicConfigurationDTO: publicConfigurationDTO, shouldShowNoMandatoryUpdate: false)
        
        // then
        XCTAssert(dummyUpgradeManager.isCalledShow == false)
    }
    
    func test_givenIsMandatoryFalseVersionHigherThanBundleVersionAndShouldShowNoMandatoryUpdateFalse_whenICallCheckVersion_thenUpgradeViewShow() {
        
        // given
        let publicConfigurationDTO = PublicConfigurationGenerator.getPublicConfiguration().data!
        publicConfigurationDTO.publicConfig?.update?.iOS?.isMandatory = false
        publicConfigurationDTO.publicConfig?.update?.iOS?.version = "\(Int.max)"
        
        // when
        sut.checkVersion(withPublicConfigurationDTO: publicConfigurationDTO, shouldShowNoMandatoryUpdate: false)
        
        // then
        XCTAssert(dummyUpgradeManager.isCalledShow == false)
    }
    
    // MARK: - checkAppUpgrade
    
    func test_givenAny_whenICallCheckAppUpgrade_thenSaveTheCurrentVersionOfApp() {
        
        // given
        let currentVersion = Bundle.main.object(forInfoDictionaryKey: "CFBundleShortVersionString") as? String
        
        // when
        sut.checkAppUpgrade()
        
        // then
        XCTAssert(dummyPreferencesManager.key == PreferencesManagerKeys.kVersionOfLastRun.rawValue)
        XCTAssert(dummyPreferencesManager.value as? String == currentVersion)
    }
    
    func test_givenDiferentVersion_whenICallCheckAppUpgrade_thenResetKeys() {

        // given
        dummyPreferencesManager.keysWithValues = [PreferencesManagerKeys.kVersionOfLastRun.rawValue: "-1" as AnyObject, PreferencesManagerKeys.kLoginNumber.rawValue: NSNumber(value: 3) as AnyObject, PreferencesManagerKeys.kUserSkipRateApp.rawValue: true as AnyObject, PreferencesManagerKeys.kAppRated.rawValue: true as AnyObject]
        
        // when
        sut.checkAppUpgrade()

        // then
        XCTAssert(dummyPreferencesManager.keysWithValues![PreferencesManagerKeys.kNumberLoginsRateApp.rawValue] == nil)
        XCTAssert(dummyPreferencesManager.keysWithValues![PreferencesManagerKeys.kUserSkipRateApp.rawValue] == nil)
        XCTAssert(dummyPreferencesManager.keysWithValues![PreferencesManagerKeys.kAppRated.rawValue] == nil)
    }
    
    class DummyBusManager: BusManager {
        
        override init() {
            super.init()
            routerFactory = RouterInitFactory()
        }
    }
    
}
