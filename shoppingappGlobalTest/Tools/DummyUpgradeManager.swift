//
//  DummyUpgradeManager.swift
//  shoppingapp
//
//  Created by Javier Pino on 5/12/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

#if TESTMX
@testable import shoppingappMX
#elseif TESTPE
@testable import shoppingappPE
#endif

class DummyUpgradeManager: UpgradeManager {
    
    var isCalledShow = false
    var modelSent: UpgradeNotificationTransport?
    var isCalledHide = false
    
    class func setupMock() {
        
        super.instance = DummyUpgradeManager()
    }
    
    override func show(withModel model: Model, completion: (() -> Void)?) {
        
        isCalledShow = true
        modelSent = model as? UpgradeNotificationTransport
    }
    
    override func hide(completion: (() -> Void)?) {
        
        isCalledHide = true
    }
}
