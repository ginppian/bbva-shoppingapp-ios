//
//  PublicConfigurationManagerTest.swift
//  shoppingapp
//
//  Created by Marcos on 7/11/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest
import RxSwift
import BBVA_Network

#if TESTMX
@testable import shoppingappMX
#elseif TESTPE
@testable import shoppingappPE
#endif

class PublicConfigurationManagerTest: XCTestCase {

    private var sut: PublicConfigurationManager!
    private var dummyPreferencesManager: DummyPreferencesManager!
    private var dummyPublicConfigurationStore: DummyPublicConfigurationStore!
    private var dummyGrantingTicketStore: DummyGrantingTicketStore!
    private var dummySessionManager: DummySessionManager!
    
    private let disposeBag = DisposeBag()

    override func setUp() {

        super.setUp()

        sut = PublicConfigurationManager()
        
        DummyPreferencesManager.configureDummyPreferences()
        dummyPreferencesManager = PreferencesManager.instance as? DummyPreferencesManager
        
        DummySessionManager.setupMock()
        dummySessionManager = (SessionDataManager.sessionDataInstance() as! DummySessionManager)
        
        dummyPublicConfigurationStore = DummyPublicConfigurationStore(worker: NetworkWorker(), url: ServerHostURL.getAppSettingsURL())
        sut.publicConfigurationStore = dummyPublicConfigurationStore
        dummyGrantingTicketStore = DummyGrantingTicketStore(worker: NetworkWorker(), host: ServerHostURL.getGrantingTicketURL())
        sut.grantingTicketStore = dummyGrantingTicketStore
    }

    // MARK: - saveDefaultPublicConfiguration
    
    func test_givenPublicConfigurationData_whenICallSaveDefaultPublicConfiguration_thenICallFileProviderWriteString() {
        
        //given
        
        //When
        let dummyFileProvider = DummyFileProvider()
        sut.fileProvider = dummyFileProvider
        sut.saveDefaultPublicConfiguration()
        
        //then
        XCTAssert(dummyFileProvider.isCalledWriteString == true)
    }
    
    func test_givenAny_whenICallSaveDefaultPublicConfiguration_thenISaveActualDateInPreferencesManager() {
        
        // given
        
        // when
        sut.saveDefaultPublicConfiguration()
        
        // then
        XCTAssert(dummyPreferencesManager.key == PreferencesManagerKeys.kLastTimeUpdatePublicConfiguration.rawValue)
        XCTAssert((dummyPreferencesManager.value as! Date) < Date())
    }
    
    func test_givenPublicConfigurationData_whenICallSaveDefaultPublicConfiguration_thenPublicConfigurationDTOMatch() {
        
        //given
        let publicConfigurationDTO = PublicConfigurationManager.DefaultPublicConfigurationDTO()
        
        //When
        let dummyFileProvider = DummyFileProvider()
        sut.fileProvider = dummyFileProvider
        sut.saveDefaultPublicConfiguration()
        
        //then
        XCTAssert(dummyFileProvider.dataToWrite == publicConfigurationDTO.toJSONString())
    }
    
    func test_givenAny_whenICallSaveDefaultPublicConfiguration_thenPublicConfigurationDTOMatch() {
        
        // given
        let publicConfigurationDTO = sut.loadDefaultPublicConfigurationInformation()
        
        // when
        sut.saveDefaultPublicConfiguration()
        
        // then
        XCTAssert(publicConfigurationDTO.publicConfig == PublicConfigurationManager.DefaultPublicConfigurationDTO().publicConfig)
    }
    
    // MARK: - savePublicConfiguration
    
    func test_givenPublicConfigurationDTO_whenICallSavePublicConfigurationFile_thenICalledWriteStringAndDataToWriteMatch() {
        
        // given
        let dummyFileProvider = DummyFileProvider()
        sut.fileProvider = dummyFileProvider
        
        let publicConfigurationDTO = PublicConfigurationGenerator.getPublicConfiguration().data!
        let jsonString = publicConfigurationDTO.toJSONString()
        let fileName = PublicConfigurationManager.bundleDefaultConfigurationFileName + ".json"
        
        // when
        sut.savePublicConfiguration(publicConfigurationDTO)
        
        // then
        XCTAssert(dummyFileProvider.isCalledWriteString == true)
        XCTAssert(dummyFileProvider.dataToWrite == jsonString)
        XCTAssert(dummyFileProvider.fileNameSent == fileName)
    }
    
    func test_givenPublicConfigurationDTO_whenICallSavePublicConfigurationFile_thenSaveValueWithKeyLastTimeUpdatePublicConfiguration() {
        
        // given
        let dummyFileProvider = DummyFileProvider()
        sut.fileProvider = dummyFileProvider
        
        let publicConfigurationDTO = PublicConfigurationGenerator.getPublicConfiguration().data!
        let dateCompare = Calendar.current.date(byAdding: .minute, value: 1, to: Date())!
        
        // when
        sut.savePublicConfiguration(publicConfigurationDTO)
        
        // then
        XCTAssert(dummyPreferencesManager.key == PreferencesManagerKeys.kLastTimeUpdatePublicConfiguration.rawValue)
        XCTAssert((dummyPreferencesManager.value as! Date) < dateCompare)
    }
    
    // MARK: - loadDefaultPublicConfigurationInformation
    
    func test_givenAny_whenICallLoadDefaultPublicConfigurationInformation_thenPublicConfigurationDTOMatch() {
        
        // given
        
        // when
        let publicConfigurationDTO = sut.loadDefaultPublicConfigurationInformation()
        
        // then
        XCTAssert(publicConfigurationDTO.publicConfig == PublicConfigurationManager.DefaultPublicConfigurationDTO().publicConfig)
    }
    
    // MARK: - localPublicConfiguration
    
    func test_givenAny_whenICallLocalPublicConfiguration_thenICallFileProviderReadFileNameAsString() {
        
        // given
        let dummyFileProvider = DummyFileProvider()
        sut.fileProvider = dummyFileProvider
        
        // when
        _ = sut.localPublicConfiguration()
        
        // then
        XCTAssert(dummyFileProvider.isCalledReadFileNameAsString == true)
    }
    
    func test_givenConfigFileUnsaved_whenICallLocalPublicConfiguration_thenICalledLoadDefaultPublicConfigurationInformationAndPublicConfigurationDTOMatch() {
        
        // given
        let dummyFileProvider = DummyFileProvider()
        sut.fileProvider = dummyFileProvider
        
        // when
        let localPublicConfiguration = sut.localPublicConfiguration()
        
        // then
        XCTAssert(localPublicConfiguration.publicConfig == PublicConfigurationManager.DefaultPublicConfigurationDTO().publicConfig)
    }
    
    func test_givenConfigFile_whenICallLocalPublicConfiguration_thenPublicConfigurationDTOMatch() {
        
        // given
        let dummyFileProvider = DummyFileProvider()
        sut.fileProvider = dummyFileProvider
        
        let fileName = PublicConfigurationManager.bundleDefaultConfigurationFileName + ".json"
        let fileContent = PublicConfigurationGenerator.getPublicConfigurationDataContentJson()
        
        dummyFileProvider.publicConfigurationJson = fileContent
        dummyFileProvider.writeString(fileContent, fileName: fileName)
        
        let publicConfigurationFromFileContent = PublicConfigurationDTO.deserialize(from: fileContent)
        
        // when
        let localPublicConfiguration = sut.localPublicConfiguration()
        
        // then
        XCTAssert(localPublicConfiguration.publicConfig == publicConfigurationFromFileContent?.publicConfig)
    }
    
    func test_givenConfigFileWrong_whenICallLocalPublicConfiguration_thenICalledLoadDefaultPublicConfigurationInformationAndPublicConfigurationDTOMatch() {
        
        // given
        let dummyFileProvider = DummyFileProvider()
        sut.fileProvider = dummyFileProvider
        
        dummyFileProvider.forceWrongReadFile = true
        
        let fileName = PublicConfigurationManager.bundleDefaultConfigurationFileName + ".json"
        let fileContent = "fileContentWrong"
        dummyFileProvider.writeString(fileContent, fileName: fileName)
        
        // when
        let localPublicConfiguration = sut.localPublicConfiguration()
        
        // then
        XCTAssert(localPublicConfiguration.publicConfig == PublicConfigurationManager.DefaultPublicConfigurationDTO().publicConfig)
    }
    
    // MARK: - publicConfigurationErrorLoadedFromStore
    
    func test_givenAny_whenICallPublicConfigurationErrorLoadedFromStore_thenICallFileProviderReadFileNameAsString() {
        
        // given
        let dummyFileProvider = DummyFileProvider()
        sut.fileProvider = dummyFileProvider
        
        // when
        sut.publicConfigurationErrorLoadedFromStore(error: ErrorBO())
        
        // then
        XCTAssert(dummyFileProvider.isCalledReadFileNameAsString == true)
    }
    
    func test_givenConfigFileUnsaved_whenICallPublicConfigurationErrorLoadedFromStore_thenICalledLoadDefaultPublicConfigurationInformationAndPublicConfigurationDTOMatch() {
        
        // given
        let dummyFileProvider = DummyFileProvider()
        sut.fileProvider = dummyFileProvider
        
        var publicConfigurationReceived: PublicConfigurationDTO?
        
        let expectationOnNext = XCTestExpectation(description: "Waiting for onNext")
        
        // when
        sut.downloadPublicConfigurationPublisher
            .subscribe(onNext: { publicConfig in
                
                publicConfigurationReceived = publicConfig
                expectationOnNext.fulfill()
            }).addDisposableTo(disposeBag)
        
        sut.publicConfigurationErrorLoadedFromStore(error: ErrorBO())
        
        // then
        wait(for: [expectationOnNext], timeout: 10)
        XCTAssert(publicConfigurationReceived?.publicConfig == PublicConfigurationManager.DefaultPublicConfigurationDTO().publicConfig)
    }
    
    func test_givenConfigFile_whenICallPublicConfigurationErrorLoadedFromStore_thenPublicConfigurationDTOMatch() {
        
        // given
        let dummyFileProvider = DummyFileProvider()
        sut.fileProvider = dummyFileProvider
        
        let fileName = PublicConfigurationManager.bundleDefaultConfigurationFileName + ".json"
        let fileContent = PublicConfigurationGenerator.getPublicConfigurationDataContentJson()
        
        dummyFileProvider.publicConfigurationJson = fileContent
        dummyFileProvider.writeString(fileContent, fileName: fileName)
        
        let publicConfigurationFromFileContent = PublicConfigurationDTO.deserialize(from: fileContent)
        var publicConfigurationReceived: PublicConfigurationDTO?
        
        let expectationOnNext = XCTestExpectation(description: "Waiting for onNext")
        
        // when
        sut.downloadPublicConfigurationPublisher
            .subscribe(onNext: { publicConfig in
                
                publicConfigurationReceived = publicConfig
                expectationOnNext.fulfill()
            }).addDisposableTo(disposeBag)
        
        sut.publicConfigurationErrorLoadedFromStore(error: ErrorBO())
        
        // then
        wait(for: [expectationOnNext], timeout: 10)
        XCTAssert(publicConfigurationReceived?.publicConfig == publicConfigurationFromFileContent?.publicConfig)
    }
    
    func test_givenConfigFileWrong_whenICallPublicConfigurationErrorLoadedFromStore_thenICalledLoadDefaultPublicConfigurationInformationAndPublicConfigurationDTOMatch() {
        
        // given
        let dummyFileProvider = DummyFileProvider()
        sut.fileProvider = dummyFileProvider
        
        dummyFileProvider.forceWrongReadFile = true
        
        let fileName = PublicConfigurationManager.bundleDefaultConfigurationFileName + ".json"
        let fileContent = "fileContentWrong"
        dummyFileProvider.writeString(fileContent, fileName: fileName)
        
        var publicConfigurationReceived: PublicConfigurationDTO?
        
        let expectationOnNext = XCTestExpectation(description: "Waiting for onNext")
        
        // when
        sut.downloadPublicConfigurationPublisher
            .subscribe(onNext: { publicConfig in
                
                publicConfigurationReceived = publicConfig
                expectationOnNext.fulfill()
            }).addDisposableTo(disposeBag)
        
        sut.publicConfigurationErrorLoadedFromStore(error: ErrorBO())
        
        // then
        wait(for: [expectationOnNext], timeout: 10)
        XCTAssert(publicConfigurationReceived?.publicConfig == PublicConfigurationManager.DefaultPublicConfigurationDTO().publicConfig)
    }
    
    // MARK: - publicConfigurationSuccessLoadedFromStore
    
    func test_givenEmptyPublicConfigurationDTO_whenICallPublicConfigurationSuccessLoadedFromStore_thenPublicConfigurationDTOIsEmpty() {
        
        // given
        let publicConfigurationDTO = PublicConfigurationDTO()
        var publicConfigurationReceived: PublicConfigurationDTO?
        
        let expectationOnNext = XCTestExpectation(description: "Waiting for onNext")
        
        // when
        sut.downloadPublicConfigurationPublisher
            .subscribe(onNext: { publicConfig in
                
                publicConfigurationReceived = publicConfig
                expectationOnNext.fulfill()
            }).addDisposableTo(disposeBag)
        
        sut.publicConfigurationSuccessLoadedFromStore(publicConfigurationDTO: publicConfigurationDTO)
        
        // then
        wait(for: [expectationOnNext], timeout: 10)
        XCTAssert(publicConfigurationReceived?.publicConfig == publicConfigurationDTO.publicConfig)
    }
    
    func test_givenPublicConfigurationDTO_whenICallPublicConfigurationSuccessLoadedFromStore_thenICalledWriteStringAndDataToWriteMatch() {
        
        // given
        let dummyFileProvider = DummyFileProvider()
        sut.fileProvider = dummyFileProvider
        
        let publicConfigurationDTO = PublicConfigurationGenerator.getPublicConfiguration().data!
        let jsonString = publicConfigurationDTO.toJSONString()
        let fileName = PublicConfigurationManager.bundleDefaultConfigurationFileName + ".json"
        
        // when
        sut.publicConfigurationSuccessLoadedFromStore(publicConfigurationDTO: publicConfigurationDTO)
        
        // then
        XCTAssert(dummyFileProvider.isCalledWriteString == true)
        XCTAssert(dummyFileProvider.dataToWrite == jsonString)
        XCTAssert(dummyFileProvider.fileNameSent == fileName)
    }
    
    func test_givenPublicConfigurationDTO_whenICallPublicConfigurationSuccessLoadedFromStore_thenSaveValueWithKeyLastTimeUpdatePublicConfiguration() {
        
        // given
        let dummyFileProvider = DummyFileProvider()
        sut.fileProvider = dummyFileProvider
        
        let publicConfigurationDTO = PublicConfigurationGenerator.getPublicConfiguration().data!
        let dateCompare = Calendar.current.date(byAdding: .minute, value: 1, to: Date())!
        
        // when
        sut.publicConfigurationSuccessLoadedFromStore(publicConfigurationDTO: publicConfigurationDTO)
        
        // then
        XCTAssert(dummyPreferencesManager.key == PreferencesManagerKeys.kLastTimeUpdatePublicConfiguration.rawValue)
        XCTAssert((dummyPreferencesManager.value as! Date) < dateCompare)
    }
    
    func test_givenPublicConfigurationDTO_whenICallPublicConfigurationSuccessLoadedFromStore_thenPublicConfigurationDTOMatch() {
        
        // given
        let publicConfigurationDTO = PublicConfigurationGenerator.getPublicConfiguration().data!
        var publicConfigurationReceived: PublicConfigurationDTO?
        
        let expectationOnNext = XCTestExpectation(description: "Waiting for onNext")
        
        // when
        sut.downloadPublicConfigurationPublisher
            .subscribe(onNext: { publicConfig in
                
                publicConfigurationReceived = publicConfig
                expectationOnNext.fulfill()
            }).addDisposableTo(disposeBag)
        
        sut.publicConfigurationSuccessLoadedFromStore(publicConfigurationDTO: publicConfigurationDTO)
        
        // then
        wait(for: [expectationOnNext], timeout: 10)
        XCTAssert(publicConfigurationReceived?.publicConfig == publicConfigurationDTO.publicConfig)
    }
    
    // MARK: - grantingTicketError
    
    func test_givenAny_whenICallGrantingTicketError_thenICallFileProviderReadFileNameAsString() {
        
        // given
        let dummyFileProvider = DummyFileProvider()
        sut.fileProvider = dummyFileProvider
        
        // when
        sut.grantingTicketError(error: ErrorBO())
        
        // then
        XCTAssert(dummyFileProvider.isCalledReadFileNameAsString == true)
    }
    
    func test_givenConfigFileUnsaved_whenICallGrantingTicketError_thenICalledLoadDefaultPublicConfigurationInformationAndPublicConfigurationDTOMatch() {
        
        // given
        let dummyFileProvider = DummyFileProvider()
        sut.fileProvider = dummyFileProvider
        
        var publicConfigurationReceived: PublicConfigurationDTO?
        
        let expectationOnNext = XCTestExpectation(description: "Waiting for onNext")
        
        // when
        sut.downloadPublicConfigurationPublisher
            .subscribe(onNext: { publicConfig in
                
                publicConfigurationReceived = publicConfig
                expectationOnNext.fulfill()
            }).addDisposableTo(disposeBag)
        
        sut.grantingTicketError(error: ErrorBO())
        
        // then
        wait(for: [expectationOnNext], timeout: 10)
        XCTAssert(publicConfigurationReceived?.publicConfig == PublicConfigurationManager.DefaultPublicConfigurationDTO().publicConfig)
    }
    
    func test_givenConfigFile_whenICallGrantingTicketError_thenPublicConfigurationDTOMatch() {
        
        // given
        let dummyFileProvider = DummyFileProvider()
        sut.fileProvider = dummyFileProvider
        
        let fileName = PublicConfigurationManager.bundleDefaultConfigurationFileName + ".json"
        let fileContent = PublicConfigurationGenerator.getPublicConfigurationDataContentJson()
        
        dummyFileProvider.publicConfigurationJson = fileContent
        dummyFileProvider.writeString(fileContent, fileName: fileName)
        
        let publicConfigurationFromFileContent = PublicConfigurationDTO.deserialize(from: fileContent)
        var publicConfigurationReceived: PublicConfigurationDTO?
        
        let expectationOnNext = XCTestExpectation(description: "Waiting for onNext")
        
        // when
        sut.downloadPublicConfigurationPublisher
            .subscribe(onNext: { publicConfig in
                
                publicConfigurationReceived = publicConfig
                expectationOnNext.fulfill()
            }).addDisposableTo(disposeBag)
        
        sut.grantingTicketError(error: ErrorBO())
        
        // then
        wait(for: [expectationOnNext], timeout: 10)
        XCTAssert(publicConfigurationReceived?.publicConfig == publicConfigurationFromFileContent?.publicConfig)
    }
    
    func test_givenConfigFileWrong_whenICallGrantingTicketError_thenICalledLoadDefaultPublicConfigurationInformationAndPublicConfigurationDTOMatch() {
        
        // given
        let dummyFileProvider = DummyFileProvider()
        sut.fileProvider = dummyFileProvider
        
        dummyFileProvider.forceWrongReadFile = true
        
        let fileName = PublicConfigurationManager.bundleDefaultConfigurationFileName + ".json"
        let fileContent = "fileContentWrong"
        dummyFileProvider.writeString(fileContent, fileName: fileName)
        
        var publicConfigurationReceived: PublicConfigurationDTO?
        
        let expectationOnNext = XCTestExpectation(description: "Waiting for onNext")
        
        // when
        sut.downloadPublicConfigurationPublisher
            .subscribe(onNext: { publicConfig in
                
                publicConfigurationReceived = publicConfig
                expectationOnNext.fulfill()
            }).addDisposableTo(disposeBag)
        
        sut.grantingTicketError(error: ErrorBO())
        
        // then
        wait(for: [expectationOnNext], timeout: 10)
        XCTAssert(publicConfigurationReceived?.publicConfig == PublicConfigurationManager.DefaultPublicConfigurationDTO().publicConfig)
    }
    
    // MARK: - grantingTicketSuccess
    
    func test_givenAny_whenICallGrantingTicketSuccess_thenICalledGetPublicConfiguration() {
        
        // given
        
        // when
        sut.grantingTicketSuccess(grantingTicketResponseDTO: GrantingTicketResponseDTO())
        
        // then
        XCTAssert(dummyPublicConfigurationStore.isCalledGetPublicConfiguration == true)
    }

    // MARK: - savePublicConfiguration

    func test_givenPublicConfigurationData_whenICallSavePublicConfigurationFile_thenICallFileProviderWriteString() {

        //given

        //When
        let dummyFileProvider = DummyFileProvider()
        sut.fileProvider = dummyFileProvider
        sut.savePublicConfiguration(PublicConfigurationDTO())

        //then
        XCTAssert(dummyFileProvider.isCalledWriteString == true)
    }

    func test_givenAny_whenICallSaveToFolderConfigurationFileData_thenISaveActualDateInPreferencesManager() {

        // given

        // when
        sut.savePublicConfiguration(PublicConfigurationDTO())

        // then
        XCTAssert(dummyPreferencesManager.key == PreferencesManagerKeys.kLastTimeUpdatePublicConfiguration.rawValue)
        XCTAssert((dummyPreferencesManager.value as! Date) < Date())
    }
    
    func test_givenPublicConfigurationData_whenICallSavePublicConfigurationFile_thenPublicConfigurationDTOMatch() {
        
        //given
        let publicConfigurationDTO = PublicConfigurationGenerator.getPublicConfiguration().data!
        
        //When
        let dummyFileProvider = DummyFileProvider()
        sut.fileProvider = dummyFileProvider
        sut.savePublicConfiguration(publicConfigurationDTO)
        
        //then
        XCTAssert(dummyFileProvider.dataToWrite == publicConfigurationDTO.toJSONString())
    }

    // MARK: - IsNecessaryToUpdate
    
    func test_givenANonSavedConfiguration_whenICallIsNecessaryToUpdate_thenIsNecessaryToUpdateTrue() {
        
        //given
        dummyPreferencesManager.key = nil
        dummyPreferencesManager.value = nil
        
        //when
        let isNecessaryToUpdate = sut.isNecessaryToUpdate
        
        //then
        XCTAssert(isNecessaryToUpdate == true)
    }
    
    func test_givenAConfigurationSavedLessThanTimeIntervalToUpdatePublicConfiguration_whenICallIsNecessaryToUpdate_thenIsNecessaryToUpdateFalse() {

        //given
        dummyPreferencesManager.key = PreferencesManagerKeys.kLastTimeUpdatePublicConfiguration.rawValue
        dummyPreferencesManager.value = Date() as AnyObject

        //when
        let isNecessaryToUpdate = sut.isNecessaryToUpdate

        //then
        XCTAssert(isNecessaryToUpdate == false)
    }

    func test_givenAConfigurationSavedMoreThanTimeIntervalToUpdatePublicConfiguration_whenICallIsNecessaryToUpdate_thenIsNecessaryToUpdateTrue() {

        //given
        let date = Calendar.current.date(byAdding: .minute, value: -PublicConfigurationManager.minutesTimestampToUpdatePublicConfiguration, to: Date())
        dummyPreferencesManager.key = PreferencesManagerKeys.kLastTimeUpdatePublicConfiguration.rawValue
        dummyPreferencesManager.value = date as AnyObject

        //when
        let isNecessaryToUpdate = sut.isNecessaryToUpdate

        //then
        XCTAssert(isNecessaryToUpdate == true)
    }

    // MARK: - GetPublicConfiguration
    
    func test_givenAnonymousSession_whenICallGetPublicConfiguration_thenICallStoreGetPublicConfiguration() {

        //given
        dummySessionManager.isUserAnonymous = true
        
        //when
        _ = sut.getPublicConfiguration()

        //then
        XCTAssert(dummyPublicConfigurationStore.isCalledGetPublicConfiguration == true)
    }
    
    func test_givenUserLogged_whenICallGetPublicConfiguration_thenICallStoreGetPublicConfiguration() {
        
        //given
        dummySessionManager.isUserLogged = true
        
        //when
        _ = sut.getPublicConfiguration()
        
        //then
        XCTAssert(dummyPublicConfigurationStore.isCalledGetPublicConfiguration == true)
    }
    
    func test_givenNotAnonymousSessionAndUserNotLogged_whenICallGetPublicConfiguration_thenINotCallStoreGetPublicConfiguration() {
        
        //given
        dummySessionManager.isUserLogged = false
        dummySessionManager.isUserAnonymous = false
        
        //when
        _ = sut.getPublicConfiguration()
        
        //then
        XCTAssert(dummyPublicConfigurationStore.isCalledGetPublicConfiguration == false)
    }
    
    func test_givenNotAnonymousSessionAndUserNotLogged_whenICallGetPublicConfiguration_thenICallStoreGrantingTicketAnonymous() {
        
        //given
        dummySessionManager.isUserLogged = false
        dummySessionManager.isUserAnonymous = false
        
        //when
        _ = sut.getPublicConfiguration()
        
        //then
        XCTAssert(dummyGrantingTicketStore.isCalledGrantingTicketAnonymous == true)
    }
    
    func test_givenNotAnonymousSessionAndUserNotLogged_whenICallGetPublicConfiguration_thenICallStoreGrantingTicketAnonymousAndMatchGrantingTicketDTO() {
        
        //given
        dummySessionManager.isUserLogged = false
        dummySessionManager.isUserAnonymous = false
        
        let anonymousInformationBO = Configuration.anonymousInformation()
        
        let grantingTicketDTO = GrantingTicketDTO(consumerID: anonymousInformationBO.consumerId,
                                               userID: anonymousInformationBO.userId,
                                               authenticationType: "61",
                                               password: nil,
                                               idAuthenticationData: nil,
                                               backendUserRequestUserID: "",
                                               accessCode: anonymousInformationBO.userId,
                                               dialogId: "")
        
        //when
        _ = sut.getPublicConfiguration()
        
        //then
        XCTAssert(dummyGrantingTicketStore.grantingTicketSended?.consumerID == grantingTicketDTO.consumerID)
        XCTAssert(dummyGrantingTicketStore.grantingTicketSended?.userID == grantingTicketDTO.userID)
        XCTAssert(dummyGrantingTicketStore.grantingTicketSended?.authenticationType == grantingTicketDTO.authenticationType)
        XCTAssert(dummyGrantingTicketStore.grantingTicketSended?.password == grantingTicketDTO.password)
        XCTAssert(dummyGrantingTicketStore.grantingTicketSended?.idAuthenticationData == grantingTicketDTO.idAuthenticationData)
        XCTAssert(dummyGrantingTicketStore.grantingTicketSended?.backendUserRequestUserID == grantingTicketDTO.backendUserRequestUserID)
        XCTAssert(dummyGrantingTicketStore.grantingTicketSended?.accessCode == grantingTicketDTO.accessCode)
        XCTAssert(dummyGrantingTicketStore.grantingTicketSended?.dialogId == grantingTicketDTO.dialogId)
    }
    
    func test_givenUserLogged_whenICallGetPublicConfiguration_thenINotCallStoreGrantingTicketAnonymous() {
        
        //given
        dummySessionManager.isUserLogged = true
        
        //when
        _ = sut.getPublicConfiguration()
        
        //then
        XCTAssert(dummyGrantingTicketStore.isCalledGrantingTicketAnonymous == false)
    }
}

class DummyPublicConfigurationStore: PublicConfigurationStore {

    var isCalledGetPublicConfiguration = false

    override func getPublicConfiguration() {

        isCalledGetPublicConfiguration = true
    }
}

class DummyGrantingTicketStore: GrantingTicketStore {
    
    var isCalledGrantingTicketAnonymous = false
    var grantingTicketSended: GrantingTicketDTO?
    
    override func grantingTicketAnonymous(grantingTicket: GrantingTicketDTO) {
        
        isCalledGrantingTicketAnonymous = true
        grantingTicketSended = grantingTicket
    }
}

class DummyFileProvider: FileProvider {

    var isCalledReadFileNameAsString = false
    var isCalledWriteString = false
    var fileNameSent = ""
    var dataToWrite = ""
    var forceWrongReadFile = false
    var publicConfigurationJson: String = ""

    override func readFileNameAsString(forFileName fileName: String) -> String {

        isCalledReadFileNameAsString = true
        fileNameSent = fileName
        
        if forceWrongReadFile {
            return "FileWrong"
        }
        
        return publicConfigurationJson
    }

    override func writeString(_ data: String, fileName: String) {

        dataToWrite = data
        fileNameSent = fileName
        isCalledWriteString = true
    }
}
