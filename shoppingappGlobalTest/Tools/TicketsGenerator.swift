//
//  TicketsGenerator.swift
//  shoppingapp
//
//  Created by jesus.martinez on 16/5/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class TicketsGenerator {

    class func acceptedPurchase() -> TicketModel {

        let stringTicket = "{\"aps\":{\"alert\":{\"loc-args\":[\"01424\",\"481515\",\"SUSHI TAI\",\"3.21\",\"MXP\",\"160722084808\",\"EDW PLAT\"],\"loc-key\":\"00001\"},\"sound\":\"default\"}}"

        let jsonData = stringTicket.data(using: .utf8)
        let dictionaryTicket = try? JSONSerialization.jsonObject(with: jsonData!, options: .mutableLeaves)

        let ticketModel = TicketModel(withPayload: dictionaryTicket as! [AnyHashable: Any])

        return ticketModel

    }

    class func deniedPurchase() -> TicketModel {

        let stringTicket = "{\"aps\":{\"alert\":{\"loc-args\":[\"01424\",\"481515\",\"SUSHI TAI\",\"3.21\",\"MXP\",\"160722084808\",\"EDW PLAT\"],\"loc-key\":\"00050\"},\"sound\":\"default\"}}"

        let jsonData = stringTicket.data(using: .utf8)
        let dictionaryTicket = try? JSONSerialization.jsonObject(with: jsonData!, options: .mutableLeaves)

        let ticketModel = TicketModel(withPayload: dictionaryTicket as! [AnyHashable: Any])

        return ticketModel

    }

}
