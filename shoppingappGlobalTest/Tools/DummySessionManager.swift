//
//  DummySessionManager.swift
//  shoppingapp
//
//  Created by Jesús Ángel Sánchez Sánchez on 12/3/19.
//  Copyright © 2019 BBVA. All rights reserved.
//

import Foundation

#if TESTMX
@testable import shoppingappMX
#elseif TESTPE
@testable import shoppingappPE
#endif

class DummySessionManager: SessionDataManager {
    
    class func setupMock() {
        
        super.instance = DummySessionManager()
    }
    
    var isCalledCloseSession = false
    var isCalledRequestLogout = false
    var isCalledClearTabBarSelected = false
    
    override func closeSession() {
        
        isCalledCloseSession = true
    }
    
    override func requestLogout() {
        
        isCalledRequestLogout = true
    }
    
    override func clearTabBarSelected() {
        
        isCalledClearTabBarSelected = true
    }
}
