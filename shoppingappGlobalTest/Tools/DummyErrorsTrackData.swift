//
//  DummyErrorsTrackData.swift
//  shoppingapp
//
//  Created by jesus.martinez on 2/1/19.
//  Copyright © 2019 BBVA. All rights reserved.
//

#if TESTMX
@testable import shoppingappMX
#elseif TESTPE
@testable import shoppingappPE
#endif

import Foundation

class DummyErrorTrackData: ErrorTrackData {
    
    var isCalledReset = false
    var isCalledAppend = false
    var isCalledFormat = false
    
    var errorCodeReceived: Int?
    var errorNameReceived = ""
    var forceIsEmptyValue = false
    
    override func reset() {
        
        super.reset()
        
        isCalledReset = true
    }
    
    override func append(errorCode: Int?, errorName: String) {
        
        super.append(errorCode: errorCode, errorName: errorName)
        
        isCalledAppend = true
        
        errorCodeReceived = errorCode
        errorNameReceived = errorName
    }
    
    override func format() -> String {
        
        isCalledFormat = true
        
        return super.format()
    }
    
    override func isEmpty() -> Bool {
        return forceIsEmptyValue
    }
}
