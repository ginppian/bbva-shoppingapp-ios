//
//  CustomDateTest.swift
//  shoppingapp
//
//  Created by jesus.martinez on 12/7/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class CustomDateTest: XCTestCase {

    let calendar = Calendar(identifier: Calendar.Identifier.gregorian)

    func test_givenDate_whenICallLocal_thenReturnCurrentLocale() {

        // when
        let locale = Date.locale()

        // then
        XCTAssert(locale == Locale.current)

    }

    func test_givenALocale_whenICallDateFormatterLocalized_thenReturnDateFormatterWithTheSameLocale() {

        // given
        let locale = Date.locale()

        // when
        let dateFormatter = Date.dateFormatterLocalized(withLocale: locale)

        // then
        XCTAssert(dateFormatter.locale == locale)

    }

    func test_givenDateStringInAnAppropiatedFormat_whenICallDateFromStringWithFormat_thenReturnADateMatchedWithDateString() {

        // given
        let year = 2011
        let month = 11
        let day = 12
        let dateString = "\(year)-\(month)-\(day)"
        let format = Date.DATE_FORMAT_SERVER

        // when
        let date = Date.date(fromLocalTimeZoneString: dateString, withFormat: format)

        // then
        let dateComponents = calendar.dateComponents([.year, .month, .day], from: date!)
        XCTAssert(dateComponents.year == year)
        XCTAssert(dateComponents.month == month)
        XCTAssert(dateComponents.day == day)

    }

    func test_givenDateStringInAnBadFormat_whenICallDateFromStringWithFormat_thenReturnNil() {

        // given
        let year = 2011
        let month = 11
        let day = 12
        let dateString = "\(year)-\(month)-\(day)"
        let format = Date.DATE_FORMAT_LONG_SERVER

        // when
        let date = Date.date(fromLocalTimeZoneString: dateString, withFormat: format)

        // then
        XCTAssert(date == nil)

    }

    func test_givenADateAndAnAppropiateFormat_whenICallStringFormat_thenReturnADateStringFormatted() {

        // given
        let dateExpected = "2012-12-12"
        let format = Date.DATE_FORMAT_SERVER
        let date = Date.date(fromLocalTimeZoneString: dateExpected, withFormat: format)!

        // when
        let dateFormatted = date.string(format: format)

        // then
        XCTAssert(dateExpected == dateFormatted)

    }

}
