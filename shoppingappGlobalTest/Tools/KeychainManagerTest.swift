//
//  KeychainManagerTest.swift
//  shoppingapp
//
//  Created by Azize Bulbul on 12/01/2018.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class KeychainManagerTest: XCTestCase {

    override func setUp() {
        super.setUp()
        
        KeychainManagerTestHelper.configureKeychainManagerForTesting()
    }

    override func tearDown() {
        super.tearDown()
    }
    
    // MARK: - UniqueIdentifier
    
    func test_givenUniqueIdentifier_whenICallGetUniqueIdentifier_thenUniqueIdentifierMatch() {
        
        // given
        let expectedUniqueIdentifier: String = "UniqueIdentifier"
        KeychainManager.shared.saveUniqueIdentifier(forUniqueIdentifier: expectedUniqueIdentifier)
        
        // when
        let uniqueIdentifier = KeychainManager.shared.getUniqueIdentifier()
        
        // then
        XCTAssert(uniqueIdentifier == expectedUniqueIdentifier)
    }
    
    // MARK: - isUserSaved
    
    func test_givenASavedUsernameAndUserId_whenICallIsUserSaved_thenIGetTrue() {
        
        //given
        KeychainManager.shared.saveFirstname(forFirstname: "firstname")
        KeychainManager.shared.saveUserId(forUserId: "userId")
        
        //when
        let isSaved = KeychainManager.shared.isUserSaved()
        
        //then
        XCTAssert(isSaved == true)
    }
    
    func test_givenOnlySavedUsername_whenICallsUserSaved_thenIGetFalse() {
        
        //given
        KeychainManager.shared.saveFirstname(forFirstname: "firstname")
        
        //when
        let isSaved = KeychainManager.shared.isUserSaved()
        
        //then
        XCTAssert(isSaved == false)
    }
    
    // Tests that do not apply to MX
    #if !TESTMX
    func test_givenOnlySavedUserId_whenICallsUserSaved_thenIGetFalse() {
        
        //given
        KeychainManager.shared.saveUserId(forUserId: "userId")
        
        //when
        let isSaved = KeychainManager.shared.isUserSaved()
        
        //then
        XCTAssert(isSaved == false)
    }
    #endif
    
    // MARK: - UserId
    
    func test_givenAnEmptyUserId_whenICallGetUserId_thenUserIdIsEmpty() {
        
        //given
        let expectedUserId: String = ""
        KeychainManager.shared.saveUserId(forUserId: expectedUserId)
        
        //when
        let userId = KeychainManager.shared.getUserId()
        
        //then
        XCTAssert(userId == expectedUserId)
    }
    
    func test_givenAnUserId_whenICallGetUserId_thenIGetCorrectUserId() {
        
        //given
        let expectedUserId: String = "userId"
        KeychainManager.shared.saveUserId(forUserId: expectedUserId)
        
        //when
        
        let userId = KeychainManager.shared.getUserId()
        
        //then
        XCTAssert(userId == expectedUserId)
    }
    
    // MARK: - Firstname

    func test_givenAnEmptyUsername_whenICallGetFirstname_thenIGetEmptyUsername() {

        //given
        let expectedUsername: String = ""
        KeychainManager.shared.saveFirstname(forFirstname: expectedUsername)

        //when
        let username = KeychainManager.shared.getFirstname()

        //then
        XCTAssert(username == expectedUsername)
    }
    
    func test_givenAnUsername_whenICallGetFirstname_thenIGetCorrectUsername() {

        //given
        let expectedUsername: String = "username"
        KeychainManager.shared.saveFirstname(forFirstname: expectedUsername)

        //when
        let username = KeychainManager.shared.getFirstname()

        //then
        XCTAssert(username == expectedUsername)
    }
    
    // MARK: - resetKeyChainItems
    
    func test_givenAny_whenICallResetKeyChainItems_thenIGetResetItems() {

        //given

        //when
        KeychainManager.shared.resetKeyChainItems()

        //then
        XCTAssert(KeychainManager.shared.getUserId() == "")
        XCTAssert(KeychainManager.shared.getFirstname() == "")
        XCTAssert(KeychainManager.shared.getUniqueIdentifier() == nil)
    }
}
