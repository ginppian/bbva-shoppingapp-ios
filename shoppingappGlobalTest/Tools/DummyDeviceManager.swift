//
//  DummyDeviceManager.swift
//  shoppingapp
//
//  Created by daniel.petrovic on 26/7/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

#if TESTMX
@testable import shoppingappMX
#elseif TESTPE
@testable import shoppingappPE
#endif

class DummyDeviceManager: DeviceManager {

    var modelName: String = "iphone 7"
    var operatingSystemVersion: String = "10.2"
    var operatingSystem: String
    var bundleVersion: String = "v01"

    override init() {
        operatingSystem = "iOS"
      
        super.init()
    }
    override func getModelName() -> String {
        return modelName
    }

    override func getOperatingSystemVersion() -> String {
        return operatingSystemVersion
    }

    override func getOperatingSystem() -> String {
        return operatingSystem
    }

    override func getValueBundleKey(forKey key: DevicesManagerKeys) -> Any? {
        if key == DevicesManagerKeys.CFBundleShortVersionString {
            return bundleVersion
        } else {
            return ""
        }
    }

}
