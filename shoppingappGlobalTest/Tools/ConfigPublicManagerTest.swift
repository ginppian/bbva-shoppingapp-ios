//
//  ConfigPublicManagerTest.swift
//  shoppingapp
//
//  Created by AZIZEBULBUL on 29/01/2018.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest
import RxSwift

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class ConfigPublicManagerTest: XCTestCase {

    var sut: ConfigPublicManager!
    var dummyPublicManager: DummyPublicManager!
    let dummyDidacticAreaDataManager = DummyDidacticAreaDataManager()
    let dummyConfigPublicDataManager = DummyConfigPublicDataManager()
    let dummyPreferencesManager = DummyPreferencesManager()

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        dummyPublicManager = DummyPublicManager()

        sut = ConfigPublicManager.sharedInstance()

        DidacticAreaDataManager.sharedInstance = dummyDidacticAreaDataManager
        PreferencesManager.instance = dummyPreferencesManager
        ConfigPublicDataManager.sharedInstance = dummyConfigPublicDataManager
    }

    // MARK: Help file

    func test_givenAny_whenICallGetConfigFile_thenICallGetConfigFile() {
       //given

        //when
        dummyPublicManager.getConfigFile()

        //then
        XCTAssert(dummyPublicManager.isCalledGetConfigFile == true)

    }

    func test_givenAny_whenICallWriteToPlist_thenICallWriteToPlistWithTheSameFileName() {

        //given

        //when
        dummyPublicManager.writeToPlist(forItems: "test Description", withFilename: "test")

        //then
        XCTAssert(dummyPublicManager.isCalledWriteToPlist == true)
        XCTAssert(dummyPublicManager.fileName == "test")
        XCTAssert(dummyPublicManager.dataJSONString == "test Description")

    }

    func test_givenASavedData_whenICallReadFromFile_thenICallReadFromFileWithTheSameFileName() {

        //given
        dummyPublicManager.writeToPlist(forItems: "test Description", withFilename: "test")

        //when
       let data = dummyPublicManager.readFromSavedFile(forFilename: "test")

        //then
        XCTAssert(data == sut.readFromSavedFile(forFilename: "test"))
        XCTAssert(dummyPublicManager.isCalledReadFromFile == true)
        XCTAssert(dummyPublicManager.fileName == "test")

    }

    func test_givenANonSavedData_whenICallReadFromFile_thenICallReadFromFileWithTheSameFileName() {

        //given

        //when
        let data = dummyPublicManager.readFromSavedFile(forFilename: "test")

        //then
        XCTAssert(data.isEmpty)
        XCTAssert(dummyPublicManager.isCalledReadFromFile == true)
        XCTAssert(dummyPublicManager.fileName == "test")

    }

    func test_givenConfigsWithNoValues_whenICallGetHelpBo_thenICallHelpBoAndCorrectData() {

        //given
        dummyPublicManager.configs = nil

        //when
        let helpBo = dummyPublicManager.getConfigBO(forSection: "cards", withKey: PreferencesManagerKeys.kIndexToShowCards)

        //then
        XCTAssert(dummyPublicManager.isCalledHelpBO == true)
        XCTAssert(helpBo == nil)

    }

    func test_givenConfigurationWithValuesForKeyCard_whenICallGetHelp_thenICallGetConfigBOBySectionAndCorrectKeyAndWithSectionValues() {

        //given
        dummyPublicManager.configs = ConfigGenerator.getHelpBO()

        //When
        let array = dummyPublicManager.getConfigBOBySection(forKey: "cards")

        //then
        XCTAssert(dummyPublicManager.isCalledGetConfigBOBySection == true)
        XCTAssert(dummyPublicManager.sentCardKey == "cards")
        XCTAssert(!array.isEmpty)

    }

    func test_givenConfigurationWithValuesForKeyDifferentThanCard_whenICallGetHelp_thenICallGetConfigBOBySectionAndNoValue() {

        //given

        dummyPublicManager.configs = ConfigGenerator.getHelpBO()

        //When
        let array = dummyPublicManager.getConfigBOBySection(forKey: "test")

        //then
        XCTAssert(dummyPublicManager.isCalledGetConfigBOBySection == true)
        XCTAssert(array.isEmpty)

    }

    func test_givenAny_whenICallGetHelp_thenICallGetConfigBOBySectionAndNoValue() {

        //given

        dummyPublicManager.configs = ConfigGenerator.getHelpBO()

        //When
        let array = dummyPublicManager.getConfigBOBySection(forKey: "test")

        //then
        XCTAssert(dummyPublicManager.isCalledGetConfigBOBySection == true)
        XCTAssert(array.isEmpty)

    }

    func test_givenASavedData_whenICallRemoveExistingFile_thenIRemoveTheFile() {

        //given
        dummyPublicManager.writeToPlist(forItems: "test Description", withFilename: "test")

        //when
        dummyPublicManager.removeExistingFile(withFileName: "test")

        //then
        XCTAssert(dummyPublicManager.isCalledRemovedExistingFile == true)
        XCTAssert(dummyPublicManager.fileName == "test")

    }

    // MARK: Help (getJsonFileURL)

    func test_givenAny_whenICallGetJsonFileURL_thenResultFilledAndIsTheSameFileName() {

        // given

        // when
        let result = sut.getJsonFileURL(withFilename: Constants.didactic_area_file)

        // then
        XCTAssert(result.absoluteString.isEmpty == false)
        XCTAssert(result.absoluteString.range(of: Constants.didactic_area_file) != nil)
    }

    // MARK: Help (readJson)

    func test_givenFileNoExit_whenICallReadJson_thenResultIsEmpty() {

        // given

        // when
        let result = sut.readJson(withFilename: "fileNoExist")

        // then
        XCTAssert(result.isEmpty == true)
    }

    // MARK: Help (writeJson)

    func test_givenAny_whenICallWriteJson_thenResultFilledAndDataSavedIsTheSame() {

        // given

        // when
        sut.writeJson(withData: "data", andFilename: "fileName")

        // then
        XCTAssert(sut.readJson(withFilename: "fileName").isEmpty == false)
        XCTAssert(sut.readJson(withFilename: "fileName") == "data")
    }

    // MARK: Help (getConfigFile)

    func test_givenAny_whenICallGetConfigFile_thenICallConfigPublicDataManagerProvideHelpConfigFile() {

        // given

        // when
        sut.getConfigFile()

        // then
        XCTAssert(dummyConfigPublicDataManager.isCalledProvideHelpConfigFile == true)

    }

    func test_givenAny_whenICallGetConfigFile_thenConfigsShouldBeFilled() {

        // given

        // when
        sut.getConfigFile()

        // then
        XCTAssert(sut.configs != nil)
    }

    func test_givenConfigFilesBO_whenICallGetConfigFile_thenConfigFilesBOMatchWithInteractorReturned() {

        // given

        // when
        sut.getConfigFile()

        let configFilesBO = ConfigFilesBO(configFilesEntity: dummyConfigPublicDataManager.configFilesEntity!)

        // then
        XCTAssert(sut.configs?.status == configFilesBO.status)
        XCTAssert(sut.configs?.version == configFilesBO.version)

        XCTAssert(sut.configs?.items[0].itemId == configFilesBO.items[0].itemId)
        XCTAssert(sut.configs?.items[0].image == configFilesBO.items[0].image)
        XCTAssert(sut.configs?.items[0].platform == configFilesBO.items[0].platform)

        XCTAssert(sut.configs?.items[0].help.backgroundColor?.alpha == configFilesBO.items[0].help.backgroundColor?.alpha)
        XCTAssert(sut.configs?.items[0].help.backgroundColor?.hexString == configFilesBO.items[0].help.backgroundColor?.hexString)
        XCTAssert(sut.configs?.items[0].help.titleColor?.alpha == configFilesBO.items[0].help.titleColor?.alpha)
        XCTAssert(sut.configs?.items[0].help.titleColor?.hexString == configFilesBO.items[0].help.titleColor?.hexString)
        XCTAssert(sut.configs?.items[0].help.descriptionColor?.alpha == configFilesBO.items[0].help.descriptionColor?.alpha)
        XCTAssert(sut.configs?.items[0].help.descriptionColor?.hexString == configFilesBO.items[0].help.descriptionColor?.hexString)
        XCTAssert(sut.configs?.items[0].help.descriptionOnTapColor?.alpha == configFilesBO.items[0].help.descriptionOnTapColor?.alpha)
        XCTAssert(sut.configs?.items[0].help.descriptionOnTapColor?.hexString == configFilesBO.items[0].help.descriptionOnTapColor?.hexString)

        XCTAssert(sut.configs?.items[0].texts![0].language == configFilesBO.items[0].texts![0].language)
        XCTAssert(sut.configs?.items[0].texts![0].title == configFilesBO.items[0].texts![0].title)
        XCTAssert(sut.configs?.items[0].texts![0].description == configFilesBO.items[0].texts![0].description)

        XCTAssert(sut.configs?.items[0].sections![0].sectionId == configFilesBO.items[0].sections![0].sectionId)
    }

    func test_givenAny_whenICallGetConfigFile_thenICallGetValueWithPreferencesKey() {

        // given

        // when
        sut.getConfigFile()

        // then
        XCTAssert(dummyPreferencesManager.isCalledGetValueWithPreferencesKey == true)
    }

    func test_givenAny_whenICallGetConfigFile_thenICallSaveValueWithPreferencesKey() {

        // given

        // when
        sut.getConfigFile()

        // then
        XCTAssert(dummyPreferencesManager.isCalledSaveValueWithPreferencesKey == true)
    }

    func test_givenEqualVersion_whenICallGetConfigFile_thenICallGetVersionAndMatchVersion() {

        // given
        dummyPreferencesManager.forceNewVersion = false

        // when
        sut.getConfigFile()

        // then
        XCTAssert(dummyPreferencesManager.version == sut.configs?.version)
    }

    func test_givenNewVersion_whenICallGetConfigFile_thenICallGetVersionAndNotMatchVersion() {

        // given
        dummyPreferencesManager.forceNewVersion = true

        // when
        sut.getConfigFile()

        // then
        XCTAssert(dummyPreferencesManager.version != sut.configs?.version)
    }

    func test_givenNewVersion_whenICallGetConfigFile_thenICallDeleteValueWithPreferencesKey() {

        // given
        dummyPreferencesManager.forceNewVersion = true

        // when
        sut.getConfigFile()

        // then
        XCTAssert(dummyPreferencesManager.isCalledDeleteValueWithPreferencesKey == true)
    }

    // MARK: Help (getConfigBOBySection)

    func test_givenKeySectionValidAndNotTextForCurrentDefaultLanguage_whenICallGetConfigBOBySection_thenResultShouldBeEmpty() {

        // given
        sut.configs = ConfigGenerator.getHelpBO()
        let keySection = Constants.SECTION_TO_SHOW_CARDS

        for configFileBO in sut.configs!.items {

            for i in 0..<configFileBO.texts.count {
                configFileBO.texts[i].language = "fr"
            }
        }

        // when
        let result = sut.getConfigBOBySection(forKey: keySection)

        // then
        XCTAssert(result.isEmpty)
    }

    func test_givenKeySectionValid_whenICallGetConfigBOBySection_thenResultFilled() {

        // given
        sut.configs = ConfigGenerator.getHelpBO()
        let keySection = Constants.SECTION_TO_SHOW_CARDS

        // when
        let result = sut.getConfigBOBySection(forKey: keySection)

        // then
        XCTAssert(!result.isEmpty)
    }

    func test_givenKeySectionValid_whenICallGetConfigBOBySection_thenIGetConfigFileBOWithEqualSectionId() {

        // given
        sut.configs = ConfigGenerator.getHelpBO()
        let keySection = Constants.SECTION_TO_SHOW_CARDS

        // when
        let result = sut.getConfigBOBySection(forKey: keySection)

        // then
        XCTAssert(result[0].sections[0].sectionId == keySection)
    }

    func test_givenKeySectionValid_whenICallGetConfigBOBySection_thenIGetConfigFileBOMatchWithConfigsReturned() {

        // given
        sut.configs = ConfigGenerator.getHelpBO()
        let keySection = Constants.SECTION_TO_SHOW_CARDS

        // when
        let result = sut.getConfigBOBySection(forKey: keySection)

        // then
        XCTAssert(sut.configs?.items[0].itemId == result[0].itemId)
        XCTAssert(sut.configs?.items[0].image == result[0].image)
        XCTAssert(sut.configs?.items[0].platform == result[0].platform)

        XCTAssert(sut.configs?.items[0].section.backgroundColor?.alpha == result[0].section.backgroundColor?.alpha)
        XCTAssert(sut.configs?.items[0].section.backgroundColor?.hexString == result[0].section.backgroundColor?.hexString)
        XCTAssert(sut.configs?.items[0].section.titleColor?.alpha == result[0].section.titleColor?.alpha)
        XCTAssert(sut.configs?.items[0].section.titleColor?.hexString == result[0].section.titleColor?.hexString)
        XCTAssert(sut.configs?.items[0].section.descriptionColor?.alpha == result[0].section.descriptionColor?.alpha)
        XCTAssert(sut.configs?.items[0].section.descriptionColor?.hexString == result[0].section.descriptionColor?.hexString)
        XCTAssert(sut.configs?.items[0].section.descriptionOnTapColor?.alpha == result[0].section.descriptionOnTapColor?.alpha)
        XCTAssert(sut.configs?.items[0].section.descriptionOnTapColor?.hexString == result[0].section.descriptionOnTapColor?.hexString)

        XCTAssert(sut.configs?.items[0].texts![0].language == result[0].texts![0].language)
        XCTAssert(sut.configs?.items[0].texts![0].title == result[0].texts![0].title)
        XCTAssert(sut.configs?.items[0].texts![0].description == result[0].texts![0].description)

        XCTAssert(sut.configs?.items[0].sections![0].sectionId == result[0].sections![0].sectionId)
    }

    func test_givenKeySectionInValid_whenICallGetConfigBOBySection_thenResultEmpty() {

        // given
        sut.configs = ConfigGenerator.getHelpBO()
        let keySection = "section_invalid"

        // when
        let result = sut.getConfigBOBySection(forKey: keySection)

        // then
        XCTAssert(result.isEmpty)
    }

    func test_givenKeySectionValidAndSectionsNil_whenICallGetConfigBOBySection_thenResultEmpty() {

        // given
        sut.configs = ConfigGenerator.getHelpBO()
        sut.configs?.items[0].sections = nil
        let keySection = Constants.SECTION_TO_SHOW_CARDS

        // when
        let result = sut.getConfigBOBySection(forKey: keySection)

        // then
        XCTAssert(result.isEmpty)
    }

    func test_givenKeySectionValidAndSectionIdNil_whenICallGetConfigBOBySection_thenResultEmpty() {

        // given
        sut.configs = ConfigGenerator.getHelpBO()
        sut.configs?.items[0].sections[0].sectionId = nil
        let keySection = Constants.SECTION_TO_SHOW_CARDS

        // when
        let result = sut.getConfigBOBySection(forKey: keySection)

        // then
        XCTAssert(result.isEmpty)
    }

    func test_givenKeySectionValidAndSectionIdEmpty_whenICallGetConfigBOBySection_thenResultEmpty() {

        // given
        sut.configs = ConfigGenerator.getHelpBO()
        sut.configs?.items[0].sections[0].sectionId = ""
        let keySection = Constants.SECTION_TO_SHOW_CARDS

        // when
        let result = sut.getConfigBOBySection(forKey: keySection)

        // then
        XCTAssert(result.isEmpty)
    }

    // MARK: Help (getConfigBO)

    func test_givenHelpBOWithValidSection_whenICallGetConfigBO_thenConfigFileBOReturnedFilled() {

        // given
        sut.configs = ConfigGenerator.getHelpBO()
        let section = Constants.SECTION_TO_SHOW_CARDS
        let keyPreferences = PreferencesManagerKeys.kIndexToShowCards

        // when
        let result = sut.getConfigBO(forSection: section, withKey: keyPreferences)

        // then
        XCTAssert(result != nil)
    }

    func test_givenHelpBOWithValidSection_whenICallGetConfigBO_thenConfigFileBOMatchWithReturnedConfigBO() {

        // given
        sut.configs = ConfigGenerator.getHelpBO()
        let section = Constants.SECTION_TO_SHOW_CARDS
        let keyPreferences = PreferencesManagerKeys.kIndexToShowCards

        // when
        let result = sut.getConfigBO(forSection: section, withKey: keyPreferences)

        // then
        XCTAssert(result === sut.configs?.items[0])
    }

    func test_givenHelpBOWithInValidSection_whenICallGetConfigBO_thenConfigFileBOReturnedEmpty() {

        // given
        sut.configs = ConfigGenerator.getHelpBO()
        let section = "section_invalid"
        let keyPreferences = PreferencesManagerKeys.kIndexToShowCards

        // when
        let result = sut.getConfigBO(forSection: section, withKey: keyPreferences)

        // then
        XCTAssert(result == nil)
    }

    func test_givenHelpBOWithValidSectionAndSectionsNil_whenICallGetConfigBO_thenConfigFileBOReturnedEmpty() {

        // given
        sut.configs = ConfigGenerator.getHelpBO()
        sut.configs?.items[0].sections = nil
        let section = Constants.SECTION_TO_SHOW_CARDS
        let keyPreferences = PreferencesManagerKeys.kIndexToShowCards

        // when
        let result = sut.getConfigBO(forSection: section, withKey: keyPreferences)

        // then
        XCTAssert(result == nil)
    }

    func test_givenHelpBOWithValidSectionAndSectionIdNil_whenICallGetConfigBO_thenConfigFileBOReturnedEmpty() {

        // given
        sut.configs = ConfigGenerator.getHelpBO()
        sut.configs?.items[0].sections[0].sectionId = nil
        let section = Constants.SECTION_TO_SHOW_CARDS
        let keyPreferences = PreferencesManagerKeys.kIndexToShowCards

        // when
        let result = sut.getConfigBO(forSection: section, withKey: keyPreferences)

        // then
        XCTAssert(result == nil)
    }

    func test_givenHelpBOWithValidSectionAndSectionIdEmpty_whenICallGetConfigBO_thenConfigFileBOReturnedEmpty() {

        // given
        sut.configs = ConfigGenerator.getHelpBO()
        sut.configs?.items[0].sections[0].sectionId = ""
        let section = Constants.SECTION_TO_SHOW_CARDS
        let keyPreferences = PreferencesManagerKeys.kIndexToShowCards

        // when
        let result = sut.getConfigBO(forSection: section, withKey: keyPreferences)

        // then
        XCTAssert(result == nil)
    }

    func test_givenConfigFilesBO_whenICallGetIndexToShowed_thenIndexToShowIsCorrect() {

        // given
        sut.configs = ConfigGenerator.getHelpBO()
        let keyPreferences = PreferencesManagerKeys.kIndexToShowCards
        var configFilesBO = [ConfigFileBO]()
        configFilesBO.append((sut.configs?.items[0])!)

        // when
        let indexToShow = sut.getIndexToShowed(forKey: keyPreferences, withSectionsArray: configFilesBO)

        // then
        XCTAssert(indexToShow >= 0)
    }

    func test_givenConfigFilesBO_whenICallGetIndexToShowed_thenICallGetValueWithPreferencesKey() {

        // given
        sut.configs = ConfigGenerator.getHelpBO()
        let keyPreferences = PreferencesManagerKeys.kIndexToShowCards
        var configFilesBO = [ConfigFileBO]()
        configFilesBO.append((sut.configs?.items[0])!)

        // when
        _ = sut.getIndexToShowed(forKey: keyPreferences, withSectionsArray: configFilesBO)

        // then
        XCTAssert(dummyPreferencesManager.isCalledGetValueWithPreferencesKey == true)
    }

    func test_givenConfigFilesBO_whenICallGetIndexToShowed_thenICallSaveValueWithPreferencesKeyWithEqualIndexToShowSaved() {

        // given
        sut.configs = ConfigGenerator.getHelpBO()
        let keyPreferences = PreferencesManagerKeys.kIndexToShowCards
        var configFilesBO = [ConfigFileBO]()
        configFilesBO.append((sut.configs?.items[0])!)

        // when
        let indexToShow = sut.getIndexToShowed(forKey: keyPreferences, withSectionsArray: configFilesBO)

        // then
        XCTAssert(dummyPreferencesManager.isCalledSaveValueWithPreferencesKey == true)
        XCTAssert(dummyPreferencesManager.indexToShowSaved as! Int == indexToShow)
    }

    func test_givenKeySectionValidAndSectionNil_whenICallGetConfigBOBySection_thenResultSectionEmpty() {
        
        // given
        sut.configs = ConfigGenerator.getHelpBO()
        sut.configs?.items[0].section = nil
        let keySection = Constants.SECTION_TO_SHOW_CARDS
        
        // when
        let result = sut.getConfigBOBySection(forKey: keySection)
        
        // then
        XCTAssert(result[0].section == nil)
    }
    
    // MARK: Didactic Area (getDidacticArea)

    func test_givenAny_whenICallGetDidacticArea_thenICallDidacticAreaDataManagerServiceDidacticArea() {

        // given

        // when
        sut.getDidacticArea()

        // then
        XCTAssert(dummyDidacticAreaDataManager.isCalledServiceDidacticArea == true)

    }

    func test_givenAny_whenICallGetDidacticArea_thenDidacticAreaShouldBeFilled() {

        // given

        // when
        sut.getDidacticArea()

        // then
        XCTAssert(sut.didacticArea != nil)
    }

    func test_givenDidacticAreasBO_whenICallGetDidacticArea_thenDidacticAreaBOMatchWithInteractorReturned() {

        // given

        // when
        sut.getDidacticArea()

        let didacticAreasBO = DidacticAreasBO(didacticAreasEntity: dummyDidacticAreaDataManager.didacticAreasEntity!)

        // then
        XCTAssert(sut.didacticArea?.version == didacticAreasBO.version)
        XCTAssert(sut.didacticArea?.items![0].helpId == didacticAreasBO.items![0].helpId)
        XCTAssert(sut.didacticArea?.items![0].type == didacticAreasBO.items![0].type)
        XCTAssert(sut.didacticArea?.items![0].imageBackground == didacticAreasBO.items![0].imageBackground)
        XCTAssert(sut.didacticArea?.items![0].platform == didacticAreasBO.items![0].platform)
        XCTAssert(sut.didacticArea?.items![0].startDate == didacticAreasBO.items![0].startDate)
        XCTAssert(sut.didacticArea?.items![0].endDate == didacticAreasBO.items![0].endDate)

        XCTAssert(sut.didacticArea?.items![0].titleColor?.alpha == didacticAreasBO.items![0].titleColor?.alpha)
        XCTAssert(sut.didacticArea?.items![0].titleColor?.hexString == didacticAreasBO.items![0].titleColor?.hexString)

        XCTAssert(sut.didacticArea?.items![0].descriptionColor?.alpha == didacticAreasBO.items![0].descriptionColor?.alpha)
        XCTAssert(sut.didacticArea?.items![0].descriptionColor?.hexString == didacticAreasBO.items![0].descriptionColor?.hexString)

        XCTAssert(sut.didacticArea?.items![0].descriptionOnTapColor?.alpha == didacticAreasBO.items![0].descriptionOnTapColor?.alpha)
        XCTAssert(sut.didacticArea?.items![0].descriptionOnTapColor?.hexString == didacticAreasBO.items![0].descriptionOnTapColor?.hexString)

        XCTAssert(sut.didacticArea?.items![0].texts![0].language == didacticAreasBO.items![0].texts![0].language)
        XCTAssert(sut.didacticArea?.items![0].texts![0].title == didacticAreasBO.items![0].texts![0].title)
        XCTAssert(sut.didacticArea?.items![0].texts![0].description == didacticAreasBO.items![0].texts![0].description)

        //---------------

        XCTAssert(sut.didacticArea?.items![1].imageThumb == didacticAreasBO.items![1].imageThumb)

        XCTAssert(sut.didacticArea?.categories?.defaultCategory![0].titleColor?.alpha == didacticAreasBO.categories?.defaultCategory![0].titleColor?.alpha)
        XCTAssert(sut.didacticArea?.categories?.defaultCategory![0].titleColor?.hexString == didacticAreasBO.categories?.defaultCategory![0].titleColor?.hexString)

        XCTAssert(sut.didacticArea?.categories?.defaultCategory![0].descriptionColor?.alpha == didacticAreasBO.categories?.defaultCategory![0].descriptionColor?.alpha)
        XCTAssert(sut.didacticArea?.categories?.defaultCategory![0].descriptionColor?.hexString == didacticAreasBO.categories?.defaultCategory![0].descriptionColor?.hexString)

        XCTAssert(sut.didacticArea?.categories?.defaultCategory![0].descriptionOnTapColor?.alpha == didacticAreasBO.categories?.defaultCategory![0].descriptionOnTapColor?.alpha)
        XCTAssert(sut.didacticArea?.categories?.defaultCategory![0].descriptionOnTapColor?.hexString == didacticAreasBO.categories?.defaultCategory![0].descriptionOnTapColor?.hexString)

        XCTAssert(sut.didacticArea?.categories?.defaultCategory![0].texts![0].language == didacticAreasBO.categories?.defaultCategory![0].texts![0].language)
        XCTAssert(sut.didacticArea?.categories?.defaultCategory![0].texts![0].title == didacticAreasBO.categories?.defaultCategory![0].texts![0].title)
        XCTAssert(sut.didacticArea?.categories?.defaultCategory![0].texts![0].description == didacticAreasBO.categories?.defaultCategory![0].texts![0].description)

        XCTAssert(sut.didacticArea?.categories?.defaultCategory![0].imageBackground == didacticAreasBO.categories?.defaultCategory![0].imageBackground)
        XCTAssert(sut.didacticArea?.categories?.defaultCategory![0].categoryId == didacticAreasBO.categories?.defaultCategory![0].categoryId)
    }

    func test_givenAny_whenICallGetDidacticArea_thenICallGetValueWithPreferencesKey() {

        // given

        // when
        sut.getDidacticArea()

        // then
        XCTAssert(dummyPreferencesManager.isCalledGetValueWithPreferencesKey == true)
    }

    func test_givenAny_whenICallGetDidacticArea_thenICallSaveValueWithPreferencesKey() {

        // given

        // when
        sut.getDidacticArea()

        // then
        XCTAssert(dummyPreferencesManager.isCalledSaveValueWithPreferencesKey == true)
    }

    func test_givenEqualVersion_whenICallGetDidacticArea_thenICallGetVersionAndMatchVersion() {

        // given
        dummyPreferencesManager.forceNewVersion = false

        // when
        sut.getDidacticArea()

        // then
        XCTAssert(dummyPreferencesManager.version == sut.didacticArea?.version)
    }

    func test_givenNewVersion_whenICallGetDidacticArea_thenICallGetVersionAndNotMatchVersion() {

        // given
        sut.didacticArea?.version = "0.1"
        dummyPreferencesManager.forceNewVersion = true

        // when
        sut.getDidacticArea()

        // then
        XCTAssert(dummyPreferencesManager.version != sut.didacticArea?.version)
    }

    func test_givenNewVersion_whenICallGetDidacticArea_thenICallSaveValueWithPreferencesKeyWithResetIndexByNewVersion() {

        // given
        sut.didacticArea?.version = "0.1"
        dummyPreferencesManager.forceNewVersion = true

        // when
        sut.getDidacticArea()

        // then
        XCTAssert(dummyPreferencesManager.isCalledSaveValueWithPreferencesKey == true)
        XCTAssert(dummyPreferencesManager.valueIndex == -1)
    }

    func test_givenNewVersionWithDidacticAreasBOAndIndexsOfCategories_whenICallGetDidacticArea_thenIndexsOfCategoriesMatch() {

        // given
        sut.didacticArea?.version = "0.1"
        dummyPreferencesManager.forceNewVersion = true

        // when
        sut.getDidacticArea()

        let itemsIndexsOfCategories = ConfigGenerator.getIndexsOfCategories(fromDidacticAreasBO: sut.didacticArea!)
        let itemsCategories = dummyPreferencesManager.itemsCategories

        // then
        XCTAssert(itemsIndexsOfCategories[0][Constants.DidacticArea.day_of_category] as! String == itemsCategories![0][Constants.DidacticArea.day_of_category] as! String)
        XCTAssert(itemsIndexsOfCategories[0][Constants.DidacticArea.time_of_category] as! String == itemsCategories![0][Constants.DidacticArea.time_of_category] as! String)
        XCTAssert(itemsIndexsOfCategories[0][Constants.DidacticArea.index_of_category] as! Int == itemsCategories![0][Constants.DidacticArea.index_of_category] as! Int)
        XCTAssert(itemsIndexsOfCategories[0][Constants.DidacticArea.count_of_category] as! Int == itemsCategories![0][Constants.DidacticArea.count_of_category] as! Int)
    }

    // MARK: Didactic Area (getDidacticAreaToShow)

    func test_givenEmpty_whenICallGetDidacticAreaToShow_thenDidacticAreaBOEmptyAnddidacticCategoryBOEmpty() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO()
        didacticAreaBO.itemsIdSpecial = nil
        didacticAreaBO.itemsDidacticArea = nil

        sut.didacticArea = didacticAreaBO

        // when
        let result = sut.getDidacticAreaToShow()

        // then
        XCTAssert(result.didacticAreaBO == nil)
        XCTAssert(result.didacticCategoryBO == nil)
    }

    func test_givenItemsIdSpecialWithStartDateAndEndDateBetweenCurrentDate_whenICallGetDidacticAreaToShow_thenDidacticAreaBOFilledAndMatch() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO()
        didacticAreaBO.itemsIdSpecial = ConfigGenerator.getItemsIdSpecial()
        let item = didacticAreaBO.itemsIdSpecial![0]

        item.startDate = Calendar.current.date(byAdding: .day, value: -1, to: Date())
        item.endDate = Calendar.current.date(byAdding: .day, value: 1, to: Date())

        sut.didacticArea = didacticAreaBO

        // when
        let result = sut.getDidacticAreaToShow()

        // then
        XCTAssert(result.didacticAreaBO != nil)
        XCTAssert(result.didacticAreaBO === item)
    }

    func test_givenItemsIdSpecialWithStartDateAndEndDateOutsideCurrentDate_whenICallGetDidacticAreaToShow_thenDidacticAreaBOEmpty() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO()
        didacticAreaBO.itemsDidacticArea = nil
        didacticAreaBO.itemsIdSpecial = ConfigGenerator.getItemsIdSpecial()
        let item = didacticAreaBO.itemsIdSpecial![0]

        item.startDate = Calendar.current.date(byAdding: .day, value: -2, to: Date())
        item.endDate = Calendar.current.date(byAdding: .day, value: -1, to: Date())

        sut.didacticArea = didacticAreaBO

        // when
        let result = sut.getDidacticAreaToShow()

        // then
        XCTAssert(result.didacticAreaBO == nil)
    }

    func test_givenItemsDidacticAreaWithDifferentTypeOffer_whenICallGetDidacticAreaToShow_thenDidacticAreaBOFilled() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO()
        didacticAreaBO.itemsIdSpecial = nil
        didacticAreaBO.itemsDidacticArea![0].type = .didactic

        sut.didacticArea = didacticAreaBO

        // when
        let result = sut.getDidacticAreaToShow()

        // then
        XCTAssert(result.didacticAreaBO != nil)
    }

    func test_givenItemsDidacticArea_whenICallGetDidacticAreaToShow_thenICallGetValueWithPreferencesKey() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO()
        didacticAreaBO.itemsIdSpecial = nil

        sut.didacticArea = didacticAreaBO

        // when
        _ = sut.getDidacticAreaToShow()

        // then
        XCTAssert(dummyPreferencesManager.isCalledGetValueWithPreferencesKey == true)
    }

    func test_givenItemsDidacticAreaWithIndexSavedMoreThanItemsCount_whenICallGetDidacticAreaToShow_thenICallResetDidacticArea() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO()
        didacticAreaBO.itemsIdSpecial = nil

        sut.didacticArea = didacticAreaBO

        dummyPreferencesManager.forceResetDidacticArea = true

        // when
        _ = sut.getDidacticAreaToShow()

        // then
        XCTAssert(dummyPreferencesManager.isCalledSaveValueWithPreferencesKey == true)
        XCTAssert(dummyPreferencesManager.valueIndex == -1)
    }

    // MARK: Didactic Area (getIndexDidacticAreaToShow)

    func test_givenItemsDidacticAreaWithIndexSavedMoreThanItemsCount_whenICallGetIndexDidacticAreaToShow_thenICallResetDidacticAreaAndIndexToShowEqualToZero() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO()
        didacticAreaBO.itemsIdSpecial = nil

        sut.didacticArea = didacticAreaBO

        dummyPreferencesManager.forceResetDidacticArea = true

        // when
        let result = sut.getIndexDidacticAreaToShow(onItems: (sut.didacticArea?.itemsDidacticArea)!, andKey: PreferencesManagerKeys.kIndexDidacticArea)

        // then
        XCTAssert(dummyPreferencesManager.isCalledSaveValueWithPreferencesKey == true)
        XCTAssert(dummyPreferencesManager.valueIndex == -1)
        XCTAssert(result == 0)
    }

    func test_givenItemsDidacticArea_whenICallGetIndexDidacticAreaToShow_thenIndexToShowGreaterOrEqualToZero() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO()
        didacticAreaBO.itemsIdSpecial = nil

        sut.didacticArea = didacticAreaBO

        // when
        let result = sut.getIndexDidacticAreaToShow(onItems: (sut.didacticArea?.itemsDidacticArea)!, andKey: PreferencesManagerKeys.kIndexDidacticArea)

        // then
        XCTAssert(result >= 0)
    }

    func test_givenItemsDidacticArea_whenICallGetIndexDidacticAreaToShow_thenDidacticAreaBOByIndexToShowIsFilled() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO()
        didacticAreaBO.itemsIdSpecial = nil

        sut.didacticArea = didacticAreaBO

        // when
        let result = sut.getIndexDidacticAreaToShow(onItems: (sut.didacticArea?.itemsDidacticArea)!, andKey: PreferencesManagerKeys.kIndexDidacticArea)

        // then
        XCTAssert(sut.didacticArea?.itemsDidacticArea![result] != nil)
    }

    // MARK: Didactic Area (getDidacticAreaToShow & getIndexDidacticAreaToShow)

    func test_givenItemsDidacticArea_whenICallGetDidacticAreaToShowAndIndexToShow_thenSaveValueDidacticAreaWithIndexToShow() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO()
        didacticAreaBO.itemsIdSpecial = nil

        sut.didacticArea = didacticAreaBO

        // when
        _ = sut.getDidacticAreaToShow()
        let indexToShow = sut.getIndexDidacticAreaToShow(onItems: (sut.didacticArea?.itemsDidacticArea)!, andKey: PreferencesManagerKeys.kIndexDidacticArea)

        // then
        XCTAssert(dummyPreferencesManager.isCalledSaveValueWithPreferencesKey == true)
        XCTAssert(dummyPreferencesManager.indexToShowSaved as! Int == indexToShow)
    }

    func test_givenItemsDidacticAreaWithDifferentTypeOffer_whenICallGetDidacticAreaToShow_thenDidacticAreaBOMatchWithItemsDidacticAreaByIndexToShow() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO()
        didacticAreaBO.itemsIdSpecial = nil
        didacticAreaBO.itemsDidacticArea![0].type = .didactic

        sut.didacticArea = didacticAreaBO

        // when
        let result = sut.getDidacticAreaToShow()
        let indexToShow = sut.getIndexDidacticAreaToShow(onItems: (sut.didacticArea?.itemsDidacticArea)!, andKey: PreferencesManagerKeys.kIndexDidacticArea)

        // then
        XCTAssert(result.didacticAreaBO === sut.didacticArea?.itemsDidacticArea![indexToShow])
    }

    // MARK: Didactic Area (getDidacticAreaToShow Type Offer)

    func test_givenItemsDidacticAreaWithTypeOfferAndCategoriesWithDefaultCategory_whenICallGetDidacticAreaToShow_thenDidacticAreaBOAndDidacticCategoryBOFilled() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO()
        didacticAreaBO.itemsIdSpecial = nil
        didacticAreaBO.itemsDidacticArea![0].type = .offer

        sut.didacticArea = didacticAreaBO

        // when
        let result = sut.getDidacticAreaToShow()

        // then
        XCTAssert(result.didacticAreaBO != nil)
        XCTAssert(result.didacticCategoryBO != nil)
    }

    func test_givenItemsDidacticAreaWithTypeOfferAndWithoutCategories_whenICallGetDidacticAreaToShow_thenDidacticAreaBOFilledAndDidacticCategoryBOEmpty() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO()
        didacticAreaBO.itemsIdSpecial = nil
        didacticAreaBO.itemsDidacticArea![0].type = .offer
        didacticAreaBO.categories = nil

        sut.didacticArea = didacticAreaBO

        // when
        let result = sut.getDidacticAreaToShow()

        // then
        XCTAssert(result.didacticAreaBO != nil)
        XCTAssert(result.didacticCategoryBO == nil)
    }

    func test_givenItemsDidacticAreaWithTypeOfferAndWithoutDefaultCategory_whenICallGetDidacticAreaToShow_thenDidacticAreaBOFilledAndDidacticCategoryBOEmpty() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO()
        didacticAreaBO.itemsIdSpecial = nil
        didacticAreaBO.itemsDidacticArea![0].type = .offer
        didacticAreaBO.categories?.defaultCategory = nil

        sut.didacticArea = didacticAreaBO

        // when
        let result = sut.getDidacticAreaToShow()

        // then
        XCTAssert(result.didacticAreaBO != nil)
        XCTAssert(result.didacticCategoryBO == nil)
    }

    func test_givenItemsDidacticAreaWithTypeOfferAndAllDidacticCategoriesBOWithTimeBO_whenICallGetDidacticAreaToShow_thenDidacticAreaBOFilledAndDidacticCategoryBOFilled() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO()
        didacticAreaBO.itemsIdSpecial = nil
        didacticAreaBO.itemsDidacticArea![0].type = .offer
        didacticAreaBO.categories = ConfigGenerator.getDidacticCategoriesBO()

        sut.didacticArea = didacticAreaBO

        // when
        let result = sut.getDidacticAreaToShow()

        // then
        XCTAssert(result.didacticAreaBO != nil)
        XCTAssert(result.didacticCategoryBO != nil)
    }

    func test_givenItemsDidacticAreaWithTypeOfferAndAllDidacticCategoriesBOWithoutTimeBOAndWithoutDefaultCategories_whenICallGetDidacticAreaToShow_thenDidacticAreaBOFilledAndDidacticCategoryBOEmpty() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO()
        didacticAreaBO.itemsIdSpecial = nil
        didacticAreaBO.itemsDidacticArea![0].type = .offer
        didacticAreaBO.categories = ConfigGenerator.getDidacticCategoriesBO()
        didacticAreaBO.categories?.monday?.morning = nil
        didacticAreaBO.categories?.monday?.afternoon = nil
        didacticAreaBO.categories?.monday?.night = nil
        didacticAreaBO.categories?.tuesday?.morning = nil
        didacticAreaBO.categories?.tuesday?.afternoon = nil
        didacticAreaBO.categories?.tuesday?.night = nil
        didacticAreaBO.categories?.wednesday?.morning = nil
        didacticAreaBO.categories?.wednesday?.afternoon = nil
        didacticAreaBO.categories?.wednesday?.night = nil
        didacticAreaBO.categories?.thursday?.morning = nil
        didacticAreaBO.categories?.thursday?.afternoon = nil
        didacticAreaBO.categories?.thursday?.night = nil
        didacticAreaBO.categories?.friday?.morning = nil
        didacticAreaBO.categories?.friday?.afternoon = nil
        didacticAreaBO.categories?.friday?.night = nil
        didacticAreaBO.categories?.saturday?.morning = nil
        didacticAreaBO.categories?.saturday?.afternoon = nil
        didacticAreaBO.categories?.saturday?.night = nil
        didacticAreaBO.categories?.sunday?.morning = nil
        didacticAreaBO.categories?.sunday?.afternoon = nil
        didacticAreaBO.categories?.sunday?.night = nil
        didacticAreaBO.categories?.defaultCategory = nil

        sut.didacticArea = didacticAreaBO

        // when
        let result = sut.getDidacticAreaToShow()

        // then
        XCTAssert(result.didacticAreaBO != nil)
        XCTAssert(result.didacticCategoryBO == nil)
    }

    func test_givenItemsDidacticAreaWithTypeOfferAndCategoriesWithDefaultCategory_whenICallGetDidacticAreaToShow_thenICallGetValueWithPreferencesKey() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO()
        didacticAreaBO.itemsIdSpecial = nil
        didacticAreaBO.itemsDidacticArea![0].type = .offer

        sut.didacticArea = didacticAreaBO

        // when
        _ = sut.getDidacticAreaToShow()

        // then
        XCTAssert(dummyPreferencesManager.isCalledGetValueWithPreferencesKey == true)
    }

    func test_givenItemsDidacticAreaWithTypeOfferAndCategoriesWithDefaultCategory_whenICallGetDidacticAreaToShow_thenICallResetDidacticArea() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO()
        didacticAreaBO.itemsIdSpecial = nil
        didacticAreaBO.itemsDidacticArea![0].type = .offer

        sut.didacticArea = didacticAreaBO

        dummyPreferencesManager.forceResetDidacticArea = true

        // when
        _ = sut.getDidacticAreaToShow()

        // then
        XCTAssert(dummyPreferencesManager.isCalledSaveValueWithPreferencesKey == true)
        XCTAssert(dummyPreferencesManager.valueIndex == -1)
    }

    func test_givenItemsDidacticAreaWithTypeOfferAndCategoriesWithDefaultCategory_whenICallGetDidacticAreaToShow_thenSaveValueDidacticAreaWithIndexToShow() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO()
        didacticAreaBO.itemsIdSpecial = nil
        didacticAreaBO.itemsDidacticArea![0].type = .offer

        sut.didacticArea = didacticAreaBO

        let indexToShowExpected = sut.getIndexDidacticAreaToShow(onItems: (sut.didacticArea?.categories?.defaultCategory)!, andKey: PreferencesManagerKeys.kIndexDefaultCategoryDidacticArea)

        // when
        _ = sut.getDidacticAreaToShow()

        // then
        XCTAssert(dummyPreferencesManager.isCalledSaveValueWithPreferencesKey == true)
        XCTAssert(dummyPreferencesManager.indexDefaultCategoryToShowSaved as! Int == indexToShowExpected)
    }

    func test_givenItemsDidacticAreaWithTypeOfferAndCategoriesWithDefaultCategory_whenICallGetDidacticAreaToShow_thenDidacticCategoryBOMatchWithItemsDidacticCategoryBOByIndexToShow() {

        // given
        let didacticAreaBO = ConfigGenerator.getDidacticAreasBO()
        didacticAreaBO.itemsIdSpecial = nil
        didacticAreaBO.itemsDidacticArea![0].type = .offer

        sut.didacticArea = didacticAreaBO

        // when
        let indexToShow = sut.getIndexDidacticAreaToShow(onItems: (sut.didacticArea?.categories?.defaultCategory)!, andKey: PreferencesManagerKeys.kIndexDefaultCategoryDidacticArea)

        // then
        XCTAssert(sut.didacticArea?.categories?.defaultCategory![indexToShow] === sut.didacticArea?.categories?.defaultCategory![indexToShow])
    }

    class DummyPublicManager: ConfigPublicManager {

        var sentCardKey = ""
        var isCalledGetConfigBOBySection = false
        var isCalledGetConfigFile = false
        var isCalledWriteToPlist = false
        var isCalledReadFromFile = false
        var isCalledHelpBO = false
        var isCalledDataShouldBeSaved = false
        var isCalledRemovedExistingFile = false
        var isCalledGetDidacticArea = false

        var fileName = ""
        var version = ""
        var dataJSONString = ""

        override func getConfigBOBySection(forKey key: String) -> [ConfigFileBO] {

            isCalledGetConfigBOBySection = true
            sentCardKey = key

            return super.getConfigBOBySection(forKey: key)
        }
        override func getConfigFile() {
            isCalledGetConfigFile = true
        }
        override func writeToPlist(forItems items: String, withFilename filename: String) {
            dataJSONString = items
            fileName = filename
            isCalledWriteToPlist = true
            super.writeToPlist(forItems: items, withFilename: filename)
        }
        override func readFromSavedFile(forFilename filename: String) -> String {
            fileName = filename
            isCalledReadFromFile = true
            return super.readFromSavedFile(forFilename: filename)
        }

        override func getConfigBO(forSection section: String, withKey key: PreferencesManagerKeys) -> ConfigFileBO? {
            isCalledHelpBO = true
            return super.getConfigBO(forSection: section, withKey: key)

        }

        override func removeExistingFile(withFileName fileName: String) {
            self.fileName = fileName
            isCalledRemovedExistingFile = true
            super.removeExistingFile(withFileName: fileName)
        }

        override func getDidacticArea() {
            isCalledGetDidacticArea = true
        }
    }

    class DummyConfigPublicDataManager: ConfigPublicDataManager {

        var isCalledProvideHelpConfigFile = false

        var forceError = false
        var errorBO: ErrorBO?
        var configFilesEntity: ConfigFilesEntity?

        override func provideHelpConfigFile() -> Observable<ModelEntity> {

            isCalledProvideHelpConfigFile = true

            if !forceError {

                if configFilesEntity == nil {
                    configFilesEntity = ConfigGenerator.getHelpEntity()
                }

                return Observable <ModelEntity>.just(configFilesEntity!)

            } else {

                if errorBO == nil {
                    errorBO = ErrorBO(error: ErrorEntity(message: "Error"))
                    errorBO!.isErrorShown = false
                }

                return Observable.error(ServiceError.GenericErrorBO(error: errorBO!))
            }
        }
    }

    class DummyDidacticAreaDataManager: DidacticAreaDataManager {

        var isCalledServiceDidacticArea = false

        var forceError = false
        var errorBO: ErrorBO?
        var didacticAreasEntity: DidacticAreasEntity?

        override func serviceDidacticArea() -> Observable<ModelEntity> {

            isCalledServiceDidacticArea = true

            if !forceError {

                if didacticAreasEntity == nil {
                     didacticAreasEntity = ConfigGenerator.getDidacticAreasEntity()
                }

                return Observable <ModelEntity>.just(didacticAreasEntity!)

            } else {

                if errorBO == nil {
                    errorBO = ErrorBO(error: ErrorEntity(message: "Error"))
                    errorBO!.isErrorShown = false
                }

                return Observable.error(ServiceError.GenericErrorBO(error: errorBO!))
            }

        }

    }

    class DummyPreferencesManager: PreferencesManager {

        var isCalledGetValueWithPreferencesKey = false
        var version: String?
        var forceNewVersion = false
        var isCalledSaveValueWithPreferencesKey = false
        var valueIndex: Int?
        var itemsCategories: [[String: Any]]?
        var forceResetDidacticArea = false
        var indexToSaved: AnyObject?
        var forceIndexToShowCorrect = false
        var indexToShowSaved: AnyObject?
        var indexDefaultCategoryToShowSaved: AnyObject?
        var isCalledDeleteValueWithPreferencesKey = false

        override func getValueWithPreferencesKey(forKey key: PreferencesManagerKeys) -> AnyObject? {

            isCalledGetValueWithPreferencesKey = true

            if !forceNewVersion {
                version = "0.1"
            } else {
                version = "2.0"
            }

            if key == PreferencesManagerKeys.kSavedDidacticAreaVersion {
                return version as AnyObject
            }

            if key == PreferencesManagerKeys.kIndexDidacticArea {

                if forceResetDidacticArea {
                    return 1000 as AnyObject
                }
            }

            if key == PreferencesManagerKeys.kIndexDefaultCategoryDidacticArea {

                return 1 as AnyObject

            }

            return 0 as AnyObject
        }

        override func saveValueWithPreferencesKey(forValue value: AnyObject, withKey key: PreferencesManagerKeys) -> Bool {

            isCalledSaveValueWithPreferencesKey = true

            if key == PreferencesManagerKeys.kIndexDidacticArea {

                indexToSaved = value

                if value as! Int >= 0 {
                    indexToShowSaved = 1 as AnyObject
                }

                if forceNewVersion || forceResetDidacticArea {
                    valueIndex = -1
                }
            }

            if key == PreferencesManagerKeys.kIndexDefaultCategoryDidacticArea {

                if value as! Int >= 0 {
                    indexDefaultCategoryToShowSaved = 0 as AnyObject
                }
            }

            if key == PreferencesManagerKeys.kSaveIndexsOfCategories {

                itemsCategories = value as? [[String: Any]]
            }

            if key == PreferencesManagerKeys.kIndexToShowCards {

                indexToShowSaved = 0 as AnyObject
            }

            return true
        }

        override func deleteValueWithPreferencesKey(forKey key: PreferencesManagerKeys) -> Bool {
            
            isCalledDeleteValueWithPreferencesKey = true
            return true
        }
    }
}
