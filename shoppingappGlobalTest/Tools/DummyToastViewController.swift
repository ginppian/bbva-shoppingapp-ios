//
//  DummyToastManager.swift
//  shoppingapp
//
//  Created by Arturo Marzo de la Fuente on 20/9/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import Foundation

#if TESTMX
@testable import shoppingappMX
#elseif TESTPE
@testable import shoppingappPE
#endif

class DummyToastViewController: ToastViewController {
    
    var toastVisible = false
    
    class func insertDummyToast() {
        
        ToastManager.shared().toastViewController = DummyToastViewController()
    }
    
    override func isThereAToastVisible() -> Bool {
        
        return toastVisible
    }
}
