//
//  SideMenuWalletCellTest.swift
//  shoppingapp
//
//  Created by Marcos on 12/6/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest
import CellsNativeComponents

#if TESTMX
@testable import shoppingappMX
#elseif TESTPE
@testable import shoppingappPE
#endif

class ConfigurationWalletCellTest: XCTestCase {

    var sut: ConfigurationWalletCellPresenter<ConfigurationWalletCellViewDummy>!
    var dummyPreferencesManager: DummyPreferencesManager!
    var dummyBusManager: DummyBusManager!
    var dummySessionManager: DummySessionManager!

    override func setUp() {
        super.setUp()
        dummyBusManager = DummyBusManager()
        sut = ConfigurationWalletCellPresenter<ConfigurationWalletCellViewDummy>(busManager: dummyBusManager)
        dummyPreferencesManager = DummyPreferencesManager()
        PreferencesManager.instance = dummyPreferencesManager
        
        sut.notificationManager = DummyNotificationManager()
        
        DummySessionManager.setupMock()
        dummySessionManager = (SessionDataManager.sessionDataInstance() as! DummySessionManager)
    }
    
    // MARK: - configurationChanged - kConfigBalancesAvailable
    
    func test_givenConfigurationPreferencesDTOWithKeyConfigBalancesAvailableAndValueTrue_whenConfigurationChangedCalled_thenICalledSaveInPreferences() {
        
        //given
        let keyConfig = PreferencesManagerKeys.kConfigBalancesAvailable.rawValue
        let configurationPreferencesDTO = ConfigurationPreferencesDTO(enabled: true, keyConfig: keyConfig)
        
        //when
        sut.configurationChanged(configurationPreferencesDTO)
        
        //then
        XCTAssert(dummyPreferencesManager.isCalledSaveValueWithPreferencesKey == true)
    }
    
    func test_givenConfigurationPreferencesDTOWithKeyConfigBalancesAvailableAndValueFalse_whenConfigurationChangedCalled_thenICalledSaveInPreferences() {
        
        //given
        let keyConfig = PreferencesManagerKeys.kConfigBalancesAvailable.rawValue
        let configurationPreferencesDTO = ConfigurationPreferencesDTO(enabled: false, keyConfig: keyConfig)
        
        //when
        sut.configurationChanged(configurationPreferencesDTO)
        
        //then
        XCTAssert(dummyPreferencesManager.isCalledSaveValueWithPreferencesKey == true)
    }
    
    func test_givenConfigurationPreferencesDTOWithKeyConfigBalancesAvailableAndValueTrue_whenConfigurationChangedCalled_thenBalancesAvailableWillBeTrue() {
        
        //given
        let keyConfig = PreferencesManagerKeys.kConfigBalancesAvailable.rawValue
        let configurationPreferencesDTO = ConfigurationPreferencesDTO(enabled: true, keyConfig: keyConfig)
        
        //when
        sut.configurationChanged(configurationPreferencesDTO)
        
        //then
        XCTAssert(dummyPreferencesManager.isBalancesAvailableSave == true)
    }
    
    func test_givenConfigurationPreferencesDTOWithKeyConfigBalancesAvailableAndValueFalse_whenConfigurationChangedCalled_thenBalancesAvailableWillBeFalse() {
        
        //given
        let keyConfig = PreferencesManagerKeys.kConfigBalancesAvailable.rawValue
        let configurationPreferencesDTO = ConfigurationPreferencesDTO(enabled: false, keyConfig: keyConfig)
        
        //when
        sut.configurationChanged(configurationPreferencesDTO)
        
        //then
        XCTAssert(dummyPreferencesManager.isBalancesAvailableSave == false)
    }
    
    func test_givenConfigurationPreferencesDTOWithKeyConfigBalancesAvailableAndValueFalse_whenConfigurationChangedCalledAndAnErrorOccuredWhileSavingInPreferences_thenIShowDefaultModalWithError() {
        
        //given
        let keyConfig = PreferencesManagerKeys.kConfigBalancesAvailable.rawValue
        let configurationPreferencesDTO = ConfigurationPreferencesDTO(enabled: false, keyConfig: keyConfig)
        
        let dummyView = ConfigurationWalletCellViewDummy()
        sut.view = dummyView
        
        //when
        dummyPreferencesManager.forceError = true
        sut.configurationChanged(configurationPreferencesDTO)
        
        //then
        XCTAssert(sut.view?.isCalledShowError == true)
        XCTAssert(sut.view?.errorModelText == Localizables.common.key_common_generic_error_text)
        XCTAssert(dummyView.isCalledShowError == true)
    }
    
    // MARK: - configurationChanged - kConfigNotificationsAvailable
    
    func test_givenConfigurationPreferencesDTOWithKeyConfigNotificationsAvailableAndValueTrue_whenConfigurationChangedCalled_thenICalledSaveInPreferences() {
        
        //given
        let keyConfig = PreferencesManagerKeys.kConfigNotificationsAvailable.rawValue
        let configurationPreferencesDTO = ConfigurationPreferencesDTO(enabled: true, keyConfig: keyConfig)
        
        //when
        sut.configurationChanged(configurationPreferencesDTO)
        
        //then
        XCTAssert(dummyPreferencesManager.isCalledSaveValueWithPreferencesKey == true)
    }
    
    func test_givenConfigurationPreferencesDTOWithKeyConfigNotificationsAvailableAndValueFalse_whenConfigurationChangedCalled_thenICalledSaveInPreferences() {
        
        //given
        let keyConfig = PreferencesManagerKeys.kConfigNotificationsAvailable.rawValue
        let configurationPreferencesDTO = ConfigurationPreferencesDTO(enabled: false, keyConfig: keyConfig)
        
        //when
        sut.configurationChanged(configurationPreferencesDTO)
        
        //then
        XCTAssert(dummyPreferencesManager.isCalledSaveValueWithPreferencesKey == true)
    }
    
    func test_givenConfigurationPreferencesDTOWithKeyConfigNotificationsAvailableAndValueTrue_whenConfigurationChangedCalled_thenNotificationsAvailableWillBeTrue() {
        
        //given
        let keyConfig = PreferencesManagerKeys.kConfigNotificationsAvailable.rawValue
        let configurationPreferencesDTO = ConfigurationPreferencesDTO(enabled: true, keyConfig: keyConfig)
        
        //when
        sut.configurationChanged(configurationPreferencesDTO)
        
        //then
        XCTAssert(dummyPreferencesManager.isNotificationsAvailableSave == true)
    }
    
    func test_givenConfigurationPreferencesDTOWithKeyConfigNotificationsAvailableAndValueFalse_whenConfigurationChangedCalled_thenNotificationsAvailableWillBeFalse() {
        
        //given
        let keyConfig = PreferencesManagerKeys.kConfigNotificationsAvailable.rawValue
        let configurationPreferencesDTO = ConfigurationPreferencesDTO(enabled: false, keyConfig: keyConfig)
        
        //when
        sut.configurationChanged(configurationPreferencesDTO)
        
        //then
        XCTAssert(dummyPreferencesManager.isNotificationsAvailableSave == false)
    }
    
    func test_givenConfigurationPreferencesDTOWithKeyConfigVibrationEnabledAndValueTrue_whenConfigurationChangedCalled_thenVibrationEnabledWillBeTrue() {
        
        //given
        let keyConfig = PreferencesManagerKeys.kConfigVibrationEnabled.rawValue
        let configurationPreferencesDTO = ConfigurationPreferencesDTO(enabled: true, keyConfig: keyConfig)
        
        //when
        sut.configurationChanged(configurationPreferencesDTO)
        
        //then
        XCTAssert(dummyPreferencesManager.isVibrationEnabledSave == true)
    }
    
    func test_givenConfigurationPreferencesDTOWithKeyConfigVibrationEnabledAndValueFalse_whenConfigurationChangedCalled_thenVibrationEnabledWillBeFalse() {
        
        //given
        let keyConfig = PreferencesManagerKeys.kConfigVibrationEnabled.rawValue
        let configurationPreferencesDTO = ConfigurationPreferencesDTO(enabled: false, keyConfig: keyConfig)
        
        //when
        sut.configurationChanged(configurationPreferencesDTO)
        
        //then
        XCTAssert(dummyPreferencesManager.isVibrationEnabledSave == false)
    }
    
    func test_givenConfigurationPreferencesDTOWithKeyConfigTicketPromotionsEnabledAndValueTrue_whenConfigurationChangedCalled_thenTicketPromotionsEnabledWillBeTrue() {
        
        //given
        let keyConfig = PreferencesManagerKeys.kConfigTicketPromotionsEnabled.rawValue
        let configurationPreferencesDTO = ConfigurationPreferencesDTO(enabled: true, keyConfig: keyConfig)
        
        //when
        sut.configurationChanged(configurationPreferencesDTO)
        
        //then
        XCTAssert(dummyPreferencesManager.isTicketPromotionsEnabledSave == true)
    }
    
    func test_givenConfigurationPreferencesDTOWithKeyConfigTicketPromotionsEnabledAndValueFalse_whenConfigurationChangedCalled_thenTicketPromotionsEnabledWillBeFalse() {
        
        //given
        let keyConfig = PreferencesManagerKeys.kConfigTicketPromotionsEnabled.rawValue
        let configurationPreferencesDTO = ConfigurationPreferencesDTO(enabled: false, keyConfig: keyConfig)
        
        //when
        sut.configurationChanged(configurationPreferencesDTO)
        
        //then
        XCTAssert(dummyPreferencesManager.isTicketPromotionsEnabledSave == false)
    }
    
    func test_givenConfigurationPreferencesDTOWithKeyConfigNotificationsAvailableAndValueFalse_whenConfigurationChangedCalledAndAnErrorOccuredWhileSavingInPreferences_thenIShowDefaultModalWithError() {
        
        //given
        let keyConfig = PreferencesManagerKeys.kConfigNotificationsAvailable.rawValue
        let configurationPreferencesDTO = ConfigurationPreferencesDTO(enabled: false, keyConfig: keyConfig)
        
        let dummyView = ConfigurationWalletCellViewDummy()
        sut.view = dummyView
        
        //when
        dummyPreferencesManager.forceError = true
        sut.configurationChanged(configurationPreferencesDTO)
        
        //then
        XCTAssert(sut.view?.isCalledShowError == true)
        XCTAssert(sut.view?.errorModelText == Localizables.common.key_common_generic_error_text)
        XCTAssert(dummyView.isCalledShowError == true)
    }
    
    // MARK: - configurationChanged - checkError
    
    func test_givenConfigurationPreferencesDTOWithKeyConfigBalancesAvailable_whenConfigurationChangedAndErrorOccuredWhileSavingInPreferencesAndICalledCheckError_thenIPublishParamsSettingsTransportInChannel() {

        //given
        let keyConfig = PreferencesManagerKeys.kConfigBalancesAvailable.rawValue
        let configurationPreferencesDTO = ConfigurationPreferencesDTO(enabled: false, keyConfig: keyConfig)
        
        let dummyView = ConfigurationWalletCellViewDummy()
        sut.view = dummyView

        //when
        dummyPreferencesManager.forceError = true
        sut.configurationChanged(configurationPreferencesDTO)
        sut.checkError()

        //then
        XCTAssert(dummyBusManager.isCalledPublishParamConfigurationChanged == true)
        XCTAssert(sut.settingsTransport.isBalancesAvailable == !configurationPreferencesDTO.enabled)
    }
    
    func test_givenConfigurationPreferencesDTOWithKeyConfigNotificationsAvailable_whenConfigurationChangedAndErrorOccuredWhileSavingInPreferencesAndICalledCheckError_thenIPublishParamsSettingsTransportInChannel() {
        
        //given
        let keyConfig = PreferencesManagerKeys.kConfigNotificationsAvailable.rawValue
        let configurationPreferencesDTO = ConfigurationPreferencesDTO(enabled: true, keyConfig: keyConfig)
        
        let dummyView = ConfigurationWalletCellViewDummy()
        sut.view = dummyView
        
        //when
        dummyPreferencesManager.forceError = true
        sut.configurationChanged(configurationPreferencesDTO)
        sut.checkError()
        
        //then
        XCTAssert(dummyBusManager.isCalledPublishParamConfigurationChanged == true)
        XCTAssert(sut.settingsTransport.isNotificationsAvailable == !configurationPreferencesDTO.enabled)
    }
    
    func test_givenConfigurationPreferencesDTO_whenConfigurationChangedAndErrorOccuredWhileSavingInPreferencesAndICalledCheckError_thenIPublishTheSameParamsSettingsTransport() {
        
        //given
        let keyConfig = PreferencesManagerKeys.kConfigNotificationsAvailable.rawValue
        let configurationPreferencesDTO = ConfigurationPreferencesDTO(enabled: true, keyConfig: keyConfig)
        
        let dummyView = ConfigurationWalletCellViewDummy()
        sut.view = dummyView
        
        //when
        dummyPreferencesManager.forceError = true
        sut.configurationChanged(configurationPreferencesDTO)
        sut.checkError()
        
        //then
        XCTAssert(sut.settingsTransport == dummyBusManager.dummySettingsTransport)
    }
    
    // MARK: - showAppSettings
    
    func test_givenAny_whenICallShowAppSettings_thenICallNotificationManagerCheckAuthorizedStatus() {
        
        // given
        let dummyView = ConfigurationWalletCellViewDummy()
        sut.view = dummyView
        
        // when
        sut.showAppSettings()
        
        // then
        XCTAssert((sut.notificationManager as! DummyNotificationManager).isCalledCheckAuthorizedStatus == true)
    }
    
    func test_givenNotificationsAuthorizationNotDetermined_whenICallShowAppSettings_thenICallNotificationManagerShowRequestNotificationAutorization() {
        
        // given
        let dummyView = ConfigurationWalletCellViewDummy()
        sut.view = dummyView
        
        (sut.notificationManager as! DummyNotificationManager).authorizationDetermined = false
        
        // when
        sut.showAppSettings()
        
        // then
        XCTAssert((sut.notificationManager as! DummyNotificationManager).isCalledShowRequestNotificationAutorization == true)
    }
    
    func test_givenNotificationsAuthorizationDetermined_whenICallShowAppSettings_thenICallOpenAppSettings() {
        
        // given
        let dummyView = ConfigurationWalletCellViewDummy()
        sut.view = dummyView
        
        (sut.notificationManager as! DummyNotificationManager).authorizationDetermined = true
        
        // when
        sut.showAppSettings()
        
        // then
        XCTAssert(dummyView.isCalledOpenAppSettings == true)
    }
    
    // MARK: - reactionForConfigMenu
    
    func test_givenItemIdNotificationsAndIsRegisterDeviceTokenFalseAndNotificationsIsAuthorizedFalse_whenICallReactionForConfigMenu_thenICalledNavToConfigDeviceNotifications() {
        
        // given
        let dummyView = ConfigurationWalletCellViewDummy()
        sut.view = dummyView
        
        (sut.notificationManager as! DummyNotificationManager).notificationsIsAuthorized = [false]
        
        dummyPreferencesManager.apnsToken = "1234"
        dummyPreferencesManager.registerDeviceToken = "0000"
        
        let itemId = ConfigurationWalletCellPageReaction.ConfigItemIds.kConfigNotifications.rawValue
        let enabled = true
        
        // when
        sut.reactionForConfigMenu(withItemId: itemId, andEnabled: enabled)
        
        // then
        wait(for: [dummyBusManager.expectationNotify], timeout: 10)
        XCTAssert(dummyBusManager.isCalledNavToConfigDeviceNotifications == true)
        
    }
    
    func test_givenItemIdNotificationsAndIsRegisterDeviceTokenFalseAndNotificationsIsAuthorizedTrue_whenICallReactionForConfigMenu_thenICalledNavToConfigDeviceNotifications() {
        
        // given
        let dummyView = ConfigurationWalletCellViewDummy()
        sut.view = dummyView
        
        (sut.notificationManager as! DummyNotificationManager).notificationsIsAuthorized = [true]
        
        dummyPreferencesManager.apnsToken = "1234"
        dummyPreferencesManager.registerDeviceToken = "0000"
        
        let itemId = ConfigurationWalletCellPageReaction.ConfigItemIds.kConfigNotifications.rawValue
        let enabled = true
        
        // when
        sut.reactionForConfigMenu(withItemId: itemId, andEnabled: enabled)
        
        // then
        wait(for: [dummyBusManager.expectationNotify], timeout: 10)
        XCTAssert(dummyBusManager.isCalledNavToConfigDeviceNotifications == true)
        
    }
    
    func test_givenItemIdNotificationsAndIsRegisterDeviceTokenTrueAndNotificationsIsAuthorizedTrue_whenICallReactionForConfigMenu_thenICalledNavToConfigMenuSectionNotifications() {
        
        // given
        let dummyView = ConfigurationWalletCellViewDummy()
        sut.view = dummyView
        
        (sut.notificationManager as! DummyNotificationManager).notificationsIsAuthorized = [true]
        
        dummyPreferencesManager.apnsToken = "1234"
        dummyPreferencesManager.registerDeviceToken = "1234"
        
        let itemId = ConfigurationWalletCellPageReaction.ConfigItemIds.kConfigNotifications.rawValue
        let enabled = true
        
        // when
        sut.reactionForConfigMenu(withItemId: itemId, andEnabled: enabled)
        
        // then
        wait(for: [dummyBusManager.expectationNotify], timeout: 10)
        XCTAssert(dummyBusManager.isCalledNavToConfigMenuSectionNotifications == true)
        
    }
    
    func test_giveSettingsTransportAndIsRegisterDeviceTokenTrueAndNotificationsIsAuthorizedTrueAndTokenMatchWithRegisteredToken_whenICallShowRequestNotificationAutorizationWithIsGrantedTrue_thenICallBusPublishWithTheSameSettingsTransport() {
        
        // given
        let dummyView = ConfigurationWalletCellViewDummy()
        sut.view = dummyView
        
        (sut.notificationManager as! DummyNotificationManager).notificationsIsAuthorized = [true]
        
        dummyPreferencesManager.apnsToken = "1234"
        dummyPreferencesManager.registerDeviceToken = "1234"
        
        let itemId = ConfigurationWalletCellPageReaction.ConfigItemIds.kConfigNotifications.rawValue
        let enabled = true
        
        let settingsTransport = SettingsTransport(isNotificationsAvailable: true, isBalancesAvailable: dummyPreferencesManager.isBalancesAvailable, isVibrationEnabled: dummyPreferencesManager.isVibrationEnabled, isTicketPromotionsEnabled: dummyPreferencesManager.isTicketPromotionsEnabled)
        
        // when
        sut.reactionForConfigMenu(withItemId: itemId, andEnabled: enabled)
        
        // then
        wait(for: [dummyBusManager.expectationNotify, dummyBusManager.expectationPublishData], timeout: 10)
        XCTAssert(dummyBusManager.isCalledPublishParamConfigurationChanged == true)
        XCTAssert(dummyBusManager.dummySettingsTransport == settingsTransport)
    }
    
    func test_givenItemIdNotificationsAndIsRegisterDeviceTokenFalseAndNotificationsIsAuthorizedTrue_whenICallReactionForConfigMenu_thenICalledRegisterDeviceOnNotificationsServiceAndTokenMatch() {
        
        // given
        let dummyView = ConfigurationWalletCellViewDummy()
        sut.view = dummyView
        
        (sut.notificationManager as! DummyNotificationManager).notificationsIsAuthorized = [true]
        
        dummyPreferencesManager.apnsToken = "1234"
        dummyPreferencesManager.registerDeviceToken = "0000"
        
        let itemId = ConfigurationWalletCellPageReaction.ConfigItemIds.kConfigNotifications.rawValue
        let enabled = true
        
        let apnsToken = dummyPreferencesManager.getValueWithPreferencesKey(forKey: PreferencesManagerKeys.kApnsToken) as? String
        
        dummySessionManager.isUserLogged = true

        // when
        sut.reactionForConfigMenu(withItemId: itemId, andEnabled: enabled)
        
        // then
        XCTAssert(dummyView.isCalledRegisterDeviceOnNotificationsService == true)
        XCTAssert(dummyView.sentToken == apnsToken)
    }
    
    func test_givenItemIdNotificationsAndIsRegisterDeviceTokenFalseAndNotificationsIsAuthorizedTrue_whenICallReactionForConfigMenu_thenINotCalledRegisterDeviceOnNotificationsService() {
        
        // given
        let dummyView = ConfigurationWalletCellViewDummy()
        sut.view = dummyView
        
        (sut.notificationManager as! DummyNotificationManager).notificationsIsAuthorized = [false]
        
        dummyPreferencesManager.apnsToken = "1234"
        dummyPreferencesManager.registerDeviceToken = "0000"
        
        let itemId = ConfigurationWalletCellPageReaction.ConfigItemIds.kConfigNotifications.rawValue
        let enabled = true
        
        dummySessionManager.isUserLogged = false
        
        // when
        sut.reactionForConfigMenu(withItemId: itemId, andEnabled: enabled)
        
        // then
        XCTAssert(dummyView.isCalledRegisterDeviceOnNotificationsService == false)
    }
    
    // MARK: - registerDeviceTokenSuccess
    
    func test_givenToken_whenICallRegisterDeviceTokenSuccess_thenICalledSaveRegisterDeviceTokenAndTokenMatch() {
        
        // given
        let token = "1234"
        
        // when
        sut.registerDeviceTokenSuccess(withToken: token)
        
        // then
        XCTAssert(dummyPreferencesManager.isCalledSaveValueWithPreferencesKey == true)
        XCTAssert(dummyPreferencesManager.registerDeviceTokenSave == token)
    }

    // MARK: - showRequestNotificationAutorization
    
    func test_givenAny_whenICallShowRequestNotificationAutorization_thenICalNotificationManagerShowRequestNotificationAuthorization() {
    
        // given
        (sut.notificationManager as! DummyNotificationManager).notificationsIsAuthorized = [true]
        
        // when
        sut.showRequestNotificationAutorization()
        
        // then
        XCTAssert((sut.notificationManager as! DummyNotificationManager).isCalledShowRequestNotificationAutorization == true)
    }
    
    // MARK: - applicationWillEnterForeground
    
    func test_givenNotificationAuthorizedFalse_whenICallApplicationWillEnterForeground_thenICallBusPublishWithRightSettingsTransport() {
        
        //given
        let dummyView = ConfigurationWalletCellViewDummy()
        sut.view = dummyView
        
        (sut.notificationManager as! DummyNotificationManager).notificationsIsAuthorized = [false]
        
        let settingsTransport = SettingsTransport(isNotificationsAvailable: false, isBalancesAvailable: dummyPreferencesManager.isBalancesAvailable, isVibrationEnabled: dummyPreferencesManager.isVibrationEnabled, isTicketPromotionsEnabled: dummyPreferencesManager.isTicketPromotionsEnabled)
        
        // when
        sut.applicationWillEnterForeground()
        
        //then
        wait(for: [dummyBusManager.expectationPublishData], timeout: 10)
        XCTAssert(dummyBusManager.isCalledPublishParamConfigurationChanged == true)
        XCTAssert(dummyBusManager.dummySettingsTransport == settingsTransport)
    }
    
    func test_givenNotificationAuthorizedTrueAndApnsTokenSavedAndRegisteredTokenSavedMatches_whenICallApplicationWillEnterForeground_thenICallBusPublishWithRightSettingsTransport() {
        
        //given
        let dummyView = ConfigurationWalletCellViewDummy()
        sut.view = dummyView
        
        (sut.notificationManager as! DummyNotificationManager).notificationsIsAuthorized = [true]
        
        dummyPreferencesManager.apnsToken = "1234"
        dummyPreferencesManager.registerDeviceToken = "1234"
        
        let settingsTransport = SettingsTransport(isNotificationsAvailable: true, isBalancesAvailable: dummyPreferencesManager.isBalancesAvailable, isVibrationEnabled: dummyPreferencesManager.isVibrationEnabled, isTicketPromotionsEnabled: dummyPreferencesManager.isTicketPromotionsEnabled)
        
        // when
        sut.applicationWillEnterForeground()
        
        //then
        wait(for: [dummyBusManager.expectationPublishData], timeout: 10)
        XCTAssert(dummyBusManager.isCalledPublishParamConfigurationChanged == true)
        XCTAssert(dummyBusManager.dummySettingsTransport == settingsTransport)
    }
    
    func test_givenNotificationAuthorizedTrueAndApnsTokenSavedAndRegisteredTokenSavedNotMatches_whenICallApplicationWillEnterForeground_thenICallBusPublishWithRightSettingsTransport() {
        
        //given
        let dummyView = ConfigurationWalletCellViewDummy()
        sut.view = dummyView
        
        (sut.notificationManager as! DummyNotificationManager).notificationsIsAuthorized = [true]
        
        dummyPreferencesManager.apnsToken = "1234"
        dummyPreferencesManager.registerDeviceToken = "4321"
        
        let settingsTransport = SettingsTransport(isNotificationsAvailable: false, isBalancesAvailable: dummyPreferencesManager.isBalancesAvailable, isVibrationEnabled: dummyPreferencesManager.isVibrationEnabled, isTicketPromotionsEnabled: dummyPreferencesManager.isTicketPromotionsEnabled)
        
        // when
        sut.applicationWillEnterForeground()
        
        //then
        wait(for: [dummyBusManager.expectationPublishData], timeout: 10)
        XCTAssert(dummyBusManager.isCalledPublishParamConfigurationChanged == true)
        XCTAssert(dummyBusManager.dummySettingsTransport == settingsTransport)
    }
    
    func test_givenSavedToken_whenICallApplicationWillEnterForeground_thenIDontCallViewRegisterDeviceOnNotificationsService() {
        
        //given
        let dummyView = ConfigurationWalletCellViewDummy()
        sut.view = dummyView
        
        dummyPreferencesManager.apnsToken = "1234"
        dummyPreferencesManager.registerDeviceToken = "1234"
        
        //when
        sut.applicationWillEnterForeground()
        
        //then
        XCTAssert(dummyView.isCalledRegisterDeviceOnNotificationsService == false)
    }
    
    func test_givenNoSavedToken_whenICallApplicationWillEnterForeground_thenICallNotificationManagerIsAuthorizedStatus() {
        
        //given
        let dummyView = ConfigurationWalletCellViewDummy()
        sut.view = dummyView
        
        dummyPreferencesManager.apnsToken = nil
        dummyPreferencesManager.registerDeviceToken = nil
        
        //when
        sut.applicationWillEnterForeground()
        
        //then
        XCTAssert((sut.notificationManager as! DummyNotificationManager).isCalledIsAuthorizedStatus == true)
    }
    
    func test_givenNoSavedTokenAndCurrentStatusAuthorizedFalse_whenICallApplicationWillEnterForeground_thenIDontCallViewRegisterDeviceOnNotificationsService() {
        
        //given
        let dummyView = ConfigurationWalletCellViewDummy()
        sut.view = dummyView
        
        dummyPreferencesManager.apnsToken = nil
        dummyPreferencesManager.registerDeviceToken = nil
        (sut.notificationManager as! DummyNotificationManager).notificationsIsAuthorized = [false]
        
        //when
        sut.applicationWillEnterForeground()
        
        //then
        XCTAssert(dummyView.isCalledRegisterDeviceOnNotificationsService == false)
    }
    
    func test_givenNoSavedTokenAndCurrentStatusAuthorizedTrueAndUserNotLogged_whenICallApplicationWillEnterForeground_thenIDontCallViewRegisterDeviceOnNotificationsService() {
        
        //given
        let dummyView = ConfigurationWalletCellViewDummy()
        sut.view = dummyView
        
        dummyPreferencesManager.apnsToken = "1234"
        dummyPreferencesManager.registerDeviceToken = nil
        (sut.notificationManager as! DummyNotificationManager).notificationsIsAuthorized = [true]
        
        dummySessionManager.isUserLogged = false
        
        //when
        sut.applicationWillEnterForeground()
        
        //then
        XCTAssert(dummyView.isCalledRegisterDeviceOnNotificationsService == false)
    }
    
    func test_givenNoSavedTokenAndCurrentStatusAuthorizedTrueAndUserLogged_whenICallApplicationWillEnterForeground_thenICallViewRegisterDeviceOnNotificationsService() {
        
        //given
        let dummyView = ConfigurationWalletCellViewDummy()
        sut.view = dummyView
        
        dummyPreferencesManager.apnsToken = "1234"
        dummyPreferencesManager.registerDeviceToken = nil
        (sut.notificationManager as! DummyNotificationManager).notificationsIsAuthorized = [true]
        
        dummySessionManager.isUserLogged = true
        
        //when
        sut.applicationWillEnterForeground()
        
        //then
        XCTAssert(dummyView.isCalledRegisterDeviceOnNotificationsService == true)
    }
    
    func test_givenSavedTokenAndCurrentStatusAuthorizedTrue_whenICallApplicationWillEnterForeground_thenICallCloseConfigurationScreen() {
        
        //given
        let dummyView = ConfigurationWalletCellViewDummy()
        sut.view = dummyView
        
        dummyPreferencesManager.apnsToken = "1234"
        dummyPreferencesManager.registerDeviceToken = "1234"
        (sut.notificationManager as! DummyNotificationManager).notificationsIsAuthorized = [true]
        
        dummyBusManager.currentCellsPage = ConfigurationWalletCellPageReaction.CELLS_PAGE_CONFIG_DEVICE_NOTIFICATIONS
        
        //when
        sut.applicationWillEnterForeground()
        
        //then
        wait(for: [dummyBusManager.expectationNotify], timeout: 10)
        XCTAssert(dummyBusManager.isCalledCloseConfigDeviceNotifications == true)
    }
    
    // MARK: - pageCellUnload
    
    func test_givenAny_whenICallPageCellUnload_thenICalledNavToConfigMenuSectionNotifications() {
        
        // given
        let dummyView = ConfigurationWalletCellViewDummy()
        sut.view = dummyView
        let cellPage = ConfigurationWalletCellPageReaction.CELLS_PAGE_CONFIG_DEVICE_NOTIFICATIONS
        sut.isNotifyCloseCellsPageDeviceNotifications = true
        
        // when
        sut.pageCellUnload(withPage: cellPage)
        
        // then
        wait(for: [dummyBusManager.expectationNotify], timeout: 10)
        XCTAssert(dummyBusManager.isCalledNavToConfigMenuSectionNotifications == true)
    }
    
    func test_givenAny_whenICallPageCellUnload_thenNotifyCloseCellsPageDeviceNotificationsIsFalse() {
        
        // given
        let dummyView = ConfigurationWalletCellViewDummy()
        sut.view = dummyView
        let cellPage = ConfigurationWalletCellPageReaction.CELLS_PAGE_CONFIG_DEVICE_NOTIFICATIONS
        sut.isNotifyCloseCellsPageDeviceNotifications = true
        
        // when
        sut.pageCellUnload(withPage: cellPage)
        
        // then
        XCTAssert(sut.isNotifyCloseCellsPageDeviceNotifications == false)
    }
    
    func test_givenAny_whenICallPageCellUnload_thenICalledShowToastSuccessWithCorrectData() {
        
        // given
        let dummyView = ConfigurationWalletCellViewDummy()
        sut.view = dummyView
        let cellPage = ConfigurationWalletCellPageReaction.CELLS_PAGE_CONFIG_DEVICE_NOTIFICATIONS
        sut.isNotifyCloseCellsPageDeviceNotifications = true
        
        // when
        sut.pageCellUnload(withPage: cellPage)
        
        // then
        wait(for: [dummyView.expectationShowToast], timeout: 10)
        XCTAssert(dummyView.isCalledShowToastSuccess == true)
        XCTAssert(dummyView.sentToastMessage == Localizables.notifications.key_ios_permission_accepted_text)
        XCTAssert(dummyView.sentToastBackgroundColor == .BBVADARKGREEN)
    }
    
    // MARK: - registerDeviceTokenFailure
    
    func test_givenErrorBOAndIsCallTokenRegisterTransparentlyForTheUserFalse_whenICallRegisterDeviceTokenFailure_thenIShowModalWithError() {
        
        // given
        let dummyView = ConfigurationWalletCellViewDummy()
        sut.view = dummyView
        
        let message = "Error"
        let errorBO = ErrorBO(error: ErrorEntity(message: message))
        sut.isCallTokenRegisterTransparentlyForTheUser = false
        
        // when
        sut.registerDeviceTokenFailure(error: errorBO)
        
        // then
        XCTAssert(sut.view?.isCalledShowError == true)
        XCTAssert(sut.view?.errorModelText == message)
        XCTAssert(dummyView.isCalledShowError == true)
    }
    
    func test_givenErrorBOAndIsCallTokenRegisterTransparentlyForTheUserFalse_whenICallRegisterDeviceTokenFailure_thenICallHideLoading() {
        
        // given
        let dummyView = ConfigurationWalletCellViewDummy()
        sut.view = dummyView
        
        let errorBO = ErrorBO(error: ErrorEntity(message: "Error"))
        sut.isCallTokenRegisterTransparentlyForTheUser = false
        
        // when
        sut.registerDeviceTokenFailure(error: errorBO)
        
        // then
        XCTAssert(dummyBusManager.isCalledHideLoading == true)
    }
    
    func test_givenErrorBOAndIsCallTokenRegisterTransparentlyForTheUserTrue_whenICallRegisterDeviceTokenFailure_thenINotShowModalWithError() {
        
        // given
        let dummyView = ConfigurationWalletCellViewDummy()
        sut.view = dummyView
        
        let errorBO = ErrorBO(error: ErrorEntity(message: "Error"))
        sut.isCallTokenRegisterTransparentlyForTheUser = true
        
        // when
        sut.registerDeviceTokenFailure(error: errorBO)
        
        // then
        XCTAssert(sut.view?.isCalledShowError == false)
        XCTAssert(dummyView.isCalledShowError == false)
    }
    
    func test_givenErrorBOAndIsCallTokenRegisterTransparentlyForTheUserTrue_whenICallRegisterDeviceTokenFailure_thenINotCallHideLoading() {
        
        // given
        let dummyView = ConfigurationWalletCellViewDummy()
        sut.view = dummyView
        
        let errorBO = ErrorBO(error: ErrorEntity(message: "Error"))
        sut.isCallTokenRegisterTransparentlyForTheUser = true
        
        // when
        sut.registerDeviceTokenFailure(error: errorBO)
        
        // then
        XCTAssert(dummyBusManager.isCalledHideLoading == false)
    }
    
    // MARK: - setModel
    
    func test_givenErrorBO_whenICallSetModel_thenErrorBOBeFilled() {
        
        // given
        let errorBO = ErrorBO(error: ErrorEntity(message: "Error"))
        
        // when
        sut.setModel(model: errorBO)
        
        // then
        XCTAssert(sut.errorBO === errorBO)
    }
    
    // MARK: - registerDeviceTokenSuccess
    
    func test_givenToken_whenICallRegisterDeviceTokenSuccess_thenSaveToken() {
        
        // given
        let token = "token"
        
        // when
        sut.registerDeviceTokenSuccess(withToken: token)
        
        // then
        XCTAssert(dummyPreferencesManager.isCalledSaveValueWithPreferencesKey == true)
        XCTAssert(dummyPreferencesManager.registerDeviceTokenSave == token)
    }
    
    func test_givenToken_whenICallRegisterDeviceTokenSuccessAndSettingsTransportAndTokenMatchWithRegisteredTokenAndIsCallTokenRegisterTransparentlyForTheUserFalse_thenICallBusPublishWithTheSameSettingsTransport() {
        
        //given
        let token = "token"
        dummyPreferencesManager.registerDeviceToken = token
        dummyPreferencesManager.apnsToken = token
        
        let settingsTransport = SettingsTransport(isNotificationsAvailable: dummyPreferencesManager.isNotificationsAvailable, isBalancesAvailable: dummyPreferencesManager.isBalancesAvailable, isVibrationEnabled: dummyPreferencesManager.isVibrationEnabled, isTicketPromotionsEnabled: dummyPreferencesManager.isTicketPromotionsEnabled)
        
        sut.isCallTokenRegisterTransparentlyForTheUser = false
        dummyBusManager.currentCellsPage = ConfigurationWalletCellPageReaction.CELLS_PAGE_CONFIG_DEVICE_NOTIFICATIONS
        
        // when
        sut.registerDeviceTokenSuccess(withToken: token)
        
        //then
        wait(for: [dummyBusManager.expectationNotify, dummyBusManager.expectationPublishData], timeout: 10)
        XCTAssert(dummyBusManager.isCalledPublishParamConfigurationChanged == true)
        XCTAssert(dummyBusManager.dummySettingsTransport == settingsTransport)
    }
    
    func test_givenToken_whenICallRegisterDeviceTokenSuccessAndSettingsTransportAndTokenNotMatchWithRegisteredTokenAndIsCallTokenRegisterTransparentlyForTheUserFalse_thenICallBusPublishWithTheSameSettingsTransport() {
        
        //given
        let token = "token"
        dummyPreferencesManager.registerDeviceToken = token
        dummyPreferencesManager.apnsToken = "1234"
        
        let settingsTransport = SettingsTransport(isNotificationsAvailable: false, isBalancesAvailable: dummyPreferencesManager.isBalancesAvailable, isVibrationEnabled: dummyPreferencesManager.isVibrationEnabled, isTicketPromotionsEnabled: dummyPreferencesManager.isTicketPromotionsEnabled)
        
        sut.isCallTokenRegisterTransparentlyForTheUser = false
        dummyBusManager.currentCellsPage = ConfigurationWalletCellPageReaction.CELLS_PAGE_CONFIG_DEVICE_NOTIFICATIONS
        
        // when
        sut.registerDeviceTokenSuccess(withToken: token)
        
        //then
        wait(for: [dummyBusManager.expectationNotify, dummyBusManager.expectationPublishData], timeout: 10)
        XCTAssert(dummyBusManager.isCalledPublishParamConfigurationChanged == true)
        XCTAssert(dummyBusManager.dummySettingsTransport == settingsTransport)
    }
    
    func test_givenTokenAndIsCallTokenRegisterTransparentlyForTheUserFalse_whenICallRegisterDeviceTokenSuccess_thenICallHideLoading() {
        
        // given
        let token = "token"
        sut.isCallTokenRegisterTransparentlyForTheUser = false
        
        // when
        sut.registerDeviceTokenSuccess(withToken: token)
        
        // then
        XCTAssert(dummyBusManager.isCalledHideLoading == true)
    }
    
    func test_givenTokenAndIsCallTokenRegisterTransparentlyForTheUserFalse_whenICallRegisterDeviceTokenSuccess_thenNotifyCloseCellsPageDeviceNotificationsIsTrue() {
        
        // given
        let dummyView = ConfigurationWalletCellViewDummy()
        sut.view = dummyView
        
        let token = "token"
        sut.isCallTokenRegisterTransparentlyForTheUser = false
        dummyBusManager.currentCellsPage = ConfigurationWalletCellPageReaction.CELLS_PAGE_CONFIG_DEVICE_NOTIFICATIONS
        
        // when
        sut.registerDeviceTokenSuccess(withToken: token)
        
        // then
        wait(for: [dummyBusManager.expectationNotify], timeout: 10)
        XCTAssert(sut.isNotifyCloseCellsPageDeviceNotifications == true)
    }
    
    func test_givenTokenAndIsCallTokenRegisterTransparentlyForTheUserFalse_whenICallRegisterDeviceTokenSuccess_thenNavigateToCloseConfigDeviceNotifications() {
        
        // given
        let dummyView = ConfigurationWalletCellViewDummy()
        sut.view = dummyView
        
        let token = "token"
        sut.isCallTokenRegisterTransparentlyForTheUser = false
        dummyBusManager.currentCellsPage = ConfigurationWalletCellPageReaction.CELLS_PAGE_CONFIG_DEVICE_NOTIFICATIONS
        
        // when
        sut.registerDeviceTokenSuccess(withToken: token)
        
        // then
        wait(for: [dummyBusManager.expectationNotify], timeout: 10)
        XCTAssert(dummyBusManager.isCalledCloseConfigDeviceNotifications == true)
    }
    
    class ConfigurationWalletCellViewDummy: ConfigurationWalletCellViewProtocol {

        var isCalledShowError = false
        var errorModelText = ""
        var isCalledOpenAppSettings = false
        var isCalledRegisterDeviceOnNotificationsService = false
        var sentToken = ""
        var isCalledShowToastSuccess = false
        var sentToastMessage = ""
        var sentToastBackgroundColor: UIColor?
        
        let expectationShowToast = XCTestExpectation(description: "Waiting for success toast")

        func showError(error: ModelBO) {
            
            isCalledShowError = true
            if let error = error as? ErrorBO {
                errorModelText = error.errorMessage()!
            }
        }

        func changeColorEditTexts() {
        }

        func openAppSettings() {
            
            isCalledOpenAppSettings = true
        }
        
        func registerDeviceOnNotificationsService(withToken token: String) {
            
            isCalledRegisterDeviceOnNotificationsService = true
            sentToken = token
        }
        
        func showToastSuccess(withSuccessMessage message: String, andBackgroundColor color: UIColor) {
            
            isCalledShowToastSuccess = true
            sentToastMessage = message
            sentToastBackgroundColor = color
            
            expectationShowToast.fulfill()
        }
    }

    class DummyBusManager: BusManager {

        var isCalledPublishParamConfigurationChanged = false
        var dummySettingsTransport: SettingsTransport?
        var isCalledNavToConfigDeviceNotifications = false
        var isCalledNavToConfigMenuSectionNotifications = false
        var isCalledPublishDataSetting = false
        var isCalledCloseConfigDeviceNotifications = false
        
        let expectationNotify = XCTestExpectation(description: "Checking notifications authorization")
        let expectationPublishData = XCTestExpectation(description: "Checking notifications authorization")
        
        var isCalledShowLoading = false
        var isCalledHideLoading = false
        
        override init() {
            super.init()
            routerFactory = RouterInitFactory()
        }

        override func publishData<T>(tag: String, _ event: ActionSpec<T>, _ values: T) {

            if tag == ConfigurationWalletCellPageReaction.ROUTER_TAG && event === ConfigurationWalletCellPageReaction.PARAMS_CONFIGURATION_CHANGED {
                
                isCalledPublishParamConfigurationChanged = true
                dummySettingsTransport = SettingsTransport.deserialize(from: values as? [String: Any])
                
            } else if tag == LeftMenuPageReaction.ROUTER_TAG && event === LeftMenuPageReaction.EVENT_PARAMS_CONFIGURATION {
                
                isCalledPublishDataSetting = true
                dummySettingsTransport = SettingsTransport.deserialize(from: values as? [String: Any])
            }
            
            expectationPublishData.fulfill()
        }
        
        override func notify<T>(tag: String, _ event: ActionSpec<T>) {
            
            if tag == ConfigurationWalletCellPageReaction.ROUTER_TAG && event === ConfigurationWalletCellPageReaction.EVENT_NAV_TO_CONFIG_DEVICE_NOTIFICATIONS {
                isCalledNavToConfigDeviceNotifications = true
            } else if tag == ConfigurationWalletCellPageReaction.ROUTER_TAG && event === ConfigurationWalletCellPageReaction.EVENT_NAV_TO_CONFIG_MENU_SECTION_NOTIFICATIONS {
                isCalledNavToConfigMenuSectionNotifications = true
            } else if tag == ConfigurationWalletCellPageReaction.ROUTER_TAG && event === ConfigurationWalletCellPageReaction.EVENT_CLOSE_CONFIG_DEVICE_NOTIFICATIONS {
                isCalledCloseConfigDeviceNotifications = true
            }
            
            expectationNotify.fulfill()
        }
        
        override func showLoading(completion: (() -> Void)?) {
            isCalledShowLoading = true
        }
        
        override func hideLoading(completion: (() -> Void)?) {
            isCalledHideLoading = true
        }
    }

    class DummyPreferencesManager: PreferencesManager {

        var isCalledGetValueWithPreferencesKey = false
        var isCalledSaveValueWithPreferencesKey = false
        var isBalancesAvailableSave: Bool?
        var isNotificationsAvailableSave: Bool?
        var isVibrationEnabledSave: Bool?
        var isTicketPromotionsEnabledSave: Bool?
        var registerDeviceTokenSave: String?

        var forceError = false
        
        var forceGetValueTrue = false
        var registerDeviceToken: String?
        var apnsToken: String?
        
        var isBalancesAvailable = false
        var isNotificationsAvailable = false
        var isVibrationEnabled = false
        var isTicketPromotionsEnabled = false

        override func saveValueWithPreferencesKey(forValue value: AnyObject, withKey key: PreferencesManagerKeys) -> Bool {

            isCalledSaveValueWithPreferencesKey = true
            
            if forceError {
                return false
            }
            
            switch key {
            case PreferencesManagerKeys.kConfigBalancesAvailable:
                isBalancesAvailableSave = (value as? Bool)
            case PreferencesManagerKeys.kConfigNotificationsAvailable:
                isNotificationsAvailableSave = (value as? Bool)
            case PreferencesManagerKeys.kConfigVibrationEnabled:
                isVibrationEnabledSave = (value as? Bool)
            case PreferencesManagerKeys.kConfigTicketPromotionsEnabled:
                isTicketPromotionsEnabledSave = (value as? Bool)
            case PreferencesManagerKeys.kRegisterDeviceToken:
                registerDeviceTokenSave = (value as? String)
            default:
                break
            }
            
            return true
        }
        
        override func getValueWithPreferencesKey(forKey key: PreferencesManagerKeys) -> AnyObject? {
            
            isCalledGetValueWithPreferencesKey = true
            
            if forceGetValueTrue {
                return true as AnyObject
            }
            
            switch key {
            case PreferencesManagerKeys.kRegisterDeviceToken:
                return registerDeviceToken as AnyObject
            case PreferencesManagerKeys.kConfigBalancesAvailable:
                return isBalancesAvailable as AnyObject
            case PreferencesManagerKeys.kConfigNotificationsAvailable:
                return isNotificationsAvailable as AnyObject
            case PreferencesManagerKeys.kConfigVibrationEnabled:
                return isVibrationEnabled as AnyObject
            case PreferencesManagerKeys.kConfigTicketPromotionsEnabled:
                return isTicketPromotionsEnabled as AnyObject
            case PreferencesManagerKeys.kApnsToken:
                return apnsToken as AnyObject
            default:
                break
            }
            
            return false as AnyObject
        }
    }
}
