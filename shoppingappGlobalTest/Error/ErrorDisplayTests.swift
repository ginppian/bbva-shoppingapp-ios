//
//  ErrorDisplayTests.swift
//  shoppingapp
//
//  Created by jesus.martinez on 8/1/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class ErrorDisplayTests: XCTestCase {

    // MARK: - Init Error

    func test_givenNotErrorBO_whenICallInitError_thenMessageShouldBeNilAndIconColorShouldBeBBVALightRed() {

        // given

        // when
        let errorDisplay = ErrorDisplay(error: nil)

        // then
        XCTAssert(errorDisplay.message == nil)
        XCTAssert(errorDisplay.codeMessage == nil)
        XCTAssert(errorDisplay.iconColor == .BBVALIGHTRED)
    }

    func test_givenErrorBOWithDefaultValuesAndMessage_whenICallInitError_thenDataShouldMatchWithErrorBO() {

        // given
        let errorBO = ErrorBO(error: ErrorEntity(message: "Error"))

        // when
        let errorDisplay = ErrorDisplay(error: errorBO)

        // then
        XCTAssert(errorDisplay.message != nil)
        XCTAssert(errorDisplay.message == errorBO.errorMessage())
        XCTAssert(errorDisplay.codeMessage == errorBO.code)
        XCTAssert(errorDisplay.iconColor == .BBVALIGHTRED)
        XCTAssert(errorDisplay.showCancel == errorBO.allowCancel)
    }
    
    func test_givenErrorBOWithExpiredSessionError_whenICallInitError_thenErrorDisplayMatch() {
        
        // given
        let errorEntity = ErrorEntity(message: "Error")
        errorEntity.httpStatus = ConstantsHTTPCodes.STATUS_403
        errorEntity.code = ErrorCode.expiredSession.rawValue
        let errorBO = ErrorBO(error: errorEntity)
        
        // when
        let errorDisplay = ErrorDisplay(error: errorBO)
        
        // then
        XCTAssert(errorDisplay.message == errorBO.errorMessage())
        XCTAssert(errorDisplay.codeMessage == errorBO.errorCode())
    }
    
    func test_givenErrorBOWithExpiredSessionError_whenICallInitError_thenDataShouldMatchWithErrorBO() {
        
        // given
        let errorEntity = ErrorEntity(message: "Error")
        errorEntity.httpStatus = ConstantsHTTPCodes.STATUS_403
        errorEntity.code = ErrorCode.expiredSession.rawValue
        let errorBO = ErrorBO(error: errorEntity)
        
        // when
        let errorDisplay = ErrorDisplay(error: errorBO)
        
        // then
        XCTAssert(errorDisplay.message == Localizables.login.key_expired_tsec)
        XCTAssert(errorDisplay.codeMessage == nil)
    }

    func test_givenErrorBOWithCustomValuesAndMessage_whenICallInitError_thenDataShouldMatchWithErrorBO() {

        // given
        let errorBO = ErrorBO(error: ErrorEntity(message: "Error"))
        errorBO.allowCancel = true

        // when
        let errorDisplay = ErrorDisplay(error: errorBO)

        // then
        XCTAssert(errorDisplay.message != nil)
        XCTAssert(errorDisplay.message == errorBO.errorMessage())
        XCTAssert(errorDisplay.codeMessage == errorBO.code)
        XCTAssert(errorDisplay.iconColor == .BBVALIGHTRED)
        XCTAssert(errorDisplay.showCancel == errorBO.allowCancel)
    }
    
    func test_givenErrorBOWithCustomOkButtonAndCancelButtonAndErrorTypeInfo_whenICallInitError_thenDataShouldMatchWithErrorBO() {

        // given
        let errorBO = ErrorBO()
        errorBO.allowCancel = true
        errorBO.errorType = .info
        errorBO.okButtonTitle = Localizables.login.key_login_disconnect_understood_text

        // when
        let errorDisplay = ErrorDisplay(error: errorBO)

        // then
        XCTAssert(errorDisplay.codeMessage == errorBO.code)
        XCTAssert(errorDisplay.iconColor == .BBVALIGHTBLUE)
        XCTAssert(errorDisplay.showCancel == errorBO.allowCancel)
        XCTAssert(errorDisplay.okTitle == errorBO.okButtonTitle)
        XCTAssert(errorDisplay.messageAttributed == errorBO.messageAttributed)
    }
    
    func test_givenErrorBOWithErrorTypeWarning_whenICallInitError_thenIconNamedAndIconColorShouldBeMatch() {

        // given
        let errorBO = ErrorBO()
        errorBO.errorType = .warning

        // when
        let errorDisplay = ErrorDisplay(error: errorBO)

        // then
        XCTAssert(errorDisplay.iconColor == .BBVALIGHTRED)
        XCTAssert(errorDisplay.iconNamed == ConstantsImages.Common.alert_icon)
    }

    func test_givenErrorBOWithErrorTypeInfo_whenICallInitError_thenIconNamedAndIconColorShouldBeMatch() {

        // given
        let errorBO = ErrorBO()
        errorBO.errorType = .info

        // when
        let errorDisplay = ErrorDisplay(error: errorBO)

        // then
        XCTAssert(errorDisplay.iconColor == .BBVALIGHTBLUE)
        XCTAssert(errorDisplay.iconNamed == ConstantsImages.Common.info_icon)
    }
}
