//
//  shoppingappTest.swift
//  shoppingappTest
//
//  Created by ruben.fernandez on 8/6/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class ErrorPresenterTest: XCTestCase {

    var sut: ErrorPresenter<ErrorViewDummy>!

    var dummyBusManager: DummyBusManager!

    override func setUp() {
        super.setUp()
        dummyBusManager = DummyBusManager()
        sut = ErrorPresenter<ErrorViewDummy>(busManager: dummyBusManager)
    }

    override func tearDown() {
        super.tearDown()
    }

    // MARK: - didLoad
    func test_whenICallDidLoad_thenCallConfigureView() {

        //given
        let dummyView = ErrorViewDummy()
        sut.view = dummyView

        //when
        sut.didLoad()

        //then
        XCTAssert(dummyView.isCalledConfigureView == true)
    }
    
    func test_whenICallDidLoad_thenCallShowMessageError() {

        //given
        let dummyView = ErrorViewDummy()
        sut.view = dummyView

        //when
        sut.didLoad()

        //then
        XCTAssert(dummyView.isCalledShowMessageError == true)

    }

    func test_givenAErrorMessage_whenICallDidLoad_thenTheErrorMessageInViewIsTheSame() {

        //given
        let errorBO = ErrorBO(error: ErrorEntity(message: "Error en el password"))

        sut.errorBO = errorBO
        let dummyView = ErrorViewDummy()
        sut.view = dummyView

        //when
        sut.didLoad()

        //then
        XCTAssertEqual(dummyView.errorDisplay?.message, errorBO.errorMessage())
    }

    class ErrorViewDummy: ErrorViewProtocol {

        var isCalledConfigureView = false
        var isCalledShowMessageError = false

        var errorDisplay: ErrorDisplay?

        func configureView() {
            isCalledConfigureView = true
        }

        func showMessageError(error: ErrorDisplay) {
            errorDisplay = error
            isCalledShowMessageError = true
        }

        func showError(error: ModelBO) {
        }

        func changeColorEditTexts() {
        }
    }

    class DummyBusManager: BusManager {

        override init() {
            super.init()
            routerFactory = RouterInitFactory()
        }

    }

}
