//
//  ShoppingGuaranteePresenterTest.swift
//  shoppingapp
//
//  Created by Rubén Jacobo on 10/17/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
@testable import shoppingappMX
#elseif TESTPE
@testable import shoppingappPE
#endif

class ShoppingGuaranteePresenterTest: XCTestCase {
    var sut: ShoppingGuaranteePresenter<DummyView>!
    
    override func setUp() {
        
        super.setUp()
        DummyPreferencesManager.configureDummyPreferences()
        sut = ShoppingGuaranteePresenter<DummyView>()
    }
    
    // MARK: - ViewLoaded
    
    // MARK: - ViewLoaded - Show Promotion Guarantee
    
    func test_givenPromotionBO_whenICallViewLoaded_thenICallShowPromotionGuarantee() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        
        sut.promotionBO = promotionBO
        
        // when
        sut.viewLoaded()
        
        //then
        XCTAssert(dummyView.isCalledShowPromotionGuarantee)
        
    }
    
    // MARK: - ViewLoaded - Contact details is filled
    
    func test_givenPromotionBO_whenICallViewLoaded_thenContactDetailsIsFilled() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        
        sut.promotionBO = promotionBO
        
        // when
        sut.viewLoaded()
        
        //then
        XCTAssert(promotionBO.contactDetails != nil)
        
    }
    
    // MARK: - ViewLoaded - Contact details description is correct
    
    func test_givenPromotionBO_whenICallViewLoaded_thenContactDetailsDescriptionIsCorrect() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        let contactDetails = promotionBO.contactDetails![0]
        sut.promotionBO = promotionBO
        
        // when
        sut.viewLoaded()
        
        //then
        XCTAssert(dummyView.guaranteeValue == contactDetails.description)
        
    }
    
    // MARK: - ViewLoaded - Contact details description count is correct
    func test_givenPromotionBO_whenICallViewLoaded_thenContactDetailsDescriptionCountIsCorrect() {
        
        let dummyView = DummyView()
        sut.view = dummyView
        
        // given
        let promotionBO = PromotionsGenerator.getPromotionDetail()
        let contactDetails = promotionBO.contactDetails![0]
        
        sut.promotionBO = promotionBO
        
        // when
        sut.viewLoaded()
        
        //then
        XCTAssert(dummyView.guaranteeValue?.count == contactDetails.description?.count)
        
    }
    
    // MARK: - DummyData
    
    class DummyView: ShoppingGuaranteeViewProtocol, ViewProtocol {
        
        var guaranteeValue: String?
        var titleValue: String?
        var isCalledShowPromotionGuarantee = false
        
        func showPromotionGuarantee(guarantee: String, withTitle title: String) {
            guaranteeValue = guarantee
            titleValue = title
            isCalledShowPromotionGuarantee = true
        }
        
        func showError(error: ModelBO) {
            
        }
        
        func changeColorEditTexts() {
            
        }
    }
    
    class DummyBusManager: BusManager {
        override init() {
            super.init()
            routerFactory = RouterInitFactory()
        }
    }
}
