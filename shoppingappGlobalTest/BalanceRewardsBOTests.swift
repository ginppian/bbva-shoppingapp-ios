//
//  BalanceRewardsBOTests.swift
//  shoppingapp
//
//  Created by jesus.martinez on 19/4/18.
//  Copyright © 2018 BBVA. All rights reserved.
//

import XCTest

#if TESTMX
    @testable import shoppingappMX
#elseif TESTPE
    @testable import shoppingappPE
#endif

class BalanceRewardsBOTests: XCTestCase {

    // MARK: - rewardBreakDown id

    func test_givenAnyIdAndRewardBreakDownEmpty_whenICallRewardBreadDownId_thenReturnNil() {

        // given
        let id = RewardBreakDownId.current
        let sut = BalanceRewardsGenerator.balanceEmptyBO()

        // when
        let returned = sut.rewardBreakDown(id)

        // then
        XCTAssert(returned == nil)

    }

    func test_givenIdUnknonAndRewardBreakDownNoEmpty_whenICallRewardBreadDownId_thenReturnNil() {

        // given
        let id = RewardBreakDownId.unknown
        let sut = BalanceRewardsGenerator.balanceCompleteBO()

        // when
        let returned = sut.rewardBreakDown(id)

        // then
        XCTAssert(returned == nil)

    }

    func test_givenAndIdDifferentThanUnknounAndRewardBreakDownNoEmptyAndConstaintsTheId_whenICallRewardBreadDownId_thenReturnRewardBreakDownBOCorrect() {

        // given
        let id = RewardBreakDownId.current
        let sut = BalanceRewardsGenerator.balanceCompleteBO()

        // when
        let returned = sut.rewardBreakDown(id)

        // then
        XCTAssert(returned != nil)
        XCTAssert(returned?.id == id)

    }

    func test_givenAndIdDifferentThanUnknounAndRewardBreakDownNoEmptyAndDoesNotConstaintTheId_whenICallRewardBreadDownId_thenReturnNil() {

        // given
        let id = RewardBreakDownId.current
        let sut = BalanceRewardsGenerator.balanceCompleteBO()
        sut.rewardBreakDown.remove(at: 3)

        // when
        let returned = sut.rewardBreakDown(id)

        // then
        XCTAssert(returned == nil)

    }
}
