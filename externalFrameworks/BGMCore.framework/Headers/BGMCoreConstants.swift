//
//  BGMCoreConstants.swift
//  mobileNucleoSDK
//
//  Created by Manish Sama on 11/22/16.
//  Copyright © 2016 BBVA. All rights reserved.
//

import UIKit

/**
    Completion Handler which serves as a call back for data obtained from a webservice call
    
    - Parameter jsonResponse : Actual response body in terms of jsonResponse
    - Parameter  errorOrSuccessResponse : HTTP URL Response which has statusCode and
        for error : BGMError(data model) is sent
        for success : BGMSuccessHeadersResponse (data model) is sent
    - Parameter  opCode : The operation code from the call in the module
    - Parameter  success : Gives boolean value if call is success or failure
 */
public typealias SuccessFailureHandler = (_ jsonResponse: Any,_ errorOrSuccessResponse: Any, _ opCode : String, _ success : Bool)-> ()

/// The Core constants for internal calls
public class BGMCoreConstants: NSObject {
    
    @available(*, deprecated)
    private static let BGM_CORE_LOGIN_APPEND_URL        = "ASO/TechArchitecture/grantingTickets/V02"
    @available(*, deprecated)
    private static let BGM_CORE_USER_DETAILS_APPEND_URL = "NAS/users/GLOBAL"
    @available(*, deprecated)
    private static let BGM_CORE_ACCOUNTS_APPEND_URL     = "NAS/accounts?views=full&expand=related-contracts%2Climits%2Cholds%2Ccustomized-formats"
    
    @available(*, deprecated)
    private static let BGM_CORE_ERROR_MESSAGE_FOR_NO_URL_PATH       = "Please provide url Path"
    @available(*, deprecated)
    private static let BGM_CORE_ERROR_DESCRIPTION_FOR_NO_URL_PATH   = "URL path is empty.Please provide url which needs to be appended to host url"
    
    //MARK: Class variables
    /// The previous host url (deprecated)
    @available(*, deprecated)
    public static let BGM_CORE_URL                  = "http://52.51.52.11:8000/"
  
    /// Operation code for login (deprecated)
    @available(*, deprecated)
    public static let BGM_CORE_LOGIN_OPCODE        = "0011A"
    /// Operation code for user details (deprecated)
    @available(*, deprecated)
    public static let BGM_CORE_USER_DETAILS_OPCODE = "0011B"
    /// Operation code for accounts (deprecated)
    @available(*, deprecated)
    public static let BGM_CORE_ACCOUNTS_OPCODE     = "0011C"
    /// Operation code for the aggregator
    public static let BGM_CORE_AGGREGATOR_OPCODE      = "0011D"
    /// Operation code for the app settings
    public static let BGM_CORE_APP_SETTINGS_OPCODE    = "AS001"
    /// Operation code for public login
    public static let BGM_CORE_PUBLIC_LOGIN_OPCODE     = "COM014"
    /*
     HTTP COMMON HEADER FIELDS AND VALUE CONSTANTS
     */
    /// The key name of the header for the authentication
    public static let BGM_CORE_SESSION_ID_KEY          = "tsec"
    /// The key name of the header for the authentication
    public static let BGM_CORE_CONTACT_ID_KEY          = "ContactId"
    /// The key name of the header for the TDD
    public static let BGM_CORE_CONSUMER_REQUEST_ID_KEY = "ConsumerRequestId"
    /// The key name of the header for the TDD
    public static let BGM_CORE_BBVA_USER_AGENT_KEY          = "bbva-user-agent"
    /// The key for the content type HTTP header
    public static let BGM_CORE_CONTENT_TYPE_KEY        = "Content-Type"
    /// The value of the content type HTTP header
    public static let BGM_CORE_CONTENT_TYPE_VALUE      = "application/json"
    /// The key of the language HTTP header
    public static let BGM_CORE_ACCEPT_LANG_KEY        = "Accept-Language"
    /// The value of the language HTTP header
    public static let BGM_CORE_ACCEPT_LANG_VALUE      = "es"
    /// Unique Device Identifier key
    public static let BGM_DEVICE_ID_KEY                     = "vnd.bbva.device-id"
    /// Device OS name key
    public static let BGM_DEVICE_OS_NAME_KEY                = "vnd.bbva.device-os-name"
    /// Device Model name key
    public static let BGM_DEVICE_MODEL_NAME_KEY             = "vnd.bbva.device-model-name"
    /// Device model manufacturer name key
    public static let BGM_DEVICE_MODEL_FACTURER_NAME_KEY    = "vnd.bbva.device-model-facturer"
    /// Device screen size key
    public static let BGM_DEVICE_SCREEN_SIZE_KEY            = "vnd.bbva.device-screen-size"
    /// Device OS version key
    public static let BGM_DEVICE_OS_VERSION_KEY             = "vnd.bbva.device-os-version"
    /// Device screen resolution key
    public static let BGM_DEVICE_SCREEN_RESOLUTION_KEY      = "vnd.bbva.device.dpi"
    /// Device app name key
    public static let BGM_DEVICE_APP_NAME_KEY               = "vnd.bbva.device.app-name"
    /// Device app version key
    public static let BGM_DEVICE_APP_VERSION_KEY            = "vnd.bbva.device.app-version"
    /// Device manufacturer
    public static let BGM_DEVICE_MANUFACTURER_VALUE         = "Apple"
    
    public static let BGM_CONTENT_DISPOSITION_KEY           = "Content-Disposition"

    public static let BGM_ATTACHMENT_TYPE                   = "attachment"
	
	public static let BGM_PDF_BINARY 						= "PDF"
	
	public static let BGM_BINARY_DATA 						= "binaryData"

    /*
    LOGIN FIELDS AND VALUE CONSTANTS
     */
    public static let BGM_CORE_CONSUMER_ID_KEY          = "consumerID"
    public static let BGM_CORE_PUBLIC_KEY               = "public"
    public static let BGM_CORE_AUTHENTICATION_TYPE_KEY  = "authenticationType"
    public static let BGM_CORE_AUTHENTICATION_DATA_KEY  = "authenticationData"
    public static let BGM_CORE_USER_ID_KEY              = "userID"
    public static let BGM_CORE_AUTHENTICATION_KEY       = "authentication"
    public static let BGM_CORE_ACCESS_CODE_KEY          = "accessCode"
    public static let BGM_CORE_DIALOG_ID_KEY            = "dialogId"
    public static let BGM_CORE_BACKEND_REQUEST_KEY      = "backendUserRequest"
    public static let BGM_CORE_LOGIN_GRANTING_TICKET    = "grantingTicket"
    public static let BGM_CORE_LOGIN_TECH_ARCHITECTURE  = "TechArchitecture"

    /*
        HTTP REQUEST METHOD STRINGS
    */
    /// The name of the HTTP GET Method
    public static let BGM_CORE_HTTP_METHOD_GET     = "GET"
    /// The name of the HTTP POST Method
    public static let BGM_CORE_HTTP_METHOD_POST    = "POST"
    /// The name of the HTTP PUT Method
    public static let BGM_CORE_HTTP_METHOD_PUT     = "PUT"
    /// The name of the HTTP DELETE Method
    public static let BGM_CORE_HTTP_METHOD_DELETE  = "DELETE"
    /// The name of the HTTP HEAD Method
    public static let BGM_CORE_HTTP_METHOD_HEAD    = "HEAD"
    /// The name of the HTTP PATCH Method
    public static let BGM_CORE_HTTP_METHOD_PATCH   = "PATCH"
    /// The name of the HTTP TRACE Method
    public static let BGM_CORE_HTTP_METHOD_TRACE   = "TRACE"
    /// The name of the HTTP CONNECT Method
    public static let BGM_CORE_HTTP_METHOD_CONNECT = "CONNECT"
    
    /*
     ERROR CONSTANTS
     */
    /// The error code key
    public static let BGM_CORE_ERROR_CODE_KEY                      = "error-code"
    /// The system error code key
    public static let BGM_CORE_SYSTEM_ERROR_CODE_KEY               = "system-error-code"
    /// The error message key
    public static let BGM_CORE_ERROR_MESSAGE_KEY                   = "error-message"
    /// The error description key
    public static let BGM_CORE_ERROR_DESCRIPTION_KEY               = "system-error-description"
    /// The empty response key
    public static let BGM_CORE_NIL_RESPONSE_ERROR_KEY              = "nil response from server"
    
    /*
     Error Codes
     */
    /// The internal error code for JSON parsing
    public static let BGM_JSON_CONVERSION_EXCEPTION_ERROR_CODE = 600
    /// The internal error for empty responses
    public static let BGM_NIL_RESPONSE_ERROR_CODE = 700
    
    /*
    Url Constants
    */
    public static let BGM_CORE_EXPAND_KEY                       = "expand"
    public static let BGM_CORE_PARAMS_KEY                       = "params"
    public static let BGM_CORE_AGGREGATOR_KEY                   = "aggregator"
    public static let BGM_CORE_METADATA_ID_KEY                  = "metadata"
    public static let BGM_CORE_IMAGE_ID_KEY                     = "byteFile"
    public static let BGM_CORE_IMAGE_NAME_KEY                   = "test.png"
    public static let BGM_CORE_DATA_KEY                         = "data"
    public static let BGM_CORE_BASE64_KEY                       = "base64"

    /*
     Mutipart Constants
    */
    public static let BGM_BOUNDARY                              = "*****"
    public static let BGM_CRLF                                  = "\r\n"
    public static let BGM_LF                                    = "\n"
    public static let BGM_QUOTES                                = "\""
    public static let BGM_ESCAPED_QUOTES                        = "\\\""
    public static let BGM_TWO_HYPHENS                           = "--"
    public static let BGM_MULTIPART_FORM_DATA                   = "multipart/form-data"
	public static let BGM_MULTIPART_MIX_DATA                    = "multipart/mixed"
    public static let BGM_BOUNDARY_HEADER                       = "; boundary="
    public static let BGM_MULTIPART_CONTENT_TYPE                = "multipart/form-data;boundary=" + BGMCoreConstants.BGM_BOUNDARY
    public static let BGM_CONTENT_ID_KEY                        = "Content-ID"
    public static let BGM_MULTIPART_PART_PREFIX                 = "multipart"
    public static let BGM_MULTIPART_CONTENT_PREFIX              = "Content"
    public static let BGM_CR                                    = "\r"
    public static let BGM_SEMI_COLLON                           = ";"
    public static let BGM_COLLON                                = ":"

    public static let BGM_CORE_FILE                             = "file"
    public static let BGM_CORE_NAME                             = "name"
    public static let BGM_APP_SETTINGS                          = "app-settings"
    public static let BGM_PUBLIC_SETTINGS                       = "public-settings"
    public static let BGM_APP_SETTINGS_ENVIRONMENT              = "public-settings-id"
    public static let BGM_ADD_CONTENT_TYPE_IN_BODY              = "addContentType"
    public static let BGM_SIGNATURES_CALL                       = "signaturesCall"
    public static let BGM_APP_SETTINGS_FILE_NAME                = "app-settings.json"
    

}

public enum BGMGlomoAppConfiguration : String {
    case kApiVersion = "api_versions"
    case kAppSettings = "app_settings"
    case kConnectConfig = "connect_config"
    case kCurrencies = "currencies"
    case kExpandParams = "expand_params"
    case kOnOffFeatures = "onoff_features"
    case kOperations = "operations"
    case kServerConfig = "server_config"
    case kUIConfig = "ui_config"
    case kWorkflowLocal = "workflow_local"
    case kWorkflow = "workflow"
    
    public static func getCurrentConfigurations() -> [BGMGlomoAppConfiguration] {
        return [.kApiVersion, .kAppSettings, .kConnectConfig, .kCurrencies, .kExpandParams, .kOnOffFeatures, .kOperations, .kServerConfig, .kUIConfig, .kWorkflowLocal, .kWorkflow]
    }
}

extension UIDevice {
    
    var modelName: String {
        var systemInfo = utsname()
        uname(&systemInfo)
        let machineMirror = Mirror(reflecting: systemInfo.machine)
        let identifier = machineMirror.children.reduce("") { identifier, element in
            guard let value = element.value as? Int8, value != 0 else { return identifier }
            return identifier + String(UnicodeScalar(UInt8(value)))
        }
        
        switch identifier {
        case "iPod5,1":                                 return "iPod Touch 5"
        case "iPod7,1":                                 return "iPod Touch 6"
        case "iPhone3,1", "iPhone3,2", "iPhone3,3":     return "iPhone 4"
        case "iPhone4,1":                               return "iPhone 4s"
        case "iPhone5,1", "iPhone5,2":                  return "iPhone 5"
        case "iPhone5,3", "iPhone5,4":                  return "iPhone 5c"
        case "iPhone6,1", "iPhone6,2":                  return "iPhone 5s"
        case "iPhone7,2":                               return "iPhone 6"
        case "iPhone7,1":                               return "iPhone 6 Plus"
        case "iPhone8,1":                               return "iPhone 6s"
        case "iPhone8,2":                               return "iPhone 6s Plus"
        case "iPhone8,3", "iPhone8,4":                  return "iPhone SE"
        case "iPhone9,1", "iPhone9,3":                  return "iPhone 7"
        case "iPhone9,2", "iPhone9,4":                  return "iPhone 7 Plus"
        case "iPhone10,1", "iPhone10,4":                return "iPhone 8"
        case "iPhone10,2", "iPhone10,5":                return "iPhone 8 Plus"
        case "iPhone10,3", "iPhone10,6":                return "iPhone X"
        case "iPad2,1", "iPad2,2", "iPad2,3", "iPad2,4":return "iPad 2"
        case "iPad3,1", "iPad3,2", "iPad3,3":           return "iPad 3"
        case "iPad3,4", "iPad3,5", "iPad3,6":           return "iPad 4"
        case "iPad4,1", "iPad4,2", "iPad4,3":           return "iPad Air"
        case "iPad5,3", "iPad5,4":                      return "iPad Air 2"
        case "iPad2,5", "iPad2,6", "iPad2,7":           return "iPad Mini"
        case "iPad4,4", "iPad4,5", "iPad4,6":           return "iPad Mini 2"
        case "iPad4,7", "iPad4,8", "iPad4,9":           return "iPad Mini 3"
        case "iPad5,1", "iPad5,2":                      return "iPad Mini 4"
        case "iPad6,3", "iPad6,4", "iPad6,7", "iPad6,8":return "iPad Pro"
        case "AppleTV5,3":                              return "Apple TV"
        case "i386", "x86_64":                          return "Simulator"
        default:                                        return identifier
        }
    }
    
}

public extension Bundle {
    var displayName: String? {
        return object(forInfoDictionaryKey: "CFBundleDisplayName") as? String
    }
    
    var releaseVersionNumber: String? {
        return self.infoDictionary?["CFBundleShortVersionString"] as? String
    }
    
    var buildVersionNumber: String? {
        return self.infoDictionary?["CFBundleVersion"] as? String
    }
}
