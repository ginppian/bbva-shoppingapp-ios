//
//  mobileNucleoSDK.h
//  mobileNucleoSDK
//
//  Created by Rajiv Dutta on 11/17/16.
//  Copyright © 2016 BBVA. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for mobileNucleoSDK.
FOUNDATION_EXPORT double mobileNucleoSDKVersionNumber;

//! Project version string for mobileNucleoSDK.
FOUNDATION_EXPORT const unsigned char mobileNucleoSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <mobileNucleoSDK/PublicHeader.h>


