//
//  BGMUICatalog.h
//  BGMUICatalog
//
//  Created by Enrique Ricalde on 4/5/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for BGMUICatalog.
FOUNDATION_EXPORT double BGMUICatalogVersionNumber;

//! Project version string for BGMUICatalog.
FOUNDATION_EXPORT const unsigned char BGMUICatalogVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <BGMUICatalog/PublicHeader.h>


