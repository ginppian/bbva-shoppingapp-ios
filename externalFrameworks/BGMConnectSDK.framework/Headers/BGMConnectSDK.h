//
//  BGMConnectSDK.h
//  BGMConnectSDK
//
//  Created by Manish Sama on 9/20/17.
//  Copyright © 2017 BBVADCC. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for BGMConnectSDK.
FOUNDATION_EXPORT double BGMConnectSDKVersionNumber;

//! Project version string for BGMConnectSDK.
FOUNDATION_EXPORT const unsigned char BGMConnectSDKVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <BGMConnectSDK/PublicHeader.h>

#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/sysctl.h>
#include <stdlib.h>
