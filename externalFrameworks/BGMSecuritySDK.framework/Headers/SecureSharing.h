/********* SecureSharing.m SecureSharing interface *******/

#import <Foundation/Foundation.h>

@interface SecureSharing : NSObject

//Initializers
- (instancetype) initWithContext:(NSString *)accessGroup password:(NSString *)password;

//Query Methods
- (NSDictionary *) contains:(NSString *)key;

//Getter Methods
- (NSDictionary *) getItem:(NSString *)key;
- (NSDictionary *) getItem:(NSString *)key defaultValue:(NSString *)defaultValue;
- (NSDictionary *) getItem:(NSString *)key defaultValue:(NSString *)defaultValue password:(NSString *)password;

//Setter Methods
- (NSDictionary *) putItem:(NSString *)key value:(NSString *)value;
- (NSDictionary *) putItem:(NSString *)key value:(NSString *)value password:(NSString *)password;

//Destructive Methods
- (NSDictionary *) removeItem:(NSString *)key;


@end
