//
//  SSLPinning.h
//  SSLPinning
//
//  Created by Oriol Bellet on 3/6/16.
//  Copyright © 2016 Vintegris S.L. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SSLCheckerProtocol.h"
#import "ErrorInfo.h"

@interface SSLPinning : NSObject

+(SSLPinning*)getInstance:(NSString*)cfgFilePath error:(NSError**)error;
+(SSLPinning*)getInstance:(NSString*)cfgFilePath withResetData:(BOOL)resetData error:(NSError**)error;
-(void)updateCatalog;
-(ErrorInfo*)lastUpdateError;
-(id <SSLCheckerProtocol>)checker;

@end

