//
//  VTReturnCode.h
//  VinTokenSDK
//
//  Created by Ferran Rigual on 8/22/13.
//  Copyright (c) 2013 Vintegris. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "VTErrorCode.h"

@interface VTReturnCode : NSObject

-(instancetype)init;
-(instancetype)initWithRc:(NSUInteger)rc;
-(instancetype)initWithRc:(NSUInteger)rc withDescription:(NSString*)description;

-(void)setRC:(NSUInteger)rc;
-(void)setRC:(NSUInteger)rc withDescription:(NSString*)description;

-(NSUInteger)getRC;
-(NSString*)getDescription;

@end
