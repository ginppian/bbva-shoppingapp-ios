//
//  TokenType.h
//  VinTokenSDK
//
//  Created by Ferran Rigual on 8/22/13.
//  Copyright (c) 2013 Vintegris. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, TokenTypeEnum) {
    TOTP,
    HOTP,
    CHROTP
};

@interface TokenType : NSObject <NSCopying>

@property (nonatomic) TokenTypeEnum type;

-(id)initWithType: (TokenTypeEnum)type;
-(id)initWithTypeString:(NSString*)typeString;

+(NSDictionary*)getTypeNames;

-(void)setTypeWithString:(NSString*)str;
-(NSString *)getString;

@end
