//
//  HostData.h
//  LibMCrypt
//
//  Created by Karim Razak on 27/2/15.
//  Copyright (c) 2015 Karim Razak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface HostData : NSObject

@property NSString *hostData;
@property NSString *dataCheck;

@end
