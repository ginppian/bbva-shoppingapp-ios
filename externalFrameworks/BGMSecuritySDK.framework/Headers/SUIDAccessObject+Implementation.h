//
//  SUIDAccessObject+Implementation.h
//
//  Created by BBVA on 22/11/16.
//
//

#import <Foundation/Foundation.h>

#import "SUIDAccessObject.h"
#import "SecureSharing.h"
#import "MinSecureSharing.h"

@interface SUIDAccessObject (Implementation)

@property (nonatomic, strong)  SecureSharing* secureSharing;
@property (nonatomic, strong)  MinSecureSharing* minSecureSharing;

- (void)initializeDMZKeychainStoreItem:(BOOL)isDMZ password:(NSString *)password context:(NSString *)context andCompletion:(void (^)(NSDictionary *dictResult))completionBlock;

- (void)removeKeychainStoreItem:(BOOL)isDMZ key:(NSString *)key andCompletion:(void (^)(NSDictionary *dictResult))completionBlock;

- (void)containsKeychainStoreItem:(BOOL)isDMZ key:(NSString *)key andCompletion:(void (^)(NSDictionary *dictResult))completionBlock;

- (void)putKeychainStoreItem:(BOOL)isDMZ key:(NSString *)key value:(NSString *)value password:(NSString *)password andCompletion:(void (^)(NSDictionary *dictResult))completionBlock;

- (void)getKeychainStoreItem:(BOOL)isDMZ key:(NSString *)key defaultValue:(NSString *)defaultValue password:(NSString *)password andCompletion:(void (^)(NSDictionary * dictResult))completionBlock;

@end
