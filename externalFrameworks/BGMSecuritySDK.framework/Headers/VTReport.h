//
//  VTReport.h
//  VinTokenSDK
//
//  Created by Oficina Tecnica on 06/11/14.
//  Copyright (c) 2014 Vintegris. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "VTReturnCode.h"

@interface VTReport : NSObject

@property(nonatomic,strong) VTReturnCode* rc;
@property(nonatomic,strong) NSString* report;

-(instancetype)initWithRc:(VTReturnCode*)rc andReport:(NSString*)report;
-(instancetype)initWithReport:(NSString*)report;
-(instancetype)initWithReturnCode:(VTReturnCode *)rc;
-(NSString*)getRcDesc;


@end
