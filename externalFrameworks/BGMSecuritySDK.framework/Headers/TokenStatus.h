//
//  TokenStatus.h
//  VinTokenSDK
//
//  Created by Ferran Rigual on 8/22/13.
//  Copyright (c) 2013 Vintegris. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, TokenStatusEnum) {
    ACTIVE,
    INACTIVE,
    REVOKED,
    SYNCFAILED,
    UNK
};

@interface TokenStatus : NSObject <NSCopying>

@property (nonatomic) TokenStatusEnum status;

+(NSDictionary*)getStatusNames;
+(bool)isValidStatus:(NSString*)str;

-(id)initWithStatus:(TokenStatusEnum)status;

-(void)setStatusWithString:(NSString*)str;
- (NSString *)getString;
@end
