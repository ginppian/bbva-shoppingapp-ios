/**LICENSE**/
/********* SUIDAccessObject.h Object Interface *******/

#import <Foundation/Foundation.h>

static const NSString *kPassword;
static const NSString *kKey;
static const NSString *kValue;
static const NSString *kDefaultValue;
static const NSString *kSharedGroup;
static const NSString *kArguments;

@interface SUIDAccessObject : NSObject

/**
 *  Initializer for UIDAccessObject with keychain access group
 *
 *  @param accessGroup Keychain access group
 */
- (instancetype)initWithGroup:(NSString *)accessGroup;

/**
 *  Getter for the UID value and store it, if it isn't stored previously
 *
 *  @param onSuccess block of code that is executed when the getter ends with success
 *  @param onError block of code that is executed when the getter ends with fail
 */
- (void)getUIDShortened: (BOOL)shortened onSuccess:(void (^)(NSString *))onSuccess onError:(void (^)(NSString *))onError;
/**
 *  Remove the UID stored if it was stored previously
 *
 *  @param onSuccess block of code that is executed when the remove method ends with success
 *  @param onError block of code that is executed when the remove method ends with fail
 */
- (void)removeElementOnSuccess:(void (^)(NSString *))onSuccess onError:(void (^)(NSString *))onError;

@end
