//
//  VinTokenSDK.h
//  VinTokenSDK
//
//  Created by Ferran Rigual on 8/22/13.
//  Copyright (c) 2013 Vintegris. All rights reserved.
//

#import <Foundation/Foundation.h>

@class VTArray;
@class VTToken;
@class TokenPub;
@class TokenType;
@class VTReturnCode;
@class VTOtp;
@class VTReport;
@class VTSRepositoryOperations;

@interface VinTokenSDK : NSObject

+(VinTokenSDK*)getInstance;

+(void) initAccessGroup:(NSString*)accGroup;

+(void) initAccessGroup:(NSString*)accGroup withMigrationMode:(int) migrationMode;

+(void) initRepositoryMode: (VTSRepositoryOperations*) operations;

-(VTArray*)listTokens:(TokenType*)type;

-(VTToken*)getToken:(NSString*)refTok;

-(VTReturnCode*)deleteToken:(NSString*)refTok;

-(VTToken*)setTokenDesc:(NSString*)refTok withName:(NSString*)name;

-(VTOtp*)genOtp:(NSString*)refTok withPin:(NSString*)pin andChallenge:(NSString*)challenge withType:(TokenType*)type;

-(VTOtp*)genOtp:(NSString*)refTok withPin:(NSString*)pin andChallenge:(NSString*)challenge;

-(VTOtp*)genNextTotp:(NSString*)refTok withPin:(NSString*)pin;

-(VTOtp*)genNextOtp:(NSString*)refTok withPin:(NSString*)pin andChallenge:(NSString*)challenge withType:(TokenType*)type;

-(VTReturnCode*)syncToken:(NSString*)refTok withPin:(NSString*)pin;

-(VTToken*)registerToken:(NSString*)authCode withPin:(NSString*)pin;

-(VTToken*)registerToken:(NSString*)authCode withPin:(NSString*)pin withRequestAppData:(NSString*)requestAppData;

-(VTToken*)registerToken:(NSString*)authCode withDeviceId:(NSString*)deviceId withSerial:(NSString*)serial withPin:(NSString*)pin withRequestAppData:(NSString*)requestAppData withHeaders:(NSDictionary*)headers;

-(VTToken*)registerTokenByUri:(NSString*)uri withPin:(NSString*)pin;

-(VTReport*)getAppReport:(int)logLines;

@end
