//
//  VTToken.h
//  VinTokenSDK
//
//  Created by Ferran Rigual on 8/22/13.
//  Copyright (c) 2013 Vintegris. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "VTReturnCode.h"
#import "TokenPub.h"

@interface VTToken : TokenPub

@property (nonatomic, strong) VTReturnCode * rc;

-(instancetype)initWithTokenPub:(TokenPub *)token;
-(instancetype)initWithReturnCode:(VTReturnCode *)rc;


@end
