/********* SecureSharingCrypto.h SecureSharingCrypto interface *******/

#import <Foundation/Foundation.h>

@interface SecureSharingCrypto : NSObject

+ (NSString *)encrypt:(NSString *)message password:(NSString *)password;
+ (NSString *)decrypt:(NSString *)base64EncodedString password:(NSString *)password;

@end
