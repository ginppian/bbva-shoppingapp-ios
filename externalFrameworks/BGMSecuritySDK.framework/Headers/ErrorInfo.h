//
//  ErrorInfo.h
//  SSLPinning
//
//  Created by Oriol Bellet on 21/6/16.
//  Copyright © 2016 Vintegris S.L. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "SSLPinningException.h"

@interface ErrorInfo : NSObject

@property (nonatomic) long timestamp;
@property (nonatomic, strong) NSString* desc;
@property (nonatomic) int code;
@property (nonatomic, strong) NSString* name;
@property (nonatomic, strong) NSDictionary* userInfo;

-(id)initWithException:(SSLPinningException*)ex;
-(id)initWithCode:(int)code withDesc:(NSString*)desc withUserInfo:(NSDictionary*)userInfo withName:(NSString*)name;

@end
