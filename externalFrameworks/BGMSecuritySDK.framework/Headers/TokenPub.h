//
//  TokenPub.h
//  VinTokenSDK
//
//  Created by Ferran Rigual on 8/22/13.
//  Copyright (c) 2013 Vintegris. All rights reserved.
//

#import <Foundation/Foundation.h>

@class VTSToken;
@class TokenType;

@interface TokenPub : NSObject

@property (nonatomic, strong) TokenType * type;
@property (nonatomic, strong) NSString * serial;
@property (nonatomic, strong) NSString * desc;
@property (nonatomic) bool needsPin;
@property (nonatomic) bool useQRCode;
@property (nonatomic, strong) NSNumber* totpTimeIncrement;
@property (nonatomic, strong) NSNumber* chrotpTimeIncrement;

-(id)initWithToken:(VTSToken*)token;

@end
