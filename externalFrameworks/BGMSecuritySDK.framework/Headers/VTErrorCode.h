//
//  VTErrorCode.h
//  VinTokenSDK
//
//  Created by Oriol Bellet on 23/6/17.
//  Copyright © 2017 Vintegris. All rights reserved.
//

#ifndef VTErrorCode_h
#define VTErrorCode_h

static const unsigned int VTRC_INIT_BASE = 1000;
static const unsigned int VTRC_TOKEN_BASE = 2000;
static const unsigned int VTRC_CORE_BASE = 3000;
static const unsigned int VTRC_REPOSITORY_BASE = 4000;

// General error codes
static const unsigned int VTRC_OK = 0;
static const unsigned int VTRC_NODEF = 100;

//static const unsigned int VTRC_INIT_GEN = VTRC_INIT_BASE+1;
static const unsigned int VTRC_INIT_NOT_INITED = VTRC_INIT_BASE+2;
static const unsigned int VTRC_INPUT_BAD_PARAM = VTRC_INIT_BASE+3;

// Core error codes
static const unsigned int VTRC_CORE_INTERNAL = VTRC_CORE_BASE+1;
static const unsigned int VTRC_CORE_OPENSSL  = VTRC_CORE_BASE+2;
static const unsigned int VTRC_ERR_CORE_NOT_IMPLEMENTED = VTRC_CORE_BASE+3;

//static const unsigned int VTRC_ERR_ENROLL_PARAMS = VTRC_CORE_BASE + 112;

static const unsigned int VTRC_SYNC_FAILED = VTRC_CORE_BASE+201;
static const unsigned int VTRC_SYNC_MGMT = VTRC_CORE_BASE+202;
static const unsigned int VTRC_SYNC_MGMT_REMOTE = VTRC_CORE_BASE+203;
static const unsigned int VTRC_SYNC_NETWORK = VTRC_CORE_BASE+204;

static const unsigned int VTRC_OTP_GENER = VTRC_CORE_BASE+301;
///static const unsigned int VTRC_OTP_NULL = VTRC_CORE_BASE+302;

// Token error codes
static const unsigned int VTRC_TOKEN_STORE      = VTRC_TOKEN_BASE + 1;
static const unsigned int VTRC_TOKEN_NOT_FOUND  = VTRC_TOKEN_BASE + 2;
static const unsigned int VTRC_TOKEN_EMPTY_LIST = VTRC_TOKEN_BASE + 3;
static const unsigned int VTRC_TOKEN_PARSE_OTP  = VTRC_TOKEN_BASE + 4;

//Enroll errors
static const unsigned int VTRC_ENROLL_NETWORK = 3111;
static const unsigned int VTRC_ENROLL_BAD_AUTHCODE_PIN = 3112;
static const unsigned int VTRC_ENROLL_REQUEST_PARAMS = 3113;

//static const unsigned int VTRC_ENROLL_BAD_CA_CERT = 3121;
static const unsigned int VTRC_ENROLL_SERVER_NOT_FOUND = 3122;
//static const unsigned int VTRC_ENROLL_SERVER_CERT_EXPIRED = 3123;
static const unsigned int VTRC_ENROLL_SERVER_CERT_NO_TRUST = 3124;
//static const unsigned int VTRC_ENROLL_SERVER_CERT_HOSTNAME = 3125;
static const unsigned int VTRC_ENROLL_CONNECTION = 3126;

static const unsigned int VTRC_ENROLL_FREJA_GENERAL                     = 3130;
static const unsigned int VTRC_ENROLL_STS_GENERAL                       = 3131;
static const unsigned int VTRC_ENROLL_STS_PARAMS                        = 3132;
static const unsigned int VTRC_ENROLL_STS_EXPIRED                       = 3133;
static const unsigned int VTRC_ENROLL_STS_DBACCESS                      = 3134;
static const unsigned int VTRC_ENROLL_STS_UNASSIGNED_REF                = 3135;
static const unsigned int VTRC_ENROLL_STS_ACTIVATED                     = 3136;
static const unsigned int VTRC_ENROLL_STS_CYPHER_GEN                    = 3137;
static const unsigned int VTRC_ENROLL_STS_HSM_GEN                       = 3138;
static const unsigned int VTRC_ENROLL_STS_EXC_GEN                       = 3139;
static const unsigned int VTRC_ENROLL_STS_APPCODE_NOT_FOUND_NOR_INVALID = 3160;
static const unsigned int VTRC_ENROLL_STS_APPCODE_NO_MATCH_DDBB         = 3161;
static const unsigned int VTRC_ENROLL_STS_DEVICE_ID_NO_MATCH_DDBB       = 3162;

static const unsigned int VTRC_ENROLL_SOAP_FAULT = 3141;
static const unsigned int VTRC_ENROLL_BAD_RESPONSE= 3142;
static const unsigned int VTRC_ENROLL_BAD_RESPONSE_PARAM= 3143;
static const unsigned int VTRC_ENROLL_PARSING_RESPONSE= 3144;

//static const unsigned int VTRC_ENROLL_TOKEN_GENERATION = 3151;
//static const unsigned int VTRC_ENROLL_TOKEN_STORING = 3152;

//errores del repositorio de passwords
//static const unsigned int VTRC_ERR_PIN_MGMT          = VTRC_CORE_BASE+501;
static const unsigned int VTRC_ERR_PIN_NOT_FOUND 	= VTRC_CORE_BASE+502;
static const unsigned int VTRC_ERR_PIN_REPO          = VTRC_CORE_BASE+503;

//Errores de regsitro mediante keyUri
static const unsigned int VTRC_ERR_KEYURI_PARAMS         = VTRC_CORE_BASE + 601;
static const unsigned int VTRC_ERR_KEYURI_FORMAT         = VTRC_CORE_BASE + 602;
static const unsigned int VTRC_ERR_URI_NULL              = VTRC_CORE_BASE + 603;
static const unsigned int VTRC_ERR_INVALID_SCHEME        = VTRC_CORE_BASE + 604;
static const unsigned int VTRC_ERR_INVALID_TYPE          = VTRC_CORE_BASE + 605;
static const unsigned int VTRC_ERR_INVALID_SECRET        = VTRC_CORE_BASE + 606;
static const unsigned int VTRC_ERR_INVALID_ALGORITHM     = VTRC_CORE_BASE + 607;
static const unsigned int VTRC_ERR_INVALID_DIGITS        = VTRC_CORE_BASE + 608;
static const unsigned int VTRC_ERR_INVALID_PERIOD        = VTRC_CORE_BASE + 609;
static const unsigned int VTRC_ERR_SECRET_FORMAT         = VTRC_CORE_BASE + 610;

#endif /* VTErrorCode_h */
