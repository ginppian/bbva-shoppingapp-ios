

#define kErrorCodeSecureSharingInstanceNotInitialized       701
#define kErrorMessageSecureSharingInstanceNotInitialized    @"Storage is not initialized"

static NSString * SUCCESS = @"success";
static NSString * SUCCESS_MESSAGE = @"OK";
static NSString * ERROR   = @"error";
static NSString * RESPONSE_CODE = @"code";
static NSString * RESPONSE_MESSAGE = @"message";
static NSString * ERROR_PERMISSION_DENIED = @"002";
