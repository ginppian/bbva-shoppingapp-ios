//
//  kpubData.h
//  LibMCrypt
//
//  Created by Karim Razak on 27/2/15.
//  Copyright (c) 2015 Karim Razak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface KpubData : NSObject

@property NSString *kPubValue;
@property NSString *kPubCheck;

@end
