//
//  SSLPinningExcpetion.h
//  SSLPinning
//
//  Created by Oriol Bellet on 7/6/16.
//  Copyright © 2016 Vintegris S.L. All rights reserved.
//

#import "ErrorCodeException.h"

@interface SSLPinningException : ErrorCodeException


@end
