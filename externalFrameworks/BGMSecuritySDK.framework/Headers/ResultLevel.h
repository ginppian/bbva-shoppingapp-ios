//
//  ResultLevel.h
//  SSLPinning
//
//  Created by Oriol Bellet on 7/6/16.
//  Copyright © 2016 Vintegris S.L. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ResultLevel : NSObject

typedef NS_ENUM(NSUInteger, ResultLevelEnum) {

    SSLP_SUCCESS = 0,
    SSLP_WARNING = 1,
    SSLP_FAILED = 2
    
};

@property (nonatomic) int severity;

-(id)initWithSeverity:(int)severity;
-(BOOL)isWorseThan:(ResultLevel*)other;

@end
