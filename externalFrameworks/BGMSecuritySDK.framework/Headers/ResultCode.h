//
//  ResultCode.h
//  SSLPinning
//
//  Created by Oriol Bellet on 21/6/16.
//  Copyright © 2016 Vintegris S.L. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ResultCode : NSException

#define RC_SUCCESS                0
#define ERROR_NOTSET             -1

#define ERR_BASE_INIT			100
#define ERR_BASE_CATALOG		200
#define ERR_BASE_CONN			300
#define ERR_BASE_REPO			400
#define ERR_BASE_TRUST			500
#define ERR_BASE_HOSTVERIF		600
#define ERR_BASE_STRENGTH		700
#define ERR_BASE_UPDATER		800
#define ERR_BASE_SIGNER         900

/// Initialization
#define ERROR_INIT_CLASS                    ERR_BASE_INIT + 1
#define ERROR_INIT_RESET                    ERR_BASE_INIT + 2
#define ERROR_INIT_NODEVICEUID              ERR_BASE_INIT + 3
#define ERROR_INIT_NOCONFIG                 ERR_BASE_INIT + 4
#define ERROR_INIT_INVALIDCONFIG            ERR_BASE_INIT + 5
#define ERROR_INIT_MISSINGPARAM             ERR_BASE_INIT + 6
#define ERROR_INIT_PUBKEY                   ERR_BASE_INIT + 7

#define ERROR_SIGNATURE_VERIFICATION        ERR_BASE_INIT + 11
#define ERROR_SIGNATURE_GENERATION          ERR_BASE_INIT + 12

#define ERROR_XML_ENCODING                  ERR_BASE_INIT + 31
#define ERROR_XML_IO                        ERR_BASE_INIT + 32
#define ERROR_XML_SAX                       ERR_BASE_INIT + 33
#define ERROR_XML_PARSE                     ERR_BASE_INIT + 34
#define ERROR_XML_TRANSFORM                 ERR_BASE_INIT + 35

/// Catalog
#define ERROR_SITECATALOG_REVISION          ERR_BASE_CATALOG + 1
#define ERROR_SITECATALOG_SIGNATURE         ERR_BASE_CATALOG + 2
#define ERROR_SITECATALOG_NOTFOUND          ERR_BASE_CATALOG + 3
#define ERROR_SITECATALOG_VERSION           ERR_BASE_CATALOG + 4
#define ERROR_SITECATALOG_HOST_NOT_FOUND 	ERR_BASE_CATALOG + 5

/// Connection
#define ERROR_CONN_INVALID_HOST             ERR_BASE_CONN + 1
#define ERROR_CONN_GENERIC_IO               ERR_BASE_CONN + 2
#define ERROR_CONN_PROTO_NO_HTTPS           ERR_BASE_CONN + 3
#define ERROR_CONN_CONNFAIL                 ERR_BASE_CONN + 4
#define ERROR_CONN_TIMEOUT                  ERR_BASE_CONN + 5
#define ERROR_CONN_SRVCERT_INVALID          ERR_BASE_CONN + 6
#define ERROR_CONN_INVALID_URL              ERR_BASE_CONN + 7
#define ERROR_CONN_UNKNOWNHOST              ERR_BASE_CONN + 8

/// SSL
#define ERROR_CONN_SSL_GENERIC              ERR_BASE_CONN + 50
#define ERROR_CONN_SSL_INIT                 ERR_BASE_CONN + 51
#define ERROR_CONN_SSL_HANDSHAKE            ERR_BASE_CONN + 52
#define ERROR_CONN_SSL_KEY                  ERR_BASE_CONN + 53
#define ERROR_CONN_SSL_PROTOCOL             ERR_BASE_CONN + 54
#define ERROR_CONN_SSL_UNVERIFIED           ERR_BASE_CONN + 55

/// Repository
#define ERROR_REPO_INIT                     ERR_BASE_REPO + 1
#define ERROR_REPO_LOADCATALOG              ERR_BASE_REPO + 2
#define ERROR_REPO_SAVECATALOG              ERR_BASE_REPO + 3
#define ERROR_REPO_CORRUPT                  ERR_BASE_REPO + 4

/// Verif Generic
#define ERROR_EXCEPTION_GENERIC             ERR_BASE_TRUST + 1
#define ERROR_EXCEPTION_CERTIFICATE         ERR_BASE_TRUST + 2
#define ERROR_EXCEPTION_SSL                 ERR_BASE_TRUST + 3

/// Trust verif
#define ERROR_TRUST_VERIFIER_INIT               ERR_BASE_TRUST + 11
#define ERROR_TRUST_UNTRUSTED_CERT              ERR_BASE_TRUST + 12
#define ERROR_TRUST_CERT_DATE_AFTERVALIDITY     ERR_BASE_TRUST + 21
#define ERROR_TRUST_CERT_DATE_BEFOREVALIDITY    ERR_BASE_TRUST + 22

/// Host verif
#define ERROR_HOSTVERIF_NOMATCH             ERR_BASE_HOSTVERIF + 1
#define ERROR_HOSTVERIF_NOCN                ERR_BASE_HOSTVERIF + 2

/// Strength verif
#define ERROR_STRENGTH_PUBKEY               ERR_BASE_STRENGTH + 1
#define ERROR_STRENGTH_SUITEBLACK           ERR_BASE_STRENGTH + 2
#define ERROR_STRENGTH_SUITENOWHITE         ERR_BASE_STRENGTH + 3

/// Updater
#define ERROR_UPDATER_GENERAL               ERR_BASE_UPDATER + 1
#define ERROR_UPDATER_SSL                   ERR_BASE_UPDATER + 2
#define ERROR_UPDATER_HTTP                  ERR_BASE_UPDATER + 3
#define ERROR_UPDATER_IO                    ERR_BASE_UPDATER + 4
#define ERROR_UPDATER_CONN                  ERR_BASE_UPDATER + 5
#define ERROR_UPDATER_INVALIDATED           ERR_BASE_UPDATER + 6

/// Signer tool
#define ERROR_SIGNER_CONFIG                 ERR_BASE_SIGNER + 1
#define ERROR_SIGNER_KEYSTOREPWD            ERR_BASE_SIGNER + 2
#define ERROR_SIGNER_KEYSTORE               ERR_BASE_SIGNER + 3
#define ERROR_SIGNER_KEYSTOREIO             ERR_BASE_SIGNER + 4

@end
