//
//  ErrorCodeException.h
//  SSLPinning
//
//  Created by Oriol Bellet on 7/6/16.
//  Copyright Â© 2016 Vintegris S.L. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ErrorCodeException : NSException

@property (nonatomic) int errCode;

-(id)initWithName:(NSString *)aName reason:(NSString *)aReason userInfo:(NSDictionary *)aUserInfo errCode:(int)errCode;
-(id)initWithErrCode:(int)errCode;
-(NSString*)getMessage;

@end