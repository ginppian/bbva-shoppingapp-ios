//
//  VTOtp.h
//  VinTokenSDK
//
//  Created by Ferran Rigual on 8/22/13.
//  Copyright (c) 2013 Vintegris. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "VTReturnCode.h"

@interface VTOtp : NSObject

@property (nonatomic, strong) VTReturnCode * rc;
@property (nonatomic, strong) NSString * otp;

-(instancetype)initWithOtp:(NSString*)otp;
-(instancetype)initWithReturnCode:(VTReturnCode *)rc;

@end
