//
//  NSString(HexString).h
//  BBVA-UIDSecureStorage
//
//  Created by Intel on 22/2/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (HexString)

+ (NSString *)hexadecimalStringFromData:(NSData *)dataIn;

@end
