/********* KeychainStoreItem.h KeychainStoreItem interface *******/

#import <Foundation/Foundation.h>

@interface KeychainStoreItem : NSObject

- (instancetype)initWithService:(NSString *)service accessGroup:(NSString *)accessGroup;

- (BOOL) containsKeychainItem:(NSString *)key;
- (NSString*) getKeychainItem:(NSString *)key;
- (BOOL) setKeychainItem:(NSString *)key withValue:(NSString *)value;
- (BOOL) updateKeychainItem:(NSString *)key withValue:(NSString *)value;
- (BOOL) removeKeychainItem:(NSString *)key;

@end
