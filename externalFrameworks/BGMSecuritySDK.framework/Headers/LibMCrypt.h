//
//  LibMCrypt.h
//  LibMCrypt
//
//  Created by Karim Razak on 10/2/15.
//  Copyright (c) 2015 Vintegris. All rights reserved.
//
#import "KpubData.h"
#import "HostData.h"
#import <Foundation/Foundation.h>

@interface LibMCrypt : NSObject

-(KpubData*)generateKpub:(NSString*)infoToKpubCheck
            __attribute__((deprecated("Use generateKpub:error: instead.")));

-(NSString*)decryptHostData:(NSString *)hostData
             __attribute__((deprecated("Use decryptHostData:error: instead.")));

-(HostData*)encryptHostData:(NSString *)dataToHost infoToDataCheck:(NSString*)infoToDataCheck
            __attribute__((deprecated("Use encryptHostData:infoToDataCheck:error: instead.")));

-(KpubData*)generateKpub:(NSString*)infoToKpubCheck error:(NSError**)error;
-(NSString*)decryptHostData:(NSString *)hostData error:(NSError**)error;
-(HostData*)encryptHostData:(NSString *)dataToHost infoToDataCheck:(NSString*)infoToDataCheck error:(NSError**)error;
-(HostData*)encryptHostData:(NSString *)dataToHost infoToDataCheck:(NSString*)infoToDataCheck withKeyTag:(NSString*)keyTag error:(NSError**)error;

@end
