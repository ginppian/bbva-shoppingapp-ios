/********* MinSecureSharing.h MinSecureSharing interface *******/

#import <Foundation/Foundation.h>

@interface MinSecureSharing : NSObject

//Initializers
- (instancetype) initWithContext:(NSString *)accessGroup;

//Query Methods
- (NSDictionary *) contains:(NSString *)key;

//Getter Methods
- (NSDictionary *) getItem:(NSString *)key;
- (NSDictionary *) getItem:(NSString *)key defaultValue:(NSString *)defaultValue;

//Setter Methods
- (NSDictionary *) putItem:(NSString *)key value:(NSString *)value;

//Destructive Methods
- (NSDictionary *) removeItem:(NSString *)key;

@end
