//
//  CheckResult.h
//  SSLPinning
//
//  Created by Oriol Bellet on 7/6/16.
//  Copyright © 2016 Vintegris S.L. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ResultLevel.h"

@interface CheckResult : NSObject

@property (nonatomic, strong) ResultLevel* level;
@property (nonatomic) int code;
@property (nonatomic, strong) NSString* desc;

-(id)initWithLevel:(ResultLevel*)level withCode:(int)code withDesc:(NSString*)desc;

-(BOOL)isFailed;
-(BOOL)isSuccess;

+(CheckResult*)OK;
-(CheckResult*)supersede:(CheckResult*)target;

-(NSString*)stringValue;

@end
