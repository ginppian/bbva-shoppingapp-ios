/********* NSString+Base64Additions.h NSString+Base64Additions interface *******/

#import <Foundation/NSString.h>

@interface NSString (Base64Additions)

+ (NSString *)base64StringFromData:(NSData *)data length:(NSUInteger)length;

@end
