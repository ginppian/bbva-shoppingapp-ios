//
//  VTArray.h
//  VinTokenSDK
//
//  Created by Ferran Rigual on 8/22/13.
//  Copyright (c) 2013 Vintegris. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "VTReturnCode.h"

@interface VTArray : NSObject

-(instancetype)initWithArray:(NSArray *)array;
-(instancetype)initWithReturnCode:(VTReturnCode *)rc;

@property (nonatomic, strong) NSArray * array;
@property (nonatomic, strong) VTReturnCode * rc;

@end
