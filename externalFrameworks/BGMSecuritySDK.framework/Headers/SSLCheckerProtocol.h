//
//  SSLCheckerProtocol.h
//  SSLPinning
//
//  Created by Oriol Bellet on 5/7/16.
//  Copyright © 2016 Vintegris S.L. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CheckResult.h"

@protocol SSLCheckerProtocol

-(CheckResult*) checkUrl:(NSString*) urlStr;
-(CheckResult*) checkEstablished:(NSURLProtectionSpace*) connection;

@end
