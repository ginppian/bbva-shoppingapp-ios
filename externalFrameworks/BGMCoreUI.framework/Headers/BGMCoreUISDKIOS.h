//
//  BGMCoreUISDKIOS.h
//  BGMCoreUISDKIOS
//
//  Created by Manish Sama on 11/22/16.
//  Copyright © 2016 BBVA. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for BGMCoreUISDKIOS.
FOUNDATION_EXPORT double BGMCoreUISDKIOSVersionNumber;

//! Project version string for BGMCoreUISDKIOS.
FOUNDATION_EXPORT const unsigned char BGMCoreUISDKIOSVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <BGMCoreUISDKIOS/PublicHeader.h>


