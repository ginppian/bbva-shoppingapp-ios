//
//  BGMAnalytics.h
//  BGMAnalytics
//
//  Created by Bala Gourabathina on 1/23/17.
//  Copyright © 2017 BBVA. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for BGMAnalytics.
FOUNDATION_EXPORT double BGMAnalyticsVersionNumber;

//! Project version string for BGMAnalytics.
FOUNDATION_EXPORT const unsigned char BGMAnalyticsVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <BGMAnalytics/PublicHeader.h>

#import "TrackingHelper.h"
