echo  "<?xml version=\"1.0\" encoding=\"UTF-8\"?> 
<!DOCTYPE plist PUBLIC \"-//Apple//DTD PLIST 1.0//EN\" \"http://www.apple.com/DTDs/PropertyList-1.0.dtd\">
<plist version=\"1.0\">
<dict>
<key>items</key>
<array>
<dict>
<key>assets</key>
<array>
<dict>
<key>kind</key>
<string>software-package</string>
<key>url</key>
<string>$1</string>
</dict>
<dict>
<key>kind</key>
<string>display-image</string>
<key>url</key>
<string>https://is3-ssl.mzstatic.com/image/thumb/Purple118/v4/46/60/84/46608427-2b31-e34a-dd7a-c7abb8baf111/AppIcon-1x_U007emarketing-sRGB-85-220-5.png/57x0w.jpg</string>
</dict>
<dict>
<key>kind</key>
<string>full-size-image</string>
<key>url</key>
<string>https://is3-ssl.mzstatic.com/image/thumb/Purple118/v4/46/60/84/46608427-2b31-e34a-dd7a-c7abb8baf111/AppIcon-1x_U007emarketing-sRGB-85-220-5.png/512x0w.jpg</string>
</dict>
</array>
<key>metadata</key>
<dict>
<key>bundle-identifier</key>
<string>$2</string>
<key>bundle-version</key>
<string>$3</string>
<key>kind</key>
<string>software</string>
<key>title</key>
<string>ShoppingApp</string>
</dict>
</dict>
</array>
</dict>
</plist>" > manifest_$4.plist
